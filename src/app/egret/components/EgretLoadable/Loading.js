import React, {useContext} from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import ConstantList from "../../../app/appConfig";
import AppContext from "../../../app/appContext";
import {DEFAULT_ICON, ICON_BY_THEME} from "../../../app/appConst";

const useStyles = makeStyles(theme => ({
  loading: {
    position: "fixed",
    left: 0,
    right: 0,
    top: "calc(50% - 20px)",
    margin: "auto",
    height: "40px",
    width: "40px",
    "& img": {
      position: "absolute",
      height: "20px",
      width: "auto",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      margin: "auto"
    },
    "& .MuiCircularProgress-svg": {
      scale: 1.3
    }
  }
}));

const Loading = props => {
  const classes = useStyles();
	const {theme} = useContext(AppContext);
	const iconUrls = ICON_BY_THEME[theme?.code] || DEFAULT_ICON;

	return (
    <div className={classes.loading}>
      <img src={iconUrls.loading} alt="" />
      <CircularProgress thickness={4} color="primary"/>
    </div>
  );
};

export default Loading;
