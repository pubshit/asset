import React from "react";
import { Dialog, Button,DialogActions } from "@material-ui/core";
import PropTypes from "prop-types";
const ConfirmationDialog = ({
  open,
  onConfirmDialogClose,
  text,
  title,
  agree,
  cancel,
  onYesClick,
	size
}) => {
  return (
    <Dialog
      maxWidth={size || "xs"}
      fullWidth
      open={open}
      onClose={onConfirmDialogClose}
    >
      <div className="pt-24 px-20 pb-8">
        <h4 className="capitalize">{title}</h4>
				<p dangerouslySetInnerHTML={{__html: text}}></p>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-12"
              onClick={onConfirmDialogClose}
            >
              {cancel}
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={onYesClick}
            >
             {agree}
            </Button>
          </div>
        </DialogActions>
      </div>
    </Dialog>
  );
};

export default ConfirmationDialog;
ConfirmationDialog.propTypes = {
	size: PropTypes.oneOf(["xl", "xs", "sm", "md", "lg"])
}
