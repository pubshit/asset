import axios from "axios";
import ConstantList from "app/appConfig";
import { STATUS_DEPARTMENT, appConst } from "app/appConst";


const API_PATH_REQUIREMENT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/resources/requirements";
const API_PATH_DOC_TEMPLATE = ConstantList.API_ENPOINT_ASSET_MAINTANE +  "/api/doc-template-configs";
const API_PATH_RESOURCE_FILE = ConstantList.API_ENPOINT_ASSET_MAINTANE +  "/resources/files";
const API_PATH_VOUCHER_ATTRIBUTES = ConstantList.API_ENPOINT_ASSET_MAINTANE +  "/api/voucher-attributes";
export const SSE_PATH = API_PATH_REQUIREMENT + '/realtime';

const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;

export const getManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/management-departments",
    config
  );
};

export const connectRealtime = (config) => {
  return new EventSource(SSE_PATH, config)
}

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + linkFile,
    responseType: "blob",
  });
};

export const getTemplateByModel = (params) => {
  return axios.get(API_PATH_DOC_TEMPLATE, {params});
}

export const getResourceTemplate = (params) => {
  return axios({
    method: "get",
    url: API_PATH_RESOURCE_FILE + "/template/" + params?.templateId,
    params,
    responseType: "blob",
  });
}

export const exportToWord = (url, searchObject) => {
  return axios.get(url, {
    params: { ...searchObject },
    responseType: "blob",
  });
};

export const getVoucherAttributes = (params) => {
  return axios.get(API_PATH_VOUCHER_ATTRIBUTES + "/voucher", {params});
}

export const getInventoryVoucherAttributes = (params) => {
  return axios.get(API_PATH_VOUCHER_ATTRIBUTES + "/inventory", {params});
}

export const getDxVoucherAttributes = (params) => {
  return axios.get(API_PATH_VOUCHER_ATTRIBUTES + "/de-xuat", {params});
}

export const saveVoucherAttributes = (payload) => {
  return axios.post(API_PATH_VOUCHER_ATTRIBUTES, payload);
}
