import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  withStyles,
  MuiThemeProvider,
} from "@material-ui/core";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { logoutUser } from "app/redux/actions/UserActions";
import { PropTypes } from "prop-types";
import { isMdScreen } from "utils";
import TopBarMenu from "./TopBarMenu";

const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.primary.main,
  },
});

class Layout1Topbar extends Component {
  state = {};

  render() {
    const {t, theme, settings} = this.props;

    const topbarTheme = settings.themes[settings.layout1Settings.topbar.theme] || theme;
    return (
      <MuiThemeProvider theme={topbarTheme}>
        <div className="topbar">
          <TopBarMenu t={t} />
        </div>
      </MuiThemeProvider>
    );
  }
}

Layout1Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings,
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, { setLayoutSettings, logoutUser })(Layout1Topbar)
  )
);
