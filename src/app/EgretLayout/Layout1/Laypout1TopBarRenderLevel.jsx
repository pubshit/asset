import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {AppBar, Card, CardContent, Grid, Icon, Popover, Tab, Tabs} from '@material-ui/core';
import TouchRipple from '@material-ui/core/ButtonBase';
import {TabPanel, a11yProps} from 'app/appFunction';
import {NavLink} from "react-router-dom";
import {withRouter} from 'react-router-dom';
import ConstantList from "app/appConfig";
import {useHistory} from 'react-router-dom/cjs/react-router-dom.min';

const Laypout1RenderLevel = (props) => {
  const {data, t} = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [tabValue, setTabValue] = useState(0);
  const [tabActive, setTabActive] = useState(null);
  const [tabItemActive, setTabItemActive] = useState(0);
  const [isHasChildren, setIsHasChildren] = useState(false)
  const history = useHistory();
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  
  const handleCheckChildren = (data, count = 0) => {
    data = data?.children ? data.children : data;
    let children = []
    data?.filter(item => {
      if(item?.children?.length > 0) {
        children = [...children, ...item.children]
      }
    })
    if (children?.length > 0) {
      count++
      handleCheckChildren(children, count)
      return
    }
    setIsHasChildren(count > 1)
  }
  useEffect(() => {
    setTabValue(0)
  }, []);
  
  const handleClick = (event, index, data) => {
    if (data?.path?.length > 0 && data?.children?.length === 0) {
      data.path = !data?.path?.startsWith(ConstantList.ROOT_PATH) ? ConstantList.ROOT_PATH + data?.path : data?.path
      history.push(data.path)
    } else {
      setAnchorEl(event.currentTarget);
      setTabActive(index);
      handleCheckChildren(data);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
    setTabActive(null);
  };

  const handleChangeTabValue = (e, newValue) => {
    setTabValue(newValue);
  };

  const renderListItem = (item = {}, arrayItemChildren = []) => {
    return item?.children?.length > 0
      ? <>
        {item?.children?.map((itemChildren, index) => {
          let visible = item.isVisible;
          if (
            itemChildren?.path?.length > 0
            && !itemChildren?.path?.startsWith(ConstantList.ROOT_PATH)
          ) {
            itemChildren.path = ConstantList.ROOT_PATH + itemChildren.path;
          }
          if (itemChildren.children && itemChildren.children.length > 0) {
            arrayItemChildren?.push(itemChildren);
          } else if (visible) {
            return (
              <div key={index}>
                {index === 0 && !isHasChildren && (
                  <div className="pl-10 pb-0 font-weight-bold text-uppercase text-muted âa">
                    {t(item?.name)}
                  </div>
                )}
                <NavLink key={index} to={itemChildren.path} className="label-menu-item">
                  <TouchRipple key={itemChildren.name} name="child" className="w-100">
                    {t(itemChildren.name)}
                  </TouchRipple>
                </NavLink>
              </div>
            )
          }
        })}
      </>
      : (
        <NavLink
          key={item?.id}
          to={item?.path?.length > 0 && !item?.path?.startsWith(ConstantList.ROOT_PATH)
            ? ConstantList.ROOT_PATH + item?.path
            : item?.path
          }
          className="label-menu-item">
          <TouchRipple key={item?.name} name="child" className="w-100">
            {t(item?.name)}
          </TouchRipple>
        </NavLink>
      )
  };
  
  const renderLevel = (item, isSubMenu) => {
    let arrayItemChildren = []
    return (
      <div className={isSubMenu ? "" : 'array-item-children-container'}>
        {isSubMenu
          ? <div>
            {renderListItem(item, arrayItemChildren)}
          </div>
          : renderListItem(item, arrayItemChildren)
        }

        {arrayItemChildren?.map((itemChildren, index) => {
          return (
            <div key={index} className="pl-10 pb-0 font-weight-bold text-uppercase text-muted aaa">
              {t(itemChildren.name)}
              {renderLevel(itemChildren, true)}
            </div>
          )
        })}
      </div>
    )
  };
  
  const calculationAnchorPosition = () => {
    let top = anchorEl?.getBoundingClientRect()?.bottom + 10; // set anchor position to bottom button
    let left = anchorEl?.getBoundingClientRect()?.left
      - anchorEl?.getBoundingClientRect()?.width
      + 250; // add more a half of anchor element
    
    return {
      top,
      left
    }
  }
  
  const renderLabelItem = (item) => {
    if (
      item?.path?.length > 0
      && !item?.path?.startsWith(ConstantList.ROOT_PATH)
    ) {
      item.path = ConstantList.ROOT_PATH + item.path;
    }
    
    return <div className={item?.children?.length === 0 ? 'label-item no-children' : 'label-item'}>
      {
        item?.children?.length === 0 ?
          <NavLink key={item?.id} to={item.path} className="">
            <Icon fontSize="small" color="disabled" className='icon-item-left'>
              drag_handle
            </Icon>
            {t(item.name)}
          </NavLink>
          :
          <>
            <Icon fontSize="small" color="disabled" className='icon-item-left'>
              drag_handle
            </Icon>
            <span>
            {t(item?.name)}
          </span>
            {
              item?.children?.length > 0 &&
              <Icon fontSize="small" color="disabled" className='icon-item-right'>
                arrow_forward
              </Icon>
            }
          </>
      }
    </div>
  }
  
  return (
    <div className='nav-item-container'>
      <TouchRipple
        key={data.name}
        className={"w-100 nav-item" + (tabActive === data?.id ? " active" : "")}
        aria-describedby={id}
        variant="contained"
        color="primary"
        onClick={(e) => handleClick(e, data?.id, data)}
      >
        <span className='item-text'>
          {t(data?.name)}
        </span>
      </TouchRipple>
      <div className="anchor-container"
           style={{
             position: 'absolute',
             opacity: anchorEl ? 1 : 0,
             top: calculationAnchorPosition().top || "",
             left: calculationAnchorPosition().left || "",
             transition: anchorEl ? `opacity 300ms` : 'opacity 0ms',
           }}
      ></div>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        style={{
          maxHeight: '500px',
        }}
        
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        className={isHasChildren ? "popover-container" : 'popover-container-no-children'}
      >
        <Card className={'topbar-card-menu p-12'}>
          <CardContent className={!isHasChildren ? 'topbar-card-menu-content' : ""}>
            <AppBar position="static" color="default" className='bg-white appbar-menu-card scrollable-content'>
              <div
                className={!isHasChildren ? 'title-appbar-menu border-bottom' : "title-appbar-menu"}>{t(data?.name)}</div>
              {
                isHasChildren &&
                <Tabs
                  orientation="vertical"
                  value={tabValue}
                  onChange={handleChangeTabValue}
                  variant="scrollable"
                  scrollButtons="on"
                  indicatorColor="secondary"
                  textColor="primary"
                  aria-label="scrollable force tabs example"
                >
                  {data?.children?.map((item, index) => {
                    return (
                      <Tab
                        key={index}
                        value={index}
                        label={renderLabelItem(item)}
                        {...a11yProps(index)}
                        className={'tab-item' + (tabItemActive === index ? " active" : "")}
                        onClick={() => setTabItemActive(index)}
                      />
                    )
                  })}
                </Tabs>
              }
            </AppBar>
            <div className={!isHasChildren ? 'main-content mt-10' : "tab-panel-container"}>
              {data?.children?.map((item, index) => {
                return isHasChildren
                  ? (
                    <TabPanel
                      key={index}
                      value={tabValue}
                      index={index}
                      className="tab-panel-menu-item"
                    >
                      <Grid container key={index}>
                        {renderLevel(item)}
                      </Grid>
                    </TabPanel>
                  ) : (
                    <Grid container key={index}>
                      {renderLevel(item)}
                    </Grid>
                  )
              })}
            </div>
          </CardContent>
        </Card>
      </Popover>
    </div>
  );
}

Laypout1RenderLevel.propTypes = {
  data: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired,
};

export default withRouter(Laypout1RenderLevel)
