import React, { Component } from 'react';
import { Grid, Icon, IconButton, MenuItem, Switch, withStyles } from '@material-ui/core';
import { navigations } from "app/navigations";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { withTranslation } from "react-i18next";
import localStorageService from "app/services/localStorageService";
import Laypout1TopBarRenderLevel from './Laypout1TopBarRenderLevel';
import Brand from '../SharedCompoents/Brand';
import ConstantList from "../../appConfig";
import jwtAuthService from 'app/services/jwtAuthService';
import { EgretMenu } from 'egret';
import { logoutUser } from "app/redux/actions/UserActions";
import { Link } from "react-router-dom";
import {LightTooltip} from "../../views/Component/Utilities";
import {appConst} from "../../appConst";
import {getTheHighestRole, getUserInformation} from "../../appFunction";
const ViewEgretHorizontalNav = withTranslation()(Laypout1TopBarRenderLevel);

class TopBarMenu extends Component {
  state = {
    navigations: [],
  }

  handleClick = (event, index) => {
    this.setState({
      anchorIndex: index,
    })
  };

  handleClose = (index) => {
    this.setState({
      anchorIndex: null,
    })
  };

  componentDidMount = () => {
    let navigations = this.getNavigation();
    this.setState({ navigations })
  }

  componentDidUpdate = (prevProps, nextProps) => {
    if(prevProps?.anchorIndex !== nextProps?.anchorIndex) {
      this.setState({
        ["anchorEl_" + prevProps?.anchorIndex]: null,
      });
    }
  }
  
  getNavigation() {
    const {currentUser} = getUserInformation();
    const userRoles = currentUser?.roles?.map(item => item.authority);
    // let navigation = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.NAVIGATIONS);
    const navigationsByRole = this.filterNavigationsByRoles(navigations, userRoles)
    return navigationsByRole || [];
  }
  
  filterNavigationsByRoles = (navigations = [], userRoles = []) => {
    function hasCommonRoles(menuRoles, userRoles) {
      if (!menuRoles || menuRoles.length === 0) return false;
      return menuRoles?.some(role => userRoles?.includes(role));
    }
    
    function filterChildren(menu) {
      if (!menu.children || menu.children.length === 0) {
        return hasCommonRoles(menu.roles, userRoles) ? menu : null;
      }
      
      const filteredChildren = menu.children
        ?.map(child => filterChildren(child))
        ?.filter(child => child !== null && child.isVisible);
      
      if (filteredChildren.length > 0 || hasCommonRoles(menu.roles, userRoles)) {
        return { ...menu, children: filteredChildren };
      }
      
      return null;
    }
    
    return navigations
      .map(menu => filterChildren(menu))
      .filter(menu => menu !== null && menu.isVisible);
  }

  renderLogoSwitch = () => (
    // Open Brand component file to replace logo and text
    <Brand color="white"/>
  );

  handleSignOut = () => {
    this.props.logoutUser();
  };

  renderUser = () => {
    const { t } = this.props;
    let imagePath = ConstantList.ROOT_PATH + "assets/images/avatar.jpg";

    let user = jwtAuthService.getLoginUser();
    if (user != null && user.imagePath != null) {
      imagePath = ConstantList.API_ENPOINT + user.imagePath;
    }

    if (user != null && user.imagePath != null) {
      imagePath = ConstantList.API_ENPOINT + user.imagePath;
    }
    return (
      <div className="sidenav__user" style={{ order: 1 }}>
        <EgretMenu
          menuButton={
            <div className="username-photo">
              <span className="username">
                <LightTooltip
                  title={user?.displayName}
                  placement="top"
                  enterDelay={300}
                  leaveDelay={200}
                >
                  <div>
                    {user ? user?.displayName : ""}
                  </div>
                </LightTooltip>
              </span>
              <img src={imagePath} alt="user" />
            </div>
          }
          >
            <MenuItem className="p-0">
              <Link className="menu-item-user w-100 flex flex-middle py-4 px-16" to="/">
                <Icon> home </Icon>
                <span className="pl-16">
                  {" "}
                  {t("Dashboard.dashboard")}{" "}
                </span>
              </Link>
            </MenuItem>
            <MenuItem className="p-0">
              <Link
                className="menu-item-user w-100 flex flex-middle py-4 px-16"
                to="/page-layouts/user-profile"
              >
                <Icon> person </Icon>
                <span className="pl-16"> {t("Dashboard.profile")} </span>
              </Link>
            </MenuItem>
            <MenuItem className="p-0">
              <div
                className="menu-item-user w-100 flex flex-middle py-4 px-16"
                onClick={this.handleSignOut}
              >
                <Icon>exit_to_app</Icon>
                <span className="pl-16"> {t("Dashboard.logout")} </span>
              </div>

            </MenuItem>
          </EgretMenu>
      </div>
    );
  };

  render() {
    const { t } = this.props;
    let navigations = this.getNavigation();
    
    return (
      <div className="container-topbar-navigation MuiPaper-elevation2">
        <div className='logo pl-30'>
          {this.renderLogoSwitch()}
        </div>
        <Grid container spacing={1} className="topbar-navigation" >
          {navigations?.map((navigation, index) => {
            return (
              <ViewEgretHorizontalNav key={index} data={navigation} t={t}/>
            )
          })}
        </Grid>
        <div className='account'>
          {this.renderUser()}
        </div>
      </div>
    );
  }
}

TopBarMenu.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  settings: state.layout.settings,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(
    mapStateToProps,
    { setLayoutSettings, logoutUser }
  )(TopBarMenu)
);
