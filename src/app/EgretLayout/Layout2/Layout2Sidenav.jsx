import React, {Component, Fragment} from "react";
import PropTypes from "prop-types";
import {withStyles, MuiThemeProvider, Tooltip, IconButton, MenuItem, Icon} from "@material-ui/core";
import {connect} from "react-redux";
import {
  setLayoutSettings,
  setDefaultSettings
} from "app/redux/actions/LayoutActions";
import {isMdScreen} from "utils";
import {Link, withRouter} from "react-router-dom";
import Sidenav from "../SharedCompoents/Sidenav";
import Brand from "../SharedCompoents/Brand";
import ConstantList from "app/appConfig";
import authService from "../../services/jwtAuthService";
import {LightTooltip} from "../../views/Component/Utilities";
import {EgretMenu} from "egret";
import {logoutUser} from "app/redux/actions/UserActions";


const IconButtonWhite = withStyles((theme) => ({
  root: {
    backgroundColor: "transparent",
    padding: "5px",
  },
}))(IconButton);

const IconSmall = withStyles(() => ({
  root: {
    fontSize: "1rem",
  },
}))(Icon);

class Layout2Sidenav extends Component {
  state = {
    sidenavToggleChecked: false,
    hidden: true,
  };

  componentDidMount() {
    this.unlistenRouteChange = this.props.history.listen((location, action) => {
      if (isMdScreen()) {
        this.updateSidebarMode({ mode: "close" });
      }
    });

    setTimeout(() => {
      this.setState({ hidden: false });
    }, 400);
  }

  componentWillUnmount() {
    this.unlistenRouteChange();
  }

  updateSidebarMode = (sidebarSettings) => {
    let { settings, setLayoutSettings, setDefaultSettings } = this.props;
    const updatedSettings = {
      ...settings,
      layout2Settings: {
        ...settings.layout2Settings,
        leftSidebar: {
          ...settings.layout2Settings.leftSidebar,
          ...sidebarSettings,
        },
      },
    };
    setLayoutSettings(updatedSettings);
    setDefaultSettings(updatedSettings);
  };

  handleSidenavToggle = () => {
    let { sidenavToggleChecked } = this.state;
    let mode = sidenavToggleChecked ? "full" : "compact";
    this.updateSidebarMode({ mode });
    this.setState({ sidenavToggleChecked: !sidenavToggleChecked });
  };

  handleSignOut = () => {
    this.props.logoutUser();
  };

  renderUser = () => {
    let imagePath = ConstantList.ROOT_PATH + "assets/images/avatar-svg.svg";

    let user = authService.getLoginUser();
    if (user?.imagePath) {
      imagePath = ConstantList.API_ENPOINT + user.imagePath;
    }
    return (
      <div className="sidenav__user" style={{ order: 1 }}>
        <LightTooltip
          title={user ? user.displayName : ""}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <div className="username-photo">
            <img src={imagePath} alt="user" />
          </div>
        </LightTooltip>
      </div>
    );
  };

  renderFooterSidebar = () => {
    const {t, i18n} = this.props;
    return (
      <div className="sidenav__login_logout">
        <div className="user__menu">
          <EgretMenu
            menuButton={
              <LightTooltip
                title={""}
                placement="top"
                enterDelay={300}
                leaveDelay={200}
              >
                <IconButtonWhite className="" size="medium">
                  <IconSmall> settings </IconSmall>
                </IconButtonWhite>
              </LightTooltip>
            }
          >
            <MenuItem className="p-0">
              <Link className="menu-item-user w-100 flex flex-middle py-4 px-16" to="/">
                <Icon> home </Icon>
                <span className="pl-16">
                  {" "}
                  {t("Dashboard.dashboard")}{" "}
                </span>
              </Link>
            </MenuItem>
            <MenuItem className="p-0">
              <Link
                className="menu-item-user w-100 flex flex-middle py-4 px-16"
                to="/page-layouts/user-profile"
              >
                <Icon> person </Icon>
                <span className="pl-16"> {t("Dashboard.profile")} </span>
              </Link>
            </MenuItem>
          </EgretMenu>
          <Tooltip title={t("Dashboard.logout")}>
            <IconButton
              aria-label="Delete"
              className="icon-logout"
              size="medium"
              onClick={this.handleSignOut}
            >
              <IconSmall>exit_to_app</IconSmall>
            </IconButton>
          </Tooltip>
        </div>

        {/* todo: menu login logout */}
      </div>
    );
  };

  render() {
    let { theme, settings } = this.props;
    const sidenavTheme = settings.themes[settings.layout1Settings.leftSidebar.theme] || theme;
    return (
      <MuiThemeProvider theme={sidenavTheme}>
        <div className="sidenav bg-white">
          <div className="sidenav__hold">
            {!this.state.hidden && (
              <Fragment>
                <div className="py-13 px-18 brand-area">
                  <Brand color="main"/>
                </div>
                <Sidenav>{this.renderUser()}</Sidenav>
                {this.renderFooterSidebar()}
              </Fragment>
            )}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Layout2Sidenav.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  setDefaultSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  setDefaultSettings: PropTypes.func.isRequired,
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  user: state.user,
  settings: state.layout.settings,
});

export default withStyles({}, { withTheme: true })(
  withRouter(
    connect(
      mapStateToProps,
      {
        setLayoutSettings,
        setDefaultSettings,
        logoutUser,
      }
    )(Layout2Sidenav)
  )
);
