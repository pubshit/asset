import React, { Component, Fragment } from "react";
import AppContext from "app/appContext";
import Footer from "../SharedCompoents/Footer";
import Layout2Sidenav from "./Layout2Sidenav";
import { PropTypes } from "prop-types";
import {classList, isMdScreen} from "utils";
import { connect } from "react-redux";
import { renderRoutes } from "react-router-config";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { withStyles } from "@material-ui/styles";
import {withTranslation} from "react-i18next";
import LoadingIndicator from "../SharedCompoents/LoadingIndicator";
import Layout2Topbar from "./Layout2Topbar";


const ViewLayout2Topbar = withTranslation()(Layout2Topbar);
const ViewLayout2SideNav = withTranslation()(Layout2Sidenav);

const styles = theme => {
  return {
    layout: {
      backgroundColor: theme.background.default,
      color: theme.palette.text.primary
    }
  };
};

class Layout2 extends Component {
  state = {};

  componentDidMount() {
    if (isMdScreen()) {
      this.updateSidebarMode({ mode: "close" });
    }
  }

  updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = this.props;
    setLayoutSettings({
      ...settings,
      layout2Settings: {
        ...settings.layout2Settings,
        leftSidebar: {
          ...settings.layout2Settings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  render() {
    let { settings, classes } = this.props;
    let { layout2Settings } = settings;
    let layoutClasses = {
      [classes.layout]: true,
      [`${settings.activeLayout} sidenav-${layout2Settings.leftSidebar.mode}`]: true,
      "topbar-fixed": layout2Settings.topbar.fixed
    };
    return (
      <AppContext.Consumer>
        {({ routes, pageLoading }) => (
          <Fragment>
            <div className={classList(layoutClasses)}>
              {layout2Settings.leftSidebar.show && <ViewLayout2SideNav settings={settings} />}

              <div className="content-wrap position-relative">
                <LoadingIndicator show={pageLoading} />
                {layout2Settings.topbar.show && layout2Settings.topbar.fixed && (
                  <ViewLayout2Topbar/>
                )}

                <div className="scrollable-content">
                  {layout2Settings.topbar.show &&
                    !layout2Settings.topbar.fixed && <Layout2Topbar />}
                  <div className="content">{renderRoutes(routes)}</div>
                  <div className="my-auto" />
                  {settings.footer.show && !settings.footer.fixed && <Footer />}
                </div>

                {settings.footer.show && settings.footer.fixed && <Footer />}
              </div>
            </div>
          </Fragment>
        )}
      </AppContext.Consumer>
    );
  }
}

Layout2.propTypes = {
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  settings: state.layout.settings
});

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    { setLayoutSettings }
  )(Layout2)
);
