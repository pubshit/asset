import React, { Component } from "react";
import {
  withStyles,
  MuiThemeProvider, IconButton, Icon,
} from "@material-ui/core";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import {withRouter} from "react-router-dom";
import {isMdScreen} from "../../../utils";
import ConstantList from "app/appConfig";
import authService from "../../services/jwtAuthService";

const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.primary.main,
  },
});

class Layout2Topbar extends Component {
  state = {};

  updateSidebarMode = (sidebarSettings) => {
    let { settings, setLayoutSettings } = this.props;

    setLayoutSettings({
      ...settings,
      layout2Settings: {
        ...settings.layout2Settings,
        leftSidebar: {
          ...settings.layout2Settings.leftSidebar,
          ...sidebarSettings,
        },
      },
    });
  };

  handleSidebarToggle = () => {
    let { settings } = this.props;
    let { layout2Settings } = settings;

    let mode;
    if (isMdScreen()) {
      mode = layout2Settings.leftSidebar.mode === "close" ? "mobile" : "close";
    } else {
      mode = layout2Settings.leftSidebar.mode === "full" ? "close" : "full";
    }
    
    this.updateSidebarMode({ mode });
  };

  render() {
    let { theme, settings } = this.props;
    const topbarTheme = settings.themes[settings.layout2Settings.topbar.theme] || theme;
    
    return (
      <MuiThemeProvider theme={topbarTheme}>
        <div className="topbar">
          <div
            className="topbar-hold bg-white"
            // style={{ backgroundColor: topbarTheme.palette.primary.main }}
          >
            <div className="flex flex-space-between flex-middle h-100">
              <div className="flex">
                <IconButton onClick={this.handleSidebarToggle}>
                  <Icon>menu</Icon>
                </IconButton>

                <div className="hide-on-mobile">
                </div>
              </div>
              <div className="flex flex-middle">
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Layout2Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, { setLayoutSettings, logoutUser })(Layout2Topbar)
  )
);
