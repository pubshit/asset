const Layout2Settings = {
  topbar: {
    show: true,
    fixed: true,
  },
  navbar: {
    show: true,
    theme: 'slateDark2'
  },
  leftSidebar: { // Only for mobile devices
    show: true,
    mode: 'full', // full, close, compact, mobile,
    bgOpacity: .96, // 0 ~ 1
  }
}

export default Layout2Settings;