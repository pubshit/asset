import ConstantList from "../../appConfig";
import {appConst, variable} from "../../appConst";

const COlOR = {
  PRIMARY_FIRST: ConstantList.COLOR.PRIMARY_FIRST,
  PRIMARY_SECOND: ConstantList.COLOR.PRIMARY_FIRST,
  SUPPORT_FIRST: ConstantList.COLOR.SUPPORT_FIRST,
  SUPPORT_SECOND: ConstantList.COLOR.SUPPORT_SECOND,
  SUPPORT_THIRD: ConstantList.COLOR.SUPPORT_THIRD,
  MENU: {
    MAIN: "#fff",
  },
  BACKGROUND: {
    MAIN: "#E8EAF6",
    PAPER: "#fff",
  },
  WHITE: "#fff",
};

export const themeColors = {
  white: {
    palette: {
      type: "light",
      key: "white",
      primary: {
        main: "#ffffff",
        contrastText: "rgba(0,0,0,.87)",
      },
      secondary: {
        main: "#ff9e43",
      },
    },
  },
  slateDark1: {
    palette: {
      type: "dark",
      key: "slateDark1",
      primary: {
        main: "#222A45",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43",
      },
      background: {
        paper: "#222A45",
        default: "#1a2038",
      },
    },
  },
  slateDark2: {
    palette: {
      type: "dark",
      key: "slateDark2",
      primary: {
        main: "#1a2038",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43",
      },
      background: {
        paper: "#222A45",
        default: "#1a2038",
      },
    },
  },
  purple1: {
    palette: {
      type: "light",
      key: "purple1",
      primary: {
        main: COlOR.PRIMARY_FIRST,
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43", //COlOR.PRIMARY_SECOND,
      },
      background: {
        default: COlOR.BACKGROUND.MAIN,
        paper: COlOR.WHITE,
      },
      menu: {
        main: "#fff", //COlOR.MENU.MAIN
        color: "#3949AB",
        colorHover: "#5C6BC0",
      },
      text: {
        main: "#000",
        secondary: "#3948AB",
      },
    },
  },
  purple2: {
    palette: {
      type: "light",
      key: "purple2",
      primary: {
        main: "#6a75c9",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43",
      },
    },
  },
  purpleDark1: {
    palette: {
      type: "dark",
      key: "purpleDark1",
      primary: {
        main: "#7467ef",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43",
      },
      background: {
        paper: "#222A45",
        default: "#1a2038",
      },
    },
  },
  purpleDark2: {
    palette: {
      type: "dark",
      key: "purpleDark2",
      primary: {
        main: "#6a75c9",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#ff9e43",
      },
      background: {
        paper: "#222A45",
        default: "#1a2038",
      },
    },
  },
  blue: {
    palette: {
      type: "light",
      key: "blue",
      primary: {
        main: "#3366FF",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#FFAF38",
        contrastText: "#ffffff",
      },
    },
  },
  blueDark: {
    palette: {
      type: "dark",
      key: "blueDark",
      primary: {
        main: "#3366FF",
        contrastText: "#ffffff",
      },
      secondary: {
        main: "#FF4F30",
        contrastText: "#ffffff",
      },
      background: {
        paper: "#222A45",
        default: "#1a2038",
      },
    },
  },
  [appConst.OBJECT_THEME.THEME_BLUE.code]: {
    primary: {
      main: "#1A5E83",
      dark: "#12445f",
      light: "#4296C4",
      lightActive: "#E8ECFC",
      active: "#5790b8",
      hover: "#e5f9ff",
      lightHover: "rgb(233 246 253)",
    },
    secondary: {
      main: "#17A2B8",
      light: "#8ad2d3",
    },
    background: {
      default: "#eff5f9",
      header: "#E9ECEF",
      border: "#DEE2E6",
    },
    default: {
      main: "rgba(0, 0, 0, 0.54)",
      light: "#DEE2E6",
    },
    palette: {
      darkblue: "#043450",
      pink: "#f8afbe",
      yellow: "#ffd66d",
      goldenrod: "#c7a754",
      red: "#cc0033",
			error: {
				main: "#e52d00",
				light: "#ffd3d3"
			},
			success: {
				main: "#43b54d"
			},
			info: {
				main: "#29aae3",
			},
			warning: {
				main: "#ff9e43",
			},
      secondary: {
        main: "#ff9e43", //COlOR.PRIMARY_SECOND,
      },
      background: {
        default: "#eff5f9",
        paper: COlOR.WHITE,
      },
      menu: {
        main: "#fff", //COlOR.MENU.MAIN
        color: "#3949AB",
        colorHover: "#5C6BC0",
      },
      text: {
        main: "#000",
        secondary: "#3948AB",
      },
      assets: {
        fixedAsset: "#043450",
        iat: "#4296C4",
        supplier: "#8ad2d3",
      },
      bar: {
        column: "#8ad2d3",
        line: "#f8afbe",
      },
    },
    signal: {
      success: "#43b54d",
      info: "#29aae3",
      error: "#e52d00",
      warning: "#ff9e43",
    },
  },
  [appConst.OBJECT_THEME.THEME_GREEN.code]: {
    primary: {
      main: "#01723A",
      dark: "#02532b",
      light: "#4296C4",
      active: "#4ba468",
      hover: "rgba(208,231,216,0.8)",
      lightHover: "rgb(230 248 237)",
      lightActive: "rgba(208,231,216,0.8)",
    },
    secondary: {
      main: "#66A59A",
      light: "#9cd1c8",
    },
    background: {
      default: "#eff5f9",
      header: "#E9ECEF",
      border: "#DEE2E6",
    },
    default: {
      main: "rgba(0, 0, 0, 0.54)",
      light: "#DEE2E6",
    },
    palette: {
      green: "#98ce6a",
      yellow: "#ffeead",
      orange: "#EF8D19",
      teal: "#3dac9b",
      red: "#db503a",
      secondary: {
        main: "#ff9e43", //COlOR.PRIMARY_SECOND,
      },
			error: {
				main: "#db503a",
				light: "#ffd3d3"
			},
			success: {
				main: "#98ce6a"
			},
			info: {
				main: "#3dac9b",
			},
			warning: {
				main: "#ef8748",
			},
      assets: {
        fixedAsset: "#EF8D19",
        iat: "#02532b",
        supplier: "#9cd1c8",
      },
      bar: {
        column: "#9cd1c8",
        line: "#EF8D19",
      },
    },
    signal: {
      success: "#98ce6a",
      info: "#3dac9b",
      error: "#db503a",
      warning: "#ef8748",
    },
  },
  [appConst.OBJECT_THEME.THEME_199.code]: {
    primary: {
      main: "#10948E",
      dark: "#0a8883",
      light: "#10948E",
      lightActive: "#dcf8f6",
      lightHover: "#e7f8f8",
      active: "#5ccbc4",
      hover: "#eefcfb",
    },
    secondary: {
      main: "#0a8883",
      light: "#8ad2d3",
    },
    background: {
      default: "#eff5f9",
      header: "#E9ECEF",
      border: "#DEE2E6",
    },
    default: {
      main: "rgba(0, 0, 0, 0.54)",
      light: "#DEE2E6",
    },
    palette: {
      darkblue: "#043450",
      pink: "#f8afbe",
      yellow: "#ffd66d",
      goldenrod: "#c7a754",
      red: "#cc0033",
			secondary: {
				main: "#ff9e43", //COlOR.PRIMARY_SECOND,
			},
			error: {
				main: "#D83F31",
				light: "#ffd3d3"
			},
			success: {
				main: "#88cd6f"
			},
			info: {
				main: "#05BFDB",
			},
			warning: {
				main: "#E9C46A",
			},
      assets: {
        fixedAsset: "#043450",
        iat: "#4296C4",
        supplier: "#8ad2d3",
      },
      bar: {
        column: "#8ad2d3",
        line: "#f8afbe",
      },
    },
    signal: {
      success: "#88cd6f",
      info: "#05BFDB",
      error: "#D83F31",
      warning: "#E9C46A",
    },
  },
};
