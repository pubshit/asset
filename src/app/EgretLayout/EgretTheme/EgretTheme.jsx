import React, { Component, useContext } from "react";
import { MuiThemeProvider } from "@material-ui/core";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { Helmet } from "react-helmet";
import AppContext from "app/appContext";

class EgretTheme extends Component {

  render() {
    let { children, settings } = this.props;
    let activeTheme = {
      ...settings.themes[settings.activeTheme],
      custom: {
        ...settings.themes[settings.activeTheme],
      }
    };

    return (
      <MuiThemeProvider theme={activeTheme}>
        <Helmet>
          <style>
            {`
              :root {
                --primary: ${activeTheme.custom.primary.main};
                --light-primary: ${activeTheme.custom.primary.light};
                --dark-primary: ${activeTheme.custom.primary.dark};
                --primary-hover: ${activeTheme.custom.primary.hover};
                --primary-light-hover: ${activeTheme.custom.primary.lightHover};
                --primary-light-active: ${activeTheme.custom.primary.lightActive};
                --primary-active: ${activeTheme.custom.primary.active};
                --default: ${activeTheme.custom.default.main};
                --palette-pink: ${activeTheme.custom.palette.pink};
                --palette-goldenrod: ${activeTheme.custom.palette.goldenrod};
                --palette-darkblue: ${activeTheme.custom.palette.darkblue};
                --palette-red: ${activeTheme.custom.palette.red};
                --secondary: ${activeTheme.custom.secondary.main};
                --light-secondary: ${activeTheme.custom.secondary.light};
                --error: ${activeTheme.palette.error.main};
                --bg-default: ${activeTheme.palette.background.default};
                --bg-table-header: ${activeTheme.custom.primary.main};
                --bg-table-border: ${activeTheme.custom.background.border};
                --bg-paper: ${activeTheme.palette.background.paper}; 
                --text-body: ${activeTheme.palette.text.primary}; 
                --text-muted: ${activeTheme.palette.text.secondary}; 
                --text-hint: ${activeTheme.palette.text.hint}; 
                --signal-success: ${activeTheme.custom.palette?.success?.main};
                --signal-info: ${activeTheme.custom.palette?.info?.main};
                --signal-error: ${activeTheme.custom.palette?.error?.main};
                --signal-light-error: ${activeTheme.custom.palette?.error?.light};
                --signal-warning: ${activeTheme.custom.palette?.warning?.main};
                --font: Roboto,"Helvetica Neue",sans-serif;
                --font-caption: 400 12px/20px var(--font);
                --font-body-1: 400 14px/20px var(--font);
                --font-body-2: 500 14px/24px var(--font);
                --font-subheading-1: 400 15px/24px var(--font);
                --font-subheading-2: 400 16px/28px var(--font);
                --font-headline: 400 24px/32px var(--font);
                --font-title: 500 18px/26px var(--font);
                --font-display-1: 400 34px/40px var(--font);
                --font-display-2: 400 45px/48px var(--font);
                --font-display-3: 400 56px/56px var(--font);
                --font-display-4: 300 112px/112px var(--font);
                
                ${activeTheme.shadows
                .map((shadow, i) => {
                  return `--elevation-z${i}: ${shadow};`;
                })
                .join(" ")} 

                ::-webkit-scrollbar {
                  -webkit-appearance: none;
                  height: 8px;
                }
                ::-webkit-scrollbar-thumb {
                  border: 2px solid white;
                  border-radius: 4px;
                  background-color: rgba(0, 0, 0, .3);
                }
              }
            `}
          </style>
        </Helmet>

        {children}
      </MuiThemeProvider>
    );
  }
}

EgretTheme.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  settings: state.layout.settings,
  setLayoutSettings: PropTypes.func.isRequired
});

EgretTheme.contextType = AppContext;

export default connect(
  mapStateToProps,
  { setLayoutSettings }
)(EgretTheme);
