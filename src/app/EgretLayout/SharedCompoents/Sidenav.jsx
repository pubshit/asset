import React, { Component, Fragment } from "react";
import Scrollbar from "react-perfect-scrollbar";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { navigations } from "../../navigations";
import { EgretVerticalNav } from "egret";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { withTranslation } from 'react-i18next';
import localStorageService from "../../services/localStorageService";
import {appConst} from "../../appConst";


const ViewEgretVerticalNav = withTranslation()(EgretVerticalNav);

class Sidenav extends Component {
  state = {
    closePopoverMenu: false,
  };

  updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = this.props;
    let activeLayoutSettingsName = settings.activeLayout+"Settings";
    let activeLayoutSettings = settings[activeLayoutSettingsName];

    setLayoutSettings({
      ...settings,
      [activeLayoutSettingsName]: {
        ...activeLayoutSettings,
        leftSidebar: {
          ...activeLayoutSettings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  renderOverlay = () => (
    <div
      onClick={() => this.updateSidebarMode({ mode: "close" })}
      className="sidenav__overlay"
    />
  );

  getNavigation() {
    let navigation = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.NAVIGATIONS);
    return navigation || [];
  }
  render() {
    let { settings } = this.props;
    let isNavClose = settings.layout1Settings.leftSidebar.mode === "close";
    let navigation = this.getNavigation()
    return (
      <Fragment>
        <Scrollbar options={{suppressScrollX: true}} className="scrollable">
          {this.props.children}
          <ViewEgretVerticalNav navigation={navigation} isNavClose={isNavClose}/>
        </Scrollbar>
        {this.renderOverlay()}
      </Fragment>
    );
  }
}

Sidenav.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  settings: state.layout.settings
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      setLayoutSettings
    }
  )(Sidenav)
);