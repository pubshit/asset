import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { appConst } from "app/appConst";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  formControl: {
    margin: theme.spacing(),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  icon_m: {
    marginRight: "8px",
  },
});

class LanguageSelect extends React.Component {
  state = {
    selected: "vi",
    hasError: false,
  };

  handleChange(value) {
    const { i18n } = this.props;
    i18n.changeLanguage(value);
  }
  render() {
    const { t, i18n, classes, isNgang = false } = this.props;
    const { hasError } = this.state;
    
    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl} error={hasError}>
          {
            <div>
              <i className={`fa fa-globe ${classes.icon_m} `}></i>
              <span>Ngôn ngữ: </span>
              <span
                onClick={() => this.handleChange(appConst.LANGUAGES.VIE)}
                className={i18n.language === appConst.LANGUAGES.VIE ? "language-active" : ""}
              >
                Tiếng Việt
              </span>
              <span
                onClick={() => this.handleChange(appConst.LANGUAGES.ENG)}
                className={i18n.language === appConst.LANGUAGES.ENG ? "language-active" : ""}
              >
                English
              </span>
            </div>
          }
          {hasError && <FormHelperText>This is required!</FormHelperText>}
        </FormControl>
      </form>
    );
  }
}

LanguageSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LanguageSelect);
