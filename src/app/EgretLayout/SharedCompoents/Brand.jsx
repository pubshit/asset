import React, { Component } from "react";
import ConstantList from "../../appConfig";
import { Link } from "react-router-dom";
import { Helmet } from 'react-helmet';
import Constant from "../../appConfig";
import AppContext from "../../appContext";
import {appConst} from "../../appConst";
import PropTypes from "prop-types";
class Brand extends Component {
  state = {
    url: ""
  };

  componentDidMount=() => {
    const {theme} = this.context;
    const {color} = this.props;
    let url = {
      white: {
        [appConst.OBJECT_THEME.THEME_199.code]: "logo-x-amis-white.png",
        [appConst.OBJECT_THEME.THEME_BLUE.code]: "utc.png",
        [appConst.OBJECT_THEME.THEME_GREEN.code]: "logo-i-amis-white.png",
      },
      main: {
        [appConst.OBJECT_THEME.THEME_199.code]: "logo-x-amis-blue.png",
        [appConst.OBJECT_THEME.THEME_BLUE.code]: "utc.png",
        [appConst.OBJECT_THEME.THEME_GREEN.code]: "logo-i-amis.png",
      },
    }
    this.setState({url: url[color || "white"]?.[theme?.code] || "utc.png"})
  }

  render() {
    return (
      <>
        <div className="flex flex-middle flex-center brand logo-menu">
          <Link to={ConstantList.HOME_PAGE} className="flex flex-middle flex-column">
            <img src={`/assets/images/logos/${this.state.url}`} alt="" />
            AMMIS
          </Link>
        </div>
      </>
    );
  }
}
export default Brand;
Brand.contextType = AppContext;
Brand.propTypes = {
  color: PropTypes.oneOf(["main", "white"])
}
