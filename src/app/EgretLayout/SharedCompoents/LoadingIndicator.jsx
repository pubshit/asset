import * as React from "react";
import "../../../styles/views/_loading-indicator.scss";
import Loading from "../../../egret/components/EgretLoadable/Loading";

const LoadingIndicator = ({ show }) => {
  return (
    <>
      {show && (
        <div className="loading-icon">
          <Loading />
        </div>
      )}
    </>
  );
};

export default LoadingIndicator;
