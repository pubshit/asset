import React, { useState } from 'react';
import { withStyles, MuiThemeProvider, Button } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import localStorageService from 'app/services/localStorageService';
import { useTranslation, withTranslation } from 'react-i18next';
import LanguageSelect from './LanguageSelect';

const Footer = ({ theme, settings }) => {
  const { t } = useTranslation();
  const footerTheme = settings.themes[settings.footer.theme] || theme;

  let currentUser = localStorageService.getSessionItem('currentUser');
  let departmentUser = localStorageService.getSessionItem("departmentUser");
  const ViewLanguageSelect = withTranslation()(LanguageSelect);

  return (
    <MuiThemeProvider theme={footerTheme}>
      <Helmet>
        <style>
        </style>
      </Helmet>
      <div className="footer flex flex-middle">
        <div className="flex-middle container px-sm-30 w-100">
          <span className="user">
            <i className="fa fa-user icon_m" aria-hidden="true"></i>
            {currentUser?.displayName}
          </span>
          <span className="office">
            <i className="fa fa-building icon_m" aria-hidden="true"></i>
            {/* {`Phòng ban:  | Chi nhánh: | Kho:  `} */}
            {t('footer.department')}: <b className="text-goldenrod">{departmentUser?.name ? departmentUser?.name : ""}</b>
          </span>
          <span  className="language">
            <ViewLanguageSelect isNgang={true} />
          </span>
        </div>
      </div>
    </MuiThemeProvider>
  );
};

Footer.propTypes = {
  settings: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  settings: state.layout.settings,
});

export default withStyles({}, { withTheme: true })(connect(mapStateToProps, {})(Footer));
