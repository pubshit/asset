import { useEffect } from 'react';

const useFavicon = (iconUrl) => {
	useEffect(() => {
		const link = document.querySelector("link[rel~='icon']");
		const splashScreen = document.getElementById("splash-screen");
		
		if (!link) {
			const newLink = document.createElement('link');
			newLink.rel = 'icon';
			newLink.href = iconUrl;
			document.head.appendChild(newLink);
		} else {
			link.href = iconUrl;
		}
	}, [iconUrl]);
};

export default useFavicon;
