import axios from "axios";
import history from "history.js";
import ConstantList from "../appConfig";
import MenuService from "../services/MenuService";
import localStorageService from "./localStorageService";
import { appConst } from "app/appConst";
import {
  getOneById,
  getOneByIdNew,
} from "app/views/InforOrganization/InforOrganizationService";
import {isSuccessfulResponse} from "../appFunction";
const config = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    Authorization: "Basic Y29yZV9jbGllbnQ6c2VjcmV0",
  },
};
class JwtAuthService {
  user = {
    userId: "1",
    role: "ADMIN",
    displayName: "Watson Joyce",
    email: "watsonjoyce@gmail.com",
    photoURL: ConstantList.ROOT_PATH + "assets/images/avatar.jpg",
    age: 25,
    token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh",
  };
  async getCurrentUser() {
    let url = ConstantList.API_ENPOINT + "/api/users/getCurrentUser";
    return await axios.get(url);
  }
	
  getDepartmentByCurrentUser = () => {
    let url =
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/user-departments/current-user";
    return axios.get(url);
  };
	
  getOauthToken = (requestBody) => {
    let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/oauth/token";
    return axios
			.post(
				url,
				requestBody,
				config
			)
  };
	
  async loginWithUserNameAndPassword(username, password) {
    let requestBody =
      "client_id=core_client&grant_type=password&client_secret=secret";
    requestBody =
      requestBody + "&username=" + username + "&password=" + password;
    await this.getOauthToken(requestBody).then((response) => {
			let dateObj = new Date(Date.now() + response.data.expires_in * 1000);
			localStorageService.setItem(appConst.SESSION_STORAGE_KEY.ACCESS_TOKEN_EXPIRED, dateObj);
			this.setSession(response.data.access_token);
		});
    
		await this.handleGetCurrentUserInfo();
		await this.handleGetDepartmentByCurrentUser();
		await this.handleOrganizationInformation();
    await this.handleGetNavigations();
  }
	
	handleGetCurrentUserInfo = async () => {
		try {
			let result = await this.getCurrentUser();
			if (result?.status === 200 && result?.data) {
				localStorageService.setSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER, result?.data);
				this.setLoginUser(result?.data);
			}
		} catch (error) {
			console.log(error);
		}
	}
	
	handleGetDepartmentByCurrentUser = async () => {
		try {
			let result = await this.getDepartmentByCurrentUser();
			if (result?.status === appConst.CODE.SUCCESS && result?.data?.data?.id) {
				localStorageService.setSessionItem(
					appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER,
					result?.data?.data
				);
			} else {
				localStorageService.setSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER, {});
			}
		} catch (error) {
			console.log(error);
		}
	}
	
	handleOrganizationInformation = async () => {
		try {
			let searchObject = {
				orgId: this.getLoginUser()?.org?.id,
			};
			let resInforOrg = await getOneById();
			let resConfigOrg = await getOneByIdNew(searchObject);
			if (isSuccessfulResponse(resInforOrg?.data?.code)) {
				let configOrg = {
					...resInforOrg?.data?.data,
					configDto: {
						...resConfigOrg?.data?.data,
					},
				};
				localStorageService.setSessionItem(
					appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION,
					configOrg
				);
				localStorageService.setSessionItem(
					"addressOfEnterprise",
					resInforOrg?.data?.data?.province
				);
			}
		} catch (error) {
			console.log("result", error);
		}
	}
	
	handleGetNavigations = async () => {
		try {
			let result = await MenuService.getAllMenuItemByRoleList();
			if (result?.status === 200 && result?.data) {
				localStorageService.setSessionItem(appConst.SESSION_STORAGE_KEY.NAVIGATIONS, result.data);
			}
		} catch (error) {
			console.log(error);
		}
	}

  loginWithEmailAndPassword = (email, password) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.user);
      }, 1000);
    }).then((data) => {
      this.setUser(data);
      this.setSession(data.token);
      return data;
    });
  };

  loginWithToken = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.user);
      }, 100);
    }).then((data) => {
      this.setSession(data.token);
      this.setUser(data);
      return data;
    });
  };

  async logout() {
    try {
      if (ConstantList.AUTH_MODE == "Keycloak") {
        // UserService.doLogout();
        this.setSession(null);
        this.removeUser();
        history.push(ConstantList.HOME_PAGE);
      } else {
        this.setSession(null);
        this.removeUser();
        history.push(ConstantList.LOGIN_PAGE);
      }
    } catch (error) {
      console.log(error);
    }
  }

  setSession(token) {
    axios.defaults.headers.common["Accept-Language"] = "vi";
    if (token) {
      localStorageService.setItem(appConst.SESSION_STORAGE_KEY.JWT_ACCESS_TOKEN, token);
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    } else {
      localStorageService.removeItem(appConst.SESSION_STORAGE_KEY.JWT_ACCESS_TOKEN);
      localStorageService.removeItem(appConst.SESSION_STORAGE_KEY.ACCESS_TOKEN_EXPIRED);
      delete axios.defaults.headers.common["Authorization"];
    }
  }
  
  async setLoginUser(user) {
    localStorageService.setItem(appConst.SESSION_STORAGE_KEY.AUTH_USER, user);
    return user;
  }
	
  getLoginUser = () => {
    return localStorageService.getItem(appConst.SESSION_STORAGE_KEY.AUTH_USER);
  };

  setUser = (user) => {
    localStorageService.setItem(appConst.SESSION_STORAGE_KEY.AUTH_USER, user);
  };
	
  removeUser = () => {
    localStorageService.removeItem(appConst.SESSION_STORAGE_KEY.AUTH_USER);
    localStorageService.removeSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
    localStorageService.removeSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER);
    localStorageService.removeSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
    localStorageService.removeSessionItem(appConst.SESSION_STORAGE_KEY.ADDRESS_OF_ENTERPRISE);
    localStorageService.removeSessionItem(appConst.SESSION_STORAGE_KEY.NAVIGATIONS);
  };
  
  getCurrentUserInfo = async () => {
    await this.handleGetCurrentUserInfo();
    await this.handleGetNavigations();
    await this.handleGetDepartmentByCurrentUser();
    await this.handleOrganizationInformation();
  }
	
	checkToken = async () => {
		let token = localStorageService.getItem(appConst.SESSION_STORAGE_KEY.JWT_ACCESS_TOKEN);
		let expire_time = localStorageService.getItem(appConst.SESSION_STORAGE_KEY.ACCESS_TOKEN_EXPIRED);
		let dateObj = new Date(expire_time);
		if (token && dateObj > new Date()) {
			let currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
			await this.setSession(token);
			if (!currentUser?.id) {
        try {
          await this.getCurrentUserInfo();
          return {
            shouldUpdateNavigations: true,
          }
        } catch (e) {
          console.error(e);
        } finally {
          history.push(ConstantList.HOME_PAGE);
        }
			} else {
        let authUser = this.getLoginUser();
        if (authUser?.id !== currentUser.id) {
          await this.getCurrentUserInfo();
          return {
            shouldUpdateNavigations: true,
          }
        }
      }
		}	else {
			history.push(ConstantList.LOGIN_PAGE)
		}
	}
}

export default new JwtAuthService();
