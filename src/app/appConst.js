export const PRINT_TEMPLATE_CODE = {
  SUPPLIES_MANAGEMENT: {
    INVENTORY_REPORT: {
      INVENTORY_BY_UNIT_PRICE: "supplies_inventory_by_unit_price",
      INVENTORY_BY_PRODUCT: "supplies_inventory_by_product",
    }
  },
  WAREHOUSE_MANAGEMENT: {
    INVENTORY: {
      BY_PRODUCT: "inventory_by_product",
      BY_UNIT_PRICE: "inventory_by_code_and_unit_price",
      IN_DETAIL: "inventory_in_detail",
    }
  }
}

export const appConst = {
  KEY: {
    ENTER: "Enter",
  },

  CODE: {
    SUCCESS: 200,
    DEPRECIATION_YEAR_ERROR: 400055,
    ERROR400: 400,
    INTERNAL_SERVER_ERROR: 500,
    CONFLIG: 409,
    ACCESS_DENIED: 403,
  },

  assetClass: {
    TSCD: 1,
    CCDC: 2,
    VT: 3,
  },
  listStatusAssets: {
    LUU_MOI: {
      code: "TT001",
      indexOrder: 0,
      name: "Lưu kho, mới",
    },
    NHAP_KHO: {
      code: "TT002",
      indexOrder: 1,
      name: "Nhập kho",
    },
    DANG_SU_DUNG: {
      code: "TT003",
      indexOrder: 2,
      name: "Đang sử dụng",
    },
    DANG_BAO_DUONG: {
      code: "TT004",
      indexOrder: 3,
      name: "Đang sửa chữa-bảo dưỡng",
    },
    DA_HONG: {
      code: "TT005",
      indexOrder: 4,
      name: "Đã hỏng, nhập kho",
    },
    DA_THANH_LY: {
      code: "TT006",
      indexOrder: 5,
      name: "Đã thanh lý, tiêu hủy",
    },
    CHUYEN_DI: {
      code: "TT007",
      indexOrder: 6,
      name: "Chuyển đi",
    },
    KHAC: {
      code: "TT009",
      indexOrder: 7,
      name: "Khác, không rõ, không tồn tại",
    },
    DE_XUAT_THANH_LY: {
      code: "TT008",
      indexOrder: 8,
      name: "Đề xuất thanh lý",
    },
  },
  assetType: [
    {
      code: 1,
      name: "Tài sản cố định",
    },
    {
      code: 2,
      name: "Công cụ dụng cụ",
    },
    {
      code: 3,
      name: "Vật tư/Hàng hóa",
    },
  ],
  listRisk: [
    {
      value: "TYPE_A",
      riskType: "Loại A - Trang thiết bị có mức độ rủi ro thấp",
    },
    {
      value: "TYPE_B",
      riskType: "Loại B - Trang thiết bị có mức độ rủi ro trung bình thấp",
    },
    {
      value: "TYPE_C",
      riskType: "Loại C - Trang thiết bị có mức độ rủi ro trung bình cao",
    },
    {
      value: "TYPE_D",
      riskType: "Loại D - Trang thiết bị có mức độ rủi ro cao",
    },
  ],

  bidingPackage: [
    { code: 1, value: "NHOM_1", riskType: "Nhóm 1" },
    { code: 2, value: "NHOM_2", riskType: "Nhóm 2" },
    { code: 3, value: "NHOM_3", riskType: "Nhóm 3" },
    { code: 4, value: "NHOM_4", riskType: "Nhóm 4" },
  ],
  bidingPackageObject: {
    NHOM_1: { code: 1, value: "NHOM_1", riskType: "Nhóm 1" },
    NHOM_2: { code: 2, value: "NHOM_2", riskType: "Nhóm 2" },
    NHOM_3: { code: 3, value: "NHOM_3", riskType: "Nhóm 3" },
    NHOM_4: { code: 4, value: "NHOM_4", riskType: "Nhóm 4" },
  },
  assetTypeObject: {
    TSCD: {
      code: 1,
      name: "Tài sản cố định",
    },
    CCDC: {
      code: 2,
      name: "Công cụ dụng cụ",
    },
    VT: {
      code: 3,
      name: "Vật tư/Hàng hóa",
    },
  },

  assetTypeInventoryCountObject: {
    DEPARTMENT: {
      code: 1,
      name: "Theo phòng ban",
    },
    ALL: {
      code: 2,
      name: "Toàn đơn vị",
    },
  },

  listStatusInstrumentTools: {
    LUU_MOI: { code: "TT001", indexOrder: 0, name: "Lưu kho, mới" },
    NHAP_KHO: { code: "TT002", indexOrder: 1, name: "Nhập kho" },
    DANG_SU_DUNG: { code: "TT003", indexOrder: 2, name: "Đang sử dụng" },
    DANG_BAO_DUONG: {
      code: "TT004",
      indexOrder: 3,
      name: "Đang sửa chữa-bảo dưỡng",
    },
    DA_HONG: {
      code: "TT005",
      indexOrder: 4,
      name: "Đã hỏng, đề xuất nhập kho",
    },
    DA_THANH_LY: {
      code: "TT006",
      indexOrder: 5,
      name: "Đã thanh lý, tiêu hủy",
    },
    CHUYEN_DI: { code: "TT007", indexOrder: 6, name: "Chuyển đi" },
    DE_XUAT_THANH_LY: {
      code: "TT008",
      indexOrder: 7,
      name: "Đề xuất thanh lý",
    },
    KHAC: {
      code: "TT009",
      indexOrder: 8,
      name: "Khác, không rõ, không tồn tại",
    },
  },

  listStatusAllocation: [
    { code: "MOITAO", indexOrder: 0, name: "Mới tạo" },
    { code: "DACAPPHAT", indexOrder: 1, name: "Đã cấp phát" },
    { code: "TRAVE", indexOrder: 2, name: "Trả về" },
    { code: "CHOXACNHAN", indexOrder: 3, name: "Chờ tiếp nhận" },
  ],

  STATUS_ALLOCATION: {
    MOI_TAO: { code: "MOITAO", indexOrder: 0, name: "Mới tạo" },
    DA_CAP_PHAT: { code: "DACAPPHAT", indexOrder: 1, name: "Đã cấp phát" },
    TRA_VE: { code: "TRAVE", indexOrder: 2, name: "Trả về" },
    CHO_XAC_NHAN: { code: "CHOXACNHAN", indexOrder: 3, name: "Chờ tiếp nhận" },
  },

  voucherStatus: {
    MOI_TAO: "MOITAO",
    DA_CAP_PHAT: "DACAPPHAT",
    TRA_VE: "TRAVE",
  },

  listStatusTransfer: [
    { transferStatusIndex: 0, indexOrder: 0, name: "Chờ xác nhận" },
    { transferStatusIndex: 1, indexOrder: 1, name: "Đã xác nhận" },
    { transferStatusIndex: 2, indexOrder: 2, name: "Trả về" },
    { transferStatusIndex: 3, indexOrder: 3, name: "Đã điều chuyển" },
    { transferStatusIndex: 4, indexOrder: 4, name: "Hủy xác nhận" },
  ],

  listStatusTransferCCDC: [
    { transferStatusIndex: 0, indexOrder: 0, name: "Chờ xác nhận" },
    { transferStatusIndex: 1, indexOrder: 1, name: "Đã xác nhận" },
    { transferStatusIndex: 3, indexOrder: 3, name: "Đã điều chuyển" },
    { transferStatusIndex: 4, indexOrder: 4, name: "Hủy xác nhận" },
  ],

  listStatusRecallSlip: [
    { transferStatusIndex: 1, indexOrder: 1, name: "Đang thống kê" },
    { transferStatusIndex: 2, indexOrder: 2, name: "Đã thống kê" },
    { transferStatusIndex: 3, indexOrder: 3, name: "Đang tổng hợp" },
    { transferStatusIndex: 4, indexOrder: 4, name: "Đã thu hồi" },
  ],

  listStatusInventory: [
    { code: "MOI", indexOrder: 0, name: "Mới" },
    { code: "DANGKIEMKE", indexOrder: 1, name: "Đang kiểm kê" },
    { code: "DAKIEMKE", indexOrder: 2, name: "Đã kiểm kê" },
  ],

  listStatusInventoryObject: {
    NEW: { code: "MOI", indexOrder: 0, name: "Mới" },
    DANG_KIEM_KE: { code: "DANGKIEMKE", indexOrder: 1, name: "Đang kiểm kê" },
    DA_KIEM_KE: { code: "DAKIEMKE", indexOrder: 2, name: "Đã kiểm kê" },
  },

  listStatusTransferToAnother: [
    { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
    { code: "PROCESSING", indexOrder: 1, name: "Đang chuyển đi" },
    { code: "TRANSFERRED", indexOrder: 2, name: "Đã chuyển đi" },
  ],

  listStatusTransferToAnotherObject: {
    KE_HOACH: { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
    DANG_CHUYEN_DI: {
      code: "PROCESSING",
      indexOrder: 1,
      name: "Đang chuyển đi",
    },
    DA_CHUYEN_DI: { code: "TRANSFERRED", indexOrder: 2, name: "Đã chuyển đi" },
  },

  listLiquidateStatus: [
    { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
    { code: "PROCESSING", indexOrder: 1, name: "Đang thanh lý" },
    { code: "LIQUIDATED", indexOrder: 2, name: "Đã thanh lý" },
  ],

  listLiquidateStatusObject: {
    NEW: { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
    PROCESSING: { code: "PROCESSING", indexOrder: 1, name: "Đang thanh lý" },
    LIQUIDATED: { code: "LIQUIDATED", indexOrder: 2, name: "Đã thanh lý" },
  },

  listCommonObject: {
    HANG_SAN_XUAT: { code: "hsx", name: "Hãng sản xuất" },
    LOAI_THIET_BI_Y_TE: { code: "ltbyt", name: "Loại thiết bị ý tế" },
    HINH_THUC_MUA_SAM: { code: "htms", name: "Hình thức mua sắm" },
    HINH_THUC_BAO_TRI: { code: "htbt", name: "Hình thức bảo trì" },
    HOP_DONG: { code: "hd", name: "Hợp đồng" },
  },

  listStatusWarehouseTransfer: [
    { transferStatusIndex: 1, indexOrder: 1, name: "Mới tạo" },
    { transferStatusIndex: 2, indexOrder: 2, name: "Đang thực hiện" },
    { transferStatusIndex: 3, indexOrder: 3, name: "Kết thúc" },
  ],

  listStatusWarehouseTransferObject: {
    MOI_TAO: { code: 1, indexOrder: 1, name: "Mới tạo" },
    DANG_THUC_HIEN: { code: 2, indexOrder: 2, name: "Đang thực hiện" },
    KET_THUC: { code: 3, indexOrder: 3, name: "Kết thúc" },
  },

  listStatusPurchasePlaning: [
    { purchasePlaningStatusIndex: 1, indexOrder: 1, name: "Đang xử lý" },
    { purchasePlaningStatusIndex: 2, indexOrder: 2, name: "Đã duyệt danh mục" },
    { purchasePlaningStatusIndex: 3, indexOrder: 3, name: "Đã duyệt báo giá" },
    { purchasePlaningStatusIndex: 4, indexOrder: 4, name: "Đã kết thúc" },
  ],

  subListStatusPurchasePlaning: [
    { purchasePlaningStatusIndex: 1, indexOrder: 1, name: "Đang xử lý" },
    { purchasePlaningStatusIndex: 2, indexOrder: 2, name: "Đã tổng hợp" },
    { purchasePlaningStatusIndex: 3, indexOrder: 3, name: "Đã duyệt báo giá" },
    { purchasePlaningStatusIndex: 4, indexOrder: 4, name: "Đã kết thúc" },
  ],

  listStatusPurchasePlaningObject: {
    DANG_XU_LY: {
      purchasePlaningStatusIndex: 1,
      indexOrder: 1,
      name: "Đang xử lý",
    },
    DA_DUYET_DANH_MUC: {
      purchasePlaningStatusIndex: 2,
      indexOrder: 2,
      name: "Đã duyệt danh mục",
    },
    DA_DUYET_BAO_GIA: {
      purchasePlaningStatusIndex: 3,
      indexOrder: 3,
      name: "Đã duyệt báo giá",
    },
    KET_THUC: {
      purchasePlaningStatusIndex: 4,
      indexOrder: 4,
      name: "Đã kết thúc",
    },
  },

  statusPurchasePlaning: {
    DANG_XU_LY: { code: 1, name: "Đang xử lý" },
    DA_XU_LY: { code: 2, name: "Đã duyệt" },
    DA_KET_THUC: { code: 3, name: "Đã kết thúc" },
  },

  statusProduct: {
    CHUA_MUA: { code: 1, name: "Chưa mua" },
    DA_MUA: { code: 2, name: "Đã mua" },
  },

  statusLiquidate: {
    all: 0,
    delete: 1,
  },

  rowsPerPageOptions: {
    popup: [5, 10, 20, 30],
    table: [5, 10, 25, 50, 75, 100],
  },

  tabTransfer: {
    tabAll: 0, // tất cả trạng thái
    tabWaitConfirmation: 1, //chờ xác nhận
    tabConfirmation: 2, //thái đã xác nhận
    tabTransferred: 3, //đã điều chuyển
    tabCancelConfirm: 4, //Hủy xác nhận
  },

  tabInventory: {
    tabAll: 0, //tất cả trạng thái
    tabPlan: 1, // trạng thái thêm mới
    tabTakingInventory: 2, // trạng thái đang kiểm kê
    tabInventory: 3, //trạng thái đã kiểm kê
  },

  tabDialogInventory: {
    tabInfo: 0,
    tabNotRecorded: 1,
    tabCouncil: 2,
  },

  tabAllocation: {
    tabAll: 0, //tất cả trạng thái
    tabNew: 1, // trạng thái tạo mới
    tabWaitConfirmation: 2, //trạng thái chờ xác nhận
    tabAllocated: 3, // trạng thái đã cấp phát
    tabReturn: 4, //trạng thái trả về
  },

  tabCalibration: {
    tabAll: 0, //tất cả trạng thái
    tabNew: 1, // trạng thái tạo mới
    tabApprovedList: 2, //trạng thái chờ xác nhận
    tabAllocatedCost: 3, // trạng thái đã cấp phát
    tabEnd: 4, //trạng thái trả về
  },

  tabLiquidate: {
    tabAll: 0, //Trạng thái tất cả
    tabPlan: 1, //Trạng thái kế hoạch
    tabIsLiquiDating: 2, // trạng thái đang thanh lý
    tabLiquidated: 3, // trạng thái đã thanh lý
  },

  tabTransferToAnother: {
    tabAll: 0, //tất cả trạng thái
    tabPlan: 1, // tab kế hoạch
    tabMoving: 2, // tab đang chuyển đi
    tabMoved: 3, //đã chuyển đi
  },

  tabAsset: {
    tabManagement: 0, //Tài sản quản lý
    tabGrantedForUse: 1, //Được cấp sử dụng
    tabFullyDepreciated: 2, // Hết niên hạn (đã KH hết)
  },
  tabVerificationCalibration: {
    effective: 0, //Có hiệu lực
    ineffective: 1, //Hết hiệu lực
    all: 2, //Tất cả
  },
  tabVerification: {
    pending: 0, //Chờ xử lý
    verificating: 1, //Đang kiểm định
    verificated: 2, //Đã kiểm định
    acceptance: 3, //Nghiệm thu
  },
  tabVerificationStatus: {
    pending: 1, //Chờ xử lý
    verificating: 2, //Đang kiểm định
    verificated: 3, //Đã kiểm định
  },

  tabPurchasePlaning: {
    tabAll: 0, // Tất cả
    tabPlan: 1, //Đang xử lý
    tabProcessed: 2, // Đã xử lý
    tabQuoteApproved: 3, // Đã kết thúc
    tabEnd: 4, // Đã kết thúc
  },

  tabVerificationPlanning: {
    tabAll: 0, // Tất cả
    tabNew: 1, //Tạo mới
    tabApprovedList: 2, // Đã duyệt danh mục
    tabApprovedCost: 3, // Đã duyệt báo giá
    tabApprovedEnd: 4, // Kết thúc
  },

  active: {
    edit: 0, //sửa
    delete: 1, //xóa
    view: 2, //xem
    check: 3, //duyêt phiếu
    receive: 4, //tiếp nhận
    print: 5, //print,
    propose: 6, // đề xuất
    history: 7, // history
    compare: 8, // so sánh,
    qrcode: 9,
    card: 10,
    copy: 11,
    cancel: 12,
  },

  documentType: {
    IAT_DOCUMENT: "IAT_DOCUMENT",
    ASSET_DOCUMENT: "ASSET_DOCUMENT",
    ASSET_DOCUMENT_ALLOCATION: "ASSET_DOCUMENT_ALLOCATION",
    ASSET_DOCUMENT_INVENTORY_COUNT: "ASSET_DOCUMENT_INVENTORY_COUNT",
    ASSET_DOCUMENT_LIQUIDATE: "ASSET_DOCUMENT_LIQUIDATE",
    ASSET_DOCUMENT_MAINTAIN: "ASSET_DOCUMENT_MAINTAIN",
    ASSET_DOCUMENT_RECEIPT: "ASSET_DOCUMENT_RECEIPT",
    ASSET_DOCUMENT_TRANSFER: "ASSET_DOCUMENT_TRANSFER",
    ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT:
      "ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT",
  },
  documentTypeNumbers: {
    IAT_DOCUMENT: 0,
    ASSET_DOCUMENT: 1,
    ASSET_DOCUMENT_RECEIPT: 2,
    ASSET_DOCUMENT_TRANSFER: 3,
    ASSET_DOCUMENT_ALLOCATION: 4,
    ASSET_DOCUMENT_MAINTAIN: 5,
    ASSET_DOCUMENT_INVENTORY_COUNT: 6,
    ASSET_DOCUMENT_LIQUIDATE: 7,
    ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT: 8,
  },
  //Danh sách trạng thái TS khi tạo Phiếu đề xuất Bảo trì, bảo dưỡng
  listStatusAssetInMaintenanceProposal: [
    { code: 1, indexOrder: 1, name: "Chờ tổng hợp" },
    { code: 2, indexOrder: 2, name: "Đã tổng hợp" },
  ],
  listStatusCalibrationObject: {
    MOI_TAO: { code: 1, indexOrder: 1, name: "Mới tạo" },
    DA_DUYET_DANH_MUC: { code: 2, indexOrder: 2, name: "Đã duyệt danh mục" },
    DA_DUYET_BAO_GIA: { code: 3, indexOrder: 3, name: "Đã duyệt báo giá" },
    DA_DUYET_KET_THUC: { code: 4, indexOrder: 4, name: "Kết thúc" },
  },
  listStatuscalibration: [
    { code: 1, indexOrder: 1, name: "Mới tạo" },
    { code: 2, indexOrder: 2, name: "Đã duyệt danh mục" },
    { code: 3, indexOrder: 3, name: "Đã duyệt báo giá" },
    { code: 4, indexOrder: 4, name: "Kết thúc" },
  ],

  listStatusverificationObject: {
    CO_HIEU_LUC: { code: 1, indexOrder: 1, name: "Đang xử lý" },
    HET_HIEU_LUC: { code: 2, indexOrder: 2, name: "Đã xử lý" },
  },
  listStatusverification: [
    { code: 1, indexOrder: 1, name: "Đang xử lý" },
    { code: 2, indexOrder: 2, name: "Đã xử lý" },
  ],

  listStatusAssetsKDObject: {
    CHO_XU_LY: { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    DANG_KIEM_DINH: { code: 2, indexOrder: 2, name: "Đang kiểm định" },
    DA_KIEM_DINH: { code: 3, indexOrder: 3, name: "Đã kiểm định" },
  },
  listStatusAssetsHCObject: {
    CHO_XU_LY: { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    DANG_HIEU_CHUAN: { code: 2, indexOrder: 2, name: "Đang hiệu chuẩn" },
    DA_HIEU_CHUAN: { code: 3, indexOrder: 3, name: "Đã hiệu chuẩn" },
  },

  listStatusAssetsKD: [
    { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    { code: 2, indexOrder: 2, name: "Đang kiểm định" },
    { code: 3, indexOrder: 3, name: "Đã kiểm định" },
  ],

  listStatusAssetsHC: [
    { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    { code: 2, indexOrder: 2, name: "Đang hiệu chuẩn" },
    { code: 3, indexOrder: 3, name: "Đã hiệu chuẩn" },
  ],

  listFormNameAssetInMaintenanceProposal: {
    TU_BAO_TRI: {
      code: 1,
      indexOrder: 1,
      name: "Tự bảo trì",
      id: "7a9e2dde-dafa-4ae5-9926-1b98ac9407fa",
    },
  },

  tabStatus: {
    tabWaiting: 0, // Chờ xử lý
    tabProcessed: 1, //Đã xử lý,
    tabMaintenance: 2, //Đề xuất bảo trì
    tabPlan: 3, //Kế hoạch
    tabPreProcessed: 4, //Đang xử lý
    tabContractLiquidate: 5, // Thanh lý hợp đồng
    tabAcceptance: 6, //Nghiệm thu
    tabMaintenanceAssets: 0, // Tài sản bảo trì
    tabMaintenanceProposals: 1, // Đề xuất bảo trì
    tabAll: 0, //Tất cả
    tablPending: 1, // Chờ xử lý
    tabProcessing: 2, // Đang xử lý
    tabApproved: 3, // Đã xử lý
  },
  statusAsset: {
    processing: 1, //chờ xử lý
    approved: 2, //Đã xử lý
    STORAGE_NEW: 0, //Lưu kho, mới
    USEING: 2, // Đang sử dụng
  },
  statusPhieu: {
    dangTongHop: 1,
    daTongHop: 2,
  },
  listHinhThuc: {
    TU_BAO_TRI: "TBT",
    BAO_TRI_HANG: "BTH",
    BAO_TRI_NGOAI: "BTN",
  },
  CHU_KY_STATUS: {
    CHO_XU_LY: 1,
    DA_XU_LY: 2,
  },
  SESSION_STORAGE_KEY: {
    DEPARTMENT_USER: "departmentUser",
    CURRENT_USER: "currentUser",
    CONFIG_ORGANIZATION: "config_organizations",
    ADDRESS_OF_ENTERPRISE: "addressOfEnterprise",
    EVENTSOURCE: "event-source",
    NAVIGATIONS: "navigations",
    ACCESS_TOKEN_EXPIRED: "ATE",
    AUTH_USER: "AU",
    JWT_ACCESS_TOKEN: "JAT",
  },
  listStatusAssetReevaluate: {
    CHO_DANH_GIA: { indexOrder: 1, name: "Chờ đánh giá" },
    DANG_DANH_GIA: { indexOrder: 2, name: "Đang đánh giá" },
    DA_DANH_GIA: { indexOrder: 3, name: "Đã đánh giá" },
  },
  statusAssetReevaluate: [
    { id: 1, name: "Chờ đánh giá" },
    { id: 2, name: "Đang đánh giá" },
    { id: 3, name: "Đã đánh giá" },
  ],
  listOriginUnderHealthInsurance: [
    { code: 1, name: "1 - Nguồn ngân sách" },
    { code: 2, name: "2 - Xã hội hóa" },
    { code: 3, name: "3 - Nguồn khác" },
  ],
  // Phân loại tài sản trong kho
  typeOfWarehouseAssets: [
    { id: "A", name: "A - Mới nhập về" },
    { id: "B", name: "B - Các phòng ban trả lại vẫn sử dụng được" },
    { id: "C", name: "C - Các phòng ban trả lại không sử dụng được" },
  ],
  STATUS_TAI_SAN_NGHIEM_THU: {
    LOAI_A: { code: "A", name: "A - Mới nhập về" },
    LOAI_B: { code: "B", name: "B - Các phòng ban trả lại vẫn sử dụng được" },
    LOAI_C: { code: "C", name: "C - Các phòng ban trả lại không sử dụng được" },
  },
  expectedRecoveryStatus: [
    { id: 1, name: "Đang thống kê" },
    { id: 2, name: "Đã thống kê" },
  ],
  STATUS_EXPECTED_RECOVERY: {
    DANG_THONG_KE: { id: 1, name: "Đang thống kê" },
    DA_THONG_KE: { id: 2, name: "Đã thống kê" },
    DANG_TONG_HOP: { id: 3, name: "Đang tổng hợp" },
    DA_THU_HOI: { id: 4, name: "Đã thu hồi" },
  },
  revokedstatus: [
    { indexOrder: 1, id: 1, name: "Đang tổng hợp" },
    { indexOrder: 2, id: 2, name: "Đã thu hồi" },
  ],
  STATUS_REVOKED: {
    PROCESSING: { id: 1, name: "Đang tổng hợp" },
    PROCESSED: { id: 2, name: "Đã thu hồi" },
  },
  tabDeviceTroubleshootingDialog: {
    INFO: {
      valueTab: "ReportIncident",
      nameTab: "Thông tin sự cố",
    },
    EVALUATE: {
      valueTab: "RateIncident",
      nameTab: "Đánh giá sự cố",
    },
    ACCEPTANCE: {
      valueTab: "AcceptIncident",
      nameTab: "Nghiệm thu sự cố",
    },
    ATTACHMENTS: {
      valueTab: "Attachments",
      nameTab: "HỒ SƠ ĐÍNH KÈM",
    },
  },
  //Hình thức sửa chữa
  HINH_THUC_SUA_CHUA: {
    DON_VI_SUA_CHUA: {
      code: 1,
      name: "Đơn vị sửa chữa",
    },
    SUA_CHUA_NGOAI: {
      code: 2,
      name: "Sửa chữa ngoài",
    },
  },
  //Quy mô sửa chữa
  QUY_MO_SUA_CHUA: {
    SC_LON: {
      code: 1,
      name: "Sửa chữa lớn",
    },
    SC_NHO_LE: {
      code: 2,
      name: "Sửa chữa/Thay thế nhỏ lẻ",
    },
  },
  LIST_HINH_THUC_SUA_CHUA: [
    {
      code: 1,
      name: "Đơn vị sửa chữa",
    },
    {
      code: 2,
      name: "Sửa chữa ngoài",
    },
  ],
  // Phân loại tài sản trong kho
  TYPE_OF_WAREHOUSE_ASSETS: {},
  //   Tình trạng nghiệm thu:
  STATUS_NGHIEM_THU: {
    THIET_BI_SU_DUNG_DUOC: {
      code: 1,
      name: "Thiết bị sử dụng được",
    },
    THIET_BI_KHONG_SU_DUNG_DUOC: {
      code: 2,
      name: "Thiết bị không sử dụng được",
    },
  },
  LIST_STATUS_NGHIEM_THU: [
    {
      code: 1,
      name: "Thiết bị sử dụng được",
    },
    {
      code: 2,
      name: "Thiết bị không sử dụng được",
    },
  ],

  // Yêu cầu xử lý:
  YEU_CAU_XU_LY: {
    TAN_DUNG_LINH_KIEN_DE_THAY_THE: {
      code: 1,
      name: "Tận dụng linh kiện để thay thế",
    },
    BAN_PHE_LIEU: {
      code: 2,
      name: "Bán phế liệu",
    },
    THU_HOI_THANH_LY: {
      code: 3,
      name: "Thu hồi thanh lý",
    },
    HUY_PHAN_LOAI_RAC: {
      code: 4,
      name: "Hủy phân loại rác",
    },
    KHAC: {
      code: 5,
      name: "Khác",
    },
  },
  listStatusReportIncidentForm: [
    { id: 1, name: "Chờ xử lý" },
    { id: 2, name: "Đang xử lý" },
    { id: 3, name: "Chờ duyệt" },
    { id: 4, name: "Đã duyệt" },
    { id: 5, name: "Yêu cầu bổ sung" },
    { id: 6, name: "Đã xử lý" },
  ],
  TABS_REPAIR: {
    ALL: { code: 0, name: "Tất cả" },
    WAITING: { code: 1, name: "Chờ xử lý" },
    PROCESSING: { code: 2, name: "Đang xử lý" },
    PENDING_APPROVAL: { code: 3, name: "Chờ duyệt" },
    APPROVAL: { code: 4, name: "Đã duyệt" },
    REQUEST_FOR_ADDITIONAL_INFORMATION: { code: 5, name: "Yêu cầu bổ sung" },
    PROCESSED: { code: 6, name: "Đã xử lý" },
  },

  statusReportIncidentForm: {
    CHO_XU_LY: { code: 1, name: "Chờ xử lý" },
    DANG_XU_LY: { code: 2, name: "Đang xử lý" },
    DA_XU_LY: { code: 3, name: "Đã xử lý" },
  },
  LANGUAGES: {
    VIE: "vi",
    ENG: "en",
  },
  listMadeIn: {
    MUA_TRONG_NUOC: { id: 1, name: "Mua trong nước" },
    MUA_NGOAI_NUOC: { id: 2, name: "Mua ngoài nước" },
  },
  voucherType: {
    KIEM_KE: {
      id: 1,
      name: "Kiểm kê",
    },
    THANH_LY: {
      id: 2,
      name: "Thanh lý",
    },
  },
  PAGE_INDEX: {
    FIRST_PAGE: 0,
  },
  TYPE_CODES: {
    NCC_BDSC: "NCC-BDSC", // Nhà cung cấp bảo dưỡng, sửa chữa
    NCC_CU: "NCC-CU", //Nhà cung cấp cung ứng,
    NCC_KD: "NCC-KD", //Nhà cung cấp kiểm định
    NCC_KX: "NCC-KX", // Nhà cung cấp kiểm xạ
    NCC_HC: "NCC-HC", // Nhà cung cấp hiệu chuản
  },
  tabReevaluate: {
    tabAll: 0,
    tabNew: 1,
    tabReevaluating: 2,
    tabReevaluated: 3,
  },
  useTypes: [
    { code: 1, name: "Sử dụng vật tư trong kho" },
    { code: 2, name: "Vật tư mua ngoài không nhập kho" },
  ],
  //Hình thức kiểm định
  HINH_THUC_KIEM_DINH: {
    LAN_DAU: {
      code: "1",
      name: "Lần đầu",
    },
    DINH_KY: {
      code: "2",
      name: "Định kỳ",
    },
    DOT_XUAT: {
      code: "3",
      name: "Đột xuất",
    },
  },
  hinhThucKiemDinhOptions: [
    { code: 1, name: "Lần đầu" },
    { code: 2, name: "Định kỳ" },
    { code: 3, name: "Đột xuất" },
  ],
  KET_QUA_KIEM_DINH: {
    DAT: {
      code: "1",
      name: "Đạt",
    },
    KHONG_DAT: {
      code: "2",
      name: "Không đạt",
    },
  },
  TRANG_THAI_KIEM_DINH: {
    CON_HIEU_LUC: {
      code: "1",
      name: "Còn hiệu lực",
    },
    HET_HIEU_LUC: {
      code: "2",
      name: "Hết hiệu lực",
    },
  },
  historyTabs: {
    allocation: 0, // Tab ls cấp phát
    transfer: 1, // Tab ls điều chuyển
    repairing: 2, // Tab ls sửa chữa
    maintainPlaning: 3, // Tab ls bảo dưỡng
    maintainCalibration: 4, // Tab ls kiểm định - hiệu chuẩn
  },
  madeIn: [
    { code: 1, name: "Trong nước" },
    { code: 2, name: "Ngoài nước" },
  ],
  MADE_IN: {
    TRONG_NUOC: { code: 1, name: "Trong nước" },
    NGOAI_NUOC: { code: 2, name: "Ngoài nước" },
  },
  madeInCode: {
    TRONG_NUOC: 1,
    NGOAI_NUOC: 2,
  },
  typeStore: [
    { code: 0, name: "Kho vật tư" },
    { code: 1, name: "Kho tài sản" },
  ],
  typeStoreObject: {
    KHO_VAT_TU: { code: 0, name: "Kho vật tư" },
    KHO_TAI_SAN: { code: 1, name: "Kho tài sản" },
  },
  tabAggregateAssetsByAssetGroup: {
    tabTSCD: 0,
    tabCCDC: 1,
  },
  tabWarehouseTransfer: {
    tabAll: 0,
    tabNew: 1,
    tabProcessing: 2,
    tabEnd: 3,
  },
  tabSuggestSupplies: {
    tabAll: 0,
    tabSuggest: 1,
    tabConfirm: 2,
    tabRefuse: 3,
    tabApproved: 4,
    tabReview: 5,
  },
  tabInventoryDeliveryVoucher: {
    tabAll: 0,
    tabConfirm: 1,
    tabRefuse: 2,
    tabWaiting: 3,
    tabApproved: 4,
  },
  typeExcel: {
    excel: 0,
    excelQr: 1,
  },
  listStatusInventoryStore: [
    { code: "MOITAO", indexOrder: 1, name: "Mới tạo" },
    { code: "DANGTHUCHIEN", indexOrder: 2, name: "Đang thực hiện" },
    { code: "KETTHUC", indexOrder: 3, name: "Kết thúc" },
  ],

  rowsPerPage: {
    category: 10,
    voucher: 5,
  },
  NOTE_INDEX: {
    IS_TEMPOARY: 0,
    IS_MANAGEMENT_CODE: 1,
    IS_MULTIPLE: 2,
    IS_MANAGE_ACCOUNTANT: 3,
    IS_ALLOCATION: 4,
    IS_TOTAL_ALLOCATION_TIME: 5,
    IS_QUANTITY: 6,
  },
  CCDC_NOTE_MESSAGE: [
    {
      index: 0,
      name: "isTemporary",
      message: "CCDC có thời gian quản lý ngắn hạn.",
    },
    {
      index: 1,
      name: "isManagementCode",
      message: "Phòng ban quản lý sẽ dùng mã quản lý, để quản lý CCDC.",
    },
    {
      index: 2,
      name: "isMultiple",
      message: [
        "Khi chọn tính năng sẽ kích hoạt nhân bản nhập nhiều số Serial một lần để nhân bản ra nhiều CCDC khác Serial.",
        "CHÚ Ý: Số lượng số serial không được quá số lượng bản ghi đã nhập.",
      ],
    },
    {
      index: 3,
      name: "isManageAccountant",
      message: "CCDC sẽ được quản lý bởi kế toán.",
    },
    {
      index: 4,
      name: "isAllocation",
      message: [
        "Kỳ phân bổ sẽ xác định giá trị của Công cụ dụng cụ được phân bổ theo kỳ kế toán nào: ",
        "- Phân bổ hết vào chi phí 1 lần hay hàng tháng/năm phân bổ vào chi phí 1 lần. ",
        "- Chọn Kỳ phân bổ là 'Tháng' hoặc 'Năm' trong trường hợp giá trị của CCDC phân bổ vào chi phí qua nhiều kỳ. " +
          "Sau đó nhập số lần tổng số lần phân bổ CCDC vào trường thông tin 'Tổng thời gian phân bổ (Số kỳ). ",
        "VD: - Chọn Kỳ phân bổ là '1 lần' thì Thời gian phân bổ là 1 - Chọn Kỳ phân bổ là 'Tháng' và nhập Thời gian phân bổ là 3",
      ],
    },
    {
      index: 5,
      name: "isTotalAllotmentTime",
      message: "Xem ghi chú ở Kỳ phân bổ hoặc Ngày bắt đầu tính phân bổ",
    },
    {
      index: 6,
      name: "isQuantity",
      message:
        "Mỗi CCDC chỉ có 1 số serial và không thể trùng nhau. Để nhập nhiều số serial vui lòng chọn nhân bản CCDC",
    },
  ],
  TSCD_NOTE_MESSAGE: [
    {
      index: 0,
      name: "isTemporary",
      message: "TSCĐ có thời gian quản lý ngắn hạn.",
    },
    {
      index: 3,
      name: "isManageAccountant",
      message: "TSCĐ sẽ được quản lý bởi kế toán.",
    },
  ],
  productTypeCode: {
    VTHH: "VTHH",
    TSCĐ: "TSCD",
    CCDC: "CCDC",
  },
  typeAssets: {
    TSCD: "fixedAssets",
    CCDC: "instrumentTools",
  },
  LOAI_KHO: {
    KHO_VAT_TU: { code: 0, name: "Kho vật tư" },
    KHO_TAI_SAN: { code: 1, name: "Kho tài sản" },
  },
  LOAI_CAP_PHAT: {
    PHONG_BAN: 1,
    NGUOI_SU_DUNG: 2,
  },
  optionSummaryReport: {
    TDCT: { code: 0, name: "Theo dõi chi tiết" },
    TDTH: { code: 1, name: "Theo dõi tổng hợp" },
  },
  optionExactSearch: {
    XDDKP: { code: 0, name: "Xem đích danh khoa phòng" },
  },
  listStatusPurchaseReqCount: [
    { code: 1, name: "Đang xử lý" },
    { code: 2, name: "Đã xử lý" },
    { code: 3, name: "Đã lập kế hoạch" },
  ],
  listStatusPurchaseReqCountObject: {
    DANG_XU_LY: { code: 1, name: "Đang xử lý" },
    DA_XU_LY: { code: 2, name: "Đã xử lý" },
    DA_LAP_KE_HOACH: { code: 3, name: "Đã lập kế hoạch" },
  },
  listStatusVerificationPlanningObject: {
    TAO_MOI: { code: 1, name: "Tạo mới" },
    DA_DUYET_DANH_MUC: { code: 2, name: "Đã duyệt danh mục" },
    DA_DUYET_BAO_GIA: { code: 3, name: "Đã duyệt báo giá" },
    DA_DUYET_KET_THUC: { code: 4, name: "Kết thúc" },
  },
  listStatusVerificationPlanning: [
    { code: 1, name: "Tạo mới" },
    { code: 2, name: "Đã duyệt danh mục" },
    { code: 3, name: "Đã duyệt báo giá" },
    { code: 4, name: "Kết thúc" },
  ],
  listStatusPurchaseReqCountDialog: [
    { code: 1, name: "Đang xử lý" },
    { code: 2, name: "Đã xử lý" },
  ],
  STATUS_PURCHASE_REQUEST_COUNT: {
    DANG_TONG_HOP: { code: 1, name: "Đang tổng hợp" },
    DA_TONG_HOP: { code: 2, name: "Đã tổng hợp" },
  },
  tabPurchaseReqCountDialog: {
    tabHasProduct: 0,
    tabProductNotYet: 1,
    tabProductNotCount: 2,
  },
  tabPurchaseReqCount: {
    tabAll: 0,
    tabCounting: 1,
    tabCounted: 2,
    tabPlaned: 3, //Đã lập kế hoạch
  },
  listStatusPurchaseRequest: [
    {
      code: 1,
      name: "Chờ tổng hợp",
    },
    {
      code: 2,
      name: "Đang tổng hợp",
    },
    {
      code: 3,
      name: "Đã tổng hợp",
    },
  ],
  listStatusPurchaseRequestObject: {
    CHO_TONG_HOP: {
      code: 1,
      name: "Chờ tổng hợp",
    },
    DANG_TONG_HOP: {
      code: 2,
      name: "Đang tổng hợp",
    },
    DA_TONG_HOP: {
      code: 3,
      name: "Đã tổng hợp",
    },
  },
  STATUS_PURCHASE_REQUEST: {
    DANG_TONG_HOP: { code: 1, name: "Chờ tổng hợp" },
    DA_TONG_HOP: { code: 2, name: "Đã tổng hợp" },
    DA_LAP_KE_HOACH: { code: 3, name: "Đã lập kế hoạch" },
  },
  tabPurchaseRequest: {
    tabAll: 0, // tất cả trạng thái
    tabWaitCount: 1, //Đang thổng hợp
    tabCounted: 2, //đã tổng hợp
    tabPlaned: 3, //Đã lập kế hoạch
  },
  TYPE_PURCHASE: {
    TSCD_CCDC: 1,
    VT: 2,
  },
  listStatusProductInPurchaseRequest: {
    CHO_DUYET: {
      code: 1,
      name: "Chờ duyệt",
    },
    DA_DUYET: {
      code: 2,
      name: "Đã duyệt",
    },
    KHONG_DUYET: {
      code: 3,
      name: "Không duyệt",
    },
  },
  listStatusProductInPurchaseRequestArray: [
    {
      code: 1,
      name: "Chờ duyệt",
    },
    {
      code: 2,
      name: "Đã duyệt",
    },
    {
      code: 3,
      name: "Không duyệt",
    },
  ],

  listAllocationPeriod: [
    { code: "ONCE", name: "1 Lần" },
    { code: "MONTH", name: "Tháng" },
    { code: "YEAR", name: "Năm" },
  ],
  ALLOCATION_PERIOD: {
    ONCE: { code: "ONCE", value: 1 },
    MONTH: { code: "MONTH", value: 36 },
    YEAR: { code: "YEAR", value: 3 },
  },
  paramPlaningDialog: {
    assets: "list/purchase_planing",
    supplies: "list/supplies_purchase_planing",
  },
  listStatusSpareParts: {
    DA_HONG: { code: 0, name: "Đã hỏng" },
    HOAT_DONG: { code: 1, name: "Hoạt động" },
  },
  STATUS_INVENTORY_COUNT_VOUCHER: {
    MOI_TAO: { code: "MOI", indexOrder: 0, name: "Mới" },
    DANG_KIEM_KE: { code: "DANGKIEMKE", indexOrder: 1, name: "Đang kiểm kê" },
    DA_KIEM_KE: { code: "DAKIEMKE", indexOrder: 2, name: "Đã kiểm kê" },
  },
  STATUS_TRANSFER: {
    CHO_XAC_NHAN: {
      transferStatusIndex: 0,
      indexOrder: 0,
      name: "Chờ xác nhận",
    },
    DA_XAC_NHAN: {
      transferStatusIndex: 1,
      indexOrder: 1,
      name: "Đã xác nhận",
    },
    TRA_VE: {
      transferStatusIndex: 2,
      indexOrder: 2,
      name: "Trả về",
    },
    DA_DIEU_CHUYEN: {
      transferStatusIndex: 3,
      indexOrder: 3,
      name: "Đã điều chuyển",
    },
    HUY_XAC_NHAN: {
      transferStatusIndex: 4,
      indexOrder: 4,
      name: "Hủy xác nhận",
    },
  },
  productPurchaseTypeCode: {
    TSCD_CCDC: "TSCD_CCDC",
    VTHH: "VTHH",
  },
  STATUS_WAREHOUSE_TRANSFER: {
    MOI_TAO: {
      code: "MOITAO",
      indexOrder: 1,
      name: "Mới tạo",
    },
    DANG_THUC_HIEN: {
      code: "DANGTHUCHIEN",
      indexOrder: 2,
      name: "Đang thực hiện",
    },
    KET_THUC: {
      code: "KETTHUC",
      indexOrder: 3,
      name: "Kết thúc",
    },
  },
  listStatusSuggestSuppliesObject: {
    DANG_THONG_KE: { code: 1, indexOrder: 1, name: "Đang thống kê" },
    DA_THONG_KE: { code: 2, indexOrder: 2, name: "Đã thống kê" },
    XAC_NHAN: { code: 3, indexOrder: 3, name: "Đã duyệt" },
    TU_CHOI: { code: 4, indexOrder: 4, name: "Từ chối" },
    DANG_DUYET: { code: 5, indexOrder: 5, name: "Đang duyệt" },
  },
  listStatusSuggestSupplies: [
    { code: 1, indexOrder: 1, name: "Đang thống kê" },
    { code: 2, indexOrder: 2, name: "Đã thống kê" },
    { code: 3, indexOrder: 3, name: "Đã duyệt" },
    { code: 4, indexOrder: 4, name: "Từ chối" },
    { code: 5, indexOrder: 5, name: "Đang duyệt" },
  ],
  listStatusInventoryDeliveryVoucher: [
    { code: 1, indexOrder: 1, name: "Xác nhận" },
    { code: 2, indexOrder: 2, name: "Từ chối" },
    { code: 3, indexOrder: 3, name: "Chờ xử lý" },
    { code: 4, indexOrder: 4, name: "Đã xử lý" },
  ],
  listStatusInventoryReceivingVoucherObject: {
    CHO_XU_LY: { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    DA_XU_LY: { code: 2, indexOrder: 2, name: "Đã xử lý" },
  },
  listStatusInventoryReceivingVoucher: [
    { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    { code: 2, indexOrder: 2, name: "Đã xử lý" },
  ],
  listPrintArSucoOptions: [
    { name: "Mẫu PVT", code: 1 },
    { name: "Mẫu HCQT", code: 2 },
    { name: "Mẫu KTTS01", code: 3 },
    { name: "Mẫu DNMTS", code: 4 },
  ],
  PRINT_AR_SUCO_OPTIONS: {
    MAU_PVT: { name: "Mẫu PVT", code: 1 },
    MAU_HCQT: { name: "Mẫu HCQT", code: 2 },
    MAU_KTTS01: { name: "Mẫu KTTS01", code: 3 },
    MAU_DNMTS: { name: "Mẫu DNMTS", code: 4 },
  },
  listStatusInventoryDeliveryVoucherObject: {
    XAC_NHAN: { code: 1, indexOrder: 1, name: "Xác nhận" },
    TU_CHOI: { code: 2, indexOrder: 2, name: "Từ chối" },
    CHO_XU_LY: { code: 3, indexOrder: 3, name: "Chờ xử lý" },
    DA_XU_LY: { code: 4, indexOrder: 4, name: "Đã xử lý" },
  },
  listHdChiTietsRole: [
    { name: "Trưởng ban", code: 1 },
    { name: "Ủy viên", code: 2 },
    { name: "Thư ký", code: 3 },
  ],
  OBJECT_HD_CHI_TIETS: {
    TRUONG_BAN: { name: "Trưởng ban", code: 1 },
    UY_VIEN: { name: "Ủy viên", code: 2 },
    THU_KY: { name: "Thư ký", code: 3 },
  },
  listHdType: [
    { name: "Thanh lý", code: 1 },
    { name: "Chuyển đi", code: 2 },
    { name: "Đánh giá lại", code: 3 },
    { name: "Kiểm kê", code: 4 },
    { name: "Kiểm nhập", code: 5 },
  ],
  OBJECT_HD_TYPE: {
    THANH_LY: { name: "Thanh lý", code: 1 },
    CHUYEN_DI: { name: "Chuyển đi", code: 2 },
    DANH_GIA_LAI: { name: "Đánh giá lại", code: 3 },
    KIEM_KE: { name: "Kiểm kê", code: 4 },
    KIEM_NHAP: { name: "Kiểm nhập", code: 5 },
  },
  listStatusHoiDong: [
    { code: 1, name: "Hoạt động" },
    { code: 0, name: "Không hoạt động" },
  ],
  STATUS_HOI_DONG: {
    HOAT_DONG: { code: 1, name: "Hoạt động" },
    KHONG_HOAT_DONG: { code: 0, name: "Không hoạt động" },
  },
  STATUS_CHUYEN_DI: {
    KE_HOACH: { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
    DANG_CHUYEN_DI: {
      code: "PROCESSING",
      indexOrder: 1,
      name: "Đang chuyển đi",
    },
    DA_CHUYEN_DI: { code: "TRANSFERRED", indexOrder: 2, name: "Đã chuyển đi" },
  },
  TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG: {
    INFO: { CODE: 0, NAME: "Thông tin phiếu" },
    SIGN: { CODE: 1, NAME: "Phần ký" },
    FILES: { CODE: 2, NAME: "Hồ sơ đính kèm" },
  },
  listStatusChuKy: [
    { code: 1, name: "Chờ xử lý" },
    { code: 2, name: "Bảo trì xong, hoạt động bình thường" },
    { code: 3, name: "Mới sửa chữa, không thực hiện bảo trì" },
    { code: 4, name: "Phát hiện hỏng, không thực hiện bảo trì" },
    { code: 5, name: "Đã BT, đề xuất thay thế linh kiện" },
  ],
  listExecutionTiems: [
    { code: 1, name: "Lần 1" },
    { code: 2, name: "Lần 2" },
    { code: 3, name: "Lần 3" },
    { code: 4, name: "Lần 4" },
  ],
  STATUS_CHU_KY: {
    CHO_XU_LY: {
      // Trạng thái này k tính tổng tiền
      code: 1,
      name: "Chờ xử lý",
    },
    DA_XU_LY: {
      code: 2,
      name: "Hoạt động bình thường",
    },
    VUA_SUA_KHONG_BT: {
      // Trạng thái này k tính tổng tiền
      code: 3,
      name: "Mới sửa chữa, không thực hiện bảo trì",
    },
    HONG_KHONG_BT: {
      // Trạng thái này k tính tổng tiền
      code: 4,
      name: "Hỏng, không bảo trì",
    },
    DA_XU_LY_DXTT_LINH_KIEN: {
      code: 5,
      name: "Đã xử lý đề xuất thay thế linh kiện",
    },
  },
  listStatusNghiemThuBTBD: [
    // { code: 0, name: "Chờ xử lý" }, // Chỉ dùng ở màn ds ck chờ xử lý
    { code: 1, name: "Đang xử lý" }, // dùng cả cho dialog NT + DS
    { code: 2, name: "Đã xử lý" }, // dùng cả cho dialog NT + DS
  ],
  STATUS_NGHIEM_THU_BTBD: {
    CHO_XU_LY: { code: 0, name: "Chờ xử lý" }, // Chỉ dùng ở màn ds ck chờ xử lý
    DANG_XU_LY: { code: 1, name: "Đang xử lý" }, // dùng cả cho dialog NT + DS
    DA_XU_LY: { code: 2, name: "Đã xử lý" }, // dùng cả cho dialog NT + DS
  },

  productTypeCodeFilterPlaning: [
    {
      code: 1,
      name: "Tài sản cố định",
    },
    {
      code: 2,
      name: "Công cụ dụng cụ",
    },
  ],
  STATUS_INVENTORY_DELIVERY_VOUCHER: {
    CHO_XU_LY: { code: 1, indexOrder: 1, name: "Chờ xử lý" },
    DA_XU_LY: { code: 2, indexOrder: 2, name: "Đã xử lý" },
  },
  tabInventoryReceivingVoucher: {
    tabAll: 0,
    tabProcessing: 1,
    tabProcessed: 2,
  },
  STATUS_MAINTAINANCE_PROPOSAL: {
    CHO_TONG_HOP: { code: 1, indexOrder: 1, name: "Chờ tổng hợp" },
    DA_TONG_HOP: { code: 2, indexOrder: 2, name: "Đã tổng hợp" },
  },
  OPTION_TYPE_KH_KIEM_DINH: [
    {
      code: 1,
      indexOrder: 1,
      name: "Lần đầu",
    },
    {
      code: 2,
      indexOrder: 2,
      name: "Định kỳ",
    },
    {
      code: 3,
      indexOrder: 3,
      name: "Đột xuất",
    },
  ],
  OBJECT_RESULT_KIEM_DINH: {
    DAT: {
      code: 1,
      indexOrder: 1,
      name: "Đạt",
    },
    KHONG_DAT: {
      code: 2,
      indexOrder: 2,
      name: "Không đạt",
    },
  },
  LIST_RESULT_KIEM_DINH: [
    {
      code: 1,
      indexOrder: 1,
      name: "Đạt",
    },
    {
      code: 2,
      indexOrder: 2,
      name: "Không đạt",
    },
  ],

  STATUS_NGHIEM_THU_KD_HC: {
    //Kiểm định - hiệu chuẩn
    DANG_XU_LY: { code: 1, name: "Đang xử lý" },
    DA_XU_LY: { code: 2, name: "Đã xử lý" },
  },

  TYPE_KD_HC: {
    //Kiểm định - hiệu chẩn
    KIEM_DINH: { code: 1, name: "Kiểm định" },
    HIEU_CHUAN: { code: 2, name: "Hiệu chuẩn" },
    KIEM_XA: { code: 3, name: "Kiểm xạ phòng" },
    KIEM_DINH_KIEM_XA: { code: 4, name: "Kiểm định - Kiểm xạ phòng" },
  },

  listTemporary: [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
  ],

  listManageAccountant: [
    { value: true, name: "Tài sản tính khấu hao" },
    { value: false, name: "Tài sản không tính khấu hao" },
  ],

  listOptionsExcelMaintainPlaning: [
    {
      name: "Xuất Excel tất cả",
      code: "asset_maintenance",
    },
    {
      name: "Xuất Excel đã có hợp đồng",
      code: "asset_get_contracted",
    },
    {
      name: "Xuất Excel không có hợp đồng",
      code: "asset_accept_without_contract",
    },
    {
      name: "Xuất Excel chưa được chốt báo giá",
      code: "asset_without_maintenance_price",
    },
  ],
  OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING: {
    TAT_CA_OLD_TEMPLATE: {
      name: "Xuất Excel tất cả",
      code: "asset _maintenance_unapproved",
    },
    TAT_CA_NEW_TEMPLATE: {
      name: "Xuất Excel tất cả",
      code: "asset_maintenance",
    },
    DA_CO_HOP_DONG: {
      name: "Xuất Excel đã có hợp đồng",
      code: "asset_get_contracted",
    },
    KHONG_CO_HOP_DONG: {
      name: "Xuất Excel không có hợp đồng",
      code: "asset_accept_without_contract",
    },
    CHUA_DUOC_CHOT_GIA_HOP_DONG: {
      name: "Xuất Excel chưa được chốt giá",
      code: "asset_without_maintenance_price",
    }, // Giống với làm thầu nhưng khác tên dựa trên đơn vị
  },

  TYPES_CONTRACT: {
    BTBD: "HDBTBD",
    KD: "HDKD",
    HC: "HDHC",
    KX: "HDKX",
    CU: "HDCU",
  },

  OBJECT_TYPES_HHTH_MAINTAIN: {
    TU_BAO_TRI: { name: "Tự bảo trì", code: "TBT" },
    BAO_TRI_HANG: { name: "Bảo trì hãng", code: "BTH" },
    BAO_TRI_NGOAI: { name: "Bảo trì ngoài", code: "BTN" },
  },

  localizationVi: {
    pagination: {
      labelDisplayedRows: "{from}-{to} trong {count}",
      firstTooltip: "Trang đầu",
      previousTooltip: "Trang trước",
      nextTooltip: "Trang tiếp",
      lastTooltip: "Trang cuối",
      labelRowsSelect: "hàng mỗi trang",
    },
  },

  TYPES_FILE: {
    PDF: "application/pdf",
    IMAGE: {
      JPG: "image/jpg",
      JPEG: "image/jpeg",
      PNG: "image/png",
    },
    IMAGE_ARRAY: ["image/jpg", "image/jpeg", "image/png"],
    TEXT: "text/plain",
    EXCEL: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  },

  listOptionsExcelPurchaseRequest: [
    {
      name: "Xuất Excel tất cả",
      code: 1,
    },
    {
      name: "Xuất Excel theo bản ghi",
      code: 2,
    },
  ],
  OBJECT_OPTIONS_EXCEL_PURCHASE_REQUEST: {
    TAT_CA: {
      name: "Xuất Excel tất cả",
      code: 1,
    },
    THEO_BAN_GHI: {
      name: "Xuất Excel theo bản ghi",
      code: 2,
    },
  },

  OBJECT_PERMISSION_DEFAULT: {
    isView: false,
    isShow: false,
    isDisabled: false,
    isRequired: false,
  },

  OBJECT_CHART: {
    listTimeUnits: [
      { name: "Tháng", code: 1 },
      { name: "Quý", code: 2 },
      { name: "Năm", code: 3 },
    ],
    TIME_UNITS: {
      MONTH: { name: "Tháng", code: 1 },
      QUARTER: { name: "Quý", code: 2 },
      YEAR: { name: "Năm", code: 3 },
    },
    listQuarter: [
      { name: "Quý 1", code: 1 },
      { name: "Quý 2", code: 2 },
      { name: "Quý 3", code: 3 },
      { name: "Quý 4", code: 4 },
    ],
  },

  OBJECT_THEME: {
    THEME_BLUE: {
      name: "Xanh da trời",
      code: "themeBlue",
    },
    THEME_GREEN: {
      name: "Xanh lá",
      code: "themeGreen",
    },
    THEME_199: {
      name: "Xanh ngọc bích",
      code: "themeLightJadeGreen",
    },
  },
  listTheme: [
    {
      name: "Xanh da trời",
      code: "themeBlue",
    },
    {
      name: "Xanh lá",
      code: "themeGreen",
    },
  ],

  STATUS_RECEIPT: {
    waitApprove: {
      name: "Đang thống kê",
      code: "waitApprove",
      indexOrder: 1,
    },
    approved: {
      name: "Đã thống kê",
      code: "approved",
      indexOrder: 2,
    },
    allocated: {
      name: "Đã duyệt",
      code: "allocated",
      indexOrder: 3,
    },
    refuse: {
      name: "Từ chối",
      code: "allocated",
      indexOrder: 4,
    },
    review: {
      name: "Đang duyệt",
      code: "review",
      indexOrder: 5,
    },
    cancelApproval: {
      name: "Hủy duyệt",
      code: "cancelApproval",
      indexOrder: 6,
    },
  },
  LIST_RECEIPT: [
    {
      name: "Đang thống kê",
      code: "waitApprove",
      indexOrder: 1,
    },
    {
      name: "Đã thống kê",
      code: "approved",
      indexOrder: 2,
    },
    {
      name: "Đang duyệt",
      code: "review",
      indexOrder: 5,
    },
    {
      name: "Đã duyệt",
      code: "allocated",
      indexOrder: 3,
    },
    {
      name: "Từ chối",
      code: "refuse",
      indexOrder: 4,
    },
  ],
  OBJECT_TAB_RECEIPT: {
    all: 0,
    waitApprove: 1,
    approved: 2,
    allocated: 3,
    refuse: 4,
    review: 5,
  },

  TYPE_DASHBOARD: {
    ANALYTICS: 1,
    NOTIFICATION: 2,
  },

  REQUIREMENTS_CATEGORY: {
    ALLOCATION: { code: 1, name: "Cấp phát" },
    TRANSFER: { code: 2, name: "Điều chuyển" },
    REPAIR: { code: 3, name: "Sửa chữa - Thay thế" },
    MAINTENANCE: { code: 4, name: "Bảo trì - Bảo dưỡng" },
    ASSET_PURCHASING: { code: 5, name: "Mua sắm tài sản" },
    REPOSSESSION: { code: 6, name: "Thu hồi" },
  },
  STATUS_REQUIREMENTS: {
    NOT_PROCESSED_YET: 1,
    PROCESSED: 2,
  },
  TYPE_REQUIREMENT: {
    RECEIVE_ALLOCATION_FIXED_ASSET: {
      code: "1.1",
      name: "Tiếp nhận cấp phát TSCĐ",
    },
    RECEIVE_ALLOCATION_IAT: { code: "1.2", name: "Tiếp nhận cấp phát CCDC" },
    APPROVE_RECEIPT_FIXED_ASSET: {
      code: "1.3",
      name: "Phê duyệt phiếu lĩnh TSCĐ",
    },
    APPROVE_RECEIPT_FIXED_IAT: {
      code: "1.4",
      name: "Phê duyệt phiếu lĩnh CCDC",
    },
    APPROVE_TRANSFER_FIXED_ASSET: {
      code: "2.1",
      name: "Xác nhận phiếu điều chuyển TSCĐ",
    },
    RECEIVE_TRANSFER_FIXED_ASSET: {
      code: "2.2",
      name: "Tiếp nhận phiếu điều chuyển TSCĐ",
    },
    APPROVE_TRANSFER_IAT: {
      code: "2.3",
      name: "Xác nhận phiếu điều chuyển CCDC",
    },
    RECEIVE_TRANSFER_IAT: {
      code: "2.4",
      name: "Tiếp nhận phiếu điều chuyển CCDC",
    },
    REPAIR_REPORT: { code: "3.1", name: "Phê duyệt phiếu lĩnh TSCĐ" },
    REPAIR_EVALUATE: { code: "3.2", name: "Tiếp nhận cấp phát TSCĐ" },
    REPAIR_ACCEPTANCE: { code: "3.3", name: "Phê duyệt phiếu lĩnh CCDC" },
    RECEIVE_MAINTAIN_PROPOSAL: {
      code: "4.1",
      name: "Tiếp nhận đề xuất bảo trì",
    },
    APPROVE_MAINTAIN_PLANING: {
      code: "4.2",
      name: "Duyệt danh mục bảo trì",
    },
    APPROVE_MAINTAIN_PLANING_SIGN_CONTRACT: {
      code: "4.3",
      name: "Duyệt danh mục báo giá bảo trì",
    },
    APPROVE_PURCHASE_REQUEST_COUNT: {
      code: "5.1",
      name: "Phê duyệt danh mục tổng hợp YCMS",
    },
    APPROVE_PURCHASE_REQUEST_SIGN_CONTRACT: {
      code: "5.2",
      name: "Duyệt danh mục báo giá TS YCMS",
    },
    MAINTAIN_PLANING_SIGN_CONTRACT: {
      code: "5.3",
      name: "Phê duyệt phiếu lĩnh CCDC",
    },
  },

  listUsageState: [
    { code: "A", name: "Mới nhập" },
    { code: "B", name: "Vẫn sử dụng được" },
    { code: "C", name: "Hỏng, không sử dụng được" },
  ],
  OBJECT_USAGE_STATE: {
    NEW: { code: "A", name: "Mới nhập" },
    IN_WORKING_CONDITION: { code: "B", name: "Vẫn sử dụng được" },
    IN_DISREPAIR: { code: "C", name: "Hỏng, không sử dụng được" },
  },

  OBJECT_SEARCH_MAX_SIZE: {
    pageIndex: 1,
    pageSize: 100000,
  },
  OBJECT_USED_CONDITION: {
    TOT: { code: 1, name: "Tốt" },
    TRUNG_BINH: { code: 2, name: "Trung bình" },
    HONG: { code: 3, name: "Hỏng" },
  },

  tabRecallSlip: {
    tabExpected: 0, // tab dự kiến thu hồi
    tabRecall: 1, // tab thu hồi
  },

  tabRecallSlipTool: {
    tabExpected: 0, // tab dự kiến thu hồi
    tabRecall: 1, // tab thu hồi
  },

  OBJECT_RECALLSLIP: {
    THU_HOI: { code: 1, name: "Thu hồi" },
    DU_KIEN_THU_HOI: { code: 0, name: "Dự kiến thu hồi" },
  },

  OBJECT_STATUS_EXPORT_STORE: {
    exported: { name: "Đã xuất" },
    notExported: { name: "Chưa xuất" },
  },

  listProductManagementType: [
    { value: true, name: "Quản lý riêng lẻ" },
    { value: false, name: "Quản lý nhóm" },
  ],
  PRODUCT_MANAGEMENT_TYPE: {
    SINGLE_MANAGEMENT: { value: true, name: "Quản lý riêng lẻ" },
    GROUP_MANAGEMENT: { value: false, name: "Quản lý nhóm" },
  },

  OBJECT_ACTIVE: {
    [true]: { value: true, name: "Đã kích hoạt" },
    [false]: { value: false, name: "chưa kích hoạt" },
  },

  LIST_TABS_ASSET_RECEPTION: [
    { name: "Tất cả", code: 0 },
    { name: "Chờ xử lý", code: 1 },
    { name: "Đã xử lý", code: 2 },
  ],
  TABS_ASSET_RECEPTION: {
    ALL: { name: "Tất cả", code: 0 },
    PROCESSING: { name: "Chờ xử lý", code: 1 },
    PROCESSED: { name: "Đã xử lý", code: 2 },
  },
  LIST_STATUS_ASSET_RECEPTION: [
    {
      name: "Chờ xử lý",
      indexOrder: 1,
      code: 1,
    },
    {
      name: "Đã xử lý",
      indexOrder: 2,
      code: 2,
    },
  ],
  STATUS_ASSET_RECEPTION: {
    CHO_XU_LY: {
      name: "Chờ xử lý",
      indexOrder: 1,
    },
    DA_XU_LY: {
      name: "Đã xử lý",
      indexOrder: 2,
    },
  },
  TAB_STATUS_ASSET_RECEPTION: {
    ALL: 0,
    CHO_XU_LY: 1,
    DA_XU_LY: 2,
  },

  TYPES_MONEY: {
    DOT: { code: 1, name: "Dấu chấm", value: "." },
    COMMA: { code: 2, name: "Dấu phẩy", value: "," },
    SPACE: { code: 3, name: "Dấu cách", value: " " },
  },

  TYPES_EXPORT_STORE: {
    XUAT_KHO: { code: 0, name: "Xuất kho" },
    THEM_MOI: { code: 1, name: "Thêm mới" },
  },

  INVALID_END_PARAMS: ["[", "]"],

  EXPORT_SUPPLY_STATUS: {
    DU_KIEN_XUAT_KHO: {
      code: "DU_KIEN_XUAT_KHO",
      indexOrder: 1,
      name: "Dự kiến xuất kho",
    },
    DA_XUAT_KHO: { code: "DA_XUAT_KHO", indexOrder: 2, name: "Đã xuất kho" },
    MUA_NGOAI: { code: "MUA_NGOAI", indexOrder: 3, name: "Mua ngoài" },
    DA_XOA: { code: "DA_XOA", indexOrder: 4, name: "Đã xóa" },
  },
  TAB_PURCHASE_DIALOG: {
    INFO: { code: 0, name: "Thông tin phiếu" },
    INFO_EVALUATE: { code: 1, name: "Thông tin đánh giá" },
    INFO_CONFIRM: { code: 1, name: "Thông tin xác nhận" },
  },

  FILE_TYPE: {
    IMAGE: 1,
    ATTACHMENTS: 2,
  },

  listComprehensiveItemsTabs: [
    { code: 1, name: "TSCĐ" },
    { code: 2, name: "CCDC" },
    { code: 3, name: "VT" },
  ],
  TABS_COMPREHENSIVE_ITEMS: {
    TSCD: { code: 1, name: "Tài sản cố định" },
    CCDC: { code: 2, name: "Công cụ dụng cụ" },
    VT: { code: 3, name: "Vật tư" },
  },

  STATUS_BIDDING: {
    OPEN: { code: 1, name: "Mở thầu" },
    CLOSE: { code: 2, name: "Đóng thầu" },
  },
  LIST_STATUS_BIDDING: [
    { code: 1, name: "Mở thầu" },
    { code: 2, name: "Đóng thầu" },
  ],

  listTabsApprovalRequest: [
    { code: 1, name: "Yêu cầu chờ xử lý" },
    { code: 2, name: "Lịch sử yêu cầu" },
  ],
  TABS_APPROVAL_REQUEST: {
    PENDING_REQUEST: { code: 1, name: "Yêu cầu chờ xử lý" },
    HISTORY: { code: 2, name: "Lịch sử yêu cầu" },
  },
  PRODUCT_BIDDING: {
    OUTSIDE: { code: 0, name: "SP không thuộc trong DM thầu" },
    INSIDE: { code: 1, name: "SP trong DM thầu" },
  },
  STATUS_SHOPPING_PLAN: {
    ACTIVE: { value: true, index: 1, name: "Kích hoạt" },
    UN_ACTIVE: { value: false, index: 0, name: "Không kích hoạt" },
  },
  TAB_STATUS_SHOPPING_PLAN: {
    ALL: { index: 0, name: "Tất cả" },
    ACTIVE: { index: 1, name: "Kích hoạt" },
    UN_ACTIVE: { index: 2, name: "Không kích hoạt" },
  },
  LIST_STATUS_SHOPPING_PLAN: [
    { value: true, index: 1, name: "Kích hoạt" },
    { value: false, index: 0, name: "Không kích hoạt" },
  ],

  listInventoryReportType: [
    {
      code: 1,
      name: "Tổng hợp theo đơn giá",
      templateCode: PRINT_TEMPLATE_CODE.SUPPLIES_MANAGEMENT.INVENTORY_REPORT.INVENTORY_BY_UNIT_PRICE,
    },
    {
      code: 2,
      name: "Tổng hợp theo sản phẩm",
      templateCode: PRINT_TEMPLATE_CODE.SUPPLIES_MANAGEMENT.INVENTORY_REPORT.INVENTORY_BY_PRODUCT,
    },
  ],

  INVENTORY_REPORT_TYPE: {
    UNIT_PRICE: {
      code: 1,
      name: "Tổng hợp theo đơn giá",
      templateCode: PRINT_TEMPLATE_CODE.SUPPLIES_MANAGEMENT.INVENTORY_REPORT.INVENTORY_BY_UNIT_PRICE,
    },
    PRODUCT: {
      code: 2,
      name: "Tổng hợp theo sản phẩm",
      templateCode: PRINT_TEMPLATE_CODE.SUPPLIES_MANAGEMENT.INVENTORY_REPORT.INVENTORY_BY_PRODUCT,
    },
  },
  INVENTORY_CARD_TYPE: {
    NK: { code: 1, name: "Nhập kho" },
    XK: { code: 2, name: "Xuất kho" },
  },
  listInventoryType: [
    {
      code: 1,
      name: "Tồn kho theo sản phẩm",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.BY_PRODUCT,
    },
    {
      code: 2,
      name: "Tồn kho theo mã, đơn giá",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.BY_UNIT_PRICE,
    },
    {
      code: 3,
      name: "Tồn kho theo chi tiết",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.IN_DETAIL,
    },
  ],
  INVENTORY_TYPE: {
    PRODUCT: {
      code: 1,
      name: "Tồn kho theo sản phẩm",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.BY_PRODUCT,
    },
    UNIT_PRICE: {
      code: 2,
      name: "Tồn kho theo mã, đơn giá",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.BY_UNIT_PRICE,
    },
    DETAIL: {
      code: 3,
      name: "Tồn kho theo chi tiết",
      templateCode: PRINT_TEMPLATE_CODE.WAREHOUSE_MANAGEMENT.INVENTORY.IN_DETAIL,
    },
  },
};

export const variable = {
  listInputName: {
    all: "all",
    accountantOriginalCost: "accountantOriginalCost",
    accountantCarryingAmount: "accountantCarryingAmount",
    accountantQuantity: "accountantQuantity",
    allocationFor: "allocationFor",
    assetSourcePercentage: "percentage",
    createDateBottom: "createDateBottom",
    createDateTop: "createDateTop",
    yearPutIntoUse: "yearPutIntoUse",
    numberOfClones: "numberOfClones",
    numberOfAllocations: "numberOfAllocations",
    warrantyMonth: "warrantyMonth",
    isBuyLocally: "isBuyLocally",
    isMedicalEquipment: "isMedicalEquipment",
    isManageAccountant: "isManageAccountant",
    isCreateQRCode: "isCreateQRCode",
    inventoryOriginalCost: "inventoryOriginalCost",
    inventoryCarryingAmount: "inventoryCarryingAmount",
    inventoryQuantity: "inventoryQuantity",
    isTemporary: "isTemporary",
    formAllocation: "formAllocation", //name form allocation
    formName: "formName", //name form allocation
    note: "note",
    originalCost: "originalCost",
    quantity: "quantity",
    handoverDepartment: "handoverDepartment",
    receiverPerson: "receiverPerson",
    receiverDepartment: "receiverDepartment",
    switch: "switch",
    dateOfReception: "dateOfReception",
    New: "New",
    financialCode: "financialCode",
    repairForm: "repairForm",
    arHinhThuc: "arHinhThuc",
    isProposal: "phieuDeXuat",
    status: "status",
    assetReevaluate: "assetReevaluate",
    donViBaoHanh: "donViBaoHanh",
    implementingUnit: "donViThucHien",
    total: "thanhTien",
    sample: "chonMau",
    work: "congViec",
    unitPrice: "unitPrice",
    chuKyKiemDinh: "chuKyKiemDinh",
    contract: "contract",
    donViKiemDinh: "donViKiemDinh",
    donViHieuChuan: "donViHieuChuan",
    donViKiemXa: "donViKiemXa",
    assetGroup: "assetGroup",
    isMultiple: "isMultiple",
    addAssets: "addAssets",
    duplicateSeri: "serial number duplicates",
    warehouseTransfer: "warehouseAssetTransfer",
    arAccessary: "arAccessary",
    arType: "arType",
    product: "product",
    unit: "unit",
    arGhichu: "arGhichu",
    dxFrom: "dxFrom",
    dxTo: "dxTo",
    chiPhiThucHien: "chiPhiThucHien",
    managementDepartment: "managementDepartment",
    arPhuTungs: "arPhuTungs",
    child: "child",
    department: "department",
    listManageDepartment: "listManageDepartment",
    originalCostFrom: "originalCostFrom",
    originalCostTo: "originalCostTo",
    listData: "listData",
    ckDate: "ckDate",
    ckDonGia: "ckDonGia",
    depreciationRate: "depreciationRate",
    useDepartment: "useDepartment",
    supplyUnit: "supplyUnit",
    listCommonObject: "listCommonObject",
    listSupplier: "listSupplier",
    store: "store",
    isBhytMaMay: "isBhytMaMay",
    isAccreditation: "isAccreditation",
    isVerification: "isVerification",
    actionsButtonGroup: "actions-button-group",
    usePerson: "usePerson",
    isExpiredAssetHasVTNotRepaired: "isExpiredAssetHasVTNotRepaired",
    isFullyDepreciatedAsset: "isFullyDepreciatedAsset",
    asset: "asset",
    supply: "supply",
    code: "code",
    viewIndex: "viewIndex",
    acceptanceDate: "acceptanceDate",
    active: "active",
    arScope: "arScope",
    keyword: "keyword",
    name: "name",
    managementCode: "managementCode",
    serialNumber: "serialNumber",
    firstLevelApprovingDepartment: "firstLevelApprovingDepartment",
    firstLevelSubmittingDepartment: "firstLevelSubmittingDepartment",
    firstLevelSubmitter: "firstLevelSubmitter",
    firstLevelApprover: "firstLevelApprover",
    secondLevelApprovingDepartment: "secondLevelApprovingDepartment",
    secondLevelApprover: "secondLevelApprover",
    valueComplex: "valueComplex",
    valueBoolean: "valueBoolean",
    valueDatetime: "valueDatetime",
    valueNumeric: "valueNumeric",
    valueText: "valueText",
    previewTemplate: "previewTemplate",
    previewDetail: "previewDetail",
    fixedAssets: "fixedAssets",
    iats: "iats",
    supplies: "supplies",
    approvedQuantity: "approvedQuantity",
    dxRequestQuantity: "dxRequestQuantity",
    listFixedAsset: "listFixedAsset",
    listIat: "listIat",
    listSupplies: "listSupplies",
    times: "times",
    keySearch: "keySearch",
    quantityOfVoucher: "quantityOfVoucher",
    expectedFixedAssets: "expectedFixedAssets",
    expectedIats: "expectedIats",
    expectedSupplies: "expectedSupplies",
    billDate: "billDate",
    billNumber: "billNumber",
    fromDate: "fromDate",
    toDate: "toDate",
    headOfDepartment: "headOfDepartment",
    deputyDepartment: "deputyDepartment",
    secondarySignatory: "secondarySignatory",
    director: "director",
    deputyDirector: "deputyDirector",
    chiefAccountant: "chiefAccountant",
    storekeeper: "storekeeper",
    handoverPerson: "handoverPerson",
    createdPerson: "createdPerson",
    handoverRepresentative: "handoverRepresentative",
  },
  listFunctionConstants: {
    errorMessage: "errorMessage",
    message: "message",
    apiSubErrors: "apiSubErrors",
  },
  axiosMethod: {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
  },
  componentName: {
    APPROVAL_REQUEST: "ApprovalRequest",
  },
  listRowColors: {
    error: "rgba(255, 0, 0, .2)",
    subWarning: "rgba(255, 455, 95, .29)",
    Warning: "rgba(255, 146, 39, .39)",
  },
  cssTheme: {
    [appConst.OBJECT_THEME.THEME_BLUE.code]: {
      primary: {
        main: "#1A5E83",
        dark: "#12445f",
        light: "#4296C4",
        lightActive: "#E8ECFC",
        hover: "rgba(232,236,252,0.4)",
        lightHover: "rgba(66,150,196,0.1)",
      },
      secondary: {
        main: "#17A2B8",
        light: "#8ad2d3",
      },
      background: {
        default: "#eff5f9",
        header: "#E9ECEF",
        border: "#DEE2E6",
      },
      default: {
        main: "rgba(0, 0, 0, 0.54)",
        light: "#DEE2E6",
      },
      palette: {
        darkblue: "#043450",
        pink: "#f8afbe",
        yellow: "#ffd66d",
        goldenrod: "#c7a754",
        red: "#cc0033",
      },
      signal: {
        success: "#43b54d",
        info: "#29aae3",
        error: "#e52d00",
        warning: "#ff9e43",
      },
    },
    [appConst.OBJECT_THEME.THEME_GREEN.code]: {
      primary: {
        main: "#01723A",
        dark: "#02532b",
        light: "#4296C4",
        active: "#4ba468",
        hover: "rgba(208,231,216,0.8)",
        lightHover: "#e8eaf6",
      },
      secondary: {
        main: "#66A59A",
        light: "#9cd1c8",
      },
      background: {
        default: "#eff5f9",
        header: "#E9ECEF",
        border: "#DEE2E6",
      },
      default: {
        main: "rgba(0, 0, 0, 0.54)",
        light: "#DEE2E6",
      },
      palette: {
        green: "#98ce6a",
        yellow: "#ffeead",
        orange: "#EF8D19",
        teal: "#3dac9b",
        red: "#db503a",
      },
      signal: {
        success: "#98ce6a",
        info: "#3dac9b",
        error: "#db503a",
        warning: "#ef8748",
      },
    },
    [appConst.OBJECT_THEME.THEME_199.code]: {
      primary: {
        main: "#10948E",
        dark: "#0a8883",
        light: "#10948E",
        lightActive: "#E8ECFC",
        hover: "rgba(232,236,252,0.4)",
        lightHover: "rgba(66,150,196,0.1)",
      },
      secondary: {
        main: "#0a8883",
        light: "#8ad2d3",
      },
      background: {
        default: "#eff5f9",
        header: "#E9ECEF",
        border: "#DEE2E6",
      },
      default: {
        main: "rgba(0, 0, 0, 0.54)",
        light: "#DEE2E6",
      },
      palette: {
        darkblue: "#043450",
        pink: "#f8afbe",
        yellow: "#ffd66d",
        goldenrod: "#c7a754",
        red: "#cc0033",
      },
      signal: {
        success: "#88D66C",
        info: "#05BFDB",
        error: "#D83F31",
        warning: "#E9C46A",
      },
    },
  },
  css: {
    primary: {
      main: "#1A5E83",
      dark: "#12445f",
      light: "#4296C4",
      lightActive: "#E8ECFC",
      hover: "rgba(232,236,252,0.4)",
      lightHover: "rgba(66,150,196,0.1)",
    },
    secondary: {
      main: "#17A2B8",
      light: "#8ad2d3",
    },
    background: {
      default: "#eff5f9",
      header: "#E9ECEF",
      border: "#DEE2E6",
    },
    default: {
      main: "rgba(0, 0, 0, 0.54)",
      light: "#DEE2E6",
    },
    palette: {
      darkblue: "#043450",
      pink: "#f8afbe",
      yellow: "#ffd66d",
      goldenrod: "#c7a754",
      red: "#cc0033",
    },
  },
  formPrint: {
    layout: {
      A3: "A3",
      A4: "A4",
      A5: "A5",
      landscape: "landscape",
      portrait: "portrait",
      letter: "letter",
    },
    margin: {
      vertical: "vertical",
      horizontal: "horizontal",
    },
  },
  regex: {
    numberExceptThisSymbols: ["e", "E", "+", "-", "."],
    decimalNumberExceptThisSymbols: ["e", "E", "+", "-"],
    specialCharactersKey: ",./<>?;':\"}{][\\~!@#$%^&*()_+|-=",
    nameSpecialCharactersExcept: ",/<>?;':\"}{][\\~!@#$%^&()_|=`",
    phoneValid: "^([0]{1}[0-9]{9,10})?$",
    addressValid:
      "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯàáâãèéêìíòóôõùúăđĩũơưẠ-ỹ0-9\\s,./\\-_:]*$",
    emailValid: "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$",
    urlWebsiteValid: "^https?|http?:\\/\\/.*\\.com.*",
    codeValid: "[a-zA-Z0-9._\\-\\/]+$",
    regexNoDoubleDots: "^(?!.*\\.\\.).*$",
    numberQuantityFloatValid: "^\\d+(\\.\\d{1,2})?$",
    onlyNumberAndDot: "^\\d+(\\.\\d+)*$",
    onlyTwoCharacterFloat: "^\\d+(\\.\\d{1,2})?$",
  },
};

export const STATUS_REPORT = {
  WAITING: 1,
  PROCESSING: 2,
  PROCESSED: 3,
};

export const listStatusDisplayIconDelete = [
  STATUS_REPORT.PROCESSING,
  STATUS_REPORT.PROCESSED,
];
export const formatDate = "YYYY-MM-DDTHH:mm:ss";
export const typeWarehouseVoucher = {
  Receiving: 1,
  Delivery: 2,
  Transfer: 3,
};

export const OPTIONS_PRINT_ASSET = [
  {
    name: "Toàn bộ",
    code: 0,
  },
  {
    name: "Theo danh sách",
    code: 1,
  },
];

export const OBJECT_PRINT_ASSET = {
  TOAN_BO: {
    name: "Toàn bộ",
    code: 0,
  },
  THEO_DANH_SACH: {
    name: "Theo danh sách",
    code: 1,
  },
};

export const OPTIONS_EXCEL_ASSET = [
  {
    name: "Xuất Excel",
    code: 0,
  },
  {
    name: "Xuất Excel QR",
    code: 1,
  },
];
export const OPTIONS_EXCEL_QR = [
  {
    name: "Khổ 1 (70x40mm x 1tem)",
    code: 0,
  },
  {
    name: "Khổ 2 (60x40mm x 1tem)",
    code: 1,
  },
  {
    name: "Khổ 3 (50x30mm x 1tem)",
    code: 2,
  },
  {
    name: "Khổ 4 (35x22mm x 3tem)",
    code: 3,
  },
];
export const OPTIONS_EXCEL_QR_OBJECT = {
  _70x40: {
    name: "Khổ 1 (70x40mm x 1tem)",
    code: 0,
  },
  _60x40: {
    name: "Khổ 2 (60x40mm x 1tem)",
    code: 1,
  },
  _50x30: {
    name: "Khổ 3 (50x30mm x 1tem)",
    code: 2,
  },
  _35x22: {
    name: "Khổ 4 (35x22mm x 3tem)",
    code: 3,
  },
};
export const OPTIONS_EXCEL_QR_CARD = [
  {
    name: "Toàn bộ",
    code: 0,
  },
  {
    name: "Không chứa QR",
    code: 1,
  },
  {
    name: "Thẻ QR",
    code: 2,
  },
];

export const OPTIONS_EXCEL_QR_CARD_OBJECT = {
  TOAN_BO: {
    name: "Toàn bộ",
    code: 0,
  },
  KHONG_QR: {
    name: "Không chứa QR",
    code: 1,
  },
  THE_QR: {
    name: "Thẻ QR",
    code: 2,
  },
};

export const subVoucherType = [
  { code: -2, name: "Thanh lý" },
  { code: -1, name: "Xuất kho vật tư" },
  { code: 1, name: "Nhập kho vật tư" },
  { code: 2, name: "Cấp phát" },
  { code: 3, name: "Điều chuyển" },
  { code: 4, name: "Tiếp nhận" },
  { code: 5, name: "Điều chuyển sang đơn vị khác" },
  { code: 6, name: "Thu hồi" },
];

export const keySearch = {
  supply: "supply",
  asset: "asset",
  product: "product",
  keySearchDonViBaoHanh: "keySearchDonViBaoHanh",
};

export const STATUS_THANH_LY_TSCD = [
  appConst.listStatusAssets.LUU_MOI.indexOrder, //0,1,2,4,7,8
  appConst.listStatusAssets.NHAP_KHO.indexOrder,
  appConst.listStatusAssets.DANG_SU_DUNG.indexOrder,
  appConst.listStatusAssets.DA_HONG.indexOrder,
  appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder,
  appConst.listStatusAssets.KHAC.indexOrder,
];

export const STATUS_CHUYEN_DI_TSCD = [
  appConst.listStatusAssets.LUU_MOI.indexOrder, //0,1,2,4,7,8
  appConst.listStatusAssets.NHAP_KHO.indexOrder,
  appConst.listStatusAssets.DANG_SU_DUNG.indexOrder,
  appConst.listStatusAssets.DA_HONG.indexOrder,
  appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder,
  appConst.listStatusAssets.KHAC.indexOrder,
];

export const STATUS_CHUYEN_DI_CCDC = [
  appConst.listStatusInstrumentTools.LUU_MOI.indexOrder, //0,1,2,4,7,8
  appConst.listStatusInstrumentTools.NHAP_KHO.indexOrder,
  appConst.listStatusInstrumentTools.DANG_SU_DUNG.indexOrder,
  appConst.listStatusInstrumentTools.DA_HONG.indexOrder,
  appConst.listStatusInstrumentTools.DE_XUAT_THANH_LY.indexOrder,
  appConst.listStatusInstrumentTools.KHAC.indexOrder,
];

export const STATUS_THANH_LY_CCDC = [
  appConst.listStatusInstrumentTools.LUU_MOI.indexOrder, //0,1,2,4,7,8
  appConst.listStatusInstrumentTools.NHAP_KHO.indexOrder,
  appConst.listStatusInstrumentTools.DANG_SU_DUNG.indexOrder,
  appConst.listStatusInstrumentTools.DA_HONG.indexOrder,
  appConst.listStatusInstrumentTools.DE_XUAT_THANH_LY.indexOrder,
  appConst.listStatusInstrumentTools.KHAC.indexOrder,
];

export const STATUS_ASSET_FOR_ALLOCATION = [
  appConst.listStatusInstrumentTools.LUU_MOI.indexOrder, //0
  appConst.listStatusInstrumentTools.NHAP_KHO.indexOrder, //1
];

export const STATUS_RECALLSHIP = [
  appConst.listStatusAssets.DANG_SU_DUNG.indexOrder, // 2,4,7,8
  appConst.listStatusAssets.DA_HONG.indexOrder,
  appConst.listStatusAssets.KHAC.indexOrder,
  appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder,
];

export const BO_PHAN_CAN_SUA = {
  SUA_TAI_SAN: {
    code: 1,
    name: "Máy chính",
    text: "MaintainResquest.RateIncident.assetRepair",
  },
  SUA_PHU_TUNG: {
    code: 2,
    name: "Phụ tùng của TS",
    text: "MaintainResquest.RateIncident.accesoryRepair",
  },
  SUA_CA_HAI: {
    code: 3,
    name: "Cả hai",
    text: "MaintainResquest.RateIncident.allRepair",
  },
};

export const STATUS_DEPARTMENT = {
  HOAT_DONG: { code: 1, name: "Hoạt động" },
  KHONG_HOAT_DONG: { code: 0, name: "Không hoạt động" },
};
export const STATUS_SUPPLIER = {
  HOAT_DONG: { code: 1, name: "Hoạt động" },
  KHONG_HOAT_DONG: { code: 0, name: "Không hoạt động" },
};
export const STATUS_STORE = {
  HOAT_DONG: { code: 1, name: "Hoạt động" },
  KHONG_HOAT_DONG: { code: 0, name: "Không hoạt động" },
};
export const STATUS_DON_VI_THUC_HIEN = {
  HOAT_DONG: { code: 1, name: "Hoạt động" },
  KHONG_HOAT_DONG: { code: 0, name: "Không hoạt động" },
};
export const TYPE_CODE_SUPPLIER = {
  NCC: { name: "ncc", code: "ncc" },
};
export const TYPE_STORE = {
  NHAP_KHO: { code: 1, name: "Nhập kho" },
  XUAT_KHO: { code: 2, name: "Xuất kho" },
  CHUYEN_KHO: { code: 3, name: "Chuyển kho" },
};
export const LIST_PURCHASE_REQUEST = [
  { code: "DANGXULY", indexOrder: 1, name: "Đang xử lý" },
  { code: "DADUYET", indexOrder: 2, name: "Đã duyệt" },
  { code: "DAKETTHUC", indexOrder: 3, name: "Đã kết thúc" },
];
export const MAINTAIN_PLANING_STATUS = {
  MOI_TAO: { code: "MOITAO", indexOrder: 1, name: "Mới tạo" },
  DA_DUYET: { code: "DADUYET", indexOrder: 2, name: "Đã duyệt" },
  DA_BAO_GIA: { code: "DABAOGIA", indexOrder: 3, name: "Đã chốt báo giá" },
  DA_KET_THUC: { code: "DAKETTHUC", indexOrder: 4, name: "Đã kết thúc" },
};
export const listMaintainPlaningStatus = [
  {
    code: MAINTAIN_PLANING_STATUS.MOI_TAO.code,
    indexOrder: MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder,
    name: MAINTAIN_PLANING_STATUS.MOI_TAO.name,
  },
  {
    code: MAINTAIN_PLANING_STATUS.DA_DUYET.code,
    indexOrder: MAINTAIN_PLANING_STATUS.DA_DUYET.indexOrder,
    name: MAINTAIN_PLANING_STATUS.DA_DUYET.name,
  },
  {
    code: MAINTAIN_PLANING_STATUS.DA_BAO_GIA.code,
    indexOrder: MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder,
    name: MAINTAIN_PLANING_STATUS.DA_BAO_GIA.name,
  },
  {
    code: MAINTAIN_PLANING_STATUS.DA_KET_THUC.code,
    indexOrder: MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder,
    name: MAINTAIN_PLANING_STATUS.DA_KET_THUC.name,
  },
];

export const LISTLIQUIDATE = {
  NEW: { code: "NEW", indexOrder: 0, name: "Kế hoạch" },
  PROCESSING: { code: "PROCESSING", indexOrder: 1, name: "Đang thanh lý" },
  LIQUIDATED: { code: "LIQUIDATED", indexOrder: 2, name: "Đã thanh lý" },
};

export const STATUS_CHU_KY_CHO_XU_LY_DEFAULT = [
  //1
  appConst.STATUS_CHU_KY.CHO_XU_LY.code,
];

export const STATUS_CHU_KY_DANG_XU_LY_DEFAULT = [
  //2,3,4,5 Dùng cho cả đã xử lý
  appConst.STATUS_CHU_KY.DA_XU_LY.code,
  appConst.STATUS_CHU_KY.VUA_SUA_KHONG_BT.code,
  appConst.STATUS_CHU_KY.HONG_KHONG_BT.code,
  appConst.STATUS_CHU_KY.DA_XU_LY_DXTT_LINH_KIEN.code,
];
export const PURCHASE_PLANING_TYPE_OBJECT = {
  VAT_TU: { code: 2, name: "Kế hoạch mua sắm vật tư" },
  TAI_SAN: { code: 1, name: "Kế hoạch mua sắm TSCD/CCDC" },
};

export const ROUTES_PATH = {
  URL_PREFIX: "/org",
  LOGIN_PAGE: "session/signin",
  HOME_PAGE: "dashboard/analytics",
  FIXED_ASSET: "/fixed-assets/page",
  ASSET_REPAIR: "/asset/maintain_request",
  MAINTENANCE: "/asset/maintain_planing",
  ASSET_RECEPTION: "asset/reception",
  IAT_MANAGEMENT: {
    RECEPTION: "instruments-and-tools/reception",
    REPAIR: "instruments-and-tools/repair",
  },
  FIXED_ASSET_MANAGEMENT: {
    REPAIR: {
      ROUTE: "/asset/maintain_request",
      AR_SU_CO: "/api/ar-suco",
    },
    DEPRECIATION: "fixed-assets/depreciation"
  },
  COMPREHENSIVE_MANAGEMENT: {
    RECEPTION: "comprehensive/reception",
    RECEIPT: "comprehensive/receipt",
  },
  APPROVAL_REQUEST: "approval-request",
  SYSTEM_MANAGEMENT: {
    ORG_INFO: "list/infor_organization",
  },
};

export const TYPE_OF = Object.freeze({
  OBJECT: "object",
  ARRAY: "array",
  STRING: "string",
});

export const KILOBYTE = 1024;
export const MEGABYTE = KILOBYTE * 1024;

export const LIST_IMAGE_TYPE = [
  "image/gif",
  "image/jpeg",
  "image/png",
  "image/svg+xml",
  "image/webp",
  "image/bmp",
];

export const LIST_ORGANIZATION = {
  ASVN: { code: 1, name: "Bệnh viện demo oct" },
  STAGING: { code: 2, name: "Bệnh viện demo oct" },
  PRODUCT_BV199: { code: 3, name: "Bệnh viện 199" },
  BV199: { code: 4, name: "Bệnh viện 199" },
  BV_VAN_DINH: { code: 5, name: "Bệnh viện đa khoa Vân Đình" },
  BV_HOE_NHAI: { code: 6, name: "Bệnh viện Hòe Nhai" },
  BV_CONG_THAI_NGUYEN: { code: 7, name: "Bệnh viện công Thái Nguyên" },
  TTYTTP_MONG_CAI: { code: 8, name: "Trung tâm y tế thành phố móng cái" },
  SYT_TINH_QN: { code: 9, name: "Trung tâm y tế thành phố móng cái" },
  PRODUCT_VTL: { code: 10, name: "Bệnh viện demo vtl" },
  BVDK_BA_VI: { code: 11, name: "Bệnh viện đa khoa Ba Vì" },
  BVDK_SOC_SON: { code: 12, name: "Bệnh viện đa khoa Sóc Sơn" },
  BVDK_MY_DUC: { code: 13, name: "Bệnh viện đa khoa Mỹ Đức" },
};

export const DEFAULT_TOOLTIPS_PROPS = {
  placement: "right-start",
  enterDelay: 300,
  leaveDelay: 200,
  popperOptions: {
    modifiers: {
      offset: { enabled: true, offset: "50px, 20px" },
    },
  },
};

export const TRACKER_CODE = {
  REPAIR: "repair",
  APPROVAL_PROCESS: "approval_process",
  TWO_LEVEL_APPROVAL_PROCESS: "two_level_approval_process",
};

export const DATA_TYPE = {
  NUMBER: "numeric",
  TEXT: "text",
  BOOLEAN: "boolean",
  COMPLEX: "complex",
  DATETIME: "datetime",
};

export const EXECUTE_ATTRIBUTE = {
  READ: "r",
  WRITE: "w",
  EXECUTE: "x",
};

export const APPROVAL_LEVEL = {
  FIRST_LEVEL: 1,
};

export const TRACKER_CODE_RELATE = {
  REPAIR: "repair",
};

export const MODEL_CODE = {
  TWO_LEVEL_APPROVAL_FORM: "TwoLevelApprovalForm",
};

export const DEFAULT_PRINT_EXCEL = {
  excelProps: {
    show: true,
  },
};

export const PRINT_TEMPLATE_MODEL = {
  COMPREHENSIVE_MANAGEMENT: {
    RECEIVING: "receiving",
  },
  PURCHASE_MANAGEMENT: {
    REQUEST: "purchase_request",
  },
  FIXED_ASSET_MANAGEMENT: {
    LIST: {
      ASSET_BOOK_OF_USE_DEPARTMENT: "fixed_asset_book_of_use_department",
      ASSET_PROFILE: "asset_profile",
    },
    REPAIR: {
      INCIDENT: "incident",
    },
    ALLOCATION: "allocation_voucher_fixed_asset",
    TRANSFER: "transfer_voucher_fixed_asset",
    MAINTAIN_PLANING: {
      PLAN: "maintenance_plan",
      ACCEPTANCE: {
        FOR_SUPPLIER: "maintenance_acceptance",
        FOR_DEPARTMENT: "maintenance_handover",
      },
    },
    INVENTORY_COUNT_VOUCHER: "inventory_count_voucher_fixed_asset",
  },
  IAT_MANAGEMENT: {
    REPAIR: {
      INCIDENT: "incident",
    },
    ALLOCATION: "allocation_voucher_iat",
    TRANSFER: "transfer_voucher_iat",
    INVENTORY_COUNT_VOUCHER: "inventory_count_voucher_iat",
  },
  SUPPLIES_MANAGEMENT: {
    RECEIVING: "supplies_receiving",
    DELIVERY: "supplies_delivery",
    INVENTORY_REPORT: "supplies_inventory_report",
    SUPPLIES_INVENTORY: "supplies_inventory",
    INVENTORY: "inventory",
  },
  WAREHOUSE_MANAGEMENT: {
    RECEIVING: "warehouse_receiving",
    DELIVERY: "warehouse_delivery",
  },
};

export const PRINT_DOCUMENT_TYPE = {
  WORD: "word",
  PDF: "pdf",
};

export const ICON_OBJECT = {
  edit: {
    text: "Sửa bản ghi",
    icon: "edit",
    color: "primary",
    iconAction: appConst.active.edit,
  },
  view: {
    text: "Xem",
    icon: "visibility",
    color: "primary",
    iconAction: appConst.active.view,
  },
  delete: {
    text: "Xóa bản ghi",
    icon: "delete",
    color: "error",
    iconAction: appConst.active.delete,
  },
};

export const DEFAULT_ICON = {
  title: "/assets/images/logos/utc.png",
  loading: "/assets/images/logos/logo-x-main.png",
};

export const ICON_BY_THEME = {
  [appConst.OBJECT_THEME.THEME_BLUE.code]: DEFAULT_ICON,
  [appConst.OBJECT_THEME.THEME_GREEN.code]: {
    title: "/assets/images/logos/utc.png",
    loading: "/assets/images/logos/logo-i.png",
  },
};

export const DEFAULT_MAX_STRING_LENGTH = 255;
