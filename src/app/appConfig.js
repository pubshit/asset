const {LIST_ORGANIZATION, appConst, ROUTES_PATH} = require("./appConst");
const APPLICATION_PATH = "/";
const VOUCHER_TYPE = {
  Liquidate: -2, // Thanh lý
  StockOut: -1, //Xuất kho vật tư
  StockIn: 1, //Nhập kho vật tư
  Allocation: 2, //Cấp phát || (xuất kho VT + assetClass = 3)
  Transfer: 3, //Điều chuyển
  ReceivingAsset: 4, //Tiếp nhận tài sản
  TransferToAnotherUnit: 5, //Điều chuyển đơn vị khác
  Outbound: 2, // Xuất kho,
  Store_Transfer: 6, //Chuyển kho vật tư
};

const ERROR_CODE = {
  Unauthorized: 401,
};

const ROLES = {
  ROLE_USER: "ROLE_USER", // người dùng
  ROLE_ASSET_MANAGER: "ROLE_ASSET_MANAGER", // người quản lý vật tư
  ROLE_ASSET_USER: "ROLE_ASSET_USER", // đại diện phòng ban
  ROLE_ADMIN: "ROLE_ADMIN", // admin
  ROLE_SUPER_ADMIN: "ROLE_SUPER_ADMIN", // super admin
  ROLE_ORG_ADMIN: "ROLE_ORG_ADMIN", //
  ROLE_ACCOUNTANT: "ROLE_ACCOUNTANT", // Kế toán
  NO_PERMISSION: "NO_PERMISSION", // Không có quyền
};

const COlOR = {
  PRIMARY_FIRST: "#4B49AC",
  PRIMARY_SECOND: "#98BDFF",
  SUPPORT_FIRST: "#7DA0FA",
  SUPPORT_SECOND: "#7978E9",
  SUPPORT_THIRD: "#F3797E",
};

const LAYOUT = {
  LAYOUT_HORIZONTAL: "layout1",   // layout ngang
  LAYOUT_VERTICAL: "layout2",     // layout dọc
}

const DB = {
  LOCAL: "LOCAL",
  ASVN: "ASVN",
  PRODUCT_DEMO: "STAGING",
  PRODUCT_BV199: "PRODUCT_BV199",
  PRODUCT_BVC_THAI_NGUYEN: "PRODUCT_BVC_THAI_NGUYEN",
  PRODUCT_TTYTTP_MONG_CAI: "PRODUCT_TTYTTP_MONG_CAI",
  PRODUCT_VAN_DINH: "PRODUCT_VAN_DINH",
  PRODUCT_HOE_NHAI: "PRODUCT_HOE_NHAI",
  PRODUCT_SYT_TINH_QN: "PRODUCT_SYT_TINH_QN",
  BV199: "BV199",
  BV_VAN_DINH: "BV_VAN_DINH",
  BVDK_BA_VI: "BVDK_BA_VI",
  BVDK_SOC_SON: "BVDK_SOC_SON",
  BVDK_MY_DUC: "BVDK_MY_DUC",
}

const CURRENT_ORG = LIST_ORGANIZATION.ASVN;
const DB_BY_ORG = {
  [LIST_ORGANIZATION.ASVN.code]: DB.ASVN,
  [LIST_ORGANIZATION.PRODUCT_BV199.code]: DB.PRODUCT_BV199,
  [LIST_ORGANIZATION.PRODUCT_VTL.code]: DB.PRODUCT_DEMO,
};

const ORG_CODE_FOR_PRINT = {
  [LIST_ORGANIZATION.ASVN.code]: LIST_ORGANIZATION.BV199.code,
  [LIST_ORGANIZATION.BV199.code]: LIST_ORGANIZATION.BV199.code,
  [LIST_ORGANIZATION.PRODUCT_BV199.code]: LIST_ORGANIZATION.BV199.code,
  [LIST_ORGANIZATION.BVDK_BA_VI.code]: LIST_ORGANIZATION.BV199.code,
  [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code,
  [LIST_ORGANIZATION.BV_HOE_NHAI.code]: LIST_ORGANIZATION.BV_HOE_NHAI.code,
  [LIST_ORGANIZATION.BV_VAN_DINH.code]: LIST_ORGANIZATION.BV_VAN_DINH.code,
  [LIST_ORGANIZATION.BVDK_MY_DUC.code]: LIST_ORGANIZATION.ASVN.code,
  [LIST_ORGANIZATION.SYT_TINH_QN.code]: LIST_ORGANIZATION.ASVN.code,
  [LIST_ORGANIZATION.TTYTTP_MONG_CAI.code]: LIST_ORGANIZATION.ASVN.code,
  [LIST_ORGANIZATION.PRODUCT_VTL.code]: LIST_ORGANIZATION.BV199.code,
};

const THEME_BY_ORG = {
  [LIST_ORGANIZATION.ASVN.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.STAGING.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.BVDK_BA_VI.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.BV199.code]: appConst.OBJECT_THEME.THEME_199,
  [LIST_ORGANIZATION.PRODUCT_BV199.code]: appConst.OBJECT_THEME.THEME_199,
  [LIST_ORGANIZATION.BV_VAN_DINH.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.BVDK_MY_DUC.code]: appConst.OBJECT_THEME.THEME_BLUE,
  [LIST_ORGANIZATION.TTYTTP_MONG_CAI.code]: appConst.OBJECT_THEME.THEME_GREEN,
  [LIST_ORGANIZATION.SYT_TINH_QN.code]: appConst.OBJECT_THEME.THEME_GREEN,
  [LIST_ORGANIZATION.PRODUCT_VTL.code]: appConst.OBJECT_THEME.THEME_GREEN,
};

const LAYOUT_BY_ORG = {
  [LIST_ORGANIZATION.ASVN.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.BVDK_BA_VI.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.STAGING.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.BV199.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.PRODUCT_BV199.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.BV_VAN_DINH.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.BVDK_MY_DUC.code]: LAYOUT.LAYOUT_HORIZONTAL,
  [LIST_ORGANIZATION.TTYTTP_MONG_CAI.code]: LAYOUT.LAYOUT_VERTICAL,
  [LIST_ORGANIZATION.SYT_TINH_QN.code]: LAYOUT.LAYOUT_VERTICAL,
  [LIST_ORGANIZATION.PRODUCT_VTL.code]: LAYOUT.LAYOUT_VERTICAL,
}

const ENDPOINT = {
  [DB.LOCAL]: {
    API_ENPOINT: "https://asvn.oceantech.com.vn/asvn",
    // API_ENPOINT: "http://192.168.31.40:8093/asset",                                      //dev-local
    API_ENPOINT_ASSET_MAINTANE: "http://192.168.2.209:9090",                             //dev-local
  },
  [DB.ASVN]: {
    API_ENPOINT: "https://asvn.oceantech.com.vn/asvn",                                   //dev
    API_ENPOINT_ASSET_MAINTANE: "https://asset-management-dev.oceantech.com.vn",         //dev
  },
  [DB.PRODUCT_BV199]: {
    API_ENPOINT: "http://bv199.oceantech.com.vn/asvn",                                   //product-demo-bv199
    API_ENPOINT_ASSET_MAINTANE: "http://asset-management-bv199.oceantech.com.vn",        //product-demo-bv199
  },
  [DB.BV199]: {
    API_ENPOINT: "https://api-qlts.benhvien199.vn/asvn",                                 //bv199
    API_ENPOINT_ASSET_MAINTANE: "https://api-qlts.benhvien199.vn/asvnam",                //bv199
  },
  [DB.PRODUCT_VAN_DINH]: {
    API_ENPOINT: "http://qltsdemo.benhviendakhoavandinh.vn/x-amis",                      //product-demo-bvdk-van-dinh
    API_ENPOINT_ASSET_MAINTANE: "http://qltsdemo.benhviendakhoavandinh.vn/x-amis-m",     //product-demo-bvdk-van-dinh
  },
  [DB.BV_VAN_DINH]: {
    API_ENPOINT: "http://192.168.0.37:9002/x-amis",                                      //BV-Van-Dinh
    API_ENPOINT_ASSET_MAINTANE: "http://192.168.0.37:9003/x-amis-m",                     //BV-Van-Dinh
  },
  [DB.PRODUCT_BVC_THAI_NGUYEN]: {
    API_ENPOINT: "http://qlts-bvcthainguyen.xhis.vn/x-amis",                             //bvc-thai-nguyen
    API_ENPOINT_ASSET_MAINTANE: "http://qlts-bvcthainguyen.xhis.vn/x-amis-m",            //bvc-thai-nguyen
  },
  [DB.PRODUCT_HOE_NHAI]: {
    API_ENPOINT: "https://asvnstaging.oceantech.com.vn/asvn",                            //product-demo
    API_ENPOINT_ASSET_MAINTANE: "https://asvnamstaging.oceantech.com.vn",                //product-demo
  },
  [DB.PRODUCT_SYT_TINH_QN]: {
    API_ENPOINT: "http://qlts.soyte.tinhquangninh.amisvn.com/x-amis",                    //syt-tinh-quang-ninh
    API_ENPOINT_ASSET_MAINTANE: "http://qlts.soyte.tinhquangninh.amisvn.com/x-amis-m",   //syt-tinh-quang-ninh
  },
  [DB.PRODUCT_TTYTTP_MONG_CAI]: {
    API_ENPOINT: "http://qlts-ttyttpmongcai.amisvn.com/x-amis",                          //ttyttp-mong-cai
    API_ENPOINT_ASSET_MAINTANE: "http://qlts-ttyttpmongcai.amisvn.com/x-amis-m",         //ttyttp-mong-cai
  },
  [DB.PRODUCT_DEMO]: {
    API_ENPOINT: "https://asvnstaging.oceantech.com.vn/asvn",                            //product-demo
    API_ENPOINT_ASSET_MAINTANE: "https://asvnamstaging.oceantech.com.vn",                //product-demo
  },
  [DB.BVDK_BA_VI]: {
    API_ENPOINT: "http://api.bvdk-bavi.xhis.vn/x-amis",                                  //product-bvdk-bavi
    API_ENPOINT_ASSET_MAINTANE: "http://api.bvdk-bavi.xhis.vn/x-amis-m",                 //product-bvdk-bavi
  },
  [DB.BVDK_SOC_SON]: {
    API_ENPOINT: "http://172.16.0.12:9002/x-amis",                                //product-bvdk-socson
    API_ENPOINT_ASSET_MAINTANE: "http://172.16.0.12:9003/x-amis-m",               //product-bvdk-socson
  },
  [DB.BVDK_MY_DUC]: {
    API_ENPOINT: "http://api.bvdk-myduc.xhis.vn/x-amis",                                //product-bvdk-myduc
    API_ENPOINT_ASSET_MAINTANE: "http://api.bvdk-myduc.xhis.vn/x-amis-m",               //product-bvdk-myduc
  },
};

const LANGUAGE_FILE_NAMES = {
	[appConst.OBJECT_THEME.THEME_BLUE.code]: ["translation"],
	[appConst.OBJECT_THEME.THEME_GREEN.code]: ["translation-vtl"],
}

const CURRENT_ENDPOINT = ENDPOINT[DB_BY_ORG[CURRENT_ORG.code || LIST_ORGANIZATION.ASVN.code] || DB.ASVN];
const ACTIVE_LAYOUT = LAYOUT_BY_ORG[CURRENT_ORG.code] || LAYOUT.LAYOUT_HORIZONTAL;
const DEFAULT_FILE_NAME = LANGUAGE_FILE_NAMES[appConst.OBJECT_THEME.THEME_BLUE.code];
const LANG_FILE_NAME = LANGUAGE_FILE_NAMES[THEME_BY_ORG[CURRENT_ORG.code]?.code];

module.exports = Object.freeze({
  ROOT_PATH: APPLICATION_PATH,
  ACTIVE_LAYOUT: ACTIVE_LAYOUT, //layout1 = vertical, layout2=horizontal
  URL_PREFIX: ROUTES_PATH.URL_PREFIX, // org
  API_ENPOINT: CURRENT_ENDPOINT.API_ENPOINT,
  API_ENPOINT_ASSET_MAINTANE: CURRENT_ENDPOINT.API_ENPOINT_ASSET_MAINTANE,
  LOGIN_PAGE: APPLICATION_PATH + ROUTES_PATH.LOGIN_PAGE, //Nếu là Spring
  HOME_PAGE: APPLICATION_PATH + ROUTES_PATH.HOME_PAGE, //Nếu là Spring
  VOUCHER_TYPE,
  ROLES,
  COLOR: COlOR,
  ERROR_CODE,
  CURRENT_ORG,
  ORG_CODE_FOR_PRINT: ORG_CODE_FOR_PRINT,
  THEME_BY_ORG: THEME_BY_ORG[CURRENT_ORG.code],
	LANG_FILE_NAME: LANG_FILE_NAME || DEFAULT_FILE_NAME, // translation file name
});
