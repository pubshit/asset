import "../fake-db";
import "../styles/_app.scss";
import React, {useEffect, useState} from "react";
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import EgretTheme from "./EgretLayout/EgretTheme/EgretTheme";
import AppContext from "./appContext";
import history from "history.js";
import routes from "./RootRoutes";
import {Store} from "./redux/Store";
import Auth from "./auth/Auth";
import EgretLayout from "./EgretLayout/EgretLayout";
import AuthGuard from "./auth/AuthGuard";
import {appConst, DEFAULT_ICON, ICON_BY_THEME, LIST_ORGANIZATION} from "./appConst";
import {connectRealtime} from "./appServices";
import ConstantList from "./appConfig";
import {useFavicon} from "./hooks";

const App = () => {
  const [pageLoading, setPageLoading] = useState(false);
  const [theme, setTheme] = useState(ConstantList.THEME_BY_ORG || appConst.OBJECT_THEME.THEME_BLUE);
  const [eventSource, setEventSource] = useState(null);
  const [eventData, setEventData] = useState([]);
	
	const currentOrg = ConstantList.CURRENT_ORG;
	const printCode = ConstantList.ORG_CODE_FOR_PRINT[currentOrg.code || LIST_ORGANIZATION.ASVN.code];
	const iconUrls = ICON_BY_THEME[theme?.code] || DEFAULT_ICON;
	
	useFavicon(iconUrls.title);

  useEffect(() => {
    const handleGetNewEvent = () => {
      let newEvent = connectRealtime({});
      newEvent?.addEventListener("message", (e) => {
        const newData = JSON.parse(e.data)
        let newArr = [...eventData];
        newArr.push(newData);
        setEventData([...newArr]);
      })
      setEventSource(newEvent);
    }

    window.addEventListener('beforeunload', (e) => {
      eventSource?.close();
    })
    window.addEventListener('load', () => {
      handleGetNewEvent();
    })

    currentOrg.printCode = printCode;

    return () => {
      eventSource?.close();
      setEventSource(null);
    }
  }, []);
  
  return (
    <AppContext.Provider value={{
      routes,
      pageLoading,
      setPageLoading,
      theme,
      setTheme,
      eventData,
      setEventData,
      currentOrg,
    }}>
      <Provider store={Store}>
        <EgretTheme>
          <Auth>
            <Router history={history}>
              <AuthGuard>
                <EgretLayout/>
              </AuthGuard>
            </Router>
          </Auth>
        </EgretTheme>
      </Provider>
    </AppContext.Provider>
  );
};

export default App;
