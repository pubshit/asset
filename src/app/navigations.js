import ConstantList from "./appConfig";


const roles = ConstantList.ROLES;

export const navigations = [
  {
    name: "Dashboard.dashboard",
    code: "001",
    description: "Trang chủ",
    path: "/dashboard/analytics",
    icon: "dashboard",
    isVisible: true,
    children: [],
    roles: [
      roles.ROLE_ASSET_MANAGER,
      roles.ROLE_ORG_ADMIN,
      roles.ROLE_ASSET_USER,
      roles.ROLE_USER
    ]
  },
  {
    name: "Dashboard.assetManagement",
    code: "005.003",
    description: "Quản lý tài sản",
    path: "\"\"",
    icon: "web_asset",
    isVisible: true,
    children: [
      {
        name: "Asset.list_asset",
        code: "008.002.001",
        description: "Danh sách tài sản",
        path: "fixed-assets/page",
        icon: "list",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_ASSET_USER,
          roles.ROLE_ADMIN,
          roles.ROLE_USER,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_MANAGER
        ]
      },
      {
        name: "Asset.allocation_asset",
        code: "008.002.003",
        description: "Cấp phát tài sản",
        path: "fixed-assets/allocation-vouchers",
        icon: "low_priority",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ASSET_USER,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Asset.transfer_asset",
        code: "008.002.004",
        description: "Điều chuyển tài sản",
        path: "fixed-assets/transfer-vouchers",
        icon: "wrap_text",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_USER,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Asset.asset_transfer_to_another_unit",
        code: "008.002.005",
        description: "Chuyển đi",
        path: "fixed-assets/transfer-to-another-unit-vouchers",
        icon: "launch",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Asset.asset_liquidate",
        code: "008.002.006",
        description: "Thanh lý tài sản",
        path: "fixed-assets/liquidate-vouchers",
        icon: " ",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Asset.inventory_count_voucher",
        code: "008.002.009",
        description: "Kiểm kê tài sản",
        path: "fixed-assets/inventory-count-vouchers",
        icon: "calculate",
        isVisible: true,
        children: [
          {
            name: "Asset.inventory_count_voucher",
            code: "008.002.009.001",
            description: "Kiểm kê tài sản",
            path: "fixed-assets/inventory-count-vouchers",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ACCOUNTANT,
              roles.ROLE_ADMIN,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "Asset.inventory_count_voucher_department",
            code: "008.002.009.002",
            description: "Kiểm kê TSCĐ toàn đơn vị",
            path: "/fixed-assets/inventory-count-vouchers-department",
            icon: null,
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ACCOUNTANT,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ORG_ADMIN
            ]
          }
        ],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.warehouseTransfer",
        code: "008.002.014",
        description: "Chuyển kho",
        path: "fixed-assets/warehouse-tranfers",
        icon: "swap_horiz",
        isVisible: true,
        children: [],
        roles: [
          // roles.ROLE_ASSET_MANAGER,
          // roles.ROLE_ADMIN,
          // roles.ROLE_SUPER_ADMIN,
          // roles.ROLE_ORG_ADMIN
        ]
      }
    ],
    roles: [
      roles.ROLE_USER,
      roles.ROLE_ASSET_USER,
      roles.ROLE_ADMIN,
      roles.ROLE_ASSET_MANAGER,
      roles.ROLE_ORG_ADMIN,
      roles.ROLE_ACCOUNTANT,
      roles.ROLE_SUPER_ADMIN
    ]
  },
  {
    name: "Quản lý",
    code: "005",
    description: "Quản lý (New)",
    path: "",
    icon: "manage",
    isVisible: false,
    children: [
      /*{
        name: "Dashboard.purchase",
        code: "005.001",
        description: "Quản lý mua sắm",
        path: "\"\"",
        icon: "local_mall",
        isVisible: true,
        children: [
          {
            name: "Dashboard.Purchase.purchase_asset",
            code: "008.001.001",
            description: "Mua sắm tài sản",
            path: "\"\"",
            icon: "shopping_cart",
            isVisible: true,
            children: [
              {
                name: "Dashboard.Purchase.purchaseRequest",
                code: "008.001.001.001",
                description: "Yêu cầu mua sắm",
                path: "/list/purchase_request",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ASSET_USER,
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_ASSET_MANAGER,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ADMIN
                ]
              },
              {
                name: "Dashboard.Purchase.purchase_request_count",
                code: "008.001.001.002",
                description: "Tổng hợp yêu cầu mua sắm",
                path: "/list/purchase_request_count",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ADMIN,
                  roles.ROLE_ASSET_MANAGER,
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_SUPER_ADMIN
                ]
              },
              {
                name: "Dashboard.Purchase.purchasePlaning",
                code: "008.001.001.003",
                description: "Kế hoạch mua sắm",
                path: "/list/purchase_planing",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ADMIN,
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ASSET_MANAGER
                ]
              }
            ],
            roles: [
              roles.ROLE_ASSET_USER,
              roles.ROLE_USER,
              roles.ROLE_ADMIN,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "Dashboard.Purchase.purchase_supplies",
            code: "008.001.002",
            description: "Mua sắm vật tư",
            path: "\"\"",
            icon: "shopping_cart",
            isVisible: true,
            children: [
              {
                name: "Dashboard.Purchase.suppliesPurchaseRequest",
                code: "008.001.002.001",
                description: "Yêu cầu mua sắm",
                path: "/list/supplies_purchase_request",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ADMIN,
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ASSET_USER,
                  roles.ROLE_ASSET_MANAGER
                ]
              },
              {
                name: "Dashboard.Purchase.supplies_purchase_request_count",
                code: "008.001.002.002",
                description: "Tổng hợp yêu cầu mua sắm",
                path: "/list/supplies_purchase_request_count",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ADMIN,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ASSET_MANAGER,
                  roles.ROLE_ORG_ADMIN
                ]
              },
              {
                name: "Dashboard.Purchase.suppliespurchasePlaning",
                code: "008.001.002.003",
                description: "Kế hoạch mua sắm",
                path: "/list/supplies_purchase_planing",
                icon: "keyboard_arrow_right",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ASSET_MANAGER,
                  roles.ROLE_ADMIN
                ]
              }
            ],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "Dashboard.Purchase.ShoppingPlan",
            code: "008.001.003.001",
            description: "Kế hoạch mua sắm",
            path: "list/purchase-shopping-plan",
            icon: null,
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_ASSET_MANAGER
            ]
          }
        ],
        roles: [
          // roles.ROLE_ASSET_MANAGER,
          // roles.ROLE_ACCOUNTANT,
          // roles.ROLE_ORG_ADMIN,
          // roles.ROLE_ASSET_USER
        ]
      },*/
      {
        name: "Dashboard.assetManagement",
        code: "005.003",
        description: "Quản lý tài sản",
        path: "\"\"",
        icon: "web_asset",
        isVisible: true,
        children: [
          {
            name: "Asset.list_asset",
            code: "008.002.001",
            description: "Danh sách tài sản",
            path: "fixed-assets/page",
            icon: "list",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ACCOUNTANT,
              roles.ROLE_ASSET_USER,
              roles.ROLE_ADMIN,
              roles.ROLE_USER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "Asset.allocation_asset",
            code: "008.002.003",
            description: "Cấp phát tài sản",
            path: "fixed-assets/allocation-vouchers",
            icon: "low_priority",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "Asset.transfer_asset",
            code: "008.002.004",
            description: "Điều chuyển tài sản",
            path: "fixed-assets/transfer-vouchers",
            icon: "wrap_text",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_SUPER_ADMIN
            ]
          },
          {
            name: "Asset.asset_transfer_to_another_unit",
            code: "008.002.005",
            description: "Chuyển đi",
            path: "fixed-assets/transfer-to-another-unit-vouchers",
            icon: "launch",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ADMIN
            ]
          },
          {
            name: "Asset.asset_liquidate",
            code: "008.002.006",
            description: "Thanh lý tài sản",
            path: "fixed-assets/liquidate-vouchers",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ADMIN
            ]
          },
          {
            name: "Asset.inventory_count_voucher",
            code: "008.002.009",
            description: "Kiểm kê tài sản",
            path: "fixed-assets/inventory-count-vouchers",
            icon: "calculate",
            isVisible: true,
            children: [
              {
                name: "Asset.inventory_count_voucher",
                code: "008.002.009.001",
                description: "Kiểm kê tài sản",
                path: "fixed-assets/inventory-count-vouchers",
                icon: " ",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ACCOUNTANT,
                  roles.ROLE_ADMIN,
                  roles.ROLE_ORG_ADMIN
                ]
              },
              {
                name: "Asset.inventory_count_voucher_department",
                code: "008.002.009.002",
                description: "Kiểm kê TSCĐ toàn đơn vị",
                path: "/fixed-assets/inventory-count-vouchers-department",
                icon: null,
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ACCOUNTANT,
                  roles.ROLE_SUPER_ADMIN,
                  roles.ROLE_ORG_ADMIN
                ]
              }
            ],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ACCOUNTANT,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "Dashboard.subcategory.warehouseTransfer",
            code: "008.002.014",
            description: "Chuyển kho",
            path: "fixed-assets/warehouse-tranfers",
            icon: "swap_horiz",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ORG_ADMIN
            ]
          }
        ],
        roles: [
          roles.ROLE_USER,
          roles.ROLE_ASSET_USER,
          roles.ROLE_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.toolsManagement",
        code: "005.004",
        description: "Quản lý công cụ dụng cụ",
        path: "\"\"",
        icon: "web_asset",
        isVisible: true,
        children: [
          {
            name: "InstrumentToolsList.instrumentToolsList",
            code: "008.003.001",
            description: "Danh sách công cụ dụng cụ",
            path: "instruments-and-tools/page",
            icon: "list",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_USER,
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_SUPER_ADMIN
            ]
          },
          {
            name: "InstrumentsToolsAllocation.InstrumentsToolsAllocation",
            code: "008.003.002",
            description: "Cấp phát công cụ dụng cụ",
            path: "instruments-and-tools/allocation-vouchers",
            icon: "low_priority",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "InstrumentToolsTransfer.instrumentToolsTransfer",
            code: "008.003.004",
            description: "Điều chuyển CCDC",
            path: "instruments-and-tools/transfer-vouchers",
            icon: "wrap_text",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "InstrumentsToolsInventory.inventoryCountVoucher",
            code: "008.003.005",
            description: "Kiểm kê CCDC",
            path: "instruments-and-tools/inventory-count-vouchers",
            icon: " ",
            isVisible: true,
            children: [
              {
                name: "InstrumentsToolsInventory.inventoryCountVoucher",
                code: "008.003.005.001",
                description: "Kiểm kê CCDC",
                path: "instruments-and-tools/inventory-count-vouchers",
                icon: "",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ADMIN,
                  roles.ROLE_ACCOUNTANT,
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_SUPER_ADMIN
                ]
              },
              {
                name: "InstrumentsToolsInventory.inventoryCountVoucherDepartment",
                code: "008.003.005.002",
                description: "Kiểm kê CCDC toàn đơn vị",
                path: "instruments-and-tools/inventory-count-vouchers-department",
                icon: "",
                isVisible: true,
                children: [],
                roles: [
                  roles.ROLE_ORG_ADMIN,
                  roles.ROLE_ACCOUNTANT,
                  roles.ROLE_ADMIN,
                  roles.ROLE_SUPER_ADMIN
                ]
              }
            ],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN
            ]
          },
          {
            name: "InstrumentToolsType.liquidate",
            code: "008.003.006",
            description: "Thanh Lý CCDC",
            path: "instruments-and-tools/liquidate-vouchers",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ADMIN,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "TransferToAnotherUnit.InstrumentsToolTransferToAnotherUnit",
            code: "008.003.007",
            description: "CCDC chuyển đi",
            path: "instruments-and-tools/transfer-to-another-unit-vouchers",
            icon: "launch",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "Dashboard.subcategory.warehouseTransfer",
            code: "008.003.008",
            description: "Chuyển kho",
            path: "instruments-and-tools/warehouse-tranfers",
            icon: "swap_horiz",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN
            ]
          }
        ],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_USER,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_ASSET_USER,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ],
      },
      {
        name: "Dashboard.materialManagement",
        code: "008.004",
        description: "Quản lý vật tư",
        path: "\"\"",
        icon: "assignment",
        isVisible: true,
        children: [
          {
            name: "Store.inventory_receiving_voucher",
            code: "008.004.001",
            description: "Nhập kho",
            path: "/list/inventory_receiving_voucher",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ADMIN,
              roles.ROLE_ASSET_USER,
              roles.ROLE_ASSET_MANAGER
            ]
          },
          {
            name: "Store.inventory_delivery_voucher",
            code: "008.004.002",
            description: "Xuất kho",
            path: "/list/inventory_delivery_voucher",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_USER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ADMIN
            ]
          },
          {
            name: "InventoryReport.inventory_report",
            code: "008.004.003",
            description: "Báo cáo tồn kho",
            path: "/report/inventory_report",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_SUPER_ADMIN,
              roles.ROLE_ORG_ADMIN
            ]
          },
          {
            name: "Dashboard.subcategory.warehouseTransfer",
            code: "008.004.004",
            description: "Chuyển kho",
            path: "material-management/warehouse-tranfers",
            icon: " ",
            isVisible: true,
            children: [],
            roles: [
              roles.ROLE_ASSET_MANAGER,
              roles.ROLE_ORG_ADMIN,
              roles.ROLE_ADMIN,
              roles.ROLE_SUPER_ADMIN
            ]
          }
        ],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_USER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN
        ],
      }
    ],
    roles: [
      // roles.ROLE_ORG_ADMIN,
      // roles.ROLE_ASSET_USER,
      // roles.ROLE_USER,
      // roles.ROLE_ACCOUNTANT,
      // roles.ROLE_ASSET_MANAGER,
      // roles.ROLE_ADMIN,
    ],
  },
  {
    name: "Dashboard.summary_report.title",
    code: "006",
    description: "Báo cáo (New)",
    path: "\"\"",
    icon: "insert_chart_outlined",
    isVisible: true,
    children: [
      {
        name: "Dashboard.summary_report.aggregate_assets_by_asset_group",
        code: "005.001",
        description: "Tổng hợp theo nhóm tài sản",
        path: "/summary_report/aggregate_assets_by_asset_group",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.summary_report.amount_of_assets_grouped_by_asset_group",
        code: "005.002",
        description: "Số lượng tài sản theo nhóm",
        path: "/summary_report/amount_of_assets_grouped_by_asset_group",
        icon: "poll",
        isVisible: false,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ASSET_MANAGER
        ]
      },
      {
        name: "Dashboard.summary_report.amount_of_assets_grouped_by_department",
        code: "005.003",
        description: "BC tài sản theo phòng ban",
        path: "/summary_report/amount_of_assets_grouped_by_department",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_USER,
          roles.ROLE_ASSET_MANAGER
        ]
      },
      {
        name: "Dashboard.summary_report.amount_of_assets_grouped_by_product_category",
        code: "005.004",
        description: "Số lượng sản phẩm theo DMSP",
        path: "/summary_report/amount_of_assets_grouped_by_product_category",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.summary_report.report_increase_or_decrease_assets",
        code: "005.005",
        description: "Báo cáo tăng giảm số lượng tài sản",
        path: "/summary_report/increase_decrease_reported_assets",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ACCOUNTANT
        ]
      },
      {
        name: "Dashboard.summary_report.report_decrease_assets",
        code: "005.006",
        description: "Báo cáo giảm tài sản",
        path: "/summary_report/decrease_reported_assets",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ADMIN,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.summary_report.report_increase_assets",
        code: "005.007",
        description: "Báo cáo tăng tài sản",
        path: "/summary_report/increase_reported_assets",
        icon: "poll",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ACCOUNTANT,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.summary_report.summaryByMedicalEquipment",
        code: "006.008",
        description: "Tổng hợp theo loại thiết bị y tế",
        path: "summary-report/summary-of-assets-by-medical-equipment",
        icon: "ballot",
        isVisible: false,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ACCOUNTANT
        ]
      },
      {
        name: "Dashboard.summary_report.statistics_of_assets_under_contract",
        code: "006.009",
        description: "Thông kê tài sản theo hợp đồng",
        path: "summary_report/statistics_of_assets_under_contract",
        icon: null,
        isVisible: false,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_SUPER_ADMIN
        ]
      }
    ],
    roles: [
      roles.ROLE_ADMIN,
      roles.ROLE_ACCOUNTANT,
      roles.ROLE_ORG_ADMIN,
      roles.ROLE_ASSET_MANAGER
    ]
    
  },
  {
    name: "Dashboard.category",
    code: "010",
    description: "Quản lý danh mục (New)",
    path: "\"\"",
    icon: "category",
    isVisible: true,
    parent: null,
    children: [
      {
        name: "Dashboard.subcategory.product",
        code: "010.001",
        description: "Danh sách sản phẩm",
        path: "/list/product",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.productType",
        code: "010.002",
        description: "Loại sản phẩm",
        path: "/list/producttype",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.productCategory",
        code: "010.003",
        description: "Danh mục sản phẩm",
        path: "/list/product_category",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.assetGroup",
        code: "010.004",
        description: "Danh mục tài sản",
        path: "/list/asset_group",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.assetSource",
        code: "010.005",
        description: "Nguồn vốn tài sản",
        path: "/list/assetsource",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.department",
        code: "010.006",
        description: "Danh sách phòng ban",
        path: "/list/department",
        icon: " keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.commonObjectType",
        code: "010.007",
        description: "Nhóm tham số dùng chung",
        path: "/list/CommonObjectType",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.commonObject",
        code: "010.008",
        description: "Tham số dùng chung",
        path: "/list/CommonObject",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.supplier",
        code: "010.009",
        description: "Nhà cung cấp",
        path: "/list/supplier",
        icon: " keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.contract",
        code: "010.010",
        description: "Hợp đồng",
        path: "/list/contract",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.stockKeepingUnit",
        code: "010.011",
        description: "Đơn vị tính",
        path: "/list/stock_keeping_unit",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.assetType",
        code: "010.012",
        description: "Loại tài sản",
        path: "/list/asset_type",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.store",
        code: "010.013",
        description: "Kho",
        path: "/list/stores",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.productAttribute",
        code: "010.014",
        description: "Thuộc tính sản phẩm",
        path: "/list/product_attribute",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.productTemplate",
        code: "010.015",
        description: "Mẫu sản phẩm",
        path: "/list/product_template",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.assetStatus",
        code: "010.017",
        description: "Trạng thái tài sản",
        path: "/list/asset_status",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.inventoryCountStatus",
        code: "010.018",
        description: "Trạng thái kiểm kê",
        path: "/asset/inventory_count_status",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.allocationStatus",
        code: "010.021",
        description: "Trạng thái cấp phát",
        path: "/list/allocation_status",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.transferStatus",
        code: "010.022",
        description: "Trạng thái điều chuyển",
        path: "/list/asset_transfer_status",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.iatType",
        code: "010.023",
        description: "Loại công cụ dụng cụ",
        path: "/list/InstrumentAndToolsType",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.medicalEquipmentType",
        code: "010.024",
        description: "Loại thiết bị y tế",
        path: "/list/medicalEquipmentType",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.shoppingForm",
        code: "010.025",
        description: "Hình thức mua sắm",
        path: "/list/shoppingForm",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ADMIN
        ]
      },
      {
        name: "Dashboard.subcategory.council",
        code: "010.026",
        description: "Hội đồng",
        path: "list/council",
        icon: "keyboard_arrow_right",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ASSET_MANAGER
        ]
      },
      {
        name: "BiddingList.title",
        code: "010.027",
        description: "Danh mục thầu",
        path: "list/bidding",
        icon: " ",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_ORG_ADMIN,
          roles.ROLE_ASSET_MANAGER
        ]
      }
    
    
    ],
    roles: [
      roles.ROLE_ASSET_MANAGER,
      roles.ROLE_ORG_ADMIN,
      roles.ROLE_ADMIN,
    ],
  },
  {
    // name: "Dashboard.system",
    name: "Quản lý",
    code: "011",
    description: "Hệ thống (New)",
    path: "\"\"",
    icon: "memory",
    isVisible: true,
    children: [
      {
        name: "manage.user",
        code: "007.001",
        description: "Quản lý người dùng",
        path: "/manage/user",
        icon: "recent_actors",
        isVisible: true,
        children: [],
        roles: [
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      },
      {
        name: "Thông tin DN",
        code: "007.005",
        description: "Thông tin doanh nghiệp",
        path: "/list/infor_organization",
        icon: "contacts",
        isVisible: true,
        parent: {
          name: "Dashboard.system",
          code: "011",
          description: "Hệ thống (New)",
          path: "\"\"",
          icon: "memory",
          isVisible: true,
          parent: null,
          children: null,
          roles: null
        },
        children: [],
        roles: [
          roles.ROLE_ADMIN,
          roles.ROLE_SUPER_ADMIN,
          roles.ROLE_ORG_ADMIN
        ]
      }
    ],
    roles: [
      roles.ROLE_ORG_ADMIN,
      roles.ROLE_ADMIN,
      roles.ROLE_SUPER_ADMIN
    ]
  }
];
