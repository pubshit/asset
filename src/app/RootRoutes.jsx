import React from "react";
import { Redirect } from "react-router-dom";

import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import utilitiesRoutes from "./views/utilities/UtilitiesRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";
import materialRoutes from "./views/material-kit/MaterialRoutes";
import chartsRoute from "./views/charts/ChartsRoute";
import invoiceRoutes from "./views/invoice/InvoioceRoutes";
import calendarRoutes from "./views/calendar/CalendarRoutes";
import formsRoutes from "./views/forms/FormsRoutes";
import mapRoutes from "./views/map/MapRoutes";
import pageLayoutRoutes from "./views/page-layouts/PageLayoutRoutees";
import ListRoute from "./views/list/ListRoute";
import administrativeUnitRoutes from "./views/AdministrativeUnit/AdministrativeUnitRoutes";
import productRoute from "./views/Product/ProductRoutes";
import productTypeRoutes from "./views/ProductType/ProductTypeRoutes";
import InstrumentAndToolsTypeRoutes from "./views/InstrumentAndToolsType/InstrumentAndToolsTypeRoutes";
import InstrumentToolsListRoutes from "./views/InstrumentToolsList/InstrumentToolsListRoutes";
import productCategoryRoute from "./views/ProductCategory/ProductCategoryRoutes";
import productAttributeRoutes from "./views/ProductAttribute/ProductAttributeRoutes";
import userRoutes from "./views/User/UserRoutes";
import departmentRoutes from "./views/Department/DepartmentRoutes";
import ConstantList from "./appConfig";
import CommonObjectTypeRoutes from "./views/CommonObjectType/CommonObjectTypeRoutes";
import CommonObjectRoutes from "./views/CommonObject/CommonObjectRoutes";
import SupplierRoutes from "./views/Supplier/SupplierRoutes";
import ContractRoutes from "./views/Contract/ContractRoutes";
import AssetSourceRoutes from "./views/AssetSource/AssetSourceRoutes";
import StockKeepingUnitRoutes from "./views/StockKeepingUnit/StockKeepingUnitRoutes";
import AssetTypeRoutes from "./views/AssetType/AssetTypeRoutes";
import AssetGroupRoutes from "./views/AssetGroup/AssetGroupRoutes";
import AssetAllocationRoutes from "./views/AssetAllocation/AssetAllocationRoutes";
import AssetStatusRoutes from "./views/AssetStatus/AssetStatusRoutes";
import ProductTemplateRoutes from "./views/ProductTemplate/ProductTemplateRoutes";
import AssetRoutes from "./views/Asset/AssetRoutes";
import MaintainRequestStatusRoutes from "./views/MaintainRequestStatus/MaintainRequestStatusRoutes";
import AssetTransferRoutes from "./views/AssetTransfer/AssetTransferRoutes";
import MaintainRequestRoutes from "./views/MaintainResquest/MaintainRequestRoutes";
import InventoryCountVoucherRoutes from "./views/InventoryCountVoucher/InventoryCountVoucherRoutes";
import InventoryCountStatus from "./views/InventoryCountStatus/InventoryCountStatusRoutes";
import AggregateAssetsByAssetGroupRoutes from "./views/SummaryReport/AggregateAssetsByAssetGroup/AggregateAssetsByAssetGroupRoutes";
import AmountOfAssetsGroupedByAssetGroupRoutes from "./views/SummaryReport/AmountOfAssetsGroupedByAssetGroup/AmountOfAssetsGroupedByAssetGroupRoutes";
import AssetDepreciationCalculatorRoutes from "./views/AssetDepreciationCalculator/AssetDepreciationCalculatorRoutes";
import StaffRoutes from "./views/Staff/StaffRoutes";
import StoreRoutes from "./views/Store/StoreRoutes";
import InventoryReceivingVoucherRoutes from "./views/InventoryReceivingVoucher/InventoryReceivingVoucherRoutes";
import InventoryDeliveryVoucherRoutes from "./views/InventoryDeliveryVoucher/InventoryDeliveryVoucherRoutes";
import StockSummaryReportRoutes from "./views/StockSummaryReport/StockSummaryReportRoutes";
import InventoryReportRoutes from "./views/InventoryReport/InventoryReportRoutes";
import ReceivingAssetRoutes from "./views/ReceivingAsset/ReceivingAssetRoutes";
import PurchaseRequestStatusRoutes from "./views/PurchaseRequestStauts/PurchaseRequestStautsRoutes";
import PurchaseRequestRoutes from "./views/PurchaseRequest/PurchaseRequestRoutes";
import PurchasePlaningRoutes from "./views/PurchasePlaning/PurchasePlaningRoutes";
import MenuRoutes from "./views/Menus/MenuRoutes";
import PurchaseRequestCountRoutes from "./views/PurchaseRequestCount/PurchaseRequestCountRoutes";
import SuppliesPurchaseRequestRoutes from "./views/SuppliesPurchaseRequest/SuppliesPurchaseRequestRoutes";
import SuppliesPurchaseRequestCountRoutes from "./views/SuppliesPurchaseRequestCount/SuppliesPurchaseRequestCountRoutes";
import SuppliesPurchasePlaningRoutes from "./views/SuppliesPurchasePlaning/SuppliesPurchasePlaningRoutes";
import AmountOfAssetsGroupedByDepartmentRoutes from "./views/SummaryReport/AmountOfAssetsGroupedByDepartment/AmountOfAssetsGroupedByDepartmentRoutes";
import AmountOfAssetsGroupedByProductCategoryRoutes from "./views/SummaryReport/AmountOfAssetsGroupedByProductCategory/AmountOfAssetsGroupedByProductCategoryRoutes";
import DecreaseInReportedAssetsRoutes from "./views/SummaryReport/DecreaseInReportedAssets/DecreaseInReportedAssetsRoutes";
import IncreaseInReportedAssetsRoutes from "./views/SummaryReport/IncreaseInReportedAssets/IncreaseInReportedAssetsRoutes";
import AssetLiquidateRoutes from "./views/AssetLiquidate/AssetLiquidateRoutes";
import AllocationStatusRoutes from "./views/AllocationStatus/AllocationStatusRoutes";
import OrganizationRoutes from "./views/Organization/OrganizationRoutes";
import AssetTransferStatusRoutes from "./views/AssetTransferStatus/AssetTransferStatusRoutes";
import increaseOrDecreaseInReportedAssetsRoutes from "./views/SummaryReport/IncreaseOrDecreaseInReportedAssets/IncreaseOrDecreaseInReportedAssetsRoutes";
import AssetTransferToAnotherUnitRoutes from "./views/AssetTransferToAnotherUnit/AssetTransferToAnotherUnitRoutes";
import MaintainPlaningRoutes from "./views/MaintainPlaning/MaintainPlaningRoutes";
import AssetNoteRoutes from "./views/AssetNote/AssetNoteRoutes";
import InforOrganizationRouter from "./views/InforOrganization/InforOrganizationRouter";
import MedicalEquipmentTypeRoutes from "./views/MedicalEquipmentType/MedicalEquipmentTypeRoutes";
import ShoppingFormRoutes from "./views/ShoppingForm/ShoppingFormRoutes";
import IntrumentsToolsAllocationRoutes from "./views/InstrumentsToolAllocation/InstrumentsToolsAllocationRoutes";
import InstrumentToolsTransferRoutes from "./views/InstrumentToolsTransfer/InstrumentToolsTransferRoutes";
import InstrumentsToolsInventoryRoutes from "./views/InstrumentsToolsInventory/InstrumentsToolsInventoryRoutes";
import InstrumentsToolTransferToAnotherUnitRoutes from "./views/InstrumentsToolTransferToAnotherUnit/InstrumentsToolTransferToAnotherUnitRoutes";
import InstrumentsToolLiquidateRoutes from "./views/InstrumentsToolLiquidate/InstrumentsToolLiquidateRoutes";
import AssetReevaluateRoutes from "./views/AssetReevaluate/AssetReevaluateRoutes";
import WarehouseReceivingRoutes from "./views/WarehouseReceiving/WarehouseReceivingRoutes";
import WarehouseDeliveryRoutes from "./views/WarehouseDelivery/WarehouseDeliveryRoutes";
import WarehouseTransferFixedAssetsRoutes from "./views/WarehouseTranfers/WarehouseTransferFixedAssetsRoutes";
import WarehouseTransferInstrumentsToolsRoutes from "./views/WarehouseTranfers/WarehouseTransferInstrumentsToolsRoutes";
import WarehouseTransferMaterialManagementRoutes from "./views/WarehouseTranfers/WarehouseTransferMaterialManagementRoutes";
import VerificationRoutesCalibration from "./views/VerificationCalibration/VerificationRoutesCalibration";
import InventoryRoutes from "./views/Inventory/InventoryRoutes";
import CouncilRoutes from "./views/Council/CouncilRoutes";
import InventorySuggestedSuppliesRoutes from "./views/InventorySuggestedSupplies/InventorySuggestedSuppliesRoutes";
import CalibrationAssetsRouters from "./views/CalibrationAssets/CalibrationAssetsRouters";
import AccreditationRouters from "./views/AccreditationPlan/AccreditationRouters";
import CalibrationListRoutes from "./views/Calibration/CalibrationRoutes";
import ToolAllocationReportRoutes from "./views/SummaryReport/ToolAllocationReport/ToolAllocationReportRoutes";
import ReceiptRoutes from "./views/Receipt/ReceiptRoutes";
import RecallSlipRoutes from "./views/RecallSlip/RecallSlipRoutes";
import InstrumentsToolsReceiptRoutes from "./views/InstrumentsToolsReceipt/InstrumentsToolsReceiptRoutes";
import InventoryCountVoucherDepartmentRoutes from "./views/InventoryCountVoucherDepartment/InventoryCountVoucherDepartmentRoutes";
import InstrumentsToolsInventoryDepartmentRoutes from "./views/InstrumentsToolsInventoryDepartment/InstrumentsToolsInventoryDepartmentRoutes";
import SummaryOfAssetsByMedicalEquipmentGroupRoutes
  from "./views/SummaryReport/SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupRoutes";
import RecallSlipIatRoutes from "./views/RecallSlipIat/RecallSlipIatRouter";
import InstrumentsToolsRepairingRoutes from "./views/InstrumentsToolsRepairing/InstrumentsToolsRepairingRoutes";
import InstrumentsToolsReceptionRoutes from "./views/InstrumentsToolsReception/InstrumentsToolsReceptionRoutes";
import StatisticsOfAssetsUnderContractRoutes from "./views/SummaryReport/StatisticsOfAssetsUnderContract/StatisticsOfAssetsUnderContractRoutes";
import ComprehensiveReceptionRoutes from "./views/ComprehensiveReception/ReceptionRoutes";
import BiddingListRoutes from "./views/bidding-list/BiddingListRoutes";
import ComprehensiveReceiptRoutes from "./views/ComprehensiveReceipt/ReceiptRoutes"
import AssetRepairRoutes from "./views/AssetRepair/AssetRepairRoutes";
import ApprovalRequestRoutes from "./views/ApprovalRequest/ApprovalRequestRoutes";
import ShoppingPlanRoutes from "./views/ShoppingPlan/ShoppingPlanRoutes";
import InventoryCardRoutes from "./views/InventoryCard/InventoryCardRoutes"
import BeginningDepreciationRoutes from "./views/beginning-depreciation/BeginningDepreciationRoutes";

const redirectRoute = [
  {
    path: ConstantList.ROOT_PATH,
    exact: true,
    component: () => <Redirect to={ConstantList.HOME_PAGE} />, //Luôn trỏ về HomePage được khai báo trong appConfig
  },
];

const errorRoute = [
  {
    component: () => <Redirect to={ConstantList.ROOT_PATH + "session/404"} />,
  },
];

const routes = [
  ...AssetNoteRoutes,
  ...increaseOrDecreaseInReportedAssetsRoutes,
  ...DecreaseInReportedAssetsRoutes,
  ...IncreaseInReportedAssetsRoutes,
  ...sessionRoutes,
  ...dashboardRoutes,
  ...administrativeUnitRoutes,
  ...materialRoutes,
  ...utilitiesRoutes,
  ...chartsRoute,
  ...calendarRoutes,
  ...invoiceRoutes,
  ...formsRoutes,
  ...mapRoutes,
  ...productRoute,
  ...productTypeRoutes,
  ...InstrumentAndToolsTypeRoutes,
  ...CommonObjectTypeRoutes,
  ...AssetSourceRoutes,
  ...ContractRoutes,
  ...CommonObjectRoutes,
  ...SupplierRoutes,
  ...productCategoryRoute,
  ...StockKeepingUnitRoutes,
  ...AssetTypeRoutes,
  ...AssetGroupRoutes,
  ...AssetStatusRoutes,
  ...AssetAllocationRoutes,
  ...AssetRoutes,
  ...pageLayoutRoutes,
  ...ListRoute,
  ...departmentRoutes,
  ...productAttributeRoutes,
  ...ProductTemplateRoutes,
  ...userRoutes,
  ...redirectRoute,
  ...AssetTransferRoutes,
  ...MaintainRequestStatusRoutes,
  ...InventoryCountVoucherRoutes,
  ...InventoryCountStatus,
  ...AggregateAssetsByAssetGroupRoutes,
  ...AmountOfAssetsGroupedByAssetGroupRoutes,
  ...AssetDepreciationCalculatorRoutes,
  ...StaffRoutes,
  ...StoreRoutes,
  ...InventoryReceivingVoucherRoutes,
  ...InventoryDeliveryVoucherRoutes,
  ...StockSummaryReportRoutes,
  ...InventoryReportRoutes,
  ...InventorySuggestedSuppliesRoutes,
  ...ReceivingAssetRoutes,
  ...PurchaseRequestStatusRoutes,
  ...PurchaseRequestRoutes,
  ...PurchasePlaningRoutes,
  ...MenuRoutes,
  ...OrganizationRoutes,
  ...PurchaseRequestCountRoutes,
  ...SuppliesPurchaseRequestRoutes,
  ...SuppliesPurchaseRequestCountRoutes,
  ...SuppliesPurchasePlaningRoutes,
  ...AmountOfAssetsGroupedByDepartmentRoutes,
  ...AmountOfAssetsGroupedByProductCategoryRoutes,
  ...StatisticsOfAssetsUnderContractRoutes,
  ...AssetLiquidateRoutes,
  ...AssetTransferToAnotherUnitRoutes,
  ...AllocationStatusRoutes,
  ...AssetTransferStatusRoutes,
  ...MaintainPlaningRoutes,
  ...InforOrganizationRouter,
  ...MedicalEquipmentTypeRoutes,
  ...ShoppingFormRoutes,
  ...InstrumentToolsListRoutes,
  ...IntrumentsToolsAllocationRoutes,
  ...InstrumentToolsTransferRoutes,
  ...InstrumentsToolsInventoryRoutes,
  ...InstrumentsToolTransferToAnotherUnitRoutes,
  ...InstrumentsToolLiquidateRoutes,
  ...AssetReevaluateRoutes,
  ...WarehouseReceivingRoutes,
  ...WarehouseDeliveryRoutes,
  ...WarehouseTransferFixedAssetsRoutes,
  ...WarehouseTransferInstrumentsToolsRoutes,
  ...WarehouseTransferMaterialManagementRoutes,
  ...InventoryRoutes,
  ...CouncilRoutes,
  ...RecallSlipRoutes,
  ...RecallSlipIatRoutes,
  ...ComprehensiveReceiptRoutes,
  //hieu chinh
  ...CalibrationListRoutes,
  ...CalibrationAssetsRouters,
  // kiem dinh
  ...AccreditationRouters,
  ...VerificationRoutesCalibration,
  ...ToolAllocationReportRoutes,
  ...ReceiptRoutes,
  ...InstrumentsToolsReceiptRoutes,
  ...InventoryCountVoucherDepartmentRoutes,
  ...InstrumentsToolsInventoryDepartmentRoutes,
  ...SummaryOfAssetsByMedicalEquipmentGroupRoutes,
  ...InstrumentsToolsRepairingRoutes,
  ...InstrumentsToolsReceptionRoutes,
  ...ComprehensiveReceptionRoutes,
  ...BiddingListRoutes,
  ...AssetRepairRoutes,
  ...ApprovalRequestRoutes,
  ...ShoppingPlanRoutes,
  ...InventoryCardRoutes,
  ...BeginningDepreciationRoutes,
  ...errorRoute,
];

export default routes;
