import { Box, Paper, Tooltip } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { createFilterOptions } from "@material-ui/lab/useAutocomplete";
import moment from "moment";
import React from "react";
import {
  appConst,
  DEFAULT_MAX_STRING_LENGTH, formatDate,
  LIST_IMAGE_TYPE,
  LIST_ORGANIZATION,
  MEGABYTE, PRINT_TEMPLATE_MODEL,
  variable,
} from "./appConst";
import ConstantList, { CURRENT_ORG } from "./appConfig";
import Draggable from "react-draggable";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import localStorageService from "./services/localStorageService";
import { toast } from "react-toastify";
import { format } from "date-fns";
import i18n from "../i18n";
import { saveAs } from "file-saver";
import {getTemplateByModel} from "./appServices";
import { debounce } from "lodash";
import {dateFormat} from "react-big-calendar/lib/utils/propTypes";

const filterAutocomplete = createFilterOptions();
const t = i18n.t.bind(i18n);

let formatSiteBV199 = [
  LIST_ORGANIZATION.BV199.code,
  LIST_ORGANIZATION.PRODUCT_BV199.code,
].includes(CURRENT_ORG.code);

export const convertNumberPrice = (value, type) => {
  let { DOT, COMMA, SPACE } = appConst.TYPES_MONEY;
  let typeConvert = {
    [DOT.code]: DOT.value,
    [COMMA.code]: COMMA.value,
    [SPACE.code]: SPACE.value,
  };
  let thousandSeparator = typeConvert[type] !== undefined && typeConvert[type] !== null
    ? typeConvert[type]
    : formatSiteBV199 ? DOT.value : COMMA.value
  let number = Number(value ? value : 0);
  let plainNumber = number
    .toFixed(1)
    .replace(
      /\d(?=(\d{3})+\.)/g,
      "$&" + thousandSeparator
    );
  return value ? plainNumber.substring(0, plainNumber.length - 2) : null;
};

export const convertMoney = (amount, type, decimalType) => {
  let { DOT, COMMA, SPACE } = appConst.TYPES_MONEY;
  let typeConvert = {
    [DOT.code]: DOT.value,
    [COMMA.code]: COMMA.value,
    [SPACE.code]: SPACE.value,
  };
  let decimalTypes = {
    [DOT.code]: COMMA.value,
    [COMMA.code]: DOT.value,
  };
  let decimalSeparator = decimalType || decimalTypes[type] || DOT.value;
  let thousandSeparator = typeConvert[type] !== undefined && typeConvert[type] !== null
    ? typeConvert[type]
    : formatSiteBV199 ? DOT.value : COMMA.value
  // Làm tròn số tiền đến 2 chữ số sau dấu phẩy
  let roundedAmount = Math.round(amount * 100) / 100;

  // Tách phần nguyên và phần thập phân
  let integerPart = Math.floor(roundedAmount);
  let decimalPart = Math.round((roundedAmount - integerPart) * 100);

  // Định dạng phần nguyên với dấu cách giữa các nhóm ba chữ số
  let formattedIntegerPart = integerPart
    ?.toString()
    ?.replace(
      /\B(?=(\d{3})+(?!\d))/g,
      thousandSeparator,
    );

  // Kết hợp phần nguyên và phần thập phân
  let result;
  if (decimalPart === 0) {
    result = formattedIntegerPart;
  } else {
    result = `${formattedIntegerPart}${
      formatSiteBV199 ? COMMA.value : decimalSeparator
    }${decimalPart.toString().padStart(2, "0")}`;
  }
  return amount ? result : null;
};

export const convertNumberPriceRoundUp = (value) => {
  let number = Number(value ? value : 0);
  return !isNaN(number) ? convertNumberPrice(Math.ceil(number)) : "";
};

export const checkInvalidDate = (date) => {
  let newDate = new Date(date);
  return isNaN(Date.parse(newDate));
};

export const formatDateTypeArray = (createDate) => {
  if (!Array.isArray(createDate)) return createDate;

  if (createDate && createDate?.length > 0) {
    createDate[1] = createDate[1] - 1; //trường hợp khi tháng tăng lên 1
    createDate = new Date(...createDate);
  }
  return createDate;
};

export const checkMinDate = (date, minDate) => {
  let newMinDate = new Date(minDate);
  let newDate = new Date(date);
  return newDate < newMinDate;
};

export const compareDate = (firstDate, lastDate) => {
  const newFirstDate = new Date(firstDate);
  const newLastDate = new Date(lastDate);

  const dayFirst = newFirstDate.getDate();
  const monthFirst = newFirstDate.getMonth();
  const yearFirst = newFirstDate.getFullYear();

  const dayLast = newLastDate.getDate();
  const monthLast = newLastDate.getMonth();
  const yearLast = newLastDate.getFullYear();

  if (yearFirst < yearLast) {
    return -1;
  } else if (yearFirst > yearLast) {
    return 1;
  } else if (monthFirst < monthLast) {
    return -1;
  } else if (monthFirst > monthLast) {
    return 1;
  } else if (dayFirst < dayLast) {
    return -1;
  } else if (dayFirst > dayLast) {
    return 1;
  } else {
    return 0;
  }
};

export const checkObject = (obj) => {
  return Object.keys(obj ? obj : {}).length === 0;
};
export const isCheckLenght = (data, number = DEFAULT_MAX_STRING_LENGTH) => {
  return data?.length > number;
};
export const isCheckCharacterAa9 = (string) => {
  let regex = /^[a-zA-Z0-9]*$/;
  return regex.test(string);
};
export const isNumberRegex = (string) => {
  let regex = /^[0-9.]*$/;
  return regex.test(string);
};
export const isSpecialCharacters = (value) => {
  //các ký tự đặc biệt
  let regex = /[~`!@#$%^&*()+=\-[\]\\';,/{}|\\":<>?]/;
  return !regex.test(value);
};

export const formatDateDto = (date) => {
  return date ? moment(date).format("YYYY-MM-DDTHH:mm:ss") : null;
};
export const formatDateDtoZ = (date) => {
  return date
    ? format(new Date(date), "yyyy-MM-dd'T'HH:mm:ss.SSS") + "Z"
    : null;
};
export const formatDateDtoMore = (date, type) => {
  return date
    ? moment(date).format(
        `YYYY-MM-DDT${type === "start" ? "00:00:00" : "23:59:59"}`
      )
    : null;
};
export const formatTimestampToDate = (date) => {
  return date ? moment(date).format(`DD/MM/YYYY`) : null;
};

export const formatDateToTimestamp = (date) => {
  return date ? moment(date).valueOf() : null;
};

export const formatDateTimeDto = (date, hours, minutes, seconds) => {
  const now = new Date(date);
  hours && now.setHours(hours);
  minutes && now.setMinutes(minutes);
  seconds && now.setSeconds(seconds);
  let formatDate = isValidDate(now)
    ? moment(now).format("YYYY-MM-DDTHH:mm:ss")
    : null;
  return formatDate;
};

export const formatDateNoTime = (date) => {
  return date ? moment(date).format("YYYY-MM-DD") : null;
};

export function TabPanel(props) {
  const { children, value, index, boxProps, indexes = [], ...other } = props;
  let hidden = false;
  if (indexes?.length > 0) {
    hidden = !indexes?.includes(value);
  } else {
    hidden = value !== index;
  }
  if (indexes?.length > 0) {
    return (
      <React.Fragment key={index}>
        <div
          role="tabpanel"
          hidden={hidden}
          id={`scrollable-force-tabpanel-${index}`}
          aria-labelledby={`scrollable-force-tab-${index}`}
          {...other}
        >
          <Box p={3} className={boxProps?.className}>
            {children}
          </Box>
        </div>
      </React.Fragment>
    );
  }
  return (
    <React.Fragment key={index}>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3} className={boxProps?.className}>
            {children}
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  boxProps: PropTypes.node,
  index: PropTypes.any.isRequired,
  indexes: PropTypes.array,
  value: PropTypes.any.isRequired,
};

export const filterOptions = (options, params, isNew, displayLable) => {
  try {
    params.inputValue = params.inputValue?.trim();
    let filtered = filterAutocomplete(options, params);
    
    if (isNew && filtered?.length === 0) {
      filtered.push({ [displayLable]: "Thêm mới", code: "New" });
    }
    
    return filtered;
  } catch (e) {
    if (isNew && options?.length === 0) {
      options?.push({ [displayLable]: "Thêm mới", code: "New" });
    }
    return options;
  }
};

export const getOptionSelected = (option, value) => {
  // getOptionSelected default
  return (
    (value?.id && option?.id === value?.id) // Default object selected
    || (value?.code && option?.code === value?.code)  // appConst object selected
    || (value?.indexOrder && option?.indexOrder === value?.indexOrder)
    || false
  );
};

export const removeCommas = (inputString) => {
  return formatSiteBV199
    ? inputString.replaceAll(".", "")
    : inputString.replace(/,/g, "");
};

export const removeDartline = (inputString) => {
  if (inputString) {
    let text = inputString;
    let toArray = text?.trim().split("");
    if (toArray[0] === "-") {
      toArray.shift();
    }
    return toArray.join("").trim();
  }
  return "";
};

export const getTheHighestRole = () => {
	let permissions = {
		addressOfEnterprise: null,
		currentUser: null,
		departmentUser: null,
		organization: null,
		isRoleOrgAdmin: false,
		isRoleSuperAdmin: false,
		isRoleAdmin: false,
		isRoleAccountant: false,
		isRoleAssetManager: false,
		isRoleAssetUser: false,
		isRoleUser: false,
	};
  const currentUser = JSON.parse(
    sessionStorage.getItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER)
  );
  const departmentUser =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER
    ) || null;
  const organization =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    ) || null;
  const addressOfEnterprise =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.ADDRESS_OF_ENTERPRISE
    ) || "";

  if (currentUser) {
		permissions.addressOfEnterprise = addressOfEnterprise;
		permissions.currentUser = currentUser;
		permissions.departmentUser = departmentUser;
		permissions.organization = organization;
				
    let listRole = currentUser?.roles;
    let isRoleAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ADMIN
    );
    let isRoleAssetManager = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER
    );
    let isRoleAssetUser = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_USER
    );
    let isRoleUser = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_USER
    );
    let isRoleSuperAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_SUPER_ADMIN
    );
    let isRoleAccountant = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ACCOUNTANT
    );
    let isRoleOrgAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN
    );

    if (departmentUser?.id) {
      departmentUser.text = departmentUser.name + " - " + departmentUser.code;
    }

    if (isRoleSuperAdmin) {
      permissions.isRoleSuperAdmin = isRoleSuperAdmin;
      return permissions;
    }

    if (isRoleOrgAdmin) {
      permissions.isRoleOrgAdmin = isRoleOrgAdmin;
      return permissions;
    }

    if (isRoleAdmin) {
      permissions.isRoleAdmin = isRoleAdmin;
      return permissions;
    }

    if (isRoleAccountant) {
      permissions.isRoleAccountant = isRoleAccountant;
      return permissions;
    }

    if (isRoleAssetManager) {
      permissions.isRoleAssetManager = isRoleAssetManager;
      return permissions;
    }

    if (isRoleAssetUser) {
      permissions.isRoleAssetUser = isRoleAssetUser;
      return permissions;
    }

    if (isRoleUser) {
      permissions.isRoleUser = isRoleUser;
      return permissions;
    }
  }
	
	return permissions;
};

export const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: "-1.5em",
  },
}))(Tooltip);

export const PaperComponent = (props) => {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
};

export const a11yProps = (index) => {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
};

export function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  // let objectSeparator = {
  //   [LIST_ORGANIZATION.BV199.code]: {
  //     thousandSeparator: appConst.TYPES_MONEY.DOT.value,
  //     decimalSeparator: appConst.TYPES_MONEY.COMMA.value,
  //   },
  //   [LIST_ORGANIZATION.PRODUCT_BV199.code]: {
  //     thousandSeparator: appConst.TYPES_MONEY.DOT.value,
  //     decimalSeparator: appConst.TYPES_MONEY.COMMA.value,
  //   },
  //   [null]: {
  //     thousandSeparator: appConst.TYPES_MONEY.COMMA.value,
  //     decimalSeparator: appConst.TYPES_MONEY.DOT.value,
  //   },
  // };

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator={
        formatSiteBV199
          ? appConst.TYPES_MONEY.DOT.value
          : appConst.TYPES_MONEY.COMMA.value
      }
      decimalSeparator={
        formatSiteBV199
          ? appConst.TYPES_MONEY.COMMA.value
          : appConst.TYPES_MONEY.DOT.value
      }
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export const checkMaxWordLength = (value) => {
  const max_limit = 20;
  if (value?.split(" ")?.some((word) => word.length > max_limit)) {
    return false;
  }
  return true;
};

export const sortDataByDate = (data, dateName) => {
  const sortedData = [...data].sort((a, b) => {
    return new Date(b[dateName]) - new Date(a[dateName]);
  });
  return sortedData;
};

export const convertFromToDate = (date) => {
  const newDate = new Date(date);
  return {
    fromDate: isValidDate(date)
      ? moment(newDate).format("YYYY-MM-DD") + "T00:00:00"
      : null,
    toDate: isValidDate(date)
      ? moment(newDate).format("YYYY-MM-DD") + "T23:59:59"
      : null,
  };
};
export const convertFromToDateZ = (date) => {
  const newDate = new Date(date);
  return {
    fromDate: isValidDate(date)
      ? moment(newDate).format("YYYY-MM-DD") + "T00:00:00.000Z"
      : null,
    toDate: isValidDate(date)
      ? moment(newDate).format("YYYY-MM-DD") + "T23:59:59.999Z"
      : null,
  };
};
export const convertDateToHHMMSSDDMMYYYY = (date) => {
  const newDate = new Date(date);
  return isValidDate(date)
    ? moment(newDate).format("HH:mm:ss DD/MM/YYYY")
    : null;
};
export const getRole = () => {
  const currentUser = JSON.parse(
    sessionStorage.getItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER)
  );
  if (currentUser) {
    let permissions = {
      currentUser: currentUser,
      isRoleOrgAdmin: false,
      isRoleSuperAdmin: false,
      isRoleAdmin: false,
      isRoleAccountant: false,
      isRoleAssetManager: false,
      isRoleAssetUser: false,
      isRoleUser: false,
    };
    let listRole = currentUser?.roles;

    let isRoleAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN
    );
    if (isRoleAdmin) {
      permissions.isRoleAdmin = isRoleAdmin;
    }

    let isRoleAssetManager = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER
    );
    if (isRoleAssetManager) {
      permissions.isRoleAssetManager = isRoleAssetManager;
    }

    let isRoleAssetUser = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_USER
    );
    if (isRoleAssetUser) {
      permissions.isRoleAssetUser = isRoleAssetUser;
    }

    let isRoleUser = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_USER
    );
    if (isRoleUser) {
      permissions.isRoleUser = isRoleUser;
    }

    let isRoleSuperAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_SUPER_ADMIN
    );
    if (isRoleUser) {
      permissions.isRoleSuperAdmin = isRoleSuperAdmin;
    }

    let isRoleAccountant = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ACCOUNTANT
    );
    if (isRoleAccountant) {
      permissions.isRoleAccountant = isRoleAccountant;
    }

    let isRoleOrgAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN
    );
    if (isRoleOrgAdmin) {
      permissions.isRoleOrgAdmin = isRoleOrgAdmin;
    }
    return permissions;
  }
};

export const isValidDate = (date) =>
  date && new Date(date).getTime() === new Date(date).getTime();

export const handleKeyDownIntegerOnly = (e) =>
  variable.regex.numberExceptThisSymbols.includes(e.key) && e.preventDefault();

export const handleKeyDownFloatOnly = (e) =>
  variable.regex.decimalNumberExceptThisSymbols.includes(e.key) &&
  e.preventDefault();

export const handleKeyDownSpecial = (e) =>
  variable.regex.specialCharactersKey.includes(e.key) && e.preventDefault();

export const handleKeyDownNameSpecialExcept = (e) =>
  variable.regex.nameSpecialCharactersExcept.includes(e.key) &&
  e.preventDefault();

export const spliceString = (name, cutFrom) => {
  let targetStr = cutFrom ? cutFrom : " - "; // chuỗi con để tìm vị trí cắt vd: targetStr = " - "
  let index = name?.indexOf(targetStr); // Tìm vị trí của chuỗi con

  return index !== -1 ? name?.substring(0, index) : name;
};

export const convertNumberToWords = (value) => {
  const defaultNumbers = " hai ba bốn năm sáu bảy tám chín";

  const chuHangDonVi = ("1 một" + defaultNumbers).split(" ");
  const chuHangChuc = ("lẻ mười" + defaultNumbers).split(" ");
  const chuHangTram = ("không một" + defaultNumbers).split(" ");

  function convert_block_three(number) {
    if (number === "000") return "";
    var _a = number + ""; //Convert biến 'number' thành kiểu string

    //Kiểm tra độ dài của khối
    switch (_a.length) {
      case 0:
        return "";
      case 1:
        return chuHangDonVi[_a];
      case 2:
        return convert_block_two(_a);
      case 3:
        var chuc_dv = "";
        if (_a.slice(1, 3) !== "00") {
          chuc_dv = convert_block_two(_a.slice(1, 3));
        }
        var tram = chuHangTram[_a[0]] + " trăm";
        return tram + " " + chuc_dv;
    }
  }

  function convert_block_two(number) {
    var dv = chuHangDonVi[number[1]];
    var chuc = chuHangChuc[number[0]];
    var append = "";
    // Nếu chữ số hàng đơn vị là 5
    if (number[0] > 0 && number[1] == 5) {
      dv = "lăm";
    }
    // Nếu số hàng chục lớn hơn 1
    if (number[0] > 1) {
      append = " mươi";

      if (number[1] === 1) {
        dv = " mốt";
      }
    }

    return chuc + "" + append + " " + dv;
  }
  const dvBlock = "1 nghìn triệu tỷ".split(" ");

  function NumberToText(number) {
    let str = parseInt(number) + "";
    let i = 0;
    let arr = [];
    let index = str.length;
    let result = [];
    let rsString = "";

    if (index === 0 || str === "NaN") {
      return "";
    }

    // Chia chuỗi số thành một mảng từng khối có 3 chữ số
    while (index >= 0) {
      arr.push(str.substring(index, Math.max(index - 3, 0)));
      index -= 3;
    }

    // Lặp từng khối trong mảng trên và convert từng khối đấy ra chữ Việt Nam
    for (i = arr.length - 1; i >= 0; i--) {
      if (arr[i] !== "" && arr[i] !== "000") {
        result.push(convert_block_three(arr[i]));

        // Thêm đuôi của mỗi khối
        if (dvBlock[i]) {
          result.push(dvBlock[i]);
        }
      }
    }

    // Join mảng kết quả lại thành chuỗi string
    rsString = result.join(" ");

    // Trả về kết quả kèm xóa những ký tự thừa
    return rsString.replace(/[0-9]/g, "").replace(/ /g, " ").replace(/ $/, "");
  }
  return (
    NumberToText(value).charAt(0).toUpperCase() + NumberToText(value).slice(1)
  );
};

export const handleKeyUp = (event, callBack = () => {}) => {
  if (!event?.target?.value) {
    callBack();
  }
};

export const isSuccessfulResponse = (code) => appConst.CODE.SUCCESS === code;

export const handleKeyDown = (event, callBack = () => {}, component) => {
  if (event.key === appConst.KEY.ENTER) {
    callBack();
  }
};

export const handlePreventOnEnter = (e) => {
  if (e.key === appConst.KEY.ENTER) {
    e.preventDefault();
  }
};

export const getRoleCategory = () => {
  const currentUser = JSON.parse(
    sessionStorage.getItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER)
  );
  const departmentUser =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER
    ) || {};

  if (currentUser) {
    let permissions = {
      departmentUser,
      currentUser,
      isRoleOrgAdmin: false,
      isRoleSuperAdmin: false,
      isRoleAdmin: false,
      isRoleAccountant: false,
      isRoleAssetManager: false,
      isRoleAssetUser: false,
      isRoleUser: false,
    };
    let listRole = currentUser?.roles;

    if (departmentUser) {
      departmentUser.text = departmentUser.name + " - " + departmentUser.code;
    }

    let isRoleAccountant = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ACCOUNTANT
    );
    if (isRoleAccountant) {
      permissions.isRoleAccountant = isRoleAccountant;
      return permissions;
    }

    let isRoleOrgAdmin = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN
    );
    if (isRoleOrgAdmin) {
      permissions.isRoleOrgAdmin = isRoleOrgAdmin;
      return permissions;
    }

    let isRoleAssetManager = listRole?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER
    );
    if (isRoleAssetManager) {
      permissions.isRoleAssetManager = isRoleAssetManager;
      return permissions;
    }

    return permissions;
  }
};

export const handleThrowResponseMessage = (res) => {
  // todo: handle message response in various case
  const { message, data, code, apiSubErrors } = res?.data;
  const singleMessageKey = variable.listFunctionConstants.errorMessage;
  const multipleMessageKey = variable.listFunctionConstants.message;

  toast.clearWaitingQueue();
  if (handleErrorByCodeOrStatus(code)) {
    // case: specific code
    return;
  } else if (apiSubErrors?.length) {
    // case: code = 500 but there's no specific message
    return apiSubErrors?.forEach((x) => {
      toast.warning(x[singleMessageKey]);
    });
  } else if (!data) {
    // case: code = 500 but there's no specific message
    return toast.warning(message);
  }
  let firstObject = data?.[0];
  if (Object.keys(firstObject).includes(singleMessageKey)) {
    toast.warning(firstObject[singleMessageKey] || message); //case:  data = [{errorMessage: null}]
  } else if (Object.keys(firstObject).includes(multipleMessageKey)) {
    //case: multiple message
    data?.forEach((x) => {
      toast.warning(x[multipleMessageKey]);
    });
  } else {
    toast.error(t("general.error"));
  }
};

const handleErrorByCodeOrStatus = (code) => {
  const codes = appConst.CODE;
  const messages = {
    [codes.SUCCESS]: t("general.success"),
    [codes.CONFLIG]: t("general.confligResponseMessage"),
    [codes.ACCESS_DENIED]: t("general.accessDeniedResMessage"),
  };

  if (Object.keys(messages).includes(code?.toString())) {
    if (isSuccessfulResponse(code)) {
      toast.success(messages[code]);
    } else {
      toast.warning(messages[code]);
    }
    return true;
  }

  return false;
};

export const getAssetStatus = (indexOrder) => ({
  isNewInStore: appConst.listStatusAssets.LUU_MOI.indexOrder === indexOrder,
  isWarehouseReceive:
    appConst.listStatusAssets.NHAP_KHO.indexOrder === indexOrder,
  isUsing: appConst.listStatusAssets.DANG_SU_DUNG.indexOrder === indexOrder,
  isBroken: appConst.listStatusAssets.DA_HONG.indexOrder === indexOrder,
  isLiquidated: appConst.listStatusAssets.DA_THANH_LY.indexOrder === indexOrder,
  isTransferedToAnotherUnit:
    appConst.listStatusAssets.CHUYEN_DI.indexOrder === indexOrder,
  isMaintainingOrReparing:
    appConst.listStatusAssets.DANG_BAO_DUONG.indexOrder === indexOrder,
  isOthers: appConst.listStatusAssets.KHAC.indexOrder === indexOrder,
  isProposedLiquidation:
    appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder === indexOrder,
});

export const classStatus = (status) => {
  const statusClassMap = {
    [appConst.statusPurchasePlaning.DANG_XU_LY.code]: "status status-warning",
    [appConst.statusPurchasePlaning.DA_XU_LY.code]: "status status-success",
    [appConst.statusPurchasePlaning.DA_KET_THUC.code]: "status status-info",
  };

  return statusClassMap[status] || "";
};

export const classStatusSubTable = (status) => {
  const statusClassMap = {
    [appConst.listStatusProductInPurchaseRequest.CHO_DUYET.code]:
      "status status-warning",
    [appConst.listStatusProductInPurchaseRequest.DA_DUYET.code]:
      "status status-success",
    [appConst.listStatusProductInPurchaseRequest.KHONG_DUYET.code]:
      "status status-error",
  };

  return statusClassMap[status] || "";
};

export const checkStatusSubTable = (status) => {
  let itemStatus = appConst.listStatusProductInPurchaseRequestArray.find(
    (item) => item.code === status
  );
  return itemStatus?.name;
};

export const functionExportToExcel = async (
  apiFunction = () => {},
  searchObject,
  fileName,
  setPageLoading = () => {}
) => {
  try {
    setPageLoading(true);
    let res = await apiFunction(searchObject);
    // check nếu lỗi parse thì có thể parse sang json để lấy message
    if (!isSuccessfulResponse(res?.status)) {
      return toast.error(t("general.error"));
    }

    try {
      let newData = new Blob([res?.data], { type: "application/json" });
      let data = JSON.parse(await newData.text());
      if (!isSuccessfulResponse(data?.code)) {
        handleThrowResponseMessage({ data });
      }
    } catch (e) {
      console.error(e);
      toast.success(t("general.successExport"));
      let blob = new Blob([res?.data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      saveAs(blob, fileName);
    }
  } catch (e) {
    console.error(e);
    toast.error(t("general.error"));
  } finally {
    setPageLoading(false);
  }
};

export const functionExportToWord = async (
  apiFunction = async () => {},
  urlExport,
  searchObject,
  fileName,
  setPageLoading = () => {}
) => {
  try {
    setPageLoading(true);
    let res = await apiFunction(urlExport, searchObject);
    
    if (!res || !isSuccessfulResponse(res.status)) {
      return toast.error(t("general.error"));
    }
    
    try {
      let newData = new Blob([res?.data], { type: "application/json" });
      let data = JSON.parse(await newData.text());
      if (!isSuccessfulResponse(data?.code)) {
        handleThrowResponseMessage({ data });
      }
    } catch (e) {
      toast.success(t("general.successExport"));
      let blob = new Blob([res?.data], {
        type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      });
      saveAs(blob, fileName);
    }
  } catch (error) {
    console.error("Error exporting Word file:", error);
  } finally {
    setPageLoading(false);
  }
};

export const getUserInformation = () => {
  const currentUser = JSON.parse(
    sessionStorage.getItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER)
  );
  const departmentUser =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER
    ) || null;
  const organization =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    ) || null;
  const addressOfEnterprise =
    localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.ADDRESS_OF_ENTERPRISE
    ) || "";

  if (departmentUser?.id) {
    departmentUser.text = departmentUser.name + " - " + departmentUser.code;
  }

  return {
    currentUser,
    departmentUser,
    organization,
    addressOfEnterprise,
  };
};

export function removeFalsy(params) {
  const result = {};

  Object.keys(params).forEach((key) => {
    const val = params[key];

    if (val) result[key] = val;
  });

  return result;
}

export const isInValidDateForSubmit = () => {
  return (
    document.getElementById("date-picker-dialog-helper-text")?.innerHTML
      ?.length > 0
  );
};

export const isInValidForSubmit = () => {
  let helperTextElement = document.querySelector(".MuiFormHelperText-root");
  if (!helperTextElement) {
    return false;
  }

  let invalid = Object.values(helperTextElement?.classList || {})?.includes(
    "Mui-error"
  );
  return invalid && helperTextElement?.innerHTML?.length > 0;
};

export const openFileInNewWindow = (file, title) => {
  const listFileType = appConst.TYPES_FILE;

  if (
    file?.type === listFileType.PDF ||
    file?.type === listFileType.TEXT ||
    listFileType.IMAGE_ARRAY.includes(file?.type)
  ) {
    let fileURL = URL.createObjectURL(file);
    let newWindow = window.open(fileURL);
    newWindow.onload = () => {
      newWindow.document.title = title;
    };
    newWindow.document.close();
  } else {
    toast.warning(t("general.viewAttachmentError"));
  }
};

export const labelDisplayedRows = ({ from, to, count }) =>
  `${from}-${to} ${t("general.of")} ${
    count !== -1 ? count : "more than " + to
  }`;

export const handleCheckFilesSize = (files) => {
  if (!files.length) {
    return true;
  }

  let hasOverSizeFile = files?.some((file) => file.size > 10 * MEGABYTE);

  if (hasOverSizeFile) {
    toast.warning("Kích thước tệp không được quá 10MB");
    return true;
  }

  return false;
};

export const isTypeNumber = (value) => !isNaN(Number(value));

export const removeAccents = (str) => {
  return str ? str.normalize("NFD").replace(/[\u0300-\u036f]/g, "") : "";
};

export const validateYear = (value) => {
  const currentYear = new Date().getFullYear();
  const listRegex = [
    new RegExp(`^(19[0-9]{2}|20[0-9]{2}|${currentYear})$`),
    /^[1-9][0-9]{3}$/,
  ];
  let check = true;
  for (let regex of listRegex) {
    if (!regex.test(value)) {
      check = false;
      break;
    }
  }
  return check;
};

export const checkCount = (count) => (count ? (count > 99 ? "99+" : count) : 0);

export const convertDepartmentNameForSignature = (
  name,
  departments,
  prefix
) => {
  let string = "";

  if (!name) {
    return null;
  }

  // Chuẩn hóa chuỗi gốc
  let normalizedName = removeAccents(name).toLowerCase();

  // Lặp qua tất cả các từ khóa trong mảng departments
  departments?.forEach((department) => {
    let normalizedDepartment = removeAccents(department).toLowerCase();
    if (normalizedName.includes(normalizedDepartment)) {
      // Tìm vị trí của từ khóa trong chuỗi đã chuẩn hóa
      let startIndex = normalizedName.indexOf(normalizedDepartment);
      if (startIndex !== -1) {
        // Tìm vị trí bắt đầu của từ khóa trong chuỗi gốc
        let originalStartIndex = normalizedName
          .toLowerCase()
          .indexOf(normalizedDepartment.toLowerCase());
        let originalEndIndex = originalStartIndex + normalizedDepartment.length;

        // Loại bỏ từ khóa khỏi chuỗi gốc
        string =
          name.substring(0, originalStartIndex) +
          name.substring(originalEndIndex);

        // Cập nhật chuỗi gốc và chuỗi đã chuẩn hóa sau khi loại bỏ từ khóa
        name = string.trim();
        normalizedName = removeAccents(name).toLowerCase();
      }
    }
  });

  // Nếu không có từ nào trong mảng departments được tìm thấy
  if (!string) {
    string = name;
  }

  return `${prefix || ""}${string || ""}`;
};

export const convertToAcronym = (string) => {
  const words = string?.split(" ");
  return words
    ?.map((word) => {
      if (word.match(/[A-Z]{2,}/)) {
        return word;
      }
      return word.charAt(0).toUpperCase();
    })
    .join("");
};

export const defaultPaginationProps = () => {
  return {
    align: "left",
    className: "px-16",
    component: "div",
    labelRowsPerPage: t("general.rows_per_page"),
    labelDisplayedRows,
    backIconButtonProps: {
      "aria-label": "Previous Page",
    },
    nextIconButtonProps: {
      "aria-label": "Next Page",
    },
  };
};

export const encodeWithVariableCharacters = (str = "", specialChars = []) => {
  return str
    ?.split("")
    ?.map((char) => {
      return specialChars.includes(char) ? encodeURIComponent(char) : char;
    })
    ?.join("");
};

export const formatDateArray = (date) => {
  const [year, month, day, hours, minutes, seconds] = date;
  const formattedDate = `${year}-${
    String(month).padStart(2, "0")
  }-${
    String(day).padStart(2, "0")
  }T${
    String(hours || 0).padStart(2, "0")
  }:${
    String(minutes || 0).padStart(2, "0")
  }:${
    String(seconds || 0).padStart(2, "0")
  }`;
  return date ? formattedDate : null;
};

// isCompare vì là mảng nên khi hiển thị số tháng phải thêm 1
const formatToISO = (arr, isCompare = false) => {
  const year = arr[0] || "1970"; // Mặc định năm là 1970 nếu không có
  const month = isCompare ? arr[1] : +arr[1] + 1 || "01"; // Mặc định tháng là 01
  const day = arr[2] || "01"; // Mặc định ngày là 01
  const hour = arr[3] || "00"; // Mặc định giờ là 00
  const minute = arr[4] || "00"; // Mặc định phút là 00
  const second = arr[5] || "00"; // Mặc định giây là 00

  // Format với padding để đảm bảo đúng định dạng
  return `${year}-${String(month).padStart(2, "0")}-${String(day).padStart(
    2,
    "0"
  )}T${String(hour).padStart(2, "0")}:${String(minute).padStart(
    2,
    "0"
  )}:${String(second).padStart(2, "0")}`;
};

export const validateAndFormatDateArr = (arr, isCompare) => {
  if (!Array.isArray(arr) || !arr) {
    return null;
  }
  return formatToISO(arr, isCompare);
};

export const getFirstAndLastDayOfMonth = (date) => {
  const firstDay = moment(date).startOf("month");
  const lastDay = moment(date).endOf("month");

  return {
    firstDayFormatted: date ? formatDateDto(firstDay) : null,
    lastDayFormatted: date ? formatDateDto(lastDay) : null,
  };
};

export const formatToDateEndDate = (start, end) => {
  if (!start || !end) return "";
  let startText = moment(start).format(`DD/MM/YYYY`);
  let endText = moment(end).format(`DD/MM/YYYY`);
  return {
    __html: `${startText} <span class="fa">&#xf30b;</span> ${endText}`,
  };
};

export const compareMoney = (num1, num2, accuracy) => {
  const epsilon = accuracy || 1e-6; // Độ chính xác 10^-6 là mặc định
  return Math.abs(num1 - num2) < epsilon; // true nếu 2 số gần đúng
};

export const convertObjectToStringByFields = (obj, fields) => {
  // Duyệt qua mảng fields và lấy giá trị tương ứng từ object
  const values = fields
    .map((key) => obj[key])
    ?.filter(Boolean) //Lọc giá trị null | undefined
    .join(" - "); // Nối chuỗi

  // Nối các giá trị lại với nhau bằng chuỗi " - "
  return values;
};

export const removeEmptyFields = (obj = {}) => {
  return Object.entries(obj).reduce((acc, [key, value]) => {
    if (value) {
      acc[key] = value;
    }
    return acc;
  }, {});
};

//format phần thập phân lớn về dạng 2 chữ số sau phần thập phân
export const correctPrecision = (value = 0) =>
  Math.round((value + Number.EPSILON) * 100) / 100;

export const handleGetTemplatesByModel = async (model) => {
  const {organization} = getUserInformation();
  try {
    let searchObject = {
      orgId: organization?.org?.id,
      model,
    }
    let res = await getTemplateByModel(searchObject);
    let {code, data} = res?.data;
    if (isSuccessfulResponse(code)) {
      return data
    } else {
      handleThrowResponseMessage(res);
    }
  } catch (e) {
    console.error(e)
  }
}

export const onInputChangeDataSearch = debounce((value, func) => {
  func(value);
}, 400);

export const showTextWithSeparator = (value, separator) => {
  return value ? `${separator}${value}` : null
}
