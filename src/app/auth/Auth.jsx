import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { setUserData } from "../redux/actions/UserActions";
import jwtAuthService from "../services/jwtAuthService";
import history from "history.js";
import ConstantList from "../appConfig";
import axios from "axios";
import AppContext from "../appContext";

class Auth extends Component {
  static contextType = AppContext; // Khai báo context ở đây
  state = {
    shouldReRender: false,
  };

  constructor(props, context) {
    super(props, context);
    let {setPageLoading} = context;
    try {
      setPageLoading(true);
      jwtAuthService.checkToken().then((data) => {
        this.setState({
          shouldReRender: Boolean(data?.shouldUpdateNavigations),
        })
        axios.interceptors.response.use(
          (response) => {
            return response;
          },
          (error) => {
            if (error.response?.status === ConstantList.ERROR_CODE.Unauthorized) {
              history.push(ConstantList.LOGIN_PAGE);
              return Promise.reject(error);
            }
          }
        );
      });
    } catch (e) {
      console.error(e);
    } finally {
      setPageLoading(false);
    }
  }
  
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return Boolean(nextState?.shouldReRender);
  }
  
  render() {
    const { children } = this.props;
    return <Fragment>{children}</Fragment>;
  }
}

const mapStateToProps = state => ({
  setUserData: PropTypes.func.isRequired,
  login: state.login
});

export default connect(
  mapStateToProps,
  { setUserData }
)(Auth);
