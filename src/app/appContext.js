import React from "react";

const AppContext = React.createContext({
	routes: [],
	pageLoading: false,
	setPageLoading: () => {},
	theme: {},
	setTheme: () => {},
	eventData: [],
	setEventData: () => {},
	currentOrg: {},
});

export default AppContext;
