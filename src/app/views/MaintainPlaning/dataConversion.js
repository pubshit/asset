import {convertFromToDate} from "../../appFunction";
import {appConst, STATUS_CHU_KY_CHO_XU_LY_DEFAULT, STATUS_CHU_KY_DANG_XU_LY_DEFAULT} from "../../appConst";
import moment from "moment";

export const convertPayloadChuKy = (state) => {
  const {
    dxTo,
    dxFrom,
    ntTo,
    ntFrom,
    selectedList,
    department,
    commonObject,
    supplier,
    contract,
    isNtWithoutContract,
    chuKyStatuses,
    times,
    searchObject,
    tabValue,
    isExcel,
  } = state;
	const isTabWaiting = appConst.tabStatus.tabWaiting === tabValue;
  let chuKyStatusesByTab = {
    [appConst.tabStatus.tabWaiting]: [...STATUS_CHU_KY_CHO_XU_LY_DEFAULT],
    [appConst.tabStatus.tabPreProcessed]: chuKyStatuses?.code
      ? [chuKyStatuses?.code]
      : [...STATUS_CHU_KY_CHO_XU_LY_DEFAULT, ...STATUS_CHU_KY_DANG_XU_LY_DEFAULT],
    [appConst.tabStatus.tabProcessed]: chuKyStatuses?.code
      ? [chuKyStatuses?.code]
      : STATUS_CHU_KY_DANG_XU_LY_DEFAULT,
  };
  let nghiemThuStatusByTab = {
    [appConst.tabStatus.tabWaiting]: appConst.STATUS_NGHIEM_THU_BTBD.CHO_XU_LY.code,
    [appConst.tabStatus.tabPreProcessed]: appConst.STATUS_NGHIEM_THU_BTBD.DANG_XU_LY.code,
    [appConst.tabStatus.tabProcessed]: appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code,
  };
  let statusByTab = {
    [appConst.tabStatus.tabWaiting]: appConst.CHU_KY_STATUS.CHO_XU_LY,
    [appConst.tabStatus.tabPreProcessed]: appConst.CHU_KY_STATUS.DA_XU_LY,
    [appConst.tabStatus.tabProcessed]: appConst.CHU_KY_STATUS.DA_XU_LY,
  };
  let othersPayloadByTab = {
    [appConst.tabStatus.tabWaiting]: {lanThu: times?.code},
    [appConst.tabStatus.tabPreProcessed]: {
      ntTo: convertFromToDate(ntTo).toDate,
      ntFrom: convertFromToDate(ntFrom).fromDate,
      isSortedByDonViThucHien: Boolean(department?.id),
    },
    [appConst.tabStatus.tabProcessed]: {
      ntTo: convertFromToDate(ntTo).toDate,
      ntFrom: convertFromToDate(ntFrom).fromDate,
      lanThu: times?.code,
    },
  }
  let newDxFrom = moment(dxFrom).startOf("month");
  let newDxTo = moment(dxTo).endOf("month");
  let amChuKyIds = !isTabWaiting ? selectedList?.map(x => x.id) : []

  return {
    ...searchObject,
    dxTo: convertFromToDate(newDxTo).toDate,
    dxFrom: convertFromToDate(newDxFrom).fromDate,
    chinhthucId: commonObject?.id,
    donViThucHienId: supplier?.id,
    contractId: contract?.contractId,
    khoaPhongSuDungId: department?.id,
    amChuKyIds: amChuKyIds?.join(","),
    isCkNotHasContract: isNtWithoutContract,
    chuKyStatuses: isExcel ? chuKyStatusesByTab[tabValue] : chuKyStatusesByTab[tabValue]?.join(","),
    nghiemThuStatus: nghiemThuStatusByTab[tabValue],
    status: statusByTab[tabValue],
    ...othersPayloadByTab[tabValue],
  }
}

export const convertPayloadMaintainProposal = (state) => ({
  ...state?.searchObject,
  dxTo: convertFromToDate(state?.dxTo).toDate,
  dxFrom: convertFromToDate(state?.dxFrom).fromDate,
  status: state?.status ?? null,
  dxPhongBanId: state?.department?.id,
  tnPhongBanId: state?.tnPhongBan?.id,
})

export const convertPayloadAcceptance = (state) => ({
  ...state?.searchObject,
  lanThu: state?.times?.code,
  donViThucHienId: state?.supplier?.id,
  contractId: state?.contract?.contractId,
  ngayKyBienBanTo: convertFromToDate(state?.dxTo).toDate,
  ngayKyBienBanFrom: convertFromToDate(state?.dxFrom).fromDate,
  trangThai: state?.chuKyStatuses?.code || null,
})
