import React, { useContext, useEffect, useRef, useState } from "react";
import {
  AppBar,
  Button,
  ButtonGroup,
  ClickAwayListener,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
  IconButton,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Tab,
  Tabs,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  createAndUpdateAsset,
  exportToExcelMaintainPlaning,
  exportToExcelMaintainPlaningSignedContract,
  exportToExcelMaintainPlaningSignedNoContract, exportToExcelNewOrApproval,
  exportToExcelPricingHasNotFinalizedYet,
  getByAssetKetHoach,
  getDocumentById,
  searchByPageAssetKeHoach,
  searchByPageDeXuatTaiSan,
  searchByPageHinhThucNew,
} from "./MaintainPlaningService";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import HeaderPlanDialog from "./Components/HeaderPlanDialog";
import ContentLeftDialog from "./Components/ContentLeftDialog";
import ContentRightDialog from "./Components/ContentRightDialog";
import AddAssetDialog from "./Components/AddAssetDialog";
import {
	appConst,
	formatDate,
	LIST_ORGANIZATION,
	listMaintainPlaningStatus,
	MAINTAIN_PLANING_STATUS, PRINT_TEMPLATE_MODEL,
	variable,
} from "../../appConst";
import moment from "moment";
import MaterialTable, { MTableToolbar } from "material-table";
import VoucherFilePopup from "./Dialog/VoucherFilePopup";
import {
  a11yProps,
	convertFromToDate,
  functionExportToExcel,
  getTheHighestRole, getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  PaperComponent,
  TabPanel,
} from "app/appFunction";
import ContractDialog from "../Contract/ContractDialog";
import SupplierDialog from "../Supplier/SupplierDialog";
import {getTemplateByModel} from "../../appServices";

toast.configure({
  autoClose: 4000,
  draggable: false,
  limit: 3,
});

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "sticky",
    zIndex: theme.zIndex.drawer + 1,
    top: 0,
    margin: 0,
    width: "100.6%",
    boxShadow: "none",
  },
  content: {
    marginTop: theme.mixins.toolbar.minHeight,
  },
}));

const MaintainPlaningDialog = (props) => {
  const { setPageLoading, currentOrg, } = useContext(AppContext);
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  let { open, amDexuatIds, asset, handleClose, isView, item } = props;
  const { currentUser, departmentUser, organization } = getUserInformation();
  const roles = getTheHighestRole();

  const [openTaiSanDialog, setOpenTaiSanDialog] = useState(false);
  const [shouldOpenSubDialog, setShouldOpenSubDialog] = useState({
    openContractDialog: false,
    supplierDialog: false,
  });
  const [state, setState] = useState({ // thông tin ts được chọn
    ckSoluong: "",
    amChukyDtos: [],
    donViThucHienId: null,
    donViThucHienText: null,
    isHinhThuc: true,
    donGia: "",
    thanhTien: "",
  });
  const [stateHeader, setStateHeader] = useState({
    khTen: "",
    khNam: "",
    khGhiChu: "",
    khTrangThai: "",
    status: null,
    khNgayDuyet: null,
    khTongtien: 0,
    khPhongBan: amDexuatIds ? item?.khPhongBan : departmentUser,
    khPhongBanId: amDexuatIds ? item?.khPhongBanId : departmentUser?.id,
    khPhongBanText: amDexuatIds ? item?.khPhongBanText : departmentUser?.text,
  });
  const [tabValue, setTabValue] = useState(0);
  const [maintainPlanings, setMaintainPlanings] = useState([]);
  const [tempMaintainPlanings, setTempMaintainPlanings] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [arrayDate, setArrayDate] = useState([]);
  const [filterState, setFilterState] = useState({
    commonObject: null,
    useDepartment: null,
    supplier: null,
    keyword: "",
  });
  const [isKetThuc, setIsKetThuc] = useState(false);
  const [assetFile, setAssetFile] = useState({
    item: {},
    shouldOpenPopupAssetFile: false,
    documents: null,
    documentType: appConst.documentType.ASSET_DOCUMENT_MAINTAIN,
    amAttachmentIds: null,
    isEditAssetDocument: false,
  });
  const [listData, setListData] = useState({
    listHinhThuc: [],
    selectedList: [],
  });
  const [openExcel, setOpenExcel] = useState(false);
	const [listPrintOptions, setListPrintOptions] = useState([]);
	const anchorRef = useRef();
  const form = useRef();
	const isBv199 = currentOrg?.code === LIST_ORGANIZATION.PRODUCT_BV199.code
		|| currentOrg?.code === LIST_ORGANIZATION.BV199.code;
  const khStatus = {
    isMoiTao: stateHeader?.status?.indexOrder === MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder
      || !stateHeader?.status?.indexOrder,
    isDaDuyet: stateHeader?.status?.indexOrder === MAINTAIN_PLANING_STATUS.DA_DUYET.indexOrder,
    isDaBaoGia: stateHeader?.status?.indexOrder === MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder,
    isDaKetThuc: stateHeader?.status?.indexOrder === MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder,
  }
  const optionsExceldisplay = {
    [appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.TAT_CA_NEW_TEMPLATE.code]: true,
    [appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.DA_CO_HOP_DONG.code]: true,
    [appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.KHONG_CO_HOP_DONG.code]: true,
		[appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.CHUA_DUOC_CHOT_GIA_HOP_DONG.code]: !khStatus.isDaKetThuc,
  }

  useEffect(() => {
    let status = listMaintainPlaningStatus.find(item => item.indexOrder === stateHeader?.khTrangThai)
    let isKetThuc = stateHeader?.khTrangThai === MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder
    setStateHeader({
      ...stateHeader,
      status,
    })
    setIsKetThuc(isKetThuc);
  }, [stateHeader?.khTrangThai]);

  useEffect(() => {
    if (asset?.id) {
      hanleGetAssetKeHoach(); // detail phiếu
      handleGetListAssetKeHoach(); // danh sách TS
    }
    else {
      if (amDexuatIds?.length > 0) {
        updatePageData();
      }
      setStateHeader(prevState => ({
        ...prevState,
        khPhongBan: amDexuatIds?.length > 0 ? item?.khPhongBan : departmentUser,
        khPhongBanId: amDexuatIds?.length > 0 ? item?.khPhongBanId : departmentUser?.id,
        khPhongBanText: amDexuatIds?.length > 0 ? item?.khPhongBanText : departmentUser?.name,
        tuNam: new Date().getFullYear(),
        denNam: new Date().getFullYear(),
        status: MAINTAIN_PLANING_STATUS.MOI_TAO,
      }))
    }
    hanleGetListImplementation();
		handleGetTemplatesByModel();
  }, [asset?.id]);

  useEffect(() => {
    if (maintainPlanings.length > 0) {
      let totalCost = maintainPlanings?.reduce(
        (sum, item) => sum + Number(item.thanhTien || 0), 0
      );
      let newData = listData?.selectedList?.map(x => {
        let existItem = maintainPlanings?.find(i => i.tsId === x.tsId);
        if (existItem) {
          return {
            ...existItem,
          }
        } else {
          return x;
        }
      })
      setListData({
        ...listData,
        selectedList: newData,
      });
      setStateHeader({ ...stateHeader, khTongtien: totalCost });
    }
  }, [maintainPlanings]);

  useEffect(() => {
    let newArray = maintainPlanings?.filter(ts => (
      !filterState?.commonObject?.id
      || ts.chinhthucId === filterState.commonObject.id
    ) && (
        !filterState?.supplier?.id
        || ts.donViThucHienId === filterState?.supplier?.id
      ) && (
        !filterState?.useDepartment?.id
        || ts.khoaPhongSuDungId === filterState?.useDepartment?.id
      ) && (
        !filterState?.keyword
        || ts.tsTen?.toLowerCase().includes(filterState?.keyword?.toString()?.toLowerCase())
        || ts.tsMa?.toLowerCase().includes(filterState?.keyword?.toString()?.toLowerCase())
      ))

    if (filterState?.supplier || filterState?.useDepartment || filterState?.commonObject || filterState?.keyword) {
      setTempMaintainPlanings(newArray);
      setState({
        ...newArray?.[0],
      })
    } else {
      setTempMaintainPlanings(maintainPlanings);
    }
  }, [maintainPlanings, filterState]);

  useEffect(() => {
    setIsEdit(false);
    setState({
      ...maintainPlanings?.[0],
    })
  }, [filterState]);

  useEffect(() => {
    let dataObj = {};
    const totalCost = arrayDate?.reduce((sum, item) => sum + Number(item?.ckDonGia || 0), 0);

    dataObj = {
      ...state,
      amChukyDtos: arrayDate?.map((item, index) => ({ ...item, lanThu: index + 1 })),
    };

    if (dataObj) {
      const dataArray = maintainPlanings.map((item) => {
        if (item?.tsId === state?.tsId) {
          item.amChukyDtos = arrayDate?.map((item, index) => ({ ...item, lanThu: index + 1 }));
          item.thanhTien = totalCost;
        }
        return item
      });

      setMaintainPlanings(dataArray);
      setState({ ...dataObj, thanhTien: totalCost, })
    }
  }, [arrayDate]);

  const hanleGetAssetKeHoach = () => {
    getByAssetKetHoach(asset?.id).then((res) => {
      const { data, code } = res.data;
      if (code === appConst.CODE.SUCCESS && data) {
        data.khPhongBan = {
          id: data.khPhongBanId,
          name: data.khPhongBanText,
        };
        setStateHeader(data);
        setAssetFile({
          ...assetFile,
          item: data?.amAttachmentDtoSet,
          documents: data?.amAttachmentDtoSet,
          amAttachmentIds: data?.amAttachmentIds,
        })
      }
      else {
        handleThrowResponseMessage(res);
      }
    })
      .catch((err) => {
        toast.error(t("general.error"))
      });
  };

  const handleGetListAssetKeHoach = async () => {
    setPageLoading(true);
    let searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 9999;
    const sendData = Object.assign({}, searchObject, { dxId: asset?.id });
    await searchByPageAssetKeHoach(sendData).then(({ data }) => {
      let newArray = data?.data?.content?.map(ts => ({
        ...ts,
        contract: ts.contractId
          ? {
            contractId: ts.contractId,
            contractName: ts.contractName,
            contractCode: ts.contractCode,
          }
          : null,
        donViThucHien: ts.donViThucHienId
          ? {
            id: ts.donViThucHienId,
            name: ts.donViThucHienText,
          }
          : null,
      }))
      newArray?.sort((a, b) => {
        const phongBanA = a.khoaPhongSuDungText || '';
        const phongBanB = b.khoaPhongSuDungText || '';

        if (phongBanA === phongBanB) {
          return a.tsTen.localeCompare(b.tsTen);
        }

        if (!phongBanA) return -1;
        if (!phongBanB) return 1;

        return phongBanA.localeCompare(phongBanB);
      });
      setMaintainPlanings(newArray);
      setState({ ...newArray[0] } ?? {});
      setArrayDate(newArray[0]?.amChukyDtos ?? []);
      setPageLoading(false);
    }).catch((err) => {
      setPageLoading(false);
      toast.error(t("general.error"))
    })
  };

  const updatePageData = async () => {
    setPageLoading(true);
    const newData = [];
    if (amDexuatIds?.length > 0) {
      amDexuatIds.forEach((item) => newData.push(item.id));
    }
    const sendData = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      amDexuatIds: newData,
    };
    try {
      const { data } = await searchByPageDeXuatTaiSan(sendData);
      if (data?.data?.content) {
        setMaintainPlanings(data.data.content);
      }
    } catch (error) {
      // Handle error if needed
      toast.error(t("general.error"))
      setPageLoading(false)
    } finally {
      setPageLoading(false);
    }
  };

  const handleChangeTabValue = (event, value) => {
    setTabValue(value);
  };

  const handleChange = (event, source, dataItem) => {
    event?.persist?.();
    if (
      source === variable.listInputName.implementingUnit
      || source === variable.listInputName.total
      || source === variable.listInputName.work
      || source === variable.listInputName.note
    ) {
      const dataArray = maintainPlanings.map((item) => {
        if (item?.tsId === dataItem?.tsId) {
          return { ...item, [event.target.name]: event.target.value };
        }
        return item;
      });
      setMaintainPlanings(dataArray);
      setState((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.value,
      }));
    }
    if (source === variable.listInputName.quantity) {
      const valueNumber = Number(event?.target?.value) ?? 0;
      const chuKyCount = dataItem?.amChukyDtos?.filter(
        item => item.ckTrangthai === appConst.CHU_KY_STATUS.DA_XU_LY
      )?.length

      if (valueNumber >= chuKyCount) { //chặn nhập dữ liệu < số chu kỳ đã xử lý
        const dataArray = maintainPlanings.map((item) => {
          if (item?.tsId === dataItem?.tsId) {
            return {
              ...item,
              [event.target.name]: +event.target.value > 365
                ? 365
                : +event.target.value,
            };
          }
          return item;
        });
        setMaintainPlanings(dataArray);
        setState((prevState) => ({
          ...prevState,
          [event?.target?.name]: +event?.target?.value > 365
            ? 365
            : +event?.target?.value,
        }));
      }
    } else if (source === variable.listInputName.keySearch) {
      setState(prevState => ({ ...prevState, [source]: event?.target?.value }))
    }
  };

  const handleChageFormatNumber = (event, source, dataItem) => {
    const thanhTien = event.target.value * (dataItem?.ckSoluong > 0 ? dataItem?.ckSoluong : 1);

    if (event.target.name === variable.listInputName.unitPrice) {
      const dataArray = maintainPlanings.map((item) => {
        if (item?.tsId === dataItem?.tsId) {
          return {
            ...item,
            [event.target.name]: event.target.value,
            thanhTien: thanhTien,
          };
        }
        return item;
      });
      setMaintainPlanings(dataArray);
    }
    setState((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
      thanhTien,
    }));
  };

  const handleChangeHeader = (value, name, source) => {
    let newData = {
      ...stateHeader,
    }
    if (source) {
      if (
        value?.indexOrder !== MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder
        && maintainPlanings?.length === 0
      ) {
        toast.warning(t("MaintainPlaning.messages.emptyAsset"));
        return;
      } else if (
        !stateHeader?.khNgayDuyet
        && value?.indexOrder === MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder
      ) {
        toast.warning(t("MaintainPlaning.messages.emptyKhNgayDuyet"));
        return;
      } else if (value?.indexOrder < stateHeader?.khTrangThai) {
        toast.warning(t("MaintainPlaning.messages.selectStatusError"));
        return;
      } else if (value?.indexOrder === MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder) {
        let check = maintainPlanings?.some(item => !item?.chinhthucId)
        if (check) {
          toast.warning(t("MaintainPlaning.messages.emptyHinhThucBT"));
          return;
        }
      } else if (!value && stateHeader?.khTrangThai) {
        return;
      }

      setState({
        ...state,
        ngayKyHopDong: null,
      })
    }
    if (value?.indexOrder === MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder) {
      newData.khNgayDuyet = null;
    } else if (
      value?.indexOrder !== MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder
      && value?.indexOrder !== MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder
    ) {
      let newArray = maintainPlanings?.map(as => {
        as.ngayKyHopDong = null;
        return as;
      })
      setMaintainPlanings(newArray);
    }
    if (name === "khPhongBan") {
      setMaintainPlanings([]);
    }
    setStateHeader({
      ...newData,
      [name]: value,
    });
  };

  const handleChangeStatus = (value, name) => {
    let newData = {}
    const isMoiTao = value?.indexOrder === MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder;
    const isDaDuyet = value?.indexOrder === MAINTAIN_PLANING_STATUS.DA_DUYET.indexOrder;
    const isDaChotBaoGia = value?.indexOrder === MAINTAIN_PLANING_STATUS.DA_BAO_GIA.indexOrder;
    const isKetThuc = value?.indexOrder === MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder;


    if (!isMoiTao && maintainPlanings?.length === 0) {
      toast.warning(t("MaintainPlaning.messages.emptyAsset"));
      return;
    } else if (value?.indexOrder < stateHeader?.khTrangThai) {
      toast.warning(t("MaintainPlaning.messages.selectStatusError"));
      return;
    } else if (!value && stateHeader?.khTrangThai) {
      return;
    } else if (isMoiTao) {
      newData.khNgayDuyet = null;
    } else if (isDaDuyet) {
      let invalidAssets = maintainPlanings?.filter(ts => !ts.ckSoluong || !ts.congViec || !ts.chinhthucId);
      if (invalidAssets?.length > 0) {
        toast.warning(`Điền thông tin yêu cầu (hình thức, số lượng chu kỳ, công việc) cho các tài sản: ${invalidAssets.map(ts => ts.tsMa).join(", ")
          }`)
        return;
      }
    } else if (isMoiTao || isDaDuyet) {
      let newArray = maintainPlanings?.map(as => {
        as.ngayKyHopDong = null;
        return as;
      })
      setMaintainPlanings(newArray);
    } else if (isDaChotBaoGia) {
      if (!stateHeader?.khNgayDuyet) {
        toast.warning(t("MaintainPlaning.messages.emptyKhNgayDuyet"));
        return;
      } else {
        let check = maintainPlanings?.some(item => !item?.chinhthucId)
        if (check) {
          toast.warning(t("MaintainPlaning.messages.emptyHinhThucBT"));
          return;
        }
      }
    } else if (value?.indexOrder < stateHeader?.khTrangThai) {
      toast.warning(t("MaintainPlaning.messages.selectStatusError"));
      return;
    }

    setStateHeader(prevState => ({
      ...prevState,
      ...newData,
      [name]: value,
    }));
  }

  const handleFormSubmit = async (e) => {
    if (e.target.id !== "maintain-dialog-submit-form") return;
    const newData = [];
    if (amDexuatIds?.length > 0) {
      amDexuatIds.forEach((item) => newData.push(item.id));
    }
    const sendData = {
      // ...stateHeader,
      amAttachmentDtoSet: [],
      amAttachmentIds: assetFile?.amAttachmentIds ?? [],
      amTaiSanDtos: maintainPlanings?.map(ts => {
        ts.ngayKyHopDong = ts.ngayKyHopDong
          ? moment(ts.ngayKyHopDong).format(formatDate)
          : null;
        ts?.amChukyDtos?.map((ck, i) => {
          ck.ckDate = ck.ckDate
            ? moment(ck.ckDate).format('YYYY-MM-DDTHH:mm:ss')
            : null;
          ck.ckDonGia = Number(ck.ckDonGia);
          ck.lanThu = i + 1;
          return ck
        })
        ts.donViThucHienId = ts?.donViThucHien?.id;
        ts.donViThucHienText = ts?.donViThucHien?.name;
        return ts
      }),
      id: stateHeader?.id,
      khGhiChu: stateHeader?.khGhiChu,
      khMa: stateHeader?.khMa,
      tuNam: stateHeader?.tuNam,
      denNam: stateHeader?.denNam,
      khNgay: stateHeader?.khNgay,
      khNgayDuyet: stateHeader?.khNgayDuyet,
      khPhongBanId: stateHeader?.khPhongBan?.id,
      khPhongBanText: stateHeader?.khPhongBan?.name,
      khTen: stateHeader?.khTen,
      khThang: stateHeader?.khThang,
      khTongtien: stateHeader?.khTongtien,
      khTrangThai: stateHeader?.status?.indexOrder,
      phieuDxIds: newData,
    };
    if (validateSubmit()) {
      setPageLoading(true);
      try {
        const result = await createAndUpdateAsset(sendData)
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          let value = result?.data?.data;
          let newArray = value?.amTaiSanDtos?.map(ts => ({
            ...ts,
            contract: ts.contractId
              ? {
                contractId: ts.contractId,
                contractName: ts.contractName,
                contractCode: ts.contractCode,
              }
              : null,
            donViThucHien: ts?.donViThucHienId
              ? {
                id: ts.donViThucHienId,
                name: ts.donViThucHienText,
              }
              : null,
          })) || [];

          if (stateHeader?.id) {
            toast.success(t("general.updateSuccess"))
            setIsKetThuc(khStatus.isDaKetThuc);
          }
          else {
            toast.success(t("general.addSuccess"))
          }
          setMaintainPlanings([...newArray]);
          setStateHeader({
            ...stateHeader,
            id: value?.id,
            khMa: value?.khMa,
          })

            return true;
        } else {
          handleThrowResponseMessage(result);
        }
      } catch (error) {
        toast.error(t("general.error"));
      } finally {
        setPageLoading(false);
      }
    }
  };

  const handleChangeDate = (value) => {
    const itemData = {
      ...state,
      amChukyDtos: value,
    };
    let dataObj = {};
    let item = maintainPlanings.find((item) => item?.tsId === state?.tsId);
    dataObj = { ...item, ...itemData };
    if (dataObj) {
      const dataArray = maintainPlanings.map((item) =>
        item?.tsId === state?.tsId ? dataObj : item
      );
      setMaintainPlanings(dataArray);
    }
  };

  const handleRowClick = (rowData) => {
    if (rowData !== null) {
      let amChukyDtos = rowData?.amChukyDtos ? rowData?.amChukyDtos : [];
      setArrayDate(amChukyDtos);
      maintainPlanings.map((item) => {
        if (item?.tsId === rowData?.tsId) {
          let isTuBaoTri = appConst.listHinhThuc.TU_BAO_TRI === item?.chinhthucCode
          item.isHinhThuc = isTuBaoTri
          if (isTuBaoTri) {
            item.donViThucHienId = organization?.id;
            item.donViThucHienText = organization?.name;
          }
          setState({ ...item, amChukyDtos });
        }
      });
      setIsEdit(true);
    }
  };

  const handleSelect = (value, source) => {
    const newItem = item => ({
      ...item,
      [source]: value,
      contract: null,
      contractId: "",
      contractName: "",
      contractCode: "",
    })
    
    if(source === "ngayKyHopDong"  && value === null) {
      setArrayDate([]);
    }

    const dataArray = maintainPlanings.map((item) => {
      return item?.tsId === state?.tsId ? newItem(item) : item
    }
    );
    setState(newItem(state));
    setMaintainPlanings(dataArray);
  };

  const handleSelectionChange = (selectedRows, rowData) => {
    let newSelectedData = [];
    let newData = maintainPlanings || [];
    let hasItemIsFinished = selectedRows?.some(x => x.isFinished);
    if (hasItemIsFinished) {
      toast.warning("Không thể chọn tài sản đã hoàn thành nghiệm thu hết các chu kỳ");

    }
    newData = maintainPlanings?.map(item => {
      let isExistItem = selectedRows?.some(x => x.tsId === item.tsId);
      if (isExistItem && !item.isFinished) {
        newSelectedData.push(item);
      }
      if (item.isFinished) {
        return {
          ...item,
          tableData: {
            ...item.tableData,
            checked: false,
          }
        };
      } else {
        return item;
      }
    })

    handleSetListData(newSelectedData, "selectedList");
    setMaintainPlanings([...newData]);
  };

  const handleAddAsset = (rowData = []) => {
    const newData = [];
    rowData.map((item, index) => {
      newData.push(
        item?.tsId
          ? item
          : {
            tsId: item?.id,
            tsTen: item?.name,
            tsMa: item?.code,
            tsModel: item?.model,
            tsSerialNo: item?.serialNumber,
            tsTrangThaiTen: item?.statusName,
            donGia: null,
            thanhTien: null,
            khoaPhongSuDungId: item?.useDepartmentId,
            khoaPhongSuDungMa: item?.useDepartmentCode,
            khoaPhongSuDungText: item?.useDepartmentName,
            congViec: null,
            ghiChu: null,
            ckSoluong: null,
            tsTrangthai: null,
            tsNgayDuKien: null,
            amChukyDtos: null,
            cmauId: null,
            cmauText: null,
            donViThucHienId: null,
            donViThucHienText: null,
            chinhthucId: null,
            chinhthucText: null,
            chinhthucCode: null,
            tsNamsx: item?.yearOfManufacture,
            tsNamsd: item?.yearPutIntoUse,
          }
      )
    });
    if (newData?.length === 0) {
      toast.warning(t("general.emptyDataSelected"))
    }
    newData?.sort((a, b) => {
      const phongBanA = a.khoaPhongSuDungText || '';
      const phongBanB = b.khoaPhongSuDungText || '';

      if (phongBanA === phongBanB) {
        return a.tsTen.localeCompare(b.tsTen);
      }

      if (!phongBanA) return -1;
      if (!phongBanB) return 1;

      return phongBanA.localeCompare(phongBanB);
    });
    setMaintainPlanings([...newData]);
    setState(newData[0])
    setOpenTaiSanDialog(false);
  };

  const handleRadioButtonSelect = (value, itemData) => {
    if (value != null) {
      const newData = {
        chinhthucId: value?.id || "",
        chinhthucText: value?.name || "",
        chinhthucCode: value?.code || "",
      };
      if (appConst.listHinhThuc.TU_BAO_TRI === value?.code) {
        setState({
          ...state,
          isHinhThuc: true,
          donViThucHien: organization?.org,
          donViThucHienId: organization?.id,
          donViThucHienText: organization?.name,
          ...newData,
        });
        const dataArray = maintainPlanings.map((item) => {
          if (item?.tsId === itemData?.tsId) {
            return {
              ...item,
              ...newData,
              donViThucHien: organization?.org,
              donViThucHienId: organization?.id,
              donViThucHienText: organization?.name,
            };
          }
          return item;
        });
        setMaintainPlanings(dataArray);
      } else {
        setState({
          ...state,
          isHinhThuc: false,
          donViThucHien: null,
          donViThucHienId: "",
          donViThucHienText: "",
          ...newData,
        });
        const dataArray = maintainPlanings.map((item) => {
          if (item?.tsId === itemData?.tsId) {
            return {
              ...item,
              ...newData,
              donViThucHien: null,
              donViThucHienId: "",
              donViThucHienText: "",
            };
          }
          return item;
        });

        setMaintainPlanings(dataArray);
      }
    }
  };

  const removeAssetInlist = (asset) => {
    maintainPlanings.splice(maintainPlanings.indexOf(asset), 1);
    setMaintainPlanings(maintainPlanings);
    toast.dismiss();
    toast.info(t("asset_liquidate.deleteFromList"));
  }

  const validateSubmit = () => {
    let check = true;

    if (validateFormDatePicker()) return;

    if (maintainPlanings.length <= 0) {
      toast.warning(t("MaintainPlaning.emptyAssetList"))
      check = false;
    }
    else {
      for (let i = 0; i < maintainPlanings?.length; i++) {
        let as = maintainPlanings[i];
        if (!khStatus?.isMoiTao) {
          if (!as?.chinhthucId) {
            check = false;
            toast.warning(t("MaintainPlaning.messages.emptyHinhThucBT"));
            break;
          }
          // else if (as?.ckSoluong <= 0 || as?.amChukyDtos?.length <= 0) {
          //   check = false;
          //   toast.warning(t("MaintainPlaning.messages.ckForAsset") + as?.tsTen + " (" + as?.tsMa + ")");
          //   break;
          // }
          else if (!as?.congViec) {
            check = false;
            toast.warning(t("MaintainPlaning.messages.emptyCongViec"))
            break;
          }
          else if (
            as?.ngayKyHopDong
            && !as.donViThucHienId
          ) {
            check = false;
            toast.warning(t("MaintainPlaning.messages.emptyUnitWhenSignContract") + as?.tsTen + " (" + as?.tsMa + ")")
            break;
          }
          else if (as?.ngayKyHopDong) {
            let ngayKyHopDong = convertFromToDate(as?.ngayKyHopDong).fromDate;
            let khNgayDuyet = convertFromToDate(stateHeader?.khNgayDuyet).fromDate;
            if (as?.amChukyDtos?.length < as?.ckSoluong) {
              check = false;
              toast.warning(t("MaintainPlaning.messages.ckForAsset") + as?.tsTen + " (" + as?.tsMa + ")");
              break;
            } else if (moment(ngayKyHopDong).isBefore(khNgayDuyet)) {
              check = false;
              toast.warning(as?.tsTen + " (" + as?.tsMa + ")" + " có ngày ký hợp đồng nhỏ hơn ngày duyệt");
              break;
            }
            as?.amChukyDtos?.map((ck, i) => {
              if (!ck.ckDate) {
                check = false;
                toast.warning(t("MaintainPlaning.messages.emptyChuKy"));
              }
              else if (new Date(ck.ckDate).getFullYear() < +stateHeader?.tuNam) {
                check = false;
                toast.warning("TS " + as?.tsMa + " " + t("MaintainPlaning.messages.minCkDateCompareWithPlan"));
              }
              else if (new Date(ck.ckDate).getFullYear() > +stateHeader?.denNam) {
                check = false;
                toast.warning("TS " + as?.tsMa + ": " + t("MaintainPlaning.messages.maxCkDateCompareWithPlan"));
              }
            })
            if (check === false) break;
          }
          else if (as?.chinhthucCode === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code && khStatus.isDaBaoGia) {
            let emptyCk = as?.amChukyDtos?.some(x => !x.ckDate) || as.ckSoluong !== as?.amChukyDtos?.length;
            if (emptyCk) {
              check = !emptyCk;
              toast.warning(t("MaintainPlaning.messages.ckForAsset") + as?.tsTen + " (" + as?.tsMa + ")");
              break;
            }
          }
        }
      }
    }
    return check;
  }

  const validateFormDatePicker = () => {
    return document.getElementById("date-picker-dialog-helper-text")?.innerHTML?.length > 0;
  }

  let columnsVoucherFile = [
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 100,
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },

      render: rowData =>
        // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <IconButton size="small" onClick={() => handleRowDataCellEditAssetFile(rowData)}>
            <Icon fontSize="small" color="primary">edit</Icon>
          </IconButton>
          <IconButton size="small" onClick={() => handleRowDataCellDeleteAssetFile(rowData?.id)}>
            <Icon fontSize="small" color="error">delete</Icon>
          </IconButton>
        </div>
      // )
    },
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "note",
      align: "left",
      minWidth: "250px",
    },
  ];

  const handleAddAssetDocumentItem = () => {
    // getNewCodeDocument(assetFile?.documentType)
    //   .then(({ data }) => {
    //     if (appConst.CODE.SUCCESS === data?.code) {
    setAssetFile({
      ...assetFile,
      item: {},
      shouldOpenPopupAssetFile: true,
    });
    //     return
    //   }
    //   toast.warning(data?.message)
    // }
    // )
    // .catch(() => {
    //   toast.warning(t("general.error_reload_page"));
    // });
  };

  const handleAssetFilePopupClose = () => {
    setAssetFile({ ...assetFile, shouldOpenPopupAssetFile: false, });
  };

  const getAssetDocument = (document) => {
    let amAttachmentIds = assetFile?.amAttachmentIds ? assetFile?.amAttachmentIds : [];
    let documents = assetFile?.documents ? assetFile?.documents : [];
    document && (document.description = document?.note)
    document && amAttachmentIds.push(document?.id);
    document && documents.push(document);
    setAssetFile({ ...assetFile, amAttachmentIds, documents });
  };

  const handleUpdateAssetDocument = (document) => {
    let documents = assetFile?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    document && (document.description = document?.note)
    documents[indexDocument] = document;
    setAssetFile({ ...assetFile, documents });
  };

  const handleRowDataCellEditAssetFile = (rowData) => {
    getDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;
      fileDescriptionIds.push(data?.data?.file?.id)

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = assetFile?.documentType;
      document.isEditAssetDocument = true;

      setAssetFile({
        ...assetFile,
        item: document,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true,
      });
    });
  };

  const handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, amAttachmentIds } = assetFile;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = amAttachmentIds?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    amAttachmentIds.splice(indexId, 1);
    setAssetFile({ ...assetFile, amAttachmentIds, documents });
  };

  const exportToExcel = async (option) => {
    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.TSCD,
        khPhongBanId: stateHeader.khPhongBanId,
        khTrangThai: stateHeader?.status?.indexOrder,
        khIds: [stateHeader?.id],
      };
      let template = listPrintOptions?.find(x => x.id === option ?.id)

      if (filterState?.commonObject) {
        searchObject.chinhthucId = filterState.commonObject?.id;
      }
      if (filterState?.useDepartment) {
        searchObject.khoaPhongSuDungId = filterState.useDepartment?.id;
      }
      if (filterState?.supplier) {
        searchObject.donViThucHienId = filterState.supplier?.id;
      }
      searchObject.templateId = option?.id;
      searchObject.exportUrl = template?.exportUrl || option?.exportUrl

      let apiFunc;
      let fileName = stateHeader?.khMa ? stateHeader?.khMa + " - " : ""
      switch (option?.code) {
        case appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.TAT_CA_NEW_TEMPLATE.code:
          apiFunc = exportToExcelMaintainPlaning;
          break;
        case appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.DA_CO_HOP_DONG.code:
          apiFunc = exportToExcelMaintainPlaningSignedContract;
          break;
        case appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.KHONG_CO_HOP_DONG.code:
          apiFunc = exportToExcelMaintainPlaningSignedNoContract;
          break;
        case appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.CHUA_DUOC_CHOT_GIA_HOP_DONG.code:
          apiFunc = exportToExcelPricingHasNotFinalizedYet;
          break;
        case appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.TAT_CA_OLD_TEMPLATE.code:
          apiFunc = exportToExcelNewOrApproval;
          break;
        default:
          apiFunc = exportToExcelPricingHasNotFinalizedYet;
          break;
      }
      fileName = (template?.name || option?.name);

      await functionExportToExcel(
        apiFunc,
        searchObject,
        fileName,
      )
    }
    catch (error) {
			console.error(error);
      toast.warning(t("general.failExport"));
    }
    finally {
      setPageLoading(false);
    }
  };
	
	const handleGetTemplatesByModel = async () => {
		try {
			setPageLoading(true);
			let searchObject = {
				orgId: organization?.org?.id,
				model: PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.MAINTAIN_PLANING.PLAN,
			}
			let res = await getTemplateByModel(searchObject);
			let {code, data} = res?.data;
			if (isSuccessfulResponse(code)) {
				setListPrintOptions([...data]);
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (e) {
			console.error(e)
		} finally {
			setPageLoading(false);
		}
	}
	
  const setFilterData = (value, name) => {
    setFilterState(prevState => ({
      ...prevState,
      [name]: value,
    }))
    setState({})
  };

  const handleExportToExcel = async (option) => {
    if (isKetThuc || isView || khStatus.isDaDuyet) {
      await exportToExcel(option);
      return;
    }
    
    try {
      const fakeEvent = {
        target: {id: "maintain-dialog-submit-form"}
      };
      handleFormSubmit(fakeEvent).then((isSuccess) => {
        if (isSuccess) {
          exportToExcel(option);
        }
      })
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
    }
  }

  const handleSelectContract = (value) => {
    if (variable.listInputName.New === value?.code) {
      setShouldOpenSubDialog(prevState => ({
        ...prevState,
        openContractDialog: true,
      }))
      return;
    }

    const dataArray = maintainPlanings.map((item) => {
      if (item?.tsId === state?.tsId) {
        if (khStatus.isDaBaoGia && value?.performedTimesNumber !== item.ckSoluong && value) {
          toast.warning("Số lần thực hiện không tương thích");
        } else {
          setArrayDate(item.amChukyDtos?.slice(0, value?.performedTimesNumber))
          setState({
            ...item,
            amChukyDtos: item.amChukyDtos?.slice(0, value?.performedTimesNumber),
            contract: value || null,
            contractId: value?.contractId || value?.id || null,
            contractName: value?.contractName || null,
            contractCode: value?.contractCode || null,
            ckSoluong: value?.performedTimesNumber || item?.ckSoluong,
          });
          return {
            ...item,
            amChukyDtos: item.amChukyDtos?.slice(0, value?.performedTimesNumber),
            contract: value,
            contractId: value?.contractId || value?.id || null,
            contractName: value?.contractName || null,
            contractCode: value?.contractCode || null,
            ckSoluong: value?.performedTimesNumber || item?.ckSoluong,
          };
        }
      }
      return item;
    });

    setMaintainPlanings(dataArray);
  };

  const handleSelectDonViThucHien = (value) => {
    if (variable.listInputName.New === value?.code) {
      setShouldOpenSubDialog(prevState => ({
        ...prevState,
        supplierDialog: true,
      }))
      return;
    }

    const newItem = item => ({
      ...item,
      donViThucHien: value,
      contract: null,
      contractId: "",
      contractCode: "",
      contractName: "",
    })
    const dataArray = maintainPlanings.map((item) =>
      item?.tsId === state?.tsId ? newItem(item) : item
    );

    setMaintainPlanings(dataArray);
    setState(newItem(state));
  };

  const hanleGetListImplementation = async () => {
    if (listData?.listHinhThuc?.length > 0) return;

    try {
      let res = await searchByPageHinhThucNew({
        code: appConst.listCommonObject.HINH_THUC_BAO_TRI.code
      })
      const { data, status } = res;
      if (isSuccessfulResponse(status) && data) {
        handleSetListData(data, "listHinhThuc");
      }
      else {
        toast.error(t("general.error"));
      }
    }
    catch (err) {
      toast.error(t("general.error"));
    }
  }

  const handleDialogClose = () => {
    setShouldOpenSubDialog({
      openContractDialog: false,
      supplierDialog: false,
    })
  }

  const handleToggleOpenExcel = async (e) => {
    if (khStatus.isMoiTao || khStatus.isDaDuyet) {
			let foundOption = listPrintOptions?.find(
				x => x.code === appConst.OBJECT_OPTIONS_EXCEL_MAINTAIN_PLANING.TAT_CA_OLD_TEMPLATE.code
			);
      await handleExportToExcel(foundOption);
			return;
    }
    setOpenExcel(!openExcel)
  };

  const handleMenuItemClick = (event, option) => {
    handleExportToExcel(option);
    setOpenExcel(false);
  };

  const handleSetListData = (value = [], name) => {
    setListData({
      ...listData,
      [name]: value,
    })
  };

  const handleCopyAssetInformation = () => {
    const { selectedList = [] } = listData;
    if (selectedList.length <= 0) {
      toast.warning(t("MaintainPlaning.messages.noAssetChosen"));
      return;
    }

    try {
      let clone = maintainPlanings.map((ts) => {
        const matchingItem = selectedList.find((selected) => ts.tsId === selected.tsId);
        const isInformationOwner = state?.tsId === ts.tsId; // item được lấy thông tin để copy
        const tuBaoTriCode = appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code;
        if (matchingItem && !ts?.isAccepted) {
          let item = {
            ...ts,
            tableData: {
              ...ts.tableData,
              checked: false,
            },
            amChukyDtos: state?.amChukyDtos?.map(ck => ({
              ...ck,
              id: isInformationOwner ? ck.id : null
            })),
            ghiChu: state?.ghiChu,
            ngayKyHopDong: state?.ngayKyHopDong,
          };
          if (khStatus?.isMoiTao) {
            item.congViec = state?.congViec;
            item.ckSoluong = state?.ckSoluong;
            item.chinhthucText = state?.chinhthucText;
            item.chinhthucCode = state?.chinhthucCode;
            item.chinhthucId = state?.chinhthucId;
            item.donViThucHien = state?.donViThucHien;
            item.contract = state?.contract;
            item.contractName = state?.contract?.contractName;
            item.contractCode = state?.contract?.contractCode;
            item.contractId = state?.contractId || state?.contract?.contractId;
          } else if (
            item.ngayKyHopDong === state.ngayKyHopDong
            || item.contract?.id === state.contract?.id
          ) {
            item.donViThucHien = state?.donViThucHien;
            item.congViec = state?.congViec;
            item.contract = state?.contract;
            item.contractName = state?.contract?.contractName;
            item.contractCode = state?.contract?.contractCode;
            item.contractId = state?.contractId || state?.contract?.contractId;
          }
          if (ts.ckSoluong !== state.ckSoluong) {
            item.amChukyDtos = item.amChukyDtos?.slice(0, ts?.ckSoluong || state?.ckSoluong);
          }
          return item;
        }
        return ts;
      });

      setMaintainPlanings([...clone]);
      setState({ ...clone[0] });
      setArrayDate(clone[0]?.amChukyDtos);
      setListData({
        ...listData,
        selectedList: [],
      })
      toast.info(t("MaintainPlaning.copySuccess"));
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
    }
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth="xl"
      fullWidth={true}
    >
      <DialogTitle id="draggable-dialog-title">
        {t("MaintainPlaning.dialog")}
      </DialogTitle>

      <ValidatorForm
        ref={form}
        id="maintain-dialog-submit-form"
        onSubmit={handleFormSubmit}
      >
        <DialogContent className="py-0 " style={{ maxHeight: 550 }}>
          <div className="mp-0">
            <AppBar
              className={classes.appBar}
              position="static"
              color="default"
            >
              <Tabs
                className="tabsStatus tabs-status-maintain"
                value={tabValue}
                onChange={handleChangeTabValue}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
                style={{ background: "white" }}
              >
                <Tab className="tab" label={t("Asset.tab.planInformation")} {...a11yProps(0)} />
                <Tab className="tab" label={t("Asset.tab.attachedFile")} {...a11yProps(1)} />
              </Tabs>
            </AppBar>

            <TabPanel value={tabValue} index={0} className="bg-white">
              <Grid container spacing={1} justifyContent="space-between">
                <HeaderPlanDialog
                  t={t}
                  roles={roles}
                  isView={isView}
                  khStatus={khStatus}
                  listData={listData}
                  itemData={stateHeader}
                  filterState={filterState}
                  onChange={handleChangeHeader}
                  handleChangeStatus={handleChangeStatus}
                  setFilterData={setFilterData}
                  setOpenTaiSanDialog={setOpenTaiSanDialog}
                  amDexuatIds={amDexuatIds}
                />

                <ContentLeftDialog
                  t={t}
                  isView={isView}
                  itemData={state}
                  onChange={handleChange}
                  removeAssetInlist={removeAssetInlist}
                  onRowClick={handleRowClick}
                  handleFormSubmit={() => { }}
                  maintainPlanings={maintainPlanings}
                  tempMaintainPlanings={tempMaintainPlanings}
                  onSelectionChange={handleSelectionChange}
                  khStatus={khStatus}
                  onAddAsset={handleCopyAssetInformation}
                />

                <ContentRightDialog
                  t={t}
                  isEdit={isEdit}
                  isView={isView}
                  itemData={state}
                  khStatus={khStatus}
                  arrayDate={arrayDate}
                  isKetThuc={isKetThuc}
                  onChange={handleChange}
                  onSelect={handleSelect}
                  setArrayDate={setArrayDate}
                  listData={listData}
                  onChangeDate={handleChangeDate}
                  stateHeader={stateHeader}
                  khNgayDuyet={stateHeader?.khNgayDuyet}
                  handleSelectContract={handleSelectContract}
                  onRadioButtonSelect={handleRadioButtonSelect}
                  handleSelectDonViThucHien={handleSelectDonViThucHien}
                />
              </Grid>
            </TabPanel>
            <TabPanel value={tabValue} index={1} className="bg-white">
              <Grid item md={12} sm={12} xs={12}>
                {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
                  <Button
                    size="small"
                    variant="contained"
                    color="primary"
                    onClick={handleAddAssetDocumentItem}
                  >
                    {t('AssetFile.addAssetFile')}
                  </Button>
                }

              </Grid>
              <Grid item md={12} sm={12} xs={12} className="mt-16">
                <MaterialTable
                  data={assetFile?.documents || []}
                  columns={columnsVoucherFile}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                    }
                  }}
                  options={{
                    draggable: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    padding: 'dense',
                    rowStyle: rowData => ({
                      backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    headerStyle: {
                      backgroundColor: '#358600',
                      color: '#fff',
                    },
                    maxBodyHeight: '285px',
                    minBodyHeight: '285px',
                  }}
                  components={{
                    Toolbar: props => (
                      <div className="w-100">
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </TabPanel>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              className="mr-12"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.close")}
            </Button>
            {stateHeader?.id && (
              <>
                <ButtonGroup
                  ref={anchorRef}
                  className="mr-16 align-bottom"
                  variant="contained"
                  aria-label="split button"
                >
                  <Button
                    aria-controls={openExcel ? "split-button-menu" : undefined}
                    aria-expanded={openExcel ? "true" : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    variant="contained"
                    color="primary"
                    onClick={(e) => handleToggleOpenExcel(e)}
                  >
                    {
                      isKetThuc || isView
                        ? t("general.exportToExcel")
                        : t("general.saveAndExportToExcel")
                    }
                  </Button>
                </ButtonGroup>

                <Popper
                  open={openExcel}
                  anchorEl={anchorRef.current}
                  role={undefined}
                  transition
                  disablePortal
                  style={{ zIndex: 999 }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={() => setOpenExcel(false)}>
                      <MenuList id="split-button-menu" autoFocusItem>
                        {listPrintOptions.map(
                          (option, index) => (optionsExceldisplay[option.code]) && (
                            <MenuItem
                              key={index}
                              onClick={(event) => handleMenuItemClick(event, option)}
                            >
                              {option.name}
                            </MenuItem>
                          )
                        )}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Popper>
              </>
            )}
            <Button
              variant="contained"
              disabled={isView || isKetThuc}
              className="mr-15"
              color="primary"
              type="submit"
              id="maintain-dialog-submit-button"
            >
              {t("general.save")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
      {openTaiSanDialog && (
        <AddAssetDialog
          open={openTaiSanDialog}
          amDexuatIds={amDexuatIds}
          maintainPlanings={
            maintainPlanings
              ? [...maintainPlanings]
              : []
          }
          handleClose={() => setOpenTaiSanDialog(false)}
          onAddAsset={handleAddAsset}
          managementDepartmentId={stateHeader?.khPhongBan?.id}
        />
      )}
      {assetFile?.shouldOpenPopupAssetFile && (
        <VoucherFilePopup
          open={assetFile?.shouldOpenPopupAssetFile}
          handleClose={handleAssetFilePopupClose}
          itemAssetDocument={assetFile?.item}
          getAssetDocument={getAssetDocument}
          handleUpdateAssetDocument={handleUpdateAssetDocument}
          documentType={assetFile?.documentType}
          item={assetFile.item}
          t={t}
          i18n={i18n}
        />)
      }
      {shouldOpenSubDialog?.openContractDialog && (
        <ContractDialog
          t={t} i18n={i18n}
          handleClose={handleDialogClose}
          open={shouldOpenSubDialog?.openContractDialog}
          handleOKEditClose={handleDialogClose}
          item={{
            contractName: state.keySearch,
            supplier: state?.donViThucHien,
            contractDate: state?.ngayKyHopDong,
          }}
          contractTypeCode={appConst.TYPES_CONTRACT.BTBD}
          ngayKyHopDong={state?.ngayKyHopDong}
          handleSelect={(value) => handleSelectContract(value, state)}
          isFromPlaningDialog={true}
        />
      )}
      {shouldOpenSubDialog?.supplierDialog &&
        <SupplierDialog
          t={t}
          i18n={i18n}
          item={{
            name: state?.keySearch,
          }}
          disabledType
          typeCodes={[appConst.TYPE_CODES.NCC_BDSC]}
          open={shouldOpenSubDialog.supplierDialog}
          handleSelect={handleSelectDonViThucHien}
          handleClose={handleDialogClose}
        />}
    </Dialog>
  );
};

export default MaintainPlaningDialog;
