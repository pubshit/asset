import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/maintain_planing" + ConstantList.URL_PREFIX;
const API_PATH_EXPORT_TO_EXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_COMMON_OBJECT = ConstantList.API_ENPOINT + "/api/commonobject";
const API_ENPOINT_ASSET_MAINTANE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;

export const updateMaintainPlaning = (maintainPlaning) => {
  return axios.put(API_PATH + "/update/" + maintainPlaning.id, maintainPlaning);
};

export const createMaintainPlaning = (maintainPlaning) => {
  return axios.post(API_PATH + "/create", maintainPlaning);
};

export const getOneById = (id) => {
  return axios.get(API_PATH + "/getOne/" + id);
};

export const deleteById = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};

export const deleteCheck = (id) => {
  return axios.delete(API_PATH + "/deleteCheck/" + id);
};

export const searchByPage = (searchObject) => {
  return axios.post(API_PATH + "/searchByPage", searchObject);
};

export const checkDuplicateCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkDuplicateCode";
  return axios.get(url, config);
};

export const exportToExcel = (searchObject, tabValue) => {
  let endUrl = '';
  switch (tabValue) {
    case appConst.tabStatus.tabMaintenance:
      endUrl = '/de-xuat-bao-tri';
      break;
    case appConst.tabStatus.tabProcessed:
      endUrl = '/da-bao-tri';
      break;
    case appConst.tabStatus.tabContractLiquidate:
      endUrl = '/am-contracts';
      break;
    default:
      endUrl = '/chu-ky-bao-tri';
      break;
  }
  return axios({
    method: "post",
    url: API_PATH_EXPORT_TO_EXCEL + endUrl,
    data: searchObject,
    responseType: "blob",
  });
};

export const exportToExcelNewOrApproval = (searchObject) => {
  let {exportUrl, ...rest} = searchObject;
  return axios({
    method: "post",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + exportUrl,
    data: rest,
    responseType: "blob",
  });
};

export const exportToExcelPricingHasNotFinalizedYet = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORT_TO_EXCEL + "/tai-san-bao-tri-chua-chot-bao-gia",
    data: searchObject,
    responseType: "blob",
  });
}

export const exportToExcelMaintainPlaning = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORT_TO_EXCEL + "/tai-san-bao-tri-da-chot-bao-gia",
    data: searchObject,
    responseType: "blob",
  });
}

export const exportToExcelMaintainPlaningSignedContract = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORT_TO_EXCEL + "/tai-san-bao-tri-da-ky-hop-dong",
    data: searchObject,
    responseType: "blob",
  });
};

export const exportToExcelMaintainPlaningSignedNoContract  = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORT_TO_EXCEL + "/tai-san-bao-tri-da-ky-khong-hop-dong",
    data: searchObject,
    responseType: "blob",
  });
};

export const searchByPageChoXuLy = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  return axios.get(
    API_ENPOINT_ASSET_MAINTANE + "am-chu-ky/search-by-page",
    config
  );
};

export const searchByPageDeXuat = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/search-by-page",
    searchObject
  );
};

export const searchByPageDeXuatTaiSan = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-ke-hoach/de-xuat/tai-san",
    searchObject
  );
};

// http://asset-management-dev.oceantech.com.vn:80/api/am-ke-hoach/save

export const createOrUpdateKeHoach = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-ke-hoach/save",
    searchObject
  );
};

// http://asset-management-dev.oceantech.com.vn:80/api/am-ke-hoach/get-danh-sach

export const searchByPageKeHoach = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-ke-hoach/get-danh-sach",
    searchObject
  );
};

// http://asset-management-dev.oceantech.com.vn:80/api/am-ke-hoach/save
export const createAndUpdateAsset = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-ke-hoach/save",
    searchObject
  );
};

export const deletePlan = (id) => {
  return axios.delete(API_ENPOINT_ASSET_MAINTANE + "am-ke-hoach/" + id);
};

export const getReceiverDepartment = (data) => {
  return axios.post(
    ConstantList.API_ENPOINT + "/api/assetDepartment/org/searchByPage",
    data,
    data
  );
};

export const createMaintainProposal = (data) => {
  return axios.post(API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/create", data);
};
export const getAssetsMaintainProposal = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/get-assets",
    searchObject
  );
};

export const updateMaintainProposal = (id, data) => {
  return axios.put(
    API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/update/" + id,
    data
  );
};

export const getMaintainProposalById = (id) => {
  return axios.get(API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/" + id);
};

export const deleteMaintainProposalById = (id) => {
  return axios.delete(API_ENPOINT_ASSET_MAINTANE + "am-de-xuat/" + id);
};

export const getMaintainFormList = (searchObject) => {
  return axios.post(
    API_PATH_COMMON_OBJECT + "/list/objecttypecode",
    searchObject
  );
};

// /api/am-chu-ky/update-status/:id

export const updateStatusReveice = (id) => {
  return axios.patch(
    API_ENPOINT_ASSET_MAINTANE + `am-chu-ky/update-status/${id}`
  );
};

export const getDetailChuKy = (id) => {
  return axios.get(API_ENPOINT_ASSET_MAINTANE + `am-chu-ky/${id}`);
};

export const searchByPageAcceptance = (params) => {
  return axios.get(API_ENPOINT_ASSET_MAINTANE + `am-nghiem-thu/search`, {
    params,
  });
};

export const getAcceptanceDetail = (id) => {
  return axios.get(API_ENPOINT_ASSET_MAINTANE + `am-nghiem-thu/${id}`);
};

export const createAcceptance = (payload) => {
  return axios.post(API_ENPOINT_ASSET_MAINTANE + `am-nghiem-thu`, payload);
};

export const updateAcceptance = (payload) => {
  return axios.put(
    API_ENPOINT_ASSET_MAINTANE + `am-nghiem-thu/${payload?.id}`,
    payload
  );
};

export const deleteAcceptance = (id) => {
  return axios.delete(API_ENPOINT_ASSET_MAINTANE + `am-nghiem-thu/${id}`);
};

// http://192.168.1.27:8065/asvn/api/v1/user-departments/current-user
export const getUserDepartments = () => {
  return axios.get(
    ConstantList.API_ENPOINT + "/api/v1/user-departments/current-user"
  );
};

// http://asset-management-dev.oceantech.com.vn:80/api/am-chu-ky/search-by-page
// /api/am-ke-hoach/detail/{id}
export const searchByPageTaiSan = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + "am-tai-san/search-by-page",
    searchObject
  );
};

// /api/am-ke-hoach/detail/{id}

export const searchByPageAssetKeHoach = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + `am-ke-hoach/detail/${searchObject.dxId}`,
    searchObject
  );
};

// /api/am-ke-hoach/{id}
export const getByAssetKetHoach = (id) => {
  return axios.get(API_ENPOINT_ASSET_MAINTANE + `am-ke-hoach/${id}`);
};

// http://asset-management-dev.oceantech.com.vn:80/api/am-ke-hoach/org/asset

export const searchByPageAsset = (searchObject) => {
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + `am-ke-hoach/org/asset`,
    searchObject
  );
};

// https://asvn.oceantech.com.vn/asvn/api/commonobject/list/objecttypecode

export const searchByHinhThuc = (searchObject) => {
  return axios.post(
    ConstantList.API_ENPOINT + `/api/commonobject/list/objecttypecode`,
    searchObject
  );
};

export const searchByPageHinhThucNew = (searchObject) => {
  return axios.post(
    ConstantList.API_ENPOINT + `/api/commonobject/org/list/objecttypecode`,
    searchObject
  );
};

//AM attachment
export const getDocumentById = (id) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/am-attachment/" + id
  );
};

export const createDocument = (data) => {
  return axios.post(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/am-attachment",
    data
  );
};

export const updateDocument = (data, id) => {
  return axios.put(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/am-attachment/" + id,
    data
  );
};

export const uploadFile = (formData) => {
  return axios.post(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/file-upload",
    formData
  );
};

export const getDetailPlanComparision = (id) => {
  //Đối chiếu kế hoạch thực tế Detail
  return axios.get(API_ENPOINT_ASSET_MAINTANE + `am-ke-hoach/comparison/` + id);
};

export const getAssetsPlanComparision = (searchObject) => {
  //Đối chiếu kế hoạch thực tế Assets
  return axios.post(
    API_ENPOINT_ASSET_MAINTANE + `am-chu-ky/comparison/search-by-page`,
    searchObject
  );
};

export const searchByPageDepartment = (searchObject) => {
  let url = API_PATH_ASSET_DEPARTMENT + "/searchByPage";
  return axios.post(url, searchObject);
};

// maintainplaning contracts for droplist
export const searchByPageMaintainContract = (params) => {
  let config = {
    params,
  };
  let url = API_ENPOINT_ASSET_MAINTANE + "contracts/search";
  return axios.get(url, config);
};

// maintainplaning contracts for table
export const searcgByPageMaintainContractTable = (params) => {
  let config = {
    params,
  };
  let url = API_ENPOINT_ASSET_MAINTANE + "am-contracts/search";
  return axios.get(url, config);
};

// End contract
export const liquidateContract = (id, searchObject) => {
  let url = API_ENPOINT_ASSET_MAINTANE + "am-contracts/liquidate/" + id;
  return axios.put(url, searchObject);
};
