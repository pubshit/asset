import {appConst, listMaintainPlaningStatus} from "../../appConst";
import {convertNumberPrice, convertNumberPriceRoundUp, formatTimestampToDate} from "../../appFunction";
import React from "react";
import moment from "moment/moment";

export const ckColumns = (props) => {
  const {t, isTabProcessed = false, isTabWaiting = false, isTabPreProcessed = false} = props;
  const columns = [
    {
      title: t("Asset.managementCode"),
      field: "tsMaquanly",
      minWidth: 160,
			align: "left",
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("MaintainPlaning.khMa"),
      field: "khMa",
      minWidth: 120,
			align: "left",
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("MaintainPlaning.managementDepartment"),
      field: "khPhongBanText",
      minWidth: 200,
			align: "left",
    },
    {
      title: t("Asset.formName"),
      field: "chinhthucText",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("Asset.performingUnit"),
      field: "donViThucHienText",
			align: "left",
      minWidth: 260,
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
			align: "left",
      minWidth: 200,
    },
    {
      title: t("MaintainPlaning.status"),
      field: "",
      minWidth: 180,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => appConst.listStatusChuKy.find(
        x => x.code === rowData.ckTrangthai
      )?.name || "",
      hidden: !isTabProcessed && !isTabPreProcessed,
    },
    {
      title: t("Asset.executionTimes"),
      field: "lanThu",
      align: "center",
      minWidth: 120,
      hidden: !isTabWaiting,
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("MaintainPlaning.cost"),
      field: "ckNtChiPhi",
      minWidth: 180,
      cellStyle: { textAlign: "right" },
      render: rowData => convertNumberPriceRoundUp(rowData.ckNtChiPhi) || 0,
      hidden: !isTabProcessed && !isTabPreProcessed,
    },
    {
      title: t("Asset.model"),
      field: "tsModel",
      minWidth: 100,
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 100,
    },
    {
      title: t("Asset.manufacturer"),
      field: "tsHangsx",
      minWidth: 160,
      hidden: !isTabProcessed,
    },
    {
      title: t("Asset.useDepartment"),
      field: "khoaPhongSuDungText",
      minWidth: 200,
      hidden: !isTabProcessed,
    },
    {
      title: t("MaintainPlaning.nguyenNhanSuCo"),
      field: "nguyenNhanSuCo",
      minWidth: 200,
      hidden: isTabWaiting,
    },
    {
      title: t("MaintainPlaning.noiDungSuCo"),
      field: "noiDungSuCo",
      minWidth: 200,
      hidden: !isTabProcessed && !isTabPreProcessed,
    },
    {
      title: t("MaintainPlaning.deXuatKhacPhuc"),
      field: "deXuatKhacPhuc",
      minWidth: 180,
      cellStyle: { textAlign: "left" },
      hidden: !isTabProcessed && !isTabPreProcessed,
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const contractColumns = (props) => {
  const {t, handleStatusContract = () => {}, page = 0, rowsPerPage = 10} = props;
  const columns = [
    {
      title: t("general.index"),
      field: "index",
      align: "center",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        page * rowsPerPage + rowData.tableData.id + 1,
    },
    {
      title: t("MaintainPlaning.contractCode"),
      field: "contractCode",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.contractDate"),
      field: "contractDate",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.contractDate
        ? formatTimestampToDate(rowData.contractDate)
        : "",
    },
    {
      title: t("MaintainPlaning.contractLiquidationDate"),
      field: "contractDate",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.contractLiquidationDate
        ? formatTimestampToDate(rowData.contractLiquidationDate)
        : "",
    },
    {
      title: t("MaintainPlaning.status"),
      field: "khTrangThai",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      minWidth: 180,
      render: handleStatusContract,
    },
    {
      title: t("MaintainPlaning.contractName"),
      field: "contractName",
      align: "left",
      minWidth: 280,
    },
    {
      title: t("MaintainPlaning.implementingAgencies"),
      field: "supplierName",
      align: "left",
      minWidth: 360,
    },
    {
      title: t("MaintainPlaning.contractContent"),
      field: "contractContent",
      minWidth: 180,
      maxWidth: 200,
      align: "left",
      render: rowData => <div className="text-ellipsis" dangerouslySetInnerHTML={{__html: rowData.contractContent}}/>
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const planColumns = (props) => {
  const {t, checkStatus = () => {}, page = 0, rowsPerPage = 10} = props;
  const columns = [
    {
      title: t("general.index"),
      field: "index",
      align: "center",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        page * rowsPerPage + rowData.tableData.id + 1,
    },
    {
      title: t("MaintainPlaning.khMa"),
      field: "khMa",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("purchasePlaning.name"),
      field: "khTen",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("purchasePlaning.status"),
      field: "khTrangThai",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      minWidth: 160,
      render: (rowData) => {
        const statusIndex = rowData?.khTrangThai;
        let className = "";
        if (statusIndex === listMaintainPlaningStatus[0].indexOrder) {
          className = "status status-warning";
        } else if (
          statusIndex === listMaintainPlaningStatus[1].indexOrder
        ) {
          className = "status status-success";
        } else if (
          statusIndex === listMaintainPlaningStatus[2].indexOrder
        ) {
          className = "status status-info";
        } else if (
          statusIndex === listMaintainPlaningStatus[3].indexOrder
        ) {
          className = "status status-error";
        } else {
          className = "";
        }
        return (
          <span className={className}>{checkStatus(statusIndex)}</span>
        );
      },
    },
    {
      title: t("MaintainPlaning.tuNam"),
      field: "tuNam",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.denNam"),
      field: "denNam",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.departmentName"),
      field: "khPhongBanText",
      align: "left",
      minWidth: 240,
    },
    {
      title: t("MaintainPlaning.khGhiChu"),
      field: "khGhiChu",
      minWidth: 200,
      align: "left",
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const proposalColumns = (props) => {
  const {t} = props;
  const columns = [
    {
      title: t("MaintenanceProposal.votes"),
      field: "dxMa",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintenanceProposal.proposedDate"),
      minWidth: 100,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => formatTimestampToDate(rowData.dxThoiGian),
    },
    {
      title: t("MaintenanceProposal.status"),
      field: "dxTrangthaiMota",
      minWidth: 160,
      align: "left",
    },
    {
      title: t("MaintenanceProposal.proposedDepartment"),
      field: "dxPhongBanText",
      align: "left",
      minWidth: 280,
    },
    {
      title: t("MaintenanceProposal.receiverDepartment"),
      field: "tnPhongBanText",
      align: "left",
      minWidth: 280,
    },
    {
      title: t("MaintenanceProposal.note"),
      field: "dxGhichu",
      minWidth: 200,
      align: "left",
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const acceptanceColumns = (props) => {
  const {t} = props;
  const columns = [
    {
      title: t("MaintainPlaning.code"),
      field: "ma",
      minWidth: 140,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.ngayKyBienBan"),
      field: "ma",
      minWidth: 140,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.ngayKyBienBan
        ? formatTimestampToDate(rowData?.ngayKyBienBan)
        : "",
    },
    {
      title: t("MaintainPlaning.status"),
      field: "trangThai",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => {
        let className = "";
        let value = appConst.listStatusNghiemThuBTBD.find(
          x => x.code === rowData?.trangThai
        )

        if (rowData?.trangThai === appConst.STATUS_NGHIEM_THU_BTBD.DANG_XU_LY.code) {
          className = "status status-warning";
        } else if (rowData?.trangThai === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code) {
          className = "status status-success";
        }

        return <span className={className}>{value?.name}</span>
      }
    },
    {
      title: t("MaintainPlaning.name"),
      field: "ten",
      minWidth: 160,
    },
    {
      title: t("MaintainPlaning.contractNumber"),
      field: "contractText",
      minWidth: 200,
    },
    {
      title: t("MaintainPlaning.implementingAgencies"),
      field: "donViThucHien",
      minWidth: 260,
    },
    {
      title: t("MaintainPlaning.executionTiems"),
      field: "lanThu",
      align: "center",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.total"),
      field: "tongTien",
      minWidth: 160,
      cellStyle: {
        textAlign: "right",
      },
      render: rowData => convertNumberPrice(rowData?.tongTien)
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const acceptanceSubColumns = (props) => {
  const {t} = props;
  const columns = [
    {
      title: t("general.index"),
      field: "index",
      align: "left",
      minWidth: "60px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1
    },
    {
      title: t("Asset.code"),
      field: "tsMa",
      minWidth: "120px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("MaintainPlaning.dateSignContract"),
      field: "ngayKyHopDong",
      minWidth: 160,
      align: "center",
      render: (rowData) =>
        rowData?.ngayKyHopDong
          ? moment(rowData?.ngayKyHopDong).format("DD/MM/YYYY")
          : "",
    },
    {
      title: t("MaintainPlaning.executionTimes"),
      field: "lanThu",
      minWidth: "100px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.lanThu
    },
    {
      title: t("Asset.model"), //Model
      field: "tsModel",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 100,
      maxWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "tsNamsx",
      minWidth: "140px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearPutIntoUse"),
      field: "tsNamsd",
      minWidth: "140px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.status"),
      field: "tsTrangThaiTen",
      minWidth: "160px",
      align: "center",
      render: rowData => appConst.listStatusChuKy.find(
        x => x.code === rowData.ckTrangthai
      )?.name || ""
    },
    {
      title: t("MaintainPlaning.useDepartment"),
      field: "khoaPhongSuDungText",
      minWidth: "260px",
      align: "left",
    },
    {
      title: t("MaintainPlaning.implementingForm"),
      field: "chinhthucText",
      minWidth: "160px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.intoMoney"),
      field: "ckNtTongChiPhi",
      minWidth: "160px",
      align: "left",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.ckNtTongChiPhi),
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const planSubColumns = (props) => {
  const {t} = props;
  const columns = [
    {
      title: t("general.index"),
      field: "index",
      align: "left",
      minWidth: "60px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.code"),
      field: "tsMa",
      minWidth: "120px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("MaintainPlaning.dateSignContract"),
      field: "ngayKyHopDong",
      minWidth: 160,
      align: "center",
      render: (rowData) =>
        rowData?.ngayKyHopDong
          ? moment(rowData?.ngayKyHopDong).format("DD/MM/YYYY")
          : "",
    },
    {
      title: t("Asset.model"), //Model
      field: "tsModel",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "tsNamsx",
      align: "center",
      minWidth: "140px",
    },
    {
      title: t("Asset.yearPutIntoUse"),
      field: "tsNamsd",
      align: "center",
      minWidth: "140px",
    },
    {
      title: t("MaintainPlaning.status"),
      field: "tsTrangThaiTen",
      minWidth: "160px",
      align: "left",
    },
    {
      title: t("MaintainPlaning.useDepartment"),
      field: "khoaPhongSuDungText",
      minWidth: "160px",
      align: "left",
    },
    {
      title: t("MaintainPlaning.cycle"),
      field: "ckSoluong",
      minWidth: "100px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return rowData?.ckSoluong && rowData?.ckSoluong > 0 ? (
          <span>{`${rowData?.ckSoluong} lần/năm`}</span>
        ) : (
          ""
        );
      },
    },
    {
      title: t("MaintainPlaning.implementingForm"),
      field: "chinhthucText",
      minWidth: "160px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};

export const proposalSubColumns = (props) => {
  const {t} = props;
  const columns = [
    {
      title: t("Asset.code"),
      field: "tsMa",
      maxWidth: "120px",
      minWidth: "100px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("Asset.managementCode"),
      field: "tsMaquanly",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.model"), //Model
      field: "tsModel",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 100,
      maxWidth: 160,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "tsNamSd",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: "Hãng sx",
      field: "tsHangsx",
      align: "left",
      minWidth: 160,
    },
  ];

  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
};
