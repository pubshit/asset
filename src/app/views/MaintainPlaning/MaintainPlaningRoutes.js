import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const MaintainPlaningTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./MaintainPlaningTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(MaintainPlaningTable);

const MaintainPlaningRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "asset/maintain_planing",
    exact: true,
    component: ViewComponent
  }
];

export default MaintainPlaningRoutes;