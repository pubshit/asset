import React, { useEffect, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  Link,
  Radio,
  TablePagination,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import MaterialTable, { MTableToolbar } from 'material-table';
import SearchIcon from '@material-ui/icons/Search';
import { searchByText } from '../Supplier/SupplierService';
import { appConst } from '../../appConst'


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

function UnitDialog({ t, onClose, open, handleSelect, unit }) {
  const [selectedValue, setSelectedValue] = useState('');
  const [selectedItem, setSelectedItem] = useState('');
  const [listUnit, setListUnit] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isHiddenButton, setIsHiddenButton] = useState(false);
  const [timeoutId, setTimeoutId] = useState(null);
  const [totalElements, setTotalElements] = useState(0)
  const [query, setQuery] = useState({
    keyword: '',
    page: 0,
    size: 10,
  });

  useEffect(() => {
    setSelectedValue(unit?.donViThucHienId || '');
  }, [unit]);

  useEffect(() => {
    const handleSearchByText = async (keyword, page, size) => {
      setIsLoading(true);
      try {
        const { status, data } = await searchByText(keyword, page, size);
        if (status === 200) {
          setListUnit(data?.content || []);
          setTotalElements(data?.totalElements)
          setIsHiddenButton(data?.content.length > 0 ? false : true);
        } else {
          console.warn('Error');
        }
      } catch (error) {
        console.warn('Error', error);
      } finally {
        setIsLoading(false);
      }
    };

    handleSearchByText(query.keyword, query.page, query.size);
  }, [query]);

  const handleChangeRadio = (event, data) => {
    setSelectedItem({ donViThucHienText: data?.name, donViThucHienId: data?.id });
    setSelectedValue(event.target.value);
  };

  const handleTextChange = (event) => {
    const keyword = event.target.value;
    clearTimeout(timeoutId);
    const newTimeoutId = setTimeout(() => {
      setQuery({
        ...query,
        keyword: keyword
      });
    }, 1000);
    setTimeoutId(newTimeoutId);
  };

  const handleSearch = () => {
    // Handle search logic
  };

  const handleChangePage = (event, newPage) => {
    setQuery({
      ...query,
      page: newPage
    })
  };

  const setRowsPerPage = (event) => {
    setQuery({
      ...query,
      size: event.target.value
    })
  }

  const columns = [
    {
      title: 'Đơn vị thực hiện',
      field: 'custom',
      align: 'left',
      width: '100px',
      cellStyle: {
        padding: '0px',
        paddingLeft: '10px',
      },
      render: (rowData) => (
        <Radio
          id={`radio${rowData.id}`}
          name={rowData.id}
          style={{ padding: '0px' }}
          value={rowData.id}
          checked={selectedValue === rowData.id}
          onChange={(event) => handleChangeRadio(event, rowData)}
        />
      ),
    },
    {
      title: 'Mã đơn vị',
      field: 'code',
      align: 'left',
      width: '150',
    },
    {
      title: 'Tên đơn vị',
      field: 'name',
      align: 'left',
      width: '150',
    },
  ];

  return (
    <Dialog onClose={onClose} open={open} PaperComponent={PaperComponent} fullWidth maxWidth="md">
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">Chọn đơn vị thực hiện</span>
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={6} sm={12} xs={12}></Grid>
          <Grid item md={2} sm={12} xs={12}>
            {isHiddenButton && (
              <Button
                size="small"
                className="mt-14 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => { }}
              >
                {t('general.add_asset')}
              </Button>
            )}
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box"
                style={{ marginTop: '16px' }}
                onChange={handleTextChange}
                placeholder="Tìm kiếm theo mã hoặc tên đơn vị"
                id="search_box"
                startAdornment={
                  <InputAdornment>
                    <Link>
                      <SearchIcon
                        onClick={handleSearch}
                        style={{
                          position: 'absolute',
                          top: '0',
                          right: '0',
                        }}
                      />
                    </Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={listUnit}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: (rowData) => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? '#EEE' : '#FFF',
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '320px',
                minBodyHeight: '320px',
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: '100%' }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => { }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElements}
              page={query.page}
              rowsPerPage={query.size}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <div className="flex flex-space-between flex-middle">
          <Button className="mb-16 mr-12" variant="contained" color="secondary" onClick={() => onClose()}>
            {t('general.cancel')}
          </Button>
          <Button
            className="mb-16 mr-16"
            variant="contained"
            color="primary"
            onClick={() => handleSelect(selectedItem)}
          >
            {t('general.select')}
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
}

export default UnitDialog;
