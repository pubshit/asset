import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  TextField,
  FormControlLabel,
  DialogContent,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  MuiPickersUtilsProvider,
  DateTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  createMaintainPlaning,
  updateMaintainPlaning,
  searchByPage,
} from "./MaintainPlaningService";
import Draggable from "react-draggable";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import SelectAssetPopup from "../Component/Asset/SelectAssetPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class CreateMaintainPlaningDialog extends Component {
  state = {
    id: "",
    status: null,
    maintainPlaningDate: new Date(),
    asset: null,
    maintainPlanings: [],
    shouldOpenNotificationPopup: false,
    Notification: "",
    shouldOpenAssetPopup: false,
  };

  listIndexOder = [
    { id: 0, name: "Kế hoạch" },
    { id: 1, name: "Đang thực hiện" },
    { id: 2, name: "Đã thực hiện" },
  ];

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    },
    ()=> console.log(this.state.status));
  };

  componentWillMount() {
    let { open, handleClose, item, asset } = this.props;
    if (asset !== null && asset.id !== null) {
      this.setState({ ...item,asset: asset  });
    } else {
      toast.warn("Chưa chọn tài sản.");
    }
  }

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    if (!this.state.maintainPlaningDate) {
      toast.warn("Chưa chọn ngày kế hoạch");
      toast.clearWaitingQueue();
      return;
    }
    if (this.state.status === null) {
      toast.warn("Chưa chọn trạng thái");
      toast.clearWaitingQueue();
      return;
    }
    if (id) {
      updateMaintainPlaning({
        ...this.state,
      }).then(() => {
        toast.info("Sửa kế hoạch thành công.");
        this.props.handleOKEditClose();
      });
    } else {
      createMaintainPlaning({
        ...this.state,
      }).then(() => {
        toast.info("Thêm kế hoạch thành công.");
        this.props.handleOKEditClose();
      });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  render() {
    let {
      id,
      status,
      asset,
      maintainPlaningDate,
      shouldOpenAssetPopup,
      shouldOpenNotificationPopup,
    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4 className="">{t("MaintainPlaning.dialog")}</h4>
        </DialogTitle>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent style={{ minHeight: "110px" }}>
            <Grid className="" container spacing={2}>
              <Grid className="" item md={6} sm={12} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog"
                    style={{ marginTop: "2px" }}
                    label={t("MaintainPlaning.maintainPlaningDate")}
                    format="dd/MM/yyyy"
                    value={maintainPlaningDate}
                    onChange={(date) => {
                      this.handleDateChange(date, "maintainPlaningDate");
                    }}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid className="" item md={6} sm={12} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="statusId">
                    {t("MaintainPlaning.status")}
                  </InputLabel>
                  <Select
                    value={status}
                    onChange={(status) => this.handleChange(status, "status")}
                    inputProps={{
                      name: "status",
                      id: "statusId",
                    }}
                  >
                    {this.listIndexOder.map((item) => {
                      return (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                style={{ marginRight: "15px" }}
                color="primary"
                onClick={() => this.handleFormSubmit()}
                // type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default CreateMaintainPlaningDialog;
