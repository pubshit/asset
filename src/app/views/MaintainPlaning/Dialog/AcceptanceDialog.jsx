import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from "@material-ui/core";
import { NumberFormatCustom, PaperComponent, convertNumberPrice, convertNumberPriceRoundUp } from "app/appFunction";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AsynchronousAutocompleteSub from "app/views/utilities/AsynchronousAutocompleteSub";
import { appConst, formatDate, variable } from "app/appConst";
import { createAcceptance, updateAcceptance } from "../MaintainPlaningService";
import moment from "moment";
import ValidatedDatePicker from "app/views/Component/ValidatePicker/ValidatePicker";
import MaterialTable from "material-table";
import { useContext } from "react";
import AppContext from "app/appContext";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

export default function AcceptanceDialog(props) {
  const { setPageLoading } = useContext(AppContext);
  const {
    t, i18n,
    open,
    isView,
    handleClose,
    item,
    assets,
    handleOKEditClose,
    state
  } = props;
  const [stateAcceptance, setStateAcceptance] = useState({}); // state phiếu NT
  const [amChukys, setAmChukys] = useState([]); //list ck đã chọn
  const [ckNtChiPhi, setCkNtChiPhi] = useState()
  const [totalCost, setTotalCost] = useState(
    (state?.item?.tongTien)
    || 0
  );

  const [currentStatus, setCurrentStatus] = useState(appConst.STATUS_CHU_KY.CHO_XU_LY.code);
  const [currentStatusChange, setCurrentStatusChange] = useState(appConst.STATUS_CHU_KY.CHO_XU_LY.code);

  const isDisplayCost = (value) => [
    appConst.STATUS_CHU_KY.VUA_SUA_KHONG_BT.code,
    appConst.STATUS_CHU_KY.HONG_KHONG_BT.code,
  ].includes(value)

  const nonChargeableStatusCodes = [
    appConst.STATUS_CHU_KY.CHO_XU_LY.code,
    appConst.STATUS_CHU_KY.DA_XU_LY.code,
    appConst.STATUS_CHU_KY.DA_XU_LY_DXTT_LINH_KIEN.code,
  ];

  const chargeableFromVuaSuaCodes = [
    appConst.STATUS_CHU_KY.VUA_SUA_KHONG_BT.code,
    appConst.STATUS_CHU_KY.HONG_KHONG_BT.code,
  ];

  useEffect(() => {
    if (!item?.id) {
      setStateAcceptance({
        ...stateAcceptance,
        status: appConst.STATUS_NGHIEM_THU_BTBD.DANG_XU_LY
      })
      state?.assets?.map(x => {
        setCkNtChiPhi(x.ckDonGia)
      })
    }
    else {
      setStateAcceptance({
        ...item,
        status: appConst.listStatusNghiemThuBTBD.find(
          x => x.code === item?.trangThai
        ),
      })

    }
  }, [item]);

  useEffect(() => {
    let newArray = assets ? assets : [];

    newArray?.map(item => {
      item.status = appConst.listStatusChuKy.find(x => x.code === item.ckTrangthai)

      return item;
    })

    setAmChukys([...newArray]);
  }, [assets]);

  useEffect(() => {
    if (
      stateAcceptance?.status?.code
      === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code
    ) {
      handleFillStatusCk();
      if (!isView) {
        setStateAcceptance({
          ...stateAcceptance,
          ngayKyBienBan: null,
        })
      }
    }
  }, [stateAcceptance.status]);

  useEffect(() => {
    const totalCost = assets.reduce((acc, item) => {
      if (!isDisplayCost(item.ckTrangthai)) {        
        return acc + item.ckDonGia;
      }
      return acc;
    }, 0);
    setTotalCost(totalCost);
  }, [assets])

  const handleFormSubmit = async () => {

    const sendData = {
      ...stateAcceptance,
      isTuBaoTri: props.isTuBaoTri,
      trangThai: stateAcceptance?.status?.code,
      ngayKyBienBan: stateAcceptance?.ngayKyBienBan
        ? moment(stateAcceptance?.ngayKyBienBan).format(formatDate)
        : null,
      amChukys,
      chinhThucCode: state?.commonObject?.code,
      lanThu: state?.times?.code,
      contractId: state?.contract?.contractId,
      tongTien: state?.contract?.cost,
      isNtWithoutContract: state?.isNtWithoutContract,
      ckNtChiPhi: ckNtChiPhi
    }
    if (!validatation(sendData)) return;
    setPageLoading(true);

    try {
      let res = stateAcceptance?.id
        ? await updateAcceptance(sendData)
        : await createAcceptance(sendData)
      const { code, data, message } = res.data;
      if (appConst.CODE.SUCCESS === code) {
        stateAcceptance?.id
          ? toast.success(t("general.updateSuccess"))
          : toast.success(t("general.addSuccess"))
        handleOKEditClose();
      }
      else {
        toast.warning(t(message));
      }
    }
    catch (err) {
      toast.error(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  }

  const handleSetRowDataCell = (value, name, index) => {
    let newArray = amChukys ? [...amChukys] : [];
    let item = newArray[index];

    item[name] = value;
    newArray[index][name] = value;

    if (variable.listInputName.status === name) {
      setCurrentStatusChange(value?.code);
      if (nonChargeableStatusCodes.includes(value?.code)) {
        setCurrentStatus(value?.code)
      }
      newArray[index].ckTrangthai = value?.code;
      setStateAcceptance({
        ...stateAcceptance,
        tongTien: sumTotalCost(newArray)
      });
      setTotalCost(sumTotalCost(newArray));
    }
    if (name === "ckNtChiPhi") {
      if (chargeableFromVuaSuaCodes.includes(item?.status?.code)) {
        let totalPrice = 0;
        for (let i = 0; i < newArray.length; i++) {
          if (chargeableFromVuaSuaCodes.includes(newArray[i]?.status?.code)) {
            totalPrice += Number(newArray[i].ckNtChiPhi);
          }
        }
        setTotalCost((newArray.reduce((total, item) => Number(total) + Number(item.ckNtChiPhi ||0), 0)) - totalPrice)
      } else {
        setTotalCost(newArray.reduce((total, item) => Number(total) + Number(item.ckNtChiPhi ||0), 0))
      }
    } 

    if (name === "status") {
      let total = 0;
        newArray?.forEach(x => {
          if (nonChargeableStatusCodes.includes(x.status?.code)) {
            total += Number(x.ckNtChiPhi);
          }
        })
        setTotalCost(total);
      }

    setAmChukys(newArray);
  };

  const handleSetStateData = (value, name) => {
    setStateAcceptance({
      ...stateAcceptance,
      [name]: value,
    })
  };

  const validatation = (item) => {
    let { ngayKyBienBan } = stateAcceptance;
    
    if (item?.trangThai === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code) {
      let check = item?.amChukys?.some(
        ck => ck.ckTrangthai === appConst.STATUS_CHU_KY.CHO_XU_LY.code
      )
      if (check) {
        toast.warning(t("MaintainPlaning.acceptanceMessage.cycleStatusError"));
        return !check; //Trạng thái phiếu đã xly thì các ck phải có trạng thái khác chờ xly
      }
      if (new Date(ngayKyBienBan) > new Date()) {
        toast.warning(t("MaintainPlaning.acceptanceMessage.exceedToday"));
        return false;
      }
    }
    return true;
  }
  
  const sumTotalCost = (amChukys) => amChukys?.reduce((total, item) => {
    if (
      item.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY.code
      || item.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY.code
    ) {
      return Number(total) + Number(item?.ckNtChiPhi)
    }
    else {
      return total;
    }
  }, 0);

  const columns = [
    {
      title: t("Asset.stt"),
      field: "stt",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (rowData?.tableData?.id + 1),
    },
    {
      title: t("Asset.code"),
      field: "tsMa",
      minWidth: 150,
      align: "left",
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 150,
      align: "left",
    },
    {
      title: t("Asset.model"),
      field: "tsModel",
      minWidth: 150,
      align: "left",
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 150,
      align: "left",
    },
    {
      title: t("Asset.useDepartment"),
      field: "khoaPhongSuDungText",
      minWidth: 150,
      align: "left",
    },
    {
      title: t("Asset.status"),
      field: "",
      minWidth: 250,
      align: "left",
      render: (rowData) => (
        <AsynchronousAutocompleteSub
          searchFunction={() => { }}
          searchObject={{}}
          label={t("MaintainPlaning.status")}
          listData={appConst.listStatusChuKy}
          setListData={() => { }}
          displayLable="name"
          onSelect={(value) => handleSetRowDataCell(
            value,
            "status",
            rowData?.tableData?.id
          )}
          value={rowData.status ? rowData.status : null}
          readOnly={isView}
        />
      ),
    },
    {
      title: t("MaintainPlaning.cost"),
      field: "",
      hidden: (!stateAcceptance?.id && props.isTuBaoTri) ,
      minWidth: 250,
      align: "left",
      render: (rowData) => (
        <TextValidator
          fullWidth
          label={t("MaintainPlaning.cost")}
          value={rowData?.ckNtChiPhi || rowData?.ckDonGia}
          name="ckNtChiPhi"
          type="text"
          onChange={(e) => handleSetRowDataCell(
            e?.target?.value,
            e?.target?.name,
            rowData?.tableData?.id
          )}
          InputProps={{
            inputComponent: NumberFormatCustom,
            inputProps: {
              style: { textAlign: "right" },
            },
            readOnly: isView 
          }}
          validators={[
            stateAcceptance?.status?.code === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code 
              ? "required"
              : "minNumber:0",
            "minNumber:0", 
            (totalCost < 0 && rowData?.ckNtChiPhi > 0) ? "maxNumber:0" : "minNumber:0", 
          ]}
          errorMessages={[
            t("general.required"),
            t("MaintainPlaning.messages.costsError"),
            t("general.maxTotalCostError"),
          ]}
        />),
    },
    {
      title: t("MaintainPlaning.nguyenNhanSuCo"),
      field: "",
      minWidth: 250,
      align: "left",
      render: (rowData) => isShowColumn(rowData) ? (
        <TextValidator
          fullWidth
          label={t("MaintainPlaning.nguyenNhanSuCo")}
          value={rowData?.nguyenNhanSuCo || ""}
          name="nguyenNhanSuCo"
          onChange={(e) => handleSetRowDataCell(
            e?.target?.value,
            e?.target?.name,
            rowData?.tableData?.id
          )}
          InputProps={{
            readOnly: isView
          }}
        />
      ) : "",
    },
    {
      title: t("MaintainPlaning.noiDungSuCo"),
      field: "",
      minWidth: 250,
      align: "left",
      render: (rowData) => isShowColumn(rowData) ? (
        <TextValidator
          fullWidth
          label={t("MaintainPlaning.noiDungSuCo")}
          value={rowData?.noiDungSuCo || ""}
          name="noiDungSuCo"
          onChange={(e) => handleSetRowDataCell(
            e?.target?.value,
            e?.target?.name,
            rowData?.tableData?.id
          )}
          InputProps={{
            readOnly: isView
          }}
        />
      ) : "",
    },
    {
      title: t("MaintainPlaning.deXuatKhacPhuc"),
      field: "",
      minWidth: 250,
      align: "left",
      render: (rowData) => isShowColumn(rowData) ? (
        <TextValidator
          fullWidth
          label={t("MaintainPlaning.deXuatKhacPhuc")}
          value={rowData?.deXuatKhacPhuc || ""}
          name="deXuatKhacPhuc"
          onChange={(e) => handleSetRowDataCell(
            e?.target?.value,
            e?.target?.name,
            rowData?.tableData?.id
          )}
          InputProps={{
            readOnly: isView
          }}
        />
      ) : "",
    },
  ];

  // show column với những trạng thái đã BT
  const isShowColumn = (rowData) => rowData.ckTrangthai
    === appConst.STATUS_CHU_KY.DA_XU_LY_DXTT_LINH_KIEN.code
    || rowData.ckTrangthai === appConst.STATUS_CHU_KY.HONG_KHONG_BT.code;

  const handleFillStatusCk = () => { //chuyen ck cho xu ly => da xu ly
    let newArray = amChukys || [];

    newArray?.map(item => {
      if (item.ckTrangthai === appConst.STATUS_CHU_KY.CHO_XU_LY.code) {
        item.status = appConst.STATUS_CHU_KY.DA_XU_LY;
        item.ckTrangthai = appConst.STATUS_CHU_KY.DA_XU_LY.code;
      }

      return item;
    })
  }
  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth="lg"
      fullWidth
    >
      <DialogTitle
        className='cursor-move pb-0'
        id="draggable-dialog-title"
      >
        {t("MaintainPlaning.acceptance")}
      </DialogTitle>
      <ValidatorForm
        onSubmit={handleFormSubmit}
        className="py-5"
      >
        <DialogContent>
          <Grid container spacing={1}>
            {/* <Grid item md={1} sm={1} xs={12}>
              <TextValidator
                fullWidth
                label={t("MaintainPlaning.year")}
                value={stateAcceptance?.nam || ""}
                InputProps={{
                  readOnly: isView,
                }}
                type="number"
                name="nam"
                onChange={(e) => handleSetStateData(
                  e?.target?.value,
                  e?.target?.name,
                )}
                validators={[
                  "isNumber",
                  "minNumber:0",
                  `maxNumber:${new Date().getFullYear()}`,
                ]}
                errorMessages={[
                  t("purchasePlaning.is_number"),
                  t("general.minNumberError"),
                  t("general.maxYearNow"),
                ]}
              />
            </Grid>
            <Grid item md={1} sm={1} xs={12}>
              <TextValidator
                fullWidth
                label={t("MaintainPlaning.month")}
                value={stateAcceptance?.thang || ""}
                InputProps={{
                  readOnly: isView,
                }}
                type="number"
                name="thang"
                onChange={(e) => handleSetStateData(
                  e?.target?.value,
                  e?.target?.name,
                )}
                validators={[
                  "isNumber",
                  "minNumber:0",
                  `maxNumber:${new Date().getMonth() + 1}`
                ]}
                errorMessages={[
                  t("purchasePlaning.is_number"),
                  t("general.minNumberError"),
                  t("general.maxMonthNow"),
                ]}
              />
            </Grid> */}
            {stateAcceptance?.id && (
              <Grid item md={2} sm={2} xs={12}>
                <TextValidator
                  fullWidth
                  label={t("MaintainPlaning.code")}
                  value={stateAcceptance?.ma || ""}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </Grid>
            )}
            <Grid item md={2} sm={2} xs={12}>
              <ValidatedDatePicker
                autoOk
                fullWidth
                margin="none"
                readOnly={isView}
                InputProps={{ readOnly: isView }}
                inputVariant="standard"
                format="dd/MM/yyyy"
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                label={
                  <span>
                    {stateAcceptance?.status?.code
                      === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code
                      && <span className="colorRed">*</span>}
                    {t("MaintainPlaning.ngayKyBienBan")}
                  </span>
                }
                value={
                  stateAcceptance?.ngayKyBienBan
                    ? stateAcceptance?.ngayKyBienBan
                    : null
                }
                disabled={
                  stateAcceptance?.status?.code
                  !== appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code
                }
                onChange={(date) => handleSetStateData(date, "ngayKyBienBan")}
                maxDate={new Date()}
                maxDateMessage={t("general.maxDateNow")}
                invalidDateMessage={t("general.invalidDateFormat")}
                validators={
                  stateAcceptance?.status?.code
                  === appConst.STATUS_NGHIEM_THU_BTBD.DA_XU_LY.code
                  && ["required"]
                }
                errorMessages={[t("general.required")]}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </Grid>
            <Grid item md={2} sm={2} xs={12}>
              <TextValidator
                fullWidth
                label={t("MaintainPlaning.name")}
                value={stateAcceptance?.ten || ""}
                name="ten"
                onChange={(e) => handleSetStateData(
                  e?.target?.value,
                  e?.target?.name,
                )}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            <Grid item md={2} sm={2} xs={12}>
              <AsynchronousAutocompleteSub
                searchFunction={() => { }}
                searchObject={{}}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("MaintainPlaning.status")}
                  </span>
                }
                listData={appConst.listStatusNghiemThuBTBD}
                setListData={() => { }}
                displayLable="name"
                onSelect={(value) => handleSetStateData(
                  value,
                  "status",
                )}
                value={stateAcceptance.status ? stateAcceptance.status : null}
                readOnly={isView}
                validators={["required",]}
                errorMessages={[t("general.required"),]}
              />
            </Grid>
            {/* {stateAcceptance?.id && (
              <Grid item md={2} xs={12}>
                <TextValidator
                  fullWidth
                  label={t("MaintainPlaning.total")}
                  value={convertNumberPrice(stateAcceptance?.tongTien) || ""}
                  name="tongTien"
                  onChange={(e) => handleSetStateData(
                    e?.target?.value,
                    e?.target?.name,
                  )}
                  InputProps={{
                    inputProps: {
                      className: "text-align-right",
                    }
                  }}
                />
              </Grid>
            )} */}
            <Grid item md={2} xs={12}>
              {(!props.isTuBaoTri || (stateAcceptance?.id && stateAcceptance?.tongtien > 0)) && (
                <TextValidator
                  fullWidth
                  label={t("MaintainPlaning.total")}
                  value={convertNumberPrice(totalCost) || 0}
                  name="tongTien"
                  InputProps={{
                    inputProps: {
                      className: "text-align-right",
                      readOnly: true
                    }
                  }}
                />
              )}
            </Grid>
            {stateAcceptance?.id && stateAcceptance?.lanThu && (
              <Grid item md={2} xs={12} className="flex flex-bottom">
                Lần nghiệm thu thứ: {stateAcceptance?.lanThu || ""}
              </Grid>
            )}
            {stateAcceptance?.id && (
              <Grid item md={4} sm={4} xs={12}>
                <TextField
                  fullWidth
                  label={t("MaintainPlaning.implementingAgencies")}
                  value={stateAcceptance?.donViThucHienText || ""}
                  InputProps={{
                    readOnly: isView,
                  }}
                  disabled
                />
              </Grid>
            )}
            <Grid item md={stateAcceptance?.id ? 6 : 4} xs={12}>
              <TextValidator
                fullWidth
                label={t("MaintainPlaning.note")}
                value={stateAcceptance?.ghiChu || ""}
                name="ghiChu"
                onChange={(e) => handleSetStateData(
                  e?.target?.value,
                  e?.target?.name,
                )}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>

            <Grid item xs={12} className="mt-20">
              <MaterialTable
                data={amChukys ? amChukys : []}
                columns={columns}
                title={""}
                options={{
                  toolbar: false,
                  search: false,
                  paging: false,
                  selection: false,
                  sorting: false,
                  draggable: false,
                  minBodyHeight: 255,
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
									maxBodyHeight: 400,
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                  pagination: appConst.localizationVi.pagination
                }}
                components={{
                  Toolbar: (props) => (
                    <div className="w-100">
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
              />
            </Grid>
          </Grid>

        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={handleClose}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            type="submit"
            disabled={isView}
          >
            {t("general.save")}
          </Button>
        </DialogActions>
      </ValidatorForm>

      {/* {shouldOpenDialogProduct && (
        <ProductDialog
          t={t}
          i18n={i18n}
          handleClose={handleAddNewDialogClose}
          open={shouldOpenDialogProduct}
          handleOKEditClose={handleAddNewDialogClose}
          item={itemProduct}
          type="addFromRateIncident"
          selectProduct={handleAddNewProduct}
          indexLinhKien={indexLinhKien}
        />
      )}
      {shouldOpenDialogSku && (
        <StockKeepingUnitEditorDialog
          t={t}
          i18n={i18n}
          handleClose={handleAddNewDialogClose}
          open={shouldOpenDialogSku}
          handleOKEditClose={handleAddNewDialogClose}
          item={itemProduct}
          selectUnit={handleAddNewSku}
          itemIndex={indexLinhKien}
        />
      )} */}
    </Dialog>
  )
}
