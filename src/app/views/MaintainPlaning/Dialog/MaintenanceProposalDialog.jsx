import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
  IconButton,
  TextField,
} from "@material-ui/core";
import { appConst, STATUS_DEPARTMENT, variable } from "app/appConst";
import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import localStorageService from "../../../services/localStorageService";
import SelectAssetAllPopup from "../../../views/Component/Asset/SelectAssetAllPopup";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {
  createMaintainProposal,
  searchByPageDepartment,
  searchByPageHinhThucNew,
  updateMaintainProposal,
} from "../MaintainPlaningService";
import { withStyles } from "@material-ui/core/styles";
import AppContext from "../../../appContext";
import {
  filterOptions,
  convertFromToDateZ,
  getTheHighestRole,
  getUserInformation,
  LightTooltip,
  PaperComponent,
  spliceString, isSuccessfulResponse, handleThrowResponseMessage
} from "app/appFunction";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { Label } from "../../Component/Utilities";
import { getListUserByDepartmentId } from "app/views/AssetTransfer/AssetTransferService";
import { PrintMultipleFormDialog } from "../../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../../FormCustom/constant";
import { searchByPageDepartmentByOrg } from "../../Department/DepartmentService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const useStyles = (theme) => ({
  content: {
    maxHeight: "60vh",
  },
});

class MaintenanceProposalDialog extends React.Component {
  constructor(props) {
    super(props);
    this.contentRef = React.createRef();
    this.handleRowDataCellChangeNote =
      this.handleRowDataCellChangeNote.bind(this);
  }
  currentUser = localStorageService.getSessionItem("currentUser");
  state = {
    proposedDate: new Date(),
    status: appConst.STATUS_MAINTAINANCE_PROPOSAL.CHO_TONG_HOP.code,
    proposedDepartment: null,
    department: null,
    proposedDepartmentId: "",
    receiverDepartment: null,
    note: "",
    listAssets: [],
    listForm: [],
    listReceiverDepartment: [],
    listProposedDepartment: [],
    listDepartment: [],
    assetVouchers: [],
    shouldOpenAssetPopup: false,
    statusIndexOrders: [
      appConst.listStatusAssets.LUU_MOI.indexOrder,
      appConst.listStatusAssets.DANG_SU_DUNG.indexOrder,
      appConst.listStatusAssets.KHAC.indexOrder,
      appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder,
      appConst.listStatusAssets.DANG_BAO_DUONG.indexOrder,
    ],
    isPrint: false,
    saveAndPrint: false,
    item: {},
    text: "",
    permission: {
      proposedDepartment: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      department: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      receiverDepartment: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      proposedDate: { ...appConst.OBJECT_PERMISSION_DEFAULT, isDisabled: true },
      status: { ...appConst.OBJECT_PERMISSION_DEFAULT, isView: true },
      note: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      code: { ...appConst.OBJECT_PERMISSION_DEFAULT, isDisabled: true },
      nguoiLap: { ...appConst.OBJECT_PERMISSION_DEFAULT, isDisabled: true },
    },
  };

  handleFormSubmit = async () => {
    let { item, t, handleOKEditClose } = this.props;
    let { saveAndPrint } = this.state;
    let { setPageLoading } = this.context;
    let dataSearch = this.convertDto(this.state);
// return;
    if (this.validateSubmit(dataSearch)) {
      setPageLoading(true);
      try {
        let result = !item?.id
          ? await createMaintainProposal(dataSearch)
          : await updateMaintainProposal(item?.id, dataSearch);

        const { code, message } = result.data;
        if (isSuccessfulResponse(code)) {
          !item?.id
            ? toast.success(t("general.addSuccess"))
            : toast.success(t("general.updateSuccess"));
          !saveAndPrint && handleOKEditClose();
          this.setState({
            item: result ? result.data?.data : {},
            isPrint: saveAndPrint,
          });
        } else {
          handleThrowResponseMessage(result);
        }
      } catch (error) {
        toast.error(t("general.error"));
      } finally {
        setPageLoading(false);
      }
    }
  };

  componentDidMount = () => {
    let { item = {} } = this.props;
    let roles = getTheHighestRole();
    let { departmentUser, currentUser } = getUserInformation();
    const { isRoleAssetUser, isRoleAssetManager } = roles;

    if (item?.id) {
      this.setState({
        ...roles,
        id: item?.id,
        ...this.convertDtoItem(item)
      }, this.handleCheckPermission);
    }
    else {
      this.setState({
        ...roles,
        status: appConst.STATUS_MAINTAINANCE_PROPOSAL.CHO_TONG_HOP,
        proposedDepartment: isRoleAssetUser
          ? departmentUser
          : null,
        proposedDepartmentId: isRoleAssetUser
          ? departmentUser?.id
          : null,
        receiverDepartment: isRoleAssetManager
          ? departmentUser
          : null,
        department: isRoleAssetManager || isRoleAssetUser
          ? departmentUser
          : null,
        receiverDepartmentId: isRoleAssetManager
          ? departmentUser?.id
          : null,
        nguoiLap: isRoleAssetManager || isRoleAssetUser
          ? {
            personDisplayName: currentUser?.person?.displayName,
            personId: currentUser?.person?.id
          } : null,
      }, this.handleCheckPermission);
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    let {
      permission,
      proposedDepartment,
      departmentUser,
      receiverDepartment,
      isRoleAssetManager,
      department,
    } = this.state;

    if (prevState.proposedDepartment?.id !== proposedDepartment?.id && isRoleAssetManager) {
      if (proposedDepartment?.id !== departmentUser?.id) {
        permission.receiverDepartment.isDisabled = true;
        receiverDepartment = departmentUser;
      } else if (this?.state?.id !== prevState?.id) {
        permission.receiverDepartment.isDisabled = false;
        receiverDepartment = receiverDepartment;
      } else {
        permission.receiverDepartment.isDisabled = false;
        receiverDepartment = null;
      }
      this.setState({ permission, receiverDepartment })
    }
    if (prevState.department?.id !== department?.id) {
      permission.nguoiLap.isDisabled = isRoleAssetManager && !department?.id;
    }
  }

  convertDtoItem = (item) => {
    let data = {
      code: item?.dxMa || "",
      assetVouchers: item?.assets
        ? item?.assets?.map((asset, index) => {
          return {
            ...asset,
            asset: {
              code: asset?.tsMa,
              name: asset?.tsTen,
              id: asset?.tsId,
              model: asset?.tsModel,
              serialNumber: asset?.tsSerialNo,
              managementCode: asset?.tsMaquanly,
              originalCost: asset?.tsNguyenGia,
              carryingAmount: asset?.tsGtcl,
              yearPutIntoUse: asset?.tsNamsd,
              manufacturerName: asset?.tsHangsx,
              yearOfManufacture: asset.tsNamsd,
              unitName: asset.tsDvtTen,
              unitId: asset.tsDvtId,
            },
            formName: asset?.chinhthucId
              ? {
                id: asset?.chinhthucId || null,
                name: asset?.chinhthucText || null,
                code: asset?.chinhthucCode || null,
              }
              : null,
            khoaPhongSuDungName: asset.khoaPhongSuDungText,
            asNote: asset?.ghiChu || "",
            assetId: asset?.tsId,
            stt: index + 1,

          };
        })?.sort((a, b) => (a.tsTen).localeCompare(b.tsTen))
        : [],
      note: item?.dxGhichu || "",
      content: item?.dxNoiDung || "",
      department: {
        id: item?.phongBanLapId || "",
        text: item?.phongBanLapText || ""
      },
      nguoiLap: item?.nguoiLapId ? {
        personId: item?.nguoiLapId || "",
        personDisplayName: item?.nguoiLapText || ""
      } : null,
      proposedDate: item?.dxThoiGian || null,
      proposedDepartment: {
        id: item?.dxPhongBanId,
        text: item?.dxPhongBanText,
        name: item?.dxPhongBanText,
      },
      proposedDepartmentId: item?.dxPhongBanId,
      receiverDepartment: {
        id: item?.tnPhongBanId,
        name: item?.tnPhongBanText,
      },
      receiverDepartmentId: item?.tnPhongBanId,
      status: appConst.listStatusAssetInMaintenanceProposal.find(x => x.code === item?.dxTrangthai) || null,
    };
    return item ? data : {};
  };

  convertDtoAssets = (data) => {
    let dataSearch = [];
    let listData = [...data];
    if (listData?.length > 0) {
      try {
        return listData?.map(ts => ({
          chinhthucId: ts?.formName?.id ? ts?.formName?.id : "",
          chinhthucText: ts?.formName?.name ? ts?.formName?.name : "",
          tsId: ts?.assetId ? ts?.assetId : "",
          tsMa: ts?.asset?.code || "",
          ghiChu: ts?.asNote ? ts?.asNote : "",
          khoaPhongSuDungId: ts?.khoaPhongSuDungId,
          khoaPhongSuDungName: ts?.khoaPhongSuDungName,
        }));
      } catch (e) { }
      return dataSearch;
    } else {
      return [];
    }
  };

  convertDto = (data) => {
    return {
      assets: data?.assetVouchers
        ? this.convertDtoAssets(data?.assetVouchers)
        : [],
      dxGhichu: data?.note ? data?.note : "",
      dxPhongBanId: data?.proposedDepartment?.id
        ? data?.proposedDepartment?.id
        : "",
      dxPhongBanText: data?.proposedDepartment?.text
        ? data?.proposedDepartment?.text
        : "",
      dxThoiGian: data?.proposedDate
        ? convertFromToDateZ(data?.proposedDate).fromDate
        : convertFromToDateZ(new Date()).fromDate,
      dxTrangthai: data?.status?.code,
      dxTrangthaiMota: data?.status
        ? appConst.listStatusAssetInMaintenanceProposal.find(
          (item) => item.code === data?.status
        )?.name
        : "", //"Chờ xử lý"
      orgId: this.currentUser?.org?.id || "",
      tnPhongBanId: data?.receiverDepartment?.id || "",
      tnPhongBanText:
        data?.receiverDepartment?.text || data?.receiverDepartment?.name,
      tsTongBaotri: 0,
      tsTongDx: 0,
      dxNoiDung: data?.content || this.contentRef.current.state.value,
      phongBanLapId: data?.department?.id
        ? data?.department?.id
        : "",
      phongBanLapText: data?.department?.text
        ? data?.department?.text
        : "",
      nguoiLapId: data?.nguoiLap?.personId || null,
      nguoiLapText: data?.nguoiLap?.personDisplayName || null,
    };
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: item || null,
      receiverDepartmentId: item?.id || "",
      assetVouchers: [],
    });
  };

  handleSetDataReceiverDepartment = (data) => {
    this.setState({
      listReceiverDepartment: data,
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  handleClosePrint = () => {
    this.setState({
      isPrint: false,
      saveAndPrint: false,
    }, () => this.props.handleOKEditClose());
  };

  handleOpenAssetPopup = () => {
    let { t } = this.props;
    if (this.state.receiverDepartment) {
      this.setState({
        shouldOpenAssetPopup: true,
      });
    } else {
      toast.warning(t("MaintenanceProposal.warning.emptyReceiverDepartment"));
    }
  };

  handleRowDataCellChange = (rowData, value, source) => {
    let { assetVouchers } = this.state;
    if (variable.listInputName.formName === source) {
      assetVouchers.map((assetVoucher) => {
        if (assetVoucher?.tableData?.id === rowData?.tableData?.id) {
          assetVoucher.formName = value || null;
        }
      });
      this.setState({ assetVouchers });
    }
  };

  handleRowDataCellChangeNote = (rowData, event) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        assetVoucher.asNote = event.target.value;
      }
    });
    this.setState({ assetVouchers: assetVouchers });
  };

  handleSelectAsset = (items) => {
    let { assetVouchers, receiverDepartment, proposedDepartment } = this.state;
    assetVouchers = [];
    if (items?.length > 0) {
      items.map((item, index) => {
        item.assetId = item.asset?.id;
        item.khoaPhongSuDungName = item.asset?.useDepartmentName;
        item.khoaPhongSuDungId = item.asset?.useDepartmentId;
        item.stt = index + 1;

        if (
          proposedDepartment?.id !== receiverDepartment?.id
          || !item.asset?.useDepartmentId
          || item.asset?.useDepartmentId === proposedDepartment?.id
        ) {
          assetVouchers.push(item);
        }
      });
			assetVouchers?.sort(
				(a, b) => (a.asset?.name || a.asset?.tsTen)?.localeCompare(b.asset?.name || b.asset?.tsTen)
			);
      this.setState({ assetVouchers }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning("Chưa có tài sản nào được chọn");
    }
  };
  handleSetListForm = (data) => this.setState({ listForm: data || [] });

  handleRowDataDeleteInList = (rowData) => {
    let { assetVouchers } = this.state;
    let { t } = this.props;
    try {
      assetVouchers?.length > 0 &&
        assetVouchers.splice(rowData?.tableData?.id, 1);
      toast.info(t("general.removeInList"));
    } catch {
      toast.warning(t("general.removeInListError"));
    }
    this.setState({ assetVouchers });
  };

  validateSubmit = (sendData) => {
    let { t } = this.props;
    if (sendData?.assets?.length === 0) {
      toast.warning(t("MaintainPlaning.emptyAssetList"));
      return false;
    }
    else if (sendData?.assets?.length > 0) {
      let check = true;

      check = !sendData?.assets?.some(asset => asset?.khoaPhongSuDungId
        && asset.khoaPhongSuDungId !== sendData.dxPhongBanId
      )

      !check && toast.warning(t("MaintainPlaning.messages.notMatchUseDepartment"))
      return check;
    }
    return true;
  };

  handleSetData = (value, name, source) => {
    if (source === "proposedDepartment") {
      this.setState({ assetVouchers: [] });
    }
    if (source === "department") {
      this.setState({ nguoiLap: null });
    }
    this.setState({ [name]: value });
  }

  handleSelectPerson = (nguoiLap) => {
    this.setState({ nguoiLap })
  }

  handleCheckPermission = () => {
    let { isRoleAssetUser, permission, isRoleOrgAdmin, isRoleAccountant } = this.state;
    let { isView } = this.props;
    if (isView) {
      for (const key of Object.keys(permission)) {
        permission[key].isView = true;
        permission[key].isDisabled = false;
        permission[key].isRequired = false;
      }
    } else {
      // disabled
      if (isRoleAssetUser) {
        permission.proposedDepartment.isDisabled = true;
      }
      if (isRoleAccountant || isRoleOrgAdmin) {
        permission.department.isDisabled = false;
        permission.nguoiLap.isDisabled = false;
      } else {
        permission.department.isDisabled = true;
        permission.nguoiLap.isDisabled = true;
      }

      // required
      permission.proposedDepartment.isRequired = true;
      permission.department.isRequired = true;
      permission.receiverDepartment.isRequired = true;
      permission.department.isRequired = true;
    }

    this.setState({ permission });
  }

  render() {
    let {
      open,
      t,
      i18n,
      isView,
      handleClose,
      classes,
    } = this.props;

    let {
      proposedDate,
      proposedDepartment,
      department,
      nguoiLap,
      status,
      note,
      content,
      receiverDepartment,
      shouldOpenAssetPopup = false,
      assetVouchers,
      code,
      isPrint,
      item,
      permission,
    } = this.state;
    let searchObjectForm = { code: appConst.listCommonObject.HINH_THUC_BAO_TRI.code };
    const { currentOrg } = this.context;
    const { MAINTAIN_PLANING } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    let searchObjectPropsedDepartment = {
      pageIndex: 0,
      pageSize: 1000000,
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code
    };
    let searchObjectPerson = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: department?.id
    }
    let defaultContent = ''
    if (proposedDepartment && department) {
      if (proposedDepartment?.id === department?.id) {
        defaultContent = `Để đảm bảo trang thiết bị hoạt động tốt phục vụ công tác chuyên môn, kéo dài thời gian sử dụng của máy móc. ${spliceString(department.text)} đề xuất bảo trì các trang thiết bị cụ thể như sau:\n`;
      } else {
        defaultContent = `Để đảm bảo trang thiết bị hoạt động tốt phục vụ công tác chuyên môn, kéo dài thời gian sử dụng của máy móc. ${spliceString(department.text)} đề xuất bảo trì các trang thiết bị cho ${spliceString(proposedDepartment.name)} cụ thể như sau:\n`;
      }
    }
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        minWidth: 80,
        hidden: isView,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: {
                  offset: { enabled: true, offset: "10px, 0px" },
                },
              },
            }}
          >
            <IconButton
              size="small"
              disabled={isView}
              onClick={() => this.handleRowDataDeleteInList(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("Asset.stt"),
        field: "stt",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        minWidth: 150,
        align: "left",
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        minWidth: 200,
        align: "left",
      },
      {
        title: t("Asset.formName"), //Hình thức
        field: "formName",
        minWidth: 150,
        align: "left",
        render: (rowData) => isView ? (
          <TextField
            fullWidth
            variant="standard"
            InputProps={{
              readOnly: true,
            }}
            value={rowData?.formName?.name ? rowData?.formName?.name : ""}
          />
        ) : (
          <AsynchronousAutocompleteSub
            listData={this.state.listForm}
            setListData={this.handleSetListForm}
            searchFunction={searchByPageHinhThucNew}
            searchObject={searchObjectForm}
            defaultValue={rowData?.formName ? rowData?.formName : null}
            displayLable={"name"}
            typeReturnFunction="list"
            value={rowData?.formName ? rowData?.formName : null}
            onSelect={(formName) =>
              this.handleRowDataCellChange(
                rowData,
                formName,
                variable.listInputName.formName
              )
            }
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        ),
      },
      {
        title: t("Asset.serialNumber"), //Model
        field: "asset.serialNumber",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.model"), //Model
        field: "asset.model",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.stockKeepingUnitTable"),
        field: "asset.unitName",
        align: "left",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
      },
      // {
      //   title: t("Asset.yearIntoUse"),
      //   field: "asset.yearPutIntoUse",
      //   align: "left",
      //   hidden: isMaintenanceProposalDialog,
      //   minWidth: 80,
      //   cellStyle: {
      //     textAlign: "center",
      //   },
      // },
      {
        title: t("Asset.yearOfManufactureTable"),
        field: "asset.yearOfManufacture",
        align: "left",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturerTable"),
        field: "asset.manufacturerName",
        align: "left",
        minWidth: 150,
      },
      {
        title: t("Asset.useDepartment"),
        field: "khoaPhongSuDungName",
        align: "left",
        minWidth: 150,
        // render: rowData => rowData.khoaPhongSuDungName || rowData.khoaPhongSuDungText
      },
      {
        title: t("Asset.note"),
        field: "asNote",
        minWidth: 250,
        align: "left",
        render: (rowData) => (
          <TextValidator
            rowsMax={2}
            className="w-100"
            onChange={(e) => this.handleRowDataCellChangeNote(rowData, e)}
            type="text"
            name="asNote"
            value={rowData.asNote || ""}
            InputProps={{
              readOnly: isView,
            }}
          />
        ),
      },
    ];

    let titleName = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    );

    const newDate = new Date(item?.dxThoiGian);
    const day = String(newDate?.getUTCDate())?.padStart(2, "0");
    const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
    const year = String(newDate?.getFullYear());

    let dataView = {
      dataPrint: {
        ...item,
        phongBanLapText: spliceString(item?.phongBanLapText),
        tnPhongBanText: spliceString(item?.tnPhongBanText),
        dxNoiDung: item?.dxNoiDung,
        day: item?.dxThoiGian ? day : " ..... ",
        month: item?.dxThoiGian ? month : " ..... ",
        year: item?.dxThoiGian ? year : " ..... ",
        totalQuantity: item?.assets?.reduce((total, item) => total + item?.ckSoluong || 1, 0),
        assets: item?.assets?.map((i, x) => {
          return {
            ...i,
            index: x + 1,
            ckSoluong: i?.ckSoluong || 1,
            tsSerialNoTxt: i?.tsSerialNo ? i?.tsSerialNo : "....",
            tsModelTxt: i?.tsModel ? i?.tsModel : "....",
            tsHangsxTxt: i?.tsHangsx ? i?.tsHangsx : "....",
            tsNamsxTxt: i?.tsNamsx ? i?.tsNamsx : "....",
            tsNamsdTxt: i?.tsNamsd ? i?.tsNamsd : "....",
          };
        }),
      },
      titleName,
    };

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle
          className="cursor-move pb-0"
          id="draggable-dialog-title"
        >
          {t("MaintenanceProposal.title")}
        </DialogTitle>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="py-5"
        >
          <DialogContent className={classes.content}>
            <Grid container spacing={1}>
              <Grid item md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("MaintenanceProposal.votes")}
                  value={code}
                  name="votes"
                  InputProps={{
                    readOnly: permission.code.isView,
                  }}
                  disabled={permission.code.isDisabled}
                />
              </Grid>
              <Grid item md={4} sm={6} xs={12}>
                <ValidatePicker
                  className="w-100"
                  margin="none"
                  id="mui-pickers-date"
                  label={t("MaintenanceProposal.proposedDate")}
                  inputVariant="standard"
                  type="text"
                  autoOk={false}
                  format="dd/MM/yyyy"
                  name={"proposedDate"}
                  readOnly={permission.proposedDate.isView}
                  value={proposedDate}
                  minDate={new Date("01/01/1900")}
                  minDateMessage={
                    "Ngày hết hạn bảo hành không được nhỏ hơn ngày 01/01/1900"
                  }
                  maxDateMessage={
                    "Ngày hết hạn bảo hành không được lớn hơn ngày 01/01/2100"
                  }
                  invalidDateMessage={t("general.invalidDateFormat")}
                  disabled={permission.proposedDate.isDisabled}
                />
              </Grid>
              <Grid item md={4} sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("MaintenanceProposal.status")}
                  searchFunction={() => { }}
                  listData={appConst.listStatusAssetInMaintenanceProposal}
                  displayLable="name"
                  readOnly
                  name="status"
                  onSelect={this.handleSetData}
                  value={status || null}
                />
              </Grid>

              <Grid item md={4} sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <Label isRequired={permission.proposedDepartment.isRequired}>
                      {t("MaintenanceProposal.proposedDepartment")}
                    </Label>
                  }
                  searchFunction={searchByPageDepartmentByOrg}
                  searchObject={{}}
                  listData={this.state.listProposedDepartment}
                  showCode="code"
                  setListData={(value) => this.handleSetData(value, "listProposedDepartment")}
                  displayLable="name"
                  value={proposedDepartment || null}
                  onSelect={(value) => this.handleSetData(
                    value,
                    "proposedDepartment",
                    "proposedDepartment",
                  )}
                  typeReturnFunction="category"
                  readOnly={permission.proposedDepartment.isView}
                  disabled={permission.proposedDepartment.isDisabled}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={4} sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <Label isRequired>
                      {t("MaintenanceProposal.receiverDepartment")}
                    </Label>
                  }
                  searchFunction={searchByPageDepartmentByOrg}
                  searchObject={{ isAssetManagement: true }}
                  listData={this.state.listReceiverDepartment}
                  showCode="code"
                  setListData={this.handleSetDataReceiverDepartment}
                  value={receiverDepartment ? receiverDepartment : null}
                  displayLable={"name"}
                  isNoRenderParent={true}
                  isNoRenderChildren={true}
                  readOnly={permission.receiverDepartment.isView}
                  onSelect={this.handleSelectReceiverDepartment}
                  typeReturnFunction="category"
                  filterOptions={filterOptions}
                  disabled={permission.receiverDepartment.isDisabled}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={4} sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <Label isRequired={permission.department.isRequired}>
                      {t("MaintenanceProposal.department")}
                    </Label>
                  }
                  searchFunction={searchByPageDepartment}
                  searchObject={searchObjectPropsedDepartment}
                  listData={this.state.listDepartment}
                  setListData={(value) => this.handleSetData(value, "listDepartment")}
                  displayLable="text"
                  value={department || null}
                  onSelect={(value) => this.handleSetData(
                    value,
                    "department",
                    "department",
                  )}
                  readOnly={permission.department.isView}
                  disabled={permission.department.isDisabled}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>

              <Grid item md={4} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("MaintenanceProposal.person")}
                    </span>
                  }
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchObjectPerson}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  readOnly={permission.nguoiLap.isView}
                  value={nguoiLap || null}
                  onSelect={nguoiLap => this.handleSelectPerson(nguoiLap)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  disabled={permission.nguoiLap.isDisabled || !department?.id}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={8} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span>{t("MaintenanceProposal.note")}</span>}
                  name="note"
                  value={note}
                  InputProps={{
                    readOnly: isView,
                  }}
                  onChange={this.handleChange}
                />
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span>{t("MaintenanceProposal.content")}</span>}
                  name="content"
                  ref={this.contentRef}
                  multiline
                  minRows={3}
                  InputLabelProps={{
                    shrink: !!defaultContent || !!content
                  }}
                  value={content || defaultContent}
                  InputProps={{
                    readOnly: isView,
                  }}
                  onChange={this.handleChange}
                />
              </Grid>
            </Grid>
            <Grid
              container spacing={2}
              justifyContent="space-between" className="py-10"
            >
              <Grid item xs={12}>
                <Button
                  className="mb-16 mr-16 align-bottom"
                  variant="contained"
                  color="primary"
                  disabled={isView || !receiverDepartment?.id}
                  onClick={this.handleOpenAssetPopup}
                >
                  {t("general.select_asset")}
                </Button>
              </Grid>
              <Grid item xs={12}>
                <MaterialTable
                  data={assetVouchers ? assetVouchers : []}
                  columns={columns}
                  title={""}
                  options={{
                    toolbar: false,
                    search: false,
                    paging: false,
                    selection: false,
                    sorting: false,
                    draggable: false,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div className="w-100">
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                />
              </Grid>
            </Grid>
            {receiverDepartment && shouldOpenAssetPopup && (
              <SelectAssetAllPopup
                t={t}
                i18n={i18n}
                // isRoleAssetManager={isRoleAssetManager}
                open={shouldOpenAssetPopup}
                handleSelect={this.handleSelectAsset}
                statusIndexOrders={this.state.statusIndexOrders}
                handleClose={this.handleAssetPopupClose}
                handoverDepartment={proposedDepartment}
                assetVouchers={assetVouchers}
                receiverDepartmentId={this.state.receiverDepartmentId}
                isMaintenanceProposalDialog
              />
            )}
          </DialogContent>
          <DialogActions>
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.close")}
            </Button>
            {!isView &&
              <>
                <Button
                  className="mb-16 mr-12 ml-0 align-bottom"
                  variant="contained"
                  color="primary"
                  type="submit"
                  onClick={() => this.setState({ saveAndPrint: true })}
                >
                  {t("general.saveAndPrint")}
                </Button>
                <Button
                  className="mb-16 mr-12 ml-0 align-bottom"
                  variant="contained"
                  color="primary"
                  type="submit"
                  onClick={() => this.setState({ saveAndPrint: false })}
                >
                  {t("general.save")}
                </Button>
              </>
            }
          </DialogActions>
        </ValidatorForm>
        {isPrint && (
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleClosePrint}
            open={isPrint}
            item={dataView || item}
            title={t("MaintenanceProposal.title")}
            urls={[
              ...MAINTAIN_PLANING.PROPOSAL.GENERAL,
              ...(MAINTAIN_PLANING.PROPOSAL[currentOrg?.printCode] || []),
            ]}
          />
        )}
      </Dialog>
    );
  }
}

MaintenanceProposalDialog.contextType = AppContext;
export default withStyles(useStyles)(MaintenanceProposalDialog);
