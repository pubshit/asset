import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from "@material-ui/core";
import { PaperComponent, convertNumberPrice, } from "app/appFunction";
import { ValidatorForm } from "react-material-ui-form-validator";
import "react-toastify/dist/ReactToastify.css";
import MaterialTable, { MTableToolbar } from 'material-table';
import { useState } from 'react';
import { useEffect } from 'react';
import { appConst } from 'app/appConst';
import moment from 'moment';

export default function ComparePlaningDialog(props) {
  const { t, open, handleClose, item, assets } = props;
  const [state, setState] = useState({});
  const [completedAssets, setCompletedAssets] = useState([]);
  const [unfulfilledAssets, setUnfulfilledAssets] = useState([]);

  useEffect(() => {
    setState(item);
    //eslint-disable-next-line
    assets?.map(asset => {
      if (appConst.statusAsset.processing === asset.ckTrangthai) {
        unfulfilledAssets.push(asset);
      }
      else if (appConst.statusAsset.approved === asset.ckTrangthai) {
        completedAssets.push(asset);
      }
    })
    setCompletedAssets(completedAssets);
    setUnfulfilledAssets(unfulfilledAssets);
  }, [item])

  const handleFormSubmit = () => {

  };

  let completedColumns = [
    {
      title: t("Asset.stt"),
      field: "stt",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.tableData?.id + 1
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("Asset.model"), //Model
      field: "tsModel",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.maintainPlaningDate"),
      field: "ckDate",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.ckDate
        ? moment(rowData?.ckDate).format("DD/MM/YYYY")
        : "",
    },
    {
      title: t("MaintainPlaning.completedDate"),
      field: "ckNtDate",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.ckNtDate
        ? moment(rowData?.ckNtDate).format("DD/MM/YYYY")
        : "",
    },
    {
      title: t("MaintainPlaning.htCost"),
      field: "ckNtTongChiPhi",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "right",
      },
      render: rowData => rowData?.ckNtTongChiPhi
        ? convertNumberPrice(rowData?.ckNtTongChiPhi)
        : 0,
    },
  ];

  let unfulfilledColumns = [
    {
      title: t("Asset.stt"),
      field: "stt",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.tableData?.id + 1
    },
    {
      title: t("Asset.name"),
      field: "tsTen",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("Asset.model"), //Model
      field: "tsModel",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.maintainPlaningDate"),
      field: "ckDate",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.ckDate
        ? moment(rowData?.ckDate).format("DD/MM/YYYY")
        : "",
    },
    {
      title: t("MaintainPlaning.dkCost"),
      field: "ckNtTongChiPhi",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "right",
      },
      render: rowData => rowData?.ckNtTongChiPhi
        ? convertNumberPrice(rowData?.ckNtTongChiPhi)
        : 0,
    },
  ];

  return (
    <div>
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle
          className='cursor-move pb-0'
          id="draggable-dialog-title"
        >
          <h4>{t("MaintainPlaning.comparePlan")}</h4>
        </DialogTitle>
        <ValidatorForm
          onSubmit={handleFormSubmit}
          className="py-5"
        >
          <DialogContent className='pt-0'>
            <p className='mb-24'>Kế hoạch {state?.khMa ?? ""} - {state?.khNam ?? ""}</p>

            <Grid container spacing={1}>
              <Grid item container md={6} xs={12}>
                <Grid item xs={12}>
                  <div className="flex spaceBetween">
                    <h5 className='text-uppercase font-weight-bold text-primary'>{t("MaintainPlaning.completed")}</h5>
                    <span>Chi phí hoàn thành:&nbsp;{
                      state?.totalCostDaThucHien 
                        ? convertNumberPrice(state?.totalCostDaThucHien)
                        : ""
                    }</span>
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <MaterialTable
                    data={completedAssets}
                    columns={completedColumns}
                    title={""}
                    options={{
                      toolbar: false,
                      search: false,
                      paging: false,
                      selection: false,
                      sorting: false,
                      padding: false,
                      draggable: false,
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                        paddingLeft: "10px!important",
                        paddingRight: "10px!important",
                        textAlign: "center",
                      },
                      minBodyHeight: 300
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div className="w-100">
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                  />
                </Grid>
              </Grid>

              <Grid item container md={6} xs={12}>
                <Grid item xs={12}>
                  <div className="flex spaceBetween">
                    <h5 className='text-uppercase font-weight-bold text-primary'>{t("MaintainPlaning.incomplete")}</h5>
                    <span>{t("MaintainPlaning.incomplete")}:&nbsp;{
                      (state?.totalCostDuKien && state?.khTongtien)
                        ? convertNumberPrice(state?.khTongtien - state?.totalCostDaThucHien)
                        : 0
                    }</span>
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <MaterialTable
                    data={unfulfilledAssets}
                    columns={unfulfilledColumns}
                    title={""}
                    options={{
                      toolbar: false,
                      search: false,
                      paging: false,
                      selection: false,
                      sorting: false,
                      padding: false,
                      draggable: false,
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                        paddingLeft: "10px!important",
                        paddingRight: "10px!important",
                        textAlign: "center",
                      },
                      minBodyHeight: 300
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div className="w-100">
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={handleClose}
            >
              {t("general.close")}
            </Button>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    </div>
  )
}
