import React, { Component } from "react";
import { Button, Dialog, DialogActions, Grid} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContext from "app/appContext";
import { PaperComponent } from "../../Component/Utilities";
import {appConst, PRINT_TEMPLATE_MODEL, STATUS_DON_VI_THUC_HIEN, variable} from "../../../appConst";
import CustomMaterialTable from '../../CustomMaterialTable';
import {searchByPageChoXuLy, searchByPageMaintainContract} from "../MaintainPlaningService"
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { searchByPage } from "../../Department/DepartmentService";
import moment from "moment";
import {convertFromToDate} from "../../../appFunction";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import {searchByTextNew} from "../../Supplier/SupplierService";
import {PrintPreviewTemplateDialog} from "../../Component/PrintPopup/PrintPreviewTemplateDialog";

class AssetPrintDepartmentsDialog extends Component {
    state = {
        pageIndex: 0,
        pageSize: 10,
        keyword: "",
        listAsset: [],
        shouldOpenPrintDialog: false,
        chuKyStatuses: [],
        listTimes: [],
        listSupplier: [],
        listDepartment: [],
    };

    updatePageData = async () => {
        let { t, amNghiemThuIds = [] } = this.props;
        let { setPageLoading } = this.context;
        let searchObject = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            donViThucHienId: this.state.supplier?.id,
            contractId: this.state.contract?.contractId,
            khoaPhongSuDungId: this.state.department?.id,
            amNghiemThuIds: amNghiemThuIds?.join(","),
            chuKyStatuses: this.state.chuKyStatuses?.map(x => x?.code)?.join(","),
            ntTo: convertFromToDate(this.state.ntTo).toDate,
            ntFrom: convertFromToDate(this.state.ntFrom).fromDate,
            lanThu: this.state.times?.code,
        }

        try {
            setPageLoading(true)
            const result = await searchByPageChoXuLy(searchObject);
            const { data, code } = result?.data
            if (appConst.CODE.SUCCESS === code) {
                this.setState({
                    listAsset: data
                })
            }
        } catch (error) {
            toast.error(error)
        } finally {
            setPageLoading(false)
        }
    }

    componentDidMount() {
        this.updatePageData()
    }

    handleChangePage = (event, newPage) => {
        this.setState({
            pageIndex: newPage,
        });
    };

    setRowsPerPage = (event) => {
        this.setState({ pageSize: event.target.value, pageIndex: 0 });
    };

    handleOpenPrintDialog = () => {
        let { amNghiemThuIds } = this.props;
        this.setState({
            item: {
                externalParams: {
                    ...appConst.OBJECT_SEARCH_MAX_SIZE,
                    donViThucHienId: this.state.supplier?.id,
                    contractId: this.state.contract?.contractId,
                    khoaPhongSuDungId: this.state.department?.id,
                    amNghiemThuIds: amNghiemThuIds?.join(","),
                    chuKyStatuses: this.state.chuKyStatuses?.map(x => x?.code)?.join(","),
                    ntTo: convertFromToDate(this.state.ntTo).toDate,
                    ntFrom: convertFromToDate(this.state.ntFrom).fromDate,
                    lanThu: this.state.times?.code,
                }
            },
            shouldOpenPrintDialog: true,
        })
    }

    handleCloseDialog = () => {
        this.setState({
            shouldOpenPrintDialog: false
        })
    }

    handleFormSubmit = () => {
        this.handleOpenPrintDialog()
    }
    
    setData = (value, source) => {
        if (source === "chuKyStatuses") {
            let newStatus = [];
            value?.forEach(x => {
                if (!newStatus.includes(x)) {
                    newStatus.push(x);
                }
            });
            this.setState({
                [source]: newStatus
            }, this.updatePageData);
            return;
        } else if (source === variable.listInputName.contract) {
            let listTimes = [];
            if (value?.performedTimesNumber) {
                listTimes = Array.from({ length: value?.performedTimesNumber }, (_, i) => ({
                    name: `Thứ ${i + 1}`,
                    code: i + 1,
                }))
            }
            console.log(listTimes);
            this.setState({
                listTimes,
                [source]: value,
            }, this.updatePageData);
        }
        this.setState({
            [source]: value
        }, this.updatePageData);
    }
    render() {
        let { open, t } = this.props;
        let {
            pageIndex,
            pageSize = 10,
            listDepartment,
            department,
            listAsset,
            shouldOpenPrintDialog,
            chuKyStatuses,
            ntFrom,
            ntTo,
            times,
            listSupplier,
            supplier,
            contract,
            listTimes,
        } = this.state;

        let departmentSearchObject = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
        };
        let supplierUnitSearchObject = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            typeCodes: [appConst.TYPE_CODES.NCC_BDSC],
            isActive: STATUS_DON_VI_THUC_HIEN.HOAT_DONG.code
        };
        let searchObjectContract = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            supplierId: supplier?.id,
            contractTypeCode: appConst.TYPES_CONTRACT.BTBD
        }

        let columns = [
            {
                title: t("STT"),
                feild: "",
                align: "center",
                minWidth: 100,
                render: (rowData) => (pageSize * pageIndex) + (rowData?.tableData?.id + 1)
            },
            {
                title: t("Asset.name"),
                field: "tsTen",
                minWidth: 200,
            },
            {
                title: t("Asset.managementCode"),
                field: "tsMa",
                minWidth: 150,
            },
            {
                title: t("MaintainPlaning.khMa"),
                field: "khMa",
                minWidth: 120,
            },
            {
                title: t("Asset.contractSignDate"),
                field: "ngayKyHopDong",
                align: "center",
                minWidth: 200,
                render: (rowData) => moment(rowData?.ngayKyHopDong).format("DD/MM/YYYY")
            },
            {
                title: t("Contract.times"),
                field: "lanThu",
                align: "center",
                minWidth: 120,
            },
            {
                title: t("Asset.useDepartment"),
                field: "khoaPhongSuDungText",
                minWidth: 250,
            },
            {
                title: t("Asset.manufacturer"),
                field: "tsHangsx",
                minWidth: 150,
            },
            {
                title: t("Asset.country_manufacturer"),
                field: "tsXuatXu",
                minWidth: 150,
            },
            {
                title: t("Asset.stockKeepingUnitTable"),
                field: "tsDvtTen",
                align: "center",
                minWidth: 150,
            },

        ]

        return (
            <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'lg'} fullWidth>
                <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
                    {t('general.listAsseteAcceptance')}
                </DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                    <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }}>
                        <Grid container spacing={2}>
                            <Grid item md={3} sm={12} xs={12}>
                                <ValidatePicker
                                    autoOk
                                    label={t("MaintainPlaning.ntFrom")}
                                    value={ntFrom ?? null}
                                    name="ntFrom"
                                    onChange={this.setData}
                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                    maxDateMessage={ntTo ? t("MaintainPlaning.messages.maxDateFromDateNtTo") : t("general.maxDateDefault")}
                                    maxDate={ntTo}
                                />
                            </Grid>
                            <Grid item md={3} sm={12} xs={12}>
                                <ValidatePicker
                                    autoOk
                                    label={t("MaintainPlaning.ntTo")}
                                    value={ntTo ?? null}
                                    name="ntTo"
                                    onChange={this.setData}
                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                    minDateMessage={ntFrom ? t("MaintainPlaning.messages.minDateToDateNtFrom") : t("general.minYearDefault")}
                                    minDate={ntFrom}
                                />
                            </Grid>
                            <Grid item md={3} sm={12} xs={12}>
                                <AsynchronousAutocompleteSub
                                    searchFunction={searchByPage}
                                    searchObject={departmentSearchObject}
                                    label={t("MaintainPlaning.useDepartment")}
                                    listData={listDepartment}
                                    typeFunction="default"
                                    displayLable="text"
                                    onSelect={(value) =>
                                        this.setData(value, variable.listInputName.department)
                                    }
                                    value={department ? department : null}
                                />
                            </Grid>
                            <Grid item md={3} sm={12} xs={12}>
                                <AsynchronousAutocompleteSub
                                    multiple
                                    searchFunction={() => {
                                    }}
                                    searchObject={{}}
                                    label={t("MaintainPlaning.status")}
                                    listData={appConst.listStatusChuKy}
                                    setListData={() => {
                                    }}
                                    displayLable="name"
                                    name="chuKyStatuses"
                                    onSelect={this.setData}
                                    value={chuKyStatuses ? chuKyStatuses : null}
                                />
                            </Grid>
                            <Grid item md={3} sm={12} xs={12}>
                                <AsynchronousAutocompleteSub
                                    searchFunction={searchByTextNew}
                                    searchObject={supplierUnitSearchObject}
                                    label={t("MaintainPlaning.implementingAgencies")}
                                    listData={listSupplier}
                                    nameListData="listSupplier"
                                    setListData={this.setData}
                                    displayLable="name"
                                    name="supplier"
                                    onSelect={this.setData}
                                    value={supplier || null}
                                />
                            </Grid>
                            <Grid item md={3} sm={12} xs={12}>
                                <AsynchronousAutocompleteSub
                                    searchFunction={searchByPageMaintainContract}
                                    searchObject={searchObjectContract}
                                    label={t("Contract.title")}
                                    selectedOptionKey="contractId"
                                    name="contract"
                                    onSelect={this.setData}
                                    displayLable="contractName"
                                    showCode="contractCode"
                                    typeReturnFunction="category"
                                    value={contract ? contract : null}
                                />
                            </Grid>
                            {contract?.contractId && (
                                <Grid item md={3} sm={12} xs={12}>
                                    <AsynchronousAutocompleteSub
                                        label={t("MaintainPlaning.times")}
                                        searchFunction={() => {}}
                                        searchObject={{}}
                                        listData={listTimes?.length > 0 ? listTimes : []}
                                        displayLable="name"
                                        name="times"
                                        onSelect={this.setData}
                                        value={times ? times : null}
                                    />
                                </Grid>
                            )}
                            <Grid item md={12} xs={12}>
                                <CustomMaterialTable
                                    className="customPlaning"
                                    data={listAsset?.content || []}
                                    columns={columns}
                                    options={{
                                        paging: true,
                                        pageSize,
                                    }}
                                    localization={{
                                        body: {
                                            emptyDataSourceMessage: `${t(
                                                "general.emptyDataMessageTable"
                                            )}`,
                                        },
                                        pagination: appConst.localizationVi.pagination
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <div className="flex flex-space-between flex-middle">
                            <Button
                                variant="contained"
                                className="mr-12"
                                color="secondary"
                                onClick={() => this.props.handleOKEditClose()}
                            >
                                {t('general.cancel')}
                            </Button>
                            <Button
                                className="mr-16"
                                variant="contained"
                                color="primary"
                                type="submit"
                            >
                                {t('general.print')}
                            </Button>
                        </div>
                    </DialogActions>
                </ValidatorForm>
                {shouldOpenPrintDialog && (
                    <PrintPreviewTemplateDialog
                        t={t}
                        handleClose={this.handleCloseDialog}
                        open={shouldOpenPrintDialog}
                        item={this.state.item}
                        title={t("Phiếu in nghiệm thu bảo trì cho phòng ban")}
                        model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.MAINTAIN_PLANING.ACCEPTANCE.FOR_DEPARTMENT}
                        externalConfig={{
                            params: { ...this.state.item.externalParams },
                        }}
                    />
                )}
            </Dialog>
        );
    }
}

AssetPrintDepartmentsDialog.contextType = AppContext;
export default AssetPrintDepartmentsDialog;
