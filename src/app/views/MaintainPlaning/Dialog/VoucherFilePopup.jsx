import React, { Component } from "react";
import { Button, Card, Dialog, DialogActions, Divider, Grid, Icon, IconButton } from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { EgretProgressBar } from 'egret'
import axios from "axios";
import ConstantList from "app/appConfig";
import FileSaver from 'file-saver';
import GetAppSharpIcon from '@material-ui/icons/GetAppSharp';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContext from "app/appContext";
import ImportExcelDialog from "app/views/Component/ImportExcel/ImportExcelDialog";
import { createDocument, updateDocument, uploadFile } from "../MaintainPlaningService";
import {PaperComponent} from "../../Component/Utilities";
import {appConst} from "../../../appConst";
import {handleCheckFilesSize} from "../../../appFunction";


class VoucherFilePopup extends Component {
    state = {
        name: "",
        code: "",
        note: "",
        shouldOpenImportExcelDialog: false,
        shouldOpenNotificationPopup: false,
        dragClass: "",
        attachments: [],
        files: [],
        queProgress: 0,
        progress: 0,
        documentType: this.props.documentType,
        fileDescriptionIds: [],
    };

    convertDto = () => {
        let {
            name,
            note,
            attachments,
            fileDescriptionIds = [],
        } = this.state;

        return {
            name: name,
            note: note,
            file: attachments[0]?.data,
            fileDescriptionId: fileDescriptionIds[0],
        };
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        let { setPageLoading } = this.context;
        let { id } = this.state;

        if (this.state?.fileDescriptionIds.length <= 0) {
            toast.warning("Chưa có hoặc chưa tải tập tin");
            return;
        }
        if (this.state?.fileDescriptionIds.length < this.state.files?.length) {
            toast.warning("Có tập tin chưa được tải lên");
            return;
        }
        setPageLoading(true);
        let dataState = this.convertDto()
        if (id) {
            updateDocument(dataState, id)
                .then(({ data }) => {
                    toast.success("Thành công");
                    this.props.handleUpdateAssetDocument(data?.data);
                    this.props.handleClose();
                })
                .catch((err) => {
                    toast.error("Có lỗi xảy ra");
                    console.log(err);
                })
                .finally(() => {
                    setPageLoading(false);
                })
        } else {
            createDocument(dataState)
                .then(({ data }) => {
                    toast.success("Thành công");
                    this.props.getAssetDocument(data?.data);
                    this.props.handleClose();
                })
                .catch((err) => {
                    toast.error("Có lỗi xảy ra");
                    console.log(err);
                })
                .finally(() => {
                    setPageLoading(false);
                })
        }
    };

    componentWillMount() {
        let { itemAssetDocument, item = {} } = this.props;
        let { isEditAssetDocument } = item;
        let files = [];
        if (isEditAssetDocument) {
            files.push({
                ...itemAssetDocument?.file,
                isEditAssetDocument
            })
        }
        this.setState({
            ...itemAssetDocument,
            files
        });
    }

    handleDialogClose = () => {
        this.setState({
            shouldOpenNotificationPopup: false,
            shouldOpenImportExcelDialog: false
        })
    }

    handleOKEditClose = () => {
        this.setState({
            shouldOpenNotificationPopup: false,
            shouldOpenImportExcelDialog: false
        })
        this.updatePageData()
    }

    handleFileUploadOnSelect = event => {
        let files = event.target.files;
        this.fileUpload(files[0]).then(() => {
            toast.info("Tải tập tin thành công");
        });
    }

    handleFileSelect = event => {
        let files = event.target.files;
        let arrayFromFiles = Array.from(files);
        let list = [...this.state.files];

        if (handleCheckFilesSize(arrayFromFiles)) {
            return;
        }
        for (const iterator of files) {
            list[0] = {
                ...list[0],
                file: iterator,
                uploading: false,
                error: false,
                progress: 0
            }
        }

        this.setState({
            files: list
        }, () => this.uploadSingleFile(this.state.files?.length - 1));
    };

    handleRemove = () => {
        this.setState({
            files: [],
            attachments: [],
            fileDescriptionIds: []
        });
    };

    fileUpload = (file) => {
        let { setPageLoading } = this.context;
        let formData = new FormData();
        formData.append('uploadfile', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
        setPageLoading(true);

        return uploadFile(formData)
            .then(({ data }) => {
                setPageLoading(false)
                let attachment = data;
                let { attachments, fileDescriptionIds } = this.state;
                attachments.push([attachment]);
                fileDescriptionIds = [attachment?.data?.id];
                this.setState({ attachments, fileDescriptionIds });
                toast.info("Tải tập tin thành công");
            })
            .catch(() => {
                toast.error("Lỗi tải file");
                setPageLoading(false)
            })
    }

    uploadSingleFile = async (index) => {
        let allFiles = [...this.state.files];
        let file = this.state.files[index];
    
        await this.fileUpload(file?.file);
    
        allFiles[index] = { ...file, uploading: true, success: true, error: false };
    
        this.setState({
          files: [...allFiles]
        });
    };

    handleViewDocument = index => {
        let file = this.state.files[index];
        let contentType = file?.contentType;
        let fileName = file?.name;
        const listFileType = appConst.TYPES_FILE;
        const url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/document/" + file?.id;

        axios.get(url, { responseType: 'arraybuffer' }).then((successResponse) => {
            let document = successResponse.data;
            let file = new Blob([document], { type: contentType });

            if (
              file?.type === listFileType.PDF
              || file?.type === listFileType.TEXT
              || listFileType.IMAGE_ARRAY.includes(file?.type)
            ) {
                let fileURL = URL.createObjectURL(file, fileName);
                return window.open(fileURL);
            } else {
                toast.warning('Định dạng tệp tin không thể xem. Hãy tải xuống')
            }
        })

    }

    handleDownloadDocument = index => {
        let file = this.state.files[index];
        let contentType = file?.contentType;
        let fileName = file?.name;
        const url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/document/" + file?.id;
        axios.get(url, { responseType: 'arraybuffer' }).then((successResponse) => {
            let document = successResponse.data;
            let file = new Blob([document], { type: contentType });
            return FileSaver.saveAs(file, fileName);
        });
    }

    render() {
        let { open, t, i18n } = this.props;
        let { files } = this.state;
        let isEmpty = files.length === 0;
        let {
            name,
            note,
            shouldOpenImportExcelDialog,
        } = this.state;

        return (
            <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
                <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
                    {t('general.saveUpdate')}
                </DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                    {shouldOpenImportExcelDialog && (
                        <ImportExcelDialog
                            t={t}
                            i18n={i18n}
                            handleClose={this.handleDialogClose}
                            open={shouldOpenImportExcelDialog}
                            handleOKEditClose={this.handleOKEditClose}
                        />
                    )}
                    <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }}>
                        <Grid className="" container spacing={1}>
                            {/* <Grid item md={3} sm={12} xs={12} hidden>
                                //Mã hồ sơ tài sản
                                <div className="mt-24"><label
                                    style={{ fontWeight: 'bold' }}>{t('AssetFile.code')} : </label> {this.state.code}</div>
                            </Grid> */}

                            <Grid item md={4} sm={12} xs={12}>
                                <TextValidator
                                    className="w-100"
                                    label={
                                        <span>
                                            <span className="colorRed">*</span>
                                            {t('AssetFile.name')}
                                        </span>
                                    }
                                    onChange={this.handleChange}
                                    type="text"
                                    name="name"
                                    value={name}
                                    validators={["required"]}
                                    errorMessages={[t('general.required')]}
                                />
                            </Grid>

                            <Grid item md={8} sm={12} xs={12}>
                                <TextValidator
                                    className="w-100"
                                    label={t('AssetFile.description')}
                                    onChange={this.handleChange}
                                    type="text"
                                    name="note"
                                    value={note}
                                />
                            </Grid>

                        </Grid>
                        <div className="mt-12">
                            {this.state?.files?.length === 0 && <div className="flex flex-wrap">
                                <label htmlFor="upload-single-file">
                                    <Button
                                        size="small"
                                        className="capitalize"
                                        component="span"
                                        variant="contained"
                                        color="primary"
                                    >
                                        <div className="flex flex-middle">
                                            <span>{t('general.select_file')}</span>
                                        </div>
                                    </Button>
                                </label>
                                <input
                                    className="display-none"
                                    onChange={this.handleFileSelect}
                                    id="upload-single-file"
                                    type="file"
                                />
                                <div className="px-16"></div>
                            </div>}
                            <Card className="mb-24" elevation={2}>
                                <div className="p-16">
                                    <Grid
                                        container
                                        spacing={2}
                                        justifyContent="center"
                                        alignItems="center"
                                        direction="row"
                                    >
                                        <Grid item lg={4} md={4}>
                                            {t('general.file_name')}
                                        </Grid>
                                        <Grid item lg={4} md={4}>
                                            {t('general.size')}
                                        </Grid>
                                        <Grid item lg={4} md={4}>
                                            {t('general.action')}
                                        </Grid>
                                    </Grid>
                                </div>
                                <Divider/>

                                {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

                                {files.map((item, index) => {
                                    let { success, error, progress, isEditAssetDocument } = item;
                                    return (
                                        <div className="px-16 py-8" key={index}>
                                            <Grid
                                                container
                                                spacing={2}
                                                justifyContent="center"
                                                alignItems="center"
                                                direction="row"
                                            >
                                                <Grid item lg={4} md={4} sm={12} xs={12} className="text-ellipsis">
                                                    {item?.name ?? item?.file?.name}
                                                </Grid>
                                                    <Grid item lg={1} md={1} sm={12} xs={12}>
                                                    {/* {(0 / 1024 / 1024).toFixed(1)} MB */}
                                                    {((item?.contentSize ? item?.contentSize : item?.file?.size) / 1024 / 1024).toFixed(1)} MB
                                                    </Grid>
                                               
                                                {(isEditAssetDocument || success) ? (
                                                    <Grid item lg={2} md={2} sm={12} xs={12}>
                                                        <EgretProgressBar value={100}></EgretProgressBar>
                                                    </Grid>
                                                ) : (
                                                    <Grid item lg={2} md={2} sm={12} xs={12}>
                                                        <EgretProgressBar value={progress}></EgretProgressBar>
                                                    </Grid>
                                                )}
                                                <Grid item lg={1} md={1} sm={12} xs={12}>
                                                    {error && <Icon fontSize="small" color="error">error</Icon>}
                                                </Grid>
                                                <Grid item lg={4} md={4} sm={12} xs={12}>
                                                    <div className="flex">
                                                        {!isEditAssetDocument && (
                                                            <IconButton disabled={success} size="small" title={t('general.upload')}
                                                                onClick={() => this.uploadSingleFile(index)}>
                                                                <Icon color={success ? "disabled" : "primary"} fontSize="small">cloud_upload</Icon>
                                                            </IconButton>
                                                        )}
                                                        {isEditAssetDocument && (<IconButton size="small" title={t('general.viewDocument')}
                                                            onClick={() => this.handleViewDocument(index)}>
                                                            <Icon fontSize="small" color="primary">visibility</Icon>
                                                        </IconButton>
                                                        )}

                                                        {isEditAssetDocument && (<IconButton size="small" title={t('general.downloadDocument')}
                                                            onClick={() => this.handleDownloadDocument(index)}>
                                                            <Icon fontSize="small" color="default"><GetAppSharpIcon /></Icon>
                                                        </IconButton>
                                                        )}
                                                        <IconButton size="small" title={t('general.removeDocument')}
                                                            onClick={() => this.handleRemove()}>
                                                            <Icon fontSize="small" color="error">delete</Icon>
                                                        </IconButton>

                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    );
                                })}
                            </Card>
                        </div>

                    </DialogContent>
                    <DialogActions>
                        <div className="flex flex-space-between flex-middle">
                            <Button
                                variant="contained"
                                className="mr-12"
                                color="secondary"
                                onClick={() => this.props.handleClose()}
                            >
                                {t('general.cancel')}
                            </Button>
                            <Button
                                className="mr-16"
                                variant="contained"
                                color="primary"
                                type="submit"
                            >
                                {t('general.save')}
                            </Button>

                        </div>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

VoucherFilePopup.contextType = AppContext;
export default VoucherFilePopup;
