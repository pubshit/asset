import {
  Button, FormControl,
  Grid, Input, InputAdornment,
  TextField,
} from "@material-ui/core";
import React, { useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import {
	convertFromToDate,
	convertNumberPrice,
	filterOptions,
	getTheHighestRole, getUserInformation,
	handleKeyDown,
	handleKeyDownIntegerOnly, handlePreventOnEnter
} from "app/appFunction";
import { appConst, listMaintainPlaningStatus, STATUS_SUPPLIER } from "app/appConst";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import ValidatedDatePicker from "app/views/Component/ValidatePicker/ValidatePicker";
import moment from "moment";
import { searchByPageHinhThucNew } from "../MaintainPlaningService";
import { Autocomplete } from "@material-ui/lab";
import { searchByPage, searchByPageDepartmentNew } from "app/views/Department/DepartmentService";
import { searchByTextNew } from "app/views/Supplier/SupplierService";
import { getListOrgManagementDepartment, getManagementDepartment } from "app/views/Asset/AssetService";
import SearchIcon from "@material-ui/icons/Search";
import { Label } from "../../Component/Utilities";

const HeaderPlanDialog = (props) => {
  const {
    t,
    roles,
    isView,
    onChange,
    itemData,
    khStatus,
    listData,
    filterState,
    setFilterData,
    setOpenTaiSanDialog,
    handleChangeStatus,
  } = props;
  const {
    isRoleAssetManager = false,
  } = roles;
  const { organization } = getUserInformation();
  const { commonObject, supplier, useDepartment, keyword } = filterState;
  const [listSupplier, setListSupplier] = useState([]);
  const [listUseDepartment, setListUseDepartment] = useState([]);
  let commonSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };
  const hinhThucBTBDSearchObject = {
    code: appConst.listCommonObject.HINH_THUC_BAO_TRI.code
  }
  const searchObjectReceiverDepartment = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  };

  const handleOpenAssetDialog = () => {
    setOpenTaiSanDialog(true);
  };

  const handleGetSupplierUnit = async () => {
    try {
      let res = await searchByTextNew({
        ...commonSearchObject,
        isActive: STATUS_SUPPLIER.HOAT_DONG.code,
        typeCodes: [appConst.TYPE_CODES.NCC_BDSC]
      })

      if (res?.data) {
        const { currentUser } = getTheHighestRole();
        let newArray = res?.data?.content ?? []
        newArray?.push(currentUser?.org);
        setListSupplier(newArray);
      }
    }
    catch (e) {
    }
  };

  const handleFocusSupplierUnit = () => {
    if (listSupplier?.length > 0) return;

    handleGetSupplierUnit();
  };

  return (
    <Grid item container spacing={1} onKeyDown={handlePreventOnEnter}>
      {/* Dong 1 */}
      <Grid item xs={4} md={2}>
        <TextValidator
          label={
            <Label isRequired>{t("MaintainPlaning.tuNam")}</Label>
          }
          className="w-100"
          type="number"
          name="tuNam"
          InputProps={{
            readOnly: isView,
          }}
          value={itemData?.tuNam || ""}
          onKeyDown={handleKeyDownIntegerOnly}
          onChange={(e) => onChange(
            e?.target?.value,
            e?.target?.name,
          )}
          validators={
            itemData?.denNam
              ? ["required", `maxNumber:${itemData?.denNam}`, `minNumber:1900`]
              : ["required"]
          }
          errorMessages={[t("general.required"), t("MaintainPlaning.maxKhNam"), t(`general.minYearDefault`)]}
        />
      </Grid>
      <Grid item xs={4} md={2}>
        <TextValidator
          label={
            <Label isRequired>{t("MaintainPlaning.denNam")}</Label>
          }
          className="w-100"
          type="number"
          name="denNam"
          InputProps={{
            readOnly: isView,
          }}
          value={itemData?.denNam || ""}
          onKeyDown={handleKeyDownIntegerOnly}
          onChange={(e) => onChange(
            e?.target?.value,
            e?.target?.name,
          )}
          validators={
            itemData?.tuNam
              ? ["required", `minNumber:${itemData?.tuNam}`, `minNumber:1900`]
              : ["required"]
          }
          errorMessages={[t("general.required"), t("MaintainPlaning.minKhNam"), t(`general.minYearDefault`)]}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <AsynchronousAutocompleteSub
          searchFunction={() => { }}
          searchObject={{}}
          label={
            <span>
              <span className="colorRed">* </span>{" "}
              <span>{t('MaintainPlaning.status')}</span>
            </span>
          }
          listData={listMaintainPlaningStatus}
          setListData={() => { }}
          displayLable="name"
          name="status"
          onSelect={handleChangeStatus}
          readOnly={isView}
          value={itemData?.status ? itemData.status : null}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required"),]}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <ValidatedDatePicker
          autoOk
          fullWidth
          margin="none"
          readOnly={isView}
          inputVariant="standard"
          format="dd/MM/yyyy"
          KeyboardButtonProps={{
            "aria-label": "change date",
          }}
          label={
            <span>
              {khStatus?.isDaDuyet && <span className="colorRed">*</span>}
              {t("MaintainPlaning.approvalDate")}
            </span>
          }
          value={itemData?.khNgayDuyet ? itemData?.khNgayDuyet : null}
          onChange={(date) => {
            onChange(
              convertFromToDate(date).fromDate,
              "khNgayDuyet",
            );
          }}
          disabled={!khStatus?.isDaDuyet}
          maxDate={new Date()}
          maxDateMessage={t("general.maxDateNow")}
          invalidDateMessage={t("general.invalidDateFormat")}
          validators={khStatus?.isDaDuyet ? ["required"] : []}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={8} md={4}>
        <TextValidator
          label={
            <span>
              <span className="colorRed">* </span>
              <span>{t("purchasePlaning.name")}</span>
            </span>
          }
          className="w-100"
          type="text"
          InputProps={{
            readOnly: isView,
          }}
          name="khTen"
          value={itemData?.khTen}
          onChange={(e) => onChange(
            e?.target?.value,
            e?.target?.name,
          )}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <AsynchronousAutocompleteSub
          label={
            <span>
              <span className="colorRed">*</span>
              <span>{t("MaintenanceProposal.receiverDepartment")}</span>
            </span>
          }
          searchFunction={isRoleAssetManager ? getListOrgManagementDepartment : searchByPageDepartmentNew}
          searchObject={isRoleAssetManager ? {} : searchObjectReceiverDepartment}
          typeReturnFunction={isRoleAssetManager ? "list" : "category"}
          value={itemData?.khPhongBan || null}
          displayLable="name"
          isNoRenderParent
          isNoRenderChildren
          onSelect={(value) => onChange(
            value,
            "khPhongBan",
          )}
          filterOptions={filterOptions}
          disabled={isRoleAssetManager || !khStatus?.isMoiTao || props?.amDexuatIds?.length}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={12} md={8}>
        <TextValidator
          label={t("Ghi chú")}
          className="w-100"
          type="text"
          InputProps={{
            readOnly: isView,
          }}
          name="khGhiChu"
          value={itemData?.khGhiChu}
          onChange={(e) => onChange(
            e?.target?.value,
            e?.target?.name,
          )}
        />
      </Grid>

      {/* Dong 2 */}
      <Grid item xs={12} md={12}></Grid>
      <Grid item xs={12} md={2}>
        <Button
          className="mt-12 "
          variant="contained"
          color="primary"
          disabled={
            isView
            || Boolean(!khStatus.isMoiTao && itemData?.status)
            || !itemData?.khPhongBan?.id
          }
          onClick={() => handleOpenAssetDialog()}
        >
          {t("general.add_asset")}
        </Button>
      </Grid>
      <Grid item xs={12} md={2}>
        <AsynchronousAutocompleteSub
          searchFunction={searchByPageHinhThucNew}
          searchObject={hinhThucBTBDSearchObject}
          label={t("MaintainPlaning.implementingForm")}
          listData={listData?.listHinhThuc}
          // setListData={setListCommonObject}
          displayLable="name"
          typeReturnFunction="list"
          value={commonObject ? commonObject : null}
          onSelect={(value) => setFilterData(
            value,
            "commonObject",
          )}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <Autocomplete
          options={listSupplier}
          value={supplier ? supplier : null}
          onFocus={handleFocusSupplierUnit}
          onChange={(e, value) => setFilterData(
            value,
            "supplier"
          )}
          noOptionsText={t("general.noOption")}
          getOptionLabel={option => option?.name}
          renderInput={params => (
            <TextField
              {...params}
              label={t("MaintainPlaning.implementingAgencies")}
              value={supplier?.name || ""}
            />
          )}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <AsynchronousAutocompleteSub
          searchFunction={searchByPage}
          searchObject={commonSearchObject}
          label={t("MaintainPlaning.useDepartment")}
          listData={listUseDepartment}
          setListData={setListUseDepartment}
          displayLable="text"
          onSelect={(value) => setFilterData(
            value,
            "useDepartment"
          )}
          value={useDepartment ? useDepartment : null}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <FormControl fullWidth>
          <Input
            className="search_box w-100 mt-16"
            onChange={(e) => setFilterData(
              e.target.value,
              "keyword"
            )}
            placeholder={t("MaintainPlaning.filter")}
            value={keyword || ""}
            id="search_box"
            startAdornment={
              <InputAdornment position="end">
                <SearchIcon className="searchTable" />
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default HeaderPlanDialog;
