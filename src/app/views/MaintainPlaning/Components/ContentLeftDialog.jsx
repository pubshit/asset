import { Grid, Icon, TablePagination, IconButton } from "@material-ui/core";
import React, {useEffect, useState} from "react";
import CustomMaterialTable from "../../CustomMaterialTable";
import { appConst } from "app/appConst";
import { LightTooltip } from "app/appFunction";
import { useTranslation } from 'react-i18next';

function MaterialButton(props) {
  const { item, khStatus, itemData } = props;
  const { t } = useTranslation();
  const isSelectedRow = item.tsId === itemData?.tsId;

  return (
    <div>
      <IconButton
        disabled={!khStatus.isMoiTao}
        size="small"
        onClick={() => props.onSelect(item, appConst.active.delete)}
      >
        <Icon
          fontSize="small"
          color={khStatus.isMoiTao ? "error" : "disabled"}
        >
          delete
        </Icon>
      </IconButton>
      {isSelectedRow  && (
        <LightTooltip
          title={t("MaintainPlaning.copy")}
          placement="right"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "-15px, 10px" } },
            },
          }}
        >
          <IconButton
            disabled={item.isFinished}
            size="small"
            onClick={() => props.onSelect(item, appConst.active.copy)}
            // disabled={khStatus?.isDaBaoGia}
          >
            <Icon
              fontSize="small"
              color="primary"
            >
              content_copy
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

const ContentLeftDialog = (props) => {
  const {
    t,
    khStatus,
    isView,
    onRowClick,
    onSelectionChange,
    removeAssetInlist,
    tempMaintainPlanings,
    itemData,
    onAddAsset,
  } = props;

  const [selectedRow, setSelectedRow] = useState("");

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "center",
      hidden: isView,
      width: "250",
      headerStyle: {
        whiteSpace: "nowrap",
      },
      render: (rowData) =>
        !props?.item?.isView && (
          <MaterialButton
            itemData={itemData}
            khStatus={khStatus}
            item={rowData}
            onSelect={(rowData, method) => {
              if (appConst.active.delete === method) {
                removeAssetInlist(rowData);
              } else if (appConst.active.copy === method) {
                onAddAsset(rowData);
              } else {
                alert("Call Selected Here:" + rowData?.id);
              }
            }}
          />
        ),
    },
    {
      title: t("MaintainPlaning.assetCode"),
      field: "tsMa",
      align: "left",
      minWidth: "120px",
    },
    {
      title: t("MaintainPlaning.assetName"),
      field: "tsTen",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("MaintainPlaning.managementCode"),
      field: "tsMaquanly",
      align: "left",
      minWidth: "120px",
    },
    {
      title: t("MaintainPlaning.model"),
      field: "tsModel",
      align: "left",
      minWidth: "120px",
    },
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      align: "left",
      minWidth: "120px",
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "tsNamsx",
      align: "center",
      minWidth: "130px",
    },
    {
      title: t("Asset.yearPutIntoUse"),
      field: "tsNamsd",
      align: "center",
      minWidth: "130px",
    },
    {
      title: t("MaintainPlaning.useDepartment"),
      field: "khoaPhongSuDungText",
      align: "left",
      minWidth: "180px",
    },
    {
      title: t("MaintainPlaning.cycle"),
      field: "ckSoluong",
      align: "left",
      minWidth: "120px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.status"),
      field: "tsTrangThaiTen",
      align: "left",
      minWidth: "120px",
    },
  ];
  const handleRowClick = (e, rowData) => {
    onRowClick(rowData);
    setSelectedRow(rowData);
  };

  const handleGetRowColor = rowData => {
    if (rowData.isFinished) {
      return "rgb(209 221 224)";
    } else {
      return selectedRow?.tsId === rowData.tsId ? "#EEE" : "#FFF";
    }
  }
  return (
    <Grid item md={8} xs={12}>
      <CustomMaterialTable
        className="customPlaning"
        data={[...tempMaintainPlanings] || []}
        columns={columns}
        columnActions={[]}
        onRowClick={(e, rowData) => handleRowClick(e, rowData)}
        options={{
          selection: !isView,
          paging: true,
          pageSize: 10,
          toolbar: false,
          maxBodyHeight: 617,
          pageSizeOptions: appConst.rowsPerPageOptions.table,
          rowStyle: (rowData) => ({
            backgroundColor: handleGetRowColor(rowData),
          }),
					selectionProps: (rowData) => ({
						disabled: selectedRow?.tsId === rowData.tsId
					}),
        }}
        localization={{
          pagination: appConst.localizationVi.pagination
        }}
        onSelectionChange={(selectedRows, rowData) => {
          onSelectionChange(selectedRows, rowData);
        }}
      />
    </Grid>
  );
};

export default ContentLeftDialog;
