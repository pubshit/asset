import React, { useContext, useEffect, useState } from 'react';
import {
  Dialog,
  Grid,
  DialogActions,
  DialogTitle,
  DialogContent,
  Button,
  TablePagination,
  Input,
  InputAdornment,
} from '@material-ui/core';
import SearchIcon from "@material-ui/icons/Search";
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CustomMaterialTable from '../../CustomMaterialTable';
import { searchByPageAsset } from '../MaintainPlaningService';
import { appConst } from '../../../appConst';
import AsynchronousAutocompleteSub from '../../utilities/AsynchronousAutocompleteSub'
import { searchByPage as SearchByPageDepartment } from "../../Department/DepartmentService";
import { ValidatorForm } from 'react-material-ui-form-validator';
import AppContext from 'app/appContext';
import { PaperComponent } from 'app/appFunction';

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const AddAssetDialog = (props) => {
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const { open, amDexuatIds, handleClose, onAddAsset, maintainPlanings = [] } = props;
  const [listAsset, setListAsset] = useState([]);
  const [assetVouchers, setAssetVouchers] = useState(maintainPlanings);
  const [totalElements, setTotalElements] = useState(0);
  const [checkAll, setCheckAll] = useState(0);
  const [listDepartment, setListDepartment] = useState([]);
  const [query, setQuery] = useState({
    pageIndex: 0,
    pageSize: 10,
    keyword: "",
    useDepartment: null,
  });
  const searchObjectDepartment = {
    pageIndex: 1,
    pageSize: 9999,
  };

  useEffect(() => {
    handleSearchByPageAsset();
  }, [query.pageIndex, query.pageSize, query.useDepartment]);

  const handleSearchByPageAsset = () => {
    const newData = [];
    if (amDexuatIds?.length > 0) {
      amDexuatIds.forEach((item) => newData.push(item.id));
    }
    const sendData = {
      pageIndex: query.pageIndex + 1,
      pageSize: query.pageSize,
      amDexuatIds: newData,
      assetClass: appConst.assetClass?.TSCD,
      keyword: query.keyword?.trim(),
      useDepartmentId: query.useDepartment?.id,
      managementDepartmentId: props.managementDepartmentId,
    };
    setPageLoading(true);
    try {
      searchByPageAsset(sendData).then(({ data }) => {
        if (data?.code === appConst.CODE.SUCCESS && data?.data?.content) {
          // eslint-disable-next-line
          data?.data?.content?.map(item => {
            let existItem = assetVouchers?.some(asset =>
              asset.tsId === item.id
              || asset.id === item.id
            )
            item.tableData = {
              ...item.tableData,
              checked: existItem,
            }
          })
          setListAsset(data?.data?.content);
          setTotalElements(data?.data?.totalElements ?? 0);
        }
        setPageLoading(false);
      });
    } catch (error) {
      setPageLoading(false);
      toast.error(t("general.error"));
    }
  };

  const handleChangePage = (event, newPage) => {
    setQuery({
      ...query,
      pageIndex: newPage,
    });
  };

  const setRowsPerPage = (event) => {
    setQuery({ ...query, pageSize: event.target.value, pageIndex: 0 });
  };

  let columns = [
    {
      title: t('MaintainPlaning.assetCode'),
      field: 'code',
      align: 'center',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.assetName'),
      field: 'name',
      align: 'left',
      minWidth: '200px',
    },
    {
      title: t('MaintainPlaning.model'),
      field: 'model',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.seri'),
      field: 'serialNumber',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t("Asset.yearOfManufactureTable"),
      field: "yearOfManufacture",
      align: "center",
      minWidth: "70px",
    },
    {
      title: t("Asset.yearIntoUse"),
      field: "yearPutIntoUse",
      align: "center",
      minWidth: "70px",
    },
    {
      title: t('MaintainPlaning.useDepartment'),
      field: 'useDepartmentName',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.status'),
      field: 'statusName',
      align: 'left',
      minWidth: '120px',
    },
  ];

  const handleCheck = (rows, rowData) => {
    let newArray = assetVouchers ? assetVouchers : [];
    let existItem = newArray?.find(item =>
      item?.tsId === rowData?.id
      || item.id === rowData?.id
    )

    if (rowData?.id) {                          // Case normal select
      if (existItem) {
        newArray.splice(newArray.indexOf(existItem), 1);
      }
      else {
        newArray.push(rowData);
      }
      handleCheckTable(!Boolean(existItem), rowData);
    }
    else {                                      // Case select all
      if (rows?.length === listAsset?.length) { // select
        rows?.map(ts => {
          existItem = newArray?.find(item =>
            item?.tsId === ts?.id
            || item.id === ts?.id
          )
          !existItem && newArray.push(ts);
        })
        handleCheckTable(true);
      }
      else {                                    // unselect
        listAsset?.map(ts => {
          let newItem = newArray?.find(item =>
            item?.tsId === ts?.id
            || item.id === ts?.id
          )
          newArray.splice(newArray.indexOf(newItem), 1);
        })
        handleCheckTable(false)
      }
    }

    setAssetVouchers([...newArray])
  };

  const handleSelectDepartment = (useDepartment) => {
    setQuery({ ...query, useDepartment, pageIndex: 0 })
  };

  const handleChange = (event) => {
    setQuery({ ...query, [event?.target?.name]: event?.target?.value });
  };

  const handleKeyDownEnterSearch = (event) => {
    if (event.key === appConst.KEY.ENTER) {
      handleSearchByPageAsset();
    }
  };

  const handleKeyUp = (event) => {
    if (!event?.target?.value) {
      handleSearchByPageAsset();
    }
  };

  const handleCheckTable = (checked, rowData) => {
    if (rowData?.id) {
      setListAsset([
        ...listAsset.map(item => {
          if (item.id === rowData?.id) {
            item.tableData = {
              ...item.tableData,
              checked,
            }
          }
          return item
        })
      ])
    }
    else {
      setListAsset([
        ...listAsset.map(item => {
          item.tableData = {
            ...item.tableData,
            checked,
          }
          return item
        })
      ])

    }
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth>
      <DialogTitle className='cursor-move pb-0' id="draggable-dialog-title">
        <h4 className="">{t('MaintainPlaning.selectAsset')}</h4>
      </DialogTitle>

      <DialogContent>
        <ValidatorForm>
          <div className="mp-0">
            <Grid container spacing={2} justifyContent="space-between">
              <Grid item container md={12} sm={12} xs={12} spacing={2}>
                <Grid item md={4} sm={4} xs={12}>
                  <Input
                    label={t("Asset.enterSearch")}
                    type="text"
                    name="keyword"
                    value={query?.keyword || ""}
                    onChange={handleChange}
                    onKeyDown={handleKeyDownEnterSearch}
                    onKeyUp={handleKeyUp}
                    className="w-100 mt-16"
                    id="search_box"
                    placeholder={t("Asset.enterSearch")}
                    startAdornment={
                      <InputAdornment>
                        <SearchIcon
                          onClick={handleSearchByPageAsset}
                          className='searchTable'
                        />
                      </InputAdornment>
                    }
                  />
                </Grid>
                <Grid item md={4} sm={4} xs={12}></Grid>
                <Grid item md={4} sm={4} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("general.filterByDepartment")}
                    searchFunction={SearchByPageDepartment}
                    searchObject={searchObjectDepartment}
                    listData={listDepartment}
                    setListData={setListDepartment}
                    defaultValue={query?.useDepartment ? query?.useDepartment : null}
                    displayLable={"text"}
                    value={query?.useDepartment ? query?.useDepartment : null}
                    onSelect={handleSelectDepartment}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              </Grid>

              <Grid item md={12} xs={12}>
                <CustomMaterialTable
                  className="customPlaning"
                  data={listAsset || []}
                  columns={columns}
                  columnActions={[]}
                  // onRowClick={(e, rowData) => handleRowClick(e, rowData)}
                  options={{
                    selection: true,
                  }}
                  onSelectionChange={handleCheck}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[10, 25, 50, 100]}
                  component="div"
                  labelRowsPerPage={t('general.rows_per_page')}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`
                  }
                  count={totalElements}
                  rowsPerPage={query.pageSize}
                  page={query.pageIndex}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={setRowsPerPage}
                />
              </Grid>
            </Grid>
          </div>
        </ValidatorForm>
      </DialogContent>
      <DialogActions>
        <div className="flex flex-space-between flex-middle mt-12">
          <Button variant="contained" className="mr-12" color="secondary" onClick={() => handleClose()}>
            {t('general.close')}
          </Button>
          <Button
            variant="contained"
            style={{ marginRight: '15px' }}
            color="primary"
            onClick={() => {
              onAddAsset(assetVouchers);
            }}
          >
            {t('general.select')}
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
};

export default AddAssetDialog;
