import { Button, Grid, TextField } from "@material-ui/core";
import React from "react";
import { TextValidator } from "react-material-ui-form-validator";
import UnitDialog from "../UnitDialog";

const AddInfoAsset = (props) => {
  const {
    itemData,
    unit,
    handleSelect,
    t,
    i18n,
    shouldOpenUnitDialog,
    renderDatePickers,
    handleClickOpen,
  } = props;

  const handleChange = (event, source) => {
    console.log({ event });
    console.log({ source });
  };

  return (
    <>
      <Grid item md={4} xs={12} style={{ border: "1px solid" }}>
        <Grid item md={12} xs={12}>
          <TextField
            label="Chu kỳ"
            className="mt-8"
            width="100%"
            name="ckSoluong"
            type="number"
            InputLabelProps={{ shrink: true }}
            value={itemData?.ckSoluong}
            placeholder="Bao nhiêu lần/năm"
            onChange={handleChange}
          />
        </Grid>
        {renderDatePickers()}

        <Grid
          container
          item
          md={12}
          sm={12}
          xs={12}
          className="mt-10"
          justifyContent="space-between"
        >
          <Grid item md={10} sm={10} xs={10}>
            <TextValidator
              className="w-100"
              size="small"
              InputProps={{
                readOnly: true,
              }}
              label={
                <span>
                  <span className="colorRed">* </span>{" "}
                  <span>{t("Asset.performingUnit")}</span>
                </span>
              }
              value={unit ? unit.name : ""}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              onChange={handleChange}
            />
          </Grid>
          <Grid item md={2} sm={2} xs={2}>
            <Button
              size="small"
              style={{ float: "right" }}
              className="w-85 mt-12"
              variant="contained"
              color="primary"
              onClick={() => handleClickOpen()}
              // disabled={!props?.isAddAsset}
            >
              {t("general.select")}
            </Button>
            {shouldOpenUnitDialog && (
              <UnitDialog
                open={shouldOpenUnitDialog}
                onClose={() => {}}
                unit={unit}
                handleSelect={handleSelect}
                t={t}
              />
            )}
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            label={t("Asset.unitPrice")}
            className="mt-8 mr-12"
            type="number"
            name="unitPrice"
            InputLabelProps={{ shrink: true }}
            value={itemData?.unitPrice}
            onChange={handleChange}
          />
          <TextField
            label={t("Product.amount")}
            className="mt-8 ml-12"
            type="number"
            name="intoMoney"
            InputLabelProps={{ shrink: true }}
            value={itemData?.intoMoney}
            onChange={handleChange}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            label={t("MaintainPlaning.selectTemplate")}
            className="mt-8"
            type="text"
            name="unit"
            InputLabelProps={{ shrink: true }}
            value={""}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            className="mt-12"
            fullWidth
            multiline
            label={t("MaintainPlaning.taskExecution")}
            InputProps={{
              rows: 3,
            }}
            value={itemData?.work}
            onChange={handleChange}
            variant="outlined"
            name="work"
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            className="mt-12"
            fullWidth
            label={t("MaintainPlaning.Note")}
            multiline
            InputProps={{
              rows: 3,
            }}
            value={itemData?.note}
            onChange={handleChange}
            variant="outlined"
            name="note"
          />
        </Grid>
      </Grid>
    </>
  );
};

export default AddInfoAsset;
