import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  makeStyles,
  InputAdornment,
  IconButton,
  Icon
} from "@material-ui/core";
import React, {useContext, useMemo, useState} from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { searchByPageMaintainContract } from "../MaintainPlaningService";
import {appConst, LIST_ORGANIZATION, STATUS_DON_VI_THUC_HIEN, variable} from "../../../appConst";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";
import {
	filterOptions,
	handlePreventOnEnter,
	convertFromToDate, handleKeyDownIntegerOnly
} from "app/appFunction";
import SelectSupplyPopup from "app/views/Component/SupplyUnit/SelectSupplyPopup";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {Label, LightTooltip, NumberFormatCustom} from "../../Component/Utilities";
import { endOfYear, startOfYear } from "date-fns";
import { formatDateDto } from "../../../appFunction";
import {searchByTextNew} from "../../Supplier/SupplierService";
import AppContext from "../../../appContext";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  formControl: {
    display: "flex",
    flexDirection: "row",
  },
  textLabel: {
    marginRight: "24px",
    marginTop: "14px",
  },
  textDonGia: {
    marginTop: "8px",
    width: "100%",
  },
}));

const ContentRightDialog = (props) => {
  const classes = useStyles();
  const {
    t,
    isEdit,
    isView,
    onSelect,
    itemData = {},
    onChange,
    khStatus,
    arrayDate = [],
    khNgayDuyet,
    setArrayDate,
    listData = {},
    onRadioButtonSelect,
    handleSelectContract,
    stateHeader,
    handleSelectDonViThucHien,
  } = props;
	const {currentOrg} = useContext(AppContext);

  const [shouldOpenUnitDialog, setShouldOpenUnitDialog] = useState(false);
  const [currentChuKyIndex, setCurrentChuKyIndex] = useState(null);
  const isTuBaoTri = appConst.listHinhThuc.TU_BAO_TRI === itemData?.chinhthucCode;
  const searchObjectContract = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    supplierId: itemData?.donViThucHien?.id,
    contractTypeCode: appConst.TYPES_CONTRACT.BTBD,
    ngayKyHopDong: itemData?.ngayKyHopDong ? formatDateDto(itemData?.ngayKyHopDong) : null
  };
  const searchObjectSupplier = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
		isActive: STATUS_DON_VI_THUC_HIEN.HOAT_DONG.code,
    typeCodes: [appConst.TYPE_CODES.NCC_BDSC],
  };
	const thousandTypeByOrg = {
		[LIST_ORGANIZATION.BV199.code]: appConst.TYPES_MONEY.DOT.value,
		[LIST_ORGANIZATION.PRODUCT_BV199.code]: appConst.TYPES_MONEY.DOT.value,
	}
	const decimalTypeByOrg = {
		[LIST_ORGANIZATION.BV199.code]: appConst.TYPES_MONEY.COMMA.value,
		[LIST_ORGANIZATION.PRODUCT_BV199.code]: appConst.TYPES_MONEY.COMMA.value,
	}
	const thousandSeparator = thousandTypeByOrg[currentOrg?.code] || appConst.TYPES_MONEY.COMMA.value;
	const decimalSeparator = decimalTypeByOrg[currentOrg?.code] || appConst.TYPES_MONEY.DOT.value;

  const handleChangePeriod = (value, name, index) => {
    const updatedDates = arrayDate ? [...arrayDate] : [];

    if (name === variable.listInputName.ckDate) {
      updatedDates[index] = {
        ...updatedDates[index],
        ckNgay: value ? value?.getDate() : null,
        ckThang: value ? value?.getMonth() + 1 : null,
        ckNam: value ? value?.getFullYear() : null,
      };
    }
    if (name === variable.listInputName.ckDonGia) {
      updatedDates[index] = {
        ...updatedDates[index],
        ckDonGia: value ? value : null
      };
    }

    updatedDates[index] = {
      ...updatedDates[index],
      [name]: value
    }
		
		if (!updatedDates[index].ckDate && !updatedDates[index].ckDonGia) {
			Object.keys(updatedDates[index]).forEach(key => {
				updatedDates[index][key] = null;
			});
		}

    setArrayDate(updatedDates);
  };

  const handleCopyValue = (index) => {
    const newArray = arrayDate ? [...arrayDate] : [];
    const currentItem = arrayDate[index];

    for (let i = 0; i < itemData?.ckSoluong; i++) {
      let item = newArray[i];
      if (item) {
        item.ckDonGia = item.ckDonGia ? item.ckDonGia : currentItem?.ckDonGia;
      }
      else {
        newArray.push({
          ckDonGia: currentItem?.ckDonGia
        })
      }
    }

    setArrayDate(newArray);
  }

  const handleShowCopyIcon = index => setCurrentChuKyIndex(index);

  const renderChuKy = useMemo(() => {
    return Array.from({ length: itemData?.ckSoluong }, (_, i) => {
      const item = arrayDate?.length > 0 ? arrayDate[i] : {}
      const preValue = arrayDate?.length > 0
        ? arrayDate?.[i - 1]?.ckDate
        : new Date('01/01/1900');
      const pickerValue = arrayDate?.length > 0
        ? item?.ckDate
        : null;
      const ckDonGiaValue = arrayDate?.length > 0
        ? item?.ckDonGia
        : null;
      return (
        <Grid item container key={i}>
          {(!isTuBaoTri) && (
            <Grid item md={2} xs={12} className="pr-4">
              <TextValidator
                className="w-100"
                label={t("MaintainPlaning.performedTime")}
                name="lanThu"
                value={i + 1}
								type="number"
                InputProps={{
                  inputProps: {
                    className: "text-center",
                  },
                  readOnly: true,
                }}
                onKeyDown={handlePreventOnEnter}
                disabled={
                  appConst.STATUS_CHU_KY.CHO_XU_LY.code < item?.ckTrangthai
                  || !isEdit
                }
              />
            </Grid>
          )}
          <Grid item md={isTuBaoTri ? 12 : !itemData.ngayKyHopDong ? 10 : 5} xs={12} className="px-4">
            <ValidatedDatePicker
              readOnly={isView}
              margin="none"
              fullWidth
              views={
                isTuBaoTri
                  ? ['year', 'month', 'date']
                  : ['year', 'month']
              }
              openTo={isTuBaoTri ? "date" : "month"}
              format={
                isTuBaoTri
                  ? "dd/MM/yyyy"
                  : "MM/yyyy"
              }
              label={
                <Label isRequired={itemData.ngayKyHopDong || (itemData.isHinhThuc && khStatus.isDaBaoGia)}>
                  {t("MaintainPlaning.time")}&nbsp; {i + 1}
                </Label>
              }
              autoOk
              value={pickerValue ? pickerValue : null}
              onChange={(date) => {
                handleChangePeriod(
                  date,
                  variable.listInputName.ckDate,
                  i
                );
              }}
              KeyboardButtonProps={{ "aria-label": "change date", }}
              minDate={preValue || startOfYear(new Date(stateHeader?.tuNam, 0)) || undefined}
              minDateMessage={
                preValue
                  ? `Ngày của lần ${i + 1} phải lớn hơn ngày của lần ${i}`
                  : "Ngày của chu kỳ không dược nhỏ hơn từ năm của phiếu"
              }
              maxDate={endOfYear(new Date(stateHeader?.denNam, 0)) || undefined}
              maxDateMessage={t("MaintainPlaning.messages.maxCkDateCompareWithPlan")}
              invalidDateMessage={t("general.invalidDateFormat")}
              disabled={
                appConst.STATUS_CHU_KY.CHO_XU_LY.code < item?.ckTrangthai
                || !isEdit
                || item?.amNghiemThuId
								|| (!itemData.ngayKyHopDong && !itemData.isHinhThuc)
              }
              validators={itemData.ngayKyHopDong || (itemData.isHinhThuc && khStatus.isDaBaoGia) ? ["required"] : []}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          {(!isTuBaoTri && itemData.ngayKyHopDong) && (
            <Grid item md={5} xs={12} className="pl-4">
              <TextValidator
                className="w-100"
                label={t("MaintainPlaning.value")}
                name="ckDonGia"
								type="text"
                value={ckDonGiaValue || ""}
                onKeyDown={(e) => {
									handlePreventOnEnter(e);
									handleKeyDownIntegerOnly(e);
								}}
                onFocus={(e) => handleShowCopyIcon(i)}
                onChange={(e) => {
                  handleChangePeriod(
                    e.target.value,
                    e.target.name,
                    i
                  );
                }}
                InputProps={{
                  inputComponent: NumberFormatCustom,
									inputProps: {
										thousandSeparator,
										decimalSeparator,
										// pass props to NumberFormatCustom in here
									},
                  endAdornment: currentChuKyIndex === i ? (
                    <InputAdornment position="end">
                      <LightTooltip
                        title={"Sao chép giá trị"}
                        placement="right"
                        enterDelay={300}
                        leaveDelay={200}
                        PopperProps={{
                          popperOptions: {
                            modifiers: { offset: { enabled: true, offset: "-15px, 10px" } },
                          },
                        }}
                      >
                        <IconButton size="small" onClick={() => handleCopyValue(i)}>
                          <svg
                            viewBox="0 0 24 24"
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            fill="var(--primary)"
                            width={20}
                          >
                            <g id="SVGRepo_bgCarrier" strokeWidth="1"></g>
                            <g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g>
                            <g id="SVGRepo_iconCarrier" fill="var(--primary)">
                              <g id="🔍-Product-Icons" stroke="none" strokeWidth="2" fill="red" fillRule="evenodd">
                                <g id="ic_fluent_copy_move_24_regular" fill="var(--primary)" fillRule="nonzero">
                                  <path d="M17.5,12 C20.5375661,12 23,14.4624339 23,17.5 C23,20.5375661 20.5375661,23 17.5,23 C14.4624339,23 12,20.5375661 12,17.5 C12,14.4624339 14.4624339,12 17.5,12 Z M5.50280381,4.62704038 L5.5,6.75 L5.5,17.2542087 C5.5,19.0491342 6.95507456,20.5042087 8.75,20.5042087 L11.7348483,20.5050555 C12.0226688,21.0561051 12.3871778,21.5607511 12.8142135,22.0048315 L8.75,22.0042087 C6.12664744,22.0042087 4,19.8775613 4,17.2542087 L4,6.75 C4,5.76928848 4.62744523,4.93512464 5.50280381,4.62704038 Z M18.2843055,14.5885912 C18.1137959,14.4704696 17.8862041,14.4704696 17.7156945,14.5885912 L17.6464466,14.6464466 L17.5885912,14.7156945 C17.4704696,14.8862041 17.4704696,15.1137959 17.5885912,15.2843055 L17.6464466,15.3535534 L19.2917119,16.999 L14.4937119,17 L14.4038362,17.0080557 C14.1997432,17.0450996 14.0388115,17.2060313 14.0017675,17.4101244 L13.9937119,17.5 L14.0017675,17.5898756 C14.0388115,17.7939687 14.1997432,17.9549004 14.4038362,17.9919443 L14.4937119,18 L19.2937119,17.999 L17.6464466,19.6464466 L17.5885912,19.7156945 C17.4535951,19.9105626 17.4728803,20.179987 17.6464466,20.3535534 C17.820013,20.5271197 18.0894374,20.5464049 18.2843055,20.4114088 L18.3535534,20.3535534 L20.8831913,17.8212104 L20.9201733,17.7711351 L20.9622078,17.6910366 L20.9882445,17.6082776 L20.9980501,17.5443521 L20.9980501,17.4557501 L20.9882655,17.3920225 L20.9767316,17.3488008 L20.9444953,17.2708095 L20.9204888,17.2292723 L20.8832098,17.1788087 L18.3535534,14.6464466 L18.2843055,14.5885912 Z M17.75,2 C18.9926407,2 20,3.00735931 20,4.25 L20.000576,11.4984036 C19.5265076,11.3006539 19.0240064,11.1574367 18.5009089,11.0765886 L18.5,4.25 C18.5,3.83578644 18.1642136,3.5 17.75,3.5 L8.75,3.5 C8.33578644,3.5 8,3.83578644 8,4.25 L8,17.25 C8,17.6642136 8.33578644,18 8.75,18 L11.0189701,18.000332 C11.0585148,18.5198198 11.159124,19.0222369 11.3136354,19.5004209 L8.75,19.5 C7.50735931,19.5 6.5,18.4926407 6.5,17.25 L6.5,4.25 C6.5,3.00735931 7.50735931,2 8.75,2 L17.75,2 Z" id="🎨-Color"> </path>
                                </g>
                              </g>
                            </g>
                          </svg>
                        </IconButton>
                      </LightTooltip>
                    </InputAdornment>
                  ) : "",
                }}
                disabled={appConst.STATUS_CHU_KY.CHO_XU_LY.code < item?.ckTrangthai || !isEdit}
              />
            </Grid>
          )}
        </Grid>
      );
    });
  }, [
    arrayDate,
    isEdit, isView,
    itemData?.ckSoluong,
    itemData?.ngayKyHopDong,
    itemData?.isHinhThuc,
    itemData?.chinhthucCode,
    currentChuKyIndex,
    khStatus.isDaBaoGia,
  ]);

  const handleSelect = (item, source) => {
		const value = convertFromToDate(item).fromDate;
    onSelect(value, source);
  };

  const handleChangeSelect = (event, itemData) => {
    const selectedObject = listData?.listHinhThuc?.find(
      (item) => item.id === event.target.value
    );

    onRadioButtonSelect(selectedObject, itemData);
  };

  return (
    <>
      <Grid
        item container
        md={4} xs={12}
        spacing={1}
        justifyContent="space-between"
        className="my-4 bg-maintain-plaining-infor"
      >
        <Grid item md={12} xs={12}>
          <FormControl component="fieldset" className={classes.formControl}>
            <FormLabel
              id="demo-radio-buttons-group-label"
              className={classes.textLabel}
            >
              {!khStatus?.isMoiTao && (
                <span className="colorRed">*</span>
              )}
              Hình thức thực hiện:
            </FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              defaultValue="female"
              name="radio-buttons-group"
              value={itemData?.chinhthucId || null}
              onChange={(event) => handleChangeSelect(event, itemData)}
            >
              {listData?.listHinhThuc?.map((item, index) => {
                return (
                  <FormControlLabel
                    key={item?.id}
                    value={item?.id}
                    control={<Radio />}
                    label={item?.name}
                    disabled={
                      !isEdit || isView
                      || khStatus?.isDaBaoGia
                      || khStatus?.isDaKetThuc
                    }
                  />
                );
              })}
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid
          item
          md={(
            (khStatus?.isDaBaoGia || khStatus?.isDaKetThuc)
            && !isTuBaoTri
          ) ? 3 : 12}
          xs={12}
        >
          <TextValidator
            label={
              <span>
                {!khStatus?.isMoiTao && <span className="colorRed">*</span>}
                {t("MaintainPlaning.ckSoluong")}
              </span>
            }
            onKeyDown={handlePreventOnEnter}
            className="mt-8 w-80"
            fullWidth
            InputProps={{ readOnly: isView }}
            name="ckSoluong"
            type="number"
            value={itemData?.ckSoluong || ""}
            onChange={(e) =>
              onChange(e, variable.listInputName.quantity, itemData)
            }
            disabled={!isEdit || !khStatus?.isMoiTao}
            validators={
              (khStatus?.isDaDuyet || khStatus?.isDaBaoGia)
                ? ["required", "minNumber:1"]
                : []
            }
            errorMessages={[t("general.required"), t("general.minNumberError"),]}
          />
        </Grid>
        {((khStatus?.isDaBaoGia || khStatus?.isDaKetThuc) && !isTuBaoTri) && (
          <Grid item xs={12} md={9}>
            <ValidatedDatePicker
              autoOk
              fullWidth
              margin="none"
              className="w-100 mt-8"
              readOnly={isView}
              InputProps={{
                readOnly: isView,
              }}
              inputVariant="standard"
              format="dd/MM/yyyy"
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
              label={
                <span>
                  {t("MaintainPlaning.contractSignDate")}
                </span>
              }
              disabled={!isEdit || khStatus.isDaKetThuc || itemData?.isAccepted}
              value={itemData?.ngayKyHopDong ? itemData?.ngayKyHopDong : null}
              onChange={(date) => handleSelect(date, "ngayKyHopDong")}
              minDate={
                khNgayDuyet
                  ? khNgayDuyet
                  : new Date('01/01/1900')
              }
              minDateMessage={
                khNgayDuyet
                  ? t("MaintainPlaning.messages.minContractSignDate")
                  : t("general.minDateDefault")
              }
              maxDate={new Date()}
              maxDateMessage={t("general.maxDateNow")}
              invalidDateMessage={t("general.invalidDateFormat")}
              clearable
              clearLabel={t("general.remove")}
              cancelLabel={t("general.cancel")}
              okLabel={t("general.select")}
            />
          </Grid>
        )}
        <Grid item md={12} sm={12} xs={12} spacing={1} className="chu-ky-container">
          {renderChuKy}
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-10">
          <AsynchronousAutocompleteSub
            label={
              <Label isRequired={itemData.ngayKyHopDong}>{t("Asset.performingUnit")}</Label>
            }
            searchFunction={searchByTextNew}
            searchObject={searchObjectSupplier}
            displayLable="name"
            onSelect={handleSelectDonViThucHien}
            readOnly={isView}
            value={itemData?.donViThucHien || null}
						filterOptions={(options, params) => filterOptions(
							options,
							params,
							true,
							"name"
						)}
						onInputChange={(e) => onChange(e, variable.listInputName.keySearch)}
            disabled={
              itemData?.isHinhThuc
              || isView
              || !isEdit
              || itemData?.isAccepted
            }
            validators={itemData.ngayKyHopDong ? ["required",] : []}
            errorMessages={[t("general.required")]}
          />
        </Grid>
        {itemData.ngayKyHopDong && (
          <Grid item md={12} xs={12}>
            <AsynchronousAutocompleteSub
              searchFunction={searchByPageMaintainContract}
              searchObject={searchObjectContract}
              label={
                <span>
                  {t("MaintainPlaning.contract")}
                </span>
              }
              displayLable="contractName"
              showCode="contractCode"
              typeReturnFunction="category"
              onSelect={(value) => handleSelectContract(
                value,
                itemData
              )}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                true,
                "contractName"
              )}
              onInputChange={(e) => onChange(e, variable.listInputName.keySearch)}
              readOnly={isView}
              value={itemData?.contract || null}
              disabled={!isEdit || !itemData?.donViThucHien?.id || itemData?.isAccepted}
              errorMessages={[t("general.required")]}
            />
          </Grid>
        )}
        <Grid item md={12} xs={12}>
          <TextValidator
            className="mt-12"
            fullWidth
            multiline
            label={
              <span>
                {!khStatus.isMoiTao && (<span className="colorRed">*</span>)}
                {t("MaintainPlaning.taskExecution")}
              </span>
            }
            InputProps={{
              rows: 7,
              readOnly: isView,
            }}
            value={itemData?.congViec || ""}
            onChange={(e) => onChange(e, variable.listInputName.work, itemData)}
            variant="outlined"
            name="congViec"
            validators={!khStatus.isMoiTao ? ["required"] : []}
            errorMessages={[t("general.required")]}
            disabled={!isEdit || itemData?.isAccepted}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            className="mt-12"
            fullWidth
            label={t("MaintainPlaning.Note")}
            multiline
            InputProps={{
              rows: 1,
              readOnly: isView,
            }}
            value={itemData?.ghiChu || ""}
            onChange={(e) => onChange(e, variable.listInputName.note, itemData)}
            variant="outlined"
            name="ghiChu"
            disabled={!isEdit || (khStatus.isDaKetThuc && !isView)}
          />
        </Grid>

        {shouldOpenUnitDialog && (
          <SelectSupplyPopup
            open={shouldOpenUnitDialog}
            onClose={() => setShouldOpenUnitDialog(false)}
            unit={itemData}
            handleSelect={handleSelect}
            t={t}
            typeCodes={[appConst.TYPE_CODES.NCC_BDSC]}
          />
        )}
      </Grid>
    </>
  );
};

export default ContentRightDialog;
