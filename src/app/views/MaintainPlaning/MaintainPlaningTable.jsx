import React from "react";
import {
  AppBar,
  Button,
  Card,
  Checkbox,
  Collapse,
  FormControl,
  FormControlLabel,
  Grid,
  Icon,
  IconButton,
  Input,
  InputAdornment,
  Tab,
  TablePagination,
  Tabs,
  TextField,
} from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import SearchIcon from "@material-ui/icons/Search";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  appConst, LIST_ORGANIZATION,
  listMaintainPlaningStatus,
  MAINTAIN_PLANING_STATUS,
  STATUS_DON_VI_THUC_HIEN,
  PRINT_TEMPLATE_MODEL,
  variable,
} from "../../appConst";
import AppContext from "app/appContext";
import {
	convertFromToDate,
	formatTimestampToDate,
	getTheHighestRole, getUserInformation, handleKeyDown,
	handleKeyUp, handleThrowResponseMessage, isSuccessfulResponse,
	isValidDate,
	LightTooltip,
} from "app/appFunction";
import { Breadcrumb, ConfirmationDialog } from "egret";
import FileSaver from "file-saver";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import CustomMaterialTable from "../CustomMaterialTable";
import MaintenanceProposalDialog from "./Dialog/MaintenanceProposalDialog";
import MaintainPlaningDialog from "./MaintainPlaningDialog";
import {
  deleteAcceptance,
  deleteMaintainProposalById,
  deletePlan,
  exportToExcel,
  getAcceptanceDetail,
  getAssetsPlanComparision,
  getDetailPlanComparision,
  getMaintainProposalById,
  searcgByPageMaintainContractTable,
  searchByPageAcceptance,
  searchByPageAssetKeHoach,
  searchByPageChoXuLy,
  searchByPageDeXuat,
  searchByPageHinhThucNew,
  searchByPageKeHoach,
  searchByPageMaintainContract,
  searchByPageTaiSan,
} from "./MaintainPlaningService";
import moment from "moment";
import AcceptanceDialog from "./Dialog/AcceptanceDialog";
import ComparePlaningDialog from "./Dialog/ComparePlaningDialog";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getAllManagementDepartmentByOrg, searchByPage, searchByPageDepartmentByOrg } from "../Department/DepartmentService";
import { searchByTextNew } from "../Supplier/SupplierService";
import ContractDialog from "../Contract/ContractDialog";
import { addDays, format } from "date-fns";
import { getItemById } from "../Contract/ContractService";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AssetPrintDepartmentsDialog from "./Dialog/AssetPrintDepartmentsDialog"
import { withRouter } from "react-router-dom";
import { handleConvertAcceptanceData, handleConvertProposalPrintData } from "../FormCustom/MaintainPlaning";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm"
import { convertPayloadAcceptance, convertPayloadChuKy, convertPayloadMaintainProposal } from "./dataConversion";
import {
  acceptanceColumns,
  acceptanceSubColumns,
  ckColumns,
  contractColumns,
  planColumns, planSubColumns,
  proposalColumns, proposalSubColumns
} from "./columns";
import { getFirstAndLastDayOfMonth } from "../../appFunction";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 3000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const { item, itemEdit, tabValue, commonObject } = props;
  const {
    hasEditPermission,
    hasDeletePermission,
    isRoleAssetManager,
    isRoleAdmin,
    isRoleOrgAdmin,
    currentUser,
    openAdvanceSearch,
    selectedList,
    isCheckedAcceptance,
  } = itemEdit;
  const isCheck = (hasDeletePermission && hasEditPermission)
    || isRoleAssetManager || isRoleAdmin || isRoleOrgAdmin;
  const isCreatedPerson = item?.createdBy === currentUser?.username;
  const isChoTongHop = appConst.statusPhieu.dangTongHop === item.dxTrangthai;

  const isMoiTaoKH = item?.khTrangThai === MAINTAIN_PLANING_STATUS.MOI_TAO.indexOrder;
  const isDaKetThucKH = item?.khTrangThai === MAINTAIN_PLANING_STATUS.DA_KET_THUC.indexOrder;

  const isDangXuLy = item?.trangThai === appConst.STATUS_NGHIEM_THU_BTBD.DANG_XU_LY.code; // TT nghiệm thu
  const isContractCanLiquidate = item?.canLiquidate && !item.contractLiquidationDate // TT hợp đồng

  const isTabChoXuLy = tabValue === appConst.tabStatus.tabWaiting;
  const isTabNghiemThu = tabValue === appConst.tabStatus.tabAcceptance
  const isTabPLan = tabValue === appConst.tabStatus.tabPlan
  const isTabProposal = tabValue === appConst.tabStatus.tabMaintenance;
  const isTabContractLiquidate = tabValue === appConst.tabStatus.tabContractLiquidate
  const isAllowNghiemThu = commonObject?.code === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code || isCheckedAcceptance;
  const isShowDeleteIcon = (
    isCreatedPerson && (
      (isMoiTaoKH && isTabPLan) || (isTabProposal && isChoTongHop)
    )
    || (isTabNghiemThu && isDangXuLy)
  )

  return (
    <div className="none_wrap" id="actions-button-group">
      {(isChoTongHop || (isTabChoXuLy && isAllowNghiemThu && !openAdvanceSearch)) && isCheck && (
        <Checkbox
          checked={item?.checked}
          onChange={() => props.onSelect(item, appConst.active.check)}
        />
      )}
      {(
        (isCheck && !isDaKetThucKH && isTabPLan)
        || (isTabNghiemThu && isDangXuLy)
        || (isTabContractLiquidate && isContractCanLiquidate)
      ) && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {/* Sửa phiếu đề xuất */}
      {(isRoleOrgAdmin || isCreatedPerson) && isChoTongHop && isTabProposal && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.propose)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {/* Xóa phiếu đề xuất */}
      {isShowDeleteIcon && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {item?.tsTrangthai === appConst.tabStatus.tablPending && isCheck && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.receive")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.receive)}
          >
            <ArrowForwardOutlinedIcon className="iconArrow" />
          </IconButton>
        </LightTooltip>
      )}
      {/* View phiếu đề xuất || kế hoạch */}
      {(isTabProposal
        || isTabPLan
        || isTabNghiemThu
        || isTabContractLiquidate
      ) && (
          <LightTooltip
            title={t("InstrumentToolsTransfer.watchInformation")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() =>
                props.onSelect(
                  item,
                  appConst.active.view,
                  variable.listInputName.isProposal
                )
              }
            >
              <VisibilityIcon size="small" className="iconEye" />
            </IconButton>
          </LightTooltip>
        )}
      {(isTabProposal || isTabNghiemThu) && (
        <LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
            disabled={selectedList?.length > 1 && isTabProposal}
          >
            <Icon fontSize="small" color={!(selectedList?.length > 1 && isTabProposal) ? "primary" : ""}>
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

class MaintainPlaningTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 10,
    rowsPerPage1: 10,
    page: 0,
    page1: 0,
    totalElements: 0,
    totalElements1: 0,
    MaintainRequestStatus: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenMainProposal: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenAssetPrintDialog: false,
    shouldOpenNotificationPopup: false,
    Notification: "",
    asset: null,
    dxFrom: null,
    dxTo: null,
    tabValue: 0,
    data: [],
    itemList: [],
    selectedRow: "",
    isLoading: false,
    shouldOpenAcceptanceDialog: false,
    itemReceive: "",
    isPrint: false,
    isView: false,
    status: null,
    selectedData: {},
    openExcel: false,
    openAdvanceSearch: false,
    dxPhongBanId: "",
    tnPhongBanId: "",
    commonObject: null,
    supplier: null,
    listCommonObject: [],
    listSupplier: [],
    times: null,
    listTimes: [],
    shouldOpenConfirmationAcceptance: false,
    isAcceptance: false,
    isCheckedAcceptance: false,
    checkAll: false,
    isNtWithoutContract: false,
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => {
    });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search, this);

  setPage = (page) => {
    this.setState({ page, itemList1: [], checkAll: false }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0, itemList1: [] }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  setPage1 = (page1) => {
    this.setState({ page1 }, () => {
      this.updatePageDataAsset(this.state.selectedData);
    });
  };

  setRowsPerPage1 = (event) => {
    this.setState({ rowsPerPage1: event.target.value, page1: 0 }, () => {
      this.updatePageDataAsset(this.state.selectedData);
    });
  };

  handleChangePage1 = (event, newPage) => {
    this.setPage1(newPage);
  };

  search = () => {
    this.setState({ page: 0 }, () => this.updatePageData(this.state.tabValue));
  }

  checkData = () => {
    if (!this.data || this.data.length === 0) {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "general.noti_check_data",
      });
    } else if (this.data.length === this.state.itemList.length) {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };

  updatePageData = async () => {
    const { t } = this.props;
    const {
      keyword,
      page,
      rowsPerPage,
      selectedList,
      department,
      tabValue,
    } = this.state;
    let chuKyTabs = [
      appConst.tabStatus.tabWaiting,
      appConst.tabStatus.tabPreProcessed,
      appConst.tabStatus.tabProcessed,
    ]
    const listTabsActionChecked = [
      appConst.tabStatus.tabWaiting,
      appConst.tabStatus.tabMaintenance,
    ]

    let { setPageLoading } = this.context;
    const searchObject = {
      keyword: keyword?.trim(),
      pageIndex: page + 1,
      pageSize: rowsPerPage,
    };
    this.setState({
      item: null,
      itemList: [],
      totalElements: 0,
    });
    if (tabValue && tabValue !== appConst.tabStatus.tabWaiting) {
      this.setState({ isNtWithoutContract: false })
    }
    try {
      setPageLoading(true);
      let value;
      if (appConst.tabStatus.tabMaintenance === tabValue) {      // Tab đề xuất
        value = await this.updateTableDataMaintainProposal(searchObject);
      } else if (appConst.tabStatus.tabPlan === tabValue) {      // Tab kế hoạch
        searchObject.khPhongBanId = department?.id;
        const result = await searchByPageKeHoach(searchObject);
        value = result?.data?.data;
      } else if (appConst.tabStatus.tabContractLiquidate === tabValue) { // Tab kế hoạch
        value = await this.updateTableDataContractLiquidate(searchObject)
      } else if (appConst.tabStatus.tabAcceptance === tabValue) { // Tab Nghiệm thu
        value = await this.updateTableDataAcceptance(searchObject);
      } else if (chuKyTabs.includes(tabValue)) { // Các tab của chu kỳ
        value = await this.updateTableDataCk(searchObject, tabValue);
      }
      if (value?.content?.length) {
        this.setState({
          itemList: value?.content?.map((item, index) => {
            item.index = index + 1;
            if (listTabsActionChecked.includes(tabValue)) {
              item.checked = selectedList?.some((x) => x.id === item.id);
            }
            return item;
          }),
          totalElements: value?.totalElements,
        });
      }
      setPageLoading(false);
    } catch (err) {
      console.log(err)
      toast.error(t("general.error"));
      setPageLoading(false);
    }
  };

  updateTableDataCk = async (searchObject, tabValue) => {
    const { t } = this.props;
    const newSearchObject = {
      ...this.state,
      searchObject,
      tabValue,
    };

    try {
      let sendData = convertPayloadChuKy(newSearchObject);
      const res = await searchByPageChoXuLy(sendData);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({ assets: data?.content });
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  updateTableDataMaintainProposal = async (searchObject) => {
    const { t } = this.props;

    try {
      const sendData = convertPayloadMaintainProposal({ ...this.state, searchObject });
      const res = await searchByPageDeXuat(sendData);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({ assets: data?.content });
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  updateTableDataAcceptance = async (searchObject) => {
    const { t } = this.props;
    let { selectedList } = this.state;

    try {
      const sendData = convertPayloadAcceptance({ ...this.state, searchObject });
      const res = await searchByPageAcceptance(sendData);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        let count = 0;
        let upldateData = data?.content?.map(item => {
          selectedList?.forEach(x => {
            if (x?.id === item?.id) {
              item.checked = true;
              count++;
            }
          })
        })
        this.setState({ assets: upldateData, checkAll: count === upldateData?.length });
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      console.log(e)
      toast.error(t("general.error"));
    }
  }

  updateTableDataContractLiquidate = async (searchObject) => {
    const { t } = this.props;

    try {
      const sendData = {
        ...searchObject,
        supplierId: this.state.supplier?.id,
        contractLiquidationDateTo: convertFromToDate(this.state.dxTo).toDate,
        contractLiquidationDateFrom: convertFromToDate(this.state.dxFrom).fromDate,
      }
      const res = await searcgByPageMaintainContractTable(sendData);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({ assets: data?.content });
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenMainProposal: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenNotificationPopup: false,
      shouldOpenConfirmationPlanningDialog: false,
      shouldOpenAcceptanceDialog: false,
      shouldOpenCompareDialog: false,
      shouldOpenContractLiquidateDialog: false,
      shouldOpenConfirmationAcceptance: false,
      isPrint: false,
      isView: false,
      item: null,
      selectedList: [],
    }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  handleEditorDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenMainProposal: false,
      shouldOpenConfirmationPlanningDialog: false,
      shouldOpenAcceptanceDialog: false,
      isPrint: false,
      isView: false,
      item: null,
      selectedList: [],
    }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  handleMaintenanceProposalDialogClose = () => {
    this.setState({
      shouldOpenMainProposal: false,
      isView: false,
    });
    this.updatePageData(this.state.tabValue);
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenMainProposal: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenAcceptanceDialog: false,
      shouldOpenCompareDialog: false,
      shouldOpenContractLiquidateDialog: false,
      shouldOpenAssetPrintDialog: false,
      selectedList: [],
      item: null,
      checkAll: false,
    }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  handleConfirmationResponse = async () => {
    let { id, tabValue } = this.state;
    let { t } = this.props;
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let res
      if (appConst.tabStatus.tabMaintenance === tabValue) {
        res = await deleteMaintainProposalById(id);
      } else if (appConst.tabStatus.tabAcceptance === tabValue) {
        res = await deleteAcceptance(id);
      } else if (appConst.tabStatus.tabPlan === tabValue) {
        res = await deletePlan(id);
      }
      const { code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        toast.success(t("general.deleteSuccess"));
      } else {
        toast.warning(message);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
      this.handleDialogClose();
    }
  };

  handlePrint = async (rowData) => {
    let { tabValue } = this.state;
    if (tabValue === appConst.tabStatus.tabAcceptance) {
      this.handleGetDetailAcceptance(rowData).then(() => {
        this.setState({ isPrint: true });
      })
      return;
    }
    this.setState({
      item: rowData ? rowData : {},
      isPrint: true,
    });
  };

  componentDidMount = async () => {
    let { location, history, t } = this.props;
    let { TYPE_REQUIREMENT } = appConst;
    const roles = getTheHighestRole();

    if (location?.state?.id) {
      const tabValueByType = {
        [TYPE_REQUIREMENT.RECEIVE_MAINTAIN_PROPOSAL.code]: appConst.tabStatus.tabMaintenance,
        [TYPE_REQUIREMENT.APPROVE_MAINTAIN_PLANING.code]: appConst.tabStatus.tabPlan,
        [TYPE_REQUIREMENT.APPROVE_MAINTAIN_PLANING_SIGN_CONTRACT.code]: appConst.tabStatus.tabPlan,
      }
      const handleEditByType = {
        [TYPE_REQUIREMENT.RECEIVE_MAINTAIN_PROPOSAL.code]: (rowData) => this.handleEditMaintainProposal(rowData?.id),
        [TYPE_REQUIREMENT.APPROVE_MAINTAIN_PLANING.code]: this.handleEdit,
        [TYPE_REQUIREMENT.APPROVE_MAINTAIN_PLANING_SIGN_CONTRACT.code]: this.handleEdit,
      }

      try {
        this.handleChangeTabValue(null, tabValueByType[location.state.type]);
      } catch {
        toast.error(t("general.error"));
      } finally {
        switch (location?.state?.status) {
          case appConst.STATUS_REQUIREMENTS.PROCESSED:
            await this.handleView({ id: location?.state?.id })
            break;
          case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
            await handleEditByType[location.state.type]?.({ id: location?.state?.id })
            break;
          default:
            break;
        }
        history.push(location?.state?.path, null);
      }
    }

    this.setState({
      ...roles,
    }, async () => {
      await this.updatePageData(this.state.tabValue);
      await this.getListCommonObject();
    });
  }

  getListCommonObject = async () => {
    try {
      const res = await searchByPageHinhThucNew({ code: appConst.listCommonObject.HINH_THUC_BAO_TRI.code });
      if (res.status === appConst.CODE.SUCCESS && Array.isArray(res?.data)) {
        this.setState({ listCommonObjects: res?.data }, () => {
          let codeTuBaoTri = res?.data?.find(i => i?.code === appConst.listHinhThuc.TU_BAO_TRI);
          this.setState({ codeTuBaoTri })
        })
      }
    } catch (error) {
      console.error(error);
    }
  }
  handleEditItem = () => {
    this.setState({
      shouldOpenMainProposal: true,
    });
  };

  handleOpenConfirmationPlanningDialog = () => {
    let { t } = this.props;
    let { tabValue, selectedList = [] } = this.state;
		let {isRoleOrgAdmin} = getTheHighestRole();
		let {departmentUser} = getUserInformation();
    let isIncludeDifferentDepartment = false;
    let isIncludeDaTongHop = false;
		let isDepartmentDiffersCurrent = false;
    selectedList?.forEach(
      (item) => {
        if (
          item?.dxTrangthai ===
          appConst.STATUS_MAINTAINANCE_PROPOSAL.DA_TONG_HOP.indexOrder
        ) {
          isIncludeDaTongHop = true;
        }
        if (item.tnPhongBanId !== selectedList[0]?.tnPhongBanId) {
          isIncludeDifferentDepartment = true;
        } else if (item.tnPhongBanId !== departmentUser?.id && !isRoleOrgAdmin) {
					isDepartmentDiffersCurrent = true;
				}
      }
    );

    if (tabValue === appConst.tabStatus.tabPlan) {
      this.handlePlanning()
    } else if (tabValue === appConst.tabStatus.tabMaintenance) {
      if (isIncludeDifferentDepartment) { 											//case: more than 1 receiving department in selected list
        toast.warning(t("MaintainPlaning.warnDepartmentNotSynchronized"));
        return;
      } else if (isDepartmentDiffersCurrent) { 									// case: receiver department !== current department
				toast.warning(t("MaintainPlaning.messages.ReceivingDepartmentDiffersFromCurrent"));
        return;
      }
      if (isIncludeDaTongHop) { 																//case: invalid status
        toast.warning(t("MaintainPlaning.warnHaveSynthesized"));
        return;
      }
			
      this.setState({
        shouldOpenConfirmationPlanningDialog: true,
      });
    }
  };

  handleClick = (event, item) => {
    let { MaintainRequestStatus } = this.state;
    if (item.checked === null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < MaintainRequestStatus.length; i++) {
      if (
        MaintainRequestStatus[i].checked === null ||
        MaintainRequestStatus[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (MaintainRequestStatus[i].id === item.id) {
        MaintainRequestStatus[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      MaintainRequestStatus: MaintainRequestStatus,
    });
  };

  handleDelete = (rowData) => {
    this.setState({
      id: rowData?.id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handlePlanning = (event) => {
    this.setState({
      asset: "",
      shouldOpenEditorDialog: true,
      item: {
        khPhongBanId: this.state?.selectedList?.[0]?.tnPhongBanId,
        khPhongBanText: this.state?.selectedList?.[0]?.tnPhongBanText,
        khPhongBan: {
          id: this.state?.selectedList?.[0]?.tnPhongBanId,
          name: this.state?.selectedList?.[0]?.tnPhongBanText,
        },
      }
    });
  };

  handleDateChange = (date, name) => {
    this.setState({ [name]: date, page: 0 }, () => {
      if (date === null || isValidDate(date)) {
        this.updatePageData(this.state.tabValue);
      }
    });
  };

  handleChangeTabValue = (event, value, key) => {
    const tab = appConst.tabStatus;
    if (Object.values(tab).includes(value)) {
      this.setState({
        ...this.state,
        dxFrom: null,
        dxTo: null,
        ntFrom: null,
        ntTo: null,
        supplier: null,
        commonObject: null,
        chuKyStatuses: null,
        keyword: "",
        department: null,
        itemList1: [],
        selectedList: [],
        tabValue: value,
        page: 0,
        page1: 0,
        rowsPerPage:
          value === tab.tabMaintenance || value === tab.tabPlan || value === tab.tabAcceptance ? 5 : 10,
        rowsPerPage1:
          value === tab.tabMaintenance || value === tab.tabPlan || value === tab.tabAcceptance ? 5 : 10,
        contract: null,
        times: null,
        isCheckedAcceptance: false,
        openAdvanceSearch: false,
        isAcceptance: false,
      },
        () => this.updatePageData(value)
      );
    }
  };

  renderMaterialTable = (
    title,
    data,
    columns,
    columnActions = [],
    onRowClick = () => {
    }
  ) => {
    const options = {
      draggable: false,
      sorting: false,
      rowStyle: (rowData) => this.handleGetRowColor(rowData),
      minBodyHeight:
        this.state.tabValue === appConst.tabStatus.tabMaintenance ||
          this.state.tabValue === appConst.tabStatus.tabPlan ||
          this.state.tabValue === appConst.tabStatus.tabAcceptance
          ? 250
          : 455,
      maxBodyHeight:
        this.state.tabValue === appConst.tabStatus.tabMaintenance ||
          this.state.tabValue === appConst.tabStatus.tabPlan ||
          this.state.tabValue === appConst.tabStatus.tabAcceptance
          ? 250
          : 455,
    };
    return (
      <CustomMaterialTable
        title={title}
        data={data}
        columns={columns}
        columnActions={columnActions}
        onRowClick={(e, rowData) => onRowClick(e, rowData)}
        options={options}
      />
    );
  };

  renderTablePagination = (
    totalElements,
    rowsPerPage,
    page,
    handleChangePage,
    setRowsPerPage
  ) => (
    <TablePagination
      align="left"
      className="px-16"
      rowsPerPageOptions={appConst.rowsPerPageOptions.table}
      component="div"
      labelRowsPerPage={this.props.t("general.rows_per_page")}
      labelDisplayedRows={({ from, to, count }) =>
        `${from}-${to} ${this.props.t("general.of")} ${count !== -1 ? count : `more than ${to}`
        }`
      }
      count={totalElements}
      rowsPerPage={rowsPerPage}
      page={page}
      backIconButtonProps={{
        "aria-label": "Previous Page",
      }}
      nextIconButtonProps={{
        "aria-label": "Next Page",
      }}
      onPageChange={handleChangePage}
      onRowsPerPageChange={setRowsPerPage}
    />
  );

  handleCheckboxToggle = (item) => {
    const { itemList, selectedList } = this.state;

    let selectedItem = itemList.find((row) => row.id === item?.id);
    if (selectedItem) {
      let existItem = selectedList?.find((item) => item.id === selectedItem.id);
      if (!existItem) {
        //eslint-disable-next-line
        selectedList?.push(selectedItem);
      } else {
        selectedList.map((item, index) =>
          item.id === existItem.id ? selectedList.splice(index, 1) : null
        );
      }
      itemList?.map(item => {
        if (item.id === selectedItem.id) {
          item.checked = !Boolean(existItem)
        }
      })
    }
    this.setState({ itemList, selectedList });
  };


  updatePageDataAsset = async (item) => {
    const { t } = this.props;
    const { tabValue } = this.state;
    const keyword = this.state.keyword;
    const page = this.state.page1;
    const rowsPerPage = this.state.rowsPerPage1;
    const dxId = item?.id;
    const searchObject = {
      keyword: keyword,
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      dxId: dxId,
    };
    this.setState({
      itemList1: [],
      totalElements1: 0,
    });
    if (appConst.tabStatus.tabMaintenance === tabValue) {
      try {
        let res = await searchByPageTaiSan(searchObject);
				let {data} = res?.data;
				
        this.setState({
          itemList1: data?.content?.sort((a, b) => (a.tsTen).localeCompare(b.tsTen)),
          totalElements1: data?.totalElements,
        });
      } catch (error) {
        toast.error(t("general.error"));
      }
    }
    if (appConst.tabStatus.tabPlan === tabValue) {
      try {
        const { data } = await searchByPageAssetKeHoach(searchObject);
        const newData = data?.data?.content?.map((item, index) => ({
          ...item,
          index: index + 1,
        }));
        this.setState({
          itemList1: newData,
          totalElements1: data?.data?.totalElements,
        });
      } catch (error) {
        toast.error(t("general.error"));
      }
    }
    if (appConst.tabStatus.tabAcceptance === tabValue) {
      try {
        const { data } = await getAcceptanceDetail(item?.id)
        this.setState({
          itemList1: data?.data?.amChukys
        })
      } catch (error) {
        toast.error(t("general.error"))
      }
    }
  };

  handleRowClick = (e, rowData) => {
    let actionsCell = document.activeElement.parentElement
    if (actionsCell?.id === variable.listInputName.actionsButtonGroup) return; // Nếu click btn action k gọi api cho subTable

    this.setState({
      selectedRow: rowData.id,
      selectedData: rowData
    });
    this.updatePageDataAsset(rowData);
  };

  handleEditMaintainProposal = async (rowData) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      let res = await getMaintainProposalById(rowData?.id);
      const { data, code, message } = res.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          item: data,
          shouldOpenMainProposal: true,
        });
      } else {
        toast.warning(message);
      }
      setPageLoading(false);
    } catch (error) {
      toast.error(t("general.error"));
      setPageLoading(false);
    }
  };

  handleView = async (rowData) => {
    let { t } = this.props;
    let { tabValue } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    if (appConst.tabStatus.tabMaintenance === tabValue) {
      try {
        let res = await getMaintainProposalById(rowData?.id);
        const { data, code, message } = res.data;
        if (appConst.CODE.SUCCESS === code) {
          this.setState({
            item: data,
            shouldOpenMainProposal: true,
            isView: true,
          });
        } else {
          toast.warning(message);
        }
        setPageLoading(false);
      } catch (error) {
        toast.error(t("general.error"));
        setPageLoading(false);
      }
    } else if (appConst.tabStatus.tabPlan === tabValue) {
      this.setState(
        {
          asset: rowData,
          shouldOpenEditorDialog: true,
          isView: true,
        },
        () => {
          setPageLoading(true);
        }
      );
    } else if (appConst.tabStatus.tabAcceptance === tabValue) {
      this.handleGetDetailAcceptance(rowData).then(() => {
        this.setState({
          isView: true,
          shouldOpenAcceptanceDialog: true,
        });
      })
    } else if (appConst.tabStatus.tabContractLiquidate === tabValue) {
      this.setState({
        isView: true,
      }, () => {
        this.handleEditContractLiquidate(rowData);
      });
    }
  };

  handleStatusSearch = (e, value, source) => {
    this.setState({ [source]: value?.code }, () => {
      this.updatePageData(this.state.tabValue);
    });
  };

  handleEdit = (rowData) => {
    let { tabValue } = this.state;
    if (tabValue === appConst.tabStatus.tabPlan) {
      this.handleEditMaintainPlainning(rowData);
    } else if (tabValue === appConst.tabStatus.tabAcceptance) {
      this.handleEditAcceptance(rowData);
    } else if (tabValue === appConst.tabStatus.tabContractLiquidate) {
      this.handleEditContractLiquidate(rowData);
    }
  }

  handleEditMaintainPlainning = (rowData) => {
    this.setState({
      asset: rowData,
      shouldOpenEditorDialog: true,
    });
  };

  handleComparePlaning = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let searchObject = {};
    searchObject.khId = rowData?.id;
    try {
      setPageLoading(true);
      let res = await getDetailPlanComparision(rowData?.id);
      let resAsset = await getAssetsPlanComparision(searchObject);
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          assets: resAsset?.data?.data?.content,
          item: data,
          shouldOpenCompareDialog: true,
        });
      } else {
        toast.warning(message);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleExportExcel = (e) => {
    this.exportToExcel();
  };

  // ExportExcel
  handleExportToExcel = async (type) => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      dxTo,
      dxFrom,
      keyword,
      contract,
      tabValue,
      supplier,
      currentUser,
      commonObject,
      selectedList = [],
    } = this.state;
    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.TSCD,
      };

      if (tabValue === appConst.tabStatus.tabMaintenance) { // tab đề xuất
				searchObject.amDexuatIds = selectedList?.map((item) => item.id);
				searchObject = convertPayloadMaintainProposal({ ...this.state, searchObject });
      } else { // tab chờ, đang, đã xử lý
        if (
          [
            appConst.tabStatus.tabWaiting,
            appConst.tabStatus.tabProcessed,
            appConst.tabStatus.tabPreProcessed,
          ].includes(tabValue)
        ) { // Chờ xử lý
          searchObject = {
            ...searchObject,
            ...convertPayloadChuKy({ ...this.state, isExcel: true }),
            keyword: keyword?.trim(),
          }
        } else if (tabValue === appConst.tabStatus.tabContractLiquidate) {//Tab thanh ly
          if (dxFrom) {
            searchObject.contractLiquidationDateFrom = convertFromToDate(dxFrom).fromDate
          }
          if (dxTo) {
            searchObject.contractLiquidationDateTo = convertFromToDate(dxTo).toDate
          }
          searchObject.supplierId = supplier?.id
					searchObject.chinhthucId = commonObject?.id;
					searchObject.donViThucHienId = supplier?.id;
					searchObject.status = tabValue + 1;
					searchObject.contractId = contract?.contractId;
					searchObject.keyword = keyword?.trim();
        }
        if (dxFrom) {
          searchObject.fromDate = getFirstAndLastDayOfMonth(dxFrom).firstDayFormatted;
        }
        if (dxTo) {
          searchObject.toDate = getFirstAndLastDayOfMonth(dxTo).lastDayFormatted;
        }
      }

      const res = await exportToExcel(
        searchObject,
        tabValue
      );

      if (appConst.CODE.SUCCESS === res?.status) {
        toast.success(t("general.successExport"));
      }

      const blob = new Blob([res?.data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      let fileName = '';
      switch (tabValue) {
        case appConst.tabStatus.tabMaintenance:
          fileName = 'Tài sản đề xuất bảo trì.xlsx';
          break;
        case appConst.tabStatus.tabProcessed:
          fileName = 'Thống kê tài sản bảo trì, bảo dưỡng, đã nghiệm thu.xlsx';
          break;
        case appConst.tabStatus.tabContractLiquidate:
          fileName = 'Hợp đồng đề xuất bảo trì.xlsx'
          break;
        default:
          fileName = 'Chu kỳ bảo trì.xlsx';
          break;
      }
      FileSaver.saveAs(blob, fileName);
    } catch (error) {
      toast.warning(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  setData = (value, name, source) => {
    let { currentUser } = this.state;
    if (name === variable.listInputName.listSupplier) {
      let listSupplier = value ? [...value, currentUser?.org] : [];
      this.setState({ listSupplier });
      return;
    }
    try {
      if (["dxTo", "dxFrom"].includes(name)) {
        if (value === null || isValidDate(value)) {
          this.setState({ [name]: value }, () => {
            this.setPage(0);
          })
        }
        return;
      }
      this.setState({ [name]: value, selectedList: [] }, () => {
        if (source !== variable.listInputName.listData) {
          if (name === "listSupplier") {
            this.setState({ contract: null, times: null });
          }
          if (name === "commonObject") {
            this.setState({ contract: null, times: null });
          }
          if (name === "supplier") {
            this.setState({ contract: null });
          }
          if (name === "contract") {
            let time = [];
            if (value?.performedTimesNumber) {
              for (let index = 0; index < value?.performedTimesNumber; index++) {
                time.push({ name: `Thứ ${index + 1}`, code: index + 1 })
              }
            }
            this.setState({ listTimes: time, times: null });
          }
          this.setPage(0);
        }
      });

    } catch (error) {
      console.log(error)
    }
  }

  onCheckInputChange = () => {
    let { commonObject } = this.state;

    switch (commonObject?.code) {
      case appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code:

        break;

      case appConst.OBJECT_TYPES_HHTH_MAINTAIN.BAO_TRI_HANG.code:

        break;

      case appConst.OBJECT_TYPES_HHTH_MAINTAIN.BAO_TRI_NGOAI.code:

        break;

      default:
        break;
    }
  }
  setDataCommonObject = (value, name) => {
    this.setState({ [name]: value }, () => {
      if (value) {
        this.setPage(0);
      }
    });
  }

  handleDeparmentSearch = (value, source) =>
    this.setState({ [source]: value?.code }, () => {
      this.updatePageData(this.state.tabValue);
    });

  handleOpenAdvanceSearch = () => {
    this.setState({ openAdvanceSearch: !this.state.openAdvanceSearch, commonObject: null });
  };

  handleOpenAcceptanceAdvance = () => {
    this.setState({ isAcceptance: !this.state.isAcceptance, commonObject: null });
  };

  handleOpenPrint = () => {
    let { selectedList } = this.state
    if (selectedList.length > 0) {
      this.setState({
        shouldOpenAssetPrintDialog: true
      })
    } else {
      toast.warning("Bạn chưa chọn phiếu nghiệm thu")
    }

  }

  checkStatus = (status) => {
    let itemStatus = listMaintainPlaningStatus.find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  validateAcceptance = () => {
    let { t } = this.props;
		let {selectedList, contract, times, itemList, isCheckedAcceptance, isNtWithoutContract} = this.state;
		let {isRoleOrgAdmin} = getTheHighestRole();
		let {departmentUser} = getUserInformation();
		
    if (selectedList.length > 0 && !isNtWithoutContract) return false
    if (selectedList.length <= 0 && isNtWithoutContract) {
      toast.warning(t("MaintainPlaning.messages.noAssetChosen"));
      return true;
    }
    if (!isCheckedAcceptance && !isNtWithoutContract) {
      if (itemList.length <= 0) {
        toast.warning(t("MaintainPlaning.messages.emptyAsset"));
        return true;
      }
      if (!contract?.contractId) {
        toast.warning(t("MaintainPlaning.messages.choseContractBefore"));
        return true;
      } else if (!times?.code) {
        toast.warning(t("MaintainPlaning.messages.choseTimesBefore"));
        return true;
      } else {
        return false;
      }
    } else if (isNtWithoutContract) {
			let firstManagementDepartmentId = selectedList?.[0]?.khPhongBanId;
			let hasOtherManagementDepartment = selectedList?.some(x => x.khPhongBanId !== firstManagementDepartmentId);
			let hasAssetsOfOtherMgtDepartment = selectedList?.some(x => x.khPhongBanId !== departmentUser?.id);
			
			if (hasOtherManagementDepartment && isRoleOrgAdmin) {
				toast.warning(t("MaintainPlaning.messages.differentMgtDepartmentInList"));
				return true;
			} else if (hasAssetsOfOtherMgtDepartment && !isRoleOrgAdmin) {
				toast.warning(t("MaintainPlaning.messages.differentMgtDepartment"));
				return true;
			} else {
				return false
			}
		}
		
		return false;
  }

  handleOpenAcceptanceDialog = () => {
    let { selectedList, keyword = "", tabValue } = this.state;
    if (this.validateAcceptance()) return;
    if (selectedList.length > 0) {
      this.setState({
        assets: selectedList,
        shouldOpenAcceptanceDialog: true,
      });
    } else {
			let searchObject = {
				...appConst.OBJECT_SEARCH_MAX_SIZE,
				keyword: keyword?.trim(),
			}
			this.updateTableDataCk(searchObject, tabValue).then((data) => {
				this.setState({
					assets: data?.content || [],
					shouldOpenConfirmationAcceptance: true,
				});
			})
    }
  };

  getListItemsAll = async () => {
    const { t } = this.props;
    const {
      keyword,
      page,
      tabValue,
    } = this.state;

    let { setPageLoading } = this.context;
    const searchObject = {
      keyword: keyword?.trim(),
      pageIndex: page + 1,
      pageSize: 9999,
    };
    let newArr = [];
    try {
      setPageLoading(true);
      let value = await this.updateTableDataCk(searchObject, tabValue);
      if (value?.content?.length) {
        newArr = value?.content?.map((item, index) => {
          item.index = index + 1;
          return item;
        });
      } else {
        newArr = [];
      }
      setPageLoading(false);
      return newArr;
    } catch (err) {
      setPageLoading(false);
      toast.error(t("general.error"));
      return newArr;
    }
  }

  handleOpenAcceptanceDialogAll = async () => {
    let { t } = this.props;
    let { contract, times } = this.state;
    let itemList =  await this.getListItemsAll();
    this.setState({
      selectedList: itemList,
      assets: itemList?.map(asset => ({
        ...asset,
        ckNtChiPhi: asset?.ckDonGia
      })),
      item: {
        ...contract,
        tongTien: contract?.cost
      }
    }, () => {
      if (!contract?.contractId) {
        toast.warning(t("MaintainPlaning.messages.choseContractBefore"));
        return;
      }
      if (!times?.code) {
        toast.warning(t("MaintainPlaning.messages.choseTimesBefore"));
        return;
      }
      if (itemList.length <= 0) {
        toast.warning(t("MaintainPlaning.messages.emptyAsset"));
        return;
      }
      this.setState({
        shouldOpenAcceptanceDialog: true,
        shouldOpenConfirmationAcceptance: false
      })
    })
  };

  handleEditAcceptance = async (rowData) => {
    this.handleGetDetailAcceptance(rowData).then(() => {
      this.setState({
        shouldOpenAcceptanceDialog: true,
      });
    })
  }

  handleGetDetailAcceptance = async (rowData) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      let res = await getAcceptanceDetail(rowData?.id)
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          assets: data?.amChukys,
          item: data,
        });
      } else {
        toast.warning(message);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }

  }

  onCheckAcceptance = (ev) => {
    let { tabValue } = this.state
    this.setState({
      isNtWithoutContract: !this.state.isNtWithoutContract
    }, () => this.updatePageData(tabValue))
  }

  handleShowSearchAndButtonByTab = () => {
    let {
      tabValue,
      isRoleAdmin,
      isRoleAssetManager,
      isRoleAssetUser,
      isRoleOrgAdmin,
      commonObject
    } = this.state;
    const isTabWaiting = appConst.tabStatus.tabWaiting === tabValue;
    const isTabPreProcessed = appConst.tabStatus.tabPreProcessed === tabValue;
    const isTabProcessed = appConst.tabStatus.tabProcessed === tabValue;
    const isTabAcceptance = appConst.tabStatus.tabAcceptance === tabValue;
    const isTabProposal = appConst.tabStatus.tabMaintenance === tabValue;
    const isTabPlan = appConst.tabStatus.tabPlan === tabValue;
    const isTabContractLiquidate = appConst.tabStatus.tabContractLiquidate === tabValue;
    const isTuBaoTri = commonObject?.code === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code
    const isCkTab = isTabWaiting || isTabPreProcessed || isTabProcessed;

    return {
      isCkTab,
      isTabWaiting,
      isTabPreProcessed,
      isTabProcessed,
      isTabAcceptance,
      isTabProposal,
      isTabPlan,
      isTabContractLiquidate,
      isTuBaoTri,
      showAdvancedSearchProposal: isTabProposal,
      showAdvancedSearchCycle: isTabWaiting
        || isTabPreProcessed
        || isTabProcessed
        || isTabContractLiquidate
        || isTabAcceptance,
      showButtonAcceptance: isTabWaiting && !isRoleAssetUser,
      showButtonAdvancedSearch: isTabWaiting
        || isTabPreProcessed
        || isTabProcessed
        || isTabProposal
        || isTabContractLiquidate
        || isTabAcceptance,
      showButtonExcel: isTabWaiting
        || isTabPreProcessed
        || isTabProcessed
        || isTabProposal
        || isTabContractLiquidate,
      showButtonPlan: isTabPlan || isTabProposal && (
        isRoleAssetManager || isRoleAdmin || isRoleOrgAdmin
      ),
      showButtonProposal: isTabProposal && (
        isRoleAssetManager || isRoleAssetUser
        || isRoleAdmin || isRoleOrgAdmin
      ),
      showTabAcceptance: isRoleAssetManager || isRoleAdmin || isRoleOrgAdmin,
      showTabContractLiquidate: isRoleAssetManager || isRoleAdmin || isRoleOrgAdmin,
      showTabPlan: isRoleAssetManager || isRoleAdmin || isRoleOrgAdmin,
      showSearchInput: isTabWaiting
        || isTabPreProcessed
        || isTabProcessed
        || isTabPlan,
      showButtonPrint: isTabAcceptance,
    }
  }

  handleStatusContract = (rowData) => {
    let { t } = this.props;
    let className = "";
    let value = "";

    if (!rowData?.canLiquidate) {
      className = "status status-warning";
      value = t("MaintainPlaning.notLiquidateYet");
    } else if (rowData?.canLiquidate && !rowData.contractLiquidationDate) {
      className = "status status-success";
      value = t("MaintainPlaning.canLiquidate");
    } else if (rowData?.canLiquidate && rowData.contractLiquidationDate) {
      className = "status status-info";
      value = t("MaintainPlaning.liquidated");
    }

    return <span className={className}>{value}</span>
  }

  handleGetRowColor = (rowData) => {
    let { selectedRow, commonObject, tabValue } = this.state;
    let backgroundColor = "";
    let tenDaysLater = addDays(new Date(), 6);
    const isTabWaiting = tabValue === appConst.tabStatus.tabWaiting;

    if (rowData.ckDate && isTabWaiting) {
      let currentMonth = new Date().getMonth();
      let currentYear = new Date().getFullYear();
      let rowMonth = new Date(rowData?.ckDate).getMonth();
      let rowYear = new Date(rowData?.ckDate).getFullYear();
      if (
        commonObject?.code === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code
          ? (new Date(rowData?.ckDate) === new Date())
          : (currentYear === rowYear && currentMonth === rowMonth)
      ) {
        backgroundColor = variable.listRowColors.Warning; // todo: warning for today
      } else if (
        rowData?.chinhthucCode === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code
          ? (new Date(rowData?.ckDate) < new Date())
          : (currentYear > rowYear || (currentYear === rowYear && currentMonth > rowMonth))
      ) {
        backgroundColor = variable.listRowColors.error; // todo: warning for past
      } else if (
        new Date(rowData.ckDate) <= tenDaysLater
        && new Date(rowData?.ckDate) > new Date()
        && commonObject?.code === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code
      ) {
        backgroundColor = variable.listRowColors.subWarning; // todo: warning for next 10 days from today
      } else {
        backgroundColor = "#FFF"
      }
    }
    else {
      backgroundColor = selectedRow && selectedRow === rowData?.id ? "#EEE" : "#FFF"
    }

    return ({
      backgroundColor: backgroundColor,
    })
  }

  handleEditContractLiquidate = async (rowData) => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      const data = await getItemById(rowData?.contractId);
      if (data?.status === appConst.CODE.SUCCESS) {
        this.setState({
          item: {
            ...rowData,
            ...data?.data,
            supplier: {
              code: rowData?.supplierCode,
              id: rowData?.supplierId,
              name: rowData?.supplierName
            },
            contractDate: new Date(rowData?.contractDate),
          },
          shouldOpenContractLiquidateDialog: true,
        }, () => {
          setPageLoading(false);
        });
      }
    } catch (error) {
    } finally {
      setPageLoading(false);
    }
  }

  handleKeyUpTextChangeSearch = (e) => handleKeyUp(e, this.search)

  handleCheck = (event, rowData) => {
    let { selectedList = [], itemList = [] } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      let newArray = itemList?.map((item) => ({ ...item, checked: event.target.checked })) || [];
      this.setState({
        itemList: [...newArray],
        selectedList: event.target.checked ? [...newArray] : [],
        checkAll: event.target.checked,
      })
    } else {
      let existItem = selectedList?.find(item => item?.id === rowData?.id);
      if (!existItem) {
        selectedList.push(rowData);
      } else {
        selectedList.splice(selectedList?.indexOf(existItem), 1)
      }

      itemList.map((item) => {
        if (item.id === rowData?.id) {
          item.checked = event.target.checked;
        }
        return item;
      });

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        itemList,
        checkAll: selectedList?.length === itemList.length,
      });
    }
  };

  handleSelectAction = (rowData, method) => {
    let actions = {
      [appConst.active.edit]: this.handleEdit,
      [appConst.active.delete]: this.handleDelete,
      [appConst.active.check]: this.handleCheckboxToggle,
      [appConst.active.view]: this.handleView,
      [appConst.active.print]: this.handlePrint,
      [appConst.active.propose]: this.handleEditMaintainProposal,
      [appConst.active.compare]: this.handleComparePlaning,
    };

    if (Object.keys(actions)?.includes(method.toString())) {
      actions[method]?.(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      rowsPerPage1,
      page1,
      totalElements1,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenMainProposal,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenConfirmationPlanningDialog,
      shouldOpenContractLiquidateDialog,
      shouldOpenNotificationPopup,
      tabValue,
      itemList1,
      isLoading,
      shouldOpenAcceptanceDialog,
      shouldOpenCompareDialog,
      isPrint,
      isView,
      dxFrom,
      dxTo,
      assets,
      selectedList,
      department,
      listDepartment,
      openAdvanceSearch,
      tnPhongBan,
      listTnPhongBan,
      supplier,
      listSupplier,
      commonObject,
      listCommonObject,
      contract,
      chuKyStatuses,
      times,
      listTimes,
      shouldOpenConfirmationAcceptance,
      isAcceptance,
      isCheckedAcceptance,
      ntTo,
      ntFrom,
      isNtWithoutContract,
      checkAll,
      shouldOpenAssetPrintDialog
    } = this.state;

    let TitlePage = t("Dashboard.subcategory.maintainPlaning");
    const {
      isTabWaiting,
      isTabPreProcessed,
      isTabProcessed,
      isTabAcceptance,
      isTabProposal,
      isTabPlan,
      isTuBaoTri,
      isTabContractLiquidate,
      showAdvancedSearchProposal,
      showAdvancedSearchCycle,
      showButtonAdvancedSearch,
      showButtonAcceptance,
      showButtonExcel,
      showButtonPlan,
      showButtonProposal,
      showSearchInput,
      showTabAcceptance,
      showTabContractLiquidate,
      showTabPlan,
      showButtonPrint,
      isCkTab,
    } = this.handleShowSearchAndButtonByTab();
    const { currentOrg } = this.context;
    const { MAINTAIN_PLANING } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    let implementationFormSearchObject = {
      code: appConst.listCommonObject.HINH_THUC_BAO_TRI.code,
    };
    let departmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
    };
    let supplierUnitSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      typeCodes: [appConst.TYPE_CODES.NCC_BDSC],
      isActive: STATUS_DON_VI_THUC_HIEN.HOAT_DONG.code
    };
    let searchObjectContract = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      supplierId: supplier?.id,
      contractTypeCode: appConst.TYPES_CONTRACT.BTBD
    }
    const isShowActionColumns = (
      tabValue === appConst.tabStatus.tabWaiting
      && commonObject?.code === appConst.OBJECT_TYPES_HHTH_MAINTAIN.TU_BAO_TRI.code
      && !openAdvanceSearch
    )
      || [
        appConst.tabStatus.tabAcceptance,
        appConst.tabStatus.tabMaintenance,
        appConst.tabStatus.tabPlan,
        appConst.tabStatus.tabContractLiquidate,
      ].includes(tabValue)

    const commonColumnProps = {
      render: (rowData) => {
        if (rowData?.chinhthucCode === appConst.listHinhThuc.TU_BAO_TRI) {
          return rowData?.ckDate
            ? moment(rowData.ckDate).format("DD/MM/YYYY")
            : ""
        }
        if (rowData?.ckDate) {
          return moment(rowData?.ckDate).format('MM/yyyy');
        }
      }
    };

    const commonColumns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "left",
        hidden: !isNtWithoutContract && !isTabAcceptance,
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("Asset.code"),
        field: "tsMa",
        align: "left",
        hidden: isTabAcceptance,
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        }
      },
      {
        title: t("MaintainPlaning.maintainPlaningDate"),
        field: "maintainPlaningDate",
        hidden: isTabAcceptance,
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "center",
        },
        render: commonColumnProps.render,
      },
      {
        title: t("MaintainPlaning.issueDate"),
        field: "maintainPlaningDate",
        align: "left",
        hidden: !isTabProcessed,
        minWidth: 140,
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => rowData.ckNtDate
          ? formatTimestampToDate(rowData.ckNtDate)
          : null,
      },
    ];

    const tabColumns = {
      [appConst.tabStatus.tabProcessed]: [
        ...commonColumns,
        ...ckColumns({ t, isTabProcessed }),
      ],
      [appConst.tabStatus.tabWaiting]: [
        ...commonColumns,
        ...ckColumns({ t, isTabWaiting }),
      ],
      [appConst.tabStatus.tabPreProcessed]: [
        ...commonColumns,
        ...ckColumns({ t, isTabPreProcessed }),
      ],
      [appConst.tabStatus.tabAcceptance]: [
        ...commonColumns,
        ...acceptanceColumns({
          t,
        })
      ],
      [appConst.tabStatus.tabMaintenance]: [
        ...proposalColumns({
          t,
        })
      ],
      [appConst.tabStatus.tabPlan]: [
        ...planColumns({
          t,
          checkStatus: this.checkStatus,
          page,
          rowsPerPage,
        })
      ],
      [appConst.tabStatus.tabContractLiquidate]: [
        ...contractColumns({
          t,
          handleStatusContract: this.handleStatusContract,
          page,
          rowsPerPage,
        })
      ],
    };

    let columnActions = [{
      title: t("general.action"),
      field: "custom",
      align: "center",
      hidden: !isShowActionColumns,
      minWidth: 80,
      sorting: false,
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          itemEdit={this.state}
          tabValue={tabValue}
          commonObject={commonObject}
          onSelect={this.handleSelectAction}
        />
      ),
    },
    ];

    const tabColumns1 = {
      [appConst.tabStatus.tabMaintenance]: [
        ...proposalSubColumns({ t }),
      ],
      [appConst.tabStatus.tabPlan]: [
        ...planSubColumns({ t }),
      ],
      [appConst.tabStatus.tabAcceptance]: [
        ...acceptanceSubColumns({ t }),
      ],
      // Add columns for other tabs here
    };

    const isCheckTable = () => {
      let isRenderNote = false;
      let isRenderSubTable = false;

      if (isTabWaiting) {
        isRenderNote = true
      } else if (isTabPlan || isTabProposal || isTabAcceptance) {
        isRenderSubTable = true
      }

      return {
        isRenderSubTable,
        isRenderNote,
      };
    };

    let dataView = handleConvertProposalPrintData(item);

    let dataViewAcceptance = handleConvertAcceptanceData(item);

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Asset.title"),
                path: "/list/maintain_planing",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              key={0}
              value={appConst.tabStatus.tabWaiting}
              label={t("MaintainPlaning.tabWaiting")} //chờ xử lý
            />
            <Tab
              key={1}
              value={appConst.tabStatus.tabPreProcessed}
              label={t("MaintainPlaning.tabPreProcessed")} // Đang xử lý
            />
            <Tab
              key={2}
              value={appConst.tabStatus.tabProcessed}
              label={t("MaintainPlaning.tabProcessed")}  // Đã xử lý
            />
            {showTabAcceptance && (
              <Tab
                key={3}
                value={appConst.tabStatus.tabAcceptance}
                label={t("MaintainPlaning.tabAcceptance")}  // Nghiệm thu
              />
            )}
            <Tab
              key={4}
              value={appConst.tabStatus.tabMaintenance}
              label={t("MaintainPlaning.tabMaintenance")}  // Đề xuất
            />,
            {showTabPlan && (
              <Tab
                key={5}
                value={appConst.tabStatus.tabPlan}
                label={t("MaintainPlaning.tabPlan")} // Kế hoạch
              />
            )}
            {showTabContractLiquidate && (
              <Tab
                key={6}
                value={appConst.tabStatus.tabContractLiquidate}
                label={t("MaintainPlaning.tabContractLiquidate")} // Kế hoạch
              />
            )}
          </Tabs>
        </AppBar>

        <CustomValidatorForm onSubmit={() => { }}>
          <Grid container spacing={2} className="mt-12">
            <Grid item md={8} sm={12} xs={12}>
              {showButtonProposal && (
                <Button
                  className="mt-12 mr-16 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleEditItem();
                  }}
                >
                  {t("general.addProposal")}
                </Button>
              )}
              {showButtonPlan && ( // Button Lập kế hoạch
                <Button
                  className="mt-12 mr-16 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={this.handleOpenConfirmationPlanningDialog}
                  disabled={selectedList?.length <= 0 && isTabProposal}
                >
                  {t("general.planning")}
                </Button>
              )}
              {showButtonExcel && (
                <Button
                  className="mt-12 mr-16 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={this.handleExportToExcel}
                >
                  {t("general.exportToExcel")}
                </Button>
              )}
              {showButtonAcceptance && (
                <Button
                  className="mt-12 mr-16 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={this.handleOpenAcceptanceAdvance}
                  disabled={openAdvanceSearch}
                >
                  {t("MaintainPlaning.filter_button")}
                  <ArrowDropDownIcon />
                </Button>
              )}
              {showButtonAdvancedSearch && (
                <Button
                  className="mt-12 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={this.handleOpenAdvanceSearch}
                  disabled={isAcceptance}
                >
                  {t("general.advancedSearch")}
                  <ArrowDropDownIcon />
                </Button>
              )}
              {showButtonPrint && (
                <Button
                  className="mt-12 align-bottom ml-16"
                  variant="contained"
                  color="primary"
                  onClick={this.handleOpenPrint}
                >
                  {t("general.print")}
                </Button>
              )}
            </Grid>

            {showSearchInput && (
              <Grid item md={4} sm={12} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100 mt-16"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUpTextChangeSearch}
                    placeholder={
                      isTabPlan
                        ? t("MaintainPlaning.filterByPlaningVoucher")
                        : t("MaintainPlaning.filter")
                    }
                    value={keyword}
                    id="search_box"
                    startAdornment={
                      <InputAdornment position="end">
                        <SearchIcon className="searchTable" onClick={this.search} />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
            )}

            <Grid item md={12} sm={12} xs={12}>
              {/* Tìm kiếm nâng cao */}
              <Collapse in={openAdvanceSearch || isAcceptance} className="mb-20">
                {/* Tab đã, chờ, đang, đã xử lý */}
                {showAdvancedSearchCycle && (
                  <Card elevation={0} className="pt-8 pb-12 p-16">
                    <Grid container spacing={2}>
                      {/* Bộ lọc từ ngày đến ngày */}
                      {((isCheckedAcceptance && isTabWaiting) || openAdvanceSearch) &&
                        <>
                          <Grid item md={3} sm={12} xs={12}>
                            <ValidatePicker
                              label={isCkTab ? t("MaintainPlaning.dxFromMonth") : t("MaintainPlaning.dxFrom")}
                              views={isCkTab ? ['month', 'year'] : ['year', 'month', 'date']}
                              format={isCkTab ? "MM/yyyy" : "dd/MM/yyyy"}
                              value={dxFrom || null}
                              onChange={(date) => {
                                this.handleDateChange(date, "dxFrom");
                              }}
                              maxDateMessage={dxTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                              maxDate={dxTo}
                            />
                          </Grid>
                          <Grid item md={3} sm={12} xs={12}>
                            <ValidatePicker
                              margin="none"
                              fullWidth
                              autoOk
                              label={isCkTab ? t("MaintainPlaning.dxToMonth") : t("MaintainPlaning.dxTo")}
                              views={isCkTab ? ['month', 'year'] : ['year', 'month', 'date']}
                              format={isCkTab ? "MM/yyyy" : "dd/MM/yyyy"}
                              value={dxTo ?? null}
                              onChange={(date) => {
                                this.handleDateChange(date, "dxTo");
                              }}
                              KeyboardButtonProps={{ "aria-label": "change date", }}
                              invalidDateMessage={t("general.invalidDateFormat")}
                              minDateMessage={dxFrom ? t("general.minDateToDate") : t("general.minYearDefault")}
                              maxDateMessage={t("general.maxDateDefault")}
                              minDate={dxFrom || undefined}
                              clearable
                            />
                          </Grid>
                        </>}
                      {/* Ngày nghiệm thu từ - đến*/}
                      {(isTabProcessed) && (
                        <>
                          <Grid item md={3} sm={12} xs={12}>
                            <ValidatePicker
                              margin="none"
                              fullWidth
                              autoOk
                              id="date-picker-dialog"
                              label={t("MaintainPlaning.ntFrom")}
                              format="dd/MM/yyyy"
                              value={ntFrom ?? null}
                              onChange={(date) => {
                                this.handleDateChange(date, "ntFrom");
                              }}
                              KeyboardButtonProps={{ "aria-label": "change date", }}
                              invalidDateMessage={t("general.invalidDateFormat")}
                              minDateMessage={t("general.minYearDefault")}
                              maxDateMessage={ntTo ? t("MaintainPlaning.messages.maxDateFromDateNtTo") : t("general.maxDateDefault")}
                              maxDate={ntTo || undefined}
                              clearable
                            />
                          </Grid>
                          <Grid item md={3} sm={12} xs={12}>
                            <ValidatePicker
                              margin="none"
                              fullWidth
                              autoOk
                              id="date-picker-dialog"
                              label={t("MaintainPlaning.ntTo")}
                              format="dd/MM/yyyy"
                              value={ntTo ?? null}
                              onChange={(date) => {
                                this.handleDateChange(date, "ntTo");
                              }}
                              KeyboardButtonProps={{ "aria-label": "change date", }}
                              invalidDateMessage={t("general.invalidDateFormat")}
                              minDateMessage={ntFrom ? t("MaintainPlaning.messages.minDateToDateNtFrom") : t("general.minYearDefault")}
                              maxDateMessage={t("general.maxDateDefault")}
                              minDate={ntFrom || undefined}
                              clearable
                            />
                          </Grid>
                        </>
                      )}
                      {/* Hình thức thực hiện */}
                      {(isCheckedAcceptance || openAdvanceSearch)
                        && (!isTabContractLiquidate && !isTabAcceptance)
                        && <Grid item md={3} sm={12} xs={12}>
                          <AsynchronousAutocompleteSub
                            searchFunction={searchByPageHinhThucNew}
                            searchObject={implementationFormSearchObject}
                            label={t("MaintainPlaning.implementingForm")}
                            listData={listCommonObject?.filter(x => x?.code !== appConst.listHinhThuc.TU_BAO_TRI)}
                            setListData={(value) => this.setData(
                              value,
                              "listCommonObject",
                              variable.listInputName.listData
                            )}
                            displayLable="name"
                            onSelect={(value) => this.setData(
                              value,
                              "commonObject"
                            )}
                            disabled={isAcceptance}
                            value={commonObject ? commonObject : null}
                            typeReturnFunction="list"
                          />
                        </Grid>}
                      {/* Đơn vị thực hiện */}
                      <Grid item md={3} sm={12} xs={12}>
                        <AsynchronousAutocompleteSub
                          searchFunction={searchByTextNew}
                          searchObject={supplierUnitSearchObject}
                          label={t("MaintainPlaning.implementingAgencies")}
                          listData={listSupplier}
                          nameListData="listSupplier"
                          setListData={this.setData}
                          displayLable="name"
                          name="supplier"
                          onSelect={this.setData}
                          value={supplier || null}
                        />
                      </Grid>

                      {/* Checkbox nghiệm thu */}
                      {(isTabWaiting && isAcceptance) && !contract?.contractId && <Grid item md={3} xs={12}>
                        <FormControlLabel
                          style={{ transform: "translate(10px, 10px)" }}
                          control={
                            <Checkbox
                              checked={isNtWithoutContract}
                              onChange={(ev) => this.onCheckAcceptance(ev)}
                            />
                          }
                          label="Không có hợp đồng"
                        />
                      </Grid>}
                      {openAdvanceSearch
                        && (!isTabContractLiquidate && !isTabAcceptance)
                        && <Grid item md={3} sm={12} xs={12}>
                          <AsynchronousAutocompleteSub
                            searchFunction={searchByPage}
                            searchObject={departmentSearchObject}
                            label={t("MaintainPlaning.useDepartment")}
                            listData={listDepartment}
                            nameListData="listDepartment"
                            setListData={this.setData}
                            displayLable="text"
                            name={variable.listInputName.department}
                            onSelect={this.setData}
                            value={department ? department : null}
                          />
                        </Grid>}
                      {(isTabWaiting && !isCheckedAcceptance || isTabAcceptance) && <Grid item md={3} xs={12}>
                        <AsynchronousAutocompleteSub
                          searchFunction={searchByPageMaintainContract}
                          searchObject={searchObjectContract}
                          label={t("Contract.title")}
                          selectedOptionKey="contractId"
                          onSelect={(value) => this.setData(
                            value,
                            "contract"
                          )}
                          displayLable="contractName"
                          showCode="contractCode"
                          typeReturnFunction="category"
                          value={contract ? contract : null}
                          disabled={isNtWithoutContract}
                        />
                      </Grid>}
                      {(isTabWaiting || isTabAcceptance) && contract?.contractId &&
                        <Grid item md={3} sm={12} xs={12}>
                          <AsynchronousAutocompleteSub
                            label={t("MaintainPlaning.times")}
                            searchFunction={() => {
                            }}
                            searchObject={{}}
                            listData={listTimes?.length > 0 ? listTimes : []}
                            displayLable="name"
                            onSelect={(value) => this.setData(
                              value,
                              "times"
                            )}
                            // disabled={!contract?.contractId}
                            value={times ? times : null}
                          />
                        </Grid>}
                      {!isTabWaiting && !isTabContractLiquidate && !isTabAcceptance && <Grid item md={3} xs={12}>
                        <AsynchronousAutocompleteSub
                          searchFunction={() => {
                          }}
                          searchObject={{}}
                          label={t("MaintainPlaning.status")}
                          listData={appConst.listStatusChuKy}
                          setListData={() => {
                          }}
                          displayLable="name"
                          onSelect={(value) => this.setData(
                            value,
                            "chuKyStatuses"
                          )}
                          value={chuKyStatuses ? chuKyStatuses : null}
                        />
                      </Grid>}
                      {(isTabWaiting && isAcceptance) && <Grid item md={3} xs={12}>
                        {showButtonAcceptance && (
                          <Button
                            className="mt-12 mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={this.handleOpenAcceptanceDialog}
                          >
                            {t("general.acceptance")}
                          </Button>
                        )}
                      </Grid>}
                    </Grid>
                  </Card>
                )}

                {/* Tab đề xuất */}
                {showAdvancedSearchProposal && (
                  <Card elevation={0} className="pt-8 pb-12 p-16">
                    <Grid container spacing={2}>
                      <Grid item md={4} sm={12} xs={12}>
                        <ValidatePicker
                          label={t("MaintainPlaning.dxFrom")}
                          value={dxFrom ?? null}
                          onChange={(date) => {
                            this.handleDateChange(date, "dxFrom");
                          }}
                          maxDateMessage={dxTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                          maxDate={dxTo}
                        />
                      </Grid>
                      <Grid item md={4} sm={12} xs={12}>
                        <ValidatePicker
                          label={t("MaintainPlaning.dxTo")}
                          format="dd/MM/yyyy"
                          value={dxTo ?? null}
                          onChange={(date) => {
                            this.setData(date, "dxTo");
                          }}
                          minDateMessage={dxFrom ? t("general.minDateToDate") : t("general.minYearDefault")}
                          maxDateMessage={t("general.maxDateDefault")}
                          minDate={dxFrom || undefined}
                        />
                      </Grid>
                      <Grid item md={4} sm={12} xs={12}>
                        <Autocomplete
                          size="small"
                          id="combo-box"
                          name="status"
                          className="mt-3"
                          options={appConst.listStatusAssetInMaintenanceProposal}
                          onChange={(e, value) =>
                            this.handleStatusSearch(
                              e,
                              value,
                              variable.listInputName.status
                            )
                          }
                          getOptionLabel={(option) => option.name}
                          renderInput={(params) => (
                            <TextField {...params} label={t("Asset.status")} />
                          )}
                        />
                      </Grid>
                      <Grid item md={4} sm={12} xs={12}>
                        <AsynchronousAutocompleteSub
                          searchFunction={searchByPage}
                          searchObject={departmentSearchObject}
                          label={t("MaintenanceProposal.proposedDepartment")}
                          listData={listDepartment}
                          setListData={(value) =>
                            this.setData(
                              value,
                              "listDepartment",
                              variable.listInputName.listData
                            )
                          }
                          displayLable="text"
                          onSelect={(value) =>
                            this.setData(
                              value,
                              variable.listInputName.department
                            )
                          }
                          value={department ? department : null}
                        />
                      </Grid>
                      <Grid item md={4} sm={12} xs={12}>
                        <AsynchronousAutocompleteSub
                          searchFunction={searchByPageDepartmentByOrg}
                          searchObject={{ isAssetManagement: true }}
                          displayLable={"name"}
                          typeReturnFunction="category"
                          showCode="code"
                          label={t("allocation_asset.receiverDepartment")}
                          listData={listTnPhongBan}
                          setListData={(value) => this.setData(
                            value,
                            "listTnPhongBan"
                          )}
                          onSelect={(value) => this.setData(
                            value,
                            "tnPhongBan"
                          )}
                          value={tnPhongBan ? tnPhongBan : null}
                        />
                      </Grid>
                      <Grid item md={4} sm={12} xs={12}>
                        {showButtonAcceptance && (
                          <Button
                            className="mt-12 mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={this.handleOpenAcceptanceDialog}
                          >
                            {t("general.acceptance")}
                          </Button>
                        )}
                      </Grid>
                    </Grid>
                  </Card>
                )}
              </Collapse>
              <Grid item xs={12}>
                <div></div>
                <Grid container spacing={2} justifyContent="space-between">
                  {/* <Grid item md={isCheckTable() ? 6 : 12} xs={12} > */}
                  <Grid item md={12} xs={12}>
                    {this.renderMaterialTable(
                      "general.list",
                      [...itemList],
                      tabColumns[tabValue] || [],
                      columnActions,
                      (e, rowData) => this.handleRowClick(e, rowData),
                      isLoading
                    )}
                    {this.renderTablePagination(
                      totalElements,
                      rowsPerPage,
                      page,
                      this.handleChangePage,
                      this.setRowsPerPage
                    )}
                    {isCheckTable().isRenderNote && (
                      <div className="flex highlight-note-container">
                        <span className="p-8 text-error over-expected">
                          {t("MaintainPlaning.overExpected")}
                        </span>
                        {isTuBaoTri ? (
                          <span className="p-8 expected-today">
                            {t("MaintainPlaning.expectedToday")}
                          </span>
                        ) : (
                          <span className="p-8 expected-today">
                            {t("MaintainPlaning.expectedThisMonth")}
                          </span>
                        )}
                        {isTuBaoTri && (
                          <span className="p-8 expected-next-ten-days">
                            {t("MaintainPlaning.expectedNextTenDays")}
                          </span>
                        )}
                      </div>
                    )}
                  </Grid>

                  {isCheckTable().isRenderSubTable && (
                    <Grid item md={12} xs={12}>
                      {this.renderMaterialTable(
                        "general.list",
                        itemList1,
                        tabColumns1[tabValue] || []
                      )}
                      {this.renderTablePagination(
                        totalElements1,
                        rowsPerPage1,
                        page1,
                        this.handleChangePage1,
                        this.setRowsPerPage1
                      )}
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </Grid>

          </Grid>
        </CustomValidatorForm>
        {isPrint && isTabProposal && (
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleDialogClose}
            open={isPrint && isTabProposal}
            item={dataView || item}
            title={t("MaintenanceProposal.title")}
            urls={[
              ...MAINTAIN_PLANING.PROPOSAL.GENERAL,
              ...(MAINTAIN_PLANING.PROPOSAL[currentOrg?.printCode] || []),
            ]}
          />
        )}
        {isPrint && isTabAcceptance && (
          <PrintPreviewTemplateDialog
            t={t}
            handleClose={this.handleDialogClose}
            open={isPrint && isTabAcceptance}
            item={item}
            title={t("Phiếu in nghiệm thu bảo trì với đơn vị thực hiện")}
            model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.MAINTAIN_PLANING.ACCEPTANCE.FOR_SUPPLIER}
          />
        )}

        {shouldOpenEditorDialog && (
          <MaintainPlaningDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleEditorDialogClose}
            open={shouldOpenEditorDialog}
            handleOKEditClose={this.handleOKEditClose}
            item={item}
            asset={this.state.asset}
            amDexuatIds={selectedList || []}
            isView={isView}
          />
        )}

        {/* dialog de xuat */}
        {shouldOpenMainProposal && (
          <MaintenanceProposalDialog
            t={t}
            item={item}
            isView={isView}
            handleClose={this.handleMaintenanceProposalDialogClose}
            open={shouldOpenMainProposal}
            handleOKEditClose={this.handleOKEditClose}
          />
        )}

        {shouldOpenConfirmationDialog && (
          <ConfirmationDialog
            title={t("general.confirm")}
            open={shouldOpenConfirmationDialog}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("general.deleteConfirm")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        {shouldOpenCompareDialog && (
          <ComparePlaningDialog
            t={t}
            i18n={i18n}
            item={item}
            assets={assets}
            isView={isView}
            handleClose={this.handleDialogClose}
            open={shouldOpenCompareDialog}
            handleOKEditClose={this.handleOKEditClose}
          />
        )}
        {shouldOpenAcceptanceDialog && (
          <AcceptanceDialog
            t={t}
            i18n={i18n}
            item={item}
            state={this.state}
            assets={assets}
            isView={isView}
            isTuBaoTri={isCheckedAcceptance}
            handleClose={this.handleDialogClose}
            open={shouldOpenAcceptanceDialog}
            handleOKEditClose={this.handleOKEditClose}
          />
        )}
        {shouldOpenConfirmationDeleteAllDialog && (
          <ConfirmationDialog
            open={shouldOpenConfirmationDeleteAllDialog}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDeleteAll}
            text={t("general.deleteAllConfirm")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        {shouldOpenConfirmationAcceptance && (
          <ConfirmationDialog
            title={t("general.noti")}
            open={shouldOpenConfirmationAcceptance}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleOpenAcceptanceDialogAll}
            text={`Thực hiện nghiệm thu với tất cả tài sản của hợp đồng ${contract?.contractName} lần thứ ${times?.code}`}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        {shouldOpenConfirmationPlanningDialog && (
          <ConfirmationDialog
            open={shouldOpenConfirmationPlanningDialog}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handlePlanning}
            title={t("general.confirm")}
            text={
              t("general.planningConfirm") +
              `${selectedList?.length}` +
              t("general.planningConfirmNext")
            } // 1 là số lượng phiếu (trạng thái chờ xử lý) đã chọn ở bảng danh sách phiếu. Hiện tại đang fix cứng
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        {shouldOpenContractLiquidateDialog && (
          <ContractDialog
            t={t} i18n={i18n}
            handleClose={this.handleDialogClose}
            open={shouldOpenContractLiquidateDialog}
            handleOKEditClose={this.handleOKEditClose}
            item={item}
            isView={isView}
            isMaintainPlaning={true}
            contractTypeCode={appConst.TYPES_CONTRACT.BTBD}
          />
        )}
        {shouldOpenAssetPrintDialog && (
          <AssetPrintDepartmentsDialog
            t={t} i18n={i18n}
            open={shouldOpenAssetPrintDialog}
            handleOKEditClose={this.handleOKEditClose}
            item={item}
            amNghiemThuIds={selectedList?.map(x => x.id) || []}
          />
        )}
      </div>
    );
  }
}

MaintainPlaningTable.contextType = AppContext;
export default withRouter(MaintainPlaningTable);
