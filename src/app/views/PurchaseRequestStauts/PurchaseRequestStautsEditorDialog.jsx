import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { getAllPurchaseRequestStauts, addNewOrUpdatePurchaseRequestStauts, searchByPage, checkCode } from './PurchaseRequestStautsService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { handleKeyDownNameSpecialExcept } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class PurchaseRequestStautsEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    shouldOpenNotificationPopup: false,
    indexOrder:0
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(this.props.t('AssetType.noti.dupli_code'));
        // this.setState({shouldOpenNotificationPopup: true,
        //   Notification:"AssetType.noti.dupli_code"})
        // alert("Code đã được sử dụng");
      } else {
        if (id) {
          addNewOrUpdatePurchaseRequestStauts({
            ...this.state
          }).then(() => {
            toast.info("Sửa trạng thái thành công")
            this.props.handleOKEditClose();
          });
        } else {
          addNewOrUpdatePurchaseRequestStauts({
            ...this.state
          }).then(() => {
            toast.info("Thêm mới trạng thái thành công")
            this.props.handleOKEditClose();
          });
        }
      }
    });
    
    
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;
    this.setState({
      ...this.props.item
    }, function () {

    }
    );
  }
  componentDidMount() {
    ValidatorForm.addValidationRule('isNumber', (value) => {
      let regexp = /[e,E]/g;
      let result = value.match(regexp);
      if (result !== null && result.length > 0) {
          return false;
      }
      return true;
    });
  }
  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let {
      name,
      code,
      shouldOpenNotificationPopup,indexOrder
    } = this.state;

    return (
      <Dialog open={open} maxWidth="md" PaperComponent={PaperComponent}>
       {shouldOpenNotificationPopup && (
         <NotificationPopup
           title={t('general.noti')}
           open={shouldOpenNotificationPopup}
          // onConfirmDialogClose={this.handleDialogClose}
           onYesClick={this.handleDialogClose}
           text={t(this.state.Notification)}
           agree={t('general.agree')}
        />
      )}
        <DialogTitle className="cursor-move" id="draggable-dialog-title">
          {t("purchaseRequestStauts.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
        <DialogContent>
            <Grid className="mb-16" container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span style= {{color :"red"}}>* </span> <span> {t("purchaseRequestStauts.code")}</span></span>}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", "matchRegexp:^[a-zA-Z0-9_.-]*$"]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>

              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span><span style= {{color :"red"}}>* </span> <span> {t("purchaseRequestStauts.name")}</span></span>}
                  onChange={this.handleChange}
                  onKeyDown={(e) => {
                    handleKeyDownNameSpecialExcept(e)
                  }}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
              <TextValidator
                  className="w-100 mb-16"
                  label={t('InventoryCountStatus.indexOrder')}
                  onChange={this.handleChange}
                  type="text"
                  name="indexOrder"
                  value={indexOrder}
                  validators={['minNumber:0', 'matchRegexp:^[0-9]{1,9}$']}
                  errorMessages={[t('general.invalidFormat'),t('general.invalidFormat')]}
                />
                </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="secondary" className="mr-12" onClick={() => this.props.handleClose()}>{t('general.cancel')}</Button>
              <Button variant="contained" color="primary" className="mr-12" type="submit">
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog >
    );
  }
}

export default PurchaseRequestStautsEditorDialog;
