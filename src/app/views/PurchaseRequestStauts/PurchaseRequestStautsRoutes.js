import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const PurchaseRequestStautsTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./PurchaseRequestStautsTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(PurchaseRequestStautsTable);

const PurchaseRequestStautsRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/purchase_request_status",
    exact: true,
    component: ViewComponent
  }
];

export default PurchaseRequestStautsRoutes;