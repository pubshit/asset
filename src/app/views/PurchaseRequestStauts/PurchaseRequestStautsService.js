import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT +
  "/api/purchase_request_status" +
  ConstantList.URL_PREFIX;

export const getAllPurchaseRequestStauts = () => {
  return axios.get(API_PATH + "/1/100000");
};

export const getPurchaseRequestStautsByPage = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH + params;
  return axios.get(url);
};
export const searchByPage = (searchObject) => {
  return axios.post(API_PATH + "/searchByPage", searchObject);
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdatePurchaseRequestStauts = (PurchaseRequestStauts) => {
  return axios.post(API_PATH, PurchaseRequestStauts);
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
