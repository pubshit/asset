import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetTransferTable = EgretLoadable({
  loader: () => import("./AssetTransferTable")
});
const ViewComponent = withTranslation()(AssetTransferTable);

const AssetTransferRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/transfer-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default AssetTransferRoutes;