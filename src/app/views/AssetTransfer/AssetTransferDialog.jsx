import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  getAssetDocumentById,
  getNewCodeDocument,
} from "../Asset/AssetService";
import {
  updateStatus,
  createTransfer,
  updateTransfer,
} from "./AssetTransferService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TransferScrollableTabsButtonForce from "./TransferScrollableTabsButtonForce";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkInvalidDate,
  checkMinDate,
  formatDateDto,
  formatDateTimeDto,
  getTheHighestRole,
  isCheckLenght,
  isInValidDateForSubmit,
  isSuccessfulResponse
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import RefuseTransferDialog from "../Component/InstrumentToolsTransfer/RefuseTransferDialog"
import moment from "moment";
import { PaperComponent } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Transfer,
    handoverPerson: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: [2], // cấp phát
    handoverPersonClone: false,
    assetDocumentList: [],
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER,
    shouldOpenPopupAssetFile: false,
    transferStatus: null,
    shouldOpenRefuseTransfer: false,
    isConfirm: false,
    isReceive: false,
    note: null,
  };
  voucherType = ConstantList.VOUCHER_TYPE.Transfer; //Điều chuyển

  convertDto = (state) => {
    let data = {
      assetVouchers: state?.assetVouchers,
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      handoverPersonId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      receiverDepartmentId: state?.receiverDepartmentId,
      receiverPersonName: state?.receiverPerson?.personDisplayName,
      receiverPersonId: state?.receiverPerson?.personId,
      transferStatusIndex: state?.transferStatusIndex,
      note: state?.note
    }
    return data;
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable.listInputName.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowData.tableData?.id) {
        assetVoucher.note = valueText.target.value;
      }
    });
    this.setState({ assetVouchers: assetVouchers, handoverPersonClone: true });
  };

  selectUsePerson = (item, rowdata) => {
    let { assetVouchers } = this.state;
    assetVouchers.map(async (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.usePerson = item ? item : null;
        assetVoucher.usePersonId = item?.id;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true }
    );
  };

  validateForm = () => {
    let {
      assetVouchers,
      handoverDepartment,
      handoverPerson,
      transferStatus,
      receiverDepartment,
      issueDate,
      receiverPerson
    } = this.state;
    if (checkInvalidDate(issueDate)) {
      toast.warning('Ngày chứng từ không tồn tại (Thông tin phiếu).');
      return false;
    }
    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(`Ngày chứng từ không được nhỏ hơn ngày "01/01/1900"(Thông tin phiếu).`);
      return false;
    }
    if (!handoverDepartment) {
      toast.warning("Vui lòng chọn phòng ban bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!transferStatus) {
      toast.warning("Vui lòng chọn trạng thái điều chuyển(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!handoverPerson) {
      toast.warning("Vui lòng nhập người bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!receiverDepartment) {
      toast.warning("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }
    if (receiverDepartment?.id === handoverDepartment?.id) {
      toast.warning("Phòng ban bàn giao không được trùng với Phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn tài sản");
      toast.clearWaitingQueue();
      return false;
    }
    if (!receiverPerson?.personId) {
      toast.warning("Vui lòng chọn người tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      return false;
    }

    let checkInvalidReceptionDate = assetVouchers?.find(x => new Date(x?.asset?.dateOfReception).getTime() > new Date(issueDate).getTime());
    
    if(checkInvalidReceptionDate) {
      toast.warning(`Ngày tiếp nhận của ${checkInvalidReceptionDate?.asset?.code} không được lớn hơn ngày chứng từ(Thông tin phiếu).`);
      toast.clearWaitingQueue();
      return false;
    }

    if (this.validateAssetVouchers()) return;
    return true;
  }
  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning("Ghi chú không nhập quá 255 ký tự.");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let {
      t,
      handleOKEditClose = () => { },
    } = this.props;
    if (e.target.id !== "assetTransferDialog") return;
    if (!this.validateForm()) return;
    if (isInValidDateForSubmit()) { return; }

    let dataState = this.convertDto(this.state);
    setPageLoading(true);
    try {
      let res = id
        ? await updateTransfer(id, dataState)
        : await createTransfer(dataState)
      const { code, data, message } = res?.data;

      if (isSuccessfulResponse(code)) {
        handleOKEditClose();
        toast.success(message);
      }
      else {
        toast.warning(message);
      }
    }
    catch (err) {
      toast.clearWaitingQueue();
      toast.warning(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  };

  handleDateChange = (date, name) => {
    if (name === "issueDate" && date) {
      this.setState({
        [name]: date,
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  handleSelectAsset = (items) => {
    let assetValid = {
      asset: {},
      isAssetValid: true
    }
    // eslint-disable-next-line no-unused-expressions
    items?.forEach((element) => {
      let issueDateTimestamp = moment(this.state.issueDate)
      let dateOfReception = moment(element?.asset?.dateOfReception)
      if (issueDateTimestamp.isBefore(dateOfReception)) {
        assetValid.isAssetValid = false
        assetValid.asset = element
      }
      element.asset.useDepartment = this.state?.receiverDepartment;
      element.assetId = element.asset?.id;
    });
    if (!assetValid.isAssetValid) {
      toast.warning(`Ngày tiếp nhận của tài sản ${assetValid?.asset?.asset?.name} lớn hơn ngày chứng từ`)
      return
    }
    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning('Chưa có tài sản nào được chọn');
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  componentWillMount() { }

  componentDidMount() {
    const { isRoleAssetUser, departmentUser } = getTheHighestRole();
    let { item } = this.props;
    if (item.id) {
      this.setState({
        ...this.props.item,
        handoverPersonClone: true,
        handoverDepartment: {
          id: item?.handoverDepartmentId,
          text: item?.handoverDepartmentName,
          name: item?.handoverDepartmentName,
        },
        handoverPerson: {
          personId: item?.handoverPersonId,
          personDisplayName: item?.handoverPersonName,
        },
        receiverDepartment: {
          id: item?.receiverDepartmentId,
          text: item?.receiverDepartmentName,
          name: item?.receiverDepartmentName,
        },
        receiverPerson: {
          personId: item?.receiverPersonId,
          personDisplayName: item?.receiverPersonName,
        },
      });
    } else {
      let statusCreate = appConst.STATUS_TRANSFER.CHO_XAC_NHAN;
      this.setState({
        ...this.props.item,
        transferStatus: statusCreate,
        transferStatusIndex: statusCreate?.transferStatusIndex
      });
      if (isRoleAssetUser) {
        this.setState({
          handoverDepartment: {
            ...departmentUser,
            text: departmentUser?.name
          },
          handoverDepartmentId: departmentUser?.id,
        });
      }
    }
  }

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment?.id) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
          departmentId: handoverDepartment?.id,
        },
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.info("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleReceiverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  selectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: item,
      receiverDepartmentId: item?.id,
    })
  };

  handleSelectReceiverDepartment = (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );

    let receiverDepartment = {
      id: item?.id,
      name: item?.name,
      text: item?.name,
    };

    this.setState(
      {
        receiverDepartment: item ? receiverDepartment : null,
        receiverDepartmentId: item?.id,
        assetVouchers: assetVouchers,
        listReceiverPerson: [],
        receiverPerson: null,
        receiverPersonId: null,
        isCheckReceiverDP: !item,
      }
    );
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let handoverDepartment = {
      id: item?.id,
      name: item?.text || item?.name,
      text: item?.text || item?.name,
    };

    this.setState(
      {
        handoverDepartment: item ? handoverDepartment : null,
        handoverDepartmentId: item?.id,
        assetVouchers: [],
        listHandoverPerson: [],
        handoverPerson: null,
        handoverPersonId: null,
      },
    );
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleStatus = (dataStatus) => {
    if (typeof dataStatus !== "number") {
      if (dataStatus?.target?.id !== "assetTransferDialog") return;
    }

    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { t } = this.props;
    let status = this.state.isStatus ? this.state?.transferStatus?.indexOrder : dataStatus;
    let issueDate = formatDateTimeDto(this.state.issueDate, 23, 59, 59);
    let dataState = this.convertDto(this.state)
    let dataTranfer = {
      dataState: dataState,
      id: this.state?.id,
      note: this.state?.note,
      status: status,
      issueDate: issueDate,
    }
    updateStatus(dataTranfer)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("InstrumentToolsTransfer.notification.confirmSucces"));
          this.props.handleOKEditClose();
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("InstrumentToolsTransfer.notification.faliConfirm"));
        setPageLoading(false);
      });
  }

  handleRefuse = () => {
    this.setState({
      shouldOpenRefuseTransfer: true
    });
  }

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    },
      () => this.props.handleOKEditClose()
    );
  }

  selectAssetTransferStatusStatus = (item) => {
    this.setState({
      transferStatus: item,
      transferStatusIndex: item?.indexOrder
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  clearStateDate = () => {
    this.setState({
      handoverDepartment: null,
      handoverDepartmentId: "",
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      transferStatus: appConst.STATUS_TRANSFER.CHO_XAC_NHAN,
      transferStatusIndex: appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder,
    })
  }

  render() {
    const { isRoleAssetUser, isRoleAssetManager, isRoleOrgAdmin } = getTheHighestRole();
    let { open, t, i18n } = this.props;
    let searchObjectStatus = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.handoverDepartment
        ? this.state.handoverDepartment?.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let {
      isView,
      isConfirm,
      isStatus,
      isReceive,
    } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        <ValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={isStatus ? this.handleStatus : this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("AssetTransfer.dialog")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <TransferScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAssetTransferStatusStatus={this.selectAssetTransferStatusStatus}
              handleHandoverDepartmentPopupOpen={this.handleHandoverDepartmentPopupOpen}
              handleSelectHandoverDepartment={this.handleSelectHandoverDepartment}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleHandoverDepartmentPopupClose={this.handleHandoverDepartmentPopupClose}
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupOpen={this.handleReceiverDepartmentPopupOpen}
              handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleReceiverDepartmentPopupClose={this.handleReceiverDepartmentPopupClose}
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              selectUsePerson={this.selectUsePerson}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              getAssetDocument={this.getAssetDocument}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              selectReceiverDepartment={this.selectReceiverDepartment}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              isRoleAssetUser={isRoleAssetUser}
              isRoleAssetManager={isRoleAssetManager}
              isRoleOrgAdmin={isRoleOrgAdmin}
            />

            {this.state?.shouldOpenRefuseTransfer &&
              <RefuseTransferDialog
                t={t}
                open={this.state?.shouldOpenRefuseTransfer}
                item={this.state}
                handleChange={this.handleChange}
                handleClose={this.handleCloseRefuseTransferDialog}
                handleConfirmReason={() => this.handleStatus(appConst.STATUS_TRANSFER.HUY_XAC_NHAN.indexOrder)}
              />
            }
          </DialogContent>
          <DialogActions>
            <div className="flex flex-middle px-24 g-10">
              <Button
                variant="contained"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {isConfirm && (
                <Button
                  className="btnRefuse"
                  variant="contained"
                  onClick={this.handleRefuse}
                >
                  {t("InstrumentToolsTransfer.refuse")}
                </Button>
              )}
              {(isConfirm || isReceive) ? (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(
                    isConfirm
                      ? appConst.STATUS_TRANSFER.DA_XAC_NHAN.indexOrder
                      : appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                  )} >
                  {isConfirm && t("InstrumentToolsTransfer.confirm")}
                  {isReceive && t("general.receive")}
                </Button>
              ) : (
                <>
                  {!isView && (
                    <Button variant="contained" color="primary" type="submit">
                      {t("general.save")}
                    </Button>
                  )}
                </>
              )}
              {this.state?.transferStatusIndex === appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                && this.state.id && (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      this.props.handleQrCode(this.state, true);
                    }}
                  >
                    {t("Asset.PrintQRCode")}
                  </Button>
                )}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetTransferDialog.contextType = AppContext;
export default AssetTransferDialog;
