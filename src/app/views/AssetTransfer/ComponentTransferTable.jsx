import {
    Card,
    Grid,
    Input,
    Button,
    Collapse,
    FormControl,
    InputAdornment,
    TablePagination,
    TextField,
} from "@material-ui/core";
import React, { useContext, useEffect } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import AssetTransferDialog from "./AssetTransferDialog";
import MaterialTable, { MTableToolbar } from "material-table";
import {appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, variable} from "app/appConst";
import { dataViewDCTSTB } from "../FormCustom/DCTSTB";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { NumberFormatCustom, filterOptions } from "app/appFunction";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import {importExcelAssetTransferURL,} from './AssetTransferService';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import { Autocomplete, Pagination } from "@material-ui/lab";
import AssetsQRPrint from "../Asset/ComponentPopups/AssetsQRPrint";
import viLocale from "date-fns/locale/vi";
import CardContent from "@material-ui/core/CardContent";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { searchByPageDeparment } from "../Department/DepartmentService";
import { STATUS_DEPARTMENT } from "../../appConst";
import {exportExcelFileError} from "../../appServices";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";

function ComponentTransferTable(props) {
  let {t, i18n, getRowData} = props;
  let {
    item,
    page,
    isPrint,
    itemList,
    fromDate,
    openAdvanceSearch,
    toDate,
    rowsPerPage,
    isRoleAdmin,
    statusIndex,
    totalElements,
    transferStatus,
    originalCostTo,
    isRoleAssetUser,
    originalCostFrom,
    receiverDepartment,
    handoverDepartment,
    hasCreatePermission,
    listHandoverDepartment,
    listReceiverDepartment,
    shouldOpenEditorDialog,
    shouldOpenImportExcelDialog,
    shouldOpenConfirmationDialog,
    shouldOpenConfirmationDeleteAllDialog,
    openPrintQR,
    products,
    isDisable,
    selectedItem
  } = props?.item;
  const {currentOrg} = useContext(AppContext);
  const {TRANSFER} = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
  let isButtonAll = !statusIndex;
  const searchObject = {...appConst.OBJECT_SEARCH_MAX_SIZE,};
  const searchObjectDepartment = {
    ...searchObject,
    checkPermissionUserDepartment: false,
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
  }
  const isShowPrintPreview = [
    LIST_ORGANIZATION.BVDK_MY_DUC.code,
  ].includes(currentOrg?.code);

    useEffect(() => {
        let value = appConst.listStatusTransfer.find(item => item.indexOrder === statusIndex)
        props.handleSetDataSelect(value, "transferStatus")
    }, [statusIndex]);

    const handleKeyDown = (e) => {
        if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
            e.preventDefault();
        }
        props.handleKeyDownEnterSearch(e);
    };

    return (
        <>
            <Grid container spacing={2} justifyContent="space-between">
                <Grid item md={6} xs={12}>
                    {(hasCreatePermission || isRoleAdmin) && isButtonAll && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleButtonAdd({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("Asset.transfer_asset")}
                        </Button>
                    )}
                    {!isRoleAssetUser && (
                        <Button
                            className="align-bottom mr-16"
                            variant="contained"
                            color="primary"
                            onClick={props.importExcel}
                        >
                            {t("general.importExcel")}
                        </Button>
                    )}
                    {!isRoleAssetUser && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.exportToExcel}
                        >
                            {t("general.exportToExcel")}
                        </Button>
                    )}
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>


                    {shouldOpenImportExcelDialog && (
                        <ImportExcelDialog
                            t={t}
                            i18n={i18n}
                            handleClose={props.handleDialogClose}
                            open={shouldOpenImportExcelDialog}
                            handleOKEditClose={props.handleOKEditClose}
                            exportExampleImportExcel={props.exportExampleImportExcel}
                            exportFileError={exportExcelFileError}
                            url={importExcelAssetTransferURL()}
                        />
                    )}

                    {shouldOpenConfirmationDeleteAllDialog && (
                        <ConfirmationDialog
                            open={shouldOpenConfirmationDeleteAllDialog}
                            onConfirmDialogClose={props.handleDialogClose}
                            onYesClick={props.handleDeleteAll}
                            text={t("general.deleteAllConfirm")}
                        />
                    )}

                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("AssetTransfer.filter")}
                            id="search_box"
                            startAdornment={
                                <InputAdornment position="end">
                                    <SearchIcon
                                        onClick={() => props.search()}
                                        className="searchTable"
                                    />
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
                {/* Bộ lọc xuất excel */}
                <Grid item xs={12}>
                    <Collapse in={openAdvanceSearch}>
                        <ValidatorForm onSubmit={() => { }}>
                            <Card elevation={2}>
                                <CardContent>
                                    <Grid container xs={12} spacing={2}>
                                        {/* Phòng bàn giao */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteTransfer
                                                label={t("allocation_asset.handoverDepartment")}
                                                searchFunction={searchByPageDeparment}
                                                searchObject={searchObjectDepartment}
                                                listData={listHandoverDepartment || []}
                                                setListData={(data) => props?.handleSetDataSelect(
                                                    data,
                                                    "listHandoverDepartment",
                                                    variable.listInputName.listData,
                                                )}
                                                typeReturnFunction='category'
                                                defaultValue={handoverDepartment ? handoverDepartment : null}
                                                displayLable={"name"}
                                                value={handoverDepartment ? handoverDepartment : null}
                                                onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        {/* Phòng tiếp nhận */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteTransfer
                                                className="w-100"
                                                label={t("allocation_asset.receiverDepartment")}
                                                searchFunction={searchByPageDeparment}
                                                searchObject={searchObjectDepartment}
                                                listData={listReceiverDepartment || []}
                                                setListData={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "listReceiverDepartment",
                                                    variable.listInputName.listData,
                                                )}
                                                defaultValue={receiverDepartment ? receiverDepartment : null}
                                                displayLable={"name"}
                                                value={receiverDepartment ? receiverDepartment : null}
                                                onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                                                filterOptions={(options, params) => {
                                                    params.inputValue = params.inputValue.trim()
                                                    return filterOptions(options, params)
                                                }}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        {/* Từ ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxFrom")}
                                                    format="dd/MM/yyyy"
                                                    value={fromDate ?? null}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "fromDate"
                                                    )}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    maxDate={toDate ? new Date(toDate) : undefined}
                                                    minDateMessage={t("general.minDateMessage")}
                                                    maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        {/* Đến ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxTo")}
                                                    format="dd/MM/yyyy"
                                                    value={toDate ?? null}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "toDate"
                                                    )}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    minDate={fromDate ? new Date(fromDate) : undefined}
                                                    minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                    maxDateMessage={t("general.maxDateMessage")}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        {/* Trạng thái */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <Autocomplete
                                                fullWidth
                                                options={appConst.listStatusTransfer.filter(i => i.transferStatusIndex !== appConst.STATUS_TRANSFER.TRA_VE.indexOrder)}
                                                defaultValue={transferStatus ? transferStatus : null}
                                                value={transferStatus ? transferStatus : null}
                                                onChange={(e, value) => props.handleSetDataSelect(value, "transferStatus")}
                                                filterOptions={(options, params) => {
                                                    params.inputValue = params.inputValue.trim()
                                                    return filterOptions(options, params)
                                                }}
                                                getOptionLabel={option => option.name}
                                                disabled={isDisable}
                                                noOptionsText={t("general.noOption")}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        value={transferStatus?.name || ""}
                                                        label={t("maintainRequest.status")}
                                                    />
                                                )}
                                            />
                                        </Grid>
                                        {/* Nguyên giá từ - đến */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <TextValidator
                                                fullWidth
                                                label={t("general.originalCostFrom")}
                                                value={originalCostFrom}
                                                name="originalCostFrom"
                                                onKeyUp={props.handleKeyUp}
                                                onKeyDown={handleKeyDown}
                                                onChange={(e) => props.handleSetDataSelect(
                                                    e?.target?.value,
                                                    e?.target?.name,
                                                )}
                                                InputProps={{
                                                    inputComponent: NumberFormatCustom,
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={3}>
                                            <TextValidator
                                                fullWidth
                                                label={t("general.originalCostTo")}
                                                value={originalCostTo}
                                                name="originalCostTo"
                                                onKeyUp={props.handleKeyUp}
                                                onKeyDown={handleKeyDown}
                                                onChange={(e) => props.handleSetDataSelect(
                                                    e?.target?.value,
                                                    e?.target?.name,
                                                )}
                                                InputProps={{
                                                    inputComponent: NumberFormatCustom,
                                                }}
                                            />
                                        </Grid>
                                        {/* Button xuất excel */}
                                        {/* <Grid item container xs={12} sm={12} md={3} justifyContent="flex-end">
                                            <Grid item>

                                            </Grid>
                                        </Grid> */}

                                    </Grid>
                                </CardContent>
                            </Card>
                        </ValidatorForm>
                    </Collapse>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {isPrint && (
                          isShowPrintPreview ? (
                            <PrintPreviewTemplateDialog
                              t={t}
                              handleClose={props.handleDialogClose}
                              open={isPrint}
                              item={item}
                              title={t("Phiếu điều chuyển tài sản thiết bị")}
                              model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.TRANSFER}
                            />
                          ) : (
                            <PrintMultipleFormDialog
                              t={t}
                              i18n={props.item?.i18n}
                              handleClose={props.handleDialogClose}
                              open={isPrint}
                              item={dataViewDCTSTB(item)}
                              title={t("Phiếu điều chuyển tài sản thiết bị")}
                              urls={[
                                ...TRANSFER.GENERAL,
                                ...(TRANSFER[currentOrg?.printCode] || []),
                              ]}
                            />
                          )
                        )}

                        {shouldOpenEditorDialog && (
                            <AssetTransferDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                handleQrCode={props.handleQrCode}
                            />
                        )}
                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {openPrintQR && (
                            <AssetsQRPrint
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                    </div>
                    <MaterialTable
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            maxBodyHeight: "490px",
                            minBodyHeight: "260px",
                            padding: "dense",
                            toolbar: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: selectedItem?.id === rowData?.id
                                    ?
                                    "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        labelRowsPerPage={t("general.rows_per_page")}
                        labelDisplayedRows={({ from, to, count, page }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
                        }
                        component="paper"
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{ "aria-label": "Previous Page", }}
                        nextIconButtonProps={{ "aria-label": "Next Page", }}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid >
        </>
    )
}

export default ComponentTransferTable;
