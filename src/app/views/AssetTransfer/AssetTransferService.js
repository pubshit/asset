import axios from "axios";
import ConstantList from "../../appConfig";
import {STATUS_DEPARTMENT} from "app/appConst";
const API_PATH = ConstantList.API_ENPOINT + "/api/asset_transfer_status";
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_TRANSFER =
  ConstantList.API_ENPOINT + "/api/v1/fixed-assets/transfer-vouchers";
const API_PATH_USER =
  ConstantList.API_ENPOINT + "/api/v1/user-departments/page";
const API_PATH_STORE = ConstantList.API_ENPOINT + "/api/stores/org";
const API_PATH_TEMPLATE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

//api mới
export const searchByPage = (searchObject) => {
  let url = API_PATH_TRANSFER + `/page`;
  let config = {
    params: searchObject,
  };
  return axios.get(url, config);
};

export const getItemById = (id) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.get(url);
};
export const createTransfer = (data) => {
  return axios.post(API_PATH_TRANSFER, data);
};
export const updateTransfer = (id, data) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.put(url, data);
};
export const deleteItem = (id) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.delete(url);
};
export const getCountStatus = () => {
  let url = API_PATH_TRANSFER + "/count-by-statuses";
  return axios.get(url);
};

export const personSearchByPage = (searchObject) => {
  let url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const updateStatus = (data) => {
  let config = {
    params: {
      note: data?.note,
      issueDate: data?.issueDate,
    },
  };
  let url = API_PATH_TRANSFER + "/" + data?.id + "/status/" + data?.status;
  return axios.put(url, data?.dataState, config);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/dieu-chuyen-tai-san-co-dinh",
    data: searchObject,
    responseType: "blob",
  });
};

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};

export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const searchReceiverDepartment = (searchObject) => {
  let url = API_PATH_user_department + "/searchByPage";
  return axios.post(url, {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

export const getStatus = () => {
  return axios.get(API_PATH);
};

//get phong ban bàn giao
export const getListManagementDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

//get phòng ban theo id người
export const findDepartmentById = (id) => {
  let config = {
    params: { userId: id, isActive: STATUS_DEPARTMENT.HOAT_DONG.code },
  };
  let url = API_PATH_department + "/main-department";
  return axios.get(url, config);
};

// get người theo id phòng
export const getListUserByDepartmentId = (searchObject) => {
  let config = { params: searchObject };
  return axios.get(API_PATH_USER, config);
};

// get kho theo id phòng
export const getListWarehouseByDepartmentId = (searchObject) => {
  const { pageIndex, pageSize, ...rest } = searchObject;
  let url =
    API_PATH_STORE + "/searchByText/" + pageIndex + "/" + searchObject.pageSize;
  return axios.post(url, { ...rest });
};

export const exportExampleImportExcelAssetTransfer = () => {
  return axios({
    method: "post",
    url: API_PATH_TEMPLATE + "/import-transferred-fixed-asset",
    responseType: "blob",
  });
};

export const importExcelAssetTransferURL = () => {
  return (
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/fixed-assets/transfer-vouchers/import"
  );
};
