import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid, 
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import localStorageService from "app/services/localStorageService";
import {convertNumberPrice, convertNumberPriceRoundUp} from "app/appFunction";


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center"
  },
  title: {
    fontSize: '0.975rem',
    fontWeight: 'bold',
    marginBottom: '0px'
  },
  text: {
    fontSize: '0.8rem',
    fontWeight: 'bold',
    marginTop: '0px'
  },
  textAlignLeft: {
    textAlign: "left",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: 'bold'
  },
  table: {
    width: '100%',
    border: '1px solid',
    borderCollapse: 'collapse'
  },
  w_3: {
    border: '1px solid',
    width: '3%',
    textAlign: 'center'
  },
  w_5: {
    border: '1px solid',
    width: '5%',
    textAlign: 'center'
  },
  w_7: {
    border: '1px solid',
    width: '7%',
    textAlign: 'center'
  },
  w_8: {
    border: '1px solid',
    width: '8%',
    textAlign: 'center'
  },
  w_10: {
    border: '1px solid',
    width: '10%',
  },
  w_25: {
    border: '1px solid',
    width: '25%',
  },
  w_40: {
    border: '1px solid',
    width: '40%',
  },
  border: {
    border: '1px solid',
    padding: "0 5px"
  },
  name: {
    border: '1px solid',
    paddingLeft: "5px",
    textAlign: 'left'
  },
  sum: {
    border: '1px solid',
    paddingLeft: "5px",
    fontWeight: 'bold',
    textAlign: 'left'
  },
  represent: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 12%'
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: "0 12%",
    textTransform: 'uppercase'
  },
}

export default class AssetTransferPrint extends Component {
  state = {
    type: 2,
    rowsPerPage: 1,
    page: 0,
    totalElements: 0,
    departmentId: '',
    asset: {},
    isView: false
  };

  handleFormSubmit = () => {
    
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write(content.innerHTML);
    
    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    let { item } = this.props;
    this.setState({
      ...item
    }, function () {
    }
    );
  }
  componentDidMount() {
  }

  render() {
    let { open, t, item } = this.props;
    let now = new Date(this.state?.issueDate);
    let currentUser = localStorageService.getSessionItem("currentUser");
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth  >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="">{t('Phiếu điều chuyển')}</span>
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{height: '0px', width: '0px', position: 'absolute', print :{ size: 'auto',  margin: '0mm' }}}></iframe>
        
        <ValidatorForm className="validator-form-scroll-dialog" ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id='divcontents'>
          <Grid>
            <div style={{textAlign: 'center'}}>

              <div style={{ display:'flex'}}>
                <div style={{flexGrow: 1}}>
                  <p style={style.title}>{currentUser?.org?.name}</p>
                </div>
                <div style={{flexGrow: 2}}>
                  <p style={style.title} >CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
                  <p style={style.text}>Độc lập - Tự do - Hạnh phúc</p>
                </div>
              </div>
              
              <div>
                <p style={style.title}>BIÊN BẢN GIAO NHẬN THIẾT BỊ</p>
                <p style={style.text}>
                  {this.state?.issueDate
                    ? <p style={style.text}>
                      Hà nội, ngày {now.getDate()} tháng {now.getMonth() + 1} năm {now.getFullYear()}
                    </p>
                    : <p> Hà nội, ngày....tháng....năm......</p>
                  }
                </p>
              </div>
  
              <div style={style.textAlignLeft}>
                <p style={style.forwarder}>BÊN GIAO:</p>
                <ul mr-4>
                  <li><p>Phòng bàn giao: {item?.handoverDepartment?.name}</p> </li>
                  <li><p>Người bàn giao: {item?.handoverPersonName}</p>  </li>
                </ul>
              </div>
  
              <div style={style.textAlignLeft}>
                <p style={style.forwarder}>BÊN NHẬN:</p>
                <ul mr-4>
                  <li> <p>Phòng tiếp nhận: {this.state?.receiverDepartment?.name}</p> </li>
                  <li> <p>Người tiếp nhận: {this.state?.receiverPersonName}</p> </li>
                </ul>
              </div>

              <div>
                {
                  <table style={style.table} className="table-report">
                    <tr>
                      <th style={style.border} rowSpan="2">STT</th>
                      <th style={style.w_25} rowSpan="2">Tên, ký hiệu, quy cách, cấp hạng TSCĐ</th>
                      <th style={style.w_8} rowSpan="2">Số hiệu TSCĐ</th>
                      <th style={style.w_8} rowSpan="2">Nước sản xuất</th>
                      <th style={style.w_8} rowSpan="2">Năm sản xuất</th>
                      <th style={style.w_7} rowSpan="2">Năm đưa vào SD</th>
                      
                      <th style={{ ...style.w_40, ...style.textAlignCenter}} colSpan="5">Tính nguyên giá tài sản cố định</th>
                      <th style={style.border} rowSpan="2">Ghi chú</th>
                    </tr>
                    <tr>
                      <th style={style.w_3}>SL</th>
                      <th style={style.w_5}>Đơn giá (VNĐ)</th>
                      <th style={style.w_7}>Các loại phí, thuế</th>
                      <th style={style.w_10}>Tổng nguyên giá(VNĐ)</th>
                      <th style={style.w_8}>Tài liệu kĩ thuật</th>
                    </tr>

                  
                  {item?.assetVouchers && item?.assetVouchers?.map((row, index) => {
                    return(
                      <tbody>
                      <tr>
                        <td style={style.border}>{index + 1 || '' }</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.asset?.name || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.asset?.code || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.asset?.madeIn || ''}</td>
                        <td style={style.border}>{row?.asset?.yearOfManufacture || ''}</td>
                        <td style={style.border}>{row?.asset?.yearPutIntoUse || ''}</td>                     
                       
                        <td style={style.border}>{item?.asset ? "1" : ''}</td>
                        <td style={style.border}>{convertNumberPriceRoundUp(row?.asset?.originalCost)}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}></td>
                        <td style={style.border}>{convertNumberPriceRoundUp(row?.asset?.originalCost)}</td>
                        <td style={style.border}></td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}></td>                        
                      </tr>
                      </tbody>
                    )
                  })}

                    <tr>
                      <td style={style.border}></td>
                      <td style={style.sum}>Tổng cộng:</td>
                      <td style={style.border}></td>
                      <td style={style.border}></td> 
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>  
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>                   
                    </tr>
                                      
                  </table>
                }
              
              </div>
              <div style={style.signContainer}>
                <div><p>ĐẠI DIỆN BÊN GIAO</p></div>
                <div><p>ĐẠI DIỆN BÊN NHẬN</p></div>
              </div>
              
            </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">              
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>
         
        </ValidatorForm>
        
      </Dialog>
    );
  }
}


// export default AssetTransferPrint;
