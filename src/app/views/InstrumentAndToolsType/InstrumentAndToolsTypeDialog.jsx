import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Paper } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import Draggable from 'react-draggable';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import SelectInstrumentToolsParentPopup from './SelectInstrumentToolsParentPopup';
import { addNewOrUpdateInstrumentTools, checkCode, checkParent } from './InstrumentAndToolsTypeService';
import {appConst, variable} from 'app/appConst';

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
    //etc you get the idea
});

function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

function InstrumentAndToolsTypeDialog(props) {
    const {
        open,
        t,
        item,
        handleClose,
        handleSelect
    } = props

    const [itemInstrumentTools, setItemInstrumentTools] = useState({});
    const [shouldOpenSelectParentPopup, SetShouldOpenSelectParentPopup] = useState(false);

    useEffect(() => {
        item ? setItemInstrumentTools({ ...item, assetClass: appConst.assetClass.CCDC }) : setItemInstrumentTools({ assetClass: appConst.assetClass.CCDC })
    }, [item])


    const openParentPopup = () => {
        SetShouldOpenSelectParentPopup(true)
    }

    const handleFormSubmit = async () => {
        let { id, code } = itemInstrumentTools;
        checkCode(id, code).then(({ data }) => {
            if (appConst.CODE.SUCCESS === data?.code) {
                if (data?.data) {
                    toast.warning(t('InstrumentToolsType.notification.duplicationCode'));
                }
                else {
                    if (id) {
                        checkParent(itemInstrumentTools).then((isCheck) => {
                            if (isCheck.data) {
                                toast.error(t("InstrumentToolsType.notification.updateFailParent"));
                                toast.clearWaitingQueue();
                            } else {
                                addNewOrUpdateInstrumentTools(itemInstrumentTools).then(() => {
                                    toast.success(t("InstrumentToolsType.notification.updateSuccess"));
                                    handleClose();
                                });
                            }
                        });
                    } else {
                        addNewOrUpdateInstrumentTools(itemInstrumentTools).then((data) => {
                            if (data?.data?.code === appConst.CODE.SUCCESS) {
                                handleSelect?.(data?.data?.data);
                                toast.success(t("InstrumentToolsType.notification.addSuccess"));
                            } else {
                                toast.error(data?.data?.message);
                            }
                            handleClose();

                        }).catch(error => {
                            toast.error(t("general.error"));
                        });
                    }
                }
            }
            else {
                toast.error(data?.message);
            }
        });
    }
    const handleChangeCode = (e) => {
        setItemInstrumentTools({
            ...itemInstrumentTools,
            code: e.target.value,
        })
    }
    const handleChangeOrderNumber = (e) => {
        setItemInstrumentTools({
            ...itemInstrumentTools,
            viewIndex: e.target.value
        })
    }
    const handleChangeName = (e) => {
        setItemInstrumentTools({
            ...itemInstrumentTools,
            name: e.target.value
        })
    }
    const handleClosePopup = () => {
        SetShouldOpenSelectParentPopup(false);
    }
    const handleSelectParent = (parent) => {
        setItemInstrumentTools({
            ...itemInstrumentTools,
            parent: parent
        })
        SetShouldOpenSelectParentPopup(false)
    }
    return (
        <Dialog
            open={open}
            PaperComponent={PaperComponent}
            maxWidth="sm"
            fullWidth
        >
            <DialogTitle className="cursor-move" id="draggable-dialog-title">
                {t("InstrumentToolsType.saveUpdate")}
            </DialogTitle>
            <ValidatorForm onSubmit={handleFormSubmit}>
                <DialogContent>
                    <Grid container spacing={1}>
                        <Grid item sm={6} xs={12}>
                            <TextValidator
                                // className="w-80"
                                fullWidth
                                size="small"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label={t("InstrumentToolsType.parent")}
                                value={itemInstrumentTools?.parent ? itemInstrumentTools?.parent?.name : ""}
                            />
                            {shouldOpenSelectParentPopup && (
                                <SelectInstrumentToolsParentPopup
                                    t={t}
                                    open={shouldOpenSelectParentPopup}
                                    handleClose={handleClosePopup}
                                    handleSelect={handleSelectParent}
                                    item={itemInstrumentTools}
                                />
                            )}
                        </Grid>
                        <Grid item sm={2} xs={12}>
                            <Button
                                size="small"
                                className="mt-10"
                                variant="contained"
                                color="primary"
                                onClick={openParentPopup}
                            >
                                {t('general.select')}
                            </Button>
                        </Grid>
                        <Grid item sm={4} xs={12}>
                            <TextValidator
                                className="w-100 "
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("InstrumentToolsType.viewIndex")}</span>
                                    </span>
                                }
                                onChange={handleChangeOrderNumber}
                                type="text"
                                name="code"
                                value={itemInstrumentTools?.viewIndex}
                                validators={["required", "matchRegexp:^[0-9.]{1,20}$", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                                errorMessages={[t("general.required"), t("general.invalidFormat"), t("general.invalidFormat")]}
                            />
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <TextValidator
                                className="w-100 "
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("InstrumentToolsType.code")}</span>
                                    </span>
                                }
                                onChange={handleChangeCode}
                                type="text"
                                name="code"
                                value={itemInstrumentTools?.code}
                                validators={["required", "matchRegexp:^[a-zA-Z0-9_-]*$", "matchRegexp:^.{1,20}$"]}
                                errorMessages={[t("general.required"), "Mã loại CCDC chỉ bao gồm các ký tự A-Z, a-z, 0-9, -, _", "Tối đa 20 ký tự"]}
                            />
                        </Grid>
                        <Grid item sm={6} xs={12}>
                            <TextValidator
                                className="w-100"
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("InstrumentToolsType.name")}</span>
                                    </span>
                                }
                                onChange={handleChangeName}
                                type="text"
                                name="name"
                                value={itemInstrumentTools?.name}
                                validators={["required"]}
                                errorMessages={[t("general.required")]}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-12"
                            onClick={() => handleClose()}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            className="mr-15"
                        >
                            {t("general.save")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
}

export default InstrumentAndToolsTypeDialog