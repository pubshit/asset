import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "../../appConst";
const API_PATH = ConstantList.API_ENPOINT + "/api/assetgroup" + ConstantList.URL_PREFIX;
const assetClass = appConst.assetClass.CCDC

export const getByRoot = (searchObject) => {
      var params = searchObject?.assetClass + "/" + searchObject?.pageIndex + "/" + searchObject?.pageSize;
      var url = API_PATH + "/getbyroot/" + params;
      return axios.get(url);
};
export const deleteCheckItem = (id) => {
      return axios.delete(API_PATH + `/${assetClass}/${id}`);
};
export const addNewOrUpdateInstrumentTools = ToolType => {
      return axios.post(API_PATH, ToolType);
};
export const getItemById = id => {
      return axios.get(API_PATH + "/" + id);
};
export const checkCode = (id, code) => {
      const config = { params: { id: id, code: code, assetClass: appConst.assetClass.CCDC } };
      let url = API_PATH + "/check-code";
      return axios.get(url, config);
};
export const checkParent = (dto) => {
      let url = API_PATH + "/checkParent";
      return axios.post(url, dto);
};
export const searchByPage = (searchObject) => {
      return axios.post(API_PATH + "/searchByPage", searchObject);
};
