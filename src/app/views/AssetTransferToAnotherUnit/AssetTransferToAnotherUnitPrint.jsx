import React, { useContext, useEffect, useState } from "react";
import { Dialog, Button, DialogActions, Grid } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import {
  convertNumberPrice,
  convertNumberPriceRoundUp, getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import Signature from "../FormCustom/Component/Signature";
import DateToText from "../FormCustom/Component/DateToText";
import { PaperComponent } from "../Component/Utilities";
import { getAccessoriesByVoucherId } from "../AssetAllocation/AssetAllocationService";
import { toast } from "react-toastify";
import AppContext from "../../appContext";


const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontWeight: "bold",
    marginBottom: "0px",
  },
  text: {
    fontWeight: "bold",
    marginTop: "0px",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: "bold",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_3: {
    border: "1px solid",
    width: "3%",
    textAlign: "center",
    padding: 5,
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
    padding: 5,
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
    padding: 5,
  },
  w_9: {
    border: "1px solid",
    width: "9%",
    textAlign: "center",
    padding: 5,
  },
  w_11: {
    border: "1px solid",
    width: "11%",
    padding: 5,
  },
  w_20: {
    border: "1px solid",
    width: "20%",
    padding: 5,
  },
  mt_25minus: {
    marginTop: -25,
  },
  border: {
    border: "1px solid",
    padding: 5,
  },
  name: {
    border: "1px solid",
    paddingLeft: "5px",
    textAlign: "left",
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "left",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 12%",
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px"
  },
  textCenter: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    width: "33%",
  },
  pt_40: {
    paddingTop: 40,
  },
  bold: {
    fontWeight: "bold",
  },
  alignCenter: {
    alignItems: "center"
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "14pt",
  },
};

export default function AssetTransferToAnotherUnitPrint(props) {
  let { open, handleClose, t, item } = props;
  let now = new Date(item?.issueDate);
  let arrOriginalCost = [];
  const { organization } = getUserInformation();
  const { setPageLoading } = useContext(AppContext);
  const [accessories, setAccessories] = useState([]);

  useEffect(() => {
    handleGetAccessories();
  }, []);

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };
  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return convertNumberPrice(sum);
  };

  const handleGetAccessories = async () => {
    let { item, t } = props;
    try {
      setPageLoading(true);
      let res = await getAccessoriesByVoucherId(item?.id);
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        setAccessories(data ? [...data] : []);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth="md"
      fullWidth={true}
    >
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("TransferToAnotherUnit.transferSlip")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{ height: "0px", width: "0px", position: "absolute" }}
        title="ifmcontentstoprint"
      ></iframe>
      <ValidatorForm
        onSubmit={handleFormSubmit}
        className="validator-form-scroll-dialog"
      >
        <DialogContent id="divcontents" className="dialog-print">
          <Grid container className="form-print form-print-landscape" style={{ textAlign: "center", ...style.a4Container }}>
            <div>
              <div
                style={{ display: "flex", justifyContent: "space-between", ...style.bold }}
              >
                <div item xs={5}>
                  <div>Đơn vị:&nbsp;{organization?.org?.name || "....................."}</div>
                  <div>Mã QHNS:&nbsp;{organization?.budgetCode || ".........................."}</div>
                </div>

                <div>Mẫu số: C50-HD</div>
              </div>

              <div style={{ margin: "30px 0" }}>
                <div style={style.title}>BIÊN BẢN GIAO NHẬN THIẾT BỊ</div>
                <DateToText isLocation noUppercase date={item?.issueDate} />
              </div>

              {
                <div>
                  <p style={style.textAlignLeft}>
                    Căn cứ Quyết định số: ....... ngày ...... tháng ...... năm
                    ...... của .................. về việc bàn giao TSCĐ
                  </p>
                  <div style={style.textAlignLeft}>
                    <p>Bên giao nhận TSCĐ gồm:</p>
                    <ul>
                      <li>
                        <div style={{
                          display: "grid",
                          gridTemplateColumns: "5fr 3fr"
                        }}>
                          <p>
                            Đại diện bên giao: Ông/Bà:&nbsp;{item?.handoverPersonName}
                          </p>
                          <p>
                            Chức vụ:
                            .................................{" "}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div style={{
                          display: "grid",
                          gridTemplateColumns: "5fr 3fr"
                        }}>
                          <p>
                            Đại diện bên nhận: Ông/Bà: {("..").repeat(20)}
                          </p>
                          <p>
                            Chức vụ: .................................{" "}
                          </p>
                        </div>
                        <p>
                        </p>
                      </li>
                    </ul>
                  </div>
                  <p style={style.textAlignLeft}>
                    Địa điểm giao nhận TSCĐ: {item?.transferAddress}
                  </p>
                  <p style={style.textAlignLeft}>
                    Xác nhận việc giao nhận TSCĐ như sau:
                  </p>
                </div>
              }
              <div>
                {
                  <table style={style.table} className="table-report">
                    <thead>
                      <tr>
                        <th style={style.w_3}>STT</th>
                        <th style={style.w_11}>Mã tài sản</th>
                        <th style={style.w_20}>Tên tài sản</th>
                        <th style={style.w_5}>Nước SX</th>
                        <th style={style.w_5}>Năm SX</th>
                        <th style={style.w_5}>Năm SD</th>
                        <th style={style.w_5}>SL</th>
                        <th style={style.w_20}>Người tiếp nhận</th>
                        <th style={style.w_9}>Nguyên giá</th>
                      </tr>
                    </thead>

                    <tbody>
                      {item.assetVouchers?.length > 0
                        ? item.assetVouchers.map((row, index) => {
                          arrOriginalCost.push(row?.asset?.originalCost);
                          return (
                            <tr>
                              <td
                                style={{ ...style.border, ...style.textAlignCenter, }}
                              >
                                {index + 1 || ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignLeft, }}
                              >
                                {row?.asset?.code || ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignLeft, }}
                              >
                                {row?.asset?.name || ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignLeft, }}
                              >
                                {row?.asset?.madeIn || ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignCenter, }}
                              >
                                {row?.asset?.yearOfManufacture || ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignCenter, }}
                              >
                                {row?.asset?.yearPutIntoUse ||
                                  row?.assetYearPutIntoUser ||
                                  ""}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignCenter, }}
                              >
                                {row?.quantity ? row?.quantity : 1}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignLeft, }}
                              >
                                {/* {item?.handoverPersonName} */}
                              </td>
                              <td
                                style={{ ...style.border, ...style.textAlignRight, }}
                              >
                                {convertNumberPriceRoundUp(
                                  row?.asset?.originalCost || 0
                                )}
                              </td>
                            </tr>
                          );
                        })
                        : ""}
                      <tr>
                        <td
                          colSpan={2}
                          style={{
                            ...style.border,
                            ...style.textAlignLeft,
                            ...style.forwarder,
                          }}
                        >
                          Tổng cộng:
                        </td>
                        <td
                          colSpan={7}
                          style={{ ...style.border, ...style.textAlignRight }}
                        >
                          {sumArray(arrOriginalCost)}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                }
              </div>
              {
                <div>
                  <div style={{ ...style.title, marginTop: "20px" }}>DỤNG CỤ, PHỤ TÙNG KÈM THEO</div>
                  <table style={style.table} className="table-report">
                    <thead>
                      <tr>
                        <th style={style.w_3}>STT</th>
                        <th style={style.w_20}>Tên, quy cách dụng cụ, phụ tùng</th>
                        <th style={style.w_9}>Đơn vị tính</th>
                        <th style={style.w_3}>Số lượng</th>
                        <th style={style.w_9}>Giá trị</th>
                      </tr>
                    </thead>
                    <tbody>
                      {accessories.length
                        ? accessories?.map((item, index) => (
                          <tr>
                            <td style={{ ...style.border, ...style.textAlignCenter }}>
                              {index + 1}
                            </td>
                            <td style={{ ...style.border, ...style.textAlignLeft }}>
                              {item.productName || ""}
                            </td>
                            <td style={{ ...style.border, ...style.textAlignCenter }}>
                              {item.unitName || ""}
                            </td>
                            <td style={{ ...style.border, ...style.textAlignCenter }}>
                              {item.quantity || ""}
                            </td>
                            <td style={{ ...style.border, ...style.textAlignRight }}></td>
                          </tr>
                        )) : (
                          <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        )}
                    </tbody>
                  </table>
                </div>
              }
              <div>
                <div style={style.signContainer}>
                  <Signature
                    title="Thủ trưởng đơn vị"
                    note={"Bên nhận"}
                    sign="Ký, họ tên, đóng dấu"
                    titleProps={{ fontSize: "18px" }}
                    signProps={{ fontSize: "18px" }}
                  />
                  <Signature
                    title="Kế toán trưởng"
                    note={"Bên nhận"}
                    sign="Ký, họ tên"
                    titleProps={{ fontSize: "18px" }}
                    signProps={{ fontSize: "18px" }}
                  />
                  <Signature
                    title="Người nhận"
                    sign="Ký, họ tên"
                    titleProps={{ fontSize: "18px" }}
                    signProps={{ fontSize: "18px" }}
                  />
                  <Signature
                    title="Người giao"
                    sign="Ký, họ tên"
                    titleProps={{ fontSize: "18px" }}
                    signProps={{ fontSize: "18px" }}
                  />
                </div>
              </div>
            </div>
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-16"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("In")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
