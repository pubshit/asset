import {
    Button,
    Card,
    Collapse,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination,
    TextField
} from "@material-ui/core";
import React, { useContext, useState } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import AssetTransferToAnotherUnitDialog from "./AssetTransferToAnotherUnitDialog";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst } from "app/appConst";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import { Autocomplete } from "@material-ui/lab";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { useEffect } from "react";
import AssetsQRPrintNew from "../Asset/ComponentPopups/AssetsQRPrintNew";
import { convertNumberPriceRoundUp, filterOptions, getUserInformation, isSuccessfulResponse } from "../../appFunction";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { getAccessoriesByVoucherId } from "../AssetAllocation/AssetAllocationService";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";


function ComponentTransferToAnotherUnitTable(props) {
    const { currentOrg } = useContext(AppContext);
    const { TRANSFER_TO_ANOTHER_UNIT } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    let isButtonAdd = props?.item?.tabValue === appConst.tabTransferToAnother.tabAll;
    let isButtonDelete = props?.item?.tabValue === appConst.tabTransferToAnother.tabPlan || props?.item?.tabValue === appConst.tabTransferToAnother.tabMoving
    let isSearchCreateDate = props?.item?.tabValue === appConst.tabTransferToAnother.tabPlan;
    const [accessories, setAccessories] = useState([]);
    let { t, i18n, setItemState } = props;
    let {
        page,
        itemList,
        isSearch,
        rowsPerPage,
        totalElements,
        shouldOpenEditorDialog,
        shouldOpenConfirmationDialog,
        shouldOpenConfirmationDeleteAllDialog,
        isPrint,
        statusTransfer,
        openPrintQR,
        products,
        item,
        itemAsset
    } = props?.item

    useEffect(() => {
        setItemState({})
        //eslint-disable-next-line
    }, [])
    useEffect(() => {
        if (props?.item?.item?.id) {
            handleGetAccessories();
        }
    }, [props?.item?.item?.id])

    const handleGetAccessories = async () => {
        try {
            let res = await getAccessoriesByVoucherId(item?.id);
            const { code, data } = res?.data;

            if (isSuccessfulResponse(code)) {
                setAccessories(data ? [...data] : []);
            }
        } catch (e) {
        } finally {
        }
    }
    const { organization } = getUserInformation();



    const newDate = new Date(item?.issueDate)
    const day = String(newDate?.getDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());

    let dataView = {
        ...item,
        organization,
        day,
        month,
        year,
        assetName: "TSCĐ",
        nameType: "tài sản",
        isEmtyAsset: accessories?.length <= 0,
        isContainsAsset: accessories?.length > 0,
        accessories: accessories?.map((i, x) => {
            return {
                ...i,
                index: x + 1
            }
        }),
        assetVouchers: item?.assetVouchers?.map((i, x) => {
            return {
                ...i,
                index: x + 1,
                assetYearPutIntoUser: i?.asset?.yearPutIntoUse ||
                    i?.assetYearPutIntoUser ||
                    "",
                quantity: i?.quantity || 1,
                asset: {
                    ...i.asset,
                    originalCost: convertNumberPriceRoundUp(i?.asset?.originalCost || 0),
                },
            }
        }),
        totalPrice: convertNumberPriceRoundUp(item?.assetVouchers?.reduce((total, item) => total + (item?.asset?.originalCost || 0), 0)) || 0
    }

    return (
        <>
            <Grid container spacing={3} className="alignItemsFlexEnd spaceBetween">
                <Grid container spacing={3} item md={12} sm={12} xs={12} className="flex spaceBetween">
                    <Grid item md={6} xs={12}>
                        {isButtonAdd && (
                            <Button
                                className="align-bottom  mr-16"
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    props.handleEditItem({
                                        startDate: new Date(),
                                        endDate: new Date(),
                                    });
                                }}
                            >
                                {t("TransferToAnotherUnit.buttonAdd")}
                            </Button>
                        )}

                        {isButtonDelete && (
                            <Button
                                className="align-bottom mr-16"
                                variant="contained"
                                color="primary"
                                onClick={props.handleDeleteAllAssetTransfer}
                            >
                                {t("TransferToAnotherUnit.delete")}
                            </Button>
                        )}
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.handleExportToExcel}
                        >
                            {t("general.exportToExcel")}
                        </Button>
                        <Button
                            className="align-bottom mr-16 mt-16"
                            variant="contained"
                            color="primary"
                            style={{ paddingRight: "3px" }}
                            onClick={() => props.setState({ isSearch: !props?.item?.isSearch })}
                        >
                            {t("Tìm kiếm nâng cao")}
                            <ArrowDropDownIcon />
                        </Button>
                    </Grid>
                    <Grid item md={6} sm={12} xs={12} className="flex alignItemsFlexEnd justifyContentEnd">
                        <FormControl fullWidth>
                            <Input
                                className="search_box w-100"
                                onChange={props.handleTextChange}
                                onKeyDown={props.handleKeyDownEnterSearch}
                                onKeyUp={props.handleKeyUp}
                                placeholder={t("TransferToAnotherUnit.filter")}
                                id="search_box"
                                startAdornment={
                                    <InputAdornment position="end">
                                        <SearchIcon
                                            onClick={() => props.search()}
                                            className="searchTable"
                                        />
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid item xs={12} className="pt-0 pb-0">
                    <Collapse in={isSearch}>
                        <Card elevation={1} className="pt-8 pb-12 pl-16 pr-0">
                            <Grid container spacing={2}>
                                {(isSearchCreateDate || isButtonAdd) &&
                                    <>
                                        <Grid item md={3} sm={4} xs={12}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("TransferToAnotherUnit.createDateStart")}
                                                    type="text"
                                                    autoOk={true}
                                                    format="dd/MM/yyyy"
                                                    name={"createDateBottom"}
                                                    value={props.item?.createDateBottom}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    maxDate={props.item?.createDateTop ? (new Date(props.item?.createDateTop)) : undefined}
                                                    minDateMessage={t("general.minYearDefault")}
                                                    maxDateMessage={props.item?.createDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "createDateBottom")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item md={3} sm={4} xs={12}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("TransferToAnotherUnit.createDateEnd")}
                                                    type="text"
                                                    autoOk={true}
                                                    format="dd/MM/yyyy"
                                                    name={"createDateTop"}
                                                    value={props.item?.createDateTop}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    minDate={props.item?.createDateBottom ? (new Date(props.item?.createDateBottom)) : undefined}
                                                    minDateMessage={props.item?.createDateBottom ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                    maxDateMessage={t("general.maxDateDefault")}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "createDateTop")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                    </>
                                }

                                {(!isSearchCreateDate || isButtonAdd) &&
                                    <>
                                        <Grid item md={3} sm={4} xs={12} >
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("TransferToAnotherUnit.issueDateBottom")}
                                                    type="text"
                                                    autoOk={true}
                                                    format="dd/MM/yyyy"
                                                    name={"issueDateBottom"}
                                                    value={props.item?.issueDateBottom}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    maxDate={props.item?.issueDateTop ? (new Date(props.item?.issueDateTop)) : undefined}
                                                    minDateMessage={t("general.minYearDefault")}
                                                    maxDateMessage={props.item?.issueDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "issueDateBottom")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item md={3} sm={4} xs={12} >
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("TransferToAnotherUnit.issueDateTop")}
                                                    type="text"
                                                    autoOk={true}
                                                    format="dd/MM/yyyy"
                                                    name={"issueDateTop"}
                                                    value={props.item?.issueDateTop}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    minDate={props.item?.issueDateBottom ? (new Date(props.item?.issueDateBottom)) : undefined}
                                                    minDateMessage={props.item?.issueDateBottom ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                    maxDateMessage={t("general.maxDateDefault")}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "issueDateTop")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                    </>
                                }

                                {isButtonAdd &&
                                    <Grid item md={3} sm={4} xs={12} >
                                        <Autocomplete
                                            id="combo-box"
                                            className="mt-3"
                                            fullWidth
                                            size="small"
                                            options={appConst.listStatusTransferToAnother}
                                            onChange={(e, value) => props.handleChangeSelect(value)}
                                            getOptionLabel={(option) => option.name}
                                            defaultValue={statusTransfer ? statusTransfer : null}
                                            filterOptions={filterOptions}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    label={t("TransferToAnotherUnit.status")}
                                                    variant="standard"
                                                    value={statusTransfer ? statusTransfer : null}
                                                />
                                            }
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                }
                            </Grid>
                        </Card>
                    </Collapse>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {shouldOpenEditorDialog && (
                            <AssetTransferToAnotherUnitDialog
                                t={t}
                                i18n={props.item?.i18n}
                                handleClose={props.handleDialogClose}
                                open={props.item?.shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={props.item?.item}
                            />
                        )}

                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.cancel_asset_allocation")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {shouldOpenConfirmationDeleteAllDialog && (
                            <ConfirmationDialog
                                open={shouldOpenConfirmationDeleteAllDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleDeleteAll}
                                text={t("general.deleteAllConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {isPrint && (
                            <PrintMultipleFormDialog
                                t={t}
                                i18n={i18n}
                                maxWidth={"lg"}
                                handleClose={props.handleDialogClose}
                                open={isPrint}
                                item={dataView}
                                title={t("TransferToAnotherUnit.transferSlip")}
                                urls={[
                                    ...TRANSFER_TO_ANOTHER_UNIT.GENERAL,
                                    ...(TRANSFER_TO_ANOTHER_UNIT[currentOrg?.printCode] || []),
                                ]}
                            />
                        )}

                        {openPrintQR && (
                            <AssetsQRPrintNew
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                    </div>
                    <MaterialTable
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            draggable: false,
                            // selection: true,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: itemAsset?.id === rowData?.id
                                    ? "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "450px",
                            minBodyHeight: "250px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                textAlign: "center",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            props.handleAddIds(rows)
                        }}
                        onRowClick={(e, rowData) => { return props?.setItemState(rowData) }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        component="div"
                        labelRowsPerPage={t("general.rows_per_page")}
                        labelDisplayedRows={({ from, to, count }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                            }`
                        }
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{ "aria-label": "Previous Page", }}
                        nextIconButtonProps={{ "aria-label": "Next Page", }}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default ComponentTransferToAnotherUnitTable;
