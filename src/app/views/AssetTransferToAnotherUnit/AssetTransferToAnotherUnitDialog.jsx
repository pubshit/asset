import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import {
  personSearchByPage,
  addAsset,
  updateAsset,
} from "./AssetTransferToAnotherUnitService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TransferToAnotherUnitScrollableTabs from "./TransferToAnotherUnitScrollableTabs"
import {
  deleteAssetDocumentByIdV1,
  getAssetDocumentById,
  getListOrgManagementDepartment,
  getNewCodeDocument,
  searchByPageAssetDocument
} from "../Asset/AssetService";
import {checkInvalidDate, checkMinDate, checkObject, formatDateDto} from "app/appFunction";
import AppContext from "app/appContext";
import { STATUS_CHUYEN_DI_TSCD, appConst } from "app/appConst";
import localStorageService from "app/services/localStorageService";
import { PaperComponent } from "../Component/Utilities";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});


class AssetTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.TransferToAnotherUnit,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.TransferToAnotherUnit,
    handoverPersonName: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenSelectPersonPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_CHUYEN_DI_TSCD, //đang sử dụng và bảo dưỡng, không rõ
    handoverPersonClone: false,
    managementDepartment: null,
    personIds: [],
    persons: [],
    transferAddress: "",
    code: null,
    conclude: null,
    transferToAnotherUnitStatus: null,
    shouldOpenPopupAssetFile: false,
    isRoleManager: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT,
    documentTypeNumber: appConst.documentTypeNumbers.ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT,
    voucherId: null,
    documents: [],
    receiverOrg: null,
    listOrganization: [],
  };

  voucherType = ConstantList.VOUCHER_TYPE.TransferToAnotherUnit; //Điều chuyển

  convertDto = (state) => {
    let data = {
      assetVouchers: state?.assetVouchers,
      code: state?.code,
      conclude: state?.conclude,
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      handoverPersonId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      name: state?.name,
      transferAddress: state?.transferAddress,
      transferToAnotherUnitStatus: state?.transferToAnotherUnitStatus,
      hoiDongId: this.state?.hoiDongId,
      receiverOrgId: state?.receiverOrg?.id,
      receiverOrgName: state?.receiverOrg?.name,
      persons: state?.persons ? state?.persons?.map(item => {
        return {
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }
      }) : [],
    }
    return data;
  }

  handleSetTransferToAnotherUnitUserList = (item) => {
    this.setState({ persons: item || [] })
  }

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id })
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        assetVoucher.note = valueText.target.value;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true }
    );
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  validateForm = () => {
    let {
      assetVouchers,
      issueDate,
      transferToAnotherUnitStatus,
      transferAddress,
      handoverDepartment,
      handoverPerson,
      persons
    } = this.state;

    if (checkInvalidDate(issueDate)) {
      toast.warning('Ngày chuyển đi không tồn tại.');
      return false;
    }

    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(`Ngày chuyển đi không được nhỏ hơn ngày "01/01/1900".`);
      return false;
    }

    if (!handoverDepartment?.id) {
      toast.warning("Chưa chọn phòng ban bàn giao.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!handoverPerson?.personId) {
      toast.warning("Chưa chọn người bàn giao.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (!transferToAnotherUnitStatus) {
      toast.warning("Chưa chọn trạng thái phiếu.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (!transferAddress) {
      toast.warning("Chưa nhập địa chỉ chuyển đi.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn tài sản chuyển đi");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (this.state?.hoiDongId && persons?.length === 0) {
      toast.warning("Hội đồng không có thành viên")
      toast.clearWaitingQueue();
      return false;
    }

    if (!this.state?.persons || persons?.length === 0) {
      toast.warning("Chưa chọn người chuyển đi")
      toast.clearWaitingQueue();
      return false;
    }
    if (assetVouchers.length > 0 && issueDate) {
      const invalidAsset = assetVouchers.some(asset => new Date(asset?.asset?.dateOfReception) > new Date(issueDate));
      if (invalidAsset) {
        toast.warning("Ngày điều chuyển phải lớn hơn ngày tiếp nhận");
        toast.clearWaitingQueue();
        return false;
      }
    }
    return true;
  }

  handleFormSubmit = (e) => {
    if (e.target.id !== "formTransferToAnotherUnitDialog") return;
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { id } = this.state;
    if (!this.validateForm()) return;
    setPageLoading(true);
    let dataState = this.convertDto(this.state);
    if (id) {
      updateAsset(id, dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            this.props.handleOKEditClose();
          } else {
            toast.warning(data.message)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
          setPageLoading(false)
        })
    } else {
      addAsset(dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.info(data.message);
            this.props.handleOKEditClose();
          } else {
            toast.warning(data.message)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
          setPageLoading(false)
        })
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    }, () => {
      if(name === "issueDate") {
        this.setState({ assetVouchers: [] });
      }
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleSelectAsset = (items) => {
    // eslint-disable-next-line no-unused-expressions
    items?.forEach((element) => {
      // element.asset.useDepartment = this.state?.receiverDepartment;
      element.assetId = element.asset?.id;
    });
    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning('Chưa có tài sản nào được chọn');
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState(
      {
        assetVouchers,
      },
      () => { toast.info("Xóa tài sản trong danh sách tài sản thành công") }
    );
  };
  
  componentDidMount() {
    let { item } = this.props;
    if (item?.id) {
      this.setState({
        ...this.props.item,
        handoverPerson: item?.handoverPersonId ? {
          personId: item?.handoverPersonId,
          personDisplayName: item?.handoverPersonName
        } : null,
        receiverOrg: item?.receiverOrgId ? {
          id: item?.receiverOrgId,
          name: item?.receiverOrgName
        } : null,
        handoverPersonClone: true,
      });
    }

    this.setState({ ...this.props.item });
    this.checkRole();
  }

  checkRole = async () => {
    let { isRoleManager } = this.state
    let currentUser = localStorageService.getSessionItem("currentUser");
    let isRoleAdmin = currentUser?.roles?.some((role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN || role.name === ConstantList.ROLES.ROLE_ADMIN)

    if (isRoleAdmin) return;

    isRoleManager = currentUser?.roles?.some((role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER)

    if (isRoleManager) {
      await this.getManagementDepartment()
    }
  }

  getManagementDepartment = () => {
    getListOrgManagementDepartment({}).then(({ data }) => {
      let handoverDepartment = data ? data[0] : null
      this.setState({
        handoverDepartment: handoverDepartment,
        handoverDepartmentId: handoverDepartment?.id,
        managementDepartmentId: handoverDepartment?.id,
        isRoleManager: true
      });
    })
  }
  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      handoverPersonId: null,
      listHandoverPerson: [],
      assetVouchers: [],
      managementDepartmentId: item?.id
    });
  };

  handleChangeSelect = (item) => {
    this.setState({
      transferToAnotherUnitStatus: item?.code
    })
  }

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true
    })
  }

  handleSelectPerson = (listPerson) => {
    let personIds = [];

    if (listPerson?.length > 0) {
      listPerson.forEach((item) => {
        personIds.push(item?.personId)
      })
      this.setState({
        persons: listPerson,
        personIds: personIds
      })
    } else {
      this.setState({
        persons: [],
        personIds: []
      })
    }
    this.handleSelectPersonPopupClose();
  }

  removePersonInlist = (personId) => {
    let { persons, personIds } = this.state;
    let index = persons.findIndex(x => x.person?.id === personId);
    let indexId = personIds.findIndex(personIds => personIds === personId);
    persons.splice(index, 1);
    personIds.splice(indexId, 1);

    this.setState({ persons, personIds },
      () => { toast.info("Xóa thành công") }
    );
  }

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false
    })
  }

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
        },
        () => {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    this.setState(
      {
        handoverDepartment: {
          id: item?.id,
          name: item?.text,
          text: item?.text,
        }
      },
    );
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: item?.id,
    };
    let result = false;
    let { handoverPerson } = this.state;
    personSearchByPage(handoverPersonSearchObject).then(({ data }) => {
      result = data?.content?.some((person) => handoverPerson?.id === person?.id);
      if (result) {
        this.setState({ assetVouchers: [] });
      } else {
        this.setState({ handoverPerson: null });
      }
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null, handoverPerson: null });
    }
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = { id: item.id, name: item.name };
    this.setState({ handoverDepartment: department }, function () {
      this.handleHandoverDepartmentPopupClose();
    });
    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  handleAddAssetDocumentItem = async () => {
    try {
      const result = await getNewCodeDocument(this.state?.documentType);
      if (result != null && result.data && result.data.code) {
        const item = result.data.data;
        this.setState({
          item: {
            code: item,
          },
          shouldOpenPopupAssetFile: true,
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  // handleAssetFilePopupClose = () => {
  //   this.setState({
  //     shouldOpenPopupAssetFile: false,
  //   });
  // };
  handleAssetFilePopupClose = () => {
    this.setState({
      idHoSoDK: null,
      shouldOpenPopupAssetFile: false,
    });
  };

  // getAssetDocumentId = (documentId) => {
  //   let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
  //   documentId && listAssetDocumentId.push(documentId);
  //   this.setState({ listAssetDocumentId: listAssetDocumentId });
  // };
  getAssetDocumentId = (documentId) => {
    let { listAssetDocumentId } = this.state;
    let newArray = listAssetDocumentId ? listAssetDocumentId : []
    let existItem = newArray?.find(id => id === documentId);
    if (!existItem) {
      newArray.push(documentId);
    }
    this.setState({
      listAssetDocumentId: newArray,
    });
  };

  getAssetDocument = (document) => {
    console.log(document)
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleRowDataCellEditAssetFile = async (rowData) => {
    try {
      const { data } = await getAssetDocumentById(rowData.id);
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({
          item: data?.data,
          idHoSoDK: rowData.id,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        })
      } else {
        toast.warn(data?.message)
      }
    } catch (error) {
      // Xử lý lỗi nếu có
      console.error(error);
    }
  };



  handleRowDataCellDeleteAssetFile = (rowData) => {
    let { listAssetDocumentId } = this.state;
    let { t } = this.props;
    listAssetDocumentId.splice(
      listAssetDocumentId?.indexOf(rowData?.id),
      1
    )
    this.setState({ listAssetDocumentId });
    deleteAssetDocumentByIdV1(rowData.id).then((data) => {
      if (data) {
        toast.success(t("general.deleteSuccess"));
        if (this.state.id) {
          this.updatePageAssetDocument();
        }
        else {
          let documents = this.state.documents?.filter(item => item?.id !== rowData?.id)
          this.setState({ documents })
        }
      }
      else {
        toast.error(t("general.error"));
      }
    }).catch((error) => {
      toast.error(t("general.error"));
    });
  };

  handleSetDataHandoverDepartment = (data) => {
    this.setState({
      listHandoverDepartment: data
    })
  }

  handleSetDataHandoverPerson = (data) => {
    this.setState({
      listHandoverPerson: data
    })
  }

  handleAddFileLocal = (listFile) => {
    let newList = []
    newList.push(listFile)
    this.setState({
      documents: [...this.state.documents, ...newList],
    })
    this.handleAssetFilePopupClose()
  }

  updatePageAssetDocument = () => {
    let searchObject = {};
    searchObject.voucherId = this.props.item.id;
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = 100000;
    searchObject.documentType = this.state.documentTypeNumber;
    searchByPageAssetDocument(searchObject).then((response) => {
      const { data } = response;
      if (data) {
        this.setState({
          documents: data.content,
          totalElements: data.totalElements,
        });
      }
    });
  };

  render() {
    let { open, t, item, i18n } = this.props;
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let { shouldOpenNotificationPopup } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <CustomValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          id='formTransferToAnotherUnitDialog'
          className="validator-form-scroll-dialog"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            {item?.id ? t('TransferToAnotherUnit.updateDialog') : t('TransferToAnotherUnit.addDialog')}
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <TransferToAnotherUnitScrollableTabs
              t={t}
              i18n={i18n}
              item={this.state}
              handleSelectHandoverDepartment={this.handleSelectHandoverDepartment}
              handleHandoverDepartmentPopupClose={this.handleHandoverDepartmentPopupClose}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupClose={this.handleReceiverDepartmentPopupClose}
              handleReceiverPersonPopupClose={this.handleReceiverPersonPopupClose}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              handoverPersonSearchObject={handoverPersonSearchObject}
              selectHandoverDepartment={this.selectHandoverDepartment}
              handleChangeSelect={this.handleChangeSelect}
              handleChange={this.handleChange}
              openSelectPersonPopup={this.openSelectPersonPopup}
              handleSelectPerson={this.handleSelectPerson}
              handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
              removePersonInlist={this.removePersonInlist}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleSetDataHandoverDepartment={this.handleSetDataHandoverDepartment}
              handleSetDataHandoverPerson={this.handleSetDataHandoverPerson}
              getAssetDocumentId={this.getAssetDocumentId}
              getAssetDocument={this.getAssetDocument}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              itemAssetDocument={this.state?.item}
              handleSetTransferToAnotherUnitUserList={this.handleSetTransferToAnotherUnitUserList}
              handleSetHoiDongId={this.handleSetHoiDongId}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              updatePageAssetDocument={this.updatePageAssetDocument}
              handleAddFileLocal={this.handleAddFileLocal}
              voucherId={this.state?.id}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!this.props?.item?.isView &&
                <Button
                  variant="contained"
                  color="primary"
                  className="mr-16"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
            </div>
          </DialogActions>
        </CustomValidatorForm>
      </Dialog>
    );
  }
}
AssetTransferDialog.contextType = AppContext;
export default AssetTransferDialog;
