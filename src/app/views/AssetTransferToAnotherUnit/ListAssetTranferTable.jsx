import { Icon, IconButton } from "@material-ui/core";
import { appConst } from "app/appConst";
import { convertNumberPrice } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";


function ListAssetTranferTable(props) {
  const { t } = props;
  let columns = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "center",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("Asset.code"),
      field: "assetCode",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "assetManagementCode",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "assetName",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Asset.serialNumber"),
      field: "assetSerialNumber",
      align: "center",
      minWidth: 100,
    },
    {
      title: t("Asset.model"),
      field: "assetModel",
      align: "center",
      minWidth: 100,
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "assetYearPutIntoUse",
      align: "center",
      minWidth: 105,
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "assetYearOfManufacture",
      align: "left",
      minWidth: 105,
    },
    {
      title: t("Asset.manufacturer"),
      field: "manufacturerName",
      align: "left",
      minWidth: 110,
    },
    {
      title: t("Asset.originalCost"),
      field: "assetOriginalCost",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right"
      },
      render: (rowData) => convertNumberPrice(rowData?.assetOriginalCost),
    },
    {
      title: t("Asset.carryingAmount"),
      field: "assetCarryingAmount",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right"
      },
      render: (rowData) => convertNumberPrice(rowData?.assetCarryingAmount),
    },
    {
      title: t("Asset.accumulatedDepreciationAmount"),
      field: "",
      align: "left",
      minWidth: 150,
    },
  ];
  return (
    <div>
      <MaterialTable
        data={props?.itemAsset.assetVouchers ? props?.itemAsset?.assetVouchers : []}
        columns={columns}
        options={{
            draggable: false,
            toolbar: false,
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            sorting: false,
            padding: "dense",
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
              textAlign: "center",
            },
            maxBodyHeight: "225px",
            minBodyHeight: "225px",
          }}
        localization={{
          body: {
            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
          },
        }}
        components={{
          Toolbar: (props) => (
            <div style={{ witdth: "100%" }}>
              <MTableToolbar {...props} />
            </div>
          ),
        }}
        onSelectionChange={(rows) => {
          this.data = rows;
        }}
      />
    </div>
  );
}

export default ListAssetTranferTable;
