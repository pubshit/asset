import ConstantList from "../../appConfig";
import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tab,
  Tabs,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountStatus,
  deleteAllItem,
  getListQRCode,
} from "./AssetTransferToAnotherUnitService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import FileSaver from "file-saver";
import { Helmet } from "react-helmet";
import moment from "moment";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import ComponentTransferToAnotherUnitTable from "./ComponentTransferToAnotherUnitTable";
import {
  TabPanel,
  convertFromToDate,
  formatDateTimeDto,
  formatDateTypeArray,
  getTheHighestRole,
  isValidDate, isSuccessfulResponse, handleThrowResponseMessage,
} from "app/appFunction";
import { formatDateDto } from "../../appFunction";
import ListAssetTranferTable from "./ListAssetTranferTable";
import { LightTooltip } from "../Component/Utilities";

toast.configure({
  autoClose: 4000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;

  return (
    <div className="none_wrap">
      {appConst.listStatusTransferToAnother[2].code !==
        item?.transferToAnotherUnitStatus && (
          <>
            <LightTooltip
              className="p-5"
              title={t("general.editIcon")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.onSelect(item, appConst.active.edit)}
              >
                <Icon fontSize="small" color="primary">
                  edit
                </Icon>
              </IconButton>
            </LightTooltip>

            <LightTooltip
              className="p-5"
              title={t("general.deleteIcon")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.onSelect(item, appConst.active.delete)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </>
        )}

      <LightTooltip
        className="p-5"
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>

      {appConst.listStatusTransferToAnotherObject.DA_CHUYEN_DI.code
        === item.transferToAnotherUnitStatus && (
          <LightTooltip
            className="p-5"
            title={t("general.viewIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.view)}
            >
              <Icon fontSize="small" color="primary">
                visibility
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class AssetTransferToAnotherUnitTable extends React.Component {
  constructor(props) {
    super(props)
    this.anchorRef = React.createRef();
  }
  state = {
    keyword: null,
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetTransferToAnotherUnitTable: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    tabValue: 0,
    isRoleManager: false,
    createDateTop: null,
    createDateBottom: null,
    issueDateTop: null,
    issueDateBottom: null,
    statuses: null,
    ids: [],
    isSearch: false,
    itemAsset: {},
    checkAll: false,
    openPrintQR: false,
    products: []
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.TransferToAnotherUnit; //Điều chuyển

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
      createDateTop: null,
      createDateBottom: null,
      issueDateTop: null,
      issueDateBottom: null,
      statusTransfer: null,
      isSearch: false,
    });
    if (appConst?.tabTransferToAnother?.tabAll === newValue) {
      this.setState(
        {
          statuses: null,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransferToAnother?.tabPlan === newValue) {
      this.setState(
        {
          statuses: [appConst.STATUS_CHUYEN_DI.KE_HOACH.code],
          statusTransfer: appConst.STATUS_CHUYEN_DI.KE_HOACH
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransferToAnother?.tabMoving === newValue) {
      this.setState(
        {
          statuses: [appConst.STATUS_CHUYEN_DI.DANG_CHUYEN_DI.code],
          statusTransfer: appConst.STATUS_CHUYEN_DI.DANG_CHUYEN_DI
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransferToAnother?.tabMoved === newValue) {
      this.setState(
        {
          statuses: [appConst.STATUS_CHUYEN_DI.DA_CHUYEN_DI.code],
          statusTransfer: appConst.STATUS_CHUYEN_DI.DA_CHUYEN_DI
        },
        () => this.updatePageData()
      );
    }
  };

  handleToggleOpenExcel = (e) => {
    const { openExcel } = this.state;

    this.setState((prevState) => ({
      openExcel: !openExcel,
    }));
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page, itemAsset: {} }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  handleDateChange = (date, name) => {
    this.setState({ [name]: date }, () => { (isValidDate(date) || date === null) && this.search() });
  };

  updatePageData = () => {
    let { t } = this.props;
    let { selectedList } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword?.trim() || null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.statuses = this.state?.statuses?.toString();
    searchObject.createDateTop = convertFromToDate(this.state?.createDateTop)?.toDate;
    searchObject.createDateBottom = convertFromToDate(this.state?.createDateBottom)?.fromDate;
    searchObject.issueDateTop = convertFromToDate(this.state?.issueDateTop)?.toDate;
    searchObject.issueDateBottom = convertFromToDate(this.state?.issueDateBottom)?.fromDate;
    searchByPage(searchObject)
      .then((res) => {
        let {code, data} = res?.data;
        if (isSuccessfulResponse(code)) {
          if (data?.number && data?.content?.length <= 0) {
            return this.setPage(0);
          }
          let countItemChecked = 0
          const itemList = data?.content?.map(item => {
            item.createDate = formatDateTypeArray(item?.createDate)

            if (selectedList?.find(checkedItem => checkedItem === item.id)) {
              countItemChecked++
            }
            return {
              ...item,
              checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
            }
          })

          this.setState({
            itemList,
            totalElements: data?.totalElements,
            ids: [],
            checkAll: itemList?.length === countItemChecked && itemList?.length > 0
          })
        } else {
          handleThrowResponseMessage(res);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleChangeSelect = (item) => {
    this.setState(
      {
        statusTransfer: item,
        statuses: [item?.code],
      },
      () => this.search()
    );
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      isPrint: false,
    });
    this.getCountStatus();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(data.message);
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          }
          this.updatePageData();
          this.handleDialogClose();
        } else {
          toast.warning(data.message);
        }
      })
      .catch((error) => {
        toast.error(this.props.t("general.error"));
      });
  };

  componentDidMount() {
    let roles = getTheHighestRole();
    this.setState(
      {
        ...roles,
        statuses: null,
      },
      () => this.updatePageData()
    );
    this.getCountStatus();
  }

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countPlan, countMoving, countMoved;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusTransferToAnother[0].code === item?.status) {
            countPlan = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusTransferToAnother[1].code === item?.status) {
            countMoving = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusTransferToAnother[2].code === item?.status) {
            countMoved = this.checkCount(item?.count);
            return;
          }
        });
        this.setState({ countPlan, countMoving, countMoved }, () =>
          setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleAddIds = (data) => {
    let ids = [];
    data?.map((item) => ids.push(item?.id));
    this.setState({ ids });
  };

  handleDeleteAllAssetTransfer = () => {
    if (this.state?.selectedList?.length > 0) {
      this.setState({
        shouldOpenConfirmationDeleteAllDialog: true,
      });
      return;
    }
    toast.warning("Chưa chọn bản ghi nào");
  };

  handleDeleteAll = () => {
    deleteAllItem(this.state?.selectedList)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(data.message);
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          }
          this.updatePageData();
          this.handleDialogClose();
        } else {
          toast.warning(data.message);
        }
      })
      .catch((error) => {
        toast.error(this.props.t("general.error"));
      });
  };

  /* Export to excel */
  handleExportToExcel = async (e, type) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let {
      statusTransfer,
      currentUser,
      issueDateTop,
      issueDateBottom,
      selectedList,
      createDateTop,
      createDateBottom
    } = this.state;
    let searchObject = {
      orgId: currentUser?.org?.id,
      assetClass: appConst.assetClass.TSCD,
      ids: selectedList
    };

    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword?.trim() || null;
    if (statusTransfer) {
      searchObject.transferToAnotherUnitStatus = statusTransfer?.indexOrder;
    }
    if (issueDateTop) {
      searchObject.toDate = convertFromToDate(issueDateTop).toDate;
    }
    if (issueDateBottom) {
      searchObject.fromDate = convertFromToDate(issueDateBottom).fromDate;
    }
    searchObject.createDateTop = convertFromToDate(createDateTop)?.toDate;
    searchObject.createDateBottom = convertFromToDate(createDateBottom)?.fromDate;
    setPageLoading(true);
    try {
      let res = await exportToExcel(searchObject);

      try {
        //Nếu call api bị lỗi 500 thì convert được sang object để check lỗi
        const data = JSON.parse(res?.data)
        toast.warning(data.message)
      }
      catch (e) {
        // Nếu call api thành công data ở dạng blob chỉ có thể lưu
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, "AssetTransferToAnotherUnit.xlsx");
        toast.success(t("general.successExport"));
      }
    }
    catch (e) {
      console.log(e)
    }
    finally {
      setPageLoading(false);
    }
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  setStateTransferToAnother = (item) => {
    this.setState(item);
  };

  formatDataDto = (transferToAnotherUnit) => {
    let personIds = [];
    let listAssetDocumentId = [];
    let userIds = [];

    transferToAnotherUnit.createDate = formatDateTypeArray(
      transferToAnotherUnit?.createDate
    );
    transferToAnotherUnit.handoverDepartment = {
      id: transferToAnotherUnit?.handoverDepartmentId,
      code: transferToAnotherUnit?.handoverDepartmentCode,
      name: transferToAnotherUnit?.handoverDepartmentName,
    };

    transferToAnotherUnit.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse,
        originalCost: Math.ceil(item?.assetOriginalCost),
        carryingAmount: item?.assetCarryingAmount,
        managementCode: item?.assetManagementCode,
        yearOfManufacture: item?.assetYearOfManufacture,
        madeIn: item?.assetMadeIn,
      };
      item.asset = data;
      return item;
    });

    transferToAnotherUnit?.persons?.map((item) => {
      personIds?.push(item?.id);
      userIds?.push(item?.userId);
      return item;
    });
    transferToAnotherUnit.personIds = personIds;
    transferToAnotherUnit.userIds = userIds;

    transferToAnotherUnit?.documents?.map((item) =>
      listAssetDocumentId?.push(item?.id)
    );
    transferToAnotherUnit.listAssetDocumentId = listAssetDocumentId;

    return transferToAnotherUnit;
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.listStatusTransferToAnother[0].code:
        return (
          <span className="status status-success">
            {appConst.listStatusTransferToAnother[0].name}
          </span>
        );
      case appConst.listStatusTransferToAnother[1].code:
        return (
          <span className="status status-warning">
            {appConst.listStatusTransferToAnother[1].name}
          </span>
        );
      case appConst.listStatusTransferToAnother[2].code:
        return (
          <span className="status status-error">{appConst.listStatusTransferToAnother[2].name}</span>
        );

      default:
        break;
    }
  };
  handleSetItemState = async (rowData) => {
    this.setState({
      itemAsset: rowData,
    });
  };

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };
  handlePrintQRCode = async (id) => {
    let { t } = this.props
    try {
      let res = await getListQRCode(id);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          products: res?.data?.data?.length > 0 ? [...res?.data?.data] : [],
          openPrintQR: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }

  handleGetVoucherById = async (id) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    try {
      setPageLoading(true);
      let res = await getItemById(id);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        let item = this.formatDataDto(data);
        this.setState({
          item,
        });
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleEdit = (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.handleGetVoucherById(id).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
      });
    })
  }

  handleView = (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.handleGetVoucherById(id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: true,
        },
        shouldOpenEditorDialog: true,
      });
    })
  }

  handlePrint = (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.handleGetVoucherById(id).then(() => {
      this.setState({
        isPrint: true,
      });
    })
  }

  render() {
    const { t, i18n } = this.props;
    let { tabValue, checkAll } = this.state;
    let TitlePage = t("Asset.asset_transfer_to_another_unit");
    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "center",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        width: 120,
        align: "center",
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              let { setPageLoading } = this.context;
              if (appConst.active.edit === method) {
                this.handleEdit(rowData.id);
              } else if (appConst.active.view === method) {
                this.handleView(rowData.id);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData.id);
              } else if (appConst.active.qrcode === method) {
                this.handlePrintQRCode(rowData.id)
              }
              else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        align: "center",
        width: 50,
        render: (rowData) =>
          this.state.page * this.state.rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("TransferToAnotherUnit.createDate"),
        field: "createDate",
        align: "left",
        width: "150",
        render: (rowData) =>
          rowData.createDate ? (
            <span>{moment(rowData.createDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("TransferToAnotherUnit.issueDate"),
        field: "issueDate",
        align: "center",
        minWidth: 130,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("TransferToAnotherUnit.code"),
        field: "code",
        minWidth: 100,
        align: "center",
      },
      {
        title: t("TransferToAnotherUnit.status"),
        field: "transferToAnotherUnitStatus",
        align: "center",
        minWidth: "200px",
        render: (rowData) =>
          this.checkStatus(rowData.transferToAnotherUnitStatus),
      },
      {
        title: t("TransferToAnotherUnit.name"),
        field: "name",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("TransferToAnotherUnit.transferAddress"),
        field: "transferAddress",
        minWidth: "200px",
        align: "left",
      },
    ];

    return (
      <div className="mt-sm-30 mx-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/transfer-to-another-unit-vouchers",
              },
              { name: t("Asset.asset_transfer_to_another_unit") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("TransferToAnotherUnit.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("TransferToAnotherUnit.plan")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countPlan || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("TransferToAnotherUnit.moving")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countMoving || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("TransferToAnotherUnit.moved")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countMoved || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          className="TabPanelTable mp-0"
          value={tabValue}
          index={appConst?.tabTransferToAnother?.tabAll}
        >
          <ComponentTransferToAnotherUnitTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransferToAnother}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            handleDateChange={this.handleDateChange}
            columns={columns}
            handleDeleteAllAssetTransfer={this.handleDeleteAllAssetTransfer}
            handleAddIds={this.handleAddIds}
            handleChangeSelect={this.handleChangeSelect}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetTranferTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>

        <TabPanel
          value={tabValue}
          index={appConst?.tabTransferToAnother?.tabPlan}
          className="mp-0"
        >
          <ComponentTransferToAnotherUnitTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransferToAnother}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            handleDateChange={this.handleDateChange}
            columns={columns}
            handleDeleteAllAssetTransfer={this.handleDeleteAllAssetTransfer}
            handleAddIds={this.handleAddIds}
            handleChangeSelect={this.handleChangeSelect}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetTranferTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabTransferToAnother?.tabMoving}
          className="mp-0"
        >
          <ComponentTransferToAnotherUnitTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransferToAnother}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            handleDateChange={this.handleDateChange}
            columns={columns}
            handleDeleteAllAssetTransfer={this.handleDeleteAllAssetTransfer}
            handleAddIds={this.handleAddIds}
            handleChangeSelect={this.handleChangeSelect}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetTranferTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabTransferToAnother?.tabMoved}
          className="mp-0"
        >
          <ComponentTransferToAnotherUnitTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransferToAnother}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            handleDateChange={this.handleDateChange}
            columns={columns}
            handleDeleteAllAssetTransfer={this.handleDeleteAllAssetTransfer}
            handleAddIds={this.handleAddIds}
            handleChangeSelect={this.handleChangeSelect}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetTranferTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
      </div>
    );
  }
}

AssetTransferToAnotherUnitTable.contextType = AppContext;
export default AssetTransferToAnotherUnitTable;
