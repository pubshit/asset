import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_voucher =
  ConstantList.API_ENPOINT +
  "/api/v1/fixed-assets/transfer-to-another-unit-vouchers";
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_staff =
  ConstantList.API_ENPOINT + "/api/staff" + ConstantList.URL_PREFIX;
const API_PATH_DOCUMENT = ConstantList.API_ENPOINT + "/api/v1/asset-documents";
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";

export const searchByPage = (searchObject) => {
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      statuses: searchObject?.statuses,
      createDateBottom: searchObject?.createDateBottom,
      createDateTop: searchObject?.createDateTop,
      issueDateTop: searchObject?.issueDateTop,
      issueDateBottom: searchObject?.issueDateBottom,
    },
  };
  let url = API_PATH_voucher + "/page";
  return axios.get(url, config);
};

export const addAsset = (asset) => {
  return axios.post(API_PATH_voucher, asset);
};

export const updateAsset = (id, asset) => {
  let url = API_PATH_voucher + "/" + id;
  return axios.put(url, asset);
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH_voucher + "/" + id);
};

export const deleteAllItem = (listId) => {
  let config = {
    params: { ids: listId?.toString() },
  };
  return axios.delete(API_PATH_voucher, config);
};

export const getItemById = (id) => {
  return axios.get(API_PATH_voucher + "/" + id);
};

export const getCountStatus = () => {
  let url = API_PATH_voucher + "/count-by-statuses";
  return axios.get(url);
};

export const getNewCode = () => {
  let url = API_PATH_voucher + "/new-code";
  return axios.get(url);
};

export const personSearchByPage = (searchObject) => {
  let url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/asset-transfer-to-unit",
    data: searchObject,
    responseType: "blob",
  });
};

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};

export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const getManagementDeparmentByOrg = (searchObject) => {
  let url = API_PATH_department + "/management-departments";
  return axios.get(url);
};

//Nhân viên
export const searchEmployeesByPage = (searchObject) => {
  let url = API_PATH_staff + "/searchByPage";
  return axios.post(url, searchObject);
};

// document
export const getNewCodeDocument = () => {
  let config = {
    params: {
      type: "ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT",
    },
  };
  let url = API_PATH_DOCUMENT + "/new-code";
  return axios.get(url, config);
};
export const getListQRCode = (id) => {
  let url = API_PATH_NEW + `/voucher/print-asset-qr-code/${id}`;
  return axios.get(url);
};

export const getAccessoriesByVoucherId = (id) => {
  let url = API_PATH_NEW + `/voucher/${id}/fixed-asset/accessories`;
  return axios.get(url);
};
