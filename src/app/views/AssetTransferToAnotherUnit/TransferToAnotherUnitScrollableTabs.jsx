import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from "material-table";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { a11yProps, checkInvalidDate, convertNumberPrice, filterOptions } from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { appConst } from "app/appConst";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import { searchByPage } from "../Council/CouncilService";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import { Label, TabPanel, LightTooltip } from "../Component/Utilities";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import {searchByPage as searchByPageOrg} from "../Organization/OrganizationService";

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => props.onSelect(item, appConst.active.delete)}
      >
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function TransferToAnotherUnitScrollableTabs(props) {
  const t = props.t;
  const i18n = props.i18n;
  let { item } = props
  let { shouldOpenPopupAssetFile } = item
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isCheckStatusNew, setIsCheckStatusNew] = React.useState(false);
  const [listTransferCountBoard, setListTransferCountBoard] = useState(null);
  const [listTransferPerson, setListTransferPerson] = useState(
    props?.item?.persons ? [...props?.item?.persons] : []
  )
  const [transferBoard, setTransferBoard] = useState(null)
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({
    departmentId: '',
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  });
  let searchObject = {
    keyword: "",
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    type: appConst.OBJECT_HD_TYPE.CHUYEN_DI.code,
    isActive: appConst.STATUS_HOI_DONG.HOAT_DONG.code
  }
  let transferToAnotherUnitStatus = appConst.listStatusTransferToAnother.find(
    (status) => status.code === props?.item?.transferToAnotherUnitStatus
  ) || "";

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? '',
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
    });

  }, [props?.item?.handoverDepartment]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleRowDataCellDelete = (item) => {
    let newDataRow = listTransferPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetTransferToAnotherUnitUserList(newDataRow)
    setListTransferPerson(newDataRow || []);
  };

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "center",
      hidden: props?.item?.isView,
      minWidth: "150px",
      render: (rowData) =>
        !props?.item?.isView && (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (appConst.active.delete === method) {
                props.removeAssetInlist(rowData.asset.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
    },
    {
      title: t("Asset.stt"),
      field: "asset.code",
      width: "50px",
      headerStyle: {
        textAlign: "center",
      },
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    { title: t("Asset.code"), field: "asset.code", minWidth: "150px", align: "center" },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("TransferToAnotherUnit.yearPutIntoUse"),
      field: "asset.yearPutIntoUse",
      align: "center",
      minWidth: "150px"
    },
    {
      title: t("TransferToAnotherUnit.originalCost"),
      field: "asset.originalCost",
      align: "right",
      minWidth: "150px",
      render: (rowData) => convertNumberPrice(rowData?.asset?.originalCost),
    },
    {
      title: t("TransferToAnotherUnit.carryingAmount"),
      field: "asset.carryingAmount",
      align: "right",
      minWidth: "150px",
      render: (rowData) => convertNumberPrice(rowData?.asset?.carryingAmount),
    },
    { title: t("Asset.accumulatedDepreciationAmount"), field: "", align: "left", minWidth: "150px" },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "center",
      minWidth: "150px",
    },
  ];

  const columnsTransferCount = [
    {
      title: t("TransferToAnotherUnit.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.transferToAnotherUnitStatus !== appConst.listStatusTransferToAnotherObject.DA_CHUYEN_DI.code ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 100,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => handleRowDataCellDelete(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      },
    ] : []),
    {
      title: t("TransferToAnotherUnit.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    }
  ]

  let columnsAssetFile = [
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      (
        // !props?.item?.isView && 
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          {!props.item?.isView && (
            <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}>
                <Icon fontSize="small" color="error">delete</Icon>
              </IconButton>
            </LightTooltip>
          )}
        </div>
      )
    },
    {
      title: t("general.stt"),
      field: "code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1 || '',
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 350,
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
    }
  };
  const handleChangeSelecttransferBoard = (item) => {
    setTransferBoard(item);
    props.handleSetHoiDongId(item?.id)
    props.handleSetTransferToAnotherUnitUserList(item?.hdChiTiets)
    item?.hdChiTiets?.map(person => {
      person.handoverDepartment = {
        departmentId: person.departmentId,
        departmentName: person.departmentName,
        name: person.departmentName
      }
      return person
    })
    setListTransferPerson(item?.hdChiTiets || []);
  };

  return (  
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("ReceivingScrollableTabsButtonForce.tab.InformationVoting")} {...a11yProps(0)} />
          <Tab label={t("TransferToAnotherUnit.personsMove")} {...a11yProps(1)} />
          <Tab label={t("MaintainResquest.AttachedFile.title")} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={2} xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <DateTimePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("Ngày tạo phiếu")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                name={"createDate"}
                value={props?.item?.createDate}
                InputProps={{
                  readOnly: true,
                }}
                readOnly={true}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} xs={6}>
            <CustomValidatePicker
              label={
                <Label isRequired>
                  {t("TransferToAnotherUnit.issueDate")}
                </Label>
              }
              name={"issueDate"}
              value={props?.item?.issueDate || null}
              onChange={(date) => props.handleDateChange(date, "issueDate")}
              readOnly={props?.item?.isView}
              validators={!isCheckStatusNew ? ["required"] : []}
              errorMessages={t("general.required")}
              onBlur={() =>
                handleBlurDate(props?.item?.issueDate, "issueDate")
              }
              clearable
              maxDate={new Date()}
              maxDateMessage={t("allocation_asset.maxDateMessage")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired>
                  {t("allocation_asset.handoverDepartment")}
                </Label>
              }
              isNoRenderChildren
              searchFunction={getListOrgManagementDepartment}
              searchObject={props.handoverPersonSearchObject}
              listData={props.item.listHandoverDepartment}
              setListData={props.handleSetDataHandoverDepartment}
              displayLable="name"
              readOnly={props?.item?.isRoleManager || props?.item?.isView}
              value={props.item?.handoverDepartment || null}
              onSelect={props.selectHandoverDepartment}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              typeReturnFunction="list"
              showCode="showCode"
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <Label isRequired>
                  {t("allocation_asset.handoverPerson")}
                </Label>
              }
              disabled={!props?.item?.handoverDepartment?.id || props.item?.isView}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByHandoverDepartment}
              defaultValue={props?.item?.handoverPerson ?? ''}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.handoverPerson ?? null}
              onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              name="code"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.code")}
              InputProps={{
                readOnly: true,
              }}
              disabled={!props.item?.code}
              value={props.item?.code || ""}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              name="name"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.name")}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              value={props.item?.name || ""}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  <span> {t("TransferToAnotherUnit.status")}</span>
                </span>
              }
              searchFunction={() => { }}
              listData={appConst.listStatusTransferToAnother}
              setListData={props.handleSetDataHandoverPerson}
              selectedOptionKey="code"
              displayLable={"name"}
              readOnly={props?.item?.isView}
              value={transferToAnotherUnitStatus || null}
              onSelect={props.handleChangeSelect}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("TransferToAnotherUnit.receiverOrg")}
              searchFunction={searchByPageOrg}
              searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
              listData={props.item?.listOrganization}
              setListData={props.handleDateChange}
              nameListData="listOrganization"
              displayLable={"name"}
              readOnly={props?.item?.isView}
              typeReturnFunction="category"
              name="receiverOrg"
              value={props.item?.receiverOrg || null}
              onSelect={props.handleDateChange}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              name="transferAddress"
              onChange={props?.handleChange}
              label={
                <Label isRequired> {t("TransferToAnotherUnit.transferAddress")}</Label>
              }
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              value={props.item?.transferAddress || ""}
              validators={["required"]}
              errorMessages={t("general.required")}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              name="conclude"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.conclude")}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              value={props.item?.conclude || ""}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            {!props?.item?.isView && (
              <Button
                variant="contained"
                color="primary"
                size="small"
                className="mb-16 w-100"
                onClick={props.openPopupSelectAsset}
              >
                {t("TransferToAnotherUnit.choose")}
              </Button>
            )}
            {props.item.shouldOpenAssetPopup && (
              <SelectAssetAllPopup
                t={t}
                i18n={i18n}
                open={props.item.shouldOpenAssetPopup}
                handleSelect={props.handleSelectAsset}
                isAssetTransfer={props.item.isAssetTransfer}
                statusIndexOrders={props.item.statusIndexOrders}
                handleClose={props.handleAssetPopupClose}
                handoverDepartment={props.item?.handoverDepartment}
                dateOfReceptionTop={props?.item?.issueDate}
                assetVouchers={
                  props.item.assetVouchers ? props.item.assetVouchers : []
                }
              />
            )}
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={props.item.assetVouchers ? props.item.assetVouchers : []}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  textAlign: "center",
                },
                maxBodyHeight: "225px",
                minBodyHeight: "225px",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>

      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction='category'
            searchObject={searchObject}
            label={t('TransferToAnotherUnit.transferCountBoard')}
            listData={listTransferCountBoard}
            setListData={setListTransferCountBoard}
            displayLable={"name"}
            onSelect={handleChangeSelecttransferBoard}
            value={transferBoard}
            disabled={props?.item?.isView}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="pt-16">
          <MaterialTable
            data={listTransferPerson || []}
            columns={columnsTransferCount}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Grid item md={12} sm={12} xs={12}>
          {!props?.item?.isView && (
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          )}
          {/* {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocumentId={props.getAssetDocumentId}
              getAssetDocument={props.getAssetDocument}
              handleUpdateAssetDocument={props.handleUpdateAssetDocument}
              documentType={props.item?.documentType}
              item={props.item}
              t={t}
              i18n={i18n}
            />
          )}*/}
          {shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocumentId={props.getAssetDocumentId}
              getAssetDocument={props.getAssetDocument}
              handleUpdateAssetDocument={props.handleUpdateAssetDocument}
              documentType={props.item?.documentType}
              item={props.item}
              isIAT={true}
              t={t}
              i18n={i18n}
            />
          )}
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item.documents || []}
            columns={columnsAssetFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "290px",
              minBodyHeight: "290px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div>
  );
}
