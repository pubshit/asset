import { appConst, variable } from "../../appConst";
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  Grid,
  Icon,
  IconButton,
  withStyles,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  autoCreateMaBHYT,
  autoCreateMaKeToan,
  createAsset,
  getAssetDocumentById,
  getNewCodeDocument,
  updateAsset,
  uploadImageAsset,
} from "./AssetService";
import {
  searchByPage as statusSearchByPage,
  searchByPage as searchByPageAssetStatus,
} from "../AssetStatus/AssetStatusService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import ScrollableTabsButtonForceNew from "./ScrollableTabsButtonForceNew";
import { personSearchByPage } from "../AssetTransfer/AssetTransferService";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import {
  checkInvalidDate,
  checkObject,
  formatDateDto,
  getAssetStatus,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate,
} from "../../appFunction";
import AppContext from "app/appContext";
import { validateAllocationFor, validateSubmit } from "./AssetValidate";
import { getStockKeepingUnit } from "../StockKeepingUnit/StockKeepingUnitService";
import ComponentDialog from "./Component/ComponentDialog";
import { createFilterOptions } from "@material-ui/lab/Autocomplete";
import ProductDialog from "../Product/ProductDialog";
import { searchByPageProductOrg } from "../MaintainResquest/MaintainRequestService";
import { debounce } from "lodash";
import { getNewCode } from "../Product/ProductService";
import localStorageService from "app/services/localStorageService";
import { PaperComponent } from "../Component/Utilities";


const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  dialogTitle: {
    position: "relative",
    cursor: "move",
    paddingTop: "5px",
    paddingBottom: "5px",
    background: "#fff",
    // borderBottom: "0.5px solid var(--primary)",
  },
  dialogContent: {
    overflow: "auto",
    maxHeight: "78vh",
    paddingTop: "0px",
    paddingBottom: "0px",
    background: "#DEE2E6",
  },
});

class AssetEditorDialogNew extends Component {
  state = {
    name: "",
    code: "",
    qrcode: "",
    managementCode: "",
    // allocationFor: appConst.LOAI_CAP_PHAT.PHONG_BAN,
    receiverPerson: null,
    madeIn: "",
    model: "",
    usedTime: null,
    warrantyMonth: "",
    yearOfManufacture: 0,
    serialNumber: "",
    useDepartment: null,
    managementDepartment: null,
    installationLocation: "",
    supplyUnit: null,
    dayStartedUsing: null,
    dateOfReception: this.props.isTiepNhan ? this.props.item.issueDate : new Date(),
    assetSource: null,
    status: null,
    originalCost: 0,
    carryingAmount: null,
    warrantyExpiryDate: null,
    depreciationDate: new Date(),
    depreciationPeriod: null,
    depreciationRate: null,
    accumulatedDepreciationAmount: null,
    product: null,
    biddingPackage: null,
    yearPutIntoUse: null,
    assetGroup: null,
    manufacturer: null,
    isEditCode: false,
    isCreateQRCode: true,
    checkUseDepartmentByStatus: false,
    shouldOpenPersonPopup: false,
    shouldOpenSelectUsePersonPopup: false,
    shouldOpenSelectUseDepartmentPopup: false,
    shouldOpenSelectAssetGroupPopup: false,
    shouldOpenSelecPopup: false,
    usePerson: null,
    displayName: null,
    attributes: [],
    shouldOpenPopupSelectAttribute: false,
    shouldOpenAssetAllocationEditorDialog: false,
    unitPrice: null,
    openAssetAllocationPopup: false,
    shouldOpenSelectUserDepartment: false,
    shouldOpenPopupAssetFile: false,
    assetDocumentList: [],
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    isEditAssetDocument: false,
    isManageAccountant: false,
    isTemporary: false,
    isReceivingAsset: false,
    storage: [],
    quantity: null,
    quantityCode: 1,
    listCodes: "",
    store: null,
    loading: false,
    listAssetDocumentId: [],
    assetStatusNew: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder, // mới
    AssetStatusAllocation: appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder, // đã cấp phát
    documentType: appConst.documentType.ASSET_DOCUMENT,
    productTypeCode: appConst.productTypeCode.TSCĐ,
    shouldOpenPopupSelectAssetSource: false,
    assetSources: [],
    isNew: false,
    isMedicalEquipment: false,
    isBuyLocally: null,
    isAllocation: false,
    shouldOpenAllocationEditorDialog: false,
    numberOfClones: 1,
    shouldOpenDialogStore: false,
    shouldOpenDialogManufacturer: false,
    shouldOpenDialogShoppingForm: false,
    shouldOpenDialogUnit: false,
    shouldOpenDialogMedicalEquipment: false,
    imageUrl: null,
    bhytMaMay: null,
    shouldGenerateBhytMaMay: null,
    financialCode: null,
    soVBQuyDinhKiemDinh: null,
    donViKiemDinh: null,
    chuKyGiaHanCapPhep: null,
    issueDate: null,
    isFinancialCode: false,
    donViBaoHanh: null,
    donViBaoHanhId: null,
    donViBaoHanhText: null,
    imageSaveLocal: null,
    contract: null,
    contractId: null,
    isUpdateAsset: null,
    assetAllocation: null,
    soNamKhConLai: null,
    namKhGanNhat: null,
    soNamDaKhauHao: 0,
    shouldOpenSelectSupplyPopup: false,
    shouldOpenAddContractDialog: false,
    shouldOpenPopupAssetAccessoryDetails: false,
    shouldOpenAddSupplierDialog: false,
    assetAccessories: [],
    listUnit: [],
    itemProduct: {},
    keywordSearch: {
      index: null,
      keywordSearch: "",
    },
    shouldOpenDialogProduct: false,
    indexLinhKien: null,
    listProductOrg: [],
    isAddAssets: false,
    serialNumbers: [],
    valueSerialNumber: "",
    isClones: false,
    isSupply: false,
    madeIns: {},
    quantityAsset: null,
    itemAssetAccessory: {},
    donViHieuChuan: null,
    hanHieuChuan: null,
    hanKiemXa: null,
    hanKiemDinh: null,
    thongTuHieuChuan: null,
    thongTuKiemXa: null,
    usePersonId: null,
    allocationFor: false,
    messageError: null,
    isShowToast: true,
    keySearch: "",
    isErrorGeneralInfomation: false,
    isErrorDepreciationInformation: false,
    isErrorOriginTools: false,
    isErrorInstrumentToolProperties: false,
    isErrorSparePart: false,
    listBidding: []
  };

  handleCheckClones = (e) => {
    const isClonesChecked = e.target.checked;

    this.setState({
      isClones: isClonesChecked,
      serialNumbers: [],
      quantityAsset: 0,
    });
  };

  handleChangeSeri = (e) => {
    const { serialNumbers, quantityAsset } = this.state;
    const { value } = e.target;

    if (serialNumbers?.length > 9 && !quantityAsset) return;

    if (value) {
      this.setState({ valueSerialNumber: value });
    }
  };

  validateInput(inputValue) {
    const regex = /^[a-zA-Z0-9]*$/;
    return regex.test(inputValue);
  }

  handleKeyUp = (e, t) => {
    e.preventDefault();
    const { valueSerialNumber, serialNumbers, quantityAsset } = this.state;

    if (e.keyCode === 13 && valueSerialNumber) {
      if (quantityAsset && serialNumbers?.length > quantityAsset - 1) {
        toast.warn(t('Validator.loiNhanBan'))
        return
      }
      if (serialNumbers.includes(valueSerialNumber)) {
        toast.warn(t('Validator.ErrorInputSerial'));
        return;
      }

      this.setState((prevState) => ({
        serialNumbers: [...prevState.serialNumbers, valueSerialNumber],
        valueSerialNumber: "",
      }));

      // const isValid = this.validateInput(valueSerialNumber);
      // if (isValid) {
      //   this.setState((prevState) => ({
      //     serialNumbers: [...prevState.serialNumbers, valueSerialNumber],
      //     valueSerialNumber: "",
      //   }));
      // } else {
      //   this.setState({ valueSerialNumber: valueSerialNumber });
      // }
    }
  };

  handleDeleteSeri = (index) => {
    const { serialNumbers } = this.state;
    const updatedSerialNumbers = serialNumbers.filter((_, i) => i !== index);
    this.setState({ serialNumbers: updatedSerialNumbers });
  };

  handleDialogClose = () => {
    this.getDanhSachLinhKien();
    this.setState({
      shouldOpenDialogProduct: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenDialogProduct: false,
    });
  };

  setItemProduct = (value) => {
    this.setState({
      itemProduct: value,
    });
  };

  searchObjectUnit = { pageIndex: 1, pageSize: 1000000 };

  getProductOrg = async (query) => {
    try {
      let res = await searchByPageProductOrg(query);
      if (res?.data?.content && res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listProductOrg: res?.data?.content,
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  handleSelectLinhKien = (value, index, name) => {
    const department = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
    if (value?.code === variable.listInputName.New) {
      getNewCode()
        .then((result) => {
          if (result != null && result?.data && result?.data?.code) {
            let item = result.data;
            this.setState({
              indexLinhKien: index,
              shouldOpenDialogProduct: true,
              itemProduct: {
                ...item,
                name: this.state.keywordSearch.keywordSearch,
                managementPurchaseDepartment: department
              }
            });
          }
        })
        .catch(() => {
          alert(this.props.t("general.error_reload_page"));
        });
    } else {
      let { assetAccessories = [], } = this.state;
      let item = assetAccessories ? assetAccessories[index] : null;
      if (item) {
        if (variable.listInputName.product === name) {
          item.productId = value?.id ?? "";
          item.productName = value?.name ?? "";
          item.productType = value?.type ?? "";
          item.unit = value?.defaultSku?.sku ?? null;
          item.unitName = value?.defaultSku?.sku?.name ?? "";
          item.unitId = value?.defaultSku?.sku?.id ?? "";
        }
        else if (variable.listInputName.unit === name) {
          item.unitName = value?.name ?? "";
          item.unitId = value?.id ?? "";
        }
        item[name] = value;
        assetAccessories[index] = item;
      }
      this.setState({
        assetAccessories,
        keywordSearch: {
          index: null,
          keywordSearch: "",
        },
        phuTung: false,
      });
    }
  };

  openAssetAccessoryDetails = (rowData) => {
    this.setState({
      itemAssetAccessory: { ...rowData },
      shouldOpenPopupAssetAccessoryDetails: true
    })
  }
  closeAssetAccessoryDetails = () => {
    this.setState({
      shouldOpenPopupAssetAccessoryDetails: false
    })
  }

  filterAutocomplete = createFilterOptions();

  handleChangeInputLinhKien = (e, index, source) => {
    let { assetAccessories = [] } = this.state;
    let item = assetAccessories ? assetAccessories[index] : {};
    if (item) {
      if ((e.target.value && Number(e.target.value) >= 0) || source) {
        item[e.target.name] = e.target.value;
        if (e.target.name === "originQuantity") {
          item.usingQuantity = e.target.value;
        }

      }
      else {
        item[e.target.name] = 0;
      }
    }
    this.setState({ assetAccessories });
  };

  debouncedSetSearchTerm = debounce((newTerm, index) => {
    this.setState({
      keywordSearch: {
        index: index,
        keywordSearch: newTerm,
      },
    });
  }, 300);

  handleSearchListLinhKien = (e) => {
    this.debouncedSetSearchTerm(e.target.value);
  };

  getDanhSachLinhKien = () => {
    let { t, isView } = this.props;
    let dataSpareParts = [];

  };

  async getListStockKeepingUnit() {
    try {
      let res = await getStockKeepingUnit(this.searchObjectUnit);
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({
          listUnit: res?.data?.content,
        });
      }
    } catch (error) { console.error(error) }
  }

  componentDidMount() {
    this.getListStockKeepingUnit();
    this.getProductOrg({
      pageSize: 20,
      pageIndex: 0,
      keyword: this.state.keywordSearch.keywordSearch,
      productTypeCode: appConst.productTypeCode.VTHH,
    });
  }

  setListSpareparts = (value) => {
    this.setState({
      assetAccessories: value,
    });
  };

  convertDto = (state) => {
    return {
      accumulatedDepreciationAmount: state?.accumulatedDepreciationAmount,
      acceptanceDate: state?.acceptanceDate ? formatDateDto(state?.acceptanceDate) : null,
      allocationPeriod: state?.allocationPeriod,
      allocationPeriodValue: state?.allocationPeriodValue,
      assetGroupId: state?.assetGroupId,
      assetGroup: state?.assetGroup,
      assetAccessories: state?.assetAccessories,
      assetSources: state?.assetSources,
      attributes: state?.attributes,
      carryingAmount: state?.carryingAmount,
      circulationNumber: state?.circulationNumber,
      code: state?.code,
      countryOriginId: state?.countryOriginId,
      dateOfReception: formatDateDto(state?.dateOfReception),
      dayStartedUsing: state?.dayStartedUsing,
      depreciationDate: formatDateDto(state?.depreciationDate),
      depreciationPeriod: state?.depreciationPeriod,
      depreciationRate: state?.depreciationRate,
      installationLocation: state?.installationLocation,
      invoiceNumber: state?.invoiceNumber,
      isBuyLocally: state?.isBuyLocally,
      isManageAccountant: state?.isManageAccountant,
      isTemporary: state?.isTemporary,
      listAssetDocumentId: state?.listAssetDocumentId,
      lotNumber: state?.lotNumber,
      madeIn: state?.madeIn,
      managementCode: state?.managementCode,
      managementDepartmentId: state?.managementDepartmentId
        ? state?.managementDepartmentId
        : state?.managementDepartment?.id,
      manufacturerId: state?.manufacturerId
        ? state?.manufacturerId
        : state?.manufacturer?.id,
      manufacturer: state?.manufacturer,
      medicalEquipmentId: state?.medicalEquipmentId
        ? state?.medicalEquipmentId
        : state?.medicalEquipment?.id,
      medicalEquipmentName: state?.medicalEquipment?.name,
      model: state?.model,
      name: state?.name,
      ngayHoaDon: formatDateDto(state?.ngayHoaDon),
      note: state?.note,
      numberOfAllocations: state?.numberOfAllocations,
      numberOfAllocationsRemaining: state?.numberOfAllocationsRemaining,
      originalCost: state?.originalCost,
      productId: state?.productId ? state?.productId : state?.product?.id,
      qrcode: state?.qrcode,
      receiverPersonId: state?.receiverPersonId
        ? state?.receiverPersonId
        : state?.receiverPerson?.id,
      riskClassification: state?.riskClassification,
      serialNumber: state?.serialNumber,
      shoppingFormId: state?.shoppingFormId
        ? state?.shoppingFormId
        : state?.shoppingForm?.id,
      shoppingFormName: state?.shoppingForm?.name,
      storeId: state?.storeId ? state?.storeId : state?.store?.id,
      supplyUnitId: state?.supplyUnit?.id || state?.supplyUnitId,
      supplyUnit: state?.supplyUnit,
      unitId: state?.unitId ? state?.unitId : state?.unit?.id,
      unitPrice: state?.unitPrice,
      useDepartmentId: state?.useDepartmentId
        ? state?.useDepartmentId
        : state?.useDepartment?.id,
      useDepartmentLateId: state?.useDepartmentLateId
        ? state?.useDepartmentLateId
        : state?.useDepartmentLate?.id,
      usePersonId: state?.usePersonId || state?.usePerson?.personId,
      usedTime: state?.usedTime,
      usageState: state?.usageState?.code,
      warrantyExpiryDate: formatDateDto(state?.warrantyExpiryDate),
      warrantyMonth: state?.warrantyMonth,
      yearOfManufacture: state?.yearOfManufacture,
      yearPutIntoUse: state?.yearPutIntoUse,
      listCodes: state?.listCodes,
      numberOfClones: state?.numberOfClones,
      imageUrl: state.imageUrl,
      bhytMaMay: state.isBhytMaMay ? state.bhytMaMay : null,
      shouldGenerateBhytMaMay: state.shouldGenerateBhytMaMay ? state.shouldGenerateBhytMaMay : null,
      financialCode: state.financialCode,
      hanKiemDinh: formatDateDto(state?.hanKiemDinh),
      hanHieuChuan: formatDateDto(state?.hanHieuChuan),
      hanKiemXa: formatDateDto(state?.hanKiemXa),
      thongTuHieuChuan: state?.thongTuHieuChuan,
      thongTuKiemDinh: state?.thongTuKiemDinh,
      thongTuKiemXa: state?.thongTuKiemXa,
      donViKiemDinhId: state?.donViKiemDinh?.id,
      donViKiemDinhTen: state?.donViKiemDinh?.name,
      donViHieuChuanId: state?.donViHieuChuan?.id,
      donViHieuChuanTen: state?.donViHieuChuan?.name,
      donViKiemXaId: state?.donViKiemXa?.id,
      donViKiemXaTen: state?.donViKiemXa?.name,
      ngayKiemDinh: formatDateDto(state?.ngayKiemDinh),
      ngayHieuChuan: formatDateDto(state?.ngayHieuChuan),
      ngayKiemXa: formatDateDto(state?.ngayKiemXa),
      soTemHieuChuan: state?.soTemHieuChuan,
      soTemKiemDinh: state?.soTemKiemDinh,
      soTemKiemXa: state?.soTemKiemXa,
      chuKyGiaHanCapPhep: state.chuKyGiaHanCapPhep,
      issueDate: formatDateDto(state.issueDate),
      isFinancialCode: state.isFinancialCode,
      donViBaoHanhId: state.donViBaoHanhId,
      donViBaoHanhText: state.donViBaoHanhText,
      contract: state.contract,
      contractId: state.contractId,
      donViBaoHanh: state.donViBaoHanh,
      soNamKhConLai: state.soNamKhConLai,
      namKhGanNhat: state.namKhGanNhat,
      soNamDaKhauHao: state.soNamDaKhauHao,
      serialNumbers: state.serialNumbers,
      isClones: state?.isClones,
      isBhytCodeGenerated: state?.isClones && state?.isBhytMaMay,
      quantityAsset: state?.quantityAsset >= 2 ? state?.quantityAsset : null,
      documentType: null,
      documents: state.documents,
      product: state?.product,
      unit: state?.unit,
      decisionCode: state?.decisionCode?.decisionCode,
      symbolCode: state?.symbolCode,
      brand: state?.brand,
    };
  };

  handleDateChange = (date, name) => {
    let { t } = this.props;
    if (variable.listInputName.acceptanceDate === name) {
      let newDate = date
        ? !checkInvalidDate(date)
          ? new Date(date)
          : null
        : null;
      newDate?.setMonth(
        newDate.getMonth() +
        Number(this.state?.warrantyMonth ? this.state?.warrantyMonth : 0)
      );
      let warrantyExpiryDate = newDate || null;

      this.setState({
        [name]: date,
        warrantyExpiryDate: warrantyExpiryDate,
      });
      return;
    }
    this.setState({
      [name]: date,
    });
  };

  handleChangeSelect = (value, source) => {
    if (!value) {
      this.setState({
        numberOfAllocations: "",
        numberOfAllocationsRemaining: "",
        carryingAmount: "",
        listStore: variable.listInputName.managementDepartment === source
          ? [] : this.state.listStore,
        store: null,
        [source]: value,
      });
      return;
    }
    if (variable.listInputName.donViBaoHanh === source) {
      this.setState({
        donViBaoHanhId: value.id,
        donViBaoHanhText: value.name,
      });
    }
    if (variable.listInputName.contract === source) {
      this.setState({
        contractDate: value?.contractDate,
        contractId: value?.id,
      });
    }
    if (variable.listInputName.assetGroup === source) {
      this.setState({
        assetGroupId: value?.id,
        assetGroup: {
          ...value,
          name: [
            value?.name && value.name,
            value?.depreciationRate && `${value?.depreciationRate}%`,
            value?.namSuDung && `${value?.namSuDung} năm`,
          ]
            .filter(Boolean)
            .join(" - "),
        },
        soNamKhConLai: value?.namSuDung,
      });
    }
    if (variable.listInputName.managementDepartment === source) {
      this.setState({ listStore: [], store: null });
    }
    this.setState({ [source]: value });
  };

  handleChangeSelectContract = (value) => {
    if (variable.listInputName.New === value?.code) {
      this.setState({
        shouldOpenAddContractDialog: true,
      });
    }
    this.setState({
      contract: value,
      contractId: value?.id,
      contractDate: value?.contractDate,
      supplyUnit: value?.supplier
    });
  };

  handleSelectDVBH = (value) => {
    if (variable.listInputName.New === value?.code) {
      this.setState({
        shouldOpenAddSupplierDialog: true,
      });
      return;
    }
    this.setState({
      donViBaoHanh: value,
      donViBaoHanhId: value?.id,
    });
  };

  selectMedicalEquipment = (medicalEquipmentSelected) => {
    if (medicalEquipmentSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogMedicalEquipment: true,
      });
      return;
    }

    this.setState(
      {
        medicalEquipment: medicalEquipmentSelected,
        medicalEquipmentId: medicalEquipmentSelected?.id,
      },
      () => {
        if (this?.state?.isBhytMaMay) {
          this.handleAutoCreateMaBHYT();
        }
      }
    );
  };

  selectUnit = (unitSelected) => {
    if (unitSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogUnit: true,
      });
      return;
    }

    this.setState({
      unit: unitSelected,
      unitId: unitSelected?.id,
    });
  };

  selectShoppingForm = (shoppingFormSelected) => {
    if (shoppingFormSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogShoppingForm: true,
      });
      return;
    }

    this.setState({
      shoppingForm: shoppingFormSelected,
      shoppingFormId: shoppingFormSelected?.id,
    });
  };

  handleChange = (event, source) => {
    event.persist();
    let {
      isMedicalEquipment,
      isBuyLocally,
      yearPutIntoUse,
    } = this.state;

    if (variable.listInputName.yearPutIntoUse === source) {
      yearPutIntoUse = event.target.value;
      this.setState({ yearPutIntoUse: yearPutIntoUse });
      return;
    }

    if (variable.listInputName.numberOfClones === source) {
      this.setState({
        numberOfClones: Number(event.target.value),
      });
      return;
    }

    if (variable.listInputName.warrantyMonth === source) {
      const acceptanceDate = this.state.acceptanceDate;
      let warrantyExpiryDate = null;
      if (acceptanceDate) {
        warrantyExpiryDate = new Date(acceptanceDate);
        warrantyExpiryDate.setMonth(warrantyExpiryDate.getMonth() + Number(event.target.value || 0));
      }

      this.setState({
        [event.target.name]: event.target.value,
        warrantyExpiryDate: warrantyExpiryDate,
      });
    }

    if (variable.listInputName.isBuyLocally === source) {
      if (
        (isBuyLocally && Number(event.target.value) === 1) ||
        (isBuyLocally === false && Number(event.target.value) === 2)
      ) {
        isBuyLocally = null;
      } else {
        isBuyLocally = Number(event.target.value) === 1;
      }
      this.setState({ isBuyLocally: isBuyLocally });
      return;
    }

    if (variable.listInputName.isMedicalEquipment === source) {
      isMedicalEquipment = event.target.checked;
      this.setState({ isMedicalEquipment: isMedicalEquipment });
      return;
    }
    if (variable.listInputName.isManageAccountant === source) {
      if (event.target.value / 1 === 1) {
        this.setState({
          isTemporary: false,
          isManageAccountant: true
        });
      }
      else {
        this.setState({
          isTemporary: true,
          isManageAccountant: false
        });
      }
      return;
    }

    if (variable.listInputName.isCreateQRCode === source) {
      this.setState({
        isCreateQRCode: event.target.checked,
        qrcode: event.target.checked ? this.state.code : null,
      });
      return;
    } else if (variable.listInputName.allocationFor === source) {
      this.setState({
        [event.target.name]: event.target.checked,
        usePerson: this.props?.item?.usePerson?.id && event.target.checked ?
          {
            ...this.props?.item?.usePerson,
            personId: this.props?.item?.usePerson?.id,
            personDisplayName: this.props?.item?.usePerson?.displayName
          } : null,
        usePersonId: !event.target.checked ? null : this.state?.usePersonId
      });
      return;
    } else if (variable.listInputName.financialCode === source) {
      let isChecked = event.target.checked
      this.setState({ isFinancialCode: isChecked });
      if (isChecked) {        
        this.handleCreateMaKeToan(event)
      }
    }
    if (variable.listInputName.addAssets === source) {
      this.setState({ isAddAssets: event.target.checked });
    }
    if (variable.listInputName.depreciationRate === source) {
      if (event.target.value < 0)
        return;
      this.setState({ [event.target.name]: event.target.value, });
      return
    }
    else if (variable.listInputName.isBhytMaMay === source) {
      let isChecked = event.target.checked;
      this.setState({
        isBhytMaMay: isChecked,
        bhytMaMay: "",
        shouldGenerateBhytMaMay: Boolean(isChecked)
      }, () => {
        if (isChecked && !this.props.isTiepNhan) {           
          this.handleAutoCreateMaBHYT();
        }
      });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    }, () => {
      if (this?.state?.isBhytMaMay) {
        if ([
          variable.listInputName.name,
          variable.listInputName.managementCode,
          variable.listInputName.serialNumber,
        ].includes(event.target.name)) {
          this.handleAutoCreateMaBHYT();
        }
      }
    });

  };

  handleCreateMaKeToan = (isChecked) => {
    try {
      autoCreateMaKeToan().then((res) => {
        this.setState({
          financialCode: isChecked ? res.data : null,
        });
      });
    } catch (error) {
      console.error(error);
    }
  }
  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  saveImageWithId = async (id) => {
    try {
      if (id && this.state.imageSaveLocal !== null) {
        const { data } = await uploadImageAsset(id, this.state.imageSaveLocal);
        if (appConst.CODE.SUCCESS === data?.code && data?.data?.length > 0) {
          toast.success(data?.message);
        } else {
          toast.warn(data?.message);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  createAsset = async (state, t) => {
    const { setPageLoading } = this.context;

    try {
      const res = await createAsset(state);
      const { data, code } = res?.data;
      setPageLoading(false);
      if (isSuccessfulResponse(code)) {
        this.saveImageWithId(data.data?.id);

        if (this.state?.isAllocation) {
          this.setState({
            shouldOpenAllocationEditorDialog: true,
            assetAllocation: data?.length > 0 ? data : [data],
          });
        } else {
          toast.success(t("general.addSuccess"));
          this.props.handleOKEditClose();
        }
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      this.handleRequestError(t, error);
    }
  };

  handleRequestError(t, error) {
    let { setPageLoading } = this.context;
    toast.error(t("toastr.error"));
    toast.clearWaitingQueue();
    setPageLoading(false);
  }

  updateAsset = (id, state, t) => {
    let { setPageLoading } = this.context;
    updateAsset(id, state)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          if (this.state?.isAllocation) {
            this.setState({
              isUpdateAsset: true,
              shouldOpenAllocationEditorDialog: true,
              assetAllocation: [data?.data],
            });
            return;
          }
          toast.success(t(data?.message));
          this.props.handleOKEditClose();
          return;
        }
        else {
          toast.warning(t(data?.message));
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        toast.clearWaitingQueue();
        setPageLoading(false);
      });
  };

  customSetState = (data) => {
    this.setState(data);
  };

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let { isReceivingAsset, id, serialNumbers, quantityAsset, isShowToast } = this.state;
    if (e?.target?.id !== "parent") return;
    setPageLoading(true);
    let dataState = this.convertDto(this.state);
    if (!validateSubmit(this.state, this.customSetState, toast, this.props)) {
      setPageLoading(false);
      return;
    }

    if (isReceivingAsset && isShowToast) {
      this.props.handleSelect({ ...dataState, documentType: null });
      setPageLoading(false);
      return;
    }
    if (!validateAllocationFor(this.state, this.customSetState, toast)) return;

    if (quantityAsset && serialNumbers?.length > 0) {
      if (+quantityAsset > serialNumbers.length || quantityAsset < serialNumbers.length) {
        toast.warn("Số serial phải bằng số lượng nhân bản")
        return
      }
    }

    setPageLoading(true);
    if (!id) {
      await this.createAsset(dataState, t);
    } else {
      await this.updateAsset(this.state?.id, dataState, t);
    }
  };

  handleClose = () => {
    const { contract } = this.state;
    if (variable.listInputName.New === contract?.code) {
      this.setState({ contract: null });
    }
    this.setState({
      shouldOpenAllocationEditorDialog: false,
      shouldOpenDialogStore: false,
      shouldOpenDialogManufacturer: false,
      shouldOpenDialogShoppingForm: false,
      shouldOpenDialogUnit: false,
      shouldOpenDialogMedicalEquipment: false,
      isAllocation: false,
      shouldOpenAddContractDialog: false,
      shouldOpenPopupAssetAccessoryDetails: false,
      shouldOpenAddSupplierDialog: false,
      isSupply: false,
      keySearch: "",
    });
  };

  handleCloseAll = () => {
    this.props.handleOKEditClose();
    this.setState({
      shouldOpenAllocationEditorDialog: false,
    });
  };

  handleFormSubmitAllocation = () => {
    this.setState({
      isAllocation: true,
    });
  };

  UNSAFE_componentWillMount() {
    let { item, isReceivingAsset, isEditLocal } = this.props;
    const searchObject = { pageIndex: 1, pageSize: 1000000 };
    let storage;
    let { assetAccessories } = item;
    assetAccessories?.map(accessory => {
      accessory.product = {
        name: accessory?.productName,
        id: accessory?.productId,
        type: accessory?.productType,
      }
      accessory.unit = {
        id: accessory?.unitId,
        name: accessory?.unitName,
      }
      return accessory;
    })
    this.setState({
      isMedicalEquipment: this.props?.isMedicalEquipment,
      managementCode: item?.managementCode ||
        (item?.managementDepartment?.code
          ? item.managementDepartment.code + "-"
          : "") ||
        ""
    });
    if (isReceivingAsset) {
      searchByPageAssetStatus(searchObject).then((data) => {
        storage = data.data.content.find(
          (element) => element.indexOrder === appConst.listStatusAssets.LUU_MOI.indexOrder
        );
        this.setState(
          {
            ...this.props.item,
            status: storage,
            assetAccessories: assetAccessories ? assetAccessories : [],
            isReceivingAsset: isReceivingAsset,
            isEditLocal,
            checkUseDepartmentByStatus:
              !item.status ||
              !item.status.indexOrder ||
              item.status.indexOrder === appConst.listStatusAssets.LUU_MOI,
            usePerson: item?.usePerson?.id ? {
              ...item?.usePerson,
              personId: item?.usePerson?.id,
              personDisplayName: item?.usePerson?.displayName
            } : null,
            allocationFor: Boolean(item?.usePerson),
          },);
      });
    } else {

      this.setState({
        ...item,
        assetAccessories: assetAccessories ? assetAccessories : [],
        isReceivingAsset: isReceivingAsset,
        checkUseDepartmentByStatus:
          !item.status ||
          !item.status.indexOrder ||
          item.status.indexOrder === 0,
        usePerson: item?.usePerson?.id ? {
          ...item?.usePerson,
          personId: item?.usePerson?.id,
          personDisplayName: item?.usePerson?.displayName
        } : null,
        allocationFor: Boolean(item?.usePerson),
        donViKiemDinh: item?.donViKiemDinhId ? {
          name: item?.tenDonViKiemDinh,
          id: item?.donViKiemDinhId
        } : null,
        donViHieuChuan: item?.donViHieuChuanId ? {
          name: item?.tenDonViHieuChuan,
          id: item?.donViHieuChuanId
        } : null,
        donViKiemXa: item?.donViKiemXaId ? {
          name: item?.tenDonViKiemXa,
          id: item?.donViKiemXaId
        } : null,
        usageState: appConst.listUsageState.find(x => x.code === item.usageState)
      });
    }

    if (!item?.id) {
      let status = appConst.listStatusAssets.LUU_MOI;
      this.setState({
        status,
        isNew: true,
        usageState: appConst.OBJECT_USAGE_STATE.NEW
      });
    }
  }

  refreshCode = () => {
    getNewCode()
      .then((result) => {
        if (result && result.data && result.data.code) {
          let { isCreateQRCode } = this.state;
          this.setState({
            code: result.data.data,
            qrcode: isCreateQRCode ? result.data.code : null,
          });
        }
      })
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  openSelectProductPopup = () => {
    this.setState({
      shouldOpenSelectProductPopup: true,
    });
  };

  openSelectAssetGroupPopup = () => {
    this.setState({
      shouldOpenSelectAssetGroupPopup: true,
    });
  };

  openSelectSupplyPopup = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: true,
    });
  };

  handleSelectSupplyPopupClose = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: false,
    });
  };

  handleSelectSupply = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        isSupply: true,
        shouldOpenAddSupplierDialog: true,
        supplyUnit: null,
        supplyUnitId: "",
      });
      return;
    }
    this.setState(
      {
        supplyUnit: item,
        supplyUnitId: item?.id,
      },
    );
    if (checkObject(item)) {
      this.setState({ supplyUnit: null });
    }
  };

  handleSelectAssetGroupPopupClose = () => {
    this.setState({
      shouldOpenSelectAssetGroupPopup: false,
    });
  };

  handleSelectProductPopupClose = () => {
    this.setState({
      shouldOpenSelectProductPopup: false,
    });
  };

  handleSelectProduct = (item) => {
    let { isTiepNhan } = this.props;
    this.setState(
      {
        product: item,
        productId: item?.id,
        name: item.name,
        madeIn: item.madeIn,
        yearOfManufacture: item.yearOfManufacture,
        model: item.model,
        warrantyExpiryDate: item.warrantyExpiryDate,
        manufacturer: item.manufacturerId
          ? {
            id: item.manufacturerId,
            name: item.manufacturerName,
          }
          : item.manufacturer || null,
        serialNumber: item.serialNumber,
        unit: item.defaultSku?.sku || item?.unitId ? {
          id: item?.unitId || item.defaultSku?.sku?.id,
          name: item?.unitName || item.defaultSku?.sku?.name
        } : null,
        supplyUnit: item?.supplierId || isTiepNhan ? {
          id: !isTiepNhan ? item?.supplierId : this.state?.supplyUnit?.id,
          name: !isTiepNhan ? item?.supplierName : this.state?.supplyUnit?.name
        } : null,
        attributes: item?.attributes,
        decisionCode: this.state?.isReceivingAsset ? this?.state?.decisionCode : item?.decisionCode ? {
          decisionCode: item?.decisionCode,
        } : null,
        unitPrice: item?.unitPrice,
        originalCost: item?.unitPrice,
      },
      () => {
        this.handleSelectProductPopupClose();
      }
    );
    if (checkObject(item)) {
      this.setState({ product: this.state.product });
    }
  };

  handleAddAttribute = (attributes) => {
    // eslint-disable-next-line no-unused-expressions
    attributes?.forEach((item) => {
      item.attributeId = item?.attribute?.id;
      item.name = item?.attribute?.name;
    });

    this.setState({ attributes }, function () {
      this.handleSelectAttributePopupClose();
    });
  };

  handleAddAssetSource = (items) => {
    // eslint-disable-next-line no-unused-expressions
    items?.forEach((item) => {
      item.assetSourceId = item?.assetSource?.id;
      item.assetSourceName = item?.assetSource?.name;
      item.assetSourceCode = item?.assetSource?.code;
    });
    if (items?.length === 1) {
      items[0].percentage = 100
      items[0].value = this.state.originalCost
    }
    this.setState({ assetSources: items }, function () {
      this.handleSelectAssetSourcePopupClose();
      if (this?.state?.isBhytMaMay) {
        this.handleAutoCreateMaBHYT();
      }
    });
  };

  openPopupSelectAttribute = () => {
    this.setState({
      shouldOpenPopupSelectAttribute: true,
    });
  };

  openAssetAllocationEditorDialog = () => {
    this.setState({
      shouldOpenAssetAllocationEditorDialog: true,
    });
  };

  handleCloseAssetAllocationEditorDialog = () => {
    this.props.handleOKEditClose();
    this.setState({
      shouldOpenAssetAllocationEditorDialog: false,
    });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleSelectAttributePopupClose = () => {
    this.setState({
      shouldOpenPopupSelectAttribute: false,
    });
  };

  handleSelectAssetSourcePopupClose = () => {
    this.setState({
      shouldOpenPopupSelectAssetSource: false,
    });
  };

  openPopupSelectAssetSource = () => {
    this.setState({
      shouldOpenPopupSelectAssetSource: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    });
  };

  openSelectUseDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectUseDepartmentPopup: true,
    });
  };

  openSelectUsePersonPopup = () => {
    this.setState({
      shouldOpenSelectUsePersonPopup: true,
    });
  };

  handleSelectDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    });
  };

  handleSelectUseDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectUseDepartmentPopup: false,
    });
  };

  handleSelectUsePersonPopupClose = () => {
    this.setState({
      shouldOpenSelectUsePersonPopup: false,
    });
  };

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name };
    this.setState(
      {
        managementDepartment: department,
        managementDepartmentId: department?.id,
      },
      () => this.handleSelectDepartmentPopupClose()
    );
    if (checkObject(item)) {
      this.setState({ managementDepartment: null });
    }
  };

  handleSelectUseDepartment = (item) => {
    let department = { id: item.id, name: item.text, text: item.text };
    this.setState({ useDepartment: department }, function () {
      this.handleSelectUseDepartmentPopupClose();
    });

    if (checkObject(item)) {
      this.setState({ useDepartment: null });
    }

    let usePersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: item.id,
    };
    let result = 0;
    let { usePerson } = this.state;
    personSearchByPage(usePersonSearchObject).then((data) => {
      data.data.content.map((person) => {
        if (usePerson) {
          if (usePerson.id === person.id) {
            ++result;
          }
        }
        return person;
      });

      if (result !== 0) {
        this.setState(
          { receiverDepartment: { id: item.id, name: item.text } },
          function () {
            this.handleSelectUseDepartmentPopupClose();
          }
        );
      } else {
        this.setState(
          {
            receiverDepartment: { id: item.id, name: item.text },
            usePerson: null,
          },
          function () {
            this.handleSelectUseDepartmentPopupClose();
          }
        );
      }
    });
  };

  handleSelectAssetGroup = (item) => {
    this.setState(
      {
        assetGroup: item,
        assetGroupId: item?.id,
        depreciationRate: item?.depreciationRate,
        soNamKhConLai: item?.namSuDung,
      },
      () => this.handleSelectAssetGroupPopupClose()
    );
    if (checkObject(item)) {
      this.setState({ assetGroup: null, depreciationRate: null });
    }
  };

  selectManufacturer = (manufacturerSelected) => {
    if (manufacturerSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogManufacturer: true,
      });
      return;
    }

    this.setState({
      manufacturer: manufacturerSelected,
      manufacturerId: manufacturerSelected?.id,
    });
  };

  selectUsingStatus = (usageState) => {
    this.setState({
      usageState,
    });
  };

  selectStatus = (statusSelected) => {
    if (
      !statusSelected ||
      !statusSelected.indexOrder ||
      statusSelected.indexOrder === 0
    ) {
      this.setState({
        status: statusSelected,
        useDepartment: null,
        usePerson: null,
        checkUseDepartmentByStatus: true,
        shouldOpenSelectUserDepartment: false,
        shouldOpenSelecPopup: false,
      });
    } else {
      this.setState({
        status: statusSelected,
        checkUseDepartmentByStatus: false,
        shouldOpenSelectUserDepartment: true,
        shouldOpenSelecPopup: true,
      });
    }
  };

  selectStores = (store) => {
    let { managementDepartment } = this.state;
    if (store?.code === variable.listInputName.New) {
      this.setState({
        item: {
          departmentName: managementDepartment?.name + " - " + managementDepartment?.code,
          departmentId: managementDepartment?.id,
        },
        shouldOpenDialogStore: true,
        listStore: []
      });
      return;
    }
    this.setState({
      store: store,
      storeId: store?.id,
    });
  };

  selectUseDepartment = (departmentSelected) => {
    this.setState({ useDepartment: departmentSelected });
  };

  selectManagementDepartment = (departmentSelected) => {
    this.setState({
      managementDepartment: departmentSelected,
      managementDepartmentId: departmentSelected?.id,
    });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { attributes } = this.state;
    if (attributes && attributes.length > 0) {
      attributes.forEach((element) => {
        if (
          element.tableData &&
          rowData.tableData &&
          element.tableData.id === rowData.tableData.id
        ) {
          if (event.target.name === "valueText") {
            element.valueText = event.target.value;
            this.setState({ attributes });
          }
        }
      });
    }
  };

  handleChangeFormatNumber = (event) => {
    let { assetSources } = this.state;
    if (variable.listInputName.originalCost === event.target.name) {
      this.setState({
        [event.target.name]: event.target.value,
        assetSources: +event.target.value === 0 ? [] : assetSources,
      }, () => {
        this.handleRowDataCellChangeAssetSource(null, null, event.target.value);
      });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
      assetSources,
    });
  };

  handleRowDataCellChangeAssetSource = (rowData, event, changeOriginalCost) => {
    let { assetSources, originalCost } = this.state;
    assetSources?.forEach((element) => {
      let inputValue = Number(event?.target?.value || element?.value);
      if (element?.tableData?.id === rowData?.tableData?.id) {
        if (variable.listInputName.assetSourcePercentage === event?.target?.name) {
          if (inputValue > 100) {
            inputValue = 100;
          } else if (inputValue < 0) {
            inputValue = 0;
          }
          element.percentage = assetSources?.length === 1 ? 100 : inputValue;
          if (originalCost && originalCost >= 0) {
            element.value = assetSources?.length === 1
              ? originalCost
              : (inputValue * originalCost) / 100;
          }
        } else {
          element.value = assetSources?.length === 1 ? originalCost : inputValue;
          if (originalCost && originalCost >= 0) {
            element.percentage = assetSources?.length === 1
              ? 100
              : Math.round(((inputValue || 0) / originalCost) * 100)
          }
        }
      }
      else if (!rowData && changeOriginalCost >= 0) {
        if (+element.percentage > 0) {
          element.value = (
            (element.percentage ? element.percentage : 0)
            * Number(changeOriginalCost)
          ) / 100
        }
      }
    });
    this.setState({ assetSources });
  };


  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    let index = attributes?.findIndex(
      (attribute) => attribute?.attribute?.id === rowData?.attribute?.id
    );
    attributes.splice(index, 1);
    this.setState({ attributes });
  };

  handleRowDataCellDeleteAssetSource = (rowData) => {
    let { assetSources, originalCost } = this.state;

    if (assetSources && assetSources.length > 0) {
      for (let index = 0; index < assetSources.length; index++) {
        if (
          assetSources[index].tableData.id === rowData.tableData.id
        ) {
          assetSources.splice(index, 1);
          break;
        }
      }
    }
    if (assetSources.length === 1) {
      assetSources[0].percentage = 100
      assetSources[0].value = originalCost
    }
    this.setState({ assetSources }, () => {
      if (this?.state?.isBhytMaMay) {
        this.handleAutoCreateMaBHYT();
      }
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId
      ? this.state?.listAssetDocumentId
      : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) =>
        fileDescriptionIds.push(item?.file?.id)
      );

      if (document) {
        document.fileDescriptionIds = fileDescriptionIds;
        document.documentType = this.state.documentType;
      }

      this.setState({
        item: document,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true,
      });
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) =>
        fileDescriptionIds.push(item?.file?.id)
      );

      if (document) {
        document.fileDescriptionIds = fileDescriptionIds;
        document.documentType = this.state.documentType;
      }

      this.setState({
        item: {
          ...document,
          isView: true
        },
        shouldOpenPopupAssetFile: true,
      });
    });
  }

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex((document) => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(
      (documentId) => documentId === id
    );
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state.documentType || appConst.documentType.ASSET_DOCUMENT)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return;
        }
        toast.success(data?.message);
      })
      .catch(() => {
        toast.warning(this.props.t("general.error"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(
      (item) => item?.id === document?.id
    );
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data,
    });
  };

  handleSaveImageAsset = (item) => {
    this.setState({ imageUrl: item });
  };

  handleAutoCreateMaBHYT = async () => {
    let dataState = this.convertDto(this.state);
    let { managementDepartment } = this.state;
    
    const newData = {
      name: dataState.name,
      medicalEquipmentId: dataState.medicalEquipmentId,
      serialNumber: dataState.serialNumber,
      managementCode: dataState.managementCode || managementDepartment?.code,
      assetSources: dataState.assetSources,
      code: dataState?.code 
    };
    try {
      const result = await autoCreateMaBHYT(newData);
      if (appConst.CODE.SUCCESS === result.data?.code) {
        this.setState({ bhytMaMay: result.data?.data });
      }
    } catch (e) {
      console.error(e);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { item } = this.props;
    if (prevProps?.item !== item) {
      this.setState({ ...item });
    }
    if (prevState.keywordSearch !== this.state.keywordSearch) {
      this.getProductOrg({
        pageSize: 20,
        pageIndex: 0,
        keyword: this.state.keywordSearch.keywordSearch,
        productTypeCode: appConst.productTypeCode.VTHH,
      });
    }
  }

  handleSelectAssetMain = (event, rowData) => {
    const newData = this.state.assetSources.map((item) => {
      if (item.assetSourceId === rowData.assetSourceId) {
        return {
          ...item,
          isMainAssetSource: item?.isMainAssetSource ? !item?.isMainAssetSource : item.assetSourceId === rowData.assetSourceId,
          nameOriginHI: item.isMainAssetSource ? "" : item.assetSource.nameOriginHI,
        }
      } else {
        return {
          ...item,
          isMainAssetSource: null
        };
      }
    });
    this.setState({ assetSources: newData });
  };

  handleRowDataCellDeleteSparePart = (rowData) => {
    let listSparepartsUpdate = this.state.assetAccessories.filter(
      (item, index) => index !== rowData.tableData.id
    );
    this.setState({
      assetAccessories: listSparepartsUpdate,
    });
  };

  handleAddNewSparePart = () => {
    let { assetAccessories = [] } = this.state;
    let newArray = assetAccessories ? assetAccessories : []
    newArray.push({
      product: null,
      productName: "",
      productType: "",
      productId: "",
      unit: null,
      unitId: "",
      unitName: "",
      originQuantity: 0,
      brokenQuantity: 0,
      usingQuantity: 0,
      description: "",
    })
    this.setState({ assetAccessories: newArray });
  };

  handlePressEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      return false;
    }
  };

  handleSelectMadeIn = (value) => {
    if (value) {
      if (value?.code === appConst.madeInCode.TRONG_NUOC) {
        this.setState({
          isBuyLocally: true,
          madeIn: "Việt Nam",
          madeIns: appConst.madeIn[0]
        });
      } else if (value?.code === appConst.madeInCode.NGOAI_NUOC) {
        this.setState({ isBuyLocally: false, madeIns: appConst.madeIn[1], madeIn: "", });
      }
    }
    else {
      this.setState({ isBuyLocally: false });
    }
  };

  handleSelectUsePerson = (item) => {
    this.setState({
      usePerson: item ? item : null,
      usePersonId: item?.personId
    });
  };

  handleSetState = (value, source) => {
    this.setState({
      [source]: value,
    });
  };

  isValidDateState = (arrDate = []) => arrDate.some(date => date && !isValidDate(date));

  handleFormError = (errors) => {
    const findError = (key) => Boolean(errors?.find(i => i?.props?.className?.includes(key)));
    const isErrorGeneralInfomation = findError('isTabGeneralInfomation');
    const isErrorDepreciationInformation = findError('isTabDepreciationInformation');
    const isErrorOriginTools = findError('isTabOriginTools');
    const isErrorInstrumentToolProperties = findError('isTabInstrumentToolProperties');
    const isErrorSparePart = findError('isTabSparePart');

    this.setState({
      isErrorGeneralInfomation,
      isErrorDepreciationInformation,
      isErrorOriginTools,
      isErrorInstrumentToolProperties,
      isErrorSparePart,
    }, () => {
      this.handleSetShowToast(false);
      this.context.setPageLoading(false);
    });
  };

  handleSetShowToast = (isShow) => {
    this.setState({ isShowToast: isShow })
  }

  render() {
    let {
      open,
      t,
      i18n,
      isAddAsset,
      isQRCode,
      isDisableQuantity,
      isView,
      itemAssetDocument,
      handleClose,
      isDangSuDung,
      classes,
    } = this.props;
    let {
      warrantyMonth,
      yearPutIntoUse,
      isReceivingAsset,
      quantity,
      loading,
      item,
      product,
      assetGroup,
      shouldOpenSelectAssetGroupPopup,
      shouldOpenPopupAssetFile,
      documentType,
      productTypeCode,
      shouldOpenAllocationEditorDialog,
      shouldOpenDialogStore,
      shouldOpenDialogManufacturer,
      shouldOpenDialogShoppingForm,
      shouldOpenDialogMedicalEquipment,
      isUpdateAsset,
      assetAllocation,
      shouldOpenSelectSupplyPopup,
      supplyUnit,
      status,
    } = this.state;

    const departmentSearchObject = {
      pageIndex: 1,
      pageSize: 100,
      keyword: '',
      checkPermissionUserDepartment: true,
    }
    let assetStatus = getAssetStatus(status?.indexOrder)

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="xl"
        fullWidth={true}
        scroll={"paper"}
      >
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="parent"
          onSubmit={(event) => this.handleFormSubmit(event)}
          onError={this.handleFormError}
          className="validator-form-scroll-dialog"
          onKeyDown={this.handlePressEnter}
        >
          <DialogTitle className={classes.dialogTitle}>
            <span className="mb-1">{!this.props.item?.id ? t("Asset.addAsset") : t("Asset.updateAsset")}</span>
            <IconButton
              size='small'
              onClick={() => handleClose()}
              className='position-top-right mt-8 mr-8'
            >
              <Icon fontSize='small'>
                close
              </Icon>
            </IconButton>
          </DialogTitle>

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={1}>
              <ScrollableTabsButtonForceNew
                t={t}
                i18n={i18n}
                isView={isView}
                isDangSuDung={isDangSuDung}
                item={this.state}
                quantityCode={this.state.quantityCode}
                itemAssetDocument={itemAssetDocument}
                refreshCode={this.refreshCode}
                selectProduct={this.selectProduct}
                handleChange={this.handleChange}
                selectUseDepartment={this.selectUseDepartment}
                selectManufacturer={this.selectManufacturer}
                selectUsingStatus={this.selectUsingStatus}
                selectStatus={this.selectStatus}
                selectStores={this.selectStores}
                handleDateChange={this.handleDateChange}
                handleChangeFormatNumber={this.handleChangeFormatNumber}
                openSelectSupplyPopup={this.openSelectSupplyPopup}
                handleSelectSupply={this.handleSelectSupply}
                handleSelectSupplyPopupClose={this.handleSelectSupplyPopupClose}
                openSelectProductPopup={this.openSelectProductPopup}
                handleSelectProduct={this.handleSelectProduct}
                handleSelectProductPopupClose={this.handleSelectProductPopupClose}
                shouldOpenAssetAllocationEditorDialog={this.state.shouldOpenAssetAllocationEditorDialog}
                openAssetAllocationEditorDialog={this.openAssetAllocationEditorDialog}
                handleCloseAssetAllocationEditorDialog={this.handleCloseAssetAllocationEditorDialog}
                handleSelectAssetGroup={this.handleSelectAssetGroup}
                handleSelectAssetGroupPopupClose={this.handleSelectAssetGroupPopupClose}
                openSelectAssetGroupPopup={this.openSelectAssetGroupPopup}
                openSelectDepartmentPopup={this.openSelectDepartmentPopup}
                handleSelectManagementDepartment={this.handleSelectManagementDepartment}
                handleSelectDepartmentPopupClose={this.handleSelectDepartmentPopupClose}
                openSelectUseDepartmentPopup={this.openSelectUseDepartmentPopup}
                handleSelectUseDepartment={this.handleSelectUseDepartment}
                handleSelectUseDepartmentPopupClose={this.handleSelectUseDepartmentPopupClose}
                openSelectUsePersonPopup={this.openSelectUsePersonPopup}
                handleSelectUsePerson={this.handleSelectUsePerson}
                handleSelectUsePersonPopupClose={this.handleSelectUsePersonPopupClose}
                handleRowDataCellChange={this.handleRowDataCellChange}
                handleChangeQuantity={this.handleChangeQuantity}
                openPopupSelectAttribute={this.openPopupSelectAttribute}
                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
                handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
                handleRowDataCellViewAssetFile={this.handleRowDataCellViewAssetFile}
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                isAddAsset={isAddAsset}
                handleRowDataCellDelete={this.handleRowDataCellDelete}
                openPopupSelectAssetSource={this.openPopupSelectAssetSource}
                handleAddAssetSource={this.handleAddAssetSource}
                handleSelectAssetSourcePopupClose={this.handleSelectAssetSourcePopupClose}
                handleRowDataCellChangeAssetSource={this.handleRowDataCellChangeAssetSource}
                handleRowDataCellDeleteAssetSource={this.handleRowDataCellDeleteAssetSource}
                handleChangeSelect={this.handleChangeSelect}
                selectMedicalEquipment={this.selectMedicalEquipment}
                selectUnit={this.selectUnit}
                selectShoppingForm={this.selectShoppingForm}
                quantity={quantity}
                warrantyMonth={warrantyMonth}
                yearPutIntoUse={yearPutIntoUse}
                isQRCode={isQRCode}
                isDisableQuantity={isDisableQuantity}
                getAssetDocument={this.getAssetDocument}
                handleUpdateAssetDocument={this.handleUpdateAssetDocument}
                handleClose={this.handleClose}
                handleCloseAll={this.handleCloseAll}
                handleSetDataSelect={this.handleSetDataSelect}
                handleSaveImageAsset={this.handleSaveImageAsset}
                handleSelectAssetMain={this.handleSelectAssetMain}
                isRoleAccountant={this.props.isRoleAccountant}
                handleMapDataAsset={this.props.handleMapDataAsset}
                isTiepNhan={this.props?.isTiepNhan}
                isTTHK={this.props.isTTHK}
                handleChangeSelectContract={this.handleChangeSelectContract}
                handleSelectDVBH={this.handleSelectDVBH}
                handleRowDataCellDeleteSparePart={this.handleRowDataCellDeleteSparePart}
                handleAddNewSparePart={this.handleAddNewSparePart}
                setListSpareparts={this.setListSpareparts}
                assetAccessories={this.state.assetAccessories}
                handleChangeSeri={this.handleChangeSeri}
                handleKeyUp={this.handleKeyUp}
                handleDeleteSeri={this.handleDeleteSeri}
                handleSelectMadeIn={this.handleSelectMadeIn}
                handleCheckClones={this.handleCheckClones}
                isEditLocal={this.props.isEditLocal}
                handleSearchListLinhKien={this.handleSearchListLinhKien}
                debouncedSetSearchTerm={this.debouncedSetSearchTerm}
                handleSelectLinhKien={this.handleSelectLinhKien}
                handleChangeInputLinhKien={this.handleChangeInputLinhKien}
                openAssetAccessoryDetails={this.openAssetAccessoryDetails}
                closeAssetAccessoryDetails={this.closeAssetAccessoryDetails}
                assetStatus={assetStatus}
                handleSetShowToast={this.handleSetShowToast}
                handleSetState={this.handleSetState}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div
              className="flex flex-space-between flex-middle"
              style={{ background: "#f5f5f5" }}
            >
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => {
                  this.props.handleClose();
                }}
              >
                {t("general.cancel")}
              </Button>
              {(this.state.status ? this.state.status.indexOrder : null) !==
                appConst.listStatusAssets.DA_THANH_LY.indexOrder &&
                isReceivingAsset && (
                  <Button
                    className="mr-12"
                    variant="contained"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.context.setPageLoading(true);
                      this.handleSetShowToast(true);
                    }}
                    disabled={isView && !isDangSuDung}
                  >
                    {!this.props.item?.id ? t("general.add") : t("general.update")}
                  </Button>
                )}
              {(this.state.status ? this.state.status.indexOrder : null) !==
                appConst.listStatusAssets.DA_THANH_LY.indexOrder &&
                !isReceivingAsset && (
                  <Button
                    className="mr-12"
                    variant="contained"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.context.setPageLoading(true);
                      this.handleSetShowToast(true);
                    }}
                    disabled={isView && !isDangSuDung}
                  >
                    {!this.props.item?.id ? t("general.save") : t("general.update")}
                  </Button>
                )}
              {this.props.isButtonAllocation && (
                <Button
                  className="mr-12"
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={isView}
                  onClick={() => {
                    this.handleFormSubmitAllocation();
                    this.handleSetShowToast(true);
                  }
                  }
                >
                  Lưu và Cấp phát
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>

        <ComponentDialog
          {...this.state}
          t={t}
          i18n={i18n}
          shouldOpenSelectSupplyPopup={shouldOpenSelectSupplyPopup}
          handleSelectSupply={this.handleSelectSupply}
          supplyUnit={supplyUnit}
          handleSelectSupplyPopupClose={this.handleSelectSupplyPopupClose}
          handleClose={this.handleClose}
          shouldOpenDialogManufacturer={shouldOpenDialogManufacturer}
          handleSetDataSelect={this.handleSetDataSelect}
          selectManufacturer={this.selectManufacturer}
          selectUsingStatus={this.selectUsingStatus}
          selectStores={this.selectStores}
          shouldOpenDialogStore={shouldOpenDialogStore}
          handleSelectAssetGroupPopupClose={this.handleSelectAssetGroupPopupClose}
          handleSelectAssetGroup={this.handleSelectAssetGroup}
          assetGroup={assetGroup}
          shouldOpenAssetAllocationEditorDialog={this.state.shouldOpenAssetAllocationEditorDialog}
          handleCloseAssetAllocationEditorDialog={this.handleCloseAssetAllocationEditorDialog}
          selectUnit={this.selectUnit}
          shouldOpenAllocationEditorDialog={shouldOpenAllocationEditorDialog}
          handleCloseAll={this.handleCloseAll}
          assetAllocation={assetAllocation}
          isUpdateAsset={isUpdateAsset}
          shouldOpenSelectProductPopup={this.state.shouldOpenSelectProductPopup}
          product={product}
          handleSelectProduct={this.handleSelectProduct}
          handleSelectProductPopupClose={this.handleSelectProductPopupClose}
          productTypeCode={productTypeCode}
          shouldOpenPopupAssetFile={shouldOpenPopupAssetFile}
          handleAssetFilePopupClose={this.handleAssetFilePopupClose}
          itemAssetDocument={itemAssetDocument}
          getAssetDocument={this.getAssetDocument}
          handleUpdateAssetDocument={this.handleUpdateAssetDocument}
          documentType={documentType}
          item={item}
          shouldOpenDialogMedicalEquipment={shouldOpenDialogMedicalEquipment}
          selectMedicalEquipment={this.selectMedicalEquipment}
          shouldOpenDialogShoppingForm={shouldOpenDialogShoppingForm}
          selectShoppingForm={this.selectShoppingForm}
          shouldOpenSelectAssetGroupPopup={shouldOpenSelectAssetGroupPopup}
          handleChangeSelectContract={this.handleChangeSelectContract}
          handleSelectDVBH={this.handleSelectDVBH}
          assetId={this.props.assetId}
          isSupply={this.state.isSupply}
          departmentSearchObject={departmentSearchObject}
          openAssetAccessoryDetails={this.openAssetAccessoryDetails}
          closeAssetAccessoryDetails={this.closeAssetAccessoryDetails}
          shouldOpenPopupAssetAccessoryDetails={this.state.shouldOpenPopupAssetAccessoryDetails}
          setListSpareparts={this.setListSpareparts}
          isView={isView}
          handleAddAttribute={this.handleAddAttribute}
          handleSelectAttributePopupClose={this.handleSelectAttributePopupClose}
        />

        {this.state.shouldOpenDialogProduct && (
          <ProductDialog
            t={t}
            i18n={this.props.i18n}
            handleClose={this.handleDialogClose}
            open={this.state.shouldOpenDialogProduct}
            handleOKEditClose={this.handleOKEditClose}
            item={this.state.itemProduct}
            type="addFromRateIncident"
            selectProduct={this.handleSelectLinhKien}
            indexLinhKien={this.state.indexLinhKien}
          />
        )}
      </Dialog>
    );
  }
}

AssetEditorDialogNew.contextType = AppContext;
export default withStyles(styles)(AssetEditorDialogNew);
