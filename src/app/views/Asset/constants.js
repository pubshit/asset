import { TextValidator } from "react-material-ui-form-validator";
import { appConst, variable } from "../../appConst";
import React from "react";
import { Button, FormControlLabel, Radio } from "@material-ui/core";
import moment from "moment";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import {
    NumberFormatCustom,
    convertNumberPrice,
    filterOptions,
    handleKeyDownIntegerOnly, handleKeyDownFloatOnly, convertMoney, getOptionSelected,
} from "app/appFunction";
import { Autocomplete } from "@material-ui/lab";

export const columns_AssetFile = (t) => [
  {
    title: t("general.stt"),
    field: "code",
    maxWidth: "50px",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("AssetFile.code"),
    field: "code",
    align: "left",
    maxWidth: 80,
  },
  {
    title: t("AssetFile.name"),
    field: "name",
    align: "left",
    minWidth: "250px",
  },
  {
    title: t("AssetFile.description"),
    field: "description",
    align: "left",
    width: "250px",
  },
];

export const columns_Attributes = (t, props, isView = false) => [
  {
    title: t("general.stt"),
    field: "code",
    maxWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("ProductAttribute.code"),
    field: "attribute.code",
    align: "left",
    maxWidth: 120,
    render: (rowData) => rowData?.attribute?.code,
  },
  {
    title: t("ProductAttribute.name"),
    field: "attribute.name",
    align: "left",
    maxWidth: 350,
    render: (rowData) => rowData?.attribute?.name,
  },
  {
    title: t("ProductAttribute.valueText"),
    field: "valueText",
    render: (rowData) => (
      <TextValidator
        isTabInstrumentToolProperties={true}
        className="w-100"
        onKeyDown={(e) => {
          variable.regex.decimalNumberExceptThisSymbols.includes(e.key) &&
            e.preventDefault();
        }}
        onChange={(valueText) =>
          props.handleRowDataCellChange(rowData, valueText)
        }
        type="text"
        name="valueText"
        field={t("ProductAttribute.valueText")}
        value={rowData?.valueText || ""}
        disabled={isView}
        validators={["required"]}
        errorMessages={[t("general.required")]}
      />
    ),
  },
];

export const columns_AssetSource = (
  t,
  props,
  item,
  handleSelectAssetMain,
  isView = false
) => [
  {
    title: t("general.stt"),
    field: "code",
    maxWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("AssetSource.code"),
    field: "assetSource.code",
    align: "left",
    minWidth: 80,
    maxWidth: 100,
    render: (rowData) =>
      rowData?.assetSourceCode || rowData?.code || rowData?.assetSource?.code,
  },
  {
    title: t("AssetSource.name"),
    field: "assetSource.name",
    align: "left",
    minWidth: 300,
    maxWidth: 350,
    render: (rowData) =>
      rowData?.assetSourceName || rowData?.name || rowData?.assetSource?.name,
  },
  {
    title: t("Asset.MainSourceOfCapital"),
    field: "assetSource.name",
    align: "left",
    minWidth: 150,
    maxWidth: 150,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      return (
        <>
          <Radio
            name="radSelected"
            value={rowData.id}
            checked={
              item?.isMainAssetSource || rowData?.isMainAssetSource === true
            }
            onClick={(event) => handleSelectAssetMain(event, rowData)}
            disabled={isView}
          />
          <div style={{ color: "red" }}>{rowData?.nameOriginHI}</div>
        </>
      );
    },
  },
  {
    title: t("AssetSource.percentage"),
    field: "percentage",
    minWidth: 80,
    maxWidth: 100,
    render: (rowData) => (
      <TextValidator
        className={`w-100 isTabOriginTools inputPrice`}
        onChange={(e) => props.handleRowDataCellChangeAssetSource(rowData, e)}
        onKeyDown={handleKeyDownIntegerOnly}
        type="text"
        name="percentage"
        value={rowData?.percentage || ""}
        validators={["minNumber: 1", "maxNumber:100"]}
        errorMessages={[
          "Không được nhập nhỏ hơn 0",
          "Không được nhập lớn hơn 100",
        ]}
        disabled={isView || item?.assetSources?.length === 1}
      />
    ),
  },
  {
    title: t("AssetSource.value"),
    field: "value",
    align: "right",
    minWidth: 100,
    maxWidth: 120,
    render: (rowData) => (
      <TextValidator
        className={`w-100 isTabOriginTools inputPrice`}
        onChange={(valueText) =>
          props.handleRowDataCellChangeAssetSource(rowData, valueText)
        }
        onKeyDown={handleKeyDownFloatOnly}
        type="text"
        name="value"
        InputProps={{
          inputComponent: NumberFormatCustom,
        }}
        value={rowData?.value || ""}
        disabled={isView}
      />
    ),
  },
];

export const columns_AssetTable = (t, convertNumberPriceRoundUp, stt) => [
  {
    title: t("Asset.stt"),
    field: "code",
    sorting: false,
    minWidth: "40px",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => stt + (rowData.tableData.id + 1),
  },
  {
    title: t("Asset.managementCode"),
    field: "managementCode",
    align: "left",
    sorting: false,
    minWidth: "140px",
  },
  {
    title: t("Asset.code"),
    field: "code",
    align: "left",
    sorting: false,
    minWidth: "110px",
  },
  {
    title: t("Asset.name"),
    field: "name",
    align: "left",
    sorting: false,
    minWidth: "220px",
  },
  {
    title: t("Product.stockKeepingUnit"),
    field: "unit.name",
    align: "left",
    sorting: false,
    minWidth: "90px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.receive_date"),
    field: "name",
    align: "left",
    sorting: false,
    minWidth: "150px",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) =>
      rowData?.dateOfReception
        ? moment(rowData?.dateOfReception).format("DD/MM/YYYY")
        : null,
  },
  {
    title: t("Asset.yearIntoUseTable"),
    field: "yearPutIntoUse",
    sorting: false,
    align: "left",
    minWidth: "120px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.model"),
    field: "model",
    align: "left",
    sorting: false,
    minWidth: "150px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.decisionCode"),
    field: "decisionCode",
    align: "left",
    sorting: false,
    minWidth: "150px",
    cellStyle: {
      textAlign: "left",
    },
  },
  {
    title: t("Asset.symbolCode"),
    field: "symbolCode",
    align: "left",
    sorting: false,
    minWidth: "130px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.serialNumber"),
    field: "serialNumber",
    align: "left",
    sorting: false,
    minWidth: "130px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.yearOfManufactureTable"),
    field: "yearOfManufacture",
    align: "left",
    sorting: false,
    minWidth: 60,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.manufacturerTable"),
    field: "manufacturer.name",
    align: "left",
    sorting: false,
    minWidth: 200,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.BHYTcode"),
    field: "bhytMaMay",
    align: "left",
    sorting: false,
    minWidth: 150,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.madeIn"),
    field: "madeIn",
    align: "left",
    sorting: false,
    minWidth: 130,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.supplier"),
    field: "supplyUnit.name",
    align: "left",
    sorting: false,
    minWidth: 220,
    cellStyle: {
      textAlign: "left",
    },
  },
  {
    title: t("Asset.lotNumber"),
    field: "lotNumber",
    align: "left",
    sorting: false,
    minWidth: 130,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.invoiceDate"), 
    field: "ngayHoaDon",
    align: "left",
    sorting: false,
    minWidth: 130,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => 
      rowData?.ngayHoaDon 
      ? moment(rowData?.ngayHoaDon).format("DD/MM/YYYY")
      : null
  },
  {
    title: t("Asset.originalCost"),
    field: "originalCost",
    sorting: false,
    minWidth: "150px",
    align: "left",
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) =>
      rowData?.originalCost
        ? convertNumberPriceRoundUp(rowData?.originalCost)
        : 0,
  },
  {
    title: t("Asset.depreciationRateTable"),
    field: "depreciationRate",
    sorting: false,
    minWidth: 80,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.carryingAmountTable"),
    field: "carryingAmount",
    align: "left",
    width: "120",
    sorting: false,
    minWidth: "120px",
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) =>
      rowData?.carryingAmount
        ? convertNumberPriceRoundUp(rowData?.carryingAmount)
        : 0,
  },
  {
    title: t("Asset.status"),
    field: "status.name",
    align: "left",
    width: "120",
    sorting: false,
    minWidth: "150px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.store"),
    field: "store.name",
    align: "left",
    width: "120",
    sorting: false,
    minWidth: "150px",
  },
  {
    title: t("Asset.useDepartment"),
    field: "useDepartment.name",
    align: "left",
    sorting: false,
    minWidth: "250px",
  },
];

export const column_SparePart = (t, props, isView) => [
  {
    title: t("general.stt"),
    field: "",
    width: "50px",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: (
      <span>
        {props?.item?.assetAccessories?.length > 0 && (
          <span className="colorRed">*</span>
        )}
        {t("SparePart.sparePart")}
      </span>
    ),
    field: "product",
    align: "left",
    minWidth: "200px",
    render: (rowData) => (
      <Autocomplete
        id="combo-box"
        fullWidth
        size="small"
        options={props?.item?.listProductOrg}
        value={rowData?.product ? rowData?.product : null}
        onChange={(e, value) =>
          props.handleSelectLinhKien(
            value,
            rowData?.tableData?.id,
            variable.listInputName.product
          )
        }
        getOptionSelected={getOptionSelected}
        getOptionLabel={(option) => {
          return option?.name || "";
        }}
        filterOptions={(options, params) =>
          filterOptions(options, params, true, "name")
        }
        noOptionsText={t("general.noOption")}
        renderInput={(params) => (
          <TextValidator
            {...params}
            className="w-100 isTabSparePart"
            variant="standard"
            onChange={(e) =>
              props.handleSearchListLinhKien(e, rowData?.tableData?.id)
            }
            field={t("SparePart.sparePart")}
            value={rowData?.product?.name || ""}
            onBlur={() => {
              props.debouncedSetSearchTerm("");
            }}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
        )}
        disabled={isView}
      />
    ),
  },
  {
    title: t("SparePart.unit"),
    field: "unit",
    align: "center",
    minWidth: 120,
    render: (rowData) => rowData?.unit?.name,
  },
  {
    title: (
      <span>
        {props?.item?.assetAccessories?.length > 0 && (
          <span className="colorRed">*</span>
        )}
        {t("Asset.originQuantity")}
      </span>
    ),
    field: "originQuantity",
    align: "left",
    minWidth: 80,
    render: (rowData) => (
      <TextValidator
        className="w-100 isTabSparePart"
        variant="standard"
        type="number"
        name="originQuantity"
        InputProps={{
          readOnly: isView,
          inputProps: {
            className: "text-center",
          },
        }}
        field={t("Asset.originQuantity")}
        value={rowData?.originQuantity ? rowData?.originQuantity : 0}
        size="small"
        InputLabelProps={{
          shrink: true,
        }}
        onKeyDown={(e) => {
          variable.regex.numberExceptThisSymbols.includes(e?.key) &&
            e.preventDefault();
        }}
        onChange={(e) =>
          props.handleChangeInputLinhKien(e, rowData?.tableData?.id)
        }
        disabled={
          !props?.assetStatus?.isNewInStore || rowData?.brokenQuantity > 0
        }
        validators={["required", "minNumber:1"]}
        errorMessages={[t("general.required"), t("general.minNumberError")]}
      />
    ),
  },
  {
    title: t("Asset.brokenQuantity"),
    field: "brokenQuantity",
    align: "left",
    minWidth: 80,
    render: (rowData) => (
      <TextValidator
        className="w-100 isTabSparePart"
        variant="standard"
        type="number"
        name="brokenQuantity"
        InputProps={{
          readOnly: isView,
          inputProps: {
            className: "text-center",
          },
        }}
        value={
          !isNaN(rowData?.brokenQuantity || "") ? rowData?.brokenQuantity : 0
        }
        size="small"
        placeholder=""
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) =>
          props.handleChangeInputLinhKien(e, rowData?.tableData?.id)
        }
        disabled
      />
    ),
  },
  {
    title: t("Asset.usingQuantity"),
    field: "usingQuantity",
    align: "left",
    minWidth: 80,
    render: (rowData) => (
      <TextValidator
        className="w-100 isTabSparePart"
        variant="standard"
        type="number"
        name="usingQuantity"
        InputProps={{
          readOnly: isView,
          inputProps: {
            className: "text-center",
          },
        }}
        value={
          !isNaN(rowData?.usingQuantity || "") ? rowData?.usingQuantity : 0
        }
        size="small"
        placeholder=""
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) =>
          props.handleChangeInputLinhKien(e, rowData?.tableData?.id)
        }
        disabled
      />
    ),
  },
  {
    title: t("SparePart.description"),
    field: "description",
    align: "left",
    width: "150px",
    minWidth: "150px",
    render: (rowData) => (
      <TextValidator
        className="w-100 isTabSparePart"
        variant="standard"
        type="text"
        name="description"
        InputProps={{
          readOnly: isView,
        }}
        value={rowData?.description ?? ""}
        size="small"
        placeholder=""
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) =>
          props.handleChangeInputLinhKien(
            e,
            rowData?.tableData?.id,
            "description"
          )
        }
        disabled={isView}
      />
    ),
  },
];
