import React, { useEffect, useMemo } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Typography,
  Tabs,
  Box,
  Tab,
  AppBar,
  Checkbox,
  Button,
  Grid,
  FormControlLabel,
  IconButton,
  Icon,
  FormControl,
  RadioGroup,
  Radio,
} from "@material-ui/core";
import QRCode from "qrcode.react";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import SelectProductPopup from "../Component/Product/SelectProductPopup";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import SelectManagementDepartmentPopup from "../Component/Department/SelectManagementDepartmentPopup";
import SelectMultiAssetSourcePopup from "./ComponentPopups/SelectMultiAssetSourcePopup";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { searchByPage as statusSearchByPage } from "../AssetStatus/AssetStatusService";
import { searchByPage as storesSearchByPage } from "../Store/StoreService";
import MaterialTable, { MTableToolbar } from "material-table";
import SelectSupplyPopup from "../Component/SupplyUnit/SelectSupplyUnitPopup";
import SelectProductAttributespopup from "../Component/ProductAttribute/SelectProductAttributespopup";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import NumberFormat from "react-number-format";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AssetQRPrint from "./ComponentPopups/AssetQRPrint";
import AssetAllocationEditorDialog from "../AssetAllocation/AssetAllocationEditorDialog";
import {
  getManufacturer,
  getMedicalEquipment,
  getShoppingForm,
  uploadImageAsset,
} from "./AssetService";
import { getStockKeepingUnit } from "../StockKeepingUnit/StockKeepingUnitService";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { appConst } from "app/appConst";
import SelectAssetGroupPopup from "../Component/AssetGroup/SelectAssetGroupPopup";
import StoreEditorDialog from "../Store/StoreEditorDialog";
import CommonObjectDialog from "../CommonObject/CommonObjectDialog";
import MedicalEquipmentTypeDialog from "../MedicalEquipmentType/MedicalEquipmentTypeDialog";
import ShoppingFormDialog from "../ShoppingForm/ShoppingFormDialog";
import {
  checkInvalidDate,
  filterOptions,
  isSpecialCharacters,
} from "app/appFunction";
import StockKeepingUnitEditorDialog from "../StockKeepingUnit/StockKeepingUnitEditorDialog";
// import localStorageService from "app/services/localStorageService";
// import appConfig from "app/appConfig";
import UploadCropImagePopup from "../page-layouts/UploadCropImagePopup";
import ConstantList from "../../appConfig";
import { toast } from "react-toastify";
import InfoLeft from "./Component/InfoLeft";
import InfoRight from "./Component/InfoRight";

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: "-10px",
    left: "-25px",
    width: "80px",
  },
}))(Tooltip);

// function MaterialButton(props) {
//   const { t } = props;
//   return (
//     <LightTooltip
//       title={t("Asset.reload_code")}
//       placement="top"
//       enterDelay={300}
//       leaveDelay={200}
//     >
//       <IconButton onClick={() => props.refreshCode()}>
//         <Icon color="primary">refresh</Icon>
//       </IconButton>
//     </LightTooltip>
//   );
// }

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
  },
  w30: {
    width: "30%",
  },
  w40: {
    width: "40% !important",
  },
  w65: {
    width: "65%",
  },
}));

// const useSessionItem = (key) => {
//   const [value, setValue] = useState(() =>
//     localStorageService.getSessionItem(key)
//   );
//   useEffect(() => {
//     setValue(localStorageService.getSessionItem(key));
//   }, [key]);
//   return value;
// };

const CustomAppBar = ({ value, handleChangeValue, t }) => {
  return (
    <AppBar position="static" color="default">
      <Tabs
        value={value}
        onChange={handleChangeValue}
        variant="scrollable"
        scrollButtons="on"
        indicatorColor="primary"
        textColor="primary"
        aria-label="scrollable force tabs example"
      >
        <Tab label={t("Asset.tab_info_asset")} type="submit" />
        <Tab label={t("Asset.tab_depreciation_information")} type="submit" />
        <Tab label={t("Asset.tab_attribute")} />
        <Tab label={t("Asset.tab_asset_source_file")} />
        <Tab label={t("Asset.tab_asset_file")} />
      </Tabs>
    </AppBar>
  );
};

const API_PATH = ConstantList.API_ENPOINT;

export default function ScrollableTabsButtonForce(props) {
  const {
    openSelectProductPopup,
    t,
    i18n,
    handleChange,
    item,
    isQRCode,
    handleSetDataSelect,
    selectUnit,
    openSelectAssetGroupPopup,
    selectStores,
    selectStatus,
    openSelectDepartmentPopup,
    handleSelectManagementDepartment,
    handleSelectDepartmentPopupClose,
    openSelectUseDepartmentPopup,
    isAddAsset,
    handleSelectUseDepartment,
    handleSelectUseDepartmentPopupClose,
    warrantyMonth,
    openSelectSupplyPopup,
    handleSelectSupply,
    handleSelectSupplyPopupClose,
    handleClose,
    selectShoppingForm,
    selectMedicalEquipment,
    handleSaveImageAsset,
  } = props;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [openPrintQR, setValueOpenPrintQR] = React.useState(false);
  const [shouldOpenImageDialog, setshouldOpenImageDialog] =
    React.useState(false);
  const [selectedImage, setSelectedImage] = React.useState(null);
  // const currentUser = useSessionItem("currentUser");

  const rowStyle = useMemo(
    () => ({
      backgroundColor: (rowData) =>
        rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
    }),
    []
  );

  const searchObjectStore = useMemo(
    () => ({ pageIndex: 1, pageSize: 1000000, type: 1 }),
    []
  ); // kho tài sản
  const searchObjectStatus = useMemo(
    () => ({ pageIndex: 1, pageSize: 1000000, statusIndexOrders: [0, 2] }),
    []
  );
  const searchObjectUnit = useMemo(
    () => ({ pageIndex: 1, pageSize: 1000000 }),
    []
  );

  const handleChangeValueOpenQR = (newValue) => {
    setValueOpenPrintQR(newValue);
  };

  const handleChangeValue = (event, newValue) => {
    setValue(newValue);
  };

  const filterAutocomplete = createFilterOptions();

  const allocationForValue = { department: 1, person: 2 };
  const BuyLocally = { domestic: 1, foreign: 2 };

  let riskClassification =
    appConst.listRisk.find(
      (risk) => risk.value === props?.item?.riskClassification
    ) || "";
  // let biddingPackage =
  //   appConst.subGroups.find(
  //     (risk) => risk.value === props?.item?.biddingPackage
  //   ) || null;
  let qrInfo =
    `Mã TS: ${props.item?.code}\n` +
    `Mã QL: ${props.item?.managementCode}\n` +
    `Tên TS: ${props.item?.product?.name}\n` +
    `Nước SX: ${props.item?.madeIn}\n` +
    `Năm SX: ${props.item?.yearOfManufacture}\n` +
    `Ngày SD: ${props.item?.yearPutIntoUse}\n` +
    `PBSD: ${props.item?.useDepartment?.name}\n`;

  let columnsAssetFile = [
    {
      title: t("general.stt"),
      field: "code",
      width: "50px",
      align: "center",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "10px",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      width: "200px",
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      width: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      width: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      width: "150px",
      render: (rowData) => (
        <div className="none_wrap">
          <LightTooltip
            title={t("general.editIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() =>
                props.handleRowDataCellDeleteAssetFile(rowData?.id)
              }
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
  ];

  let columnsAttributes = [
    {
      title: t("general.stt"),
      field: "code",
      width: "50px",
      align: "center",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "10px",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("ProductAttribute.code"),
      field: "attribute.code",
      align: "left",
      width: "200px",
    },
    {
      title: t("ProductAttribute.name"),
      field: "attribute.name",
      align: "left",
      width: "350px",
    },
    {
      title: t("ProductAttribute.valueText"),
      field: "valueText",
      render: (rowData) => (
        <TextValidator
          className="w-100"
          onKeyDown={(e) =>
            appConst?.numberExceptThisSymbols.includes(e.key) &&
            e.preventDefault()
          }
          onChange={(valueText) =>
            props.handleRowDataCellChange(rowData, valueText)
          }
          type="number"
          name="valueText"
          value={rowData?.valueText || ""}
        />
      ),
    },
    {
      title: t("general.action"),
      field: "valueText",
      width: "150px",
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.handleRowDataCellDelete(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
  ];

  let columnsAssetSource = [
    {
      title: t("general.stt"),
      field: "code",
      width: "50px",
      align: "center",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "10px",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetSource.code"),
      field: "assetSource.code",
      align: "left",
      width: "200",
    },
    {
      title: t("AssetSource.name"),
      field: "assetSource.name",
      align: "left",
      width: "350",
    },
    {
      title: t("AssetSource.percentage"),
      field: "percentage",
      render: (rowData) => (
        <TextValidator
          className="w-100"
          onKeyDown={(e) =>
            appConst?.numberExceptThisSymbols.includes(e.key) &&
            e.preventDefault()
          }
          onChange={(valueText) =>
            props.handleRowDataCellChangeAssetSource(rowData, valueText)
          }
          type="number"
          name="percentage"
          value={rowData?.percentage || ""}
          validators={["minNumber: 1", "maxNumber:100"]}
          errorMessages={[
            "Không được nhập nhỏ hơn 0",
            "Không được nhập lớn hơn 100",
          ]}
        />
      ),
    },
    {
      title: t("AssetSource.value"),
      field: "value",
      render: (rowData) => (
        <TextValidator
          className="w-100"
          onKeyDown={(e) =>
            appConst?.numberExceptThisSymbols.includes(e.key) &&
            e.preventDefault()
          }
          onChange={(valueText) =>
            props.handleRowDataCellChangeAssetSource(rowData, valueText)
          }
          type="number"
          name="value"
          value={rowData?.value || ""}
        />
      ),
    },
    {
      title: t("general.action"),
      field: "",
      width: "150",
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.handleRowDataCellDeleteAssetSource(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
  ];

  const handleSubmit = (event) => {};
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  };

  useEffect(() => {
    const isManageAccountant = props.item && props.item.isManageAccountant;
    ValidatorForm.addValidationRule("isSpecialCharacters", isSpecialCharacters);
    ValidatorForm.addValidationRule("isManageAccountant", (value) => {
      return isManageAccountant ? (value || value === 0 ? true : false) : true;
    });
  }, [props?.item]);

  // const isCheckRole = () => {
  //   const { roles } = currentUser;
  //   const { ROLE_ORG_ADMIN, ROLE_ASSET_MANAGER } = appConfig.ROLES;
  //   const isAdminAndAssetManager = roles.some(
  //     (role) =>
  //       role.authority === ROLE_ORG_ADMIN && role.name === ROLE_ASSET_MANAGER
  //   );
  //   const isAdminOnly = roles.some((role) => role.authority === ROLE_ORG_ADMIN);
  //   const isAssetManagerOnly = roles.some(
  //     (role) => role.name === ROLE_ASSET_MANAGER
  //   );
  //   if (isAdminAndAssetManager || isAdminOnly) {
  //     return false;
  //   } else if (isAssetManagerOnly) {
  //     return true;
  //   }
  //   return false;
  // };

  const handleDialogClose = () => {
    setshouldOpenImageDialog(false);
  };

  const handleUpdate = async (value) => {
    let formData = new FormData();
    formData.append("uploadfile", value);
    try {
      const { data } = await uploadImageAsset(props?.item?.id, formData);
      if (data.code === 200) {
        setSelectedImage(`${API_PATH}/${data?.data}`);
        setshouldOpenImageDialog(false);
      } else {
        toast.warn(data.message);
      }
    } catch (error) {
      console.error("Error", error);
      setshouldOpenImageDialog(false);
    }
  };

  const handleOpenUploadDialog = () => {
    setshouldOpenImageDialog(true);
  };

  return (
    <form
      className={classes.root}
      value={value}
      index={0}
      onSubmit={(event) => handleSubmit(event)}
    >
      <CustomAppBar value={value} handleChangeValue={handleChangeValue} t={t} />
      <TabPanel value={value} index={0} style={{ height: "570px" }}>
        <Grid container spacing={2}>
          <Grid md={6} sm={12} xs={12}>
            <InfoLeft
              item={item}
              isQRCode={isQRCode}
              selectUnit={selectUnit}
              isAddAsset={isAddAsset}
              onClose={handleClose}
              selectStores={selectStores}
              selectStatus={selectStatus}
              onChange={handleChange}
              warrantyMonth={warrantyMonth}
              onSelectSupply={handleSelectSupply}
              selectShoppingForm={selectShoppingForm}
              onSetDataSelect={handleSetDataSelect}
              openSelectSupplyPopup={openSelectSupplyPopup}
              openSelectProductPopup={openSelectProductPopup}
              selectMedicalEquipment={selectMedicalEquipment}
              onSelectUseDepartment={handleSelectUseDepartment}
              openSelectAssetGroupPopup={openSelectAssetGroupPopup}
              openSelectDepartmentPopup={openSelectDepartmentPopup}
              openSelectUseDepartmentPopup={openSelectUseDepartmentPopup}
              onSelectSupplyPopupClose={handleSelectSupplyPopupClose}
              onSelectManagementDepartment={handleSelectManagementDepartment}
              onSelectDepartmentPopupClose={handleSelectDepartmentPopupClose}
              onSelectUseDepartmentPopupClose={
                handleSelectUseDepartmentPopupClose
              }
              onDateChange={props.handleDateChange}
              yearPutIntoUse={props.yearPutIntoUse}
              selectManufacturer={props.selectManufacturer}
              onChangeSelect={props.handleChangeSelect}
              isDisableQuantity={props.isDisableQuantity}
            />
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <InfoRight
              item={item}
              isQRCode={isQRCode}
              selectUnit={selectUnit}
              isAddAsset={isAddAsset}
              onClose={handleClose}
              selectStores={selectStores}
              selectStatus={selectStatus}
              onChange={handleChange}
              warrantyMonth={warrantyMonth}
              onSelectSupply={handleSelectSupply}
              selectShoppingForm={selectShoppingForm}
              onSetDataSelect={handleSetDataSelect}
              openSelectSupplyPopup={openSelectSupplyPopup}
              openSelectProductPopup={openSelectProductPopup}
              selectMedicalEquipment={selectMedicalEquipment}
              onSelectUseDepartment={handleSelectUseDepartment}
              openSelectAssetGroupPopup={openSelectAssetGroupPopup}
              openSelectDepartmentPopup={openSelectDepartmentPopup}
              openSelectUseDepartmentPopup={openSelectUseDepartmentPopup}
              onSelectSupplyPopupClose={handleSelectSupplyPopupClose}
              onSelectManagementDepartment={handleSelectManagementDepartment}
              onSelectDepartmentPopupClose={handleSelectDepartmentPopupClose}
              onSelectUseDepartmentPopupClose={
                handleSelectUseDepartmentPopupClose
              }
              onDateChange={props.handleDateChange}
              yearPutIntoUse={props.yearPutIntoUse}
              selectManufacturer={props.selectManufacturer}
              onChangeSelect={props.handleChangeSelect}
              onSaveImageAsset={handleSaveImageAsset}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel
        value={value}
        index={1}
        style={{ height: "450px", overflow: "hidden" }}
      >
        <Grid container spacing={1}>
          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Asset.originalCost")}
                </span>
              }
              onChange={props.handleChangeFormatNumber}
              name="originalCost"
              value={props.item.originalCost}
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
              validators={["required", "matchRegexp:^.{1,15}$"]}
              errorMessages={[
                t("general.required"),
                "Nguyên giá không được nhập quá 15 ký tự",
              ]}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12} className="mt-10"></Grid>

          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Asset.depreciationRate")}
                </span>
              }
              onKeyDown={(e) =>
                appConst?.numberExceptThisSymbols.includes(e.key) &&
                e.preventDefault()
              }
              onChange={props.handleChange}
              type="number"
              name="depreciationRate"
              value={props?.item?.depreciationRate}
              validators={[
                "isManageAccountant",
                "minNumber:0",
                "maxNumber:100",
              ]}
              errorMessages={[
                t("general.required"),
                "Tỷ lệ khấu hao không được nhỏ hơn 0",
                "Tỷ lệ khấu hao không được lớn hơn 100",
              ]}
            />
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={t("Asset.unitPrice")}
              onChange={props.handleChangeFormatNumber}
              name="unitPrice"
              value={props?.item?.unitPrice}
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
              validators={["matchRegexp:^.{1,15}$"]}
              errorMessages={["Đơn giá không được nhập quá 15 ký tự"]}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12} className="mt-10"></Grid>

          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <CustomValidatePicker
              margin="none"
              fullWidth
              id="date-picker-dialog mt-2"
              style={{ marginTop: "2px" }}
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Asset.depreciationDate")}
                </span>
              }
              inputVariant="standard"
              autoOk={true}
              maxDateMessage={t("EQAPlanning.messRegistrationStartDate")}
              format="dd/MM/yyyy"
              value={props.item.depreciationDate}
              onChange={(date) =>
                props.handleDateChange(date, "depreciationDate")
              }
              validators={["isManageAccountant"]}
              errorMessages={t("general.required")}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
              invalidDateMessage={t("general.invalidDateFormat")}
              minDate={
                props?.item?.isManageAccountant &&
                new Date(props?.item?.dateOfReception)
              }
              minDateMessage={
                props?.item?.isManageAccountant &&
                "Ngày bắt đầu tính khẩu hao không được nhỏ hơn ngày tiếp nhận"
              }
              onBlur={() =>
                handleBlurDate(props.item.depreciationDate, "depreciationDate")
              }
              clearable
              clearLabel={t("general.remove")}
              cancelLabel={t("general.cancel")}
              okLabel={t("general.select")}
            />
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={t("Asset.usedTime")}
              onKeyDown={(e) =>
                appConst?.numberExceptThisSymbols.includes(e.key) &&
                e.preventDefault()
              }
              onChange={props.handleChange}
              type="number"
              name="usedTime"
              value={props?.item?.usedTime}
              validators={["minNumber:0", "maxNumber:1000"]}
              errorMessages={[
                "Thời gian sử dụng không được nhỏ hơn 0",
                "Thời gian sử dụng không được lớn hơn 1000",
              ]}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12} className="mt-10"></Grid>

          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={t("Asset.accumulatedDepreciationAmount")}
              onChange={props.handleChange}
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
              name="accumulatedDepreciationAmount"
              value={props.item.accumulatedDepreciationAmount}
              disabled={true}
            />
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={t("Asset.depreciationPeriod")}
              onKeyDown={(e) =>
                appConst?.numberExceptThisSymbols.includes(e.key) &&
                e.preventDefault()
              }
              onChange={props.handleChange}
              type="number"
              name="depreciationPeriod"
              value={props?.item?.depreciationPeriod}
              validators={["minNumber:0", "maxNumber:1000"]}
              errorMessages={[
                "Thời gian tính khấu hao không được nhỏ hơn 0",
                "Thời gian tính khấu hao không được lớn hơn 1000",
              ]}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12} className="mt-10"></Grid>

          <Grid item md={5} sm={12} xs={12} className="mt-10">
            <TextValidator
              className="w-100"
              label={t("Asset.carryingAmount")}
              onChange={props.handleChangeFormatNumber}
              name="carryingAmount"
              value={props.item.carryingAmount}
              id="formatted-numberformat-carryingAmount"
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
              disabled={true}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2} style={{ height: "450px" }}>
        <Grid container>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={props.openPopupSelectAttribute}
            >
              {t("Asset.add_attribute")}
            </Button>
            {props.item.shouldOpenPopupSelectAttribute && (
              <SelectProductAttributespopup
                open={props.item.shouldOpenPopupSelectAttribute}
                handleSelect={props.handleAddAttribute}
                attributes={props.item.attributes || []}
                handleClose={props.handleSelectAttributePopupClose}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item.attributes || []}
              columns={columnsAttributes}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle,
                maxBodyHeight: "320px",
                minBodyHeight: "320px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={3} style={{ height: "450px" }}>
        <Grid container>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={props.openPopupSelectAssetSource}
            >
              {t("Asset.add_assetSource")}
            </Button>
            {props.item.shouldOpenPopupSelectAssetSource && (
              <SelectMultiAssetSourcePopup
                open={props.item.shouldOpenPopupSelectAssetSource}
                handleSelect={props.handleAddAssetSource}
                assetSources={props.item.assetSources || []}
                handleClose={props.handleSelectAssetSourcePopupClose}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item?.assetSources || []}
              columns={columnsAssetSource}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "343px",
                minBodyHeight: "343px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={4} style={{ height: "450px" }}>
        <Grid container>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item.documents || []}
              columns={columnsAssetFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "343px",
                minBodyHeight: "343px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <Grid>
        {props?.item?.shouldOpenAllocationEditorDialog && (
          <AssetAllocationEditorDialog
            t={t}
            i18n={i18n}
            open={props?.item?.shouldOpenAllocationEditorDialog}
            handleOKEditClose={props?.handleCloseAll}
            handleCloseAllocationEditorDialog={props?.handleClose}
            assetAllocation={props?.item?.assetAllocation || []}
            item={{}}
            isAllocation={true}
            isUpdateAsset={props?.item?.isUpdateAsset}
          />
        )}
      </Grid>

      {shouldOpenImageDialog && (
        <UploadCropImagePopup
          t={t}
          i18n={i18n}
          handleClose={handleDialogClose}
          handleUpdate={handleUpdate}
          open={shouldOpenImageDialog}
          // uploadUrl={API_PATH + "avatarUpload"}
          acceptType="png;jpg;gif;jpeg"
          type="IMAGE_ASSET"
        />
      )}

      {props.item.shouldOpenSelectProductPopup && (
        <SelectProductPopup
          open={props.item.shouldOpenSelectProductPopup}
          handleSelect={props.handleSelectProduct}
          selectedItem={props.item.product != null ? props.item.product : {}}
          handleClose={props.handleSelectProductPopupClose}
          fromAssetDialog={true}
          t={t}
          i18n={i18n}
          productTypeCode={
            props.item.productTypeCode != null
              ? props.item.productTypeCode
              : props.productTypeCode
          }
        />
      )}

      {props.item.shouldOpenPopupAssetFile && (
        <VoucherFilePopup
          open={props.item.shouldOpenPopupAssetFile}
          handleClose={props.handleAssetFilePopupClose}
          itemAssetDocument={props.itemAssetDocument}
          getAssetDocument={props.getAssetDocument}
          handleUpdateAssetDocument={props.handleUpdateAssetDocument}
          documentType={props.item?.documentType}
          item={props.item}
          t={t}
          i18n={i18n}
        />
      )}

      {props?.item?.shouldOpenDialogMedicalEquipment && (
        <MedicalEquipmentTypeDialog
          t={t}
          i18n={i18n}
          handleClose={props.handleClose}
          handleOKEditClose={props.handleClose}
          selectMedicalEquipment={props.selectMedicalEquipment}
          open={props?.item?.shouldOpenDialogMedicalEquipment}
          handleSetDataSelect={(data) =>
            props.handleSetDataSelect(data, "listMedicalEquipment")
          }
        />
      )}

      {props?.item?.shouldOpenDialogShoppingForm && (
        <ShoppingFormDialog
          t={t}
          i18n={i18n}
          handleClose={props.handleClose}
          handleOKEditClose={props.handleClose}
          open={props?.item?.shouldOpenDialogShoppingForm}
          selectShoppingForm={props?.selectShoppingForm}
          handleSetDataSelect={(data) =>
            props.handleSetDataSelect(data, "listShoppingForm")
          }
        />
      )}

      {props?.item?.shouldOpenDialogUnit && (
        <StockKeepingUnitEditorDialog
          t={t}
          i18n={i18n}
          handleClose={props.handleClose}
          handleOKEditClose={props.handleClose}
          open={props?.item?.shouldOpenDialogUnit}
          handleSetDataSelect={(data) =>
            props.handleSetDataSelect(data, "listUnit")
          }
        />
      )}
      {props.shouldOpenAssetAllocationEditorDialog && (
        <AssetAllocationEditorDialog
          t={t}
          i18n={i18n}
          handleClose={props.handleCloseAssetAllocationEditorDialog}
          open={props.shouldOpenAssetAllocationEditorDialog}
          handleOKEditClose={props.handleCloseAssetAllocationEditorDialog}
          item={{}}
        />
      )}

      {props.item.shouldOpenSelectAssetGroupPopup && (
        <SelectAssetGroupPopup
          t={t}
          open={props.item.shouldOpenSelectAssetGroupPopup}
          handleClose={props.handleSelectAssetGroupPopupClose}
          handleSelect={props.handleSelectAssetGroup}
          fromAssetDialog={true}
          i18n={i18n}
          selectedItem={
            props.item.assetGroup != null ? props.item.assetGroup : {}
          }
        />
      )}

      {props?.item?.shouldOpenDialogStore && (
        <StoreEditorDialog
          t={t}
          i18n={i18n}
          handleOKEditClose={props.handleClose}
          handleClose={props.handleClose}
          open={props?.item?.shouldOpenDialogStore}
          handleSetDataSelect={(data) =>
            props.handleSetDataSelect(data, "listStore")
          }
        />
      )}
    </form>
  );
}
