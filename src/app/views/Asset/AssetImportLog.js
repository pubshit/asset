const CheckLogReponse = (data) => {
    if(data.slice(17)=="error_Department"){
        return data.substring(0, 17) + "Phòng bàn giao không tồn tại hoặc trống";
    }
     else if(data.slice(17)=="error_UserDepartment"){
        return data.substring(0, 17)+ "Người bàn giao không thuộc bất kỳ phòng ban nào hoặc trống";
    }
     else if(data.slice(17) == "error_Receiver_Department"){
        return data.substring(0, 17)+"Phòng tiếp nhận không tồn tại hoặc trống";
    }
     else if(data.slice(17)== "error_Receiver_UserDepartment"){
        return data.substring(0, 17)+" Người tiếp nhận không thuộc bất kỳ phòng ban nào hoặc trống";
    } 
    else if(data.slice(17) == "error_Asset"){
        return data.substring(0, 17)+"Tài sản không thuộc bất kỳ phòng ban nào hoặc trống";
    } 
    else if(data.slice(17) == "error_Allocation_Status"){
        return data.substring(0, 17)+"Trạng thái cấp phát không tồn tại";
    } 
     else if(data.slice(17)== "error_Tranfer_Status"){
        return data.substring(0, 17)+"Trạng thái điều chuyển không tồn tại";
    } 
    else if(data.slice(17) == "error_FortmatTime"){
        return data.substring(0, 17)+"Lỗi định dạng ngày tháng hoặc trống";
    } 
    else if(data.slice(17) == "error_Liquidate_Status"){
        return data.substring(0, 17)+"Trạng thái thanh lý không tồn tại";
    } 
    else if(data.slice(17) == "error_Transfer_To_Another_Status"){
        return data.substring(0, 17)+"Trạng thái điều chuyển đơn vị khác không tồn tại";
    } 

    return "Import Successfully";
}
export default CheckLogReponse;