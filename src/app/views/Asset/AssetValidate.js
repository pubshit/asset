import {checkInvalidDate, compareDate, compareMoney, isCheckLenght} from "app/appFunction";
import moment from "moment";

export const validateSubmit = (state, setState, toast, props) => {
  let {
    product,
    assetGroup,
    name,
    managementDepartment,
    quantity,
    originalCost,
    depreciationRate,
    assetSources,
    dateOfReception,
    depreciationDate,
    managementCode,
    lotNumber,
    serialNumber,
    model,
    madeIn,
    installationLocation,
    note,
    yearOfManufacture,
    warrantyMonth,
    depreciationPeriod,
    usedTime,
    unitPrice,
    circulationNumber,
    warrantyExpiryDate,
    isManageAccountant,
    numberOfClones,
    serialNumbers,
  } = state;
  const { t } = props;

  let assetSourcesTotal = Math.ceil(
    assetSources?.reduce(
      (total, currentItem) => total + Number(currentItem.value),
      0
    )
  );

  if (!product) {
    toast.warn("Chưa chọn sản phẩm (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (checkInvalidDate(dateOfReception)) {
    toast.warn("Định dạng ngày tiếp nhận không hợp lệ (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (compareDate(dateOfReception, new Date()) > 0) {
    toast.warning(`Ngày tiếp nhận không được lớn hơn ngày hiện tại`);
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(managementCode, 255)) {
    toast.warn("Mã quản lý không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(lotNumber, 20)) {
    toast.warn("Số hóa đơn không nhập quá 20 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (!name) {
    toast.warn("Chưa chọn tên tài sản (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(name, 255)) {
    toast.warn("Tên tài sản không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (!assetGroup) {
    toast.warn("Chưa chọn danh mục tài sản (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (!managementDepartment) {
    toast.warn("Chưa chọn phòng ban quản lý (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (quantity < 0) {
    toast.warn("Số lượng không được nhỏ hơn 0 (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }

  if (!numberOfClones) {
    toast.warn("Chưa chọn số lượng nhân bản (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }

  if (numberOfClones < 1) {
    toast.warn("Số lượng nhân bản phải lớn hơn 0 (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }

  if (isCheckLenght(numberOfClones, 255)) {
    toast.warn("Số lượng nhân bản không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (warrantyMonth && (warrantyMonth < 0 || warrantyMonth > 1000)) {
    toast.warn(
      "Nhập lại số tháng bảo hành nằm trong khoảng 0 đến 1000 (THÔNG TIN CHUNG)."
    );
    toast.clearWaitingQueue();
    return false;
  }
  if (warrantyExpiryDate && compareDate(warrantyExpiryDate, "01/01/2100") > 0) {
    toast.warn(
      "Ngày hết hạn bảo hành không được lớn hơn ngày 01/01/2100 (THÔNG TIN CHUNG)."
    );
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(serialNumber, 255)) {
    toast.warn("Số serial không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(model, 255)) {
    toast.warn("Model không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(madeIn, 255)) {
    toast.warn("Xuất xứ không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(yearOfManufacture, 4)) {
    toast.warn("Năm sản xuất không hợp lệ (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (yearOfManufacture && yearOfManufacture < 1900) {
    toast.warn("Năm sản xuất không được nhỏ hơn năm 1900 (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (
    yearOfManufacture >
    (moment(dateOfReception).year() || moment(props?.itemAssetDocument?.issueDate).year())
  ) {
    toast.warn("Năm sản xuất không được lớn hơn năm tiếp nhận (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(installationLocation, 255)) {
    toast.warn("Vị trí lắp đặt không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(note, 255)) {
    toast.warn("Ghi chú không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(circulationNumber, 255)) {
    toast.warn("Sổ lưu hành không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
    toast.clearWaitingQueue();
    return false;
  }
  if (+originalCost < 0) {
    toast.warn("Chưa nhập nguyên giá.");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(originalCost, 15)) {
    toast.warn("Nguyên giá không được nhập quá 15 ký tự.");
    toast.clearWaitingQueue();
    return false;
  }
  if (isCheckLenght(originalCost, 255)) {
    toast.warn("Nguyên giá không nhập quá 255 ký tự.");
    toast.clearWaitingQueue();
    return false;
  }
  if (isManageAccountant && !depreciationRate && depreciationRate !== 0) {
    toast.warn("Chưa nhập tỷ lệ khấu hao.");
    toast.clearWaitingQueue();
    return false;
  }
  if (depreciationRate && depreciationRate < 0) {
    toast.warn("Tỷ lệ khấu hao không được nhỏ hơn 0.");
    toast.clearWaitingQueue();
    return false;
  }
  if (depreciationRate && depreciationRate > 100) {
    toast.warn("Tỷ lệ khấu hao nằm không được lớn hơn 100.");
    toast.clearWaitingQueue();
    return false;
  }
  // if (isCheckLenght(unitPrice, 15)) {
  //   toast.warn("Đơn giá không được nhập quá 15 ký tự.");
  //   toast.clearWaitingQueue();
  //   return false;
  // }
  if (checkInvalidDate(depreciationDate)) {
    toast.warn("Định dạng ngày bắt đầu tính khấu hao bổ không hợp lệ.");
    toast.clearWaitingQueue();
    return false;
  }
  if (
    isManageAccountant &&
    compareDate(depreciationDate || new Date(), Date.parse(dateOfReception)) < 0
  ) {
    toast.warn("Ngày bắt đầu tính khấu hao không được nhỏ hơn ngày tiếp nhận.");
    toast.clearWaitingQueue();
    return false;
  }
  if (usedTime && (usedTime < 0 || usedTime > 1000)) {
    toast.warn("Nhập lại thời gian sử dụng nằm trong khoảng 0 đến 1000.");
    toast.clearWaitingQueue();
    return false;
  }
  if (
    depreciationPeriod &&
    (depreciationPeriod < 0 || depreciationPeriod > 1000)
  ) {
    toast.warn("Nhập lại thời gian tính khấu hao nằm trong khoảng 0 đến 1000.");
    toast.clearWaitingQueue();
    return false;
  }
  if ((!assetSources || assetSources?.length === 0) && +originalCost > 0) {
    toast.warn("Chưa nhập nguồn vốn tài sản.");
    toast.clearWaitingQueue();
    return false;
  } else {
    if (+originalCost === 0) {return true;}
    let checkValueAssetSource = true;
    let sumPercentage = assetSources?.reduce(
      (a, b) => a + Number(b?.percentage),
      0
    );
    assetSources?.forEach((element) => {
      if (element?.value === null || element?.value === 0) {
        checkValueAssetSource = false;
      }
    });
    if (!checkValueAssetSource) {
      toast.warn("Chưa nhập giá trị nguồn vốn tài sản.");
      toast.clearWaitingQueue();
      return false;
    }
    if (!compareMoney(Number(originalCost), Number(assetSourcesTotal))) {
      toast.warn("Nguồn vốn tài sản không bằng nguyên giá.");
      toast.clearWaitingQueue();
      return false;
    }
    if (sumPercentage !== 100) {
      toast.warn("Nguồn vốn phải bằng 100%.");
      toast.clearWaitingQueue();
      return false;
    }
  }
  if (serialNumbers?.length >= 10 && !state?.quantityAsset) {
    toast.warning(t("Validator.ErrorSerialLength"));
    return false;
  }
  return true;
};
export const validateAllocationFor = (state, setState, toast) => {
  let {
    status,
    allocationFor,
    useDepartment,
    usePerson,
    assetStatusNew,
    serialNumbers,
  } = state;

  if (status?.indexOrder !== assetStatusNew) {
    if (allocationFor && allocationFor === 1) {
      // setState({ usePerson: null });
      // if (!useDepartment || !useDepartment.id) {
      //   toast.warning("Vui lòng chọn phòng ban sử dụng");
      //   toast.clearWaitingQueue();
      //   return false;
      // }
    } else if (allocationFor) {
      if (!usePerson || !usePerson.personId) {
        toast.warning("Vui lòng chọn người sử dụng");
        toast.clearWaitingQueue();
        return false;
      }
    }
  }
  return true;
};
