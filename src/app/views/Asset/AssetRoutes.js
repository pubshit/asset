import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import {  withTranslation } from 'react-i18next';
const AssetTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./AssetTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(AssetTable);

const AssetRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/page",
    exact: true,
    component: ViewComponent
  }
];

export default AssetRoutes;
