import React, { useContext } from "react";
import {
  AppBar,
  Button,
  ButtonGroup, Card,
  ClickAwayListener,
  FormControl,
  Grid,
  Icon,
  IconButton,
  Input,
  InputAdornment,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Tab,
  Tabs,
  TextField,
} from "@material-ui/core";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { MTableToolbar } from "material-table";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import {
  checkTTHK,
  deleteItem,
  exportExampleImportExcel,
  exportExcelAssetFollowService,
  exportExcelAssetCardService,
  exportExcelFileError,
  exportToExcel,
  exportToExcelQR,
  getItemById,
  getListOrgManagementDepartment,
  getMedicalEquipment,
  getNewCode,
  getTreeUser, importExcelAssetUrl,
  printAssetBookService,
  printAssetCard,
  searchByPage,
  exportExcelAssetBookService,
} from "./AssetService";
import { getTreeView as getAllDepartmentTreeView, searchByPageDepartmentNew } from "../Department/DepartmentService";
import { searchByPage as statusSearchByPage } from "../AssetStatus/AssetStatusService";
import { searchByPage as assetGroupSearchByPage } from "../AssetGroup/AssetGroupService";
import AssetEditorDialogNew from "./AssetEditorDialogNew";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import FileSaver, { saveAs } from "file-saver";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import SearchIcon from "@material-ui/icons/Search";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { Helmet } from "react-helmet";
import ViewAssetHistoryPopup from "../Component/ViewAssetHistoryPopup/ViewAssetHistoryPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import {
  OPTIONS_EXCEL_ASSET,
  STATUS_STORE,
  appConst,
  variable,
  OPTIONS_EXCEL_QR, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, DEFAULT_TOOLTIPS_PROPS
} from "app/appConst";
import {
  checkObject,
  convertNumberPriceRoundUp,
  filterOptions,
  getRole,
  getTheHighestRole,
  isSuccessfulResponse,
  getAssetStatus,
  handleKeyDown,
  handleKeyDownFloatOnly,
  handleKeyUp,
  getUserInformation,
  functionExportToExcel,
  formatDateDto,
  isValidDate,
  formatDateDtoMore,
  convertFromToDate,
  handleGetTemplatesByModel
} from "../../appFunction";
import { searchReceiverDepartment } from "../AssetAllocation/AssetAllocationService";
import CustomTablePagination from "../CustomTablePagination";
import { columns_AssetTable } from "./constants";
import CustomMaterialTable from "../CustomMaterialTable";
import TreeView from "@material-ui/lab/TreeView";
import { TreeItem } from "@material-ui/lab";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { searchByPage as storesSearchByPage } from "../Store/StoreService";
import Collapse from "@material-ui/core/Collapse";
import CardContent from "@material-ui/core/CardContent";
import { withRouter } from "react-router-dom";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import AssetCard from "../FormCustom/AssetCard";
import AssetsQRPrintAccording from "../Asset/ComponentPopups/AssetsQRPrintAccording";
import { searchByPage as searchByPageBidding } from "../bidding-list/BiddingListService";
import ValidatedDatePicker from "../Component/ValidatePicker/ValidatePicker";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import {LightTooltip, NumberFormatCustom} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { currentOrg } = useContext(AppContext);
  const { t } = useTranslation();
  const { isRoleOrgAdmin, isRoleAssetUser, isRoleUser, isRoleAssetManager } = getTheHighestRole();
  const { departmentUser } = getUserInformation();
  const item = props.item;
  const status = item?.status?.indexOrder;
  const hasDeletePermission = props.hasDeletePermission;
  const hasViewPermission = props.hasViewPermission;
  const isUsing = getAssetStatus(status).isUsing;
  const isDepartmentInUse = item?.useDepartment?.id !== departmentUser?.id;
  let hasEditPermission = props.hasEditPermission &&
    ![
      appConst.listStatusInstrumentTools.DA_THANH_LY.indexOrder,
      appConst.listStatusInstrumentTools.CHUYEN_DI.indexOrder,
    ].includes(status);
  let allowdAUEditAssetUsing = isUsing && isRoleAssetUser;
  let notAllowdAMEditAssetUsing = isUsing && !isDepartmentInUse && isRoleAssetManager;
  let hasPrintPermission = currentOrg?.code === LIST_ORGANIZATION.BVDK_MY_DUC.code

  if (allowdAUEditAssetUsing) {
    hasEditPermission = true
  }
  if (notAllowdAMEditAssetUsing) {
    hasEditPermission = false
  }
  if (isRoleOrgAdmin && currentOrg?.code === LIST_ORGANIZATION.BV_VAN_DINH.code) { //temporary
    hasEditPermission = true;
  }
  return (
    <div className="none_wrap">
      {(hasEditPermission) && (
        <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
            <Icon fontSize="small" color="primary">edit</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {hasDeletePermission &&
        getAssetStatus(item?.status?.indexOrder).isNewInStore
        && (
          <LightTooltip title={t("general.deleteIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        )}
      <LightTooltip title={t("general.view")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">visibility</Icon>
        </IconButton>
      </LightTooltip>
      {hasViewPermission && !isRoleUser && (
        <LightTooltip title={t("general.historyIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.history)}>
            <Icon fontSize="small" color="primary">history</Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip title={t("Asset.AssetCard")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.card)}>
          <Icon fontSize="small" color="primary">assignment_icon</Icon>
        </IconButton>
      </LightTooltip>
      {hasPrintPermission && <LightTooltip title={t("general.print")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.print)}>
          <Icon fontSize="small" color="primary">print</Icon>
        </IconButton>
      </LightTooltip>}
    </div>
  );
}

class AssetTable extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result?.data?.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
    this.anchorRef = React.createRef();
    this.anchorRefQR = React.createRef();
  }

  state = {
    keyword: "",
    status: [],
    assetGroup: null,
    rowsPerPage: 10,
    page: 0,
    Asset: [],
    item: {},
    managementDepartment: null,
    departmentId: "",
    departmentTreeView: {},
    shouldOpenEditorDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenPrintAssetBook: false,
    shouldOpenPrintAssetCard: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenAssetHistory: false,
    isAddAsset: false,
    isTemporary: null,
    listManageDepartment: [],
    isSearch: false,
    isPrintAssetFollow: false,
    isPrintAssetBook: false,
    currentUser: null,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasViewPermission: false,
    useDepartment: null,
    yearPutIntoUse: null,
    childrenOrg: null,
    isQRCode: true,
    isDisableQuantity: false,
    searchDepartmentObject: {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      orgId: getUserInformation().organization?.org?.id,
    },
    isButtonAllocation: true,
    isMedicalEquipment: false,
    tabValue: 0,
    isRoleAssetManager: false,
    isManagedByAssetManager: true,
    isUseByAssetManager: false,
    isRoleAccountant: false,
    isTTHK: true, //check thông tin khấu hao
    isView: false, //view,
    assetId: null,
    openExcel: false,
    treeList: null,
    usePersonId: null,
    isRoleAssetUser: false,
    itemPrintBookAsset: null,
    itemCard: null,
    fromDate: null,
    toDate: null,
    useDepartmentFilter: null,
    isDangSuDung: false,
    soNamKhConLai: null,
    medicalEquipment: null,
    openPrintQR: false,
    openPrintQRToggle: false,
    decisionCode: null,
    textSearchAdvance: "",
    year: null,
    shouldOpenPrintHistory: false,
    listUseDepartment: []
  };
  treeView = null;

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      itemList: [],
      tabValue: newValue,
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabAsset?.tabManagement === newValue) {
      this.setState(
        {
          isManagedByAssetManager: true,
          isUseByAssetManager: false,
          soNamKhConLai: null,
        },
        this.updatePageData
      );
    }
    else if (appConst?.tabAsset?.tabGrantedForUse === newValue) {
      this.setState(
        {
          isManagedByAssetManager: false,
          isUseByAssetManager: true,
          soNamKhConLai: null,
        },
        this.updatePageData
      );
    }
    else if (appConst?.tabAsset?.tabFullyDepreciated === newValue) {
      this.setState(
        {
          isManagedByAssetManager: null,
          isUseByAssetManager: null,
          soNamKhConLai: 0,
        },
        this.updatePageData
      );
    }
  };

  handleTextChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  setPage = (page) => {
    this.setState({ page: page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      useDepartment,
      yearPutIntoUse,
      childrenOrg,
      isManagedByAssetManager,
      isUseByAssetManager,
      usePersonId,
      store,
      originalCostFrom,
      originalCostTo,
      keyword,
      managementDepartment,
      isTemporary,
      isManageAccountant,
      statusIds,
      page,
      rowsPerPage,
      assetGroup,
      soNamKhConLai,
      medicalEquipment,
      dateOfReceptionBottom,
      dateOfReceptionTop,
      decisionCode,
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        managementDepartmentId: managementDepartment?.id,
        isTemporary: isTemporary?.value,
        isManageAccountant: isManageAccountant?.value,
        statusIds: statusIds?.toString(),
        pageIndex: page + 1,
        pageSize: rowsPerPage,
        assetGroupId: assetGroup?.id,
        keyword: keyword.trim(),
        soNamKhConLai,
        decisionCode: decisionCode?.decisionCode,
      };
      searchObject.medicalEquipmentId = medicalEquipment?.id;
      if (variable.listInputName.all !== this.state.departmentId) {
        searchObject.departmentId = this.state.departmentId;
      }
      searchObject.assetGroupId = this.state?.assetGroup
        ? this.state.assetGroup?.id
        : null;

      if (useDepartment) searchObject.useDepartmentId = useDepartment?.id;
      if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
      if (isValidDate(dateOfReceptionBottom)) searchObject.dateOfReceptionBottom = formatDateDtoMore(dateOfReceptionBottom, "start");
      if (isValidDate(dateOfReceptionTop)) searchObject.dateOfReceptionTop = formatDateDtoMore(dateOfReceptionTop);
      if (originalCostFrom) {
        searchObject.originalCostFrom = originalCostFrom;
      }
      if (originalCostTo) {
        searchObject.originalCostTo = originalCostTo;
      }
      if (childrenOrg) searchObject.orgId = childrenOrg?.id;
      if (isManagedByAssetManager)
        searchObject.isManagedByAssetManager = isManagedByAssetManager;
      if (isUseByAssetManager)
        searchObject.isUseByAssetManager = isUseByAssetManager;
      if (usePersonId) searchObject.usePersonId = usePersonId;
      if (store) {
        searchObject.storeId = this.state.store?.id;
      }

      let res = await searchByPage(searchObject)
      const { data, code, message } = res?.data;
      if (isSuccessfulResponse(code)) {
        if (data?.content?.length <= 0 && data?.number) {
          setPageLoading(true);
          this.setPage(0);
          return;
        }
        this.setState({
          itemList: data?.content?.length > 0
            ? [...data?.content]
            : [],
          totalElements: data?.totalElements,
        });
        setPageLoading(false);
      }
      else {
        toast.warning(message);
        setPageLoading(false);
      }
    }
    catch (err) {
      toast.error(t("toastr.error"));
      setPageLoading(false);
    }
    finally {
    }
  };

  convertSearchParams = (searchObject) => {
    let params = {};

    for (const [key, value] of Object.entries(searchObject)) {
      if (Array.isArray(value)) {
        params[key] = value.toString();
      } else { // lấy cả những value bằng false
        params[key] = value;
      }
    }

    return params;
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenPrintAssetBook: false,
      shouldOpenPrintAssetCard: false,
      shouldOpenAssetHistory: false,
      shouldOpenPrintHistory: false,
      isTTHK: true,
      item: {}
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenAssetHistory: false,
      shouldOpenImportExcelDialog: false,
    });
    this.updatePageData();
  };

  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    deleteItem(this.state?.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          } else {
            this.updatePageData();
          }
          toast.info(t(data?.message));
        } else {
          toast.warning(t(data?.message));
        }
        this.handleDialogClose();
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    let { location, history } = this.props;
    if (location.state?.tabValue) {
      this.handleChangeTabValue({}, location.state?.tabValue);
      history.push(location?.pathname, null);
    } else {
      this.updatePageData();
    }
    this.getRoleCurrentUser();
    if (getRole().isRoleAssetUser) {
      this.getTreeView();
    }
    this.handleGetDocumentTemplates();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission
    } = this.state;
    let roles = getTheHighestRole();
    let {
      currentUser,
      isRoleAdmin,
      isRoleOrgAdmin,
      isRoleAccountant,
      isRoleAssetManager,
      isRoleUser,
      isRoleAssetUser,
      departmentUser,
    } = roles;
    if (currentUser) {
      //user
      if (isRoleUser) {
        if (!hasViewPermission) {
          hasViewPermission = true;
        }
      }
      // người quản lý vật tư
      if (isRoleAssetManager) {
        if (!hasDeletePermission) {
          hasDeletePermission = true;
        }
        if (!hasEditPermission) {
          hasEditPermission = true;
        }
        if (!hasViewPermission) {
          hasViewPermission = true;
        }
      }
      // người đại diện phòng ban
      if (isRoleAssetUser) {
        if (!hasViewPermission) {
          hasViewPermission = true;
          isRoleAssetUser = true;
        }
      }
      // admin hoặc supper_admin
      if (isRoleOrgAdmin) {
        if (!hasDeletePermission) {
          hasDeletePermission = true;
        }
        if (!hasEditPermission) {
          hasEditPermission = true;
        }
        if (!hasViewPermission) {
          hasViewPermission = true;
        }
      }
    }

    this.setState({
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission,
      ...roles,
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleGetCodeAssetAdd = async () => {
    try {
      const result = await getNewCode();
      if (result != null && result.data && result.data.code) {
        const item = result.data;
        this.setState({
          item: {
            code: item.data,
          },
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  handleAddItem = () => {
    const { OBJECT_USAGE_STATE } = appConst;
    const { departmentUser } = getUserInformation();
    let item = {};

    item.isManageAccountant = true;
    item.isCheck = false;
    item.usageState = OBJECT_USAGE_STATE.NEW.code;
    item.isTemporary = false;
    item.voided = false;
    item.allocationFor = 1;
    item.managementDepartment = departmentUser;
    this.handleGetCodeAssetAdd();
    this.setState({
      item: item,
      isQRCode: false,
      shouldOpenEditorDialog: true,
      isButtonAllocation: true,
      isAddAsset: true,
      isDisableQuantity: false,
      isView: false,
    });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  printAssetBook = async () => {
    if (!this.state?.useDepartmentFilter) {
      toast.warning("Vui lòng chọn phòng ban sử dụng")
      return
    }

    this.setState({
      shouldOpenPrintAssetBook: true,
      item: {
        externalParams: {
          assetGroupId: this.state?.assetGroupFilter?.id,
          departmentId: this.state?.useDepartmentFilter?.id,
          fromDate: formatDateDto(this.state?.fromDate),
          toDate: formatDateDto(this.state?.toDate),
        },
      }
    });
  }

  exportExcelAssetBook = () => {
    if (!this.state?.useDepartmentFilter?.id && this.state?.isPrintAssetFollow) {
      toast.warning("Vui lòng chọn phòng ban sử dụng")
      return
    }
    let { setPageLoading } = this.context;
    let { year } = this.state;
    try {
      let searchObject = {};
      searchObject.assetGroupId = this.state?.assetGroupFilter?.id;
      searchObject.departmentId = this.state?.useDepartmentFilter?.id;
      searchObject.fromDate = this.state?.fromDate ? convertFromToDate(this.state?.fromDate).fromDate : ""
      searchObject.toDate = this.state?.toDate ? convertFromToDate(this.state?.toDate).toDate : "";
      searchObject.templateId = this.state.listDocumentTemplates[0]?.id;

      if (this.state?.isPrintAssetBook) {
        searchObject.yearOfRelease = year?.getFullYear();
      }

      let fileName = this.state?.isPrintAssetBook ? "Sổ tài sản" : "Số theo dõi tại phòng ban";
      let funcExport = this.state?.isPrintAssetBook ? exportExcelAssetBookService : exportExcelAssetFollowService;
      functionExportToExcel(funcExport, searchObject, fileName, setPageLoading);
    } catch (e) {
      toast.error("general.error");
    }
  }

  exportExcelAssetCard = () => {
    let { setPageLoading } = this.context
    setPageLoading(true)
    let searchObject = {};
    searchObject.assetId = this.state?.assetCardId;
    functionExportToExcel(exportExcelAssetCardService, searchObject, "AssetCard.xlsx", setPageLoading);
  }

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked === false) {
        selectAllItem = false;
      }
      if (Asset[i].id === item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  handleSelectAllClick = () => {
    let { Asset } = this.state;
    for (var i = 0; i < Asset.length; i++) {
      Asset[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, Asset: Asset });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = () => {
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  selectStatus = (statusSelected) => {
    let statusIds = [];
    // eslint-disable-next-line no-unused-expressions
    statusSelected?.forEach((item) => {
      statusIds.push(item?.id);
    });
    this.setState(
      {
        status: statusSelected,
        statusIds: statusIds,
      },
      function () {
        this.search();
      }
    );
  };

  selectAssetGroup = (assetGroupSelected) => {
    this.setState({ assetGroup: assetGroupSelected }, function () {
      this.search();
    });
  };

  selectAssetGroupAssetBook = (assetGroupSelected) => {
    this.setState({ assetGroupFilter: assetGroupSelected });
  };

  selectUseDepartment = (selected) => {
    this.setState({ useDepartment: selected }, () => {
      this.search();
    });
  };

  selectUseDepartmentAssetBook = (selected) => {
    this.setState({ useDepartmentFilter: selected });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    })
  }

  selectDecisionCode = (selected) => {
    this.setState({ decisionCode: selected }, () => {
      this.search();
    });
  };

  search = () => {
    this.setPage(0);
  };

  handleKeyUp = (e) => handleKeyUp(e, this.search);

  // ExportExcel
  exportToExcel = async (type) => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const { organization } = getUserInformation();
    const isExportExcelNormal = type === appConst.typeExcel.excel;

    try {
      const {
        keyword,
        managementDepartment,
        isTemporary,
        isManageAccountant,
        statusIds,
        assetGroup,
        page,
        rowsPerPage,
        useDepartment,
        yearPutIntoUse,
        isManagedByAssetManager,
        isUseByAssetManager,
        originalCostTo,
        originalCostFrom,
        medicalEquipment,
        soNamKhConLai,
        dateOfReceptionBottom,
        dateOfReceptionTop,
        store,
        decisionCode,
      } = this.state;
      setPageLoading(true);

      const searchObject = {
        keyword: keyword.trim(),
        managementDepartmentId: managementDepartment?.id || null,
        isTemporary: isTemporary?.value,
        isManageAccountant: isManageAccountant?.value,
        statusIds: statusIds || null,
        assetGroupId: assetGroup?.id || null,
        pageIndex: page + 1,
        pageSize: rowsPerPage,
        useDepartmentId: useDepartment?.id || null,
        yearPutIntoUse: yearPutIntoUse,
        orgId: organization?.org?.id || null,
        isManagedByAssetManager,
        isUseByAssetManager,
        originalCostTo,
        originalCostFrom,
        typeExcel:
          isExportExcelNormal
            ? appConst.typeExcel.excel
            : appConst.typeExcel.excelQr,
        medicalEquipmentId: medicalEquipment?.id,
        soNamKhConLai,
        dateOfReceptionBottom: isValidDate(dateOfReceptionBottom) ? convertFromToDate(dateOfReceptionBottom).fromDate : null,
        dateOfReceptionTop: isValidDate(dateOfReceptionTop) ? convertFromToDate(dateOfReceptionTop).toDate : null,
        storeId: store?.id,
        decisionCode: decisionCode?.decisionCode,
      };
      const params = this.convertSearchParams(searchObject);

      await functionExportToExcel(
        isExportExcelNormal
          ? exportToExcel
          : exportToExcelQR,
        params,
        t("exportToExcel.fixedAsset"),
      )
    } catch (error) {
      console.log(error)
      toast.warning(this.props.t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  exportExampleImportExcel = () => {
    let { t } = this.props;
    exportExampleImportExcel()
      .then((res) => {
        toast.success(t("general.successExport"));
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, t("Asset.importExcelFileName"));
      })
      .catch((err) => {
        console.error(err);
      });
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);

      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };

      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () { }
      );
    });
  };

  selectManagementDepartment = (item) => {
    this.setState({ managementDepartment: item }, function () {
      this.search();
    });
  };

  handleChange = (value, name) => {
    if (["dateOfReceptionBottom", "dateOfReceptionTop"].includes(name)) {
      this.setState({ [name]: value }, () => {
        if (value === null || isValidDate(value)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: value }, () => {
      this.search();
    });
  };
  // end Tree view

  selectYear = (value) => {
    if (value) {
      this.setState({ yearPutIntoUse: value.year }, () => this.search());
    } else {
      this.setState({ yearPutIntoUse: value }, () => this.search());
    }
  };

  // check thông tin khấu hao được sửa hay không
  handleCheckTTKH = (id) => {
    checkTTHK(id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          isTTHK: data.data,
        });
      } else {
        toast.warn(data?.message);
      }
    });
  };

  //edit
  handleEdit = async (id, method) => {
    let { setPageLoading, currentOrg } = this.context;
    const { isRoleOrgAdmin } = getTheHighestRole();
    setPageLoading(true);
    let { t } = this.props;
    this.handleCheckTTKH(id);

    try {
      const { data } = await getItemById(id);
      const isSuccess = data?.code === appConst.CODE.SUCCESS;

      if (isSuccess) {
        const asset = data?.data || {};
        const isMedicalEquipment =
          !checkObject(asset.riskClassification) ||
          asset.medicalEquipment ||
          asset.circulationNumber;

        const checkStatus = asset?.status?.indexOrder
          === appConst.listStatusAssets.LUU_MOI.indexOrder;
        const isDangSuDung = asset?.status?.indexOrder
          === appConst.listStatusInstrumentTools.DANG_SU_DUNG.indexOrder;
        const isDaThanhLy = asset?.status?.indexOrder
          === appConst.listStatusInstrumentTools.DA_THANH_LY.indexOrder;
        const listAssetDocumentId =
          asset.documents?.map((item) => item.id) || [];
        let isView = isDangSuDung || isDaThanhLy;

        if (currentOrg?.code === LIST_ORGANIZATION.BV_VAN_DINH.code) {
          isView = (isDangSuDung || isDaThanhLy) && !isRoleOrgAdmin;
        }

        this.setState({
          item: {
            ...asset,
            managementDepartmentId: asset.managementDepartment?.id,
            assetGroupId: asset.assetGroup?.id,
            storeId: asset.store?.id,
            supplyUnitId: asset.supplyUnit?.id,
            attributes: asset.attributes || [],
            contract: {
              ...asset.contract,
              contractName: asset.contract
                ? `${asset.contract.contractName} - ${asset.contract.contractCode}`
                : null,
            },
            listAssetDocumentId,
            assetGroup: {
              ...asset.assetGroup,
              name: [
                asset.assetGroup?.name && asset.assetGroup.name,
                asset.assetGroup?.depreciationRate &&
                  asset.assetGroup?.depreciationRate ? `${asset.assetGroup?.depreciationRate}%` : null,
                asset.assetGroup?.namSuDung,
              ]
                .filter(Boolean)
                .join(" - "),
            },
            depreciationRate: asset?.depreciationRate || asset.assetGroup?.depreciationRate,
            madeIns: asset?.madeIn
              ? asset?.isBuyLocally
                ? appConst.madeIn[0]
                : appConst.madeIn[1]
              : {},
          },
          isQRCode: true,
          shouldOpenEditorDialog: true,
          isButtonAllocation: checkStatus,
          isAddAsset: false,
          isDisableQuantity: true,
          isMedicalEquipment,
          isEdit: true,
          isView: isView,
          assetId: id,
          isDangSuDung: isDangSuDung
        });
        if (method === appConst.active.view) {
          this.setState({ isView: true, isDangSuDung: false });
        }
      }
      setPageLoading(false);
    } catch (error) {
      console.log(error);

      toast.error(t("toastr.error"));
      setPageLoading(false);
    }
  };

  handlePrintAssetCard = async (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { t } = this.props;
    let searchObject = {
      assetId: id
    }
    try {
      let res = await printAssetCard(searchObject)
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          itemCard: res?.data?.data,
          shouldOpenPrintAssetCard: true,
          assetCardId: id
        })
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handlePrintHistory = async (id) => {
    this.setState({
      shouldOpenPrintHistory: true,
      item: {
        id,
      }
    })
  }

  handleQrCode = (type) => {
    let { itemList } = this.state

    this.setState({
      openPrintQR: true,
      products: itemList,
      typeSuffering: type
    })
  }

  getItemForMapping = async (id) => {
    try {
      const data = await getItemById(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        return data?.data?.data || {};
      }
      return {};
    } catch (error) {
      return {};
    }
  }

  handleMapDataAsset = async (event, itemSelected, item) => {
    const value = await this.getItemForMapping(itemSelected?.id);
    let madeIns = {};
    if (value?.madeIn) {
      if (value?.isBuyLocally) {
        madeIns = appConst.madeIn[0];
      } else {
        madeIns = appConst.madeIn[1];
      }
      // return madeIns
    }

    if (item?.product?.id && item?.decisionCode?.decisionCode) {
      value.decisionCode = item?.decisionCode;
      value.product = item.product;
    };

    const newData = {
      ...value,
      id: null,
      assetAccessories: value?.assetAccessories?.map((phuTung) => ({
        ...phuTung,
        unit: {
          name: phuTung.unitName,
          id: phuTung.unitId,
        },
        product: {
          name: phuTung.productName,
          type: phuTung.productType,
          id: phuTung.productId,
        },
      })),
      serialNumber: item?.serialNumber || "",
      assetGroupId: value?.assetGroup?.id,
      depreciationRate: value?.assetGroup?.depreciationRate,
      useDepartment: "",
      allocationFor: "",
      serialNumbers: item?.serialNumbers || "",
      valueSerialNumber: item?.valueSerialNumber || "",
      madeIns: madeIns,
      soNamKhConLai: value?.assetGroup?.namSuDung
    };
    for (const prop of [
      "status",
      "code",
      "qrCode",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      "dayStartedUsing",
      "accumulatedDepreciationAmount",
      "lastYearOfDepreciation",
      "useDepartment",
      "usePerson",
      "financialCode",
      "usageState",
    ]) {
      delete newData[prop];
    }
    this.setState({ item: newData });
  };

  handleToggleOpenExcel = (e) => {
    const { openExcel } = this.state;

    this.setState((prevState) => ({
      openExcel: !openExcel,
    }));
  };

  handleToggleOpenPrintQR = () => {
    const { openPrintQRToggle } = this.state

    this.setState((prevState) => ({
      openPrintQRToggle: !openPrintQRToggle,
    }));
  }

  handleMenuItemClick = (e, type) => {
    this.exportToExcel(type);
  };

  handleMenuItemPrintQrClick = (e, type) => {
    this.handleQrCode(type)
  }

  getTreeView = async () => {
    try {
      const { data } = await getTreeUser();

      if (data?.code === appConst.CODE.SUCCESS) {
        const newData = data?.data || [];
        const treeList = newData.map((item) => {
          const department = {
            id: "root",
            text: item.departmentName,
            children: item.users.map((user) => ({
              id: user.personId,
              text: user.displayName,
              // You can include other user properties here if needed
            })),
          };
          return department;
        });

        this.setState({ treeList });
      }
    } catch (error) {
      // Handle any errors that occur during the API request
      console.error("Error fetching tree data:", error);
    }
  };

  renderTree = (nodes) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.text}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => this.renderTree(node))
        : null}
    </TreeItem>
  );

  getAssetBydepartment = (event, value) => {
    this.setState(
      { usePersonId: value !== "root" ? value : null },
      this.updatePageData
    );
  };

  handleSelectStore = (store) => {
    this.setState({ store }, () => {
      this.search();
    });
  };

  handleSetState = (data, source) => {
    this.setState({ [source]: data })
  }

  handleSelectMedicalEquipment = (medicalEquipment) => {
    this.setState({ medicalEquipment }, () => {
      this.search();
    });
  };

  handleGetDocumentTemplates = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let data = await handleGetTemplatesByModel(
        PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.LIST.ASSET_BOOK_OF_USE_DEPARTMENT
      );
      if (data) {
        this.setState({
          listDocumentTemplates: data,
        })
      }
    } catch (e) {
      console.error(e)
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    const { t, i18n } = this.props;
    const { currentOrg } = this.context;
    let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
    let searchObjectAssetGroup = {
      ...searchObject,
      assetClass: appConst.assetClass.TSCD,
    };
    let searchObjectBidding = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      status: appConst.STATUS_BIDDING.OPEN.code
    };
    let dateNow = new Date().getFullYear();
    let listYear = [];
    const filterAutocomplete = createFilterOptions();
    const importUrl = importExcelAssetUrl();

    for (let i = dateNow; i >= 1970; i--) {
      listYear.push({ year: i });
    }

    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      assetGroup,
      item,
      status,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenImportExcelDialog,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenAssetHistory,
      shouldOpenPrintAssetBook,
      shouldOpenPrintAssetCard,
      managementDepartment,
      isSearch,
      isPrintAssetFollow,
      isPrintAssetBook,
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission,
      useDepartment,
      searchDepartmentObject,
      isButtonAllocation,
      isQRCode,
      isDisableQuantity,
      tabValue,
      isRoleUser,
      isRoleAssetManager,
      isRoleAccountant,
      isView,
      openExcel,
      isRoleAssetUser,
      departmentUser,
      listStore,
      store,
      originalCostFrom,
      originalCostTo,
      itemPrintBookAsset,
      itemCard,
      fromDate,
      toDate,
      useDepartmentFilter,
      assetGroupFilter,
      isManageAccountant,
      isTemporary,
      medicalEquipment,
      openPrintQR,
      products,
      openPrintQRToggle,
      typeSuffering,
      dateOfReceptionBottom,
      dateOfReceptionTop,
      decisionCode,
      textSearchAdvance,
      year,
      shouldOpenPrintHistory,
    } = this.state;
    let stt = page * rowsPerPage;

    let TitlePage = t("Asset.list_asset");
    const columnsAssetTable = columns_AssetTable(
      t,
      convertNumberPriceRoundUp,
      stt
    );
    const searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      managementDepartmentId: departmentUser?.id,
      isActive: STATUS_STORE.HOAT_DONG.code,
    };

    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        minWidth: "95px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            isRoleUser={isRoleUser}
            isRoleAssetUser={isRoleAssetUser}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasViewPermission={hasViewPermission}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData?.id);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.history) {
                this.setState({
                  item: rowData,
                  shouldOpenAssetHistory: true,
                });
              } else if (method === appConst.active.view) {
                this.handleEdit(rowData?.id, appConst.active.view);
              } else if (method === appConst.active.card) {
                this.handlePrintAssetCard(rowData?.id);
              } else if (method === appConst.active.print) {
                this.handlePrintHistory(rowData?.id)
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/page",
              },
              { name: TitlePage },
            ]}
          />
        </div>

        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              value={appConst.tabAsset.tabManagement}
              label={t("Asset.tab.management")}
            />
            {isRoleAssetManager && (
              <Tab
                className="tab"
                value={appConst.tabAsset.tabGrantedForUse}
                label={t("Asset.tab.grantedForUse")}
              />
            )}
            <Tab
              className="tab"
              value={appConst.tabAsset.tabFullyDepreciated}
              label={t("Asset.tab.tabFullyDepreciated")}
            />
          </Tabs>
        </AppBar>

        <ValidatorForm ref="form" onSubmit={() => { }}>
          {/* head search box */}
          <Grid
            container
            spacing={2}
            alignItems="flex-end"
            justifyContent="space-between"
          >
            <Grid item md={8} xs={12}>
              {hasEditPermission && (
                <Button
                  className="mt-10 align-bottom mr-16"
                  variant="contained"
                  color="primary"
                  onClick={this.handleAddItem}
                >
                  {t("general.add")}
                </Button>
              )}
              {hasEditPermission && (
                <Button
                  className="align-bottom mr-12 mt-12"
                  variant="contained"
                  color="primary"
                  onClick={this.importExcel}
                >
                  {t("general.importExcel")}
                </Button>
              )}

              <ButtonGroup
                className="align-bottom mr-12 mt-12"
                aria-label="button group"
                variant="contained"
                ref={this.anchorRefQR}
              >
                <Button
                  aria-controls={openPrintQRToggle ? "split-button-menu" : undefined}
                  aria-expanded={openPrintQRToggle ? "true" : undefined}
                  aria-label="select merge strategy"
                  aria-haspopup="menu"
                  style={{ paddingRight: "3px" }}
                  variant="contained"
                  color="primary"
                  onClick={this.handleToggleOpenPrintQR}
                >
                  {t("Asset.PrintQRCode")}
                  <ArrowDropDownIcon />
                </Button>
              </ButtonGroup>
              <Popper
                open={openPrintQRToggle}
                anchorEl={this.anchorRefQR.current}
                role={undefined}
                transition
                disablePortal
                style={{ zIndex: 999 }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={() => { }}>
                    <MenuList id="split-button-menu" autoFocusItem>
                      {OPTIONS_EXCEL_QR.map((option, index) => (
                        <MenuItem
                          key={option.code}
                          onClick={(event) =>
                            this.handleMenuItemPrintQrClick(event, option.code)
                          }
                        >
                          {option.name}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>

              </Popper>

              <Button
                className="mr-12 mt-12 pr-3"
                variant="contained"
                color="primary"
                onClick={() => {
                  this.setState({
                    isPrintAssetFollow: !this.state.isPrintAssetFollow,
                    useDepartmentFilter: null,
                    assetGroupFilter: null,
                    isPrintAssetBook: false,
                    textSearchAdvance: "Sổ theo dõi tại phòng ban"
                  });
                }}
              >
                {t("Asset.departmentalAssetTrackingLog")}
                <ArrowDropDownIcon />
              </Button>

              <Button
                className="mr-12 mt-12 pr-3"
                variant="contained"
                color="primary"
                onClick={() => {
                  this.setState({
                    isPrintAssetBook: !this.state.isPrintAssetBook,
                    useDepartmentFilter: null,
                    assetGroupFilter: null,
                    isPrintAssetFollow: false,
                    textSearchAdvance: "Sổ tài sản",
                  });
                }}
              >
                {t("Asset.AssetBook")}
                <ArrowDropDownIcon />
              </Button>

              <Button
                ref={this.anchorRef}
                variant="contained"
                aria-label="split button"
                aria-controls={openExcel ? "split-button-menu" : undefined}
                aria-expanded={openExcel ? "true" : undefined}
                aria-haspopup="menu"
                color="primary"
                className="align-bottom mr-12 mt-12 pr-3"
                onClick={(e) => this.handleToggleOpenExcel(e)}
              >
                {t("general.exportToExcel")}
                <ArrowDropDownIcon />
              </Button>

              <Popper
                open={openExcel}
                anchorEl={this.anchorRef.current}
                role={undefined}
                transition
                disablePortal
                style={{ zIndex: 999 }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={() => { }}>
                    <MenuList id="split-button-menu" autoFocusItem>
                      {OPTIONS_EXCEL_ASSET.map((option, index) => (
                        <MenuItem
                          key={option.code}
                          onClick={(event) =>
                            this.handleMenuItemClick(event, option.code)
                          }
                        >
                          {option.name}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Popper>

              {shouldOpenConfirmationDeleteAllDialog && (
                <ConfirmationDialog
                  open={shouldOpenConfirmationDeleteAllDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDeleteAll}
                  text={t("general.deleteAllConfirm")}
                />
              )}
              <Button
                className="mr-12 mt-12 pr-3"
                variant="contained"
                color="primary"
                onClick={() => {
                  if (this.state.isSearch) {
                    this.setState({ isSearch: false });
                  } else {
                    this.setState({ isSearch: true });
                  }
                }}
              >
                {t("Tìm kiếm nâng cao")}
                <ArrowDropDownIcon />
              </Button>
            </Grid>

            <Grid item md={4} sm={12} xs={12}>
              <FormControl fullWidth>
                <Input
                  className="search_box w-100"
                  onChange={this.handleTextChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  name="keyword"
                  placeholder={t("Asset.enterSearchTable")}
                  id="search_box"
                  autoComplete="off"
                  startAdornment={
                    <InputAdornment position="end">
                      <SearchIcon
                        onClick={() => this.search()}
                        className="searchTable"
                      />
                    </InputAdornment>
                  }
                />
              </FormControl>
            </Grid>
          </Grid>

          <Collapse in={isSearch}>
            <Card elevation={1} className="mt-30 mt-16">
              <CardContent>
                <Grid container spacing={2} alignItems="flex-end" >
                  <Grid item md={12} xs={12} className="text-center text-uppercase py-0">
                    <b >Tìm kiếm nâng cao</b>
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("Asset.filter_manage_department")}
                      searchFunction={
                        isRoleAssetManager
                          ? getListOrgManagementDepartment
                          : searchByPageDepartmentNew
                      }
                      searchObject={isRoleAssetManager ? {} : searchDepartmentObject}
                      typeReturnFunction={isRoleAssetManager ? "list" : "category"}
                      defaultValue={managementDepartment}
                      displayLable={"name"}
                      isNoRenderChildren
                      value={managementDepartment}
                      onSelect={this.selectManagementDepartment}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                      showCode={"showCode"}
                      listData={this.state?.listManagementDepartment || []}
                      setListData={(data) => this.handleSetState(data, "listManagementDepartment")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("Asset.filter_temporary")}
                      searchFunction={() => { }}
                      searchObject={{}}
                      listData={appConst.listTemporary}
                      setListData={() => { }}
                      displayLable="name"
                      selectedOptionKey="code"
                      value={isTemporary || null}
                      onSelect={(value) => this.handleChange(value, "isTemporary")}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocomplete
                      label={t("general.filterAssetGroup")}
                      searchFunction={assetGroupSearchByPage}
                      searchObject={searchObjectAssetGroup}
                      defaultValue={assetGroup}
                      displayLable={"name"}
                      value={assetGroup}
                      onSelect={this.selectAssetGroup}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  {isRoleAssetManager && (
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocompleteSub
                        isFocus={true}
                        label={t("general.filterStore")}
                        searchFunction={storesSearchByPage}
                        searchObject={searchObjectStore}
                        listData={listStore}
                        setListData={(data) => this.handleSetState(data, "listStore")}
                        defaultValue={store}
                        displayLable={"name"}
                        value={store || null}
                        onSelect={this.handleSelectStore}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                  )}
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("general.filterByDepartment")}
                      searchFunction={searchReceiverDepartment}
                      searchObject={searchDepartmentObject}
                      defaultValue={useDepartment}
                      displayLable={"text"}
                      value={useDepartment}
                      onSelect={this.selectUseDepartment}
                      filterOptions={(options, params) => {
                        params.inputValue = params.inputValue.trim();
                        return filterAutocomplete(options, params);
                      }}
                      noOptionsText={t("general.noOption")}
                      listData={this.state?.listUseDepartment || []}
                      setListData={(data) => this.handleSetState(data, "listUseDepartment")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("general.filterStatus")}
                      searchFunction={statusSearchByPage}
                      multiple={true}
                      searchObject={searchObject}
                      displayLable={"name"}
                      selectedOptionKey="code"
                      value={status}
                      onSelect={this.selectStatus}
                      filterOptions={(options, params) => {
                        params.inputValue = params.inputValue.trim();
                        return filterAutocomplete(options, params);
                      }}
                      noOptionsText={t("general.noOption")}
                      listData={this.state?.listStatus || []}
                      setListData={(data) => this.handleSetState(data, "listStatus")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("Asset.filterDepreciated")}
                      searchFunction={() => { }}
                      searchObject={{}}
                      listData={appConst.listManageAccountant}
                      selectedOptionKey="value"
                      setListData={() => { }}
                      defaultValue={isManageAccountant}
                      displayLable="name"
                      value={isManageAccountant}
                      onSelect={(value) => this.handleChange(value, "isManageAccountant")}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <Autocomplete
                      id="combo-box"
                      fullWidth
                      size="small"
                      options={listYear}
                      onChange={(e, value) => this.selectYear(value)}
                      getOptionSelected={(option, value) => option?.year === value?.year}
                      getOptionLabel={(option) => option.year.toString()}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Chọn năm"
                          variant="standard"
                        />
                      )}
                      filterOptions={(options, params) => {
                        params.inputValue = params.inputValue.trim();
                        return filterAutocomplete(options, params);
                      }}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <TextValidator
                      fullWidth
                      label={t("general.originalCostFrom")}
                      value={originalCostFrom}
                      name="originalCostFrom"
                      onKeyDown={(e) => {
                        handleKeyDownFloatOnly(e)
                        this.handleKeyDownEnterSearch(e);
                      }}
                      onKeyUp={this.handleKeyUp}
                      onChange={this.handleTextChange}
                      InputProps={{
                        inputComponent: NumberFormatCustom,
                        endAdornment: (
                          <InputAdornment position="end">
                            <SearchIcon
                              onClick={this.search}
                              className="text-muted"
                            />
                          </InputAdornment>
                        )
                      }}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <TextValidator
                      fullWidth
                      label={t("general.originalCostTo")}
                      value={originalCostTo}
                      name="originalCostTo"
                      onKeyDown={(e) => {
                        handleKeyDownFloatOnly(e);
                        this.handleKeyDownEnterSearch(e);
                      }}
                      onKeyUp={this.handleKeyUp}
                      onChange={this.handleTextChange}
                      InputProps={{
                        inputComponent: NumberFormatCustom,
                        endAdornment: (
                          <InputAdornment position="end">
                            <SearchIcon
                              onClick={this.search}
                              className="text-muted"
                            />
                          </InputAdornment>
                        )
                      }}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        margin="none"
                        fullWidth
                        autoOk
                        id="date-picker-dialog"
                        label={t("general.dateOfReceptionBottom")}
                        format="dd/MM/yyyy"
                        value={dateOfReceptionBottom ?? null}
                        maxDate={dateOfReceptionTop}
                        onChange={(data) => this.handleChange(data, "dateOfReceptionBottom")}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        minDateMessage={t("general.minDateMessage")}
                        maxDateMessage={dateOfReceptionTop ? t("general.maxDateFromDate") : t("general.maxDateMessage")}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        margin="none"
                        fullWidth
                        autoOk
                        id="date-picker-dialog"
                        label={t("general.dateOfReceptionTop")}
                        format="dd/MM/yyyy"
                        value={dateOfReceptionTop ?? null}
                        minDate={dateOfReceptionBottom}
                        onChange={(data) => this.handleChange(data, "dateOfReceptionTop")}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        minDateMessage={dateOfReceptionBottom ? t("general.minDateToDate") : t("general.minDateMessage")}
                        maxDateMessage={t("general.maxDateMessage")}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("MedicalEquipmentType.title")}
                      searchFunction={getMedicalEquipment}
                      searchObject={searchObject}
                      typeReturnFunction="category"
                      displayLable={"name"}
                      value={medicalEquipment || null}
                      onSelect={this.handleSelectMedicalEquipment}
                      noOptionsText={t("general.noOption")}
                      listData={this.state?.listMedicalEquipment || []}
                      setListData={(data) => this.handleSetState(data, "listMedicalEquipment")}
                    />
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={t("Asset.decisionCode")}
                      searchFunction={searchByPageBidding}
                      searchObject={searchObjectBidding}
                      value={decisionCode}
                      displayLable={"decisionCode"}
                      typeReturnFunction="category"
                      onSelect={this.selectDecisionCode}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                      listData={this.state?.listDecisionCode || []}
                      setListData={(data) => this.handleSetState(data, "listDecisionCode")}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Collapse>

          <Collapse in={isPrintAssetFollow || isPrintAssetBook}>
            <Card elevation={1} className="mt-30 mt-16">
              <CardContent>
                <Grid container spacing={2} alignItems="flex-end">
                  <Grid item md={12} xs={12} className="text-center text-uppercase py-0">
                    <b>{textSearchAdvance}</b>
                  </Grid>
                  {(isPrintAssetFollow || isPrintAssetBook) && (
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocomplete
                        label={t("Asset.AssetGroup")}
                        searchFunction={assetGroupSearchByPage}
                        searchObject={searchObjectAssetGroup}
                        defaultValue={assetGroupFilter}
                        displayLable={"name"}
                        value={assetGroupFilter}
                        onSelect={this.selectAssetGroupAssetBook}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim();
                          return filterAutocomplete(options, params);
                        }}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                  )}
                  {isPrintAssetFollow && (
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocomplete
                        label={
                          <span>
                            {isPrintAssetFollow && <span className="colorRed">*</span>}
                            {t("Asset.useDepartment")}
                          </span>
                        }
                        searchFunction={searchReceiverDepartment}
                        searchObject={searchDepartmentObject}
                        defaultValue={useDepartmentFilter}
                        displayLable={"text"}
                        value={useDepartmentFilter}
                        onSelect={(useDepartmentFilter) => this.selectUseDepartmentAssetBook(useDepartmentFilter)}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim();
                          return filterAutocomplete(options, params);
                        }}
                        noOptionsText={t("general.noOption")}
                        validators={["required"]}
                        errorMessages={[t('general.required')]}
                      />
                    </Grid>
                  )}
                  {isPrintAssetFollow && (
                    <Grid item md={3} sm={6} xs={6} >
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          className="w-100"
                          id="mui-pickers-date"
                          label={t("Asset.fromDate")}
                          type="text"
                          autoOk={false}
                          format="dd/MM/yyyy"
                          name={"fromDate"}
                          value={fromDate}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          maxDate={toDate ? (new Date(toDate)) : undefined}
                          clearable
                          onChange={(date) => this.handleDateChange(date, "fromDate")}
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                  )}
                  {isPrintAssetFollow && (
                    <Grid item md={3} sm={6} xs={6}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          className="w-100"
                          id="mui-pickers-date"
                          label={t("Asset.toDate")}
                          type="text"
                          autoOk={false}
                          format="dd/MM/yyyy"
                          name={"toDate"}
                          value={toDate}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          minDate={fromDate ? (new Date(fromDate)) : undefined}
                          clearable
                          onChange={(date) => this.handleDateChange(date, "toDate")}
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                  )}
                  {isPrintAssetBook && (
                    <Grid item md={2} sm={6} xs={6} >
                      <ValidatedDatePicker
                        label={t("general.year")}
                        autoOk
                        views={["year"]}
                        format="yyyy"
                        name={"fromDate"}
                        value={year}
                        disableFuture
                        animateYearScrolling
                        onChange={(date) => this.handleDateChange(date, "year")}
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </Grid>
                  )}
                  <Grid item md={12} sm={12} xs={12}>
                    {isPrintAssetFollow && <Button
                      className="align-bottom mr-16"
                      variant="contained"
                      color="primary"
                      onClick={this.printAssetBook}
                    >
                      {t("Asset.printDepartmentalAssetTrackingLog")}
                    </Button>}
                    {!isPrintAssetFollow && (
                      <Button
                        className="align-bottom mr-16"
                        variant="contained"
                        color="primary"
                        onClick={this.exportExcelAssetBook}
                      >
                        {t("Asset.exportExcel")}
                      </Button>
                    )}
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Collapse>
        </ValidatorForm>

        <Grid container spacing={2} className={"mt-16"}>
          {isRoleAssetUser && (
            <Grid
              item
              md={3}
              xs={12}
              className="p-0 mt-8 asset_department overflow-auto"
            >
              <div className="bg-white">
                <div className="text-white font-weight-bold treeview-title">
                  {t("general.list_user_department")}
                </div>
                {this.state.treeList && (
                  <TreeView
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpanded={["root"]}
                    defaultExpandIcon={<ChevronRightIcon />}
                    onNodeSelect={this.getAssetBydepartment}
                  >
                    {this.state.treeList.map((node) => this.renderTree(node))}
                  </TreeView>
                )}
              </div>
            </Grid>
          )}

          <Grid
            item
            md={isRoleAssetUser ? 9 : 12}
            xs={12}
            style={{ overflowX: "auto" }}
          >
            <div>
              {shouldOpenAssetHistory && (
                <ViewAssetHistoryPopup
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenAssetHistory}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}
              {shouldOpenEditorDialog && (
                <AssetEditorDialogNew
                  t={t}
                  i18n={i18n}
                  isAddAsset={this.state.isAddAsset}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  isButtonAllocation={isButtonAllocation}
                  item={item}
                  isQRCode={isQRCode}
                  isDisableQuantity={isDisableQuantity}
                  isMedicalEquipment={this.state?.isMedicalEquipment}
                  isRoleAccountant={isRoleAccountant}
                  handleMapDataAsset={this.handleMapDataAsset}
                  isTTHK={this.state.isTTHK}
                  isView={isView}
                  assetId={this.state.assetId}
                  isDangSuDung={this.state?.isDangSuDung}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}

              {shouldOpenPrintAssetBook && (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenPrintAssetBook}
                  item={this.state.item}
                  title={t("Sổ theo dõi tại nơi sử dụng")}
                  model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.LIST.ASSET_BOOK_OF_USE_DEPARTMENT}
                  externalConfig={{
                    params: this.state.item?.externalParams,
                  }}
                />
              )}
              {shouldOpenPrintHistory && (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenPrintHistory}
                  item={this.state.item}
                  title={t("Phiếu in lý lịch máy")}
                  model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.LIST.ASSET_PROFILE}
                />
              )}
              {shouldOpenPrintAssetCard && (
                <AssetCard
                  t={t}
                  i18n={i18n}
                  open={shouldOpenPrintAssetCard}
                  handleClose={this.handleDialogClose}
                  handleOKEditClose={this.handleOKEditClose}
                  item={itemCard}
                  useDepartmentFilter={useDepartmentFilter}
                  assetGroupFilter={assetGroupFilter}
                  exportExcelAssetCard={this.exportExcelAssetCard}
                />
              )}
            </div>
            <div>
              {shouldOpenImportExcelDialog && (
                <ImportExcelDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenImportExcelDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  exportExampleImportExcel={this.exportExampleImportExcel}
                  exportFileError={exportExcelFileError}
                  url={importUrl}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}

              {openPrintQR && (
                <AssetsQRPrintAccording
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleCloseQrCode}
                  open={openPrintQR}
                  typeSuffering={typeSuffering}
                  items={products}
                />
              )}
            </div>
            <CustomMaterialTable
              title={t("general.list")}
              data={itemList}
              style={{ overflowX: "auto" }}
              columns={columnsAssetTable}
              columnActions={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                selection: false,
                actionsColumnIndex: -1,
                sorting: false,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: "450px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
            />
            <CustomTablePagination
              totalElements={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              handleChangePage={this.handleChangePage}
              setRowsPerPage={this.setRowsPerPage}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

AssetTable.contextType = AppContext;
export default withRouter(AssetTable);
