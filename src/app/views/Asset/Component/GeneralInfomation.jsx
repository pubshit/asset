import { Grid, makeStyles, TextField } from '@material-ui/core';
import React, { useEffect, useMemo, useState } from 'react';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { useTranslation } from 'react-i18next';
import { searchByPage } from '../../Product/ProductService';
import { appConst } from '../../../appConst';
import { getManufacturer } from '../AssetService';
import { filterOptions } from '../../../appFunction';
import AsynchronousAutocompleteTransfer from '../../utilities/AsynchronousAutocompleteTransfer';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    marginBottom: theme.spacing(2),
  },
  imgContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center', // Canh giữa ảnh theo chiều ngang và dọc
    width: 200,
    height: 200,
  },
  img: {
    maxHeight: '100%',
    maxWidth: '100%',
    objectFit: 'contain', // Đảm bảo ảnh hiển thị hợp lý trong không gian giới hạn
  },
  defaultImage: {
    width: 200,
    height: 200,
    backgroundColor: '#f0f0f0', // Màu nền của ảnh mặc định
    display: 'flex',
    justifyContent: 'center', // Căn chữ "Chọn ảnh" chính giữa theo chiều ngang
    alignItems: 'center', // Căn chữ "Chọn ảnh" chính giữa theo chiều dọc
    color: '#aaaaaa', // Màu chữ nhạt
    fontSize: '14px', // Cỡ chữ
  },
}));

const GeneralInfomation = (props) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const {item} = props;
  const [imageUrl, setImageUrl] = useState('');
  const [defaultImage, setDefaultImage] = useState(true);
  const [listSanPham, setListSanPham] = useState([]);
  const [query, setQuery] = useState({
    keyword: '',
    pageIndex: 0,
    pageSize: 10,
  });
  
  const filterAutocomplete = createFilterOptions();
  const handleChange = () => { };

  const searchObjectUnit = useMemo(() => ({ pageIndex: 1, pageSize: 1000000 }), []);

  const searchObjectProduct = useMemo(() => ({ pageIndex: 1, pageSize: 1000000 }), []);

  useEffect(() => {
    let searchObject = {};
    searchObject.keyword = query.keyword.trim();
    searchObject.pageIndex = query.pageIndex + 1;
    searchObject.pageSize = query.pageSize;
    try {
      searchByPage(searchObject).then((res) => {
        if (res.status === appConst.CODE.SUCCESS) {
          setListSanPham(res.data?.content);
        } else {
          return;
        }
      });
    } catch (e) {
      console.error(e);
    }
  }, [query]);

  const handleChangeSearch = (event) => {
    setQuery({
      ...query,
      keyword: event.target.value,
    });
  };

  return (
    <Grid container spacing={2} justifyContent="space-between">
      {/* Hàng 1 Cột 1*/}
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          className={classes.textField}
          label="Mã quản lý"
          id="outlined-size-small"
          name="managementCode"
          variant="outlined"
          size="small"
        />
        <Autocomplete
          id="size-small-standard"
          size="small"
          options={listSanPham}
          getOptionLabel={(option) => option.name}
          defaultValue={[]}
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            let filtered = filterAutocomplete(options, params);
            return filtered;
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              className={classes.textField}
              variant="outlined"
              label="Chọn sản phẩm"
              placeholder="Nhập tên sản phẩm"
              onChange={handleChangeSearch}
              inputProps={{
                ...params.inputProps,
                type: 'search',
              }}
              value={item?.product || null}
            />
          )}
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Model"
          id="outlined-size-small"
          name="model"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Xuất xứ"
          id="outlined-size-small"
          name="madeIn"
          variant="outlined"
          size="small"
        />
        {/*<Autocomplete*/}
        {/*  id='size-small-standard'*/}
        {/*  size='small'*/}
        {/*  options={appConst.subGroups}*/}
        {/*  getOptionLabel={(option) => option.riskType}*/}
        {/*  defaultValue={[]}*/}
        {/*  filterOptions={(options, params) => {*/}
        {/*    params.inputValue = params.inputValue.trim();*/}
        {/*    let filtered = filterAutocomplete(options, params);*/}
        {/*    return filtered;*/}
        {/*  }}*/}
        {/*  renderInput={(params) => (<TextField*/}
        {/*    {...params}*/}
        {/*    className={classes.textField}*/}
        {/*    variant='outlined'*/}
        {/*    label='Phân nhóm gói '*/}
        {/*    inputProps={{*/}
        {/*      ...params.inputProps,*/}
        {/*      type: 'search',*/}
        {/*    }}*/}
        {/*  />)}*/}
        {/*/>*/}
        <AsynchronousAutocompleteTransfer
          className={classes.textField}
          isFocus={true}
          label={t('Chọn sản phẩm')}
          searchFunction={searchByPage}
          searchObject={searchObjectProduct}
          // listData={item?.listManufacturer}
          // setListData={(data) => onSetDataSelect(data, 'listManufacturer')}
          // defaultValue={item?.manufacturer}
          typeReturnFunction="category"
          displayLable={'name'}
          // value={item?.manufacturer}
          // onSelect={selectManufacturer}
          filterOptions={(options, params) => filterOptions(options, params, true, 'name')}
          noOptionsText={t('general.noOption')}
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Số hóa đơn"
          id="outlined-size-small"
          name="invoiceNumber"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Hình thức mua sắm cb"
          id="outlined-size-small"
          name="shoppingForm"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Đơn vị cung cấp"
          id="outlined-size-small"
          name="supplyUnit"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Đơn vị bảo hành"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Phân loại tài sản cố định"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
      </Grid>

      {/* Hàng 1 Cột 2*/}
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          className={classes.textField}
          label="Mã theo BHYT"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Tên tài sản"
          id="outlined-size-small"
          name="name"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label={t('Asset.serialNumber')}
          id="outlined-size-small"
          name="serialNumber"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Quốc gia"
          id="outlined-size-small"
          variant="outlined"
          size="small"
        />
        <Autocomplete
          id="size-small-standard"
          size="small"
          options={appConst.listRisk}
          getOptionLabel={(option) => option.riskType}
          defaultValue={[]}
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            return filterAutocomplete(options, params);
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              className={classes.textField}
              variant="outlined"
              label="Phân loại rủi ro"
              inputProps={{
                ...params.inputProps,
                type: 'search',
              }}
            />
          )}
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Ngày hóa đơn"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Số hợp đồng"
          id="outlined-size-small"
          name="contract"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Kho nhập"
          id="outlined-size-small"
          name="store"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Thời gian bảo hành"
          id="outlined-size-small"
          name="warrantyMonth"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Phân loại TBYT"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
      </Grid>

      {/* Hàng 1 Cột 3*/}
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          className={classes.textField}
          label="Mã kế toán"
          id="outlined-size-small"
          name="financialCode"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Tên quản lý"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
        <AsynchronousAutocompleteTransfer
          className={classes.textField}
          isFocus={true}
          label={t('Asset.manufacturer')}
          searchFunction={getManufacturer}
          searchObject={searchObjectUnit}
          // listData={item?.listManufacturer}
          // setListData={(data) => onSetDataSelect(data, 'listManufacturer')}
          // defaultValue={item?.manufacturer}
          typeReturnFunction="category"
          displayLable={'name'}
          value={item?.manufacturer}
          // onSelect={selectManufacturer}
          filterOptions={(options, params) => filterOptions(options, params, true, 'name')}
          noOptionsText={t('general.noOption')}
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Số lưu hành/Giấy phép nhập khẩu"
          id="outlined-size-small"
          name="circulationNumber"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Năm sản xuất"
          id="outlined-size-small"
          name="yearOfManufacture"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Đơn vị tính cb"
          id="outlined-size-small"
          // defaultValue="Small"
          name="unit"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Ngày hợp đồng"
          id="outlined-size-small"
          // defaultValue="Small"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Phòng quản lý cb"
          id="outlined-size-small"
          name="managementDepartment"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Ngày hết hạn bảo hành"
          id="outlined-size-small"
          // defaultValue="Small"
          name="warrantyExpiryDate"
          variant="outlined"
          size="small"
        />
        <TextField
          fullWidth
          className={classes.textField}
          label="Vị trí lắp đặt"
          id="outlined-size-small"
          name="installationLocation"
          variant="outlined"
          size="small"
        />
      </Grid>
      {/* Hàng 1 Cột 4*/}
      <Grid item xs={12} sm={6} md={2} alignItems="center">
        {imageUrl ? ( // imageUrl là biến chứa đường dẫn của ảnh
          <div className={classes.imgContainer}>
            <img
              src={imageUrl}
              alt="Image"
              className={classes.img}
              onError={() => {
                // Nếu ảnh không thể render được, sẽ hiển thị ảnh mặc định
                setDefaultImage(true);
              }}
            />
          </div>
        ) : defaultImage ? ( // Nếu không có đường dẫn của ảnh hoặc ảnh không render được, hiển thị ảnh mặc định
          <div className={classes.imgContainer}>
            <div className={classes.defaultImage}>
              <span>QR</span>
            </div>
          </div>
        ) : null}
        {imageUrl ? ( // imageUrl là biến chứa đường dẫn của ảnh
          <div className={classes.imgContainer}>
            <img
              src={imageUrl}
              alt="Image"
              className={classes.img}
              onError={() => {
                // Nếu ảnh không thể render được, sẽ hiển thị ảnh mặc định
                setDefaultImage(true);
              }}
            />
          </div>
        ) : defaultImage ? ( // Nếu không có đường dẫn của ảnh hoặc ảnh không render được, hiển thị ảnh mặc định
          <div className={classes.imgContainer}>
            <div className={classes.defaultImage}>
              <span>Chọn ảnh</span>
            </div>
          </div>
        ) : null}
      </Grid>
    </Grid>
  );
};

export default GeneralInfomation;
