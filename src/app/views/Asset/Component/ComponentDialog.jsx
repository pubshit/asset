import React from "react";
import SelectSupplyPopup from "../../Component/SupplyUnit/SelectSupplyUnitPopup";
import CommonObjectDialog from "app/views/CommonObject/CommonObjectDialog";
import { appConst } from "app/appConst";
import StoreEditorDialog from "app/views/Store/StoreEditorDialog";
import SelectAssetGroupPopup from "app/views/Component/AssetGroup/SelectAssetGroupPopup";
import AssetAllocationEditorDialog from "app/views/AssetAllocation/AssetAllocationEditorDialog";
import StockKeepingUnitEditorDialog from "app/views/StockKeepingUnit/StockKeepingUnitEditorDialog";
import { Grid } from "@material-ui/core";
import SelectProductPopup from "app/views/Component/Product/SelectProductPopup";
import VoucherFilePopup from "app/views/Component/AssetFile/VoucherFilePopup";
import MedicalEquipmentTypeDialog from "app/views/MedicalEquipmentType/MedicalEquipmentTypeDialog";
import ShoppingFormDialog from "app/views/ShoppingForm/ShoppingFormDialog";
import ContractDialog from "../../Contract/ContractDialog";
import SupplierDialog from "app/views/Supplier/SupplierDialog";
import AssetAccessoryDetails from "../ComponentPopups/AssetAccessoryDetails";
import SelectProductAttributespopup from "../../Component/ProductAttribute/SelectProductAttributespopup";

const ComponentDialog = (props) => {
  const {
    t,
    i18n,
    shouldOpenSelectSupplyPopup,
    handleSelectSupply,
    supplyUnit,
    handleSelectSupplyPopupClose,
    shouldOpenDialogManufacturer,
    handleClose,
    handleSetDataSelect,
    selectManufacturer,
    shouldOpenDialogStore,
    selectStores,
    shouldOpenSelectAssetGroupPopup,
    assetGroup,
    handleSelectAssetGroupPopupClose,
    handleSelectAssetGroup,
    shouldOpenAssetAllocationEditorDialog,
    handleCloseAssetAllocationEditorDialog,
    shouldOpenDialogUnit,
    selectUnit,
    shouldOpenAllocationEditorDialog,
    handleCloseAll,
    assetAllocation,
    isUpdateAsset,
    shouldOpenSelectProductPopup,
    product,
    handleSelectProduct,
    handleSelectProductPopupClose,
    productTypeCode,
    shouldOpenPopupAssetFile,
    handleAssetFilePopupClose,
    itemAssetDocument,
    getAssetDocument,
    handleUpdateAssetDocument,
    documentType,
    item,
    shouldOpenDialogMedicalEquipment,
    selectMedicalEquipment,
    shouldOpenDialogShoppingForm,
    selectShoppingForm,
    shouldOpenAddContractDialog,
    handleChangeSelectContract,
    shouldOpenAddSupplierDialog,
    handleSelectDVBH,
    isSupply = false,
    isReceivingAsset,
    departmentSearchObject,
    itemAssetAccessory,
    closeAssetAccessoryDetails,
    shouldOpenPopupAssetAccessoryDetails,
    setListSpareparts,
    assetAccessories,
    isView,
    keySearch,
    shoppingFormText,
    keySearchDonViBaoHanh,
    shouldOpenPopupSelectAttribute,
    handleAddAttribute,
    attributes,
    handleSelectAttributePopupClose
  } = props;

  return (
    <div>
      {/* Nhà cung cấp */}
      {shouldOpenSelectSupplyPopup && (
        <SelectSupplyPopup
          {...props}
          open={shouldOpenSelectSupplyPopup}
          handleSelect={handleSelectSupply}
          selectedItem={supplyUnit != null ? supplyUnit : {}}
          handleClose={handleSelectSupplyPopupClose}
          t={t}
          i18n={i18n}
        />
      )}

      {/* Hãng sản xuất */}
      {shouldOpenDialogManufacturer && (
        <CommonObjectDialog
          {...props}
          t={t}
          i18n={i18n}
          handleClose={handleClose}
          handleOKEditClose={handleClose}
          isTSCD
          typeCode={appConst.listCommonObject.HANG_SAN_XUAT.code} //code hãng sản xuất
          open={shouldOpenDialogManufacturer}
          handleSetDataSelect={(data) =>
            handleSetDataSelect(data, "listManufacturer")
          }
          item={{
            name: props.keySearch
          }}
          selectManufacturer={selectManufacturer}
        />
      )}

      {/* Kho */}
      {shouldOpenDialogStore && (
        <StoreEditorDialog
          t={t}
          i18n={i18n}
          item={{
            ...item,
            keySearch: props.keySearch
          }}
          departmentSearchObject={departmentSearchObject}
          handleOKEditClose={handleClose}
          handleClose={handleClose}
          open={shouldOpenDialogStore}
          // handleSetDataSelect={(data) => handleSetDataSelect(data, "listStore")}
          selectStores={selectStores}
        />
      )}

      {/*  */}
      {shouldOpenSelectAssetGroupPopup && (
        <SelectAssetGroupPopup
          {...props}
          t={t}
          open={shouldOpenSelectAssetGroupPopup}
          handleClose={handleSelectAssetGroupPopupClose}
          handleSelect={handleSelectAssetGroup}
          fromAssetDialog={true}
          i18n={i18n}
          selectedItem={assetGroup != null ? assetGroup : {}}
        />
      )}

      {/*  */}
      {shouldOpenAssetAllocationEditorDialog && (
        <AssetAllocationEditorDialog
          {...props}
          t={t}
          i18n={i18n}
          handleClose={handleCloseAssetAllocationEditorDialog}
          open={shouldOpenAssetAllocationEditorDialog}
          handleOKEditClose={handleCloseAssetAllocationEditorDialog}
          item={{
            name: props.keySearch
          }}
        />
      )}

      {/*  */}
      {shouldOpenDialogUnit && (
        <StockKeepingUnitEditorDialog
          {...props}
          t={t}
          i18n={i18n}
          handleClose={handleClose}
          handleOKEditClose={handleClose}
          open={shouldOpenDialogUnit}
          handleSetDataSelect={(data) => handleSetDataSelect(data, "listUnit")}
          selectUnit={selectUnit}
        />
      )}

      <Grid>
        {shouldOpenAllocationEditorDialog && (
          <AssetAllocationEditorDialog
            {...props}
            t={t}
            i18n={i18n}
            open={shouldOpenAllocationEditorDialog}
            handleOKEditClose={handleCloseAll}
            handleCloseAllocationEditorDialog={handleClose}
            assetAllocation={assetAllocation || []}
            item={{}}
            isAllocation={true}
            isUpdateAsset={isUpdateAsset}
          />
        )}
      </Grid>
      {shouldOpenSelectProductPopup && (
        <SelectProductPopup
          {...props}
          open={shouldOpenSelectProductPopup}
          handleSelect={handleSelectProduct}
          selectedItem={product != null ? product : {}}
          handleClose={handleSelectProductPopupClose}
          fromAssetDialog={true}
          t={t} i18n={i18n}
          productTypeCode={productTypeCode}
          assetClass={appConst.assetClass.TSCD}
          isReceivingAsset={isReceivingAsset}
        />
      )}

      {shouldOpenPopupAssetFile && (
        <VoucherFilePopup
          {...props}
          t={t}
          i18n={i18n}
          open={shouldOpenPopupAssetFile}
          handleClose={handleAssetFilePopupClose}
          itemAssetDocument={itemAssetDocument}
          getAssetDocument={getAssetDocument}
          handleUpdateAssetDocument={handleUpdateAssetDocument}
          documentType={documentType}
          item={item}
          isTSCD={true}
        />
      )}
      {/* Loại thiết vị y tế */}
      {shouldOpenDialogMedicalEquipment && (
        <MedicalEquipmentTypeDialog
          {...props}
          t={t}
          i18n={i18n}
          item={{
            name: keySearch,
          }}
          handleClose={handleClose}
          handleOKEditClose={handleClose}
          selectMedicalEquipment={selectMedicalEquipment}
          open={shouldOpenDialogMedicalEquipment}
          handleSetDataSelect={(data) =>
            handleSetDataSelect(data, "listMedicalEquipment")
          }
        />
      )}

      {shouldOpenDialogShoppingForm && (
        <ShoppingFormDialog
          {...props}
          t={t}
          i18n={i18n}
          item={{
            name: shoppingFormText,
          }}
          handleClose={handleClose}
          handleOKEditClose={handleClose}
          open={shouldOpenDialogShoppingForm}
          selectShoppingForm={selectShoppingForm}
          handleSetDataSelect={(data) =>
            handleSetDataSelect(data, "listShoppingForm")
          }
        />
      )}

      {shouldOpenAddContractDialog &&
        <ContractDialog
          {...props}
          t={t}
          i18n={i18n}
          open={shouldOpenAddContractDialog}
          handleClose={handleClose}
          contractTypeCode={appConst.TYPES_CONTRACT.CU}
          handleSelect={handleChangeSelectContract}
          isVisibleType
          item={{
            name: keySearch
          }}
          typeCodesSupplier={[appConst.TYPE_CODES.NCC_CU]}
        />}
      {/* Nhà cung cấp */}
      {shouldOpenAddSupplierDialog &&
        <SupplierDialog
          {...props}
          t={t}
          i18n={i18n}
          item={{
            ...item,
            name: keySearch || keySearchDonViBaoHanh,
          }}
          typeCodes={[
            ...(keySearchDonViBaoHanh ? [appConst.TYPE_CODES.NCC_BDSC] : []),
            ...(keySearch ? [appConst.TYPE_CODES.NCC_CU] : []),
          ]}
          open={shouldOpenAddSupplierDialog}
          handleSelectDVBH={isSupply ? handleSelectSupply : handleSelectDVBH}
          handleClose={handleClose}
        />}

      {
        shouldOpenPopupAssetAccessoryDetails &&
        <AssetAccessoryDetails
          t={t}
          i18n={i18n}
          item={itemAssetAccessory}
          assetAccessories={assetAccessories}
          open={shouldOpenPopupAssetAccessoryDetails}
          handleClose={closeAssetAccessoryDetails}
          setListSpareparts={setListSpareparts}
          isView={isView}
        />
        // closeAssetAccessoryDetails,
        // shouldOpenPopupAssetAccessoryDetails
      }

      {shouldOpenPopupSelectAttribute && (
        <SelectProductAttributespopup
          open={shouldOpenPopupSelectAttribute}
          handleSelect={handleAddAttribute}
          attributes={attributes || []}
          handleClose={handleSelectAttributePopupClose}
          t={t}
          i18n={i18n}
        />
      )}
    </div>
  );
};

export default ComponentDialog;
