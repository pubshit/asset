import { Grid, makeStyles } from "@material-ui/core";
import React from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { useTranslation } from "react-i18next";
import { checkInvalidDate } from "../../../appFunction";
import {appConst, variable} from "../../../appConst";
import SelectManagementDepartmentPopup from "app/views/Component/Department/SelectManagementDepartmentPopup";
import SelectSupplyPopup from "../../Component/SupplyUnit/SelectSupplyUnitPopup";
import SelectDepartmentPopup from "../../Component/Department/SelectDepartmentPopup";
import CustomValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import {
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import moment from "moment";
import { NumberFormatCustom } from "../../Component/Utilities";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    marginBottom: theme.spacing(2),
  },
  imgContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center", // Canh giữa ảnh theo chiều ngang và dọc
    width: 200,
    height: 200,
  },
  img: {
    maxHeight: "100%",
    maxWidth: "100%",
    objectFit: "contain", // Đảm bảo ảnh hiển thị hợp lý trong không gian giới hạn
  },
  defaultImage: {
    width: 200,
    height: 200,
    backgroundColor: "#f0f0f0", // Màu nền của ảnh mặc định
    display: "flex",
    justifyContent: "center", // Căn chữ "Chọn ảnh" chính giữa theo chiều ngang
    alignItems: "center", // Căn chữ "Chọn ảnh" chính giữa theo chiều dọc
    color: "#aaaaaa", // Màu chữ nhạt
    fontSize: "14px", // Cỡ chữ
  },
}));

const DepreciationInformation = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const {
    item,
    onChange,
    onSelectSupply,
    onSelectUseDepartment,
    onSelectSupplyPopupClose,
    onSelectManagementDepartment,
    onSelectDepartmentPopupClose,
    onSelectUseDepartmentPopupClose,
    onDateChange,
    isTTHK = true,
    itemAssetDocument,
    isView = false,
  } = props;

  const isSaveStoreNew = item?.status?.indexOrder === appConst.listStatusAssets.LUU_MOI.indexOrder && item?.id

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      onDateChange(null, name);
    }
  };

  const defaultClassname = "w-100 isTabDepreciationInformation";
  return (
    <Grid container>
      <Grid
        container
        spacing={2}
        justifyContent="space-between"
        style={{ margin: "0px 12px" }}
      >
        {/* Hàng 1 Cột 1*/}
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="mt-12" item xs={12}>
            {
              isView ?
                <TextValidator
                  className={defaultClassname}
                  label={
                    <span>
                      {t("ReceivingScrollableTabsButtonForce.issueDate")}
                    </span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  value={item?.dateOfReception ||
                    itemAssetDocument?.issueDate ? moment(item?.dateOfReception ||
                      itemAssetDocument?.issueDate).format("DD/MM/YYYY") : ''}
                /> :
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                  <CustomValidatePicker
                    className={defaultClassname}
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("ReceivingScrollableTabsButtonForce.issueDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"dateOfReception"}
                    value={
                      item?.dateOfReception ||
                      itemAssetDocument?.issueDate ||
                      new Date()
                    }
                    onChange={(date) => props.onDateChange(date, "dateOfReception")}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    minDate={new Date("01/01/1900")}
                    minDateMessage={t("general.minDateDefault")}
                    maxDate={new Date()}
                    maxDateMessage={t("general.maxDateNow")}
                    disabled={!isTTHK || item?.isReceivingAsset}
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                  />
                </MuiPickersUtilsProvider>
            }
          </Grid>
          <Grid className="mt-12" item xs={12}>
            {
              isView ?
                <TextValidator
                  className={defaultClassname}
                  label={
                    <span>
                      {t("Asset.depreciationDate")}
                    </span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  value={item?.depreciationDate ? moment(item?.depreciationDate).format("DD/MM/YYYY") : ''}
                /> :
                <CustomValidatePicker

                  className={defaultClassname}
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Asset.depreciationDate")}
                    </span>
                  }
                  inputVariant="standard"
                  autoOk={true}
                  maxDateMessage={t("EQAPlanning.messRegistrationStartDate")}
                  format="dd/MM/yyyy"
                  value={item?.depreciationDate || new Date()}
                  onChange={(date) => props.onDateChange(date, "depreciationDate")}
                  validators={["isManageAccountant"]}
                  errorMessages={t("general.required")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  minDate={
                    item?.isManageAccountant && new Date(item?.dateOfReception)
                  }
                  minDateMessage={
                    item?.isManageAccountant && t("general.depreciationDateError")
                  }
                  onBlur={() =>
                    handleBlurDate(item.depreciationDate, "depreciationDate")
                  }
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  disabled={!isTTHK && !isView}
                />
            }
          </Grid>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.accumulatedDepreciationAmount")}
              // onChange={onChange}
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
              }}
              name="accumulatedDepreciationAmount"
              value={item?.accumulatedDepreciationAmount || 0}
              disabled={!isView}
            />
          </Grid>
        </Grid>
        {/* Hàng 1 Cột 2*/}
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("InstrumentToolsList.yearPutIntoUse")}
              type="number"
              name="yearPutIntoUse"
              value={props?.item?.yearPutIntoUse || ""}
              onChange={(e) => props?.onChange(e, "yearPutIntoUse")}
              disabled={!isView}
              InputProps={{
                readOnly: isView,
              }}
            />
          </Grid>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.NumberOfYearsDepreciated")}
              onChange={onChange}
              type="text"
              name="soNamDaKhauHao"
              value={item?.soNamDaKhauHao || 0}
              validators={["matchRegexp:^.{1,255}$"]}
              errorMessages={[t("general.errorInput255")]}
              disabled={!isView}
              InputProps={{
                readOnly: isView,
              }}
            />
          </Grid>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={`${defaultClassname} inputPrice`}
              label={t("Asset.carryingAmount")}
              onChange={props.onChangeFormatNumber}
              name="carryingAmount"
              value={(isTTHK ? item?.originalCost : item?.carryingAmount) || 0}
              id="formatted-numberformat-carryingAmount"
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
              }}
              disabled={!isView}
            />
          </Grid>
        </Grid>
        {/* Hàng 1 Cột 3*/}
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={`${defaultClassname} inputPrice`}
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Asset.originalCost")}
                </span>
              }
              onKeyDown={(e) => {
                variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)
                  && e.preventDefault();
              }}
              onChange={props.onChangeFormatNumber}
              name="originalCost"
              value={props.item.originalCost || 0}
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
              }}
              validators={["required", "minNumber:0"]}
              errorMessages={[
                t("general.required"),
                t("InventoryCountVoucher.messages.originalCostError"),
              ]}
              disabled={item?.isReceivingAsset ? isView : (!isTTHK || isSaveStoreNew) && !isView}
            />
          </Grid>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.remainingYears")}
              onChange={onChange}
              type="text"
              name="soNamKhConLai"
              value={item?.soNamKhConLai || 0}
              validators={["matchRegexp:^.{1,255}$"]}
              errorMessages={[t("general.errorInput255")]}
              disabled={!isView}
              InputProps={{
                readOnly: isView,
              }}
            />
          </Grid>
        </Grid>
        {/* Hàng 1 Cột 4*/}
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={`${defaultClassname} inputPrice`}
              label={t("Asset.unitPrice")}
              onKeyDown={(e) => {
                variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)
                  && e.preventDefault();
              }}
              onChange={props.onChangeFormatNumber}
              name="unitPrice"
              value={item?.unitPrice || 0}
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
              }}
              validators={["minNumber:0"]}
              errorMessages={[t("general.minNumberError")]}
              disabled={item?.isReceivingAsset ? isView : (!isTTHK || isSaveStoreNew) && !isView}
            />
          </Grid>

          <Grid className="mt-12" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.lastYearOfDepreciation")}
              onChange={onChange}
              type="text"
              name="namKhGanNhat"
              value={item?.namKhGanNhat || ""}
              validators={["matchRegexp:^.{1,255}$"]}
              errorMessages={[t("general.errorInput255")]}
              disabled={!isView}
              InputProps={{
                readOnly: isView,
              }}
            />
          </Grid>
        </Grid>

        {item?.shouldOpenSelectDepartmentPopup && (
          <SelectManagementDepartmentPopup
            open={item.shouldOpenSelectDepartmentPopup}
            handleSelect={onSelectManagementDepartment}
            selectedItem={
              item.managementDepartment != null ? item.managementDepartment : {}
            }
            handleClose={onSelectDepartmentPopupClose}
            t={t}
            i18n={i18n}
          />
        )}

        {item?.shouldOpenSelectSupplyPopup && (
          <SelectSupplyPopup
            open={item?.shouldOpenSelectSupplyPopup}
            handleSelect={onSelectSupply}
            selectedItem={item?.supplyUnit != null ? item?.supplyUnit : {}}
            handleClose={onSelectSupplyPopupClose}
            t={t}
            i18n={i18n}
          />
        )}

        {item.shouldOpenSelectUseDepartmentPopup && (
          <SelectDepartmentPopup
            open={item.shouldOpenSelectUseDepartmentPopup}
            handleSelect={onSelectUseDepartment}
            selectedItem={item.useDepartment != null ? item.useDepartment : {}}
            handleClose={onSelectUseDepartmentPopupClose}
            t={t}
            i18n={i18n}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default DepreciationInformation;
