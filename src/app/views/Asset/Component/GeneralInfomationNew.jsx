import {
  Backdrop,
  Button,
  Checkbox,
  Chip,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid, Icon, IconButton,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { useTranslation } from "react-i18next";
import {
  filterOptions,
  getAssetStatus,
  getOptionSelected,
  getUserInformation,
  isSuccessfulResponse
} from "../../../appFunction";
import { getStockKeepingUnit } from "../../StockKeepingUnit/StockKeepingUnitService";
import { appConst, keySearch, STATUS_STORE, variable } from "../../../appConst";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  getListManagementDepartment,
  getManufacturer,
  getMedicalEquipment,
  getShoppingForm,
  searchByPage,
  uploadImageAsset,
  uploadImageAssetCreate,
} from "../AssetService";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { searchByPage as storesSearchByPage } from "../../Store/StoreService";
import AssetQRPrintNew from "../ComponentPopups/AssetQRPrintNew";
import UploadCropImagePopup from "../../page-layouts/UploadCropImagePopup";
import { toast } from "react-toastify";
import ConstantList from "../../../appConfig";
import { searchByTextDVBH } from "../../Supplier/SupplierService";
import { searchByPage as contractSearchByPage, searchByPageList } from "../../Contract/ContractService";
import {
  getByRootNew,
} from "app/views/AssetGroup/AssetGroupService";
import { searchByPage as searchByPageSupply } from "../../Supplier/SupplierService";
import DialogNote from "../../Component/DialogNote/DialogNote";
import NotePopup from "app/views/Component/NotificationPopup/NotePopup";
import viLocale from "date-fns/locale/vi";
import moment from "moment";
import { getListUserByDepartmentId } from "app/views/AssetTransfer/AssetTransferService";
import QRCodeImage from "../ComponentPopups/QRCodeImage";
import CircularProgress from "@material-ui/core/CircularProgress";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { FadeLayout, Label } from "../../Component/Utilities";
import clsx from "clsx";
import { debounce } from "utils";
import { searchByPage as searchByPageBidding } from "../../bidding-list/BiddingListService";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    marginBottom: theme.spacing(2),
  },
  imgContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center", // Canh giữa ảnh theo chiều ngang và dọc
    width: "120px",
    minHeight: "120px",
    margin: "0 auto",
  },
  img: {
    maxHeight: "100%",
    maxWidth: "100%",
    objectFit: "contain", // Đảm bảo ảnh hiển thị hợp lý trong không gian giới hạn
  },
  defaultImage: {
    width: 120,
    height: 120,
    backgroundColor: "#f0f0f0", // Màu nền của ảnh mặc định
    display: "flex",
    justifyContent: "center", // Căn chữ "Chọn ảnh" chính giữa theo chiều ngang
    alignItems: "center", // Căn chữ "Chọn ảnh" chính giữa theo chiều dọc
    color: "#aaaaaa", // Màu chữ nhạt
    fontSize: "14px", // Cỡ chữ
  },
  formControlRoot: {
    display: "flex",
    alignItems: "center",
    gap: "8px",
    width: "300px",
    flexWrap: "wrap",
    flexDirection: "row",
    border: "2px solid lightgray",
    padding: 4,
    borderRadius: "4px",
    "&> div.container": {
      gap: "6px",
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
    },
    "& > div.container > span": {
      backgroundColor: "gray",
      padding: "1px 3px",
      borderRadius: "4px",
    },
  },
  backdrop: {
    zIndex: () => {
      return theme.zIndex.drawer + 1
    },
    '& img': {
      height: "80%"
    }
  },
}));

const API_PATH = ConstantList.API_ENPOINT + "/";

const GeneralInfomationNew = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const {
    item,
    selectUnit,
    isAddAsset,
    selectStores,
    onChange,
    warrantyMonth,
    selectShoppingForm,
    onSetDataSelect,
    openSelectProductPopup,
    selectMedicalEquipment,
    openSelectUseDepartmentPopup,
    selectUsingStatus,
    selectManufacturer,
    onChangeSelect,
    onSaveImageAsset,
    isRoleAccountant = false,
    handleMapDataAsset,
    itemAssetDocument,
    handleChangeSelectContract,
    handleSelectDVBH,
    handleSelectAssetGroup,
    isView = false,
    handleKeyUp,
    handleChangeSeri,
    handleDeleteSeri,
    isEditLocal = false,
    handleSelectMadeIn,
    isDangSuDung = false,
    isTiepNhan
  } = props;
  const [selectedImage, setSelectedImage] = useState();
  const [defaultImage, setDefaultImage] = useState(true);
  const [openPrintQR, setValueOpenPrintQR] = useState(false);
  const [shouldOpenImageDialog, setShouldOpenImageDialog] = useState(false);
  const [listAsset, setListAsset] = useState([]);
  const [listDvBaoHanh, setListDvBaoHanh] = useState([]);
  const [listDvKiemDinh, setListDvKiemDinh] = useState([]);
  const [listDvKiemXa, setListDvKiemXa] = useState([]);
  const [listDvHieuChuan, setListDvHieuChuan] = useState([]);
  const [listHopDong, setListHopDong] = useState([]);
  const [listManagementDepartment, setListManagementDepartment] = useState([]);
  const [listFixedAssets, setListFixedAssets] = useState([]);
  const [listSupply, setListSupply] = useState([]);
  const [listUsePerson, setListUsePerson] = useState([]);
  const [shouldShowImgLayout, setShouldShowImgLayout] = useState(false);
  const [shouldOpenViewImageDialog, setShouldOpenViewImageDialog] = useState(false);
  const isNewStoreSave = getAssetStatus(item?.status?.indexOrder).isNewInStore
  const [query, setQuery] = useState({
    pageIndex: 0,
    pageSize: 10,
    keyword: "",
    keySearch: "",
  });
  const [loading, setLoading] = useState(false);
  const [isCheck, setIsCheck] = useState(false);
  const searchParamUserByUseDepartment = {
    departmentId: item?.useDepartment?.id,
    pageSize: 10000
  };

  const accountantOrTemporaryValue = { isManageAccountant: 1, isTemporary: 2 };
  const { listUsageState } = appConst;

  useEffect(() => {
    handleGetFixedAssets();
  }, [item?.assetGroup]);

  useEffect(() => {
    handleGetManagementDepartment();
    handleGetFixedAssets();
  }, []);

  const handleGetListDvbh = async (text = query.keyword) => {
    const newSearch = {
      pageIndex: query.pageIndex,
      pageSize: query.pageSize,
      keyword: text,
      typeCodes: [appConst.TYPE_CODES.NCC_BDSC],
    };
    try {
      let res = await searchByTextDVBH(newSearch);
      const { data } = res;
      if (data) {
        setListDvBaoHanh([]);
        if (data?.content?.length > 0) {
          setListDvBaoHanh(data.content);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleGetListDvkd = async (text = query.keyword) => {
    const newSearch = {
      pageIndex: query.pageIndex,
      pageSize: query.pageSize,
      keyword: text,
      typeCodes: [appConst.TYPE_CODES.NCC_KD],
    };
    try {
      let res = await searchByTextDVBH(newSearch);
      const { data } = res;
      if (data) {
        setListDvKiemDinh([]);
        if (data?.content?.length > 0) {
          setListDvKiemDinh(data.content);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleGetListDvhc = async (text = query.keyword) => {
    const newSearch = {
      pageIndex: query.pageIndex,
      pageSize: query.pageSize,
      keyword: text,
      typeCodes: [appConst.TYPE_CODES.NCC_HC],
    };
    try {
      let res = await searchByTextDVBH(newSearch);
      const { data } = res;
      if (data) {
        setListDvHieuChuan([]);
        if (data?.content?.length > 0) {
          setListDvHieuChuan(data.content);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };
  const handleGetListDvkx = async (text = query.keyword) => {
    const newSearch = {
      pageIndex: query.pageIndex,
      pageSize: query.pageSize,
      keyword: text,
      typeCodes: [appConst.TYPE_CODES.NCC_KX],
    };
    try {
      let res = await searchByTextDVBH(newSearch);
      const { data } = res;
      if (data) {
        setListDvKiemXa([]);
        if (data?.content?.length > 0) {
          setListDvKiemXa(data.content);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchListDvbh = (value = "", source) => {
    if (variable.listInputName.donViBaoHanh === source) {
      handleGetListDvbh(value);
      props.handleSetDataSelect(value, keySearch.keySearchDonViBaoHanh)
    }
    if (variable.listInputName.donViKiemDinh === source) {
      handleGetListDvkd(value);
    }
    if (variable.listInputName.donViHieuChuan === source) {
      handleGetListDvhc(value);
    }
    if (variable.listInputName.donViKiemXa === source) {
      handleGetListDvkx(value);
    }
  };

  const handleGetManagementDepartment = () => {
    getListManagementDepartment()
      .then(({ data }) => {
        setListManagementDepartment([]);
        if (data?.length > 0) {
          setListManagementDepartment(data);
        }
      })
      .catch((error) => console.error(error));
  };

  const handleGetListContract = (value) => {
    try {
      const searchObject = {};
      searchObject.pageIndex = query.pageIndex;
      searchObject.pageSize = query.pageSize;
      searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
      searchObject.keyword = value || "";
      props.handleSetDataSelect(value, "keySearch");
      contractSearchByPage(searchObject).then(({ data }) => {
        if (data) {
          const updatedList = data?.content?.map((item) => ({
            ...item,
            supplier: item.supplierId
              ? {
                id: item.supplierId,
                name: item.supplierName,
              }
              : null
          })) || [];

          setListHopDong(updatedList);
        }
      });
    } catch (error) { }
  };

  function flattenArray(arr) {
    let result = [];

    function flattenHelper(subArr) {
      for (const item of subArr) {
        result.push({ ...item });
        if (item.children && item.children.length > 0) {
          flattenHelper(item.children);
        }
      }
    }

    flattenHelper(arr);
    return result;
  }

  const handleGetFixedAssets = async (keyword = "") => {
    try {
      const { } = getUserInformation();
      const searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        assetClass: appConst.assetClass.TSCD,
      };

      const res = await getByRootNew(searchObject);
      const content = res?.data?.content || [];
      let newListData = flattenArray(content);
      const modifiedList = newListData.map((value) => ({
        ...value,
        name: [
          value?.name && value.name,
          value?.depreciationRate && `${value?.depreciationRate}%`,
          value?.namSuDung && value?.namSuDung + " năm",
        ]
          .filter(Boolean)
          .join(" - "),
      }));

      setListFixedAssets(modifiedList);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (item?.imageUrl) {
      const url = API_PATH + item?.imageUrl;
      setSelectedImage(url);
    } else {
      setSelectedImage();
    }
  }, [item.imageUrl]);

  const filterAutocomplete = createFilterOptions();

  const searchObjectUnit = useMemo(
    () => ({ ...appConst.OBJECT_SEARCH_MAX_SIZE }),
    []
  );

  let riskClassification =
    appConst.listRisk.find((risk) => risk.value === item?.riskClassification) ||
    "";

  const searchObjectStore = useMemo(
    () => ({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      managementDepartmentId: item?.managementDepartment?.id,
      isActive: STATUS_STORE.HOAT_DONG.code,
    }),
    [item?.managementDepartment]
  );

  const searchObjectSupply = useMemo(
    () => ({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: query?.keyword?.trim()
    }),
    [query?.keyword]
  );

  const handleChangeValueOpenQR = (newValue) => {
    setValueOpenPrintQR(newValue);
  };

  let qrInfo =
    `Mã TS: ${item?.code}\n` +
    `Mã QL: ${item?.managementCode}\n` +
    `Tên TS: ${item?.product?.name}\n` +
    `Nước SX: ${item?.madeIn}\n` +
    `Năm SX: ${item?.yearOfManufacture}\n` +
    `Ngày SD: ${item?.yearPutIntoUse}\n` +
    `PBSD: ${item?.useDepartment?.name}\n`;

  const handleOpenUploadDialog = () => {
    setShouldOpenImageDialog(true);
  };

  const handleUploadImage = async (value) => {
    let formData = new FormData();
    formData.append("uploadfile", value);
    if (props?.item?.id) {
      try {
        const { data } = await uploadImageAsset(props?.item?.id, formData);
        if (data.code === appConst.CODE.SUCCESS) {
          onSaveImageAsset(data?.data);
          setSelectedImage(`${API_PATH}/${data?.data}`);
        } else {
          toast.warn(data.message);
        }
      } catch (error) {
      } finally {
        setShouldOpenImageDialog(false);
      }
    } else {
      const data = await uploadImageAssetCreate(formData);
      let imageUrl = data?.data?.data;
      setSelectedImage(imageUrl);
      onSaveImageAsset(imageUrl);

      //Không có id lưu local
      setShouldOpenImageDialog(false);
    }
  };

  const handleDialogClose = () => {
    setShouldOpenImageDialog(false);
  };

  useEffect(() => {
    if (query.keySearch === variable.listInputName.asset) {
      handleSearchAsset();
    }
  }, [item.id, query]);

  useEffect(() => {
    setIsCheck(item?.isBhytMaMay);

  }, [item?.isBhytMaMay])

  const handleSearchAsset = async () => {
    if (!item?.id) {
      setLoading(true);
      setListAsset([]);
      let searchObject = {
        ...query,
        decisionCode: item?.isReceivingAsset ? typeof item?.decisionCode === "object" ? item?.decisionCode?.decisionCode :
          typeof item?.decisionCode === "string" ? item?.decisionCode :
            "" : ""
      }
      try {
        let res = await searchByPage(searchObject)
        const { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          setListAsset(data?.content);
        }
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    }
  };

  const handleSearch = (value = "", source) => {
    setQuery((pre) => ({ ...pre, keyword: value, keySearch: source }));
    props.handleSetDataSelect(value, "keySearch");
  };

  const getSerialNumbers = () => {
    return item?.serialNumbers || [];
  };

  const isSerialNumbersError = () => {
    const quantityAsset = item?.quantityAsset ?? null;
    const serialNumbers = item?.serialNumbers || [];
    if (
      item.isBhytMaMay && item.isClones
      && serialNumbers?.length <= 0
      && serialNumbers?.length !== quantityAsset
      && quantityAsset !== 0
    ) {
      return true;
    }

    if (quantityAsset && serialNumbers.length === 0) {
      return false;
    }

    if (
      !quantityAsset &&
      serialNumbers.length > 0 &&
      serialNumbers.length <= 10
    ) {
      return false;
    }

    if (quantityAsset && serialNumbers.length > 0) {
      return serialNumbers.length !== +quantityAsset;
    }

    return !quantityAsset && serialNumbers.length > 10;


  };

  const getSerialNumbersHelperText = (t) => {
    const quantityAsset = item?.quantityAsset ?? null;
    const serialNumbers = item?.serialNumbers || [];
    if (quantityAsset >= 1 && serialNumbers.length > 0) {
      if (serialNumbers.length > quantityAsset) {
        return t("Validator.loiNhanBan");
      } else if (serialNumbers.length < quantityAsset) {
        return t("Validator.loiSerial");
      }
    } else if (serialNumbers.length > 0) {
      return isSerialNumbersError() ? t("Asset.ErrorSerialNumbers") : "";
    } else if (
      item.isBhytMaMay && item.isClones
      && serialNumbers?.length <= 0
      && serialNumbers?.length !== quantityAsset
    ) {
      return t("Asset.bhytGeneratedErrorWhenMultiple");
    }

    return "";
  };

  const handleShowImgLayout = e => {
    setShouldShowImgLayout(true);
  }

  const handleHiddenImgLayout = e => {
    setShouldShowImgLayout(false);
  }

  const handleOpenViewImageDialog = e => {
    setShouldOpenViewImageDialog(true);
  }

  const handleCloseViewImageDialog = e => {
    setShouldOpenViewImageDialog(false);
  }
  const onInputChangeDataSearch = debounce((value, key, func) => {
    if (key) {
      func(value, key);
    } else {
      func(value)
    }
  }, 500);

  const defaultClassname = "w-100 isTabGeneralInfomation";
  const isShowMBHYT = isTiepNhan ? !isView : (!item?.id && !isView && !isEditLocal);

  return (
    <Grid container>
      <Grid
        container
        spacing={2}
        justifyContent="space-between"
        className="p-0"
        style={{ margin: "0px 12px" }}
      >
        {/* Hàng 1 Cột 1*/}
        <Grid item xs={10}>
          <Grid container spacing={2}>
            {isShowMBHYT && (
              <>
                <Grid className="mt-3" item xs={12} sm={6} md={4}>
                  <div className="flex flex-middle w-100">
                    <div className="w-100">
                      <Autocomplete
                        id="combo-box"
                        size="small"
                        options={listAsset}
                        onChange={(event, value) =>
                          handleMapDataAsset(event, value, item)
                        }
                        getOptionLabel={(option) => option.name || ""}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim();
                          return filterAutocomplete(options, params);
                        }}
                        loading={loading}
                        renderInput={(params) => (
                          <TextValidator
                            className={defaultClassname}
                            {...params}
                            label={t("Asset.searchAsset")}
                            placeholder={t("Asset.SimilarProperty")}
                            variant="standard"
                            onChange={(e) => {
                              const inputValue = e?.target?.value;
                              onInputChangeDataSearch(inputValue, keySearch.asset, handleSearch);
                            }}
                            onFocus={() => {
                              onInputChangeDataSearch("", keySearch.asset, handleSearch);
                            }}
                            value={query?.keyword || ""}
                            InputProps={{
                              ...params.InputProps,
                              endAdornment: (
                                <React.Fragment>
                                  {loading ? <CircularProgress color="inherit" size={10} /> : null}
                                  {params.InputProps.endAdornment}
                                </React.Fragment>
                              ),
                            }}
                          />
                        )}
                        noOptionsText={t("general.noOption")}
                      />
                    </div>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <DialogNote
                        content={t("Note.searchAsset")}
                        className="mt-16"
                      />
                    </div>
                  </div>
                </Grid>

                <Grid className="mt-12" item xs={12} sm={6} md={4}>
                  <div className="flex flex-middle w-100">
                    <div>
                      <FormControlLabel
                        className="w-100"
                        onChange={(event) =>
                          props.handleCheckClones(event, "addAssets")
                        }
                        name="isCheckFinancialCode"
                        control={
                          <Checkbox
                            checked={item?.isClones}
                            disabled={isView || item?.isReceivingAsset}
                          />
                        }
                        label={t("Asset.AssetCloning")}
                      />
                    </div>

                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <DialogNote content={t("Note.isCheckClone")} />
                    </div>
                  </div>
                </Grid>

                <Grid className="mt-12" item xs={12} sm={6} md={4}>
                  <FormControlLabel
                    className="w-100"
                    onChange={(event) =>
                      props.onChange(event, "isBhytMaMay")
                    }
                    name="isBhytMaMay"
                    control={
                      <Checkbox
                        checked={isCheck}
                        disabled={isView}
                      />
                    }
                    label={t("Asset.isMaBHYT")}
                  />
                </Grid>

                {item?.isClones && (
                  <Grid item xs={12} sm={6} md={4}>
                    <TextValidator
                      className={defaultClassname}
                      fullWidth
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("Asset.quantityAsset")}
                        </span>
                      }
                      onChange={onChange}
                      type="number"
                      name="quantityAsset"
                      value={item?.quantityAsset || ""}
                      validators={["required", "matchRegexp:^[1-9][0-9]*$"]}
                      errorMessages={[
                        t("general.required"),
                        t("general.minNumberError"),
                      ]}
                      disabled={isView}
                    />
                  </Grid>
                )}
              </>
            )}
            {/* 1 */}
            {!item?.isClones && (
              <>
                <Grid item xs={12} sm={6} md={4}>
                  <div className="flex flex-middle w-100">
                    <div className="w-100">
                      <TextValidator
                        className={defaultClassname}
                        label={t("Asset.managementCode")}
                        onChange={onChange}
                        type="text"
                        name="managementCode"
                        value={item?.managementCode || ""}
                        validators={["matchRegexp:^.{1,255}$"]}
                        errorMessages={[t("general.errorInput255")]}
                        InputProps={{
                          readOnly: isView
                        }}
                      />
                    </div>
                    <div>
                      <DialogNote
                        content={t("Note.noteManagementCode")}
                        className="mt-16"
                      />
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                  <div className="flex flex-middle w-100">
                    <div className="w-100">
                      <TextValidator
                        className={defaultClassname}
                        label={t("Asset.healthInsuranceCode")}
                        onChange={onChange}
                        type="text"
                        name="bhytMaMay"
                        value={item?.bhytMaMay || ""}
                        disabled={!item?.isBhytMaMay && !isView}
                        InputProps={{
                          readOnly: isView
                        }}
                      />
                    </div>
                    <div>
                      <DialogNote
                        content={t("Note.notebhytMaMay")}
                        className="mt-16"
                      />
                    </div>
                  </div>
                </Grid>

                <Grid className="mt-12" item xs={12} sm={6} md={4}>
                  <div className="flex flex-middle w-100">
                    <div>
                      <FormControlLabel
                        className="w-100"
                        onChange={(event) => onChange(event, "financialCode")}
                        name="isCheckFinancialCode"
                        control={
                          <Checkbox
                            checked={item?.isFinancialCode}
                            disabled={!isRoleAccountant || isView}
                          />
                        }
                        label={
                          <span style={{ fontSize: "14px" }}>
                            {t("Asset.Paid")}
                          </span>
                        }
                      />
                    </div>
                    <div>
                      <DialogNote
                        content={t("Note.noteIsCheckFinancialCode")}
                      />
                    </div>
                  </div>
                </Grid>
              </>
            )}
            {/* 2 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <div className="flex flex-middle w-100">
                <div className={item?.id ? "w-100" : "w-80"}>
                  <TextValidator
                    className={defaultClassname}
                    size="small"
                    InputProps={{
                      readOnly: true,
                    }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Asset.product")}
                      </span>
                    }
                    value={item?.product != null ? item?.product?.name : ""}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  // disabled={isView}
                  // onKeyPress={false}
                  />
                </div>
                {!item?.id && (
                  <div className="w-20">
                    <Button
                      size="small"
                      style={{ float: "right" }}
                      className="w-100 mt-12"
                      variant="contained"
                      color="primary"
                      onClick={openSelectProductPopup}
                    >
                      {t("general.select")}
                    </Button>
                  </div>
                )}
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("Asset.name")}
                  </span>
                }
                onChange={onChange}
                type="text"
                name="name"
                value={item?.name || ""}
                validators={["required", "matchRegexp:^.{1,255}$"]}
                errorMessages={[
                  t("general.required"),
                  t("general.errorInput255"),
                ]}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            {!item?.isClones && (
              <Grid item xs={12} sm={6} md={4}>
                <TextValidator
                  className={defaultClassname}
                  label={t("Asset.AccountingCode")}
                  type="text"
                  name="financialCode"
                  value={item?.financialCode || ""}
                  disabled={!item?.financialCode && !isView}
                  InputProps={{
                    readOnly: isView,
                  }}
                />
              </Grid>
            )}

            {/* 3 */}
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.decisionCode")}
                type="text"
                name="decisionCode"
                value={
                  typeof item?.decisionCode === "object" ? item?.decisionCode?.decisionCode || "" :
                    typeof item?.decisionCode === "string" ? item?.decisionCode || "" :
                      ""
                }
                disabled
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.symbolCode")}
                onChange={onChange}
                type="text"
                name="symbolCode"
                value={item?.symbolCode ? item?.symbolCode : ""}
                defaultValue={item?.symbolCode ? item?.symbolCode : ""}
                // disabled={isView}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={!item?.isClones ? 4 : 12}>
              {!item?.isClones ? (
                <TextValidator
                  className={defaultClassname}
                  label={t("Asset.serialNumber")}
                  onChange={onChange}
                  type="text"
                  name="serialNumber"
                  value={item?.serialNumber || ""}
                  InputProps={{
                    readOnly: isView,
                  }}
                />
              ) : (
                <Autocomplete
                  multiple
                  value={getSerialNumbers()}
                  clearIcon={false}
                  id="tags-standard"
                  options={[]}
                  freeSolo
                  disableClearable
                  name="serialNumbers"
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        variant="outlined"
                        label={option}
                        {...getTagProps({ index })}
                        onDelete={() => handleDeleteSeri(index)}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextValidator
                      className={defaultClassname}
                      {...params}
                      label={t("Asset.serialNumber")}
                      value={item?.valueSerialNumber || ""}
                      onChange={(event, values) =>
                        handleChangeSeri(event, values)
                      }
                      onKeyUp={(event, value) => handleKeyUp(event, t)}
                      error={isSerialNumbersError()}
                      helperText={getSerialNumbersHelperText(t)}
                    />
                  )}
                />
              )}
            </Grid>
            {/* 4 */}
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={"Model"}
                onChange={onChange}
                type="text"
                name="model"
                value={item?.model ? item?.model : ""}
                defaultValue={item?.model ? item?.model : ""}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.brand")}
                onChange={onChange}
                type="text"
                name="brand"
                value={item?.brand || ""}
                InputProps={{
                  readOnly: isView
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                isFocus={true}
                searchFunction={() => { }}
                label={t("Asset.usingStatus")}
                listData={listUsageState}
                displayLable={"name"}
                selectedOptionKey="code"
                disabled={isAddAsset}
                readOnly={isView}
                value={item?.usageState || null}
                onSelect={selectUsingStatus}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                isFocus={true}
                label={t("Asset.manufacturer")}
                searchFunction={getManufacturer}
                searchObject={searchObjectUnit}
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                defaultValue={item?.manufacturer}
                typeReturnFunction="category"
                displayLable={"name"}
                readOnly={isView}
                value={item?.manufacturer || null}
                onSelect={selectManufacturer}
                filterOptions={(options, params) => {
                  options
                    .filter((option) => option.name.includes(params.inputValue))
                    .sort((a, b) => {
                      // Sắp xếp các tùy chọn sao cho các tùy chọn có giá trị khớp từ trái qua phải được đẩy lên trước
                      const indexOfA = a.name
                        .toLowerCase()
                        .indexOf(params.inputValue.toLowerCase());
                      const indexOfB = b.name
                        .toLowerCase()
                        .indexOf(params.inputValue.toLowerCase());
                      if (indexOfA === 0 && indexOfB !== 0) {
                        return -1;
                      } else if (indexOfA !== 0 && indexOfB === 0) {
                        return 1;
                      } else {
                        return 0;
                      }
                    });
                  return filterOptions(options, params, true, "name");
                }}
                noOptionsText={t("general.noOption")}
              />
            </Grid>

            {/* 5 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    label={
                      <span>
                        {t("Asset.madeIn")}
                      </span>
                    }
                    className="w-100"
                    InputProps={{
                      readOnly: true,
                    }}
                    value={item?.madeIns ? item?.madeIns?.name : ''}
                  /> :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    size="small"
                    options={appConst.madeIn}
                    onChange={(e, value) =>
                      handleSelectMadeIn(value, "isBuyLocally")
                    }
                    value={item?.madeIns || null}
                    getOptionLabel={(option) => option.name || ""}
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={t("Asset.madeIn")}
                        value={item?.madeIns ? item?.madeIns?.name : ''}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                  />
              }
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.Nation")}
                onChange={onChange}
                type="text"
                name="madeIn"
                value={item?.madeIn || ""}
                disabled={item?.isBuyLocally && !isView}
                InputProps={{
                  readOnly: isView
                }}
              />
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("InstrumentToolsList.circulationNumber")}
                onChange={onChange}
                type="text"
                name="circulationNumber"
                value={item?.circulationNumber || ""}
                InputProps={{
                  readOnly: isView
                }}
              />
            </Grid>

            {/* 5 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    fullWidth
                    label={
                      <span>
                        {t("InstrumentToolsList.riskClassification")}
                      </span>
                    }
                    InputProps={{
                      readOnly: true,
                    }}
                    value={riskClassification ? riskClassification?.riskType : ''}
                  /> :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    fullWidth
                    size="small"
                    options={appConst.listRisk}
                    onChange={(e, riskClassification) =>
                      onChangeSelect(
                        riskClassification?.value,
                        "riskClassification"
                      )
                    }
                    getOptionLabel={(option) => option.riskType}
                    value={riskClassification || null}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterAutocomplete(options, params);
                    }}
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={t("InstrumentToolsList.riskClassification")}
                        variant="standard"
                        value={riskClassification ? riskClassification?.riskType : ''}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                  />
              }
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.yearOfManufacture")}
                type="number"
                name="yearOfManufacture"
                onKeyDown={(e) =>
                  variable.regex.numberExceptThisSymbols.includes(e.key) &&
                  e.preventDefault()
                }
                onChange={onChange}
                value={item?.yearOfManufacture ? item?.yearOfManufacture : ""}
                validators={["minNumber: 1900", "matchRegexp:^[1-9][0-9]{3}$"]}
                errorMessages={[
                  t("general.yearOfManufactureError1900"),
                  t("general.yearOfManufactureError"),
                ]}
                InputProps={{
                  readOnly: isView
                }}
              />
            </Grid>

            {/* 6 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                isFocus={true}
                label={t("InstrumentToolsList.shoppingForm")}
                searchFunction={getShoppingForm}
                searchObject={searchObjectUnit}
                listData={item?.listShoppingForm}
                setListData={(data) =>
                  onSetDataSelect(data, "listShoppingForm")
                }
                readOnly={isView}
                typeReturnFunction="category"
                displayLable={"name"}
                value={item?.shoppingForm || null}
                onSelect={selectShoppingForm}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                noOptionseText={t("general.noOption")}
                onInputChange={(event) => props.handleSetDataSelect(event.target.value, "shoppingFormText")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                label={<span>
                        <span className="colorRed">*</span>
                  {t("InstrumentToolsList.unit")}
                      </span>}
                searchFunction={getStockKeepingUnit}
                searchObject={searchObjectUnit}
                listData={item?.listUnit}
                setListData={(data) => onSetDataSelect(data, "listUnit")}
                onInputChange={(event) => props.handleSetDataSelect(event.target.value, "keySearch")}
                readOnly={isView}
                displayLable={"name"}
                value={item?.unit || ""}
                onSelect={selectUnit}
                filterOptions={(options, params) => filterOptions(options, params, true, "name")}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </Grid>

            {/* 7 */}
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={<span>{t("InstrumentToolsList.lotNumber")}</span>}
                onChange={onChange}
                value={item?.lotNumber || itemAssetDocument?.billNumber || ""}
                type="text"
                name="lotNumber"
                InputProps={{
                  readOnly: isView,
                }}
                disabled={item?.isReceivingAsset}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatedDatePicker
                label={t("ReceivingScrollableTabsButtonForce.dateBill")}
                readOnly={isView}
                value={item?.ngayHoaDon ? item?.ngayHoaDon : null}
                onChange={(date) => props.onDateChange(date, "ngayHoaDon")}
                invalidDateMessage={t("general.invalidDateFormat")}
                minDate={new Date("01/01/1900")}
                minDateMessage={t("general.minDateDefault")}
                maxDate={new Date()}
                maxDateMessage={t("general.maxDateNow")}
                disabled={item?.isReceivingAsset}
              />
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    fullWidth
                    label={t("Asset.someContracts")}
                    InputProps={{
                      readOnly: true,
                    }}
                    value={item?.contract?.contractName || itemAssetDocument?.contract?.contractName || ''}
                  />
                  :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    fullWidth
                    size="small"
                    name="contract"
                    options={listHopDong}
                    onChange={(e, value) =>
                      handleChangeSelectContract(value, "contract")
                    }
                    getOptionSelected={(option, value) => option?.contractId === value?.contractId}
                    value={item?.contract || itemAssetDocument?.contract || null}
                    getOptionLabel={(option) => option.contractName || ""}
                    filterOptions={(options, params) =>
                      filterOptions(options, params, true, "contractName")
                    }
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={t("Asset.someContracts")}
                        variant="standard"
                        value={item?.contract?.contractName || itemAssetDocument?.contract?.contractName || ''}
                        onChange={(e) => onInputChangeDataSearch(e?.target?.value, null, handleGetListContract)}
                        onFocus={() => {
                          onInputChangeDataSearch("", null, handleGetListContract);
                        }}
                      />
                    )}
                    disabled={item?.isReceivingAsset}
                  />
              }
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <ValidatedDatePicker
                label={t("Asset.DayContract")}
                name={"warrantyExpiryDate"}
                value={
                  item?.contract?.contractDate ||
                  itemAssetDocument?.contract?.contractDate ||
                  null
                }
                readOnly={isView}
                disabled
              />
            </Grid>

            {/* 8 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isTabGeneralInfomation
                label={t("component.Supply.name")}
                searchFunction={searchByPageSupply}
                searchObject={searchObjectSupply}
                listData={listSupply}
                setListData={setListSupply}
                displayLable={"name"}
                readOnly={isView}
                onInputChange={(event) => props.handleSetDataSelect(event.target.value, "keySearch")}
                value={item?.supplyUnit || null}
                onSelect={props.onSelectSupply}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                noOptionsText={t("general.noOption")}
                disabled={item?.isReceivingAsset || (!isView && item?.contract?.id && item?.contract?.supplier?.id) || item?.decisionCode?.decisionCode || item?.decisionCode}
              />
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    label={t("Asset.managementDepartment")}
                    className="w-100"
                    InputProps={{
                      readOnly: true,
                    }}
                    value={item?.managementDepartment ? item?.managementDepartment?.name : ''}
                  />
                  :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    fullWidth
                    size="small"
                    options={listManagementDepartment || []}
                    name="managementDepartment"
                    onChange={(e, managementDepartment) =>
                      onChangeSelect(managementDepartment, "managementDepartment")
                    }
                    getOptionSelected={getOptionSelected}
                    getOptionLabel={(option) => option.text || option.name || ""}
                    value={item?.managementDepartment || null}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterAutocomplete(options, params);
                    }}
                    renderInput={(params) => (
                      <TextValidator
                        className={defaultClassname}
                        {...params}
                        label={
                          <span>
                            <span className="colorRed">*</span>
                            {t("Asset.managementDepartment")}
                          </span>
                        }
                        value={item?.managementDepartment?.text || item?.managementDepartment?.name || ""}
                        variant="standard"
                        validators={["required"]}
                        errorMessages={[t("general.required")]}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                    disabled={item?.isReceivingAsset || Boolean(item?.id)}
                  />
              }
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("Asset.store")}
                  </span>
                }
                searchFunction={storesSearchByPage}
                searchObject={searchObjectStore}
                listData={item?.listStore}
                setListData={(data) => onSetDataSelect(data, "listStore")}
                onInputChange={(event) => props.handleSetDataSelect(event.target.value, "keySearch")}
                displayLable={"name"}
                readOnly={isView}
                value={item?.store || null}
                onSelect={selectStores}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                noOptionsText={t("general.noOption")}
                disabled={(!isView && !isAddAsset) || !item?.managementDepartment?.id || item?.isReceivingAsset}
                validators={isAddAsset ? ["required"] : []}
                errorMessages={[t("general.required")]}
              />
            </Grid>

            {/* 9 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    label={t("Asset.maintenanceWarrantyUnit")}
                    className="w-100"
                    InputProps={{
                      readOnly: true,
                    }}
                    value={item?.donViBaoHanh ? item?.donViBaoHanh?.name : ''}
                  />
                  :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    fullWidth
                    size="small"
                    name="donViBaoHanh"
                    options={listDvBaoHanh}
                    onChange={(e, value) =>
                      handleSelectDVBH(value, variable.listInputName.donViBaoHanh)
                    }
                    value={item?.donViBaoHanh || null}
                    getOptionSelected={getOptionSelected}
                    getOptionLabel={(option) => option.name || ""}
                    filterOptions={(options, params) =>
                      filterOptions(options, params, true, "name")
                    }
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={t("Asset.maintenanceWarrantyUnit")}
                        variant="standard"
                        value={item?.donViBaoHanh?.name || ''}
                        onChange={(e) => {
                          const inputValue = e?.target?.value;
                          onInputChangeDataSearch(inputValue, variable.listInputName.donViBaoHanh, handleSearchListDvbh);
                        }}
                        onFocus={() => {
                          onInputChangeDataSearch("", variable.listInputName.donViBaoHanh, handleSearchListDvbh);
                        }}
                      />
                    )}
                  />
              }
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatedDatePicker
                id="mui-pickers-date"
                className={defaultClassname}
                label={<Label isRequired={Boolean(warrantyMonth > 0)}>{t("Asset.acceptanceDate")}</Label>}
                readOnly={isView}
                autoOk
                format="dd/MM/yyyy"
                name="acceptanceDate"
                value={item?.acceptanceDate || null}
                onChange={props.handleDateChange}
                invalidDateMessage={t("general.invalidDateFormat")}
                validators={Number(warrantyMonth) > 0 ? ["required"] : []}
                errorMessages={[t("general.required"),]}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.warrantyMonth")}
                onChange={(e) => onChange(e, "warrantyMonth")}
                type="number"
                name="warrantyMonth"
                value={warrantyMonth || ""}
                onKeyDown={(e) =>
                  variable.regex.numberExceptThisSymbols.includes(e.key) &&
                  e.preventDefault()
                }
                validators={["minNumber:0", "maxNumber:1000"]}
                errorMessages={[
                  t("general.NumberOfMonthsOfWarranty0"),
                  t("general.NumberOfMonthsOfWarranty1000"),
                ]}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatePicker
                className="w-100"
                margin="none"
                id="mui-pickers-date"
                label={t("Asset.warrantyExpiryDate")}
                inputVariant="standard"
                type="text"
                readOnly={isView}
                format="dd/MM/yyyy"
                name={"warrantyExpiryDate"}
                value={item?.warrantyExpiryDate || null}
                minDate={new Date("01/01/1900")}
                minDateMessage={t("general.minDateMessage")}
                maxDateMessage={t("general.maxDateMessage")}
                invalidDateMessage={t("general.invalidDateFormat")}
                disabled
                onChange={() => { }}
              />
            </Grid>

            {/* 10 */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              {
                isView ?
                  <TextValidator
                    fullWidth
                    label={t("Asset.fixedAssetsClassification")}
                    InputProps={{
                      readOnly: true,
                    }}
                    value={item?.assetGroup?.name || ''}
                  />
                  :
                  <Autocomplete
                    className="mt-3"
                    id="combo-box"
                    fullWidth
                    size="small"
                    options={listFixedAssets || []}
                    name="assetGroup"
                    onChange={(e, assetGroup) =>
                      handleSelectAssetGroup(assetGroup, "assetGroup")
                    }
                    getOptionSelected={
                      (option, value) => option?.id === value?.id || option?.assetGroupId === value?.assetGroupId
                    }
                    getOptionLabel={(option) => option.name || ""}
                    value={item?.assetGroup || ""}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterAutocomplete(options, params);
                    }}
                    renderInput={(params) => (
                      <TextValidator
                        className={defaultClassname}
                        {...params}
                        label={
                          <span>
                            <span style={{ color: "red" }}>*</span>
                            {t("Asset.fixedAssetsClassification")}
                          </span>
                        }
                        value={item?.assetGroup?.name || ""}
                        variant="standard"
                        validators={["required"]}
                        errorMessages={[t("general.required")]}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                  // disabled={isView}
                  />
              }
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                label={t("MedicalEquipmentType.title")}
                searchFunction={getMedicalEquipment}
                searchObject={searchObjectUnit}
                listData={item?.listMedicalEquipment}
                setListData={(data) =>
                  onSetDataSelect(data, "listMedicalEquipment")
                }
                onInputChange={(e) => props.handleSetDataSelect(e.target.value, "keySearch")}
                readOnly={isView}
                typeReturnFunction="category"
                displayLable={"name"}
                value={item?.medicalEquipment || null}
                onSelect={selectMedicalEquipment}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.installationLocation")}
                onChange={onChange}
                type="text"
                name="installationLocation"
                value={item?.installationLocation || ""}
                validators={["matchRegexp:^.{1,255}$"]}
                errorMessages={[t("general.errorInput255")]}
                InputProps={{
                  readOnly: isView && !isDangSuDung,
                }}
              />
            </Grid>
            <Grid className="mt-3" item xs={12} md={12} lg={8}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.note")}
              onChange={onChange}
              type="text"
              name="note"
              value={item?.note || ""}
              validators={["matchRegexp:^.{1,255}$"]}
              errorMessages={[t("general.errorInput255")]}
              InputProps={{
                readOnly: isView && !isDangSuDung,
              }}
            />
          </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2} >
          <Grid item xs={12}>
            {!item?.isClones && (
              <div
                style={{
                  overflow: "hidden",
                  whiteSpace: "nowrap",
                  textOverflow: "ellipsis",
                  textAlign: "center",
                  fontWeight: "600",
                }}
                title={
                  item?.listCodes !== null && item?.listCodes !== ""
                    ? item?.listCodes
                    : item?.code
                }
              >
                {!item?.numberOfClones || item?.numberOfClones === 1
                  ? item?.code
                  : null}
              </div>
            )}
          </Grid>
          <Grid className="mt-12 mb-12" item xs={12}>
            <div
              style={{
                overflow: "hidden",
                whiteSpace: "nowrap",
                textOverflow: "ellipsis",
                textAlign: "center",
                fontWeight: "600",
              }}
            >
              {item?.status?.name || null}
            </div>
          </Grid>


          <div
            className={clsx(
              classes.imgContainer,
              "position-relative"
            )}
            onMouseEnter={handleShowImgLayout}
            onMouseLeave={handleHiddenImgLayout}
          >
            <FadeLayout
              show={shouldShowImgLayout}
              blur
              fullSize
              justifyContent="space-evenly"
              alignItems="center"
              backgroundColor="fade"
            >
              {(item?.imageUrl) && (
                <IconButton onClick={handleOpenViewImageDialog}>
                  <Icon fontSize="small" color="primary">
                    visibility
                  </Icon>
                </IconButton>
              )}
              {(!isView || (isView && isDangSuDung)) && (
                <IconButton onClick={handleOpenUploadDialog}>
                  <Icon fontSize="small" color="primary">
                    upload
                  </Icon>
                </IconButton>
              )}
            </FadeLayout>
            {item?.imageUrl // imageUrl là biến chứa đường dẫn của ảnh
              ? (
                <img
                  src={selectedImage}
                  alt="Image"
                  className={classes.img}
                  onError={() => {
                    // Nếu ảnh không thể render được, sẽ hiển thị ảnh mặc định
                    setDefaultImage(true);
                  }}
                />
              ) : (
                defaultImage && ( // Nếu không có đường dẫn của ảnh hoặc ảnh không render được, hiển thị ảnh mặc định
                  <div className={classes.defaultImage}>
                    <span>{t("Asset.ChooseAPhoto")}</span>
                  </div>
                )
              )}
          </div>
          {props.item?.id && !item?.isReceivingAsset &&
            <>
              <div
                className={classes.imgContainer}
              >
                <div className="flex-column flex-middle">
                  <div style={{ height: 155, width: 175 }}>
                    <QRCodeImage item={item} height={175} width={175} />
                  </div>
                  <div>
                    <Button
                      size="small"
                      variant="text"
                      color="primary"
                      onClick={() => handleChangeValueOpenQR(true)}
                    >
                      {t("Asset.PrintQRCode")}
                    </Button>
                  </div>
                  {openPrintQR && (
                    <AssetQRPrintNew
                      t={t}
                      i18n={i18n}
                      handleClose={handleChangeValueOpenQR}
                      open={openPrintQR}
                      qrValue={qrInfo}
                      item={item}
                    />
                  )}
                </div>
              </div>
            </>
          }
          <RadioGroup
            aria-label="isManageAccountant"
            name="isManageAccountant"
            onChange={(isManageAccountant) =>
              props.handleChange(isManageAccountant, "isManageAccountant")
            }
            style={{ display: "inline" }}
          >
            <div className="flex flex-middle">
              <FormControlLabel
                value={accountantOrTemporaryValue.isManageAccountant}
                control={
                  <Radio checked={props.item?.isManageAccountant} disabled={isView} />
                }
                label={t("Asset.ManageAccountant.manageAccountant")}
                style={{ marginRight: 5 }}
              />
              <NotePopup
                typeIndex={appConst.NOTE_INDEX.IS_MANAGE_ACCOUNTANT}
                assetClass={appConst.assetClass.TSCD}
              />
            </div>

            <div className="flex flex-middle">
              <FormControlLabel
                value={accountantOrTemporaryValue.isTemporary}
                control={
                  <Radio checked={item?.isTemporary} disabled={isView} />
                }
                label={t("Asset.Temporary.temporary")}
                style={{ marginRight: 5 }}
              />
              <NotePopup
                typeIndex={appConst.NOTE_INDEX.IS_TEMPOARY}
                assetClass={appConst.assetClass.TSCD}
              />
            </div>
          </RadioGroup>
        </Grid>
        {item?.useDepartment?.id && (
          <>
            <Grid item xs={12} sm={12} md={12}>
              <Grid className="mt-12" item xs={12}>
                <label>{t("Asset.UsageInformation")}</label>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={6} md={3}>
              <FormControl component="fieldset">
                <FormLabel component="legend" disabled={true}>
                  {t("Asset.PropertyGrantedTo")}
                </FormLabel>
                <FormControlLabel
                  style={{ float: "right" }}
                  value={item?.allocationFor}
                  className="mb-16"
                  name="allocationFor"
                  onChange={(e) => onChange(e, "allocationFor")}
                  control={<Checkbox checked={item?.allocationFor} disabled={!isDangSuDung} />}
                  label={t("Asset.allocationFor.person")}
                />
              </FormControl>

            </Grid>

            <Grid className="mt-3" item xs={12} sm={6} md={3}>
              <Grid className="mt-12">
                <div className="flex flex-middle">
                  <div className={isView ? "w-100" : "w-80"}>
                    <TextValidator
                      className={defaultClassname}
                      size="small"
                      InputProps={{
                        readOnly: isView,
                      }}
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("Asset.useDepartment")}
                        </span>
                      }
                      value={item.useDepartment.name || ""}
                      disabled={!isView}
                    />
                  </div>
                  {!isView && (
                    <div className="w-20">
                      <Button
                        size="small"
                        style={{ float: "right" }}
                        className="w-20 mt-10"
                        variant="contained"
                        color="primary"
                        onClick={openSelectUseDepartmentPopup}
                        disabled={!isAddAsset}
                      >
                        {t("general.select")}
                      </Button>
                    </div>
                  )}
                </div>
              </Grid>
            </Grid>

            <Grid item md={3} sm={6} xs={12} >
              <Grid className="mt-12" item xs={12}>
                <AsynchronousAutocompleteSub
                  className={defaultClassname}
                  label={<Label isRequired={item?.allocationFor}>{t("Asset.User")}</Label>}
                  listData={listUsePerson}
                  setListData={setListUsePerson}
                  disabled={!isDangSuDung || !item?.allocationFor}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByUseDepartment}
                  readOnly={isView && !isDangSuDung}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={item?.usePerson ?? null}
                  onSelect={usePerson => props?.handleSelectUsePerson(usePerson)}
                  noOptionsText={t("general.noOption")}
                  validators={item?.allocationFor ? ["required"] : []}
                  errorMessages={item?.allocationFor ? [t('general.required')] : []}
                />
              </Grid>
            </Grid>
          </>
        )}
        <Grid className="mt-12" item xs={12} sm={6} md={3}>
          <Grid className="mt-12" item xs={12}></Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Grid className="mt-12" item xs={12}>
            <label className="font-weight-600">
              {t("Asset.TrackingInformation")}
            </label>
          </Grid>
        </Grid>

        {/* Kiểm định */}
        <Grid item xs={12} sm={6} md={2}>
          <Grid item xs={12}>
            <ValidatedDatePicker
              label={t("Asset.inspectionDate")}
              name={"ngayKiemDinh"}
              readOnly={isView}
              value={item?.ngayKiemDinh || null}
              onChange={(date) => props.onDateChange(date, "ngayKiemDinh")}
              disabled={!isNewStoreSave}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <ValidatedDatePicker
              label={t("Asset.VerificationCalibrationCycle")}
              readOnly={isView}
              name={"hanKiemDinh"}
              value={item?.hanKiemDinh || null}
              maxDate={new Date("12/31/2100")}
              maxDateMessage={t("Asset.errorMessage.maxDateDefault")}
              invalidDateMessage={t("general.invalidDateFormat")}
              onChange={(date) => props.onDateChange(date, "hanKiemDinh")}
              disabled={!isNewStoreSave}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          {
            isView ?
              <TextValidator
                label={t("Asset.CalibrationTestingUnit")}
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                value={item?.donViKiemDinh ? item?.donViKiemDinh?.name : ''}
              />
              :
              <Autocomplete
                id="combo-box"
                className="mt-3"
                fullWidth
                size="small"
                name="donViKiemDinh"
                options={listDvKiemDinh}
                onChange={(e, value) =>
                  onChangeSelect(value, variable.listInputName.donViKiemDinh)
                }
                value={item?.donViKiemDinh || ""}
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim();
                  return filterAutocomplete(options, params);
                }}
                disabled={!isNewStoreSave}
                noOptionsText={t("general.noOption")}
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params}
                    label={t("Asset.CalibrationTestingUnit")}
                    variant="standard"
                    value={item?.donViKiemDinh?.name || ""}
                    onChange={(e) => {
                      const inputValue = e?.target?.value;
                      onInputChangeDataSearch(inputValue, variable.listInputName.donViKiemDinh, handleSearchListDvbh);
                    }}
                    onFocus={() => {
                      onInputChangeDataSearch("", variable.listInputName.donViKiemDinh, handleSearchListDvbh);
                    }}
                  />
                )}
              />
          }
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.TextNumber")}
              type="text"
              name="thongTuKiemDinh"
              value={item?.thongTuKiemDinh || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Grid item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.inspectionStampNumber")}
              type="text"
              name="soTemKiemDinh"
              value={item?.soTemKiemDinh || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView,
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>

        {/* Kiểm xạ */}
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <ValidatedDatePicker
              label={t("Asset.radiationControlDate")}
              name={"ngayKiemXa"}
              readOnly={isView}
              value={item?.ngayKiemXa || null}
              onChange={(date) => props.onDateChange(date, "ngayKiemXa")}
              disabled={!isNewStoreSave}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <ValidatedDatePicker
              className="w-100"
              margin="none"
              id="mui-pickers-date"
              label={t("Asset.RadiationControlPeriod")}
              readOnly={isView}
              name={"hanKiemXa"}
              value={item?.hanKiemXa || null}
              onChange={props.onDateChange}
              disabled={!isNewStoreSave}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          {
            isView ?
              <TextValidator
                label={t("Asset.RadiationControlUnit")}
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                value={item?.donViKiemXa ? item?.donViKiemXa?.name : ''}
              />
              :
              <Autocomplete
                id="combo-box"
                className="mt-3"
                fullWidth
                size="small"
                name="donViKiemXa"
                options={listDvKiemXa}
                onChange={(e, value) =>
                  onChangeSelect(value, variable.listInputName.donViKiemXa)
                }
                value={item?.donViKiemXa || ""}
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim();
                  return filterAutocomplete(options, params);
                }}
                noOptionsText={t("general.noOption")}
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params}
                    label={t("Asset.RadiationControlUnit")}
                    variant="standard"
                    value={item?.donViKiemXa?.name || ''}
                    onChange={(e) => {
                      const inputValue = e?.target?.value;
                      onInputChangeDataSearch(inputValue, variable.listInputName.donViKiemXa, handleSearchListDvbh);
                    }}
                    onFocus={() => {
                      onInputChangeDataSearch("", variable.listInputName.donViKiemXa, handleSearchListDvbh);
                    }}
                  />
                )}
                disabled={!isNewStoreSave}
              />
          }
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.radiationControlCircular")}
              type="text"
              name="thongTuKiemXa"
              value={item?.thongTuKiemXa || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.radiationControlStampNumber")}
              type="text"
              name="soTemKiemXa"
              value={item?.soTemKiemXa || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView,
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>

        {/* Hiệu chuẩn */}
        <Grid item xs={12} sm={6} md={2}>
          {
            isView ?
              <TextValidator
                label={
                  <span>
                    {t("Asset.calibrationDate")}
                  </span>
                }
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                value={item?.ngayHieuChuan ? moment(item?.ngayHieuChuan).format("DD/MM/YYYY") : ''}
              /> :
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <ValidatedDatePicker
                  className="w-100"
                  margin="none"
                  id="mui-pickers-date"
                  label={t("Asset.calibrationDate")}
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={"ngayHieuChuan"}
                  value={item?.ngayHieuChuan || null}
                  // minDate={
                  //   item?.dateOfReception
                  //     ? new Date(item.dateOfReception)
                  //     : new Date("01/01/1900")
                  // }
                  // minDateMessage={
                  //   item?.dateOfReception
                  //     ? t("Asset.errorMessage.minDateHieuChuan")
                  //     : t("Asset.errorMessage.minDateDefault")
                  // }
                  maxDate={new Date("12/31/2100")}
                  maxDateMessage={t("Asset.errorMessage.maxDateDefault")}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  onChange={(date) => props.onDateChange(date, "ngayHieuChuan")}
                  // disabled={isView}
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  disabled={!isNewStoreSave}
                />
              </MuiPickersUtilsProvider>
          }
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <ValidatedDatePicker
            label={t("Asset.CalibrationCycle")}
            readOnly={isView}
            name={"hanHieuChuan"}
            value={item?.hanHieuChuan || null}
            maxDate={new Date("12/31/2100")}
            maxDateMessage={t("Asset.errorMessage.maxDateDefault")}
            invalidDateMessage={t("general.invalidDateFormat")}
            onChange={(date) => props.onDateChange(date, "hanHieuChuan")}
            disabled={!isNewStoreSave}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} className="mt-3">
          {
            isView ?
              <TextValidator
                label={t("Asset.CalibrationUnit")}
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                value={item?.donViHieuChuan?.name || ''}
              /> :
              <Autocomplete
                id="combo-box"
                fullWidth
                size="small"
                name="donViHieuChuan"
                options={listDvHieuChuan}
                onChange={(e, value) =>
                  onChangeSelect(value, variable.listInputName.donViHieuChuan)
                }
                value={item?.donViHieuChuan || ""}
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim();
                  return filterAutocomplete(options, params);
                }}
                noOptionsText={t("general.noOption")}
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params}
                    label={t("Asset.CalibrationUnit")}
                    variant="standard"
                    value={item?.donViHieuChuan?.name || ''}
                    onChange={(e) => {
                      const inputValue = e?.target?.value;
                      onInputChangeDataSearch(inputValue, variable.listInputName.donViHieuChuan, handleSearchListDvbh);
                    }}
                    onFocus={() => {
                      onInputChangeDataSearch("", variable.listInputName.donViHieuChuan, handleSearchListDvbh);
                    }}
                  />
                )}
                disabled={!isNewStoreSave}
              />
          }
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Grid className="" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.TextNumberCalibratio")}
              type="text"
              name="thongTuHieuChuan"
              value={item?.thongTuHieuChuan || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Grid className="" item xs={12}>
            <TextValidator
              className={defaultClassname}
              label={t("Asset.calibrationStampNumber")}
              type="text"
              name="soTemHieuChuan"
              value={item?.soTemHieuChuan || ""}
              onChange={onChange}
              InputProps={{
                readOnly: isView,
              }}
              disabled={!isNewStoreSave && !isView}
            />
          </Grid>
        </Grid>


        <Backdrop className={classes.backdrop} open={shouldOpenViewImageDialog} onClick={handleCloseViewImageDialog}>
          <img
            src={selectedImage}
            alt="Image"
            onError={() => {
              setDefaultImage(true);
            }}
          />
        </Backdrop>

        {shouldOpenImageDialog && (
          <UploadCropImagePopup
            t={t}
            i18n={i18n}
            handleClose={handleDialogClose}
            handleUpdate={handleUploadImage}
            open={shouldOpenImageDialog}
            acceptType="png;jpg;gif;jpeg"
            type="IMAGE_ASSET"
          />
        )}
      </Grid>
    </Grid>
  );
};

export default GeneralInfomationNew;
