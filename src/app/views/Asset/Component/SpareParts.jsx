import { Button, Grid } from '@material-ui/core';
import CustomMaterialTable from 'app/views/CustomMaterialTable';
import { MTableToolbar } from 'material-table';
import React from 'react'

function SpareParts(props) {
  const {
    t,
    isView,
    rowStyle,
    columnSparePart,
    assetAccessories,
    handleAddNewSparePart,
    columnsSparePartActions,
  } = props;
  return (
    <div>
        <Grid container>
          {!isView && (
            <Grid item md={12} sm={12} xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAddNewSparePart}
                className="mt-10"
              >
                {t("+")}
              </Button>
            </Grid>
          )}
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <CustomMaterialTable
              data={assetAccessories ? assetAccessories : []}
              columns={columnSparePart}
              columnActions={columnsSparePartActions}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "343px",
                minBodyHeight: "343px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
    </div>
  )
}

export default SpareParts
