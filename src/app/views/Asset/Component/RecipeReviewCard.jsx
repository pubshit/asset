import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { Add, Remove } from "@material-ui/icons";
import { variable } from "../../../appConst";


const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		width: "100%", // backgroundColor: theme.palette.background.paper,
	},
	card: {
		margin: "1.3rem auto",
		boxShadow: "none",
	},
	hidden: {
		display: "none",
	},
	title: {
		padding: "0.5rem",
		whiteSpace: "nowrap",
		overflow: "hidden",
		textOverflow: "ellipsis",
		textTransform: "uppercase",
		color: theme.custom.primary.dark || variable.css.primary.dark,
		fontWeight: "600",
		fontSize: '16px'
	},
	actions: {
		display: "flex",
		// backgroundColor: '#5c6bc0 !important',
		color: "#fff",
		padding: 0,
	},
	collapse: {
		// borderWidth: '0 1px 1px 1px',
		// borderStyle: 'solid',
		// borderColor: '#d2d2d2',
	},
	content: {
		padding: "0.5rem",
		"&:last-child": {
			paddingBottom: "0.5rem",
		},
	},
	expand: {
		transform: "rotate(90deg)",
		// marginLeft: 'auto',
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest,
		}),
		color: "red",
		"&:hover": {
			backgroundColor: "inherit",
		},
	},
	expandOpen: {
		transform: "rotate(0deg)",
		color: "red",
	},
}));

const RecipeReviewCard = ({ title, children, hidden, isOpen = false, className }) => {
	const classes = useStyles();
	const [expanded, setExpanded] = useState(isOpen);



	const handleExpandClick = () => {
		setExpanded((prevExpanded) => !prevExpanded);
	};

	return (
		<Card
			className={classnames(className, classes.card, classes.root, {
				[classes.hidden]: hidden,
			})
			}
		>
			<CardActions className={classes.actions}>
				<IconButton
					disableRipple
					className={classnames(classes.expand, {
						[classes.expandOpen]: expanded,
					})}
					onClick={handleExpandClick}
					aria-expanded={expanded}
					aria-label="Show more"
				>
					{/* Use different icons based on the expanded state */}
					{/* {expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />} */}
					{expanded ? <Remove /> : <Add />}
				</IconButton>
				<Typography title={title} className={classes.title}>
					{title}
				</Typography>
			</CardActions>
			<Collapse
				className={classes.collapse}
				in={expanded}
				timeout="auto"
				unmountOnExit
			>
				<CardContent className={classes.content}>{children}</CardContent>
			</Collapse>
		</Card>
	);
};

RecipeReviewCard.propTypes = {
	title: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	hidden: PropTypes.bool,
};

export default RecipeReviewCard;
