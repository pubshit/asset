import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT, appConst } from "app/appConst";

const API_PATH =
  ConstantList.API_ENPOINT + "/api/asset" + ConstantList.URL_PREFIX;
const API_ASSETORG_PATH = ConstantList.API_ENPOINT + "/api/assetOrganization";
const API_PATH_VOUCHER =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_MAINTAINREQUEST =
  ConstantList.API_ENPOINT + "/api/maintainRequest" + ConstantList.URL_PREFIX;
const API_PATH_DOCUMENT_OLD = ConstantList.API_ENPOINT + "/api/asset_document";
const API_PATH_DOCUMENT = ConstantList.API_ENPOINT + "/api/v1/asset-documents";
const API_PATH_DOCUMENT_RECEVING_ASSET =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset-documents/voucher";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_DEPARTMENT_V2 =
  ConstantList.API_ENPOINT + "/api/v1/asset-departments";
const API_PATH_PRODUCT =
  ConstantList.API_ENPOINT + "/api/product" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_STATUS =
  ConstantList.API_ENPOINT + "/api/assetstatus" + ConstantList.URL_PREFIX;
const API_PATH_COMMONOBJECT =
  ConstantList.API_ENPOINT + "/api/commonobject" + ConstantList.URL_PREFIX;
//api mới
const API_ASSET_NEW = ConstantList.API_ENPOINT + "/api/fixed-assets";
const API_ASSET = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/fixed-assets";
const API_PATH_FILE =
  ConstantList.API_ENPOINT +
  "/api/fileUpload/asset/assetDocument/uploadattachment";
const API_PATH_REEVALUATE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/dgl-danhgialai";
const API_PATH_HISTORY =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset/history";
const API_PATH_UPLOAD_ATTACHMENT =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/file-upload";
const API_PATH_AR_ATTACHMENT =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-attachment";
const API_PATH_SEARCH_BY_STORE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset/search-by-store";
const API_PATH_PRINT_ASSET_BOOK =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/so-theo-doi-tscd";
const API_PATH_ASSET_DEPARTMENT_ORG =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/departments";
const API_PATH_ASSET =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/fixed-assets";
//asset
//CRUD
export const searchByPage = (params) => {
  let url = API_ASSET_NEW + `/page`;
  for (const [key, value] of Object.entries(params)) {
    if (Array.isArray(value)) {
      params[key] = value.toString();
    }
  }
  return axios.get(url, { params });
};

export const searchByPageLiquidation = (params) => {
  let url = API_ASSET + `/page`;
  for (const [key, value] of Object.entries(params)) {
    if (Array.isArray(value)) {
      params[key] = value.toString();
    }
  }
  return axios.get(url, { params });
};

export const searchByPageAsset = (params) => {
  let url = API_ASSET + `/search-by-page`;
  for (const [key, value] of Object.entries(params)) {
    if (Array.isArray(value)) {
      params[key] = value.toString();
    }
  }
  return axios.get(url, { params });
};

export const getItemById = (id) => {
  return axios.get(API_ASSET_NEW + "/" + id);
};

export const updateAsset = (id, asset) => {
  return axios.put(API_ASSET_NEW + "/" + id, asset);
};

export const deleteItem = (id) => {
  return axios.delete(API_ASSET_NEW + "/" + id);
};

//api khác
export const getNewCode = () => {
  let url = API_ASSET_NEW + "/new-code";
  return axios.get(url);
};

export const createAsset = (asset) => {
  //create multiple assets
  let url = API_ASSET_NEW + `/multiple`;

  if (asset?.isClones) {
    delete asset?.bhytMaMay;
    delete asset?.code;
    delete asset?.managementCode;
    return axios.post(url, asset);
  } else {
    return axios.post(API_ASSET_NEW, asset);
  }
};

export const codeWasUsed = (asset) => {
  let config = { params: { assetId: asset?.id, code: asset?.code } };
  let url = API_ASSET_NEW + `/code-was-used`;
  return axios.post(url, asset, config);
};

export const searchOrgByParent = (searchObject) => {
  var url = API_ASSETORG_PATH + "/parent/getOrgByParent";
  return axios.post(url, searchObject);
};

export const checkDeleteAsset = (id) => {
  return axios.get(API_PATH + "/checkDeleteById/" + id);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const getAllAssets = () => {
  return axios.get(API_PATH + "/1/100000");
};

// ASSET_DEPARTMENT
export const findDepartmentById = (id) => {
  return axios.get(API_PATH_ASSET_DEPARTMENT + "/findDepartmentById/" + id);
};
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};
export const getListManagementDepartmentOrg = (searchObject) => {
  let url = API_PATH_ASSET_DEPARTMENT_ORG + "/search-by-page";
  return axios.get(url, { params: searchObject });
};
export const getListOrgManagementDepartment = (searchDepartmentObject) => {
  let config = {
    params: {
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      voucherType: searchDepartmentObject?.voucherType,
    },
  };
  if (searchDepartmentObject?.orgId) {
    return axios.get(
      API_PATH_ASSET_DEPARTMENT +
        "/getListManagementDepartment/" +
        searchDepartmentObject?.orgId,
      config
    );
  }
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};
export const getManagementDepartment = (searchDepartmentObject) => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/management-departments",
    config
  );
};
export const getListUserDepartment = (SeachObjectct) => {
  var url = API_PATH_ASSET_DEPARTMENT + "/getListUserDepartment";
  return axios.post(url, SeachObjectct);
};
export const searchByPageListUserDepartment = (SeachObjectct) => {
  var url = API_PATH_ASSET_DEPARTMENT + "/searchByPage";
  return axios.post(url, SeachObjectct);
};
export const getMapMainDepartmentByUserIds = (userIds) => {
  let url =
    API_PATH_ASSET_DEPARTMENT_V2 +
    "/main-department/map-by-userid?userIds=" +
    userIds;
  return axios.get(url);
};

export const getAllProducts = () => {
  return axios.get(API_PATH_PRODUCT + "/getall");
};

export const getAllstatuss = () => {
  return axios.get(API_PATH_ASSET_STATUS + "/1/100000");
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};

export const exportToExcel = (params) => {
  return axios({
    method: "get",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/fixed-assets/excel/export",
    params: params,
    responseType: "blob",
  });
};

export const exportToExcelQR = (params) => {
  return axios({
    method: "get",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/fixed-assets/excel/export/qr-code",
    params: params,
    responseType: "blob",
  });
};

export const exportExampleImportExcel = (asset) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/excel/template/asset",
    data: asset,
    responseType: "blob",
  });
};

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + linkFile,
    responseType: "blob",
  });
};

export const historyOfAllocation = (assetId) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      `/api/asset/history/allocation/${assetId}`
  );
};

export const historyOfTransfer = (assetId) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      `/api/asset/history/transfer/${assetId}`
  );
};

export const historyOfBrokenMessageAndRepair = (assetObjectId) => {
  return axios.post(
    API_PATH_MAINTAINREQUEST + "/historyOfBrokenMessageAndRepair",
    assetObjectId
  );
};

// API_PATH_DOCUMENT
export const searchByPageAssetDocument = (assetDocumentObject) => {
  return axios.post(
    API_PATH_DOCUMENT_OLD + `/searchByPage`,
    assetDocumentObject
  );
};

export const createAssetDocument = (assetDocument) => {
  return axios.post(API_PATH_DOCUMENT, assetDocument);
};
export const createArAttachment = (assetDocument) => {
  return axios.post(API_PATH_AR_ATTACHMENT, assetDocument);
};
export const getAssetDocumentById = (assetDocumentId) => {
  return axios.get(API_PATH_DOCUMENT + "/" + assetDocumentId);
};
export const getRecevingAssetDocumentById = (assetDocumentId) => {
  return axios.get(API_PATH_DOCUMENT_RECEVING_ASSET + "/" + assetDocumentId);
};
export const updateRecevingAssetDocument = (assetDocumentId, payload) => {
  return axios.post(
    API_PATH_DOCUMENT_RECEVING_ASSET + "/" + assetDocumentId,
    payload
  );
};
export const getArAttachmentById = (assetDocumentId) => {
  return axios.get(API_PATH_AR_ATTACHMENT + "/" + assetDocumentId);
};
export const deleteAssetDocumentById = (assetDocumentId) => {
  // Api này k có xóa, những chỗ dùng cần check lại
  return axios.delete(API_PATH_DOCUMENT + "/" + assetDocumentId);
};
export const deleteAssetDocumentByIdV1 = (assetDocumentId) => {
  return axios.delete(API_PATH_DOCUMENT_OLD + "/" + assetDocumentId);
};
export const deleteArAttachmentById = (assetDocumentId) => {
  return axios.delete(API_PATH_AR_ATTACHMENT + "/" + assetDocumentId);
};

export const updateAssetDocumentById = (value) => {
  return axios.put(`${API_PATH_DOCUMENT}/${value.idHoSoDK}`, value);
};
export const updateArAttachmentById = (assetDocument) => {
  return axios.put(API_PATH_AR_ATTACHMENT, assetDocument);
};
export const getNewCodeAssetDocument = (type) => {
  // return axios.get(API_PATH_DOCUMENT + "/addNew/getNewCode");
  return axios.get(API_PATH_DOCUMENT + `/new-code?type=${type}`);
};

export const getNewCodeAssetDocumentLiquidate = () => {
  return axios.get(
    API_PATH_DOCUMENT + "/new-code?type=ASSET_DOCUMENT_LIQUIDATE"
  );
};

export const getNewCodeDocument = (type) => {
  let config = { params: { type: type } };
  let url = API_PATH_DOCUMENT + "/new-code";
  return axios.get(url, config);
};

export const getMedicalEquipment = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/ltbyt/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};

export const getShoppingForm = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/htms/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};

export const getManufacturer = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/hsx/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};

//file
export const uploadFile = (formData) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(API_PATH_UPLOAD_ATTACHMENT, formData, config);
};
export const uploadFileOldApi = (formData) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(API_PATH_FILE, formData, config);
};

// /asvn/api/fixed-assets/{id}/image
export const uploadImageAsset = (id, object) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.put(
    ConstantList.API_ENPOINT + `/api/fixed-assets/${id}/image`,
    object,
    config
  );
};
export const uploadImageAssetCreate = (object) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(
    ConstantList.API_ENPOINT + `/api/fixed-assets/image`,
    object,
    config
  );
};

export const getListAssetReevaluate = (searchObject) => {
  let config = {
    pageIndex: searchObject?.pageIndex,
    pageSize: searchObject?.pageSize,
    keyword: searchObject?.keyword,
    useDepartmentId: searchObject?.useDepartmentId,
    assetClass: appConst.assetClass.TSCD,
    storeId: searchObject?.storeId,
  };
  return axios.post(API_PATH_REEVALUATE + "/get-assets", config);
};

// http://192.168.1.27:8065/asvn/api/fixed-assets/bhyt-ma-may
export const autoCreateMaBHYT = (assetDocument) => {
  return axios.post(
    ConstantList.API_ENPOINT + "/api/fixed-assets/bhyt-ma-may",
    assetDocument
  );
};

// /api/asset/financial-code/new
export const autoCreateMaKeToan = () => {
  return axios.get(ConstantList.API_ENPOINT + "/api/asset/financial-code/new");
};

// "http://asset-management-dev.oceantech.com.vn:80/api/asset/check-update-khau-hao/123123"
export const checkTTHK = (id) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      `/api/asset/check-update-khau-hao/${id}`
  );
};

// Lịch sử tài sản
export const getListMaintainPlaningHistory = (searchObject) => {
  // lịch sử bảo dưỡng
  return axios.post(API_PATH_HISTORY + "/bao-tri", searchObject);
};

export const getTotalCostMaintainPlaningHistory = (id) => {
  // Tổng chi phí ls bảo dưỡng
  return axios.get(API_PATH_HISTORY + "/bao-tri/total-cost/" + id);
};

export const getMaintainPlaningHistoryLinhKien = (searchObject) => {
  // Ds linh kiện ls bảo dưỡng
  return axios.get(API_PATH_HISTORY + "/bao-tri/linh-kien/", {
    params: searchObject,
  });
};

export const getListRepairingHistory = (searchObject) => {
  //lịch sử sửa chữa
  return axios.post(API_PATH_HISTORY + "/sua-chua", { ...searchObject });
};

export const getTotalCostRepairingHistory = (id) => {
  // Tổng chi phí ls sửa chữa
  return axios.get(API_PATH_HISTORY + "/sua-chua/total-cost/" + id);
};

export const getRepairingHistoryLinhKien = (searchObject) => {
  // Ds linh kiện ls sửa chữa
  return axios.get(API_PATH_HISTORY + "/sua-chua/linh-kien/", {
    params: searchObject,
  });
};

export const getAssetsByStore = (searchObject) => {
  var url = API_PATH_SEARCH_BY_STORE;
  return axios.post(url, searchObject);
};

// http://asset-management-dev.oceantech.com.vn:80/api/user-departments
export const getTreeUser = () => {
  const url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/user-departments";
  return axios.get(url);
};

export const printAssetBookService = (params) => {
  return axios.get(API_PATH_PRINT_ASSET_BOOK, {params});
};

export const exportExcelAssetFollowService = (params) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/excel/so-theo-doi-tscd",
    params,
    responseType: "blob",
  });
};

export const exportExcelAssetBookService = (params) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/excel/fixed-asset-book",
    params,
    responseType: "blob",
  });
};

export const exportToExceAllocationHistory = (assetId) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      `/api/asset/history/allocation/${assetId}/export`,
    { responseType: "blob" }
  );
};

export const exportToExceTransferHistory = (assetId) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      `/api/asset/history/transfer/${assetId}/export`,
    { responseType: "blob" }
  );
};

export const exportToExceRepairHistory = (params) => {
  return axios.post(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/ar-suco/excel/lich-su-sua-chua",
    params,
    { responseType: "blob" }
  );
};

export const exportToExceMaintenanceHistory = (params) => {
  return axios.post(
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/lich-su-bao-tri",
    params,
    { responseType: "blob" }
  );
};

export const importExcelAssetUrl = () => {
  return ConstantList.API_ENPOINT + "/api/excel/import/asset";
};
export const exportExcelAssetCardService = (searchObject) => {
  let config = {
    params: {
      ...searchObject,
    },
    responseType: "blob",
  };
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/excel/the-tscd",
    config
  );
};

export const printAssetCard = (searchObject) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/the-tscd",
    {
      params: searchObject,
    }
  );
};
