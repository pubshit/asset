import { appConst, variable } from '../../appConst';
import React, { Component } from 'react';
import { Dialog, Button, Grid, DialogActions } from '@material-ui/core';
import { ValidatorForm } from 'react-material-ui-form-validator';
import { getNewCode, updateAsset, getAssetDocumentById, createAsset, getNewCodeDocument } from './AssetService';
import { searchByPage as statusSearchByPage } from '../AssetStatus/AssetStatusService';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import DialogContent from '@material-ui/core/DialogContent';
import ScrollableTabsButtonForce from './ScrollableTabsButtonForce';
import { searchByPage as searchByPageAssetStatus } from '../AssetStatus/AssetStatusService';
import { personSearchByPage } from '../AssetTransfer/AssetTransferService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../../../styles/views/_loadding.scss';
import { checkInvalidDate, checkObject } from '../../../app/appFunction';
import AppContext from 'app/appContext';
import { validateAllocationFor, validateSubmit } from './AssetValidate';
toast.configure();

function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}
class AssetEditorDialog extends Component {
    state = {
        name: '',
        code: '',
        qrcode: '',
        managementCode: '',
        allocationFor: 1,
        receiverPerson: null,
        madeIn: '',
        model: '',
        usedTime: null,
        warrantyMonth: '',
        yearOfManufacture: 0,
        serialNumber: '',
        useDepartment: null,
        managementDepartment: null,
        installationLocation: '',
        supplyUnit: null,
        dayStartedUsing: new Date(),
        dateOfReception: new Date(),
        assetSource: null,
        status: null,
        originalCost: null,
        carryingAmount: null,
        warrantyExpiryDate: null,
        depreciationDate: new Date(),
        depreciationPeriod: null,
        depreciationRate: null,
        accumulatedDepreciationAmount: null,
        product: null,
        biddingPackage: null,
        yearPutIntoUse: null,
        assetGroup: null,
        manufacturer: null,
        isEditCode: false,
        isCreateQRCode: false,
        checkUseDepartmentByStatus: false,
        shouldOpenPersonPopup: false,
        shouldOpenSelectUsePersonPopup: false,
        shouldOpenSelectUseDepartmentPopup: false,
        shouldOpenSelectAssetGroupPopup: false,
        shouldOpenSelecPopup: false,
        usePerson: null,
        displayName: null,
        attributes: [],
        shouldOpenPopupSelectAttribute: false,
        shouldOpenAssetAllocationEditorDialog: false,
        unitPrice: null,
        openAssetAllocationPopup: false,
        shouldOpenSelectUserDepartment: false,
        shouldOpenPopupAssetFile: false,
        assetDocumentList: [],
        keyword: '',
        rowsPerPage: 10,
        page: 0,
        totalElements: 0,
        isEditAssetDocument: false,
        isManageAccountant: false,
        isTemporary: false,
        isReceivingAsset: false,
        storage: [],
        quantity: null,
        quantityCode: 1,
        listCodes: '',
        store: null,
        loading: false,
        listAssetDocumentId: [],
        assetStatusNew: 0, // mới
        AssetStatusAllocation: 2, // đã cấp phát
        documentType: appConst.documentType.ASSET_DOCUMENT,
        productTypeCode: 'TSCD',
        shouldOpenPopupSelectAssetSource: false,
        assetSources: [],
        isNew: false,
        isMedicalEquipment: false,
        isBuyLocally: null,
        isAllocation: false,
        shouldOpenAllocationEditorDialog: false,
        numberOfClones: 1,
        shouldOpenDialogStore: false,
        shouldOpenDialogManufacturer: false,
        shouldOpenDialogShoppingForm: false,
        shouldOpenDialogUnit: false,
        shouldOpenDialogMedicalEquipment: false,
        imageUrl: null,
    };

    convertDto = (state) => {
        let data = {
            accumulatedDepreciationAmount: state?.accumulatedDepreciationAmount,
            allocationFor: state?.allocationFor,
            allocationPeriod: state?.allocationPeriod,
            allocationPeriodValue: state?.allocationPeriodValue,
            assetGroupId: state?.assetGroupId,
            assetSources: state?.assetSources,
            attributes: state?.attributes,
            biddingPackage: state?.biddingPackage,
            carryingAmount: state?.carryingAmount,
            circulationNumber: state?.circulationNumber,
            code: state?.code,
            countryOriginId: state?.countryOriginId,
            dateOfReception: state?.dateOfReception,
            dayStartedUsing: state?.dayStartedUsing,
            depreciationDate: state?.depreciationDate,
            depreciationPeriod: state?.depreciationPeriod,
            depreciationRate: state?.depreciationRate,
            installationLocation: state?.installationLocation,
            invoiceNumber: state?.invoiceNumber,
            isBuyLocally: state?.isBuyLocally,
            isManageAccountant: state?.isManageAccountant,
            isTemporary: state?.isTemporary,
            listAssetDocumentId: state?.listAssetDocumentId,
            lotNumber: state?.lotNumber,
            madeIn: state?.madeIn,
            managementCode: state?.managementCode,
            managementDepartmentId: state?.managementDepartmentId
                ? state?.managementDepartmentId
                : state?.managementDepartment?.id,
            manufacturerId: state?.manufacturerId ? state?.manufacturerId : state?.manufacturer?.id,
            medicalEquipmentId: state?.medicalEquipmentId ? state?.medicalEquipmentId : state?.medicalEquipment?.id,
            model: state?.model,
            name: state?.name,
            note: state?.note,
            numberOfAllocations: state?.numberOfAllocations,
            numberOfAllocationsRemaining: state?.numberOfAllocationsRemaining,
            originalCost: state?.originalCost,
            productId: state?.productId ? state?.productId : state?.product?.id,
            qrcode: state?.qrcode,
            receiverPersonId: state?.receiverPersonId ? state?.receiverPersonId : state?.receiverPerson?.id,
            riskClassification: state?.riskClassification,
            serialNumber: state?.serialNumber,
            shoppingFormId: state?.shoppingFormId ? state?.shoppingFormId : state?.shoppingForm?.id,
            storeId: state?.storeId ? state?.storeId : state?.store?.id,
            supplyUnitId: state?.supplyUnitId ? state?.supplyUnitId : state?.supplyUnit?.id,
            unitId: state?.unitId ? state?.unitId : state?.unit?.id,
            unitPrice: state?.unitPrice,
            useDepartmentId: state?.useDepartmentId ? state?.useDepartmentId : state?.useDepartment?.id,
            useDepartmentLateId: state?.useDepartmentLateId ? state?.useDepartmentLateId : state?.useDepartmentLate?.id,
            usePersonId: state?.usePersonId ? state?.usePersonId : state?.usePerson?.id,
            usedTime: state?.usedTime,
            warrantyExpiryDate: state?.warrantyExpiryDate || null,
            warrantyMonth: state?.warrantyMonth,
            yearOfManufacture: state?.yearOfManufacture,
            yearPutIntoUse: state?.yearPutIntoUse,
            listCodes: state?.listCodes,
            numberOfClones: state?.numberOfClones,
            documents: [],
            imageUrl: state.imageUrl,
        };
        return data;
    };

    handleDateChange = (date, name) => {
        if (variable.listInputName.dateOfReception === name) {
            let newDate = date ? (!checkInvalidDate(date) ? new Date(date) : null) : null;
            // eslint-disable-next-line no-unused-expressions
            newDate?.setMonth(newDate.getMonth() + Number(this.state?.warrantyMonth ? this.state?.warrantyMonth : 0));
            let warrantyExpiryDate = newDate || null;
            this.setState({
                [name]: date,
                warrantyExpiryDate: warrantyExpiryDate,
            });
            return;
        }

        this.setState({
            [name]: date,
        });
    };

    handleChangeFormatNumber = (event) => {
        let { assetSources } = this.state;
        if (variable.listInputName.originalCost === event.target.name) {
            this.setState({
                [event.target.name]: event.target.value,
                assetSources,
            });
            return;
        }
        this.setState({
            [event.target.name]: event.target.value,
            assetSources,
        });
    };

    handleChangeSelect = (value, source) => {
        if (!value) {
            this.setState({
                numberOfAllocations: '',
                numberOfAllocationsRemaining: '',
                carryingAmount: '',
                [source]: value,
            });
            return;
        }
        this.setState({ [source]: value });
    };

    selectMedicalEquipment = (medicalEquipmentSelected) => {
        if (medicalEquipmentSelected?.code === variable.listInputName.New) {
            this.setState({
                shouldOpenDialogMedicalEquipment: true,
            });
            return;
        }

        this.setState(
            {
                medicalEquipment: medicalEquipmentSelected,
                medicalEquipmentId: medicalEquipmentSelected?.id,
            },
            function () {},
        );
    };

    selectUnit = (unitSelected) => {
        if (unitSelected?.code === variable.listInputName.New) {
            this.setState({
                shouldOpenDialogUnit: true,
            });
            return;
        }

        this.setState({
            unit: unitSelected,
            unitId: unitSelected?.id,
        });
    };

    selectShoppingForm = (shoppingFormSelected) => {
        if (shoppingFormSelected?.code === variable.listInputName.New) {
            this.setState({
                shouldOpenDialogShoppingForm: true,
            });
            return;
        }

        this.setState({
            shoppingForm: shoppingFormSelected,
            shoppingFormId: shoppingFormSelected?.id,
        });
    };

    handleChange = (event, source) => {
        event.persist();
        let { isTemporary, isManageAccountant, isMedicalEquipment, isBuyLocally, yearPutIntoUse } = this.state;

        if (variable.listInputName.yearPutIntoUse === source) {
            yearPutIntoUse = event.target.value;
            this.setState({ yearPutIntoUse: yearPutIntoUse });
            return;
        }

        if (variable.listInputName.numberOfClones === source) {
            this.setState({
                numberOfClones: Number(event.target.value),
            });
            return;
        }

        if (variable.listInputName.warrantyMonth === source) {
            let dateOfReception = this.state.dateOfReception ? this.state.dateOfReception : null;
            let newDate = dateOfReception ? new Date(dateOfReception) : null;
            // eslint-disable-next-line no-unused-expressions
            newDate?.setMonth(newDate.getMonth() + Number(event.target.value || 0));
            let warrantyExpiryDate = newDate ? newDate : null;
            this.setState({
                [event.target.name]: event.target.value,
                warrantyExpiryDate: warrantyExpiryDate,
            });
            return;
        }

        if (variable.listInputName.isBuyLocally === source) {
            if (
                (isBuyLocally && Number(event.target.value) === 1) ||
                (isBuyLocally === false && Number(event.target.value) === 2)
            ) {
                isBuyLocally = null;
            } else {
                isBuyLocally = Number(event.target.value) === 1 ? true : false;
            }
            this.setState({ isBuyLocally: isBuyLocally });
            return;
        }

        if (variable.listInputName.isMedicalEquipment === source) {
            isMedicalEquipment = event.target.checked;
            this.setState({ isMedicalEquipment: isMedicalEquipment });
            return;
        }

        if (variable.listInputName.isManageAccountant === source) {
            isManageAccountant = event.target.checked;
            this.setState({ isManageAccountant: isManageAccountant });
            return;
        }

        if (variable.listInputName.isTemporary === source) {
            isTemporary = event.target.checked;
            this.setState({ isTemporary: isTemporary });
            return;
        }

        if (variable.listInputName.isCreateQRCode === source) {
            this.setState({
                isCreateQRCode: event.target.checked,
                qrcode: event.target.checked ? this.state.code : null,
            });
            return;
        } else if (variable.listInputName.allocationFor === source) {
            if (event.target.value / 1 === 1) this.setState({ usePerson: null });
            // else if (event.target.value/1 === 2) this.setState({useDepartment : null});
            this.setState({
                [event.target.name]: event.target.value / 1,
            });
            return;
        }
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    openCircularProgress = () => {
        this.setState({ loading: true });
    };

    createAsset = (state, t) => {
        let { setPageLoading } = this.context;
        createAsset(state)
            .then(({ data }) => {
                setPageLoading(false);
                if (appConst.CODE.SUCCESS === data?.code) {
                    if (this.state?.isAllocation) {
                        this.setState({
                            shouldOpenAllocationEditorDialog: true,
                            assetAllocation: data?.data?.length > 0 ? data?.data : [data?.data],
                        });
                        return;
                    }
                    toast.info(t(data?.message));
                    this.props.handleOKEditClose();
                    return;
                }
                toast.warning(t(data?.message));
            })
            .catch(() => {
                toast.error(t('toastr.error'));
                toast.clearWaitingQueue();
                setPageLoading(false);
            });
    };

    updateAsset = (id, state, t) => {
        let { setPageLoading } = this.context;
        updateAsset(id, state)
            .then(({ data }) => {
                setPageLoading(false);
                if (appConst.CODE.SUCCESS === data?.code) {
                    if (this.state?.isAllocation) {
                        this.setState({
                            isUpdateAsset: true,
                            shouldOpenAllocationEditorDialog: true,
                            assetAllocation: [data?.data],
                        });
                        return;
                    }
                    toast.info(t(data?.message));
                    this.props.handleOKEditClose();
                    return;
                }
                toast.warning(t(data?.message));
            })
            .catch(() => {
                toast.error(t('toastr.error'));
                toast.clearWaitingQueue();
                setPageLoading(false);
            });
    };
    customSetState = (data) => {
        this.setState(data);
    };

    handleFormSubmit = async (e) => {
        let { setPageLoading } = this.context;
        let { t } = this.props;
        let { isReceivingAsset, id } = this.state;

        if (e?.target?.id !== 'parent') return;
        if (!validateSubmit(this.state, this.customSetState, toast)) return;
        if (isReceivingAsset) {
            this.props.handleSelect(this.state);
            return;
        }
        if (!validateAllocationFor(this.state, this.customSetState, toast)) return;
        let dataState = this.convertDto(this.state);

        setPageLoading(true);
        if (!id) {
            this.createAsset(dataState, t);
        } else {
            this.updateAsset(this.state?.id, dataState, t);
        }
    };

    handleClose = () => {
        this.setState({
            shouldOpenAllocationEditorDialog: false,
            shouldOpenDialogStore: false,
            shouldOpenDialogManufacturer: false,
            shouldOpenDialogShoppingForm: false,
            shouldOpenDialogUnit: false,
            shouldOpenDialogMedicalEquipment: false,
            isAllocation: false,
        });
    };

    handleCloseAll = () => {
        this.props.handleOKEditClose();
        this.setState({
            shouldOpenAllocationEditorDialog: false,
        });
    };

    handleFormSubmitAllocation = (e) => {
        this.setState({
            isAllocation: true,
        });
    };
    componentWillMount() {
        let { item, isReceivingAsset } = this.props;
        const searchObject = { pageIndex: 0, pageSize: 1000000 };
        let storage;
        this.setState({
            isMedicalEquipment: this.props?.isMedicalEquipment,
        });
        if (isReceivingAsset) {
            searchByPageAssetStatus(searchObject).then((data) => {
                storage = data.data.content.find((element) => element.indexOrder === 0);
                this.setState(
                    {
                        ...this.props.item,
                        status: storage,
                        isReceivingAsset: isReceivingAsset,
                        checkUseDepartmentByStatus:
                            !item.status || !item.status.indexOrder || item.status.indexOrder === 0 ? true : false,
                    },
                    function () {
                        let { allocationFor } = this.state;
                        if (!allocationFor) {
                            allocationFor = 1; //phòng ban
                        }

                        this.setState({ allocationFor });
                    },
                );
            });
        } else {
            this.setState(
                {
                    ...this.props.item,
                    isReceivingAsset: isReceivingAsset,
                    checkUseDepartmentByStatus:
                        !item.status || !item.status.indexOrder || item.status.indexOrder === 0 ? true : false,
                },
                function () {
                    let { allocationFor } = this.state;
                    if (!allocationFor) {
                        allocationFor = 1; //phòng ban
                    }

                    this.setState({ allocationFor });
                },
            );
        }

        if (!item?.id) {
            const searchObjectStatus = {
                pageIndex: 0,
                pageSize: 1000000,
                statusIndexOrders: [appConst.statusAsset.STORAGE_NEW, appConst.statusAsset.USEING],
            };
            statusSearchByPage(searchObjectStatus).then(({ data }) => {
                this.setState({ status: data.content[0], isNew: true });
            });
        }
    }

    refreshCode = () => {
        getNewCode()
            .then((result) => {
                if (result && result.data && result.data.code) {
                    let { isCreateQRCode } = this.state;
                    this.setState({
                        code: result.data.code,
                        qrcode: isCreateQRCode ? result.data.code : null,
                    });
                }
            })
            .catch(() => {
                toast.warning(this.props.t('general.error_reload_page'));
            });
    };
    openSelectProductPopup = () => {
        this.setState({
            shouldOpenSelectProductPopup: true,
        });
    };
    openSelectAssetGroupPopup = () => {
        this.setState({
            shouldOpenSelectAssetGroupPopup: true,
        });
    };
    openSelectSupplyPopup = () => {
        this.setState({
            shouldOpenSelectSupplyPopup: true,
        });
    };

    handleSelectSupplyPopupClose = () => {
        this.setState({
            shouldOpenSelectSupplyPopup: false,
        });
    };

    handleSelectSupply = (item) => {
        this.setState(
            {
                supplyUnit: item,
                supplyUnitId: item?.id,
            },
            function () {
                this.handleSelectSupplyPopupClose();
            },
        );
        if (checkObject(item)) {
            this.setState({ supplyUnit: null });
        }
    };
    handleSelectAssetGroupPopupClose = () => {
        this.setState({
            shouldOpenSelectAssetGroupPopup: false,
        });
    };
    handleSelectProductPopupClose = () => {
        this.setState({
            shouldOpenSelectProductPopup: false,
        });
    };
    handleSelectProduct = (item) => {
        this.setState(
            {
                product: item,
                productId: item?.id,
                name: item.name,
                supplyUnit: item.supplyUnit,
                supplyUnitId: item.supplyUnit?.id,
                madeIn: item.madeIn,
                yearOfManufacture: item.yearOfManufacture,
                model: item.model,
                warrantyExpiryDate: item.warrantyExpiryDate,
                manufacturer: item.manufacturer,
                serialNumber: item.serialNumber,
            },
            function () {
                let { product, attributes } = this.state;
                attributes = [];
                if (
                    product &&
                    product.template &&
                    product.template.properties &&
                    product.template.properties.length > 0
                ) {
                    product.template.properties.forEach((item) => {
                        let property = {};
                        property.attributeId = item?.attribute?.id;
                        property.name = item?.attribute?.name;
                        property.attribute = item.attribute;
                        property.valueNumber = 0;
                        property.valueBoolean = false;
                        property.valueText = '';

                        attributes.push(property);
                    });
                }

                this.setState({ attributes }, function () {
                    this.handleSelectProductPopupClose();
                });
            },
        );
        if (checkObject(item)) {
            this.setState({ product: this.state.product });
        }
    };

    handleAddAttribute = (attributes) => {
        // eslint-disable-next-line no-unused-expressions
        attributes?.forEach((item) => {
            item.attributeId = item?.attribute?.id;
            item.name = item?.attribute?.name;
        });

        this.setState({ attributes }, function () {
            this.handleSelectAttributePopupClose();
        });
    };

    handleAddAssetSource = (items) => {
        // eslint-disable-next-line no-unused-expressions
        items?.map((item) => {
            item.assetSourceId = item?.assetSource?.id;
            item.assetSourceName = item?.assetSource?.name;
            return item;
        });
        this.setState({ assetSources: items }, function () {
            this.handleSelectAssetSourcePopupClose();
        });
    };

    openPopupSelectAttribute = () => {
        this.setState({
            shouldOpenPopupSelectAttribute: true,
        });
    };
    openAssetAllocationEditorDialog = () => {
        this.setState({
            shouldOpenAssetAllocationEditorDialog: true,
        });
    };
    handleCloseAssetAllocationEditorDialog = () => {
        this.props.handleOKEditClose();
        this.setState({
            shouldOpenAssetAllocationEditorDialog: false,
        });
    };
    shouldOpenPopupAssetFile = () => {
        this.setState({
            item: null,
            shouldOpenPopupAssetFile: true,
        });
    };

    handleSelectAttributePopupClose = () => {
        this.setState({
            shouldOpenPopupSelectAttribute: false,
        });
    };

    handleSelectAssetSourcePopupClose = () => {
        this.setState({
            shouldOpenPopupSelectAssetSource: false,
        });
    };

    openPopupSelectAssetSource = () => {
        this.setState({
            shouldOpenPopupSelectAssetSource: true,
        });
    };

    handleAssetFilePopupClose = () => {
        this.setState({
            shouldOpenPopupAssetFile: false,
        });
    };

    openSelectDepartmentPopup = () => {
        this.setState({
            shouldOpenSelectDepartmentPopup: true,
        });
    };

    openSelectUseDepartmentPopup = () => {
        this.setState({
            shouldOpenSelectUseDepartmentPopup: true,
        });
    };

    openSelectUsePersonPopup = () => {
        this.setState({
            shouldOpenSelectUsePersonPopup: true,
        });
    };

    handleSelectDepartmentPopupClose = () => {
        this.setState({
            shouldOpenSelectDepartmentPopup: false,
        });
    };

    handleSelectUseDepartmentPopupClose = () => {
        this.setState({
            shouldOpenSelectUseDepartmentPopup: false,
        });
    };

    handleSelectUsePersonPopupClose = () => {
        this.setState({
            shouldOpenSelectUsePersonPopup: false,
        });
    };

    handleSelectManagementDepartment = (item) => {
        let department = { id: item.id, name: item.name };
        this.setState(
            {
                managementDepartment: department,
                managementDepartmentId: department?.id,
            },
            () => this.handleSelectDepartmentPopupClose(),
        );
        if (checkObject(item)) {
            this.setState({ managementDepartment: null });
        }
    };

    handleSelectUseDepartment = (item) => {
        let department = { id: item.id, name: item.text, text: item.text };
        this.setState({ useDepartment: department }, function () {
            this.handleSelectUseDepartmentPopupClose();
        });

        if (checkObject(item)) {
            this.setState({ useDepartment: null });
        }

        let usePersonSearchObject = {
            pageIndex: 0,
            pageSize: 1000000,
            departmentId: item.id,
        };
        let result = 0;
        let { usePerson } = this.state;
        personSearchByPage(usePersonSearchObject).then((data) => {
            data.data.content.map((person) => {
                if (usePerson) {
                    if (usePerson.id === person.id) {
                        ++result;
                    }
                }
                return person;
            });

            if (result !== 0) {
                this.setState({ receiverDepartment: { id: item.id, name: item.text } }, function () {
                    this.handleSelectUseDepartmentPopupClose();
                });
            } else {
                this.setState(
                    {
                        receiverDepartment: { id: item.id, name: item.text },
                        usePerson: null,
                    },
                    function () {
                        this.handleSelectUseDepartmentPopupClose();
                    },
                );
            }
        });
    };

    handleSelectUsePerson = (item) => {
        let department = {
            id: item.department.id,
            name: item.department.name,
            text: item.department.name + '-' + item.department.code,
        };

        let person = {
            id: item.user.person.id,
            displayName: item.user.displayName,
        };
        this.setState({ usePerson: person, useDepartment: department }, function () {
            this.handleSelectUsePersonPopupClose();
        });
        if (checkObject(item)) {
            this.setState({ usePerson: null, useDepartment: null });
        }
    };
    handleSelectAssetGroup = (item) => {
        this.setState(
            {
                assetGroup: item,
                assetGroupId: item?.id,
                depreciationRate: item.depreciationRate,
            },
            () => this.handleSelectAssetGroupPopupClose(),
        );
        if (checkObject(item)) {
            this.setState({ assetGroup: null, depreciationRate: null });
        }
    };
    selectAssetGroup = (assetGroupSelected) => {
        this.setState({
            assetGroup: assetGroupSelected,
            assetGroupId: assetGroupSelected?.id,
            depreciationRate: assetGroupSelected?.depreciationRate,
        });
    };

    selectManufacturer = (manufacturerSelected) => {
        if (manufacturerSelected?.code === variable.listInputName.New) {
            this.setState({
                shouldOpenDialogManufacturer: true,
            });
            return;
        }

        this.setState({
            manufacturer: manufacturerSelected,
            manufacturerId: manufacturerSelected?.id,
        });
    };

    selectAssetSource = (assetSourceSelected) => {
        this.setState({
            assetSource: assetSourceSelected,
            assetSourceId: assetSourceSelected?.id,
        });
    };

    selectStatus = (statusSelected) => {
        if (!statusSelected || !statusSelected.indexOrder || statusSelected.indexOrder === 0) {
            this.setState({
                status: statusSelected,
                useDepartment: null,
                usePerson: null,
                checkUseDepartmentByStatus: true,
                shouldOpenSelectUserDepartment: false,
                shouldOpenSelecPopup: false,
            });
        } else {
            this.setState({
                status: statusSelected,
                checkUseDepartmentByStatus: false,
                shouldOpenSelectUserDepartment: true,
                shouldOpenSelecPopup: true,
            });
        }
    };

    selectStores = (storeSelected) => {
        if (storeSelected?.code === variable.listInputName.New) {
            this.setState({
                shouldOpenDialogStore: true,
            });
            return;
        }

        this.setState({
            store: storeSelected,
            storeId: storeSelected?.id,
        });
    };

    selectSupplyUnit = (supplyUnitSelected) => {
        this.setState({
            supplyUnit: supplyUnitSelected,
            supplyUnitId: supplyUnitSelected?.id,
        });
    };

    selectUseDepartment = (departmentSelected) => {
        this.setState({ useDepartment: departmentSelected }, function () {});
    };

    selectManagementDepartment = (departmentSelected) => {
        this.setState({
            managementDepartment: departmentSelected,
            managementDepartmentId: departmentSelected?.id,
        });
    };

    handleRowDataCellChange = (rowData, event) => {
        let { attributes } = this.state;
        if (attributes && attributes.length > 0) {
            attributes.forEach((element) => {
                if (element.tableData && rowData.tableData && element.tableData.id === rowData.tableData.id) {
                    if (event.target.name === 'valueText') {
                        element.valueText = event.target.value;
                        this.setState({ attributes }, function () {
                            return;
                        });
                    }
                }
            });
        }
    };

    handleRowDataCellChangeAssetSource = (rowData, event) => {
        let { assetSources, originalCost } = this.state;
        assetSources?.length > 0 &&
            assetSources.forEach((element) => {
                if (rowData?.tableData && element?.tableData?.id === rowData?.tableData?.id) {
                    if (variable.listInputName.assetSourcePercentage === event.target.name) {
                        element.percentage = event.target.value;
                        if (originalCost && originalCost >= 0) {
                            element.value = Math.ceil((event.target.value * originalCost) / 100);
                        }
                    } else {
                        element.value = event.target.value;
                        if (originalCost && originalCost >= 0) {
                            element.percentage = ((event.target.value / originalCost) * 100).toFixed(2);
                        }
                    }
                    this.setState({ assetSources }, function () {
                        return;
                    });
                }
            });
    };

    handleRowDataCellDelete = (rowData) => {
        let { attributes } = this.state;
        let index = attributes?.findIndex((attribute) => attribute?.attribute?.id === rowData?.attribute?.id);
        attributes.splice(index, 1);
        this.setState({ attributes });
    };

    handleRowDataCellDeleteAssetSource = (rowData) => {
        let { assetSources } = this.state;
        if (assetSources && assetSources.length > 0) {
            for (let index = 0; index < assetSources.length; index++) {
                if (assetSources[index].assetSource && assetSources[index].assetSource.id === rowData.assetSource.id) {
                    assetSources.splice(index, 1);
                    break;
                }
            }
        }
        this.setState({ assetSources }, function () {});
    };

    getAssetDocument = (document) => {
        let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
        let documents = this.state?.documents ? this.state?.documents : [];
        document && listAssetDocumentId.push(document?.id);
        document && documents.push(document);
        this.setState({ listAssetDocumentId, documents });
    };

    handleRowDataCellEditAssetFile = (rowData) => {
        getAssetDocumentById(rowData.id).then(({ data }) => {
            let fileDescriptionIds = [];
            let document = data?.data ? data?.data : null;

            // eslint-disable-next-line no-unused-expressions
            document?.attachments?.map((item) => fileDescriptionIds.push(item?.file?.id));

            document.fileDescriptionIds = fileDescriptionIds;
            document.documentType = this.state.documentType;

            this.setState({
                item: document,
                shouldOpenPopupAssetFile: true,
                isEditAssetDocument: true,
            });
        });
    };

    handleRowDataCellDeleteAssetFile = (id) => {
        let { documents, listAssetDocumentId } = this.state;
        let index = documents?.findIndex((document) => document?.id === id);
        let indexId = listAssetDocumentId?.findIndex((documentId) => documentId === id);
        documents.splice(index, 1);
        listAssetDocumentId.splice(indexId, 1);
        this.setState({ listAssetDocumentId, documents });
    };

    handleAddAssetDocumentItem = () => {
        getNewCodeDocument(this.state.documentType)
            .then(({ data }) => {
                if (appConst.CODE.SUCCESS === data?.code) {
                    let item = {};
                    item.code = data?.data;
                    this.setState({
                        item: item,
                        shouldOpenPopupAssetFile: true,
                    });
                    return;
                }
                toast.warning(data?.message);
            })
            .catch(() => {
                toast.warning(this.props.t('general.error_reload_page'));
            });
    };

    handleUpdateAssetDocument = (document) => {
        let documents = this.state?.documents;
        let indexDocument = documents.findIndex((item) => item?.id === document?.id);
        documents[indexDocument] = document;
        this.setState({ documents });
    };

    handleSetDataSelect = (data, source) => {
        this.setState({
            [source]: data,
        });
    };

    handleSaveImageAsset = (item) => {
        this.setState({ imageUrl: item });
    };

    componentDidMount() {}

    render() {
        let { open, t, i18n, isAddAsset, isQRCode, isDisableQuantity } = this.props;
        let { warrantyMonth, yearPutIntoUse, isReceivingAsset, quantity, loading } = this.state;

        return (
            <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth={true} scroll={'paper'}>
                <div className={clsx('wrapperButton', !loading && 'hidden')}>
                    <CircularProgress className="buttonProgress" size={24} />
                </div>
                <ValidatorForm
                    id="parent"
                    onSubmit={(event) => this.handleFormSubmit(event)}
                    class="validator-form-scroll-dialog"
                >
                    <DialogTitle
                        id="draggable-dialog-title"
                        style={{ cursor: 'move', paddingTop: '5px', paddingBottom: '5px' }}
                    >
                        <span className="mb-1">{t('Asset.assetDialog')}</span>
                    </DialogTitle>

                    <DialogContent style={{ paddingTop: '0px', paddingBottom: '0px' }}>
                        <Grid container spacing={1} className="">
                            <ScrollableTabsButtonForce
                                t={t}
                                i18n={i18n}
                                item={this.state}
                                quantityCode={this.state.quantityCode}
                                itemAssetDocument={this.state.item}
                                refreshCode={this.refreshCode}
                                selectAssetGroup={this.selectAssetGroup}
                                selectProduct={this.selectProduct}
                                handleChange={this.handleChange}
                                // selectSupply={this.selectSupply}
                                selectAssetSource={this.selectAssetSource}
                                selectUseDepartment={this.selectUseDepartment}
                                // selectSupplyUnit={this.selectSupplyUnit}
                                selectManufacturer={this.selectManufacturer}
                                selectStatus={this.selectStatus}
                                selectStores={this.selectStores}
                                handleDateChange={this.handleDateChange}
                                handleChangeFormatNumber={this.handleChangeFormatNumber}
                                openSelectSupplyPopup={this.openSelectSupplyPopup}
                                handleSelectSupply={this.handleSelectSupply}
                                handleSelectSupplyPopupClose={this.handleSelectSupplyPopupClose}
                                openSelectProductPopup={this.openSelectProductPopup}
                                handleSelectProduct={this.handleSelectProduct}
                                handleSelectProductPopupClose={this.handleSelectProductPopupClose}
                                shouldOpenAssetAllocationEditorDialog={this.state.shouldOpenAssetAllocationEditorDialog}
                                openAssetAllocationEditorDialog={this.openAssetAllocationEditorDialog}
                                handleCloseAssetAllocationEditorDialog={this.handleCloseAssetAllocationEditorDialog}
                                handleSelectAssetGroup={this.handleSelectAssetGroup}
                                handleSelectAssetGroupPopupClose={this.handleSelectAssetGroupPopupClose}
                                openSelectAssetGroupPopup={this.openSelectAssetGroupPopup}
                                openSelectDepartmentPopup={this.openSelectDepartmentPopup}
                                handleSelectManagementDepartment={this.handleSelectManagementDepartment}
                                handleSelectDepartmentPopupClose={this.handleSelectDepartmentPopupClose}
                                openSelectUseDepartmentPopup={this.openSelectUseDepartmentPopup}
                                handleSelectUseDepartment={this.handleSelectUseDepartment}
                                handleSelectUseDepartmentPopupClose={this.handleSelectUseDepartmentPopupClose}
                                openSelectUsePersonPopup={this.openSelectUsePersonPopup}
                                handleSelectUsePerson={this.handleSelectUsePerson}
                                handleSelectUsePersonPopupClose={this.handleSelectUsePersonPopupClose}
                                handleRowDataCellChange={this.handleRowDataCellChange}
                                handleChangeQuantity={this.handleChangeQuantity}
                                handleAddAttribute={this.handleAddAttribute}
                                openPopupSelectAttribute={this.openPopupSelectAttribute}
                                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                                handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
                                handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
                                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                                isAddAsset={isAddAsset}
                                handleSelectAttributePopupClose={this.handleSelectAttributePopupClose}
                                handleRowDataCellDelete={this.handleRowDataCellDelete}
                                openPopupSelectAssetSource={this.openPopupSelectAssetSource}
                                handleAddAssetSource={this.handleAddAssetSource}
                                handleSelectAssetSourcePopupClose={this.handleSelectAssetSourcePopupClose}
                                handleRowDataCellChangeAssetSource={this.handleRowDataCellChangeAssetSource}
                                handleRowDataCellDeleteAssetSource={this.handleRowDataCellDeleteAssetSource}
                                handleChangeSelect={this.handleChangeSelect}
                                selectMedicalEquipment={this.selectMedicalEquipment}
                                selectUnit={this.selectUnit}
                                selectShoppingForm={this.selectShoppingForm}
                                quantity={quantity}
                                warrantyMonth={warrantyMonth}
                                yearPutIntoUse={yearPutIntoUse}
                                isQRCode={isQRCode}
                                isDisableQuantity={isDisableQuantity}
                                getAssetDocument={this.getAssetDocument}
                                handleUpdateAssetDocument={this.handleUpdateAssetDocument}
                                handleClose={this.handleClose}
                                handleCloseAll={this.handleCloseAll}
                                handleSetDataSelect={this.handleSetDataSelect}
                                handleSaveImageAsset={this.handleSaveImageAsset}
                            />
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <div className="flex flex-space-between flex-middle">
                            <Button
                                variant="contained"
                                className="mr-12"
                                color="secondary"
                                onClick={() => {
                                    this.props.handleClose();
                                }}
                            >
                                {t('general.cancel')}
                            </Button>
                            {(this.state.status ? this.state.status.indexOrder : null) !==
                                appConst.listStatusAssets.DA_THANH_LY.indexOrder &&
                                isReceivingAsset && (
                                    <Button className="mr-12" variant="contained" color="primary" type="submit">
                                        {t('general.add')}
                                    </Button>
                                )}
                            {(this.state.status ? this.state.status.indexOrder : null) !==
                                appConst.listStatusAssets.DA_THANH_LY.indexOrder &&
                                !isReceivingAsset && (
                                    <Button className="mr-12" variant="contained" color="primary" type="submit">
                                        {t('general.save')}
                                    </Button>
                                )}
                            {this.props.isButtonAllocation && (
                                <Button
                                    className="mr-12"
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    onClick={this.handleFormSubmitAllocation}
                                >
                                    {t('general.saveAndAllocation')}
                                </Button>
                            )}
                        </div>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

AssetEditorDialog.contextType = AppContext;
export default AssetEditorDialog;
