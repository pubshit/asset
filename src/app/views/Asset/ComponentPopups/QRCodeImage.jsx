function QRCodeImage(props) {
    let { item, width, height, isScale } = props;
    let qrCode = item?.qrCode || item.qrcode
    let styles = isScale
        ? { width: "100%", margin: "auto", transform: "scale(1.2)" }
        : { width: "100%", margin: "auto" }
    return (
        <img src={qrCode ? `data:image/png;base64,${qrCode}` : ""} alt="QR" style={styles} />
    );
}

export default QRCodeImage;