import React from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import QRCodeImage from "./QRCodeImage";
import { formatTimestampToDate } from 'app/appFunction';
import {PaperComponent} from "../../Component/Utilities";

const styles = {
  divContent: {
    overflow: "auto",
    maxHeight: 430,
    fontSize: "14px",
  },
  container: {
    // width: "30%",
    // width: "45%",
    // border: "1px solid black",
    display: "flex",
    // flexDirection: "column",
  },
  containerLg: {
    width: "45%",
    display: "flex",
    // flexDirection: "column",
  },
  flex_justify: {
    display: "grid",
    // justifyContent : "center",
    gridTemplateColumns: "1fr 1fr",
    gap: "10px",
    // flexWrap: "wrap",
  },
  flex_columns: {
    display: "flex",
    flexDirection: "column",
  },
  flex: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    gap: "10px",
  },
  flexMiddle: {
    alignItems: "center",
  },
  flexCenter: {
    justifyContent: "center",
  },
  border: {
    border: "1px solid",
    paddingLeft: 8,
    background: "white",
  },
  borderQRImage: {
    border: "1px solid",
    background: "white",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  textAlignCenter: {
    textAlign: "center",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  w_65: {
    width: "65%",
  },
  w_25: {
    width: "25%",
  },
  w_35: {
    width: "35%",
  },
  w_30: {
    width: "30%",
  },
  w_40: {
    width: "45%",
  },
  qrCodeLg: {
    margin: "auto",
    width: "100%",
    height: "100%",
  },
  ellipsis: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    width: 265
  }
}

class AssetsQRPrintNew extends React.Component {
  state = {
    items: [],
  };

  componentWillMount() {
    this.setState({ ...this.props.item, });
  }

  componentDidMount() { }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  render() {
    const { t } = this.props;
    let { open, items } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Phiếu in mã QR")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{ height: "0px", width: "0px", position: "absolute" }}
        ></iframe>
        <div>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <DialogContent id="divcontents" style={styles.divContent}>
              <div style={styles.flex_justify}>
                {
                  // style={items?.length <= 2 ? styles.containerLg : styles.container}
                  items.map(item => {
                    return <div style={styles.container}>
                      <table style={styles.table} className="table-report">
                        <tbody>
                          <tr style={{
                            ...styles.borderQRImage,
                            ...styles.textAlignCenter
                          }}>
                            <td rowspan="6" style={{
                              ...styles.borderQRImage,
                              ...styles.textAlignCenter,
                              ...styles.w_35
                            }}>
                              <div style={{ ...styles.flex_columns }}>
                                {/* Logo */}
                                {/* <div>
                                  AMMIS
                                </div> */}
                                {item?.qrCode && <QRCodeImage item={item} width="100%" height="100%" />}
                              </div>
                            </td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter,
                              ...styles.w_25
                            }}>Nhóm TS</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft,
                              ...styles.w_40
                            }}>{item?.assetGroup?.name || item?.assetGroupName}</td>
                          </tr>
                          <tr >
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter
                            }}>MSTS</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft
                            }}>{item?.code}</td>
                          </tr>
                          <tr >
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter
                            }}>TGSD</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft
                            }}>{item?.yearPutInToUse || item?.dayStartedUsing ? formatTimestampToDate(new Date(item?.dayStartedUsing)) : ""}</td>
                          </tr>
                          <tr >
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter
                            }}>DVSD</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft
                            }}>{item?.useDepartment?.name || item?.useDepartmentName}</td>
                          </tr>
                          <tr >
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter
                            }}>Người SD</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft
                            }}>{item?.person?.name || item?.usePersonName}</td>
                          </tr>
                          <tr style={{
                            ...styles.border,
                            ...styles.textAlignCenter
                          }}>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignCenter
                            }}>SERIAL</td>
                            <td style={{
                              ...styles.border,
                              ...styles.textAlignLeft
                            }}>{item?.serialNumber}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  })
                }
              </div>
            </DialogContent>

            <DialogActions>
              <div className="flex flex-space-between flex-middle">
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={() => this.props.handleClose(false)}
                >
                  {t("general.cancel")}
                </Button>
                <Button variant="contained" color="primary" type="submit">
                  {t("In")}
                </Button>
              </div>
            </DialogActions>
          </ValidatorForm>
        </div>
      </Dialog>
    );
  }
}

export default AssetsQRPrintNew;
