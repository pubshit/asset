import React from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import QRCode from "qrcode.react";
import {PaperComponent} from "../../Component/Utilities";

const styles = {
  divContent: {
    overflow: "auto",
    maxHeight: 430,
    fontSize: "11px",
  },
  container: {
    width: "32%",
  },
  containerLg: {
    width: "45%",
  },
  horizontalContainer: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gap: "10px",
  },
  flex: {
    display: "flex",
  },
  flexMiddle: {
    alignItems: "center",
  },
  flexCenter: {
    justifyContent: "center",
  },
  border: {
    border: "1px solid",
    padding: 8,
    background: "white",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
    maxWidth: "400px",
  },
  textAlignCenter: {
    textAlign: "center",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  w_65: {
    width: "65%",
  },
  w_35: {
    width: "35%",
  },
  qrCodeLg: {
    margin: "auto",
    width: "100%",
    height: "100%",
  },
  fz_13: {
    fontSize: "13px"
  },
}

class AssetsQRPrint extends React.Component {
  state = {
    items: [],
  };
  
  componentDidMount() {
    this.setState({ ...this.props.item, });
  }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  render() {
    const { t } = this.props;
    let { open, items } = this.props;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Phiếu in mã QR")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{ height: "0px", width: "0px", position: "absolute" }}
        ></iframe>
        <div>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id="divcontents" style={styles.divContent}>
            <div style={styles.horizontalContainer}>
              {
                items.map(item => {
                  return <div style={{...styles.flex, ...styles.flexCenter}} key={item.id}>
                    <table style={styles.table} className="table-report">
                      <thead>
                        <tr>
                          <th
                            style={{
                              ...styles.w_35,
                              ...styles.border,
                              ...styles.fz_13,
                            }}
                          >
                            <div style={styles.textAlignCenter}>{item?.asset?.code}</div>
                            <div style={styles.textAlignCenter}>{item?.asset?.managementCode || ""}&nbsp;</div>
                          </th>
                          <th
                            style={{
                              ...styles.w_65,
                              ...styles.border,
                              ...styles.fz_13,
                            }}
                          >
                            {item?.asset?.name || ""}
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style={{
                            ...styles.border,
                          }}>
                            {item?.qrInfo && (
                              <QRCode
                                style={styles.qrCodeLg}
                                value={item?.qrInfo}
                                renderAs="svg"
                              />
                            )}
                          </td>
                          <td style={{
                            ...styles.border,
                            ...styles.fz_13,
                          }}>
                            <div>Serial number: {item?.assetSerialNumber}</div>
                            <div>Model: {item?.assetModel}</div>
                            <div>Nước sx: {item?.asset?.madeIn}</div>
                            <div>Hãng sx: {item?.manufacturerName}</div>
                            <div>Năm sx: {item?.asset?.yearOfManufacture}</div>
                            <div>Năm sd: {item?.asset?.yearPutIntoUse}</div>
                            <div>PBSD: {item?.receiveDepartmentName}</div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                })
              }
            </div>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose(false)}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit" className="mr-12">
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
        </div>
      </Dialog>
    );
  }
}

export default AssetsQRPrint;
