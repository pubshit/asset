import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
  Grid,
  IconButton,
  Icon,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import "../../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { checkInvalidDate } from "app/appFunction";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import MaterialTable, { MTableToolbar } from "material-table";
import CustomValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import moment from "moment";
import {PaperComponent} from "../../Component/Utilities";
import CustomValidatorForm from "../../Component/ValidatorForm/CustomValidatorForm";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

class AssetAccessoryDetails extends Component {
  state = {
    assetAccessoryDetails: [],
    isSubmit: false,
  };

  handleChange = (event, data) => {
    let { assetAccessoryDetails = [] } = this.state;
    let item = assetAccessoryDetails ? assetAccessoryDetails[data.tableData.id] : {};
    if (item) {
      item[event.target.name] = event.target.value;
    }
    this.setState({ assetAccessoryDetails });
  };


  handleFormSubmit = (e) => {
    let { assetAccessories = [], item } = this.props;

    let itemSparePart = {...item} ?? {};
    if (itemSparePart) {
      itemSparePart.assetAccessoryDetails = this.state?.assetAccessoryDetails?.length > 0 ? [...this.state.assetAccessoryDetails] : [];
    }
    assetAccessories[itemSparePart?.tableData?.id] = itemSparePart;
   
    this.props?.setListSpareparts && this.props.setListSpareparts(assetAccessories)
    this.props?.handleClose && this.props.handleClose()
  };

  handleDateChange = (date, name, data) => {
    let { assetAccessoryDetails = [] } = this.state;
    let item = assetAccessoryDetails ? assetAccessoryDetails[data.tableData.id] : {};
    if (item) {
      item[name] = moment(date).format("YYYY-MM-DD");
    }
    this.setState({ assetAccessoryDetails });
  };


  componentDidMount() {
    let { item } = this.props;
    this.setState({
      assetAccessoryDetails: item?.assetAccessoryDetails?.length > 0 ? [...item?.assetAccessoryDetails] : []
    })
  }

  handleClickCheckbox = (event, data) => {
    let { assetAccessoryDetails = [] } = this.state;
    let item = assetAccessoryDetails ? assetAccessoryDetails[data.tableData.id] : {};
    if (item) {
       item.status = event.target.checked ? appConst.listStatusSpareParts.HOAT_DONG.code : appConst.listStatusSpareParts.DA_HONG.code
    }
    this.setState({ assetAccessoryDetails });
  };

  handleBlurDate = (date, name, data) => {
    if (checkInvalidDate(date)) {
      this.handleDateChange(null, name, data);
    }
  }

  handleRowDataCellDeleteAssetAccessoryDetail = (rowData) => {
    let assetAccessoryDetails = this.state.assetAccessoryDetails.filter(
      (item, index) => index !== rowData.tableData.id
    );
    this.setState({
      assetAccessoryDetails: assetAccessoryDetails,
    });
  };


  handleAddNewAssetAccessoryDetail = () => {
    let { assetAccessoryDetails = [] } = this.state;
    let newArray = assetAccessoryDetails ? assetAccessoryDetails : []
    newArray.push({
      serialNumber: "",
      model: "",
      expirationDate: moment(new Date()).format("YYYY-MM-DD"),
      description: "",
      status: appConst.listStatusSpareParts.HOAT_DONG.code,
    })
    this.setState({ assetAccessoryDetails: newArray });
  };


  render() { 
    let { open, t, isView } = this.props;

    let columns = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      minWidth: '50px',
      maxWidth: "50px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.serialNumber"),
      field: "serialNumber",
      minWidth: "100px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
       render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={e => this.handleChange(e, rowData)}
          type="text"
          name="serialNumber"
          value={rowData?.serialNumber}
          InputProps={{
            readOnly: isView
          }}
          validators={['required']}
          errorMessages={t('general.required')}
        />
    },
    {
      title: t("Asset.model"),
      field: "model",
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={e => this.handleChange(e, rowData)}
          type="text"
          name="model"
          value={rowData?.model}
          InputProps={{
            readOnly: isView
          }}
          validators={['required']}
          errorMessages={t('general.required')}
        />
    },
    {
      title: t("Asset.expirationDate"),
      field: "expirationDate",
      align: "left",
      minWidth: "150px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
        <CustomValidatePicker
          type="text"
          format="dd/MM/yyyy"
          name='expirationDate'
          value={rowData?.expirationDate}
          onChange={date => this.handleDateChange(date, "expirationDate", rowData)}
          readOnly={isView}
          minDate={new Date()}
          disabled={isView}
          validators={['required']}
          errorMessages={t('general.required')}
          minDateMessage={"Hạn bảo hành không được nhỏ hơn ngày hiện tại"}
          onBlur={() => this.handleBlurDate(rowData?.expirationDate, "expirationDate", rowData)}
        />
    },
    {
      title: t("general.description"),
      field: "description",
      align: "left",
      minWidth: "200px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={e => this.handleChange(e, rowData)}
          type="text"
          name="description"
          value={rowData?.description}
          InputProps={{
            readOnly: isView
          }}
        />
    },
    {
      title: t("Asset.status"),
      field: "status",
      align: "left",
      minWidth: "50px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      <FormControlLabel
        label={rowData?.status ? t("SparePart.is_work") : t("SparePart.has_broken")}
        value={rowData?.status}
        name="status"
        control={
          <Checkbox
            id={`radio${rowData?.id}`}
            style={{ padding: "0px", paddingLeft: "8px" }}
            name="status"
            value={rowData?.status}
            checked={rowData?.status === appConst.listStatusSpareParts.HOAT_DONG.code}
            onClick={(event) => this.handleClickCheckbox(event, rowData)}
            disabled={isView}
          />
        }
      />
         
    },
    {
      title: t("Asset.action"),
      field: "",
      align: "left",
      minWidth: 80,
      maxWidth: 80,
      hidden: isView,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
        <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              this.handleRowDataCellDeleteAssetAccessoryDetail(rowData);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />
    },
  ];
    
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
        scroll="paper"
      >
        <CustomValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle className="cursor-move mb-0" id="draggable-dialog-title">
            {t("SparePart.dialog")}
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <div>
              <Grid spacing={2}>
                {!isView && (
                  <Grid item md={12} sm={12} xs={12}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleAddNewAssetAccessoryDetail}
                      className="mt-10 mb-10"
                    >
                      {t("+")}
                    </Button>
                  </Grid>
                )}
                <MaterialTable
                  data={this.state.assetAccessoryDetails ?? []}
                  columns={columns}
                  options={{
                    draggable: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    rowStyle: rowData => ({
                      backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    headerStyle: {
                      backgroundColor: '#358600',
                      color: '#fff',
                      paddingLeft: 10,
                      paddingRight: 10,
                      textAlign: "center",
                    },
                    padding: 'dense',
                    maxBodyHeight: "350px",
                    minBodyHeight: this.state.assetAccessoryDetails?.length > 0
                      ? 0 : 73,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                    }
                  }}
                  components={{
                    Toolbar: props => (
                      <div className="w-100">
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </div>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                className="btnClose mb-16 mr-16"
                variant="contained"
                color="secondary"
                onClick={() => {
                  let { assetAccessories = [] } = this.props;  
                  this.props?.setListSpareparts && this.props.setListSpareparts(assetAccessories)
                  this.props.handleClose();
                }}
              >
                {t("InstrumentToolsTransfer.close")}
              </Button>
              {
                !isView &&
                <Button
                  className="mb-16 mr-16"
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
            </div>
          </DialogActions>
        </CustomValidatorForm>
      </Dialog>
    );
  }
}
AssetAccessoryDetails.contextType = AppContext;
export default AssetAccessoryDetails;
