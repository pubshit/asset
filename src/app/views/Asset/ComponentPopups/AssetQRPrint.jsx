import React from "react";
import {
  Dialog,
  Button,
  DialogActions,
  Grid,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import QRCode from "qrcode.react";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const styles = {
  container: {
    width: 340,
    border: "1px solid black",
  },
  grid: {
    width: "100%",
    display: 'grid',
    gridTemplateColumns: '1fr 2fr',
    fontSize: "1em",
  },
  header: {
    minHeight: 50,
    borderBottom: "1px solid black",
  },
  assetCode: {
    paddingTop: 4,
    fontWeight: "bold",
    marginBottom: -5,
    marginTop: 0,
    minWidth: 120,
  },
  managementCode: {
    fontWeight: "400",
    margin: 0,
  },
  textCenter: {
    textAlign: "center",
  },
  productName: {
    padding: '4px 8px 0',
    borderLeft: '1px solid black',
  },
  productInfo: {
    padding: "8px 8px 0",
    margin: 0,
  },
  productInfoContainer: {
    borderLeft: '1px solid black',
    display: 'grid',
    gridTemplateRows: '1fr 1fr 1fr 1fr',
  },
  qrCodeContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 120,
    minWidth: 120,
  },
  qrCode: {
    width: 100,
    height: 100,
  },
}

class AssetQRPrint extends React.Component {
  state = {
    AssetAllocation: [],
    item: {},
    asset: {},
    assetVouchers: [],
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
  };

  componentWillMount() {
    this.setState({ ...this.props.item, });
  }

  componentDidMount() { }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  render() {
    const { t } = this.props;
    let { open, qrValue, item } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Phiếu in mã QR")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{ height: "0px", width: "0px", position: "absolute" }}
        ></iframe>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id="divcontents" >
            <Grid container style={{ justifyContent: 'center' }}>
              <div style={styles.container}>
                <div style={{ ...styles.grid, ...styles.header }}>
                  <div>
                    <p style={{ ...styles.assetCode, ...styles.textCenter }}>{item?.code}</p>
                    <p style={{ ...styles.managementCode, ...styles.textCenter }}>
                      {item?.managementCode ? `(${item?.managementCode})` : ""}
                    </p>
                  </div>
                  <div style={styles.productName}>
                    {item?.product?.name}
                  </div>
                </div>
                <div style={styles.grid}>
                  <div style={styles.qrCodeContainer}>
                    {qrValue && (
                      <QRCode
                        style={styles.qrCode}
                        value={qrValue}
                        renderAs="svg"
                      />
                    )}
                  </div>
                  <div style={styles.productInfoContainer}>
                    <p style={styles.productInfo}>Nước sx: {item?.madeIn}</p>
                    <p style={styles.productInfo}>Năm sx: {item?.yearOfManufacture}</p>
                    <p style={styles.productInfo}>Ngày sd: {item?.yearPutIntoUse}</p>
                    <p style={styles.productInfo}>PBSD: {item?.useDepartment?.name}</p>
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose(false)}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AssetQRPrint;
