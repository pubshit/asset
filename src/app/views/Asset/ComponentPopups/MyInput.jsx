import React, {Component} from "react";
import {
    TextField
  } from "@material-ui/core";

class MyInput extends Component {
  constructor() {
    super();
    this.state = { value: "" };
  }

  update = (e) => {
    this.setState({value : e.target.value});
  }

  render() {
    let {keyPress, label, type, nameState} = this.props;
    let {value} = this.state;
    return <TextField id="standard-basic" fullWidth label={label} type={type}
        size="small"
        onChange={this.update}
        onKeyDown={(e) => keyPress(e, value, nameState)}
    />;
  }
}

export default MyInput;
