import {
    Grid,
    Button,
    InputAdornment,
    Input,
    Checkbox,
    TablePagination,
    Dialog,
    DialogActions,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from "material-table";
import { searchByPage } from "../../AssetSource/AssetSourceService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import AssetSourceDialog from "app/views/AssetSource/AssetSourceDialog";
import { appConst } from "app/appConst";
import {PaperComponent} from "../../Component/Utilities";

class SelectMultiAssetSourcePopup extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        rowsPerPage: 5,
        page: 0,
        data: [],
        totalElements: 0,
        itemList: [],
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        selectedItem: {},
        keyword: "",
        shouldOpenProductDialog: false,
        assetSources: [],
        shouldOpenProductAttributeDialog: false,
    };

    setPage = (page) => {
        this.setState({ page }, function () {
            this.updatePageData();
        });
    };

    setRowsPerPage = (event) => {
        this.setState(
            { rowsPerPage: event.target.value, page: 0 },
            function () {
                this.updatePageData();
            }
        );
    };

    handleChangePage = (event, newPage) => {
        this.setPage(newPage);
    };

    updatePageData = () => {
        let searchObject = {};
        searchObject.keyword = this.state.keyword.trim();
        searchObject.pageIndex = this.state.page + 1;
        searchObject.pageSize = this.state.rowsPerPage;
        searchByPage(searchObject).then(({ data }) => {
            // nếu id của 5 cái này có trong properties thì sẽ thay trạng thái isCheck bằng true
            let itemListClone = data?.data?.content || [];
            itemListClone.map((item) => {
                item.isCheck = false;
                // eslint-disable-next-line no-unused-expressions
                this.state?.assetSources?.forEach((assetSource) => {
                    if ((assetSource?.assetSource?.id || assetSource?.id) === item?.id) {
                        item.isCheck = true;
                    }
                });
            });
            this.setState({
                itemList: [...itemListClone],
                totalElements: data?.data?.totalElements,
            });
        });
    };

    componentDidMount() {
        this.setState({ assetSources: this.props.assetSources }, () =>
            this.updatePageData()
        );
    }

    handleClick = (event, item) => {
        item.isCheck = event.target.checked;
        let { assetSources } = this.state;

        if (item?.isCheck) {
            let check = assetSources.find(
                (assetSource) => assetSource?.assetSource?.id === item?.id
            );
            if (!check?.isCheck) {
                let itemAssetSource = {};
                itemAssetSource.assetSource = item;
                assetSources = assetSources.concat(itemAssetSource);
            }
        } else {
            assetSources = assetSources.filter(
                (element) => element?.assetSource?.id !== item.id
            );
        }
        this.setState({ assetSources: assetSources });
    };

    componentWillMount() { }

    handleKeyDownEnterSearch = (e) => {
        if (appConst.KEY.ENTER === e.key) {
            this.search();
        }
    };

    handleKeyUp = (e) => {
        this.search();
    };

    search() {
        this.setPage(0);
    }

    handleChange = (event, source) => {
        event.persist();
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    handleOpenProductDialog = () => {
        this.setState({
            shouldOpenProductDialog: true,
        });
    };

    handleDialogProductClose = () => {
        this.setState({
            shouldOpenProductDialog: false,
        });
    };

    handleOpenProductAttributeDialog = () => {
        this.setState({
            shouldOpenProductAttributeDialog: true,
        });
    };

    handleDialogProductAttributeClose = () => {
        this.setState(
            {
                shouldOpenProductAttributeDialog: false,
            },
            () => this.updatePageData()
        );
    };

    handleOKEditClose = () => {
        this.setState(
            {
                shouldOpenProductDialog: false,
                shouldOpenProductAttributeDialog: false,
            },
            () => this.updatePageData()
        );
    };

    handleConventDataAsset = (value) => {
        const assetSource = {
            code: value?.code,
            codeOriginHI: value?.codeOriginHI,
            id: value?.id,
            name: value?.name,
            nameOriginHI: value?.nameOriginHI
        };

        this.setState(prevState => ({
            assetSources: [...prevState.assetSources, { assetSource }]
        }));
    }

    onClickRow = (selectedRow) => {
        document.querySelector(`#radio${selectedRow.id}`).click();
    };

    render() {
        const { t, i18n, handleClose, handleSelect, open } = this.props;
        let {
            keyword,
            itemList,
            assetSources,
            shouldOpenProductAttributeDialog,
        } = this.state;
        let columns = [
            {
                title: t("general.select"),
                field: "custom",
                align: "center",
                width: "90px",
                cellStyle: {
                    paddingTop: "0px",
                    paddingBottom: "0px",
                },
                render: (rowData) => (
                    <Checkbox
                        id={`radio${rowData.id}`}
                        name="radSelected"
                        value={rowData.id}
                        checked={rowData.isCheck}
                        onClick={(event) => this.handleClick(event, rowData)}
                    />
                ),
            },
            {
                title: t("AssetSource.code"),
                field: "code",
                align: "left",
                width: "180px",
                cellStyle: {
                    textAlign: "center",
                }
            },
            { title: t("AssetSource.name"), field: "name", width: "150" },
        ];
        return (
            <Dialog
                onClose={handleClose}
                open={open}
                PaperComponent={PaperComponent}
                maxWidth={"md"}
                fullWidth
            >
                <DialogTitle
                    style={{ cursor: "move" }}
                    id="draggable-dialog-title"
                >
                    {this.props.isIat ? (
                        <span className="mb-20">
                            {t("InstrumentToolsList.tabInstrumentToolsSourceFile")}
                        </span>
                    ) : (
                        <span className="mb-20">{t("AssetSource.title")}</span>
                    )}
                </DialogTitle>
                <DialogContent>
                    <Grid item xs={12}>
                        <Input
                            label={t("general.enterSearch")}
                            type="text"
                            name="keyword"
                            value={keyword}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDownEnterSearch}
                            style={{ width: "50%" }}
                            className="mb-16 mr-12"
                            id="search_box"
                            placeholder={t("general.enterSearch")}
                            startAdornment={
                                <InputAdornment position="end">
                                    <Link to="#">
                                        {" "}
                                        <SearchIcon
                                            onClick={() => this.search(keyword)}
                                            style={{
                                                position: "absolute",
                                                top: "0",
                                                right: "0",
                                            }}
                                        />
                                    </Link>
                                </InputAdornment>
                            }
                        />
                        {itemList.length !== 0 ? (
                            ""
                        ) : (
                            <Button
                                className="mb-16 mr-16 align-bottom"
                                variant="contained"
                                color="primary"
                                onClick={() =>
                                    this.handleOpenProductAttributeDialog()
                                }
                            >
                                {this.props.isIat
                                    ? t("InstrumentToolsList.addSource")
                                    : t("Asset.add_assetSource")}
                            </Button>
                        )}
                    </Grid>
                    <div>
                        {shouldOpenProductAttributeDialog && (
                            <AssetSourceDialog
                              t={t}
                              i18n={i18n}
                              handleClose={this.handleDialogProductAttributeClose}
                              handleSelect={(value) => this.handleConventDataAsset(value)}
                              handleSetShowToast={this.props.handleSetShowToast}
                              open={shouldOpenProductAttributeDialog}
                              handleOKEditClose={this.handleOKEditClose}
                              item={{
                                name: keyword,
                              }}
                            />
                        )}
                    </div>
                    <Grid item xs={12}>
                        <MaterialTable
                            data={this.state.itemList}
                            columns={columns}
                            localization={{
                                body: {
                                    emptyDataSourceMessage: `${t(
                                        "general.emptyDataMessageTable"
                                    )}`,
                                },
                            }}
                            options={{
                                draggable: false,
                                toolbar: false,
                                selection: false,
                                actionsColumnIndex: -1,
                                paging: false,
                                search: false,
                                rowStyle: (rowData) => ({
                                    backgroundColor:
                                        rowData.tableData.id % 2 === 1
                                            ? "#EEE"
                                            : "#FFF",
                                }),
                                maxBodyHeight: "253px",
                                headerStyle: {
                                    backgroundColor: "#358600",
                                    color: "#fff",
                                },
                                padding: "dense",
                                minBodyHeight: "253px",
                            }}
                            components={{
                                Toolbar: (props) => (
                                    <div style={{ witdth: "100%" }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                            }}
                            onSelectionChange={(rows) => {
                                this.data = rows;
                            }}
                        />
                        <TablePagination
                            align="left"
                            className="px-16"
                            rowsPerPageOptions={
                                appConst.rowsPerPageOptions.popup
                            }
                            component="div"
                            count={this.state.totalElements}
                            rowsPerPage={this.state.rowsPerPage}
                            page={this.state.page}
                            labelRowsPerPage={t("general.rows_per_page")}
                            labelDisplayedRows={({ from, to, count }) =>
                                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                                }`
                            }
                            backIconButtonProps={{
                                "aria-label": "Previous Page",
                            }}
                            nextIconButtonProps={{
                                "aria-label": "Next Page",
                            }}
                            onPageChange={this.handleChangePage}
                            onRowsPerPageChange={this.setRowsPerPage}
                        />
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button
                        className="mb-16 mr-12 align-bottom"
                        variant="contained"
                        color="secondary"
                        onClick={() => handleClose()}
                    >
                        {t("general.cancel")}
                    </Button>
                    <Button
                        className="mb-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => handleSelect(assetSources)}
                    >
                        {t("general.select")}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}
export default SelectMultiAssetSourcePopup;
