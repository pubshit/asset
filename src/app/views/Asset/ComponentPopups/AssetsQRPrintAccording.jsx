import React from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import QRCodeImage from "./QRCodeImage";
import {OPTIONS_EXCEL_QR, OPTIONS_EXCEL_QR_CARD, OPTIONS_EXCEL_QR_OBJECT} from "../../../appConst";
import { PaperComponent } from "../../Component/Utilities";
import clsx from "clsx";

const styles = {
  divContent: {
    overflow: "auto",
    maxHeight: 430,
    fontSize: "12px",
    backgroundColor: "#EAEAEA",
    display: "flex",
    justifyContent: "center"
  },
  container: {
    border: "0.2px solid black",
    padding: "5px"
  },
  containerLg: {
    width: "45%",
    display: "flex",
    // flexDirection: "column",
  },
  flex_justify: {
    display: "grid",
    justifyContent: "center",
    gridTemplateColumns: "repeat(3, 1fr)",
  },
  flex_columns: {
    display: "flex",
  },
  flexCenter: {
    justifyContent: "center",
  },
  border: {
    border: "1px solid",
    background: "white",
  },
  borderQRImage: {
    border: "1px solid",
    background: "white",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  textAlignCenter: {
    textAlign: "center",
    verticalAlign: "middle",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  w_65: {
    width: "65%",
  },
  w_25: {
    width: "25%",
  },
  w_35: {
    width: "35%",
  },
  w_30: {
    width: "30%",
  },
  w_45: {
    width: "45%",
  },
  w_55: {
    width: "55%"
  },
  qrCodeLg: {
    margin: "auto",
    width: "100%",
    height: "100%",
  },
  ellipsis: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    width: 265
  },
  productInfo: {
    lineHeight: 1.4,
    fontSize: "12px",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  margin_left: {
    marginLeft: 15
  },
  margin_left_5: {
    marginLeft: 5
  },
  text_overflow_ellipsis: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis"
  },
  max_w_120: {
    maxWidth: 120
  },
  overflow_hidden: {
    overflowHidden: true,
    width: "100%"
  },
  verticalText: {
    transform: "rotate(-90deg)",
    width: "12px",
    marginLeft: "2px"
  },
  border_suffering: {
    border: "none",
    background: "white",
    whiteSpace: "nowrap",
    verticalAlign: "bottom"
  },
  border_suffering_4: {
    border: "1px solid",
    background: "white",
    whiteSpace: "nowrap",
  },
  border_top_bootom: {
    borderBottom: "0.2px solid black",
    borderTop: "0.2px solid black",
  },
  whiteSpace: {
    whiteSpace: "nowrap",
  },
  continue_suffering: {
    width: "70mm",
    height: "40mm",
    marginTop: "3mm"
  },
  continue_suffering_2: {
    width: "60mm",
  },
  continue_suffering_4: {
    width: "35mm",
    height: "22mm",
    marginTop: "3mm",
    overflow: "hidden",
  },
  continue_suffering_3: {
    width: "50mm",
    height: "30mm",
    marginTop: "3mm",
  }
}

class AssetsQRPrintAccording extends React.Component {
  state = {
    items: [],
  };

  componentDidMount() {
    this.setState({ ...this.props.item, });
  }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;

    pri.document.open();
    pri.document.write(this.getFormStyleByConfig());
    pri.document.write(content.outerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  getFormStyleByConfig = () => {
    return `
      <style>
        @page {
          margin: 0px;
          padding: 0px; 
        }
        body {
          margin: 0px;
          background: #EAEAEA;
          page-break-before: always;
        }
        #divcontents {
          background: #EAEAEA;
          display: flex;
          overflow: unset !important;
          max-height: unset !important;
        }
        .page-break {
          page-break-before: always;
        }
      </style>
    `;
  }

  handleCheckCode = (typeSuffering) => {
    let listSuffering = [
      OPTIONS_EXCEL_QR[0].code,
      OPTIONS_EXCEL_QR[1].code
    ]
    return listSuffering.includes(typeSuffering)
  }

  convertString = (str = "", length = 15) => str?.length > length ? str?.slice(0, length) + "..." : str?.length ? str : "";

  render() {
    const { t } = this.props;
    let { open, items, typeSuffering } = this.props;
    let is35x22 = typeSuffering === OPTIONS_EXCEL_QR_OBJECT._35x22.code;
    let is60x40 = typeSuffering === OPTIONS_EXCEL_QR_OBJECT._60x40.code;
    let is70x40 = typeSuffering === OPTIONS_EXCEL_QR_OBJECT._70x40.code;
    const pageNumber = {
      type0: { value: 5 },
      type3: { value: 27 },
    }
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Phiếu in mã QR")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{ height: "0px", width: "11cm", position: "absolute" }}
        ></iframe>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} >
          <DialogContent
            id="divcontents"
            style={styles.divContent} >
            <div
              style={{
                fontSize: "12px",
                width: "110mm",
                display: is35x22 ? "grid" : "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                gridTemplateColumns: is35x22 ? " repeat(3, 1fr)" : "",
                background: "#fff",
                height: "fit-content",
              }}>
              {
                items.map((item, index) => {
                  let isBreakPage = (index) % pageNumber.type0.value === 0 && !is35x22;
                  let isBreakPageType3 = [0, 1, 2].some(offset => (index - offset) % pageNumber.type3.value === 0) && is35x22;

                  return <div
                    key={index}
                    className={clsx(is35x22
                        ? (isBreakPageType3 ? `page-break` : '')
                        : (isBreakPage ? `page-break` : '')
                    )}
                  >
                      {/* Mẫu khổ 1, 2 */}
                    {this.handleCheckCode(typeSuffering) && (
                      <>
                        <div style={{
                          ...styles.container,
                          ...styles.continue_suffering,
                          ...(typeSuffering === OPTIONS_EXCEL_QR[1].code ? styles.continue_suffering_2 : {}),
                          ...(isBreakPage ? { marginTop: "20mm" } : {})
                        }}>
                          <div style={{ ...styles.padding_left_5, ...styles.text_overflow_ellipsis }}>{item?.name}</div>
                          <div style={{ ...styles.flex_columns }}>
                            <div style={{ width: is60x40 ? "55%" : "50%" }}>
                              <div style={{
                                width: "100%",
                                overflow: "hidden",
                                display: "flex",
                                justifyContent: "center",
                              }}>
                                {(item?.qrcode || item?.qrCode)
                                  && <QRCodeImage
                                    item={item}
                                    isScale={true}
                                  />
                                }
                              </div>
                            </div>
                            <div style={{ width: is60x40 ? "45%" : "40%", ...styles.padding_left_5 }}>
                              <div style={styles.productInfo}>{item?.code}</div>
                              {is70x40 && (
                                <>
                                  <div style={styles.productInfo}>Serial: {item?.serialNumber}</div>
                                  <div style={styles.productInfo}>Model: {item?.model}</div>
                                </>
                              )}
                              <div style={styles.productInfo}>NSX: {item?.yearOfManufacture}</div>
                              <div style={styles.productInfo}>XX: {item?.madeIn}</div>
                              <div style={styles.productInfo}>Hãng: {item?.manufacturer?.name || item?.manufacturerName}</div>
                              <div style={styles.productInfo}>NSD: {item?.yearPutIntoUse}</div>
                              {!is70x40 && (
                                <>
                                  <div style={styles.productInfo}>K/P: {item?.useDepartment?.code}</div>
                                  <div style={styles.productInfo}>Ng SD: {item?.usePerson?.displayName}</div>
                                </>
                              )}
                            </div>
                          </div>
                        </div>
                      </>
                    )}
                    {/* Mẫu khổ 4 */}
                    {typeSuffering === OPTIONS_EXCEL_QR[3].code && (
                      <>
                        <table style={{
                          borderCollapse: "collapse",
                          fontSize: "8px",
                          transform: 'rotate(180deg)',
                          ...styles.continue_suffering_4,
                          ...(isBreakPageType3 ? { marginTop: "20mm" } : {})
                        }}>
                          <tbody>
                            <tr style={{
                              ...styles.border_suffering_4,
                              paddingTop: "3px",
                            }}>
                              <td style={{ writingMode: "tb", border: "1px solid", height: "22mm", ...styles.text_overflow_ellipsis }}>{item?.name}</td>
                              <td style={{ writingMode: "tb", border: "1px solid" }}>{item?.code}</td>
                              <td style={{ ...styles.flex_columns, ...styles.flexCenter, alignItems: "center", width: "100%", height: "100%" }}>
                                <div style={{ width: "80px", overflow: "hidden" }}>
                                  {(item?.qrcode || item?.qrCode)
                                    && <QRCodeImage
                                      item={item}
                                      isScale={true}
                                    />
                                  }
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </>
                    )}
                    {/* Mẫu khổ 3 */}
                    {typeSuffering === OPTIONS_EXCEL_QR[2].code && (
                      <>
                        <table style={{
                          borderCollapse: "collapse",
                          fontSize: "12px",
                          ...styles.continue_suffering_3,
                          ...(isBreakPage ? { marginTop: "20mm" } : {})
                        }} >
                          <thead>
                            <tr style={{ ...styles.border }}>
                              <td style={{ whiteSpace: "nowrap", ...styles.max_w_120 }}> <div style={{ ...styles.text_overflow_ellipsis, width: 180 }}>{item.name}</div></td>
                            </tr>
                          </thead>
                          <tbody style={{ ...styles.border }}>
                            <td style={{ ...styles.border, ...styles.w_55 }}>
                              <div style={{ width: "120px", overflow: "hidden" }}>
                                {(item?.qrcode || item?.qrCode)
                                  && <QRCodeImage
                                    item={item}
                                    isScale={true}
                                  />
                                }
                              </div>
                            </td>
                            <td style={{ ...styles.border_suffering, ...styles.text_overflow_ellipsis }}>
                              <div style={styles.verticalText}>{this.convertString(item?.code ?? "")}</div>
                            </td>
                            <td style={{ ...styles.border_suffering, ...styles.text_overflow_ellipsis }}>
                              <div style={styles.verticalText}>NSX: {this.convertString(item?.yearOfManufacture ?? "", 13)}</div>
                            </td>
                            <td style={{ ...styles.border_suffering, ...styles.text_overflow_ellipsis }}>
                              <div style={styles.verticalText}>Hãng: {this.convertString((item?.manufacturer?.name || item?.manufacturerName) ?? "", 12)}</div>
                            </td>
                            <td style={{ ...styles.border_suffering, ...styles.text_overflow_ellipsis }}>
                              <div style={styles.verticalText}>K/P: {this.convertString(item?.useDepartment?.code ?? "", 13)}</div>
                            </td>
                          </tbody>
                        </table>
                      </>
                    )}
                  </div>
                })
              }
            </div>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose(false)}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AssetsQRPrintAccording;
