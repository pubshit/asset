import React, {useCallback, useEffect, useLayoutEffect, useMemo, useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Grid,
  Icon,
  IconButton,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import SelectMultiAssetSourcePopup from "./ComponentPopups/SelectMultiAssetSourcePopup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { MTableToolbar } from "material-table";
import { isSpecialCharacters } from "app/appFunction";
import RecipeReviewCard from "./Component/RecipeReviewCard";
import GeneralInfomationNew from "./Component/GeneralInfomationNew";
import DepreciationInformation from "./Component/DepreciationInformation";
import CustomMaterialTable from "../CustomMaterialTable";
import { NumberFormatCustom, LightTooltip } from "../Component/Utilities"

import {
  column_SparePart,
  // column_SparePart,
  columns_AssetFile,
  columns_AssetSource,
  columns_Attributes,
} from "./constants";
import SpareParts from "./Component/SpareParts";
import { variable } from "../../appConst";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    paddingTop: 20,
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
  },
  relative: {
    position: "relative",
  },
  customAppBarContainer: {
    display: "flex",
    flexDirection: "column",
    padding: "16px",
    backgroundColor: "#FFF",
    alignItems: "flex-start",
    position: "fixed",
    width: "15%",
    borderRadius: "8px",
  },
  customButtonText: {
    color: theme.custom.primary.main || variable.css.primary.main,
    margin: "4px 0",
    width: "100%",
    justifyContent: "flex-start",
    padding: "10px",
    fontWeight: "600",
    fontSize: "0.875rem",
  },
  activeButton: {
    color: "#FFF",
    backgroundColor: theme.custom.primary.main || variable.css.primary.main,
  },
  customAppBar: {
    marginTop: 0,
    position: "sticky",
    top: 0,
    width: "100%",
  },
  notiMessage: {
    position: "absolute",
    top: "-7px",
    right: "-12px",
    zIndex: 1,
    color: "red",
    backgroundColor: "white",
    animation: "$blink 4s",
  },

  "@keyframes blink": {
    "0%": {
      opacity: 1,
    },
    "50%": {
      opacity: 0,
    },
    "100%": {
      opacity: 1,
    },
  },
}));

export default function ScrollableTabsButtonForceNew(props) {
  const {
    openSelectProductPopup,
    t,
    i18n,
    handleChange,
    item,
    isQRCode,
    handleSetDataSelect,
    selectUnit,
    openSelectAssetGroupPopup,
    selectStores,
    selectStatus,
    openSelectDepartmentPopup,
    handleSelectManagementDepartment,
    handleSelectDepartmentPopupClose,
    openSelectUseDepartmentPopup,
    isAddAsset,
    handleSelectUseDepartment,
    handleSelectUseDepartmentPopupClose,
    warrantyMonth,
    openSelectSupplyPopup,
    handleSelectSupply,
    handleSelectSupplyPopupClose,
    handleClose,
    selectShoppingForm,
    selectMedicalEquipment,
    handleSaveImageAsset,
    handleSelectAssetMain,
    isRoleAccountant,
    handleMapDataAsset,
    isTiepNhan = false,
    isTTHK,
    itemAssetDocument,
    handleSelectAssetGroup,
    isView = false,
    handleChangeSeri,
    handleKeyUp,
    handleDeleteSeri,
    handleSelectMadeIn,
    isEditLocal,
    isDangSuDung = false,
    handleSelectUsePerson,
  } = props;

  const classes = useStyles();
  const [activeIndex, setActiveIndex] = useState(0);
  const rowStyle = useCallback(() => ({
      backgroundColor: (rowData) =>
        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
    }),
    []
  );
  const columnsAssetFile = columns_AssetFile(t, isView);
  let columnsAssetFileActions = [
    {
      title: t("general.action"),
      field: "custom",
      maxWidth: "80px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <div className="none_wrap">
          {!isView && (
            <LightTooltip
              title={t("general.editIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
              >
                <Icon fontSize="small" color="primary">
                  edit
                </Icon>
              </IconButton>
            </LightTooltip>
          )}
          {!isView && (
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() =>
                  props.handleRowDataCellDeleteAssetFile(rowData?.id)
                }
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          )}
          {isView && (
            <LightTooltip
              title={t("general.viewIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.handleRowDataCellViewAssetFile(rowData)}
              >
                <VisibilityIcon size="small" className="iconEye" />
              </IconButton>
            </LightTooltip>
          )}
        </div>
      ),
    },
  ];

  const columnsAttributes = columns_Attributes(t, props, isView);
  let columnsAttributesActions = [
    {
      title: t("general.action"),
      field: "custom",
      maxWidth: "80px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.handleRowDataCellDelete(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
  ];

  const columnsAssetSource = columns_AssetSource(
    t,
    props,
    item,
    handleSelectAssetMain,
    isView,
    NumberFormatCustom
  );
  let columnsAssetSourceActions = [
    {
      title: t("general.action"),
      field: "custom",
      maxWidth: "80px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.handleRowDataCellDeleteAssetSource(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
  ];

  let columnsSparePartActions = [
    {
      title: t("general.action"),
      field: "",
      minWidth: "80px",
      align: "center",
      render: (rowData) => (
        <div className="none_wrap">
          {!isView &&
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props?.handleRowDataCellDeleteSparePart(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          }
          {!item?.isClones && <LightTooltip
            title={t("SparePart.infoIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props?.openAssetAccessoryDetails(rowData)}
            >
              <Icon fontSize="small" color="primary">
                library_add
              </Icon>
            </IconButton>
          </LightTooltip>}
        </div>
      ),
    },
  ];
  const columnSparePart = [
    ...column_SparePart(t, props, isView)
  ];

  useEffect(() => {
    const isManageAccountant = props.item && props.item.isManageAccountant;
    ValidatorForm.addValidationRule("isSpecialCharacters", isSpecialCharacters);
    ValidatorForm.addValidationRule("isManageAccountant", (value) => {
      return isManageAccountant ? (value || value === 0) : true;
    });
  }, [props.item]);

  const scrollFunctions = {
    handleScrollToSection: (sectionId, index) => {
      const sectionElement = document.getElementById(sectionId);
      if (sectionElement) {
        setActiveIndex(index)
        sectionElement.scrollIntoView({ behavior: "smooth" });
      }
    },
  };

  const buttons = [
    {
      label: t("Asset.tab_info_asset"),
      sectionId: "generalInfo",
      isShowMessage: props.item?.isErrorGeneralInfomation
    },
    {
      label: t("Asset.tab_depreciation_information"),
      sectionId: "allocationInfo",
      isShowMessage: props.item?.isErrorDepreciationInformation
    },
    {
      label: t("Asset.tab_asset_source_file"),
      sectionId: "originTools",
      isShowMessage: props.item?.isErrorOriginTools
    },
    {
      label: t("Asset.tab_spare_parts"),
      sectionId: "spareParts",
      isShowMessage: props.item?.isErrorSparePart
    },
    {
      label: t("Asset.tab_attribute"),
      sectionId: "instrumentProperties",
      isShowMessage: props.item?.isErrorInstrumentToolProperties
    },
    {
      label: t("Asset.tab_asset_file"),
      sectionId: "toolProfile",
      isShowMessage: false
    },
  ];

  const handleScroll = () => {
    const dialogContent = document.querySelector('.MuiDialogContent-root');
    const scrollPosition = dialogContent.scrollTop;

    // Lấy các vị trí của các id tương ứng
    const positions = buttons.map((item) =>
      document.getElementById(item.sectionId).offsetTop
    );

    // Kiểm tra vị trí hiện tại và đặt active dựa trên mảng positions
    for (let i = 0; i < positions.length; i++) {
      if (scrollPosition <= 100) {
        setActiveIndex(0);
        break;
      }
      else if (
        scrollPosition >= positions[i] &&
        (i === positions.length - 1 || scrollPosition < positions[i + 1])
      ) {
        setActiveIndex(i + 1);
        break;
      }
    }
  };

  useLayoutEffect(() => {
    const dialogContent = document.querySelector('.MuiDialogContent-root')
    dialogContent.addEventListener("scroll", handleScroll);
    return () => {
      dialogContent.removeEventListener('scroll', handleScroll);
    };
  }, [buttons]);
  return (
    <div className={classes.root}>
      <div className={classes.relative}>
        <Grid container spacing={1} className="" justifyContent="space-between">
          <Grid
            container
            item
            direction="column"
            spacing={1}
            xs={2}
            sm={2}
            md={2}
            className={classes.customAppBar}
          >
            <div className={`buttonAsset ${classes.customAppBarContainer} `}>
              {buttons.map((button, index) => (
                <div className={`w-100 ${classes.relative}`} key={index}>
                  {button?.isShowMessage && <LightTooltip
                    title={t("general.requiredInformation")}
                    placement="right-end"
                    enterDelay={300}
                    leaveDelay={200}
                    className={classes.notiMessage}
                  >
                    <IconButton size="small">
                      <Icon fontSize="small" color="red">
                        report
                      </Icon>
                    </IconButton>
                  </LightTooltip>}
                  <Button
                    key={index}
                    style={{ textAlign: "left" }}
                    className={`flex-start p-10 w-100 my-4 ${classes.customButtonText} ${activeIndex === index ? "activeButton" : ""}`}
                    onClick={() => {
                      scrollFunctions.handleScrollToSection(button.sectionId, index)
                    }}
                  >
                    {button.label}
                  </Button>
                </div>
              ))}
            </div>
          </Grid>

          <Grid item xs={10} sm={10} md={10}>
            <section id='generalInfo'>
              <RecipeReviewCard
                title={t("Asset.GeneralInformation")}
                isListTaiSan={!isTiepNhan}
                isOpen={true}
                className={"mt-0"}
              >
                <GeneralInfomationNew
                  {...props}
                  isView={isView}
                  isDangSuDung={isDangSuDung}
                  item={item}
                  isQRCode={isQRCode}
                  selectUnit={selectUnit}
                  isAddAsset={isAddAsset}
                  onClose={handleClose}
                  selectStores={selectStores}
                  selectStatus={selectStatus}
                  onChange={handleChange}
                  warrantyMonth={warrantyMonth}
                  onSelectSupply={handleSelectSupply}
                  selectShoppingForm={selectShoppingForm}
                  onSetDataSelect={handleSetDataSelect}
                  openSelectSupplyPopup={openSelectSupplyPopup}
                  openSelectProductPopup={openSelectProductPopup}
                  selectMedicalEquipment={selectMedicalEquipment}
                  onSelectUseDepartment={handleSelectUseDepartment}
                  openSelectAssetGroupPopup={openSelectAssetGroupPopup}
                  openSelectDepartmentPopup={openSelectDepartmentPopup}
                  openSelectUseDepartmentPopup={openSelectUseDepartmentPopup}
                  onSelectSupplyPopupClose={handleSelectSupplyPopupClose}
                  onSelectManagementDepartment={handleSelectManagementDepartment}
                  onSelectDepartmentPopupClose={handleSelectDepartmentPopupClose}
                  onSelectUseDepartmentPopupClose={handleSelectUseDepartmentPopupClose}
                  onDateChange={props.handleDateChange}
                  yearPutIntoUse={props.yearPutIntoUse}
                  selectManufacturer={props.selectManufacturer}
                  selectUsingStatus={props.selectUsingStatus}
                  onChangeSelect={props.handleChangeSelect}
                  isDisableQuantity={props.isDisableQuantity}
                  onSaveImageAsset={handleSaveImageAsset}
                  isRoleAccountant={isRoleAccountant}
                  handleMapDataAsset={handleMapDataAsset}
                  isTiepNhan={isTiepNhan}
                  itemAssetDocument={itemAssetDocument}
                  handleSelectAssetGroup={handleSelectAssetGroup}
                  handleChangeSeri={handleChangeSeri}
                  handleKeyUp={handleKeyUp}
                  handleDeleteSeri={handleDeleteSeri}
                  handleSelectMadeIn={handleSelectMadeIn}
                  isEditLocal={isEditLocal}
                  handleSelectUsePerson={handleSelectUsePerson}
                />
              </RecipeReviewCard>
            </section>

            {/* Thông tin khấu hao */}
            <section id='allocationInfo'>
              <RecipeReviewCard
                title={t("Asset.DepreciationInformation")}
                isOpen={true}
              >
                <DepreciationInformation
                  item={item}
                  isView={isView}
                  onChange={handleChange}
                  onSelectSupply={handleSelectSupply}
                  openSelectSupplyPopup={openSelectSupplyPopup}
                  onSelectUseDepartment={handleSelectUseDepartment}
                  openSelectDepartmentPopup={openSelectDepartmentPopup}
                  openSelectUseDepartmentPopup={openSelectUseDepartmentPopup}
                  onSelectSupplyPopupClose={handleSelectSupplyPopupClose}
                  onSelectManagementDepartment={handleSelectManagementDepartment}
                  onSelectDepartmentPopupClose={handleSelectDepartmentPopupClose}
                  onSelectUseDepartmentPopupClose={handleSelectUseDepartmentPopupClose}
                  onDateChange={props.handleDateChange}
                  onChangeFormatNumber={props.handleChangeFormatNumber}
                  isTTHK={isTTHK}
                  itemAssetDocument={itemAssetDocument}
                />
              </RecipeReviewCard>
            </section>

            {/* Nguồn vốn tài sản */}
            <section id='originTools'>
              <RecipeReviewCard
                title={t("Asset.SourceOfAssetCapital")}
                isOpen={true}
              >
                <Grid container>
                  {!isView && (
                    <Grid item md={12} sm={12} xs={12}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={props.openPopupSelectAssetSource}
                        disabled={
                          !props.item.originalCost
                          || +props.item.originalCost === 0
                        }
                      >
                        {t("Asset.add_assetSource")}
                      </Button>
                      {props.item.shouldOpenPopupSelectAssetSource && (
                        <SelectMultiAssetSourcePopup
                          open={props.item.shouldOpenPopupSelectAssetSource}
                          handleSelect={props.handleAddAssetSource}
                          assetSources={props.item.assetSources?.length ? [...props.item.assetSources] : []}
                          handleClose={props.handleSelectAssetSourcePopupClose}
                          handleSetShowToast={props.handleSetShowToast}
                          t={t}
                          i18n={i18n}
                        />
                      )}
                    </Grid>
                  )}

                  <Grid item md={12} sm={12} xs={12} className="mt-16">
                    <CustomMaterialTable
                      data={[...(props.item?.assetSources || [])]}
                      columns={columnsAssetSource}
                      columnActions={isView ? [] : columnsAssetSourceActions}
                      localization={{
                        body: {
                          emptyDataSourceMessage: `${t(
                            "general.emptyDataMessageTable"
                          )}`,
                        },
                      }}
                      options={{
                        draggable: false,
                        toolbar: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        padding: "dense",
                        rowStyle,
                        headerStyle: {
                          backgroundColor: "#358600",
                          color: "#fff",
                        },
                        maxBodyHeight: "343px",
                        minBodyHeight: "343px",
                      }}
                      components={{
                        Toolbar: (props) => (
                          <div style={{ width: "100%" }}>
                            <MTableToolbar {...props} />
                          </div>
                        ),
                      }}
                      onSelectionChange={(rows) => {
                        this.data = rows;
                      }}
                    />
                  </Grid>
                </Grid>
                {/* </TabPanel> */}
              </RecipeReviewCard>
            </section>

            {/* Phụ tùng kèm theo */}
            <section id='spareParts'>
              <RecipeReviewCard
                title={t("Asset.tab_spare_parts")}
                isOpen={true}
              >
                <SpareParts
                  {...props}
                  rowStyle={rowStyle}
                  columnsSparePartActions={isView ? [] : columnsSparePartActions}
                  columnSparePart={columnSparePart}
                />
              </RecipeReviewCard>
            </section>

            {/* Thuộc tính tài sản */}
            <section id='instrumentProperties'>
              <RecipeReviewCard
                title={t("Asset.PropertyProperties")}
                isOpen={true}
              >
                <Grid container>
                  {!isView && (
                    <Grid item md={12} sm={12} xs={12}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={props.openPopupSelectAttribute}
                      >
                        {t("Asset.add_attribute")}
                      </Button>
                    </Grid>
                  )}

                  <Grid item md={12} sm={12} xs={12} className="mt-16">
                    <CustomMaterialTable
                      data={props.item.attributes || []}
                      columns={columnsAttributes}
                      columnActions={isView ? [] : columnsAttributesActions}
                      options={{
                        draggable: false,
                        toolbar: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        rowStyle,
                        maxBodyHeight: "320px",
                        minBodyHeight: "320px",
                        headerStyle: {
                          backgroundColor: "#358600",
                          color: "#fff",
                        },
                        padding: "dense",
                      }}
                      components={{
                        Toolbar: (props) => (
                          <div style={{ witdth: "100%" }}>
                            <MTableToolbar {...props} />
                          </div>
                        ),
                      }}
                      localization={{
                        body: {
                          emptyDataSourceMessage: `${t(
                            "general.emptyDataMessageTable"
                          )}`,
                        },
                      }}
                      onSelectionChange={(rows) => {
                        this.data = rows;
                      }}
                    />
                  </Grid>
                </Grid>
              </RecipeReviewCard>
            </section>

            <section id='toolProfile'>
              <RecipeReviewCard
                title={t("Asset.PropertyRecords")}
                isOpen={true}
              >
                <Grid container>
                  {!isView && (
                    <Grid item md={12} sm={12} xs={12}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={props.handleAddAssetDocumentItem}
                      >
                        {t("AssetFile.addAssetFile")}
                      </Button>
                    </Grid>
                  )}

                  <Grid item md={12} sm={12} xs={12} className="mt-16">
                    <CustomMaterialTable
                      data={props.item.documents || []}
                      columns={columnsAssetFile}
                      columnActions={columnsAssetFileActions}
                      localization={{
                        body: {
                          emptyDataSourceMessage: `${t(
                            "general.emptyDataMessageTable"
                          )}`,
                        },
                      }}
                      options={{
                        draggable: false,
                        toolbar: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        padding: "dense",
                        rowStyle,
                        headerStyle: {
                          backgroundColor: "#358600",
                          color: "#fff",
                        },
                        maxBodyHeight: "343px",
                        minBodyHeight: "343px",
                      }}
                      components={{
                        Toolbar: (props) => (
                          <div style={{ width: "100%" }}>
                            <MTableToolbar {...props} />
                          </div>
                        ),
                      }}
                      onSelectionChange={(rows) => {
                        this.data = rows;
                      }}
                    />
                  </Grid>
                </Grid>
              </RecipeReviewCard>
            </section>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
