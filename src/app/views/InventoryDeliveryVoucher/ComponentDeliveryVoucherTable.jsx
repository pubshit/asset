import React, { useContext } from "react";
import {
  Grid,
  TablePagination,
  Button,
  FormControl,
  Input,
  InputAdornment,
  Collapse,
  Card
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table"; import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

import InventoryDeliveryVoucherDialog from "./InventoryDeliveryVoucherDialog";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { getListWarehouseByDepartmentId } from "../AssetTransfer/AssetTransferService";
import { appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, STATUS_STORE } from "app/appConst";
import { filterOptions } from "app/appFunction";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { searchByPage } from "../Department/DepartmentService";
import { STATUS_DEPARTMENT } from "../../appConst";
import AppContext from "../../appContext";
import { utilizationReportPrintData } from "../FormCustom/Utilization";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import { getTheHighestRole } from "../../appFunction";

export default function ComponentDeliveryVoucherTable(props) {
  let { item, t, i18n, roles } = props;
  const { currentOrg } = useContext(AppContext);
  const { isRoleAssetManager, isRoleAssetUser, departmentUser } = getTheHighestRole();
  const { INVENTORY_DELIVERY, UTILIZATION_REPORT } = LIST_PRINT_FORM_BY_ORG.SUPPLIES_MANAGEMENT;
  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isActive: STATUS_STORE.HOAT_DONG.code,
    departmentId: isRoleAssetManager || isRoleAssetUser
      ? departmentUser?.id
      : null,
  };

  let dataView = utilizationReportPrintData(item?.dataPrint, currentOrg);
  let printUrls = item?.isSinglePrintByOrg
    ? [
      ...UTILIZATION_REPORT.GENERAL,
      ...(UTILIZATION_REPORT[currentOrg?.printCode] || []),
    ] : [
      ...INVENTORY_DELIVERY.GENERAL,
      ...(INVENTORY_DELIVERY[currentOrg?.printCode] || []),
    ];
  let exportExcelFunction = props?.item?.id ? props?.handleExportToExcelPrint : props.handleExportExcelUtilization;
  return (
    <div>
      <Grid container spacing={3} justifyContent="space-between">
        {item.shouldOpenPrintDialog && (
          [LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV_VAN_DINH.code].includes(currentOrg?.code) ? (
            <PrintPreviewTemplateDialog
              t={t}
              handleClose={props.handleDialogClose}
              open={item.shouldOpenPrintDialog}
              item={item.item}
              title={t('Phiếu xuất kho')}
              model={PRINT_TEMPLATE_MODEL.SUPPLIES_MANAGEMENT.DELIVERY}
            />
          ) : (
            <PrintMultipleFormDialog
              t={t}
              i18n={i18n}
              handleClose={props.handleDialogClose}
              open={item.shouldOpenPrintDialog}
              item={dataView}
              title={t('Phiếu xuất kho')}
              urls={printUrls}
              exportExcelFunction={exportExcelFunction}
            />
          )
        )}

        <Grid item md={6} sm={12} xs={12}>
          {!roles.isRoleAssetUser && <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleEditItem}
          >
            {t("InventoryDeliveryVoucher.add")}
          </Button>}
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleOpenAdvanceSearch}
          >
            {t("general.advancedSearch")}
            <ArrowDropDownIcon />
          </Button>
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.exportToExcel}
          >
            {t("general.exportToExcel")}
          </Button>
          <Button
            className="align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handlePrintVoucher}
          >
            {t("general.summaryPrinting")}
          </Button>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box w-100"
              onChange={props.handleTextChange}
              onKeyDown={props.handleKeyDownEnterSearch}
              onKeyUp={props.handleKeyUp}
              placeholder={t("InventoryDeliveryVoucher.enterSearch")}
              id="search_box"
              startAdornment={
                <InputAdornment position="end">
                  <SearchIcon
                    onClick={() => props.search()}
                    className="searchTable"
                  />
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} className="pt-0 pb-0">
          <Collapse in={item.openAdvanceSearch}>
            <Card elevation={0} className="pt-8 pb-12 pl-16 pr-0">
              <ValidatorForm>
                <Grid container xs={12} spacing={2}>
                  <Grid item xs={12} sm={12} md={3}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        fullWidth
                        margin="none"
                        id="mui-pickers-date"
                        label={t("InventoryReceivingVoucher.fromDate")}
                        inputVariant="standard"
                        type="text"
                        autoOk={false}
                        format="dd/MM/yyyy"
                        name={"fromDate"}
                        value={item.fromDate}
                        onChange={(date) =>
                          props.handleSetData(date, "fromDate")
                        }
                        invalidDateMessage={t("general.invalidDateFormat")}
                        maxDateMessage={item?.toDate ? t("general.maxDateFromDate") : t("general.maxDateNow")}
                        minDateMessage={t("general.minDateDefault")}
                        maxDate={item?.toDate ? (new Date(item?.toDate)) : new Date()}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={12} sm={12} md={3}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        fullWidth
                        margin="none"
                        id="mui-pickers-date"
                        label={t("InventoryReceivingVoucher.toDate")}
                        inputVariant="standard"
                        type="text"
                        autoOk={false}
                        format="dd/MM/yyyy"
                        name={"toDate"}
                        value={item.toDate}
                        onChange={(date) =>
                          props.handleSetData(date, "toDate")
                        }
                        invalidDateMessage={t("general.invalidDateFormat")}
                        maxDateMessage={t("general.maxDateNow")}
                        minDateMessage={item?.fromDate ? t("general.minDateToDate") : t("general.minDateDefault")}
                        minDate={item?.fromDate ? (new Date(item?.fromDate)) : undefined}
                        maxDate={new Date()}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={12} sm={12} md={3}>
                    <AsynchronousAutocompleteSub
                      label={t("allocation_asset.receiverDepartment")}
                      searchFunction={searchByPage}
                      searchObject={{
                        pageIndex: 1,
                        pageSize: 100000,
                        isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
                      }}
                      listData={props?.item?.listData}
                      setListData={props?.setListData}
                      typeReturnFunction="default"
                      displayLable="text"
                      value={item.receiverDepartment ? item.receiverDepartment : null}
                      onSelect={(value) => props.handleSetData(value, "receiverDepartment")}
                      filterOptions={(options, params) => filterOptions(options, params)}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={3}>
                    <AsynchronousAutocomplete
                      label={t("InventoryReceivingVoucher.store")}
                      searchFunction={getListWarehouseByDepartmentId}
                      searchObject={searchObjectStore}
                      displayLable={"name"}
                      value={item.store ? item.store : null}
                      onSelect={(value) => props.handleSetData(value, "store")}
                    />
                  </Grid>
                </Grid>
              </ValidatorForm>
            </Card>
          </Collapse>
        </Grid>
        <Grid item xs={12}>
          <div>
            {item.shouldOpenEditorDialog && (
              <InventoryDeliveryVoucherDialog
                t={t}
                i18n={i18n}
                isView={item.isView}
                handleClose={props.handleDialogClose}
                open={item.shouldOpenEditorDialog}
                handleOKEditClose={props.handleOKEditClose}
                item={item.item}
                idDelivery
              />
            )}
            {item.shouldOpenConfirmationDialog && (
              <ConfirmationDialog
                title={t("general.confirm")}
                open={item.shouldOpenConfirmationDialog}
                onConfirmDialogClose={props.handleDialogClose}
                onYesClick={props.handleConfirmationResponse}
                text={t("general.deleteConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}
          </div>
          <MaterialTable
            title={t("general.list")}
            data={item.itemList}
            columns={props.columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              sorting: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: "450px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onRowClick={props.handleRowDataClick}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
            component="div"
            count={item.totalElements}
            rowsPerPage={item.rowsPerPage}
            labelRowsPerPage={t("general.rows_per_page")}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
            }
            page={item.page}
            backIconButtonProps={{ "aria-label": "Previous Page", }}
            nextIconButtonProps={{ "aria-label": "Next Page", }}
            onPageChange={props.handleChangePage}
            onRowsPerPageChange={props.setRowsPerPage}
          />
        </Grid>
        <Grid item xs={12}>
          <MaterialTable
            title={t("general.list")}
            data={item?.voucherDetails}
            columns={props?.suppliesColumns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
              pagination: {
                labelDisplayedRows: '{from}-{to} trong {count}',
                labelRowsSelect: `${t("general.rows_per_page_table")}`,
              }
            }}
            options={{
              sorting: false,
              selection: false,
              pageSize: 5,
              actionsColumnIndex: -1,
              paging: true,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "350px",
              minBodyHeight: "250px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
          />
        </Grid>
      </Grid>
    </div>
  )
}
