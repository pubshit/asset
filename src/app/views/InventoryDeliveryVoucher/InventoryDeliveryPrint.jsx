import React, { Component, useRef } from "react";
import {
  Dialog,
  Button,
  Grid, Checkbox,
  IconButton,
  Icon,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return <div>
    <IconButton onClick={() => props.onSelect(item, 1)}>
      <Icon color="error">delete</Icon>
    </IconButton>
  </div>;
}
export default class InventoryReceivingPrint extends Component {
  state = {
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write(content.innerHTML);
    
    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;

    this.setState({
      ...this.props.item
    }, function () {
    }
    );
  }
  componentDidMount() {
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n, item } = this.props;
    let now = new Date();
    let total = 0;
    return (     
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
        <DialogTitle style={{ cursor: 'move', paddingBottom:'0px'}} id="draggable-dialog-title">
          <span className="">{t('Phiếu xuất kho')}</span>
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{height: '0px', width: '0px', position: 'absolute', print :{ size: 'auto',  margin: '0mm' }}}></iframe>
        
        <ValidatorForm className="validator-form-scroll-dialog"  ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id='divcontents'>
          <Grid>
          <div style={{textAlign: 'center'}}>
              <div style={{display:'flex'}}>
                <div style={{flexGrow: 1, textAlign: 'left'}}>
                  <p style={{fontWeight:"bold"}}>Đơn vị: </p>
                  <p style={{fontWeight:"bold"}}>Bộ phận: </p>
                </div>
                <div style={{flexGrow: 3}}>
                  <p style={{fontWeight:'bold'}}>Mẫu số 02 - VT</p>
                </div>
                <div></div>
              </div>
              
              <div style={{display:'flex',  justifyContent: 'flex-end'}}>
                <div style={{flexGrow: 1, width:'100%'}}></div>
                <div style={{flexGrow: 2,width:'100%' }}>
                  <p style={{fontWeight:'bold'}} >PHIẾU XUẤT KHO</p>
                  <p>Ngày.....tháng.....năm: {now.getFullYear()}</p>
                  <p>Số: </p>
                </div>
                <div style={{flexGrow: 1,width:'100%', textAlign:'left'}} >
                  <p style={{width:'100%', display:'inline-block'}}></p>
                  <p>Nợ:</p>
                  <p>Có:</p>
                </div>
              </div>

              <div style={{ textAlign:'left'}}>
                <p>- Họ và tên người nhận hàng: {this.props.item.receiverPerson?.displayName}</p>
                <p>- Địa chỉ (bộ phận): {this.props.item.receiverDepartment?.name}</p>
                <p>- Lý do xuất kho:</p>
                <p>Xuất tại: {this.props.item.stockReceiptDeliveryStore.name}. Địa điểm: {this.props.item.stockReceiptDeliveryStore.address}</p>
              </div>

              <div>
                <table style={{width:'100%', border:'1px solid',  borderCollapse: 'collapse'}} className="table-report">
                  <tr>
                    <th style={{ border:'1px solid'}} rowSpan="2">STT</th>
                    <th style={{ border:'1px solid', width:''}} rowSpan="2">Tên, nhãn hiệu, quy cách, phẩm chất vật tư, dụng cụ sản phẩm, hàng hoá</th>
                    <th style={{ border:'1px solid',width:''}} rowSpan="2">Mã số</th>
                    <th style={{ border:'1px solid',width:''}} rowSpan="2">Đơn vị tính</th>
                    <th style={{ border:'1px solid',width:'',textAlign: 'center'}} colSpan="2">Số lượng</th>
                    <th style={{ border:'1px solid',width:'',textAlign: 'center'}} rowSpan="2">Đơn giá(VNĐ)</th>     
                    <th style={{ border:'1px solid',width:'',textAlign: 'center'}} rowSpan="2">Thành tiền(VNĐ)</th>
                  </tr>
                  <tr>
                    <th style={{ border:'1px solid',width:''}}>Yêu cầu</th>
                    <th style={{ border:'1px solid',width:''}}>Thực xuất</th>
                  </tr>

                  {item.voucherDetails !== null ? item.voucherDetails.map((row, index) => {
                    let quantity;
                    let unitQuantity = new Number(row.quantity? row.quantity : 0);
                    if (unitQuantity != null) {
                      let plainNumber = unitQuantity.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                      quantity = plainNumber.substr(0, plainNumber.length - 2);
                    }
                    let price;
                    let unitPrice = new Number(row.price? row.price : 0);
                    if (unitPrice != null) {
                      let plainNumber = unitPrice.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                      price = plainNumber.substr(0, plainNumber.length - 2);
                    }
                    let amount;
                    let unitAmount = new Number(row.amount? row.amount : 0);
                    if (unitAmount != null) {
                      let plainNumber = unitAmount.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                      amount = plainNumber.substr(0, plainNumber.length - 2);
                    }
                    total += row.amount;
                    return(
                      <tbody>
                      <tr>
                        <td style={{ border:'1px solid'}}>{index !== null ? index+1 : '' }</td>
                        <td style={{ border:'1px solid'}}>{row.product?.name}</td>
                        <td style={{ border:'1px solid'}}>{row?.batchCode}</td>
                        <td style={{ border:'1px solid'}}>{row.sku?.name}</td>
                        <td style={{ border:'1px solid'}}></td>
                        <td style={{ border:'1px solid', textAlign:'right'}}>{quantity}</td>
                        <td style={{ border:'1px solid', textAlign:'right'}}>{price}</td>
                        <td style={{ border:'1px solid', textAlign:'right'}}>{amount}</td>
                      </tr>
                      </tbody>
                    )
                  }) : ''}

                  <tr>
                    <td style={{ border:'1px solid'}}></td>
                    <td style={{ border:'1px solid', fontWeight:"bold", textAlign:'left'}}>Cộng:</td>
                    <td style={{ border:'1px solid'}}></td>
                    <td style={{ border:'1px solid'}}></td> 
                    <td style={{ border:'1px solid'}}></td>
                    <td style={{ border:'1px solid'}}></td>
                    <td style={{ border:'1px solid'}}></td>
                    <td style={{ border:'1px solid', textAlign:'right'}}>{total}</td>          
                  </tr>
                </table>
              </div>

              <div style={{textAlign:'left'}}>
                <p>- Tổng số tiền (viết bằng chữ): </p>
                <p>- Số chứng từ gốc kèm theo:</p>
              </div>

              <div style={{marginRight:'7%', textAlign:'right'}}><p>Ngày.....tháng.....năm {now.getFullYear()}</p></div>
              <div style={{ display:'flex' , justifyContent: 'space-between', margin:'0 3%'}}>
                <div>
                  <p style={{fontWeight:"bold"}}>Người lập phiếu</p>
                  <p>(Ký, họ tên)</p>
                </div>
                <div>
                  <p style={{fontWeight:"bold"}}>Người nhận hàng</p>
                  <p>(Ký, họ tên)</p>
                </div>
                
                <div>
                  <p style={{fontWeight:"bold"}}>Thủ kho</p>
                  <p>(Ký, họ tên)</p>
                </div>
                <div>
                  <p style={{fontWeight:"bold"}}>Kế toán trưởng</p>
                  <p>(Hoặc bộ phận có nhu cầu nhập)</p>
                  <p>(Ký, họ tên)</p>
                </div>
                <div>
                  <p style={{fontWeight:"bold"}}>Giám đốc</p>
                  <p>(Ký, họ tên)</p>
                </div>
              </div>

            </div>
          </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">              
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>         
        </ValidatorForm>        
      </Dialog>
    );
  }
}

