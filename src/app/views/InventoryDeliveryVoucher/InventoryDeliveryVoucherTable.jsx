import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  getNewCode,
  exportToExcel,
  getVouchersPrint, exportToExcelPrint, exportToExcelPrintUtilization,
} from "./InventoryDeliveryVoucherService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { Helmet } from "react-helmet";
import ConstantList from "../../appConfig";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, LIST_ORGANIZATION } from "app/appConst";
import {
  LightTooltip,
  TabPanel,
  convertNumberPrice,
  isSuccessfulResponse,
  convertFromToDate,
  functionExportToExcel,
  getTheHighestRole, convertMoney
} from "app/appFunction";
import ComponentDeliveryVoucherTable from "./ComponentDeliveryVoucherTable";
import AppContext from "app/appContext";
import { variable } from "../../appConst";
import { isValidDate } from "../../appFunction";
import { getSignature } from "../Component/Signature/services";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const { isRoleAssetUser } = getTheHighestRole();
  const item = props.item;
  const isChoXuLy = appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY.indexOrder
    === item?.status;
  const isDaXuLy = appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY.indexOrder
    === item?.status;

  return (
    <div className="none_wrap">
      {isChoXuLy && !isRoleAssetUser && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isChoXuLy && !isRoleAssetUser && (
        <LightTooltip
          title={t("general.delete")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        className="p-5"
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(
            item,
            appConst.active.view
          )}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InventoryDeliveryVoucherTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenPrintDialog: false,
    tabValue: 0,
    status: [
      appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY.indexOrder,
      appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY.indexOrder,
    ],
    assetClass: appConst.assetClass.VT,
    openAdvanceSearch: false,
    receiverDepartment: null,
    fromDate: null,
    toDate: null,
    store: null,
    listData: [],
    itemList: [],
    checkAll: false,
    isSinglePrintByOrg: false,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Allocation; //Xuất kho

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      this.updatePageData();
    });
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    const {
      selectedList,
    } = this.state;
    try {
      var searchObject = {};
      searchObject.type = this.voucherType;
      searchObject.keyword = this.state?.keyword?.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.assetClass = this.state.assetClass;
      searchObject.receiverDepartmentId = this.state?.receiverDepartment?.id;
      searchObject.storeId = this.state?.store?.id;
      if (this.state.fromDate) {
        searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
      }
      if (this.state.toDate) {
        searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
      }
      if (this.state.status.length > 0) {
        searchObject.statuses = this.state.status;
      } else {
        searchObject.voucherStatus = this.state.status;
      }
      let res = await searchByPage(searchObject);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        let countItemChecked = 0;
        let newArray = data?.content?.map((voucher) => {
          let totalAmount = 0;

          if (voucher.voucherDetails) {
            data?.content?.map((voucher) => {
              let totalAmount = 0;
              voucher.voucherDetails?.map((vd) => {
                totalAmount += vd.amount;
              });
              voucher.totalAmount = totalAmount;
            });
          }
          voucher.totalAmount = totalAmount;
          if (selectedList?.find(checkedItem => checkedItem === voucher.id)) {
            countItemChecked++
          }
          return {
            ...voucher,
            checked: !!selectedList?.find(checkedItem => checkedItem === voucher.id),
          };
        });

        this.setState({
          itemList: [...newArray],
          totalElements: data?.totalElements,
          checkAll: newArray?.length === countItemChecked && newArray?.length > 0
        });
      } else {
        toast.warning(message);
      }
    } catch (error) {
      console.log(error);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabInventoryDeliveryVoucher?.tabAll === newValue) {
      this.setState({
        status: [
          appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY
            .indexOrder,
          appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY
            .indexOrder,
        ],
      }, () => this.updatePageData());
    }
    if (appConst?.tabInventoryDeliveryVoucher?.tabWaiting === newValue) {
      this.setState({
        status: [
          appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY
            .indexOrder
        ],
      }, () => this.updatePageData());
    }
    if (appConst?.tabInventoryDeliveryVoucher?.tabApproved === newValue) {
      this.setState({
        status: [
          appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY
            .indexOrder
        ],
      }, () => this.updatePageData());
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrintDialog: false,
      isSinglePrintByOrg: false,
      item: null,
      isView: null,
      id: null,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      item: null,
      isView: null,
    });
    this.updatePageData();
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { id } = this.state;

    setPageLoading(true);
    try {
      let res = await deleteItem(id);
      const { code, message } = res?.data;

      if (
        (appConst.CODE.SUCCESS === code && isSuccessfulResponse(code)) ||
        (isSuccessfulResponse(res?.status) && !code)
      ) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.success(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleEditItem = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);
    try {
      let res = await getNewCode();
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          item: {
            ...data,
            voucherDetails: data?.voucherDetails ? data?.voucherDetails : [],
            issueDate: data?.issueDate ? data?.issueDate : new Date(),
          },
          shouldOpenEditorDialog: true,
        });
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetTransfer.length; i++) {
      if (
        AssetTransfer[i].checked == null ||
        AssetTransfer[i].checked == false
      ) {
        selectAllItem = false;
      }
      if (AssetTransfer[i].id == item.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };
  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (var i = 0; i < AssetTransfer.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword;
    searchObject.assetClass = this.state.assetClass;
    searchObject.receiverDepartmentId = this.state?.receiverDepartment?.id;
    searchObject.storeId = this.state?.store?.id;
    searchObject.status = this.state.status?.length === 1 ? this.state?.status[0] : null;
    if (this.state.fromDate) {
      searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
    }
    if (this.state.toDate) {
      searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
    }
    if (this.state?.selectedList?.length) {
      searchObject.voucherIds = this.state?.selectedList
    }
    functionExportToExcel(exportToExcel, searchObject, "InventoryDeliveryVoucher.xlsx", setPageLoading);
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY
        .indexOrder:
        return (
          <span className="status status-warning">
            {appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY.name}
          </span>
        );
      case appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY
        .indexOrder:
        return (
          <span className="status status-success">
            {appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY.name}
          </span>
        );
      default:
        break;
    }
  };

  handleGetVoucherDetailsById = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);

    try {
      let res = await getItemById(rowData.id);
      let dataSign = await getSignature(rowData.id);
      const { code, data, message } = res?.data;

      if (
        appConst.CODE.SUCCESS === code ||
        appConst.CODE.SUCCESS === res?.status
      ) {
        let mergeData = {};
        if (isSuccessfulResponse(dataSign?.data?.code)) {
          mergeData = { ...dataSign?.data?.data }
        }
        mergeData = { ...mergeData, ...data }
        this.setState({
          item: {
            ...mergeData,
            handoverPersonName: mergeData?.handoverPerson?.displayName,
            handoverPersonId: mergeData?.handoverPerson?.id,
            receiverPersonId: mergeData?.receiverPerson?.id,
            receiverPersonName: mergeData?.receiverPerson?.displayName,
          }
        });
        return mergeData;
      } else {
        toast.warning(message);
      }
    } catch (err) {
      console.log(err);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleEdit = (rowData) => {
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleView = (rowData) => {
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
        isView: true,
      });
    });
  };

  handlePrint = (rowData) => {
    const { currentOrg } = this.context;

    if (currentOrg?.code === LIST_ORGANIZATION.BVDK_BA_VI.code) {
      return this.setState({
        item: rowData,
        id: rowData?.id,
        shouldOpenPrintDialog: true,
        isView: true,
      })
    }
    this.handleGetVoucherDetailsById(rowData).then((data) => {
      this.setState({
        id: rowData?.id,
        dataPrint: data,
        shouldOpenPrintDialog: true,
        isView: true,
      });
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  }

  handleSetData = (value, name) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: value, selectedList: [] }, () => {
        if (value === null || isValidDate(value)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: value, selectedList: [] }, () => {
      this.search();
    });
  };

  setListData = (value) => {
    this.setState({ listData: value })
  }

  handleChangeSelectTable = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length && itemList?.length > 0,
      });
    }
  };

  handlePrintVoucher = async () => {
    const { t } = this.props;
    let { setPageLoading } = this.context;
    this.setState({
      isSinglePrintByOrg: true,
    })
    setPageLoading(true)
    try {
      let {
        selectedList,
        receiverDepartment,
        store,
        fromDate,
        toDate,
      } = this.state;

      let searchObject = {
        voucherIds: selectedList?.join(",")
      }
      searchObject.receiverDepartmentId = receiverDepartment?.id;
      searchObject.storeId = store?.id;
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }

      const response = await getVouchersPrint(searchObject);
      if (response?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          dataPrint: {
            voucherDetails: response?.data?.data?.items,
            stores: response?.data?.data?.storeNames,
          },
          shouldOpenPrintDialog: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }
  }

  handleRowDataClick = (e, rowData) => {
    this.setState({
      selectedRow: rowData,
      voucherDetails: rowData.voucherDetails
        ? rowData.voucherDetails
        : [],
    });
  };

  handleExportToExcelPrint = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    await functionExportToExcel(
      exportToExcelPrint,
      this.state.id,
      t("exportToExcel.inventoryReceivingPrint"),
      setPageLoading
    );
  }

  handleExportExcelUtilization = async () => {
    const { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let {
        selectedList,
        receiverDepartment,
        store,
        fromDate,
        toDate,
      } = this.state;

      let searchObject = {
        voucherIds: selectedList?.join(",")
      }
      searchObject.receiverDepartmentId = receiverDepartment?.id;
      searchObject.storeId = store?.id;
      searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      searchObject.toDate = convertFromToDate(toDate).toDate;

      await functionExportToExcel(
        exportToExcelPrintUtilization,
        searchObject,
        t("exportToExcel.suppliesUtilization"),
        setPageLoading,
      )
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      checkAll
    } = this.state;
    let TitlePage = t("InventoryDeliveryVoucher.title");

    let roles = getTheHighestRole();

    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleChangeSelectTable(e)}
            />
          </>
        ),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleChangeSelectTable(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        width: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleEdit(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.view) {
                //view
                this.handleView(rowData);
              } else if (method === appConst.active.print) {
                this.handlePrint(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryDeliveryVoucher.stt"),
        field: "",
        width: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("InventoryDeliveryVoucher.issueDate"),
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate
            ? moment(rowData.issueDate).format("DD/MM/YYYY")
            : "",
      },
      {
        title: t("SuggestedSupplies.status"),
        field: "status",
        width: "100px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.status),
      },
      {
        title: t("InventoryDeliveryVoucher.code"),
        field: "voucherCode",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryDeliveryVoucher.store"),
        field: "stockReceiptDeliveryStore.name",
        minWidth: "150px",
        align: "left",
      },
      {
        title: t("InventoryDeliveryVoucher.receiverDepartment"),
        field: "receiverDepartment.name",
        minWidth: "150px",
        align: "left",
      },
      {
        title: t("InventoryDeliveryVoucher.receiverPerson"),
        field: "receiverPerson.displayName",
        minWidth: "150px",
        align: "left",
      },
      {
        title: t("InventoryReceivingVoucher.total"),
        field: "totalAmount",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.totalAmount ? convertNumberPrice(rowData.totalAmount) : "",
      },
    ];


    let suppliesColumns = [
      {
        title: t("InventoryReceivingVoucher.stt"),
        field: "",
        width: "80px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        field: "product.code",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "product.name",
        align: "left",
        minWidth: "180px",
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "sku.name",
        align: "left",
        minWidth: 40,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.purchasePlaning"),
        minWidth: "140px",
        field: "purchasePlaning.name",
        align: "left",
      },
      {
        title: t("Product.quantityOfVoucher"),
        field: "quantityOfVoucher",
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.quantity"),
        field: "quantity",
        minWidth: 80,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.price"),
        field: "price",
        minWidth: "130px",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertMoney(rowData?.price) || 0,
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        minWidth: 140,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.amount"),
        field: "amount",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: 130,
        render: (rowData) => convertNumberPrice(rowData?.amount) || 0,
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {" "}
            {t("InventoryDeliveryVoucher.title")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.materialManagement") },
              {
                name: TitlePage,
                path: "list/inventory_delivery_voucher",
              },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              label={t("InventoryDeliveryVoucher.tab.tabAll")}
            />
            <Tab
              className="tab"
              value={appConst.tabInventoryDeliveryVoucher?.tabWaiting}
              label={
                <div className="tabLable">
                  <span>{t("InventoryDeliveryVoucher.tab.tabWaiting")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.tabInventoryDeliveryVoucher?.tabApproved}
              label={
                <div className="tabLable">
                  <span>{t("InventoryDeliveryVoucher.tab.tabApproveds")}</span>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        {Object.keys(appConst.tabInventoryDeliveryVoucher)?.map((key, index) => (
          <TabPanel
            key={index}
            value={tabValue}
            index={appConst.tabInventoryDeliveryVoucher[key]}
            className="mp-0"
          >
            <ComponentDeliveryVoucherTable
              handlePrintVoucher={this.handlePrintVoucher}
              setListData={this.setListData}
              t={t}
              i18n={i18n}
              item={this.state}
              columns={columns}
              suppliesColumns={suppliesColumns}
              handleRowDataClick={this.handleRowDataClick}
              handleDialogClose={this.handleDialogClose}
              handleEditItem={this.handleEditItem}
              exportToExcel={this.exportToExcel}
              handleTextChange={this.handleTextChange}
              handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
              handleKeyUp={this.handleKeyUp}
              search={this.search}
              handleOKEditClose={this.handleOKEditClose}
              handleChangePage={this.handleChangePage}
              setRowsPerPage={this.setRowsPerPage}
              handleConfirmationResponse={this.handleConfirmationResponse}
              roles={roles}
              handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
              handleSetData={this.handleSetData}
              handleExportToExcelPrint={this.handleExportToExcelPrint}
              handleExportExcelUtilization={this.handleExportExcelUtilization}
            />
          </TabPanel>
        ))}

      </div>
    );
  }
}
InventoryDeliveryVoucherTable.contextType = AppContext;
export default InventoryDeliveryVoucherTable;
