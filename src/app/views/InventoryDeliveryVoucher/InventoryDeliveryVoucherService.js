import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
const API_PATH_VOUCHER =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_API_ENPOINT_ASSET_MAINTANE = ConstantList.API_ENPOINT_ASSET_MAINTANE;
const API_PATH_EXCEL = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_REPORT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report";

export const deleteItem = (id) => {
  return axios.delete(API_PATH_VOUCHER + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH_VOUCHER + "/" + id);
};

export const addNewOrUpdate = (voucher) => {
  if (voucher.id) {
    return axios.put(API_PATH_VOUCHER + "/" + voucher.id, voucher);
  } else {
    return axios.post(API_PATH_VOUCHER, voucher);
  }
};

export const searchByPage = (searchObject) => {
  var url = API_PATH_VOUCHER + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getNewCode = () => {
  let config = {
    params: {
      assetClass: appConst.assetClass.VT,
      voucherType: ConstantList.VOUCHER_TYPE.Allocation,
    },
  }; //
  return axios.get(
    API_PATH_VOUCHER + "/addNew/getNewCodeByVoucherType",
    config
  );
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      API_PATH_API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/nhap-xuat-kho-vat-tu",
    data: searchObject,
    responseType: "blob",
  });
};

export const getVouchersPrint = (params) => {
  let config = {params,};
  return axios.get(
    API_PATH_API_ENPOINT_ASSET_MAINTANE + "/api/report/utilization/supplies",
    config
  );
};

export const exportToExcelPrint = (id) => {
  return axios({
    method: "post",
    url:API_PATH_EXCEL + "/xuat-kho-vat-tu/" + id,
    responseType: "blob",
  });
};

export const exportToExcelPrintUtilization = (params) => {
  return axios({
    method: "post",
    url: API_PATH_REPORT + "/utilization/supplies/export-excel",
    params,
    responseType: "blob",
  });
};
