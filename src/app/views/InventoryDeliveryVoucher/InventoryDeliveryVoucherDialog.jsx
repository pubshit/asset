import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  DialogActions,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  addNewOrUpdate,
  personSearchByPage,
} from "./InventoryDeliveryVoucherService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import SelectMultiProductPopup from "../Component/Product/SelectMultiProductPopup";
import DateFnsUtils from "@date-io/date-fns";
import ConstantList from "../../appConfig";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { STATUS_STORE, appConst, variable, STATUS_DEPARTMENT } from "app/appConst";
import { getAllManagementDepartmentByOrg, getItemById, searchByPage, } from "../Department/DepartmentService";
import { getListRemainingQuantity } from "../Product/ProductService";
import {
  LightTooltip,
  NumberFormatCustom,
  PaperComponent,
  convertNumberPrice,
  formatTimestampToDate,
  getTheHighestRole,
  handleThrowResponseMessage,
  isSuccessfulResponse, formatDateDto,
  convertMoney,
  a11yProps,
  TabPanel,
  getUserInformation,
} from "app/appFunction";
import { getListWarehouseByDepartmentId } from "../AssetTransfer/AssetTransferService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { Label } from "../Component/Utilities";
import viLocale from "date-fns/locale/vi"
import AppContext from "../../appContext";
import { SignatureTabComponentV2 } from "../Component/Signature/SignatureTabComponentV2";
import { createSignature } from "../Component/Signature/services";
import { getItemById as getItemStoreById } from "../Store/StoreService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});


class InventoryDeliveryVoucherDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    handoverPerson: null,
    handoverDepartment: null,
    listDepartment: [],
    title: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    shouldOpenStorePopup: false,
    shouldOpenPersonPopup: false,
    shouldOpenProductPopup: false,
    shouldOpenHandoverStorePopup: false,
    shouldOpenreceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenDepartmentPopup: false,
    textNotificationPopup: "",
    stockReceiptDeliveryStore: null,
    voucherCode: "",
    voucherName: "",
    voucherDetails: [],
    totalElements: 0,
    product: [],
    amount: 0,
    type: ConstantList.VOUCHER_TYPE.Allocation,//Xuất kho = type cấp phát + assetClass = VT
    assetClass: appConst.assetClass.VT,
    storeId: "",
    note: null,
    depRequest: null,
    listStatus: [],
    status: null,
    searchObjectDefault: {
      pageIndex: 1,
      pageSize: 100000,
    },
    tabValue: 0,
    signature: {},
    isViewSignPermission: {
      [variable.listInputName.headOfDepartment]: false,
      [variable.listInputName.deputyDepartment]: false,
      [variable.listInputName.director]: false,
      [variable.listInputName.deputyDirector]: false,
      [variable.listInputName.chiefAccountant]: false,
      [variable.listInputName.storeKeeper]: false,
      [variable.listInputName.createdPerson]: true,
      [variable.listInputName.handoverPerson]: true,
      [variable.listInputName.receiverPerson]: true,
    }
  };
  voucherType = ConstantList.VOUCHER_TYPE.Allocation; //Xuất kho = type cấp phát + assetClass = VT 

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  convertDataState = (state) => {
    return {
      id: state.id,
      issueDate: state?.issueDate
        ? formatDateDto(state.issueDate)
        : null,
      assetClass: appConst.assetClass.VT,
      handoverDepartment: {
        id: state?.handoverDepartment?.id,
      },
      handoverPerson: state?.handoverPerson,
      receiverDepartment: {
        id: state?.receiverDepartment?.id,
      },
      receiverPerson: state?.receiverPerson,
      status: state?.status?.code,
      type: state?.type,
      stockReceiptDeliveryStore: state?.stockReceiptDeliveryStore,
      voucherDetails: state?.voucherDetails?.map(item => ({
        ...item,
        // receiptDate: item.receiptDate
        //   ? formatDateDto(item.receiptDate)
        //   : null,
        // expiryDate: item.expiryDate
        //   ? formatDateDto(item.expiryDate)
        //   : null,
      })),
      voucherCode: state?.voucherCode,
      note: state?.note,
      reason: state?.reason,
    }
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { id, voucherDetails } = this.state;
    let { t, handleConfirm = () => { } } = this.props;

    try {
      setPageLoading(true);
      let dataState = this.convertDataState(this.state);
      if (voucherDetails?.length <= 0) {
        toast.warning(t("InventoryDeliveryVoucher.check_product_in_voucher"));
        return;
      }
      const res = await addNewOrUpdate(dataState);
      const { code, data, message } = res?.data;
      if (code === appConst.CODE.SUCCESS && data) {
        id
          ? toast.success(t("general.updateSuccess"))
          : toast.success(t("general.addSuccess"));
        this.handleSaveSign(data?.id);
        this.props.handleOKEditClose();
        handleConfirm(data);
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  convertDataSign = (data = {}) => {
    return {
      directorId: data?.directorId,
      directorName: data?.directorName,
      deputyDirectorId: data?.deputyDirectorId,
      deputyDirectorName: data?.deputyDirectorName,
      chiefAccountantId: data?.chiefAccountantId,
      chiefAccountantName: data?.chiefAccountantName,
      headOfDepartmentId: data?.headOfDepartmentId,
      headOfDepartmentName: data?.headOfDepartmentName,
      deputyDepartmentId: data?.deputyDepartmentId,
      deputyDepartmentName: data?.deputyDepartmentName,
      storekeeperId: data?.storekeeperId,
      storekeeperName: data?.storekeeperName,
      handoverPersonId: data?.handoverPersonId,
      handoverPersonName: data?.handoverPersonName,
      receiverPersonId: data?.receiverPersonId,
      receiverPersonName: data?.receiverPersonName,
      voucherId: data?.voucherId,
    }
  }

  handleSaveSign = async (voucherId) => {
    try {
      const payload = this.convertDataSign({
        ...this.state.signature,
        voucherId: voucherId
      });
      const data = await createSignature(payload, voucherId);
    } catch (error) {

    }
  }

  handleProductPopupClose = () => {
    this.setState({
      shouldOpenProductPopup: false,
    });
  };

  handleSelectMultiProduct = (items) => {
    let { voucherDetails = [] } = this.state;
    let { isExportAsset = false, idDelivery = false } = this.props;
    if (items != null && items.length > 0) {
      if (isExportAsset || idDelivery) {
        let updateList = items?.map(item => {
          let idMatchItem = voucherDetails?.find(i => (i?.productId || i?.product?.id) === item?.id);
          return {
            batchCode: item?.lotNumber,
            sku: item?.sku,
            remainingQuantity: item?.remainingQuantity,
            product: item,
            price: item?.unitPrice,
            quantity: idMatchItem?.quantity || 0,
            amount: item?.unitPrice * (idMatchItem?.quantity || 0),
            receiptDate: item?.inputDate,
            expiryDate: item?.expiryDate,
            maxQuantity: idMatchItem?.maxQuantity || item?.remainingQuantity
          }
        })
        this.setState({ voucherDetails: updateList }, () => {
          this.handleProductPopupClose();
        });
      } else {
        items.forEach((item, x) => {
          let checkExisted = voucherDetails.find((el) =>
            (el?.product?.id === item?.id || el?.productId === item?.id)
            && (el?.product?.inputDate === item?.inputDate || el?.receiptDate === item?.inputDate)
            && (el?.product?.lotNumber === item?.lotNumber || el?.batchCode === item?.lotNumber)
            && (el?.product?.unitPrice === item?.unitPrice || el?.amount === item?.unitPrice)
            && (el?.product?.code === item?.code || el?.productCode === item?.code)
            && (el?.product?.expiryDate === item?.expiryDate || el?.expiryDate === item?.expiryDate)
          )
          if (!checkExisted) {
            let vc = {};
            vc.batchCode = item?.lotNumber;
            vc.sku = item?.sku;
            vc.remainingQuantity = item?.remainingQuantity;
            vc.product = item;
            vc.price = item?.unitPrice;
            vc.maxQuantity = item?.remainingQuantity;
            vc.quantity = 0;
            vc.amount = 0;
            vc.receiptDate = item?.inputDate;
            vc.expiryDate = item?.expiryDate;
            voucherDetails.push(vc);
          }
        });
        this.setState({ voucherDetails: voucherDetails }, function () {
          this.handleProductPopupClose();
        });
      }
    }

  };

  handleSelectPerson = (item) => {
    this.setState({ handoverPerson: item });
  };

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    });
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({ receiverPerson: item });
  };

  handlereceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenreceiverPersonPopup: false,
    });
  };

  getDetailDepartment = async (departmentId) => {
    if (!departmentId) return {};
    try {
      const data = await getItemById(departmentId);
      if (isSuccessfulResponse(data?.status)) {
        return {
          headOfDepartmentId: data?.data?.headOfDepartmentId,
          headOfDepartmentName: data?.data?.headOfDepartmentName,
          deputyDepartmentId: data?.data?.deputyDepartmentId,
          deputyDepartmentName: data?.data?.deputyDepartmentName,
        }
      } else {
        return {};
      }
    } catch (error) {
      return {};
    }
  }

  onMountSetSign = async (item) => {
    const { currentUser, organization } = getUserInformation();
    const itemDepartment = await this.getDetailDepartment(item?.receiverDepartmentId)
    this.setState({
      signature: {
        directorId: item?.directorId || organization?.directorId,
        directorName: item?.directorName || organization?.directorName,
        deputyDirectorId: item?.deputyDirectorId || organization?.deputyDirectorId,
        deputyDirectorName: item?.deputyDirectorName || organization?.deputyDirectorName,
        chiefAccountantId: item?.chiefAccountantId || organization?.chiefAccountantId,
        chiefAccountantName: item?.chiefAccountantName || organization?.chiefAccountantName,
        headOfDepartmentId: item?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: item?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: item?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: item?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
        storekeeperId: item?.storekeeperId,
        storekeeperName: item?.storekeeperName,
        handoverPersonId: item?.handoverPersonId,
        handoverPersonName: item?.handoverPersonName,
        receiverPersonId: item?.receiverPersonId,
        receiverPersonName: item?.receiverPersonName,
        createdPersonId: item?.createdPersonId || currentUser?.person?.id,
        createdPersonName: item?.createdPersonName || currentUser?.person?.displayName,
      }
    })
  }

  componentDidMount() {
    let { item, isExportAsset } = this.props;
    const roles = getTheHighestRole();
    this.setState({
      ...item,
      ...roles,
    }, () => {
      let { id } = this.state;
      this.onMountSetSign(item);
      if (id) {
        this.setState({
          status: appConst.listStatusInventoryReceivingVoucher.find(
            (i) => i.code === item.status
          ),
        }, this.handleGetSuppliesRemainQuantity);
      } else {
        this.setState({
          status: isExportAsset ? appConst.listStatusInventoryReceivingVoucherObject.DA_XU_LY : appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY,
          handoverDepartment: roles?.isRoleAssetManager
            ? roles?.departmentUser
            : item?.handoverDepartment || null,
        });
      }
    });
  }

  handleStorePopupClose = () => {
    this.setState({
      shouldOpenStorePopup: false,
    });
  };

  handleQuantityChange = (rowData, event) => {
    let { voucherDetails } = this.state;

    voucherDetails?.map((element) => {
      if (element?.tableData?.id === rowData?.tableData?.id) {
        element.quantity = event.target.value;
        element.amount = Number(event.target.value || 0) * element.price;
      }
    });
    this.setState({ voucherDetails });
  };

  handleConfirmationResponse = () => {
    this.setState(
      { shouldOpenNotificationPopup: false, textNotificationPopup: "" },
      function () { }
    );
  };

  handleRowDeleteProduct = (rowData) => {
    let { voucherDetails } = this.state;
    voucherDetails = voucherDetails?.filter(x => x.tableData?.id !== rowData?.tableData?.id);
    this.setState({ voucherDetails });
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleSetData = (value, name) => {
    if (variable.listInputName.handoverDepartment === name) {
      this.setState({ handoverPerson: null });
    } else if (variable.listInputName.receiverDepartment === name) {
      this.setState({ receiverPerson: null, stockReceiptDeliveryStore: null });
    }
    this.setState({ [name]: value }, () => {
      this.onCheckInputChange(name, value);
    });
  };

  onCheckInputChange = (name, value) => {
    if (name === "handoverPerson") {
      this.setState({
        signature: {
          ...this.state.signature,
          handoverPersonId: value?.id,
          handoverPersonName: value?.displayName,
        }
      })
    }
    if (name === "receiverPerson") {
      this.setState({
        signature: {
          ...this.state.signature,
          receiverPersonId: value?.id,
          receiverPersonName: value?.displayName,
        }
      })
    }
  }

  handleChangeStatus = (value) => {
    this.setState({ status: value });
  };

  handleChangeHandoverDepartment = (handoverDepartment) => {
    this.setState({
      handoverDepartment,
      listHandoverPerson: [],
      handoverPerson: null,
      listStore: [],
      stockReceiptDeliveryStore: null,
      voucherDetails: [],
    });
  };

  handleChangeReceiverDepartment = async (receiverDepartment) => {
    this.setState({
      receiverDepartment,
      listReceiverPerson: [],
      receiverPerson: null,
    });
    const itemDepartment = await this.getDetailDepartment(receiverDepartment?.id)
    this.setState({
      signature: {
        ...this.state.signature,
        headOfDepartmentId: this.state.signature?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: this.state.signature?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: this.state.signature?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: this.state.signature?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
      }
    })
  };

  getDetailStore = async (storeId) => {
    if (!storeId) return {};
    try {
      const data = await getItemStoreById(storeId);
      if (isSuccessfulResponse(data?.status)) {
        return {
          storekeeperId: data?.data?.storekeeperId,
          storekeeperName: data?.data?.storekeeperName,
        }
      } else {
        return {};
      }
    } catch (error) {
      return {};
    }
  }

  handleSelectStockReceiptDeliveryStore = async (stockReceiptDeliveryStore) => {
    let { isExportAsset } = this.props;
    let { voucherDetails } = this.state;

    const itemDepartment = await this.getDetailStore(stockReceiptDeliveryStore?.id)

    this.setState({
      stockReceiptDeliveryStore,
      voucherDetails: isExportAsset ? voucherDetails : [],
      signature: {
        ...this.state.signature,
        storekeeperId: this.state.signature?.storekeeperId || itemDepartment?.storekeeperId,
        storekeeperName: this.state.signature?.storekeeperName || itemDepartment?.storekeeperName,
      }
    }, () => {
      this.handleGetSuppliesRemainQuantity();
    });
  };

  handleGetSuppliesRemainQuantity = async () => {
    let { voucherDetails = [], stockReceiptDeliveryStore } = this.state;
    let { t, isExportAsset = false } = this.props;
    let searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 100000;
    searchObject.storeId = stockReceiptDeliveryStore?.id;
    searchObject.productIds = voucherDetails?.map(item => item.product?.id).join(',');
    if (voucherDetails?.length === 0) return; // suppliesItems rỗng thì k cần gọi api

    try {
      let res = await getListRemainingQuantity(searchObject)
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        voucherDetails?.map(x => {
          let existItem = data?.find(
            item => item?.productId === x?.product?.id
              && formatTimestampToDate(item?.inputDate) === formatTimestampToDate(x?.receiptDate)
              && item?.lotNumber === x?.batchCode
              && item?.unitPrice === x?.price
              && formatTimestampToDate(item?.expiryDate) === formatTimestampToDate(x?.expiryDate)
          )
          if (existItem) {
            x.product.remainingQuantity = existItem.remainQuantity > 0
              ? existItem.remainQuantity
              : 0;
          }
          if (isExportAsset) {
            let i = data?.find(item => item?.productId === x?.product?.id)
            if (i) {
              x.product.remainingQuantity = i.remainQuantity > 0
                ? i.remainQuantity
                : 0;
            }
          }
          return x
        });
        this.setState({
          voucherDetails,
        });
      }
      else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      console.log(e);
      toast.error(t("InventoryDeliveryVoucher.getRemainQuantityError"));
    }
  }

  handleChangeTabValue = (event, newValue) => {
    this.setState({ tabValue: newValue })
  };

  handleChangeSignature = (data = []) => {
    this.setState({
      signature: data,
    })
  }

  render() {
    let { open, t, i18n, isView, isExportAsset = false } = this.props;
    let {
      id,
      receiverDepartment,
      handoverDepartment,
      issueDate,
      rowsPerPage,
      page,
      shouldOpenProductPopup,
      voucherCode,
      product,
      receiverPerson,
      handoverPerson,
      stockReceiptDeliveryStore,
      voucherDetails,
      shouldOpenNotificationPopup,
      textNotificationPopup,
      storeId,
      note,
      reason,
      status,
      listHandoverDepartment,
      listReceiverDepartment,
      listReceiverPerson,
      listStore,
      isRoleAssetManager,
      searchObjectDefault,
    } = this.state;
    let searchObjectHandoverPerson = {
      ...searchObjectDefault,
      departmentId: handoverDepartment?.id,
    };
    let searchObjectReceiverDepartment = {
      ...searchObjectDefault,
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    };
    let searchObjectReceiverPerson = {
      ...searchObjectDefault,
      departmentId: receiverDepartment?.id,
    };
    let searchObjectStore = {
      ...searchObjectDefault,
      isActive: STATUS_STORE.HOAT_DONG.code,
      departmentId: handoverDepartment?.id,
    };
    const listStatus = [
      appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.CHO_XU_LY,
      appConst.STATUS_INVENTORY_DELIVERY_VOUCHER.DA_XU_LY,
    ]

    let columns = [
      {
        title: t("general.action"),
        field: "valueText",
        minWidth: "80px",
        hidden: isView,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <LightTooltip
            title={t("general.delete")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => this.handleRowDeleteProduct(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("InventoryDeliveryVoucher.stt"),
        field: "",
        minWidth: "50px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        field: "product.code",
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.inputDate"),
        field: "receiptDate",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatTimestampToDate(rowData?.receiptDate),
      },
      {
        title: t("Product.name"),
        field: "product.name",
        align: "left",
        minWidth: "170px",
      },
      {
        title: t("Product.stockKeepingUnit"),
        minWidth: "70px",
        field: "sku.name",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryDeliveryVoucher.quantity"),
        field: "product.quantity",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            type="text"
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            name="quantity"
            id="formatted-numberformat-originalCost"
            value={rowData.quantity || ""}
            validators={[
              "required",
              "minFloat:0.000001",
              `maxFloat:${rowData?.maxQuantity ? (rowData?.maxQuantity).toFixed(2) : 999999999}`,
              "matchRegexp:^\\d+(\\.\\d{1,2})?$"
            ]}
            errorMessages={[t("general.required"), t("general.minNumberError"), "Tổng không được lớn hơn số lượng đề xuất", t("general.quantityError")]}
            InputProps={{
              inputComponent: NumberFormatCustom,
              inputProps: {
                style: {
                  textAlign: "center",
                },
                decimalScale: 2,
                allowNegative: false,
              },

              readOnly: isView
            }}
          />

        ),
      },
      {
        title: t("InventoryDeliveryVoucher.remainingQuantity"),
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => convertMoney(rowData?.product?.remainingQuantity),
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        align: "left",
        minWidth: "100px",
      },
      {
        title: t("Product.expiryDate"),
        field: "expiryDate",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.expiryDate
          ? formatTimestampToDate(rowData.expiryDate)
          : "",
      },
      {
        title: t("Product.price"),
        field: "price",
        align: "left",
        minWidth: "130px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.price ? convertNumberPrice(rowData.price) : 0,
      },
      {
        title: t("Product.amount"),
        field: "amount",
        minWidth: "130px",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.amount ? convertNumberPrice(rowData.amount) : 0,
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("InventoryDeliveryVoucher.dialog")}</span>
          </DialogTitle>
          <DialogContent style={{ minHeight: "450px" }}>
            <AppBar position="static" color="default">
              <Tabs
                value={this.state?.tabValue}
                onChange={this.handleChangeTabValue}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab
                  label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.NAME)}
                  {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE)}
                  value={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE}
                />
                {/*<Tab*/}
                {/*  label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.NAME)}*/}
                {/*  {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE)}*/}
                {/*  value={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE}*/}
                {/*/>*/}
              </Tabs>
            </AppBar>
            <TabPanel
              value={this.state?.tabValue}
              index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE}
            >
              <Grid container spacing={1}>
                <Grid container spacing={1}>
                  <Grid item md={3} sm={12} xs={12} className="mt-12">
                    <label>
                      <span className="font-weight-blod text-center">
                        {t("InventoryDeliveryVoucher.codeLetter")}:
                      </span>{" "}
                      {voucherCode}
                    </label>
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        fullWidth
                        margin="none"
                        id="mui-pickers-date"
                        label={
                          <span>
                            <span className="colorRed"> * </span>
                            {t("SuggestedSupplies.issueDate")}
                          </span>
                        }
                        inputVariant="standard"
                        type="text"
                        autoOk={false}
                        format="dd/MM/yyyy"
                        name={"issueDate"}
                        value={issueDate}
                        onChange={(date) =>
                          this.handleSetData(date, "issueDate")
                        }
                        readOnly={isView}
                        InputProps={{
                          readOnly: isView,
                        }}
                        validators={["required"]}
                        errorMessages={[t("general.required")]}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        maxDate={new Date()}
                        maxDateMessage={t("allocation_asset.maxDateMessage")}
                        minDateMessage={t("general.minDateDefault")}
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("InventoryReceivingVoucher.status")}
                        </span>
                      }
                      searchFunction={() => { }}
                      searchObject={{}}
                      listData={listStatus}
                      displayLable="name"
                      readOnly={isView || isExportAsset}
                      value={status || null}
                      onSelect={this.handleChangeStatus}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <Label isRequired>
                          {t("InventoryDeliveryVoucher.department")}
                        </Label>
                      }
                      searchFunction={getAllManagementDepartmentByOrg}
                      searchObject={{}}
                      listData={listHandoverDepartment}
                      setListData={(value) => this.handleSetData(value, "listHandoverDepartment")}
                      displayLable="text"
                      isNoRenderChildren
                      isNoRenderParent
                      readOnly={isView || isExportAsset}
                      typeReturnFunction="list"
                      value={handoverDepartment || null}
                      onSelect={this.handleChangeHandoverDepartment}
                      disabled={isRoleAssetManager && !isView}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <Label isRequired>
                          {t("InventoryDeliveryVoucher.handoverPerson")}
                        </Label>
                      }
                      searchFunction={personSearchByPage}
                      searchObject={searchObjectHandoverPerson}
                      displayLable="displayName"
                      name="handoverPerson"
                      readOnly={isView}
                      value={handoverPerson || null}
                      onSelect={this.handleSetData}
                      disabled={!handoverDepartment?.id}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </Grid>
                  <Grid item md={3} sm={6} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("InventoryDeliveryVoucher.store")}
                        </span>
                      }
                      readOnly={isView}
                      searchFunction={getListWarehouseByDepartmentId}
                      listData={listStore}
                      setListData={(value) => this.handleSetData(value, "listStore")}
                      searchObject={searchObjectStore}
                      displayLable={"name"}
                      name="stockReceiptDeliveryStore"
                      value={stockReceiptDeliveryStore || null}
                      onSelect={this.handleSelectStockReceiptDeliveryStore}
                      disabled={!handoverDepartment?.id}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <Label isRequired>
                          {t("InventoryDeliveryVoucher.receiverDepartment")}
                        </Label>
                      }
                      readOnly={isView || isExportAsset}
                      listData={listReceiverDepartment}
                      setListData={(value) => this.handleSetData(value, "listReceiverDepartment")}
                      searchFunction={searchByPage}
                      searchObject={searchObjectReceiverDepartment}
                      typeReturnFunction="default"
                      displayLable="text"
                      value={receiverDepartment || null}
                      onSelect={this.handleChangeReceiverDepartment}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </Grid>
                  <Grid item md={3} sm={12} xs={12}>
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          {t("InventoryDeliveryVoucher.receiverPerson")}
                        </span>
                      }
                      readOnly={isView}
                      searchFunction={personSearchByPage}
                      searchObject={searchObjectReceiverPerson}
                      listData={listReceiverPerson}
                      setListData={(value) => this.handleSetData(value, "listReceiverPerson")}
                      displayLable={"displayName"}
                      value={receiverPerson ? receiverPerson : null}
                      onSelect={(value) => this.handleSetData(value, "receiverPerson")}
                      disabled={!receiverDepartment}
                    />

                  </Grid>
                  <Grid item md={12} sm={12} xs={12}>
                    <TextValidator
                      onChange={this.handleChange}
                      type="text"
                      name="note"
                      label={
                        <span>
                          <span className="colorRed"></span>
                          {t("ReceivingAsset.note")}
                        </span>
                      }
                      InputProps={{
                        readOnly: isView,
                      }}
                      className="w-100"
                      value={note}
                    />
                  </Grid>
                  <Grid item md={12} sm={12} xs={12}>
                    <TextValidator
                      onChange={this.handleChange}
                      type="text"
                      name="reason"
                      label={
                        <span>
                          <span className="colorRed"></span>
                          {t("asset_liquidate.reason")}
                        </span>
                      }
                      InputProps={{
                        readOnly: isView,
                      }}
                      className="w-100"
                      value={reason}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={1}>
                  <Grid item md={3} sm={6} xs={12}>
                    {this.state.stockReceiptDeliveryStore && (
                      <Button
                        size="small"
                        className=" mt-16"
                        variant="contained"
                        color="primary"
                        disabled={isView}
                        onClick={() =>
                          this.setState({
                            shouldOpenProductPopup: true,
                            item: {},
                          })
                        }
                      >
                        {t("component.product.title")}
                      </Button>
                    )}
                    {shouldOpenProductPopup && (
                      <SelectMultiProductPopup
                        open={shouldOpenProductPopup}
                        handleSelect={this.handleSelectMultiProduct}
                        product={product != null ? product : []}
                        assetVouchers={voucherDetails ? voucherDetails : []}
                        handleClose={this.handleProductPopupClose}
                        t={t}
                        i18n={i18n}
                        voucherType={ConstantList.VOUCHER_TYPE.StockOut}
                        voucherId={id ? id : null}
                        selectedItem={{ product: null }}
                        storeId={
                          this.state.stockReceiptDeliveryStore?.id
                            ? this.state.stockReceiptDeliveryStore?.id
                            : storeId
                        }
                        isExportAsset={this?.props?.isExportAsset}
                        date={issueDate}
                      />
                    )}
                  </Grid>
                </Grid>
                <Grid container spacing={2} className="mt-12">
                  <Grid item xs={12}>
                    <MaterialTable
                      data={voucherDetails ? voucherDetails : []}
                      columns={columns}
                      options={{
                        sorting: false,
                        toolbar: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        rowStyle: (rowData) => ({
                          backgroundColor:
                            rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "253px",
                        minBodyHeight: "253px",
                        headerStyle: {
                          backgroundColor: "#358600",
                          color: "#fff",
                        },
                        padding: "dense",
                      }}
                      localization={{
                        body: {
                          emptyDataSourceMessage: `${t(
                            "general.emptyDataMessageTable"
                          )}`,
                        },
                      }}
                      components={{
                        Toolbar: (props) => (
                          <div>
                            <MTableToolbar {...props} />
                          </div>
                        ),
                      }}
                      onSelectionChange={(rows) => {
                        this.data = rows;
                      }}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel
              value={this.state?.tabValue}
              index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE}>
              <SignatureTabComponentV2
                isExport={true}
                data={{ ...this.state?.signature }}
                permission={this.state?.isViewSignPermission}
                handleChange={this.handleChangeSignature}
              />
            </TabPanel>
          </DialogContent>
          <DialogActions className="zIndex-9999 position-sticky bottom-0 right-0">
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!isView && (
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  className="mr-12"
                >
                  {t("general.save")}
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}
      </Dialog>
    );
  }
}

export default InventoryDeliveryVoucherDialog;
InventoryDeliveryVoucherDialog.contextType = AppContext;
