import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const InventoryDeliveryVoucherTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./InventoryDeliveryVoucherTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(InventoryDeliveryVoucherTable);

const InventoryDeliveryVoucherRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/inventory_delivery_voucher",
    exact: true,
    component: ViewComponent
  }
];

export default InventoryDeliveryVoucherRoutes;