import React from "react";
import { IconButton, Button, Icon, Grid, Checkbox, FormControlLabel } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useTranslation } from "react-i18next";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import MaterialTable, { MTableToolbar } from "material-table";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import { TabPanel, filterOptions } from "app/appFunction";
import { getOptionSelected, handleKeyDownFloatOnly } from "../../appFunction";
import ValidatedDatePicker from "../Component/ValidatePicker/ValidatePicker";
import { Autocomplete } from "@material-ui/lab";
import { Label } from "../Component/Utilities";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;

  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

export default function ShoppingPlanScrollableTabsButtonForce(props) {
  const { t, i18n, item } = props;

  let {
    query = {},
    listProducts = [],
    isView = false,
  } = item;

  let columns = [
    {
      title: t("general.action"),
      field: "custom",
      align: "center",
      hidden: isView,
      minWidth: "60px",
      maxWidth: "60px",
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.delete) {
              props.handleRemoveProductInList(rowData?.tableData?.id)
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    {
      title: t("general.index"),
      field: " ",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("Product.code"),
      field: "code",
      align: "center",
      maxWidth: "100px",
      minWidth: "100px",
      render: rowData => rowData?.product?.code
    },
    {
      title: t("Product.name"),
      field: "product",
      align: "left",
      maxWidth: 80,
      minWidth: "250px",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => {
        return (
          <Autocomplete
            id="combo-box"
            fullWidth
            size="small"
            name='product'
            options={listProducts || []}
            value={rowData?.product || null}
            onChange={(e, value) => props.handleChangeDataSubTable(value, "product", rowData?.tableData?.id)}
            getOptionLabel={(option) => option.name}
            getOptionSelected={getOptionSelected}
            filterOptions={(options, params) => filterOptions(
              options,
              params,
              true,
              "name"
            )}
            disabled={isView}
            noOptionsText={t("general.noOption")}
            renderInput={(params) => (
              <TextValidator
                {...params}
                variant="standard"
                name='product'
                value={rowData?.product?.name}
                onFocus={props.handleChangeInputFocus}
                onChange={(e) => {
                  let event = {
                    target: {
                      name: e.target.name,
                      value: e.target.value,
                    }
                  }
                  props?.onInputChangeDataSearch(event, props.handleChangeInputSearch);
                }}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />
            )}
          />
        )
      }
    },
    {
      title: t("ShoppingPlan.quantity"),
      field: "quantity",
      align: "left",
      maxWidth: 80,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            className="w-100"
            onChange={(event) =>
              props.handleChangeDataSubTable(event.target.value, "quantity", rowData?.tableData?.id)
            }
            onKeyDown={handleKeyDownFloatOnly}
            type="number"
            InputProps={{
              readOnly: isView,
              inputProps: {
                className: "text-center",
              },
            }}
            name="quantity"
            validators={["minFloat:0.000001", "required", `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
            errorMessages={[t("Số lượng lớn hơn 0"), t("general.required"), t("general.quantityError")]}
            value={rowData.quantity || ""}
          />
        );
      },
    },
    {
      title: t("ShoppingPlan.totalQuantity"),
      field: "requiredQty",
      align: "left",
      maxWidth: 80,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        let remainQty = (rowData?.requiredQty || 0);

        remainQty = remainQty % 1 === 0
          ? remainQty?.toString()
          : parseFloat(remainQty?.toFixed(2));

        return remainQty;
      }
    },
    {
      title: t("ShoppingPlan.aggregateQuantity"),
      field: "approvedQty",
      align: "left",
      maxWidth: 80,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.approvedQty || 0
    },
    {
      title: t("ShoppingPlan.remainQuantity"),
      field: "remainQuantity",
      align: "left",
      maxWidth: 80,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        let remainQty = (rowData?.quantity || 0) - (rowData?.approvedQty || 0);

        remainQty = remainQty % 1 === 0
          ? remainQty?.toString()
          : parseFloat(remainQty?.toFixed(2));

        return remainQty;
      }
    },
  ];
  return (
    <>
      <AppBar position="static" color="default">
        <Tabs
          value={item.tabValue}
          onChange={(e, index) => props?.handleChangeTab(index)}
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={appConst.TAB_PURCHASE_DIALOG.INFO.name} {...a11yProps(0)} />
        </Tabs>
      </AppBar>
      <TabPanel value={item.tabValue} index={appConst.TAB_PURCHASE_DIALOG.INFO.code}>
        <Grid container spacing={1}>
          <Grid item md={4} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              label={t("Asset.managementCode")}
              onChange={(event) => props.handleChange(event.target.value, "code")}
              type="text"
              InputProps={{
                readOnly: true
              }}
              name="code"
              value={props.item.code || ""}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("ShoppingPlan.code")}
                </span>
              }
              onChange={(event) => props.handleChange(event.target.value, "managementCode")}
              type="text"
              InputProps={{
                readOnly: isView
              }}
              name="managementCode"
              value={props.item.managementCode || ""}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              label={<Label isRequired>{t("ShoppingPlan.name")}</Label>}
              onChange={(event) => props.handleChange(event.target.value, "name")}
              type="text"
              InputProps={{
                readOnly: isView
              }}
              name="name"
              value={props.item.name || ""}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <ValidatedDatePicker
              fullWidth
              margin="none"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("ShoppingPlan.dataApplyFrom")}
                </span>
              }
              inputVariant="standard"
              type="text"
              autoOk
              format="dd/MM/yyyy"
              disabled={isView}
              name={"applicationPeriodFrom"}
              value={props.item.applicationPeriodFrom}
              maxDate={props.item.applicationPeriodTo ? new Date(props.item.applicationPeriodTo) : undefined}
              minDateMessage={t("general.minDateDefault")}
              maxDateMessage={props.item.applicationPeriodTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
              invalidDateMessage={t("general.invalidDateFormat")}
              onChange={(date) => props.handleChange(date, "applicationPeriodFrom")}
              clearable
              clearLabel={t("general.remove")}
              cancelLabel={t("general.cancel")}
              okLabel={t("general.select")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <ValidatedDatePicker
              fullWidth
              margin="none"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("ShoppingPlan.dataApplyTo")}
                </span>
              }
              inputVariant="standard"
              type="text"
              autoOk
              format="dd/MM/yyyy"
              disabled={isView}
              name={"applicationPeriodTo"}
              value={props.item.applicationPeriodTo}
              minDate={props.item.applicationPeriodFrom ? new Date(props.item.applicationPeriodFrom) : undefined}
              minDateMessage={props.item.applicationPeriodFrom ? t("general.minDateToDate") : t("general.minDateDefault")}
              maxDateMessage={t("general.maxDateDefault")}
              invalidDateMessage={t("general.invalidDateFormat")}
              onChange={(date) => props.handleChange(date, "applicationPeriodTo")}
              clearable
              clearLabel={t("general.remove")}
              cancelLabel={t("general.cancel")}
              okLabel={t("general.select")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("ShoppingPlan.planer")}
                </span>
              }
              readOnly={true}
              searchFunction={() => { }}
              searchObject={{}}
              defaultValue={props?.item?.planer ?? ""}
              displayLable="displayName"
              typeReturnFunction="category"
              value={props?.item?.planer ?? ""}
              onSelect={(value) => props.handleChange(value, "planer")}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <FormControlLabel
              className={`mt-12`}
              value={props.item.isActive || ""}
              name="isTemporary"
              disabled={isView}
              onChange={(event) => props.handleChange(event.target.checked, "isActive")}
              control={<Checkbox checked={props.item.isActive} />}
              label={
                <span style={{ fontSize: "14px" }}>
                  {t("ShoppingPlan.active")}
                </span>
              }
            />
          </Grid>
          <Grid item md={8} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              label={t("Asset.note")}
              onChange={(event) => props.handleChange(event.target.value, "note")}
              type="text"
              InputProps={{
                readOnly: isView,
              }}
              name="note"
              value={props.item.note}
            />
          </Grid>
          <Grid item md={1} sm={6} xs={12} className="mt-12 mb-12">
            {!isView && (
              <Button
                className=" "
                size="small"
                variant="contained"
                color="primary"
                onClick={() => props.addRow()}
              >
                {t("+")}
              </Button>
            )}
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              title={t("Product.title")}
              data={props.item?.products?.length ? props.item?.products : []}
              columns={columns}
              options={{
                selection: false,
                actionsColumnIndex: 0,
                paging: true,
                search: false,
                sorting: false,
                toolbar: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData?.tableData?.id % 2 === 1 ? "#EEE" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: "dense",
                maxBodyHeight: "354px",
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: appConst.localizationVi.pagination
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
    </ >
  );
}
