import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/procurement-plans";
const API_PATH_PRODUCT =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/products/procurement-plans";
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/search-by-page";
  let config = {
    params: { ...searchObject },
  };
  return axios.get(url, config);
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const createShoppingPlan = (PurchaseRequest) => {
  return axios.post(API_PATH, PurchaseRequest);
};

export const updateShoppingPlan = (PurchaseRequest, id) => {
  return axios.put(API_PATH + "/" + id, PurchaseRequest);
};

export const getListProductShoppingPlan = (id, params) => {
  return axios.get(API_PATH_PRODUCT + "/" + id, {params});
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/purchaseRequestToExcel",
    data: searchObject,
    responseType: "blob",
  });
};
export const exportToExcelV2 = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/purchase-request",
    data: searchObject,
    responseType: "blob",
  });
};

export const getCountStatus = (type) => {
  let url = `${API_PATH}/count-by-status?type=${type}`;
  return axios.get(url);
};

export const exportToWord = (url, searchObject) => {
  return axios.get(url, {
    params: { ...searchObject },
    responseType: "blob",
  });
};
