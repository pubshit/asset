import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  deleteItem,
  searchByPage,
  exportToExcelV2 as exportToExcelService,
  getItemById,
  getListProductShoppingPlan,
} from "./PurchaseRequestService"
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst } from "app/appConst";
import VisibilityIcon from "@material-ui/icons/Visibility";
import AppContext from "app/appContext";
import ComponentShoppingPlanTable from "./ComponentShoppingPlanTable";
import {
  functionExportToExcel, handleThrowResponseMessage,
} from "app/appFunction";
import { LightTooltip } from "../Component/Utilities";
import { convertFromToDate, formatTimestampToDate, isSuccessfulResponse, isValidDate } from "../../appFunction";
import { withRouter } from "react-router-dom";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;

  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("InstrumentToolsTransfer.watchInformation")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <VisibilityIcon size="small" className="iconEye" />
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class ShoppingPlanTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.table[0],
    page: 0,
    item: {},
    id: "",
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    itemCount: {},
    rowDataProductsTable: [],
    tabValue: appConst.TAB_STATUS_SHOPPING_PLAN.ALL.index,
    itemList: [],
    openAdvanceSearch: false,

    applicationPeriodFrom: null,
    applicationPeriodTo: null,
  };


  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state?.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });

    const statusMap = {
      [appConst.TAB_STATUS_SHOPPING_PLAN.ALL.index]: null,
      [appConst.TAB_STATUS_SHOPPING_PLAN.ACTIVE.index]: appConst.STATUS_SHOPPING_PLAN.ACTIVE,
      [appConst.TAB_STATUS_SHOPPING_PLAN.UN_ACTIVE.index]: appConst.STATUS_SHOPPING_PLAN.UN_ACTIVE,
    };
    let isActive = null;
    if (newValue === appConst.TAB_STATUS_SHOPPING_PLAN.ACTIVE.index) {
      isActive = appConst.TAB_STATUS_SHOPPING_PLAN.ACTIVE.index
    }
    if (newValue === appConst.TAB_STATUS_SHOPPING_PLAN.UN_ACTIVE.index) {
      isActive = appConst.TAB_STATUS_SHOPPING_PLAN.UN_ACTIVE.index
    }
    this.search({
      isActive: isActive ? statusMap[isActive]?.index : null,
      statusFilter: statusMap[isActive],
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  setPage = (page) => {
    this.search({ page });
  };

  setRowsPerPage = (event) => {
    this.search({ rowsPerPage: event.target.value, page: 0 });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleSetDataSelect = (data, name) => {
    if ([
      "applicationPeriodFrom",
      "applicationPeriodTo",
    ].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      if (name === "statusFilter") {
        let isActive = null;
        if (data?.index === appConst.STATUS_SHOPPING_PLAN.ACTIVE.index) {
          isActive = appConst.STATUS_SHOPPING_PLAN.ACTIVE.index
        }
        if (data?.index === appConst.STATUS_SHOPPING_PLAN.UN_ACTIVE.index) {
          isActive = appConst.STATUS_SHOPPING_PLAN.UN_ACTIVE.index
        }
        this.setState({ isActive }, () => {
          this.search();
        });
        return
      }
      this.search();
    });
  };

  search = (searchObject) => {
    const newObject = searchObject ? { ...searchObject } : { page: 0 };
    this.setState({ ...newObject, selectedItem: null }, () => this.updatePageData())
  }

  updatePageData = async () => {
    const {
      keyword,
      page,
      rowsPerPage,
      isActive,
      applicationPeriodFrom,
      applicationPeriodTo,
    } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);
    const searchObject = {
      keyword: keyword?.trim(),
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      isActive,
    };
    
    if(applicationPeriodFrom && isValidDate(applicationPeriodFrom)) {
      searchObject.applicationPeriodFrom = convertFromToDate(applicationPeriodFrom).fromDate;
    }
    if(applicationPeriodTo && isValidDate(applicationPeriodTo)) {
      searchObject.applicationPeriodTo = convertFromToDate(applicationPeriodTo).toDate;
    }
    
    try {
      const res = await searchByPage(searchObject);
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        if (data?.totalElements && data?.content.length <= 0) {
          this.setPage(0);
        } else {
          const itemList = data?.content || [];
          const totalElements = data?.totalElements || 0;

          this.setState({
            itemList,
            totalElements,
            rowDataProductsTable: []
          });
        };
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
    ;
  };

  handleGetRowData = (rowData) => {
    this.setState({ selectedItem: rowData }, () => {
      this.handleGetDataProduct(rowData)
    })
  }

  handleGetDataProduct = async (rowData) => {
    try {
      const params = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      }
      const { data } = await getListProductShoppingPlan(rowData?.id, params);
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({ rowDataProductsTable: data?.data?.content })
      } else {
        this.setState({ rowDataProductsTable: [] })
      }
    } catch (e) {
      console.error(e)
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false,
      isPrint: false,
    });
  };

  exportToExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    const {
      keyword,
      page,
      rowsPerPage,
    } = this.state;
    setPageLoading(true);
    try {
      const searchObject = {
        keyword,
        pageIndex: page + 1,
        pageSize: rowsPerPage,
      };
      await functionExportToExcel(exportToExcelService, searchObject, "Yêu cầu mua sắm tài sản.xlsx");
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      item: null
    });
    this.updatePageData();
  };

  handleDeletePurchaseRequestStauts = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props
    let { setPageLoading } = this.context
    setPageLoading(true)
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count,
      });
    }
    try {
      const res = await deleteItem(this.state.id);
      if (isSuccessfulResponse(res?.data?.code)) {
        toast.info(this.props.t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount = () => {
    let { location, history } = this.props;

    if (location?.state?.id) {
      this.handleEdit(location?.state?.id)
      history.push(location?.state?.path, null);
    }
    this.updatePageData();
  }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (rowData) => {
    this.setState({
      id: rowData?.id,
      shouldOpenConfirmationDialog: true,
    });
  };

  formatDataDto(data) {
    return {
      ...data,
      products: data.products.map((item) => {
        return {
          ...item,
          product: {
            id: item?.productId,
            code: item?.productCode,
            name: item?.productName,
          }
        }
      }),
      planer: {
        id: data?.personId,
        displayName: data?.personName,
      },
      isView: data?.isView,
    }
  }

  handleEdit = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: false,
        },
        shouldOpenEditorDialog: true,
      })
    });
  }


  handleView = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: true,
        },
        shouldOpenEditorDialog: true,
      })
    });
  }

  handleGetDetailPurchaseRequest = async (id) => {
    const { t } = this.props;
    let { setPageLoading } = this.context
    try {
      setPageLoading(true)
      let res = await getItemById(id)
      const { code, data } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        let dataEdit = {
          ...data,
          isView: false,
        }
        this.setState({
          item: this.formatDataDto(dataEdit ?? {}),
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false)
    }
  }


  handleCheckIcon = (rowData, method) => {
    let actions = {
      [appConst.active.delete]: this.handleDelete,
      [appConst.active.edit]: this.handleEdit,
      [appConst.active.view]: this.handleView,
    };

    if (Object.keys(actions)?.includes(method?.toString())) {
      actions[method]?.(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  }
  checkStatus = (status) => {
    let itemStatus = appConst.listStatusPurchaseRequest.find(
      (item) => item.code === status
    );
    return itemStatus?.name;
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleCheckStatus = (rowData) => {
    let status = rowData?.isActive;
    let stausItem = appConst.LIST_STATUS_SHOPPING_PLAN.find(x => x.value === status);
    if (!stausItem) return "";
    return (
      <div className="align-center">
        <span className={`status status-${stausItem?.value ? "success" : "warning"}`}>
          {stausItem?.name}
        </span>
      </div>
    );
  };

  render() {
    const { t, i18n } = this.props;
    let {
      page,
      tabValue,
      rowsPerPage,
      rowDataProductsTable,
    } = this.state;
    let TitlePage = t("Dashboard.Purchase.ShoppingPlan");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        maxWidth: "120px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            onSelect={this.handleCheckIcon}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.managementCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
        headerStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("ShoppingPlan.code"),
        field: "managementCode",
        align: "left",
        minWidth: "160px",
        headerStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("ShoppingPlan.name"),
        field: "name",
        align: "left",
        minWidth: "160px",
      },
      {
        title: t("ShoppingPlan.planer"),
        field: "personName",
        align: "left",
        minWidth: "160px",
        headerStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("ShoppingPlan.status"),
        field: "isActive",
        minWidth: "160px",
        headerStyle: {
          textAlign: "center",
        },
        render: rowData => this.handleCheckStatus(rowData)
      },
      {
        title: t("ShoppingPlan.dataApplyFrom"),
        field: "applicationPeriodFrom",
        align: "center",
        minWidth: "140px",
        headerStyle: {
          textAlign: "center",
        },
        render: rowData => formatTimestampToDate(rowData?.applicationPeriodFrom)
      },
      {
        title: t("ShoppingPlan.dataApplyTo"),
        field: "applicationPeriodFrom",
        align: "center",
        minWidth: "140px",
        headerStyle: {
          textAlign: "center",
        },
        render: rowData => formatTimestampToDate(rowData?.applicationPeriodTo)
      },
      {
        title: t("Asset.note"),
        field: "note",
        align: "left",
        minWidth: "140px",
      }
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        maxWidth: '50px',
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("Product.code"),
        field: "code",
        align: "left",
        width: "150px",
        minWidth: "200px",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("Product.name"),
        field: "name",
        align: "left",
        minWidth: "300px",
        cellStyle: {
          textAlign: "left",
        }
      },
      {
        title: t("ShoppingPlan.quantity"),
        field: "quantity",
        align: "center",
        minWidth: 100,
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("ShoppingPlan.totalQuantity"),
        field: "requiredQty",
        align: "center",
        minWidth: 100,
        sorting: false,
        render: (rowData) => {
          let requiredQty = (rowData?.requiredQty || 0);

          requiredQty = requiredQty % 1 === 0
            ? requiredQty?.toString()
            : parseFloat(requiredQty?.toFixed(2));

          return requiredQty;
        }
      },
      {
        title: t("ShoppingPlan.aggregateQuantity"),
        field: "approvedQty",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.approvedQty || 0
      },
      {
        title: t("ShoppingPlan.remainQuantity"),
        field: "remainQuantity",
        align: "center",
        minWidth: 100,
        sorting: false,
        render: (rowData) => {
          let remainQty = (rowData?.quantity || 0) - (rowData?.approvedQty || 0);

          remainQty = remainQty % 1 === 0
            ? remainQty?.toString()
            : parseFloat(remainQty?.toFixed(2));

          return remainQty;
        }
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.purchase"), path: "list/purchase-shopping-plan" },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("purchaseRequest.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{appConst.STATUS_SHOPPING_PLAN.ACTIVE.name}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{appConst.STATUS_SHOPPING_PLAN.UN_ACTIVE.name}</span>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <ComponentShoppingPlanTable
          t={t}
          i18n={i18n}
          item={this.state}
          getRowData={this.handleGetRowData}
          handleEditItem={this.handleEditItem}
          handleDialogClose={this.handleDialogClose}
          handleTextChange={this.handleTextChange}
          search={this.search}
          handleOKEditClose={this.handleOKEditClose}
          handleConfirmationResponse={this.handleConfirmationResponse}
          columns={columns}
          handleChangePage={this.handleChangePage}
          setRowsPerPage={this.setRowsPerPage}
          updatePageData={this.updatePageData}
          handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          handleSetDataSelect={this.handleSetDataSelect}
          exportToExcel={this.exportToExcel}
        />
        <div>
          <MaterialTable
            data={rowDataProductsTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '350px',
              minBodyHeight: '260px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}

ShoppingPlanTable.contextType = AppContext;
export default withRouter(ShoppingPlanTable);
