import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const PurchaseRequestTable = EgretLoadable({
  loader: () => import("./ShoppingPlanTable")
});
const ViewComponent = withTranslation()(PurchaseRequestTable);

const ShoppingPlanRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/purchase-shopping-plan",
    exact: true,
    component: ViewComponent
  }
];

export default ShoppingPlanRoutes;