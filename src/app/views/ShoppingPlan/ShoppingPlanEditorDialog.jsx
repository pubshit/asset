import React, { Component } from "react";
import { Button, Dialog, DialogActions, } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import PurchaseRequestScrollableTabsButtonForce from "./ShoppingPlanScrollableTabsButtonForce";
import {
  createShoppingPlan,
  getListProductShoppingPlan,
  updateShoppingPlan,
} from "./PurchaseRequestService";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  convertFromToDate,
  getTheHighestRole,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  PaperComponent
} from "../../appFunction";
import { appConst, variable } from "../../appConst";
import ProductDialog from "../Product/ProductDialog";
import { getListsProduct, getNewCode } from "../Product/ProductService";
import AppContext from "../../appContext";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm"
import { searchByPage } from "../Product/ProductService";
import { debounce } from "utils";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class ShoppingPlanEditorDialog extends Component {
  state = {
    listProducts: [],
    status: null,
    note: "",
    isView: false,
    currentUser: null,
    products: [],
    shouldOpenDialogProduct: false,
    tabValue: 0,
    query: {
      keyword: ""
    },
    indexRowAddNew: null,
    itemProductNew: {},
    applicationPeriodFrom: new Date(),
    applicationPeriodTo: new Date(),
  };

  handleClose = () => {
    this.setState({
      shouldOpenDialogProduct: false,
      itemProductNew: {}
    })
  };
  handleOKEditClose = () => {
    this.setState({
      shouldOpenDialogProduct: false,
      itemProductNew: {},
      indexRowAddNew: null
    })
  };

  addRow = () => {
    let row = {};
    this.setState({
      products: [
        row,
        ...this.state.products,
      ]
    })
  }

  handleChange = (value, source) => {
    this.setState({
      [source]: value,
    });
  };

  handleChangeTab = (tabValue) => {
    this.setState({ tabValue });
  };

  formatDataDto = (purchaseRequest) => {
    let products = purchaseRequest.products?.map(item => {
      return {
        id: item?.id,
        productId: item?.product?.id,
        productCode: item?.product?.code,
        productName: item?.product?.name,
        quantity: item?.quantity,
      }
    });

    return {
      id: purchaseRequest?.id,
      code: purchaseRequest?.code,
      managementCode: purchaseRequest?.managementCode,
      name: purchaseRequest?.name,
      personId: purchaseRequest?.planer?.id,
      personName: purchaseRequest?.planer?.displayName,
      applicationPeriodFrom: convertFromToDate(purchaseRequest?.applicationPeriodFrom).fromDate,
      applicationPeriodTo: convertFromToDate(purchaseRequest?.applicationPeriodTo).toDate,
      isActive: purchaseRequest?.isActive ? 1 : 0,
      note: purchaseRequest?.note,
      products,
    }
  };

  validateFormSubmit = () => {
    let { products = [] } = this.state;
    let invalidItem = products.some(item => !item?.product?.id || !item?.quantity);
    if (invalidItem) {
      toast.warning("Chưa nhập đủ thông tin danh sách sản phẩm")
    }
    return invalidItem;
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let { id } = this.state;
    if (this.validateFormSubmit()) return;
    let dataSubmit = this.formatDataDto(this.state);
    setPageLoading(true);
    if (id) {
      updateShoppingPlan(
        dataSubmit,
        id
      ).then((res) => {
        if (isSuccessfulResponse(res?.data?.code)) {
          toast.success(t("general.updateSuccess"));
          this.props?.handleOKEditClose();
        }
        else {
          handleThrowResponseMessage(res);
        }
      }).catch(() => {
        toast.error(t("toastr.error"))
      }).finally(() => {
        setPageLoading(false);
      });
    } else {
      createShoppingPlan(dataSubmit).then((res) => {
        if (isSuccessfulResponse(res?.data?.code)) {
          toast.success(t("general.success"));
          this.props?.handleOKEditClose();
        }
        else {
          handleThrowResponseMessage(res);
        }
      }).catch((e) => {
        toast.error(t("toastr.error"))
      }).finally(() => {
        setPageLoading(false);
      });
    }
  };

  componentDidMount = () => {
    let { item } = this.props;
    let { currentUser } = getTheHighestRole();

    this.setState({
      ...item,
      planer: item?.planer || currentUser?.person
    }, () => {
      if (item?.id) {
        this.handleGetDataProduct(item?.id);
      }
    })
  }

  handleGetDataProduct = async (shoppingPlanId) => {
    let { products } = this.state;
    try {
      const params = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      }
      const { data } = await getListProductShoppingPlan(shoppingPlanId, params);
      if (appConst.CODE.SUCCESS === data?.code) {
        const listItem = data?.data?.content;
        products.forEach(element => {
          let itemSelected = listItem?.find(x => x?.id === element?.productId)
          if (itemSelected) {
            element.remainQuantity = itemSelected?.remainQuantity;
            element.requiredQty = Number(itemSelected?.requiredQty);
            element.approvedQty = Number(itemSelected?.approvedQty);
          }
        });
        this.setState({ products })
      }
    } catch (e) {
      console.error(e)
    }
  }

  handleChangeDataSubTable = (value, source, index) => {
    let { products = [] } = this.state;
    if (value?.code === variable.listInputName.New && source === "product") {
      this.setState({ indexRowAddNew: index });
      this.handleAddProductItem();
      return;
    }

    let purchaseProductsUpdated = products?.map((row, idx) => {
      if (index === idx) {
        if (source === "product") {
          let isExist = products.find(x => x?.product?.id === value?.id);
          if (Boolean(isExist)) {
            toast.warning("Sản phẩm đã có trong danh sách.")
          } else {
            row[source] = value;
          }
        } else {
          row[source] = value;
        }
        return row;
      }
      return row;
    })
    this.setState({
      products: purchaseProductsUpdated
    })
  };

  handleAddProductItem = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result.data && result.data.code) {
          let item = result.data;
          this.setState({
            itemProductNew: { ...item, name: this.state?.query?.keyword || "" },
            shouldOpenDialogProduct: true,
          });
        }
      })
      .catch(() => {
        toast.error(this.props.t("general.error"))
      });
  };

  getProductOrg = async (query) => {
    try {
      let res = await searchByPage(query)
      if (res?.data?.content && res?.status === appConst.CODE.SUCCESS) {
        this.setState({ listProducts: res?.data?.content || [] })
      }
    } catch (error) {
      console.error(error);
    }
  };

  handleChangeInputSearch = async (event) => {
    let { name, value } = event.target;
    this.setState({ query: { ...this.state?.query, keyword: value } }, () => {
      if (name === "product") {
        this.getProductOrg(this.state.query);
        return;
      }
    })
  };

  handleChangeInputFocus = async (event) => {
    let { name } = event.target;
    this.setState({ query: { ...this.state?.query, keyword: "" }, listProducts: [] }, () => {
      if (name === "product") {
        this.getProductOrg(this.state.query);
        return;
      }
    })
  };

  onInputChangeDataSearch = debounce((event, func) => {
    func(event)
  }, 500);

  handleSelectProduct = (value) => {
    let product = value?.id ? {
      id: value?.id,
      name: value?.name,
      code: value?.code,
    } : null
    this.handleChangeDataSubTable(product, "product", this.state?.indexRowAddNew);
  }

  handleRemoveProductInList = (index) => {
    let { products = [] } = this.state;
    products.splice(index, 1);
    this.setState({ products })
  };

  render() {
    let { open, t, i18n, item } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        <CustomValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">
              {t("ShoppingPlan.saveUpdate")}
            </span>
          </DialogTitle>

          <DialogContent>
            <PurchaseRequestScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              handleChange={this.handleChange}
              productTypeCode={this.productTypeCode}
              handleChangeDataSubTable={this.handleChangeDataSubTable}
              addRow={this.addRow}
              handleChangeTab={this.handleChangeTab}
              handleChangeInputSearch={this.handleChangeInputSearch}
              handleChangeInputFocus={this.handleChangeInputFocus}
              handleRemoveProductInList={this.handleRemoveProductInList}
              onInputChangeDataSearch={this.onInputChangeDataSearch}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mb-8">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => {
                  this.props.handleClose();
                }}
              >
                {t("general.cancel")}
              </Button>
              {!this.state.isView && <Button
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>}
            </div>
          </DialogActions>
        </CustomValidatorForm>
        {this.state.shouldOpenDialogProduct && (
          <ProductDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleClose}
            open={this.state?.shouldOpenDialogProduct}
            handleOKEditClose={this.handleClose}
            item={this.state.itemProductNew}
            selectProduct={this.handleSelectProduct}
            indexLinhKien={this.state?.indexRowAddNew}
          />
        )}
      </Dialog>
    );
  }
}

ShoppingPlanEditorDialog.contextType = AppContext;
export default ShoppingPlanEditorDialog;
