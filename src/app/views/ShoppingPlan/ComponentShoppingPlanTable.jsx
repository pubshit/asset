import {
  Button,
  Card,
  CardContent,
  Collapse,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  TablePagination
} from "@material-ui/core";
import React, { useContext } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import ShoppingPlanEditorDialog from "./ShoppingPlanEditorDialog";
import {
  handleKeyDown,
  handleKeyUp,
} from "app/appFunction";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { ValidatorForm } from "react-material-ui-form-validator";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { defaultPaginationProps, filterOptions } from "../../appFunction";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import AppContext from "../../appContext";
import { appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL } from "../../appConst";
import { exportToWord } from "./PurchaseRequestService";
import { purchaseRequestPrintData } from "../FormCustom/PurchaseRequest";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";

function ComponentShoppingPlanTable(props) {
  let {
    t,
    i18n,
    getRowData,
    handleEditItem,
    handleDialogClose,
    handleTextChange,
    search,
    handleOKEditClose,
    handleConfirmationResponse,
    columns,
    handleChangePage,
    setRowsPerPage,
  } = props;
  let {
    itemList,
    shouldOpenNotificationPopup,
    Notification,
    shouldOpenEditorDialog,
    item,
    shouldOpenConfirmationDialog,
    totalElements,
    rowsPerPage,
    page,
    isPrint,
    openAdvanceSearch,
    applicationPeriodFrom,
    applicationPeriodTo,
    selectedItem,
  } = props?.item;
  const { ASSET_PURCHASING } = LIST_PRINT_FORM_BY_ORG.SHOPPING_MANAGEMENT;
  const { currentOrg } = useContext(AppContext);

  let dataView = purchaseRequestPrintData(item);
  return (
    <Grid container spacing={2} justifyContent="space-between" className="mt-10">
      <Grid item md={7} xs={12}>
        <Button
          className="mb-16 mr-16 align-bottom"
          variant="contained"
          color="primary"
          onClick={() => {
            handleEditItem({
              startDate: new Date(),
              endDate: new Date(),
              isView: false
            });
          }}
        >
          {t("general.add")}
        </Button>
        {/* <Button
          className="mb-16 mr-16"
          variant="contained"
          color="primary"
          onClick={props.exportToExcel}
        >
          {t("general.exportToExcel")}
        </Button> */}

        <Button
          className="mb-16 align-bottom"
          variant="contained"
          color="primary"
          onClick={props.handleOpenAdvanceSearch}
        >
          {t("general.advancedSearch")}
          <ArrowDropDownIcon />
        </Button>

        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={handleDialogClose}
            text={t(Notification)}
            agree={t("general.agree")}
          />
        )}

      </Grid>
      <Grid item md={5} sm={12} xs={12}>
        <FormControl fullWidth>
          <Input
            className="search_box w-100"
            onChange={handleTextChange}
            onKeyDown={(e) => handleKeyDown(e, search)}
            onKeyUp={(e) => handleKeyUp(e, search)}
            placeholder={t("ShoppingPlan.search")}
            id="search_box"
            startAdornment={
              <InputAdornment position="end">
                <SearchIcon
                  onClick={() => search()}
                  className="searchTable"
                />
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      {/* Bộ lọc Tìm kiếm nâng cao */}
      <Grid item xs={12} className="pt-0 pb-0">
        <ValidatorForm onSubmit={() => { }}>
          <Collapse in={openAdvanceSearch}>
            <Card elevation={1}>
              <CardContent>
                <Grid container spacing={2}>
                  {/* Từ ngày */}
                  <Grid item md={3} sm={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("ShoppingPlan.dataApplyFrom")}
                        format="dd/MM/yyyy"
                        name={"applicationPeriodFrom"}
                        value={applicationPeriodFrom}
                        maxDate={applicationPeriodTo ? (new Date(applicationPeriodTo)) : undefined}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "applicationPeriodFrom"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        minDateMessage={t("general.minDateDefault")}
                        maxDateMessage={applicationPeriodTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  {/* Đến ngày */}
                  <Grid item md={3} sm={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("ShoppingPlan.dataApplyTo")}
                        format="dd/MM/yyyy"
                        name={"applicationPeriodTo"}
                        value={applicationPeriodTo}
                        minDate={applicationPeriodFrom ? (new Date(applicationPeriodFrom)) : undefined}
                        minDateMessage={applicationPeriodFrom ? t("general.minDateToDate") : t("general.minDateDefault")}
                        maxDateMessage={t("general.maxDateDefault")}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "applicationPeriodTo"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date" }}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  {/* Từ ngày */}
                  {/* <Grid item md={3} sm={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("Ngày áp dụng từ")}
                        format="dd/MM/yyyy"
                        name={"applicationPeriodToTop"}
                        value={applicationPeriodToTop}
                        maxDate={applicationPeriodToBottom ? (new Date(applicationPeriodToBottom)) : undefined}
                        minDateMessage={t("general.minDateDefault")}
                        maxDateMessage={t("general.maxDateDefault")}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "applicationPeriodToTop"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid> */}
                  {/* Đến ngày */}
                  {/* <Grid item md={3} sm={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("MaintainPlaning.dxTo")}
                        format="dd/MM/yyyy"
                        name={"fromPlanDate"}
                        value={applicationPeriodToBottom}
                        minDate={applicationPeriodToTop ? (new Date(applicationPeriodToTop)) : undefined}
                        minDateMessage={t("general.minDateDefault")}
                        maxDateMessage={t("general.maxDateDefault")}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "applicationPeriodToBottom"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date" }}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid> */}
                  <Grid item md={3} sm={4} xs={12}>
                    <AsynchronousAutocompleteSub
                      className="w-100"
                      label={t("purchaseRequest.status")}
                      searchFunction={() => { }}
                      searchObject={{}}
                      listData={appConst.LIST_STATUS_SHOPPING_PLAN || []}
                      displayLable="name"
                      value={props?.item?.statusFilter || null}
                      onSelect={(value) => props.handleSetDataSelect(value, "statusFilter")}
                      disabled={props?.item?.tabValue !== appConst.tabPurchaseRequest.tabAll}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Collapse>
        </ValidatorForm>
      </Grid>
      <Grid item xs={12}>
        <div>
          {shouldOpenEditorDialog && (
            <ShoppingPlanEditorDialog
              t={t}
              i18n={i18n}
              handleClose={handleDialogClose}
              open={shouldOpenEditorDialog}
              handleOKEditClose={handleOKEditClose}
              item={item}
            />
          )}
          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={handleDialogClose}
              onYesClick={handleConfirmationResponse}
              text={t("general.deleteConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}
          {isPrint && (
            currentOrg?.code === LIST_ORGANIZATION.BVDK_BA_VI.code ? (
              <PrintPreviewTemplateDialog
                t={t}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={item}
                title={t("Phiếu xác định nhu cầu cần mua")}
                model={PRINT_TEMPLATE_MODEL.PURCHASE_MANAGEMENT.REQUEST}
              />
            ) : (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={dataView || item}
                title={t("Phiếu xác định nhu cầu cần mua")}
                funcExportWord={exportToWord}
                payloadExport={{
                  id: dataView?.itemEdit?.id,
                  assetClass: dataView?.itemEdit?.assetClass,
                }}
                urlWord={
                  currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code
                    ? ASSET_PURCHASING.PURCHASE_REQUEST.urlPrintWord
                    : null
                }
                urls={[
                  ...ASSET_PURCHASING.PURCHASE_REQUEST.GENERAL,
                  ...ASSET_PURCHASING.PURCHASE_REQUEST[currentOrg?.printCode],
                ]}
              />
            )
          )}
        </div>
        <MaterialTable
          title={t("general.list")}
          data={itemList ? [...itemList] : []}
          columns={columns}
          localization={{
            body: {
              emptyDataSourceMessage: `${t(
                "general.emptyDataMessageTable"
              )}`,
            },
            toolbar: {
              nRowsSelected: `${t("general.selects")}`,
            },
          }}
          options={{
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            sorting: false,
            draggable: false,
            rowStyle: (rowData) => ({
              backgroundColor:
                selectedItem?.id === rowData.id
                  ? "#ccc" :
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            maxBodyHeight: "450px",
            minBodyHeight: itemList?.length > 0 ? 0 : 250,
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          onRowClick={(e, rowData) => {
            getRowData(rowData)
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
        />
        <TablePagination
          {...defaultPaginationProps()}
          rowsPerPageOptions={appConst.rowsPerPageOptions.table}
          count={totalElements}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
            }`
          }
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={setRowsPerPage}
        />
      </Grid>
    </Grid>
  )
}

export default ComponentShoppingPlanTable;
