import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const OrganizationTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./OrganizationTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(OrganizationTable);

const OrganizationRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/organization",
    exact: true,
    component: ViewComponent
  }
];

export default OrganizationRoutes;