import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/v2/organizations";

export const searchByPage = (params) => {
  let config = {
    params,
  }
  return axios.get(API_PATH + "/page", config);
};

export const createOne = Organization => {
  return axios.post(API_PATH, Organization);
};

export const updateOne = Organization => {
  return axios.put(API_PATH + "/" + Organization.id, Organization);
};


export const getOneById = id => {
  return axios.get(API_PATH + "/" + id);
};

export const deleteById = id => {
  return axios.delete(API_PATH + "/" + id);
};

