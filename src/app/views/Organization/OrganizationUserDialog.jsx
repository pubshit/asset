import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  TextField,
  FormControlLabel,
  DialogContent,
} from "@material-ui/core";
// import Paper from '@material-ui/core/Paper'
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import { createOne, updateOne } from "./OrganizationService";
import "react-toastify/dist/ReactToastify.css";
import {variable} from "../../appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class OrganizationUserDialog extends Component {
  state = {
    id: "",
    username: "",
    password: "",
    indexOrder: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
    active: false,
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleFormSubmit = () => {
    let { username, password, active } = this.state;
    let { selectListOrganizationUser } = this.props;
    let user = {
      username,
      password,
      active,
    };

    if(username && password) {
      selectListOrganizationUser(user);
    } 
  };

  componentDidMount() {
    let { item = {} } = this.props;
    this.setState({...item});
  }

  handleChange = (event, source) => {
    if (source === variable.listInputName.active) {
      this.setState({ active: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    let {
      id,
      name,
      code,
      level,
      indexOrder,
      shouldOpenNotificationPopup,
      username,
      password,
      isAdminUser,
      active

    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4 className="">{t("organization.dialog")}</h4>
        </DialogTitle>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={2}>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("user.username")}
                    </span>
                  }
                  // label={t('user.username')}
                  onChange={this.handleChange}
                  type="text"
                  name="username"
                  value={username}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>

              <Grid item sm={6} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("user.pass")}
                    </span>
                  }
                  // variant="outlined"
                  onChange={this.handleChange}
                  name="password"
                  type="password"
                  value={password}
                  validators={["required", "matchRegexp:^.{6,}$"]}
                  errorMessages={[
                    t("general.required"),
                    "Mật khẩu ít nhất có 6 kí tự",
                  ]}
                />
              </Grid>
              <Grid item sm={3} xs={12}>
                {/* <div style={{ marginTop: "24.25px" }}> */}
                  <FormControlLabel
                    value={active}
                    className=""
                    name="active"
                    onChange={(active) => this.handleChange(active, "active")}
                    control={<Checkbox checked={active} />}
                    label={t("user.active")}
                  />
                {/* </div> */}
              </Grid>
              <Grid item sm={6} xs={12}>
                {/* <div style={{ marginTop: "24.25px" }}> */}
                  <FormControlLabel
                    value={isAdminUser}
                    className=""
                    name="isAdminUser"
                    onChange={(isAdminUser) =>
                      this.handleChange(isAdminUser, "isAdminUser")
                    }
                    control={<Checkbox checked={isAdminUser} />}
                    label={t("organization.isAdminUser")}
                  />
                {/* </div> */}
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                style={{ marginRight: "15px" }}
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default OrganizationUserDialog;
