import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  DialogTitle,
  Icon,
  IconButton,
  DialogContent,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import { useTranslation } from "react-i18next";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import {createOne, getOneById, searchByPage, updateOne} from "./OrganizationService";
import OrganizationUserDialog from "./OrganizationUserDialog";
import "react-toastify/dist/ReactToastify.css";
import SelectOrganizationPopup from "../Component/Organization/SelectOrganizationPopup";
import { appConst } from "app/appConst";
import {PaperComponent} from "../Component/Utilities";
import {handleThrowResponseMessage, isSuccessfulResponse} from "../../appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">
          edit
        </Icon>
      </IconButton>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

class OrganizationDialog extends Component {
  state = {
    id: null,
    name: "",
    code: "",
    indexOrder: "",
    isActive: false,
    shouldOpenNotificationPopup: false,
    shouldOpenOrgPopup: false,
    Notification: "",
    shouldOpenOrganizationUserDialog: false,
    users: [],
    item: null,
    parent: null,
    subs: [],
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenOrganizationUserDialog: false,
      shouldOpenOrgPopup: false,
    });
  };

  handleChange = (event, source) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSelectOrg = (parent) => {
    this.setState({ parent }, () => {
      this.handleDialogClose();
    });
  };
  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props
    let { id } = this.state;

    try {
      const sendData = this.convertDataSubmit();
      const res = id ? await updateOne(sendData) : await createOne(sendData);
      const { code } = res?.data;

      if (isSuccessfulResponse(code)) {
        toast.success(t("general.success"));
        this.props.handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
      setPageLoading(false);
    }
  };

  convertDataSubmit = () => {
    return {
      id: this.state.id,
      name: this.state.name,
      code: this.state.code,
      subs: this.state.subs,
      users: this.state.users,
      subCodes: this.state.subCodes,
      parentCode: this.state.parent?.code,
      parentId: this.state.parent?.id,
      parentName: this.state.parent?.name,
    }
  }

  componentDidMount = () => {
    let { item } = this.props;
    this.setState({
      ...item,
      parent: {
        code: item?.parentCode,
        id: item?.parentId,
        name: item?.parentName,
      }
    });
  }

  selectListOrganizationUser = (item) => {
    let { users = [] } = this.state;
    let isExistUserName = users.some(user => user.username === item?.username);

    if (isExistUserName) {
      toast.warning(t("organization.duplicateUserName"))
    } else {
      users.push(item);
    }

    this.setState({
      users,
      shouldOpenOrganizationUserDialog: false,
    });
  };

  handleDeleteUser = (rowData) => {
    let { users } = this.state;
    users.map((user, index) => {
      if (user.tableData.id === rowData.tableData.id) {
        users.splice(index, 1);
      }
    });
    this.setState({ users: users });
  }

  handleEditUser = (rowData) => {
    this.setState({
      item: rowData,
      shouldOpenOrganizationUserDialog: true,
    });
  }

  render() {
    let {
      id,
      name,
      code,
      parent,
      shouldOpenNotificationPopup,
      shouldOpenOrganizationUserDialog,
      users,
    } = this.state;
    let { open, t, i18n } = this.props;
    let columns = [
      { title: t("user.username"), field: "username", width: "150" },
      {
        title: t("user.active"),
        field: "active",
        width: "150",
        render: (rowData) => {
          let active = rowData.active;
          if (active !== null) {
            return appConst.OBJECT_ACTIVE[active].name || "";
          }
        },
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        hidden: id,
        width: "250",
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleEditUser(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDeleteUser(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4 className="">{t("organization.dialog")}</h4>
        </DialogTitle>

        {shouldOpenOrganizationUserDialog && (
          <OrganizationUserDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleDialogClose}
            open={shouldOpenOrganizationUserDialog}
            selectListOrganizationUser={this.selectListOrganizationUser}
            item={this.state.item}
          />
        )}

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="mb-12" container spacing={2}>
              <Grid item sm={6} xs={6}>
                <TextValidator
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("organization.code")}
                    </span>
                  }
                  className="w-100"
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", "matchRegexp:^[^/]*$"]}
                  errorMessages={[t("general.required"), "Không được chứa ký tự "/""]}
                />
              </Grid>

              <Grid item sm={6} xs={6}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("organization.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("organization.parent")}
                  searchFunction={searchByPage}
                  searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
                  displayLable="name"
                  typeReturnFunction="category"
                  showCode
                  value={parent || null}
                  onSelect={this.handleSelectOrg}
                />
              </Grid>
            </Grid>
            <Grid item sm={6} xs={6}>
              <Button
                className="mb-12"
                variant="contained"
                color="primary"
                disabled={!!id}
                onClick={() =>
                  this.setState({
                    shouldOpenOrganizationUserDialog: true,
                    item: {},
                  })
                }
              >
                {t("organization.add")}
              </Button>
            </Grid>
            <MaterialTable
              title={t("manage.departmentList")}
              data={users ? users : []}
              columns={columns}
              options={{
                selection: false,
                actionsColumnIndex: 0,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "223px",
                minBodyHeight: "223px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                style={{ marginRight: "15px" }}
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default OrganizationDialog;
