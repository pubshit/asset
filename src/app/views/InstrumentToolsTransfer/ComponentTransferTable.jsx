import {
    Link,
    Card,
    Grid,
    Input,
    Button,
    Collapse,
    FormControl,
    InputAdornment,
    TablePagination,
    TextField,
} from "@material-ui/core";
import React, { useContext } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import InstrumentToolsTransferDialog from "./InstrumentToolsTransferDialog";
import MaterialTable, { MTableToolbar } from "material-table";
import {appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, variable} from "app/appConst";
import { dataViewDCTSTB } from "../FormCustom/DCTSTB";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import {NumberFormatCustom, filterOptions, defaultPaginationProps} from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { exportExcelFileError, importExcelTransferIatURL, } from './InstrumentToolsTransferService';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import { Autocomplete } from "@material-ui/lab";
import { useEffect } from "react";
import AssetsQRPrint from "../Asset/ComponentPopups/AssetsQRPrint";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { searchByPageDeparment } from "../Department/DepartmentService";
import { STATUS_DEPARTMENT } from "../../appConst";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";

function ComponentTransferTable(props) {
  let {t, i18n, getRowData} = props;
  let {
    item,
    page,
    toDate,
    isPrint,
    itemList,
    fromDate,
    openAdvanceSearch,
    rowsPerPage,
    isRoleAdmin,
    statusIndex,
    totalElements,
    originalCostTo,
    transferStatus,
    isRoleAssetUser,
    isRoleOrgAdmin,
    isRoleAssetManager,
    originalCostFrom,
    receiverDepartment,
    handoverDepartment,
    hasCreatePermission,
    listHandoverDepartment,
    listReceiverDepartment,
    shouldOpenEditorDialog,
    shouldOpenImportExcelDialog,
    shouldOpenConfirmationDialog,
    shouldOpenConfirmationDeleteAllDialog,
    openPrintQR,
    products,
    selectedItem
  } = props?.item;
  const {currentOrg} = useContext(AppContext);
  const {TRANSFER} = LIST_PRINT_FORM_BY_ORG.IAT_MANAGEMENT;
  let isButtonAll = statusIndex === null;
  const searchObject = {pageIndex: 1, pageSize: 1000000};
  const searchObjectDepartment = {
    ...searchObject,
    checkPermissionUserDepartment: false,
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
  }
  const importUrl = importExcelTransferIatURL();
  const isShowPrintPreview = [
    LIST_ORGANIZATION.BVDK_MY_DUC.code,
  ].includes(currentOrg?.code);

    useEffect(() => {
        let value = appConst.listStatusTransfer.find(item => item.indexOrder === statusIndex)
        props.handleSetDataSelect(value, "transferStatus")
    }, [statusIndex]);

    const handleKeyDown = (e) => {
        if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
            e.preventDefault();
        }
        props.handleKeyDownEnterSearch(e);
    };


    return (
        <>
            <Grid container spacing={2} justifyContent="space-between">
                <Grid item md={6} xs={12}>
                    {(hasCreatePermission || isRoleAdmin) && isButtonAll && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleButtonAdd({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("InstrumentToolsTransfer.instrumentToolsTransfer")}
                        </Button>
                    )}
                    {(isRoleAssetManager || isRoleOrgAdmin) && (
                        <Button
                            className="align-bottom mr-16"
                            variant="contained"
                            color="primary"
                            onClick={props.importExcel}
                        >
                            {t("general.importExcel")}
                        </Button>
                    )}
                    {!isRoleAssetUser && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.exportToExcel}
                        >
                            {t("general.exportToExcel")}
                        </Button>
                    )}
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>

                    {shouldOpenImportExcelDialog && (
                        <ImportExcelDialog
                            t={t}
                            i18n={i18n}
                            handleClose={props.handleDialogClose}
                            open={shouldOpenImportExcelDialog}
                            handleOKEditClose={props.handleOKEditClose}
                            exportExampleImportExcel={props.exportExampleImportExcel}
                            exportFileError={exportExcelFileError}
                            url={importUrl}
                        />
                    )}

                    {shouldOpenConfirmationDeleteAllDialog && (
                        <ConfirmationDialog
                            open={shouldOpenConfirmationDeleteAllDialog}
                            onConfirmDialogClose={props.handleDialogClose}
                            onYesClick={props.handleDeleteAll}
                            text={t("general.deleteAllConfirm")}
                        />
                    )}

                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("AssetTransfer.filterCCDC")}
                            id="search_box"
                            value={props.item.keyword || ""}
                            startAdornment={
                                <InputAdornment  position="end">
                                    <SearchIcon onClick={() => props.search()} className="searchTable"/>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>

                {/* Bộ lọc Tìm kiếm nâng cao */}
                <Grid item xs={12}>
                    <Collapse in={openAdvanceSearch}>
                        <ValidatorForm onSubmit={() => { }}>
                            <Card elevation={2} className="pt-8 pb-12 pl-16 pr-0">
                                <Grid container spacing={2}>
                                    {/* Phòng bàn giao */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteSub
                                            label={t("allocation_asset.handoverDepartment")}
                                            searchFunction={searchByPageDeparment}
                                            searchObject={searchObjectDepartment}
                                            listData={listHandoverDepartment || []}
                                            setListData={(data) => props?.handleSetDataSelect(
                                                data,
                                                "listHandoverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            defaultValue={handoverDepartment ? handoverDepartment : null}
                                            displayLable={"name"}
                                            value={handoverDepartment ? handoverDepartment : null}
                                            onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                                            noOptionsText={t("general.noOption")}
                                        // disabled={!isRoleOrgAdmin && !isRoleAdmin}
                                        />
                                    </Grid>
                                    {/* Phòng tiếp nhận */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteSub
                                            className="w-100"
                                            label={t("allocation_asset.receiverDepartment")}
                                            searchFunction={searchByPageDeparment}
                                            searchObject={searchObjectDepartment}
                                            listData={listReceiverDepartment || []}
                                            setListData={(data) => props.handleSetDataSelect(
                                                data,
                                                "listReceiverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            defaultValue={receiverDepartment ? receiverDepartment : null}
                                            displayLable={"name"}
                                            value={receiverDepartment ? receiverDepartment : null}
                                            onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Từ ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxFrom")}
                                                format="dd/MM/yyyy"
                                                value={fromDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "fromDate"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                maxDate={toDate ? new Date(toDate) : undefined}
                                                minDateMessage={t("general.minDateMessage")}
                                                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Đến ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxTo")}
                                                format="dd/MM/yyyy"
                                                value={toDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "toDate"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                minDate={fromDate ? new Date(fromDate) : undefined}
                                                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                maxDateMessage={t("general.maxDateMessage")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Trạng thái */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <Autocomplete
                                            fullWidth
                                            options={appConst.listStatusTransferCCDC}
                                            defaultValue={transferStatus ? transferStatus : null}
                                            value={transferStatus ? transferStatus : null}
                                            onChange={(e, value) => props.handleSetDataSelect(value, "transferStatus")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            getOptionLabel={option => option.name}
                                            disabled={!isButtonAll}
                                            noOptionsText={t("general.noOption")}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    value={transferStatus?.name || ""}
                                                    label={t("maintainRequest.status")}
                                                />
                                            )}
                                        />
                                    </Grid>
                                    {/* Nguyên giá từ - đến */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <TextValidator
                                            fullWidth
                                            label={t("general.originalCostFrom")}
                                            value={originalCostFrom}
                                            name="originalCostFrom"
                                            onKeyUp={props.handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => props.handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={3}>
                                        <TextValidator
                                            fullWidth
                                            label={t("general.originalCostTo")}
                                            value={originalCostTo}
                                            name="originalCostTo"
                                            onKeyUp={props.handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => props.handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </Card>
                        </ValidatorForm>
                    </Collapse>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {isPrint && (
                          isShowPrintPreview ? (
                            <PrintPreviewTemplateDialog
                              t={t}
                              handleClose={props.handleDialogClose}
                              open={isPrint}
                              item={item}
                              title={t("Phiếu điều chuyển y cụ dụng cụ")}
                              model={PRINT_TEMPLATE_MODEL.IAT_MANAGEMENT.TRANSFER}
                            />
                          ) : (
                            <PrintMultipleFormDialog
                              t={t}
                              i18n={props.item?.i18n}
                              handleClose={props.handleDialogClose}
                              open={isPrint}
                              item={dataViewDCTSTB(item)}
                              title={t("Phiếu điều chuyển tài sản thiết bị")}
                              urls={[
                                ...TRANSFER.GENERAL,
                                ...(TRANSFER[currentOrg?.printCode] || []),
                              ]}
                            />
                          )
                        )}

                        {shouldOpenEditorDialog && (
                            <InstrumentToolsTransferDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                handleQrCode={props.handleQrCode}
                            />
                        )}
                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {openPrintQR && (
                            <AssetsQRPrint
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                    </div>
                    <MaterialTable
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        // parentChildData={(row, rows) => {
                        //     var list = rows.find((a) => a.id === row.parentId);
                        //     return list;
                        // }}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            draggable: false,
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: selectedItem?.id === rowData?.id
                                    ? "#ccc"
                                    :  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "450px",
                            minBodyHeight: "250px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                paddingLeft: 10,
                                paddingRight: 10,
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            this.data = rows;
                        }}
                    />
                    <TablePagination
                      {...defaultPaginationProps()}
                      rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                      count={totalElements}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      onPageChange={props.handleChangePage}
                      onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default ComponentTransferTable;
