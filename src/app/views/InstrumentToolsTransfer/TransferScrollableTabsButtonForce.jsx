import React, { useEffect, useState } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import SelectInstrumentToolsAllPopup from "../Component/InstrumentTools/SelectInstrumentToolsAllPopup";
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { createFilterOptions } from "@material-ui/lab/Autocomplete";
import { getStatus } from "./InstrumentToolsTransferService";
import {
  LightTooltip,
  TabPanel,
  a11yProps,
  checkInvalidDate,
  convertNumberPrice,
  formatDateArray, handleKeyDownIntegerOnly, formatTimestampToDate
} from "app/appFunction";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import {appConst, TYPE_OF} from "app/appConst";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import moment from "moment";
import { searchByPageDeparment } from "../Department/DepartmentService";
import { STATUS_DEPARTMENT } from "../../appConst";
import {NumberFormatCustom} from "../Component/Utilities";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton disabled={props.disabled} size='small' onClick={() => props.onSelect(item, appConst.active.delete)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function TransferScrollableTabsButtonForce(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });

  const searchObject = {...appConst.OBJECT_SEARCH_MAX_SIZE};
  const searchObjectDepartment = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    checkPermissionUserDepartment: false,
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
  }
  const filterAutocomplete = createFilterOptions();
  let transferStatus = appConst.listStatusTransfer.find(status => status.indexOrder === props?.item?.transferStatus?.indexOrder) || "";
  const listStatus = props?.item?.listStatus?.filter(
    (statusTranfer) =>
      statusTranfer.indexOrder !== appConst.STATUS_TRANSFER.TRA_VE.indexOrder &&
      statusTranfer.indexOrder !== appConst.STATUS_TRANSFER.HUY_XAC_NHAN.indexOrder
  );
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (props.item?.handoverDepartment) {
      setSearchParamUserByHandoverDepartment({
        ...searchObject,
        ...searchParamUserByHandoverDepartment,
        departmentId: props.item?.handoverDepartment?.id
      })
    }
    if (props.item?.receiverDepartment) {
      setSearchParamUserByReceiverDepartment({
        ...searchObject,
        ...searchParamUserByReceiverDepartment,
        departmentId: props.item?.receiverDepartment?.id
      })
    }
  }, [props.item?.handoverDepartment, props.item?.receiverDepartment])

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton
          disabled={props?.item?.isView || props?.item?.isStatus}
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
            } else if (method === appConst.active.delete) {
              props.removeAssetInlist(rowData?.asset?.id);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />
      ),
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props.item.page) * props.item.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("InstrumentToolsList.code"),
      field: "asset.code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.name"),
      field: "asset.name",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("InstrumentToolsList.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetSerialNumber || rowData?.asset.serialNumber
    },
    {
      title: t("InstrumentToolsList.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetModel || rowData?.asset.model
    },
    {
      title: t("InstrumentToolsList.dateOfReception"),
      field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        if (typeof rowData?.asset?.dateOfReception === TYPE_OF.ARRAY) {
          return formatDateArray(rowData?.asset?.dateOfReception)
        }
        return formatTimestampToDate(rowData?.asset?.dateOfReception)
      }
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetYearOfManufacture || rowData?.asset.yearOfManufacture
    },
    {
      title: t("InstrumentToolsList.manufacturer"),
      field: "asset.manufacturerName",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.manufacturerName || rowData?.asset.manufacturerName

    },
    {
      title: t("InstrumentToolsList.availableQuantity"),
      field: "asset.availableQuantity",
      align: "left",
      minWidth: 100,
      hidden: props.item?.transferStatusIndex === appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.availableQuantity || rowData?.asset?.quantity,
    },
    {
      title: t("InstrumentToolsAllocation.quantity"),
      field: "quantity",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => (
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={quantity => props.handleRowDataCellChange(rowData, quantity, "quantity")}
          onKeyDown={handleKeyDownIntegerOnly}
          type="text"
          name="note"
          value={rowData?.quantity}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: props?.item?.isView || props?.item?.isStatus,
            inputProps: {
              className: "text-center",
            }
          }}
          validators={[
            "required",
            `maxNumber:${rowData?.availableQuantity
              ? rowData?.availableQuantity
              : rowData?.asset?.quantity
            }`,
            "minNumber:1",
          ]}
          errorMessages={[
            t('general.required'),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
            t("general.minNumberError"),
          ]}
        />
      ),
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.asset?.originalCost
        ? convertNumberPrice(rowData?.asset?.originalCost)
        : 0
    },
    {
      title: t("Asset.note"),
      field: "note",
      align: "left",
      minWidth: 200,
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note, "note")}
          type="text"
          name="note"
          value={rowData?.note || ""}
          InputProps={{
            readOnly: props?.item?.isView || props?.item?.isStatus,
          }}
          validators={["maxStringLength:255"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />,
    },
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      align: "left",
      minWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 180,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} />
          <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={6} xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <DateTimePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t('Ngày tạo phiếu')}
                  </span>}
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                name={'createDate'}
                value={props?.item?.createDate}
                InputProps={{
                  readOnly: true,
                }}
                readOnly={true}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={6} xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog mt-2"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("AssetTransfer.issueDate")}
                  </span>
                }
                name={'issueDate'}
                inputVariant="standard"
                autoOk={true}
                format="dd/MM/yyyy"
                value={props.item.issueDate}
                onChange={date => props.handleDateChange(date, "issueDate")}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                InputProps={{
                  readOnly: props?.item?.isView || props.item?.isStatus
                }}
                open={isPickerOpen}
                onOpen={() => {
                  if (props?.item?.isView || props.item?.isStatus) {
                    setIsPickerOpen(false);
                  }
                  else {
                    setIsPickerOpen(true);
                  }
                }}
                onClose={() => setIsPickerOpen(false)}
                validators={['required']}
                errorMessages={t('general.required')}
                minDate={new Date("01/01/1900")}
                minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                onBlur={() => handleBlurDate(props.item.issueDate, "issueDate")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("allocation_asset.handoverDepartment")}
                </span>
              }
              searchFunction={searchByPageDeparment}
              searchObject={searchObjectDepartment}
              listData={props.item.listHandoverDepartment}
              nameListData="listHandoverDepartment"
              setListData={props.handleSetDataSelect}
              displayLable={"name"}
              showCode="code"
              isNoRenderChildren
              isNoRenderParent
              readOnly={(props.item?.isView || props.item?.isStatus || props?.isRoleAssetUser)}
              value={props.item?.handoverDepartment || null}
              onSelect={props?.handleSelectHandoverDepartment}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {(props?.item?.isView || props?.item?.isStatus) ?
              <TextValidator
                className="w-100"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t('allocation_asset.handoverPerson')}
                  </span>
                }
                name="handoverPerson"
                value={props?.item?.handoverPersonName ?? ''}
                InputProps={{
                  readOnly: props?.item?.isView || props?.item?.isStatus,
                }}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              /> : <AsynchronousAutocompleteSub
                className="w-100"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("allocation_asset.handoverPerson")}
                  </span>
                }
                disabled={!props?.item?.handoverDepartment?.id || props.item?.isView || props.item?.isStatus}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByHandoverDepartment}
                defaultValue={props?.item?.handoverPerson ?? ''}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                value={props?.item?.handoverPerson ?? ''}
                onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />}
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {!props.item?.isNew && !props.item?.isStatus ?
              <TextValidator
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("maintainRequest.status")}
                  </span>
                }
                value={transferStatus ? transferStatus?.name : ""}
              /> : <AsynchronousAutocompleteTransfer
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("maintainRequest.status")}
                  </span>
                }
                searchFunction={getStatus}
                searchObject={{}}
                listData={listStatus || []}
                setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                defaultValue={transferStatus ? transferStatus : null}
                displayLable={'name'}
                value={transferStatus ? transferStatus : null}
                onSelect={props.selectAssetTransferStatusStatus}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
                typeReturnFunction="status"
              />}
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {(props.item?.isView || props.item?.isStatus) ?
              <TextValidator
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("allocation_asset.receiverDepartment")}
                  </span>
                }
                value={props.item?.receiverDepartment ? props.item?.receiverDepartment?.text : ""}

              /> :
              <AsynchronousAutocompleteSub
                className="w-100"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("allocation_asset.receiverDepartment")}
                  </span>
                }
                searchFunction={searchByPageDeparment}
                searchObject={searchObjectDepartment}
                listData={props.item.listReceiverDepartment}
                setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
                defaultValue={props.item?.receiverDepartment ? props.item?.receiverDepartment : ''}
                displayLable={"name"}
                value={props.item?.receiverDepartment ? props.item?.receiverDepartment : ''}
                onSelect={receiverDepartment => props?.handleSelectReceiverDepartment(receiverDepartment, "receiverDepartment")}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />}

          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {/* <TextValidator
              className="w-100"
              label={t("AssetTransfer.receiverPerson")}
              name="receiverPersonName"
              onChange={props.handleChange}
              value={props.item?.receiverPersonName ? props.item?.receiverPersonName : ""}
              InputProps={{
                readOnly: props.item?.isView || props.item?.isStatus,
              }}
            /> */}
            {(props?.item?.isView || props?.item?.isStatus) ? <TextValidator
              className="w-100"
              label={t("AssetTransfer.receiverPerson")}
              name="receiverPerson"
              onChange={props?.handleChange}
              value={props?.item?.receiverPersonName ? props?.item?.receiverPersonName : ""}
              InputProps={{
                readOnly: props?.item?.isView || props?.item?.isStatus,
              }}
            /> : <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  {/* <span className="colorRed">* </span> */}
                  {t("AssetTransfer.receiverPerson")}
                </span>
              }
              disabled={!props?.item?.receiverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByReceiverDepartment}
              defaultValue={props?.item?.receiverPerson ?? ''}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              value={props?.item?.receiverPerson ?? ''}
              onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim()
                let filtered = filterAutocomplete(options, params)
                return filtered
              }}
              noOptionsText={t("general.noOption")}
            // validators={["required"]}
            // errorMessages={[t('general.required')]}
            />}
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            {(!props?.item?.isView && !props.item?.isStatusConfirmed) &&
              <Button variant="contained" color="primary"
                size="small" className="mb-16 w-100"
                onClick={props.openPopupSelectAsset}
              >
                {t('InstrumentToolsTransfer.choose')}
              </Button>
            }

            {props.item.handoverDepartment && props.item.shouldOpenAssetPopup && (
              <SelectInstrumentToolsAllPopup
                open={props.item.shouldOpenAssetPopup}
                handleSelect={props.handleSelectAsset}
                departmentId={props.item.departmentId}
                isAssetTransfer={props.item.isAssetTransfer}
                statusIndexOrders={props.item.statusIndexOrders}
                handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                receiverDepartmentId={props.item.handoverDepartment.id || ''}
                assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                issueDateTop={props.item?.issueDate}
                isFilterManagementDepartment={true}
                isSearchForTransfer
              />
            )}
          </Grid>
        </Grid>
        <Grid item md={10} sm={12} xs={12}></Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={props.item.assetVouchers ? props.item.assetVouchers : []}
              columns={columns}
              options={{
                disableSortBy: true,
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF'
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={12} sm={12} xs={12}>
          {(!props?.item?.isView && !props.item?.isStatusConfirmed) &&
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={
                props.handleAddAssetDocumentItem
              }
            >
              {t('InstrumentToolsTransfer.addFile')}
            </Button>}

          {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              isEditAssetDocument={props?.item?.isEditAssetDocument}
              open={props.item.shouldOpenPopupAssetFile}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocument={props.getAssetDocument}
              handleUpdateAssetDocument={props.handleUpdateAssetDocument}
              documentType={props?.item?.documentType}
              item={props?.item?.item}
              t={t} i18n={i18n}
              isIAT={true}
            />)
          }
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item.documents || []}
            columns={columnsVoucherFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: 'dense',
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              maxBodyHeight: '285px',
              minBodyHeight: '285px',
            }}
            components={{
              Toolbar: props => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div >
  );
}