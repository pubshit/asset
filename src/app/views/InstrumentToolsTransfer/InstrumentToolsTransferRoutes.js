import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import {  withTranslation } from 'react-i18next';
const InstrumentToolsTransferTable = EgretLoadable({
  loader: () => import("./InstrumentToolsTransferTable")
});
const ViewComponent = withTranslation()(InstrumentToolsTransferTable);

const InstrumentToolsTransferRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "instruments-and-tools/transfer-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default InstrumentToolsTransferRoutes;