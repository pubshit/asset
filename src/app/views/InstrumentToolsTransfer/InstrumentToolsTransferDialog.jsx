import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { Button, Dialog, DialogActions, Icon, IconButton, } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { getAssetDocumentById, getNewCodeDocument, } from "../Asset/AssetService";
import { createTransfer, updateStatus, updateTransfer, } from "./InstrumentToolsTransferService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { useTranslation } from "react-i18next";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import TransferScrollableTabsButtonForce from "./TransferScrollableTabsButtonForce";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import RefuseTransferDialog from "../Component/InstrumentToolsTransfer/RefuseTransferDialog";
import { appConst } from "app/appConst";
import { variable } from "../../appConst";
import {
  checkInvalidDate,
  checkMinDate,
  formatDateDto,
  formatDateTimeDto,
  getTheHighestRole, handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import moment from "moment";
import AppContext from "app/appContext";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});


function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}
class InstrumentToolsTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Transfer,
    handoverPerson: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPersonName: null,
    issueDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: [2], // nhập kho, cấp phát và bảo dưỡng
    handoverPersonClone: false,
    assetDocumentList: [],
    documentType: appConst.documentType.IAT_DOCUMENT,
    shouldOpenPopupAssetFile: false,
    shouldOpenRefuseTransfer: false,
    assetTransferStatus: null,
    loading: false,
    isConfirm: false,
    isReceive: false,
    note: null,
  };
  voucherType = ConstantList.VOUCHER_TYPE.Transfer; //Điều chuyển

  convertDto = (state) => {
    return {
      assetVouchers: state?.assetVouchers?.map(i => ({
        ...i,
        usePersonId: i?.usePerson?.personId
      })),
      handoverDepartmentId: state?.handoverDepartment?.id,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      handoverPersonId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      receiverDepartmentId: state?.receiverDepartmentId,
      receiverPersonName: state?.receiverPerson?.personDisplayName,
      receiverPersonId: state?.receiverPerson?.personId,
      transferStatusIndex: state?.transferStatusIndex,
      note: state?.note
    };
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable.listInputName.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, valueText, source) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        if (variable.listInputName.note === source) {
          assetVoucher.note = valueText.target.value;
        }
        if (variable.listInputName.quantity === source) {
          assetVoucher.quantity = valueText.target.value;
          assetVoucher.asset = {
            ...(assetVoucher?.asset || {}),
            originalCost: (rowData?.asset?.unitPrice * +valueText.target.value) || 0,
          }
        }
        if (variable.listInputName.usePerson === source) {
          assetVoucher.usePerson = valueText;
        }
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true }
    );
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  validateForm = () => {
    let {
      assetVouchers,
      handoverDepartment,
      handoverPerson,
      transferStatus,
      receiverDepartment,
      issueDate,
    } = this.state;
    if (!handoverDepartment) {
      if (checkInvalidDate(issueDate)) {
        toast.warning('Ngày chứng từ không tồn tại (Thông tin phiếu).');
        return false;
      }
      if (checkMinDate(issueDate, "01/01/1900")) {
        toast.warning(`Ngày chứng từ không được nhỏ hơn ngày "01/01/1900"(Thông tin phiếu).`);
        return false;
      }
      toast.warning("Vui lòng chọn phòng ban bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!transferStatus) {
      toast.warning("Vui lòng chọn trạng thái điều chuyển(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!handoverPerson) {
      toast.warning("Vui lòng nhập người bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!receiverDepartment) {
      toast.warning("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    // if (!receiverPerson?.personId) {
    //   toast.warning("Vui lòng chọn người tiếp nhận(Thông tin phiếu).");
    //   toast.clearWaitingQueue();
    //   this.setState({ loading: false });
    //   return false;
    // }
    if (receiverDepartment?.id === handoverDepartment?.id) {
      toast.warning("Phòng ban bàn giao không được trùng với Phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn CCDC");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    let checkInvalidReceptionDate = assetVouchers?.find(x => new Date(x?.asset?.dateOfReception).getTime() > new Date(issueDate).getTime());

    if (checkInvalidReceptionDate) {
      toast.warning(`Ngày tiếp nhận của ${checkInvalidReceptionDate?.asset?.code} không được lớn hơn ngày chứng từ(Thông tin phiếu).`);
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    return true;
  }

  checkDataApi = async (id, dataState) => {
    if (id) {
      return await updateTransfer(id, dataState);
    } else {
      return await createTransfer(dataState);
    }
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let {
      t,
      handleOKEditClose = () => { },
    } = this.props;

    if (e.target.id !== "instrumentToolsTransferDialog") return;
    if (!this.validateForm()) return;

    let dataState = this.convertDto(this.state);

    setPageLoading(true);
    this.checkDataApi(id, dataState)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          handleOKEditClose();
          this.clearStateDate();
          // this.props.handleOKEditClose();
          toast.success(data?.message);
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        toast.clearWaitingQueue();
        setPageLoading(false);
      });
  };

  handleStatus = async (dataStatus) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    if (typeof dataStatus !== "number") {
      if (dataStatus?.target?.id !== "instrumentToolsTransferDialog") return;
    }
    setPageLoading(true);
    let status = this.state.isStatus ? this.state?.transferStatus?.indexOrder : dataStatus;
    let issueDate = formatDateTimeDto(this.state.issueDate, 23, 59, 59);
    let dataState = this.convertDto(this.state);
    let dataTranfer = {
      dataState: dataState,
      id: this.state?.id,
      note: this.state?.note,
      status: status,
      issueDate: issueDate,
    }

    try {
      let res = await updateStatus(dataTranfer);
      const { code } = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(t("general.success"));
        this.props.handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleRefuse = () => {
    this.setState({
      shouldOpenRefuseTransfer: true
    });
  }

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    },
      () => this.props.handleOKEditClose()
    );
  }

  handleDateChange = (date, name) => {
    if (name === "issueDate" && date) {
      this.setState({
        [name]: date,
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleSelectAsset = (items) => {
    let assetValid = {
      asset: {},
      isAssetValid: true
    }
    // eslint-disable-next-line no-unused-expressions
    items?.forEach(element => {
      let newMinDate = new Date(this.state.issueDate);
      let newDate = new Date(element?.asset?.dateOfReception);

      if (newDate.getTime() > newMinDate.getTime()) {
        assetValid.isAssetValid = false
        assetValid.asset = element
      }
      element.quantity = element.quantity || element?.asset?.quantity;
      element.assetId = element?.assetId ? element?.assetId : element.asset?.assetId;
      element.assetItemId = element?.assetItemId || element.asset?.id;
      element.asset = {
        ...element?.asset,
        unitPrice: element?.asset?.unitPrice || (element?.asset?.originalCost / element?.asset?.quantity) || 0,
      }
    });
    if (!assetValid.isAssetValid) {
      toast.warning(`Ngày tiếp nhận của tài sản ${assetValid?.asset?.asset?.name} lớn hơn ngày chứng từ`)
      return
    }
    this.setState({ assetVouchers: items }, function () {
      this.handleAssetPopupClose();
    });
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  componentWillMount() {
    let { item } = this.props;
    const roles = getTheHighestRole();
    let {
      departmentUser,
      isRoleOrgAdmin,
      isRoleAssetUser
    } = roles;

    if (item?.id) {
      this.setState({
        ...this.props.item,
        handoverPersonClone: true,
      });
    }
    else {
      let statusCreate = appConst.STATUS_TRANSFER.CHO_XAC_NHAN;
      this.setState({
        ...item,
        isRoleAssetUser,
        transferStatus: statusCreate,
        transferStatusIndex: statusCreate?.transferStatusIndex,
        handoverDepartment: isRoleOrgAdmin
          ? null
          : {
            id: departmentUser?.id,
            name: departmentUser?.name,
            text: departmentUser?.name + " - " + departmentUser?.code,
          }
      });
    }
  }
  componentDidMount() { }

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
          departmentId: handoverDepartment.id,
        },
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.info("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleReceiverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item || null,
      handoverDepartmentId: item?.id,
      assetVouchers: [],
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: null,
    });
  };
  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };
  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      {
        receiverDepartment: item || null,
        receiverDepartmentId: item?.id,
        listReceiverPerson: [],
        receiverPerson: null,
        receiverPersonId: null,
      }
    );
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  selectAssetTransferStatusStatus = (item) => {
    this.setState({
      transferStatus: item,
      transferStatusIndex: item?.indexOrder
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  clearStateDate = () => {
    this.setState({
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      transferStatus: appConst.STATUS_TRANSFER.CHO_XAC_NHAN,
      transferStatusIndex: appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder,
    })
  }

  render() {
    let { open, t, i18n } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let receiverPersonSearchObject = {
      pageIndex: 1,
      pageSize: 1000000,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state?.handoverDepartment
        ? this.state?.handoverDepartment?.id
        : null,
    };
    let {
      isView,
      shouldOpenNotificationPopup,
      loading,
      isConfirm,
      isStatus,
      isReceive,
      isRoleAssetUser
    } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="instrumentToolsTransferDialog"
          ref="form"
          onSubmit={isStatus ? this.handleStatus : this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("AssetTransfer.dialog")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <TransferScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAssetTransferStatusStatus={
                this.selectAssetTransferStatusStatus
              }
              handleHandoverDepartmentPopupOpen={
                this.handleHandoverDepartmentPopupOpen
              }
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleHandoverDepartmentPopupClose={
                this.handleHandoverDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupOpen={
                this.handleReceiverDepartmentPopupOpen
              }
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleReceiverDepartmentPopupClose={
                this.handleReceiverDepartmentPopupClose
              }
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              handleReceiverPersonPopupClose={
                this.handleReceiverPersonPopupClose
              }
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              getAssetDocument={this.getAssetDocument}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              handleSelectReceiverPerson={
                this.handleSelectReceiverPerson
              }
              handleSelectHandoverPerson={
                this.handleSelectHandoverPerson
              }
              isRoleAssetUser={isRoleAssetUser}
            />

            {this.state?.shouldOpenRefuseTransfer &&
              <RefuseTransferDialog
                t={t}
                open={this.state?.shouldOpenRefuseTransfer}
                item={this.state}
                handleChange={this.handleChange}
                handleClose={this.handleCloseRefuseTransferDialog}
                handleConfirmReason={() => this.handleStatus(appConst.STATUS_TRANSFER.HUY_XAC_NHAN.indexOrder)}

              />
            }
          </DialogContent>
          <DialogActions>
            <div className="flex flex-middle px-24 g-10">
              <Button
                variant="contained"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {isConfirm && (
                <Button
                  className="btnRefuse"
                  variant="contained"
                  onClick={this.handleRefuse}
                >
                  {t("InstrumentToolsTransfer.refuse")}
                </Button>
              )}
              {(isConfirm || isReceive) ? (
                <Button
                  className=""
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(
                    isConfirm
                      ? appConst.STATUS_TRANSFER.DA_XAC_NHAN.indexOrder
                      : appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                  )} >
                  {isConfirm && t("InstrumentToolsTransfer.confirm")}
                  {isReceive && t("general.receive")}
                </Button>
              ) : (
                <>
                  {!isView && (
                    <Button variant="contained" color="primary" type="submit">
                      {t("general.save")}
                    </Button>
                  )}

                </>
              )}
              {this.state?.transferStatusIndex === appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                && this.state.id && (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      this.props.handleQrCode(this.state, true);
                    }}
                  >
                    {t("Asset.PrintQRCode")}
                  </Button>
                )}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

InstrumentToolsTransferDialog.contextType = AppContext;
export default InstrumentToolsTransferDialog;
