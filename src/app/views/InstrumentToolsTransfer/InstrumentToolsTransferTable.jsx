import ConstantList from "../../appConfig";
import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel as exportIATToExcel,
  getCountStatus,
  exportExampleImportExcel,
} from "./InstrumentToolsTransferService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import FileSaver, { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import {appConst, LIST_ORGANIZATION, TYPE_OF, variable} from "app/appConst";
import AppContext from "app/appContext";
import ComponentTransferTable from "./ComponentTransferTable";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import VisibilityIcon from "@material-ui/icons/Visibility";
import {
  LightTooltip,
  TabPanel,
  convertFromToDate,
  convertNumberPrice,
  formatDateTypeArray,
  functionExportToExcel,
  getTheHighestRole, isSuccessfulResponse, handleThrowResponseMessage,
  formatDateArray, encodeWithVariableCharacters,
  formatTimestampToDate,
} from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import { withRouter } from "react-router-dom";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let {
    hasEditPermission,
    hasWatchPermission,
    hasCheckPermission,
    hasReceivePermission,
    hasPrintPermission,
    isRoleOrgAdmin,
    isRoleAssetUser,
    departmentUser,
  } = props;

  const isTaoMoi = item.transferStatusIndex === appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder;

  if (departmentUser?.id) {
    if (isRoleAssetUser) {
      hasReceivePermission = departmentUser.id === item.receiverDepartmentId;
    }

    if (
      departmentUser.id === item.handoverDepartmentId
      && !item.isOwner
      && isRoleAssetUser
      && isTaoMoi
    ) {
      hasPrintPermission = false;
    }
  }

  return (
    <div className="none_wrap">
      {hasEditPermission &&
        (item?.isOwner || isRoleOrgAdmin) && // là người tạo phiếu hoặc là admin
        isTaoMoi && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {(item?.isOwner || isRoleOrgAdmin) && isTaoMoi && (
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.delete)}
        >
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      )}
      {hasCheckPermission && item?.transferStatusIndex ===
        appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder && (
          <LightTooltip
            title={t("InstrumentToolsTransfer.hoverCheck")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.check)}
            >
              <CheckCircleIcon className="iconCheck" />
            </IconButton>
          </LightTooltip>
        )}
      {hasReceivePermission &&
        item?.transferStatusIndex ===
        appConst.STATUS_TRANSFER.DA_XAC_NHAN.indexOrder && (
          <LightTooltip
            title={t("InstrumentToolsTransfer.receive")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.receive)}
            >
              <ArrowForwardOutlinedIcon className="iconArrow" />
            </IconButton>
          </LightTooltip>
        )}
      {hasPrintPermission && (
        <LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {hasWatchPermission && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.watchInformation")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <VisibilityIcon size="small" className="iconEye" />
          </IconButton>
        </LightTooltip>
      )}
      {appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder ===
        item?.transferStatusIndex &&
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}
class InstrumentToolsTransferTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasWatchPermission: false,
    hasCreatePermission: false,
    statusIndex: null,
    tabValue: appConst.tabTransfer.tabAll,
    isRoleAssetManager: false,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataAssetVouchersTable: [],
    openAdvanceSearch: false,
    listStatus: [],
    listHandoverDepartment: [],
    listReceiverDepartment: [],
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    fromToData: null,
    openPrintQR: false,
    products: []
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Transfer; //Điều chuyển

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabTransfer?.tabAll === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          transferStatus: null,
          statusIndex: null,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransfer?.tabWaitConfirmation === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          transferStatus: null,
          statusIndex: appConst.listStatusTransfer[0].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransfer?.tabConfirmation === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          transferStatus: null,
          statusIndex: appConst.listStatusTransfer[1].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransfer?.tabTransferred === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          transferStatus: null,
          statusIndex: appConst.listStatusTransfer[3].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabTransfer?.tabCancelConfirm === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          transferStatus: null,
          statusIndex: appConst.listStatusTransfer[4].indexOrder,
        },
        () => this.updatePageData()
      );
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      toDate,
      fromDate,
      statusIndex,
      transferStatus,
      originalCostTo,
      originalCostFrom,
      handoverDepartment,
      receiverDepartment,
    } = this.state;

    setPageLoading(true);
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = encodeWithVariableCharacters(this.state.keyword?.trim(), appConst.INVALID_END_PARAMS);
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.statusIndex = statusIndex;
    if (transferStatus) {
      searchObject.transferStatusIndex = transferStatus.indexOrder;
    }
    if (handoverDepartment) {
      searchObject.handoverDepartmentId = handoverDepartment?.id;
    }
    if (receiverDepartment) {
      searchObject.receiverDepartmentId = receiverDepartment?.id;
    }
    if (fromDate) {
      searchObject.issueDateBottom = convertFromToDate(fromDate).fromDate;
    }
    if (toDate) {
      searchObject.issueDateTop = convertFromToDate(toDate).toDate;
    }
    if (originalCostFrom) {
      searchObject.originalCostFrom = Number(originalCostFrom);
    }
    if (originalCostTo) {
      searchObject.originalCostTo = Number(originalCostTo);
    }

    searchByPage(searchObject)
      .then(({ data }) => {
        this.setState(
          {
            itemList: [...data?.data?.content],
            totalElements: data?.data?.totalElements,
            rowDataAssetVouchersTable: [],
            selectedItem: null
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      isPrint: false,
      isView: false,
      item: null,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      isPrint: false,
      isView: false,
      item: null,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then(({ data }) => {
      this.setState({
        item: data?.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  exportExampleImportExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcel,
        {},
        t("exportToExcel.transferInstrumentsToolsImportExample")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    deleteItem(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(this.props.t("general.deleteSuccess"));
          this.handleDialogClose();
          this.updatePageData();
        } else {
          toast.warning(data?.message);
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    let { location, history } = this.props;
    let { TYPE_REQUIREMENT } = appConst;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleView({
            id: location?.state?.id
          })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          if (TYPE_REQUIREMENT.APPROVE_TRANSFER_IAT.code === location.state.type) {
            this.handleApprove(location?.state?.id);
          } else if (TYPE_REQUIREMENT.RECEIVE_TRANSFER_IAT.code === location.state.type) {
            this.handleReceive(location?.state?.id);
          }
          break;
        default:
          break;
      }
      history.push(location?.state?.path, null);
    }

    this.setState({
      statusIndex: null,
    }, this.updatePageData);
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCreatePermission,
      hasCheckPermission,
      hasReceivePermission,
      hasPrintPermission,
    } = this.state;
    const roles = getTheHighestRole();
    let {
      isRoleOrgAdmin,
      isRoleAssetUser,
      isRoleAssetManager,
    } = roles;

    hasDeletePermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasEditPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasWatchPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasCreatePermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasPrintPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasCheckPermission = isRoleOrgAdmin || isRoleAssetManager;
    hasReceivePermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;

    this.setState({
      ...roles,
      hasEditPermission,
      hasCheckPermission,
      hasWatchPermission,
      hasPrintPermission,
      hasDeletePermission,
      hasCreatePermission,
      hasReceivePermission,
    });
  };

  handleButtonAdd = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetTransfer.length; i++) {
      if (
        AssetTransfer[i].checked === null ||
        AssetTransfer[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (AssetTransfer[i].id === item.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };
  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (var i = 0; i < AssetTransfer.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };
  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      toDate,
      fromDate,
      currentUser,
      transferStatus,
      originalCostTo,
      originalCostFrom,
      handoverDepartment,
      receiverDepartment,
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.CCDC,
      };

      searchObject.keyword = this.state?.keyword?.trim();

      if (handoverDepartment) {
        searchObject.handoverDepartmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiverDepartmentId = receiverDepartment?.id;
      }
      if (transferStatus) {
        searchObject.statusIndex = transferStatus?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }
      if (originalCostFrom) {
        searchObject.originalCostFrom = Number(originalCostFrom);
      }
      if (originalCostTo) {
        searchObject.originalCostTo = Number(originalCostTo);
      }

      const res = await exportIATToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        toast.success(t("general.successExport"));
      }

      const blob = new Blob([res?.data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });

      FileSaver.saveAs(blob, "InstrumentToolsTransfer.xlsx");
    } catch (error) {
      toast.warning(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  setStateTransfer = (item) => {
    this.setState(item);
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countStatusWaitConfirmation,
          countStatusConfirmation,
          tranfered,
          cancelConfirm;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusTransfer[0]?.indexOrder === item?.status) {
            countStatusWaitConfirmation = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusTransfer[1]?.indexOrder === item?.status) {
            countStatusConfirmation = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusTransfer[3]?.indexOrder === item?.status) {
            tranfered = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusTransfer[4]?.indexOrder === item?.status) {
            cancelConfirm = this.checkCount(item?.count);
            return;
          }
        });
        this.setState(
          {
            countStatusWaitConfirmation,
            countStatusConfirmation,
            tranfered,
            cancelConfirm,
          },
          () => setPageLoading(false)
        );
      })
      .catch((err) => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  formatDateEdit = (transfer) => {
    let listAssetDocumentId = [];

    transfer.createDate = formatDateTypeArray(transfer?.createDate);
    transfer.transferStatus = {
      id: transfer?.transferStatusId,
      indexOrder: transfer?.transferStatusIndex,
    };

    transfer.handoverDepartment = {
      id: transfer?.handoverDepartmentId,
      code: transfer?.handoverDepartmentCode,
      name: transfer?.handoverDepartmentName,
      text:
        transfer?.handoverDepartmentName +
        " - " +
        transfer?.handoverDepartmentCode,
    };

    transfer.receiverDepartment = {
      id: transfer?.receiverDepartmentId,
      code: transfer?.receiverDepartmentCode,
      name: transfer?.receiverDepartmentName,
      text:
        transfer?.receiverDepartmentName +
        " - " +
        transfer?.receiverDepartmentCode,
    };

    transfer.handoverPerson = {
      personId: transfer?.handoverPersonId,
      personDisplayName: transfer?.handoverPersonName,
    };

    transfer.receiverPerson = {
      personId: transfer?.receiverPersonId,
      personDisplayName: transfer?.receiverPersonName,
    };

    transfer.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        assetId: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse,
        managementCode: item?.assetManagementCode,
        carryingAmount: item?.assetCarryingAmount,
        dateOfReception: item?.assetDateOfReception,
        madeIn: item?.assetMadeIn,
        originalCost: item?.assetOriginalCost,
        yearOfManufacture: item?.assetYearOfManufacture,
        originalCost: item?.assetOriginalCost,
        unitPrice: (item?.assetOriginalCost / item?.quantity) || 0,
      };
      item.receiverDepartment = {
        id: item?.assetUseDepartmentId,
        code: item?.assetUseDepartmentCode,
        name: item?.assetUseDepartmentName,
      };
      item.asset = data;
    });

    transfer?.documents?.map((item) => {
      listAssetDocumentId?.push(item?.id);
    });
    transfer.listAssetDocumentId = listAssetDocumentId;
    return transfer;
  };

  handleEdit = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getItemById(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetTransfer = data?.data ? data?.data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        assetTransfer.isStatus =
          assetTransfer?.transferStatus.indexOrder !==
            appConst?.listStatusTransfer[0].indexOrder
            ? true
            : false;
        assetTransfer.isStatusConfirmed =
          assetTransfer?.transferStatus.indexOrder ===
          appConst?.listStatusTransfer[1].indexOrder;
        assetTransfer.isView = false;
        this.setState({
          item: assetTransfer,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleView = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    
    setPageLoading(true);
    getItemById(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetTransfer = data?.data ? data?.data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        assetTransfer.isView = true;
        assetTransfer.isStatus = false;
        this.setState({
          item: assetTransfer,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleApprove = (id) => {
    let { t } = this.props;

    this.handleGetVoucherDetailById(id).then(() => {
      let {
        departmentUser,
        isRoleOrgAdmin,
        item,
      } = this.state;
      let assetTransfer = this.formatDateEdit(item || {});

      if (assetTransfer?.assetVouchers?.length > 0 && departmentUser) {
        let isAccept = assetTransfer.assetVouchers.find(i => i?.assetManagementDepartmentId === departmentUser?.id);
        if (!isAccept && !isRoleOrgAdmin) {
          //Phòng ban không quản lý các tài sản trong phiếu, không thể xác nhận.
          toast.info(t("AssetTransfer.message.checkAssetByDepartment"));
          return;
        };
      }
      assetTransfer.isView = true;
      assetTransfer.isConfirm = true;
      assetTransfer.isStatus = false;
      assetTransfer.isReceive = false;

      this.setState({
        item: assetTransfer,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleReceive = (id) => {
    this.handleGetVoucherDetailById(id).then(() => {
      let { item, } = this.state;
      let assetTransfer = this.formatDateEdit(item || {});

      assetTransfer.isView = true;
      assetTransfer.isConfirm = false;
      assetTransfer.isStatus = false;
      assetTransfer.isReceive = true;

      this.setState({
        item: assetTransfer,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handlePrint = (id) => {
    let { t } = this.props;
    let { setPageLoading, currentOrg } = this.context;
    const isShowPrintPreview = [
      LIST_ORGANIZATION.BVDK_MY_DUC.code,
    ].includes(currentOrg?.code);
    
    if (isShowPrintPreview) {
      return this.setState({
        item: {id},
        isPrint: true,
      })
    }
    setPageLoading(true);
    getItemById(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetTransfer = data?.data ? data?.data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        this.setState({
          item: assetTransfer,
          isPrint: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleCheckIcon = (rowData, method) => {
    switch (method) {
      case appConst.active.edit:
        this.handleEdit(rowData?.id);
        break;
      case appConst.active.delete:
        this.handleDelete(rowData?.id);
        break;
      case appConst.active.view:
        this.handleView(rowData?.id);
        break;
      case appConst.active.check:
        this.handleApprove(rowData?.id);
        break;
      case appConst.active.print:
        this.handlePrint(rowData?.id);
        break;
      case appConst.active.receive:
        this.handleReceive(rowData?.id);
        break;
      case appConst.active.qrcode:
        this.handleQrCode(rowData, true);
        break;
      default:
        alert("Call Selected Here: " + rowData.id);
        break;
    }
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusTransfer.find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  handleGetRowData = (rowData) => {
    this.setState({
      selectedItem: rowData,
      rowDataAssetVouchersTable: rowData?.assetVouchers ?? [],
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name, source) => {
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.originalCostFrom !== name &&
        variable.listInputName.originalCostTo !== name &&
        variable.listInputName.listData !== source
      ) {
        this.updatePageData();
      }
    });
  };

  convertDataDto = async (rowData) => {
    let { t } = this.props
    try {
      let { setPageLoading } = this.context;
      setPageLoading(true);
      let { data } = await getItemById(rowData?.id);
      setPageLoading(false);
      let transfer = data?.data ? data?.data : {};

      transfer.createDate = formatDateTypeArray(transfer?.createDate);
      transfer.transferStatus = {
        id: transfer?.transferStatusId,
        indexOrder: transfer?.transferStatusIndex,
      };

      transfer.handoverDepartment = {
        id: transfer?.handoverDepartmentId,
        code: transfer?.handoverDepartmentCode,
        name: transfer?.handoverDepartmentName,
      };

      transfer.receiverDepartment = {
        id: transfer?.receiverDepartmentId,
        code: transfer?.receiverDepartmentCode,
        name: transfer?.receiverDepartmentName,
        text:
          transfer?.receiverDepartmentName +
          " - " +
          transfer?.receiverDepartmentCode,
      };

      transfer.assetVouchers.map((item) => {
        let data = {
          id: item?.assetId,
          name: item?.assetName,
          code: item?.assetCode,
          yearPutIntoUse: item?.assetYearPutIntoUse || item?.assetYearPutIntoUser,
          managementCode: item?.assetManagementCode,
          carryingAmount: item?.assetCarryingAmount,
          dateOfReception: item?.assetDateOfReception,
          madeIn: item?.assetMadeIn,
          originalCost: item?.assetOriginalCost,
          yearOfManufacture: item?.assetYearOfManufacture,
        };

        item.usePerson = {
          id: item?.usePersonId,
          displayName: item?.usePersonDisplayName,
        };

        item.managementDepartment = {
          id: item?.assetManagementDepartmentId,
          code: item?.assetManagementDepartmentCode,
          name: item?.assetManagementDepartmentName,
        };

        item.receiveDepartmentName = transfer.receiverDepartmentName

        return (item.asset = data);
      });

      return transfer;
    } catch (error) {
      console.log(error)
      toast.error(t("general.error"))
    }
  };

  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }

  handleQrCode = async (newValue, isConvert) => {
    let assetTransfer = isConvert ? await this.convertDataDto(newValue) : newValue || []
    let productsUpdate = assetTransfer?.assetVouchers?.map(item => {
      item.qrInfo = `Mã TS: ${item?.asset?.code}\n` +
        `Mã QL: ${item?.asset?.managementCode}\n` +
        `Tên TS: ${item?.asset?.name}\n` +
        `Nước SX: ${item?.asset?.madeIn}\n` +
        `Năm SX: ${item?.asset?.yearOfManufacture}\n` +
        `Năm SD: ${item?.asset?.yearPutIntoUse}\n` +
        `PBSD: ${item?.receiveDepartmentName}\n`
      return item;
    })

    this.setState({
      products: productsUpdate,
      openPrintQR: true,
    })
  }

  handleGetVoucherDetailById = async (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      let res = await getItemById(id);
      const { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        let assetTransfer = data ? data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        this.setState({
          item: assetTransfer,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCheckPermission,
      hasReceivePermission,
      hasPrintPermission,
      isRoleAssetManager,
      rowDataAssetVouchersTable,
      isRoleAssetUser,
      isRoleOrgAdmin,
      departmentUser
    } = this.state;
    let TitlePage = t("InstrumentToolsTransfer.instrumentToolsTransfer");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasWatchPermission={hasWatchPermission}
            hasCheckPermission={hasCheckPermission}
            hasReceivePermission={hasReceivePermission}
            hasPrintPermission={hasPrintPermission}
            isRoleOrgAdmin={isRoleOrgAdmin}
            isRoleAssetManager={isRoleAssetManager}
            isRoleAssetUser={isRoleAssetUser}
            departmentUser={departmentUser}
            onSelect={(rowData, method) =>
              this.handleCheckIcon(rowData, method)
            }
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("AssetTransfer.issueDate"),
        field: "issueDate",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "transferStatusIndex",
        align: "left",
        minWidth: 200,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.transferStatusIndex;
          let className = "";
          if (statusIndex === appConst.listStatusTransfer[3].indexOrder) {
            className = "status status-info";
          } else if (
            statusIndex === appConst.listStatusTransfer[1].indexOrder
          ) {
            className = "status status-success";
          } else if (
            statusIndex === appConst.listStatusTransfer[4].indexOrder
          ) {
            className = "status status-error";
          } else if (
            statusIndex === appConst.listStatusTransfer[0].indexOrder
          ) {
            className = "status status-warning";
          } else {
            className = "";
          }
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPersonName",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartmentName",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        align: "left",
        minWidth: 200,
      },
    ];
    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: '50px',
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("Asset.code"),
        field: "assetCode",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "assetManagementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "assetName",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
      },
      {
        title: t("Asset.serialNumber"),
        field: "assetSerialNumber",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("Asset.model"),
        field: "assetModel",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("InstrumentToolsList.dateOfReception"),
        field: "asset.dateOfReception",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          if (typeof rowData?.assetDateOfReception === TYPE_OF.ARRAY) {
            return formatDateArray(rowData?.assetDateOfReception)
          }
          return formatTimestampToDate(rowData?.assetDateOfReception)
        }
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "assetYearOfManufacture",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
      },
      {
        title: t("InstrumentToolsType.quantity"),
        field: "quantity",
        align: "left",
        minWidth: 100,
        maxWidth: 200,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.assetOriginalCost
          ? convertNumberPrice(rowData?.assetOriginalCost)
          : 0
      },
      {
        title: t("Asset.note"),
        field: "note",
        align: "left",
        minWidth: 180,
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/transfer-vouchers",
              },
              { name: t("InstrumentToolsTransfer.instrumentToolsTransfer") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              value={appConst.tabTransfer.t}
              label={t("InstrumentToolsTransfer.tabAll")}
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>
                    {t("InstrumentToolsTransfer.tabWaitConfirmation")}
                  </span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusWaitConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InstrumentToolsTransfer.tabConfirmation")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InstrumentToolsTransfer.tabTransferred")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.tranfered || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InstrumentToolsTransfer.cancelConfirm")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.cancelConfirm || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabAll}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
            exportExampleImportExcel={this.exportExampleImportExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabWaitConfirmation}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabConfirmation}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabTransferred}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabCancelConfirm}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataAssetVouchersTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}
InstrumentToolsTransferTable.contextType = AppContext;
export default withRouter(InstrumentToolsTransferTable);
