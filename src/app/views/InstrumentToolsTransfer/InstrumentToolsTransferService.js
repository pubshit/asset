import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_voucher = ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_TRANSFER_VOUCHERS = ConstantList.API_ENPOINT + "/api/v1/instruments-and-tools/transfer-vouchers";
const API_PATH_transfer_vouchers_code = ConstantList.API_ENPOINT + "/api/asset_document/instruments-and-tools";
const API_PATH_person = ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department = ConstantList.API_ENPOINT + "/api/assetDepartment"
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_CCDC =
ConstantList.API_ENPOINT + "/api/instruments-and-tools";
const API_PATH_ASSET_DEPARTMENT =
ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_TEMPLATE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

export const searchByPage = (params) => {
  let url = API_PATH_TRANSFER_VOUCHERS + `/page`;
  let config = {
    params
  };
  return axios.get(url, config);
};

export const deleteItem = id => {
  return axios.delete(API_PATH_TRANSFER_VOUCHERS + "/" + id);
};

export const getItemById = id => {
  return axios.get(API_PATH_TRANSFER_VOUCHERS + "/" + id);
};

export const updateTransfer = (id, instrumentsTool) => {
  return axios.put(API_PATH_TRANSFER_VOUCHERS + "/" + id, instrumentsTool);
};

export const createTransfer = (instrumentsTool) => {
  return axios.post(API_PATH_TRANSFER_VOUCHERS, instrumentsTool);
};

export const UpdateEditInfo = instrumentsTool => {
  let url = API_PATH_TRANSFER_VOUCHERS + "/edit-info/" + instrumentsTool.id
  return axios.put(url, instrumentsTool);
};


export const updateStatus = (data) => {
  let config = {
    params: {
      note: data?.note,
      issueDate: data?.issueDate
    }
  };
  let url = API_PATH_TRANSFER_VOUCHERS + "/" + data?.id + "/status/" + data?.status;
  return axios.put(url, data, config);
};

//instrumentsToolsList
export const searchByPageCCDC = (searchObject) => {
  let url = API_PATH_CCDC + "/page/" + searchObject.pageIndex + "/" + searchObject.pageSize;
  return axios.post(url, searchObject);
};

//Document
export const getNewCodeInstrumentsToolDocument = () => {
  return axios.get(API_PATH_transfer_vouchers_code + "/new-code");
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: 'post',
    url: API_PATH_EXPORTTOEXCEL + "/dieu-chuyen-cong-cu-dung-cu",
    data: searchObject,
    responseType: 'blob',
  })
}

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};


export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

//status
const API_PATH = ConstantList.API_ENPOINT + "/api/asset_transfer_status"
export const getStatus = () => {
  return axios.get(API_PATH);
};

export const getListManagementReceiverDepartment = () => {
  let config = { params: {isActive:STATUS_DEPARTMENT.HOAT_DONG.code } }
  return axios.get(API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment", config);
};

export const getCountStatus = () => {
  let url = API_PATH_TRANSFER_VOUCHERS + "/count-by-statuses" ;
  return axios.get(url)
}

export const getListManagementDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage",{isActive:STATUS_DEPARTMENT.HOAT_DONG.code,...searchObject});
};

export const findDepartmentById = (id) =>{
  let config = { params: { userId: id,isActive:STATUS_DEPARTMENT.HOAT_DONG.code}};
  let url = API_PATH_ASSET_DEPARTMENT + "/main-department";
  return axios.get(url, config);
}

export const importExcelTransferIatURL = () => {
  return ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/transfer-vouchers/import";
};

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + linkFile,
    responseType: "blob",
  });
};

export const exportExampleImportExcel = () => {
  return axios({
    method: 'get',
    url: API_PATH_TEMPLATE + "/import-transferred-iat",
    // data: searchObject,
    responseType: 'blob',
  })
};
