import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  Paper,
  DialogTitle,
  DialogContent,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import {
  saveItem,
  addItem,
  updateItem,
  checkCode,
} from "./AssetNoteService";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SelectAssetPopup from "../Component/Asset/SelectAssetPopup";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class CategoryDialog extends Component {
  state = {
    id: "",
    name: "",
    code: "",
    description: "",
    type: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false, shouldOpenAssetPopup: false });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { t } = this.props;

    if (id) {
      updateItem({
        ...this.state,
      }).then(() => {
        toast.success(t("general.updateSuccess"));
        this.props.handleOKEditClose();
      });
    } else {
      saveItem({
        ...this.state,
      }).then(() => {
        toast.success(t("general.addSuccess"));
        this.props.handleOKEditClose();
      });
    }


  };

  componentWillMount() {
    let { item } = this.props;
    this.setState({ ...item });
  }

  openPopupSelectAsset = () => {
    this.setState({
      shouldOpenAssetPopup: true
    });
  }

  handleSelectAsset = (item) => {
    this.setState({
      asset: item,
    }, function () {
      this.setState({ shouldOpenAssetPopup: false })
    });
  }


  render() {
    let { id, shouldOpenAssetPopup, asset, note } = this.state;
    let { open, t, i18n } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4 className="">{id ? t("general.update") : t("Thêm mới")}</h4>
        </DialogTitle>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item md={12} sm={12} xs={12}>
                <Button
                  size='small'
                  className=" mt-12 float-right"
                  variant="contained"
                  color="primary"
                  onClick={this.openPopupSelectAsset}
                >
                  {t('general.select')}
                </Button>
                <TextValidator
                  InputProps={{ readOnly: true, }}
                  label={<span><span className="colorRed">* </span> <span> {t("maintainRequest.asset")}</span></span>}
                  className="w-80 mr-8"
                  value={asset ? asset?.code + "  " + asset?.name : ''}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />

                {shouldOpenAssetPopup && (
                  <SelectAssetPopup
                    open={shouldOpenAssetPopup}
                    handleSelect={this.handleSelectAsset}
                    selectedItem={asset !== null ? asset : {}}
                    handleClose={this.handleDialogClose}
                    t={t} i18n={i18n} />
                )}
              </Grid>
              <Grid item sm={12} xs={12} md={12} >
                <TextValidator
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      Ghi chú
                    </span>
                  }
                  className="w-80"
                  onChange={this.handleChange}
                  type="text"
                  name="note"
                  multiline
                  rows={2}
                  rowsMax={4}
                  value={note}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                style={{ marginRight: "15px" }}
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default CategoryDialog;
