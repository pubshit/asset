import { Icon, Card, Grid, Divider, Button, DialogActions, Dialog } from "@material-ui/core";
import React from "react";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { EgretProgressBar } from "egret";
import axios from "axios";
import ConstantList from "../../../app/appConfig";
import ImportExcelDialogError from "./ImportExcelDialogError";
import AppContext from "app/appContext";
import { appConst } from "app/appConst";
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import { handleThrowResponseMessage } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class ImportExcelDialog extends React.Component {
  constructor(props) {
    super(props);
    this.filesInput = React.createRef();
  }
  state = {
    dragClass: "",
    files: [],
    statusList: [],
    queProgress: 0,
    error: false,
    urlFileError: ""
  };

  handleFileSelect = event => {
    let files = event.target.files;
    let list = [];

    if (files?.[0]?.type !== appConst.TYPES_FILE.EXCEL) {
      this.filesInput.current.value = null;
      return toast.warning(t("general.invalidFileType"));
    }

    for (const iterator of files) {
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      files: [...list]
    }, () => {
      this.uploadSingleFile(this.state.files?.length - 1);
      this.filesInput.current.value = null;
    });
  };

  handleDragOver = event => {
    event.preventDefault();
    this.setState({ dragClass: "drag-shadow" });
  };

  handleDrop = event => {
    event.preventDefault();
    event.persist();

    let files = event.dataTransfer.files;
    let list = [];

    for (const iterator of files) {
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      dragClass: "",
      files: [...list]
    });

    return false;
  };

  handleDragStart = event => {
    this.setState({ dragClass: "drag-shadow" });
  };

  handleSingleRemove = index => {
    let files = [...this.state.files];
    files.splice(index, 1);
    this.setState({
      files: [...files]
    });
  };

  handleAllRemove = () => {
    this.setState({ files: [] });
  };
  async fileUpload(file) {
    const url = ConstantList.API_ENPOINT + "/api/excel/import/asset-iat";
    let formData = new FormData();
    formData.append('file', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.post(url, formData, config)
  }

  uploadSingleFile = index => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let allFiles = [...this.state.files];
    let file = this.state.files[index];
    this.fileUpload(file.file)
      .then(res => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === res?.data?.code) {
          alert(this.props.t("general.importSuccess"));
          window.location.reload();
        }
        else {
          handleThrowResponseMessage(res);
          if (res?.data?.apiSubErrors) {
            this.setState({
              urlFileError: res?.data?.apiSubErrors[0].urlGetFile,
              messageError: res?.data?.message,
              error: true
            })
          }
        }
      })
      .catch(res => {
        toast.warning(this.props.t("general.failImport"));
        setPageLoading(false);
      })

    allFiles[index] = { ...file, uploading: true, error: false };

    this.setState({
      files: [...allFiles]
    }, () => {
      this.filesInput.current.value = null;
    });
  };

  uploadAllFile = () => {
    let allFiles = [];

    this.state.files.map(item => {
      allFiles.push({
        ...item,
        uploading: true,
        error: false
      });

      return item;
    });

    this.setState({
      files: [...allFiles],
      queProgress: 35
    });
  };

  handleSingleCancel = index => {
    let allFiles = [...this.state.files];
    let file = this.state.files[index];

    allFiles[index] = { ...file, uploading: false, error: true };

    this.setState({
      files: [...allFiles]
    });
  };

  handleCancelAll = () => {
    let allFiles = [];

    this.state.files.map(item => {
      allFiles.push({
        ...item,
        uploading: false,
        error: true
      });

      return item;
    });

    this.setState({
      files: [...allFiles],
      queProgress: 0
    });
  };
  handleDownloadFileErro = () => {

  }

  handleCloseDialogError = () => {
    this.setState({
      error: false
    })
  }

  render() {
    const { t, handleClose, open } = this.props;
    let { files } = this.state;
    let isEmpty = files.length === 0;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t("general.importExcel")}</span>
        </DialogTitle>
        <DialogContent>
          <div className="upload-form m-sm-30">
            <div className="flex flex-wrap mb-24" spacing={2}>
              <div>
                <label htmlFor="upload-single-file">
                  <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">cloud_upload</Icon>
                      <span>{t('general.select_file')}</span>
                    </div>
                  </Button>
                </label>
                <input
                  ref={this.filesInput}
                  className="display-none"
                  onChange={this.handleFileSelect}
                  id="upload-single-file"
                  type="file"
                /> </div>
              <br></br>
              <div style={{ marginLeft: '10px' }} >
                <label>
                  <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                    onClick={this.props.exportExampleImportExcel}
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">download</Icon>
                      <span>Mẫu nhập Excel</span>
                    </div>
                  </Button>
                </label></div>
              <div className="px-16"></div>
            </div>
            <Card className="mb-24" elevation={2}>
              <div className="p-16">
                <Grid
                  container
                  spacing={2}
                  justify="center"
                  alignItems="center"
                  direction="row"
                >
                  <Grid item lg={4} md={4}>
                    {t('general.file_name')}
                  </Grid>
                  <Grid item lg={4} md={4}>
                    {t('general.size')}
                  </Grid>
                  <Grid item lg={4} md={4}>
                    {t('general.action')}
                  </Grid>
                </Grid>
              </div>
              <Divider></Divider>

              {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

              {files.map((item, index) => {
                let { file, uploading, error, progress } = item;
                return (
                  <div className="px-16 py-16" key={file.name}>
                    <Grid
                      container
                      spacing={2}
                      justify="center"
                      alignItems="center"
                      direction="row"
                    >
                      <Grid item lg={4} md={4} sm={12} xs={12}>
                        {file.name}
                      </Grid>
                      <Grid item lg={1} md={1} sm={12} xs={12}>
                        {(file.size / 1024 / 1024).toFixed(1)} MB
                      </Grid>
                      <Grid item lg={2} md={2} sm={12} xs={12}>
                        <EgretProgressBar value={progress}></EgretProgressBar>
                      </Grid>
                      <Grid item lg={1} md={1} sm={12} xs={12}>
                        {error && <Icon color="error">error</Icon>}
                        {/* {uploading && <Icon className="text-green">done</Icon>} */}
                      </Grid>
                      <Grid item lg={4} md={4} sm={12} xs={12}>
                        <div className="flex">
                          <Button
                            variant="contained"
                            color="primary"
                            disabled={uploading}
                            onClick={() => this.uploadSingleFile(index)}
                          >
                            {t('general.upload')}
                          </Button>
                          <Button
                            className="mx-8"
                            variant="contained"
                            disabled={!uploading}
                            color="secondary"
                            onClick={() => this.handleSingleCancel(index)}
                          >
                            {t('general.cancel')}
                          </Button>
                          <Button
                            variant="contained"
                            className="bg-error mr-8"
                            onClick={() => this.handleSingleRemove(index)}
                          >
                            {t('general.remove')}
                          </Button>
                        </div>
                      </Grid>
                    </Grid>
                    {this.state.error && (
                      <ImportExcelDialogError
                        t={t}
                        open={this.state.error}
                        file={this.state}
                        fileName={file.name}
                        handleClose={this.handleCloseDialogError}
                      />
                    )}
                  </div>
                );
              })}

            </Card>
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-36 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}>{t('general.close')}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
ImportExcelDialog.contextType = AppContext;
export default ImportExcelDialog;