import {Breadcrumb, ConfirmationDialog} from "egret";
import React, {Component, useContext} from "react";
import Helmet from "react-helmet";
import {useTranslation} from "react-i18next";
import MaterialTable, {MTableToolbar} from "material-table";
import {
    Button,
    ButtonGroup,
    Card,
    CardContent,
    ClickAwayListener,
    Collapse,
    FormControl,
    Grid,
    Icon,
    IconButton,
    Input,
    InputAdornment,
    MenuItem,
    MenuList,
    Paper,
    Popper,
    TablePagination,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import AppContext from "app/appContext";
import InstrumentToolsListEditorDialog from "./InstrumentToolsListEditorDialog";
import {
    deleteItem,
    exportExampleImportExcel,
    exportExcelIatBookService,
    exportExcelIatFollowService,
    exportToExcel,
    getItemById,
    searchByPageCCDC
} from "./InstrumentToolsListService";
import moment from "moment";
import {toast} from "react-toastify";
import {
    appConst,
    DEFAULT_TOOLTIPS_PROPS,
    LIST_ORGANIZATION,
    OPTIONS_EXCEL_QR,
    PRINT_TEMPLATE_MODEL,
    STATUS_STORE
} from "../../appConst";
import FileSaver from "file-saver";
import ImportExcelDialog from "./ImportExcelDialog";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import {getByRoot} from "../InstrumentAndToolsType/InstrumentAndToolsTypeService";
import {searchByPage as statusSearchByPage} from "../AssetStatus/AssetStatusService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete"
import {createFilterOptions} from "@material-ui/lab/Autocomplete";
import {getListOrgManagementDepartment, getTreeUser} from "../Asset/AssetService";
import {searchReceiverDepartment} from "../AssetAllocation/AssetAllocationService";
import {
    checkObject,
    convertFromToDate,
    convertNumberPrice,
    defaultPaginationProps,
    filterOptions,
    formatDateDto,
    formatDateDtoMore,
    functionExportToExcel,
    getAssetStatus,
    getRole,
    getTheHighestRole,
    getUserInformation,
    handleGetTemplatesByModel,
    isValidDate,
} from "../../appFunction";
import {TreeItem, TreeView} from "@material-ui/lab";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {searchByPage as storesSearchByPage} from "../Store/StoreService";
import {searchByPage as searchByPageBidding} from "../bidding-list/BiddingListService";
import AssetsQRPrintAccording from "../Asset/ComponentPopups/AssetsQRPrintAccording";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import {LightTooltip, NumberFormatCustom} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { currentOrg } = useContext(AppContext);
  const { t } = useTranslation();
  const { isRoleOrgAdmin, isRoleAssetUser, isRoleUser, isRoleAssetManager } = getTheHighestRole();
  const { departmentUser } = getUserInformation();
  const item = props.item;
  const status = item?.statusIndexOrder;
  const hasDeletePermission = props.hasDeletePermission;
  const isUsing = getAssetStatus(status).isUsing;
  const isDepartmentInUse = item?.useDepartment?.id !== departmentUser?.id;
  let hasEditPermission = props.hasEditPermission &&
    ![
      appConst.listStatusInstrumentTools.DA_THANH_LY.indexOrder,
      appConst.listStatusInstrumentTools.CHUYEN_DI.indexOrder,
    ].includes(status);
  let isAllowView = [
    appConst.listStatusInstrumentTools.DANG_SU_DUNG.indexOrder,
    appConst.listStatusInstrumentTools.DA_THANH_LY.indexOrder,
    appConst.listStatusInstrumentTools.CHUYEN_DI.indexOrder,
  ].includes(item?.statusIndexOrder);
  let allowdAUEditAssetUsing = isUsing && isRoleAssetUser;
  let notAllowdAMEditAssetUsing = isUsing && !isDepartmentInUse && isRoleAssetManager;

  if (allowdAUEditAssetUsing) {
    hasEditPermission = true
  }
  if (notAllowdAMEditAssetUsing) {
    hasEditPermission = false
  }
  if (isRoleOrgAdmin && currentOrg?.code === LIST_ORGANIZATION.BV_VAN_DINH.code) { //temporary
    hasEditPermission = true;
  }
  return (
    <div className="none_wrap">
      {(hasEditPermission) && (
        <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {(isAllowView) && (
        <LightTooltip title={t("general.viewIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
            <Icon fontSize="small" color="primary">
              visibility
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {hasDeletePermission &&
        (appConst.listStatusAssets.LUU_MOI.indexOrder === item?.statusIndexOrder) && (
          <LightTooltip title={t("general.deleteIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
    </div>
  );
}

class InstrumentToolsList extends Component {
  constructor(props) {
    super(props);
    this.anchorRef = React.createRef();
    this.anchorRefQR = React.createRef();
  }

  state = {
    keyword: "",
    status: [],
    assetGroup: null,
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    Asset: [],
    item: {},
    managementDepartment: null,
    departmentId: "",
    departmentTreeView: {},
    shouldOpenEditorDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    total: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenAssetHistory: false,
    isAddAsset: false,
    isTemporary: null,
    listManageDepartment: [],
    isSearch: false,
    currentUser: null,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasViewPermission: false,
    useDepartment: null,
    yearPutIntoUse: null,
    childrenOrg: null,
    searchDepartmentObject: {
      pageIndex: 0,
      pageSize: 1000000,
      shouldGetChildren: true,
      checkPermissionUserDepartment: getRole().isRoleAssetUser ? false : true
    },
    isButtonAllocation: true,
    originalCost: null,
    isMedicalEquipment: false,
    treeList: null,
    usePersonId: null,
    isDangSuDung: false,
    allowdEditAssetUsing: false,
    isViewRoleAssetUser: false,
    shouldOpenPrintIatBook: false,
    isPrintIatFollow: false,
    isPrintIatBook: false,
    itemPrintIatBook: null,
    fromDate: null,
    toDate: null,
    useDepartmentFilter: null,
    decisionCode: null,
    textSearchAdvance: "",
    openPrintQRToggle: false,
    openPrintQR: false,
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { useDepartment, yearPutIntoUse, originalCost, childrenOrg, usePersonId, dateOfReceptionBottom, dateOfReceptionTop } = this.state;
    let searchObject = {};
    searchObject.keyword = this.state?.keyword?.trim();
    searchObject.managementDepartmentId = this.state.managementDepartment?.id
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroupId = this.state?.assetGroup?.id
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.storeId = this.state.store?.id;
    searchObject.decisionCode = this.state?.decisionCode?.decisionCode;
    if (useDepartment) searchObject.useDepartmentId = useDepartment.id;
    if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
    if (originalCost) searchObject.originalCost = originalCost;
    if (childrenOrg) searchObject.orgId = childrenOrg.id;
    if (dateOfReceptionBottom) searchObject.dateOfReceptionBottom = formatDateDtoMore(dateOfReceptionBottom, "start");
    if (dateOfReceptionTop) searchObject.dateOfReceptionTop = formatDateDtoMore(dateOfReceptionTop);

    searchObject.usePersonId = usePersonId
    function compare(a, b) {
      if (a?.code > b?.code) return -1;
      if (a?.code < b?.code) return 1;
      return 0;
    }
    setPageLoading(true);
    searchByPageCCDC(searchObject)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          itemList: [...data.data?.sort(compare)],
          totalElements: data.total,
        });
      })
      .catch(() => {
        toast.error(this.props.t("toastr.error"));
        setPageLoading(false);
      })
  };

  convertSearchParams = (searchObject) => {
      return {
        pageIndex: searchObject?.pageIndex,
        pageSize: searchObject?.pageSize,
        keyword: searchObject?.keyword,
        departmentId: searchObject?.departmentId,
        managementDepartmentId: searchObject?.managementDepartmentId,
        isTemporary: searchObject?.isTemporary,
        isManageAccountant: searchObject?.isManageAccountant,
        status: searchObject?.status?.toString(),
        assetGroupId: searchObject?.assetGroupId,
        useDepartmentId: searchObject?.useDepartmentId,
        yearPutIntoUse: searchObject?.yearPutIntoUse,
        originalCost: searchObject?.originalCost,
        orgId: searchObject?.orgId,
        dateOfReceptionTop: searchObject?.dateOfReceptionTop,
        dateOfReceptionBottom: searchObject?.dateOfReceptionBottom,
        issueDateTop: searchObject?.issueDateTop,
        statusIndexOrders: searchObject?.statusIndexOrders?.toString(),
        decisionCode: searchObject?.decisionCode,
    };
  }

  search = () => {
    this.setPage(0);
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search()
  }

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {
    });
  };

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser();
    if (getRole().isRoleAssetUser) {
      this.getTreeView();
    }
    this.handleGetDocumentTemplates();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission,
    } = this.state;
    let roles = getTheHighestRole();
    let { isRoleAssetManager, isRoleAssetUser, isRoleOrgAdmin, isRoleUser } = roles;
    let isAdminOrAm = isRoleOrgAdmin || isRoleAssetManager;
    hasDeletePermission = isAdminOrAm;
    hasEditPermission = isAdminOrAm;
    hasViewPermission = isAdminOrAm || isRoleAssetUser || isRoleUser;

    this.setState({
      ...roles,
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission,
    });
  };

  handleAddItem = () => {
    let item = {};
    item.isManageAccountant = true;
    item.isCheck = false;
    item.isTemporary = false;
    item.voided = false;
    item.allocationFor = 1;
    this.setState({
      item: item,
      isQRCode: false,
      shouldOpenEditorDialog: true,
      isButtonAllocation: true,
      isAddAsset: true,
    });
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenAssetHistory: false,
      shouldOpenPrintIatBook: false,
      item: {},
      isDangSuDung: false,
      isView: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenAssetHistory: false,
    });
    this.updatePageData();
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;

    setPageLoading(true);
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (data) {
          setPageLoading(false);
          toast.success(data?.message);
          if ((this.state?.itemList.length - 1) === 0 && this.state.page !== 0) {
            this.setState({
              page: (this.state.page - 1)
            },
              () => this.updatePageData()
            )
          } else {
            this.updatePageData();
          }
          this.handleDialogClose();
        } else {
          toast.warning(this.props.t("InstrumentToolsList.notification.use"));
        }
      })
      .catch(() => {
        toast.error(this.props.t("toastr.error"));
        setPageLoading(false);
      })
  };

  setPage = (page) => {
    this.setState({ page: page }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  exportToExcel = () => {
    let { setPageLoading } = this.context;
    let { useDepartment, yearPutIntoUse, originalCost, childrenOrg, dateOfReceptionBottom, dateOfReceptionTop } = this.state;
    let searchObject = {};
    searchObject.keyword = this.state?.keyword?.trim();
    searchObject.managementDepartmentId = this.state.managementDepartment?.id
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroupId = this.state?.assetGroup?.id
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.decisionCode = this.state?.decisionCode?.decisionCode;
    if (useDepartment) searchObject.useDepartmentId = useDepartment.id;
    if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
    if (originalCost) searchObject.originalCost = originalCost;
    if (childrenOrg) searchObject.orgId = childrenOrg.id;
    if (isValidDate(dateOfReceptionBottom)) searchObject.dateOfReceptionBottom = formatDateDtoMore(dateOfReceptionBottom, "start");
    if (isValidDate(dateOfReceptionTop)) searchObject.dateOfReceptionTop = formatDateDtoMore(dateOfReceptionTop);

    setPageLoading(true);

    let params = this.convertSearchParams(searchObject)
    exportToExcel(params)
      .then((res) => {
        setPageLoading(false);
        toast.info(this.props.t("general.successExport"));
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, "CCDC.xlsx");
      })
      .catch((err) => {
        setPageLoading(false);
        toast.warning(this.props.t("general.failExport"));
      });
  };

  exportExampleImportExcel = () => {
    exportExampleImportExcel()
      .then((res) => {
        toast.info(this.props.t("general.successExport"));
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, "CCDC.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  printIatBook = async () => {
    if (!this.state?.useDepartmentFilter) {
      toast.warning("Vui lòng chọn phòng ban sử dụng")
      return
    }
    
    this.setState({
      shouldOpenPrintIatBook: true,
      item: {
        externalParams: {
          assetGroupId: this.state?.assetGroupFilter?.id,
          departmentId: this.state?.useDepartmentFilter?.id,
          fromDate: this.state?.fromDate ? formatDateDto(this.state?.fromDate) : "",
          toDate: this.state?.toDate ? formatDateDto(this.state?.toDate) : ""
        },
      }
    })
  }

  exportExcelIatBook = () => {
    if (!this.state?.useDepartmentFilter?.id && this.state?.isPrintIatFollow) {
      toast.warning("Vui lòng chọn phòng ban sử dụng")
      return
    }
    let { setPageLoading } = this.context;
    let {year} = this.state;
    try {
      let searchObject = {};
      searchObject.assetGroupId = this.state?.assetGroupFilter?.id;
      searchObject.departmentId = this.state?.useDepartmentFilter?.id;
      searchObject.fromDate = this.state?.fromDate ? convertFromToDate(this.state?.fromDate).fromDate : ""
      searchObject.toDate = this.state?.toDate ? convertFromToDate(this.state?.toDate).toDate : "";
      searchObject.templateId = this.state.listDocumentTemplates[0]?.id;
      
      if (this.state?.isPrintIatBook) {
        searchObject.yearOfRelease = year?.getFullYear();
      }
      
      let fileName = this.state?.isPrintIatBook ? "Sổ CCDC" : "Số theo dõi.xlsx";
      let funcExport = this.state?.isPrintIatBook ? exportExcelIatBookService : exportExcelIatFollowService;
      functionExportToExcel(funcExport, searchObject, fileName, setPageLoading);
    } catch (e) {
      toast.error("general.error");
    }
  }

  selectManagementDepartment = (item) => {
    this.setState({ managementDepartment: item }, () => this.search());
  };

  selectInstrumentToolsType = (item) => {
    this.setState({ assetGroup: item }, () => this.search());
  };

  selectUseDepartment = (selected) => {
    this.setState({ useDepartment: selected }, () => this.search());
  }

  selectIatGroupIatBook = (assetGroupSelected) => {
    this.setState({ assetGroupFilter: assetGroupSelected });
  };

  selectUseDepartmentIatBook = (selected) => {
    this.setState({ useDepartmentFilter: selected });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    })
  }

  selectStatus = (statusSelected) => {
    let listStatusId = []
    statusSelected.forEach(item => {
      listStatusId.push(item?.id)
    })
    this.setState({
      itemStatus: statusSelected,
      status: listStatusId
    }
      , () => this.search());
  };

  selectDecisionCode = (selected) => {
    this.setState({ decisionCode: selected }, () => {
      this.search();
    });
  };

  handleChange = (e, name) => {
    if (["dateOfReceptionBottom", "dateOfReceptionTop"].includes(name)) {
      this.setState({ [name]: e }, () => {
        if (e === null || isValidDate(e)) {
          this.search();
        }
      })
      return;
    }
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  getTreeView = async () => {
    try {
      const { data } = await getTreeUser();
      if (data?.code === appConst.CODE.SUCCESS) {
        const newData = data?.data || [];
        const treeList = newData.map(item => {
          const department = {
            id: "root",
            text: item.departmentName,
            children: item.users.map(user => ({
              id: user.personId,
              text: user.displayName,
              // You can include other user properties here if needed
            })),
          };
          return department;
        });
        this.setState({ treeList });
      }
    } catch (error) {
      // Handle any errors that occur during the API request
      console.error("Error fetching tree data:", error);
    }
  }

  renderTree = (nodes) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.text}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => this.renderTree(node))
        : null}
    </TreeItem>
  )

  getAssetBydepartment = (event, value) => {
    this.setState({ usePersonId: value !== 'root' ? value : null }, this.updatePageData);
  }

  getItemForMapping = async (id) => {
    try {
      const data = await getItemById(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        return data?.data?.data || {};
      }
      return {};
    } catch (error) {
      return {};
    }
  }

  handleMapDataAsset = async (event, itemSelected, item) => {
    const value = await this.getItemForMapping(itemSelected?.id);

    let assetSourcesUpdated = value?.assetSources?.map((asset) => {
      asset.assetSource = {
        code: asset?.assetSource?.code,
        id: asset?.assetSource?.id,
        name: asset?.assetSource?.name
      }
      return asset
    })
    let attributesUpdated = value?.attributes?.map((attribute) => {
      attribute.attribute = attribute.attribute?.id
        ? attribute.attribute
        : {
          code: attribute?.attributeCode || item?.attribute?.code,
          id: attribute?.attributeId || item?.attribute?.id,
          name: attribute?.attributeName || item?.attribute?.name,
        }
      attribute.name = item?.attribute?.name;

      return attribute
    })

    if (item?.product?.id && item?.decisionCode?.decisionCode) {
      value.decisionCode = item?.decisionCode;
      value.product = item.product;
    };

    const valueFormatted = {
      ...value,
      assetGroup: value?.assetGroup?.id
        ? {
          id: value?.assetGroup?.id,
          name: value?.assetGroup?.name
        }
        : null,
      manufacturer: value?.manufacturer?.id
        ? {
          name: value?.manufacturer?.name,
          id: value?.manufacturer?.id,
          code: value?.manufacturer?.code
        }
        : null,
      unit: value?.unit?.id
        ? {
          code: value?.unit?.code,
          id: value?.unit?.id,
          name: value?.unit?.name
        }
        : null,
      contract: value?.contract?.id
        ? {
          contractCode: value?.contract?.code,
          contractDate: value?.contract?.date,
          id: value?.contract?.id,
          contractName: value?.contract?.name
        }
        : null,
      store: value?.store?.id
        ? {
          code: value?.store?.code,
          id: value?.store?.id,
          name: value?.store?.name
        }
        : null,
      managementDepartment: value?.managementDepartment?.id
        ? {
          code: value?.managementDepartment?.code,
          id: value?.managementDepartment?.id,
          name: value?.managementDepartment?.name
        }
        : null,
      medicalEquipment: value?.medicalEquipment?.id
        ? {
          code: value?.medicalEquipment?.code,
          id: value?.medicalEquipment?.id,
          name: value?.medicalEquipment?.name
        }
        : null,
      product: value?.product?.id
        ? {
          name: value?.product?.name,
          id: value?.product?.id,
        }
        : null,
      shoppingForm: value?.shoppingForm?.id
        ? {
          name: value?.shoppingForm?.name,
          id: value?.shoppingForm?.id,
          code: value?.shoppingForm?.code,
        }
        : null,
      supplyUnit: value?.supplyUnit?.id
        ? {
          id: value?.supplyUnit?.id,
          name: value?.supplyUnit?.name
        }
        : null,
      assetSources: assetSourcesUpdated,
      attributes: attributesUpdated
    }
    const newData = {
      ...valueFormatted,
      id: null,
      serialNumber: item?.serialNumber || "",
      depreciationRate: value?.depreciationRate,
      useDepartment: "",
      allocationFor: "",
      serialNumbers: item?.serialNumbers || "",
    };
    for (const prop of [
      "status",
      "code",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      "accumulatedDepreciationAmount",
      "lastYearOfDepreciation",
      "useDepartment",
      "usePerson",
      "store",
      "useDepartmentId",
      "useDepartmentName",
      "useDepartmentCode",
      "serialNumber",
      "serialNumbers",
      "documents",
      "dayStartedUsing",
    ]) {
      delete newData[prop];
    }

    this.setState({ item: newData });
  };

  handleEdit = (rowData, method) => {
    let { setPageLoading, currentOrg } = this.context;
    const { isRoleOrgAdmin } = getTheHighestRole();
    const { departmentUser } = getUserInformation();
    setPageLoading(true);
    getItemById(rowData.id).then(({ data }) => {
      setPageLoading(false);
      let listAssetDocumentId = [];
      let itemInstrumentTools = data?.data ? data?.data : null;
      let checkStatus = itemInstrumentTools?.status?.indexOrder
        === appConst.listStatusInstrumentTools.LUU_MOI.indexOrder;
      let isDangSuDung = itemInstrumentTools?.status?.indexOrder ===
        appConst.listStatusInstrumentTools.DANG_SU_DUNG.indexOrder;
      let isDaThanhLy = itemInstrumentTools?.status?.indexOrder ===
        appConst.listStatusInstrumentTools.DA_THANH_LY.indexOrder;
      let allowdEditAssetUsing = isDangSuDung && (
        getRole().isRoleAssetUser
        || itemInstrumentTools?.useDepartment?.id === departmentUser?.id
      );
      let isView = isDangSuDung || isDaThanhLy;

      let isMedicalEquipment = !checkObject(itemInstrumentTools.riskClassification)
        || itemInstrumentTools?.medicalEquipment
        || itemInstrumentTools?.circulationNumber;

      if (itemInstrumentTools?.qrcode) {
        itemInstrumentTools.isCreateQRCode = true;
      }
      itemInstrumentTools = {
        ...itemInstrumentTools,
        isEdit: true,
        assetGroupId: itemInstrumentTools.assetGroup?.id,
        supplyUnitId: itemInstrumentTools.supplyUnit?.id,
        managementDepartmentId:
          itemInstrumentTools.managementDepartment?.id,
        manufacturerId: itemInstrumentTools.manufacturer?.id,
        unitId: itemInstrumentTools.unit?.id,
        shoppingFormId: itemInstrumentTools.shoppingForm?.id,
        medicalEquipmentId:
          itemInstrumentTools.medicalEquipment?.id,
        storeId: itemInstrumentTools.store?.id,
        productId: itemInstrumentTools.product?.id,
        dayStartedUsing: itemInstrumentTools?.dateOfReception,
        originalCost: Math.ceil(
          itemInstrumentTools?.originalCost
        ),
        carryingAmount: itemInstrumentTools?.carryingAmount,
      };
      itemInstrumentTools?.documents?.map((item) => {
        return listAssetDocumentId.push(item?.id);
      });
      itemInstrumentTools.listAssetDocumentId =
        listAssetDocumentId;

      if (currentOrg?.code === LIST_ORGANIZATION.BV_VAN_DINH.code) {
        isView = (isDangSuDung || isDaThanhLy) && !isRoleOrgAdmin;
      }

      this.setState({
        item: itemInstrumentTools,
        shouldOpenEditorDialog: true,
        isButtonAllocation: checkStatus,
        isAddAsset: false,
        isQRCode: true,
        isView: isView,
        isDangSuDung: isDangSuDung,
        isMedicalEquipment: isMedicalEquipment,
        allowdEditAssetUsing: allowdEditAssetUsing,
        isViewRoleAssetUser: false
      });
      if (method === appConst.active.view) {
        this.setState({ isView: true, isDangSuDung: false, isViewRoleAssetUser: true, });
      }
    })
      .catch(() => {
        toast.error(this.props.t("toastr.error"));
        setPageLoading(false);
      });
  }

  handlePrintIatFollow = () => {
    let { isPrintIatFollow } = this.state;
    let { isRoleAssetUser, departmentUser } = getTheHighestRole();
    this.setState({
      isPrintIatFollow: !isPrintIatFollow,
      useDepartmentFilter: isRoleAssetUser ? departmentUser : null,
      assetGroupFilter: null,
      isPrintIatBook: false,
      textSearchAdvance: "Sổ theo dõi tại phòng ban"
    });
  }

  handlePrintIatBook = () => {
    let { isPrintIatBook } = this.state;
    let { isRoleAssetUser, departmentUser } = getTheHighestRole();
    this.setState({
      isPrintIatBook: !isPrintIatBook,
      useDepartmentFilter: isRoleAssetUser ? departmentUser : null,
      assetGroupFilter: null,
      isPrintIatFollow: false,
      textSearchAdvance: "Sổ CCDC",
    });
  }

  handleSelectStore = (store) => {
    this.setState({ store }, () => {
      this.search();
    });
  };

  handleSetListStore = (listStore) => {
    this.setState({ listStore });
  };

  handleToggleOpenPrintQR = () => {
    const { openPrintQRToggle } = this.state

    this.setState((prevState) => ({
      openPrintQRToggle: !openPrintQRToggle,
    }));
  }

  handleMenuItemPrintQrClick = (e, type) => {
    this.handleQrCode(type)
  }

  convertItemsList = (data = []) => {
    let newArr = [];
    data.forEach(item => {
      item.useDepartment = item?.useDepartmentId
        ? {
          name: item?.useDepartmentName,
          id: item?.useDepartmentId,
          code: item?.useDepartmentCode,
        }
        : null;
      newArr.push(item);
      if (item?.quantity > 1) {
        const largeArray = Array.from({ length: item?.quantity - 1 }, () => ({ ...item }));
        newArr.push(...largeArray);
      }
    })
    return newArr;
  }

  handleQrCode = (type) => {
    let { itemList } = this.state;
    this.setState({
      openPrintQR: true,
      products: this.convertItemsList(itemList),
      typeSuffering: type
    })
  }

  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    });
  };
  
  handleGetDocumentTemplates = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let data = await handleGetTemplatesByModel(
        PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.LIST.ASSET_BOOK_OF_USE_DEPARTMENT
      );
      if (data) {
        this.setState({
          listDocumentTemplates: data,
        })
      }
    } catch (e) {
      console.error(e)
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    const { t, i18n } = this.props;
    const {
      hasDeletePermission,
      hasEditPermission,
      hasViewPermission,
      rowsPerPage,
      page,
      totalElements,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenImportExcelDialog,
      isButtonAllocation,
      isSearch,
      searchDepartmentObject,
      managementDepartment,
      useDepartment,
      assetGroup,
      itemStatus,
      originalCost,
      yearPutIntoUse,
      isQRCode,
      isRoleAssetUser,
      isRoleAdmin,
      isViewRoleAssetUser,
      treeList,
      shouldOpenPrintIatBook,
      isPrintIatFollow,
      textSearchAdvance,
      isPrintIatBook,
      itemPrintIatBook,
      fromDate,
      toDate,
      useDepartmentFilter,
      assetGroupFilter,
      listStore,
      store,
      dateOfReceptionBottom,
      dateOfReceptionTop,
      decisionCode,
      openPrintQRToggle,
      openPrintQR,
      typeSuffering,
      products,
    } = this.state;
    const {currentOrg} = this.context;

    let { departmentUser, isRoleAssetManager } = getTheHighestRole();
    let stt = page * rowsPerPage;
    let searchObjectInstrumentToolsType = { assetClass: appConst.assetClass.CCDC, pageIndex: 1, pageSize: 1000000 };
    let searchObject = { assetClass: appConst.assetClass.CCDC, pageIndex: 1, pageSize: 1000000 };
    const searchObjectStore = {
      pageIndex: 1,
      pageSize: 1000000,
      managementDepartmentId: departmentUser?.id,
      isActive: STATUS_STORE.HOAT_DONG.code,
    };
    let searchObjectBidding = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      status: appConst.STATUS_BIDDING.OPEN.code
    };
    let filterAutocomplete = createFilterOptions()
    let columns = [
      {
        title: t("InstrumentToolsList.action"),
        maxWidth: 80,
        minWidth: 80,
        sorting: false,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasViewPermission={hasViewPermission}
            isRoleAssetUser={getRole().isRoleAssetUser}
            allowdEditAssetUsing={this.state?.allowdEditAssetUsing}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.view) {
                this.handleEdit(rowData, appConst.active.view);
              }
              else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InstrumentToolsList.stt"),
        maxWidth: 50,
        sorting: false,
        align: "left",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => stt + (rowData?.tableData?.id + 1),
      },
      {
        title: t("InstrumentToolsList.code"),
        field: "code",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.dateOfReception"),
        field: "dateOfReception",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.dateOfReception
            ? moment(rowData.dateOfReception).format('DD/MM/YYYY')
            : null
      },
      {
        title: t("InstrumentToolsList.managementCode"),
        field: "managementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.name"),
        field: "name",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("Asset.decisionCode"),
        field: "decisionCode",
        align: "left",
        sorting: false,
        minWidth: "150px",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InstrumentToolsList.symbolCode"),
        field: "symbolCode",
        align: "center",
        sorting: false,
        minWidth: "150px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.dateOfUse"),
        field: "dayStartedUsing",
        align: "center",
        sorting: false,
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          rowData?.dayStartedUsing
            ? moment(rowData?.dayStartedUsing).format("DD/MM/YYYY")
            : null
        }
      },
      {
        title: t("InstrumentToolsList.serialNumber"),
        field: "serialNumber",
        align: "center",
        sorting: false,
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.model"),
        field: "model",
        align: "center",
        sorting: false,
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.lotNumber"),
        field: "lotNumber",
        align: "center",
        sorting: false,
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.invoiceDate"),
        field: "ngayHoaDon",
        align: "center",
        sorting: false,
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.ngayHoaDon
            ? moment(rowData?.ngayHoaDon).format("DD/MM/YYYY")
            : null
      },
      {
        title: t("InstrumentToolsList.manufacturer"),
        field: "manufacturerName",
        align: "center",
        sorting: false,
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "center",
        sorting: false,
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.madeIn"),
        field: "madeIn",
        align: "center",
        sorting: false,
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.suppiler"),
        field: "supplierName",
        align: "left",
        sorting: false,
        minWidth: "220px",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t('Asset.unitPrice'),
        field: 'unitPrice',
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.unitPrice
          ? convertNumberPrice(rowData.unitPrice)
          : 0,
      },
      {
        title: t('InstrumentToolsList.amount'),
        field: 'originalCost',
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.originalCost
          ? convertNumberPrice(rowData.originalCost)
          : 0,
      },
      {
        title: t("InstrumentToolsList.quantity"),
        field: "quantity",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.status"),
        field: "statusName",
        align: "left",
        minWidth: 150,
        cellStyle: {
        },
      },
      {
        title: t("InstrumentToolsList.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        minWidth: 180,
        cellStyle: {
        },
      },
      {
        title: t("InventoryCountVoucher.store"),
        field: "storeName",
        align: "left",
        minWidth: 180,
      },
    ];

    let TitlePage = t("InstrumentToolsList.instrumentToolsList");
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.toolsManagement"), path: "instruments-and-tools/page" },
              { name: TitlePage },
            ]}
          />
        </div>
        <ValidatorForm onSubmit={() => { }}>
          <Grid
            container
            spacing={2}
            alignItems="flex-end"
            justifyContent="space-between"
          >
            <Grid item md={7} xs={12} className="pl-0">
              {hasEditPermission && (
                <Button
                  className="mt-10 align-bottom mr-16"
                  variant="contained"
                  color="primary"
                  onClick={this.handleAddItem}
                >
                  {t("general.add")}
                </Button>
              )}
              <Button
                className="align-bottom mr-16 mt-16"
                variant="contained"
                color="primary"
                onClick={this.exportToExcel}
              >
                {t("general.exportToExcel")}
              </Button>
              {hasEditPermission && (
                <Button
                  className="align-bottom mr-16 mt-16"
                  variant="contained"
                  color="primary"
                  onClick={this.importExcel}
                >
                  {t("general.importExcel")}
                </Button>
              )}
              <ButtonGroup
                className="align-bottom mr-12 mt-12"
                aria-label="button group"
                variant="contained"
                ref={this.anchorRefQR}
              >
                <Button
                  aria-controls={openPrintQRToggle ? "split-button-menu" : undefined}
                  aria-expanded={openPrintQRToggle ? "true" : undefined}
                  aria-label="select merge strategy"
                  aria-haspopup="menu"
                  style={{ paddingRight: "3px" }}
                  variant="contained"
                  color="primary"
                  onClick={this.handleToggleOpenPrintQR}
                >
                  {t("Asset.PrintQRCode")}
                  <ArrowDropDownIcon />
                </Button>
              </ButtonGroup>
              <Popper
                open={openPrintQRToggle}
                anchorEl={this.anchorRefQR.current}
                role={undefined}
                transition
                disablePortal
                style={{ zIndex: 999 }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={() => { }}>
                    <MenuList id="split-button-menu" autoFocusItem>
                      {OPTIONS_EXCEL_QR.map((option, index) => (
                        <MenuItem
                          key={option}
                          onClick={(event) =>
                            this.handleMenuItemPrintQrClick(event, option.code)
                          }
                        >
                          {option.name}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>

              </Popper>
              <Button
                className="mr-16"
                variant="contained"
                color="primary"
                style={{ paddingRight: "3px" }}
                onClick={() => this.handlePrintIatFollow()}
              >
                Sổ theo dõi tại PB
                <ArrowDropDownIcon />
              </Button>
              <Button
                className="mr-16"
                variant="contained"
                color="primary"
                style={{ paddingRight: "3px" }}
                onClick={() => this.handlePrintIatBook()}
              >
                {t("InstrumentToolsList.IatBook")}
                <ArrowDropDownIcon />
              </Button>
              <Button
                className="align-bottom mr-16 mt-16"
                variant="contained"
                color="primary"
                style={{ paddingRight: "3px" }}
                onClick={() => this.setState({ isSearch: !isSearch })}
              >
                {t("Tìm kiếm nâng cao")}
                <ArrowDropDownIcon />
              </Button>
            </Grid>

            <Grid item md={5} sm={12} xs={12}>
              <FormControl fullWidth>
                <Input
                  className="search_box w-100"
                  onChange={this.handleTextChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  placeholder={t("InstrumentToolsList.enterSearch")}
                  id="search_box"
                  startAdornment={
                    <InputAdornment position='start'>
                      {" "}
                      <SearchIcon
                        onClick={() => this.search()}
                        className="searchTable"
                      />
                    </InputAdornment>
                  }
                />
              </FormControl>
            </Grid>
            <Collapse in={isSearch} className="w-100">
              <Card>
                <CardContent>
                  <Grid container spacing={2} className="mx-0">
                    <Grid
                      item
                      md={12}
                      xs={12}
                      className="text-center font-weight-bold"
                      style={{
                        fontWeight: "400",
                      }}
                    >
                      <b>Tìm kiếm nâng cao</b>
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocomplete
                        label={t("Asset.filter_manage_department")}
                        searchFunction={getListOrgManagementDepartment}
                        searchObject={searchDepartmentObject}
                        typeReturnFunction={"list"}
                        displayLable={"name"}
                        showCode={"showCode"}
                        value={managementDepartment}
                        onSelect={this.selectManagementDepartment}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim()
                          let filtered = filterAutocomplete(options, params)
                          return filtered
                        }}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    {!isRoleAssetUser && <Grid item md={3} xs={12}>
                      <AsynchronousAutocompleteSub
                        label={t("InstrumentToolsList.filterInstrumentToolsType")}
                        searchFunction={getByRoot}
                        searchObject={searchObjectInstrumentToolsType}
                        displayLable={"name"}
                        children="children"
                        value={assetGroup}
                        onSelect={this.selectInstrumentToolsType}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>}
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocompleteSub
                        label={t("InstrumentToolsList.filterUseDepartment")}
                        searchFunction={searchReceiverDepartment}
                        searchObject={searchDepartmentObject}
                        displayLable={"text"}
                        value={useDepartment}
                        onSelect={this.selectUseDepartment}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim()
                          let filtered = filterAutocomplete(options, params)
                          return filtered
                        }}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <TextValidator
                        className="w-100"
                        label={t("InstrumentToolsList.filterYearIntoUse")}
                        value={yearPutIntoUse}
                        name="yearPutIntoUse"
                        type="number"
                        onKeyDown={this.handleKeyDownEnterSearch}
                        onKeyUp={this.handleKeyUp}
                        onChange={this.handleChange}
                        validators={["matchRegexp:^[1-9][0-9]{3}$"]}
                        errorMessages={[t("general.yearPutIntoUseError")]}
                      // disabled={true}
                      />
                    </Grid>

                    {isRoleAssetManager && (
                      <Grid item md={3} xs={12}>
                        <AsynchronousAutocompleteSub
                          label={t("general.filterStore")}
                          searchFunction={storesSearchByPage}
                          searchObject={searchObjectStore}
                          listData={listStore}
                          setListData={this.handleSetListStore}
                          displayLable={"name"}
                          value={store || null}
                          onSelect={this.handleSelectStore}
                          noOptionsText={t("general.noOption")}
                        />
                      </Grid>
                    )}
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocomplete
                        label={t("general.filterStatus")}
                        searchFunction={statusSearchByPage}
                        searchObject={searchObject}
                        multiple={true}
                        Rows={1}
                        value={itemStatus ? itemStatus : []}
                        displayLable={"name"}
                        onSelect={this.selectStatus}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim()
                          let filtered = filterAutocomplete(options, params)
                          return filtered
                        }}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("general.dateOfReceptionBottom")}
                          format="dd/MM/yyyy"
                          value={dateOfReceptionBottom ?? null}
                          maxDate={dateOfReceptionTop}
                          onChange={(data) => this.handleChange(data, "dateOfReceptionBottom")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={dateOfReceptionTop ? t("general.maxDateFromDate") : t("general.maxDateMessage")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("general.dateOfReceptionTop")}
                          format="dd/MM/yyyy"
                          value={dateOfReceptionTop ?? null}
                          minDate={dateOfReceptionBottom}
                          onChange={(data) => this.handleChange(data, "dateOfReceptionTop")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          minDateMessage={dateOfReceptionBottom ? t("general.minDateToDate") : t("general.minDateMessage")}
                          maxDateMessage={t("general.maxDateMessage")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <TextValidator
                        className="w-100"
                        label={"Nguyên giá"}
                        value={originalCost}
                        name="originalCost"
                        onKeyDown={this.handleKeyDownEnterSearch}
                        onKeyUp={this.handleKeyUp}
                        onChange={this.handleChange}
                        InputProps={{
                          inputComponent: NumberFormatCustom,
                        }}
                      />
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <AsynchronousAutocompleteSub
                        label={t("Asset.decisionCode")}
                        searchFunction={searchByPageBidding}
                        searchObject={searchObjectBidding}
                        value={decisionCode}
                        displayLable={"decisionCode"}
                        typeReturnFunction="listData"
                        onSelect={this.selectDecisionCode}
                        filterOptions={filterOptions}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Collapse>

            <Collapse in={isPrintIatFollow || isPrintIatBook} className="w-100">
              <Grid
                container
                spacing={2}
                alignItems="flex-end"
                className="mt-16"
                style={{ backgroundColor: "#fafafa" }}
              >
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{
                    fontWeight: "400",
                    textTransform: "uppercase",
                    backgroundColor: "",
                    textAlign: "center",
                  }}
                >
                  <b>{textSearchAdvance}</b>
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("InstrumentToolsList.InstrumentToolsType")}
                    searchFunction={getByRoot}
                    searchObject={searchObjectInstrumentToolsType}
                    displayLable={"name"}
                    children="children"
                    value={assetGroupFilter}
                    onSelect={this.selectIatGroupIatBook}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        {isPrintIatFollow && <span className="colorRed">*</span>}
                        {t("InstrumentToolsList.useDepartment")}
                      </span>
                    }
                    searchFunction={searchReceiverDepartment}
                    searchObject={searchDepartmentObject}
                    displayLable={"text"}
                    value={useDepartmentFilter}
                    onSelect={this.selectUseDepartmentIatBook}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    disabled={isRoleAssetUser}
                    noOptionsText={t("general.noOption")}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={6} className="pr-16" >
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      className="w-100"
                      id="mui-pickers-date"
                      label={t("Asset.fromDate")}
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={"fromDate"}
                      value={fromDate}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      maxDate={toDate ? (new Date(toDate)) : undefined}
                      clearable
                      onChange={(date) => this.handleDateChange(date, "fromDate")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={3} sm={6} xs={6} className="pr-16">
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      className="w-100"
                      id="mui-pickers-date"
                      label={t("Asset.toDate")}
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={"toDate"}
                      value={toDate}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      minDate={fromDate ? (new Date(fromDate)) : undefined}
                      clearable
                      onChange={(date) => this.handleDateChange(date, "toDate")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={12} sm={12} xs={12}>
                  {isPrintIatFollow && <Button
                    className="align-bottom mr-16"
                    variant="contained"
                    color="primary"
                    onClick={this.printIatBook}
                  >
                    {t("Asset.printDepartmentalAssetTrackingLog")}
                  </Button>}
                  {isPrintIatBook && (
                    <Button
                      className="align-bottom mr-16"
                      variant="contained"
                      color="primary"
                      onClick={this.exportExcelIatBook}
                    >
                      {t("Asset.exportExcel")}
                    </Button>
                  )}
                </Grid>
              </Grid>
            </Collapse>

            <Grid container spacing={2} className={"mt-16"}>

              <div>
                {shouldOpenConfirmationDialog && (
                  <ConfirmationDialog
                    title={t("general.confirm")}
                    open={shouldOpenConfirmationDialog}
                    onConfirmDialogClose={this.handleDialogClose}
                    onYesClick={this.handleConfirmationResponse}
                    text={t("general.deleteConfirm")}
                    agree={t("general.agree")}
                    cancel={t("general.cancel")}
                  />
                )}
              </div>
              {shouldOpenImportExcelDialog && (
                <ImportExcelDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenImportExcelDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  exportExampleImportExcel={this.exportExampleImportExcel}
                />
              )}
              {shouldOpenEditorDialog && (
                <InstrumentToolsListEditorDialog
                  t={t}
                  i18n={i18n}
                  isAddAsset={this.state.isAddAsset}
                  isDangSuDung={this.state?.isDangSuDung}
                  allowdEditAssetUsing={this.state?.allowdEditAssetUsing}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  isButtonAllocation={isButtonAllocation}
                  item={item}
                  isQRCode={isQRCode}
                  isMedicalEquipment={this.state?.isMedicalEquipment}
                  handleMapDataAsset={this.handleMapDataAsset}
                  isView={this.state?.isView}
                  isRoleAdmin={isRoleAdmin}
                  isViewRoleAssetUser={isViewRoleAssetUser}
                />
              )}
              {shouldOpenPrintIatBook && (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenPrintIatBook}
                  item={this.state.item}
                  title={t("Sổ theo dõi tại nơi sử dụng")}
                  model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.LIST.ASSET_BOOK_OF_USE_DEPARTMENT}
                  externalConfig={{
                    params: this.state.item?.externalParams,
                  }}
                />
              )}
              {openPrintQR && (
                <AssetsQRPrintAccording
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleCloseQrCode}
                  open={openPrintQR}
                  typeSuffering={typeSuffering}
                  items={products}
                />
              )}
              {isRoleAssetUser && <Grid
                item md={3} xs={12}
                className="p-0 mt-8 asset_department overflow-auto"
              >
                <div className="bg-white">
                  <div className="text-white font-weight-bold treeview-title">
                    {t("general.list_user_department")}
                  </div>
                  {
                    treeList &&
                    <TreeView
                      defaultCollapseIcon={<ExpandMoreIcon />}
                      defaultExpanded={['root']}
                      defaultExpandIcon={<ChevronRightIcon />}
                      onNodeSelect={this.getAssetBydepartment}
                    >
                      {treeList.map((node) => this.renderTree(node))}
                    </TreeView>
                  }
                </div>
              </Grid>}
              <Grid item md={isRoleAssetUser ? 9 : 12} xs={12} style={{ overflowX: "auto" }}>

                <MaterialTable
                  title={t("general.list")}
                  columns={columns}
                  data={this.state.itemList}
                  style={{ overflowX: "auto" }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    draggable: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    sorting: false,
                    paging: false,
                    search: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "450px",
                    minBodyHeight: "450px",
                    padding: "dense",
                    toolbar: false,
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                />
                <TablePagination
                  {...defaultPaginationProps()}
                  rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </Grid>
        </ValidatorForm>
      </div>
    );
  }
}

InstrumentToolsList.contextType = AppContext;
export default InstrumentToolsList;
