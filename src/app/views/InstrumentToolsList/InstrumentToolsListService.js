import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT + "/api/asset" + ConstantList.URL_PREFIX;
const API_ASSETORG_PATH = ConstantList.API_ENPOINT + "/api/assetOrganization";
const API_PATH_VOUCHER =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_MAINTAINREQUEST =
  ConstantList.API_ENPOINT + "/api/maintainRequest" + ConstantList.URL_PREFIX;
const API_PATH_DOCUMENT = ConstantList.API_ENPOINT + "/api/asset_document";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_PRODUCT =
  ConstantList.API_ENPOINT + "/api/product" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_STATUS =
  ConstantList.API_ENPOINT + "/api/assetstatus" + ConstantList.URL_PREFIX;
const API_PATH_COMMONOBJECT =
  ConstantList.API_ENPOINT + "/api/commonobject" + ConstantList.URL_PREFIX;
const API_PATH_CCDC = ConstantList.API_ENPOINT + "/api/instruments-and-tools";
const API_PATH_PRINT_IAT_BOOK =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/so-theo-doi-ccdc";

export const searchByPageCCDC = (searchObject) => {
  let url = API_PATH_CCDC + `/page`;
  let config = {
    params: {
      ...searchObject,
      status: searchObject?.status?.toString(),
    },
  };
  return axios.get(url, config);
};
export const createAssetCCDC = (instrumentsTools) => {
  return axios.post(API_PATH_CCDC, instrumentsTools);
};
export const createAssetCCDCMultiple = (instrumentsTools) => {
  return axios.post(API_PATH_CCDC + "/multiple", instrumentsTools);
};
export const getItemById = (id) => {
  return axios.get(API_PATH_CCDC + "/" + id);
};
export const updateAsset = (id, instrumentsTools) => {
  return axios.put(API_PATH_CCDC + "/" + id, instrumentsTools);
};
export const deleteItem = (id) => {
  return axios.delete(API_PATH_CCDC + "/" + id);
};
export const exportToExcel = (params) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/excel/export/iat",
    params: params,
    responseType: "blob",
  });
};
export const exportExampleImportExcel = (instrumentsTools) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/excel/template/asset-iat",
    data: instrumentsTools,
    responseType: "blob",
  });
};
export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + linkFile,
    // data: instrumentsTools,
    responseType: "blob",
  });
};

export const getNewCode = (assetClass) => {
  let config = { params: { assetClass: assetClass } };
  let url = API_PATH + "/new-code";
  return axios.get(url, config);
};
export const getNewCodes = (quantityCode) => {
  return axios.get(API_PATH + "/new-codes/" + quantityCode);
};
export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getMedicalEquipment = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/ltbyt/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};
export const getShoppingForm = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/htms/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};
export const getManufacturer = (searchObject) => {
  let url =
    API_PATH_COMMONOBJECT +
    "/hsx/page/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.get(url);
};

export const searchOrgByParent = (searchObject) => {
  let url = API_ASSETORG_PATH + "/parent/getOrgByParent";
  return axios.post(url, searchObject);
};

export const checkDeleteAsset = (id) => {
  return axios.get(API_PATH + "/checkDeleteById/" + id);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  let url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const codeWasUsed = (asset) => {
  return axios.post(API_PATH + "/check/codeWasUsed", asset);
};

export const createAsset = (asset) => {
  return axios.post(API_PATH, asset);
};

export const getAllAssets = () => {
  return axios.get(API_PATH + "/1/100000");
};

// ASSET_DEPARTMENT
export const getListManagementDepartment = () => {
  return axios.get(API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment");
};
export const getListOrgManagementDepartment = (searchDepartmentObject) => {
  if (searchDepartmentObject?.orgId) {
    return axios.get(
      API_PATH_ASSET_DEPARTMENT +
        "/getListManagementDepartment/" +
        searchDepartmentObject?.orgId
    );
  }
  return axios.get(API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment");
};

export const getListUserDepartment = (SeachObjectct) => {
  var url = API_PATH_ASSET_DEPARTMENT + "/getListUserDepartment";
  return axios.post(url, SeachObjectct);
};

export const getAllProducts = () => {
  return axios.get(API_PATH_PRODUCT + "/getall");
};

export const getAllstatuss = () => {
  return axios.get(API_PATH_ASSET_STATUS + "/1/100000");
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};

export const historyOfAllocationTransfer = (assetObjectId) => {
  return axios.post(
    API_PATH_VOUCHER + "/historyOfAllocationTransfer",
    assetObjectId
  );
};

export const historyOfBrokenMessageAndRepair = (assetObjectId) => {
  return axios.post(
    API_PATH_MAINTAINREQUEST + "/historyOfBrokenMessageAndRepair",
    assetObjectId
  );
};

// API_PATH_DOCUMENT
export const searchByPageAssetDocument = (assetDocumentObject) => {
  return axios.post(API_PATH_DOCUMENT + "/searchByPage", assetDocumentObject);
};

export const createAssetDocument = (assetDocument) => {
  return axios.post(API_PATH_DOCUMENT, assetDocument);
};

export const getAssetDocumentById = (assetDocumentId) => {
  return axios.get(API_PATH_DOCUMENT + "/" + assetDocumentId);
};

export const deleteAssetDocumentById = (assetDocumentId) => {
  return axios.delete(API_PATH_DOCUMENT + "/" + assetDocumentId);
};

export const updateAssetDocumentById = (assetDocument, assetDocumentId) => {
  return axios.put(API_PATH_DOCUMENT + "/" + assetDocumentId, assetDocument);
};

export const getNewCodeAssetDocument = () => {
  return axios.get(API_PATH_DOCUMENT + "/addNew/getNewCode");
};

export const searchByPageCCDCV2 = (searchObject) => {
  let url = API_PATH_CCDC + `/page/v2`;
  let config = {
    params: {
      ...searchObject,
      status: searchObject?.status?.toString(),
      statusIndexOrders: searchObject?.statusIndexOrders?.toString(),
    },
  };
  return axios.get(url, config);
};

export const printIatBookService = (params) => {
  return axios.get(API_PATH_PRINT_IAT_BOOK, {params});
};

export const exportExcelIatFollowService = (params) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/excel/so-theo-doi-ccdc",
    params,
    responseType: "blob",
  });
};
export const exportExcelIatBookService = (params) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/excel/iat-book",
    params,
    responseType: "blob",
  });
};
