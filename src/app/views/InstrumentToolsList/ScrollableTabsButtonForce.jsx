import React, { useEffect, useLayoutEffect, useState } from "react";
import {
	Grid,
	Button,
	IconButton,
	Icon,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AllocationEditerDialog from "../InstrumentsToolAllocation/AllocationEditerDialog";
import RecipeReviewCard from '../Asset/Component/RecipeReviewCard';
import GeneralInfomation from './Component/GeneralInfomation';
import InstrumentToolProperties from "./Component/InstrumentToolProperties";
import OriginTools from "./Component/OriginTools";
import ToolProfile from "./Component/ToolProfile";
import { variable } from "../../appConst";
import { LightTooltip } from "app/appFunction";


const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		width: "100%",
		// backgroundColor: theme.palette.background.paper,
		// background: "#f5f5f5",
	},
	flexSpaceBetween: {
		display: "flex",
		justifyContent: "space-between",
	},
	w30: {
		width: "30%",
	},
	w65: {
		width: "65%",
	},
	relative: {
		position: "relative",
	},
	sticky: {
		position: "sticky",
		top: 0,
		left: 0,
	},
	// 	style={{marginTop: "20px", position: "sticky", top: 0}}
	customAppBar: {
		marginTop: '20px',
		position: 'sticky',
		top: 0,
		width: '100%'
	},
	customAppBarContainer: {
		display: "flex",
		flexDirection: "column",
		padding: "16px",
		backgroundColor: "#FFF",
		alignItems: "flex-start",
		position: "fixed",
		width: "15%",
		borderRadius: "8px",
		// border: "1px solid var(--primary)",
	},
	customButtonText: {
		color: theme.custom.primary.main || variable.css.primary.main,
		margin: "7px 0",
		width: "100%",
		justifyContent: "flex-start",
		padding: "10px",
		fontWeight: "600",
		fontSize: "14px",
	},
	notiMessage: {
		position: "absolute",
		top: "-7px",
		right: "-12px",
		zIndex: 1,
		color: "red",
		backgroundColor: "white",
		animation: "$blink 4s",
	},

	"@keyframes blink": {
		"0%": {
			opacity: 1,
		},
		"50%": {
			opacity: 0,
		},
		"100%": {
			opacity: 1,
		},
	},
}));

function ScrollableTabsButtonForce(props) {
	const t = props.t;
	const i18n = props.i18n;
	const classes = useStyles();
	const [value, setValue] = React.useState(0);
	const [activeIndex, setActiveIndex] = useState(0);
	const [isTiepNhan, setIsTiepNhat] = useState(false)
	const [openScroll, setOpenScroll] = useState(true)
	const [scrollPosition, setScrollPosition] = useState(0); // Add this state variable

	useEffect(() => {
		const handleScroll = () => {
			setScrollPosition(window.scrollY);
		};

		window.addEventListener("scroll", handleScroll);

		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, []);

	const buttons = [
		{
			label: t("InstrumentToolsList.tabinfoInstrumentTools"),
			sectionId: "generalInfo", // Assign the corresponding sectionId
			isShowMessage: props.item?.isErrorGeneralInfomation
		},
		{
			label: t("InstrumentToolsList.tabInstrumentToolsSourceFile"),
			sectionId: "originTools", // Assign the corresponding sectionId
			isShowMessage: props.item?.isErrorOriginTools
		},
		// {
		// 	label: t("InstrumentToolsList.tabDepreciationInformation"),
		// 	sectionId: "allocationInfo", // Assign the corresponding sectionId
		// },
		{
			label: t("InstrumentToolsList.tabAttribute"),
			sectionId: "instrumentProperties", // Assign the corresponding sectionId
			isShowMessage: props.item?.isErrorInstrumentToolProperties
		},
		{
			label: t("InstrumentToolsList.tabInstrumentToolsFile"),
			sectionId: "toolProfile", // Assign the corresponding sectionId
			isShowMessage: props.item?.isErrorToolProfile
		},
	];
	const handleScroll = () => {
		const dialogContent = document.querySelector('.MuiDialogContent-root');
		const scrollPosition = dialogContent.scrollTop;
		// Lấy các vị trí của các id tương ứng
		const positions = buttons.map((item) =>
			document.getElementById(item.sectionId).offsetTop
		);

		// Kiểm tra vị trí hiện tại và đặt active dựa trên mảng positions
		for (let i = 0; i < positions.length; i++) {
			if (scrollPosition <= 0) {
				setActiveIndex(0);
				break;
			}
			else if (
				scrollPosition >= positions[i] &&
				(i === positions.length - 1 || scrollPosition < positions[i + 1])
			) {
				setActiveIndex(i + 1);
				break;
			}
		}
	};

	useLayoutEffect(() => {
		const dialogContent = document.querySelector('.MuiDialogContent-root')
		dialogContent.addEventListener("scroll", handleScroll);
		return () => {
			dialogContent.removeEventListener('scroll', handleScroll);
		};
	}, [buttons]);

	const scrollFunctions = {
		handleScrollToSection: (sectionId, index) => {
			const sectionElement = document.getElementById(sectionId);
			if (sectionElement) {
				setActiveIndex(index)
				sectionElement.scrollIntoView({ behavior: "smooth" });
			}
		},
	};

	return (
		<div id="tabButton" className={classes.root}>
			<div value={value} index={0} className={classes.relative}>
				<Grid container spacing={1} className="" justifyContent="space-between">
					<Grid container item direction="column" spacing={1} xs={2} sm={2} md={2} className={classes.customAppBar}>
						<div className={`buttonAsset ${classes.customAppBarContainer}`}>
							{buttons.map((button, index) => (
								<div className={`w-100 ${classes.relative}`}>
									{button?.isShowMessage && <LightTooltip
										title={t("general.requiredInformation")}
										placement="right-end"
										enterDelay={300}
										leaveDelay={200}
										className={classes.notiMessage}
									>
										<IconButton size="small">
											<Icon fontSize="small" color="red">
												report
											</Icon>
										</IconButton>
									</LightTooltip>}
									<Button
										key={index}
										className={`w-100 ${classes.customButtonText} ${activeIndex === index ? "activeButton" : ""}`}
										onClick={() => {
											scrollFunctions.handleScrollToSection(button.sectionId, index)
										}}
									>
										{button.label}
									</Button>
								</div>
							))}
						</div>
					</Grid>
					<Grid item xs={10} sm={10} md={10}>
						<section id='generalInfo'>
							<RecipeReviewCard
								title={t("InstrumentToolsList.tabinfoInstrumentTools")}
								isListTaiSan={!isTiepNhan}
								isOpen={openScroll}
							>
								<GeneralInfomation {...props} />
							</RecipeReviewCard>
						</section>


						<section id='originTools'>
							<RecipeReviewCard
								title={t("InstrumentToolsList.tabInstrumentToolsSourceFile")}
								isListTaiSan={!isTiepNhan}
								isOpen={openScroll}
							>
								<OriginTools
									item={props.item}
									openPopupSelectAssetSource={props.openPopupSelectAssetSource}
									handleAddAssetSource={props.handleAddAssetSource}
									handleSelectAssetSourcePopupClose={props.handleSelectAssetSourcePopupClose}
									{...props}
								/>
							</RecipeReviewCard>
						</section>

						<section id='instrumentProperties'>
							<RecipeReviewCard
								title={t("InstrumentToolsList.tabAttribute")}
								isListTaiSan={!isTiepNhan}
								isOpen={openScroll}
							>
								<InstrumentToolProperties
									item={props.item}
									handleAddAttribute={props.handleAddAttribute}
									handleSelectAttributePopupClose={props.handleSelectAttributePopupClose}
									handleRowDataCellChangeAssetSource={props.handleRowDataCellChangeAssetSource}
									{...props}
								/>
							</RecipeReviewCard>
						</section>

						<section id='toolProfile'>
							<RecipeReviewCard
								title={t("InstrumentToolsList.tabInstrumentToolsFile")}
								isListTaiSan={!isTiepNhan}
								isOpen={openScroll}
							>
								<ToolProfile
									item={props.item}
									handleAddAssetDocumentItem={props.handleAddAssetDocumentItem}
									handleAssetFilePopupClose={props.handleAssetFilePopupClose}
									itemAssetDocument={props.itemAssetDocument}
									getAssetDocument={props.getAssetDocument}
									handleUpdateAssetDocument={props.handleUpdateAssetDocument}
									handleRowDataCellEditAssetFile={props.handleRowDataCellEditAssetFile}
									handleRowDataCellDeleteAssetFile={props.handleRowDataCellDeleteAssetFile}
								/>
							</RecipeReviewCard>
						</section>
					</Grid>
					<Grid>
						{props?.item?.shouldOpenAllocationEditorDialog && (
							<AllocationEditerDialog
								t={t}
								i18n={i18n}
								open={props?.item?.shouldOpenAllocationEditorDialog}
								handleOKEditClose={props?.handleCloseAll}
								handleCloseAllocationEditorDialog={props?.handleClose}
								item={{}}
								InstrumentsToolsAllocation={props?.item?.InstrumentsToolsAllocation}
								isAllocation={true}
								isUpdateAsset={props?.item?.isUpdateAsset}
							/>
						)}
					</Grid>
				</Grid>
			</div>
		</div>
	)
}

export default ScrollableTabsButtonForce;
