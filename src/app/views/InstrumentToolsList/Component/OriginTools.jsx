import React from 'react';
import { Button, Grid, Icon, IconButton } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import SelectMultiAssetSourcePopup from "../../Asset/ComponentPopups/SelectMultiAssetSourcePopup";
import MaterialTable, { MTableToolbar } from "material-table";
import { TextValidator } from "react-material-ui-form-validator";
import {appConst, variable} from "../../../appConst";
import { LightTooltip, NumberFormatCustom, convertNumberPrice, handleKeyDownIntegerOnly } from 'app/appFunction';

function OriginTools(props) {
	const { t, i18n } = useTranslation();
	const { isView } = props?.item;

	let columnsAssetSource = [
		{
			title: t("general.action"),
			field: "",
			width: "150",
			hidden: isView,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => (
				!props?.item?.isEdit &&
				<LightTooltip
					title={t("general.delete")}
					placement="top"
					enterDelay={300}
					leaveDelay={200}
				>
					<IconButton
						size="small"
						onClick={() => props.handleRowDataCellDeleteAssetSource(rowData)}
					>
						<Icon fontSize="small" color="error">
							delete
						</Icon>
					</IconButton>
				</LightTooltip>
			),
		},
		{
			title: t("general.stt"),
			field: "code",
			align: "left",
			maxWidth: 50,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData.tableData.id + 1,
		},
		{
			title: t("AssetSource.code"),
			field: "assetSource.code",
			align: "left",
			minWidth: 120,
			maxWidth: 200,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData?.assetSource?.code || rowData?.code || rowData?.assetSourceCode
		},
		{
			title: t("AssetSource.name"),
			field: "assetSource.name",
			align: "left",
			minWidth: 350,
			cellStyle: {
			},
			render: (rowData) => rowData?.assetSource?.name || rowData?.name || rowData?.assetSourceName
		},
		{
			title: t("AssetSource.percentage"),
			field: "percentage",
			minWidth: 80,
			maxWidth: 100,
			render: (rowData) => (
				<TextValidator
					className="w-100 inputPrice isTabOriginTools"
					onKeyDown={handleKeyDownIntegerOnly}
					onChange={(e) => props.handleRowDataCellChangeAssetSource(rowData, e)}
					type="number"
					name="percentage"
					value={rowData?.percentage || ""}
					validators={["minFloat: 1", "maxNumber:100", `matchRegexp:${variable.regex.onlyTwoCharacterFloat}`,]}
					errorMessages={[
						"Không được nhập nhỏ hơn 0",
						"Không được nhập lớn hơn 100",
						t("general.quantityError"),
					]}
					disabled={isView || props?.item?.assetSources?.length === 1}
				/>
			),
		},
		{
			title: t("AssetSource.value"),
			field: "value",
			align: "right",
			minWidth: 100,
			maxWidth: 120,
			render: (rowData) => (
				<TextValidator
					className="w-100 inputPrice isTabOriginTools"
					onKeyDown={handleKeyDownIntegerOnly}
					onChange={(valueText) =>
						props.handleRowDataCellChangeAssetSource(rowData, valueText)
					}
					type="text"
					name="value"
					InputProps={{
						inputComponent: NumberFormatCustom,
					}}
					value={convertNumberPrice(rowData?.value) || ""}
					disabled={isView}
				/>
			),
		},
	];


	const handleKeyDown = (e) => {
		if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
			e.preventDefault();
		}
	};

	return (
		<div className='mx-18'>
			<Grid container>
				<Grid item md={12} sm={12} xs={12}>
					{
						!props?.item?.isEdit &&
						<Button
							variant="contained"
							color="primary"
							onClick={props.openPopupSelectAssetSource}
							disabled={
								!props.item.originalCost
								|| +props.item.originalCost === 0
							}
						>
							{t("InstrumentToolsList.addSource")}
						</Button>
					}
					{props.item.shouldOpenPopupSelectAssetSource && (
						<SelectMultiAssetSourcePopup
							isIat={true}
							open={props.item.shouldOpenPopupSelectAssetSource}
							handleSelect={props.handleAddAssetSource}
							assetSources={props.item.assetSources || []}
							handleClose={props.handleSelectAssetSourcePopupClose}
							t={t}
							i18n={i18n}
						/>
					)}
				</Grid>
				<Grid item md={12} sm={12} xs={12} className="mt-16">
					<MaterialTable
						data={props.item?.assetSources || []}
						columns={columnsAssetSource}
						localization={{
							body: {
								emptyDataSourceMessage: t("general.emptyDataMessageTable"),
							},
						}}
						options={{
							draggable: false,
							toolbar: false,
							selection: false,
							actionsColumnIndex: -1,
							paging: false,
							search: false,
							sorting: false,
							padding: "dense",
							rowStyle: (rowData) => ({
								backgroundColor:
									rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
							}),
							headerStyle: {
								backgroundColor: "#358600",
								color: "#fff",
							},
							maxBodyHeight: "343px",
							minBodyHeight: "343px",
						}}
						components={{
							Toolbar: (props) => (<MTableToolbar {...props} />),
						}}
					/>
				</Grid>
			</Grid>
		</div>
	);
}

export default OriginTools;