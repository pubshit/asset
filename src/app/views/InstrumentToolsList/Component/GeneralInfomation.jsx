import React, { useState, useEffect } from "react";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { getByRoot } from "../../InstrumentAndToolsType/InstrumentAndToolsTypeService";
import { TextValidator } from "react-material-ui-form-validator";
import {
  Button,
  Checkbox,
  Chip,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useTranslation } from "react-i18next";
import DateFnsUtils from "@date-io/date-fns";
import { STATUS_STORE, appConst, keySearch, variable } from "app/appConst";
import { getStockKeepingUnit } from "../../StockKeepingUnit/StockKeepingUnitService";
import {
  getManufacturer,
  getMedicalEquipment,
  getShoppingForm,
  searchByPageCCDCV2,
} from "../InstrumentToolsListService";
import { searchByPage as storesSearchByPage } from "../../Store/StoreService";
import {
  NumberFormatCustom,
  checkInvalidDate,
  filterOptions,
  handleKeyDownIntegerOnly,
  getOptionSelected,
} from "app/appFunction";
import { getListManagementDepartment } from "app/views/Asset/AssetService";
import { searchByPageList as contractSearchByPage } from "../../Contract/ContractService";
import NotePopup from "app/views/Component/NotificationPopup/NotePopup";
import DialogNote from "app/views/Component/DialogNote/DialogNote";
import {
  searchByTextNew,
} from "../../Supplier/SupplierService";
import { searchByPage as getListProducts } from "../../Product/ProductService";
import { toast } from "react-toastify";
import { getListUserByDepartmentId } from "app/views/AssetTransfer/AssetTransferService";
import { Label } from "../../Component/Utilities";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { debounce } from "utils";
import QRCodeImage from "../../Asset/ComponentPopups/QRCodeImage";
import AssetQRPrintNew from "../../Asset/ComponentPopups/AssetQRPrintNew";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
  },
  w30: {
    width: "30%",
  },
  w65: {
    width: "65%",
  },
  relative: {
    position: "relative",
  },
  sticky: {
    position: "sticky",
    top: 0,
    left: 0,
  },
  mt15: {
    marginTop: "15px !important",
  },
}));

const GeneralInfomation = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const {
    isEdit,
    isBuyLocally,
    id,
    isMultiple,
    isView,
    quantity,
    quantityAsset,
    shouldOpenSelectAssetGroupPopup,
    serialNumbers,
    permissions,
    warrantyMonth,
  } = props.item;
  const {
    item,
    handleChange,
    handleChangeSelectContract,
    handleMapDataAsset,
    allowdEditAssetUsing,
    isViewRoleAssetUser,
    openSelectProductPopup
  } = props;
  const [openPrintQR, setValueOpenPrintQR] = useState(false);
  const [listAssetGroup, setListAssetGroup] = useState([]);
  const [listManagementDepartment, setListManagementDepartment] = useState([]);
  const [listHopDong, setListHopDong] = useState([]);
  const [listAsset, setListAsset] = useState([]);
  const [listSupply, setListSupply] = useState([]);
  const [listProducts, setListProducts] = useState([]);
  const [listUsePerson, setListUsePerson] = useState([]);
  const [query, setQuery] = useState({
    pageIndex: 1,
    pageSize: 10,
    keyword: "",
    keySearch: "",
  });
  const searchParamUserByUseDepartment = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    departmentId: item?.useDepartment?.id
  };
  const searchObjectUnit = appConst.OBJECT_SEARCH_MAX_SIZE;
  const searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isActive: STATUS_STORE.HOAT_DONG.code,
    managementDepartmentId: props?.item?.managementDepartment?.id,
  };
  const assetGroupSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    assetClass: appConst.assetClass.CCDC,
  };
  let searchObjectReceiverDepartment = appConst.OBJECT_SEARCH_MAX_SIZE;

  const accountantOrTemporaryValue = { isManageAccountant: 1, isTemporary: 2 };
  let riskClassification = appConst.listRisk.find(
    (risk) => risk.value === props?.item?.riskClassification
  ) || "";
  let qrInfo =
    `Mã TS: ${props.item?.code}\n` +
    `Mã QL: ${props.item?.managementCode}\n` +
    `Tên TS: ${props.item?.product?.name}\n` +
    `Nước SX: ${props.item?.madeIn}\n` +
    `Năm SX: ${props.item?.yearOfManufacture}\n` +
    `Ngày SD: ${props.item?.yearPutIntoUse}\n` +
    `PBSD: ${props.item?.useDepartment?.name}\n`;

  useEffect(() => {
    handleSearchAsset();
    handleSearchSupplyUnit();
    handleGetListProducts();
  }, []);

  useEffect(() => {
    if (shouldOpenSelectAssetGroupPopup) {
      setListAssetGroup([]);
    }
  }, [shouldOpenSelectAssetGroupPopup]);

  useEffect(() => {
    if (query.keySearch === keySearch.asset) {
      handleSearchAsset();
    }
    if (query.keySearch === keySearch.product) {
      handleGetListProducts();
    }
  }, [item.id, query]);
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
    }
  };

  const handleChangeValueOpenQR = (newValue) => {
    setValueOpenPrintQR(newValue);
  };

  const getOptionLabel = (option) => {
    if (option === "") {
      return "No Option Selected";
    }
    return option.riskType;
  };

  const handleGetListProducts = async (text = query.keyword) => {
    const searchObjectProduct = {
      keyword: query.keyword,
      pageIndex: 1,
      pageSize: 10,
      productTypeCode: appConst.productTypeCode.CCDC
    };
    try {
      let res = await getListProducts(searchObjectProduct);
      const { data } = res;
      if (data) {
        if (data?.content?.length > 0) {
          setListProducts(data.content);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleGetListContract = (e, isEmptyKeyword) => {
    try {
      const searchObject = {};
      searchObject.pageIndex = query.pageIndex;
      searchObject.pageSize = query.pageSize;
      searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
      if (!isEmptyKeyword) {
        searchObject.keyword = e?.target?.value || "";
      }
      props.handleSetDataSelect(e?.target?.value, "keySearch");
      contractSearchByPage(searchObject).then(({ data }) => {
        if (data) {
          const updatedList = data?.data?.content?.map((item) => ({
            ...item,
            supplier: item.supplierId
              ? {
                id: item.supplierId,
                name: item.supplierName,
              }
              : null
          })) || [];

          setListHopDong(updatedList);
        }
      });
    } catch (error) { }
  };

  const handleSearchAsset = async () => {
    if (!item?.id) {
      try {
        let searchObject = {
          ...query,
          managementDepartmentId: item?.managementDepartment?.id,
          decisionCode: item?.isIatReception ? typeof item?.decisionCode === "object" ? item?.decisionCode?.decisionCode :
            typeof item?.decisionCode === "string" ? item?.decisionCode :
              "" : ""
        }
        let res = await searchByPageCCDCV2(searchObject);
        const { data, code } = res?.data;
        if (code === appConst.CODE.SUCCESS) {
          setListAsset(data?.content);
        }
      } catch (error) {
        toast.error(t("general.error"));
      }
    }
  };

  const handleSearch = (value, source) => {
    setQuery({ ...query, keyword: value || "", keySearch: source });
  };

  const isSerialNumbersError = () => {
    const quantity = Number(item?.quantityAsset) ?? null;
    const serialNumbers = item?.serialNumbers || [];

    let flag = null;

    if (quantity && serialNumbers.length === 0) {
      flag = false;
    } else if (!quantity && serialNumbers.length > 0 && serialNumbers.length <= 10) {
      flag = false;
    } else if (quantity && serialNumbers.length > 0) {
      flag = !(serialNumbers.length === quantity);
    } else flag = !quantity && serialNumbers.length > 10;
    return flag;
  };

  const getSerialNumbersHelperText = () => {
    const quantity = item?.quantityAsset ?? null;
    const serialNumbers = item?.serialNumbers || [];
    if (quantity >= 1 && serialNumbers.length > 0) {
      if (serialNumbers.length > quantity) {
        return t("Validator.loiNhanBan");
      } else if (serialNumbers.length < quantity) {
        return t("Validator.loiSerial");
      }
    } else if (serialNumbers.length > 0) {
      return isSerialNumbersError() ? t("Asset.ErrorSerialNumbers") : "";
    }

    return "";
  };

  const handleKeyDown = (e) => {
    if (variable.regex.specialCharactersKey.includes(e.key)) {
      e.preventDefault();
    }
  };

  const handleSearchSupplyUnit = async (e) => {
    try {
      let searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      };
      searchObject.keyword = e?.target?.value?.trim();
      searchObject.typeCodes = [appConst.TYPE_CODES.NCC_CU, appConst.TYPE_CODES.NCC_BDSC,];

      const res = await searchByTextNew(searchObject);
      setListSupply([]);
      if (res?.data?.content?.length > 0) {
        setListSupply(res?.data?.content);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onInputChangeDataSearch = debounce((value, key) => {
    handleSearch(value, key);
  }, 500);

  const defaultClassname = "w-100 isTabGeneralInfomation";
  return (
    <>
      <Grid container>
        <Grid item container xs={12} spacing={2} className="mx-12">
          {/* Cột thông tin */}
          <Grid item container xs={12} sm={8} md={10} spacing={2}>
            {!item?.id && !isView && (
              <Grid className="mt-3" item xs={12} sm={6} md={4}>
                <div className="flex flex-middle w-100">
                  <div className="w-100">
                    <Autocomplete
                      id="combo-box"
                      size="small"
                      options={listAsset}
                      onChange={(event, value) =>
                        handleMapDataAsset(event, value, item)
                      }
                      getOptionLabel={(option) => option?.name || ""}
                      filterOptions={filterOptions}
                      renderInput={(params) => (
                        <TextValidator
                          className={defaultClassname}
                          {...params}
                          label={t("InstrumentToolsList.searchInstrumentTool")}
                          placeholder={t("InstrumentToolsList.SimilarProperty")}
                          variant="standard"
                          onChange={(e) => {
                            const inputValue = e?.target?.value;
                            onInputChangeDataSearch(inputValue, keySearch.asset);
                          }}
                          onFocus={() => {
                            onInputChangeDataSearch("", keySearch.asset);
                          }}
                          value={query?.keyword || ""}
                        />
                      )}
                      noOptionsText={t("general.noOption")}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <DialogNote
                      content={t("Note.searchInstrumentTool")}
                      className="mt-16"
                    />
                  </div>
                </div>
              </Grid>
            )}
            <Grid item xs={12} sm={6} md={4}>
              <ValidatePicker
                className={defaultClassname}
                fullWidth
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("InstrumentToolsList.dateOfReception")}
                  </span>
                }
                autoOk
                value={props?.item?.dateOfReception || ""}
                onChange={(date) =>
                  props.handleDateChange(date, "dateOfReception")
                }
                maxDate={new Date()}
                maxDateMessage={t("general.maxDateNow")}
                onBlur={() =>
                  handleBlurDate(
                    props?.item?.dateOfReception,
                    "dateOfReception"
                  )
                }
                disabled={permissions.dateOfReception.isDisable}
                clearable
                validators={["required"]}
                errorMessages={[t("general.required")]}
                isTabGeneralInfomation
              />
            </Grid>
            {!props?.item?.id && !props.item.isIatReception && (
              <Grid item xs={12} sm={6} md={4} className="">
                <div className="flex flex-middle">
                  <FormControlLabel
                    className="mr-4"
                    value={props.item?.isMultiple}
                    name="isMultiple"
                    onChange={(isMultiple) =>
                      handleChange(
                        isMultiple,
                        variable.listInputName.isMultiple
                      )
                    }
                    control={<Checkbox checked={props.item?.isMultiple} />}
                    label={
                      <span style={{ fontSize: "14px" }}>
                        {t("InstrumentToolsList.clone")}
                      </span>
                    }
                  />
                  <NotePopup
                    typeIndex={appConst.NOTE_INDEX.IS_MULTIPLE}
                    assetClass={appConst.assetClass.CCDC}
                  />
                </div>
              </Grid>
            )}
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.managementCode")}
                type="text"
                onChange={handleChange}
                name="managementCode"
                value={props.item.managementCode || ""}
                disabled={permissions.managementCode.isDisable}
                validators={["matchRegexp:^.{1,255}$"]}
                errorMessages={["Không nhập quá 255 ký tự"]}
                InputProps={{
                  endAdornment: (
                    <NotePopup
                      typeIndex={appConst.NOTE_INDEX.IS_MANAGEMENT_CODE}
                      assetClass={appConst.assetClass.CCDC}
                    />
                  ),
                }}
              />
            </Grid>
            {/* <Grid item xs={12} sm={6} md={4}>
              <Autocomplete
                id="combo-box"
                size="small"
                options={listProducts}
                onChange={(event, value) =>
                  props?.handleSelectProduct(value)}
                value={props.item?.product || null}
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim();
                  return filterOptions(options, params, true, "name");
                }}
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params}
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("InstrumentToolsList.select")}
                      </span>
                    }
                    variant="standard"
                    onChange={(e) => {
                      handleSearch(e?.target?.value, keySearch.product);
                      props.handleSetDataSelect(e?.target.value, "keySearch")
                    }}
                    value={props?.item?.product?.name || ""}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                )}
                noOptionsText={t("general.noOption")}
                disabled={permissions.selectProduct.isView}
              />
            </Grid> */}
            <Grid className="mt-3" item xs={12} sm={6} md={4}>
              <div className="flex flex-middle w-100">
                <div className={props?.item?.id ? "w-100" : "w-80"}>
                  <TextValidator
                    className={defaultClassname}
                    size="small"
                    InputProps={{
                      readOnly: true,
                    }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Asset.product")}
                      </span>
                    }
                    value={props?.item?.product?.name || ""}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                </div>
                {!props?.item?.id && (
                  <div className="w-20">
                    <Button
                      size="small"
                      style={{ float: "right" }}
                      className="w-100 mt-12"
                      variant="contained"
                      color="primary"
                      onClick={openSelectProductPopup}
                    >
                      {t("general.select")}
                    </Button>
                  </div>
                )}
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={
                  <Label isRequired>{t("InstrumentToolsList.name")}</Label>
                }
                onChange={handleChange}
                type="text"
                name="name"
                value={props?.item?.name || ""}
                validators={["required", "matchRegexp:^.{1,255}$"]}
                disabled={permissions.productName.isDisable}
                errorMessages={[
                  t("general.required"),
                  t("general.errorInput255"),
                ]}
              />
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("InstrumentToolsList.titlelist")}
                  </span>
                }
                searchFunction={getByRoot}
                searchObject={assetGroupSearchObject}
                listData={listAssetGroup}
                setListData={setListAssetGroup}
                displayLable={"name"}
                value={props.item.assetGroup ?? null}
                onSelect={props.handleSelectAssetGroup}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                onInputChange={(e) => {
                  e.target.name = "searchText";
                  props.handleChange(e);
                }}
                disabled={permissions.typeCCDC.isView}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                noOptionsText={t("general.noOption")}
                className={defaultClassname}
              />
            </Grid>
            {isMultiple && !id && (
              <Grid item xs={12} sm={6} md={4}>
                <TextValidator
                  className={defaultClassname}
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("InstrumentToolsList.quantityAsset")}
                    </span>
                  }
                  type="number"
                  name="quantityAsset"
                  max={10}
                  value={quantityAsset}
                  InputProps={{
                    readOnly: props?.item?.isEdit,
                  }}
                  onKeyDown={handleKeyDown}
                  onChange={(quantityAsset) =>
                    props?.handleChange(quantityAsset, "quantityAsset")
                  }
                  validators={["required", "minNumber:2", "maxNumber: 10"]}
                  errorMessages={[
                    t("general.required"),
                    t("general.minMultipleQuantityError"),
                    t("general.maxMultipleQuantityError"),
                  ]}
                />
              </Grid>
            )}
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("InstrumentToolsList.quantity")}
                  </span>
                }
                type="number"
                name="quantity"
                value={quantity || ""}
                InputProps={{
                  readOnly: props?.item?.isEdit,
                  endAdornment: (
                    <NotePopup
                      typeIndex={appConst.NOTE_INDEX.IS_QUANTITY}
                      assetClass={appConst.assetClass.CCDC}
                    />
                  ),
                }}
                onKeyDown={handleKeyDown}
                onChange={(quantity) =>
                  props?.handleChange(quantity, "quantity")
                }
                disabled={permissions.quantity.isDisable || isEdit}
                validators={["required", "minNumber:1"]}
                errorMessages={[
                  t("general.required"),
                  t("general.minNumberError"),
                ]}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.decisionCode")}
                type="text"
                name="decisionCode"
                value={
                  typeof item?.decisionCode === "object" ? item?.decisionCode?.decisionCode || "" :
                    typeof item?.decisionCode === "string" ? item?.decisionCode || "" :
                      ""
                }
                disabled
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.symbolCode")}
                onChange={props?.handleChange}
                type="text"
                name="symbolCode"
                value={props?.item?.symbolCode ? props?.item?.symbolCode : ""}
                defaultValue={props?.item?.symbolCode ? props?.item?.symbolCode : ""}
                InputProps={{
                  readOnly: isView,
                }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              sm={isMultiple ? 12 : 6}
              md={isMultiple ? 12 : 4}
            >
              {isMultiple ? (
                <Autocomplete
                  multiple
                  // multiline
                  disableClearable
                  options={[]}
                  value={serialNumbers ?? []}
                  onChange={(e, value) =>
                    props.handleAddNewSeriNumberOption(e, value)
                  }
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        variant="outlined"
                        label={option}
                        {...getTagProps({ index })}
                        onDelete={() => props.handleDeleteSerialNo(index)}
                      />
                    ))
                  }
                  freeSolo
                  renderInput={(params) => (
                    <TextValidator
                      className={defaultClassname}
                      {...params}
                      variant="standard"
                      label={t("Asset.serialNumber")}
                      error={isSerialNumbersError()}
                      helperText={getSerialNumbersHelperText()}
                    />
                  )}
                />
              ) : (
                <TextValidator
                  className={defaultClassname}
                  multiline
                  label={t("Asset.serialNumber")}
                  onChange={props?.handleChange}
                  InputProps={{
                    readOnly: permissions.serialNumber.isView
                  }}
                  type="text"
                  name="serialNumber"
                  value={props?.item?.serialNumber || ""}
                  disabled={permissions.serialNumber.isDisable || permissions.serialNumber.isView}
                />
              )}
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("InstrumentToolsList.model")}
                onChange={handleChange}
                type="text"
                name="model"
                value={props.item?.model || ""}
                disabled={permissions.model.isView}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.brand")}
                onChange={props?.handleChange}
                type="text"
                name="brand"
                value={props.item?.brand || ""}
                InputProps={{
                  readOnly: isView
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isFocus={true}
                className={defaultClassname}
                label={t("Asset.manufacturer")}
                searchFunction={getManufacturer}
                searchObject={searchObjectUnit}
                typeReturnFunction="category"
                displayLable="name"
                value={props.item.manufacturer}
                onSelect={props?.selectManufacturer}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                disabled={permissions.manufacturer.isView}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Autocomplete
                id="combo-box"
                size="small"
                className="mt-3"
                options={appConst.madeIn}
                onChange={(e, value) => handleChange(value, "isBuyLocally")}
                value={isBuyLocally ? appConst.madeIn[0] : appConst.madeIn[1]}
                getOptionLabel={(option) => option.name || ""}
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params} label={t("Asset.madeIn")} />
                )}
                disabled={permissions.isBuyLocally.isView}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.Nation")}
                onChange={handleChange}
                type="text"
                name="madeIn"
                value={props.item?.madeIn || ""}
                validators={["matchRegexp:^.{1,255}$"]}
                errorMessages={["Không nhập quá 255 ký tự"]}
                disabled={
                  props.item?.isBuyLocally?.code === appConst.madeInCode.TRONG_NUOC
                  || permissions.nation.isView
                }
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("InstrumentToolsList.circulationNumber")}
                onChange={props?.handleChange}
                disabled={permissions.circulationNumber.isView}
                name="circulationNumber"
                value={props?.item?.circulationNumber || ""}
                defaultValue={props?.item?.circulationNumber || ""}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Autocomplete
                id="combo-box"
                fullWidth
                size="small"
                className="mt-3"
                options={appConst.listRisk}
                onChange={(e, value) => props?.handleChangeSelect(
                  value?.value,
                  "riskClassification"
                )}
                disabled={permissions.riskClassification.isView}
                getOptionLabel={getOptionLabel}
                value={riskClassification ? riskClassification : null}
                filterOptions={filterOptions}
                renderInput={(params) => (
                  <TextValidator
                    isTabGeneralInfomation
                    {...params}
                    label={t("InstrumentToolsList.riskClassification")}
                    variant="standard"
                    value={riskClassification ? riskClassification : ""}
                  />
                )}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.yearOfManufacture")}
                onChange={handleChange}
                onKeyDown={handleKeyDownIntegerOnly}
                type="number"
                name="yearOfManufacture"
                value={props.item?.yearOfManufacture || ""}
                validators={["matchRegexp:^[1-9][0-9]{3}$"]}
                errorMessages={[t("general.yearOfManufactureError")]}
                disabled={permissions.yearOfManufacture.isView}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isFocus={true}
                className={defaultClassname}
                label={t("InstrumentToolsList.shoppingForm")}
                searchFunction={getShoppingForm}
                searchObject={searchObjectUnit}
                listData={props.item.listShoppingForm}
                nameListData="listShoppingForm"
                setListData={props.handleSetDataSelect}
                typeReturnFunction="category"
                displayLable={"name"}
                value={props.item?.shoppingForm}
                onSelect={props?.selectShoppingForm}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                noOptionsText={t("general.noOption")}
                disabled={permissions.shoppingForm.isView}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isFocus={true}
                className={defaultClassname}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("InstrumentToolsList.unit")}
                  </span>
                }
                searchFunction={getStockKeepingUnit}
                searchObject={searchObjectUnit}
                listData={props.item?.listUnit}
                setListData={(data) =>
                  props.handleSetDataSelect(data, "listUnit")
                }
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                displayLable={"name"}
                value={props.item?.unit}
                onSelect={props?.selectUnit}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                disabled={permissions.unit.isView}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={<span>{t("InstrumentToolsList.lotNumber")}</span>}
                onChange={handleChange}
                value={props.item?.lotNumber || ""}
                type="text"
                InputProps={{
                  readOnly: permissions.lotNumber.isView
                }}
                name="lotNumber"
                disabled={permissions.lotNumber.isDisable || permissions.lotNumber.isView}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatePicker
                className={defaultClassname}
                label={t("ReceivingScrollableTabsButtonForce.dateBill")}
                autoOk={false}
                format="dd/MM/yyyy"
                readOnly={permissions.dateBill.isView}
                value={item?.ngayHoaDon ? item?.ngayHoaDon : null}
                onChange={(date) =>
                  props.handleDateChange(date, "ngayHoaDon")
                }
                disabled={permissions.dateBill.isDisable || permissions.dateBill.isView}
                maxDate={new Date()}
                maxDateMessage={t("general.maxDateNow")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              {permissions.contract.isView ?
                <TextValidator
                  className={defaultClassname}
                  label={t("Asset.someContracts")}
                  variant="standard"
                  value={item?.contract?.contractName}
                  disabled={permissions.contract.isView}
                />
                :
                <Autocomplete
                  id="combo-box"
                  className="mt-3"
                  fullWidth
                  size="small"
                  name="contract"
                  options={listHopDong}
                  onChange={(e, value) =>
                    handleChangeSelectContract(value, "contract")
                  }
                  value={item?.contract || null}
                  getOptionLabel={(option) => option.contractName || ""}
                  getOptionSelected={getOptionSelected}
                  filterOptions={(options, params) =>
                    filterOptions(options, params, true, "contractName")
                  }
                  disabled={permissions.contract.isView}
                  renderInput={(params) => (
                    <TextValidator
                      className={defaultClassname}
                      {...params}
                      label={t("Asset.someContracts")}
                      variant="standard"
                      onChange={handleGetListContract}
                      onFocus={(e) => handleGetListContract(e, true)}
                    />
                  )}
                />
              }
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  className={defaultClassname}
                  margin="none"
                  id="mui-pickers-date"
                  label={t("Asset.DayContract")}
                  inputVariant="standard"
                  type="text"
                  autoOk={false}
                  format="dd/MM/yyyy"
                  name={"warrantyExpiryDate"}
                  value={item?.contract?.contractDate ?? null}
                  disabled
                  onChange={() => { }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Autocomplete
                id="combo-box"
                size="small"
                options={listSupply}
                onChange={(event, value) => props.handleSelectSupply(value)}
                value={props?.item?.supplyUnit || null}
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                renderInput={(params) => (
                  <TextValidator
                    className={defaultClassname}
                    {...params}
                    label={t("component.Supply.name")}
                    variant="standard"
                    onChange={(event) => {
                      props.handleSetDataSelect(event?.target?.value, "keySearch");
                      handleSearchSupplyUnit(event)
                    }}
                    onClick={(event) => handleSearchSupplyUnit(event)}
                  />
                )}
                noOptionsText={t("general.noOption")}
                disabled={
                  permissions.supplyUnit.isView
                  || !!(props.item?.contract?.id && props?.item?.contract?.supplier?.id)
                  || permissions.supplyUnit.isDisable
                  || item?.decisionCode?.decisionCode
                  || item?.decisionCode
                }
              />
            </Grid>
            <Grid item container xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                className={defaultClassname}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("Asset.managementDepartment")}
                  </span>
                }
                searchFunction={getListManagementDepartment}
                searchObject={searchObjectReceiverDepartment}
                listData={listManagementDepartment}
                setListData={setListManagementDepartment}
                displayLable={"name"}
                showCode="code"
                isNoRenderChildren
                isNoRenderParent
                readOnly={permissions.managementDepartment.isView}
                value={props?.item?.managementDepartment ?? null}
                onSelect={props.handleSelectManagementDepartment}
                typeReturnFunction="list"
                noOptionsText={t("general.noOption")}
                disabled={props?.item?.id || permissions.managementDepartment.isDisable}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isFocus={true}
                className={defaultClassname}
                label={
                  <Label isRequired={permissions.store.isRequire}>
                    {t("Asset.store")}
                  </Label>
                }
                searchFunction={storesSearchByPage}
                searchObject={searchObjectStore}
                displayLable={"name"}
                value={props.item.store}
                onSelect={props.selectStores}
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                disabled={
                  !props?.item?.managementDepartment?.id
                  || !!props?.item?.id
                  || permissions.store.isDisable
                }
                noOptionsText={t("general.noOption")}
                validators={permissions.store.isRequire ? ["required"] : []}
                errorMessages={[t("general.required")]}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatePicker
                id="mui-pickers-date"
                className={defaultClassname}
                label={<Label isRequired={Boolean(warrantyMonth > 0)}>{t("Asset.acceptanceDate")}</Label>}
                disabled={isView}
                autoOk
                format="dd/MM/yyyy"
                name="acceptanceDate"
                value={props.item?.acceptanceDate || null}
                onChange={(date) => props.handleDateChange(date, "acceptanceDate")}
                invalidDateMessage={t("general.invalidDateFormat")}
                validators={+warrantyMonth > 0 ? ["required"] : []}
                errorMessages={[t("general.required"),]}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                fullWidth
                label={t("Asset.warrantyMonth")}
                onKeyDown={handleKeyDownIntegerOnly}
                onChange={(e) => props?.handleChange(e, "warrantyMonth")}
                type="number"
                name="warrantyMonth"
                value={props?.item?.warrantyMonth}
                validators={["minNumber:0", "maxNumber:1000"]}
                errorMessages={[
                  t("general.warrantyMonthError"),
                  t("general.warrantyMonthError"),
                ]}
                disabled={permissions.warrantyMonth.isView}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ValidatePicker
                className={defaultClassname}
                label={t("Asset.warrantyExpiryDate")}
                name={"warrantyExpiryDate"}
                value={props?.item?.warrantyExpiryDate}
                disabled
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <AsynchronousAutocompleteSub
                isFocus={true}
                className={defaultClassname}
                label={t("MedicalEquipmentType.title")}
                searchFunction={getMedicalEquipment}
                searchObject={searchObjectUnit}
                listData={props.item.listMedicalEquipment}
                setListData={(data) =>
                  props.handleSetDataSelect(data, "listMedicalEquipment")
                }
                typeReturnFunction="category"
                displayLable={"name"}
                value={props.item.medicalEquipment || null}
                onInputChange={e => props.handleSetDataSelect(e.target.value, "keySearch")}
                onSelect={props?.selectMedicalEquipment}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                disabled={permissions.medicalEquipmentType.isView}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextValidator
                className={defaultClassname}
                label={t("Asset.installationLocation")}
                onChange={props?.handleChange}
                type="text"
                name="installationLocation"
                value={props.item?.installationLocation || ""}
                validators={["matchRegexp:^.{1,255}$"]}
                errorMessages={[t("general.errorInput255")]}
                InputProps={{
                  readOnly: isView && !allowdEditAssetUsing,
                }}
              />
            </Grid>
            
            <Grid item container xs={12} spacing={2} className="pl-16">
              <Grid item md={12} sm={12} xs={12} className="pl-0 mt-16 pb-0">
                <span className="font-weight-bold">Thông tin giá trị công cụ:</span>
              </Grid>
              <Grid item md={4} sm={12} xs={12} className="pl-0">
                <TextValidator
                  className={defaultClassname}
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("InstrumentToolsList.originalCost")}
                    </span>
                  }
                  onKeyDown={handleKeyDownIntegerOnly}
                  onChange={(e) =>
                    props.handleChangeFormatNumber(e, "originalCost")
                  }
                  name="originalCost"
                  value={props?.item?.originalCost ?? ""}
                  id="formatted-numberformat-originalCost"
                  disabled={permissions.originalCost.isView || permissions.originalCost.isDisable || isEdit}
                  InputProps={{
                    readOnly: props?.item?.isEdit,
                    inputComponent: NumberFormatCustom,
                    inputProps: {
                      className: "text-align-right",
                    },
                  }}
                  validators={["required", "minNumber:0"]}
                  errorMessages={[
                    t("general.required"),
                    t("general.required"),
                    t("general.nonNegativeNumber"),
                  ]}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className={defaultClassname}
                  label={t("Asset.unitPrice")}
                  name="unitPrice"
                  onChange={(e) =>
                    props.handleChangeFormatNumber(e, "unitPrice")
                  }
                  value={props.item.unitPrice ?? ""}
                  id="formatted-numberformat-originalCost"
                  InputProps={{
                    inputComponent: NumberFormatCustom,
                    inputProps: {
                      className: "text-align-right",
                    },
                  }}
                  disabled={!!id}
                />
              </Grid>
            </Grid>
          </Grid>

          {/* Cột ảnh và mã */}
          <Grid item xs={12} sm={4} md={2}>
            <Grid className="mt-12" item xs={12}>
              <div
                style={{
                  overflow: "hidden",
                  whiteSpace: "nowrap",
                  textOverflow: "ellipsis",
                  textAlign: "center",
                  fontWeight: "600",
                }}
                title={
                  props.item?.listCodes !== null && props.item?.listCodes !== ""
                    ? props.item?.listCodes
                    : props.item?.code
                }
              >
                {!props.item?.numberOfClones || props.item?.numberOfClones === 1
                  ? props.item?.code
                  : null}
              </div>
            </Grid>
            <Grid className="mt-12 mb-12" item xs={12}>
              <div
                style={{
                  overflow: "hidden",
                  whiteSpace: "nowrap",
                  textOverflow: "ellipsis",
                  textAlign: "center",
                  fontWeight: "600",
                }}
              >
                {props.item?.status?.name || null}
              </div>
            </Grid>

            {(props.item?.code) && !isView && (
              <div className="mt-12 text-center">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={props.item.isCreateQRCode}
                      onChange={(isCreateQRCode) =>
                        handleChange(isCreateQRCode, "isCreateQRCode")
                      }
                      disabled={permissions.isCreateQRCode.isView}
                    />
                  }
                  label={t("Asset.qr_code")}
                />
              </div>
            )}
            <div className="mt-12">
              {!isMultiple && (props.isCreateQRCode || props.item?.qrcode) && (
                <div className="flex-column flex-middle mt-16 ">
                  <div style={{ height: 155, width: 175 }}>
                    <QRCodeImage item={item} height={175} width={175} />
                  </div>
                  <div>
                    <Button
                      size="small"
                      variant="text"
                      color="primary"
                      onClick={() => handleChangeValueOpenQR(true)}
                    >
                      {t("Asset.PrintQRCode")}
                    </Button>
                  </div>
                  {openPrintQR && (
                    <AssetQRPrintNew
                      t={t}
                      i18n={i18n}
                      handleClose={handleChangeValueOpenQR}
                      open={openPrintQR}
                      qrValue={qrInfo}
                      item={item}
                    />
                  )}
                </div>
              )}
            </div>

            <RadioGroup
              aria-label="isManageAccountant"
              name="isManageAccountant"
              onChange={(isManageAccountant) =>
                handleChange(isManageAccountant, "isManageAccountant")
              }
              style={{ display: "inline" }}
            >
              <div className="flex flex-middle">
                <FormControlLabel
                  value={accountantOrTemporaryValue.isManageAccountant}
                  control={<Radio checked={props.item?.isManageAccountant} />}
                  label={t("InstrumentToolsList.isManageAccountant")}
                  className="mr-5"
                  disabled={permissions.isManageAccountant.isView}
                />
                <NotePopup
                  typeIndex={appConst.NOTE_INDEX.IS_MANAGE_ACCOUNTANT}
                  assetClass={appConst.assetClass.CCDC}
                />
              </div>

              <div className="flex flex-middle">
                <FormControlLabel
                  value={accountantOrTemporaryValue.isTemporary}
                  control={<Radio checked={props.item?.isTemporary} />}
                  label={t("InstrumentToolsList.isTemporary")}
                  className="mr-5"
                  disabled={permissions.isTemporary.isView}
                />
                <NotePopup
                  typeIndex={appConst.NOTE_INDEX.IS_TEMPOARY}
                  assetClass={appConst.assetClass.CCDC}
                />
              </div>
            </RadioGroup>
          </Grid>

          {/* Ghi chú và thông tin phòng ban sd */}
          <Grid item xs={12} sm={8} md={12} className="mb-8 pr-24">
            <TextValidator
              className={defaultClassname}
              label={t("Asset.note")}
              onChange={props?.handleChange}
              type="text"
              name="note"
              InputProps={{
                readOnly: isView && !allowdEditAssetUsing,
              }}
              value={props?.item?.note || ""}
              validators={["matchRegexp:^.{1,255}$"]}
              errorMessages={["Không nhập quá 255 ký tự"]}
            />
          </Grid>
          {props.item?.useDepartment?.id && (
            <>
              <Grid item xs={12} sm={12} md={12}>
                <Grid className="mt-12" item xs={12}>
                  <label>{t("Asset.UsageInformation")}</label>
                </Grid>
              </Grid>

              <Grid item xs={12} sm={6} md={4}>
                <FormControl component="fieldset">
                  <FormLabel component="legend" disabled={true}>
                    {t("Asset.PropertyGrantedTo")}
                  </FormLabel>
                  <FormControlLabel
                    style={{ float: "right" }}
                    value={item?.allocationFor}
                    className="mb-16"
                    name="allocationFor"
                    onChange={(allocationFor) =>
                      handleChange(allocationFor, "allocationFor")
                    }
                    control={<Checkbox checked={item?.allocationFor} disabled={!allowdEditAssetUsing || isViewRoleAssetUser} />}
                    label={t("Asset.allocationFor.person")}
                  />
                </FormControl>
              </Grid>

              <Grid item container xs={12} sm={6} md={4} className="pt-12">
                <Grid item md={10} sm={10} xs={10}>
                  <TextValidator
                    className={defaultClassname}
                    size="small"
                    disabled={true}
                    InputProps={{
                      readOnly: true,
                    }}
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("Asset.useDepartment")}
                      </span>
                    }
                    value={
                      props.item.useDepartment != null
                        ? props.item.useDepartment.name
                        : ""
                    }
                  />
                </Grid>
                <Grid item md={2} sm={2} xs={2}>
                  <Button
                    size="small"
                    style={{ float: "right" }}
                    className="w-85 mt-10"
                    variant="contained"
                    color="primary"
                    onClick={props.openSelectUseDepartmentPopup}
                    disabled={!props?.isAddAsset || permissions.buttonUseDepartment.isDisable}
                  >
                    {t("general.select")}
                  </Button>
                </Grid>
              </Grid>

              {/* <Grid item xs={12} sm={6} md={4}>
                <Grid item xs={12}>
                  <TextValidator
                  isTabGeneralInfomation
                    className="w-100"
                    label={t("Asset.User")}
                    type="text"
                    name="nguoiSD"
                    value={item?.usePerson?.displayName || ""}
                    disabled={true}
                  />
                </Grid>
              </Grid> */}
              <Grid item md={3} sm={6} xs={12} >
                <Grid className="" item xs={12}>
                  <AsynchronousAutocompleteSub
                    className={defaultClassname}
                    label={
                      <Label isRequired={item?.allocationFor}>
                        {t("Asset.User")}
                      </Label>
                    }
                    listData={listUsePerson}
                    setListData={setListUsePerson}
                    disabled={!allowdEditAssetUsing || !item?.allocationFor}
                    searchFunction={getListUserByDepartmentId}
                    searchObject={searchParamUserByUseDepartment}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    readOnly={(permissions.usePerson.isView && !allowdEditAssetUsing) || isViewRoleAssetUser}
                    value={item?.usePerson ?? ''}
                    onSelect={usePerson => props?.handleSelectUsePerson(usePerson)}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                    validators={item?.allocationFor ? ["required"] : []}
                    errorMessages={[t('general.required')]}
                  />
                </Grid>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
    </>
  );
};

GeneralInfomation.propTypes = {};

export default GeneralInfomation;
