import React from 'react';
import { Grid } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import CustomValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { NumberFormatCustom, checkInvalidDate, convertNumberPriceRoundUp } from "../../../appFunction";
import { useTranslation } from "react-i18next";
import NotePopup from 'app/views/Component/NotificationPopup/NotePopup';
import {appConst, variable} from 'app/appConst';
import DialogNote from 'app/views/Component/DialogNote/DialogNote';


function AllocationInformation(props) {
	const { t } = useTranslation();
	let {
		numberOfAllocations,
		unitPrice,
	} = props.item;

	let allocationRate = numberOfAllocations ? (Math.round(100 / numberOfAllocations * 100) / 100) : 0;
	let allocationPeriod = appConst.listAllocationPeriod.find(item => item.code === props?.item?.allocationPeriod);
	let checkAllocation = appConst.ALLOCATION_PERIOD.ONCE.code === allocationPeriod?.code;
	let allocationPeriodValue = 
		checkAllocation 
		// ? props?.item?.originalCost
		// : Math.ceil(Number(unitPrice ?? 0) / Number(numberOfAllocations ?? 1)) ?? "";
		? props?.item?.originalCost
		: Math.ceil(Number(props?.item?.originalCost ?? 0) / Number(numberOfAllocations ?? 1)) ?? "";

	const handleBlurDate = (date, name) => {
		if (checkInvalidDate(date)) {
			return props.handleDateChange(null, name)
		}
	}

	const handleKeyDown = (e) => {
		if (variable.regex.numberExceptThisSymbols.includes(e?.key)) {
			e.preventDefault();
		}
	};

	const handleKeyDownPreventDecimal = (e) => {
		if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
			e.preventDefault();
		}
	};

	return (
		<div className='mx-18 pb-24'>
			<Grid container spacing={2}>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={
							<span>
								<span className="colorRed">*</span>
								{t("InstrumentToolsList.originalCost")}
							</span>
						}
						onKeyDown={handleKeyDown}
						onChange={(e) => props.handleChangeFormatNumber(e, "originalCost")}
						name="originalCost"
						value={props?.item?.originalCost ? props?.item?.originalCost : null}
						defaultValue={props?.item?.originalCost ? props?.item?.originalCost : null}
						id="formatted-numberformat-originalCost"
						InputProps={{
							readOnly: props?.item?.isEdit,
							inputComponent: NumberFormatCustom,
							inputProps: {
								className: "text-align-right",
							}
						}}
						validators={["required", "minNumber:1"]}
						errorMessages={[t("general.required"), t("general.minNumberError"), ]}
					/>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={t("InstrumentToolsList.percentDistribution")}
						type="number"
						name="allocationRate"
						defaultValue={allocationRate}
						value={allocationRate ? allocationRate : 0}
						disabled={true}
						InputProps={{
							inputProps: {
								className: "text-center",
							}
						}}
					/>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={t("Asset.unitPrice")}
						name="unitPrice"
						value={unitPrice ?? ""}
						id="formatted-numberformat-originalCost"
						InputProps={{
							inputComponent: NumberFormatCustom,
							inputProps: {
								className: "text-align-right",
							}
						}}
						disabled={!!props?.item?.id}
					/>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<div className="flex">
						<div className='w-100'>
							<CustomValidatePicker
								margin="none"
								fullWidth
								id="date-picker-dialog"
								style={{ marginTop: "2px" }}
								label={
									<span>
										{/* {
											(numberOfAllocations || props?.item?.allocationPeriod)
											&& <span className="colorRed">*</span>
										} */}
										{t("InstrumentToolsList.depreciationDate")}
									</span>
								}
								inputVariant="standard"
								autoOk={true}
								type="text"
								maxDateMessage={t('EQAPlanning.messRegistrationStartDate')}
								format="dd/MM/yyyy"
								value={props?.item?.depreciationDate}
								onChange={(date) => props.handleDateChange(date, "depreciationDate")}
								KeyboardButtonProps={{
									"aria-label": "change date",
								}}
								invalidDateMessage={t("general.invalidDateFormat")}
								minDate={new Date(props?.item?.dateOfReception)}
								minDateMessage={"Ngày tính phân bổ không được nhỏ hơn ngày tiếp nhận"}
								onBlur={() => handleBlurDate(props?.item?.depreciationDate, "depreciationDate")}
								clearable
								clearLabel={t("general.remove")}
								cancelLabel={t("general.cancel")}
								okLabel={t("general.select")}
								// validators={
								// 	(numberOfAllocations || props?.item?.allocationPeriod)
								// 	&& ["required"]
								// }
								errorMessages={[t("general.required"), ]}
							/>
						</div>
						<div
							style={{
								display: "flex",
								alignItems: "center",
								justifyContent: "center",
							}}
						>
							<DialogNote
								content={appConst.CCDC_NOTE_MESSAGE[4].message}
								className="mt-16"
							/>
						</div>
					</div>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<div className="flex w-100">
						<Autocomplete
							id="combo-box"
							fullWidth
							size="small"
							className='mt-3'
							options={appConst.listAllocationPeriod}
							onChange={(e, data) => props?.handleChangeSelect(
								data?.code, 
								"allocationPeriod"
							)}
							getOptionLabel={(option) => option.name || ''}
							value={allocationPeriod ? allocationPeriod : null}
							renderInput={(params) =>
								<TextValidator
									{...params}
									label={
										<span>
											{
												(numberOfAllocations || props?.item?.depreciationDate)
												&& <span className="colorRed">*</span>
											}
											{t("InstrumentToolsList.allocation")}
										</span>
									}
									variant="standard"
									value={allocationPeriod?.name ? allocationPeriod?.name : ""}
									validators={["required"]}
									errorMessages={[t("general.required"), ]}
								/>
							}
						/>
						<span className="mt-16">
							<NotePopup
								typeIndex={appConst.NOTE_INDEX.IS_ALLOCATION}
								assetClass={appConst.assetClass.CCDC}
							/>
						</span>
					</div>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						fullWidth
						label={
							<span>
								{/* {
									(props?.item?.allocationPeriod || props?.item?.depreciationDate)
									&& <span className="colorRed">*</span>
								} */}
								<span className="colorRed">*</span>
								{t("InstrumentToolsList.totalAllotmentTime")}
							</span>
						}
						defaultValue={numberOfAllocations ? numberOfAllocations : ""}
						value={numberOfAllocations ? numberOfAllocations : ""}
						type="number"
						name="numberOfAllocations"
						onKeyDown={handleKeyDownPreventDecimal}
						onChange={(numberOfAllocations) => props.handleChange(
							numberOfAllocations, 
							"numberOfAllocations"
						)}
						validators={["minNumber:1", "required"]}
						errorMessages={[
							t("InstrumentToolsList.InvalidNumberOfAllocations"), 
							t("general.required")
						]}
						disabled={checkAllocation}
						InputProps={{
							endAdornment: (
								<NotePopup
									typeIndex={appConst.NOTE_INDEX.IS_TOTAL_ALLOCATION_TIME}
									assetClass={appConst.assetClass.CCDC}
								/>
							),
						}}
					/>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={t("InstrumentToolsList.amortizedValue")}
						name="allocationPeriodValue"
						value={!isNaN(allocationPeriodValue) ? allocationPeriodValue : 0}
						InputProps={{
							inputComponent: NumberFormatCustom,
							inputProps: {
								className: "text-align-right",
							}
						}}
						defaultValue={allocationPeriodValue ? allocationPeriodValue : ""}
						disabled={true}
					/>

				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={t("InstrumentToolsList.remainingAllotmentTime")}
						type="number"
						value={props.item?.numberOfAllocationsRemaining ? props.item?.numberOfAllocationsRemaining : 0}
						defaultValue={props.item?.numberOfAllocationsRemaining}
						disabled={true}
					/>
				</Grid>
				<Grid item md={4} sm={12} xs={12}>
					<TextValidator
						className="w-100"
						label={t("Asset.carryingAmount")}
						onChange={props.handleChangeFormatNumber}
						name="carryingAmount"
						value={convertNumberPriceRoundUp(props.item.carryingAmount)}
						id="formatted-numberformat-carryingAmount"
						InputProps={{
							inputComponent: NumberFormatCustom,
							inputProps: {
								className: "text-align-right",
							}
						}}
						disabled={true}

					/>
				</Grid>
			</Grid>
		</div>
	);
}

export default AllocationInformation;