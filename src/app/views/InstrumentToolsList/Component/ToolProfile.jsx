import React from 'react';
import {Button, Grid, Icon, IconButton} from "@material-ui/core";
import VoucherFilePopup from "../../Component/AssetFile/VoucherFilePopup";
import MaterialTable, {MTableToolbar} from "material-table";
import {useTranslation} from "react-i18next";
import {LightTooltip} from "../../Component/Utilities";


function ToolProfile(props) {
	const {t, i18n} = useTranslation();
	
	let columnsAssetFile = [
		{
			title: t("general.action"),
			field: "valueText",
			hidden: props?.item?.isView,
			maxWidth: 150,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => (
				<div className="none_wrap">
					<LightTooltip
						title={t("general.editIcon")}
						placement="top"
						enterDelay={300}
						leaveDelay={200}
					>
						<IconButton
							size="small"
							onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
						>
							<Icon fontSize="small" color="primary">
								edit
							</Icon>
						</IconButton>
					</LightTooltip>
					<LightTooltip
						title={t("general.deleteIcon")}
						placement="top"
						enterDelay={300}
						leaveDelay={200}
					>
						<IconButton
							size="small"
							onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}
						>
							<Icon fontSize="small" color="error">
								delete
							</Icon>
						</IconButton>
					</LightTooltip>
				</div>
			),
		},
		{
			title: t("general.stt"),
			field: "code",
			maxWidth: 50,
			align: "left",
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData.tableData.id + 1,
		},
		{
			title: t("AssetFile.code"),
			field: "code",
			align: "left",
			hidden: true,
			minWidth: 120,
			maxWidth: 200,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("AssetFile.name"),
			field: "name",
			align: "left",
			minWidth: 350,
		},
		{
			title: t("AssetFile.description"),
			field: "description",
			align: "left",
			minWidth: 350,
		},
	];
	
	return (
		<div className='mx-18'>
				<Grid container>
					<Grid item md={12} sm={12} xs={12}>
						{
							!props?.item?.isView &&
							<Button
								variant="contained"
								color="primary"
								onClick={props.handleAddAssetDocumentItem}
							>
								{t("InstrumentToolsList.addProfile")}
							</Button>
						}
						
						{props.item?.shouldOpenPopupAssetFile && (
							<VoucherFilePopup
								isEditAssetDocument={props?.item?.isEditAssetDocument}
								open={props.item.shouldOpenPopupAssetFile}
								handleClose={props.handleAssetFilePopupClose}
								itemAssetDocument={props.itemAssetDocument}
								getAssetDocument={props.getAssetDocument}
								handleUpdateAssetDocument={props.handleUpdateAssetDocument}
								documentType={props?.item?.documentType}
								item={props.item?.item}
								isIAT={props?.item?.isInstrumentTools}
								t={t}
								i18n={i18n}
								configItem={(item) => ({...item})}
							/>
						)}
					</Grid>
					<Grid item md={12} sm={12} xs={12} className="mt-16">
						<MaterialTable
							data={props.item.documents || []}
							columns={columnsAssetFile}
							localization={{
								body: {
									emptyDataSourceMessage: `${t(
										"general.emptyDataMessageTable"
									)}`,
								},
							}}
							options={{
								draggable: false,
								toolbar: false,
								selection: false,
								actionsColumnIndex: -1,
								paging: false,
								search: false,
								sorting: false,
								padding: "dense",
								rowStyle: (rowData) => ({
									backgroundColor:
										rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
								}),
								headerStyle: {
									backgroundColor: "#358600",
									color: "#fff",
									paddingLeft: 10,
									paddingRight: 10,
									textAlign: "center",
								},
								maxBodyHeight: "343px",
								minBodyHeight: "343px",
							}}
							components={{
								Toolbar: (props) => (
									<div style={{width: "100%"}}>
										<MTableToolbar {...props} />
									</div>
								),
							}}
							onSelectionChange={(rows) => {
								this.data = rows;
							}}
						/>
					</Grid>
				</Grid>
		</div>
	);
}

export default ToolProfile;