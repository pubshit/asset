import React from 'react';
import { Button, Grid, Icon, IconButton } from "@material-ui/core";
import SelectProductAttributespopup from "../../Component/ProductAttribute/SelectProductAttributespopup";
import MaterialTable, { MTableToolbar } from "material-table";
import { TextValidator } from "react-material-ui-form-validator";
import { useTranslation } from "react-i18next";
import {appConst, variable} from 'app/appConst';
import { LightTooltip } from 'app/appFunction';


function InstrumentToolProperties(props) {
	const { t, i18n } = useTranslation();
	const {
		isView,
	} = props.item;

	let columnsAttributes = [
		{
			title: t("general.action"),
			field: "valueText",
			align: "left",
			hidden: isView,
			mixWidth: 120,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => (
				<LightTooltip
					title={t("general.delete")}
					placement="top"
					enterDelay={300}
					leaveDelay={200}
				>
					<IconButton
						size="small"
						onClick={() => props.handleRowDataCellDelete(rowData)}
					>
						<Icon fontSize="small" color="error">
							delete
						</Icon>
					</IconButton>
				</LightTooltip>
			),
		},
		{
			title: t("general.stt"),
			field: "code",
			align: "left",
			maxWidth: 50,
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData.tableData.id + 1,
		},
		{
			title: t("ProductAttribute.code"),
			field: "attribute.code",
			align: "left",
			minWidth: 250,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("ProductAttribute.name"),
			field: "attribute.name",
			align: "left",
			minWidth: 400,
		},
		{
			title: t("ProductAttribute.valueText"),
			field: "valueText",
			align: "left",
			minWidth: 250,
			render: (rowData) => (
				<TextValidator
					multiline
					rowsMax={2}
					className="w-100 isTabInstrumentToolProperties"
					onKeyDown={(e) => {
						variable.regex.decimalNumberExceptThisSymbols.includes(e.key) &&
						  e.preventDefault();
					  }}
					onChange={(valueText) =>
						props.handleRowDataCellChange(rowData, valueText)
					}
					InputProps={{
						readOnly: isView,
					}}
					type="text"
					name="valueText"
					lableText={t("ProductAttribute.valueText")}
					value={rowData?.valueText || ""}
					validators={["required"]}
					errorMessages={[t("general.required")]}
				/>
			),
		},
	];

	return (
		<div className='mx-18'>
			<Grid container>
				<Grid item md={12} sm={12} xs={12}>
					{!isView && (
						<Button
							variant="contained"
							color="primary"
							onClick={props.openPopupSelectAttribute}
						>
							{t("InstrumentToolsList.addProperties")}
						</Button>
					)}
					{props.item.shouldOpenPopupSelectAttribute && (
						<SelectProductAttributespopup
							open={props.item.shouldOpenPopupSelectAttribute}
							handleSelect={props.handleAddAttribute}
							attributes={props.item.attributes || []}
							handleClose={props.handleSelectAttributePopupClose}
							t={t}
							i18n={i18n}
						/>
					)}
				</Grid>
				<Grid item md={12} sm={12} xs={12} className="mt-16">
					<MaterialTable
						data={props?.item?.attributes ? props?.item?.attributes : []}
						columns={columnsAttributes}
						options={{
							draggable: false,
							toolbar: false,
							selection: false,
							actionsColumnIndex: -1,
							paging: false,
							search: false,
							sorting: false,
							rowStyle: (rowData) => ({
								backgroundColor:
									rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
							}),
							maxBodyHeight: "320px",
							minBodyHeight: "320px",
							headerStyle: {
								backgroundColor: "#358600",
								color: "#fff",
							},
							padding: "dense",
						}}
						components={{
							Toolbar: (props) => (<MTableToolbar {...props} />),
						}}
						localization={{
							body: {
								emptyDataSourceMessage: t("general.emptyDataMessageTable"),
							},
						}}
					/>
				</Grid>
			</Grid>
		</div>
	);
}

export default InstrumentToolProperties;