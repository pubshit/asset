import {
  Button,
  IconButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
} from "@material-ui/core";
import React, { Component } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import ScrollableTabsButtonForce from "./ScrollableTabsButtonForce";
import {
  createAssetCCDC,
  createAssetCCDCMultiple,
  getAssetDocumentById,
  getNewCode,
  getNewCodes,
  updateAsset,
} from "./InstrumentToolsListService";
import {
  searchByPage as statusSearchByPage
} from "../AssetStatus/AssetStatusService";
import { personSearchByPage } from "../AssetAllocation/AssetAllocationService";
import { appConst, variable } from "app/appConst";
import {
  checkInvalidDate,
  checkMinDate,
  compareDate,
  formatDateDto,
  getAssetStatus,
  getTheHighestRole, getUserInformation,
} from "app/appFunction";
import { getNewCodeDocument } from "../Asset/AssetService";
import AppContext from "app/appContext";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import StockKeepingUnitEditorDialog from "../StockKeepingUnit/StockKeepingUnitEditorDialog";
import SelectProductPopup from "../Component/Product/SelectProductPopup";
import StoreEditorDialog from "../Store/StoreEditorDialog";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import CommonObjectDialog from "../CommonObject/CommonObjectDialog";
import ShoppingFormDialog from "../ShoppingForm/ShoppingFormDialog";
import MedicalEquipmentTypeDialog from "../MedicalEquipmentType/MedicalEquipmentTypeDialog";
import AssetAllocationEditorDialog from "../AssetAllocation/AssetAllocationEditorDialog";
import ContractDialog from "../Contract/ContractDialog";
import moment from "moment";
import ProductDialog from "../Product/ProductDialog";
import { getNewCode as getNewCodeProduct } from "../Product/ProductService";
import localStorageService from "../../services/localStorageService";
import InstrumentAndToolsTypeDialog from "../InstrumentAndToolsType/InstrumentAndToolsTypeDialog";
import { isValidDate } from "../../appFunction";
import SupplierDialog from "../Supplier/SupplierDialog";
import { searchByPage as productTypeSearchByPage } from "../ProductType/ProductTypeService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  formControl: {
    margin: theme.spacing(),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  dialogTitle: {
    position: "relative",
    cursor: "move",
    paddingTop: "5px",
    paddingBottom: "5px",
    background: "#fff",
  },
  dialogContent: {
    overflow: "auto",
    maxHeight: "78vh",
    paddingTop: "0px",
    paddingBottom: "0px",
    background: "#f5f5f5",
  },
});

class InstrumentToolsListEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    qrcode: "",
    managementCode: "",
    receiverPerson: null,
    madeIn: "",
    model: "",
    usedTime: 0,
    warrantyMonth: "",
    yearOfManufacture: 0,
    serialNumber: "",
    serialNumbers: [],
    serialNumberOptions: [],
    isMultiple: false,
    useDepartment: null,
    managementDepartment: null,
    installationLocation: "",
    supplyUnit: null,
    dayStartedUsing: new Date(),
    dateOfReception: new Date(),
    assetSource: null,
    status: null,
    originalCost: 0,
    carryingAmount: 0,
    warrantyExpiryDate: null,
    depreciationDate: null,
    depreciationPeriod: 0,
    depreciationRate: 0, //tl phân bổ
    accumulatedDepreciationAmount: 0,
    yearPutIntoUse: null,
    assetGroup: null,
    manufacturer: null,
    isEditCode: false,
    isCreateQRCode: false,
    checkUseDepartmentByStatus: false,
    shouldOpenPersonPopup: false,
    shouldOpenSelectUsePersonPopup: false,
    shouldOpenSelectUseDepartmentPopup: false,
    shouldOpenSelectAssetGroupPopup: false,
    shouldOpenSelecPopup: false,
    usePerson: null,
    displayName: null,
    attributes: [],
    shouldOpenPopupSelectAttribute: false,
    shouldOpenAssetAllocationEditorDialog: false,
    unitPrice: 0,
    openAssetAllocationPopup: false,
    shouldOpenSelectUserDepartment: false,
    shouldOpenPopupAssetFile: false,
    assetDocumentList: [],
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    isEditAssetDocument: false,
    isManageAccountant: false,
    isTemporary: false,
    isIatReception: false,
    storage: [],
    quantity: "",
    quantityCode: 1,
    listCodes: "",
    store: null,
    loading: false,
    listAssetDocumentId: [],
    AssetStatusNew: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder, // mới
    AssetStatusAllocation: appConst.STATUS_ALLOCATION.TRA_VE.indexOrder, // đã cấp phát
    documentType: appConst.documentType.IAT_DOCUMENT,
    productTypeCode: appConst.productTypeCode.CCDC,
    shouldOpenPopupSelectAssetSource: false,
    assetSources: [],
    isNew: false,
    circulationNumber: null,
    lotNumber: null, //số lô
    unit: null, // đơn vị
    riskClassification: null, //phân loại rủi ro
    isBuyLocally: null,
    isMedicalEquipment: false,
    shoppingForm: null,
    medicalEquipment: null,
    numberOfAllocationsRemaining: null,
    numberOfAllocations: null,
    allocationPeriod: null,
    assetGroupId: null,
    supplyUnitId: null,
    managementDepartmentId: null,
    manufacturerId: null,
    unitId: null,
    shoppingFormId: null,
    medicalEquipmentId: null,
    storeId: null,
    productId: null,
    assetClass: appConst.assetClass.CCDC,
    shouldOpenAllocationEditorDialog: false,
    InstrumentsToolsAllocation: null,
    documents: [],
    shouldOpenDialogStore: false,
    shouldOpenDialogManufacturer: false,
    shouldOpenDialogShoppingForm: false,
    shouldOpenDialogUnit: false,
    shouldOpenDialogMedicalEquipment: false,
    isInstrumentTools: true,
    searchText: "",
    quantityAsset: "",
    product: null,
    shouldOpenProductDialog: false,
    searchProduct: "",
    usePersonId: null,
    allocationFor: null,
    permissions: {
      saveButton: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      dateOfReception: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      managementCode: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      selectProduct: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      productName: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      typeCCDC: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      quantity: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      serialNumber: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      model: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      manufacturer: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      isBuyLocally: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      nation: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      circulationNumber: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      riskClassification: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      yearOfManufacture: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      shoppingForm: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      unit: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      lotNumber: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      dateBill: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      contract: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      contractDate: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      supplyUnit: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      managementDepartment: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      store: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      originalCost: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      warrantyMonth: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      warrantyExpiryDate: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      medicalEquipmentType: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      isCreateQRCode: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      isManageAccountant: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      note: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      isTemporary: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      usePerson: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      buttonUseDepartment: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
      buttonSave: {
        isDisable: false,
        isRequire: false,
        isShow: false,
        isView: false
      },
    },
    objectMessage: {
      generalInformationMessage: {

      },
      originToolsMessage: {

      },
      instrumentToolPropertiesMessage: {

      },
      toolProfileMessage: {

      }
    },
    keySearch: "",
    isErrorGeneralInfomation: false,
    isErrorOriginTools: false,
    isErrorInstrumentToolProperties: false,
  };

  convertDto = (state) => {
    let assetSourcesUpdated = state.assetSources.map((item) => {
      item.assetSourceId = item?.assetSource?.id || item?.id;
      item.assetSourceName = item?.assetSource?.name;
      return item;
    });
    if (!state?.isMultiple) {
      state?.serialNumber
        ? state?.serialNumbers?.push(state?.serialNumber)
        : (state.serialNumbers = null);
    }
    return {
      accumulatedDepreciationAmount: state?.accumulatedDepreciationAmount,
      allocationPeriod: state?.allocationPeriod,
      allocationPeriodValue: state?.allocationPeriodValue,
      assetGroupId: state?.assetGroupId
        ? state?.assetGroupId
        : state?.assetGroup?.id,
      assetSources: assetSourcesUpdated,
      attributes: state?.attributes,
      carryingAmount: Number(state?.carryingAmount),
      circulationNumber: state?.circulationNumber,
      code: state?.code,
      countryOriginId: state?.countryOriginId
        ? state?.countryOriginId
        : state?.countryOrigin?.id,
      contract: state?.contract,
      contractId: state?.contract?.contractId,
      contractDate: state?.contract?.contractDate,
      supplyUnit: state?.contract?.supplier,
      dateOfReception: state?.dateOfReception ? formatDateDto(state?.dateOfReception) : null,
      dayStartedUsing: state?.dayStartedUsing ? formatDateDto(state?.dayStartedUsing) : null,
      depreciationDate: state?.depreciationDate ? formatDateDto(state?.depreciationDate) : null,
      depreciationPeriod: state?.depreciationPeriod,
      depreciationRate: state?.depreciationRate,
      installationLocation: state?.installationLocation,
      invoiceNumber: state?.invoiceNumber,
      isBuyLocally: state?.isBuyLocally,
      isManageAccountant: state?.isManageAccountant,
      isTemporary: state?.isTemporary,
      listAssetDocumentId: state?.listAssetDocumentId,
      lotNumber: state?.lotNumber,
      madeIn: state?.madeIn,
      managementCode: state?.managementCode,
      managementDepartmentId: state?.managementDepartmentId
        ? state?.managementDepartmentId
        : state?.managementDepartment?.id,
      manufacturerId: state?.manufacturerId
        ? state?.manufacturerId
        : state?.manufacturer?.id,
      medicalEquipmentId: state?.medicalEquipmentId
        ? state?.medicalEquipmentId
        : state?.medicalEquipment?.id,
      model: state?.model,
      name: state?.name,
      ngayHoaDon: state?.ngayHoaDon ? formatDateDto(state?.ngayHoaDon) : null,
      note: state?.note,
      numberOfAllocations: state?.numberOfAllocations,
      numberOfAllocationsRemaining: state?.numberOfAllocationsRemaining,
      originalCost: state?.originalCost,
      productId: state?.productId ? state?.productId : state?.product?.id,
      qrcode: state?.qrcode,
      quantityAsset: +state?.quantityAsset,
      quantity: +state?.quantity,
      receiverPersonId: state?.receiverPersonId
        ? state?.receiverPersonId
        : state?.receiverPerson?.id,
      riskClassification: state?.riskClassification,
      serialNumber: state?.serialNumber,
      serialNumbers: state?.serialNumbers,
      shoppingFormId: state?.shoppingFormId
        ? state?.shoppingFormId
        : state?.shoppingForm?.id,
      storeId: state?.storeId ? state?.storeId : state?.store?.id,
      supplyUnitId: state?.supplyUnitId
        ? state?.supplyUnitId
        : state?.supplyUnit?.id,
      unitId: state?.unitId ? state?.unitId : state?.unit?.id,
      unitPrice: state?.unitPrice,
      useDepartmentId: state?.useDepartmentId
        ? state?.useDepartmentId
        : state?.useDepartment?.id,
      useDepartmentLateId: state?.useDepartmentLateId
        ? state?.useDepartmentLateId
        : state?.useDepartmentLate?.id,
      usePersonId: state?.usePersonId || state?.usePerson?.personId,
      usedTime: state?.usedTime,
      warrantyExpiryDate: formatDateDto(state?.warrantyExpiryDate),
      warrantyMonth: state?.warrantyMonth,
      yearOfManufacture: state?.yearOfManufacture,
      yearPutIntoUse: state?.yearPutIntoUse,
      acceptanceDate: state?.acceptanceDate ? formatDateDto(state?.acceptanceDate) : null,
      decisionCode: state?.decisionCode?.decisionCode,
      symbolCode: state?.symbolCode,
      brand: state?.brand,
    };
  };

  handleDateChange = (date, name) => {
    if (variable.listInputName.acceptanceDate === name) {
      let newDate = !checkInvalidDate(date) ? new Date(date) : null;
      newDate?.setMonth(
        newDate.getMonth()
        + Number(this.state?.warrantyMonth ? this.state?.warrantyMonth : 0)
      );
      let warrantyExpiryDate = newDate || null;
      this.setState({
        [name]: date,
        warrantyExpiryDate: date ? warrantyExpiryDate : null,
      });
      return;
    }

    this.setState({
      [name]: date,
    });
  };

  handleChangeSelect = (value, source) => {
    this.setState({ [source]: value });
  };

  handleChangeFormatNumber = (event, source) => {
    let { assetSources } = this.state;

    if (variable.listInputName.originalCost === event.target.name) {
      let originalCostValue = event.target.value;
      let priceNumber = Math.ceil(
        Number(originalCostValue) / (this.state.quantity || 1)
      );
      assetSources?.map(
        (as) => (as.value = ((as.percentage ?? 0) * originalCostValue) / 100)
      );
      this.setState({
        assetSources: +originalCostValue === 0 ? [] : assetSources,
        unitPrice: priceNumber,
        [event.target.name]: originalCostValue,
      });
      return;
    }
    if (variable.listInputName.unitPrice === source) {
      let unitPriceValue = event.target.value;
      let originalCostPrice = Math.ceil(
        Number(unitPriceValue) * this.state.quantity
      );
      assetSources?.map(
        (as) => (as.value = ((as.percentage ?? 0) * originalCostPrice) / 100)
      );
      this.setState({
        assetSources,
        originalCost: originalCostPrice,
        [event.target.name]: Math.ceil(unitPriceValue)
      });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleChangeQuantity = (event) => {
    this.setState({
      quantityCode: event.target.value,
    });
    if (event.target.value && parseInt(event.target.value) > 0) {
      this.openCircularProgress();
      getNewCodes(event.target.value)
        .then((result) => {
          if (result != null && result.data && result.data.listCodes) {
            this.setState({
              listCodes: result.data.listCodes,
              loading: false,
            });
          }
        })
        .catch(() => {
          toast.warning(this.props.t("general.error_reload_page"));
        });
    }
  };

  handleChange = (event, source) => {
    let { t } = this.props;
    let {
      madeIn,
      isMultiple,
      serialNumber,
      serialNumbers,
      isMedicalEquipment,
      permissions,
      unitPrice,
      originalCost,
    } = this.state;

    if (variable.listInputName.isBuyLocally === source) {
      madeIn = event?.code === appConst.madeInCode.TRONG_NUOC
        ? "Việt Nam"
        : "";
      this.setState({ isBuyLocally: !this.state?.isBuyLocally, madeIn });
      return;
    }
    if (variable.listInputName.isMedicalEquipment === source) {
      isMedicalEquipment = event.target.checked;
      this.setState({ isMedicalEquipment: isMedicalEquipment });
      return;
    }
    if (variable.listInputName.isMultiple === source) {
      isMultiple = event.target.checked;
      permissions.quantity.isDisable = isMultiple;
      originalCost = unitPrice || null;

      if (serialNumber && event.target.checked) {
        serialNumbers?.push(serialNumber);
      } else if (!event.target.checked) {
        serialNumbers = [];
        serialNumber = "";
      }
      this.setState({
        isMultiple,
        serialNumbers,
        serialNumber,
        quantity: 1,
        originalCost,
      });
      return;
    }
    if (variable.listInputName.warrantyMonth === source) {
      let newDate = this.state.acceptanceDate ? new Date(this.state.acceptanceDate) : null;
      newDate?.setMonth(newDate.getMonth() + Number(event.target.value || 0));
      let warrantyExpiryDate = newDate ? newDate : null;

      if (+event.target.value < 0 && event.target.value) return;

      this.setState({
        [event.target.name]: event.target.value,
        warrantyExpiryDate: +event.target.value > 0 ? warrantyExpiryDate : null,
      });
      return;
    }
    if (variable.listInputName.isCreateQRCode === source) {
      this.setState({
        isCreateQRCode: event.target.checked,
        qrcode: event.target.checked ? this.state.code : null,
      });
      return;
    }
    if (variable.listInputName.isManageAccountant === source) {
      this.setState({
        isTemporary: +event.target.value !== 1,
        isManageAccountant: +event.target.value === 1,
      });
      return;
    }
    if (variable.listInputName.quantity === source) {
      let price = Math.ceil(
        Number(this.state.unitPrice) * Number(event.target.value || 1)
      );
      permissions.serialNumber.isDisable = +event.target.value > 1

      this.setState({
        originalCost: price ? price : 0,
        [event.target.name]: event.target.value,
        serialNumber: +event.target.value > 1 ? "" : this.state.serialNumber,
      });
      return;
    }
    if (variable.listInputName.allocationFor === source) {
      this.setState({
        [event.target.name]: event.target.checked,
        usePerson: this.props?.item?.usePerson?.id && event.target.checked ?
          {
            ...this.props?.item?.usePerson,
            personId: this.props?.item?.usePerson?.id,
            personDisplayName: this.props?.item?.usePerson?.displayName
          } : null,
        usePersonId: !event.target.checked ? null : this.state?.usePersonId
      });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  validateSubmit = () => {
    let {
      yearPutIntoUse,
      assetStatus,
      name,
      managementDepartment,
      quantity,
      originalCost,
      assetSources,
      yearOfManufacture,
      managementCode,
      model,
      madeIn,
      installationLocation,
      note,
      dateOfReception,
      warrantyMonth,
      warrantyExpiryDate,
      serialNumbers,
      isMultiple,
      store,
      decisionCode,
    } = this.state;

    let newDateOfReception = new Date(dateOfReception);
    let assetSourcesTotal = Math.ceil(
      assetSources?.reduce(
        (total, currentItem) => total + Number(currentItem.value),
        0
      )
    );

    if (dateOfReception && checkInvalidDate(dateOfReception)) {
      this.setState({ loading: false });
      toast.warn("Định dạng ngày tiếp nhận không hợp lệ (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (dateOfReception && checkMinDate(dateOfReception, "1/1/1900")) {
      toast.warning(`Ngày tiếp nhận không được nhỏ hơn ngày 01/01/1900`);
      toast.clearWaitingQueue();
      return false;
    }
    if (dateOfReception && compareDate(dateOfReception, new Date()) > 0) {
      toast.warning(`Ngày tiếp nhận không được lớn hơn ngày hiện tại`);
      toast.clearWaitingQueue();
      return false;
    }
    if (yearPutIntoUse && (yearPutIntoUse < 1000 || yearPutIntoUse > 10000)) {
      this.setState({ loading: false });
      toast.warn("Định dạng năm đưa vào sử dụng sai (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (
      yearPutIntoUse &&
      dateOfReception &&
      yearPutIntoUse < newDateOfReception?.getFullYear()
    ) {
      this.setState({ loading: false });
      toast.warn(
        "Năm đưa vào sử dụng lớn hơn hoặc bằng Ngày tiếp nhận (THÔNG TIN CHUNG)."
      );
      toast.clearWaitingQueue();
      return false;
    }
    if (managementCode?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Mã quản lý không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!name) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn tên CCDC (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (name?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Tên CCDC không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!managementDepartment) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn phòng ban quản lý (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!quantity) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn số lượng (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (quantity < 0) {
      this.setState({ loading: false });
      toast.warn("Số lượng không được nhỏ hơn 0 (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if ((quantity > decisionCode?.remainQuantity) && decisionCode?.remainQuantity) {
      this.setState({ loading: false });
      toast.warn(`Số lượng CCDC không được lớn hơn số lượng thầu(${decisionCode?.remainQuantity}) của sản phẩm (THÔNG TIN CHUNG).`);
      toast.clearWaitingQueue();
    }
    if (warrantyMonth && (warrantyMonth < 0 || warrantyMonth > 1000)) {
      toast.warn(
        "Nhập lại số tháng bảo hành nằm trong khoảng 0 đến 1000 (THÔNG TIN CHUNG)."
      );
      toast.clearWaitingQueue();
      return false;
    }
    if (
      warrantyExpiryDate &&
      compareDate(warrantyExpiryDate, "01/01/2100") > 0
    ) {
      toast.warn(
        "Ngày hết hạn bảo hành không được lớn hơn ngày 01/01/2100 (THÔNG TIN CHUNG)."
      );
      toast.clearWaitingQueue();
      return false;
    }
    if (
      dateOfReception &&
      yearOfManufacture &&
      yearOfManufacture > newDateOfReception?.getFullYear()
    ) {
      this.setState({ loading: false });
      toast.warn(
        "Năm sản xuất không được lớn hơn năm tiếp nhận (THÔNG TIN CHUNG)."
      );
      toast.clearWaitingQueue();
      return false;
    }
    if (model?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Model không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (madeIn?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Xuất xứ không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (installationLocation?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Vị trí lắp đặt không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (note?.length > 255) {
      this.setState({ loading: false });
      toast.warn("Ghi chú không nhập quá 255 ký tự (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!originalCost || originalCost?.length <= 0) {
      this.setState({ loading: false });
      toast.warn("Chưa nhập thành tiền (THÔNG TIN PHÂN BỔ).");
      toast.clearWaitingQueue();
      return false;
    }
    if (!store && assetStatus?.isNewInStore) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn kho (THÔNG TIN CHUNG).");
      toast.clearWaitingQueue();
      return false;
    }
    if ((!assetSources || assetSources?.length === 0) && Number(originalCost) > 0) {
      this.setState({ loading: false });
      toast.warn("Chưa nhập nguồn vốn CCDC (NGUỒN VỐN CCDC).");
      toast.clearWaitingQueue();
      return false;
    } else {
      let checkValueAssetSource = true;
      let sumPercentage = assetSources.reduce(
        (a, b) => a + Number(b?.percentage),
        0
      );
      assetSources.forEach((element) => {
        if (element?.value === null) {
          checkValueAssetSource = false;
        }
      });
      if (!checkValueAssetSource && Number(originalCost) > 0) {
        this.setState({ loading: false });
        toast.warn("Chưa nhập giá trị nguồn vốn CCDC.");
        toast.clearWaitingQueue();
        return false;
      }
      if (
        Number(originalCost) > 0
        && originalCost?.toString() !== assetSourcesTotal?.toString()
      ) {
        toast.warn("Nguồn vốn tài sản không bằng nguyên giá.");
        toast.clearWaitingQueue();
        return false;
      }
      if (sumPercentage !== 100 && Number(originalCost) > 0) {
        toast.warn("Nguồn gốc phải bằng 100%.");
        toast.clearWaitingQueue();
        return false;
      }
      if (
        isMultiple &&
        quantity > serialNumbers?.length &&
        serialNumbers?.length > 0
      ) {
        toast.warn("Số lượng số seri phải bằng số lượng bản ghi");
        toast.clearWaitingQueue();
        return false;
      }
    }
    return true;
  };

  validateAllocationFor = () => {
    let { status, allocationFor, useDepartment, usePerson } = this.state;
    if (status?.indexOrder !== this.state?.AssetStatusNew) {
      if (!allocationFor) {
        this.setState({ usePerson: null });
        if (!useDepartment || !useDepartment.id) {
          this.setState({ loading: false });
          toast.warning("Vui lòng chọn phòng ban sử dụng");
          toast.clearWaitingQueue();
          return false;
        }
      } else if (allocationFor) {
        if (!usePerson || !usePerson.personId) {
          this.setState({ loading: false });
          toast.warning("Vui lòng chọn người sử dụng");
          toast.clearWaitingQueue();
          return false;
        }
      }
    }
    return true;
  };

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let { t, isIatReception, handleSelect = () => { } } = this.props;

    if (e.target?.id !== "InstrumentToolForm") return;
    if (!this.validateSubmit()) return;

    setPageLoading(true);
    if (isIatReception) {
      handleSelect?.(this.state);
      setPageLoading(false);
      return
    }

    let dataState = this.convertDto(this.state);
    if (!id) {
      try {
        let res = this.state.isMultiple
          ? await createAssetCCDCMultiple(dataState)
          : await createAssetCCDC(dataState);
        const { code, apiSubErrors, message } = res?.data;
        if (code && appConst.CODE.SUCCESS === code) {
          toast.success(t("general.addSuccess"));
          this.props.handleOKEditClose();
        } else if (
          appConst.CODE.ERROR400 === code &&
          variable.listInputName.duplicateSeri === message
        ) {
          let toastMessage;
          toastMessage =
            "Số seri " +
            apiSubErrors?.map((item) => item.errorMessage).join(", ") +
            " đã tồn tại";
          apiSubErrors && toast.warning(toastMessage);
        } else {
          message && toast.warning(message);
          toast.clearWaitingQueue();
        }
      } catch {
        toast.error(this.props.t("toastr.error"));
      } finally {
        this.closeWarning();
      }
    } else {
      updateAsset(id, dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            this.setState({ shouldOpenAssetAllocationEditorDialog: true });
            toast.success(t("general.updateSuccess"));
            this.props.handleOKEditClose();
          } else {
            toast.warning(data?.message);
            toast.clearWaitingQueue();
          }
        })
        .catch(() => {
          toast.error(this.props.t("toastr.error"));
        }).finally(() => {
          this.closeWarning();
        });
    }
  };

  closeWarning = () => {
    let { setPageLoading } = this.context;
    toast.clearWaitingQueue();
    setPageLoading(false);
  }

  handleSelectUsePerson = (item) => {
    this.setState({
      usePerson: item ? item : null,
      usePersonId: item?.personId
    });
  };

  getListProductType = async () => {
    let searchObject = { pageIndex: 0, pageSize: 1000000 };
    try {
      let res = await productTypeSearchByPage(searchObject)
      if (res?.data?.content && res?.status === appConst.CODE.SUCCESS) {
        let productTypeCCDC = res.data.content.find(item => item.code === appConst.productTypeCode.CCDC)
        if (productTypeCCDC) {
          this.setState({
            productType: productTypeCCDC
          })
        }
      }
    } catch (error) {
    }
  }

  async componentDidMount() {
    let { item, isIatReception, t, isView, listAssetStatus } = this.props;
    let roles = getTheHighestRole();

    let assetStatus = getAssetStatus(
      item?.status?.indexOrder || appConst.listStatusAssets.LUU_MOI.indexOrder
    )

    let { permissions } = this.state;
    permissions.serialNumber.isDisable = Boolean(Number(+item?.quantity) > 1)
    this.setState({ permissions });

    this.getListProductType();
    if (isIatReception) {
      let status = listAssetStatus?.find(item => item.indexOrder === appConst.listStatusAssets.LUU_MOI.indexOrder);
      let checkAssetStatus = getAssetStatus(status?.indexOrder);
      this.setState({
        ...roles,
        ...item,
        checkAssetStatus,
        listAssetStatus,
        status: item?.id ? item?.status : status,
        isIatReception,
      }, this.handleSetPermission);
      return;
    } else {
      this.setState({
        ...roles,
        ...item,
        isView,
        isMedicalEquipment: this.props?.isMedicalEquipment,
        usePerson: item?.usePerson?.id ? {
          ...item?.usePerson,
          personId: item?.usePerson?.id,
          personDisplayName: item?.usePerson?.displayName
        } : null,
        allocationFor: !!item?.usePerson,
      });
    }

    if (!item?.id) {
      const searchObjectStatus = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        statusIndexOrders: [
          appConst.listStatusAssets.LUU_MOI.indexOrder,
          appConst.listStatusAssets.DANG_SU_DUNG.indexOrder,
        ],
      };
      try {
        let res = await statusSearchByPage(searchObjectStatus);
        const { data } = res;
        this.setState({
          status: data.content?.find(
            item => item.indexOrder === appConst.listStatusAssets.LUU_MOI.indexOrder
          ),
          isNew: true,
        });
      } catch (err) {
        toast.error(t("general.error"));
      }
    }
    this.setState({
      assetStatus,
      checkAssetStatus: assetStatus,
      dateOfReception: item?.dateOfReception || new Date(),
      warrantyExpiryDate: item?.warrantyExpiryDate
        ? item?.warrantyExpiryDate
        : null,
      isDisable: assetStatus?.isUsing,
      isRequire: assetStatus.isNewInStore
    }, () => {
      this.handleSetPermission();
    });

  }

  componentDidUpdate(prevProps, prevState) {
    const { item } = this.props;
    if (prevProps?.item !== item) {
      let { permissions } = this.state;
      permissions.serialNumber.isDisable = +item?.quantity > 1
      this.setState({ ...item, permissions });
    }
  }

  refreshCode = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result.data && result.data.code) {
          let { isCreateQRCode } = this.state;
          this.setState({
            code: result.data.code,
            qrcode: isCreateQRCode ? result.data.code : null,
          });
        }
      })
      .catch(() => {
        toast.error(this.props.t("general.error_reload_page"));
      });
  };

  handleCloseAll = () => {
    this.props.handleOKEditClose();
    this.setState({
      shouldOpenAllocationEditorDialog: false,
    });
  };

  handleClose = () => {
    this.setState({
      shouldOpenAllocationEditorDialog: false,
      shouldOpenDialogStore: false,
      shouldOpenDialogManufacturer: false,
      shouldOpenDialogShoppingForm: false,
      shouldOpenDialogUnit: false,
      shouldOpenDialogMedicalEquipment: false,
      isAllocation: false,
      shouldOpenSelectAssetGroupPopup: false,
      shouldOpenAddContractDialog: false,
      shouldOpenAddSupplierDialog: false,
    });
  };

  openSelectProductPopup = () => {
    this.setState({
      shouldOpenSelectProductPopup: true,
    });
  };

  openSelectAssetGroupPopup = () => {
    this.setState({
      shouldOpenSelectAssetGroupPopup: true,
    });
  };

  openSelectSupplyPopup = () => {
    this.setState({
      shouldOpenSelectSupplyDialog: true,
    });
  };

  handleSelectSupplyDialogClose = () => {
    this.setState({
      shouldOpenSelectSupplyDialog: false,
      keySearch: "",
    });
  };

  handleSelectSupply = (item) => {
    if (variable.listInputName.New === item?.code) {
      this.setState({ shouldOpenAddSupplierDialog: true });
    } else {
      this.setState({
        supplyUnit: item,
        supplyUnitId: item?.id,
      });
    }
  };

  handleSelectAssetGroupPopupClose = () => {
    this.setState({
      shouldOpenSelectAssetGroupPopup: false,
    });
  };

  handleSelectProductPopupClose = () => {
    this.setState({
      shouldOpenSelectProductPopup: false,
    });
  };

  handleSelectProduct = (item) => {
    let {quantity} = this.state;
    let { departmentUser } = getUserInformation();

    if (item?.code === variable.listInputName.New) {
      getNewCodeProduct()
        .then((result) => {
          if (result != null && result?.data && result?.data?.code) {
            let item = result?.data;
            this.setState({
              product: {
                ...item,
                name: this.state.searchProduct,
                productType: this.state?.productType,
                managementPurchaseDepartment: departmentUser
              },
              shouldOpenProductDialog: true,
            });
          }
        })
        .catch(() => {
          alert(this.props?.t("general.error_reload_page"));
        });
    } else {
      console.log(item)
      this.setState(
        {
          product: item,
          productId: item?.id,
          name: item?.name,
          madeIn: item?.madeIn,
          yearOfManufacture: item?.yearOfManufacture,
          model: item?.model,
          manufacturer: item.manufacturerId
            ? {
              id: item.manufacturerId,
              name: item.manufacturerName,
            }
            : item.manufacturer || null,
          serialNumber: item?.serialNumber,
          attributes: item?.attributes,
          unit: item.defaultSku?.sku || item?.unitId ? {
            id: item?.unitId || item.defaultSku?.sku?.id,
            name: item?.unitName || item.defaultSku?.sku?.name
          } : null,
          supplyUnit: this.state?.isIatReception ? this?.state?.supplyUnit : item?.supplierId ? {
            id: item?.supplierId || this.state?.supplyUnit?.id,
            name: item?.supplierName || this.state?.supplyUnit?.name
          } : null,
          decisionCode: this.state?.isIatReception
            ? { ...this?.state?.decisionCode, remainQuantity: item?.remainQuantity }
            : item?.decisionCode ? { decisionCode: item?.decisionCode, remainQuantity: item?.remainQuantity } : null,
          quantity: item?.quantity,
          unitPrice: item?.unitPrice,
          originalCost: item?.unitPrice && item?.quantity ? item?.unitPrice * item?.quantity : 0,
        },
        () => {
          this.handleSelectProductPopupClose();
        }
      );
    }
  };

  handleAddAttribute = (attributes) => {
    attributes?.forEach((item) => {
      item.attributeId = item?.attribute?.id;
      item.name = item?.attribute?.name;
    });

    this.setState({ attributes }, () => {
      this.handleSelectAttributePopupClose();
    });
  };

  handleAddAssetSource = (items) => {
    if (items?.length === 1) {
      items[0].percentage = 100;
      items[0].value = this.state.originalCost;
    }
    this.setState({ assetSources: items }, () => {
      this.handleSelectAssetSourcePopupClose();
    });
  };

  openPopupSelectAttribute = () => {
    this.setState({
      shouldOpenPopupSelectAttribute: true,
    });
  };

  openAssetAllocationEditorDialog = () => {
    this.setState({
      shouldOpenAssetAllocationEditorDialog: true,
    });
  };

  handleCloseAssetAllocationEditorDialog = () => {
    this.props.handleOKEditClose();
    this.setState({
      shouldOpenAssetAllocationEditorDialog: false,
    });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleSelectAttributePopupClose = () => {
    this.setState({
      shouldOpenPopupSelectAttribute: false,
    });
  };

  handleSelectAssetSourcePopupClose = () => {
    this.setState({
      shouldOpenPopupSelectAssetSource: false,
    });
  };

  openPopupSelectAssetSource = () => {
    this.setState({
      shouldOpenPopupSelectAssetSource: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    });
  };

  openSelectUseDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectUseDepartmentPopup: true,
    });
  };

  openSelectUsePersonPopup = () => {
    this.setState({
      shouldOpenSelectUsePersonPopup: true,
    });
  };

  handleSelectDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    });
  };

  handleSelectUseDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectUseDepartmentPopup: false,
    });
  };

  handleSelectUsePersonPopupClose = () => {
    this.setState({
      shouldOpenSelectUsePersonPopup: false,
    });
  };

  handleSelectManagementDepartment = (item) => {
    let department = item ?? null;
    this.setState(
      {
        managementDepartment: department,
        managementDepartmentId: department?.id,
        store: null,
      },
      () => {
        this.handleSelectDepartmentPopupClose();
      }
    );
  };

  handleSelectUseDepartment = (item) => {
    let department = { id: item?.id, name: item.text, text: item.text };
    this.setState({ useDepartment: department }, () => {
      this.handleSelectUseDepartmentPopupClose();
    });

    if (Object.keys(item).length === 0) {
      this.setState({ useDepartment: null });
    }

    let usePersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: item?.id,
    };
    let result = 0;
    let { usePerson } = this.state;
    personSearchByPage(usePersonSearchObject).then((data) => {
      data.data.content.map((person) => {
        if (usePerson) {
          if (usePerson.id === person.id) {
            ++result;
          }
        }
        return person;
      });

      if (result !== 0) {
        this.setState({ receiverDepartment: { id: item?.id, name: item.text } },
          () => {
            this.handleSelectUseDepartmentPopupClose();
          }
        );
      } else {
        this.setState(
          {
            receiverDepartment: { id: item?.id, name: item.text },
            usePerson: null,
          },
          () => {
            this.handleSelectUseDepartmentPopupClose();
          }
        );
      }
    });
  };

  handleSelectAssetGroup = (value) => {
    if (value) {
      if (variable.listInputName.New === value?.code) {
        this.setState({
          shouldOpenSelectAssetGroupPopup: true,
        });
      } else {
        this.setState({
          assetGroup: value,
          assetGroupId: value?.id,
          depreciationRate: value.depreciationRate,
        });
      }
    } else {
      this.setState({
        assetGroup: null,
        assetGroupId: null,
        depreciationRate: 0,
      });
    }
  };

  selectAssetGroup = (assetGroupSelected) => {
    this.setState({
      assetGroup: assetGroupSelected,
      depreciationRate: assetGroupSelected.depreciationRate,
    });
  };

  selectManufacturer = (manufacturerSelected) => {
    if (manufacturerSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogManufacturer: true,
      });
      return;
    }

    this.setState({
      manufacturer: manufacturerSelected,
      manufacturerId: manufacturerSelected?.id,
    });
  };

  selectUnit = (unitSelected) => {
    if (unitSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogUnit: true,
      });
      return;
    }

    this.setState({
      unit: unitSelected,
      unitId: unitSelected?.id,
    });
  };

  selectAssetSource = (assetSourceSelected) => {
    this.setState({ assetSource: assetSourceSelected });
  };

  selectShoppingForm = (shoppingFormSelected) => {
    if (shoppingFormSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogShoppingForm: true,
      });
      return;
    }

    this.setState({
      shoppingForm: shoppingFormSelected,
      shoppingFormId: shoppingFormSelected?.id,
    });
  };

  selectMedicalEquipment = (medicalEquipmentSelected) => {
    if (medicalEquipmentSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogMedicalEquipment: true,
      });
      return;
    }

    this.setState({
      medicalEquipment: medicalEquipmentSelected,
      medicalEquipmentId: medicalEquipmentSelected?.id,
    });
  };

  selectStatus = (statusSelected) => {
    if (
      !statusSelected ||
      !statusSelected.indexOrder ||
      statusSelected.indexOrder === 0
    ) {
      this.setState({
        status: statusSelected,
        useDepartment: null,
        usePerson: null,
        checkUseDepartmentByStatus: true,
        shouldOpenSelectUserDepartment: false,
        shouldOpenSelecPopup: false,
      });
    } else {
      this.setState({
        status: statusSelected,
        checkUseDepartmentByStatus: false,
        shouldOpenSelectUserDepartment: true,
        shouldOpenSelecPopup: true,
      });
    }
  };

  selectStores = (store) => {
    if (store?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogStore: true,
      });
      return;
    }

    this.setState({
      store,
      storeId: store?.id,
    });
  };

  selectSupplyUnit = (supplyUnitSelected) => {
    this.setState({
      supplyUnit: supplyUnitSelected,
      supplyUnitId: supplyUnitSelected?.id,
    });
  };

  selectUseDepartment = (departmentSelected) => {
    this.setState({ useDepartment: departmentSelected }, () => { });
  };

  selectManagementDepartment = (departmentSelected) => {
    this.setState({
      managementDepartment: departmentSelected,
      managementDepartmentId: departmentSelected?.id,
    });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { attributes } = this.state;
    if (attributes != null && attributes.length > 0) {
      attributes.forEach((element) => {
        if (
          element.tableData != null &&
          rowData != null &&
          rowData.tableData != null &&
          element.tableData.id === rowData.tableData.id
        ) {
          if (event.target.name === "valueText") {
            element.valueText = event.target.value;
            this.setState({ attributes });
          }
        }
      });
    }
  };

  handleRowDataCellChangeAssetSource = (rowData, event, changeOriginalCost) => {
    let { assetSources, originalCost } = this.state;
    let value = event.target.value?.replaceAll(",", "");

    assetSources?.forEach((element) => {
      if (element?.tableData?.id === rowData?.tableData?.id) {
        element.assetSourceId = element?.assetSource?.id;
        element.assetSourceName = element?.assetSource?.name;

        if (variable.listInputName.assetSourcePercentage === event.target.name) {
          element.percentage = assetSources?.length === 1 ? 100 : event?.target?.value;
          if (originalCost && originalCost >= 0) {
            element.value = assetSources?.length === 1
              ? originalCost
              : (event?.target?.value * originalCost) / 100;
          }
        } else {
          element.value = assetSources?.length === 1 ? originalCost : +event?.target?.value;
          if (originalCost && originalCost >= 0) {
            element.percentage = assetSources?.length === 1 ? 100 : Math.round((
              (+event?.target?.value / originalCost) *
              100
            ))
          }
        }
      }
      else if (!rowData && changeOriginalCost >= 0) {
        if (+element.percentage > 0) {
          element.value = (
            (element.percentage ? element.percentage : 0)
            * Number(changeOriginalCost)
          ) / 100
        }
      }
      this.setState({ assetSources });
    });
  };

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    let index = attributes?.findIndex(
      (attribute) => attribute?.attribute?.id === rowData?.attribute?.id
    );
    attributes.splice(index, 1);
    this.setState({ attributes });
  };

  handleRowDataCellDeleteAssetSource = (rowData) => {
    let { assetSources, originalCost } = this.state;
    if (assetSources != null && assetSources.length > 0) {
      for (let index = 0; index < assetSources.length; index++) {
        if (
          assetSources[index].assetSource &&
          assetSources[index].assetSource.id === rowData.assetSource.id
        ) {
          assetSources.splice(index, 1);
          break;
        }
      }
    }
    if (assetSources.length === 1) {
      assetSources[0].percentage = 100;
      assetSources[0].value = originalCost;
    }
    this.setState({ assetSources });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId
      ? this.state?.listAssetDocumentId
      : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data ? data : null;
      document?.attachments?.map((item) =>
        fileDescriptionIds.push(item?.file?.id)
      );

      if (document) {
        document.fileDescriptionIds = fileDescriptionIds;
        document.documentType = this.state.documentType;
      }

      this.setState({
        item: document,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true,
      });
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex((document) => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(
      (documentId) => documentId === id
    );
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(appConst.documentType.IAT_DOCUMENT)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(
      (item) => item?.id === document?.id
    );
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data,
    });
  };

  handleAddNewSeriNumberOption = (event) => {
    let { serialNumbers, quantityAsset } = this.state;
    let { t } = this.props;
    if (
      (serialNumbers?.length < quantityAsset &&
        appConst.KEY.ENTER === event.key &&
        event?.target?.value) ||
      !quantityAsset ||
      quantityAsset <= 0
    ) {
      let existItem = serialNumbers?.find(
        (item) => item === event?.target?.value
      );
      if (existItem) {
        toast.warning(t("InstrumentToolsList.existSeriNo"));
        return;
      }
      serialNumbers?.push(event?.target?.value?.trim());
      this.setState({ serialNumbers });
    } else {
      toast.warning("Tối đa " + quantityAsset + " số seri");
    }
  };

  handleDeleteSerialNo = (index) => {
    let { serialNumbers } = this.state;
    serialNumbers.splice(index, 1);
    this.setState({ serialNumbers });
  };

  handleKeyDown = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      e.preventDefault();
    }
  };

  handleChangeSelectContract = (value) => {
    if (variable.listInputName.New === value?.code) {
      this.setState({
        shouldOpenAddContractDialog: true,
      });
    }
    this.setState({
      contract: value,
      contractId: value?.contractId,
      contractDate: value?.contractDate,
      supplyUnit: value?.supplier,
    });
  };

  handleSetPermission = () => {
    let {
      isView,
      isIatReception,
    } = this.props;

    let {
      isDisable,
      isRequire,
      permissions,
    } = this.state;

    if (isIatReception && !isView) {
      permissions.store.isDisable = true;
      permissions.managementDepartment.isDisable = true;
      permissions.supplyUnit.isDisable = true;
      permissions.contract.isDisable = true;
      permissions.contractDate.isDisable = true;
      permissions.dateOfReception.isDisable = true;
      permissions.dateBill.isDisable = true;
      permissions.lotNumber.isDisable = true;
    }
    if (isDisable) {
      permissions.dateOfReception.isDisable = true;
      permissions.managementCode.isDisable = true;
      permissions.productName.isDisable = true;
      permissions.quantity.isDisable = true;
      permissions.store.isDisable = true;
      permissions.originalCost.isDisable = true;
      permissions.supplyUnit.isDisable = true;
      permissions.buttonUseDepartment.isDisable = true;
    }
    if (isView) {
      Object.keys(permissions)?.forEach(key => {
        permissions[key].isView = true;
      })
    }
    if (isRequire) {
      permissions.store.isRequire = true;
    }
    this.setState({ permissions: permissions })
  }

  handleFormError = (errors) => {
    const findError = (key) => Boolean(errors?.find(i => i?.props?.className?.includes(key)));

    let isErrorGeneralInfomation = this.state?.ngayHoaDon && !isValidDate(this.state?.ngayHoaDon) || findError("isTabGeneralInfomation");
    let isErrorOriginTools = findError("isTabOriginTools");
    let isErrorInstrumentToolProperties = findError("isTabInstrumentToolProperties");

    this.setState({
      isErrorGeneralInfomation,
      isErrorOriginTools,
      isErrorInstrumentToolProperties
    });
  };

  render() {
    let {
      open,
      t,
      i18n,
      isAddAsset,
      isQRCode,
      classes,
      handleClose,
      isView,
      allowdEditAssetUsing,
      isViewRoleAssetUser,
      isRoleAdmin,
    } = this.props;
    let {
      product,
      searchText,
      useDepartment,
      productTypeCode,
      shouldOpenDialogUnit,
      shouldOpenDialogStore,
      shouldOpenAddSupplierDialog,
      shouldOpenDialogShoppingForm,
      shouldOpenSelectProductPopup,
      shouldOpenDialogManufacturer,
      shouldOpenSelectAssetGroupPopup,
      shouldOpenDialogMedicalEquipment,
      shouldOpenSelectUseDepartmentPopup,
      shouldOpenAssetAllocationEditorDialog,
      shouldOpenAddContractDialog,
      keySearch,
      checkAssetStatus,
    } = this.state;

    return (
      <Dialog open={open} maxWidth="xl" fullWidth className={classes.dialogWrapper}>
        <div onKeyDown={this.handleKeyDown}>
          <ValidatorForm
            id="InstrumentToolForm"
            ref="form"
            onSubmit={this.handleFormSubmit}
            onError={this.handleFormError}
            className="validator-form-scroll-dialog"
          >
            <DialogTitle id="draggable-dialog-title" className={classes.dialogTitle}>
              <span className="mb-1">
                {t("InstrumentToolsList.saveUpdate")}
              </span>
              <IconButton
                size="small"
                onClick={handleClose}
                className="position-top-right mt-8 mr-8"
              >
                <Icon fontSize="small">close</Icon>
              </IconButton>
            </DialogTitle>
            <DialogContent className={classes.dialogContent}>
              <Grid container spacing={1} className="">
                <ScrollableTabsButtonForce
                  t={t}
                  i18n={i18n}
                  item={{ ...this.state }}
                  isQRCode={isQRCode}
                  isView={isView}
                  allowdEditAssetUsing={allowdEditAssetUsing}
                  isViewRoleAssetUser={isViewRoleAssetUser}
                  isRoleAdmin={isRoleAdmin}
                  quantityCode={this.state.quantityCode}
                  itemAssetDocument={this.state.item}
                  refreshCode={this.refreshCode}
                  selectAssetGroup={this.selectAssetGroup}
                  selectProduct={this.selectProduct}
                  handleChange={this.handleChange}
                  selectAssetSource={this.selectAssetSource}
                  selectUseDepartment={this.selectUseDepartment}
                  selectManufacturer={this.selectManufacturer}
                  selectUnit={this.selectUnit}
                  handleChangeSelect={this.handleChangeSelect}
                  selectStatus={this.selectStatus}
                  selectStores={this.selectStores}
                  selectShoppingForm={this.selectShoppingForm}
                  selectMedicalEquipment={this.selectMedicalEquipment}
                  handleDateChange={this.handleDateChange}
                  handleChangeFormatNumber={this.handleChangeFormatNumber}
                  openSelectSupplyPopup={this.openSelectSupplyPopup}
                  handleSelectSupply={this.handleSelectSupply}
                  handleSelectSupplyPopupClose={
                    this.handleSelectSupplyPopupClose
                  }
                  openSelectProductPopup={this.openSelectProductPopup}
                  handleSelectProduct={this.handleSelectProduct}
                  handleSelectProductPopupClose={
                    this.handleSelectProductPopupClose
                  }
                  shouldOpenAssetAllocationEditorDialog={
                    this.state.shouldOpenAssetAllocationEditorDialog
                  }
                  openAssetAllocationEditorDialog={
                    this.openAssetAllocationEditorDialog
                  }
                  handleCloseAssetAllocationEditorDialog={
                    this.handleCloseAssetAllocationEditorDialog
                  }
                  handleSelectAssetGroup={this.handleSelectAssetGroup}
                  handleSelectAssetGroupPopupClose={
                    this.handleSelectAssetGroupPopupClose
                  }
                  openSelectAssetGroupPopup={this.openSelectAssetGroupPopup}
                  openSelectDepartmentPopup={this.openSelectDepartmentPopup}
                  handleSelectManagementDepartment={
                    this.handleSelectManagementDepartment
                  }
                  handleSelectDepartmentPopupClose={
                    this.handleSelectDepartmentPopupClose
                  }
                  openSelectUseDepartmentPopup={
                    this.openSelectUseDepartmentPopup
                  }
                  handleSelectUseDepartment={this.handleSelectUseDepartment}
                  handleSelectUseDepartmentPopupClose={
                    this.handleSelectUseDepartmentPopupClose
                  }
                  openSelectUsePersonPopup={this.openSelectUsePersonPopup}
                  handleSelectUsePerson={this.handleSelectUsePerson}
                  handleSelectUsePersonPopupClose={
                    this.handleSelectUsePersonPopupClose
                  }
                  handleRowDataCellChange={this.handleRowDataCellChange}
                  handleChangeQuantity={this.handleChangeQuantity}
                  handleAddAttribute={this.handleAddAttribute}
                  openPopupSelectAttribute={this.openPopupSelectAttribute}
                  shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                  handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                  handleRowDataCellEditAssetFile={
                    this.handleRowDataCellEditAssetFile
                  }
                  handleRowDataCellDeleteAssetFile={
                    this.handleRowDataCellDeleteAssetFile
                  }
                  handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                  isAddAsset={isAddAsset}
                  handleSelectAttributePopupClose={
                    this.handleSelectAttributePopupClose
                  }
                  handleRowDataCellDelete={this.handleRowDataCellDelete}
                  getAssetDocument={this.getAssetDocument}
                  handleUpdateAssetDocument={this.handleUpdateAssetDocument}
                  openPopupSelectAssetSource={this.openPopupSelectAssetSource}
                  handleAddAssetSource={this.handleAddAssetSource}
                  handleSelectAssetSourcePopupClose={
                    this.handleSelectAssetSourcePopupClose
                  }
                  handleRowDataCellChangeAssetSource={
                    this.handleRowDataCellChangeAssetSource
                  }
                  handleRowDataCellDeleteAssetSource={
                    this.handleRowDataCellDeleteAssetSource
                  }
                  validateSubmit={this.validateSubmit}
                  handleClose={this.handleClose}
                  handleCloseAll={this.handleCloseAll}
                  handleSetDataSelect={this.handleSetDataSelect}
                  handleAddNewSeriNumberOption={
                    this.handleAddNewSeriNumberOption
                  }
                  handleDeleteSerialNo={this.handleDeleteSerialNo}
                  handleChangeSelectContract={this.handleChangeSelectContract}
                  handleMapDataAsset={this.props.handleMapDataAsset}
                />
              </Grid>
            </DialogContent>
            <DialogActions className="flex flex-end">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={this.props.handleClose}
              >
                {t("general.cancel")}
              </Button>
              {(
                (
                  !checkAssetStatus?.isLiquidated && !checkAssetStatus?.isTransferedToAnotherUnit && !isView
                ) || (allowdEditAssetUsing && !isViewRoleAssetUser && !isRoleAdmin)
              ) && (
                  <Button
                    className="mr-12"
                    variant="contained"
                    color="primary"
                    type="submit"
                  >
                    {t("general.save")}
                  </Button>
                )}
            </DialogActions>
          </ValidatorForm>
          {shouldOpenDialogUnit && (
            <StockKeepingUnitEditorDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleClose}
              handleOKEditClose={this.handleClose}
              open={shouldOpenDialogUnit}
              keySearch={this?.state?.keySearch || ""}
              selectUnit={(data) => this.handleSetDataSelect(data, "unit")}
            />
          )}
          {/* Nhà cung cấp */}
          {shouldOpenAddSupplierDialog &&
            <SupplierDialog
              t={t}
              i18n={i18n}
              item={{
                name: this.state.keySearch,
              }}
              open={shouldOpenAddSupplierDialog}
              handleSelect={this.handleSelectSupply}
              handleClose={this.handleSelectSupplyDialogClose}
            />}
          {shouldOpenSelectProductPopup && (
            <SelectProductPopup
              open={shouldOpenSelectProductPopup}
              handleSelect={this.handleSelectProduct}
              selectedItem={product != null ? product : {}}
              assetClass={appConst.assetClass.CCDC}
              handleClose={this.handleSelectProductPopupClose}
              fromAssetDialog
              isReceivingAsset={this?.props?.isIatReception}
              decisionCode={this.state?.decisionCode}
              t={t}
              i18n={i18n}
              productTypeCode={
                productTypeCode != null
                  ? productTypeCode
                  : appConst.productTypeCode.CCDC
              }
            />
          )}
          {shouldOpenSelectAssetGroupPopup && (
            <InstrumentAndToolsTypeDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleClose}
              open={shouldOpenSelectAssetGroupPopup}
              handleOKEditClose={this.handleClose}
              handleSelect={(data) =>
                this.handleSetDataSelect(data, "assetGroup")
              }
              item={{ name: searchText }}
              assetClass={appConst.assetClass.CCDC}
              isIATType={true}
            />
          )}
          {shouldOpenDialogStore && (
            <StoreEditorDialog
              t={t}
              i18n={i18n}
              item={{
                name: this.state.keySearch,
                departmentName: this.state?.managementDepartment?.name,
                departmentId: this.state?.managementDepartment?.id
              }}
              handleOKEditClose={this.handleClose}
              handleClose={this.handleClose}
              open={shouldOpenDialogStore}
              selectStore={this.selectStores}
            />
          )}
          {shouldOpenSelectUseDepartmentPopup && (
            <SelectDepartmentPopup
              open={shouldOpenSelectUseDepartmentPopup}
              handleSelect={this.handleSelectUseDepartment}
              selectedItem={useDepartment != null ? useDepartment : {}}
              handleClose={this.handleSelectUseDepartmentPopupClose}
              t={t}
              i18n={i18n}
            />
          )}
          {shouldOpenDialogManufacturer && (
            <CommonObjectDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleClose}
              handleOKEditClose={this.handleClose}
              typeCode={appConst.listCommonObject.HANG_SAN_XUAT.code} //code hãng sản xuất
              item={{
                name: this.state.keySearch
              }}
              open={shouldOpenDialogManufacturer}
              selectManufacturer={(data) =>
                this.handleSetDataSelect(data, "manufacturer")
              }
            />
          )}
          {shouldOpenDialogShoppingForm && (
            <ShoppingFormDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleClose}
              handleOKEditClose={this.handleClose}
              open={shouldOpenDialogShoppingForm}
              item={{
                name: this.state.keySearch
              }}
              selectShoppingForm={this.selectShoppingForm}
              handleSetDataAsset={(data) =>
                this.handleSetDataSelect(data, "shoppingForm")
              }
            />
          )}
          {shouldOpenDialogMedicalEquipment && (
            <MedicalEquipmentTypeDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleClose}
              handleOKEditClose={this.handleClose}
              selectMedicalEquipment={this.selectMedicalEquipment}
              open={shouldOpenDialogMedicalEquipment}
              item={{
                name: this.state.keySearch
              }}
              handleSetDataAsset={(data) =>
                this.handleSetDataSelect(data, "medicalEquipment")
              }
            />
          )}

          {shouldOpenAssetAllocationEditorDialog && (
            <AssetAllocationEditorDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleCloseAssetAllocationEditorDialog}
              open={this.shouldOpenAssetAllocationEditorDialog}
              handleOKEditClose={this.handleCloseAssetAllocationEditorDialog}
              item={this.state.InstrumentsToolsAllocation}
            />
          )}
          {shouldOpenAddContractDialog && (
            <ContractDialog
              t={t}
              i18n={i18n}
              open={shouldOpenAddContractDialog}
              contractTypeCode={appConst.TYPES_CONTRACT.CU}
              handleClose={this.handleClose}
              handleSelect={this.handleChangeSelectContract}
              isVisibleType
              item={{
                name: keySearch
              }}
            />
          )}

          {this.state.shouldOpenProductDialog && (
            <ProductDialog
              t={t}
              i18n={i18n}
              handleClose={() => this.setState({ shouldOpenProductDialog: false })}
              open={this.state?.shouldOpenProductDialog}
              handleOKEditClose={() => this.setState({ shouldOpenProductDialog: false })}
              item={{ ...this.state.product, name: keySearch }}
              type={appConst.productTypeCode.CCDC}
              selectProduct={this.handleSelectProduct}
              assetClass={appConst.assetClass.CCDC}
              productType={appConst.productTypeCode.CCDC}
            />
          )}
        </div>
      </Dialog>
    );
  }
}

InstrumentToolsListEditorDialog.contextType = AppContext;
InstrumentToolsListEditorDialog.propTypes = {
  t: PropTypes.func.isRequired,
  i18n: PropTypes.any.isRequired,
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  isIatReception: PropTypes.bool,
  handleOKEditClose: PropTypes.func,
  handleClose: PropTypes.func,
  handleSelectAsset: PropTypes.func,
  item: PropTypes.any,
};
export default withStyles(styles)(InstrumentToolsListEditorDialog);
