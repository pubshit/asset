import { Icon, Card, Grid, Divider, Button, DialogActions, Dialog } from "@material-ui/core";
import React, { useContext } from "react";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import FileSaver from "file-saver";
import { exportExcelFileError } from "./InstrumentToolsListService"
import AppContext from "app/appContext";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
function ImportExcelDialogError(props) {
  const { t, open, handleClose } = props;
  const { setPageLoading } = useContext(AppContext);


  const handleDownloadFileError = () => {
    setPageLoading(true)
    exportExcelFileError(props?.file?.urlFileError)
      .then((res) => {
        setPageLoading(false)
        toast.info(props.t("general.successExport"));
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, `${props.fileName}`);
      })
      .catch((err) => {
        setPageLoading(false)
        toast.warning(props.t("general.failExport"));
      });
  }
  return (
    <Dialog onClose={handleClose} open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">{t('general.failImport')}</span>
      </DialogTitle>
      <DialogContent>
        <Card className="mb-24" elevation={2}>
          <div className="p-16">
            <Grid
              container
              spacing={2}
              justify="center"
              alignItems="center"
              direction="row"
            >
              <Grid item lg={3} md={3}>
                {t('general.file_name')}
              </Grid>
              <Grid item lg={6} md={6}>
                {t('general.messageError')}
              </Grid>
              <Grid item lg={3} md={3}>
                {t('general.showError')}
              </Grid>
            </Grid>
          </div>
          <Divider></Divider>
          <div className="px-16 py-16">
            <Grid
              container
              spacing={2}
              justify="center"
              alignItems="center"
              direction="row"
            >
              <Grid item lg={3} md={3} sm={12} xs={12}>
                {props?.fileName}
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                {props?.file?.messageError}
              </Grid>
              <Grid item lg={3} md={3} sm={12} xs={12}>
                <div className="flex">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleDownloadFileError}
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">download</Icon>
                      <span>File lỗi</span>
                    </div>
                  </Button>
                </div>
              </Grid>
            </Grid>
          </div>
        </Card>
      </DialogContent>
      <DialogActions>
        <Button
          className="mb-16 mr-36 align-bottom"
          variant="contained"
          color="secondary"
          onClick={() => handleClose()}>{t('general.close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ImportExcelDialogError;