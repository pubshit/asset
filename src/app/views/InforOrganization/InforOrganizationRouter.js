import {EgretLoadable} from "egret";
import ConstantList from "../../appConfig";
import {withTranslation} from 'react-i18next';
import {ROUTES_PATH} from "../../appConst";

const InforOrganizationTable = EgretLoadable({
  loader: () => import("./InforOrganizationTable")
});
const ViewComponent = withTranslation()(InforOrganizationTable);

const InforOrganizationRouter = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.SYSTEM_MANAGEMENT.ORG_INFO,
    exact: true,
    component: ViewComponent
  }
];

export default InforOrganizationRouter;