import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/config-organizations";

export const getOneById = () => {
  return axios.get(API_PATH);
};

export const saveOrganization = (organization) => {
  return axios.post(API_PATH, organization);
};

export const getCurrentOrg = () => {
  var url = API_PATH;
  return axios.get(url);
};

export const updateOrganization = (organization) => {
  return axios.put(API_PATH + "/update/" + organization.org, organization);
};

export const createOrganization = (organization) => {
  return axios.post(API_PATH + "/create", organization);
};

export const checkDuplicateCode = (taxCode, numberPhone) => {
  const config = { params: { taxCode: taxCode, numberPhone: numberPhone } };
  var url = API_PATH + "/checkDuplicateCode";
  return axios.get(url, config);
};

export const getOneByIdNew = (searchObject) => {
  var url = API_PATH + "/config/org";
  const config = { params: { orgId: searchObject.orgId } };
  return axios.get(url, config);
};
