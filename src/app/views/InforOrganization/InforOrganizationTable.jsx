import React from "react";
import {
  Button,
  Checkbox,
  DialogActions,
  DialogContent,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  TextField,
} from "@material-ui/core";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import localStorageService from "../../services/localStorageService";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import "react-toastify/dist/ReactToastify.css";
import { getOneById, getOneByIdNew, saveOrganization } from "./InforOrganizationService";
import DialogNote from "../Component/DialogNote/DialogNote";
import {appConst, ROUTES_PATH} from "app/appConst";
import AppContext from "../../appContext";
import {
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {searchByPageNew} from "../User/UserService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class InforOrganizationTable extends React.Component {
  state = {
    open: true,
    organization: {},
    shouldOpenImageDialog: false,
    shouldOpenPasswordDialog: false,
    name: null,
    affiliatedEstablishment: null,
    taxCode: null,
    shortName: null,
    code: null,
    province: null,
    district: null,
    commune: null,
    address: null,
    contact: null,
    numberPhone: null,
    email: null,
    configDto: {
      isGetAllStore: false,
      isAssetGroup: false
    },
    director: null,
    deputyDirector: null,
    chiefAccountant: null,
    listUser: [],
  };

  componentDidMount() {
    let { t } = this.props;
    let {organization} = getUserInformation();
    let searchObject = {
      orgId: organization?.org?.id
    }
    getOneById().then(({ data }) => {
      const item = data?.data;
      this.setState({
        ...item,
        name: item?.org?.name
          ? item?.org?.name
          : organization?.org?.name
            ? organization?.org?.name
            : "",
        director: item?.directorId
          ? {
            personId: item?.directorId,
            displayName: item?.directorName,
          }
          : null,
        deputyDirector: item?.deputyDirectorId
          ? {
            personId: item?.deputyDirectorId,
            displayName: item?.deputyDirectorName,
          }
          : null,
        chiefAccountant: item?.chiefAccountantId
          ? {
            personId: item?.chiefAccountantId,
            displayName: item?.chiefAccountantName,
          }
          : null,
      }, () => {
        getOneByIdNew(searchObject).then(({ data }) => {
          this.setState({
            configDto: {
              ...data?.data
            }
          })
        }).catch(() => {
          toast.error(t("toastr.error"))
        });
      });
    }).catch(() => {
      toast.error(t("toastr.error"))
    });
  }

  componentWillUnmount() {
    let {organization} = getUserInformation();
    this.setState({ org: organization?.org });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleCheck = (e) => {
    const name = e.target.name
    const isClonesChecked = e.target.checked;

    this.setState({
      configDto: {
        ...this.state.configDto,
        [name]: isClonesChecked,
      }
    });
  };

  handleFormSubmit = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let dataSubmit = this.convertDataSubmit();
      let res = await saveOrganization(dataSubmit)
      let { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        localStorageService.setSessionItem(
          appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION,
          data
        );
        toast.success(t("general.updateSuccess"))
      }
      else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };
  
  handleSelect = (value, name) => {
    this.setState({
      [name]: value,
    });
  };

  convertDataSubmit = () => ({
    id: this.state.id,
    name: this.state.name,
    numberPhone: this.state.numberPhone,
    address: this.state.address,
    taxCode: this.state.taxCode,
    shortName: this.state.shortName,
    code: this.state.code,
    province: this.state.province,
    district: this.state.district,
    commune: this.state.commune,
    contact: this.state.contact,
    affiliatedEstablishment: this.state.affiliatedEstablishment,
    email: this.state.email,
    configDto: this.state.configDto,
    budgetCode: this.state.budgetCode,
    org: this.state.org,
    directorId: this.state.director?.personId,
    directorName: this.state.director?.displayName,
    deputyDirectorId: this.state.deputyDirector?.personId,
    deputyDirectorName: this.state.deputyDirector?.displayName,
    chiefAccountantId: this.state.chiefAccountant?.personId,
    chiefAccountantName: this.state.chiefAccountant?.displayName,
  })

  render() {
    let { t, i18n } = this.props;
    let {
      numberPhone,
      name,
      address,
      taxCode,
      shortName,
      code,
      province,
      district,
      commune,
      contact,
      affiliatedEstablishment,
      email,
      configDto,
      budgetCode,
      director,
      deputyDirector,
      chiefAccountant,
    } = this.state;
    let TitlePage = t("Dashboard.subcategory.inforOrganization");

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('maintainRequestStatus.title') }]} /> */}
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.category"),
                path: ROUTES_PATH.SYSTEM_MANAGEMENT.ORG_INFO,
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <div className="m-sm-30">
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <DialogContent>
              <Grid container spacing={1} className="pb-10">
                <InputLabel htmlFor="isManageAccountant">
                  <span className="font-weight-600 font-size-16 text-black">
                    {t("organization.organization_info")}:
                  </span>
                </InputLabel>
              </Grid>
              <Grid container spacing={2}>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextValidator
                      className="w-100"
                      id="standard-basic"
                      onChange={(name) => this.handleChange(name, "name")}
                      name="name"
                      label={
                        <span>
                          <span className="colorRed">* </span>
                          {t("organization.name")}
                        </span>
                      }
                      value={name != null ? name : ""}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                      InputProps={{
                        // readOnly: true,
                      }}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(shortName) =>
                        this.handleChange(shortName, "shortName")
                      }
                      name="shortName"
                      label={t("organization.shortName")}
                      value={shortName ? shortName : ""}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(code) => this.handleChange(code, "code")}
                      name="code"
                      label={t("organization.code")}
                      value={code ? code : ""}
                    />
                  </FormControl>
                </Grid>

                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={this.handleChange}
                      name="budgetCode"
                      label={t("organization.budgetCode")}
                      value={
                        budgetCode || ""
                      }
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(affiliatedEstablishment) =>
                        this.handleChange(
                          affiliatedEstablishment,
                          "affiliatedEstablishment"
                        )
                      }
                      name="affiliatedEstablishment"
                      label={t("organization.affiliatedEstablishment")}
                      value={
                        affiliatedEstablishment ? affiliatedEstablishment : ""
                      }
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(taxCode) =>
                        this.handleChange(taxCode, "taxCode")
                      }
                      name="taxCode"
                      label={t("organization.taxCode")}
                      value={taxCode ? taxCode : ""}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(contact) =>
                        this.handleChange(contact, "contact")
                      }
                      name="contact"
                      label={t("organization.personContact")}
                      value={contact ? contact : ""}
                    />
                  </FormControl>
                </Grid>

                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextValidator
                      className="w-100"
                      id="standard-basic"
                      onChange={(numberPhone) =>
                        this.handleChange(numberPhone, "numberPhone")
                      }
                      name="numberPhone"
                      label={t("organization.numberPhoneContact")}
                      value={numberPhone ? numberPhone : ""}
                      validators={[
                        "matchRegexp:^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$",
                      ]}
                      errorMessages={["Số điện thoại sai định dạng"]}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextValidator
                      className="w-100"
                      id="standard-basic"
                      onChange={(email) => this.handleChange(email, "email")}
                      name="email"
                      label={t("organization.email")}
                      value={email ? email : ""}
                      validators={["isEmail"]}
                      errorMessages={["email không hợp lệ"]}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(province) =>
                        this.handleChange(province, "province")
                      }
                      name="province"
                      label={t("organization.province")}
                      value={province ? province : ""}
                    />
                  </FormControl>
                </Grid>

                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(district) =>
                        this.handleChange(district, "district")
                      }
                      name="district"
                      label={t("organization.district")}
                      value={district ? district : ""}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={(commune) =>
                        this.handleChange(commune, "commune")
                      }
                      name="commune"
                      label={t("organization.commune")}
                      value={commune ? commune : ""}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <FormControl fullWidth>
                    <TextField
                      id="standard-basic"
                      onChange={this.handleChange}
                      name="address"
                      label={t("organization.address")}
                      value={address ? address : ""}
                    />
                  </FormControl>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("organization.hospitalDirector")}
                    searchFunction={searchByPageNew}
                    searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
                    listData={this.state.listUser}
                    setListData={this.handleSelect}
                    nameListData="listUser"
                    displayLable="displayName"
                    typeReturnFunction="category"
                    name="director"
                    value={director || null}
                    onSelect={this.handleSelect}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("organization.deputyDirector")}
                    searchFunction={searchByPageNew}
                    searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
                    listData={this.state.listUser}
                    setListData={this.handleSelect}
                    nameListData="listUser"
                    displayLable="displayName"
                    typeReturnFunction="category"
                    name="deputyDirector"
                    value={deputyDirector || null}
                    onSelect={this.handleSelect}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("organization.chiefAccountant")}
                    searchFunction={searchByPageNew}
                    searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
                    listData={this.state.listUser}
                    setListData={this.handleSelect}
                    nameListData="listUser"
                    displayLable="displayName"
                    typeReturnFunction="category"
                    name="chiefAccountant"
                    value={chiefAccountant || null}
                    onSelect={this.handleSelect}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              </Grid>
              <Grid
                container
                spacing={1}
                justifyContent="space-between"
                className="pb-10 pt-30"
              >
                <InputLabel htmlFor="isManageAccountant">
                  <span className="font-weight-600 font-size-16 text-black">
                    {t("organization.organization_config")}:
                  </span>
                </InputLabel>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={3}>
                  <div className="flex flex-middle w-100">
                    <div >
                      <FormControlLabel
                        className="w-100"
                        onChange={(event) =>
                          this.handleCheck(
                            event,
                            "isGetAllStore"
                          )
                        }
                        name="isGetAllStore"
                        control={
                          <Checkbox checked={configDto?.isGetAllStore} />
                        }
                        label={
                          <span style={{ fontSize: "14px" }}>
                            {t("organization.isGetAllStore")}
                          </span>
                        }
                      />
                    </div>

                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                      <DialogNote content={t("Note.noteGetAllStore")} />
                    </div>
                  </div>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions className="pl-24">
              <Button
                variant="contained"
                color="primary"
                type="submit"
                className="mr-auto"
              >
                {t("general.update")}
              </Button>
            </DialogActions>
          </ValidatorForm>
        </div>
      </div>
    );
  }
}
InforOrganizationTable.contextType = AppContext;
export default InforOrganizationTable;
