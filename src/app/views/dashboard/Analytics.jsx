import React, { Component } from "react";
import {
  Grid,
} from "@material-ui/core";

import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { Helmet } from 'react-helmet';
import { appConst } from "../../appConst";
import {
  StatisticalChartOfDecreasedAssets,
  StatisticalChartOfIncreasedAssets,
  StatisticsOnAssetQuantityAndCostsOfRepair,
  StatisticsOnFixedAssetQuantityByGroup,
  StatisticalChartOfMedicalEquipment,
  AssetFullyDepreciated,
  AssetExpectedRepairs,
  AssetExpectedMaintain,
} from "./Component";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { ValidatorForm } from "react-material-ui-form-validator";
import moment from "moment";
import AppContext from "app/appContext";
import { Switch, TabPanel } from "../Component/Utilities";
import { RequestList } from "./Component";
import { getTheHighestRole } from "../../appFunction";
import { themeColors } from "../../EgretLayout/EgretTheme/themeColors";
class Dashboard1 extends Component {
  state = {
    analytics: {},
    assetCountByDate: [],
    allocationVoucherCountByDate: [],
    transferVoucherCountByDate: [],
    maintainRequestCountByDate: [],
    toDate: new Date(),
    fromDate: moment(new Date()).startOf('year'),
    theme: appConst.OBJECT_THEME.THEME_BLUE,
    switchChecked: false,
  };

  async componentDidMount() {
		let {
			isRoleAssetManager = false,
			isRoleAssetUser = false,
			isRoleUser = false,
			isRoleAccountant = false
		} = getTheHighestRole();

    // if (isRoleAssetManager || isRoleAssetUser || isRoleUser || isRoleAccountant) {
    //   this.setState({
    //     switchChecked: true,
    //   });
    // }
    
  }

  componentWillMount() {
    let { theme } = this.context;
    this.setState({
      theme: themeColors[theme.code],
    });
  }

  handleChangeState = (value, name) => {
    this.setState({ [name]: value });
  }

  handleGetValueTab = () => {
    let { switchChecked } = this.state;
    const { TYPE_DASHBOARD } = appConst;

    switch (switchChecked) {
      case false:
        return TYPE_DASHBOARD.ANALYTICS
      case true:
        return TYPE_DASHBOARD.NOTIFICATION
      default:
        return TYPE_DASHBOARD.ANALYTICS;
    }
  }


  render() {
    const { t, i18n } = this.props;
    let {
      toDate,
      fromDate,
      switchChecked,
    } = this.state;
    const { TYPE_DASHBOARD } = appConst;
    const tabValue = this.handleGetValueTab();
    const isAnalytics = tabValue === TYPE_DASHBOARD.ANALYTICS;

    return (
      <div className="analytics m-sm-30">
        <Helmet>
          <title>{t("Dashboard.dashboard")} | {t("web_site")}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.dashboard") }
            ]}
          />
        </div>
        <Grid container spacing={3}>
          <Grid item container spacing={2}>
            <Grid item container md={4} >
              {/*<Grid item>
                <Switch
                  className="ml-4"
                  label={
                    switchChecked
                      ? t("Dashboard.analytics.switchLabelNotice")
                      : t("Dashboard.analytics.switchLabelChart")
                  }
                  beforesvg={`
                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" fill="white" class="bi bi-graph-down" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M0 0h1v15h15v1H0zm14.817 11.887a.5.5 0 0 0 .07-.704l-4.5-5.5a.5.5 0 0 0-.74-.037L7.06 8.233 3.404 3.206a.5.5 0 0 0-.808.588l4 5.5a.5.5 0 0 0 .758.06l2.609-2.61 4.15 5.073a.5.5 0 0 0 .704.07"/>
                    </svg>
                  `}
                  checkedsvg={`
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="white"
                      class="bi bi-exclamation-lg"
                      viewBox="0 0 16 16"
                    >
                      <path d="M7.005 3.1a1 1 0 1 1 1.99 0l-.388 6.35a.61.61 0 0 1-1.214 0zM7 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0"/>
                    </svg>
                  `}
                  checked={switchChecked}
                  onChange={(e) => this.handleChangeState(
                    e.target.checked,
                    e.target.name,
                  )}
                  name="switchChecked"
                />
              </Grid>*/}
            </Grid>
            <Grid item md={2}></Grid>
            <Grid item md={3} sm={6} xs={12}>
              {isAnalytics && (
                <ValidatorForm onSubmit={() => { }}>
                  <CustomValidatePicker
                    fullWidth
                    autoOk
                    label={t("Dashboard.analytics.fromDate")}
                    size="small"
                    format="dd/MM/yyyy"
                    inputVariant="outlined"
                    name="fromDate"
                    value={fromDate || null}
                    onChange={this.handleChangeState}
                    KeyboardButtonProps={{ "aria-label": "change date", }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    maxDate={toDate}
                    maxDateMessage={toDate ? t("Dashboard.analytics.maxDateError") : t("general.maxDateDefault")}
                    minDateMessage={t("general.minDateDefault")}
                    clearable
                  />
                </ValidatorForm>
              )}
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              {isAnalytics && (
                <ValidatorForm onSubmit={() => { }}>
                  <CustomValidatePicker
                    fullWidth
                    autoOk
                    label={t("Dashboard.analytics.toDate")}
                    size="small"
                    format="dd/MM/yyyy"
                    inputVariant="outlined"
                    name="toDate"
                    value={toDate || null}
                    onChange={this.handleChangeState}
                    KeyboardButtonProps={{ "aria-label": "change date", }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    minDate={fromDate}
                    minDateMessage={fromDate ? t("Dashboard.analytics.minDateError") : t("general.minDateDefault")}
                    maxDateMessage={t("general.maxDateDefault")}
                    clearable
                  />
                </ValidatorForm>
              )}
            </Grid>
          </Grid>
        </Grid>

        <TabPanel index={TYPE_DASHBOARD.ANALYTICS} value={tabValue} boxProps={{ className: "p-0" }}>
          <Grid item container spacing={2}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <StatisticalChartOfIncreasedAssets t={t} item={this.state} />
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <StatisticalChartOfDecreasedAssets t={t} item={this.state} />
            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <StatisticsOnAssetQuantityAndCostsOfRepair t={t} item={this.state} />
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <StatisticsOnFixedAssetQuantityByGroup t={t} item={this.state} />
            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <StatisticalChartOfMedicalEquipment t={t} item={this.state} />
            </Grid>
          </Grid>
        </TabPanel>

        <TabPanel index={TYPE_DASHBOARD.NOTIFICATION} value={tabValue} boxProps={{ className: "p-0" }}>
          <Grid item container spacing={2}>
            <Grid item xs={3} className="flex flex-column">
              <div className="mb-8">
                <AssetFullyDepreciated t={t} />
              </div>
              <div className="mb-8">
                <AssetExpectedRepairs t={t} />
              </div>
              <div className="mb-8">
                <AssetExpectedMaintain t={t} />
              </div>
            </Grid>
            <Grid item xs={9}>
              <RequestList t={t} />
            </Grid>
          </Grid>
        </TabPanel>
      </div>
    );
  }
}

Dashboard1.contextType = AppContext;
export default withStyles({}, { withTheme: true })(Dashboard1);
