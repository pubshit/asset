import React from "react";
import ReactEcharts from "echarts-for-react";
import { merge } from "lodash";
import { variable } from "../../appConst";

const defaultOption = (theme) => ({
  visualMap: {
    top: 0,
    right: 10,
    pieces: [{
      gt: 0,
      lte: 20,
      color: variable.cssTheme[theme.code].secondary.light,
    }, {
      gt: 20,
      lte: 40,
      color: variable.cssTheme[theme.code].palette.yellow
    }, {
      gt: 40,
      lte: 60,
      color: variable.cssTheme[theme.code].palette.pink,
    }, {
      gt: 80,
      lte: 100,
      color: variable.cssTheme[theme.code].palette.red,
    }],
    outOfRange: {
      color: '#000'
    }
  },
  grid: {
    top: 16,
    left: 24,
    right: 0,
    bottom: 24
  },
  legend: {},
  tooltip: {},
  xAxis: {
    show: true,
    type: "category",
    showGrid: false,
    boundaryGap: false,
  },
  yAxis: {
    type: "value",
    min: 1,
    max: 100,
    splitLine: {
      show: false
    },
    axisLine: {
      show: false
    },
    axisTick: {
      show: false
    },
    axisLabel: {
      color: "rgba(0,0,0,0.54)",
      fontSize: 11,
      fontFamily: "roboto",
    }
  }
});

const ModifiedAreaChart = ({ height, option, theme }) => {
  return (
    <ReactEcharts
      style={{ height: height }}
      option={merge({}, defaultOption(theme), option)}
    />
  );
};

export default ModifiedAreaChart;
