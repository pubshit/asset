import { variable } from "../../appConst";

export const textStyle = {
  fontFamily: "Roboto, Helvetica Neue, sans-serif",
  fontSize: 12,
};

export const legendDataItem = {
  icon: "roundRect",
  textStyle: textStyle,
};

export const loadingOptionDefault = {
  color: variable.css.secondary.light,
};

export const seriesItem = {
  barWidth: 17,
};


export const handleSliceString = (str = "") => {
  return str?.length <= 15 ? str : str?.slice(0, 15) + '...'
}