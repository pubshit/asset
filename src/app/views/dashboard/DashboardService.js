import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/dashboard" + ConstantList.URL_PREFIX;
const API_PATH_REPORT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/dashboard";
const API_PATH_REQUIREMENT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/resources/requirements";
export const SSE_PATH = API_PATH_REQUIREMENT + '/realtime'

export const getDashboardAnalytics = () => {
  return axios.get(API_PATH + "/analytics");
};

export const getDashboardIncreaseAsset = (params) => {
  let config = { params };
  return axios.get(API_PATH_REPORT + "/get-increase-assets", config);
};

export const getDashboardDecreasedAsset = (params) => {
  let config = { params };
  return axios.get(API_PATH_REPORT + "/get-decrease-assets", config);
};

export const getDashboardQuantityAndCostsOfAsset = (params) => {
  let config = { params };
  return axios.get(API_PATH_REPORT + "/get-repaired-assets", config);
};
export const getAssetMedicalEquipmentChartData = (params) => {
  let config = {params};
  return axios.get(API_PATH_REPORT + "/asset-medical-equipment-chart", config)
}

export const getDashboardQuantityOfFixedAssetByGroup = (params) => {
  let config = { params };
  return axios.get(API_PATH_REPORT + "/asset-group-chart", config);
};

export const getRequirements = (params) => {
  let config = { params };
  return axios.get(API_PATH_REQUIREMENT + "/search", config);
};

export const getFullyDepreciatedAssetQuantity = (params) => {
  let config = { params };
  return axios.get(API_PATH_REPORT + "/fixed-assets/depreciation-year", config);
};

export const getExpectedRepairAssetQuantity = () => {
  return axios.get(API_PATH_REPORT + "/fixed-assets/repairing-quantity");
};
