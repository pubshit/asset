import {appConst, variable} from "../../../appConst";
import {SimpleCard} from "../../../../egret";
import React, {useEffect, useState} from "react";
import ReactEcharts from "echarts-for-react";
import {Tab, Tabs} from "@material-ui/core";
import {getDashboardQuantityAndCostsOfAsset} from "../DashboardService";
import {
  convertFromToDate,
  convertNumberPriceRoundUp,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate,
} from "../../../appFunction";
import {toast} from "react-toastify";
import {legendDataItem, loadingOptionDefault, textStyle} from "../Constant";

export const StatisticalChartOfQuantityAndMaintenanceCosts = props => {
  const {t, item = {}} = props;
  const {fromDate, toDate} = item;
  const {css} = variable;
  const { listTimeUnits, TIME_UNITS } = appConst.OBJECT_CHART;

  const [tabValue, setTabValue] = useState(1);
  const [loading, setLoading] = useState(false);
  const [listXAxisData, setListXAxisData] = useState([]);
  const [listData, setListData] = useState({
    listQuantityOfRepairedAsset: [],
    listCostsOfRepairedAsset: [],
  });

  const labelOption = {
    show: true,
    rotate: 0,
    align: 'left',
    verticalAlign: 'bottom',
    position: 'center',
    distance: 5,
    formatter: '{c}  {name|{a}}',
    fontSize: 12,
    color: css.primary.main,
    rich: {
      name: {
        textBorderColor: 'rgba(0, 0, 0, 0)',
        fontSize: 1,
        color: 'rgba(0, 0, 0, 1)',

      }
    },
  };

  useEffect(() => {
    if (
      (!fromDate || isValidDate(fromDate))
      && (!toDate || isValidDate(toDate)) && fromDate!== null && toDate !== null
    ) {
      updateChartData();
    }
  }, [fromDate, toDate, tabValue]);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const updateChartData = async () => {
    let searchObject = {
      fromDate: fromDate ? convertFromToDate(fromDate).fromDate : null,
      toDate: toDate ? convertFromToDate(toDate).toDate : null,
      type: tabValue,
    }

    try {
      setLoading(true);
      let res = await getDashboardQuantityAndCostsOfAsset(searchObject);
      const {data, code} = res?.data;
      if (isSuccessfulResponse(code)) {
        let listQuantityOfRepairedAsset = [];
        let listCostsOfRepairedAsset = [];
        let listXAxisData = [];
        data.map(item => {
          listQuantityOfRepairedAsset.push(item.quantity);
          listCostsOfRepairedAsset.push(item.totalAmount);
          listXAxisData.push(convertXAxisItem(item));
        })

        setListData({
          listQuantityOfRepairedAsset,
          listCostsOfRepairedAsset,
        })
        setListXAxisData(listXAxisData)
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      // toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  const convertXAxisItem = item => {
    switch (tabValue) {
      case TIME_UNITS.MONTH.code:
        return "T" + item.month + '/' + item.year;
      case TIME_UNITS.QUARTER.code:
        return "Q" + item.quarter + '/' + item.year;
      case TIME_UNITS.YEAR.code:
        return item.year;
    }
  }

  const convertLargeNumber = (number) => {
    const thresholds = [1000, 1000000, 1000000000];
    const names = ['ng', 'tr', 'tỷ'];

    for (let i = thresholds.length - 1; i >= 0; i--) {
      if (number >= thresholds[i]) {
        const divisor = thresholds[i];
        const result = Math.floor(number / divisor) + names[i];
        return result + (number % divisor > 0 ? ` ${convertLargeNumber(number % divisor)}` : '');
      }
    }
    return number ? number.toString() : "";
  }

  return (
    <SimpleCard
      title={t("Dashboard.analytics.statisticalChartOfQuantityAndMaintenanceCosts")}
      toolbar
      toolbarComponent={(
        <Tabs
          className="tabs"
          value={tabValue}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          {listTimeUnits?.map((item, index) => (
            <Tab
              key={index}
              tabIndex={index}
              label={item.name}
              value={item.code}
              className="tab px-0 py-2"
            />
          ))}
        </Tabs>
      )}
    >
      <ReactEcharts
        showLoading={loading}
        loadingOption={loadingOptionDefault}
        option={{
          color: [css.secondary.light, css.palette.pink],
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              crossStyle: {
                color: '#999'
              }
            },
            textStyle,
          },
          grid: { // chart padding
            top: 10,
            bottom: 50,
            left: 30,
            right: 50,
          },
          legend: { // chú thích cột ở bottom chart
            data: [
              {
                ...legendDataItem,
                name: t("Dashboard.analytics.quantityOfMaintenanceAssets"),
              },
              {
                ...legendDataItem,
                name: t("Dashboard.analytics.maintenanceCosts"),
              },
            ],
            bottom: 0,
            itemGap: 50,
          },
          xAxis: [
            {
              type: 'category',
              data: ['T1/2024', 'T2/2024', 'T3/2024', 'T4/2024'],
              axisPointer: {
                type: 'shadow'
              }
            }
          ],
          yAxis: [
            {
              type: 'value',
              name: '',
              splitLine: {
                show: false
              },
            },
            {
              type: 'value',
              name: '',
              axisLabel: {
                formatter: (value) => convertLargeNumber(value),
              },
              splitLine: {
                show: true,
                lineStyle: {
                  color: css.primary.lightActive,
                  type: "dashed",
                }
              },
            }
          ],
          series: [
            {
              name: t("Dashboard.analytics.quantityOfMaintenanceAssets"),
              type: 'bar',
              barGap: 0,
              label: labelOption,
              barWidth: 17,
              data: listData.listQuantityOfRepairedAsset,
              z: 3,
            },
            {
              name: t("Dashboard.analytics.maintenanceCosts"),
              type: 'line',
              label: {
                ...labelOption,
                color: "#d07085",
                formatter: (value) => convertNumberPriceRoundUp(value?.value) || ""
              },
              yAxisIndex: 1,
              data: listData.listCostsOfRepairedAsset,
              z:4,
            }
          ],
          dataZoom: [
            {
              type: "inside",
              start: 0,
              end: 100,
            }
          ],
        }}
      />
    </SimpleCard>
  );
}


