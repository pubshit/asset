import { variable } from "../../../appConst";
import { SimpleCard } from "../../../../egret";
import React, { useEffect, useState } from "react";
import ReactEcharts from "echarts-for-react";
import { getDashboardQuantityOfFixedAssetByGroup } from "../DashboardService";
import {
  convertFromToDate,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate,
} from "../../../appFunction";
import { handleSliceString, loadingOptionDefault, seriesItem, textStyle } from "../Constant";


export const StatisticsOnFixedAssetQuantityByGroup = props => {
  const { t, item = {} } = props;
  const { fromDate, toDate } = item;
  const { css } = variable;

  const [tabValue, setTabValue] = useState(1);
  const [loading, setLoading] = useState(false);
  const [listYAxisData, setListYAxisData] = useState([]);
  const [listData, setListData] = useState({
    listQuantityOfFixedAsset: [],
  });

  const labelOption = {
    show: true,
    rotate: 0,
    align: 'left',
    verticalAlign: 'center',
    position: 'right',
    distance: 5,
    fontSize: 12,
    color: css.primary.main,
    ...textStyle,
  };

  useEffect(() => {
    if (
      (!fromDate || isValidDate(fromDate))
      && (!toDate || isValidDate(toDate)) && fromDate !== null && toDate !== null
    ) {
      updateChartData();
    }
  }, [fromDate, toDate, tabValue]);

  const updateChartData = async () => {
    let searchObject = {
      fromDate: fromDate ? convertFromToDate(fromDate).fromDate : null,
      toDate: toDate ? convertFromToDate(toDate).toDate : null,
    }

    try {
      setLoading(true);
      let res = await getDashboardQuantityOfFixedAssetByGroup(searchObject);
      const { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        let listQuantityOfFixedAsset = [];
        let listYAxisData = [];
        data.map(item => {
          listQuantityOfFixedAsset.push(item.fixedAssetQty);
          listYAxisData.push(item.groupName);
        })

        setListData({
          listQuantityOfFixedAsset,
        })
        setListYAxisData(listYAxisData)
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      // toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  const convertLargeNumber = (number) => {
    const thresholds = [1000, 1000000, 1000000000];
    const names = ['ng', 'tr', 'tỷ'];

    for (let i = thresholds.length - 1; i >= 0; i--) {
      if (number >= thresholds[i]) {
        const divisor = thresholds[i];
        const result = Math.floor(number / divisor) + names[i];
        return result + (number % divisor > 0 ? ` ${convertLargeNumber(number % divisor)}` : '');
      }
    }
    return number ? number.toString() : "";
  }

  return (
    <SimpleCard title={t("Dashboard.analytics.StatisticsOnFixedAssetQuantityByGroup")}>
      <ReactEcharts
        showLoading={loading}
        loadingOption={loadingOptionDefault}
        option={{
          color: [item.theme.palette.bar.column],
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow',
              crossStyle: {
                color: '#999'
              }
            },
            formatter: '{b}: {c}',
            textStyle,
          },
          grid: { // chart padding
            top: 10,
            bottom: 5,
            left: 0,
            right: 50,
            containLabel: true
          },
          xAxis: [
            {
              type: 'value',
              axisPointer: {
                type: 'shadow'
              }
            }
          ],
          yAxis: [
            {
              type: 'category',
              name: '',
              data: listYAxisData,
              axisLabel: {
                ...labelOption,
                align: "right",
                showMaxLabel: 3,
                formatter: value => handleSliceString(value),
              },
              axisTick: {
                show: false,
              }
            },
          ],
          series: [
            {
              name: "aaaa",
              type: 'bar',
              label: {
                ...labelOption,
                formatter: (value) => convertLargeNumber(value.value),
              },
              barWidth: seriesItem.barWidth,
              data: listData.listQuantityOfFixedAsset,
            },
          ],
          dataZoom: [
            {
              type: "inside",
              id: "insideY",
              yAxisIndex: 0,
              filterMode: "filter",
              start: 0,
              end: 20,
              preventDefaultMouseMove: false,
              zoomOnMouseWheel: false,
              moveOnMouseMove: true,
              moveOnMouseWheel: true
            }
          ],
        }}
      />
    </SimpleCard>
  );
};