import {Card, Checkbox, FormControlLabel, Grid} from "@material-ui/core";
import PropTypes from "prop-types";
import React, {useContext, useEffect, useState} from "react";
import CardContent from "@material-ui/core/CardContent";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import {LightTooltip} from "../../../Component/Utilities";
import {RequirementGroupItem} from "./RequirementGroupItem";
import {appConst} from "../../../../appConst";
import clsx from "clsx";
import Scrollbar from "react-perfect-scrollbar";


export const RequestList = props => {
  const {t} = props;
  const {STATUS_REQUIREMENTS, REQUIREMENTS_CATEGORY} = appConst;

  const [checkedAll, setCheckedAll] = useState(true);
  const [listEmptyItem, setListEmptyItem] = useState({
    [STATUS_REQUIREMENTS.NOT_PROCESSED_YET]: {
      [REQUIREMENTS_CATEGORY.ALLOCATION.code]: true,
      [REQUIREMENTS_CATEGORY.TRANSFER.code]: true,
      [REQUIREMENTS_CATEGORY.REPAIR.code]: true,
      [REQUIREMENTS_CATEGORY.ASSET_PURCHASING.code]: true,
      [REQUIREMENTS_CATEGORY.REPOSSESSION.code]: true,
    },
    [STATUS_REQUIREMENTS.PROCESSED]: {
      [REQUIREMENTS_CATEGORY.ALLOCATION.code]: true,
      [REQUIREMENTS_CATEGORY.TRANSFER.code]: true,
      [REQUIREMENTS_CATEGORY.REPAIR.code]: true,
      [REQUIREMENTS_CATEGORY.ASSET_PURCHASING.code]: true,
      [REQUIREMENTS_CATEGORY.REPOSSESSION.code]: true,
    }
  });
  const [shouldShowNoRequirementMessage, setShouldShowNoRequirementMessage] = useState({
    [STATUS_REQUIREMENTS.NOT_PROCESSED_YET]: false,
    [STATUS_REQUIREMENTS.PROCESSED]: false,
  });

  useEffect(() => {
    handleShowNoRequirementMessage();
  }, [listEmptyItem]);

  const handleShowNoRequirementMessage = () => {
    const objectNotProcessedYet = listEmptyItem[STATUS_REQUIREMENTS.NOT_PROCESSED_YET];
    const objectProcessed = listEmptyItem[STATUS_REQUIREMENTS.PROCESSED];
    const isEmptyRequirement = (item) => item;
    let checkNotProcessedYetList = Object.values(objectNotProcessedYet)?.every(isEmptyRequirement)
    let checkProcessedList = Object.values(objectProcessed)?.every(isEmptyRequirement)

    setShouldShowNoRequirementMessage({
      [STATUS_REQUIREMENTS.NOT_PROCESSED_YET]: checkNotProcessedYetList,
      [STATUS_REQUIREMENTS.PROCESSED]: checkProcessedList,
    })
  }

  const handleCheck = (e) => {
    setCheckedAll(e.target.checked);
  }

  return (
    <Card className="request-list py-12">
      {/* Cột 1 */}
      <CardContent>
        <Grid container spacing={1}>
          <Grid item xs={8} className="text-uppercase font-weight-bold">
            <p className="mt-10 mb-0">
              {t("Dashboard.analytics.requestsListNeedToProcessed")}
            </p>
          </Grid>
          <Grid item xs={4}>
            <FormControlLabel
              className="ml-auto"
              control={
                <Checkbox
                  icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                  checkedIcon={<CheckBoxIcon fontSize="small" />}
                  size="small"
                  checked={checkedAll}
                  onChange={handleCheck}
                />
              }
              label={
                <LightTooltip title="Xem tất cả" placement= "right-end" enterDelay={300} leaveDelay={200}>
                  <div className="text-ellipsis">
                    Xem tất cả
                  </div>
                </LightTooltip>
              }
            />
          </Grid>
        </Grid>

        <p
          className={clsx(
            "text-center my-40",
            shouldShowNoRequirementMessage[STATUS_REQUIREMENTS.NOT_PROCESSED_YET] ? "" : "hidden"
          )}
        >
          <i className="text-primary">Không có yêu cầu nào cần xử lý</i>
        </p>

        <Scrollbar className="position-relative">
          <div>
            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.ASSET_PURCHASING.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.ASSET_PURCHASING.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />

            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.ALLOCATION.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.ALLOCATION.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />

            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.TRANSFER.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.TRANSFER.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />

            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.REPAIR.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.REPAIR.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />

            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.MAINTENANCE.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.MAINTENANCE.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />

            <RequirementGroupItem
              title={REQUIREMENTS_CATEGORY.REPOSSESSION.name}
              isOpen={checkedAll}
              status={STATUS_REQUIREMENTS.NOT_PROCESSED_YET}
              category={REQUIREMENTS_CATEGORY.REPOSSESSION.code}
              listEmptyItem={listEmptyItem}
              setListEmptyItem={setListEmptyItem}
            />
          </div>
        </Scrollbar>
      </CardContent>

      {/* Cột 2 */}
      <CardContent>
        <Grid container spacing={1}>
          <Grid item xs={9} className="text-uppercase font-weight-bold">
            <p className="mt-10 mb-0">
              {t("Dashboard.analytics.requestsListProcessedDuringTheDay")}
            </p>
          </Grid>
        </Grid>

        <p
          className={clsx(
            "text-center my-40",
            shouldShowNoRequirementMessage[STATUS_REQUIREMENTS.PROCESSED] ? "" : "hidden"
          )}
        >
          <i className="text-primary">Chưa có yêu cầu được xử lý</i>
        </p>

        <div>
          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.ASSET_PURCHASING.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.ASSET_PURCHASING.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />

          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.ALLOCATION.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.ALLOCATION.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />

          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.TRANSFER.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.TRANSFER.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />

          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.REPAIR.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.REPAIR.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />

          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.MAINTENANCE.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.MAINTENANCE.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />

          <RequirementGroupItem
            title={REQUIREMENTS_CATEGORY.REPOSSESSION.name}
            isOpen={checkedAll}
            status={STATUS_REQUIREMENTS.PROCESSED}
            category={REQUIREMENTS_CATEGORY.REPOSSESSION.code}
            listEmptyItem={listEmptyItem}
            setListEmptyItem={setListEmptyItem}
          />
        </div>
      </CardContent>
    </Card>
  );
};

RequestList.propType = {
  t: PropTypes.func,
}
