import { Link } from "react-router-dom";
import { CollapseCard } from "../index";
import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { getRequirements } from "../../DashboardService";
import { formatDateDto, getTheHighestRole, getUserInformation, isSuccessfulResponse } from "../../../../appFunction";
import clsx from "clsx";
import { appConst } from "app/appConst";
import moment from "moment";
import AppContext from "app/appContext";

export const RequirementGroupItem = props => {
  const { isOpen, title, category, status, listEmptyItem, setListEmptyItem } = props;
  const { departmentUser, organization } = getUserInformation();
  const { isRoleAssetManager, isRoleAssetUser } = getTheHighestRole();
  const { eventData, setEventData } = useContext(AppContext);

  const [groupTitle, setGroupTitle] = useState(title);
  const [listData, setListData] = useState([]);
  const [openAll, setOpenAll] = useState(isOpen);

  const shouldHidden = listData.length === 0;

  const isValidOrg = item => organization?.org?.id === item?.orgId;
  const isValidDepartment = item => (isRoleAssetManager || isRoleAssetUser) && item?.receiptDepartmentId === departmentUser?.id

  useEffect(() => {
    let filterData = listData?.filter(x => isValidDepartment(x) || isValidOrg(x));
    setGroupTitle(title + (filterData?.length ? ` (${filterData?.length})` : ""))
    handleCheckEmptyList();
  }, [listData]);

  useEffect(() => {
    handleUpdateListDataRealtime();
  }, [eventData]);

  useEffect(() => {
    setOpenAll(isOpen && status === appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET)
  }, [isOpen]);

  useEffect(() => {
    handleGetListRepairRequirement();
  }, []);

  const handleUpdateListDataRealtime = () => {
    let listUpdatedData = eventData.filter(item =>
      item.category === category && (isValidOrg(item) && isValidDepartment(item))
    )
    let newArray = [...listData]
    listUpdatedData.map(item => {
      let existItem = newArray?.find(x => x.id === item.id);
      if (existItem?.id) {
        newArray.splice(newArray.indexOf(existItem), 1);
      } else if (item.status === status) {
        newArray.unshift(item);
      }
    });

    setListData([...newArray]);
  }

  const handleCheckEmptyList = () => {
    let newItem = listEmptyItem;
    newItem[status][category] = listData?.length === 0
    setListEmptyItem({
      ...listEmptyItem,
      ...newItem,
    })
  }

  const handleGetListRepairRequirement = async () => {
    if (isRoleAssetUser) return;
    let searchObject = {
      category,
      status,
    }
    if (status === appConst.STATUS_REQUIREMENTS.PROCESSED) {
      searchObject.fromModifiedDate = formatDateDto(moment(new Date()).startOf("date"));
    }

    try {
      let res = await getRequirements(searchObject);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setListData([...data]);
      }
    } catch {

    }
  }

  const handleCheckValidDepartment = (item) => {
    if (isValidDepartment(item)) {
      return true;
    }
    else {
      return isValidOrg(item);
    }
  }

  return (
    <CollapseCard title={groupTitle} isOpen={openAll} color={{ title: "error" }} hidden={shouldHidden}>
      {listData?.map(item => {
        const isShowRequirement = handleCheckValidDepartment(item);

        return (
          <div
						key={item.id}
						className={clsx(
							"mb-8 text-primary",
							isShowRequirement ? "" : "hidden"
          	)}
					>
            <Link
              to={{
                pathname: item.uri,
                state: {
                  id: item.voucherId,
                  path: item.uri,
                  status: item.status,
                  category: item.category,
                  type: item.type,
                }
              }}
              className="text-underline font-weight-bold"
            >
              {item.title}
            </Link>
            <div><i>
              (TG&nbsp;{
                item.requestDate
                  ? moment(item.requestDate).format('DD/MM/YYYY HH:mm')
                  : ""
              };
              Người YC:&nbsp;{item.username || ""}
              &nbsp;-&nbsp;{item.departmentCode || ""})
            </i></div>
          </div>
        )
      })}
    </CollapseCard>
  );
};

RequirementGroupItem.propTypes = {
  title: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  listUpdatedData: PropTypes.array,
  category: PropTypes.number,
  status: PropTypes.number,
  listEmptyItem: PropTypes.object,
  setListEmptyItem: PropTypes.func,
}
