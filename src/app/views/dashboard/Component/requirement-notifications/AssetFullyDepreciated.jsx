import {Grid} from "@material-ui/core";
import PropTypes from "prop-types";
import {CardWidget} from "../CardWidget";
import {Link} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {appConst, ROUTES_PATH} from "../../../../appConst";
import {getFullyDepreciatedAssetQuantity} from "../../DashboardService";
import {isSuccessfulResponse} from "../../../../appFunction";
import {toast} from "react-toastify";


export const AssetFullyDepreciated = props => {
  const {t} = props;

  const [loading, setLoading] = useState(false);
  const [state, setState] = useState({
    remainingYear: 1, // Hiện tại mặc định năm KH còn lại là 1 năm
    unexpiredQuantity: null,
    expiredQuantity: null,
  });

  useEffect(() => {
    handleGetNewData();
  }, []);

  const handleGetNewData = async () => {
    try {
      setLoading(true);
      let config = {
        remainingYear: state.remainingYear,
      };
      let res = await getFullyDepreciatedAssetQuantity(config)
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setState({
          ...state,
          ...data,
      });
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  return (
    <CardWidget title={t("Dashboard.analytics.assetPeriod")} color="primary">
      <Grid container>
        <Grid item xs={10}>Số TS còn niên hạn {state.remainingYear} năm</Grid>
        <Grid item xs={2} className="text-align-right">{state.unexpiredQuantity || 0}</Grid>
      </Grid>

      <Grid container className="text-error">
        <Grid item xs={10} className="text-underline">
          <Link
            to={{
              pathname: ROUTES_PATH.FIXED_ASSET,
              state: {
                tabValue: appConst?.tabAsset?.tabFullyDepreciated,
                path: ROUTES_PATH.FIXED_ASSET,
              },
            }}
          >
            Hết niên hạn
          </Link>
        </Grid>
        <Grid item xs={2} className="text-align-right">{state.expiredQuantity || 0}</Grid>
      </Grid>
    </CardWidget>
  );
};

AssetFullyDepreciated.propType = {
  t: PropTypes.func,
}
