import {Grid} from "@material-ui/core";
import PropTypes from "prop-types";
import {CardWidget} from "../CardWidget";
import {Link} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {appConst, ROUTES_PATH} from "../../../../appConst";
import {getExpectedRepairAssetQuantity, getFullyDepreciatedAssetQuantity} from "../../DashboardService";
import {convertFromToDate, isSuccessfulResponse} from "../../../../appFunction";
import {toast} from "react-toastify";


export const AssetExpectedRepairs = props => {
  const {t} = props;

  const [state, setState] = useState({
    estimatedSuppliesQty: 0,
    unreceivedQty: 0,
    overdueSuppliesQty: 0,
    expiredUnreceivedQty: 0,
    expiredRepairingQty: 0,
  });

  useEffect(() => {
    handleGetNewData();
    setInterval(() => {
      handleGetNewData();
    }, 600000);
  }, []);

  const handleGetNewData = async () => {
    try {
      let res = await getExpectedRepairAssetQuantity()
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setState({
          ...data,
        });
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
    }
  }

  return (
    <CardWidget title={t("Dashboard.analytics.expectedRepairs")} color="primary">
      {/* Normal line */}
      <Grid container>
        <Grid item xs={10}>Số TS hỏng chưa sửa chữa</Grid>
        <Grid item xs={2} className="text-align-right">{state?.unreceivedQty || 0}</Grid>
      </Grid>
      <Grid container>
        <Grid item xs={10}>Số TS dự kiến có vật tư trong 1 tháng tới</Grid>
        <Grid item xs={2} className="text-align-right">{state?.estimatedSuppliesQty || 0}</Grid>
      </Grid>

      {/* Links line*/}
      <Grid container className="text-error">
        <Grid item xs={10} className="text-underline">
          <Link
            to={{
              pathname: ROUTES_PATH.ASSET_REPAIR,
              state: {
                tabValue: appConst.tabStatus.tabProcessing,
                isExpiredAssetHasVTNotRepaired: true,
                arTgCanCoVtTo: convertFromToDate(new Date()).toDate,
              }
            }}
          >
            TS quá hạn có vật tư chưa sửa chữa
          </Link>
        </Grid>
        <Grid item xs={2} className="text-align-right">{state?.overdueSuppliesQty || 0}</Grid>
      </Grid>
      <Grid container className="text-error">
        <Grid item xs={10} className="text-underline">
          <Link
            to={{
              pathname: ROUTES_PATH.ASSET_REPAIR,
              state: {
                tabValue: appConst.tabStatus.tablPending,
                isFullyDepreciatedAsset: true,
                tsSoNamKhConLai: 0,
              }
            }}
          >
            TS hỏng hết khấu hao chưa xác nhận
          </Link>
        </Grid>
        <Grid item xs={2} className="text-align-right">{state?.expiredUnreceivedQty || 0}</Grid>
      </Grid>
      <Grid container className="text-error">
        <Grid item xs={10} className="text-underline">
          <Link
            to={{
              pathname: ROUTES_PATH.ASSET_REPAIR,
              state: {
                tabValue: appConst.tabStatus.tabProcessing,
                isFullyDepreciatedAsset: true,
                tsSoNamKhConLai: 0,
              }
            }}
          >
            TS hỏng hết khấu hao đang sửa chữa
          </Link>
        </Grid>
        <Grid item xs={2} className="text-align-right">{state?.expiredRepairingQty || 0}</Grid>
      </Grid>
    </CardWidget>
  );
};

AssetExpectedRepairs.propType = {
  t: PropTypes.func,
}
