import {Grid} from "@material-ui/core";
import PropTypes from "prop-types";
import {CardWidget} from "../CardWidget";
import {Link} from "react-router-dom";
import React from "react";


export const AssetExpectedMaintain = props => {
  const {t} = props;

  return (
    <CardWidget title={t("Dashboard.analytics.expectedMaintain")} color="primary">
      {/* Normal line */}
      <Grid container>
        <Grid item xs={10}>Số TS dự kiến bảo trì trong 2 tháng tới</Grid>
        <Grid item xs={2} className="text-align-right">124</Grid>
      </Grid>
      <Grid container>
        <Grid item xs={10}>Số TS dự kiến bảo trì trong 1 tháng tới</Grid>
        <Grid item xs={2} className="text-align-right">211</Grid>
      </Grid>
      <Grid container>
        <Grid item xs={10}>Số TS dự kiến bảo trì trong 2 tuần tới</Grid>
        <Grid item xs={2} className="text-align-right">12</Grid>
      </Grid>

      {/* Links line*/}
      <Grid container className="text-error">
        <Grid item xs={10} className="text-underline">
          <Link to="/asset/maintain_planing">
            Số TS quá hạn bảo trì
          </Link>
        </Grid>
        <Grid item xs={2} className="text-align-right">3</Grid>
      </Grid>
    </CardWidget>
  );
};

AssetExpectedMaintain.propType = {
  t: PropTypes.func,
}
