import React, { useEffect, useState } from 'react'
import { variable } from "../../../appConst";
import { SimpleCard } from "../../../../egret";
import { convertFromToDate, handleThrowResponseMessage, isSuccessfulResponse, isValidDate } from "../../../appFunction";
import ReactEcharts from "echarts-for-react";
import { getAssetMedicalEquipmentChartData } from '../DashboardService';
import { toast } from "react-toastify";
import { handleSliceString, seriesItem, textStyle } from "../Constant";
import { value } from "lodash/seq";


export const StatisticalChartOfMedicalEquipment = props => {
  const { t, item = {} } = props
  const { css } = variable;
  const { fromDate, toDate } = item;
  const [loading, setLoading] = useState(false)
  const [listXAxisData, setListXAxisData] = useState([]);
  const [listData, setListData] = useState({
    fixedAssetQty: [],
    groupName: [],
    iatQty: []
  })

  const labelOption = {
    show: true,
    rotate: 0,
    align: 'center',
    formatter: value => {
      return value.value > 0 ? value.value : ""
    },
    ...textStyle,
  };

  useEffect(() => {
    if (
      (!fromDate || isValidDate(fromDate))
      && (!toDate || isValidDate(toDate)) && fromDate !== null && toDate !== null
    ) {
      updatePageData();
    }
  }, [fromDate, toDate]);

  const updatePageData = async () => {
    let searchObject = {
      fromDate: fromDate ? convertFromToDate(fromDate).fromDate : null,
      toDate: toDate ? convertFromToDate(toDate).toDate : null,
      type: 1
    }
    try {
      const res = await getAssetMedicalEquipmentChartData(searchObject)
      const { data, code } = res?.data
      if (isSuccessfulResponse(code)) {
        let fixedAssetQty = [];
        let groupName = [];
        let iatQty = [];
        data.map(item => {
          fixedAssetQty.push(item.fixedAssetQty);
          groupName.push(item.groupName);
          iatQty.push(item.iatQty);
        })
        setListData({
          fixedAssetQty,
          groupName,
          iatQty
        })
        setListXAxisData(groupName)
      } else {
        handleThrowResponseMessage(res)
      }
    } catch (e) {
      // toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }
  return (
    <SimpleCard
      title={t("Dashboard.analytics.statisticalChartOfMedicalEquipment")}
      toolbar
    >
      <ReactEcharts
        showLoading={loading}
        style={{ marginTop: "20px" }}
        loadingOption={{
          color: css.secondary.light,
        }}
        option={{
          color: [item.theme.palette.assets.fixedAsset, item.theme.palette.assets.iat,],
          visualMap: null,
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          legend: {
            data: [
              { name: "TSCĐ", icon: "roundRect" },
              { name: "CCDC", icon: "roundRect" },
            ],
            bottom: 0,
            itemGap: 50,
          },
          grid: {
            top: 0,
            bottom: 25,
            left: 10,
            right: 20,
            containLabel: true
          },
          toolbox: {
            show: false,
            orient: 'horizontal',
            left: 'center',
            top: 0,
            feature: {
              mark: {
                show: true,
              },
              dataView: {
                show: true,
                readOnly: true,
                title: "Bảng dữ liệu",
                lang: ['Bảng dữ liệu', 'Đóng', 'Làm mới'],
              },
              magicType: {
                show: true,
                type: ['line', 'bar',],
                title: {
                  line: "Biểu đồ đường",
                  bar: "Biểu đồ cột",
                }
              },
              restore: {
                show: true,
                title: "Khôi phục",
              },
              saveAsImage: {
                show: true,
                title: "Lưu ảnh biểu đồ",
              }
            }
          },
          xAxis: {
            type: 'value',
          },
          yAxis: {
            type: 'category',
            data: listXAxisData,
            axisLabel: {
              ...labelOption,
              align: "right",
              showMaxLabel: 3,
              formatter: value => handleSliceString(value),
            },
          },
          series: [
            {
              name: 'TSCĐ',
              type: 'bar',
              stack: 'total',
              barWidth: seriesItem.barWidth,
              label: labelOption,
              data: listData.fixedAssetQty
            },
            {
              name: 'CCDC',
              type: 'bar',
              barWidth: seriesItem.barWidth,
              stack: 'total',
              label: labelOption,
              data: listData.iatQty
            },
          ],
          dataZoom: [
            {
              type: "inside",
              id: "insideY",
              yAxisIndex: 0,
              filterMode: "filter",
              start: 0,
              end: 60,
              preventDefaultMouseMove: false,
              zoomOnMouseWheel: false,
              moveOnMouseMove: true,
              moveOnMouseWheel: true
            }
          ],
        }}
      />
    </SimpleCard>
  )
}


