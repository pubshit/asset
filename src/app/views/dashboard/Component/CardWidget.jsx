import {Card, CardContent, CardHeader, CircularProgress} from "@material-ui/core";
import PropTypes from "prop-types";
import {FadeLayout} from "../../Component/Utilities";
import clsx from "clsx";


export const CardWidget = props => {
  const {title, children, loading, color} = props;

  const getColor = color => {
    switch (color) {
      case "primary":
        return "text-primary";
      case "secondary":
        return "text-secondary";
      case "pink":
        return "text-pink";
      default:
        return "";
    }
  }

  return (
    <Card className={clsx(
      "position-relative",
      color ? getColor(color) : ""
    )}>
      <FadeLayout
        show={loading}
        fullSize
        justifyContent="center"
        alignItems="center"
        blur
        color="primary"
      >
        <CircularProgress color="inherit"/>
      </FadeLayout>
      {/*<CardHeader title={title} className="text-uppercase"/>*/}
      <div className="font-weight-bold text-uppercase px-12 pt-12 pb-4">
        {title}
      </div>
      <CardContent className="pt-4">
        {children}
      </CardContent>
    </Card>
  );
};

CardWidget.propType = {
  title: PropTypes.any,
  children: PropTypes.any,
  color: PropTypes.oneOf(["primary", "secondary", "pink",]),
}
