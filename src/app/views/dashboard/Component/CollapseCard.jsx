import React, {useEffect, useRef, useState} from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import clsx from "clsx";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%", // backgroundColor: theme.palette.background.paper,
  },
  card: {
    margin: "5px 0 10px",
    boxShadow: "none",
  },
  hidden: {
    display: "none",
  },
  title: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    // color: theme.palette.error.main,
    margin: 0,
  },
  actions: {
    display: "flex",
    color: "#fff",
    padding: 0,
  },
  collapse: {
  },
  content: {
    padding: "0.5rem",
    "&:last-child": {
      paddingBottom: "0.5rem",
    },
  },
  expand: {
    transform: "rotate(-90deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
    color: "red",
    "&:hover": {
      backgroundColor: "inherit",
    },
  },
  expandOpen: {
    transform: "rotate(0deg)",
    color: "red",
  },
}));

const CollapseCard = (props) => {
  const { title, children, hidden, isOpen = true, className, color, } = props;
  const classes = useStyles();
  const [expanded, setExpanded] = useState(true);
  const preIsOpen = useRef(isOpen);

  useEffect(() => {
    if (preIsOpen.current !== isOpen) {
      setExpanded(isOpen);
      preIsOpen.current = isOpen;
    }
  }, [isOpen]);


  const handleExpandClick = () => {
    setExpanded((prevExpanded) => !prevExpanded);
  };

  const getColorThroughClassName = (color) => {
    switch (color) {
      case "primary":
        return "text-primary";
      case "error":
        return "text-error";
      default:
        return "text-primary";
    }
  }

  return (
    <Card
      className={classnames(className, classes.card, classes.root, {
        [classes.hidden]: hidden,
      })
      }
    >
      <CardActions className={clsx(
        classes.actions,
        "text-error"
      )}>
        <IconButton
          size="small"
          disableRipple
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          {/* Use different icons based on the expanded state */}
          <ExpandMoreIcon fontSize="small" className={clsx(color ? getColorThroughClassName(color.title) : "")}/>
        </IconButton>
        <Typography title={title} className={classes.title}>
          {title}
        </Typography>
      </CardActions>
      <Collapse
        className={classes.collapse}
        in={expanded}
        timeout="auto"
        unmountOnExit
      >
        <CardContent className={classes.content}>{children}</CardContent>
      </Collapse>
    </Card>
  );
};

CollapseCard.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  hidden: PropTypes.bool,
  color: PropTypes.shape({
    title: PropTypes.oneOf(["primary", "error"]),
  })
};

export default CollapseCard;
