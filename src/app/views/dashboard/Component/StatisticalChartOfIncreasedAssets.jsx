import { appConst, variable } from "../../../appConst";
import { SimpleCard } from "../../../../egret";
import React, { useEffect, useState } from "react";
import ReactEcharts from "echarts-for-react";
import { Tab, Tabs } from "@material-ui/core";
import { getDashboardIncreaseAsset } from "../DashboardService";
import { convertFromToDate, handleThrowResponseMessage, isSuccessfulResponse, isValidDate } from "../../../appFunction";
import { toast } from "react-toastify";
import { seriesItem } from "../Constant";

export const StatisticalChartOfIncreasedAssets = props => {
  const { t, item = {} } = props;
  const { fromDate, toDate } = item;
  const { css } = variable;
  const { listTimeUnits, TIME_UNITS } = appConst.OBJECT_CHART;

  const [tabValue, setTabValue] = useState(1);
  const [loading, setLoading] = useState(false);
  const [listXAxisData, setListXAxisData] = useState([]);
  const [listData, setListData] = useState({
    listFixedAsset: [],
    listIat: [],
    listSupplies: [],
  });

  const labelOption = {
    show: true,
    rotate: 0,
    align: 'left',
    verticalAlign: 'bottom',
    position: 'center',
    distance: 5,
    formatter: '{c}  {name|{a}}',
    fontSize: 12,
    rich: {
      name: {
        textBorderColor: 'rgba(0, 0, 0, 0)',
        fontSize: 1,
        color: 'rgba(0, 0, 0, 0)',

      }
    },
  };

  useEffect(() => {
    if (
      (!fromDate || isValidDate(fromDate))
      && (!toDate || isValidDate(toDate)) && fromDate !== null && toDate !== null
    ) {
      updateChartData();
    }
  }, [fromDate, toDate, tabValue]);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const updateChartData = async () => {
    let searchObject = {
      fromDate: fromDate ? convertFromToDate(fromDate).fromDate : null,
      toDate: toDate ? convertFromToDate(toDate).toDate : null,
      type: tabValue,
    }

    try {
      setLoading(true);
      let res = await getDashboardIncreaseAsset(searchObject);
      const { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        let listFixedAsset = [];
        let listIat = [];
        let listSupplies = [];
        let listXAxisData = [];
        data.map(item => {
          listFixedAsset.push(item.fixedAssetQuantity);
          listIat.push(item.iatQuantity);
          listSupplies.push(Math.floor(item.suppliesQuantity));
          listXAxisData.push(convertXAxisItem(item));
        })

        setListData({
          listFixedAsset,
          listIat,
          listSupplies,
        })
        setListXAxisData(listXAxisData)
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      // toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  const convertXAxisItem = item => {
    switch (tabValue) {
      case TIME_UNITS.MONTH.code:
        return "T" + item.month + '/' + item.year;
      case TIME_UNITS.QUARTER.code:
        return "Q" + item.quarter + '/' + item.year;
      case TIME_UNITS.YEAR.code:
        return item.year;
    }
  }

  return (
    <SimpleCard
      title={t("Dashboard.analytics.assetIncreasedReport")}
      toolbar
      toolbarComponent={(
        <Tabs
          className="tabs"
          value={tabValue}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          {listTimeUnits?.map((item, index) => (
            <Tab
              key={index}
              tabIndex={index}
              label={item.name}
              value={item.code}
              className="tab px-0 py-2"
            />
          ))}
        </Tabs>
      )}
    >
      <ReactEcharts
        showLoading={loading}
        loadingOption={{
          color: css.secondary.light,
        }}
        option={{
          color: [item.theme.palette.assets.fixedAsset, item.theme.palette.assets.iat, item.theme.palette.assets.supplier],
          visualMap: null,
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          legend: { // chú thích cột ở bottom chart
            data: [
              { name: "TSCĐ", icon: "roundRect" },
              // { name: "CCDC", icon: "roundRect" },
              // { name: "VTTT", icon: "roundRect" },
            ],
            bottom: 0,
            itemGap: 50,
          },
          grid: { // chart padding
            top: 50,
            bottom: 50,
            left: 30,
            right: 25,
          },
          toolbox: {
            show: true,
            orient: 'horizontal',
            left: 'center',
            top: 'top',
          },
          xAxis: [
            {
              type: 'category',
              axisTick: { show: false },
              data: listXAxisData,
            }
          ],
          yAxis: [
            {
              type: 'value',
            }
          ],
          series: [
            {
              name: 'TSCĐ',
              type: 'bar',
              barGap: 0,
              label: labelOption,
              barWidth: seriesItem.barWidth,
              data: listData?.listFixedAsset,
              z: 3,
            },
            // {
            //   name: 'CCDC',
            //   type: 'bar',
            //   label: labelOption,
            //   barWidth: seriesItem.barWidth,
            //   data: listData?.listIat,
            //   z: 2
            // },
            // {
            //   name: 'VTTT',
            //   type: 'bar',
            //   label: labelOption,
            //   barWidth: seriesItem.barWidth,
            //   data: listData?.listSupplies,
            //   z: 1
            // },
          ],
          dataZoom: [
            {
              type: "inside",
              start: 0,
              end: 100,
            }
          ],
        }}
      />
    </SimpleCard>
  );
};