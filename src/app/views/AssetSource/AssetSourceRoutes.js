import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetSource = EgretLoadable({
  loader: () => import("./AssetSource")
});
const ViewComponent = withTranslation()(AssetSource);

const AssetSourceRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/assetsource",
    exact: true,
    component: ViewComponent
  }
];

export default AssetSourceRoutes;
