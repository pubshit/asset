import {
  Button,
  Dialog,
  DialogActions,
  Grid,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import AppContext from "app/appContext";
import React from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { saveItem, updateItem } from "./AssetSourceService";
import { appConst } from "app/appConst";
import {PaperComponent} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
class AssetSourceDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeKeyWord = this.handleChangeKeyWord.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 1,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
    this.updatePageData();
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null });
    }
  };
  
  componentDidMount() {
    let { item } = this.props;
    this.setState(item);
  }
  
  handleChangeKeyWord(event) {
    this.setState({ keyword: event.target.value });
  }
  
  handleChange = (event, source) => {
    event.persist();
    if (source === "codeOriginHI") {
      let codeOriginHI = this.state;
      codeOriginHI = event.target.value;
      let nameOriginHIValue = [...appConst.listOriginUnderHealthInsurance]?.find(
        (item) => item.code === codeOriginHI
      )?.name;
      this.setState({
        codeOriginHI: codeOriginHI,
        nameOriginHI: nameOriginHIValue,
      });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };
  handleChangeCode = (event) => {
    this.setState({ code: event.target.value });
  };

  handleFormSubmit = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { code, codeOriginHI, name, nameOriginHI, id } = this.state;
    let submitData = {
      code,
      codeOriginHI,
      name,
      nameOriginHI
    };
    setPageLoading(true);
    try {
      if (id) {
        let data = await updateItem({ ...submitData, id });
        if (data?.data?.data && data?.status === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"));
          this.props.handleClose();
        } else {
          toast.info(data?.data?.message)
        }
      } else {
        let data = await saveItem(submitData);
        if (data?.data?.data && data?.status === appConst.CODE.SUCCESS) {
          toast.success(t("general.success"));
          this.props.handleClose();
          this.props.handleSelect?.(data?.data?.data);
        } else {
          toast.info(data?.data?.message)
        }
      }
    } catch (error) {
      console.error(error);
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  render() {
    const {
      t,
      open,
      handleSetShowToast
    } = this.props;
    let {
      name,
      code,
      codeOriginHI,
    } = this.state;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm" fullWidth>
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t("AssetSource.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="mb-10" container spacing={1}>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("AssetSource.name")}
                      <span className="colorRed"> *</span>
                    </span>
                  }
                  onChange={this.handleChangeName}
                  type="text"
                  name="name"
                  value={name || ""}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      {t("AssetSource.code")}
                      <span className="colorRed"> *</span>
                    </span>
                  }
                  onChange={this.handleChangeCode}
                  type="text"
                  name="code"
                  value={code || ""}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="codeOriginHI-simple">
                    {t("AssetSource.originUnderHealthInsurance")}
                  </InputLabel>
                  <Select
                    value={codeOriginHI || null}
                    onChange={(codeOriginHI) =>
                      this.handleChange(codeOriginHI, "codeOriginHI")
                    }
                    inputProps={{
                      name: "codeOriginHI",
                      id: "codeOriginHI-simple",
                    }}
                  >
                    {appConst.listOriginUnderHealthInsurance?.map((item) => {
                      return (
                        <MenuItem key={item.code} value={item.code}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                className="mr-15"
                variant="contained"
                color="primary"
                type="submit"
                onClick={() => handleSetShowToast && this?.props?.handleSetShowToast(false)}
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
AssetSourceDialog.contextType = AppContext;
export default AssetSourceDialog;
