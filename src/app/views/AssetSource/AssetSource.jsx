import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  Tooltip,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { useTranslation } from "react-i18next";
import { deleteItem, getItemById, searchByPage } from "./AssetSourceService";
import AssetSourceDialog from "./AssetSourceDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import {appConst, DEFAULT_TOOLTIPS_PROPS} from "app/appConst";
import appConfig from "app/appConfig";
import localStorageService from "app/services/localStorageService";
import {LightTooltip} from "../Component/Utilities";
import {defaultPaginationProps, handleKeyDown} from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;

  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      {props?.isRoleOrgAdmin && <LightTooltip title={t("general.deleteIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>}
    </div>
  );
}

class AssetSource extends React.Component {
  state = {
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    keyword: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
    isRoleOrgAdmin: false
  };
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  
  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }
  
  search = async () => {
    this.updatePageData()
  }
  
  componentDidMount() {
    this.getRoleCurrentUser();
    this.updatePageData();
  }
  
  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    try {
      setPageLoading(true);
      let result = await searchByPage(searchObject);
      if (result?.status === appConst.CODE.SUCCESS && result?.data) {
        this.setState({
          itemList: [...result?.data?.data?.content],
          totalElements: result?.data?.data?.totalElements,
        });
      }
    } catch (error) {
      toast.warning("Lỗi hệ thống!");
    } finally {
      setPageLoading(false);
    }
  };
  
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  handleKeyUp = (e) => {
    if (!e.target.value || e.target.value === null) {
      this.search();
    }
  };

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        shouldOpenConfirmationDeleteAllDialog: false,
        shouldOpenNotificationPopup: false,
        data: [],
      },
      () => {
        this.updatePageData();
      }
    );
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
    }, () => {
      this.setPage(0);
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    deleteItem(this.state.id)
      .then((data) => {
        this.updatePageData();
        this.handleDialogClose();
        toast.success(t("general.success"));
      })
      .catch((err) => {
        toast.warning(t("AssetSource.noti.use"));
      });
  };
  
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };
  
  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props;
    for (let i = 0; i < list.length; i++) {
      try {
        await deleteItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    this.handleDialogClose();

    if (listAlert.length === list.length) {
      toast.warning(t("AssetSource.noti.use_all"));
    } else if (listAlert.length > 0) {
      toast.warning(t("AssetSource.noti.deleted_unused"));
    }
  }
  
  handleDeleteButtonClick = () => {
    let { t } = this.props;
    if (
      typeof this.state.data === "undefined" ||
      this.state.data.length === 0
    ) {
      toast.warning(t("general.noti_check_data"));
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };
  
  handleDeleteAll = (event) => {
    this.handleDeleteList(this.state.data).then(() => {
      this.updatePageData();
    });
  };
  
  handleGetAssetSourceById = async (rowData) => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let result = await getItemById(rowData.id);
      if (result?.data && result?.data?.code === 200) {
        this.setState({
          item: result?.data?.data,
          shouldOpenEditorDialog: true,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setPageLoading(false);
    }
  };
  
  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      isRoleOrgAdmin,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      // eslint-disable-next-line no-unused-expressions
      currentUser?.roles?.forEach((role) => {
        if (
          role.name === appConfig.ROLES.ROLE_ORG_ADMIN
        ) {
          if (!hasDeletePermission) {
            hasDeletePermission = true;
          }
          if (!hasEditPermission) {
            hasEditPermission = true;
          }
          if (!isRoleOrgAdmin) {
            isRoleOrgAdmin = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
        hasEditPermission,
        isRoleOrgAdmin,
      });
    }
  };
  
  render() {
    const { t, i18n } = this.props;
    let { keyword, shouldOpenNotificationPopup, isRoleOrgAdmin } = this.state;
    let TitlePage = t("AssetSource.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center"
        },
        headerStyle: {
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            isRoleOrgAdmin={isRoleOrgAdmin}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleGetAssetSourceById(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      { title: t("AssetSource.name"), field: "name", minWidth: "200" },
      {
        title: t("AssetSource.code"),
        field: "code",
        align: "left",
        maxWidth: 100,
        cellStyle: {
          textAlign: "center"
        },
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.category"), path: "/list/assetsource" },
              { name: TitlePage },
            ]}
          />
        </div>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.handleEditItem(null)}
            >
              {t("general.add")}
            </Button>

            {this.state.shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={this.state.shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                cancel={t("general.cancel")}
                agree={t("general.agree")}
                text={t("general.deleteAllConfirm")}
              />
            )}
            {shouldOpenNotificationPopup && (
              <NotificationPopup
                title={t("general.noti")}
                open={shouldOpenNotificationPopup}
                onYesClick={this.handleDialogClose}
                text={t(this.state.Notification)}
                agree={t("general.agree")}
              />
            )}
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("AssetSource.search")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={() => this.search(keyword)} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {this.state.shouldOpenEditorDialog && (
                <AssetSourceDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={this.state.item}
                />
              )}

              {this.state.shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={this.state.shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={this.state.itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.setState({ data: rows });
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
AssetSource.contextType = AppContext;
export default AssetSource;
