import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT + "/api/v1/asset-sources" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  let url = ConstantList.API_ENPOINT + "/api/v1/asset-sources/page";
  const config = { params: searchObject };
  return axios.get(url, config);
};

export const searchByText = (text, pageIndex, pageSize) => {
  let url = API_PATH + "/searchByText/" + pageIndex + "/" + pageSize;
  return axios.post(url, { keyword: text });
};

export const getItemById = (id) => {
  let API_PATH = ConstantList.API_ENPOINT + "/api/v1/asset-sources";
  let url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let API_PATH = ConstantList.API_ENPOINT + "/api/v1/asset-sources";
  let url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = (item) => {
  let url = ConstantList.API_ENPOINT + "/api/v1/asset-sources";
  return axios.post(url, item);
};
export const updateItem = (item) => {
  let url = ConstantList.API_ENPOINT + "/api/v1/asset-sources/" + item?.id;
  return axios.put(url, item);
};
