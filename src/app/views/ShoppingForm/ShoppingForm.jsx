import {
  Button,
  FormControl,
  Grid,
  Icon,
  IconButton, Input, InputAdornment, TablePagination, Tooltip, withStyles
} from '@material-ui/core';
import { Breadcrumb, ConfirmationDialog } from 'egret';
import React, { useContext, useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable from 'material-table';
import { deleteItem, getById, searchByPage } from './ShoppingFormService';
import ShoppingFormDialog from './ShoppingFormDialog';
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import AppContext from 'app/appContext';
import { appConst } from "../../appConst"
import { defaultPaginationProps, LightTooltip } from 'app/appFunction';

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

function MedicalEquipmentType() {
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const TitlePage = t("shoppingForm.title");

  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] = useState(false)
  const [shouldOpenEditorDialog, setShouldOpenEditorDialog] = useState(false)

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalElements, setTotalElements] = useState(0);
  const [keyword, setKeyword] = useState("");

  const [data, setData] = useState([]);
  const [itemShoppingForm, setItemShoppingForm] = useState({});
  const [itemList, setItemList] = useState([]);


  const updatePageData = () => {
    let searchObject = {};
    searchObject.keyword = keyword?.trim();
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchByPage(searchObject)
      .then(({ data }) => {
        let itemList = [...data.data.content];
        let list = []
        itemList.forEach(item => {
          let items = getListItemChild(item);
          list.push(...items);
        })
        list.sort((a, b) => a.code - b.code)
        setItemList([...list]);
        setTotalElements(data.data.totalElements);
      });
  }

  const getListItemChild = (item) => {
    let result = []
    result.push(item);
    // eslint-disable-next-line no-unused-expressions
    item?.children && item?.children.forEach(child => {
      let childs = getListItemChild(child);
      result.push(...childs)
    });
    return result;
  };
  useEffect(() => {
    updatePageData();
  }, [page, rowsPerPage])

  const search = () => {
    updatePageData();
  }

  const handleDelete = (item) => {
    setShouldOpenConfirmationDialog(true);
    setItemShoppingForm(item);
  }
  const handleDialogClose = () => {
    setShouldOpenConfirmationDialog(false);
    setShouldOpenEditorDialog(false);
    updatePageData();
  }
  const handleConfirmationResponse = () => {
    deleteItem(itemShoppingForm?.id)
      .then((res) => {
        if (appConst.CODE.SUCCESS === res?.data?.code) {
          toast.success(t("general.deleteSuccess"));
          if (itemList.length - 1 === 0 && page !== 0) {
            setPage(page - 1)
            setShouldOpenConfirmationDialog(false);
            return;
          }
          handleDialogClose();
        }
        else {
          toast.warning(res?.data?.message);
          handleDialogClose();
          updatePageData();
        }
      })
      .catch(() => {
        toast.warning(t("shoppingForm.notification.use"));
        toast.clearWaitingQueue();
      });
  }
  const handleEditItem = async (item) => {
    try {
      if (item?.parentId) {
        const data = await getById(item?.parentId);
        if (data?.status === appConst.CODE.SUCCESS) {
          item.parent = { ...(data?.data || item?.parent) }
        }
      }
      setItemShoppingForm(item);
      setShouldOpenEditorDialog(true);
    } catch (error) {

    }
  };
  const handleChangePage = (e, newPage) => {
    setPage(newPage);
  }
  const handleChangeRowsPerPage = (e) => {
    setRowsPerPage(e.target.value);
    setPage(0);
  }
  const handleTextChange = (e) => {
    setKeyword(e.target.value);
  }
  const handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      setPage(0);
      updatePageData();
    }
  }
  const handleKeyUp = (e) => {
    !e.target.value && search()
  }
  const columns = [
    {
      title: t("general.action"),
      field: "custom",
      align: "center",
      width: "250",
      headerStyle: {
        paddingLeft: "0px",
      },
      cellStyle: {
        paddingLeft: "0px",
      },
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
              handleEditItem(rowData)
              // setItemShoppingForm(rowData)
              // setShouldOpenEditorDialog(true)
            } else if (method === 1) {
              handleDelete(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    { title: t("shoppingForm.code"), field: "code", width: "150" },
    { title: t("shoppingForm.name"), field: "name", width: "150" },
  ]
  return (
    <div className="m-sm-30">
      <Helmet>
        <title>
          {TitlePage} | {t("web_site")}
        </title>
      </Helmet>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            { name: t("Dashboard.category"), path: "/list/medicalEquipmentType" },
            { name: TitlePage },
          ]}
        />
      </div>
      <Grid container spacing={2} justifyContent="space-between">
        <Grid item container md={3} xs={12}>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleEditItem(null)}
          >
            {t("general.add")}
          </Button>

          {shouldOpenEditorDialog && (
            <ShoppingFormDialog
              t={t}
              open={shouldOpenEditorDialog}
              handleClose={handleDialogClose}
              item={itemShoppingForm}
            />
          )}
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box w-100"
              placeholder={t("shoppingForm.search")}
              id="search_box"
              name="keyword"
              value={keyword}
              onKeyDown={handleKeyDownEnterSearch}
              onChange={handleTextChange}
              onKeyUp={handleKeyUp}
              startAdornment={
                <InputAdornment position='end'>
                  <Link to="#">
                    <SearchIcon
                      onClick={() => search()}
                      style={{ position: "absolute", top: "0", right: "0" }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>

        <Grid item xs={12}>

          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={handleDialogClose}
              onYesClick={handleConfirmationResponse}
              text={t("general.deleteConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}

          <MaterialTable
            title="Danh sách hình thức mua sắm"
            columns={columns}
            data={itemList}
            parentChildData={(row, rows) => {
              let list = rows.find((item) => item.id === row.parentId)
              return list
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: itemList?.length > 0
                ? 0 : 250,
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            onSelectionChange={(rows) => {
              setData(rows)
            }}
          />

          <TablePagination
            { ...defaultPaginationProps() }
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            count={totalElements}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
    </div>
  )
}

export default MedicalEquipmentType;