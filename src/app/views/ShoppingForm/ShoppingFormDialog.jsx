import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import SelectInstrumentToolsParentPopup from './SelectInstrumentToolsParentPopup';
import { saveItem, updateItem } from './ShoppingFormService';
import {appConst, variable} from "../../appConst"
import { PaperComponent } from "../Component/Utilities";
import { handleKeyDownNameSpecialExcept } from "../../appFunction";

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});

function ShoppingFormDialog(props) {
    const {
        open,
        t,
        item = {},
        handleClose
    } = props;

    const [itemShoppingForm, setItemShoppingForm] = useState({});
    const [shouldOpenSelectParentPopup, SetShouldOpenSelectParentPopup] = useState(false);

    useEffect(() => {
        if (item) {
            setItemShoppingForm({ ...item })
        }
    }, [item])


    const openParentPopup = () => {
        SetShouldOpenSelectParentPopup(true)
    }

    const handleFormSubmit = () => {
        let { id } = itemShoppingForm

        if (id) {
            updateItem(itemShoppingForm)
                .then((res) => {
                    if (appConst.CODE.SUCCESS === res?.data?.code) {
                        toast.success(t("shoppingForm.notification.updateSuccess"));
                        handleClose();
                    }
                    else {
                        toast.error(res?.data?.message)
                    }
                })
                .catch(() => {
                    toast.error(t("shoppingForm.notification.updateFail"))
                })
        } else {
            saveItem(itemShoppingForm)
                .then((res) => {
                    if (appConst.CODE.SUCCESS === res?.data?.code) {
                        handleSetDataAsset(res?.data.data);
                        toast.success(t("shoppingForm.notification.addSuccess"));
                        handleClose();
                    }
                    else {
                        toast.error(res?.data?.message)
                    }
                })
                .catch(() => {
                    toast.error(t("shoppingForm.notification.addFali"))
                })
        }
    }

    const handleSetDataAsset = (data) => {
        if (props.selectShoppingForm) props.selectShoppingForm(data);
        if (props.handleSetDataSelect) props.handleSetDataSelect([]);
    }

    const handleChangeCode = (e) => {
        setItemShoppingForm({
            ...itemShoppingForm,
            code: e.target.value,
        })
    }
    const handleChangeName = (e) => {
        setItemShoppingForm({
            ...itemShoppingForm,
            name: e.target.value
        })
    }
    const handleClosePopup = () => {
        SetShouldOpenSelectParentPopup(false);
    }
    const handleSelectParent = (parent) => {
        setItemShoppingForm({
            ...itemShoppingForm,
            parent: parent,
            parentId: parent?.id
        })
        SetShouldOpenSelectParentPopup(false)
    }
    return (
        <Dialog
            open={open}
            PaperComponent={PaperComponent}
            maxWidth="sm"
            fullWidth
        >
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                {t("shoppingForm.saveUpdate")}
            </DialogTitle>
            <ValidatorForm onSubmit={handleFormSubmit}>
                <DialogContent>
                    <Grid container spacing={1}>
                        <Grid item sm={12} xs={12}>
                            <Button
                                size="small"
                                className="float-right mt-10"
                                variant="contained"
                                color="primary"
                                onClick={openParentPopup}
                            >
                                {t('general.select')}
                            </Button>
                            <TextValidator
                                size="small"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label={<span><span className="colorRed"></span>{t("shoppingForm.parent")}</span>}
                                className="w-85"
                                value={itemShoppingForm?.parent !== null ? itemShoppingForm?.parent?.name || '' : ''}
                            />

                            {shouldOpenSelectParentPopup && (
                                <SelectInstrumentToolsParentPopup
                                    t={t}
                                    open={shouldOpenSelectParentPopup}
                                    handleClose={handleClosePopup}
                                    handleSelect={handleSelectParent}
                                    item={itemShoppingForm}
                                />
                            )}
                        </Grid>
                        <Grid item sm={12} xs={12}>
                            <TextValidator
                                className="w-100 "
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("shoppingForm.code")}</span>
                                    </span>
                                }
                                onChange={handleChangeCode}
                                type="text"
                                name="code"
                                value={itemShoppingForm?.code}
                                validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                                errorMessages={[t("general.required"), t("general.regexCode")]}
                            />
                        </Grid>

                        <Grid item sm={12} xs={12}>
                            <TextValidator
                                className="w-100"
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("shoppingForm.name")}</span>
                                    </span>
                                }
                                onChange={handleChangeName}
                                onKeyDown={(e) => {
                                    handleKeyDownNameSpecialExcept(e)
                                }}
                                type="text"
                                name="name"
                                value={itemShoppingForm?.name}
                                validators={["required", "matchRegexp:^.{1,255}$", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                                errorMessages={[t("general.required"), "Tối đa 255 ký tự", t("general.invalidFormat")]}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-12"
                            onClick={() => handleClose()}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            style={{ marginRight: "13px" }}
                        >
                            {t("general.save")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
}

export default ShoppingFormDialog;