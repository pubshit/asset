import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_PERSON =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_ALLOCATION =
  ConstantList.API_ENPOINT + "/api/v1/fixed-assets/allocation-vouchers";
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
const API_PATH_TEMPLATE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

//api mới
export const searchByPage = (params) => {
  let url = API_PATH_NEW + `/instruments-and-tools/phieu-linh/search-by-page`;
  let config = {
    params,
  };
  return axios.get(url, config);
};

export const createAllocation = (data) => {
  return axios.post(API_PATH_ALLOCATION, data);
};
export const createReceipt = (data) => {
  return axios.post(API_PATH_NEW + "/instruments-and-tools/phieu-linh", data);
};
export const updateReceipt = (id, data) => {
  let url = API_PATH_NEW + "/instruments-and-tools/phieu-linh/" + id;
  return axios.put(url, data);
};
export const getItemById = (id) => {
  let url = API_PATH_NEW + "/instruments-and-tools/phieu-linh/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let url = API_PATH_NEW + "/instruments-and-tools/phieu-linh/" + id;
  return axios.delete(url);
};
export const getCountStatus = () => {
  let url = API_PATH_NEW + "/instruments-and-tools/phieu-linh/count-by-status";
  return axios.get(url);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/cap-phat-tai-san-co-dinh",
    data: searchObject,
    responseType: "blob",
  });
};

//get phòng ban tiếp nhận
export const searchReceiverDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

//api mới
const API_PATH_VOUCHERSALLOCATION =
  ConstantList.API_ENPOINT + "/api/instruments-and-tool/allocation-vouchers";

export const getListQRCode = (id) => {
  let url = API_PATH_NEW + `/voucher/print-asset-qr-code/${id}`;
  return axios.get(url);
};

export const exportExampleImportExcelAssetAllocation = () => {
  return axios({
    method: "post",
    url: API_PATH_TEMPLATE + "/import-allocated-fixed-asset",
    responseType: "blob",
  });
};

export const importExcelAssetAllocationURL = () => {
  return (
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
    "/api/fixed-assets/transfer-vouchers/excel/import"
  );
};

//get phòng ban bàn giao
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};

export const getAccessoriesByVoucherId = (id) => {
  let url = API_PATH_NEW + `/voucher/${id}/fixed-asset/accessories`;
  return axios.get(url);
};
