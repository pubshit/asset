import {
  AppBar,
  Icon,
  IconButton,
} from "@material-ui/core";
import { variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  convertFromToDate,
  formatDateTypeArray,
  getTheHighestRole,
  handleKeyDown,
  handleThrowResponseMessage,
  isValidDate, formatTimestampToDate,
  checkCount,
} from "app/appFunction";
import { Breadcrumb } from "egret";
import FileSaver from "file-saver";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  deleteItem,
  exportToExcel as assetAllocationExportToExcel,
  getItemById,
  searchByPage,
  getCountStatus,
} from "./ReceiptService";
import ComponentReceiptTable from "./ComponentReceiptTable";
import ListAssetTable from "./ListAssetTable";
import { TabPanel } from "../Component/Utilities";
import { appConst } from "../../appConst";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { GeneralTab } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const {
    hasDeletePermission,
    hasPrintPermission,
    item,
  } = props;
  const { isRoleAssetUser, departmentUser } = getTheHighestRole();

  const isNew = appConst.STATUS_RECEIPT.waitApprove.indexOrder === item?.status; // Đang thống kê
  const shouldShowDeleteIcon = hasDeletePermission && isNew;
  const isAllowEdit = [
    appConst.STATUS_RECEIPT.refuse.indexOrder
  ].includes(item?.status)
  && ((departmentUser?.id === item?.receiptDepartmentId) || departmentUser?.id === item?.createByDepartmentId);
  const isXacNhan = [
    appConst.STATUS_RECEIPT.approved.indexOrder,
    appConst.STATUS_RECEIPT.review.indexOrder,
  ].includes(item?.status);

  const shouldShowApproveIcon = isXacNhan && !isRoleAssetUser;
  return (
    <div className="none_wrap">
      {(isAllowEdit || isNew) && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {(shouldShowDeleteIcon) && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {shouldShowApproveIcon && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.hoverCheck")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.check)}
          >
            <CheckCircleIcon className="iconCheck" />
          </IconButton>
        </LightTooltip>
      )}
      {hasPrintPermission && (
        <LightTooltip
          title={t("In phiếu")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class ReceiptTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    item: {},
    shouldOpenPrint: false,
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenNotificationPopup: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    hasSuperAccess: false,
    tabValue: 0,
    isCheckReceiverDP: true,
    isCheckHandoverDP: true,
    itemAsset: {},
    openAdvanceSearch: false,
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    voucherId: "",
    openPrintQR: false,
    products: [],
    status: null,
    isAllowEditQuantityReq: false,
  };


  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      status: null,
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });

    if (appConst.OBJECT_TAB_RECEIPT.all === newValue) {
      this.setState({
        status: null,
      }, () => this.updatePageData());
    }
    if (appConst.OBJECT_TAB_RECEIPT.waitApprove === newValue) {
      this.setState({
        status: appConst.STATUS_RECEIPT.waitApprove,
      }, () => this.updatePageData());
    }
    if (appConst.OBJECT_TAB_RECEIPT.approved === newValue) {
      this.setState({
        status: appConst.STATUS_RECEIPT.approved,
      }, () => this.updatePageData());
    }
    if (appConst.OBJECT_TAB_RECEIPT.allocated === newValue) {
      this.setState({
        status: appConst.STATUS_RECEIPT.allocated,
      }, () => this.updatePageData());
    }
    if (appConst.OBJECT_TAB_RECEIPT.refuse === newValue) {
      this.setState({
        status: appConst.STATUS_RECEIPT.refuse,
      }, () => this.updatePageData());
    }
    if (appConst.OBJECT_TAB_RECEIPT.review === newValue) {
      this.setState({
        status: appConst.STATUS_RECEIPT.review,
      }, () => this.updatePageData());
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let {
      toDate,
      fromDate,
      status,
      handoverDepartment,
      receiverDepartment,
    } = this.state;
    let searchObject = {};

    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.status = status?.indexOrder;
    if (this.state.keyword) {
      searchObject.keyword = this.state.keyword.trim();
    }
    if (handoverDepartment) {
      searchObject.departmentId = handoverDepartment?.id;
    }
    if (receiverDepartment) {
      searchObject.receiptDepartmentId = receiverDepartment?.id;
    }
    if (status && !status) {
      searchObject.status = status?.indexOrder;
    }
    if (fromDate) {
      searchObject.fromDate = convertFromToDate(fromDate).fromDate;
    }
    if (toDate) {
      searchObject.toDate = convertFromToDate(toDate).toDate;
    }
    setPageLoading(true);
    searchByPage(searchObject)
      .then(({ data }) => {
        this.setState(
          {
            itemList: [...data?.data?.content],
            totalElements: data?.data?.totalElements,
            itemAsset: {},
            page: !data?.data?.content?.length ? 0 : this.state.page,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        setPageLoading(false);
      });
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrint: false,
      isVisibility: false,
      isAllowEditQuantityReq: false,
      item: null,
      statusEdit: null
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      isVisibility: false,
      item: null,
      statusEdit: null
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleDeleteAssetAllocation = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetAllocation = (item) => {
    let { t } = this.props;
    getItemById(item.id)
      .then((result) => {
        this.setState({
          item: result.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    deleteItem(this.state.id)
      .then((res) => {
        let data = res?.data
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("general.deleteSuccess"));
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          } else {
            this.updatePageData();
          }
          this.handleDialogClose();
          return;
        }
        handleThrowResponseMessage(res);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleViews({
            id: location?.state?.id
          })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleApprove({
            id: location?.state?.id
          })
          break;
        default:
          break;
      }
      history.push(location?.state?.path, null);
    }

    this.setState({
      status: null,
    }, () => {
      this.getRoleCurrentUser();
      this.updatePageData()
      this.getCountStatus();
    });
  }

  getRoleCurrentUser = () => {
    let
      hasDeletePermission = false,
      hasEditPermission = false,
      hasPrintPermission = false,
      hasCreatePermission = false,
      hasSuperAccess = false,
      receiverDepartment = false;
    let roles = getTheHighestRole();
    let {
      isRoleAssetUser,
      isRoleAssetManager,
      isRoleAccountant,
      isRoleAdmin,
      isRoleOrgAdmin,
    } = roles;

    hasEditPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAccountant || isRoleAssetUser;
    hasSuperAccess = isRoleOrgAdmin;
    hasDeletePermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasCreatePermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAccountant || isRoleAssetUser;
    hasPrintPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAccountant || isRoleAssetUser;

    this.setState({
      ...roles,
      hasSuperAccess,
      hasEditPermission,
      receiverDepartment,
      hasPrintPermission,
      hasDeletePermission,
      hasCreatePermission,
      isRoleAssetUser,
      isRoleAdmin
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handlePrintAll = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    this.setState({
      item: assetAllocation,
      shouldOpenPrint: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props;
    this.handleDeleteList(this.data)
      .then(() => {
        this.updatePageData();
        this.handleDialogClose();
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate,
      currentUser,
      status,
      handoverDepartment,
      receiverDepartment,
      voucherId,
      keyword
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.TSCD,
        voucherId: voucherId,
      };
      searchObject.keyword = keyword?.trim();
      if (handoverDepartment) {
        searchObject.handoverDepartmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiverDepartmentId = receiverDepartment?.id;
      }
      if (status) {
        searchObject.status = status?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }

      const res = await assetAllocationExportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "AssetAllocation.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  handleNotificationPopup = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  setStateAllocation = (item) => {
    this.setState(item);
  };

  convertDataDto = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { data } = await getItemById(rowData?.id);
    setPageLoading(false);
    let itemEdit = data?.data ? data?.data : {};
    
    itemEdit.statusEdit = itemEdit?.status;
    itemEdit.createDate = formatDateTypeArray(itemEdit?.createDate);
    itemEdit.suggestDate = formatDateTypeArray(itemEdit?.suggestDate);
    itemEdit.status = appConst.LIST_RECEIPT.find(i => i.indexOrder === itemEdit?.status);

    return itemEdit;
  };

  handleApprove = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    assetAllocation.isCheckHandoverDP = false;
    assetAllocation.isCheckReceiverDP = false;
    assetAllocation.isConfirm = true;
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };

  handleEdit = async (rowData) => {
    let itemEdit = await this.convertDataDto(rowData);
    itemEdit.isCheckHandoverDP = false;
    itemEdit.isCheckReceiverDP = false;

    let status = [
      appConst.STATUS_RECEIPT.waitApprove.indexOrder,
      appConst.STATUS_RECEIPT.approved.indexOrder,
    ].includes(itemEdit?.status.indexOrder);
    let isAllowEditQuantityReq = itemEdit?.status.indexOrder === appConst.STATUS_RECEIPT.refuse.indexOrder;
    if (status) {
      this.setState({
        item: itemEdit,
        shouldOpenEditorDialog: true,
        isVisibility: false,
        isStatus: false,
        isHideButton: false,
        isAllowEditQuantityReq,
      });
      return;
    }
    this.setState({
      item: itemEdit,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: true,
    });
  };

  handleViews = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isAllowEditQuantityReq: true,
      isStatus: false,
    });
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countIsCounting,
          countIsCounted,
          countIsConfirm,
          countIsRefuse,
          countIsReivew;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code === item?.trangThai) {
            countIsCounting = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code === item?.trangThai) {
            countIsCounted = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code === item?.trangThai) {
            countIsConfirm = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.TU_CHOI.code === item?.trangThai) {
            countIsRefuse = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code === item?.trangThai) {
            countIsReivew = checkCount(item?.soLuong);
            return;
          }
        });

        this.setState({
          countIsCounting,
          countIsCounted,
          countIsConfirm,
          countIsRefuse,
          countIsReivew
        }, () => setPageLoading(false));
      })
      .catch((e) => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  checkStatus = (statusIndex) => {
    let { waitApprove, approved, allocated, refuse, review } = appConst.STATUS_RECEIPT;
    let itemStatus = appConst.LIST_RECEIPT.find((item) => item.indexOrder === statusIndex);
    let classNameByStatusIndex = {
      [waitApprove.indexOrder]: 'info',
      [approved.indexOrder]: 'warning',
      [allocated.indexOrder]: 'success',
      [refuse.indexOrder]: 'error',
      [review.indexOrder]: 'success',
    }

    return (
      <span className={`status status-${classNameByStatusIndex[statusIndex]}`}>
        {itemStatus?.name}
      </span>
    )
  };

  handleSetItemState = async (rowData) => {
    let itemEdit = await this.convertDataDto(rowData);
    this.setState({
      itemAsset: itemEdit || {},
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name, source) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.originalCostFrom !== name &&
        variable.listInputName.listData !== source
        // && !this.state.statusIndex
      ) {
        this.search();
      }
    });
  };
  getListTab = () => {
    let { t } = this.props;
    let { tabAll, tabSuggest, tabConfirm, tabRefuse, tabApproved, tabReview } = appConst.tabSuggestSupplies;
    return [
      {
        key: tabAll,
        label: t("SuggestedSupplies.tab.tabAll")
      },
      {
        key: tabSuggest,
        label: t("SuggestedSupplies.tab.tabSuggest"),
        count: this.state.countIsCounting || 0,
        type: "info",
      },
      {
        key: tabConfirm,
        label: t("SuggestedSupplies.tab.tabConfirm"),
        count: this.state.countIsCounted || 0,
        type: "warning",
      },
      {
        key: tabReview,
        label: t("SuggestedSupplies.tab.review"),
        count: this.state.countIsReivew || 0,
        type: "success",
      },
      {
        key: tabRefuse,
        label: t("SuggestedSupplies.tab.tabRefuse"),
        count: this.state.countIsConfirm || 0,
        type: "success",
      },
      {
        key: tabApproved,
        label: t("SuggestedSupplies.tab.tabApproveds"),
        count: this.state.countIsRefuse || 0,
        type: "error",
      },
    ];
  };
  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasSuperAccess,
      roles,
      tabValue,
    } = this.state;
    let TitlePage = t("Dashboard.management.receipt");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasPrintPermission={hasPrintPermission}
            roles={roles}
            hasSuperAccess={hasSuperAccess}
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrintAll(rowData);
              } else if (appConst.active.view === method) {
                this.handleViews(rowData);
              } else if (appConst.active.check === method) {
                this.handleApprove(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t('receipt.suggestDate'),
        field: "suggestDate",
        align: "left",
        minWidth: 110,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.suggestDate
          ? formatTimestampToDate(rowData.suggestDate)
          : "",
      },
      {
        title: t("allocation_asset.status"),
        field: "status",
        minWidth: 150,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.status),
      },
      {
        title: t("receipt.name"),
        field: "name",
        minWidth: 250,
        align: "left",
      },
      {
        title: t("receipt.code"),
        field: "code",
        minWidth: 150,
        align: "center",
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "departmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "personName",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiptDepartmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiptPersonName",
        minWidth: "250px",
        align: "left",
      },
    ];

    const tabs = this.getListTab();
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/inventory-count-vouchers",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <GeneralTab tabs={tabs} value={tabValue} onChange={this.handleChangeTabValue} />
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabAll}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabSuggest}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabConfirm}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabRefuse}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabApproved}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabReview}
          className="mp-0"
        >
          <ComponentReceiptTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
      </div>
    );
  }
}

ReceiptTable.contextType = AppContext;
export default ReceiptTable;
