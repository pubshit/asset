import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { TextField } from "@material-ui/core";
import { convertNumberPrice } from "app/appFunction";

function ListAssetTable(props) {
  const t = props?.t;
  let columns = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1
    },
    {
      title: t("InstrumentsToolsInventory.name"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("instrumentsToolsReceipt.quantity"),
      field: "dxRequestQuantity",
      align: "center",
      minWidth: 80,
    },
    {
      title: t("Asset.stockKeepingUnitTable"),
      field: "skuName",
      minWidth: "100px",
      align: "center",
    },
    {
      title: t("InstrumentToolsList.originalPrice"),
      field: "originalCost",
      minWidth: "120px",
      align: "center",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.originalCost || 0)
    },
    {
      title: t("Model"),
      field: "model",
      minWidth: "120px",
    },
    {
      title: t("InstrumentToolsList.serialNumber"),
      field: "serialNumber",
      minWidth: "120px",
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
      render: (rowData) =>
        <TextField
          multiline
          className="w-100"
          type="text"
          name="note"
          value={rowData.note}
          InputProps={{
            readOnly: true,
            disableUnderline: true,
          }}
        />
    },
  ];
  return (
    <MaterialTable
      data={props?.itemAsset?.assetVouchers ? props?.itemAsset?.assetVouchers : []}
      columns={columns}
      options={{
        draggable: false,
        toolbar: false,
        selection: false,
        actionsColumnIndex: -1,
        paging: false,
        search: false,
        sorting: false,
        padding: "dense",
        rowStyle: (rowData) => ({
          backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
        }),
        headerStyle: {
          backgroundColor: "#358600",
          color: "#fff",
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        maxBodyHeight: "225px",
        minBodyHeight: "225px",
      }}
      localization={{
        body: {
          emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
        },
      }}
      components={{
        Toolbar: (props) => (
          <div style={{ witdth: "100%" }}>
            <MTableToolbar {...props} />
          </div>
        ),
      }}
      onSelectionChange={(rows) => {
        this.data = rows;
      }}
    />
  );
}

export default ListAssetTable;
