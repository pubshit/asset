import React from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import {
  convertNumberPriceRoundUp,
  getTheHighestRole, getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import Title from "../FormCustom/Component/Title";
import NationalName from "../FormCustom/Component/NationalName";
import DateToText from "../FormCustom/Component/DateToText";
import Signature from "../FormCustom/Component/Signature";
import AppContext from "../../appContext";
import { getAccessoriesByVoucherId } from "./ReceiptService";
import { toast } from "react-toastify";
import moment from "moment";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontWeight: "bold",
    marginBottom: "0px",
  },
  text: {
    fontSize: "0.8rem",
    fontWeight: "bold",
    marginTop: "0px",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: "bold",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_3: {
    border: "1px solid",
    width: "3%",
    textAlign: "center",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
  },
  w_9: {
    border: "1px solid",
    width: "9%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
    textAlign: "center",
  },
  w_11: {
    border: "1px solid",
    width: "11%",
  },
  w_12: {
    border: "1px solid",
    width: "12%",
  },
  w_13: {
    border: "1px solid",
    width: "13%",
  },
  w_15: {
    border: "1px solid",
    width: "15%",
  },
  w_20: {
    border: "1px solid",
    width: "20%",
  },
  border: {
    border: "1px solid",
    padding: "0 5px",
    fontSize: "0.975rem",
  },
  name: {
    border: "1px solid",
    paddingLeft: "5px",
    textAlign: "left",
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "left",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 12%",
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: "flex",
    justifyContent: "space-between",
    textTransform: "uppercase",
  },
  textCenter: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    width: "33%",
  },
  marginTop25minus: {
    marginTop: -25,
  },
  pt_30: {
    paddingTop: 30,
  },
  pt_60: {
    paddingTop: 60,
  },
  a4Container: {
    width: "21cm",
    fontSize: "18px",
  },
  wordBreak: {
    wordBreak: "break-all"
  }
};

class ReceiptPrint extends React.Component {
  state = {
    AssetAllocation: [],
    item: {},
    asset: {},
    assetVouchers: [],
    accessories: [],
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
  };


  componentDidMount() {
    let countItem = this.props.item.assetVouchers?.reduce(
      (value, item) => value + Number(item?.dxRequestQuantity || 1), 0
    );
    let sumTotalCost = this.props.item.assetVouchers?.reduce(
      (value, item) => value + Number(item?.assetOriginalCost || 0), 0
    );
    let roles = getTheHighestRole();
    this.setState({
      ...this.props.item,
      countItem,
      sumTotalCost,
      ...roles,
    }, this.handleGetAccessories);
  }

  handleGetAccessories = async () => {
    let { item, t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let res = await getAccessoriesByVoucherId(item?.id);
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        this.setState({
          accessories: data,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenViewDialog: false,
      shouldOpenExportDialog: false,
    });
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  render() {
    const { open, t, item } = this.props;
    let { countItem, sumTotalCost, currentUser } = this.state;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">
            {t("Phiếu lĩnh tài sản cố định")}
          </span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
        ></iframe>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogContent id="divcontents" className="dialog-print">
            <div className="form-print" style={{ ...style.a4Container }} >
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div style={{
                  display: "flex"
                }}>
                  <div style={{
                    width: "60px",
                    background: "#1A5E83",
                    height: "60px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: "50%",
                    marginRight: 20,
                    padding: 5,
                    overflow: "hidden"
                  }}>
                    <img style={{ width: "100%" }} src="/assets/images/logos/logo-x-amis-white.png" alt="" />
                  </div>
                  <div>
                    <div>SỞ Y TẾ HÀ NỘI</div>
                    <div style={{ ...style.title }}>{currentUser?.org?.name}</div>
                  </div>
                </div>
                <div style={{ minWidth: 170 }}>
                  Số phiếu: <b>{item?.code}</b>
                  <br></br>
                  Ngày lập: <b><span>{moment(item?.suggestDate || new Date()).format("DD/MM/YYYY")}</span></b>
                  <br></br>
                  Lần in: <b>3</b>
                </div>
              </div>
              <div style={{
                marginLeft: 80
              }}>
                Nơi nhập: <b>{this.state?.handoverDepartment?.name}</b>
                <br></br>
                Nơi xuất: <b>{this.state?.receiverDepartment?.name}</b>
              </div>

              <div >
                <div style={{ textAlign: "center" }}>
                  <Title title={"PHIẾU LĨNH VẬT TƯ"} />
                </div>
                <div style={{ textAlign: "center" }}>
                  01/02/2024 {"->"} 18/03/2024
                </div>
                <p style={style.text}>
                </p>
              </div>
              <div>
                {
                  <table style={style.table} className="table-report">
                    <thead>
                      <tr>
                        <th style={style.w_3} rowSpan={2}>STT</th>
                        <th style={{ ...style.w_11, ...style.wordBreak }} rowSpan={2}>Mã vật tư</th>
                        <th style={{ ...style.w_15 }} rowSpan={2}>Tên vật tư, hóa chất</th>
                        <th style={style.w_12} rowSpan={2}>Đơn vị</th>
                        <th style={style.w_12} colSpan={2}>
                          Số lượng
                        </th>
                        <th style={{ ...style.w_7 }} rowSpan={2}>Ghi chú</th>
                      </tr>
                      <tr>
                        <th style={{ ...style.border }}>Yêu cầu</th>
                        <th style={{ ...style.border }}>Được sửa</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.assetVouchers?.map((row, index) => {
                        return (
                          <tr>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {index + 1 || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.asset?.code || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.asset?.name || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {row?.skuName || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {row?.dxRequestQuantity}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >

                            </td>
                            <td
                              style={{
                                ...style.border
                              }}
                            >
                              {row?.note}
                            </td>
                          </tr>
                        );
                      })}
                      <tr>
                        <td
                          colSpan={1}
                          style={{ ...style.border }}
                        >
                        </td>
                        <td
                          colSpan={3}
                          style={{
                            ...style.border,
                            ...style.textAlignLeft,
                          }}
                        >
                          <i>Cộng khoản: {this.state.assetVouchers?.length || 0}</i>
                        </td>
                        <td
                          colSpan={1}
                          style={{ ...style.border, ...style.textAlignCenter }}
                        >
                          <b>{countItem}</b>
                        </td>
                        <td
                          colSpan={1}
                          style={{ ...style.border, ...style.textAlignLeft }}
                        ></td>
                        <td
                          colSpan={1}
                          style={{ ...style.border }}
                        >
                        </td>
                      </tr>
                    </tbody>
                  </table>
                }
              </div>
              <div style={{ display: "flex", justifyContent: "end", marginTop: 20 }}>
                <DateToText date={new Date()} />
              </div>
              <div style={{ display: "flex", justifyContent: "space-between", minHeight: 200 }}>
                <Signature mNone titleProps={{ marginTop: 0 }} title="Duyệt cấp" />
                <Signature mNone titleProps={{ marginTop: 0 }} title="Người lập dự trù" nameProps={{ marginTop: 120, fontSize: 18 }} />
                <Signature mNone titleProps={{ marginTop: 0 }} title="Trưởng khoa điều trị" nameProps={{ marginTop: 120, fontSize: 18 }} />
              </div>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <Signature titleProps={{ marginTop: 0 }} title="Người lập phiếu" />
                <Signature titleProps={{ marginTop: 0 }} title="Trưởng Khoa/Phòng" name={this.state?.handoverPersonName} nameProps={{ marginTop: 120, fontSize: 18 }} />
                <Signature titleProps={{ marginTop: 0 }} title="Phòng quản lý" nameProps={{ marginTop: 120, fontSize: 18 }} />
              </div>
            </div>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle mr-16">
              <Button
                variant="contained"
                color="secondary"
                className="mr-16"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

ReceiptPrint.contextType = AppContext;
export default ReceiptPrint;
