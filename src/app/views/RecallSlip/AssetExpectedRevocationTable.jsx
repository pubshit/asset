import React, { useEffect } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { getListManagementDepartment, getListUserByDepartmentId } from './RecallSlipService';
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import viLocale from "date-fns/locale/vi";
import { useState } from "react";
import { createFilterOptions } from "@material-ui/lab";
import { a11yProps, checkInvalidDate, convertNumberPriceRoundUp, filterOptions, isCheckLenght } from "app/appFunction";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { appConst } from "app/appConst";
import { getAllManagementDepartmentByOrg } from "../Department/DepartmentService";
import { LightTooltip, TabPanel } from "../Component/Utilities";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AssetExpectedRevocationTable(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });
  let transferStatus = appConst.expectedRecoveryStatus.find(status => status.indexOrder === props?.item?.transferStatus?.indexOrder) || "";

  const searchObject = { pageIndex: 1, pageSize: 1000000 };
  const searchObjectDepartment = { pageIndex: 1, pageSize: 1000000, checkPermissionUserDepartment: false }
  const [listReceiverPerson, setListReceiverPerson] = useState(props?.item?.listReceiverPerson || []);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? '',
      pageSize: 10000
    });

    setSearchParamUserByReceiverDepartment({
      departmentId: props?.item?.receiverDepartment?.id ?? '',
      pageSize: 10000
    });

  }, [props?.item?.handoverDepartment, props?.item?.receiverDepartment]);


  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255)
    })
  }, [props?.item?.assetVouchers]);

  useEffect(() => {
    setListReceiverPerson(props?.item?.listReceiverPerson)
  }, [props?.item]);

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      hidden: props?.item?.isView,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData?.asset?.id);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
      maxWidth: 400,
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.asset?.serialNumber || rowData?.assetSerialNumber
    },
    {
      title: t("Asset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.asset?.model || rowData?.assetModel
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "asset.yearPutIntoUse",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "asset.manufacturer.name",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
      render: (rowData) => rowData?.asset?.manufacturer?.name || rowData?.manufacturerName
    },
    {
      title: t("Asset.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
        : "",
    },
    {
      title: t("Asset.carryingAmount"),
      field: "asset.carryingAmount",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.asset?.carryingAmount
        ? convertNumberPriceRoundUp(rowData?.asset?.carryingAmount)
        : "",
    },
    {
      title: t("Asset.managementDepartment"),
      field: "",
      align: "left",
      minWidth: 250,
      maxWidth: 250,
      render: (rowData) => rowData.asset?.managementDepartmentName || "",
    },
    {
      title: t("RecallSlip.importStatus"),
      field: "warehouseStatus",
      align: "left",
      minWidth: 180,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        <TextValidator
          className="w-100"
          onChange={warehouseStatus => props.handleRowDataCellChange(rowData, warehouseStatus.target.value, "warehouseStatus")}
          type="text"
          name="warehouseStatus"
          value={rowData.warehouseStatus || ""}
          InputProps={{
            readOnly: props?.item?.isView
          }}
        />
    },
    {
      title: t("Asset.note"),
      field: "note",
      align: "left",
      minWidth: 180,
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note.target.value, "note")}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props?.item?.isView
          }}
          validators={["isLengthValid"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
    }
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      render: rowData =>
        // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  }

  return (
    <form>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} type="submit" />
            {/*<Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />*/}
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={1} justifyContent="space-between">
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("AssetTransfer.issueDate")}
                    </span>
                  }
                  name={'suggestDate'}
                  inputVariant="standard"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.item.suggestDate}
                  onChange={date => props?.handleDateChange(date, "suggestDate")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  InputProps={{
                    readOnly: props?.item?.isView
                  }}
                  open={isPickerOpen}
                  onOpen={() => {
                    if (props?.item?.isView) {
                      setIsPickerOpen(false);
                    }
                    else {
                      setIsPickerOpen(true);
                    }
                  }}
                  onClose={() => setIsPickerOpen(false)}
                  validators={['required']}
                  errorMessages={t('general.required')}
                  minDate={new Date("01/01/1900")}
                  minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
                  maxDate={new Date()}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                  onBlur={() => handleBlurDate(props?.item.issueDate, "issueDate")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.isRoleAssetUser) ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.handoverDepartment")}
                    </span>
                  }
                  value={props?.item?.handoverDepartment?.text ? props?.item?.handoverDepartment?.text : ''}
                /> :
                <AsynchronousAutocompleteTransfer
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.handoverDepartment")}
                    </span>
                  }
                  searchFunction={getListManagementDepartment}
                  searchObject={searchObject}
                  disabled={props?.isFormMaintainRqquest}
                  listData={props?.item.listHandoverDepartment || []}
                  setListData={(data) => props?.handleSetDataSelect(data, "listHandoverDepartment")}
                  defaultValue={props?.item?.handoverDepartment ? props?.item?.handoverDepartment : ''}
                  displayLable={"text"}
                  value={props?.item?.handoverDepartment ? props?.item?.handoverDepartment : ''}
                  onSelect={handoverDepartment => props?.handleSelectHandoverDepartment(handoverDepartment)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              }
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView) ?
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('allocation_asset.handoverPerson')}
                    </span>
                  }
                  name="handoverPerson"
                  value={props?.item?.personName ?? ''}
                  InputProps={{
                    readOnly: props?.item?.isView
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.handoverPerson")}
                    </span>
                  }
                  disabled={!props?.item?.handoverDepartment?.id || props.item?.isView}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByHandoverDepartment}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={props?.item?.handoverPerson ?? ''}
                  onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {/* {(!props?.item?.isNew && !) || !props?.item?.id ? */}
              {props?.item?.isView ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  value={props?.item?.transferStatus?.name || ""}
                /> : <AsynchronousAutocompleteTransfer
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  listData={appConst.expectedRecoveryStatus || []}
                  setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                  value={props?.item?.transferStatus ? props?.item?.transferStatus : null}
                  displayLable={'name'}
                  disabled={props?.isFormMaintainRqquest}
                  onSelect={props.selectAssetTransferStatusStatus}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  typeReturnFunction="status"
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView) ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: props?.item?.receiptDepartmentName,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.receiverDepartment")}
                    </span>
                  }
                  value={props?.item?.receiptDepartmentName ? props?.item?.receiptDepartmentName : ""}

                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.receiverDepartment")}
                    </span>
                  }
                  searchFunction={getAllManagementDepartmentByOrg}
                  listData={props.item.listReceiverDepartment || []}
                  setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
                  typeReturnFunction="list"
                  displayLable={"text"}
                  isNoRenderChildren
                  isNoRenderParent
                  disabled={props?.isFormMaintainRqquest}
                  value={props?.item?.receiverDepartment ? props?.item?.receiverDepartment : ''}
                  onSelect={receiverDepartment => props?.handleSelectReceiverDepartment(receiverDepartment)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView) ? <TextValidator
                className="w-100"
                label={t("AssetTransfer.receiverPerson")}
                name="receiverPerson"
                onChange={props?.handleChange}
                value={props?.item?.receiptPersonName ? props?.item?.receiptPersonName : ""}
                InputProps={{
                  readOnly: props?.item?.isView
                }}
              /> : <AsynchronousAutocompleteSub
                className="w-100 mb-20"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("AssetTransfer.receiverPerson")}
                  </span>
                }
                disabled={!props?.item?.receiverDepartment?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByReceiverDepartment}
                defaultValue={props?.item?.receiverPerson ?? ''}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                value={props?.item?.receiverPerson ?? ''}
                onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />}

            </Grid>
            <Grid item md={3} sm={12} xs={12}>
              {(!props?.item?.isView && !props?.item?.isStatusConfirmed) && (
                <Button
                  variant="contained"
                  color="primary"
                  size="small" className="mb-16 w-100"
                  onClick={props?.openPopupSelectAsset}
                >
                  {t('RecallSlip.select_asset')}
                </Button>
              )}
              {props?.item?.handoverDepartment && props?.item?.shouldOpenAssetPopup && (
                <SelectAssetAllPopup
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAsset}
                  handoverDepartment={props.item?.receiverDepartment}
                  departmentId={props.item.departmentId}
                  statusIndexOrders={props.item.statusIndexOrders}
                  handleClose={props.handleAssetPopupClose}
                  t={t} i18n={i18n}
                  receiverDepartmentId={props.item?.handoverDepartment?.id}
                  assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                  dateOfReceptionTop={props.item?.issueDate}
                  isFilterManagementDepartment={false}
                  isRecallSlip
                />
              )}
            </Grid>
          </Grid>
          <Grid item md={10} sm={12} xs={12}></Grid>
          <Grid spacing={2}>
            <MaterialTable
              data={props?.item?.assetVouchers || []}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12}>
            {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={
                  props.handleAddAssetDocumentItem
                }
              >
                {t('AssetFile.addAssetFile')}
              </Button>
            }
            {props?.item?.shouldOpenPopupAssetFile && (
              <VoucherFilePopup
                open={props?.item.shouldOpenPopupAssetFile}
                handleClose={props?.handleAssetFilePopupClose}
                itemAssetDocument={props?.itemAssetDocument}
                getAssetDocument={props?.getAssetDocument}
                handleUpdateAssetDocument={props?.handleUpdateAssetDocument}
                documentType={props?.item?.documentType}
                item={props?.item}
                t={t}
                i18n={i18n}
              />)
            }
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props?.item?.documents || []}
              columns={columnsVoucherFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '285px',
                minBodyHeight: '285px',
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div >
    </form>
  );
}