import {appConst} from "../../appConst";
import {convertFromToDate} from "../../appFunction";


export const convertExcelPayload = (state, assetClass) => {
  const {
    toDate,
    fromDate,
    currentUser,
    transferStatus,
    handoverDepartment,
    receiverDepartment,
    keyword,
  } = state;
  const searchObject = {};

  searchObject.keyword = keyword.trim() || null;
  if (handoverDepartment) {
    searchObject.handoverDepartmentId = handoverDepartment?.id;
  }
  if (receiverDepartment) {
    searchObject.receiverDepartmentId = receiverDepartment?.id;
  }
  if (transferStatus) {
    searchObject.status = transferStatus?.id;
  }
  if (fromDate) {
    searchObject.fromDate = convertFromToDate(fromDate).fromDate;
  }
  if (toDate) {
    searchObject.toDate = convertFromToDate(toDate).toDate;
  }

  return {
    orgId: currentUser?.org?.id,
    assetClass,
    ...searchObject
  }
}