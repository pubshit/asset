import React, { useEffect } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import NumberFormat from 'react-number-format';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { getListUserByDepartmentId, getListWarehouseByDepartmentId } from './RecallSlipService';
import viLocale from "date-fns/locale/vi";
import { useState } from "react";
import { createFilterOptions } from "@material-ui/lab";
import {
  a11yProps,
  checkInvalidDate,
  convertNumberPriceRoundUp,
  filterOptions,
  getTheHighestRole, getUserInformation,
  isCheckLenght
} from "app/appFunction";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { appConst, STATUS_STORE } from "app/appConst";
import {searchByPageDepartmentNew} from "../Department/DepartmentService";
import { ExpectedRevocationPopup } from "../Component/ExpectedRevocationPopup/ExpectedRevocationPopup";
import { Label, LightTooltip, TabPanel } from "../Component/Utilities";
import {getListOrgManagementDepartment} from "../Asset/AssetService";



function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AssetRevocationVoucherTable(props) {
  const {organization} = getUserInformation();
  const roles = getTheHighestRole();
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });
  const searchObjectTypeStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    departmentId: props?.item?.receiverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code, // lấy ra những kho đang hoạt động
  }
  const handoverDepartmentSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  };
  const filterAutocomplete = createFilterOptions();
  const dataListWarehouse = props?.item?.assetVouchers.map(item => item.storeName)

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByReceiverDepartment({
      departmentId: props?.item?.receiverDepartment?.id ?? '',
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
    });
  }, [props?.item?.handoverDepartment, props?.item?.receiverDepartment]);
  
  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      hidden: props?.item?.isView,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
      maxWidth: 400,
      cellStyle: {
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
      cellStyle: {
      },
      render: (rowData) => rowData?.asset?.serialNumber || rowData?.assetSerialNumber
    },
    {
      title: t("RecallSlip.usageState"),
      field: "assetAttributeValue",
      align: "left",
      minWidth: 180,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        <AsynchronousAutocompleteSub
          searchFunction={()=>{}}
          listData={appConst.typeOfWarehouseAssets || []}
          value={rowData.warehouseStatus ? rowData.warehouseStatus : null}
          displayLable={'name'}
          onSelect={warehouseStatus => props?.selectAssetWarehouseStatus(warehouseStatus, rowData)}
          filterOptions={filterOptions}
          validators={['required']}
          errorMessages={t('general.required')}
          readOnly={props.item.isView}
        />
    },
    {
      title: t("Asset.warehouse"),
      field: "storeName",
      align: "left",
      minWidth: 170,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        <AsynchronousAutocompleteSub
          searchFunction={getListWarehouseByDepartmentId}
          searchObject={searchObjectTypeStore}
          displayLable={"name"}
          value={rowData.store}
          onSelect={warehouse => props?.handleSelectWarehouse(warehouse, rowData)}
          filterOptions={filterOptions}
          validators={['required']}
          errorMessages={t('general.required')}
          readOnly={props.item.isView}
        />
    },
    {
      title: t("Asset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
      cellStyle: {
      },
      render: (rowData) => rowData?.asset?.model || rowData?.assetModel
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "asset.yearPutIntoUse",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "asset.manufacturer.name",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
      cellStyle: {
      },
      render: (rowData) => rowData?.asset?.model || rowData?.manufacturerName
    },
    {
      title: t("Asset.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.asset?.originalCost
          ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
          : "",
    },
    {
      title: t("Asset.carryingAmount"),
      field: "asset.carryingAmount",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.asset?.carryingAmount
          ? convertNumberPriceRoundUp(rowData?.asset?.carryingAmount)
          : "",
    },
    {
      title: t("Asset.managementDepartment"),
      field: "",
      align: "left",
      minWidth: 220,
      maxWidth: 220,
      cellStyle: {
      },
      render: (rowData) =>  
        rowData.asset?.managementDepartment?.name 
        || rowData?.managementDepartmentName
        || rowData?.asset?.managementDepartmentName 
        || "" 
    },
    {
      title: t("Asset.useDepartment"),
      field: "",
      align: "left",
      minWidth: 220,
      maxWidth: 220,
      cellStyle: {
      },
      render: (rowData) => rowData?.useDepartmentName || rowData?.asset?.useDepartmentName 
    },
    {
      title: t("Asset.note"),
      field: "note",
      align: "left",
      minWidth: 180,
      cellStyle: {
      },
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note)}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props?.item?.isView || props?.item?.isStatus
          }}
          validators={["maxStringLength:255"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
    }
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      headerStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      headerStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      headerStyle: {
        textAlign: "center",
      },
      render: rowData =>
        // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
    }
  }

  const renderText = (data) => {
    const uniqueStrings = data.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });

    const joinedText = uniqueStrings.join(", ");
    return joinedText
  };

  return (
    <form>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} type="submit" />
            {/*<Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />*/}
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={1}>
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("RecallSlip.issueDate")}
                    </span>
                  }
                  name={'issueDate'}
                  inputVariant="standard"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.item.issueDate}
                  onChange={date => props?.handleDateChange(date, "issueDate")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  InputProps={{
                    readOnly: props?.item?.isView || props?.item?.isStatus
                  }}
                  open={isPickerOpen}
                  onOpen={() => {
                    if (props?.item?.isView || props?.item?.isStatus) {
                      setIsPickerOpen(false);
                    }
                    else {
                      setIsPickerOpen(true);
                    }
                  }}
                  onClose={() => setIsPickerOpen(false)}
                  validators={['required']}
                  errorMessages={t('general.required')}
                  minDate={new Date("01/01/1900")}
                  minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
                  maxDate={new Date()}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                  onBlur={() => handleBlurDate(props?.item.issueDate, "issueDate")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                name="voucherName"
                onChange={props?.handleChange}
                value={props?.item?.voucherName ? props?.item?.voucherName : ""}
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("RecallSlip.recall")}
                  </span>
                }
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {props?.item?.isView || props?.item?.isStatus ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  value={props?.item?.transferStatus?.name || ""}
                /> : <AsynchronousAutocompleteTransfer
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  listData={appConst.revokedstatus || []}
                  setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                  value={props?.item?.transferStatus ? props?.item?.transferStatus : null}
                  displayLable={'name'}
                  onSelect={props.selectAssetTransferStatusStatus}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  typeReturnFunction="status"
                />}
            </Grid>
            <Grid item md={2} sm={6} xs={6}>
              <TextValidator
                className="w-100"
                disabled
                InputProps={{
                  readOnly: true,
                }}
                label={t("RecallSlip.number_reacall")}
                value={props?.item?.voucherCode || ""}
              />
            </Grid>
            <Grid item md={2} sm={6} xs={6}>
              <TextValidator
                className="w-100"
                name="decisionCode"
                onChange={props?.handleChange}
                value={props?.item?.decisionCode || ""}
                label={
                  <span>
                    {t("RecallSlip.decision_number")}
                  </span>
                }
              />
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView) ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: props?.item?.receiptDepartmentName,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.receiverDepartment")}
                    </span>
                  }
                  value={props?.item?.receiverDepartmentName ?? ""}

                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("allocation_asset.receiverDepartment")}
                    </span>
                  }
                  searchFunction={
                    roles?.isRoleAssetManager
                      ? getListOrgManagementDepartment
                      : searchByPageDepartmentNew
                  }
                  searchObject={roles?.isRoleAssetManager ? {} : handoverDepartmentSearchObject}
                  listData={props.item.listReceiverDepartment || []}
                  setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
                  typeReturnFunction={
                    props.item?.isRoleAssetManager
                      ? "list"
                      : "category"
                  }
                  displayLable={"text"}
                  isNoRenderChildren
                  isNoRenderParent
                  value={props?.item?.receiverDepartment || null}
                  onSelect={receiverDepartment => props?.handleSelectReceiverDepartment(receiverDepartment)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  disabled={!roles?.isRoleOrgAdmin}
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                className="w-100"
                label={
                  <Label isRequired>{t("AssetTransfer.receiverPerson")}</Label>
                }
                disabled={!props?.item?.receiverDepartment?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByReceiverDepartment}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                readOnly={props?.item?.isView}
                value={props?.item?.receiverPerson ?? ''}
                onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />
            </Grid>
            <Grid item md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                label={t("RecallSlip.warehouse")}
                defaultValue={props?.item?.warehouse ? props.item?.warehouse : ""}
                value={renderText(dataListWarehouse)}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item md={2} sm={12} xs={12}>
              {(!props?.isFormMaintainQuest && (!props?.item?.isView && !props?.item?.isStatusConfirmed)) && (
                <Button
                  variant="contained"
                  color="primary"
                  size="small" className="mt-15 w-100"
                  onClick={props?.openPopupSelectAsset}
                >
                  {t('RecallSlip.select_asset')}
                </Button>
              )}
              {props?.item?.departmentUser && props?.item?.shouldOpenAssetPopup && (
                <SelectAssetAllPopup
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAsset}
                  handoverDepartment={props.item.receiverDepartment}
                  statusIndexOrders={props.item.statusIndexOrders}
                  handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                  receiverDepartmentId={props.item.handoverDepartment?.id || ''}
                  assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                  isRevocationStream
                  isRecallSlip
                  isFilterUseDepartment
                />
              )}
            </Grid>
            <Grid item md={2} sm={12} xs={12}>
              {(!props?.isFormMaintainQuest && (!props?.item?.isView && !props?.item?.isStatusConfirmed)) && (
                <Button
                  variant="contained"
                  color="primary"
                  size="small" className="mt-15 w-100"
                  onClick={props.openPopupExpectedRevocation}
                >
                  {t('Asset.recall_slip')}
                </Button>
              )}
              {props?.item?.shouldOpenExpectedRevocationPopup && (
                <ExpectedRevocationPopup
                  open={props.item.shouldOpenExpectedRevocationPopup}
                  handleClose={props.handleExpectedRevocationPopupClose}
                  handleOkClose={props.handleExpectedRevocationPopupOkClose}
                  assetVouchers={props.item.assetVouchers}
                  status={appConst.STATUS_EXPECTED_RECOVERY.DA_THONG_KE}
                  receiptDepartmentId={props?.item?.receiverDepartment?.id}
                  item={props.item}
                  dxIds={props.item?.dxIds}
                  t={t}
                  isDataPage={true}
                />
              )}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {!props?.item?.isView && (
                <AsynchronousAutocompleteTransfer
                  className="w-100"
                  label={
                    <span>
                      {t("RecallSlip.choose_warehouse")}
                    </span>
                  }
                  searchFunction={getListWarehouseByDepartmentId}
                  searchObject={searchObjectTypeStore}
                  defaultValue={props.item?.warehouse ? props.item?.warehouse : ''}
                  displayLable={"name"}
                  value={props?.item?.warehouse || null}
                  onSelect={warehouse => props?.handleSelectWarehouse(warehouse)}
                  filterOptions={filterOptions}
                />
              )}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {!props?.item?.isView && (
                <AsynchronousAutocompleteSub
                  className="w-100 mb-16"
                  label={t("RecallSlip.choose_status")}
                  listData={appConst.typeOfWarehouseAssets || []}
                  value={props?.item?.warehouseStatus ? props?.item?.warehouseStatus : null}
                  displayLable={'name'}
                  onSelect={props.selectAssetWarehouseStatus}
                  filterOptions={filterOptions}
                  searchFunction={() => { }}
                />
              )}
            </Grid>
          </Grid>
          <Grid item md={10} sm={12} xs={12}></Grid>
          <Grid spacing={2}>
            <MaterialTable
              data={props?.item?.assetVouchers ? props?.item?.assetVouchers : []}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12}>
            {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={
                  props.handleAddAssetDocumentItem
                }
              >
                {t('AssetFile.addAssetFile')}
              </Button>
            }
            {props?.item?.shouldOpenPopupAssetFile && (
              <VoucherFilePopup
                open={props?.item.shouldOpenPopupAssetFile}
                handleClose={props?.handleAssetFilePopupClose}
                itemAssetDocument={props?.itemAssetDocument}
                getAssetDocument={props?.getAssetDocument}
                handleUpdateAssetDocument={props?.handleUpdateAssetDocument}
                documentType={props?.item?.documentType}
                item={props?.item?.item}
                t={t}
                i18n={i18n}
              />)
            }
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props?.item?.documents || []}
              columns={columnsVoucherFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '285px',
                minBodyHeight: '285px',
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div >
    </form>
  );
}