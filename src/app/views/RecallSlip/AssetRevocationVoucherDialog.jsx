import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkInvalidDate,
  checkMinDate,
  formatDateTimeDto,
  getTheHighestRole,
  handleThrowResponseMessage,
  isCheckLenght,
  isSuccessfulResponse
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import RefuseTransferDialog from "../Component/InstrumentToolsTransfer/RefuseTransferDialog"
import localStorageService from "app/services/localStorageService";
import moment from "moment";
import AssetRevocationVoucherTable from "./AssetRevocationVoucherTable";
import { getNewCodeDocument } from "../Asset/AssetService";
import { createRevocationVoucher, updateRevotionVoucher } from "./RecallSlipService";
import { STATUS_RECALLSHIP } from "../../appConst";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class AssetRevocationVoucherDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Transfer,
    handoverPerson: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    warehouse: null,
    issueDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenExpectedRevocationPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_RECALLSHIP, // thu hồi
    handoverPersonClone: false,
    assetDocumentList: [],
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER,
    shouldOpenPopupAssetFile: false,
    transferStatus: null,
    warehouseStatus: null,
    loading: false,
    shouldOpenRefuseTransfer: false,
    isConfirm: false,
    note: null,
    departmentUser: null,
    dxIds: [],
  };
  isRoleAssetUser = getTheHighestRole().isRoleAssetUser;
  isOrgAdmin = getTheHighestRole().isRoleOrgAdmin
  voucherType = ConstantList.VOUCHER_TYPE.Transfer;

  convertDto = (state) => {
    let data = {
      assetVouchers: state?.assetVouchers?.map(item => {
        return {
          ...item,
          warehouseStatus: null
          }
      }),
      decisionCode: state?.decisionCode,
      issueDate: moment(state?.issueDate).format("YYYY-MM-DDTHH:mm:ss"),
      listAssetDocumentId: state?.listAssetDocumentId,
      receiverDepartmentId: state?.receiverDepartment?.id,
      receiverDepartmentName: state?.receiverDepartment?.text,
      receiverPersonName: state?.receiverPerson?.personDisplayName,
      receiverPersonId: state?.receiverPerson?.personId,
      transferStatusIndex: state?.transferStatusIndex,
      status: state?.transferStatus?.id,
      dxIds: this.state.dxIds,
      voucherName: state?.voucherName,
    }
    return data;
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable.listInputName.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowData.tableData?.id) {
        assetVoucher.note = valueText.target.value;
      }
    });
    this.setState({ assetVouchers: assetVouchers, handoverPersonClone: true });
  };

  selectUsePerson = (item, rowdata) => {
    let { assetVouchers } = this.state;
    assetVouchers.map(async (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.usePerson = item ? item : null;
        assetVoucher.usePersonId = item?.id;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true }
    );
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  validateForm = () => {
    let {
      assetVouchers,
      transferStatus,
      receiverDepartment,
      issueDate,
      receiverPerson
    } = this.state;
    if (checkInvalidDate(issueDate)) {
      toast.warning('Ngày chứng từ không tồn tại (Thông tin phiếu).');
      return false;
    }
    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(`Ngày chứng từ không được nhỏ hơn ngày "01/01/1900"(Thông tin phiếu).`);
      return false;
    }
    if (!transferStatus) {
      toast.warning("Vui lòng chọn trạng thái thu hồi(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!receiverDepartment) {
      toast.warning("Vui lòng chọn phòng thu hồi(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn tài sản");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!receiverPerson?.personId) {
      toast.warning("Vui lòng chọn người thu hồi(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (this.validateAssetVouchers()) return;
    return true;
  }

  validateStatusAsset = () => {
    let { assetVouchers } = this.state
    let dataAsset = this.convertDto(this.state)

    if (dataAsset?.status === appConst.STATUS_REVOKED.PROCESSING.id) {
      const isStoreName = assetVouchers.some((asset) => !asset?.storeName);
      const isWarehouseStatus = assetVouchers.some((asset) => !asset?.warehouseStatus);

      if (isStoreName) {
        toast.warning('Vui lòng nhập kho cho tài sản');
        return false;
      }
      if (isWarehouseStatus) {
        toast.warning('Vui lòng nhập trạng thái sử dụng cho tài sản');
        return false;
      }
    }
    return true

  }

  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning("Ghi chú không nhập quá 255 ký tự.");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let {
      t,
      handleOKEditClose = () => { },
    } = this.props;

    if (e.target.id !== "assetTransferDialog") return;
    if (!this.validateForm()) return;
    if (!this.validateStatusAsset()) return;
    let dataState = this.convertDto(this.state);

    setPageLoading(true);
    if (id) {
      updateRevotionVoucher(dataState, id)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            handleOKEditClose()
          } else {
            toast.warning(data.data[0].errorMessage)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
          setPageLoading(false)
        })
    } else {
      createRevocationVoucher(dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.addSuccess"));
            handleOKEditClose()
          } else {
            toast.warning(data.data[0].errorMessage ?? data.data[0].message)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
        })
    }
  };

  handleDateChange = (date, name) => {
    if (name === variable.listInputName.issueDate && date) {
      this.setState({
        [name]: date,
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  handleSelectAsset = (items) => {
    let assetValid = {
      asset: {},
      isAssetValid: true
    }

    items?.map((element) => {
      let issueDateTimestamp = moment(this.state.issueDate)
      let dateOfReception = moment(element?.asset?.dateOfReception)
      if (issueDateTimestamp.isBefore(dateOfReception)) {
        assetValid.isAssetValid = false
        assetValid.asset = element
      }
      element.asset.useDepartment = this.state?.receiverDepartment;
      element.assetId = element.asset?.assetId
        ? element.asset?.assetId
        : element.asset?.id;
      element.useDepartmentId = element.asset?.useDepartmentId;
    });
    if (!assetValid.isAssetValid) {
      toast.warning(`Ngày tiếp nhận của tài sản ${assetValid?.asset?.asset?.name} lớn hơn ngày chứng từ`)
      return
    }
    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning('Chưa có tài sản nào được chọn');
    }
  };

  removeAssetInlist = (rowData) => {
    let { assetVouchers = [], dxIds = [] } = this.state;
    let index = assetVouchers?.findIndex((x) => x.assetId === rowData.assetId);
    let listAssetByDxId = assetVouchers.filter(item => item.dxId === rowData.dxId);
    let indexDxId = dxIds.findIndex(id => id === rowData.dxId);

    assetVouchers?.splice(index, 1);
    if (listAssetByDxId?.length === 1) {
      dxIds.splice(indexDxId, 1);
    }
    if (assetVouchers.length <= 0) {
      this.setState({
        warehouse: null,
        warehouseStatus: null
      });
    }
    this.setState({
      assetVouchers,
      dxIds,
    });
  };

  componentWillMount() {
    let { item } = this.props;
    const departmentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER);
    const isEmptyDxIdsArr = item?.assetVouchers?.some(ts => ts.dxId);

    if (item.id) {
      this.setState({
        ...this.props.item,
        handoverPersonClone: true,
        dxIds: isEmptyDxIdsArr
          ? item?.assetVouchers?.map((ts) => ts.dxId).filter(Boolean)
          : [],
        departmentUser,
        handoverDepartment: {
          id: departmentUser?.id,
          text: departmentUser?.name,
        },
        assetVouchers: item?.assetVouchers?.map(ts => ({
          ...ts,
          assete: {
            ...ts.asset,
            store: {
              id: ts.storeId,
              name: ts.storeName,
            },
          },
          store: {
            id: ts.storeId,
            name: ts.storeName,
          },
          warehouseStatus: appConst.typeOfWarehouseAssets.find(x => x.id === ts.asset.assetUsageState)
        })),
        handoverPerson: {
          personId: item?.handoverPersonId,
          personDisplayName: item?.handoverPersonName,
        },
        receiverDepartment: {
          id: item?.receiverDepartmentId,
          text: item?.receiverDepartmentName,
        },
        receiverPerson: {
          personId: item?.receiverPersonId,
          personDisplayName: item?.receiverPersonName,
        },
        transferStatus: appConst.revokedstatus.find(x => x.id === item.status),
      });
    } else {
      this.setState({
        ...this.props?.item,
        departmentUser,
        // transferStatusIndex: statusCreate?.transferStatusIndex,
        receiverDepartment: item?.isTabValue === appConst.tabRecallSlip.tabRecall
          ? {
            id: departmentUser?.id,
            text: departmentUser?.name,
            name: departmentUser?.name,
          } : {
            id: item?.receiptDepartmentId,
            text: item?.receiptDepartmentName,
            name: item?.receiptDepartmentName,
          },
        transferStatus: appConst.STATUS_REVOKED.PROCESSING,
      });
    }
    getTheHighestRole().isRoleAssetUser && this.setState({
      handoverDepartment: {
        ...departmentUser,
        text: departmentUser?.name
      },
      handoverDepartmentId: departmentUser?.id,
    });
  }
  componentDidMount() {

  }

  openPopupSelectAsset = () => {
    let { voucherType, receiverDepartment } = this.state;
    if (receiverDepartment?.id) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
          departmentId: receiverDepartment?.id,
        },
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );

    } else {
      toast.info("Vui lòng chọn phòng thu hồi.");
    }
  };


  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  openPopupExpectedRevocation = () => {
    let { receiverDepartment } = this.state
    if (receiverDepartment?.id) {
      this.setState({
        shouldOpenExpectedRevocationPopup: true
      })
    } else {
      toast.info("Vui lòng chọn phòng thu hồi.");
    }
  }

  handleExpectedRevocationPopupClose = () => {
    this.setState({
      shouldOpenExpectedRevocationPopup: false,
    })
  }

  handleExpectedRevocationPopupOkClose = (assets = [], dxIds = []) => {
    this.setState({
      shouldOpenExpectedRevocationPopup: false,
      assetVouchers: assets?.map(item => ({
        ...item,
        quantity: item.quantity || item.asset?.dxRequestQuantity,
      })),
      dxIds,
    })
  }

  handleReceiverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  selectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: item,
      receiverDepartmentId: item?.id,
    })
  };

  handleSelectReceiverDepartment = (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );

    let receiverDepartment = {
      id: item?.id,
      name: item?.name,
      text: item?.name,
    };

    this.setState(
      {
        receiverDepartment: item?.id ? receiverDepartment : null,
        receiverDepartmentId: item?.id,
        assetVouchers: [],
        listReceiverPerson: [],
        receiverPerson: null,
        receiverPersonId: null,
        isCheckReceiverDP: !item,
      }
    );
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let handoverDepartment = {
      id: item?.id,
      name: item?.text || item?.name,
      text: item?.text || item?.name,
    };

    this.setState(
      {
        handoverDepartment: item ? handoverDepartment : null,
        handoverDepartmentId: item?.id,
        assetVouchers: [],
        listHandoverPerson: [],
        handoverPerson: null,
        handoverPersonId: null,
      },
    );
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  handleSelectWarehouse = (item, rowData) => {
    let { assetVouchers } = this.state;
    if (rowData?.assetId) {
      assetVouchers.map((assetVoucher) => {
        if (assetVoucher.assetId === rowData?.assetId) {
          assetVoucher.store = item;
          assetVoucher.storeId = item?.id;
          assetVoucher.storeName = item?.name;
          assetVoucher.isNotAccept = false;
        }
      });
    } else {
      assetVouchers.map((assetVoucher) => {
        assetVoucher.store = item;
        assetVoucher.storeId = item?.id;
        assetVoucher.storeName = item?.name;
        assetVoucher.isNotAccept = false;
      });
    }

    this.setState({
      warehouse: item ? item : null,
      assetVouchers,
    });
  }

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let res = await getNewCodeDocument(this.state?.documentType);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          item: {
            code: data,
          },
          shouldOpenPopupAssetFile: true,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleStatus = (dataStatus) => {
    if (typeof dataStatus !== "number") {
      if (dataStatus?.target?.id !== "assetTransferDialog") return;
    }

    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { t } = this.props;
    let status = this.state.isStatus ? this.state?.transferStatus?.indexOrder : dataStatus;
    let issueDate = formatDateTimeDto(this.state.issueDate, 23, 59, 59);
    let dataState = this.convertDto(this.state)
    let dataTranfer = {
      dataState: dataState,
      id: this.state?.id,
      note: this.state?.note,
      status: status,
      issueDate: issueDate,
    }
    updateStatus(dataTranfer)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("InstrumentToolsTransfer.notification.confirmSucces"));
          this.props.handleOKEditClose();
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("InstrumentToolsTransfer.notification.faliConfirm"));
        setPageLoading(false);
      });
  }

  handleRefuse = () => {
    this.setState({
      shouldOpenRefuseTransfer: true
    });
  }

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    },
      () => this.props.handleOKEditClose()
    );
  }

  selectAssetTransferStatusStatus = (item) => {
    this.setState({
      transferStatus: item,
      transferStatusIndex: item?.indexOrder
    });
  };

  selectAssetWarehouseStatus = (item, rowData) => {
    let { assetVouchers } = this.state;

    if (rowData?.assetId) {
      assetVouchers.map((assetVoucher) => {
        if (assetVoucher.assetId === rowData?.assetId) {
          assetVoucher.warehouseStatus = item;
          assetVoucher.assetUsageState = item?.id;
        }
      });
    } else {
      assetVouchers.map((assetVoucher) => {
        assetVoucher.warehouseStatus = item;
        assetVoucher.assetUsageState = item?.id;
      });
    }
    this.setState({
      warehouseStatus: item,
      transferStatusIndex: item?.indexOrder,
      assetVouchers
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  clearStateDate = () => {
    this.setState({
      handoverDepartment: null,
      handoverDepartmentId: "",
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      transferStatus: appConst.STATUS_TRANSFER.CHO_XAC_NHAN,
      transferStatusIndex: appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder,
    })
  }

  render() {
    let { open, t, i18n } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let receiverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let handoverPersonSearchObject = {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.handoverDepartment
        ? this.state.handoverDepartment?.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      pageIndex: 1,
      pageSize: 100,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let {
      isView,
      shouldOpenNotificationPopup,
      loading,
      isConfirm,
      isStatus,
    } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={isStatus ? this.handleStatus : this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("Thêm mới/Cập nhập phiếu thu hồi")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <AssetRevocationVoucherTable
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAssetTransferStatusStatus={
                this.selectAssetTransferStatusStatus
              }
              selectAssetWarehouseStatus={
                this.selectAssetWarehouseStatus
              }
              handleHandoverDepartmentPopupOpen={
                this.handleHandoverDepartmentPopupOpen
              }
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleSelectHandoverPerson={
                this.handleSelectHandoverPerson
              }
              handleSelectWarehouse={
                this.handleSelectWarehouse
              }
              handleHandoverDepartmentPopupClose={
                this.handleHandoverDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              handleButtonAdd={this.props.handleButtonAdd}
              openPopupSelectAsset={this.openPopupSelectAsset}
              openPopupExpectedRevocation={this.openPopupExpectedRevocation}
              handleExpectedRevocationPopupClose={this.handleExpectedRevocationPopupClose}
              handleExpectedRevocationPopupOkClose={this.handleExpectedRevocationPopupOkClose}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupOpen={
                this.handleReceiverDepartmentPopupOpen
              }
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleSelectReceiverPerson={
                this.handleSelectReceiverPerson
              }
              handleReceiverDepartmentPopupClose={
                this.handleReceiverDepartmentPopupClose
              }
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              selectUsePerson={this.selectUsePerson}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              getAssetDocument={this.getAssetDocument}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              selectReceiverDepartment={this.selectReceiverDepartment}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              isRoleAssetUser={getTheHighestRole().isRoleAssetUser}
              isRoleAssetManager={getTheHighestRole().isRoleAssetManager}
              isFormMaintainQuest={this?.props?.isFormMaintainQuest}
              isRoleOrgAdmin={getTheHighestRole().isRoleOrgAdmin}
            />

            {this.state?.shouldOpenRefuseTransfer &&
              <RefuseTransferDialog
                t={t}
                open={this.state?.shouldOpenRefuseTransfer}
                item={this.state}
                handleChange={this.handleChange}
                handleClose={this.handleCloseRefuseTransferDialog}
                handleConfirmReason={() => this.handleStatus(appConst.listStatusTransfer[4].transferStatusIndex)}
              />
            }
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              {isConfirm ? <>
                <Button
                  className="mb-16 mr-16"
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(appConst.listStatusTransfer[1].transferStatusIndex)} >
                  {t("InstrumentToolsTransfer.confirm")}
                </Button>
                <Button
                  className="btnRefuse mb-16 mr-16"
                  variant="contained"
                  onClick={this.handleRefuse}
                >
                  {t("InstrumentToolsTransfer.refuse")}
                </Button>
                <Button
                  className="btnClose mb-16 mr-16"
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    this.props.handleClose();
                  }}
                >
                  {t("InstrumentToolsTransfer.close")}
                </Button>
              </> : <>
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={() => this.props.handleClose()}
                >
                  {t("general.cancel")}
                </Button>
                {!isView && <Button variant="contained" color="primary" type="submit">
                  {t("general.save")}
                </Button>}
                {this.state?.transferStatusIndex === appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                  && this.state.id && (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        this.props.handleQrCode(this.state, true);
                      }}
                    >
                      {t("Asset.PrintQRCode")}
                    </Button>
                  )}
              </>}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetRevocationVoucherDialog.contextType = AppContext;
export default AssetRevocationVoucherDialog;
