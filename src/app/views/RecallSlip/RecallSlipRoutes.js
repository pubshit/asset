import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const RecallSlip = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./RecallSlip")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(RecallSlip);

const RecallSlipRoutes = [
  {
    path: ConstantList.ROOT_PATH + "asset/recall-slip",
    exact: true,
    component: ViewComponent
  }
];

export default RecallSlipRoutes;