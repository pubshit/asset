import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import AssetExpectedRevocationTable from "./AssetExpectedRevocationTable";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkInvalidDate,
  checkMinDate,
  formatDateTimeDto,
  getTheHighestRole,
  getUserInformation, handleThrowResponseMessage,
  isCheckLenght,
  isSuccessfulResponse
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import RefuseTransferDialog from "../Component/InstrumentToolsTransfer/RefuseTransferDialog";
import moment from "moment";
import { getAssetDocumentById, getNewCodeDocument } from "../Asset/AssetService";
import { createRecallSlip, updateRecallSlip } from "./RecallSlipService";
import { PaperComponent } from "../Component/Utilities";
import { STATUS_RECALLSHIP } from "../../appConst";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetExpectedRevocationDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Transfer,
    handoverPerson: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    suggestDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_RECALLSHIP, // thu hồi
    handoverPersonClone: false,
    assetDocumentList: [],
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER,
    shouldOpenPopupAssetFile: false,
    transferStatus: null,
    loading: false,
    shouldOpenRefuseTransfer: false,
    isConfirm: false,
    note: null,
    departmentUser: null
  };
  isRoleAssetUser = getTheHighestRole().isRoleAssetUser;
  voucherType = ConstantList.VOUCHER_TYPE.Transfer; //Điều chuyển

  convertDto = (state) => {
    let data = {
      assetVouchers: state?.assetVouchers?.map(x => {
        return {
          ...x,
          assetCode: x?.asset?.code || x?.assetCode,
        }
      }),
      departmentId: state?.handoverDepartment?.id,
      departmentName: state?.handoverDepartment?.text,
      personId: state?.handoverPerson?.personId,
      personName: state?.handoverPerson?.personDisplayName,
      suggestDate: moment(state?.suggestDate).format("YYYY-MM-DDTHH:mm:ss"),
      receiptDepartmentId: state?.receiverDepartment?.id,
      receiptDepartmentName: state?.receiverDepartment?.text,
      receiptPersonId: state?.receiverPerson?.personId,
      receiptPersonName: state?.receiverPerson?.personDisplayName,
      receiptDepartmentCode: state?.receiverPerson?.departmentCode,
      status: state?.transferStatus?.id,
    }
    return data;
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable.listInputName.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, value, source) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowData.tableData?.id) {
        assetVoucher[source] = value;
      }
    });
    this.setState({ assetVouchers: assetVouchers, handoverPersonClone: true });
  };

  selectUsePerson = (item, rowdata) => {
    let { assetVouchers } = this.state;
    assetVouchers.map(async (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.usePerson = item ? item : null;
        assetVoucher.usePersonId = item?.id;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true }
    );
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  validateForm = () => {
    let { isRoleAssetUser } = getTheHighestRole();
    let currentDate = new Date();
    let {
      assetVouchers,
      handoverDepartment,
      handoverPerson,
      transferStatus,
      receiverDepartment,
      suggestDate,
      receiverPerson
    } = this.state;
    if (checkInvalidDate(suggestDate)) {
      toast.warning('Ngày chứng từ không tồn tại (Thông tin phiếu).');
      return false;
    }
    if (checkMinDate(suggestDate, "01/01/1900")) {
      toast.warning(`Ngày chứng từ không được nhỏ hơn ngày "01/01/1900"(Thông tin phiếu).`);
      return false;
    }
    if (new Date(suggestDate) > currentDate) {
      toast.warning("Ngày chứng từ không được lớn hơn ngày hiện tại(Thông tin phiếu).");
      return false;
    }
    if (!handoverDepartment) {
      toast.warning("Vui lòng chọn phòng ban bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!transferStatus) {
      toast.warning("Vui lòng chọn trạng thái điều chuyển(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!handoverPerson) {
      toast.warning("Vui lòng nhập người bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!receiverDepartment) {
      toast.warning("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (isRoleAssetUser && (receiverDepartment?.id === handoverDepartment?.id)) {
      toast.warning("Phòng ban bàn giao không được trùng với Phòng ban tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn tài sản");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!receiverPerson?.personId) {
      toast.warning("Vui lòng chọn người tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (this.validateAssetVouchers()) return;
    return true;
  }
  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning("Ghi chú không nhập quá 255 ký tự.");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;

    let {
      t,
      handleOKEditClose = () => { },
    } = this.props;
    if (e.target.id !== "assetTransferDialog") return;
    if (!this.validateForm()) return;

    let dataState = this.convertDto(this.state);
    setPageLoading(true);
    if (id) {
      updateRecallSlip(dataState, id)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            handleOKEditClose()
          } else {
            toast.warning(data.data[0].errorMessage)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
          setPageLoading(false)
        })
    } else {
      createRecallSlip(dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.addSuccess"));
            handleOKEditClose()
          } else {
            toast.warning(data.data[0].errorMessage)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(t("general.error"))
        })
    }
  };

  handleDateChange = (date, name) => {
    if (name === "suggestDate" && date) {
      this.setState({
        [name]: date,
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  handleSelectAsset = (items) => {
    let assetValid = {
      asset: {},
      isAssetValid: true
    }

    items?.forEach((element) => {
      let suggestDateTimestamp = moment(this.state.suggestDate)
      let dateOfReception = moment(element?.asset?.dateOfReception)
      if (suggestDateTimestamp.isBefore(dateOfReception)) {
        assetValid.isAssetValid = false
        assetValid.asset = element
      }
      element.asset.useDepartment = this.state?.receiverDepartment;
      element.assetId = element?.assetId || element?.asset?.id;
    });
    if (!assetValid.isAssetValid) {
      toast.warning(`Ngày tiếp nhận của tài sản ${assetValid?.asset?.asset?.name} lớn hơn ngày chứng từ`)
      return
    }
    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning('Chưa có tài sản nào được chọn');
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  componentDidMount() {
    let { item, isFormMaintainRqquest = false } = this.props;
    const userInfo = getUserInformation();
    const roles = getTheHighestRole();
    const { departmentUser } = userInfo;

    if (item?.id) {
      this.setState({
        ...item,
        ...roles,
        ...userInfo,
        handoverPersonClone: true,
        departmentUser,
        handoverDepartment: {
          id: item?.departmentId,
          text: item?.departmentName,
        },
        handoverPerson: {
          personId: item?.personId,
          personDisplayName: item?.personName,
        },
        receiverDepartment: {
          id: item?.receiptDepartmentId,
          text: item?.receiptDepartmentName,
          name: item?.receiptDepartmentName,
        },
        receiverPerson: {
          personId: item?.receiptPersonId,
          personDisplayName: item?.receiptPersonName,
        },
        assetVouchers: item?.assetVouchers.map(assetVoucher => ({
          ...assetVoucher,
          asset: {
            ...assetVoucher.asset,
            id: assetVoucher?.assetId || assetVoucher?.asset?.id,
          }
        })),
      });
    } else {
      this.setState({
        ...item,
        ...roles,
        ...userInfo,
        transferStatusIndex: appConst.STATUS_EXPECTED_RECOVERY.DANG_THONG_KE.id,
        transferStatus: appConst.STATUS_EXPECTED_RECOVERY.DANG_THONG_KE,
      });
    }
    if (isFormMaintainRqquest) {
      this.setState({
        transferStatusIndex: appConst.STATUS_EXPECTED_RECOVERY.DA_THONG_KE.id,
        transferStatus: appConst.STATUS_EXPECTED_RECOVERY.DA_THONG_KE,
      })
    }
    getTheHighestRole().isRoleAssetUser && this.setState({
      handoverDepartment: {
        ...departmentUser,
        text: departmentUser?.name
      },
      handoverDepartmentId: departmentUser?.id,
    });
  }


  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment, receiverDepartment } = this.state;
    if (handoverDepartment && receiverDepartment) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
          departmentId: handoverDepartment?.id,
        },
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.info("Vui lòng chọn phòng ban bàn giao và phòng tiếp nhận.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleReceiverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  selectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: item,
      receiverDepartmentId: item?.id,
    })
  };

  handleSelectReceiverDepartment = (item) => {
    let { assetVouchers, departmentUser } = this.state;
    let { isRoleAssetUser } = getTheHighestRole();
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );

    if (isRoleAssetUser && departmentUser?.id !== item?.id) {
      this.handleSelectHandoverDepartment(departmentUser);
    }
    let receiverDepartment = {
      id: item?.id,
      name: item?.name,
      text: item?.text,
    };

    this.setState(
      {
        receiverDepartment: item ? receiverDepartment : null,
        receiverDepartmentId: item?.id,
        listReceiverPerson: [],
        assetVouchers: [],
        receiverPerson: null,
        receiverPersonId: null,
        isCheckReceiverDP: item ? false : true,
      }
    );
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let { departmentUser, receiverDepartment } = this.state;
    let handoverDepartment = {};

    if (item?.id === departmentUser?.id) {
      handoverDepartment = {
        id: item?.id,
        name: item?.text || item?.name,
        text: item?.text || item?.name,
      };
    } else {
      handoverDepartment = {
        id: item?.id,
        name: item?.text || item?.name,
        text: item?.text || item?.name,
      };
      if (!receiverDepartment?.id && !getTheHighestRole().isRoleOrgAdmin) {
        this.handleSelectReceiverDepartment({ ...departmentUser, text: departmentUser?.name });
      }
    }
    this.setState(
      {
        handoverDepartment: item ? handoverDepartment : null,
        handoverDepartmentId: item?.id,
        assetVouchers: [],
        listHandoverPerson: [],
        handoverPerson: null,
        handoverPersonId: null,
      },
    );
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let res = await getNewCodeDocument(this.state?.documentType);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          item: {
            code: data,
          },
          shouldOpenPopupAssetFile: true,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleStatus = (dataStatus) => {
    if (typeof dataStatus !== "number") {
      if (dataStatus?.target?.id !== "assetTransferDialog") return;
    }

    let { setPageLoading } = this.context;
    // setPageLoading(true);
    let { t } = this.props;
    let status = this.state.isStatus ? this.state?.transferStatus?.indexOrder : dataStatus;
    let suggestDate = formatDateTimeDto(this.state.suggestDate, 23, 59, 59);
    let dataState = this.convertDto(this.state)
    let dataTranfer = {
      dataState: dataState,
      id: this.state?.id,
      note: this.state?.note,
      status: status,
      suggestDate: suggestDate,
    }
    updateStatus(dataTranfer)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("InstrumentToolsTransfer.notification.confirmSucces"));
          this.props.handleOKEditClose();
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("InstrumentToolsTransfer.notification.faliConfirm"));
        setPageLoading(false);
      });
  }

  handleRefuse = () => {
    this.setState({
      shouldOpenRefuseTransfer: true
    });
  }

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    },
      () => this.props.handleOKEditClose()
    );
  }

  selectAssetTransferStatusStatus = (item) => {
    this.setState({
      transferStatus: item,
      transferStatusIndex: item?.indexOrder
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  clearStateDate = () => {
    this.setState({
      handoverDepartment: null,
      handoverDepartmentId: "",
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      transferStatus: appConst.STATUS_TRANSFER.CHO_XAC_NHAN,
      transferStatusIndex: appConst.STATUS_TRANSFER.CHO_XAC_NHAN.indexOrder,
    })
  }

  render() {
    let { open, t, i18n, item } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let receiverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let handoverPersonSearchObject = {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.handoverDepartment
        ? this.state.handoverDepartment?.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      pageIndex: 1,
      pageSize: 100,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.receiverDepartment
        ? this.state.receiverDepartment?.id
        : null,
    };
    let {
      isView,
      shouldOpenNotificationPopup,
      loading,
      isConfirm,
      departmentUser,
      isRoleOrgAdmin,
    } = this.state;
    const isOwner = item?.createByDepartmentId === departmentUser?.id || isRoleOrgAdmin;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("RecallSlip.dialog")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <AssetExpectedRevocationTable
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAssetTransferStatusStatus={
                this.selectAssetTransferStatusStatus
              }
              handleHandoverDepartmentPopupOpen={
                this.handleHandoverDepartmentPopupOpen
              }
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleSelectHandoverPerson={
                this.handleSelectHandoverPerson
              }
              handleHandoverDepartmentPopupClose={
                this.handleHandoverDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupOpen={
                this.handleReceiverDepartmentPopupOpen
              }
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleSelectReceiverPerson={
                this.handleSelectReceiverPerson
              }
              handleReceiverDepartmentPopupClose={
                this.handleReceiverDepartmentPopupClose
              }
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              selectUsePerson={this.selectUsePerson}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              getAssetDocument={this.getAssetDocument}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              selectReceiverDepartment={this.selectReceiverDepartment}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              isRoleAssetUser={getTheHighestRole().isRoleAssetUser}
              isRoleAssetManager={getTheHighestRole().isRoleAssetManager}
              isFormMaintainRqquest={this?.props?.isFormMaintainRqquest}
            />

            {this.state?.shouldOpenRefuseTransfer &&
              <RefuseTransferDialog
                t={t}
                open={this.state?.shouldOpenRefuseTransfer}
                item={this.state}
                handleChange={this.handleChange}
                handleClose={this.handleCloseRefuseTransferDialog}
                handleConfirmReason={() => this.handleStatus(appConst.listStatusTransfer[4].transferStatusIndex)}
              />
            }
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              {isConfirm ? <>
                <Button
                  className="mb-16 mr-16"
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(appConst.listStatusTransfer[1].transferStatusIndex)} >
                  {t("InstrumentToolsTransfer.confirm")}
                </Button>
                <Button
                  className="btnRefuse mb-16 mr-16"
                  variant="contained"
                  onClick={this.handleRefuse}
                >
                  {t("InstrumentToolsTransfer.refuse")}
                </Button>
                <Button
                  className="btnClose mb-16 mr-16"
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    this.props.handleClose();
                  }}
                >
                  {t("InstrumentToolsTransfer.close")}
                </Button>
              </> : <>
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={() => this.props.handleClose()}
                >
                  {t("general.cancel")}
                </Button>
                {!isView && (isOwner || !this.state.id) && (
                  <Button variant="contained" color="primary" type="submit">
                    {t("general.save")}
                  </Button>
                )}
                {this.state?.transferStatusIndex === appConst.STATUS_TRANSFER.DA_DIEU_CHUYEN.indexOrder
                  && this.state.id && (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        this.props.handleQrCode(this.state, true);
                      }}
                    >
                      {t("Asset.PrintQRCode")}
                    </Button>
                  )}
              </>}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetExpectedRevocationDialog.contextType = AppContext;
export default AssetExpectedRevocationDialog;
