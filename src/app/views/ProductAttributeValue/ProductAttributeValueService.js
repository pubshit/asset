import axios from "axios";
import ConstantList from "../../appConfig";


export const getAllProducts = () => {
  return axios.get(ConstantList.API_ENPOINT+"/api/product/getall");  
};
export const getAllProperties = () => {
  return axios.get(ConstantList.API_ENPOINT+"/api/productAttribute/1/100000");  
};

export const getProductAttributeValueByPage = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  var url = ConstantList.API_ENPOINT+"/api/product_attribute_value/"+params;
  return axios.get(url);  
};
export const searchByPage =  (keyword, pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  return axios.post(ConstantList.API_ENPOINT + "/api/product_attribute_value/searchByText/" + params, keyword);
};

export const getUserById = id => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = id => {
  return axios.delete(ConstantList.API_ENPOINT+"/api/product_attribute_value/"+id);
};

export const getItemById = id => {
  return axios.get(ConstantList.API_ENPOINT + "/api/product_attribute_value/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: {id: id, code: code } };
  var url = ConstantList.API_ENPOINT+"/api/product_attribute_value/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateProductAttributeValue = ProductAttributeValue => {
  return axios.post(ConstantList.API_ENPOINT + "/api/product_attribute_value", ProductAttributeValue);
};
