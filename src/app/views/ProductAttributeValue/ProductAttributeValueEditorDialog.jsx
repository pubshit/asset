import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch,DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { getAllProducts, getAllProperties, getAllProductAttributeValues, addNewOrUpdateProductAttributeValue, searchByPage } from './ProductAttributeValueService';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class ProductAttributeValueEditorDialog extends Component {
  state = {
    valueNumber: 0,
    valueBoolean: false,
    valueText: "",
    product: {}, productSelect: {},
    property: {}, propertySelect: {}

  };
  handleChangeProduct = (event, source) => {
    let { productSelect } = this.state;
    this.setState({
      product: productSelect.find(item => item.id == event.target.value),
      productId: event.target.value
    })
  }
  handleChangeProperty = (event, source) => {
    let { propertySelect } = this.state;
    this.setState({
      property: propertySelect.find(item => item.id == event.target.value),
      propertyId: event.target.value
    })
  }
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ valueBoolean: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    if (id) {
      addNewOrUpdateProductAttributeValue({
        ...this.state
      }).then(() => {
        this.props.handleOKEditClose();
      });
    } else {
      addNewOrUpdateProductAttributeValue({
        ...this.state
      }).then(() => {
        this.props.handleOKEditClose();
      });
    }
  };

  componentWillMount() {
    getAllProducts().then((result) => {
      let productSelect = result.data;
      this.setState({ productSelect: productSelect });
    });
    getAllProperties().then((result) => {
      let propertySelect = result.data.content;
      this.setState({ propertySelect: propertySelect });
    });
    let { open, handleClose, item } = this.props;
    this.setState({
      ...this.props.item
    }, function () {
      let { product, property } = this.state;
      if (product != null && product.id != null) {
        this.setState({ productId: product.id })
      }
      if (property != null && property.id != null) {
        this.setState({ propertyId: property.id })
      }
    }
    );
  }
  componentDidMount() {
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let {
      id,
      valueNumber,
      valueBoolean,
      valueText,
      product, productId, productSelect,
      property, propertyId, propertySelect
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md">
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <h4 className="mb-20">{t('general.saveUpdate')}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
        <DialogContent>
            <Grid className="mb-16" container spacing={4}>
              <Grid item sm={12} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="property">{t("ProductAttributeValue.property")}</InputLabel>
                  <Select
                    value={propertyId}
                    onChange={event => this.handleChangeProperty(event)}
                    inputProps={{
                      name: "property",
                      id: "property"
                    }}
                  >
                    {(propertySelect && propertySelect.length > 0 && propertySelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    )))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="product">{t("ProductAttributeValue.product")}</InputLabel>
                  <Select
                    value={productId}
                    onChange={event => this.handleChangeProduct(event)}
                    inputProps={{
                      name: "product",
                      id: "product"
                    }}
                  >
                    {(productSelect && productSelect.length > 0 && productSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    )))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t('ProductAttributeValue.valueText')}
                  onChange={this.handleChange}
                  type="text"
                  name="valueText"
                  value={valueText}
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t('ProductAttributeValue.valueNumber')}
                  onChange={this.handleChange}
                  type="text"
                  name="valueNumber"
                  value={valueNumber}
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                />
              </Grid>
              <Grid>
                <FormControlLabel
                  className="my-20"
                  control={
                    <Switch
                      checked={valueBoolean}
                      onChange={event => this.handleChange(event, "switch")}
                    />
                  }
                  label={t('IsActive')}
                />
              </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="primary"
              className="mr-36"
              type="submit"
            >
              {t('general.save')}
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => this.props.handleClose()}
            >
              {t('general.cancel')}
            </Button>
          </div>
        </DialogActions>
        </ValidatorForm>

      </Dialog>
    );
  }
}

export default ProductAttributeValueEditorDialog;
