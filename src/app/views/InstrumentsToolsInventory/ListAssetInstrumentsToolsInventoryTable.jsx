import React, { useEffect, useState } from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import { convertNumberPrice, correctPrecision, NumberFormatCustom } from "app/appFunction";
import { TableCell, TableHead, TableRow, makeStyles } from "@material-ui/core";
import clsx from "clsx";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";

function ListAssetInstrumentsToolsInventoryTable(props) {
  const { t } = props;
  const [data, setData] = useState()
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
    header: {
      zIndex: 1,
      background: "#358600",
      position: "sticky",
      top: 0,
      left: 0,
      "& .MuiTableCell-head": {
        padding: "4px" // <-- arbitrary value
      }
    },
    borderRight: {
      borderRight: "1px solid white !important",
      color: "#ffffff",
      textAlign: "center"
    },
    colorWhite: {
      color: "#ffffff",
      textAlign: "center"
    },
    mw_100: {
      minWidth: "90px"
    },
    mw_70: {
      minWidth: "70px"
    },
    mw_120: {
      minWidth: "120px"
    },
    mw_160: {
      minWidth: "160px"
    },
    tabPanel: {
      "& .MuiBox-root": {
        paddingLeft: "0",
        paddingRight: "0"
      }
    },
    textRight: {
      "& input": {
        textAlign: "right"
      }
    }
  }));
  const classes = useStyles();

  const handleDifferenceQuantity = (inv, acc) => {
    return (inv > acc) ? inv - acc : 0
  }

  const handleDifferenceAmount = (rowData) => {
    const diffQuantity = handleDifferenceQuantity(rowData?.inventoryQuantity, rowData?.accountantQuantity);
    if (diffQuantity > 0) {
      return convertNumberPrice(Math.ceil((rowData?.accountantOriginalCost / rowData?.accountantQuantity) * diffQuantity))
    } else {
      return 0
    }
  }

  let columns = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 180,
    },
    {
      title: t("InventoryCountStatus.CountAsset.seri"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: t("InventoryCountStatus.CountAsset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: t("InventoryCountStatus.CountAsset.currentStatus"),
      field: "currentStatus.name",
      align: "left",
      minWidth: 140,
    },
    {
      title: t("InventoryCountStatus.CountAsset.status"),
      align: "left",
      minWidth: 140,
      render: (rowData) => (
        <div> {rowData.status ? rowData.status?.name : ""}</div>
      ),
    },
    {
      title: t("Asset.receive_date"),
      field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.dateOfReception ? (
          <span>
            {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
          </span>
        ) : (
          ""
        ),
    },
    {
      title: t("InventoryCountVoucher.store"),
      field: "asset.store",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <div>{rowData?.store ? rowData?.store?.name : ""}</div>
      ),
    },
    {
      title: t("Asset.currentUseByDepartment"),
      field: "asset.departmentName",
      align: "left",
      minWidth: 160,
      render: (rowData) => rowData?.departmentName
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.accountantQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.accountantQuantity ? rowData.accountantQuantity : 0,
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.accountantOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.accountantOriginalCost ? convertNumberPrice((rowData.accountantOriginalCost)) : 0,
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.inventoryQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => <div>{rowData.inventoryQuantity}</div>,
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.inventoryOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <ValidatorForm onSubmit={() => { }}>
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceOriginalCost"
            defaultValue={correctPrecision(rowData?.inventoryOriginalCost)}
            value={correctPrecision(rowData?.inventoryOriginalCost) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        </ValidatorForm>
      ),
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.differenceQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.inventoryQuantity - rowData?.accountantQuantity
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.differenceOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        let differenceOriginalCost = correctPrecision((rowData?.inventoryQuantity * rowData?.inventoryOriginalCost) - (rowData?.accountantQuantity * rowData?.accountantOriginalCost)) || 0;
        return (
          <ValidatorForm onSubmit={() => { }}>
            <TextValidator
              className={clsx("w-40", classes.textRight)}
              name="differenceOriginalCost"
              defaultValue={differenceOriginalCost}
              value={differenceOriginalCost}
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: true,
                disableUnderline: true,
              }}
            />
          </ValidatorForm>
        )
      },
    },
    {
      title: t("InventoryCountVoucher.note"),
      field: "custom",
      align: "left",
      minWidth: 150,
      render: (rowData) => <div>{rowData.note}</div>,
    },
  ];
  return (
    <>
      <MaterialTable
        data={props?.itemAsset?.assets}
        columns={columns}
        options={{
          draggable: false,
          toolbar: false,
          selection: false,
          actionsColumnIndex: -1,
          paging: false,
          search: false,
          sorting: false,
          maxBodyHeight: 3,
          minBodyHeight: "273px",
          padding: "dense",
          rowStyle: (rowData) => ({
            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
          }),
          headerStyle: {
            backgroundColor: "#358600",
            color: "#fff",
            position: "relative",
            textAlign: "center",
          },
        }}
        localization={{
          body: {
            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
          },
        }}
        components={{
          Toolbar: (props) => (
            <div style={{ witdth: "100%" }}>
              <MTableToolbar {...props} />
            </div>
          ),

          Header: () => {
            return (
              <TableHead className={clsx(classes.header)}>
                <TableRow>
                  <TableCell className={clsx(classes.borderRight)} rowSpan={2}>
                    {t("Asset.stt")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                    rowSpan={2}
                  >
                    {t("Asset.managementCode")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_70)}
                    rowSpan={2}
                  >
                    {t("InstrumentsToolsInventory.code")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_70)}
                    rowSpan={2}
                  >
                    {t("InstrumentsToolsInventory.name")}
                  </TableCell>
                  <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.seri")}</TableCell>
                  <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.model")}</TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_160)}
                    rowSpan={2}
                  >
                    {t("InventoryCountStatus.CountAsset.currentStatus")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_160)}
                    rowSpan={2}
                  >
                    {t("InventoryCountStatus.CountAsset.status")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_120)}
                    rowSpan={2}
                  >
                    {t("Asset.receive_date")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_160)}
                    rowSpan={2}
                  >
                    {t("InventoryCountVoucher.store")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_160)}
                    rowSpan={2}
                  >
                    {t("Asset.currentUseByDepartment")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_70)}
                    colSpan={2}
                  >
                    {t("InventoryCountVoucher.accordingAccountingBooks")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_70)}
                    colSpan={2}
                  >
                    {t("InventoryCountVoucher.accordingInventory")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_70)}
                    colSpan={2}
                  >
                    {t("InventoryCountVoucher.deviant")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.colorWhite, classes.mw_70)}
                    rowSpan={2}
                  >
                    {t("InventoryCountVoucher.note")}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.quantity")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.originalCost")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.quantity")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.originalCost")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.quantity")}
                  </TableCell>
                  <TableCell
                    className={clsx(classes.borderRight, classes.mw_100)}
                  >
                    {t("InstrumentsToolsInventory.originalCost")}
                  </TableCell>
                </TableRow>
              </TableHead>
            );
          },
        }}
        onSelectionChange={(rows) => {
          this.data = rows;
        }}
      />
    </>
  );
}

export default ListAssetInstrumentsToolsInventoryTable;
