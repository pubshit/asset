import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { addAsset, getAssetByDepartmentByOrg, updateAsset } from './InstrumentsToolsInventoryService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../../../styles/views/_loadding.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InventoryCountScrollableTabsButtonForce from './InventoryCountScrollableTabsButtonForce'
import { appConst, variable } from "../../appConst";
import AppContext from "app/appContext";
import {
  checkInvalidDate,
  checkMinDate,
  getAssetStatus,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import { PaperComponent } from "../Component/Utilities";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import { formatDateDto } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class InventoryCountVoucherDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    assets: [],
    title: null,
    inventoryCountDate: new Date(),
    department: null,
    inventoryCountStatus: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: '',
    asset: {},
    inventoryCountPersons: [],
    shouldOpenSelectPersonPopup: false,
    shouldOpenPopupAssetFile: false,
    listAssetDocumentId: [],
    loading: false,
  };

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true
    })
  }

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false
    })
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable?.listInputName?.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  selectHandoverPerson = (item) => {
    this.setState({ handoverPerson: item }, function () {
    });
  }

  selectReceiverPerson = (item) => {
    this.setState({ receiverPerson: item }, function () {
    });
  }
  validateAsset = () => {
    let { assets } = this.state;
    let check = false;
    assets?.some(asset => {
      let isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder));
      if (isStore && !asset?.store) {
        toast.warning("Chưa chọn kho");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryQuantity && asset?.inventoryQuantity !== 0) {
        toast.warning("Số lượng không được bỏ trống");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryQuantity < 0) {
        toast.warning("Số lượng không được âm");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryOriginalCost && asset?.inventoryOriginalCost !== 0) {
        toast.warning("Thành tiền không được bỏ trống");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryOriginalCost < 0) {
        toast.warning("Thành tiền không được âm");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }
  validateSubmit = () => {
    let {
      assets,
      title,
      inventoryCountStatus,
      inventoryCountPersons,
      inventoryCountDate,
      listAssetsAdded
    } = this.state;
    const listAsset = [...(assets?.length ? assets : []), ...(listAssetsAdded?.length ? listAssetsAdded : [])];
    if (!inventoryCountDate) {
      toast.warning('Chưa chọn ngày kiểm kê');
      return false;
    }
    if (checkInvalidDate(inventoryCountDate)) {
      toast.warning('Ngày kiểm kê không tồn tại');
      return false;
    }
    if (checkMinDate(inventoryCountDate, "01/01/1900")) {
      toast.warning(`Ngày kiểm kê không được nhỏ hơn ngày "01/01/1900"`);
      return false;
    }
    if (!title) {
      this.setState({ loading: false });
      toast.warn("Chưa nhập kiểm kê CCDC.");
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountStatus) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn trạng thái.");
      toast.clearWaitingQueue();
      return false;
    }
    if (listAsset?.length <= 0) {
      toast.warning("Phòng ban không có CCDC kiểm kê");
      toast.clearWaitingQueue();
      return false;
    }
    if (this.state?.hoiDongId && (!inventoryCountPersons || inventoryCountPersons?.length === 0)) {
      toast.warning("Hội đồng không có thành viên")
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountPersons || inventoryCountPersons?.length === 0) {
      toast.warning("Chưa chọn hội đồng kiêm kê")
      toast.clearWaitingQueue();
      return false;
    }
    if (this.validateAsset()) return;
    return true;
  }
  handleFormSubmit = (e) => {
    if (e.target.id !== "instrumentsToolsInventory") return;
    let { setPageLoading } = this.context;
    let {
      id,
      inventoryCountDate,
      inventoryCountStatusId,
      assets = [],
      listAssetsAdded = [],
      title,
      hoiDongId,
      departmentId,
      assetReevaluate
    } = this.state;
    if (!this.validateSubmit()) return;
    let newData = [...assets, ...listAssetsAdded]
    let dataSubmit = {
      id,
      inventoryCountDate: inventoryCountDate ? formatDateDto(inventoryCountDate) : null,
      inventoryCountStatusId,
      assets: newData?.map(i => {
        return {
          ...i,
          storeId: i?.store?.id || i?.asset?.storeId,
          departmentId: i?.departmentId || i?.asset?.useDepartmentId,
        }
      }),
      departmentId,
      title,
      hoiDongId,
      assetReevaluate,
      inventoryCountPersons: this.state?.inventoryCountPersons ? this.state?.inventoryCountPersons?.map(item => {
        return {
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }
      }) : []
    }

    if (id) {
      updateAsset(dataSubmit)
        .then((res) => {
          if (isSuccessfulResponse(res?.data?.code)) {
            toast.success("Sửa phiếu thành công.");
            this.props.handleOKEditClose();
          } else {
            handleThrowResponseMessage(res)
          }
        })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
        })
        .finally(() => {
          setPageLoading(false)
        })
    } else {
      addAsset(dataSubmit)
        .then((res) => {
          if (isSuccessfulResponse(res?.data?.code)) {
            toast.success("Thêm mới phiếu thành công.");
            this.props.handleOKEditClose();
          } else {
            handleThrowResponseMessage(res)
          }
        })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
        })
        .finally(() => {
          setPageLoading(false)
        })
    }
  };
  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false
    });
  };
  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item?.id) {
      if (!assetVouchers) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      }
      else {
        let hasInList = false;
        assetVouchers?.forEach(element => {
          if (element?.asset?.id === item?.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState({ assetVouchers, totalElements: assetVouchers?.length }, function () {
      this.handleAssetPopupClose();
    });
  }

  componentWillMount() {
    let { item } = this.props;
    if (this.props.item?.assets) {
      this.props.item?.assets.sort((a, b) => (a.id - b.id));
    }
    this.setState({
      ...this.props.item,
      assets: this.props?.item?.assets?.filter(i => i?.accountantQuantity),
      listAssetsAdded: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 0)
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props?.item !== prevProps.item) {
      if (this.props?.item?.assets) {
        this.props.item?.assets.sort((a, b) => (a.id - b.id));
        this.setState({
          assets: this.props?.item?.assets?.filter(i => i?.accountantQuantity),
          listAssetsAdded: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 0)
        })
      }
    }
  }

  handleSetListInventoryPerson = (item) => {
    this.setState({ inventoryCountPersons: item })
  }

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id })
  }


  componentDidMount() {

  }

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState({ item: {}, voucherType: voucherType, departmentId: handoverDepartment?.id }, function () {
        this.setState({
          shouldOpenAssetPopup: true
        });
      });
    }
    else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  }

  handlePopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
      shouldOpenSelectAssetPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true
    });
  };


  handleSelectAssetPopupOpen = () => {
    if (!this.state?.department?.id) {
      toast.info("Vui lòng chọn phòng ban được kiểm kê (Thông tin phiếu)")
      return;
    }
    this.setState({
      shouldOpenSelectAssetPopup: true
    });
  };

  handleDeleteItem = (rowData) => {
    let { listAssetsAdded } = this.state;
    const index = listAssetsAdded?.findIndex(item => item?.tableData?.id === rowData?.tableData?.id);

    if (index > -1) {
      listAssetsAdded.splice(index, 1);
    }

    this.setState({ listAssetsAdded });
  }

  handleSelectAssetAdded = (assets = []) => {
    this.setState({
      listAssetsAdded: assets?.map(i => {
        return {
          ...i,
          accountantOriginalCost: (i?.asset?.originalCost || 0),
          assetId: i.assetId || i?.asset?.assetId,
          assetItemId: i.assetItemId || i?.asset?.id,
          accountantQuantity: 0,
          differenceOriginalCost: 0,
          differenceQuantity: i?.asset?.quantity,
          inventoryOriginalCost: (i?.asset?.originalCost || 0),
          inventoryQuantity: i?.asset?.quantity,
          currentStatus: i?.asset?.statusName
            || i?.currentStatus?.name
            || i?.currentStatus
            || null,
          note: "",
          store: {
            id: i?.asset?.storeId || i?.store?.id,
            name: i?.asset?.storeName || i?.store?.name
          },
          status: {
            id: i?.status?.id || i?.asset?.statusId,
            name: i?.status?.name || i?.asset?.statusName,
          },
          assetStatusId: i?.status?.id || i?.asset?.statusId,
          departmentId: i?.departmentId || i?.asset?.useDepartmentId,
          departmentName: i?.departmentName || i?.asset?.useDepartmentName,
        }
      }),
      shouldOpenSelectAssetPopup: false
    })
  }

  handleSelectDepartment = (item) => {
    let { setPageLoading } = this.context;
    this.setState({
      department: { id: item?.id, name: item?.name ? item?.name : item?.text },
      assets: [],
    }, () => {
      setPageLoading(true);
      let inventoryCountAssets = [];
      if (item?.id) {
        let searchObject = {}
        searchObject.useDepartmentId = item.id
        searchObject.assetClass = appConst.assetClass.CCDC
        getAssetByDepartmentByOrg(searchObject)
          .then(({ data }) => {
            setPageLoading(false)
            if (appConst.CODE.SUCCESS !== data?.code) {
              toast.warning(data?.message)
              return;
            }
            if (data?.data?.length > 0) {
              data.data.forEach(asset => {
                let inventoryCountAsset = {};
                inventoryCountAsset.asset = asset;
                inventoryCountAsset.assetId = asset?.assetId;
                inventoryCountAsset.currentStatus = asset?.status;
                inventoryCountAsset.status = asset?.status;
                inventoryCountAsset.assetStatusId = asset.status?.id;
                inventoryCountAsset.note = '';
                inventoryCountAsset.inventoryOriginalCost = (asset?.originalCost) || 0; //thành tiền kiểm kê
                inventoryCountAsset.inventoryQuantity = asset?.quantity || 0; //số lượng kiểm kê
                inventoryCountAsset.price = (asset?.originalCost / asset?.quantity); //số tiền 1 cái
                inventoryCountAsset.accountantOriginalCost = (asset?.originalCost) || 0; // thành tiền kiểm sổ
                inventoryCountAsset.accountantQuantity = asset?.quantity || 0; // số lượng kiểm sổ
                inventoryCountAsset.store = asset?.store ? asset?.store : null;
                inventoryCountAsset.storeId = asset?.store?.id ? asset?.store?.id : null;
                inventoryCountAsset.isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder))
                inventoryCountAssets.push(inventoryCountAsset);
              });
              this.setState({
                assets: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
                departmentId: item.id
              },);
            }
          })
          .catch((error) => {
            toast.error(this.props.t("general.error"))
            setPageLoading(false)
          })
      }
      else {
        this.setState({ assets: inventoryCountAssets });
      }
      this.handlePopupClose();
    });
    if (Object.keys(item).length === 0) {
      this.setState({ department: null, assets: null })
    }
  }

  selectInventoryCount = (item) => {
    this.setState({
      inventoryCountStatus: item,
      inventoryCountStatusId: item?.id
    });
  }
  selectStatus = (assetStatus, rowData, isTabNotRecorded) => {
    let { assets = [], listAssetsAdded = [] } = this.state;
    if (isTabNotRecorded) {
      listAssetsAdded[rowData?.tableData?.id].status = assetStatus
      listAssetsAdded[rowData?.tableData?.id].assetStatusId = assetStatus?.id
      listAssetsAdded[rowData?.tableData?.id].isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === assetStatus?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === assetStatus?.indexOrder))

    } else {
      assets[rowData?.tableData?.id].status = assetStatus
      assets[rowData?.tableData?.id].assetStatusId = assetStatus?.id
      assets[rowData?.tableData?.id].isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === assetStatus?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === assetStatus?.indexOrder))
    }
    this.setState({ assets, listAssetsAdded });
  }

  handleRowDataCellChange = (rowData, event, isTabNotRecorded) => {
    let { assets = [], listAssetsAdded = [] } = this.state;
    let item = (isTabNotRecorded ? listAssetsAdded?.[rowData?.tableData?.id] : assets?.[rowData?.tableData?.id]) || {};
    if (variable.listInputName.note === event.target.name) {
      item.note = event.target.value;
      this.setState({ assets, listAssetsAdded })
    }
    else if (variable.listInputName.inventoryOriginalCost === event.target.name) {
      let value = Number(event.target.value);
      item.inventoryOriginalCost = value;
      item.differenceOriginalCost = Math.abs(item.accountantOriginalCost - value);
      this.setState({ assets, listAssetsAdded });
    }
    else if (variable.listInputName.inventoryQuantity === event.target.name) {
      let value = Number(event.target.value);
      let inventoryOriginalCost = isTabNotRecorded ? value * (item?.asset?.originalCost / item?.asset?.quantity) : (value === item.accountantQuantity) ? item.accountantOriginalCost : (value * item?.price)
      item.inventoryQuantity = value;
      item.differenceQuantity = item?.accountantQuantity - value;
      item.inventoryOriginalCost = (inventoryOriginalCost);
      this.setState({ assets, listAssetsAdded });
    }
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true
    });
  }

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false
    });
  }

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes?.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (attributes[index]?.attribute?.id === rowData.attribute?.id) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () {
    });
  };

  selectStores = (store, rowData, isTabNotRecorded) => {
    const { assets, listAssetsAdded } = this.state;
    const dataList = isTabNotRecorded ? listAssetsAdded : assets;
    const rowId = rowData?.tableData?.id;
    if (rowId !== undefined) {
      dataList[rowId].store = store;
      dataList[rowId].storeId = store?.id;
    }

    this.setState({ assets, listAssetsAdded });
  };

  handlePrintSingleItem = (rowData) => {
    this.setState({
      itemPrint:
      {
        ...rowData,
        asset: {
          ...rowData?.asset,
          detailInfo:
            rowData?.asset?.serialNumber ||
              rowData?.asset?.model ||
              rowData?.asset?.yearPutIntoUse
              ? `${rowData?.asset?.serialNumber || "........."}/ ${rowData?.asset?.model || "........."}/ ${rowData?.asset?.yearPutIntoUse || "........."}`
              : "..".repeat(100),
          useDepartment: {
            ...rowData?.asset?.useDepartment,
            name: rowData?.asset?.useDepartment?.name || "..".repeat(100)
          }
        }
      }
    }, () => {
      this.setState({ shoundOpenPrintSingleItem: true })
    })
  }

  handleClosePrintSingleItem = () => {
    this.setState({ shoundOpenPrintSingleItem: false, itemPrint: null })
  }

  render() {
    let { open, t, i18n, item } = this.props;
    let { loading, shoundOpenPrintSingleItem, itemPrint } = this.state;
    let org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
    const newDate = new Date(item?.inventoryCountDate)
    const day = item?.inventoryCountDate ? String(newDate?.getDate())?.padStart(2, '0') : ".....";
    const month = item?.inventoryCountDate ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : ".....";
    const year = item?.inventoryCountDate ? String(newDate?.getFullYear()) : "......";
    return (
      <Dialog className="w-90 m-auto" open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth scroll="paper" >
        <div className={clsx("wrapperButton", !loading && 'hidden')} >
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id='instrumentsToolsInventory'
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            {item?.id ? t('InstrumentsToolsInventory.updateDialog') : t('InstrumentsToolsInventory.addDialog')}
          </DialogTitle>
          <DialogContent >
            <Grid>
              <InventoryCountScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
                handleChange={this.handleChange}
                selectInventoryCount={this.selectInventoryCount}
                handleSelectDepartment={this.handleSelectDepartment}
                handleDeleteItem={this.handleDeleteItem}
                handlePopupClose={this.handlePopupClose}
                selectStatus={this.selectStatus}
                selectStores={this.selectStores}
                handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
                openSelectPersonPopup={this.openSelectPersonPopup}
                handleSelectPerson={this.handleSelectPerson}
                removePersonInlist={this.removePersonInlist}
                handleRowDataCellChange={this.handleRowDataCellChange}
                handleDateChange={this.handleDateChange}
                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                handleSetListInventoryPerson={this.handleSetListInventoryPerson}
                handleSetHoiDongId={this.handleSetHoiDongId}
                handlePrintSingleItem={this.handlePrintSingleItem}
                handleSelectAssetPopupOpen={this.handleSelectAssetPopupOpen}
                handleSelectAssetAdded={this.handleSelectAssetAdded}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => { this.props.handleClose() }}
              >
                {t('general.cancel')}
              </Button>
              {!item?.isView &&
                <Button variant="contained" color="primary" type="submit" onClick={(e) => {
                  if (!this.validateSubmit()) {
                    e.preventDefault();
                  }
                }}>
                  {t('general.save')}
                </Button>
              }
            </div>
          </DialogActions>
        </ValidatorForm>
        {shoundOpenPrintSingleItem && <>
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={() => this.handleClosePrintSingleItem()}
            open={shoundOpenPrintSingleItem}
            item={{ ...itemPrint, org, day, month, year }}
            title={t("InventoryCountVoucher.printTitle")}
            urls={[
              {
                // tabLabel: "Mẫu đánh giá trình trạng",
                url: "/assets/form-print/danhGiaTinhTrangThietBi_bvHoeNhai.txt",
                config: {
                  layout: "landscape",
                  margin: "horizontal",
                },
              },
            ]}
          />
        </>}
      </Dialog>
    );
  }
}
InventoryCountVoucherDialog.contextType = AppContext;
export default InventoryCountVoucherDialog;
