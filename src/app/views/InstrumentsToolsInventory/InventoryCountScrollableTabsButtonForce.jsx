import React, { useState } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
  TableHead,
  TableRow,
  TableCell,
} from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import NumberFormat from "react-number-format";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import MaterialTable, { MTableToolbar } from "material-table";
import { getAllAssetStatuss as searchAssetStatus } from "../AssetStatus/AssetStatusService";
import { searchByPage as storesSearchByPage } from "../Store/StoreService";
import { getAllInventoryCountStatus } from "../InventoryCountStatus/InventoryCountStatusService";
import clsx from "clsx";
import moment from "moment";
import { createFilterOptions } from "@material-ui/lab/useAutocomplete";
import { appConst, STATUS_STORE } from "../../appConst";
import { a11yProps, checkInvalidDate, convertNumberPrice, correctPrecision, filterOptions, TabPanel } from "../../appFunction";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchByPage } from "../Council/CouncilService";
import { LightTooltip, NumberFormatCustom } from "../Component/Utilities";
import { toast } from "react-toastify";
import SelectAssetAllPopup from "../Component/InstrumentTools/SelectInstrumentToolsAllPopup";

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    zIndex: 1,
    background: "#358600",
    position: "sticky",
    top: 0,
    left: 0,
    "& .MuiTableCell-head": {
      padding: "4px" // <-- arbitrary value
    }
  },
  borderRight: {
    borderRight: "1px solid white !important",
    color: "#ffffff",
    textAlign: "center"
  },
  colorWhite: {
    color: "#ffffff",
    textAlign: "center"
  },
  mw_100: {
    minWidth: "90px"
  },
  mw_70: {
    minWidth: "70px"
  },
  tabPanel: {
    "& .MuiBox-root": {
      paddingLeft: "0",
      paddingRight: "0"
    }
  },
  textRight: {
    "& input": {
      textAlign: "right"
    }
  },
  textCenter: {
    "& input": {
      textAlign: "center"
    }
  }
}));

function Number(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      name={props.name}
      value={props.value}
    />
  );
}
export default function ProductScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const classes = useStyles();
  const [checkTab, setCheckTab] = useState({
    isTabInfo: true,
    isTabCouncil: false,
    isTabNotRecorded: false,
  });
  const [value, setValue] = useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const searchObjectStore = {
    pageIndex: 0,
    pageSize: 1000000,
    isActive: STATUS_STORE.HOAT_DONG.code
  }; // kho tài sản
  const filterAutocomplete = createFilterOptions();
  const [listInventoryCountBoard, setListInventoryCountBoard] = useState([]);
  const [inventoryBoard, setInventoryBoard] = useState([])
  const [listInventoryPerson, setListInventoryPerson] = useState(props?.item?.inventoryCountPersons || [])
  const handleChange = (event, newValue) => {
    setValue(newValue);
    setCheckTab({
      isTabInfo: newValue === appConst.tabDialogInventory.tabInfo,
      isTabCouncil: newValue === appConst.tabDialogInventory.tabCouncil,
      isTabNotRecorded: newValue === appConst.tabDialogInventory.tabNotRecorded,
    })
  };
  const handleSelectHandoverDepartment = (item, rowData) => {
    let newDataRow = listInventoryPerson.map((i) => {
      if (i.tableData.id === rowData.tableData.id) {
        i.handoverDepartment = item;
        i.departmentId = item?.id;
        i.departmentName = item?.name
      }
      return i;
    });
    setListInventoryPerson(newDataRow || []);
  }

  const handleRowDataCellDelete = (item) => {
    let newDataRow = listInventoryPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListInventoryPerson(newDataRow)
    setListInventoryPerson(newDataRow || []);
  };

  let searchObject = { pageIndex: 0, pageSize: 1000000, type: appConst.OBJECT_HD_TYPE.KIEM_KE.code, };
  let columns = [
    {
      title: t("general.action"),
      field: "custom",
      align: "left",
      minWidth: 60,
      hidden: checkTab?.isTabNotRecorded && props?.item?.isView,
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (<>
        {
          !checkTab?.isTabNotRecorded ? <LightTooltip
            title={t("general.print")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton size="small" onClick={() => props?.handlePrintSingleItem(rowData)}>
              <Icon fontSize="small" color="inherit">
                print
              </Icon>
            </IconButton>
          </LightTooltip>
            :
            (<LightTooltip
              title={t("general.print")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
              PopperProps={{
                popperOptions: {
                  modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                },
              }}
            >
              <IconButton size="small" onClick={() => props?.handleDeleteItem(rowData)}>
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>)
        }
      </>)
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.managementCode || rowData?.managementCode
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.code || rowData?.code
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 180,
      render: (rowData) => rowData?.asset?.name || rowData?.name
    },
    {
      title: t("InventoryCountStatus.CountAsset.seri"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => rowData?.asset?.serialNumber || rowData?.serialNumber
    },
    {
      title: t("InventoryCountStatus.CountAsset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => rowData?.asset?.model || rowData?.model
    },
    {
      title: t("InventoryCountStatus.CountAsset.currentStatus"),
      field: "currentStatus",
      align: "left",
      minWidth: 180,
      render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
    },
    {
      title: t("InventoryCountStatus.CountAsset.status"),
      align: "left",
      field: "status",
      minWidth: 180,
      render: (rowData) => {
        return (
          <AsynchronousAutocompleteSub
            searchFunction={searchAssetStatus}
            searchObject={searchObject}
            displayLable={"name"}
            readOnly={props?.item?.isView}
            value={rowData?.status || null}
            onSelect={(value) => props.selectStatus(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
        )
      },
    },
    {
      title: t("Asset.receive_date"),
      field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.dateOfReception ? (
          <span>
            {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
          </span>
        ) : (
          ""
        ),
    },
    {
      title: t("InventoryCountVoucher.store"),
      field: "asset.store",
      align: "left",
      minWidth: 160,
      render: (rowData) =>
        checkTab?.isTabNotRecorded ? rowData?.store?.name : (rowData?.isStore && (
          <AsynchronousAutocompleteSub
            searchFunction={storesSearchByPage}
            searchObject={searchObjectStore}
            defaultValue={rowData?.store}
            displayLable={"name"}
            readOnly={props?.item?.isView || checkTab?.isTabNotRecorded}
            value={rowData?.store}
            onSelect={(value) => props.selectStores(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />
        ))
    },
    {
      title: t("Asset.currentUseByDepartment"),
      field: "asset.departmentName",
      align: "left",
      hidden: !checkTab?.isTabNotRecorded,
      minWidth: 160,
      render: (rowData) => rowData?.departmentName
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.accountantQuantity",
      align: "left",
      minWidth: 60,
      hidden: checkTab?.isTabNotRecorded,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.accountantQuantity ? rowData.accountantQuantity : 0
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.accountantOriginalCost",
      align: "left",
      minWidth: 110,
      hidden: checkTab?.isTabNotRecorded,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.inventoryQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textCenter)}
          name="inventoryQuantity"
          type="number"
          defaultValue={rowData.inventoryQuantity}
          value={rowData.inventoryQuantity}
          onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
          validators={["minNumber:0"]}
          errorMessages={["Số không được âm"]}
        />
      ),
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.inventoryOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
          name="inventoryOriginalCost"
          defaultValue={rowData?.inventoryOriginalCost}
          value={rowData?.inventoryOriginalCost}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: props?.item?.isView
          }}
          validators={["required", "minNumber:0"]}
          errorMessages={[t("general.required"), "Số không được âm"]}
        />
      ),
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "asset.differenceQuantity",
      align: "left",
      minWidth: 60,
      hidden: checkTab?.isTabNotRecorded,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.differenceQuantity ? Math.abs(rowData.differenceQuantity) : 0,
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "asset.differenceOriginalCost",
      align: "left",
      minWidth: 110,
      hidden: checkTab?.isTabNotRecorded,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          name="differenceOriginalCost"
          defaultValue={correctPrecision(rowData?.differenceOriginalCost)}
          value={correctPrecision(rowData?.differenceOriginalCost) || 0}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: true,
            disableUnderline: true,
          }}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.note"),
      field: "custom",
      align: "left",
      minWidth: 150,
      render: (rowData) => (
        <TextValidator
          className="w-40"
          onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
          type="text"
          name="note"
          value={rowData.note}
          InputProps={{
            readOnly: props?.item?.isView
          }}
        />
      ),
    },
  ];

  let columnPerson = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("InventoryCountVoucher.inventoryCountPerson"),
      field: "personDisplayName",
      align: "left",
      minWidth: 400,
    },
    {
      title: t("Asset.department"),
      field: "departmentView",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      maxWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        !props?.item?.isView && <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
            } else if (method === 1) {
              props.removePersonInlist(rowData.person.id);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
  ];

  const columnsInventoryCount = [
    {
      title: t("InventoryCountVoucher.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => handleRowDataCellDelete(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      },
    ] : []),
    {
      title: t("InventoryCountVoucher.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    }
  ]


  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  }
  const handleChangeSelectInventoryBoard = (item) => {
    setInventoryBoard(item);
    props.handleSetHoiDongId(item?.id)
    props.handleSetListInventoryPerson(item?.hdChiTiets)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setListInventoryPerson(item?.hdChiTiets || []);
  };
  return (
    <form id="firstChild">
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab value={0} label={t("InstrumentsToolsInventory.tabInfo")} {...a11yProps(0)} />
            <Tab value={1} label={t("InstrumentsToolsInventory.unrecordedAssets")} {...a11yProps(1)} />
            <Tab label={t("InstrumentsToolsInventory.persons")} {...a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel className={classes.tabPanel} value={value} indexes={[appConst.tabDialogInventory.tabInfo, appConst.tabDialogInventory.tabNotRecorded]}>
          <Grid className="" container spacing={2}>
            <Grid item md={2} sm={12} xs={12}>
              <CustomValidatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InventoryCountVoucher.inventoryCountDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={'inventoryCountDate'}
                value={props.item.inventoryCountDate}
                onChange={(date) =>
                  props.handleDateChange(date, "inventoryCountDate")
                }
                InputProps={{
                  readOnly: props?.item?.isView
                }}
                minDate={new Date("01/01/1900")}
                minDateMessage={"Ngày kiểm kê không được nhỏ hơn ngày 01/01/1900"}
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                invalidDateMessage={"Ngày không tồn tại"}
                validators={['required']}
                errorMessages={t('general.required')}
                open={isPickerOpen}
                onOpen={() => setIsPickerOpen(!props?.item?.isView)}
                onClose={() => setIsPickerOpen(false)}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                onBlur={() => handleBlurDate(props?.item?.inventoryCountDate, "inventoryCountDate")}
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12}>
              <TextValidator
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InstrumentsToolsInventory.title")}
                  </span>
                }
                className="w-100 mr-16"
                name="title"
                value={props.item.title}
                onChange={props.handleChange}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                InputProps={{
                  readOnly: props?.item?.isView
                }}
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12} className="">
              {props?.item?.isView ?
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed"> * </span>
                      {t("InventoryCountVoucher.status")}
                    </span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  value={props.item.inventoryCountStatus ? props.item.inventoryCountStatus?.name : ''}
                /> : <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed"> * </span>
                      {t("InventoryCountVoucher.status")}
                    </span>
                  }
                  searchFunction={getAllInventoryCountStatus}
                  searchObject={searchObject}
                  defaultValue={props.item.inventoryCountStatus}
                  displayLable={"name"}
                  value={props.item.inventoryCountStatus}
                  onSelect={props.selectInventoryCount}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  noOptionsText={t("general.noOption")}
                />
              }
            </Grid>
            <Grid container item md={4} sm={12} xs={12} justifyContent="space-between" >
              <Grid item md={!checkTab?.isTabNotRecorded ? 9 : 12} sm={!checkTab?.isTabNotRecorded ? 9 : 12} xs={!checkTab?.isTabNotRecorded ? 9 : 12}>
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("InventoryCountVoucher.department")}
                    </span>
                  }
                  value={
                    props.item.department ? props.item.department.name : ""
                  }
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={3} sm={3} xs={3}>
                {!checkTab?.isTabNotRecorded && <Button
                  size="small"
                  className="w-100 mt-15"
                  variant="contained"
                  color="primary"
                  disabled={props.item.id || props?.item?.isView}
                  onClick={props.handleDepartmentPopupOpen}
                >
                  {t("general.select")}
                </Button>}
              </Grid>
              {props.item.shouldOpenDepartmentPopup && (
                <SelectDepartmentPopup
                  open={props.item.shouldOpenDepartmentPopup}
                  handleSelect={props.handleSelectDepartment}
                  selectedItem={
                    props.item.department != null ? props.item.department : null
                  }
                  handleClose={props.handlePopupClose}
                  t={t}
                  i18n={i18n}
                />
              )}
            </Grid>
            <Grid item md={12} sm={12} xs={12}>
              {(checkTab?.isTabNotRecorded && !props?.item?.isView) && <Button
                size="small"
                variant="contained"
                color="primary"
                disabled={!checkTab?.isTabNotRecorded}
                onClick={props.handleSelectAssetPopupOpen}
              >
                {t("InstrumentsToolsInventory.addCCDC")}
              </Button>}
              {props.item.shouldOpenSelectAssetPopup &&
                <SelectAssetAllPopup
                  t={t}
                  i18n={i18n}
                  open={props.item.shouldOpenSelectAssetPopup}
                  handleSelect={props.handleSelectAssetAdded}
                  assetVouchers={props?.item?.listAssetsAdded ? props?.item?.listAssetsAdded : []}
                  handleClose={props.handlePopupClose}
                  isNotUseDepartment={props?.item?.department?.id}
                  dateOfReceptionTop={props?.item?.inventoryCountDate}
                  filteredSelected={(item, assetVoucher) => (
                    ((assetVoucher.assetItemId || assetVoucher.asset?.id) === item.id)
                    && (assetVoucher?.asset?.dateOfReception === item?.dateOfReception)
                    && ((assetVoucher?.departmentId || assetVoucher.asset?.useDepartmentId) === item?.useDepartmentId)
                  )}
                />}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} style={{ marginTop: "12px" }}>
              <MaterialTable
                data={checkTab?.isTabNotRecorded ? [...(props?.item?.listAssetsAdded || [])] : [...(props?.item?.assets || [])]}
                columns={columns}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  maxBodyHeight: 3,
                  minBodyHeight: "273px",
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    position: "relative",
                    paddingLeft: 10,
                    paddingRight: 10,
                    textAlign: "center",
                  },
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),

                  Header: () => {
                    return (
                      <TableHead className={clsx(classes.header)}>
                        <TableRow>
                          {!(checkTab?.isTabNotRecorded && props?.item?.isView) && <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("general.action")}</TableCell>}
                          <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("Asset.stt")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} rowSpan={2} >{t("Asset.managementCode")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentsToolsInventory.code")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentsToolsInventory.name")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.seri")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.model")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.receive_date")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.store")}</TableCell>
                          {checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.currentUseByDepartment")}</TableCell>}
                          {!checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>}
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                          {!checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.deviant")}</TableCell>}
                          <TableCell className={clsx(classes.colorWhite, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.note")}</TableCell>
                        </TableRow>
                        <TableRow>
                          {!checkTab?.isTabNotRecorded &&
                            <>
                              <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                              <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                              <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                              <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                            </>
                          }
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                        </TableRow>
                      </TableHead>
                    );
                  },
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={appConst.tabDialogInventory.tabCouncil}>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              searchFunction={searchByPage}
              typeReturnFunction='category'
              searchObject={searchObject}
              label={t('InstrumentsToolsInventory.persons')}
              listData={listInventoryCountBoard}
              displayLable={"name"}
              onSelect={handleChangeSelectInventoryBoard}
              value={inventoryBoard}
              InputProps={{
                readOnly: true,
              }}
              disabled={props?.item?.isView}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim();
                let filtered = filterAutocomplete(options, params);
                return filtered;
              }}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={listInventoryPerson}
              columns={columnsInventoryCount}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingRight: 10,
                  paddingLeft: 10,
                  textAlign: "center",
                },

                maxBodyHeight: "285px",
                minBodyHeight: "285px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div>
    </form>
  );
}
