import React, { useContext, useState } from "react";
import {
  Button,
  Card,
  CardContent,
  Collapse,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  TablePagination,
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ConfirmationDialog } from "egret";
import viLocale from "date-fns/locale/vi";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { STATUS_DEPARTMENT, appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL } from "app/appConst";
import InstrumentsToolsInventoryDialog from "./InstrumentsToolsInventoryDialog";
import { useEffect } from "react";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import InstrumentsToolsInventoryDepartmentDialog from "../InstrumentsToolsInventoryDepartment/InstrumentsToolsInventoryDepartmentDialog";
import { getRole } from "app/appFunction";
import { ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { searchByPage } from "../Department/DepartmentService";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { getInventoryCountVoucherPrintData, handleConvertBVCPrintData } from "../FormCustom/InventoryCountVoucher";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";

function ComponentInventoryTable(props) {
  const { currentOrg } = useContext(AppContext);
  const { INVENTORY_COUNT } = LIST_PRINT_FORM_BY_ORG.IAT_MANAGEMENT;
  const { selectedList = [], openAdvanceSearch, itemAsset } = props?.item;
  const { isRoleAdmin, isRoleSuperAdmin, isRoleOrgAdmin, isRoleAccountant } = getRole();
  const { t, setItemState } = props;

  const [listData, setListData] = useState([])

  let isMongCai = currentOrg?.code === LIST_ORGANIZATION.TTYTTP_MONG_CAI.code;
  let isShowBtnInventoryCount = isRoleAdmin || isRoleSuperAdmin || isRoleOrgAdmin || isRoleAccountant;
  let isButtonAdd = !props?.item?.inventoryCountStatusIndexOrder;
  let dataView = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
    ? handleConvertBVCPrintData(props.item, appConst.assetClass.CCDC)
    : getInventoryCountVoucherPrintData(props.item, appConst.assetClass.CCDC);

  useEffect(() => {
    setItemState({});
  }, []);


  return (
    <>
      <Grid container spacing={2} className="alignItemsFlexEnd spaceBetween">
        <Grid item md={8} sm={12} xs={12}>
          {props.item?.hasCreatePermission && isButtonAdd && (
            <Button
              // size="small"
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                props.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                  isView: false,
                });
              }}
            >
              {t("InventoryCountVoucher.add")}
            </Button>
          )}
          {isShowBtnInventoryCount && (
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={props.handleEditItemInventoryAllDepartment}
              disabled={selectedList?.length <= 0}
            >
              {t("InstrumentsToolsInventory.titleBtn")}
            </Button>
          )}
          <Button
            // size="small"
            className="align-bottom mr-16"
            variant="contained"
            color="primary"
            onClick={props.handleExcel}
          >
            {t("general.exportToExcel")}
          </Button>
          <Button
            className="align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleOpenAdvanceSearch}
          >
            {t("general.advancedSearch")}
            <ArrowDropDownIcon />
          </Button>
          {props.item?.shouldOpenConfirmationExcel && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={props.item?.shouldOpenConfirmationExcel}
              onConfirmDialogClose={props.handleDialogClose}
              onYesClick={props.exportToExcel}
              text={t("general.exportToExcelConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box"
              onChange={props.handleTextChange}
              onKeyDown={props.handleKeyDownEnterSearch}
              onKeyUp={props.handleKeyUp}
              placeholder={t("InventoryCountVoucher.enterSearch")}
              id="search_box"
              value={props.item?.keyword}
              startAdornment={
                <InputAdornment position="end">
                  <SearchIcon
                    onClick={() => props?.search(props.item?.keyword)}
                    className="searchTable"
                  />
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>
        {/* Bộ lọc Tìm kiếm nâng cao */}
        <Grid item xs={12}>
          <Collapse in={openAdvanceSearch}>
            <ValidatorForm>
              <Card elevation={2}>
                <CardContent>
                  <Grid container xs={12} spacing={2}>
                    <Grid item md={3} sm={6} xs={12} className="pr-16" >
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          className="w-100"
                          id="mui-pickers-date"
                          label={t("InventoryCountVoucher.inventoryFromDate")}
                          type="text"
                          autoOk={false}
                          format="dd/MM/yyyy"
                          name={"inventoryCountDateBottom"}
                          value={props.item?.inventoryCountDateBottom}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          minDateMessage={t("general.minDateDefault")}
                          maxDateMessage={props.item?.inventoryCountDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                          maxDate={
                            props.item?.inventoryCountDateTop
                              ? new Date(props.item?.inventoryCountDateTop)
                              : undefined
                          }
                          onChange={(date) =>
                            props.handleDateChange(date, "inventoryCountDateBottom")
                          }
                          clearable
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={3} sm={6} xs={12} className="pr-16">
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          className="w-100"
                          id="mui-pickers-date"
                          label={t("InventoryCountVoucher.inventoryToDate")}
                          type="text"
                          autoOk={false}
                          format="dd/MM/yyyy"
                          name={"inventoryCountDateTop"}
                          value={props.item?.inventoryCountDateTop}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          minDateMessage={props.item?.inventoryCountDateBottom ? t("general.minDateToDate") : t("general.minDateDefault")}
                          maxDateMessage={t("general.maxDateDefault")}
                          minDate={
                            props.item?.inventoryCountDateBottom
                              ? new Date(props.item?.inventoryCountDateBottom)
                              : undefined
                          }
                          clearable
                          onChange={(date) =>
                            props.handleDateChange(date, "inventoryCountDateTop")
                          }
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteTransfer
                        label={t("InventoryCountVoucher.department")}
                        searchFunction={searchByPage}
                        searchObject={{
                          pageIndex: 1, pageSize: 100000, isActive: STATUS_DEPARTMENT.HOAT_DONG.code
                        }}
                        listData={listData}
                        typeReturnFunction="category"
                        setListData={setListData}
                        displayLable={"text"}
                        value={props.item?.department ? props.item?.department : null}
                        onSelect={(data) => props?.handleChangeDepartment(data, "department")}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </ValidatorForm>
          </Collapse>
        </Grid>
        <Grid item xs={12}>
          <div>
            {props.item?.shouldOpenPrintDialog && (
              isMongCai ? (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={props.handleDialogClose}
                  open={props.item?.shouldOpenPrintDialog}
                  item={props.item?.item}
                  title={t("InventoryCountVoucher.printTitle")}
                  model={PRINT_TEMPLATE_MODEL.IAT_MANAGEMENT.INVENTORY_COUNT_VOUCHER}
                />
              ) : (
                <PrintMultipleFormDialog
                  t={t}
                  i18n={props.item?.i18n}
                  handleClose={props.handleDialogClose}
                  open={props.item?.shouldOpenPrintDialog}
                  item={dataView || props.item?.item}
                  title={t("InventoryCountVoucher.printTitle")}
                  urls={[
                    ...INVENTORY_COUNT.GENERAL,
                    ...(INVENTORY_COUNT[currentOrg?.printCode] || []),
                  ]}
                />
              )
            )}

            {props.item?.shouldOpenEditorDialog && (
              <InstrumentsToolsInventoryDialog
                t={t}
                i18n={props.item?.i18n}
                handleClose={props.handleDialogClose}
                open={props.item?.shouldOpenEditorDialog}
                handleOKEditClose={props.handleOKEditClose}
                item={props.item?.item}
              />
            )}

            {props.item?.shouldOpenInventoryAllDepartmentsDialog && (
              <InstrumentsToolsInventoryDepartmentDialog
                t={t}
                i18n={props.item?.i18n}
                handleClose={props.handleDialogClose}
                open={props.item?.shouldOpenInventoryAllDepartmentsDialog}
                handleOKEditClose={props.handleOKEditClose}
                item={props.item?.item}
              />
            )}

            {props.item?.shouldOpenConfirmationDialog && (
              <ConfirmationDialog
                title={t("general.confirm")}
                open={props.item?.shouldOpenConfirmationDialog}
                onConfirmDialogClose={props.handleDialogClose}
                onYesClick={props.handleConfirmationResponse}
                text={t("general.deleteConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}
          </div>
          <MaterialTable
            title={t("general.list")}
            data={props.item?.itemList}
            columns={props.columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              draggable: false,
              // selection: true,
              sorting: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor: itemAsset?.id === rowData?.id
                  ? "#ccc"
                  : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: "250px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              let listIds = [];
              // eslint-disable-next-line no-unused-expressions
              rows?.forEach((item) => listIds.push(item.id));
              props.setState({ ids: [...listIds] });
            }}
            onRowClick={(e, rowData) => {
              return props?.setItemState(rowData);
            }}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            component="div"
            labelRowsPerPage={t("general.rows_per_page")}
            count={props.item?.totalElements}
            rowsPerPage={props.item?.rowsPerPage}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            page={props.item?.page}
            backIconButtonProps={{
              "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page",
            }}
            onChangePage={props.handleChangePage}
            onChangeRowsPerPage={props.setRowsPerPage}
          />
        </Grid>
      </Grid>
    </>
  );
}

export default ComponentInventoryTable;
