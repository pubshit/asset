/* eslint-disable no-new-wrappers */
import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import Signature from "../FormCustom/Component/Signature";
import Title from "../FormCustom/Component/Title";
import NationalName from "../FormCustom/Component/NationalName";
import HeaderLeft from "../FormCustom/Component/HeaderLeft";
import {convertNumberPrice, convertNumberPriceRoundUp} from "app/appFunction";
import { appConst } from "app/appConst";

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  tableCollapse: {
    width:'100%', 
    border:'1px solid',  
    borderCollapse: 'collapse',
  },
  textAlign: {
    textAlign: 'center',
    fontWeight: 'bold',  
    padding: "4px",
  },
  bold: {
    fontWeight: 'bold',  
  },
  border: {
    border:'1px solid',
    padding: "4px",
  },
  alignCenter: {
    border:'1px solid',
    textAlign: 'center',
    padding: "4px",
  },
  alignRight: {
    border:'1px solid',
    textAlign: 'right',
    padding: "4px",
  },
  alignRightOriginalCost: {
    border:'1px solid',
    textAlign: 'right',
    padding: "4px",
    minWidth: '76px',
    maxWidth: '80px',
  },
  alignCenterNumber: {
    border:'1px solid',
    textAlign: 'center',
    padding: "4px",
    minWidth: '60x',
    maxWidth: '70px',
  },
  code: {
    border:'1px solid',
    textAlign:'center',
    padding: "2px",
    minWidth:'100px'
  },
  date: {
    marginRight:'7%',
    textAlign:'right'
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px",
  },
  nameText: {
    textAlign: 'center',
    border:'1px solid',
    fontWeight: 'bold',  
    padding: "4px",
    minWidth: "130px",
  },
  w_33: {
    width: "33%",
  },
  d_flex: {
    display: "flex",
    paddingLeft: "20px",
  },
  a4Container: {
    margin: "auto",
    fontSize: "18px",
    // padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
  }
}

export default class InstrumentsToolsInventoryRoutes extends Component {
  state = {
    sumPrice : 0,
    sumCarryingAmount : 0,
  }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write("<style>" + `
    @media print {
      @page {
        margin: 7mm;
        size: landscape;
      }
    }` + "</style>");
    pri.document.write(content.innerHTML);
    
    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    this.setState({
      ...this.props.item
    }, function () {
    }
    );
  }
  componentDidMount() {
  }

  sumArray = (arr) => {
    let sum =  arr.reduce((a,b) => a + b);
    return  convertNumberPrice(sum);
  }
  sumArrayNumber = (arr) => {
    return arr.reduce((a,b) => a + b);
  }
  
  renderRole = (role) => {
    switch (role) {
      case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
        return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name
      case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
        return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name
      case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
        return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name
      default:
        break;
    }
  }
  render() {
    let { open, t, item } = this.props;
    let now = new Date(item?.inventoryCountDate);
    let arrAccountantQuantity = [];
    let arrAccountantOriginalCost = [];
    let arrInventoryQuantity = [];
    let arrInventoryOriginalCost = [];
    let arrDifferenceQuantity = [];
    let arrDifferenceOriginalCost = [];
    let arrCarryingAmountAccount = [];
    let arrCarryingAmountCheck = [];
    let arrDifferenceCarryingAmount = [];
    return (     
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth  >
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <span className="">{t('Phiếu kiểm kê')}</span>
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{height: '0px', width: '0px', position: 'absolute', print :{ size: 'auto',  margin: '0mm' }}}></iframe>
        
        <ValidatorForm className="validator-form-scroll-dialog" ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id='divcontents' className="dialog-print">
          <Grid container>
            <div className="form-print form-print-landscape" style={{ ...style.a4Container }}>
            <div
                  style={{ display: "flex", justifyContent: "space-between", ...style.bold }}
                >
                  <div item xs={5}>
                    <div>Đơn vị:.....................</div>
                    <div>Mã QHNS:..........................</div>
                  </div>
                  <div>Mẫu số: C53-HD</div>
                </div>
              <div style={{ textAlign: "center", marginBottom: 20  }}>
                <Title
                  title={"BIÊN BẢN KIỂM KÊ CÔNG CỤ DỤNG CỤ"}
                  date={new Date(item?.inventoryCountDate)}
                  timeOfInventory={"Thời điểm kiểm kê"}
                />
              </div>
              <div style={{marginBottom: "20px"}}>
                <div>Ban kiểm kê bao gồm: </div>
                <table style={{paddingLeft: 20}}>
                  <tbody>
                    {item?.inventoryCountPersons &&
                      item?.inventoryCountPersons?.map((person) => {
                        return (
                          <tr style={{ marginBottom: 5 }}>
                            <td style={{ minWidth: "260px" }}>
                                Ông/ Bà: {person?.personName || "......................................"}.
                            </td>
                            <td style={{ paddingLeft: "10px", minWidth:"290px" }}>
                                Chức vụ: {person?.position || ".................................................."}
                            </td>
                            <td style={{ paddingLeft: "10px", minWidth: "350px" }}>
                                Đại diện: {person?.departmentName || "..............................................................."}
                            </td>
                            <td style={{ paddingLeft: "10px", minWidth: "85px" }}>
                              {this.renderRole(person?.role)}
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
              </div>
              <div>
                <div>Đã kiểm kê kho có những công cụ dụng cụ dưới đây:</div>
                <div>
                <table style={style.tableCollapse} className="table-report">
                  <tr >
                    <th style={style.border} rowSpan="2">STT</th>
                    <th style={style.border} rowSpan="2">{t("InstrumentsToolsInventory.name")}</th>
                    <th style={style.border} rowSpan="2">{t("InstrumentsToolsInventory.code")}</th>
                    <th style={style.nameText} rowSpan="2">{t("InventoryCountVoucher.placeOfUse")}</th>
                    <th style={style.alignCenter} colSpan="3">{t("InventoryCountVoucher.accordingAccountingBooks")}</th>    
                    <th style={style.alignCenter} colSpan="3">{t("InventoryCountVoucher.accordingInventory")}</th>
                    <th style={style.alignCenter} colSpan="3">{t("InventoryCountVoucher.deviant")}</th>
                    <th style={style.border} rowSpan="2">Ghi chú</th>
                  </tr>
                  <tr>
                  <th style={{...style.alignCenter, ...style.border}}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{...style.alignCenter, ...style.border}}>Nguyên giá</th>
                        <th style={{...style.alignCenter, ...style.border}}>GTCL</th>
                        <th style={{...style.alignCenter, ...style.border}}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{...style.alignCenter, ...style.border}}>Nguyên giá</th>
                        <th style={{...style.alignCenter, ...style.border}}>GTCL</th>
                        <th style={{...style.alignCenter, ...style.border}}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{...style.alignCenter, ...style.border}}>Nguyên giá</th>
                        <th style={{...style.alignCenter, ...style.border}}>GTCL</th>
                  </tr>

                  {item.assets ? item.assets.map((row, index) => {
                    let {
                      accountantQuantity,
                      accountantOriginalCost,
                      accountantCarryingAmount,
                      inventoryQuantity,
                      inventoryOriginalCost,
                      inventoryCarryingAmount,
                    } = row;
                    let carryAmount = inventoryCarryingAmount - accountantCarryingAmount;
                    let originalCost = inventoryOriginalCost - accountantOriginalCost;
                    // inventoryCarryingAmount - accountantCarryingAmount;
                    let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
                    let totalQuantity = inventoryQuantity - accountantQuantity;
                    arrAccountantOriginalCost.push(
                      accountantOriginalCost
                    );
                    arrAccountantQuantity.push(accountantQuantity);
                    arrInventoryOriginalCost.push(
                      inventoryOriginalCost
                    );
                    arrInventoryQuantity.push(inventoryQuantity);
                    arrDifferenceQuantity.push(totalQuantity);
                    arrDifferenceOriginalCost.push(totalOriginalCost);
                    arrCarryingAmountAccount.push(
                      accountantCarryingAmount
                    );
                    arrCarryingAmountCheck.push(
                      inventoryCarryingAmount
                    );
                    arrDifferenceCarryingAmount.push(carryAmount);

                    return (
                      <tbody style={{fontSize: "14px"}}>
                        <tr>
                          <td style={{...style.alignCenter, ...style.border, textAlign: "center"}}>{index + 1}</td>
                          <td style={style.code}>
                            {row?.asset?.name}
                          </td>
                          <td style={style.code}>{row.asset?.code}</td>
                          <td style={style.border}>
                            {row.asset?.useDepartment?.name}
                          </td>

                          {/* Theo kế toán */}
                          <td style={style.alignCenterNumber}>{accountantQuantity}</td>
                          <td style={style.alignRightOriginalCost}>
                            {convertNumberPrice(Math.ceil(accountantOriginalCost))}
                          </td>
                          <td style={style.alignRightOriginalCost}>
                            {convertNumberPrice(accountantCarryingAmount)}
                          </td>

                          {/* Theo kiểm kê */}
                          <td style={style.alignCenterNumber}>{inventoryQuantity}</td>
                          <td style={style.alignRightOriginalCost}>
                            {convertNumberPrice(Math.ceil(inventoryOriginalCost))}
                          </td>
                          <td style={style.alignCenterNumber}>
                            {convertNumberPrice(inventoryCarryingAmount)}
                          </td>

                          {/* NG chênh lệch*/}
                          <td style={style.alignCenterNumber}>
                            {convertNumberPrice(totalQuantity)}
                          </td>
                          <td style={style.alignRightOriginalCost}>
                            {convertNumberPrice(Math.ceil(originalCost))}
                          </td>
                          <td style={style.alignRightOriginalCost}>
                            {convertNumberPrice(carryAmount)}
                          </td>
                          <td style={{...style.border}}>{row?.note}</td>
                        </tr>
                      </tbody>
                    )
                  }) : ''}
                      <tr>
                        <th colSpan="4">Tổng cộng</th>
                        <th style={style.alignCenter}>{this.sumArrayNumber(arrAccountantQuantity)}</th>
                        <th style={style.alignRight}>{this.sumArray(arrAccountantOriginalCost)}</th>
                        <th style={style.alignRight}>
                          {this.sumArray(arrCarryingAmountAccount)}
                        </th>
                        <th style={style.alignCenter}>{this.sumArrayNumber(arrInventoryQuantity)}</th>
                        <th style={style.alignRight}>{this.sumArray(arrInventoryOriginalCost)}</th>
                        <th style={style.alignRight}>
                          {this.sumArray(arrCarryingAmountCheck)}
                        </th>
                        <th style={style.alignCenter}>
                          {convertNumberPrice(
                            this.sumArrayNumber(arrDifferenceQuantity)
                          )}
                        </th>
                        <th style={style.alignRight}>
                          {convertNumberPrice(
                            this.sumArrayNumber(arrInventoryOriginalCost) -
                            this.sumArrayNumber(arrAccountantOriginalCost)
                          )}
                        </th>
                        <th style={style.alignRight}>
                          {convertNumberPrice(
                            this.sumArrayNumber(arrCarryingAmountCheck) -
                            this.sumArrayNumber(arrCarryingAmountAccount)
                          )}
                        </th>
                      </tr>
                </table>
              </div>

              <div style={{marginTop: "10px"}}>
                    Ý kiến giải quyết số chênh lệch ..................................................................................................................................................................................
              </div>

              </div>
              <div style={style.signature}>
                  <Signature
                    title="Thủ trưởng đơn vị"
                    sign="Ký, họ tên, đóng dấu"
                  />
                  <Signature title="Kế toán trưởng" sign="Ký, họ tên" />
                  <Signature
                    title="Trưởng ban kiểm kê"
                    sign="Ký, họ tên"
                  // date={new Date(item?.inventoryCountDate)}
                  />
                </div>
            </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">              
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>         
        </ValidatorForm>        
      </Dialog>
    );
  }
}

