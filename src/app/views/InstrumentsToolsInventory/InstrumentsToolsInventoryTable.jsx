import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountByStatus,
  getAssetByVouchers, getPrintItems, getPrintDetail,
} from "./InstrumentsToolsInventoryService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { appConst, LIST_ORGANIZATION, variable } from "app/appConst";
import AppContext from "app/appContext";
import ComponentInventoryTable from "./ComponentInventoryTable";
import localStorageService from "app/services/localStorageService";
import ListAssetInstrumentsToolsInventoryTable from "./ListAssetInstrumentsToolsInventoryTable";
import {
  convertFromToDate,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate
} from "app/appFunction";
import { LightTooltip, TabPanel } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const hasDeletePermission = props.hasDeletePermission;
  const hasEditPermission = props.hasEditPermission;

  return (
    <div className="none_wrap">
      {hasEditPermission &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder,
        ].includes(item?.inventoryCountStatus?.indexOrder)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
        ].includes(item?.inventoryCountStatus?.indexOrder)
        && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InstrumentsToolsInventoryTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationExcel: false,
    shouldOpenPrintDialog: false,
    hasEditPermission: false,
    hasDeletePermission: false,
    hasCreatePermission: false,
    isSearch: false,
    inventoryCountDateBottom: null,
    inventoryCountDateTop: null,
    ids: [],
    tabValue: 0,
    itemAsset: {},
    checkAll: false,
    openAdvanceSearch: false,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = 2; //Điều chuyển

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabInventory?.tabAll === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder: null,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabPlan === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[0].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabTakingInventory === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[1].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabInventory === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[2].indexOrder,
        },
        () => this.updatePageData()
      );
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { selectedList } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {};
      searchObject.type = appConst.assetTypeInventoryCountObject.DEPARTMENT.code;
      searchObject.keyword = this.state.keyword.trim();
      searchObject.inventoryCountDateBottom = convertFromToDate(this.state.inventoryCountDateBottom).fromDate;
      searchObject.inventoryCountDateTop = convertFromToDate(this.state.inventoryCountDateTop).toDate;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.assetClass = appConst.assetClass.CCDC;
      searchObject.inventoryCountStatusIndexOrder =
        this.state?.inventoryCountStatusIndexOrder;
      searchObject.departmentId = this.state?.department?.id;

      let res = await searchByPage(searchObject);
      const { data, code } = res?.data;

      if (isSuccessfulResponse(code)) {
        if (!data?.content?.length) {
          return this.setPage(0);
        }
        let countItemChecked = 0
        const itemList = data?.content?.map(item => {
          if (selectedList?.find(checkedItem => checkedItem === item.id)) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
          }
        })

        this.setState({
          itemAsset: null,
          itemList,
          totalElements: data?.totalElements,
          ids: [],
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(this.props.t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrintDialog: false,
      shouldOpenConfirmationExcel: false,
      shouldOpenInventoryAllDepartmentsDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationExcel: false,
      inventoryCountDateBottom: null,
      inventoryCountDateTop: null,
      shouldOpenInventoryAllDepartmentsDialog: false,
      keyword: "",
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info("Xoá thành công.");
          this.search();
          this.getCountStatus();
          this.handleDialogClose();
        } else {
          toast.warning(data.message);
        }
      })
      .catch((error) => {
        toast.error(this.props.t("general.error"));
      });
  };

  componentDidMount() {
    this.setState({
      inventoryCountStatusIndexOrder: null,
    }, () => {
      this.updatePageData();
      this.getRoleCurrentUser();
      this.getCountStatus();
    }
    );
  }

  getRoleCurrentUser = () => {
    let { hasDeletePermission, hasEditPermission, hasCreatePermission } =
      this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser?.roles?.forEach((role) => {
        //user
        if (role.name === ConstantList.ROLES.ROLE_USER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = false;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = false;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = false;
          }
        }
        // người quản lý vật tư
        if (role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = true;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = true;
          }
        }
        // người đại diện phòng ban
        if (role.name === ConstantList.ROLES.ROLE_ASSET_USER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = false;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = false;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = false;
          }
        }
        // admin hoặc supper_admin
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = true;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = true;
          }
        }
      });
      this.setState(
        {
          hasDeletePermission,
          hasEditPermission,
          hasCreatePermission,
          orgName: currentUser?.org?.name,
        },
        () => {
        }
      );
    }
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  validateAsset = () => {

    const { t, i18n } = this.props;
    let {
      selectedList
    }
      = this.state;

    if (selectedList <= 0) {
      toast.warning(t("InstrumentsToolsInventory.messages.noItemSelected"));
      return false;
    }
    return true;
  }

  convertDataAsset = (value) => {
    value?.map((asset, index) => {
      let differenceQuantity = (
        asset?.inventoryQuantity - asset?.accountantQuantity
      );
      let differenceOriginalCost = (
        (asset?.inventoryQuantity * asset?.inventoryOriginalCost) - (asset?.accountantQuantity * asset?.accountantOriginalCost)
      );
      asset.assetsId = asset?.assetId;
      asset.asset = {
        ...asset,
        name: asset?.assetName,
        code: asset?.assetCode,
        status: {
          name: asset?.updateStatusName,
          id: asset?.updateStatusId,
          indexOrder: asset?.updateStatusIndexOrder
        }
      };
      asset.status = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };
      asset.currentStatus = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };

      asset.assetId = asset?.assetId;
      asset.differenceOriginalCost = differenceOriginalCost;
      asset.differenceQuantity = differenceQuantity;
      asset.price = (
        asset?.accountantOriginalCost / asset?.accountantQuantity
      );
      asset.storeId = asset?.storeId;
      asset.store = {
        name: asset?.storeName,
        id: asset?.storeId
      };
      asset.isStore = [
        appConst.listStatusAssets.NHAP_KHO.indexOrder,
        appConst.listStatusAssets.LUU_MOI.indexOrder,
        appConst.listStatusAssets.DA_HONG.indexOrder,
      ].includes(asset?.updateStatusIndexOrder)
    });
    return value;
  }


  handleEditItemInventoryAllDepartment = async () => {
    let { setPageLoading } = this.context;
    const { t } = this.props;
    try {
      setPageLoading(true);
      if (!this.validateAsset()) return;
      const data = await getAssetByVouchers(this.state?.selectedList);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          item: {
            isView: false,
            assets: this.convertDataAsset(data?.data?.data || []),
          },
          shouldOpenInventoryAllDepartmentsDialog: true,
        });
      }
    } catch (error) {
      console.log(error)
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };


  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    item.checked = !item?.checked;

    let selectAllItem = true;
    for (let i = 0; i < AssetTransfer?.length; i++) {
      if (!AssetTransfer[i].checked) {
        selectAllItem = false;
      }
      if (AssetTransfer[i]?.id === item?.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };
  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (let i = 0; i < AssetTransfer?.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (let i = 0; i < list?.length; i++) {
      await deleteItem(list[i]?.id);
    }
  }

  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };
  handleExcel = () => {
    this.setState({
      shouldOpenConfirmationExcel: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = this.state.inventoryCountDateBottom;
    searchObject.inventoryCountDateTop = this.state.inventoryCountDateTop;
    searchObject.ids = this.state?.selectedList;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.departmentId = this.state?.department?.id;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;
    searchObject.type = appConst.assetTypeInventoryCountObject.DEPARTMENT.code;

    exportToExcel(searchObject)
      .then((res) => {
        setPageLoading(false);
        this.setState({ ids: [] });
        toast.info(this.props.t("general.successExport"));
        this.handleOKEditClose();
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "InventoryCountVoucherCCDC.xlsx");
      })
      .catch((err) => {
        setPageLoading(false);
        toast.error(this.props.t("general.failExport"));
      });
  };

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  handleDateChange = (date, name) => {
    this.setState(
      {
        [name]: date,
      },
      () => { isValidDate(date) && this.search() }
    );
  };

  handleEditData = (inventoryCountVoucher) => {
    let inventoryCountPersonIds = [];
    let userIds = [];

    inventoryCountVoucher?.assets?.map((asset, index) => {
      let differenceQuantity = (
        asset?.inventoryQuantity - asset?.accountantQuantity
      );
      let differenceOriginalCost = (
        asset?.inventoryOriginalCost - asset?.accountantOriginalCost
      );

      asset.assetId = asset?.asset?.id;
      asset.differenceOriginalCost = differenceOriginalCost;
      asset.differenceQuantity = differenceQuantity;
      asset.price = (
        asset?.accountantOriginalCost / asset?.accountantQuantity
      ) || asset?.asset?.originalCost;
      asset.storeId = asset?.store?.id;
      asset.isStore =
        appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder ||
        appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder;
    });

    inventoryCountVoucher.assets?.sort((a, b) =>
      (b.asset?.code).localeCompare(a.asset?.code)
    );
    inventoryCountVoucher?.inventoryCountPersons?.map((item) => {
      inventoryCountPersonIds.push(item?.person?.id);
      userIds.push(item?.person?.userId);
    });

    inventoryCountVoucher = {
      ...inventoryCountVoucher,
      assetId: inventoryCountVoucher?.asset?.id,
      departmentId: inventoryCountVoucher?.department?.id,
      inventoryCountPersonIds: inventoryCountPersonIds,
      userIds: userIds,
      inventoryCountStatusId: inventoryCountVoucher?.inventoryCountStatus?.id,
    };

    this.setState({
      item: inventoryCountVoucher,
      shouldOpenEditorDialog: true,
    });
  };
  setStateInventory = (item) => {
    this.setState(item);
  };
  handleSetItemState = async (rowData) => {
    this.setState({
      itemAsset: rowData,
    });
  };
  checkStatus = (status) => {
    switch (status) {
      case appConst.listStatusInventory[0].indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusInventory[0].name}
          </span>
        );
      case appConst.listStatusInventory[1].indexOrder:
        return (
          <span className="status status-warning">
            {appConst.listStatusInventory[1].name}
          </span>
        );
      case appConst.listStatusInventory[2].indexOrder:
        return (
          <span className="status status-error">
            {appConst.listStatusInventory[2].name}
          </span>
        );

      default:
        break;
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let searchObj = {
      type: 1,
      assetClass: appConst.assetClass.CCDC
    }
    getCountByStatus(searchObj)
      .then(({ data }) => {
        let countStatusNew,
          countStatusTakingInventory,
          countStatusInventory;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (
            appConst?.listStatusInventoryObject.NEW.indexOrder ===
            item?.trangThai
          ) {
            countStatusNew = this.checkCount(item?.soLuong);

            return;
          }
          if (
            appConst?.listStatusInventoryObject.DANG_KIEM_KE.indexOrder ===
            item?.trangThai
          ) {
            countStatusTakingInventory = this.checkCount(item?.soLuong);
            return;
          }
          if (
            appConst?.listStatusInventoryObject.DA_KIEM_KE.indexOrder ===
            item?.trangThai
          ) {
            countStatusInventory = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusNew,
            countStatusTakingInventory,
            countStatusInventory,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };
  handleChangeDepartment = (value, name) => {
    this.setState({ [name]: value }, () => { this.search() });
  }

  convertListAssetPrint = (data = []) => {
    return data.map(item => {
      return {
        ...item,
        asset: {
          ...item,
          code: item?.assetCode,
          name: item?.assetName,
        }
      }
    })
  };

  handleGetBVCPrintData = async (rowData) => { // todo: get data for only bvc thai nguyen
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { organization } = getUserInformation();

    try {
      setPageLoading(true);
      let params = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        assetClass: appConst.assetClass.CCDC,
      }
      let item = {
        orgName: organization?.org?.name,
      };

      let resItems = await getPrintItems(rowData?.id, params);
      let resDetail = await getPrintDetail(rowData?.id);

      if (isSuccessfulResponse(resDetail?.data?.code)) {
        item = {
          ...item,
          ...resDetail?.data?.data,
        }
      } else {
        handleThrowResponseMessage(resDetail);
      }

      if (isSuccessfulResponse(resItems?.data?.code)) {
        item = {
          ...item,
          assets: this.convertListAssetPrint(resItems?.data?.data?.content),
        }
      } else {
        handleThrowResponseMessage(resItems);
      }
      this.setState({
        item,
        shouldOpenPrintDialog: true,
      });
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleGetGeneralPrintData = async (rowData) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      let res = await getItemById(rowData.id);
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        let inventoryCountVoucher = data || {};
        inventoryCountVoucher.orgName = this.state?.orgName;
        this.setState({
          item: inventoryCountVoucher,
          shouldOpenPrintDialog: true,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handlePrint = async rowData => {
    let { currentOrg } = this.context;

    if (currentOrg.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code) {
      await this.handleGetBVCPrintData(rowData);
    } else {
      await this.handleGetBVCPrintData(rowData);
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      tabValue,
      checkAll
    } = this.state;
    let TitlePage = t("InstrumentsToolsInventory.inventoryCountVoucher");

    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        // width: "120px",
        maxWidth: 100,
        align: "left",
        sorting: false,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasEditPermission={hasEditPermission}
            hasDeletePermission={hasDeletePermission}
            onSelect={(rowData, method) => {
              let { setPageLoading } = this.context;
              if (appConst.active.edit === method) {
                setPageLoading(true);
                getItemById(rowData.id).then(({ data }) => {
                  setPageLoading(false);
                  let inventoryCountVoucher = data?.data ? data?.data : {};
                  inventoryCountVoucher.isView = false;
                  this.handleEditData(inventoryCountVoucher);
                });
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.view === method) {
                setPageLoading(true);
                getItemById(rowData.id).then(({ data }) => {
                  setPageLoading(false);
                  let inventoryCountVoucher = data?.data ? data?.data : {};
                  inventoryCountVoucher.isView = true;
                  this.handleEditData(inventoryCountVoucher);
                });
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.stt"),
        field: "",
        width: "50px",
        sorting: false,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("InventoryCountVoucher.inventoryCountDate"),
        sorting: false,
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.inventoryCountDate ? (
            <span>
              {moment(rowData.inventoryCountDate).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryCountVoucher.status"),
        field: "inventoryCountStatus.name",
        width: "150",
        align: "left",
        sorting: false,
        headerStyle: {
          textAlign: "left",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          this.checkStatus(rowData?.inventoryCountStatus?.indexOrder),
      },
      {
        title: t("InventoryCountVoucher.voucherName"),
        field: "title",
        minWidth: 150,
        align: "left",
        sorting: false,
      },
      {
        title: t("InventoryCountVoucher.department"),
        field: "department.name",
        minWidth: 150,
        align: "left",
        sorting: false,
        headerStyle: {
          textAlign: "left",
        },
        cellStyle: {
          textAlign: "left",
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/inventory-count-vouchers",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("InventoryCountVoucher.tabAll")} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabNew")}</span>
              <div className="tabQuantity tabQuantity-success">
                {this.state?.countStatusNew || 0}
              </div>
            </div>} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabTakingInventory")}</span>
              <div className="tabQuantity tabQuantity-warning">
                {this.state?.countStatusTakingInventory || 0}
              </div>
            </div>} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabInventory")}</span>
              <div className="tabQuantity tabQuantity-error">
                {this.state?.countStatusInventory || 0}
              </div>
            </div>} />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabAll}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleChangeDepartment={this.handleChangeDepartment}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryTable
            {...this.props}
            itemAsset={this.state.itemAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabPlan}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleChangeDepartment={this.handleChangeDepartment}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryTable
            {...this.props}
            itemAsset={this.state.itemAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabTakingInventory}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleChangeDepartment={this.handleChangeDepartment}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryTable
            {...this.props}
            itemAsset={this.state.itemAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabInventory}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleChangeDepartment={this.handleChangeDepartment}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryTable
            {...this.props}
            itemAsset={this.state.itemAsset}
          />
        </TabPanel>
      </div>
    );
  }
}
InstrumentsToolsInventoryTable.contextType = AppContext;
export default InstrumentsToolsInventoryTable;
