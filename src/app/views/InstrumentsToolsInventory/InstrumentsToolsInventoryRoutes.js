import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const InstrumentsToolsInventoryTable = EgretLoadable({
  loader: () => import("./InstrumentsToolsInventoryTable")
});
const ViewComponent = withTranslation()(InstrumentsToolsInventoryTable);

const InstrumentsToolsInventoryRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "instruments-and-tools/inventory-count-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default InstrumentsToolsInventoryRoutes;