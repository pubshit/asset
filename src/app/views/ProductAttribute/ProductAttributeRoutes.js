import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const ProductAttributeTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./ProductAttributeTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(ProductAttributeTable);

const productAttributeRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/product_attribute",
    exact: true,
    component: ViewComponent
  }
];

export default productAttributeRoutes;
