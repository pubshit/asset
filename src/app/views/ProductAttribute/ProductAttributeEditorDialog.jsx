import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid, DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkCode, addNewProductAttribute, updateProductAttribute } from "./ProductAttributeService";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { handleKeyDownNameSpecialExcept } from "app/appFunction";
import {appConst, variable} from "../../appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class ProductAttributeEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    product: null,
    // isCheck: true
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    let {t} = this.props;
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(t('CommonObjectType.noti.dupli_code'));
        
        // alert("Code đã được sử dụng");
      } else {
        //Nếu trả về false là code chưa sử dụng có thể dùng
        if (id) {
          updateProductAttribute({
            ...this.state
          }).then(() => {
            toast.info(this.props.t('Product.noti.updateSuccess'));
            this.props.handleOKEditClose();
          });
        } else {
          addNewProductAttribute({
            ...this.state
          }).then(() => {
            toast.info('Thêm mới thành công.');
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  selectProduct = (product) => {
    this.setState({ product: product }, function () {
    });
  }

  componentWillMount() {
    let { item } = this.props;
    this.setState(item);
  }
  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }
  render() {
    let {
      name,
      code,
      shouldOpenNotificationPopup
    } = this.state;
    let { open, t } = this.props;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t('ProductAttribute.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" id="productAttr" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"                  
                  label={<span><span className="colorRed">*</span>{t('ProductAttribute.code')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />                
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span><span className="colorRed">*</span>{t('ProductAttribute.name')}</span>}
                  onChange={this.handleChange}
                  onKeyDown={(e) => {
                    handleKeyDownNameSpecialExcept(e)
                  }}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-15"
                type="submit"
              >
                {t('general.save')}
              </Button>
              
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default ProductAttributeEditorDialog;
