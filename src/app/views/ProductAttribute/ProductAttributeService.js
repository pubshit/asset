import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_ProductAttribute = ConstantList.API_ENPOINT + "/api/productAttribute";

export const getAllProductAttributes = () => {
  return axios.get(API_PATH_ProductAttribute + "/1/1000000");
};

export const searchByPage = productAttribute => {
  return axios.post(API_PATH_ProductAttribute + "/searchByDto", productAttribute);
};

export const deleteItem = id => {
  return axios.delete(API_PATH_ProductAttribute + "/" + id);
};

export const getProductAttributeById = id => {
  return axios.get(API_PATH_ProductAttribute + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH_ProductAttribute + "/checkCode";
  return axios.get(url, config);
};

export const addNewProductAttribute = productAttribute => {
  return axios.post(API_PATH_ProductAttribute, productAttribute);
};
export const updateProductAttribute = productAttribute => {
  return axios.put(API_PATH_ProductAttribute + "/" + productAttribute.id, productAttribute);
};

export const deleteCheckItem = id => {
  return axios.delete(API_PATH_ProductAttribute  + "/delete/" + id);
};