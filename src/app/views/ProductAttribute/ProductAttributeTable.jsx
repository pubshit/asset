import React, { Component } from "react";
import {
  IconButton,
  Grid,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { searchByPage, getProductAttributeById, deleteCheckItem } from "./ProductAttributeService";
import ProductAttributeEditorDialog from "./ProductAttributeEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from 'react-i18next';
import { saveAs } from 'file-saver';
import { Helmet } from 'react-helmet';
import { withStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router-dom";
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import localStorageService from "app/services/localStorageService";
import ConstantList from "../../appConfig";
import AppContext from "app/appContext";
import { withRouter } from "react-router-dom";
import { appConst } from "app/appConst";
import { defaultPaginationProps } from "app/appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
})
const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: '-1.5em'
  }
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { hasDeletePermission } = props
  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    {
      hasDeletePermission &&
      <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
        PopperProps={{
          popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
        }} >
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">delete</Icon>
        </IconButton>
      </LightTooltip>
    }
  </div>;
}

class ProductAttributeTable extends Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    productAttributeList: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    Notification: "",
    keyword: '',
    hasDeletePermission: false,
  };
  numSelected = 0;
  rowCount = 0;
  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageData();
    })
  };

  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
    })
  };

  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.search();
    }
  };

  handleKeyUp = e => {
    if (!e.target.value) {
      this.search();
    }
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchByPage(searchObject, this.state.page, this.state.rowsPerPage).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      });
    });
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      var searchObject = {};
      searchObject.keyword = this.state.keyword?.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      const data = await searchByPage(searchObject, this.state.page, this.state.rowsPerPage);
      if (data?.status === appConst.CODE.SUCCESS) {
        this.setState({ itemList: [...data?.data?.content], totalElements: data?.data?.totalElements })
      }
    } catch (error) {
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  handleDeleteProductAttribute = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditProductAttribute = item => {
    getProductAttributeById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props
    deleteCheckItem(this.state.id).then((res) => {
      if (res?.data) {
        toast.info(t('Xoá thành công.'));
      } else {
        toast.warning(t('ProductAttribute.noti.use'));
      }
      this.updatePageData();
      this.handleDialogClose()
    }).catch((err) => {
      toast.warning(t('ProductAttribute.noti.use'));
    })
  };

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser()
  }

  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleClick = (event, item) => {
    let { productAttributeList } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < productAttributeList.length; i++) {
      if (productAttributeList[i].checked == null || productAttributeList[i].checked == false) {
        selectAllItem = false;
      }
      if (productAttributeList[i].id == item.id) {
        productAttributeList[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, productAttributeList: productAttributeList });

  };
  handleSelectAllClick = (event) => {
    let { productAttributeList } = this.state;
    for (var i = 0; i < productAttributeList.length; i++) {
      productAttributeList[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, productAttributeList: productAttributeList });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser.roles.forEach((role) => {
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
      });
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenNotificationPopup,
      hasDeletePermission
    } = this.state;
    let TitlePage = t("ProductAttribute.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        maxWidth: 80,
        cellStyle: {
          textAlign: 'center'
        },
        render: rowData => <MaterialButton item={rowData}
          hasDeletePermission={hasDeletePermission}
          onSelect={(rowData, method) => {
            if (method === 0) {
              getProductAttributeById(rowData.id).then(({ data }) => {
                if (data.parent === null) {
                  data.parent = {};
                }
                this.setState({
                  item: data,
                  shouldOpenEditorDialog: true
                });
              })
            } else if (method === 1) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      { title: t("ProductAttribute.code"), field: "code", align: "left" },
      { title: t("ProductAttribute.name"), field: "name" },

    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.category"), path: "/list/product_attribute" },
            { name: TitlePage }]} />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item container md={3} xs={12} >
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({ startDate: new Date(), endDate: new Date() });
              }
              }
            >
              {t('general.add')}
            </Button>
          </Grid>
          <Grid item md={6} sm={12} xs={12} >
            <FormControl fullWidth>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("ProductAttribute.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#"> <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <ProductAttributeEditorDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenNotificationPopup && (
                <NotificationPopup
                  title={t('general.noti')}
                  open={shouldOpenNotificationPopup}
                  // onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDialogClose}
                  text={t(this.state.Notification)}
                  agree={t('general.agree')}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '465px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

ProductAttributeTable.contextType = AppContext;
export default withRouter(ProductAttributeTable);