import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Icon,
  InputLabel,
  Radio,
  RadioGroup,
  IconButton, TableHead, TableContainer, TableBody, TableCell,
} from "@material-ui/core";
import { STATUS_REPORT, appConst, formatDate } from "app/appConst";
import AppContext from "app/appContext";
import { convertNumberPriceRoundUp, isSuccessfulResponse } from "app/appFunction";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkTotalCost,
  getDetailArSuCo,
} from "../InstrumentsToolsRepairingService";
import { Table, Td, Th, Tr, } from "./CustomTable";
import NumberFormatCustom from "../../Component/Utilities/NumberFormatCustom";
import ValidatePicker from "app/views/Component/ValidatePicker/ValidatePicker";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function AssetAcceptanceForm(props) {
  let {
    t,
    itemEdit,
    setDataNghiemThuSuCo,
    totalCost,
    setTotalCost,
    disable,
    setIsSavedAcceptanceTab,
    permission = {},
  } = props;
  let {setPageLoading} = useContext(AppContext);
  const [dataLinhKienVatTu, setDataLinhKienVatTu] = useState([]);
  const [dataItemEdit, setDataItemEdit] = useState({});
  const [dataNghiemThus, setDataNghiemThus] = useState([
    { arNtHangMuc: "", arNtKetLuan: "" },
  ]);
  const [dataForm, setDataForm] = useState({});
  const [dayFinish, setDayFinish] = useState(moment().format(formatDate));
  const [arTinhTrang, setArTinhTrang] = useState("");
  const [arYeuCauXuLy, setArYeuCauXuLy] = useState("");
  const [arTsTinhTrangSd, setArTsTinhTrangSd] = useState("")
  const [arYeuCauXuLyText, setArYeuCauXuLyText] = useState("");
  const [arChiPhi, setArChiPhi] = useState("");
  const [isExceed, setIsExceed] = useState(false);

  useEffect(() => {
    getListLinhKienVatTu();
  }, []);
  useEffect(() => {
    setDataNghiemThuSuCo({
      ...dataItemEdit,
      ...dataForm,
      arNtThoiGian: dayFinish,
      arNtTinhTrang: arTinhTrang,
      arYeuCauXuLy: arYeuCauXuLy,
      arLinhKienVatTus: dataLinhKienVatTu,
      arAttachments: [],
      arNtTsTinhTrangSd: arTsTinhTrangSd
    });
  }, [dataForm, dayFinish, arTinhTrang, arYeuCauXuLy, dataLinhKienVatTu, arTsTinhTrangSd]);

  useEffect(() => {
    disable && isCheckTotalCost();
  }, [totalCost, disable]);

  useEffect(() => {
    if (dataItemEdit.arNtTinhTrang) {
      setArTinhTrang(dataItemEdit.arNtTinhTrang)
    }
    if (dataItemEdit?.arNtTsTinhTrangSd) {
      setArTsTinhTrangSd(dataItemEdit.arNtTsTinhTrangSd)
    }
  }, [dataItemEdit.arNtTinhTrang, dataItemEdit.arNtTsTinhTrangSd])

  const getListLinhKienVatTu = async () => {
    setPageLoading(true);
    try {
      setPageLoading(true);
      const res = await getDetailArSuCo(itemEdit.id);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setArChiPhi(data?.arChiPhi);
        setDataItemEdit(data);
        setDataLinhKienVatTu(data?.arLinhKienVatTus)
        setTotalCost(
          data?.arLinhKienVatTus?.reduce((acc, item) => Number(acc) + Number(item.productTotal), 0)
          + data?.arChiPhi
        );
        setTotalCost(data?.arTongChiPhi);
      }
      // }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const handleChangeArChiPhi = (e) => {
    setArChiPhi(Number(e.target.value));
    setDataForm({
      ...dataForm,
      [e.target.name]: Number(e.target.value),
      arNghiemThus: dataNghiemThus,
    });
    setTotalCost(
      dataLinhKienVatTu?.reduce((acc, item) => Number(acc) + Number(item.productTotal), 0) +
      Number(e.target.value)
    );
  };

  const handleChangeArYeuCauXuLyText = (e) => {
    setArYeuCauXuLyText(e.target.value);
    setDataForm({
      ...dataForm,
      [e.target.name]: e.target.value,
    });
  };

  const handleDeleteHangMucNt = (i) => {
    if (dataNghiemThus?.length === 1) return;
    //eslint-disable-next-line
    dataNghiemThus?.splice(i, 1);
    setDataForm({
      ...dataForm,
      arNghiemThus: dataNghiemThus,
    });
  };

  const handleChangeDataNghiemThu = (e, i) => {
    for (let index = 0; index <= i; index++) {
      dataNghiemThus[i] = {
        arNtHangMuc:
          e.target.name === "arNtHangMuc"
            ? e.target.value
            : dataNghiemThus[i].arNtHangMuc,
        arNtKetLuan:
          e.target.name === "arNtKetLuan"
            ? e.target.value
            : dataNghiemThus[i].arNtKetLuan,
      };
    }
    setDataForm({
      ...dataForm,
      arChiPhi: Number(arChiPhi),
      arNghiemThus: dataNghiemThus,
    });
    setIsSavedAcceptanceTab(true)
  };

  const handleChangeDate = (date, name) => {
    setDayFinish(moment(date).format(formatDate));
  };

  const isCheckTotalCost = async () => {
    try {
      const result = await checkTotalCost({
        idAsset: itemEdit?.tsId,
        totalCost: totalCost,
      });
      if (result?.data?.code === 200) {
        setIsExceed(result?.data?.data === false);
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  const handleDisplayText = () => {
    if (disable) {
      return dataItemEdit?.arNtTinhTrang ===
        appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code || arTinhTrang ===
        appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code ? (
        <p>* Thiết bị đang được {dataItemEdit?.arPhongBanText} sử dụng</p>
      ) : (
        <p>* Hỏng, chờ xử lý</p>
      );
    } else {
      return arTinhTrang === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code
        ? (
          <p className="colorRed">
            * Thiết bị chuyển về cho {dataItemEdit?.arPhongBanText} tiếp tục sử
            dụng
          </p>
        ) : arTinhTrang ===
          appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code ? (
          <p className="colorRed">* Thiết bị hỏng chờ xử lý</p>
        ) : (
          ""
        );
    }
  };

  return (
    <Grid
      container
      spacing={2}
      justifyContent="space-between"
      className="asset-acceptance-form"
    >
      <Grid container spacing={3}>
        <Grid item md={4} sm={12} xs={12}>
          <ValidatePicker
            id="mui-pickers-date"
            label={t("ReportIncident.AssetAcceptanceForm.finishDay")}
            onChange={(value) => handleChangeDate(value)}
            value={dataItemEdit?.arNtThoiGian || dayFinish}
            name={"finishDay"}
            readOnly={permission.arNtThoiGian.isView}
            minDate={dataItemEdit?.arTgXacNhan}
            maxDate={new Date()}
            minDateMessage={t("ReportIncident.AssetAcceptanceForm.minDateFinishError")}
            maxDateMessage={t("ReportIncident.AssetAcceptanceForm.maxDateFinishError")}
            disabled={disable || permission.arNtThoiGian.isDisabled}
          />
        </Grid>
      </Grid>

      <Grid
        container
        spacing={1}
        justifyContent="space-between"
        className="pb-40 pt-20 mt-20"
      >
        <InputLabel htmlFor="isManageAccountant">
          <span className="pl-5 pt-10 font-weight-600 font-size-16 text-black">
            {t("ReportIncident.RateIncidentForm.listOfSuppliesToBeUsed")}:
          </span>
        </InputLabel>
        {isExceed && (
          <div className="warning">
            {t("ReportIncident.AssetAcceptanceForm.confirmTotalCost")}{" "}
          </div>
        )}
      </Grid>
      <Grid
        item xs={12} className="m-0 px-0"
      >
        <TableContainer>
          <Table>
            <TableHead>
              <Tr className="MuiTableRow-head">
                <TableCell padding="6px 24px 6px 16px">{t("Asset.stt")}</TableCell>
                <TableCell padding="6px 24px 6px 16px">
                  {t("ReportIncident.AssetAcceptanceForm.nameOfMaterialsUsed")}
                </TableCell>
                <TableCell padding="6px 24px 6px 16px">
                  {t("ReportIncident.AssetAcceptanceForm.quantity")}
                </TableCell>
                <TableCell padding="6px 24px 6px 16px">
                  {t("ReportIncident.AssetAcceptanceForm.unitPrice")}
                </TableCell>
                <TableCell padding="6px 24px 6px 16px">
                  {t("ReportIncident.AssetAcceptanceForm.totalAmount")}
                </TableCell>
              </Tr>
            </TableHead>
            <TableBody>
              {dataLinhKienVatTu?.map((item, index) => (
                <Tr className="MuiTableRow-root" key={index}>
                  <Td textAlign="center" width={"50px"}>
                    {index + 1}
                  </Td>
                  <Td>{item.productName}</Td>
                  <Td textAlign="center">{item.productQty}</Td>
                  <Td textAlign="right" width={"250px"}>{item?.productCost ? convertNumberPriceRoundUp(item?.productCost) : 0}</Td>
                  <Td textAlign="right">{convertNumberPriceRoundUp(item.productTotal) || 0}</Td>
                </Tr>
              ))}
              {dataItemEdit?.arHinhThuc
                === appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code && (
                  <Tr className="MuiTableRow-root">
                    <Td colSpan={4}>
                      {t("ReportIncident.AssetAcceptanceForm.costOfRepairIncurred")}
                    </Td>
                    <Td textAlign="right">
                      {
                        <TextValidator
                          className="w-100"
                          onChange={(e) => handleChangeArChiPhi(e)}
                          type="text"
                          name="arChiPhi"
                          InputProps={{
                            inputComponent: NumberFormatCustom,
                            inputProps: {
                              className: "text-align-right",
                            },
                            readOnly: permission.arChiPhi.isView,
                          }}
                          value={arChiPhi || ""}
                          disabled={disable || permission.arChiPhi.isDisabled}
                          validators={[
                            "minNumber:0",
                            "maxNumber:999999999999999",
                          ]}
                          errorMessages={[
                            t("general.minNumberError"),
                            t("general.maxNumberError"),
                          ]}
                        />
                      }
                    </Td>
                  </Tr>
                )}
              <Tr className="MuiTableRow-root">
                <Td colSpan={4}>
                  {t("ReportIncident.AssetAcceptanceForm.totalCost")}
                </Td>
                <Td textAlign="right">
                  {convertNumberPriceRoundUp(totalCost)}
                </Td>
              </Tr>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      <Grid
        container
        spacing={1}
        justifyContent="space-between"
        alignItems="center"
        className="pb-10 pt-20 mt-30"
      >
        {/* <Grid md={6} sm={12} xs={12}> */}
        <InputLabel htmlFor="isManageAccountant">
          <span className="pl-5 pt-10 font-weight-600 font-size-16 text-black">
            {t("ReportIncident.AssetAcceptanceForm.informationOnAcceptance")}:
          </span>
        </InputLabel>
        {!disable && !permission.arNghiemThus.isView && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={permission.arNghiemThus.isDisabled}
            onClick={() => {
              setDataNghiemThus([
                ...dataNghiemThus,
                { arNtHangMuc: "", arNtKetLuan: "" },
              ]);
            }}
          >
            {t("general.addButton")}
          </Button>
        )}
      </Grid>

      {(dataItemEdit?.arNghiemThus?.length !== 0
          ? dataItemEdit?.arNghiemThus
          : dataNghiemThus
      )?.map((item, i) => (
        <div key={i} style={{ width: "100%" }}>
          <Grid container >
            <Grid item container md={1} sm={2} xs={2} justifyContent="center" alignItems="center">
              <IconButton
                size="small"
                onClick={() => handleDeleteHangMucNt(i)}
                disabled={disable || permission.arNghiemThus.isDisabled || permission.arNghiemThus.isView}
              >
                <Icon fontSize="small" color={
                  permission.arNghiemThus.isDisabled || permission.arNghiemThus.isView ? "default" : "error"
                }>
                  delete
                </Icon>
              </IconButton>
            </Grid>
            <Grid item container md={11} sm={10} xs={10} spacing={2}>
              <Grid item md={6} sm={12} xs={12}>
                <TextValidator
                  className="mt-12"
                  fullWidth
                  multiline
                  label={t("ReportIncident.AssetAcceptanceForm.category")}
                  InputProps={{
                    rows: 2,
                    readOnly: permission.arNghiemThus.isView,
                  }}
                  value={item?.arNtHangMuc ? item?.arNtHangMuc : ""}
                  onChange={(e) => {
                    handleChangeDataNghiemThu(e, i);
                  }}
                  variant="outlined"
                  name="arNtHangMuc"
                  disabled={disable || permission.arNghiemThus.isDisabled}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={6} sm={12} xs={12}>
                <TextValidator
                  className="mt-12"
                  fullWidth
                  multiline
                  label={t("ReportIncident.AssetAcceptanceForm.result")}
                  InputProps={{
                    rows: 2,
                    readOnly: permission.arNghiemThus.isView,
                  }}
                  value={item?.arNtKetLuan ? item?.arNtKetLuan : ""}
                  onChange={(e) => {
                    handleChangeDataNghiemThu(e, i);
                  }}
                  variant="outlined"
                  name="arNtKetLuan"
                  disabled={disable || permission.arNghiemThus.isDisabled}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
          </Grid>
        </div>
      ))}
      <Grid container spacing={3} className="mt-20">
        <Grid item md={4} sm={12} xs={12}>
          <Grid item md={12} xs={12}>
            <FormControl component="fieldset">
              <FormLabel>
                {t(
                  "ReportIncident.AssetAcceptanceForm.deviceStatusDuringAcceptance"
                )}
              </FormLabel>
              <RadioGroup
                aria-label="deviceStatusDuringAcceptance"
                name="arNtTinhTrang"
                defaultValue={dataItemEdit?.arNtTinhTrang}
                value={
                  dataItemEdit?.arNtTinhTrang
                    ? dataItemEdit?.arNtTinhTrang
                    : arTinhTrang
                }
                onChange={(e) => {
                  setArTinhTrang(Number(e.target.value));
                  setArYeuCauXuLy(null);
                  setArYeuCauXuLyText(null);
                  setIsSavedAcceptanceTab(true)
                }}
                style={{ display: "inline", marginTop: "18px" }}
              >
                <FormControlLabel
                  label={appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.name}
                  value={appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code}
                  control={<Radio />}
                  disabled={permission.arNtTinhTrang.isDisabled || permission.arNtTinhTrang.isView}
                />
                <FormControlLabel
                  label={appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.name}
                  value={appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code}
                  control={<Radio />}
                  disabled={permission.arNtTinhTrang.isDisabled || permission.arNtTinhTrang.isView}
                />
              </RadioGroup>
            </FormControl>
            {handleDisplayText()}
          </Grid>
        </Grid>
        <Grid item md={8} sm={12} xs={12}>
          <Grid item md={12} xs={12}>
            <FormControl component="fieldset" required>
              <FormLabel>
                {t("ReportIncident.AssetAcceptanceForm.requestProcessing")}
              </FormLabel>
              <RadioGroup
                aria-label="deviceStatusDuringAcceptance"
                name="arYeuCauXuLy"
                defaultValue={Number(dataItemEdit?.arYeuCauXuLy)}
                value={
                  dataItemEdit?.arYeuCauXuLy
                    ? dataItemEdit?.arYeuCauXuLy
                    : arYeuCauXuLy
                }
                onChange={(e) => {
                  setArYeuCauXuLy(Number(e.target.value));
                  setArYeuCauXuLyText(null);
                  setIsSavedAcceptanceTab(true)
                }}
                style={{ display: "inline", marginTop: "18px" }}
              >
                {(dataItemEdit?.arNtTinhTrang
                  ? dataItemEdit?.arNtTinhTrang
                  : arTinhTrang) ===
                appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code ? (
                  <>
                    <FormControlLabel
                      label={appConst.YEU_CAU_XU_LY.TAN_DUNG_LINH_KIEN_DE_THAY_THE.name}
                      value={appConst.YEU_CAU_XU_LY.TAN_DUNG_LINH_KIEN_DE_THAY_THE.code}
                      control={<Radio />}
                      disabled={permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                    />
                    <FormControlLabel
                      label={appConst.YEU_CAU_XU_LY.KHAC.name}
                      value={appConst.YEU_CAU_XU_LY.KHAC.code}
                      control={<Radio />}
                      disabled={permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                    />
                  </>
                ) : (
                  <>
                    <FormControlLabel
                      label={appConst.YEU_CAU_XU_LY.BAN_PHE_LIEU.name}
                      value={appConst.YEU_CAU_XU_LY.BAN_PHE_LIEU.code}
                      control={<Radio />}
                      disabled={permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                    />

                    <FormControlLabel
                      label={appConst.YEU_CAU_XU_LY.THU_HOI_THANH_LY.name}
                      value={appConst.YEU_CAU_XU_LY.THU_HOI_THANH_LY.code}
                      control={<Radio />}
                      disabled={permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                    />
                    <FormControlLabel
                      label={appConst.YEU_CAU_XU_LY.HUY_PHAN_LOAI_RAC.name}
                      value={appConst.YEU_CAU_XU_LY.HUY_PHAN_LOAI_RAC.code}
                      control={<Radio />}
                      disabled={permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                    />
                  </>
                )}
              </RadioGroup>
            </FormControl>
            {(dataItemEdit?.arYeuCauXuLy
              ? dataItemEdit?.arYeuCauXuLy
              : arYeuCauXuLy) === appConst.YEU_CAU_XU_LY.KHAC.code && (
              <TextValidator
                className="w-100"
                name="arYeuCauXuLyText"
                onChange={(e) => {
                  handleChangeArYeuCauXuLyText(e);
                }}
                value={
                  dataItemEdit?.arYeuCauXuLyText
                    ? dataItemEdit?.arYeuCauXuLyText
                    : arYeuCauXuLyText
                }
                disabled={disable || permission.arYeuCauXuLy.isDisabled || permission.arYeuCauXuLy.isView}
                inputProps={{
                  maxLength: 255,
                }}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            )}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
AssetAcceptanceForm.contextType = AppContext;
export default AssetAcceptanceForm;
