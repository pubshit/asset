import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
import {ROUTES_PATH} from "../../appConst";

const InstrumentsToolsRepairingTable = EgretLoadable({
  loader: () => import("./InstrumentsToolsRepairingTable")
});
const ViewComponent = withTranslation()(InstrumentsToolsRepairingTable);

const InstrumentsToolsRepairingRoutes = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.IAT_MANAGEMENT.REPAIR,
    exact: false,
    component: ViewComponent
  }
];

export default InstrumentsToolsRepairingRoutes;