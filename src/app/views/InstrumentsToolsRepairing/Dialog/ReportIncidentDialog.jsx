import React, { useState, useRef, useContext } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "../../../appContext";
import {
  createArSuCo,
  updateArSuCo,
} from "../InstrumentsToolsRepairingService";
import { appConst } from "app/appConst";
import ReportIncidentForm from "../Component/ReportIncidentForm";
import BM0601 from "app/views/FormCustom/BM0601";
import BM01 from "app/views/FormCustom/BM01";
import {
  PaperComponent,
  getTheHighestRole,
  isSuccessfulResponse,
  formatDateDto,
  handleThrowResponseMessage
} from "app/appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function ReportIncidentDialog(props) {
  let { departmentUser } = getTheHighestRole();
  const { setPageLoading } = useContext(AppContext);
  let {
    open,
    t,
    handleClose = () => { },
    itemEdit = {},
    isRoleAdmin,
    isRoleAssetManager,
    isAdd
  } = props;
  const form = useRef();
  const [openBM0601, setOpenBM0601] = useState(false);
  const [openBM01, setOpenBM01] = useState(false);
  const [incident, setIncident] = useState({
    reportDate: new Date(),
    status: "",
    incidentReportingRoom: "",
    note: "",
    name: "",
    bhytMaMay: "",
    financialCode: "",
    managementDepartment: {
      name: "",
      code: "",
    },
    code: "",
    managementCode: "",
    model: "",
    serialNumber: "",
    useDepartment: {
      name: "",
      code: "",
    },
    arTgBaoCao: new Date(),
    arNoiDung: "",
    imageIds: [],
    tsModel: "",
  });
  const [anchorEl, setAnchorEl] = useState(null);
  const [listPrintArSucoOptions, setListPrintArSucoOptions] = useState(
    appConst.listPrintArSucoOptions.filter(
      item => ![appConst.PRINT_AR_SUCO_OPTIONS.MAU_KTTS01.code, appConst?.PRINT_AR_SUCO_OPTIONS?.MAU_DNMTS?.code].includes(item.code)
    )
  );

  const isDisabled = itemEdit?.arTrangThai ? itemEdit?.arTrangThai === appConst.listStatusReportIncidentForm[0].id : true

  const handleFormSubmit = async (event, name, type) => {
    const sendData = {
      reportDate: formatDateDto(incident?.reportDate),
      arTgBaoCao: formatDateDto(incident?.arTgBaoCao),
      arPhongBanId: departmentUser?.id,
      arPhongBanText: departmentUser?.name,
      arTnPhongBanId: incident?.arTnPhongBan?.id,
      arTnPhongBanText: incident?.arTnPhongBan?.name,
      arNoiDung: incident?.arNoiDung,
      tsId: incident?.tsId,
      tsMa: incident?.tsMa,
      tsMaquanly: incident?.tsMaquanly,
      tsTen: incident?.tsTen,
      tsModel: incident?.tsModel,
      imageIds: incident?.imageIds,
      assetClass: appConst.assetClass.CCDC,
    };
    if (validateSubmit(sendData)) {
      return;
    }

    incident?.id ? await handleUpdateArSuCo(sendData, name, type)
      : await handleAddArSuCo(sendData, name, type)
  };

  const validateSubmit = (sendData) => {
    if (!sendData?.tsId) {
      toast.warning(t("general.noAssetSelected"))
      return true;
    } else if (!sendData?.arTnPhongBanId) {
      toast.warning("Chưa chọn phòng ban tiếp nhận");
      return true;
    }
    return false
  }

  const handleSaveAndPrint = (event, name) => {
    handleFormSubmit(event, name)
  }

  const handleAddArSuCo = async (data, name, type) => {
    setPageLoading(true);
    try {
      const result = await createArSuCo(data)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        toast.success(t("general.addSuccess"));
        setIncident(result?.data?.data)
        if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code === type) {
          setOpenBM01(true);
        }
        else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_PVT.code === type) {
          setOpenBM0601(true);
        }
        !name && handleClose();
      } else {
        handleThrowResponseMessage(result);
      }
    } catch (error) {
      console.error(error);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const handleUpdateArSuCo = async (data, name, type) => {
    setPageLoading(true);
    try {
      const result = await updateArSuCo(data)
      if (isSuccessfulResponse(result?.data?.code)) {
        toast.success(t("general.updateSuccess"));
        setIncident(result?.data?.data)
        if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code === type) {
          setOpenBM01(true);
        }
        else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_PVT.code === type) {
          setOpenBM0601(true);
        }
        !name && handleClose();
      } else {
        toast.error(result?.data?.message);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const handleOpenMenuPrint = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenuPrint = () => {
    setAnchorEl(null);
  };

  const handlePrint = (e) => {
    if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code === e?.target?.value) {
      isDisabled
        ? handleFormSubmit(
          e,
          "saveAndPrint",
          appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code
        )
        : setOpenBM01(true);
    }
    else {
      isDisabled
        ? handleFormSubmit(
          e,
          "saveAndPrint",
          appConst.PRINT_AR_SUCO_OPTIONS.MAU_PVT.code
        )
        : setOpenBM0601(true);
    }
  };

  const handlePrintClose = async (e) => {
    setOpenBM01(false);
    setOpenBM0601(false);
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth={true}>
      <DialogTitle
        style={{ cursor: "move", paddingBottom: "0px" }}
        id="draggable-dialog-title"
      >
        <h4>{t("ReportIncident.title")}</h4>
      </DialogTitle>
      <ValidatorForm
        ref={form}
        onSubmit={handleFormSubmit}
        className="mt-12"
        class="validator-form-scroll-dialog"
      >
        <DialogContent>
          <ReportIncidentForm
            t={t}
            itemEdit={itemEdit}
            state={incident}
            setState={setIncident}
            isRoleAdmin={isRoleAdmin}
            isEdit={isDisabled}
            isRoleAssetManager={isRoleAssetManager}
            isAdd={isAdd}
            disable={false}
          />
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          {isDisabled && <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="primary"
            type="submit"
          >
            {t("general.save")}
          </Button>}
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="primary"
            onClick={handleOpenMenuPrint}
          >
            {
              isDisabled
                ? t("general.saveAndPrint")
                : t("general.print")
            }
          </Button>
          {/* Menu Print */}
          <Menu
            id="simple-menu"
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            elevation={0}
            onClose={handleCloseMenuPrint}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            {listPrintArSucoOptions.map(item => (
              <MenuItem
                onClick={handlePrint}
                value={item.code}
              >
                {item.name}
              </MenuItem>
            ))}
          </Menu>
        </DialogActions>
      </ValidatorForm>
      {openBM01 && (
        // Phiếu in "Báo Sự cố" or Phiếu in "Thông tin sự cố"
        <BM01
          item={incident}
          t={t}
          open={openBM01}
          handleClose={handlePrintClose}
        />
      )}
      {openBM0601 && (
        // Phiếu in "Báo Sự cố" or Phiếu in "Thông tin sự cố"
        <BM0601
          itemEdit={incident}
          t={t}
          open={openBM0601}
          handleClose={handlePrintClose}
        />
      )}

    </Dialog>
  );
}

ReportIncidentDialog.contextType = AppContext;
export default ReportIncidentDialog;
