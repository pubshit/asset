import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  Radio,
  TablePagination,
} from '@material-ui/core';
import SearchIcon from "@material-ui/icons/Search";
import AppContext from 'app/appContext';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst } from '../../../appConst';
import CustomMaterialTable from '../../CustomMaterialTable';
import { searchByPageAssetSuco } from '../InstrumentsToolsRepairingService';
import { PaperComponent } from "../../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const SelectAssetDialog = (props) => {
  const { t } = useTranslation();
  let { setPageLoading } = useContext(AppContext);
  const { open, handleClose, onAddAsset, useDepartment, managementDepartmentId, item = null } = props;
  const [listAsset, setListAsset] = useState([]);
  const [listSeclectAsset, setListSeclectAsset] = useState([]);
  const [selectedValue, setSelectedValue] = useState(null)
  const [totalElements, setTotalElements] = useState(0);
  const [query, setQuery] = useState({
    pageIndex: 0,
    pageSize: 5,
    keyword: ''
  });

  useEffect(() => {
    handleSearchByPageAsset();
  }, [query.pageIndex, query.pageSize, query.keyword]);

  useEffect(() => {
    setSelectedValue(item);
  }, [item]);

  const handleSearchByPageAsset = () => {
    const sendData = {
      pageIndex: query.pageIndex + 1,
      pageSize: query.pageSize,
      managementDepartmentId: managementDepartmentId || null,
      useDepartmentId: useDepartment?.id,
      assetClass: appConst.assetClass.TSCD,
      keyword: query?.keyword?.trim()
    };
    getListAssetByFilter(sendData)
  };

  const handleChangePage = (event, newPage) => {
    setQuery({
      ...query,
      pageIndex: newPage,
    });
  };
  const setRowsPerPage = (event) => {
    setQuery({ pageSize: event.target.value, pageIndex: 0 });
  };
  const getListAssetByFilter = async (searchObject) => {

    try {
      setPageLoading(true)
      const result = await searchByPageAssetSuco(searchObject)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        setListAsset(result?.data?.data?.content)
        setTotalElements(result?.data?.data?.totalElements)
      }
    } catch (error) {
      toast.error(t("genernal.error"))
    } finally {
      setPageLoading(false)
    }
  }
  const handleClick = (event, item) => {
    if (item.id != null) {
      setSelectedValue(item);
    } else {
      setSelectedValue(null);
    }
  }

  let columns = [
    {
      title: t("general.select"),
      field: "custom",
      align: "center",
      minWidth: '50px',
      maxWidth: '80px',
      render: rowData =>
        <Radio id={`radio${rowData.id}`} name="radSelected" value={rowData.id}
          checked={selectedValue?.id === rowData.id}
          onClick={(event) => handleClick(event, rowData)}
        />
    },
    {
      title: t('MaintainPlaning.assetCode'),
      field: 'code',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.managementCode'),
      field: 'managementCode',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.assetName'),
      field: 'name',
      align: 'left',
      minWidth: '260px',
    },
    {
      title: t('AssetStatus.title'),
      field: 'statusName',
      align: 'left',
      minWidth: '140px',
    },
    {
      title: t('MaintainPlaning.model'),
      field: 'model',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.seri'),
      field: 'serialNumber',
      align: 'left',
      minWidth: '120px',
    },
    {
      title: t('MaintainPlaning.managementDepartment'),
      field: 'managementDepartmentName',
      align: 'left',
      minWidth: '260px',
    },
  ];

  const handleRowClick = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  }

  const handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      setQuery({ ...query, pageIndex: 0, keyword: e.target.value })
    }
  };

  const handleKeyUp = (event) => {
    if (!event?.target?.value) {
      setQuery({
        ...query,
        keyword: ""
      })
      handleSearchByPageAsset();
    }
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth>
      <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
        {t('general.select_asset')}
      </DialogTitle>

      <DialogContent>
        <div className="mp-0">
          <Grid container spacing={2} justifyContent="flex-end" alignItems="flex-end">
            <Grid item md={6} sm={12} xs={12}><FormControl fullWidth>
              <Input
                className="search_box w-100"
                // onChange={handleTextChange}
                onKeyUp={handleKeyUp}
                onKeyDown={handleKeyDownEnterSearch}
                placeholder={t("maintainRequest.search")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      onClick={handleSearchByPageAsset}
                      className="searchTable"
                    />
                  </InputAdornment>
                }
              />
            </FormControl>
            </Grid>
            <Grid item md={12} xs={12}>
              <CustomMaterialTable
                className="customPlaning"
                data={listAsset || []}
                columns={columns}
                onRowClick={((evt, selectedRow) => handleRowClick(selectedRow))}
                options={{
                  rowStyle: (rowData) => ({
                    backgroundColor: selectedValue?.id === rowData.id ? "#EEE" : "#FFF",
                  }),

                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                component="div"
                labelRowsPerPage={t('general.rows_per_page')}
                labelDisplayedRows={({ from, to, count }) => {
                  return `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`
                }
                }
                count={totalElements}
                rowsPerPage={query.pageSize}
                page={query.pageIndex}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={setRowsPerPage}
              />
            </Grid>
          </Grid>
        </div>
      </DialogContent>
      <DialogActions>
        <div className="flex flex-space-between flex-middle mt-12">
          <Button variant="contained" className="mr-12" color="secondary" onClick={() => handleClose()}>
            {t('general.close')}
          </Button>
          <Button
            variant="contained"
            style={{ marginRight: '15px' }}
            color="primary"
            onClick={() => {
              onAddAsset(selectedValue);
            }}
          >
            {t('general.select')}
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
};

export default SelectAssetDialog;
