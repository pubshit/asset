import { FormControlLabel, Switch } from '@material-ui/core'
import React from 'react'
import { withStyles } from "@material-ui/core/styles";
import { variable } from "../../../appConst";
const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 50,
    height: 25.5,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(24px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: theme.custom.primary.main || variable.css.primary.main,
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[200],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

function OptionSummaryReport({ checked, handleChange, name, label, noPosition = false }) {
  return (
    <div
      className={
        noPosition
          ? 'flex flex-middle position-relative option-summary-report-no-position'
          : 'flex flex-middle position-relative option-summary-report'
      }
    >
      <span className='mr-20' style={{ minWidth: "110px", textAlign: "end" }}>{label}</span>
      <FormControlLabel
        control={(
          <IOSSwitch checked={checked} onChange={handleChange} name={name} />
        )}
      />
    </div>

  )
}

export default OptionSummaryReport