import axios from "axios";
import ConstantList from "../../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/summary_report" + ConstantList.URL_PREFIX;
const API_PATH_ASSET = ConstantList.API_ENPOINT + "/api/asset";
const API_PATH_PRODUCT_CATEGORY =
  ConstantList.API_ENPOINT + "/api/productcategory" + ConstantList.URL_PREFIX;
const API_PATH_NEW =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/product-category";
export const searchByPageTree = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  return axios.get(API_PATH_NEW, config);
};

export const searchByPage = (searchObject) => {
  var url = API_PATH_NEW + "/statistics";
  let config = {
    params: { ...searchObject },
  };
  return axios.get(url, config);
};

export const searchByPageAsset = (searchObject) => {
  var url = API_PATH_NEW + "/assets";
  let config = {
    params: { ...searchObject },
  };
  return axios.get(url, config);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "get",
    url: API_PATH_NEW + "/assets/export-excel",
    params: searchObject,
    responseType: "blob",
  });
};

export const getSummary = (searchObject) => {
  let url = API_PATH_NEW + "/summary";
  return axios.get(url, { params: searchObject });
};
