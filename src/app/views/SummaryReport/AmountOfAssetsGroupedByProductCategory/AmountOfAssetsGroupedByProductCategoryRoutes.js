import { EgretLoadable } from "egret";
import ConstantList from "../../../appConfig";
import { withTranslation } from "react-i18next";
const AmountOfAssetsGroupedByProductCategory = EgretLoadable({
  loader: () => import("./AmountOfAssetsGroupedByProductCategory"),
});
const ViewComponent = withTranslation()(AmountOfAssetsGroupedByProductCategory);

const AmountOfAssetsGroupedByProductCategoryRoutes = [
  {
    path:
      ConstantList.ROOT_PATH +
      "summary_report/amount_of_assets_grouped_by_product_category",
    exact: true,
    component: ViewComponent,
  },
];

export default AmountOfAssetsGroupedByProductCategoryRoutes;
