import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  TablePagination,
  DialogActions,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { ValidatorForm } from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { searchByPageAsset as searchByPage } from "./AmountOfAssetsGroupedByProductCategoryService";
import { getTheHighestRole, getUserInformation } from "app/appFunction";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class ViewAssetByProductCategoryDialog extends Component {
  state = {
    departmentId: "",
    keyword: "",
    assetGroupId: "",
    rowsPerPage: 10,
    page: 0,
    assets: [],
    item: {},
    totalElements: 0,
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let { organization } = getUserInformation();
    var searchObject = {};
    searchObject.keyword = this.state.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.productCategoryId = this.state.productCategoryId;
    searchObject.orgId = this.state.currentUser?.org?.id;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.useDepartmentId = this.state?.useDepartment?.id;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    if (this.state.assetSource && this.state.assetSource.id) {
      searchObject.assetSourceId = this.state.assetSource.id;
    }
    if (this.props.isInverted != null) {
      searchObject.isInverted = this.props.isInverted;
    }
    if (this.state?.assetClass?.code) {
      searchObject.assetClass = this.state?.assetClass?.code;
    }
    searchByPage(searchObject).then(({ data }) => {
      this.setState({
        itemsList: [...data?.data?.content],
        totalElements: data?.data?.totalElements,
      });
    }).catch((err) => {
      console.error(err);
    });
  };

  search() {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    });
  }

  handleFormSubmit = () => { };

  componentWillMount() {
    let {
      productCategoryId,
      assetClass,
      state
    } = this.props;
    let { currentUser } = getTheHighestRole();
    this.setState({
      productCategoryId,
      ...this.props.item,
      ...state,
      currentUser,
      assetClass,
    }, () => {
      if (this.state.productCategoryId) {
        this.search();
      }
    });

  }

  componentDidMount() {

  }

  componentWillUnmount() {
    this.setState({
      itemsList: [],
      totalElements: 0,
    });
  }

  render() {
    let { open, t, i18n } = this.props;
    let { keyword, rowsPerPage, page, totalElements, itemsList } = this.state;

    let columns = [
      {
        title: t("general.index"),
        field: "code",
        width: "50px",
        align: "center",
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.code"),
        field: "code",
        minWidth: "130px",
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: "260px",
      },
      {
        title: t("AssetSource.title"),
        field: "assetSourceNames",
        align: "left",
        minWidth: "200px",
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "left",
        minWidth: "140px",
      },
      {
        title: t("Asset.quantityReceipt"),
        field: "quantity",
        align: "center",
        minWidth: "140px",
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "right",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: "160px",
        render: (rowData) => {
          let number = new Number(rowData.originalCost);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("Asset.carryingAmount"),
        field: "carryingAmount",
        align: "right",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: "160px",
        render: (rowData) => {
          let number = new Number(rowData.carryingAmount);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        minWidth: "200px",
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            <span className="mb-20">
              {t(
                "amount_of_assets_grouped_by_asset_group.list_asset_by_product_category"
              )}
            </span>
          </DialogTitle>
          <DialogContent style={{ overflow: "hidden" }}>
            <Grid container justify="flex-start">
              <Grid item md={4} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUp}
                    placeholder={t("Asset.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment>
                        <Link>
                          {" "}
                          <SearchIcon
                            onClick={() => this.search()}
                            style={{
                              position: "absolute",
                              top: "0",
                              right: "0",
                            }}
                          />
                        </Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={"mt-36"}>
                <MaterialTable
                  data={itemsList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "350px",
                    minBodyHeight: "350px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense",
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                  component="div"
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                    }`
                  }
                  page={page}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                  }}
                  labelRowsPerPage={t("general.rows_per_page")}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                  }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-20"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default ViewAssetByProductCategoryDialog;
