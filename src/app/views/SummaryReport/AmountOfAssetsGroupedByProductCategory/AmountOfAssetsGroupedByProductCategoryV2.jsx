import React, { Component } from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
} from "@material-ui/core";

import MaterialTable, { MTableToolbar } from "material-table";
import {
  searchByPage,
  exportToExcel,
  getByRootAndQuantity,
} from "./AmountOfAssetsGroupedByProductCategoryService";
import ViewAssetByProductCategoryDialog from "./ViewAssetByProductCategoryDialog";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import { saveAs } from "file-saver";
import Tooltip from "@material-ui/core/Tooltip";
// treeview
import { withStyles } from "@material-ui/core/styles";
import { Helmet } from "react-helmet";
import { getListManagementDepartment } from "../../Asset/AssetService";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import ReportSearchComponent from "app/views/Component/Asset/ReportSearchComponent";

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class AmountOfAssetsGroupedByProductCategoryV2 extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    assetDepartmentId: "",
    rowsPerPage: 10000000,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByProductCategoryDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    productCategoryId: "",
    isTemporary: null,
    isManageAccountant: null,
    managementDepartmentId: "",
    managementDepartment: null,
    assetSource: null,
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {});
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    getByRootAndQuantity(searchObject).then(({ data }) => {
      data.content.map((item) => {
        if (item.remainQuantity) {
          item.remainQuantity = item.remainQuantity.toFixed(3);
        }
        if (item.totalOriginalCost) {
          item.totalOriginalCost = item.totalOriginalCost.toFixed(3);
        }
      });
      var treeValues = [];

      let itemListClone = [...data.content];

      itemListClone.forEach((item) => {
        var items = this.getListItemChild(item);
        treeValues.push(...items);
      });

      this.setState(
        {
          itemList: treeValues,
          totalElements: data.totalElements,
        },
        function () {
          console.log(this.state.itemList);
        }
      );
    });
  };

  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.productCategoryId = item.id;
    root.parentId = item.parentId;
    root.quantity = item.quantity;
    root.remainQuantity = item.remainQuantity;
    root.totalOriginalCost = item.totalOriginalCost;
    result.push(root);
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByProductCategoryDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByProductCategoryDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    this.updatePageData();
  }

  componentWillMount() {
    getListManagementDepartment().then((data) => {
      this.setState({ listManageDepartment: [...data.data] });
    });
  }

  handleFormSubmit = () => {};

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  search() {
    this.setState({ page: 0 }, function () {
      let { assetSource } = this.state;
      var searchObject = {};
      searchObject.keyword = this.state.keyword;
      if (this.state.departmentId != "all") {
        searchObject.departmentId = this.state.departmentId;
      }
      searchObject.status = this.state.status ? this.state.status : null;
      searchObject.assetGroup = this.state.assetGroup
        ? this.state.assetGroup
        : null;
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.managementDepartmentId = this.state.managementDepartmentId;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
      searchObject.departmentId = this.state.departmentId?.id;
      searchObject.useDepartmentId = this.state.useDepartment?.id;
      if (this.state.rangerValue) {
        searchObject.rangerValue = this.state.rangerValue;
      }
      if (this.state.isInverted != null) {
        searchObject.isInverted = this.state.isInverted;
      }
      if (assetSource && assetSource.id) {
        searchObject.assetSourceId = assetSource.id;
      }
      getByRootAndQuantity(searchObject).then(({ data }) => {
        data.content.map((item) => {
          if (item.remainQuantity) {
            item.remainQuantity = item.remainQuantity.toFixed(3);
          }
          if (item.totalOriginalCost) {
            item.totalOriginalCost = item.totalOriginalCost.toFixed(3);
          }
        });
        var treeValues = [];

        let itemListClone = [...data.content];

        itemListClone.forEach((item) => {
          var items = this.filterItemChild(item);
          treeValues.push(...items);
        });

        this.setState(
          {
            itemList: treeValues,
            totalElements: data.totalElements,
          },
          function () {}
        );
      });
    });
  }

  filterItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.productCategoryId = item.id;
    root.parentId = item.parentId;
    root.quantity = item.quantity;
    root.remainQuantity = item.remainQuantity;
    root.totalOriginalCost = item.totalOriginalCost;
    if (item.quantity && item.quantity != 0) {
      result.push(root);
    }
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.filterItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  getAssetBydepartment = (event, departmentId) => {
    if (departmentId) {
      this.setState({ departmentId: departmentId }, function () {
        this.search();
      });
    }
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () {}
      );
    });
  };
  // end Tree view

  handleDeleteButtonClick = (item) => {
    this.setState({
      item: item,
      shouldOpenDepartmentPopup: true,
    });
  };
  /* Export to excel */
  exportToExcel = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId != "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.useDepartmentId = this.state.useDepartment?.id;
    if (this.state.assetSource && this.state.assetSource.id) {
      searchObject.assetSourceId = this.state.assetSource.id;
    }
    if (this.state.rangerValue) {
      searchObject.rangerValue = this.state.rangerValue;
    }
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "AmountOfAssetsGroupedByProductCategory.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
  };
  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };
  handleChangeYearPutIntoUse = (event, source) => {
    let yearPutIntoUse = event.target.value;
    if (yearPutIntoUse != 0) {
      this.setState({ yearPutIntoUse: yearPutIntoUse }, function () {
        this.search();
      });
    } else {
      this.setState({yearPutIntoUse:null})
      this.search();
    }
    return;
  };
  handleSelectUseDepartment = (item) => {
    let department = { id: item.id, name: item.text, text: item.text };
    this.setState({ useDepartment: department }, function () {
      this.search();
    });
  }
  searchOriginalPrice = (rangerValue, type) => {
    let isInverted = true;
    if (type === "greater_than_or_equal_to") {
        isInverted = false;
    }
    console.log(isInverted+"WWWWWWWWWWWWWWWW")
    this.setState({ rangerValue: rangerValue, isInverted: isInverted }, function () {
      this.search();
    });
  }
  render() {
    const { t, i18n } = this.props;
    let searchObject = { pageIndex: 0, pageSize: 1000000 };

    let {
      itemList,
      item,
      shouldOpenViewAssetByProductCategoryDialog,
      managementDepartment,
      assetSource,
    } = this.state;

    let TitlePage = t(
      "Dashboard.summary_report.amount_of_assets_grouped_by_product_category"
    );
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        width: "",
        // minWidth: "100px",
        maxWidth: "100px",
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                if (rowData.productCategoryId) {
                  this.setState({
                    productCategoryId: rowData.productCategoryId,
                    item: {},
                    shouldOpenViewAssetByProductCategoryDialog: true,
                  });
                }
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("ProductCategory.title"),
        field: "name",
        minWidth: "600px",
      },
     /* {
        title: t("amount_of_assets_grouped_by_asset_group.quantity"),
        field: "quantity",
        align: "right",
        minWidth: "150px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.remainQuantity"),
        field: "remainQuantity",
        align: "right",
        minWidth: "200px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        render: (rowData) => {
          let number = new Number(rowData.remainQuantity);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "right",
        minWidth: "200px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        render: (rowData) => {
          let number = new Number(rowData.totalOriginalCost);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },*/
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t(
                  "Dashboard.summary_report.amount_of_assets_grouped_by_product_category"
                ),
              },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing="2">
          <Grid item md={3} sm={6} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="gender-simple">
                {t("Asset.filter_temporary")}
              </InputLabel>
              <Select
                // value={isTemporary}
                onChange={(isTemporary) =>
                  this.handleChange(isTemporary, "isTemporary")
                }
                inputProps={{
                  name: "isTemporary",
                  id: "gender-simple",
                }}
              >
                {this.listTemporary.map((item) => {
                  return (
                    <MenuItem key={item.name} value={item.value}>
                      {item.value != null ? item.name : "Tất cả"}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="isManageAccountant">
                {t("Asset.filter_manage_accountant")}
              </InputLabel>
              <Select
                // value={isManageAccountant}
                onChange={(isManageAccountant) =>
                  this.handleChange(isManageAccountant, "isManageAccountant")
                }
                inputProps={{
                  name: "isManageAccountant",
                  id: "isManageAccountant",
                }}
              >
                {this.listManageAccountant.map((item) => {
                  return (
                    <MenuItem key={item.name} value={item.value}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <Autocomplete
              className="mt-3"
              size="small"
              id="combo-box"
              options={this.state?.listManageDepartment}
              value={managementDepartment}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t("Asset.filter_manage_department")}
                  value={managementDepartment}
                  // variant="outlined"
                />
              )}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              onChange={(event, value) => {
                this.selectManagementDepartment(value);
              }}
            />
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <ValidatorForm>
              <AsynchronousAutocomplete
                label={
                  <span>
                    <span className="colorRed"></span>{" "}
                    <span> {t("Asset.assetSource")}</span>
                  </span>
                }
                searchFunction={searchByPageAssetSource}
                searchObject={searchObject}
                displayLable={"name"}
                value={assetSource}
                onSelect={(item) => {
                  this.setState({ assetSource: item }, () => {
                    this.search();
                  });
                }}
              />
            </ValidatorForm>
          </Grid>
        </Grid>
        <ReportSearchComponent
          t={t}
          handleChangeYearPutIntoUse={this.handleChangeYearPutIntoUse}
          handleSelectDepartment={this.handleSelectUseDepartment}
          searchOriginalPrice={this.searchOriginalPrice}>
        </ReportSearchComponent>
        <Grid container spacing="2" justify="flex-end" className="mb-12">
          {/*<Grid item md={3} sm={6} xs={12} style={{ textAlign: "end" }}>
            <Button
              className="mt-12"
              size="small"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>*/}
        </Grid>

        <div>
          {shouldOpenViewAssetByProductCategoryDialog &&
            this.state.productCategoryId && (
              <ViewAssetByProductCategoryDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={shouldOpenViewAssetByProductCategoryDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={item}
                assetSourceId={assetSource?.id}
                productCategoryId={this.state.productCategoryId}
                isInverted={this.state.isInverted}
                rangerValue={this.state.rangerValue}
              />
            )}
        </div>
        <Grid item md={12} sm={12} xs={12}>
          <MaterialTable
            title={t("general.list")}
            data={itemList}
            columns={columns}
            parentChildData={(row, rows) => {
              var list = rows.find((a) => a.productCategoryId === row.parentId);
              return list;
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              defaultExpanded: true,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
              }),
              maxBodyHeight: "350px",
              minBodyHeight: "350px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </div>
    );
  }
}

export default AmountOfAssetsGroupedByProductCategoryV2;
