import React, { Component } from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  TablePagination,
  Card,
  CardContent,
} from "@material-ui/core";

import MaterialTable, { MTableToolbar } from "material-table";
import {
  searchByPage,
  exportToExcel,
  searchByPageTree,
  getSummary,
  searchByPageAsset,
} from "./AmountOfAssetsGroupedByProductCategoryService";
import ViewAssetByProductCategoryDialog from "./ViewAssetByProductCategoryDialog";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import Tooltip from "@material-ui/core/Tooltip";
import { toast } from "react-toastify";
// treeview
import { withStyles } from "@material-ui/core/styles";
import { Helmet } from "react-helmet";
import { getListManagementDepartmentOrg } from "../../Asset/AssetService";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import { getUserInformation } from "app/appFunction";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { appConst } from "../../../appConst";
import AppContext from "app/appContext";
import OptionSummaryReport from "../Components/OptionSummaryReport";
import CustomizedTreeView from "../../Asset/TreeViewAsset";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { convertNumberPriceRoundUp, handleThrowResponseMessage, isSuccessfulResponse } from "../../../appFunction";

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

let { organization } = getUserInformation();
class AmountOfAssetsGroupedByProductCategoryV2 extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    assetDepartmentId: "",
    rowsPerPage: 10,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByProductCategoryDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    department: null,
    productCategoryId: "",
    isTemporary: null,
    isManageAccountant: null,
    managementDepartmentId: "",
    managementDepartment: null,
    assetSource: null,
    listOrganizationOrg: [],
    organizationOrg: null,
    assetClass: appConst.assetTypeObject.TSCD.code,
    optionSummaryReport: {
      name: appConst.optionSummaryReport.TDCT.name, code: appConst.optionSummaryReport.TDCT.code
    }
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  getSearchObject = () => {;
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.useDepartmentId = this.state?.useDepartment?.id;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    if (this.state.productCategoryId !== "all") {
      searchObject.productCategoryId = this.state?.productCategoryId;
    }
    if (this.state.assetSource && this.state.assetSource.id) {
      searchObject.assetSourceId = this.state.assetSource.id;
    }
    searchObject.assetClass = this.state.assetClass?.code;
    return searchObject;
  };
  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      optionSummaryReport,
    } = this.state;
    try {
      setPageLoading(true)
      var searchObject = this.getSearchObject();
      let response;
      var treeValues = [];
      let itemListClone = [];
      let total = 0;
      if (optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code) {
        response = await searchByPageAsset(searchObject);
        itemListClone = response?.data?.data?.content;
        total = response?.data?.data?.totalElements;
      } else {
        response = await searchByPage(searchObject);
        response?.data?.data?.forEach((item) => {
          item.carryingAmount = item?.totalCarryingAmount;
          item.originalCost = item?.totalOriginalCost;
        });
        itemListClone = response?.data?.data;
        total = response?.data?.totalElements;
      }
      itemListClone.forEach((item) => {
        var items = this.getListItemChild(item);
        treeValues.push(...items);
      });
      this.setState({
        itemList: treeValues,
        totalElements: total,
      },);
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false)
    }
  };

  search() {
    this.setState({ page: 0 }, () => {
      this.updatePageData()
    });
  }

  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item?.name;
    root.code = item?.code;
    root.productCategoryId = item?.id;
    root.parentId = item?.parentId;
    root.quantity = item?.quantity;
    root.remainQuantity = item?.remainQuantity;
    root.totalCarryingAmount = item?.totalCarryingAmount ? item?.totalCarryingAmount : item?.carryingAmount;
    root.totalOriginalCost = item?.totalOriginalCost ? item?.totalOriginalCost : item?.originalCost;
    result.push(root);
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByProductCategoryDialog: false,
      shouldOpenConfirmationDialog: false,
      productCategoryId: ""
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByProductCategoryDialog: false,
      shouldOpenConfirmationDialog: false,
      productCategoryId: ""
    });
    this.updatePageData();
  };

  componentDidMount() {
    let { t } = this.props;

    searchByPageOrg(organization?.org?.id).then((data) => {
      this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
    }).catch(() => {
      toast.error(t("toastr.error"))
    })

    if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
      this.handleGetDetailInformation(this.getSearchObject());
      this.getTreeView();
    }
    this.updatePageData();
  }

  componentWillMount() {
  }

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.useDepartmentId = this.state?.useDepartment?.id;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    searchObject.productCategoryId = this.state?.productCategoryId;
    if (this.state.assetSource && this.state.assetSource.id) {
      searchObject.assetSourceId = this.state.assetSource.id;
    }
    searchObject.assetClass = this.state.assetClass?.code;
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "AmountOfAssetsGroupedByProductCategory.xlsx");
      })
      .catch((err) => {
        console.log(err);
      }).finally(() => {
        setPageLoading(false);
      });
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
        if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
          this.handleGetDetailInformation(this.getSearchObject());
          this.getTreeView();
        }
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
        if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
          this.handleGetDetailInformation(this.getSearchObject());
          this.getTreeView();
        }
      });
      return;
    }
  };
  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      () => {
        this.search();
        if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
          this.handleGetDetailInformation(this.getSearchObject());
          this.getTreeView();
        }
      }
    );
  };

  handleSelectUseDepartment = (item) => {
    let department = { id: item?.id, name: item?.name, text: item?.name };
    this.setState({ useDepartment: item?.id ? department : null }, function () {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    });
  }

  selectAssetSource = (selected) => {
    this.setState({ assetSource: selected }, () => {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    })
  }

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
      managementDepartment: null,
      managementDepartmentId: null,
      useDepartment: null,
      assetSource: null,
    }, () => {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    })
  }

  handleSelectAssetType = assetType => {
    this.setState({ assetClass: assetType }, () => {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    })
  }

  yearPutIntoUse = Array.from(new Array(20), (val, index) => (new Date()).getFullYear() - index);

  handleKeyDown = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value || e.target.value === null) {
      this.search();
      if (this.state?.optionSummaryReport?.code === appConst.optionSummaryReport.TDCT.code) {
        this.handleGetDetailInformation(this.getSearchObject());
        this.getTreeView();
      }
    }
  };

  handleSetDataSelect = (data, name) => {
    this.setState({
      [name]: data
    });
  };

  handleToggle = () => {
    const newOption = {
      [appConst.optionSummaryReport.TDCT.code]: appConst.optionSummaryReport.TDTH,
      [appConst.optionSummaryReport.TDTH.code]: appConst.optionSummaryReport.TDCT,
    }
    this.setState({
      optionSummaryReport: newOption[this.state.optionSummaryReport.code] || appConst.optionSummaryReport.TDCT,
      productCategoryId: "",
    }, () => {
      this.updatePageData()
    })
  }

  handleSelectMedicalEquipment = (e, value) => {
    this.setState({
      productCategoryId: value,
    }, () => {
      this.search();
      this.handleGetDetailInformation(this.getSearchObject());
      this.getTreeView();
    })
  }

  getTreeView = async () => {
    let { t } = this.props;
    let objectSearch = {
      orgId: this.state.organizationOrg?.id || organization?.org?.id
    }
    try {
      let res = await searchByPageTree(objectSearch)
      if (isSuccessfulResponse(res?.data?.code)) {
        let departmentTreeView = {
          id: "all",
          name: t("Dashboard.subcategory.productCategory"),
          children: res?.data?.data,
        };
        this.setState({ departmentTreeView });
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    }
  }

  handleGetDetailInformation = async (searchObject) => {
    let resSatistics = await getSummary(searchObject);
    if (isSuccessfulResponse(resSatistics?.data?.code)) {
      let { data } = resSatistics?.data;
      this.setState({
        totalAsset: data?.quantity,
        totalOriginalCost: data?.totalOriginalCost,
        totalCarryingAmount: data?.totalCarryingAmount,
      });
    } else {
      handleThrowResponseMessage(resSatistics);
    }
  }

  render() {
    const { t, i18n } = this.props;
    let listAssetType = appConst.assetType.filter(x => x.code !== appConst.assetClass.VT)
    let {
      itemList,
      item,
      shouldOpenViewAssetByProductCategoryDialog,
      managementDepartment,
      assetSource,
      listOrganizationOrg,
      organizationOrg,
      yearPutIntoUse,
      useDepartment,
      assetClass,
      optionSummaryReport,
      departmentTreeView,
      totalAsset,
      totalOriginalCost,
      totalCarryingAmount,
      totalElements,
      rowsPerPage,
      page
    } = this.state;
    let { setPageLoading } = this.context;

    let TitlePage = t(
      "Dashboard.summary_report.amount_of_assets_grouped_by_product_category"
    );
    let searchObjectAssetSource = {
      pageIndex: 0,
      pageSize: 10000000,
      orgId: organizationOrg?.id || organization?.org?.id,
    };
    let searchObjectManagementDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
      isAssetManagement: true
    };
    let searchObjectUseDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
    }
    let isTDCT = optionSummaryReport?.code === appConst.optionSummaryReport.TDCT?.code;
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "center",
        hidden: isTDCT,
        width: "",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "0px",
          minWidth: "100px",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                if (rowData.productCategoryId) {
                  this.setState({
                    productCategoryId: rowData.productCategoryId,
                    item: {},
                    shouldOpenViewAssetByProductCategoryDialog: true,
                  });
                }
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "code",
        minWidth: "50px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("ProductCategory.title"),
        field: "name",
        minWidth: "300px",
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.quantity"),
        field: "quantity",
        align: "center",
        minWidth: "150px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.remainQuantity"),
        field: "totalCarryingAmount",
        align: "right",
        minWidth: "200px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => {
          return convertNumberPriceRoundUp(rowData?.totalCarryingAmount) || 0
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "right",
        minWidth: "200px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => {
          return convertNumberPriceRoundUp(rowData?.totalOriginalCost) || 0
        },
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t(
                  "Dashboard.summary_report.amount_of_assets_grouped_by_product_category"
                ),
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <div className="flex mb-16 spaceBetween">
          <Button
            className="mt-12"
            size="small"
            variant="contained"
            color="primary"
            onClick={this.exportToExcel}
          >
            {t("general.exportToExcel")}
          </Button>
          <OptionSummaryReport
            checked={this.state.optionSummaryReport?.code === appConst.optionSummaryReport.TDTH.code}
            handleChange={this.handleToggle}
            name="optionSumaryReport"
            label={this.state.optionSummaryReport?.name}
            noPosition={true}
          />
        </div>
        <Card elevation={3} className="mb-20">
          <CardContent>
            <Grid container spacing="3">
              <Grid item md={3} sm={6} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="gender-simple">
                    {t("Asset.filter_temporary")}
                  </InputLabel>
                  <Select
                    // value={isTemporary}
                    onChange={(isTemporary) =>
                      this.handleChange(isTemporary, "isTemporary")
                    }
                    inputProps={{
                      name: "isTemporary",
                      id: "gender-simple",
                    }}
                  >
                    {this.listTemporary.map((item) => {
                      return (
                        <MenuItem key={item.name} value={item.value}>
                          {item.value != null ? item.name : "Tất cả"}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="isManageAccountant">
                    {t("Asset.filter_manage_accountant")}
                  </InputLabel>
                  <Select
                    // value={isManageAccountant}
                    onChange={(isManageAccountant) =>
                      this.handleChange(isManageAccountant, "isManageAccountant")
                    }
                    inputProps={{
                      name: "isManageAccountant",
                      id: "isManageAccountant",
                    }}
                  >
                    {this.listManageAccountant.map((item) => {
                      return (
                        <MenuItem key={item.name} value={item.value}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatorForm>
                  <AsynchronousAutocomplete
                    label={t("Asset.filter_manage_department")}
                    searchFunction={getListManagementDepartmentOrg}
                    // multiple={true}
                    searchObject={searchObjectManagementDepartment}
                    typeReturnFunction={"category"}
                    defaultValue={managementDepartment}
                    displayLable={"name"}
                    value={managementDepartment}
                    onSelect={this.selectManagementDepartment}
                  />
                </ValidatorForm>
              </Grid>
              <Grid item md={3} xs={12}>
                <ValidatorForm>
                  <AsynchronousAutocomplete
                    label={t("Asset.assetSource")}
                    searchFunction={searchByPageAssetSource}
                    searchObject={searchObjectAssetSource}
                    defaultValue={assetSource}
                    displayLable={"name"}
                    typeReturnFunction="category"
                    value={assetSource}
                    onSelect={this.selectAssetSource}
                  />
                </ValidatorForm>
              </Grid>
              <Grid item md={3} xs={4}>
                <ValidatorForm>
                  <TextValidator
                    fullWidth
                    label={t("Asset.yearPutIntoUse")}
                    value={yearPutIntoUse}
                    name="yearPutIntoUse"
                    type="number"
                    onKeyUp={this.handleKeyUp}
                    onKeyDown={this.handleKeyDown}
                    onChange={(e) => this.handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                  />
                </ValidatorForm>
              </Grid>
              <Grid item md={3} xs={4}>
                <ValidatorForm>
                  <AsynchronousAutocompleteSub
                    label={t("general.filterAssetType")}
                    searchFunction={() => {
                    }}
                    searchObject={{}}
                    listData={listAssetType}
                    displayLable={"name"}
                    isNoRenderChildren
                    isNoRenderParent
                    value={assetClass || null}
                    name="assetType"
                    onSelect={this.handleSelectAssetType}
                    noOptionsText={t("general.noOption")}
                  />
                </ValidatorForm>
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatorForm>
                  <AsynchronousAutocompleteSub
                    label={t("Asset.useDepartment")}
                    searchFunction={getListManagementDepartmentOrg}
                    // multiple={true}
                    searchObject={searchObjectUseDepartment}
                    typeReturnFunction={"category"}
                    // defaultValue={useDepartment}
                    displayLable={"name"}
                    value={useDepartment}
                    onSelect={this.handleSelectUseDepartment}
                  />
                </ValidatorForm>
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <Autocomplete
                  className="mt-3"
                  size="small"
                  id="combo-box"
                  options={listOrganizationOrg}
                  value={organizationOrg}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={t("Asset.filterOrg")}
                      value={organizationOrg}
                    />
                  )}
                  getOptionLabel={(option) => option.name}
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  onChange={(event, value) => {
                    this.selectOrganizationOrg(value);
                  }}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <div>
          {shouldOpenViewAssetByProductCategoryDialog &&
            this.state.productCategoryId && (
              <ViewAssetByProductCategoryDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={shouldOpenViewAssetByProductCategoryDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={item}
                assetClass={assetClass}
                assetSourceId={assetSource?.id}
                productCategoryId={this.state?.productCategoryId}
                isInverted={this.state.isInverted}
                state={this.state}
                setPageLoading={setPageLoading}
              />
            )}
        </div>
        {optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? (
          <Grid item container md={12} sm={12} xs={12}>
            <Grid item md={3} xs={12}>
              <div className="asset_department treeview-bao-cao overflow-auto MuiPaper-elevation2">
                <div className="treeview-title">
                  <b>{t("amount_of_assets_grouped_by_asset_group.list_asset")}</b>
                </div>
                {departmentTreeView && (
                  <CustomizedTreeView
                    t={t}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpanded={["all"]}
                    defaultExpandIcon={<ChevronRightIcon />}
                    onNodeSelect={this.handleSelectMedicalEquipment}
                    style={{ background: "white", borderRadius: "4px" }}
                    treeViewItem={departmentTreeView}
                  />
                )}
              </div>
            </Grid>
            <Grid item md={9} xs={12}>
              <MaterialTable
                title={
                  <div>
                    <div className="pr-10">
                      <span className="mr-20">Tổng số lượng: <span
                        className="text-red-italic inputPrice">{totalAsset || 0}</span></span>
                      <span className="mr-20">Tổng nguyên giá: <span
                        className="text-red-italic">{convertNumberPriceRoundUp(totalOriginalCost) || 0}</span> </span>
                      <span>Tổng GTCL: <span
                        className="text-red-italic">{convertNumberPriceRoundUp(totalCarryingAmount) || 0}</span></span>
                    </div>
                  </div>
                }
                data={itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                options={{
                  sorting: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                  padding: "dense",
                  minBodyHeight: "435px",
                  maxBodyHeight: "450px",
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                component="div"
                count={totalElements}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </Grid>
        ) :
          <Grid item md={12} sm={12} xs={12}>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.productCategoryId === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                defaultExpanded: true,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
                }),
                maxBodyHeight: "350px",
                minBodyHeight: "350px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        }
      </div>
    );
  }
}
AmountOfAssetsGroupedByProductCategoryV2.contextType = AppContext
export default AmountOfAssetsGroupedByProductCategoryV2;
