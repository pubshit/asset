import React, { Component } from "react";
import {
  Grid,
  TablePagination,
  Button,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Slider,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  searchByPage,
  exportToExcel,
} from "./ReportIncreaseOrDecreaseAssetsService";
import ViewAssetByDepartmentDialog from "./ViewAssetByDepartmentDialog";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { getByRoot } from "../../AssetGroup/AssetGroupService";
import { getListManagementDepartment } from "../../Asset/AssetService";
import { Breadcrumb } from "egret";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import { searchByPage as searchByPageAssetDepartment } from "../../Department/DepartmentService";

class ReportIncreaseOrDecreaseAssets extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    assetDepartmentId: "",
    rowsPerPage: 25,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByDepartmentDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    fromToDate: moment().endOf("year"),
    fromDate: moment().startOf("year"),
    statusIndex: 5,
    statusIndexTT07: 6,
    listGroup: [],
    group: null,
    assetGroup: null,
    managementDepartment: null,
    managementDepartmentId: "",
    listManageDepartment: [],
    isTemporary: null,
    isManageAccountant: null,
    assetSource: null,
    useDepartment: null,
    year: null,
    ranger: [0, 500],
    rangerValue: 500,
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;

  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let { departmentId, status, page, rowsPerPage } = this.state;
    var searchObject = {};
    if (departmentId != "all") {
      searchObject.departmentId = departmentId;
    }
    searchObject.status = status ? status : null;
    searchObject.fromToDate = this.state.fromToDate;
    searchObject.fromDate = this.state.fromDate;
    searchObject.statusIndex = this.state.statusIndex;
    searchObject.statusIndexTT07 = this.state.statusIndexTT07;
    searchObject.assetGroupId = this.state.assetGroupId;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;

    let start = page * rowsPerPage;
    let end = (page + 1) * rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      data.map((item) => {
        if (item.remainQuantity) {
          item.remainQuantity = item.remainQuantity.toFixed(3);
        }
        if (item.totalOriginalCost) {
          item.totalOriginalCost = item.totalOriginalCost.toFixed(3);
        }
      });
      this.setState({
        itemList: data.slice(start, end),
        totalElements: data.length,
      });
    });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    this.updatePageData();
  }

  componentWillMount() {
    var searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 1000;
    getByRoot(searchObject).then(({ data }) => {
      this.setState({ listGroup: [...data.content] });
    });

    getListManagementDepartment().then(
      (data) => {
        this.setState({ listManageDepartment: [...data.data] });
      },
      function () { }
    );
  }

  handleFormSubmit = () => { };

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  search() {
    this.setState({ page: 0 }, function () {
      let {
        status,
        page,
        rowsPerPage,
        fromToDate,
        fromDate,
        assetSource,
        statusIndex,
        managementDepartmentId,
        useDepartment,
        year
      } = this.state;
      var searchObject = {};
      searchObject.status = status ? status : null;
      searchObject.fromToDate = fromToDate;
      searchObject.fromDate = fromDate;
      searchObject.statusIndex = statusIndex;
      searchObject.assetGroupId = this.state.assetGroupId;
      searchObject.statusIndexTT07 = this.state.statusIndexTT07;
      searchObject.managementDepartmentId = managementDepartmentId;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      if (assetSource && assetSource.id) {
        searchObject.assetSourceId = assetSource.id;
      }
      if (this.state.rangerValue) {
        searchObject.rangerValue = this.state.rangerValue;
      }
      if (this.state.isInverted != null) {
        searchObject.isInverted = this.state.isInverted;
      }
      if (useDepartment && useDepartment.id) {
        searchObject.useDepartmentId = useDepartment.id;
      }
      let start = page * rowsPerPage;
      let end = (page + 1) * rowsPerPage;

      if (year) searchObject.year = year;

      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: data.slice(start, end),
          totalElements: data.length,
        });
      });
    });
  }
  getAssetBydepartment = (event, departmentId) => {
    console.log(departmentId);
    if (departmentId) {
      this.setState({ departmentId: departmentId }, function () {
        this.search();
      });
    }
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () { }
      );
    });
  };
  // end Tree view

  handleChangeSlider = (event, newValue) => {
    if (newValue >= 10000) {
      newValue = 10000;
    }
    this.setState({ rangerValue: newValue }, () => {
      // this.search();
    });
  };
  handleDeleteButtonClick = (item) => {
    this.setState({
      item: item,
      shouldOpenDepartmentPopup: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromToDate = this.state.fromToDate;
    searchObject.fromDate = this.state.fromDate;
    searchObject.statusIndex = this.state.statusIndId;
    searchObject.statusIndexTT07 = this.state.statusIndexTT07;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    if (this.state.rangerValue) {
      searchObject.rangerValue = this.state.rangerValue;
    }
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    if (this.state.year) searchObject.year = this.state.year;
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "ReportIncreaseOrDecreaseAssets.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  selectGroup = (item) => {
    this.setState(
      { assetGroupId: item ? item.id : "", assetGroup: item ? item : null },
      function () {
        this.search();
      }
    );
  };
  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
  };

  selectYear = (value) => {
    if (value) {
      this.setState({ year: value.year }, () => this.search())
    } else {
      this.setState({ year: value }, () => this.search())
    }
  }
  valueLabelFormat = (value) => {
    return `${value}`;
  };
  render() {
    const { t, i18n } = this.props;
    let searchObject = { pageIndex: 0, pageSize: 10000000 };
    let dateNow = new Date().getFullYear();
    let listYear = [];
    for (let i = dateNow; i >= 1970; i--) {
      listYear.push({ year: i });
    }
    const marks = [
      {
        value: 0,
        label: "0",
      },
      {
        value: 500,
        label: "500 tr",
      },
      {
        value: 1000,
        label: "1 tỷ",
      },
      {
        value: 2000,
        label: "2 tỷ",
      },
      {
        value: 3000,
        label: "3 tỷ",
      },
      {
        value: 4000,
        label: "4 tỷ",
      },
      {
        value: 5000,
        label: "5 tỷ",
      },
      {
        value: 6000,
        label: "6 tỷ",
      },
      {
        value: 7000,
        label: "7 tỷ",
      },
      {
        value: 8000,
        label: "8 tỷ",
      },
      {
        value: 9000,
        label: "9 tỷ",
      },
    ];
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenViewAssetByDepartmentDialog,
      assetDepartmentId,
      managementDepartment,
      assetSource,
      assetGroup,
      useDepartment,
      year, rangerValue
    } = this.state;

    let TitlePage = t(
      "Dashboard.summary_report.report_increase_or_decrease_assets"
    );
    let columns = [
      {
        title: t("Asset.name"),
        field: "assetName",
        align: "",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "250px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.firstQuantity"),
        field: "firstQuantity",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "120px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.increaseQuantity"),
        field: "increaseQuantity",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "135px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.reductionQuantity"),
        field: "reductionQuantity",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "135px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.finalQuantity"),
        field: "finalQuantity",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "20px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "20px",
          minWidth: "120px",
        },
      },
      {
        title: t("Asset.madeIn"),
        field: "madeIn",
        align: "",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "120px",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "120px",
        },
      },
      {
        title: t("Asset.assetSource"),
        field: "assetSource.name",
        align: "",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "120px",
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "120px",
        },
        render: (rowData) =>
          rowData.depreciationDate ? (
            <span>{moment(rowData.depreciationDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.originalCost"),
        field: "originalCost",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "160px",
        },
        render: (rowData) => {
          let number = new Number(rowData.originalCost);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("Asset.carryingAmount"),
        field: "carryingAmount",
        align: "right",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "160px",
        },
        render: (rowData) => {
          let number = new Number(rowData.carryingAmount);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.summary_report.title") },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2}>
          <Grid item md={3} sm={6} xs={12}>
            <Autocomplete
              size="small"
              id="combo-box"
              className="mt-3"
              options={this.state?.listGroup}
              value={assetGroup}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t("AssetGroup.title")}
                  defaultValue={assetGroup}
                />
              )}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              // disabled ={!this.state.isCheck}
              onChange={(event, value) => {
                this.selectGroup(value);
              }}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="gender-simple">
                {t("Asset.filter_temporary")}
              </InputLabel>
              <Select
                style={{ marginRight: "12px" }}
                onChange={(isTemporary) =>
                  this.handleChange(isTemporary, "isTemporary")
                }
                inputProps={{
                  name: "isTemporary",
                  id: "gender-simple",
                }}
              >
                {this.listTemporary.map((item) => {
                  return (
                    <MenuItem key={item.name} value={item.value}>
                      {item.value != null ? item.name : "Tất cả"}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="isManageAccountant">
                {t("Asset.filter_manage_accountant")}
              </InputLabel>
              <Select
                // value={isManageAccountant}
                onChange={(isManageAccountant) =>
                  this.handleChange(isManageAccountant, "isManageAccountant")
                }
                inputProps={{
                  name: "isManageAccountant",
                  id: "isManageAccountant",
                }}
              >
                {this.listManageAccountant.map((item) => {
                  return (
                    <MenuItem key={item.name} value={item.value}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <Autocomplete
              className="mt-3"
              className="align-bottom"
              size="small"
              id="combo-box"
              options={this.state?.listManageDepartment}
              value={managementDepartment}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t("Asset.filter_manage_department")}
                  value={managementDepartment}
                />
              )}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              onChange={(event, value) => {
                this.selectManagementDepartment(value);
              }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justify="flex-start">
          <Grid item md={3} sm={6} xs={12}>
            <ValidatorForm>
              <AsynchronousAutocomplete
                label={
                  <span>
                    <span className="colorRed"></span>{" "}
                    <span> {t("Asset.assetSource")}</span>
                  </span>
                }
                searchFunction={searchByPageAssetSource}
                searchObject={searchObject}
                displayLable={"name"}
                value={assetSource}
                onSelect={(item) => {
                  this.setState({ assetSource: item }, () => {
                    this.search();
                  });
                }}
              />
            </ValidatorForm>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <ValidatorForm>
              <AsynchronousAutocomplete
                label={
                  <span>
                    <span className="colorRed"></span>{" "}
                    <span> {t("Asset.useDepartment")}</span>
                  </span>
                }
                searchFunction={searchByPageAssetDepartment}
                searchObject={searchObject}
                displayLable={"text"}
                value={useDepartment}
                onSelect={(item) => {
                  this.setState({ useDepartment: item }, () => {
                    this.search();
                  });
                }}
              />
            </ValidatorForm>
          </Grid>
          <Grid item md={2} xs={4}>
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              options={listYear}
              onChange={(e, value) => this.selectYear(value)}
              getOptionLabel={(option) => option.year.toString()}
              renderInput={(params) => <TextField {...params} label="Chọn năm" variant="standard" />}
            />
          </Grid>
          <Grid item md={8} sm={12} xs={12}>
            <Slider
              className="margin-top-12"
              track={false}
              value={rangerValue}
              onChange={this.handleChangeSlider}
              valueLabelDisplay="auto"
              aria-labelledby="discrete-slider-custom"
              max={10000}
              step={100}
              valueLabelFormat={this.valueLabelFormat}
              marks={marks}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <Button
              className="mt-12 mr-12"
              variant="contained"
              size="small"
              color="primary"
              onClick={() => {
                this.setState({ isInverted: false }, () => {
                  this.search();
                })
              }}
            >
              {Math.floor(rangerValue / 1000) > 0
                ? `Nhỏ hơn ${rangerValue / 1000} tỷ`
                : `Nhỏ hơn ${rangerValue} tr`}
            </Button>
            <Button
              className="mt-12"
              variant="contained"
              color="primary"
              size="small"
              onClick={() => {
                this.setState({ isInverted: true }, () => {
                  this.search();
                })
              }}
            >
              {Math.floor(rangerValue / 1000) > 0
                ? `Lớn hơn ${rangerValue / 1000} tỷ`
                : `Lớn hơn ${rangerValue} tr`}
            </Button>
          </Grid>
          <Grid item md={1} sm={6} xs={12} style={{ textAlign: "end" }}>
            <Button
              className="mt-12"
              size="small"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-12">
          <div>
            {shouldOpenViewAssetByDepartmentDialog &&
              this.state.assetDepartmentId && (
                <ViewAssetByDepartmentDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenViewAssetByDepartmentDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  assetSourceId={assetSource?.id}
                  departmentId={assetDepartmentId}
                />
              )}
          </div>
          <MaterialTable
            title={t("general.list")}
            data={itemList}
            columns={columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              defaultExpanded: true,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: "450px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
            component="div"
            count={totalElements}
            rowsPerPage={rowsPerPage}
            page={page}
            labelRowsPerPage={t("general.rows_per_page")}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            backIconButtonProps={{
              "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page",
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.setRowsPerPage}
          />
        </Grid>
      </div>
    );
  }
}

export default ReportIncreaseOrDecreaseAssets;
