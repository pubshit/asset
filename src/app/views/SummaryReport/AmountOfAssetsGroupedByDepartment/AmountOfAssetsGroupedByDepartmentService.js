import axios from "axios";
import ConstantList from "../../../appConfig";

const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/department";
const PATH_FIXED_ASSET_REPORT = API_PATH + "/fixed-assets";
const PATH_INSTRUMENTS_AND_TOOLS_REPORT = API_PATH + "/instruments-and-tools";


export const getByRootAndDepartmentOrg = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/departments/roots"
  return axios.get(url, {params: searchObject})
}

export const getByRootAndQuantityOrg = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/department/fixed-assets/quantity"
  return axios.get(url, {params: searchObject})
}

export const getByRootAndQuantityOrgIat = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/department/instruments-and-tools/quantity"
  return axios.get(url, {params: searchObject})
}

export const searchByPageFixedAsset = (params) => {
  return axios.get(PATH_FIXED_ASSET_REPORT, {params});
};

export const searchByPageInstrumentsAndTools = (params) => {
  return axios.get(PATH_INSTRUMENTS_AND_TOOLS_REPORT, {params});
};

export const exportToExcelFixedAssetByDepartment = (searchObject) => {
  return axios({
    method: 'get',
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/department/fixed-assets/export-excel",
    params: searchObject,
    responseType: 'blob',
  })
};

export const exportToExcelIatByDepartment = (searchObject) => {
  return axios({
    method: 'get',
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/department/instruments-and-tools/export-excel",
    params: searchObject,
    responseType: 'blob',
  })
};

export const exportToExcelFixedAssetStatistics = (searchObject) => {
  return axios({
    method: 'get',
    url: PATH_FIXED_ASSET_REPORT + "/statistics/export-excel",
    params: searchObject,
    responseType: 'blob',
  })
};

export const exportToExcelIatStatistics = (searchObject) => {
  return axios({
    method: 'get',
    url: PATH_INSTRUMENTS_AND_TOOLS_REPORT + "/statistics/export-excel",
    params: searchObject,
    responseType: 'blob',
  })
};

export const getFixedAssetSummary = (params) => {
  let url = PATH_FIXED_ASSET_REPORT + "/summary";
  return axios.get(url, {params});
};

export const getInstrumentsAndToolsSummary = (params) => {
  let url = PATH_INSTRUMENTS_AND_TOOLS_REPORT + "/summary";
  return axios.get(url, {params});
};

export const getFixedAssetStatistics = (params) => {
  let url = PATH_FIXED_ASSET_REPORT + "/statistics";
  return axios.get(url, {params});
};

export const getInstrumentsAndToolsStatistics = (params) => {
  let url = PATH_INSTRUMENTS_AND_TOOLS_REPORT + "/statistics";
  return axios.get(url, {params});
};


