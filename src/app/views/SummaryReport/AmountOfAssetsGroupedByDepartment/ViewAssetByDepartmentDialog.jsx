import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  TablePagination,
  DialogActions,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { ValidatorForm} from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  searchByPageFixedAsset,
  searchByPageInstrumentsAndTools,
} from "./AmountOfAssetsGroupedByDepartmentService";
import {PaperComponent} from "../../Component/Utilities";
import {
  convertNumberPrice,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../../appFunction";
import {toast} from "react-toastify";
import AppContext from "../../../appContext";

class ViewAssetByDepartmentDialog extends Component {
  state = {
    departmentId: "",
    keyword: "",
    assetGroupId: "",
    rowsPerPage: 10,
    page: 0,
    assets: [],
    item: {},
    isTSCD:true,
    totalElements: 0,
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value },  () => { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page },  () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 },  () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.isExactSearch = this.props?.isExactSearch;
    searchObject.isManageAccountant = this.props?.isManageAccountant;
    searchObject.isTemporary = this.props?.isTemporary;
    searchObject.useDeprtmentId = this.props?.useDeprtment?.id;
    searchObject.managementDepartmentId = this.props?.managementDepartment?.id;
    searchObject.assetSourceId = this.props?.assetSource?.id;
    searchObject.orgId = this.props?.organization?.id;

    try {
      setPageLoading(true);
      const res = this.props.isTSCD
        ? await searchByPageFixedAsset(searchObject)
        : await searchByPageInstrumentsAndTools(searchObject);
      const {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: data?.content?.length > 0 ? [...data.content] : [],
          totalElements: data?.totalElements,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }
  };

  search = () => {
    this.setPage(0);
  }

  componentDidMount() {
    this.updatePageData();
  }

  render() {
    let { open, handleClose, handleOKEditClose, t } = this.props;
    let {rowsPerPage, page, itemList } = this.state;

    let columns = [
      {
        title: t("general.index"),
        field: "code",
        width: "50px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: this.state.isTSCD
          ? t("Asset.code")
          : t("InstrumentToolsList.code"),
        field: "code",
        cellStyle: {
          minWidth: "120px",
          textAlign: "center"
        },
      },
      {
        title: this.state.isTSCD
          ? t("Asset.name")
          : t("InstrumentToolsList.name"),
        field: "name",
        align: "left",
        width: "150",
        cellStyle: {
          minWidth: "270px",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.model"),
        field: "model",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      ...(!this.state.isTSCD ? [{
        title: t("general.Quantity"),
        field: "quantity",
        minWidth: 80,
        cellStyle: {
          textAlign: "center"
        },
      }] : []),
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        cellStyle: {
          minWidth: "150px",
          textAlign: "right"
        },
        render: (rowData) => rowData?.originalCost
          ? convertNumberPrice(rowData?.originalCost)
          : ""
      },
      {
        title: t("Asset.carryingAmountTable"),
        field: "carryingAmount",
        hidden: !this.state.isTSCD,
        cellStyle: {
          minWidth: "150px",
          textAlign: "right"
        },
        render: (rowData) => rowData?.carryingAmount
          ? convertNumberPrice(rowData?.carryingAmount)
          : ""
      },
      {
        title: t("Asset.status"),
        field: "statusName",
        width: "150",
        cellStyle: {
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.assetSource"),
        field: "assetSourceNames",
        align: "left",
        width: "150",
        cellStyle: {
          paddingLeft: "10px",
          minWidth: "200px",
        },
      },
      {
        title: t("Asset.store"),
        field: "storeName",
        align: "left",
        width: "150",
        cellStyle: {
          minWidth: "200px",
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        width: "150",
        cellStyle: {
          minWidth: "200px",
        },
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "8px" }}
            id="draggable-dialog-title"
          >
            <span className="mb-20">
              {t(
                "amount_of_assets_grouped_by_asset_group.list_asset_by_department"
              )}
            </span>
          </DialogTitle>
          <DialogContent style={{ overflow: "hidden" }}>
            <Grid container justify="flex-start">
              <Grid item md={4} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    placeholder={t("Asset.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment position="end">
                        <SearchIcon
                          onClick={() => this.search()}
                          className="searchTable"
                        />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={"mt-12"}>
                <MaterialTable
                  data={itemList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    draggable: false,
                    sorting: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "350px",
                    minBodyHeight: "350px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense",
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-34"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default ViewAssetByDepartmentDialog;
ViewAssetByDepartmentDialog.contextType = AppContext;
