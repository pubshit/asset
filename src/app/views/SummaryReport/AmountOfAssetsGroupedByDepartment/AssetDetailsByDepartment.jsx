import React from "react";
import { AppBar, Box, Tab, Tabs, Typography } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import {
    convertNumberPrice,
    getTheHighestRole,
    getUserInformation,
    handleThrowResponseMessage,
    isSuccessfulResponse,
    removeFalsy,
    validateYear
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentAggregateAssetsByAssetGroupTable
    from "../AggregateAssetsByAssetGroup/ComponentAggregateAssetsByAssetGroupTable";
import {
    exportToExcelFixedAssetByDepartment,
    exportToExcelIatByDepartment,
    getByRootAndQuantityOrg,
    getByRootAndDepartmentOrg,
    getByRootAndQuantityOrgIat,
    searchByPageFixedAsset,
    searchByPageInstrumentsAndTools, getFixedAssetSummary, getInstrumentsAndToolsSummary
} from "./AmountOfAssetsGroupedByDepartmentService";
import OptionSummaryReport from "../Components/OptionSummaryReport"
import AmountOfAssetsGroupedByAssetGroup from "./AmountOfAssetsGroupedByDepartment";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import {TabPanel} from "../../Component/Utilities";

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});

let { isRoleAssetManager, departmentUser } = getTheHighestRole();

class AssetDetailsByDepartment extends React.Component {
    constructor(props) {
        super(props);
        this.keyPress = this.keyPress.bind(this);
    }

    state = {
        keyword: "",
        status: [],
        assetGroup: null,
        rowsPerPage: 10,
        page: 0,
        assetGroupId: "",
        assetGroupTreeView: {},
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        selectAllItem: false,
        totalElements: 0,
        shouldOpenConfirmationDeleteAllDialog: false,
        managementDepartment: null,
        useDepartment: null,
        originalCost: null,
        assetSource: null,
        store: null,
        tabValue: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD,
        isTSCD: true,
        useDepartmentId: null,
        organizationOrg: null,
        listOrganizationOrg: [],
        isExactSearch: false,
        isGetSumary: true,
        optionSummaryReport: {
            name: appConst.optionSummaryReport.TDCT.name,
            code: appConst.optionSummaryReport.TDCT.code
        }
    };

    handleChangeTabValue = (event, newValue) => {
        let { setPageLoading } = this.context;
        setPageLoading(true);
        this.setState({
            page: 0,
            rowsPerPage: this.state.rowsPerPage,
        });
        if (appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD === newValue) {
            this.setState(
                {
                    itemList: [],
                    tabValue: newValue,
                    keyword: "",
                    statusIndex: null,
                    assetGroup: {},
                    assetGroupId: "",
                    isTSCD: true,
                    yearPutIntoUse: null,
                    originalCost: null,
                    useDepartment: {},
                    useDepartmentId: null
                },
                () => this.updatePageData()
            );
        }
        if (appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC === newValue) {
            this.setState(
                {
                    itemList: [],
                    tabValue: newValue,
                    keyword: "",
                    statusIndex: null,
                    assetGroup: {},
                    assetGroupId: "",
                    isTSCD: false,
                    yearPutIntoUse: null,
                    originalCost: null,
                    useDepartment: {},
                    useDepartmentId: null
                },
                () => this.updatePageData()
            );
        }
    };

    setPage = (page) => {
        this.setState({ page, isGetSumary: true }, this.updatePageData);
    };

    setRowsPerPage = (event) => {
        this.setState({ rowsPerPage: event.target.value, page: 0, isGetSumary: false }, () => {
            this.updatePageData();
        });
    };

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage, isGetSumary: false }, () => {
            this.updatePageData();
        })
    };

    updatePageData = async () => {
        let { setPageLoading } = this.context;
        let { organization } = getUserInformation();
        let { t } = this.props
        let {
            useDepartment,
            keyword,
            originalCost,
            yearPutIntoUse,
            assetSource,
            store,
            useDepartmentId,
            isExactSearch,
            isTSCD,
            isGetSumary,
            managementDepartment,
            status,
            page,
            rowsPerPage
        } = this.state;
        if (yearPutIntoUse && !validateYear(yearPutIntoUse)) return;
        setPageLoading(true)
        let searchObject = {};
        if (useDepartmentId !== "all") {
            searchObject.useDepartmentId = this.state.useDepartmentId;
        }
        if (useDepartment?.id && useDepartment?.id !== "all") {
            searchObject.useDepartmentId = useDepartment?.id;
        }

        searchObject.assetStatusIds = status ? status?.map(i => i?.id)?.join(",") : null;
        searchObject.pageIndex = page + 1;
        searchObject.pageSize = rowsPerPage;
        searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
        searchObject.managementDepartmentId = isRoleAssetManager ? departmentUser?.id : managementDepartment?.id;
        if (keyword && keyword.trim().length > 0) searchObject.keyword = keyword?.trim();
        if (originalCost) searchObject.originalCost = originalCost;
        if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
        if (assetSource && assetSource.id) searchObject.assetSourceId = assetSource?.id;
        if (store && store.id) searchObject.storeId = store?.id;
        if (isExactSearch) searchObject.isExactSearch = isExactSearch;

        try {
            setPageLoading(true);
            const res = isTSCD
                ? await searchByPageFixedAsset(searchObject)
                : await searchByPageInstrumentsAndTools(searchObject);
            const { code, data } = res?.data;
            if (isSuccessfulResponse(code)) {
                this.setState({
                    itemList: data?.content?.length > 0 ? [...data.content] : [],
                    totalElements: data?.totalElements,
                });
            } else {
                handleThrowResponseMessage(res);
            }
            if (isGetSumary) {
                const resTotal = isTSCD
                    ? await getFixedAssetSummary(searchObject)
                    : await getInstrumentsAndToolsSummary(searchObject);
                const codeTotal = resTotal?.data?.code;
                const dataTotal = resTotal?.data?.data;

                if (isSuccessfulResponse(codeTotal)) {
                    this.setState({
                        totalAsset: dataTotal?.quantity,
                        totalOriginalCost: dataTotal?.totalOriginalCost,
                        totalCarryingAmount: dataTotal?.totalCarryingAmount,
                    })
                } else {
                    handleThrowResponseMessage(resTotal);
                }
            }
        } catch (error) {
            toast.error(t("toastr.error"))
        } finally {
            setPageLoading(false);
        }
    };


    convertAssetDepartmentTreeView = (department, quantityAsset) => {
        let children = []
        quantityAsset?.forEach(item => {
            let assetDepartment = department?.find((itemDepartment) => itemDepartment?.id === item?.id)
            if (assetDepartment) {
                assetDepartment.assetQty = item?.quantity || 0
                children.push(assetDepartment)
            }
            this.convertAssetDepartmentTreeView(assetDepartment?.children, item?.children)
        })
        return children
    }

    getTreeView = async () => {
        let { organizationOrg } = this.state
        let { organization } = getUserInformation();
        let { t } = this.props;
        let objectSearch = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            assetClass: this.state.isTSCD ? appConst.assetClass.TSCD : appConst.assetClass.CCDC,
            orgId: organizationOrg?.id || organization?.org?.id,
            isExactSearch: this.state.isExactSearch,
        }
        try {
            const getQuantityOrg = this.state.isTSCD ? getByRootAndQuantityOrg : getByRootAndQuantityOrgIat;
            let res = await getByRootAndDepartmentOrg(objectSearch)
            let resQuantity = await getQuantityOrg(objectSearch)
            if (res?.status === appConst.CODE.SUCCESS && res?.data?.data) {
                let assetGroupTreeView = {
                    id: "all",
                    name: "Danh sách phòng ban",
                    children: this.convertAssetDepartmentTreeView(res?.data?.data, resQuantity?.data?.data),
                };
                this.setState({ assetGroupTreeView: assetGroupTreeView }, () => { });
            }
        } catch (e) {
            toast.error(t("toastr.error"))
        }
    }


    componentDidMount() {
        let { t } = this.props;
        let { organization } = getUserInformation();

        if (isRoleAssetManager) {
            this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
        };

        searchByPageOrg(organization?.org?.id).then((data) => {
            this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
        }).catch(() => {
            toast.error(t("toastr.error"))
        })
        this.updatePageData();
        this.getTreeView()
    }

    componentWillMount() { }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.tabValue !== prevState.tabValue) {
            this.getTreeView()
        }
    }

    selectManagementDepartment = (item) => {
        this.setState({ managementDepartment: item }, () => {
            this.search();
        });
    };

    handleEditItem = (item) => {
        this.setState({
            item: item,
            shouldOpenEditorDialog: true,
        });
    };

    handleClick = (event, item) => {
        let { Asset } = this.state;
        if (item.checked == null) {
            item.checked = true;
        } else {
            item.checked = !item.checked;
        }
        let selectAllItem = true;
        for (let i = 0; i < Asset.length; i++) {
            if (Asset[i].checked == null || Asset[i].checked === false) {
                selectAllItem = false;
            }
            if (Asset[i].id === item.id) {
                Asset[i] = item;
            }
        }
        this.setState({ selectAllItem: selectAllItem, Asset: Asset });
    };
    handleSelectAllClick = () => {
        let { Asset } = this.state;
        for (let i = 0; i < Asset.length; i++) {
            Asset[i].checked = !this.state.selectAllItem;
        }
        this.setState({ selectAllItem: !this.state.selectAllItem, Asset: Asset });
    };

    handleDelete = (id) => {
        this.setState({
            id,
            shouldOpenConfirmationDialog: true,
        });
    };

    selectStatus = (statusSelected) => {
        this.setState({ status: statusSelected }, () => {
            this.search();
        });
    };

    selectAssetGroup = (assetGroupSelected) => {
        this.setState({ assetGroup: assetGroupSelected }, () => {
            this.search();
        });
    };

    search = async () => {
        this.setPage(0);
    };


    getAssetBydepartment = async (event, assetGroupId) => {
        if (assetGroupId && event?.target?.textContent) {
            this.setState({
                page: 0,
                useDepartment: { id: assetGroupId, name: event.target.textContent },
                useDepartmentId: assetGroupId
            },
                this.updatePageData
            );
        }
    };

    selectUseDepartment = (selected) => {
        this.setState({ useDepartment: selected }, () => {
            this.search();
        })
    }

    selectAssetSource = (selected) => {
        this.setState({ assetSource: selected }, () => {
            this.search();
        })
    }
    selectOrganizationOrg = (selected) => {
        this.setState({
            organizationOrg: selected,
            managementDepartment: null,
            useDepartment: null,
            assetSource: null,
            useDepartmentId: null,
            store: null
        }, () => {
            this.search();
            this.getTreeView();
        })
    }
    selectStore = (selected) => {
        this.setState({ store: selected }, () => {
            this.search();
        })
    }

    keyPress(e, value, nameState) {
        if (e.key === appConst.KEY.ENTER) {
            this.setState({ [nameState]: value }, () => this.search())
        }
    }

    selectYear = (value) => {
        if (value) {
            this.setState({ yearPutIntoUse: value.year }, () => this.search())
        } else {
            this.setState({ yearPutIntoUse: value }, () => this.search())
        }
    }
    handleToggle = () => {
        this.setState({
            optionSummaryReport: {
                name: this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? appConst.optionSummaryReport.TDTH.name : appConst.optionSummaryReport.TDCT.name,
                code: this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? appConst.optionSummaryReport.TDTH.code : appConst.optionSummaryReport.TDCT.code
            }
        })
    }
    handleToggleTable = () => {
        let { isExactSearch } = this.state
        this.setState({
            isExactSearch: !isExactSearch
        }, () => {
            this.updatePageData();
            this.getTreeView();
        })
    }

    handleKeyDown = (e) => {
        if (appConst.KEY.ENTER === e.key) {
            this.search();
        }
    };

    handleKeyUp = (e) => {
        !e.target.value && this.search();
    };

    handleSetDataSelect = (data, name) => {
        this.setState({
            [name]: data
        });
    };

    /* Export to excel */
    exportToExcel = async () => {
        const { setPageLoading } = this.context;

        try {
            setPageLoading(true);

            const searchObject = this.buildSearchObject();

            let exportFunction;
            let filename = "export.xlsx";
            if (this.state.isTSCD) {
                exportFunction = exportToExcelFixedAssetByDepartment;
                filename = "FixedAssetByDepartment.xlsx"
            } else {
                exportFunction = exportToExcelIatByDepartment;
                filename = "IatByDepartment.xlsx"
            }

            if (exportFunction) {
                const res = await exportFunction(removeFalsy(searchObject));

                const blob = new Blob([res.data], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                });

                saveAs(blob, filename);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setPageLoading(false);
        }
    };

    buildSearchObject = () => {
        const {
            tabValue,
            keyword,
            page,
            rowsPerPage,
            managementDepartment,
            yearPutIntoUse,
            assetSource,
            useDepartment,
            store,
            status,
            originalCost,
            isExactSearch
        } = this.state;
        const isRoleAccountant = getTheHighestRole()?.isRoleAccountant
            ? getTheHighestRole()?.isRoleAccountant
            : null
        const searchObject = {
            keyword,
            assetClass: tabValue === appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD ? appConst.assetClass.TSCD : appConst.assetClass.CCDC,
            // pageIndex: page + 1,
            // pageSize: rowsPerPage,
            managementDepartmentId: managementDepartment?.id,
            yearPutIntoUse,
            assetSourceId: assetSource?.id,
            useDepartmentId: useDepartment?.id !== "all" ? useDepartment?.id : null,
            storeId: store?.id,
            assetStatusIds: status?.map(i => i?.id).join(","),
            isManageAccountant: isRoleAccountant,
            originalCost: originalCost,
            isExactSearch: isExactSearch ? !isExactSearch : null
        };

        return searchObject;
    };


    render() {
        const { t, i18n } = this.props;
        let { organization } = getUserInformation()
        const { tabValue, rowsPerPage, page, organizationOrg } = this.state;
        const currentDepartment = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
        const config_org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
        let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
        let searchObjectAssetSource = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            orgId: organizationOrg?.id || organization?.org?.id,
        };
        let searchObjectStore = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            departmentId: getTheHighestRole().isRoleAdmin || config_org.configDto.isGetAllStore
                ? ""
                : (currentDepartment?.id || ""),
            orgId: organizationOrg?.id || organization?.org?.id
        };
        let searchObjectManagementDepartment = {
            pageIndex: 1,
            pageSize: 1000000,
            orgId: organizationOrg?.id || organization?.org?.id,
            isAssetManagement: true
        }
        let searchObjectUseDepartment = {
            pageIndex: 1,
            pageSize: 1000000,
            orgId: organizationOrg?.id || organization?.org?.id,
        }
        let dateNow = new Date().getFullYear();
        let listYear = [];
        for (let i = dateNow; i >= 1970; i--) {
            listYear.push({ year: i });
        }

        let TitlePage = t("Dashboard.summary_report.amount_of_assets_grouped_by_department");
        let columns = [
            {
                title: t("general.index"),
                field: "code",
                width: "50px",
                cellStyle: {
                    textAlign: "center"
                },
                render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
            },
            {
                title: this.state.isTSCD
                    ? t("Asset.code")
                    : t("InstrumentToolsList.code"),
                field: "code",
                cellStyle: {
                    minWidth: "120px",
                    textAlign: "center"
                },
            },
            {
                title: this.state.isTSCD
                    ? t("Asset.name")
                    : t("InstrumentToolsList.name"),
                field: "name",
                align: "left",
                width: "150",
                cellStyle: {
                    minWidth: "270px",
                },
            },
            {
                title: t("Asset.yearOfManufacture"),
                field: "yearOfManufacture",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.yearIntoUse"),
                field: "yearPutIntoUse",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.serialNumber"),
                field: "serialNumber",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.model"),
                field: "model",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            ...(!this.state.isTSCD ? [{
                title: t("general.Quantity"),
                field: "quantity",
                minWidth: 80,
                cellStyle: {
                    textAlign: "center"
                },
            }] : []),
            {
                title: t("Asset.manufacturer"),
                field: "manufacturerName",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.originalCost"),
                field: "originalCost",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "right"
                },
                render: (rowData) => rowData?.originalCost
                    ? convertNumberPrice(rowData?.originalCost)
                    : ""
            },
            {
                title: t("Asset.carryingAmountTable"),
                field: "carryingAmount",
                hidden: !this.state.isTSCD,
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "right"
                },
                render: (rowData) => rowData?.carryingAmount
                    ? convertNumberPrice(rowData?.carryingAmount)
                    : ""
            },
            {
                title: t("Asset.status"),
                field: "statusName",
                width: "150",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.assetSource"),
                field: "assetSourceNames",
                align: "left",
                width: "150",
                cellStyle: {
                    paddingLeft: "10px",
                    minWidth: "200px",
                },
            },
            {
                title: t("Asset.store"),
                field: "storeName",
                align: "left",
                width: "150",
                cellStyle: {
                    minWidth: "200px",
                },
            },
            {
                title: t("Asset.useDepartment"),
                field: "useDepartmentName",
                align: "left",
                width: "150",
                cellStyle: {
                    minWidth: "200px",
                },
            },
        ];
        return (
            <div className="m-sm-30">
                <Helmet>
                    <title>
                        {TitlePage} | {t("web_site")}
                    </title>
                </Helmet>
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            {
                                name: t("Dashboard.summary_report.title"),
                                path: "/summary_report/aggregate_assets_by_asset_group",
                            },
                            { name: TitlePage },
                        ]}
                    />
                </div>
                <div className="flex flex-end">
                    <OptionSummaryReport
                        checked={this.state.optionSummaryReport?.code === appConst.optionSummaryReport.TDTH.code}
                        handleChange={() => this.handleToggle()}
                        label={this.state.optionSummaryReport?.name}
                        noPosition="true"
                    />
                </div>
                {
                    this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? (
                        <div>
                            <AppBar position="static" color="default" className="mb-10">
                                <Tabs
                                    className="tabsStatus"
                                    value={tabValue}
                                    onChange={this.handleChangeTabValue}
                                    variant="scrollable"
                                    scrollButtons="on"
                                    indicatorColor="primary"
                                    textColor="primary"
                                    aria-label="scrollable force tabs example"
                                >
                                    <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabTSCD")} />
                                    <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabCCDC")} />
                                </Tabs>
                            </AppBar>
                            <TabPanel
                                value={tabValue}
                                index={appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD}
                                className="mp-0"
                            >
                                <ComponentAggregateAssetsByAssetGroupTable
                                    t={t}
                                    i18n={i18n}
                                    exportToExcel={this.exportToExcel}
                                    item={this.state}
                                    titleTree={t("AggregateAssetsByAssetGroup.titleTree")}
                                    selectManagementDepartment={this.selectManagementDepartment}
                                    searchObject={searchObject}
                                    selectStatus={this.selectStatus}
                                    selectUseDepartment={this.selectUseDepartment}
                                    selectAssetSource={this.selectAssetSource}
                                    searchObjectStore={searchObjectStore}
                                    searchObjectManagementDepartment={searchObjectManagementDepartment}
                                    searchObjectUseDepartment={searchObjectUseDepartment}
                                    searchObjectAssetSource={searchObjectAssetSource}
                                    selectStore={this.selectStore}
                                    listYear={listYear}
                                    keyPress={this.keyPress}
                                    selectYear={this.selectYear}
                                    getAssetBydepartment={this.getAssetBydepartment}
                                    columns={columns}
                                    handleChangePage={this.handleChangePage}
                                    setRowsPerPage={this.setRowsPerPage}
                                    selectOrganizationOrg={this.selectOrganizationOrg}
                                    handleToggleTable={this.handleToggleTable}
                                    isShowExactSearch={true}
                                    handleSetDataSelect={this.handleSetDataSelect}
                                    handleKeyDown={this.handleKeyDown}
                                    handleKeyUp={this.handleKeyUp}
                                    isRoleAssetManager={isRoleAssetManager}
                                    isShowFilterStore={true}
                                />
                            </TabPanel>
                            <TabPanel
                                value={tabValue}
                                index={appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC}
                                className="mp-0"
                            >
                                <ComponentAggregateAssetsByAssetGroupTable
                                    t={t}
                                    item={this.state}
                                    exportToExcel={this.exportToExcel}
                                    titleTree={t("AggregateAssetsByAssetGroup.titleTree")}
                                    selectManagementDepartment={this.selectManagementDepartment}
                                    searchObject={searchObject}
                                    selectStatus={this.selectStatus}
                                    selectUseDepartment={this.selectUseDepartment}
                                    selectAssetSource={this.selectAssetSource}
                                    searchObjectStore={searchObjectStore}
                                    searchObjectManagementDepartment={searchObjectManagementDepartment}
                                    searchObjectUseDepartment={searchObjectUseDepartment}
                                    searchObjectAssetSource={searchObjectAssetSource}
                                    selectStore={this.selectStore}
                                    listYear={listYear}
                                    keyPress={this.keyPress}
                                    selectYear={this.selectYear}
                                    getAssetBydepartment={this.getAssetBydepartment}
                                    columns={columns}
                                    handleChangePage={this.handleChangePage}
                                    setRowsPerPage={this.setRowsPerPage}
                                    selectOrganizationOrg={this.selectOrganizationOrg}
                                    handleToggleTable={this.handleToggleTable}
                                    isShowExactSearch={true}
                                    handleSetDataSelect={this.handleSetDataSelect}
                                    handleKeyDown={this.handleKeyDown}
                                    handleKeyUp={this.handleKeyUp}
                                    isRoleAssetManager={isRoleAssetManager}
                                />
                            </TabPanel>
                        </div>
                    ) : (
                        <AmountOfAssetsGroupedByAssetGroup
                            t={this.props.t}
                        />
                    )
                }
            </div>
        )
    }
}

AssetDetailsByDepartment.contextType = AppContext
export default AssetDetailsByDepartment;
