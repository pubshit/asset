import React, { Component } from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
} from "@material-ui/core";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { STATUS_DEPARTMENT, appConst, keySearch } from "app/appConst";
import { searchByPage } from "../../Department/DepartmentService";
import { debounce } from "lodash";
import ComponentAmountOfAssetsByAssetGroupTable from "./ComponentAssetsGroupedByDepartmentTable";
import {
  exportToExcelFixedAssetStatistics, exportToExcelIatStatistics,
  getFixedAssetStatistics,
  getInstrumentsAndToolsStatistics
} from "./AmountOfAssetsGroupedByDepartmentService";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import {
  convertNumberPrice,
  functionExportToExcel,
  getTheHighestRole,
  getUserInformation, handleKeyDown, handleKeyUp,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  validateYear
} from "../../../appFunction";
import {LightTooltip, TabPanel} from "../../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

let { isRoleAssetManager, departmentUser } = getTheHighestRole();

class AmountOfAssetsGroupedByAssetGroup extends Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, () => {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    rowsPerPage: 1000000,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByAssetGroupDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    managementDepartmentId: "",
    managementDepartment: null,
    listManageDepartment: [],
    assetSources: [],
    assetSourceId: "",
    assetSource: null,
    isTemporary: null,
    isManageAccountant: null,
    ranger: [0, 500],
    useDepartment: {},
    // rangerValue: 500,
    isInverted: null, // lấy từ 0 đến rangerValue
    query: {
      pageIndex: 0,
      pageSize: 10,
      keyword: "",
      keySearch: ""
    },
    listDepartment: [],
    tabValue: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD,
    isTSCD: true,
    itemList: [],
    organizationOrg: null,
    listOrganizationOrg: [],
    isExactSearch: false
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  yearPutIntoUse = Array.from(new Array(20), (val, index) => (new Date()).getFullYear() - index);
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { });
  };

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
      assetGroup: {},
      assetGroupId: "",
      yearPutIntoUse: null,
      isTemporary: null,
      isManageAccountant: null,
      assetSource: null,
      useDepartment: null,
      managementDepartmentId: isRoleAssetManager ? departmentUser?.id : "",
      managementDepartment: isRoleAssetManager ? departmentUser : null,
      isTSCD: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD === newValue
    }, () => this.updatePageData());
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let { isTSCD } = this.state;
    let { organization } = getUserInformation()
    if (this.state?.yearPutIntoUse && !validateYear(this.state?.yearPutIntoUse)) return;
    try {
      let searchObject = {};
      setPageLoading(true)
      searchObject.keyword = this.state.keyword;
      searchObject.assetSourceId = this.state.assetSource?.id;
      searchObject.useDepartmentId = this.state.useDepartment?.id;
      searchObject.managementDepartmentId = isRoleAssetManager ? departmentUser?.id : this.state.managementDepartment?.id || this.state.managementDepartmentId;
      searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
      searchObject.isExactSearch = this.state.isExactSearch;

      let res = isTSCD
        ? await getFixedAssetStatistics(searchObject)
        : await getInstrumentsAndToolsStatistics(searchObject);
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        let treeValues = [];
        data?.forEach((item) => {
          let items = this.getListItemChild(item);
          treeValues = treeValues.concat(items);
        });

        this.setState({
          itemList: treeValues,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  getListItemChild(item) {
    let result = [];
    result.push(item);
    if (item.children) {
      item.children?.map(child => ({ ...child, parentId: item?.id }));
      item?.children?.forEach((child) => {
        let childrenList = this.getListItemChild(child);
        result = result.concat(childrenList);
      });
    }

    return result;
  }

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    let { t } = this.props
    let { organization } = getUserInformation();

    if (isRoleAssetManager) {
      this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
    };

    searchByPageOrg(organization?.org?.id).then((data) => {
      this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
    }).catch(() => {
      toast.error(t("toastr.error"))
    })
    this.getListDepartment()
    this.updatePageData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state?.query !== prevState.query) {
      if (this.state?.query.keySearch === keySearch.asset) {
        this.getListDepartment()
      }
    }
  }

  debouncedSetSearchTerm = debounce((newTerm, source) => {
    this.setState({
      query: {
        ...this.state.query,
        keyword: newTerm,
        keySearch: source
      }
    });
  }, 300);

  handleSearchDepartment = (e, source) => {
    this.debouncedSetSearchTerm(e.target.value, source)
  };

  getListDepartment = async () => {
    let searchObject = {};
    searchObject.keyword = this.state.query.keyword.trim();
    searchObject.pageIndex = this.state.query.pageIndex + 1;
    searchObject.pageSize = this.state.query.pageSize;
    searchObject.isActive = STATUS_DEPARTMENT.HOAT_DONG.code
    try {
      let res = await searchByPage(searchObject);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.content) {
        this.setState({
          listDepartment: [...res?.data?.content]
        });
      } else {
        this.setState({
          listDepartment: [],
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  search = () => {
    this.setPage(0);
  }

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      () => {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  /* Export to excel */
  exportToExcel = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props
    let { organization } = getUserInformation();
    let searchObject = {};

    searchObject.keyword = this.state.keyword;
    searchObject.assetSourceId = this.state.assetSource?.id;
    searchObject.useDepartmentId = this.state.useDepartment?.id;
    searchObject.managementDepartmentId = this.state.managementDepartment?.id || this.state.managementDepartmentId;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    searchObject.isExactSearch = this.state.isExactSearch;

    try {
      setPageLoading(true);
      let exportFunc = this.state.isTSCD
        ? exportToExcelFixedAssetStatistics
        : exportToExcelIatStatistics;
      let fileName = this.state.isTSCD
        ? t("exportToExcel.AmountOfAssetsGroupedByAssetDepartment")
        : t("exportToExcel.AmountOfIatsGroupedByAssetDepartment");
      await functionExportToExcel(
        exportFunc,
        searchObject,
        fileName,
      )
    } catch (e) {
      console.error(e);
    } finally {
      setPageLoading(false);
    }
  };

  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      () => {
        this.search();
      }
    );
  };

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
      managementDepartment: null,
      useDepartment: null,
      assetSource: null,
      assetSourceId: null,
      store: null
    }, () => {
      this.search();
    })
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary === "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, () => {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant === "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, () => {
        this.search();
      });
      return;
    }
    if (source === "yearPutIntoUse") {
      let yearPutIntoUse = event.target.value;
      if (yearPutIntoUse !== 0) {
        this.setState({ yearPutIntoUse: yearPutIntoUse }, () => {
          this.search();
        });
      } else {
        this.setState({ yearPutIntoUse: null })
        this.search();
      }
    }
  };

  handleKeyDown = (e) => handleKeyDown(e, this.search);

  handleKeyUp = (e) => handleKeyUp(e, this.search);

  handleSetDataSelect = (data, name) => {
    this.setState({
      [name]: data
    });
  };

  handleSelectUseDepartment = (item) => {
    let department = item ? { id: item?.id, name: item?.name, text: item?.name } : null;
    this.setState({ useDepartment: department }, () => {
      this.search();
    });
  }

  handleToggleTable = () => {
    let { isExactSearch } = this.state;
    this.setState({
      isExactSearch: !isExactSearch
    }, () => {
      this.updatePageData()
    })
  }

  handleSelectAssetSources = (value) => {
    this.setState({
      assetSource: value,
    },
      this.search
    )
  }

  render() {
    const { t, i18n } = this.props;
    const { tabValue, organizationOrg } = this.state;
    let { organization } = getUserInformation()
    let searchObjectAssetSource = { ...appConst.OBJECT_SEARCH_MAX_SIZE, orgId: organizationOrg?.id, };
    let searchObjectManagementDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      orgId: organizationOrg?.id || organization?.org?.id,
      isAssetManagement: true
    }
    let searchObjectUseDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      orgId: organizationOrg?.id || organization?.org?.id,
    }

    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        cellStyle: {
          minWidth: "120px",
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.view) {
                this.setState(
                  {
                    currentDepartment: rowData,
                    shouldOpenViewAssetByAssetGroupDialog: true,
                  }
                );
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("Department.title"),
        field: "name",
        width: "300",
        align: "left",
        cellStyle: {
          minWidth: "300px",
          textAlign: "left",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.quantity"),
        field: "quantity",
        align: "left",
        cellStyle: {
          minWidth: "100px",
          textAlign: "center",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.remainQuantity"),
        field: "totalCarryingAmount",
        hidden: !this.state.isTSCD,
        align: "left",
        cellStyle: {
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData.totalCarryingAmount) || 0,
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "left",
        cellStyle: {
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData.totalOriginalCost) || 0,
      },
    ];


    return (
      <div className="">
        <AppBar position="static" color="default" className="mb-10">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabTSCD")} />
            <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabCCDC")} />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD}
          className="mp-0"
        >
          <ComponentAmountOfAssetsByAssetGroupTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            handleChange={this.handleChange}
            listTemporary={this.listTemporary}
            listManageAccountant={this.listManageAccountant}
            yearPutIntoUse={this.yearPutIntoUse}
            searchObjectAssetSource={searchObjectAssetSource}
            searchObjectManagementDepartment={searchObjectManagementDepartment}
            searchObjectUseDepartment={searchObjectUseDepartment}
            handleSelectUseDepartment={this.handleSelectUseDepartment}
            handleSearchDepartment={this.handleSearchDepartment}
            selectOrganizationOrg={this.selectOrganizationOrg}
            exportToExcel={this.exportToExcel}
            handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
            handleDepartmentPopupClose={this.handleDepartmentPopupClose}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            selectManagementDepartment={this.selectManagementDepartment}
            handleKeyDown={this.handleKeyDown}
            handleKeyUp={this.handleKeyUp}
            handleSetDataSelect={this.handleSetDataSelect}
            isShowExactSearch={true}
            handleToggleTable={this.handleToggleTable}
            handleSelectAssetSources={this.handleSelectAssetSources}
            isRoleAssetManager={isRoleAssetManager}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC}
          className="mp-0"
        >
          <ComponentAmountOfAssetsByAssetGroupTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            handleChange={this.handleChange}
            listTemporary={this.listTemporary}
            listManageAccountant={this.listManageAccountant}
            yearPutIntoUse={this.yearPutIntoUse}
            searchObjectAssetSource={searchObjectAssetSource}
            searchObjectManagementDepartment={searchObjectManagementDepartment}
            searchObjectUseDepartment={searchObjectUseDepartment}
            handleSelectUseDepartment={this.handleSelectUseDepartment}
            handleSearchDepartment={this.handleSearchDepartment}
            selectOrganizationOrg={this.selectOrganizationOrg}
            exportToExcel={this.exportToExcel}
            handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
            handleDepartmentPopupClose={this.handleDepartmentPopupClose}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            selectManagementDepartment={this.selectManagementDepartment}
            handleKeyDown={this.handleKeyDown}
            handleKeyUp={this.handleKeyUp}
            handleSetDataSelect={this.handleSetDataSelect}
            isShowExactSearch={true}
            handleToggleTable={this.handleToggleTable}
            handleSelectAssetSources={this.handleSelectAssetSources}
            isRoleAssetManager={isRoleAssetManager}
          />
        </TabPanel>
      </div>
    );
  }
}
AmountOfAssetsGroupedByAssetGroup.contextType = AppContext
export default AmountOfAssetsGroupedByAssetGroup;
