import React, {Component} from "react";
import {Button, Card, FormControl, Grid, InputLabel, MenuItem, Select, TextField,} from "@material-ui/core";
import MaterialTable, {MTableToolbar} from "material-table";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "react-toastify/dist/ReactToastify.css";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import TreeDepartmentDialog from "../AmountOfAssetsGroupedByAssetGroup/TreeDepartmentDialog";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {getListManagementDepartmentOrg} from "app/views/Asset/AssetService";
import {searchByPage as searchByPageAssetSource} from "../../AssetSource/AssetSourceService";
import OptionSummaryReport from "../Components/OptionSummaryReport"
import {appConst} from "../../../appConst";
import CardContent from "@material-ui/core/CardContent";
import ViewAssetByDepartmentDialog from "./ViewAssetByDepartmentDialog";


class ComponentAmountOfAssetsByAssetGroupTable extends Component {
    render() {
        let {
            t,
            i18n,
            columns,
            handleChange,
            listTemporary,
            listManageAccountant,
            handleSelectAssetSources,
            handleSelectUseDepartment,
            exportToExcel,
            handleSelectReceiverDepartment,
            handleDepartmentPopupClose,
            handleDialogClose,
            handleOKEditClose,
            selectManagementDepartment,
            selectOrganizationOrg,
            searchObjectManagementDepartment,
            searchObjectUseDepartment,
            searchObjectAssetSource,
            handleSetDataSelect,
            handleKeyUp,
            handleKeyDown,
            isShowExactSearch,
            isRoleAssetManager
        } = this.props
        let {
            managementDepartment,
            assetSource,
            shouldOpenDepartmentPopup,
            department,
            shouldOpenViewAssetByAssetGroupDialog,
            isTemporary,
            isManageAccountant,
            item,
            currentDepartment,
            itemList,
            isTSCD,
            listOrganizationOrg,
            organizationOrg,
            useDepartment,
            yearPutIntoUse,
            isExactSearch
        } = this.props.item;

        return (
            <div>
                <Grid className={"asset_department summary_report"}>
                    <Card>
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item md={3} sm={6} xs={12}>
                                    <FormControl fullWidth={true}>
                                        <InputLabel htmlFor="gender-simple">
                                            {t("Asset.filter_temporary")}
                                        </InputLabel>
                                        <Select
                                            isClearable={true}
                                            onChange={(isTemporary) =>
                                                handleChange(isTemporary, "isTemporary")
                                            }
                                            inputProps={{
                                                name: "isTemporary",
                                                id: "gender-simple",
                                            }}
                                        >
                                            {listTemporary.map((item) => {
                                                return (
                                                    <MenuItem key={item.name} value={item.value}>
                                                        {item.value != null ? item.name : "Tất cả"}
                                                    </MenuItem>
                                                );
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item md={3} sm={6} xs={12}>
                                    <FormControl fullWidth={true}>
                                        <InputLabel htmlFor="isManageAccountant">
                                            {t("Asset.filter_manage_accountant")}
                                        </InputLabel>
                                        <Select
                                            onChange={(isManageAccountant) =>
                                                handleChange(isManageAccountant, "isManageAccountant")
                                            }
                                            inputProps={{
                                                name: "isManageAccountant",
                                                id: "isManageAccountant",
                                            }}
                                        >
                                            {listManageAccountant.map((item) => {
                                                return (
                                                    <MenuItem key={item.name} value={item.value}>
                                                        {item.name}
                                                    </MenuItem>
                                                );
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item md={3} sm={6} xs={12}>
                                    <ValidatorForm>
                                        <AsynchronousAutocompleteSub
                                            label={t("Asset.filter_manage_department")}
                                            searchFunction={getListManagementDepartmentOrg}
                                            searchObject={searchObjectManagementDepartment}
                                            typeReturnFunction={"category"}
                                            defaultValue={managementDepartment}
                                            displayLable={"name"}
                                            value={managementDepartment}
                                            onSelect={selectManagementDepartment}
                                            disabled={isRoleAssetManager}
                                        />
                                    </ValidatorForm>
                                </Grid>
                                <Grid item md={3} xs={4}>
                                   <ValidatorForm>
                                        <TextValidator
                                            fullWidth
                                            label={t("Asset.yearPutIntoUse")}
                                            value={yearPutIntoUse}
                                            name="yearPutIntoUse"
                                            type="number"
                                            onKeyUp={handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            validators={[
                                                "matchRegexp:^[1-9][0-9]{3}$", 
                                                `minNumber: 1900`, 
                                                `maxNumber: ${new Date().getFullYear()}`
                                              ]}
                                            errorMessages={[
                                                t("general.yearOfManufactureError"),
                                                t("general.minYearDefault"),
                                                t("general.maxYearNow")
                                            ]}
                                        />
                                    </ValidatorForm>
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <ValidatorForm>
                                        <AsynchronousAutocompleteSub
                                            label={t("AggregateAssetsByAssetGroup.filterAsserOrigin")}
                                            searchFunction={searchByPageAssetSource}
                                            searchObject={searchObjectAssetSource}
                                            displayLable={"name"}
                                            typeReturnFunction="category"
                                            name="assetSource"
                                            value={assetSource}
                                            onSelect={handleSelectAssetSources}
                                        />
                                    </ValidatorForm>
                                </Grid>
                                <Grid item md={3} xs={12}>
                                   <ValidatorForm>
                                        <AsynchronousAutocompleteSub
                                            label={t("Asset.useDepartment")}
                                            searchFunction={getListManagementDepartmentOrg}
                                            searchObject={searchObjectUseDepartment}
                                            defaultValue={useDepartment}
                                            typeReturnFunction={"category"}
                                            displayLable={"name"}
                                            value={useDepartment}
                                            onSelect={handleSelectUseDepartment}
                                        />
                                   </ValidatorForm>
                                </Grid>
                                <Grid item md={3} sm={6} xs={12}>
                                    <Autocomplete
                                        className="mt-3"
                                        size="small"
                                        id="combo-box"
                                        options={listOrganizationOrg}
                                        value={organizationOrg}
                                        renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label={t("AggregateAssetsByAssetGroup.filterOrg")}
                                            value={organizationOrg}
                                        />
                                        )}
                                        getOptionLabel={(option) => option.name}
                                        getOptionSelected={(option, value) =>
                                        option.value === value.value
                                        }
                                        onChange={(event, value) => {
                                            selectOrganizationOrg(value);
                                        }}
                                    />
                                </Grid>
                                <Grid item md={3} sm={2} xs={12}>
                                    <Button
                                        className="mt-12"
                                        size="small"
                                        variant="contained"
                                        color="primary"
                                        onClick={exportToExcel}
                                    >
                                        {t("general.exportToExcel")}
                                    </Button>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <div>
                    {shouldOpenDepartmentPopup && (
                        <TreeDepartmentDialog
                            open={shouldOpenDepartmentPopup}
                            handleSelect={handleSelectReceiverDepartment}
                            selectedItem={department != null ? department : {}}
                            handleClose={handleDepartmentPopupClose}
                            t={t}
                            i18n={i18n}
                        />
                    )}
                    {shouldOpenViewAssetByAssetGroupDialog && (
                        <ViewAssetByDepartmentDialog
                            t={t}
                            i18n={i18n}
                            handleClose={handleDialogClose}
                            open={shouldOpenViewAssetByAssetGroupDialog}
                            handleOKEditClose={handleOKEditClose}
                            item={item}
                            isTSCD={isTSCD}
                            isTemporary={isTemporary}
                            isManageAccountant={isManageAccountant}
                            managementDepartment={managementDepartment}
                            assetSource={assetSource}
                            useDeprtment={currentDepartment}
                            organizationOrg={organizationOrg}
                            isExactSearch={isExactSearch}
                        />
                    )}
                </div>
                <Grid item md={12} xs={12}>
                    <div className="flex flex-end">
                        {isShowExactSearch &&
                            <OptionSummaryReport
                                checked={isExactSearch}
                                handleChange={() => this.props.handleToggleTable()}
                                label={appConst.optionExactSearch.XDDKP.name}
                                noPosition="true"
                                name="dd"
                            />
                        }
                    </div>
                </Grid>
                <MaterialTable
                    title={t("general.list")}
                    data={itemList}
                    columns={columns}
                    parentChildData={(row, rows) => {
                        return rows.find((a) => a.id === row.parentId);

                    }}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                        },
                    }}
                    options={{
                        sorting: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        draggable: false,
                        defaultExpanded: true,
                        rowStyle: (rowData) => ({
                            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "450px",
                        minBodyHeight: "450px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                    onSelectionChange={(rows) => {
                        this.data = rows;
                    }}
                />
            </div>
        );
    }

}

export default ComponentAmountOfAssetsByAssetGroupTable;
