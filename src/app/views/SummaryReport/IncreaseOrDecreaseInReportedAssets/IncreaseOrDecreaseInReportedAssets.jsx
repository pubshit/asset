import {
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import moment from "moment";
import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import { getByRoot } from "../../AssetGroup/AssetGroupService";
import { getTreeView as getAllDepartmentTreeView, } from "../../Department/DepartmentService";
import { exportToExcel, getIncreaseDecreaseReport, } from "./ReportIncreaseOrDecreaseAssetsService";
import ViewAssetByDepartmentDialog from "./ViewAssetByDepartmentDialog";
import viLocale from "date-fns/locale/vi"
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import {
  convertNumberPriceRoundUp,
  formatDateDto,
  formatDateDtoMore,
  getUserInformation, handleKeyDown,
  isValidDate,
  removeDartline
} from "app/appFunction";
import DateFnsUtils from "@date-io/date-fns";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContext from "app/appContext";
import { appConst } from "app/appConst";
import Scrollbar from "react-perfect-scrollbar";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { ValidatorForm } from "react-material-ui-form-validator";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
let { organization } = getUserInformation()
class ReportIncreaseOrDecreaseAssets extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    year: moment(new Date()).format("YYYY").toString(),
    keyword: "",
    status: [],
    assetGroupId: "",
    assetDepartmentId: "",
    rowsPerPage: 10000,
    page: 0,
    Asset: [],
    item: {},
    itemList: [],
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByDepartmentDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    fromToDate: moment().endOf("year"),
    fromDate: formatDateDtoMore(moment(new Date()).subtract(1, "months"), "start"),
    toDate: formatDateDtoMore(new Date()),
    listGroup: [],
    group: null,
    assetGroup: null,
    managementDepartment: null,
    managementDepartmentId: "",
    listManageDepartment: [],
    isTemporary: null,
    isManageAccountant: null,
    assetSource: null,
    useDepartment: null,
    ranger: [0, 500],
    rangerValue: 500,
    organizationOrg: null,
    listOrganizationOrg: []
  };
  treeView = null;

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let { departmentId, status, page, rowsPerPage } = this.state;
    let { setPageLoading } = this.context
    setPageLoading(true)
    let { t } = this.props;
    var searchObject = {};
    if (departmentId != "all") {
      searchObject.departmentId = departmentId;
    }
    searchObject.status = status ? status : null;
    searchObject.fromToDate = this.state.fromToDate;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromDate = this.state.fromDate;
    searchObject.toDate = this.state.toDate;
    searchObject.assetGroupId = this.state.assetGroupId;
    searchObject.managementDepartmentId = this.state.useDepartment?.id;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.year = this.state.year;
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id

    getIncreaseDecreaseReport(searchObject).then(({ data }) => {
      this.setState({
        itemList: data?.data?.length > 0 ? [...data.data] : [],
        totalElements: data?.total
      });
    }).catch(() => {
      toast.error(t("general.error"))
    }).finally(() => {
      setPageLoading(false)
    })
      ;
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    let { organization } = getUserInformation();
    this.setState({
      organizationOrg: organization?.org
    }, () => {
      this.updatePageData();
    })
  }

  componentWillMount() {
    var searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 1000;
    getByRoot(searchObject).then(({ data }) => {
      this.setState({ listGroup: [...data.content] });
    });

  }

  handleFormSubmit = () => { };

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  search() {
    this.setState({ page: 0 }, function () {
      let { setPageLoading } = this.context
      setPageLoading(true)
      let { t } = this.props;
      let { departmentId, status, page, rowsPerPage } = this.state;
      let searchObject = {};
      if (departmentId !== "all") {
        searchObject.departmentId = departmentId;
      }
      searchObject.status = status ? status : null;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromDate = formatDateDto(this.state.fromDate);
      searchObject.toDate = formatDateDto(this.state.toDate);
      searchObject.assetGroupId = this.state.assetGroupId;
      searchObject.managementDepartmentId = this.state.useDepartment?.id;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      searchObject.year = this.state.year;
      if (this.state.rangerValue) {
        searchObject.rangerValue = this.state.rangerValue * 1000000;
      }
      if (this.state.isInverted != null) {
        searchObject.isInverted = this.state.isInverted;
      }
      searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id

      let start = page * rowsPerPage;
      let end = (page + 1) * rowsPerPage;
      getIncreaseDecreaseReport(searchObject).then(({ data }) => {
        if (data?.code === appConst.CODE.SUCCESS) {
          this.setState({
            itemList: data?.data?.length > 0 ? [...data.data] : [],
            totalElements: data?.total
          });
        } else {
          toast.warn(data?.message)
          this.setState({
            itemList: [],
            totalElements: 0
          });
        }
      }).catch(() => {
        toast.error(t("general.error"))
      }).finally(() => {
        setPageLoading(false)
      })
    });
  }
  getAssetBydepartment = (event, departmentId) => {
    if (departmentId) {
      this.setState({ departmentId: departmentId }, function () {
        this.search();
      });
    }
  };
  handleDateChange = (dateValue, name) => {
    this.setState({
      [name]: dateValue,
    },
      () => { isValidDate(dateValue) && this.search() }
    );
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () { }
      );
    });
  };
  // end Tree view

  handleChangeSlider = (event, newValue) => {
    if (newValue >= 10000) {
      newValue = 10000;
    }
    this.setState({ rangerValue: newValue }, () => {
      // this.search();
    });
  };
  handleDeleteButtonClick = (item) => {
    this.setState({
      item: item,
      shouldOpenDepartmentPopup: true,
    });
  };

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        fromDate: fromDate ? formatDateDto(fromDate) : "",
        toDate: toDate ? formatDateDto(toDate) : "",
        orgId: this.state.organizationOrg?.id || organization?.org?.id
      };
      const res = await exportToExcel(searchObject);
      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "IncreaseOrDecreaseInReportedAssets.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
    }, () => {
      this.search();
    })
  }

  selectGroup = (item) => {
    this.setState(
      { assetGroupId: item ? item.id : "", assetGroup: item ? item : null },
      function () {
        this.search();
      }
    );
  };
  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
  };

  romanize = (num) => {
    let lookup = {
      "M": 1000,
      "CM": 900,
      "D": 500,
      "CD": 400,
      "C": 100,
      "XC": 90,
      "L": 50,
      "XL": 40,
      "X": 10,
      "IX": 9,
      "V": 5,
      "IV": 4,
      "I": 1
    }, roman = '', i;
    for (i in lookup) {
      while (num >= lookup[i]) {
        roman += i;
        num -= lookup[i];
      }
    }
    return roman;
  }

  valueLabelFormat = (value) => {
    return `${value}`;
  };

  renderUI = (data, prefix = "") => {

    // Render Product category
    return data.map((item, index) => {
      let stt = prefix
        ? prefix + "." + this.romanize(index + 1)
        : this.romanize(index + 1)
      let endTermQuantity = (item.soLuongDauKy || 0)
        + (item.soLuongTangTrongKy || 0)
        - (item.soLuongGiamTrongKy || 0);
      let endTermOriginalCost = (item.nguyenGiaDauKy || 0)
        + (item.nguyenGiaTangTrongKy || 0)
        - (item.nguyenGiaGiamTrongKy || 0);

      return <Fragment key={index}>
        <TableRow>
          <TableCell className="font-weight-bold" align="center">
            {item?.assetGroupCode || ""}
          </TableCell>
          <TableCell className="font-weight-bold" align="left">
            {item?.assetGroupName || ""}
          </TableCell>
          <TableCell align="center">
            {item.soLuongDauKy || 0}
          </TableCell>
          <TableCell align="right">
            {convertNumberPriceRoundUp(item.nguyenGiaDauKy) || 0}
          </TableCell>
          <TableCell align="center">
            {item.soLuongTangTrongKy || 0}
          </TableCell>
          <TableCell align="right">
            {convertNumberPriceRoundUp(item.nguyenGiaTangTrongKy) || 0}
          </TableCell>
          <TableCell align="center">
            {item.soLuongGiamTrongKy || 0}
          </TableCell>
          <TableCell align="right">
            {convertNumberPriceRoundUp(item.nguyenGiaGiamTrongKy) || 0}
          </TableCell>
          <TableCell align="center">
            {endTermQuantity}
          </TableCell>
          <TableCell align="right">
            {convertNumberPriceRoundUp(endTermOriginalCost) || 0}
          </TableCell>
        </TableRow>

        {/* Render product*/}
        {item?.products != null && item.products.map((entityProduct, entityProductIndex) => {
          let productEndTermQuantity = (entityProduct.soLuongDauKy || 0)
            + (entityProduct.soLuongTangTrongKy || 0)
            - (entityProduct.soLuongGiamTrongKy || 0)
          let productEndTermOriginalCost = (entityProduct.nguyenGiaDauKy || 0)
            + (entityProduct.nguyenGiaTangTrongKy || 0)
            - (entityProduct.nguyenGiaGiamTrongKy || 0);
          return (
            <TableRow key={data?.length + entityProductIndex}>
              <TableCell className="border-none">
                {entityProduct?.productCode}
              </TableCell>
              <TableCell align="left">
                {entityProduct?.productName || ""}
              </TableCell>
              <TableCell align="center">
                {entityProduct.soLuongDauKy || 0}
              </TableCell>
              <TableCell align="right">
                {convertNumberPriceRoundUp(entityProduct.nguyenGiaDauKy) || 0}
              </TableCell>
              <TableCell align="center">
                {entityProduct.soLuongTangTrongKy || 0}
              </TableCell>
              <TableCell align="right">
                {convertNumberPriceRoundUp(entityProduct.nguyenGiaTangTrongKy) || 0}
              </TableCell>
              <TableCell align="center">
                {entityProduct.soLuongGiamTrongKy || 0}
              </TableCell>
              <TableCell align="right">
                {convertNumberPriceRoundUp(entityProduct.nguyenGiaGiamTrongKy) || 0}
              </TableCell>
              <TableCell align="center">
                {productEndTermQuantity || 0}
              </TableCell>
              <TableCell align="right">
                {convertNumberPriceRoundUp(productEndTermOriginalCost) || 0}
              </TableCell>
            </TableRow>
          )
        })}
        {item?.childGroups && this.renderUI(item?.childGroups, stt)}
      </Fragment>
    })
  }
  render() {
    const { t, i18n } = this.props;
    let {
      itemList,
      item,
      shouldOpenViewAssetByDepartmentDialog,
      assetDepartmentId,
      assetSource,
      assetGroup,
      useDepartment,
      year,
      rangerValue,
      fromDate,
      toDate,
      organizationOrg,
      listOrganizationOrg
    } = this.state;
    let searchObject = { pageIndex: 0, pageSize: 1000000 }

    let { organization } = getUserInformation();
    let TitlePage = t("Dashboard.summary_report.report_increase_or_decrease_assets");

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.summary_report.title") },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2}>
        </Grid>
        <Grid container spacing={2} justifyContent="flex-start">
          <Grid item md={2} sm={2} xs={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span style={{ color: "red" }}> </span>
                    {t("WarehouseInventory.fromDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"fromDate"}
                value={fromDate}
                maxDate={new Date(toDate)}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                onChange={(date) => this.handleDateChange(date, "fromDate")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={2} xs={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span style={{ color: "red" }}> </span>
                    {t("WarehouseInventory.toDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"toDate"}
                value={toDate}
                minDate={new Date(fromDate)}
                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                maxDateMessage={t("general.maxDateMessage")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                onChange={(date) => this.handleDateChange(date, "toDate")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} xs={12}>
            <ValidatorForm onSubmit={() => { }}>
              <AsynchronousAutocompleteSub
                label={t("WarehouseInventory.filterUnit")}
                searchFunction={searchByPageOrg}
                searchObject={organization?.org?.id}
                defaultValue={organizationOrg}
                displayLable={"name"}
                typeReturnFunction="category"
                value={organizationOrg}
                onSelect={this.selectOrganizationOrg}
              />
            </ValidatorForm>
          </Grid>
          <Grid item md={2} sm={6} xs={12}>
            <Button
              className="mt-12"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-12">
          <div>
            {shouldOpenViewAssetByDepartmentDialog &&
              this.state.assetDepartmentId && (
                <ViewAssetByDepartmentDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenViewAssetByDepartmentDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  assetSourceId={assetSource?.id}
                  departmentId={assetDepartmentId}
                />
              )}
          </div>

          <div className="MuiPaper-elevation2 MuiPaper-rounded overflow-auto">
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center" rowSpan="2">STT</TableCell>
                    <TableCell align="center" rowSpan="2"> Tài sản </TableCell>
                    <TableCell align="center" colSpan="2">Số đầu kỳ </TableCell>
                    <TableCell align="center" colSpan="2">Số tăng trong kỳ</TableCell>
                    <TableCell align="center" colSpan="2">Số giảm trong kỳ</TableCell>
                    <TableCell align="center" colSpan="2">Số cuối kỳ</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">Số lượng</TableCell>
                    <TableCell align="center">Nguyên giá</TableCell>
                    <TableCell align="center">Số lượng</TableCell>
                    <TableCell align="center">Nguyên giá</TableCell>
                    <TableCell align="center">Số lượng</TableCell>
                    <TableCell align="center">Nguyên giá</TableCell>
                    <TableCell align="center">Số lượng</TableCell>
                    <TableCell align="center">Nguyên giá</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.renderUI(itemList).map(item => item)}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </Grid>
      </div>
    );
  }
}

ReportIncreaseOrDecreaseAssets.contextType = AppContext;
export default ReportIncreaseOrDecreaseAssets;
