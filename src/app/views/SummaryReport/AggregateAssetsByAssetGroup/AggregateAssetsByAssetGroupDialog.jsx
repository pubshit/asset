import React, { Component } from 'react'
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch,
  DialogActions,
} from '@material-ui/core'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import {
  createAsset,
  updateAsset,
} from './AssetService'
import { searchByPage as productSearchByPage } from '../Product/ProductService'
import { searchByPage as assetGroupSearchByPage } from '../AssetGroup/AssetGroupService'
import { searchByPage as manufacturerSearchByPage } from '../CommonObject/CommonObjectService'
import { searchByPage as supplyUnitSearchByPage } from '../Supplier/SupplierService'
import { searchByPage as assetSourceSearchByPage } from '../AssetSource/AssetSourceService'
import { searchByPage as statusSearchByPage } from '../AssetStatus/AssetStatusService'
import { getAll as departmentSearchByPage } from '../Department/DepartmentService'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import DialogTitle from '@material-ui/core/DialogTitle'
import { generateRandomId } from 'utils'
import Draggable from 'react-draggable'
import Paper from '@material-ui/core/Paper'
import DialogContent from '@material-ui/core/DialogContent'
import AsynchronousAutocomplete from '../utilities/AsynchronousAutocomplete'
import ScrollableTabsButtonForce from './ScrollableTabsButtonForce'
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  )
}
class AssetEditorDialog extends Component {
  state = {
    name: '',
    code: '',
    qrcode: '',
    madeIn: '',
    model: '',
    usedTime: 0,
    warrantyMonth: 0,
    yearOfManufacture: 0,
    serialNumber: '',
    useDepartment: null,
    managementDepartment: null,
    installationLocation: '',
    supplyUnit: null,
    dayStartedUsing: new Date(),
    assetSource: null,
    status: null,
    originalCost: 0,
    carryingAmount: 0,
    warrantyExpiryDate: new Date(),
    depreciationDate: new Date(),
    depreciationPeriod: 0,
    depreciationRate: 0,
    accumulatedDepreciationAmount: 0,
    product: null,
    assetGroup: null,
    manufacturer: null,
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    })
  }

  handleChange = (event, source) => {
    event.persist()
    if (source === 'switch') {
      this.setState({ isActive: event.target.checked })
      return
    }
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleFormSubmit = () => {
    let { id } = this.state
    let { code } = this.state
    //Nếu trả về false là code chưa sử dụng có thể dùng
    if (!id) {
      createAsset({
        ...this.state,
      }).then(() => {
        this.props.handleOKEditClose()
      })
    } else {
      updateAsset({
        ...this.state,
      }).then(() => {
        this.props.handleOKEditClose()
      })
    }
  }

  componentWillMount() {
    let { open, handleClose, item } = this.props
    this.setState(
      {
        ...this.props.item,
      },
      function () {}
    )
  }

  // selectProduct = (productSelected) => {
  //   this.setState({ product: productSelected }, function () {
  //     if (!this.state.name || this.state.name.length <= 0) {
  //       this.setState({ name: this.state.product.name });
  //     }
  //   });
  // }
  openSelectProductPopup = () => {
    this.setState({
      shouldOpenSelectProductPopup: true,
    })
  }

  openSelectSupplyPopup = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: true,
    })
  }

  handleSelectSupplyPopupClose = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: false,
    })
  }

  handleSelectSupply = (item) => {
    this.setState({ supplyUnit: item, name: item.name }, function () {
      this.handleSelectSupplyPopupClose()
    })
  }

  handleSelectProductPopupClose = () => {
    this.setState({
      shouldOpenSelectProductPopup: false,
    })
  }
  handleSelectProduct = (item) => {
    this.setState({ product: item, name: item.name }, function () {
      this.handleSelectProductPopupClose()
    })
  }

  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    })
  }

  handleSelectDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    })
  }
  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.text, text: item.text }
    this.setState({ managementDepartment: department }, function () {
      this.handleSelectDepartmentPopupClose()
    })
  }

  selectAssetGroup = (assetGroupSelected) => {
    this.setState({ assetGroup: assetGroupSelected }, function () {
    })
  }

  selectManufacturer = (manufacturerSelected) => {
    this.setState({ manufacturer: manufacturerSelected }, function () {})
  }

  selectAssetSource = (assetSourceSelected) => {
    this.setState({ assetSource: assetSourceSelected }, function () {})
  }

  selectStatus = (statusSelected) => {
    this.setState({ status: statusSelected }, function () {})
  }

  selectSupplyUnit = (supplyUnitSelected) => {
    this.setState({ supplyUnit: supplyUnitSelected }, function () {})
  }

  selectUseDepartment = (departmentSelected) => {
    this.setState({ useDepartment: departmentSelected }, function () {})
  }

  selectManagementDepartment = (departmentSelected) => {
    this.setState({ managementDepartment: departmentSelected }, function () {})
  }

  componentDidMount() {}

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props
    let {
      id,
      name,
      code,
      qrcode,
      madeIn,
      serialNumber,
      status,
      model,
      warrantyMonth,
      usedTime,
      originalCost,
      carryingAmount,
      useDepartment,
      managementDepartment,
      installationLocation,
      yearOfManufacture,
      warrantyExpiryDate,
      depreciationDate,
      supplyUnit,
      dayStartedUsing,
      depreciationPeriod,
      depreciationRate,
      accumulatedDepreciationAmount,
      product,
      assetGroup,
      assetSource,
      manufacturer,
    } = this.state

    let searchObject = { pageIndex: 0, pageSize: 1000000 }

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            <span className="mb-20">{t('general.saveUpdate')}</span>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1} className="">
              <ScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                selectAssetGroup={this.selectAssetGroup}
                selectProduct={this.selectProduct}
                handleChange={this.handleChange}
                // selectSupply={this.selectSupply}
                selectAssetSource={this.selectAssetSource}
                selectUseDepartment={this.selectUseDepartment}
                // selectSupplyUnit={this.selectSupplyUnit}
                selectManufacturer={this.selectManufacturer}
                selectStatus={this.selectStatus}
                handleDateChange={this.handleDateChange}

                openSelectSupplyPopup={this.openSelectSupplyPopup}
                handleSelectSupply={this.handleSelectSupply}
                handleSelectSupplyPopupClose={
                  this.handleSelectSupplyPopupClose
                }

                openSelectProductPopup={this.openSelectProductPopup}  
                handleSelectProduct={this.handleSelectProduct}
                handleSelectProductPopupClose={
                  this.handleSelectProductPopupClose
                }
                openSelectDepartmentPopup={this.openSelectDepartmentPopup}
                handleSelectManagementDepartment={
                  this.handleSelectManagementDepartment
                }
                handleSelectDepartmentPopupClose={
                  this.handleSelectDepartmentPopupClose
                }
              />
            </Grid>

            {/* <Grid container spacing={1}>
              <Grid item md={5} sm={12} xs={12}>
              </Grid>

              <Grid item md={2} sm={12} xs={12}>
              </Grid>

              <Grid item md={5} sm={12} xs={12}>
                <AsynchronousAutocomplete label={t("Asset.status")}
                  searchFunction={statusSearchByPage}
                  searchObject={searchObject}
                  defaultValue={status}
                  displayLable={'name'}
                  value={status}
                  onSelect={this.selectStatus}
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                />
              </Grid>
            </Grid> */}
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-36"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>

              <Button
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    )
  }
}

export default AssetEditorDialog
