import React from "react";
import { AppBar, Box, Tab, Tabs, Typography } from "@material-ui/core";
import {
  exportExcelForFixedAssetsByAssetGroup,
  exportExcelForInstrumentsAndToolsByAssetGroup,
  getListFixedAsset,
  getListIat,
  getSumaryAsset,
  getSumaryFixedAsset,
  getSumaryIat,
} from "./AggregateAssetsByAssetGroupService";
import { Breadcrumb } from "egret";
import TreeItem from "@material-ui/lab/TreeItem";
import { Helmet } from "react-helmet";
import { convertNumberPrice, getTheHighestRole, getUserInformation } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentAggregateAssetsByAssetGroupTable from "./ComponentAggregateAssetsByAssetGroupTable";
import OptionSummaryReport from "../Components/OptionSummaryReport";
import AmountOfAssetsGroupedByAssetGroup from "../AmountOfAssetsGroupedByAssetGroup/AmountOfAssetsGroupedByAssetGroupV2";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import { functionExportToExcel } from "../../../appFunction";
import {TabPanel} from "../../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});


class AssetTable extends React.Component {
  constructor(props) {
    super(props);
    this.keyPress = this.keyPress.bind(this);
  }

  state = {
    keyword: "",
    status: [],
    assetGroup: null,
    rowsPerPage: 10,
    page: 0,
    assetGroupId: "",
    assetGroupTreeView: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    managementDepartment: null,
    useDepartment: null,
    originalCost: null,
    assetSource: null,
    store: null,
    tabValue: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD,
    isTSCD: true,
    optionSummaryReport: {
      name: appConst.optionSummaryReport.TDCT.name,
      code: appConst.optionSummaryReport.TDCT.code
    },
    organizationOrg: null,
    listOrganizationOrg: []
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          statusIndex: null,
          assetGroup: {},
          assetGroupId: "",
          isTSCD: true,
          yearPutIntoUse: null,
          originalCost: null
        },
        () => {
          this.getSumary();
          this.updatePageData();
        }
      );
    }
    if (appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          statusIndex: null,
          assetGroup: {},
          assetGroupId: "",
          isTSCD: false,
          yearPutIntoUse: null,
          originalCost: null
        },
        () => {
          this.getSumary();
          this.updatePageData();
        }
      );
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  getSearchObject = () => {
    let { organization } = getUserInformation();
    let {
      useDepartment,
      keyword,
      originalCost,
      yearPutIntoUse,
      assetSource,
      store,
      assetGroupId,
    } = this.state;
    var searchObject = {};

    if (assetGroupId && assetGroupId !== "all") {
      searchObject.assetGroupId = this.state.assetGroupId;
    }

    searchObject.assetStatusIds = this.state?.status ? this.state.status?.map(i => i?.id).join(",") : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;

    if (keyword && keyword.trim().length > 0) searchObject.keyword = keyword.trim();
    searchObject.managementDepartmentId = this.state.managementDepartment
      ? this.state.managementDepartment.id
      : "";
    if (useDepartment) searchObject.useDepartmentId = useDepartment.id;
    if (originalCost) searchObject.originalCost = originalCost;
    if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
    if (assetSource && assetSource.id) searchObject.assetSourceId = assetSource.id;
    if (store && store.id) searchObject.storeId = store.id;
    searchObject.assetClass = this.state.isTSCD ? appConst.assetClass.TSCD
      : appConst.assetClass.CCDC;
    return searchObject;
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    var searchObject = this.getSearchObject();
    try {
      let dataTable;
      if (this.state.isTSCD) {
        let responseTable = await getListFixedAsset(searchObject);
        dataTable = responseTable?.data?.data;
      } else {
        let responseTable = await getListIat(searchObject);
        dataTable = responseTable?.data?.data;
      }
      this.setState({
        itemList: dataTable?.content?.length > 0 ? [...dataTable?.content] : [],
        totalElements: dataTable?.totalElements,
      })
    } catch (error) {

    } finally {
      setPageLoading(false)
    }
  };

  getSumary = async () => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    var searchObject = this.getSearchObject();

    try {
      let data;
      if (this.state.isTSCD) {
        let response = await getSumaryFixedAsset(searchObject);
        data = response?.data?.data;
      } else {
        let response = await getSumaryIat(searchObject);
        data = response?.data?.data;
      }
      this.setState({
        totalAsset: data?.quantity,
        totalOriginalCost: data?.totalOriginalCost,
        totalCarryingAmount: data?.totalCarryingAmount,
      })
    } catch (error) {

    } finally {
      setPageLoading(false)
    }
  };

  search = () => {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    });
  }

  getTreeView = async () => {
    let { t } = this.props;
    let { organization } = getUserInformation();
    let objectSearch = {
      assetClass: this.state.isTSCD ? appConst.assetClass.TSCD : appConst.assetClass.CCDC,
      orgId: this.state.organizationOrg?.id || organization?.org?.id
    }
    try {
      let res = await getSumaryAsset(objectSearch)
      if (res?.data?.code === appConst.CODE.SUCCESS) {
        let assetGroupTreeView = {
          id: "all",
          name: this.state.isTSCD
            ? t("SummaryByMedicalEquipment.fixedAssets")
            : t("SummaryByMedicalEquipment.instrumentTools"),
          children: res?.data?.data,
        };
        this.setState({ assetGroupTreeView: assetGroupTreeView }, function () { });
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    }
  }

  componentDidMount() {
    let { t } = this.props;
    let { organization } = getUserInformation();
    
    searchByPageOrg(organization?.org?.id).then((data) => {
      this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
    }).catch(() => {
      toast.error(t("toastr.error"))
    })
    this.getSumary();
    this.updatePageData();
    this.getTreeView()

  }

  componentWillMount() {
    let { isRoleAssetManager, departmentUser } = getTheHighestRole();
    if (isRoleAssetManager) {
      this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
    };
   }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.tabValue !== prevState.tabValue) {
      this.getTreeView()
    }
  }

  selectManagementDepartment = (item) => {
    this.setState({ managementDepartment: item }, function () {
      this.search();
      this.getSumary();
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked === false) {
        selectAllItem = false;
      }
      if (Asset[i].id === item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };
  handleSelectAllClick = () => {
    let { Asset } = this.state;
    for (var i = 0; i < Asset.length; i++) {
      Asset[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, Asset: Asset });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  selectStatus = (statusSelected) => {
    this.setState({ status: statusSelected }, function () {
      this.search();
      this.getSumary();
    });
  };

  selectAssetGroup = (assetGroupSelected) => {
    this.setState({ assetGroup: assetGroupSelected }, function () {
      this.search();
      this.getSumary();
    });
  };

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
      managementDepartment: null,
      useDepartment: null,
      assetSource: null,
      store: null,
      assetGroup: null,
      assetGroupId: null
    }, () => {
      this.getSumary();
      this.search();
      this.getTreeView();
    })
  }

  converAssetStatus = (dataStatus) => {
    let listStatus = [];
    dataStatus?.map(status => {
      listStatus.push(status?.id)
    })
    return listStatus.toString();
  }

  exportToExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let {
      useDepartment,
      keyword,
      originalCost,
      yearPutIntoUse,
      assetSource,
      store,
      assetGroupId,
    } = this.state;
    let { organization } = getUserInformation();
    var searchObject = {};
    if (assetGroupId && assetGroupId !== "all") {
      searchObject.assetGroupId = this.state.assetGroupId;
    }
    searchObject.assetStatusIds = this.state.status ? this.converAssetStatus(this.state.status) : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    searchObject.keyword = keyword.trim();
    searchObject.useDepartmentId = useDepartment?.id;
    searchObject.originalCost = originalCost;
    searchObject.yearPutIntoUse = yearPutIntoUse;
    searchObject.assetSourceId = assetSource?.id;
    searchObject.storeId = store?.id;
    searchObject.managementDepartmentId = this.state.managementDepartment?.id
    searchObject.assetClass = this.state.isTSCD
      ? appConst.assetClass.TSCD
      : appConst.assetClass.CCDC

    try {
      setPageLoading(true);
      const exportFuntion = this.state.isTSCD
        ? exportExcelForFixedAssetsByAssetGroup
        : exportExcelForInstrumentsAndToolsByAssetGroup;
      await functionExportToExcel(
        exportFuntion,
        searchObject,
        "AggregateAssetsGroupedByAssetGroup.xlsx"
      )
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  getAssetBydepartment = (event, value) => {
    this.setState({
      assetGroupId: value,
    }, () => {
      this.search();
      this.getSumary();
    })
  };

  selectUseDepartment = (selected) => {
    this.setState({ useDepartment: selected }, () => {
      this.search();
      this.getSumary();
    })
  }

  selectAssetSource = (selected) => {
    this.setState({ assetSource: selected }, () => {
      this.search();
      this.getSumary();
    })
  }

  selectStore = (selected) => {
    this.setState({ store: selected }, () => {
      this.search();
      this.getSumary();
    })
  }

  keyPress(e, value, nameState) {
    if (e.key === appConst.KEY.ENTER) {
      this.setState({ [nameState]: value }, () => this.search())
    }
  }

  selectYear = (value) => {
    if (value) {
      this.setState({ yearPutIntoUse: value.year }, () => this.search())
    } else {
      this.setState({ yearPutIntoUse: value }, () => this.search())
    }
  }

  handleKeyDown = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
      this.getSumary();
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value || e.target.value === null) {
      this.search();
      this.getSumary()
    };
  };

  handleSetDataSelect = (data, name) => {
    this.setState({
      [name]: data
    });
  };

  handleToggle = () => {
    this.setState({
      optionSummaryReport: {
        name: this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? appConst.optionSummaryReport.TDTH.name : appConst.optionSummaryReport.TDCT.name,
        code: this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? appConst.optionSummaryReport.TDTH.code : appConst.optionSummaryReport.TDCT.code
      },
    }, () => {
      this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code && this.updatePageData()
    })
  }

  render() {
    const { t, i18n } = this.props;
    const { tabValue, rowsPerPage, page, optionSummaryReport, isTSCD, organizationOrg } = this.state;
    let { organization } = getUserInformation();
    let { isRoleAdmin, isRoleAssetManager } = getTheHighestRole();
    const currentDepartment = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
    let searchObject = { pageIndex: 0, pageSize: 10000000 };
    let searchObjectAssetSource = { pageIndex: 0, pageSize: 10000000, orgId: organizationOrg?.id || organization?.org?.id, };
    let searchObjectStore = {
      pageIndex: 0,
      pageSize: 10000000,
      departmentId: currentDepartment?.id,
      orgId: organizationOrg?.id || organization?.org?.id,
    };
    let searchObjectManagementDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
      isAssetManagement: true
    }
    let searchObjectUseDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
    }
    let dateNow = new Date().getFullYear();
    let listYear = [];
    for (let i = dateNow; i >= 1970; i--) {
      listYear.push({ year: i });
    }
    const renderTree = (nodes) =>
      nodes.id ? (
        <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name === appConst.typeAssets.TSCD ||
          nodes.name === appConst.typeAssets.CCDC ?
          t(`AggregateAssetsByAssetGroup.${nodes.name}`) :
          nodes.name
        }>
          {Array.isArray(nodes.children)
            ? nodes.children.map((node) => renderTree(node))
            : null}
        </TreeItem>
      ) : null;
    let TitlePage = t(
      "Dashboard.summary_report.aggregate_assets_by_asset_group"
    );

    let columns = [
      {
        title: t("general.index"),
        field: "code",
        width: "50px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          textAlign: "center"
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: isTSCD ? t("Asset.code") : t("InstrumentsToolsInventory.code"),
        field: "code",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "120px",
          textAlign: "center"
        },
      },
      {
        title: isTSCD ? t("Asset.code") : t("InstrumentsToolsInventory.name"),
        field: "name",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "270px",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.model"),
        field: "model",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "150px",
          textAlign: "right"
        },
        render: (rowData) => rowData?.originalCost
          ? convertNumberPrice(rowData?.originalCost)
          : ""
      },
      {
        title: t("Asset.carryingAmountTable"),
        field: "carryingAmount",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "150px",
          textAlign: "right"
        },
        render: (rowData) => rowData?.carryingAmount
          ? convertNumberPrice(rowData?.carryingAmount)
          : ""
      },
      {
        title: t("Asset.status"),
        field: "statusName",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px",
          textAlign: "center"
        },
      },
      ...(!isTSCD ? [{
        title: t("general.Quantity"),
        field: "quantity",
        minWidth: 80,
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          textAlign: "center"
        },
      }] : []),
      {
        title: t("Asset.assetSource"),
        field: "assetSourceNames",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          minWidth: "200px",
        },
      },
      {
        title: t("Asset.store"),
        field: "storeName",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "7px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "200px",
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "7px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "200px",
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.summary_report.title"),
                path: "/summary_report/aggregate_assets_by_asset_group",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <div className="flex flex-end">
          <OptionSummaryReport
            checked={this.state.optionSummaryReport?.code === appConst.optionSummaryReport.TDTH.code}
            handleChange={() => this.handleToggle()}
            name="optionSumaryReport"
            label={this.state.optionSummaryReport?.name}
            noPosition={true}
          />
        </div>
        {optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ?
          <div>
            <AppBar position="static" color="default" className="mb-10">
              <Tabs
                className="tabsStatus"
                value={tabValue}
                onChange={this.handleChangeTabValue}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabTSCD")} />
                <Tab className="tab" label={t("AggregateAssetsByAssetGroup.tabCCDC")} />
              </Tabs>
            </AppBar>
            <TabPanel
              value={tabValue}
              index={appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD}
              className="mp-0"
            >
              <ComponentAggregateAssetsByAssetGroupTable
                t={t}
                i18n={i18n}
                item={this.state}
                selectManagementDepartment={this.selectManagementDepartment}
                searchObject={searchObject}
                selectStatus={this.selectStatus}
                selectUseDepartment={this.selectUseDepartment}
                selectAssetSource={this.selectAssetSource}
                searchObjectStore={searchObjectStore}
                searchObjectAssetSource={searchObjectAssetSource}
                searchObjectManagementDepartment={searchObjectManagementDepartment}
                searchObjectUseDepartment={searchObjectUseDepartment}
                selectStore={this.selectStore}
                listYear={listYear}
                keyPress={this.keyPress}
                selectYear={this.selectYear}
                getAssetBydepartment={this.getAssetBydepartment}
                renderTree={renderTree}
                columns={columns}
                handleChangePage={this.handleChangePage}
                setRowsPerPage={this.setRowsPerPage}
                selectOrganizationOrg={this.selectOrganizationOrg}
                exportToExcel={this.exportToExcel}
                handleSetDataSelect={this.handleSetDataSelect}
                handleKeyDown={this.handleKeyDown}
                handleKeyUp={this.handleKeyUp}
                isRoleAssetManager={isRoleAssetManager}
              />
            </TabPanel>
            <TabPanel
              value={tabValue}
              index={appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC}
              className="mp-0"
            >
              <ComponentAggregateAssetsByAssetGroupTable
                t={t}
                item={this.state}
                selectManagementDepartment={this.selectManagementDepartment}
                searchObject={searchObject}
                selectStatus={this.selectStatus}
                selectUseDepartment={this.selectUseDepartment}
                selectAssetSource={this.selectAssetSource}
                searchObjectStore={searchObjectStore}
                searchObjectAssetSource={searchObjectAssetSource}
                searchObjectManagementDepartment={searchObjectManagementDepartment}
                searchObjectUseDepartment={searchObjectUseDepartment}
                selectStore={this.selectStore}
                listYear={listYear}
                keyPress={this.keyPress}
                selectYear={this.selectYear}
                getAssetBydepartment={this.getAssetBydepartment}
                renderTree={renderTree}
                columns={columns}
                handleChangePage={this.handleChangePage}
                setRowsPerPage={this.setRowsPerPage}
                selectOrganizationOrg={this.selectOrganizationOrg}
                exportToExcel={this.exportToExcel}
                handleSetDataSelect={this.handleSetDataSelect}
                handleKeyDown={this.handleKeyDown}
                handleKeyUp={this.handleKeyUp}
                isRoleAssetManager={isRoleAssetManager}
              />
            </TabPanel>
          </div> :
          <AmountOfAssetsGroupedByAssetGroup t={this.props.t} isShowBreadcrumb={false} item={this.state} />}
      </div>
    );
  }
}

AssetTable.contextType = AppContext
export default AssetTable;
