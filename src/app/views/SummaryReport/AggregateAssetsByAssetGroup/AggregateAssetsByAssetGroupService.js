import axios from "axios";
import ConstantList from "../../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/summary_report" + ConstantList.URL_PREFIX;
const API_PATH_NEW =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/asset-group";

export const exportExcelForFixedAssetsByAssetGroup = (searchObject) => {
  return axios({
    method: "get",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/report/asset-group/fixed-assets/export-excel",
    params: searchObject,
    responseType: "blob",
  });
};
export const exportExcelForInstrumentsAndToolsByAssetGroup = (searchObject) => {
  return axios({
    method: "get",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/report/asset-group/instruments-and-tools/export-excel",
    params: searchObject,
    responseType: "blob",
  });
};
export const searchByPage = (searchObject) => {
  let url = API_PATH + "/getReportAssetGroupBySearch/searchByPage";
  return axios.post(url, searchObject);
};

export const getReportIatAssetGroupBySearchAndOrg = (searchObject) => {
  let url = API_PATH + "/iat/getReportAssetGroupBySearch/searchByPage";
  return axios.post(url, searchObject);
};

export const getTotalIatAssetGroupBySearchAndOrg = (searchObject) => {
  let url = API_PATH + "/iat/getTotalAssetGroupBySearch/searchByPage";
  return axios.post(url, searchObject);
};
export const getTotalFixedAssetGroupBySearchAndOrg = (searchObject) => {
  let url = API_PATH + "/getTotalAssetGroupBySearch/searchByPage";
  return axios.post(url, searchObject);
};
export const getSumaryAsset = (searchObject) => {
  let url = API_PATH_NEW;
  let config = {
    params: { ...searchObject },
  };
  return axios.get(url, config);
};

export const getSumaryFixedAsset = (searchObject) => {
  let url = API_PATH_NEW + "/fixed-assets/summary";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};

export const getListFixedAsset = (searchObject) => {
  let url = API_PATH_NEW + "/fixed-assets";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};

export const getListFixedAssetStatistics = (searchObject) => {
  let url = API_PATH_NEW + "/fixed-assets/statistics";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};

export const getSumaryIat = (searchObject) => {
  let url = API_PATH_NEW + "/instruments-and-tools/summary";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};

export const getListIat = (searchObject) => {
  let url = API_PATH_NEW + "/instruments-and-tools";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};

export const getListIatStatistics = (searchObject) => {
  let url = API_PATH_NEW + "/instruments-and-tools/statistics";
  let config = {
    params: { ...searchObject },
  };

  return axios.get(url, config);
};
