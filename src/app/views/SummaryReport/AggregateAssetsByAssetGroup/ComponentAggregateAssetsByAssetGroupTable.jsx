import { Button, Card, Grid, TablePagination, TextField } from "@material-ui/core";
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import MaterialTable, { MTableToolbar } from "material-table";
import { getListManagementDepartmentOrg } from "app/views/Asset/AssetService";
import { searchByPage as statusSearchByPage } from "../../AssetStatus/AssetStatusService";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import { getPageBySearchTextAndOrgStore } from "app/views/Store/StoreService";
import { appConst, STATUS_STORE } from "app/appConst";
import {convertNumberPrice, defaultPaginationProps, NumberFormatCustom} from "app/appFunction";
import CardContent from "@material-ui/core/CardContent";
import OptionSummaryReport from "../Components/OptionSummaryReport";
import CustomizedTreeView from "../../Asset/TreeViewAsset";

const styles = {
  positionRelative: {
    position: "relative"
  },
  positionAbsolue: {
    position: "absolute"
  },
  zIndex: {
    zIndex: 99
  },
  top30: {
    top: "30px"
  },
  right10: {
    right: "10px"
  }
}

class ComponentAggregateAssetsByAssetGroupTable extends Component {
  render() {

    let {
      t,
      selectManagementDepartment,
      searchObject,
      selectStatus,
      selectUseDepartment,
      selectAssetSource,
      searchObjectStore,
      searchObjectManagementDepartment,
      searchObjectUseDepartment,
      searchObjectAssetSource,
      selectStore,
      selectOrganizationOrg,
      getAssetBydepartment,
      columns,
      handleChangePage,
      setRowsPerPage,
      exportToExcel,
      isShowExactSearch,
      handleSetDataSelect,
      handleKeyDown,
      handleKeyUp,
      titleTree,
      isShowFilterStore,
      isRoleAssetManager
    } = this.props
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      assetGroup,
      assetGroupTreeView,
      status,
      managementDepartment,
      useDepartment,
      assetSource,
      store,
      tabValue,
      totalAsset,
      totalOriginalCost,
      totalCarryingAmount,
      organizationOrg,
      listOrganizationOrg,
      isExactSearch,
      yearPutIntoUse,
      originalCost,
      keyword
    } = this.props.item;
    searchObjectStore.isActive = STATUS_STORE.HOAT_DONG.code;
    return (
      <div>
        <Card elevation={3} className="mb-20">
          <CardContent>
            <ValidatorForm ref="form" onSubmit={() => { }}>
              <Grid container spacing={2} className="p-10 bg-white pt-0" alignItems="flex-end" style={{ borderRadius: "10px" }}>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("Asset.filter_manage_department")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectManagementDepartment}
                    typeReturnFunction={"category"}
                    defaultValue={managementDepartment}
                    displayLable={"name"}
                    value={managementDepartment}
                    onSelect={selectManagementDepartment}
                    disabled={isRoleAssetManager}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("general.filterStatus")}
                    searchFunction={statusSearchByPage}
                    multiple={true}
                    searchObject={searchObject}
                    defaultValue={status}
                    displayLable={"name"}
                    value={status}
                    onSelect={selectStatus}
                  />
                </Grid>

                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("AggregateAssetsByAssetGroup.filterUseDepartment")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectUseDepartment}
                    defaultValue={useDepartment}
                    typeReturnFunction={"category"}
                    displayLable={"name"}
                    value={useDepartment}
                    onSelect={selectUseDepartment}
                  />
                </Grid>

                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("AggregateAssetsByAssetGroup.filterAsserOrigin")}
                    searchFunction={searchByPageAssetSource}
                    searchObject={searchObjectAssetSource}
                    defaultValue={assetSource}
                    displayLable={"name"}
                    typeReturnFunction="category"
                    value={assetSource}
                    onSelect={selectAssetSource}
                  />
                </Grid>
                {!isShowFilterStore && (
                  <Grid item md={3} xs={12}>
                    <AsynchronousAutocomplete
                      label={t("AggregateAssetsByAssetGroup.filterStore")}
                      searchFunction={getPageBySearchTextAndOrgStore}
                      searchObject={searchObjectStore}
                      defaultValue={store}
                      displayLable={"name"}
                      value={store}
                      onSelect={selectStore}
                    />
                  </Grid>
                )}
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("AggregateAssetsByAssetGroup.labelInputNameOrCode")}
                    value={keyword}
                    name="keyword"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("AggregateAssetsByAssetGroup.originalCost")}
                    value={originalCost}
                    name="originalCost"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("AggregateAssetsByAssetGroup.labelInputYear")}
                    value={yearPutIntoUse}
                    name="yearPutIntoUse"
                    type="number"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                    validators={[
                      "matchRegexp:^[1-9][0-9]{3}$", 
                      `minNumber: 1900`, 
                      `maxNumber: ${new Date().getFullYear()}`
                    ]}
                    errorMessages={[
                      t("general.yearOfManufactureError"),
                      t("general.minYearDefault"),
                      t("general.maxYearNow")
                    ]}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="combo-box"
                    options={listOrganizationOrg}
                    value={organizationOrg}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label={t("AggregateAssetsByAssetGroup.filterOrg")}
                        value={organizationOrg}
                      />
                    )}
                    getOptionLabel={(option) => option.name}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    onChange={(event, value) => {
                      selectOrganizationOrg(value);
                    }}
                  />
                </Grid>
                <Grid item md={2} xs={4}>
                  <Button
                    className="mt-12"
                    variant="contained"
                    color="primary"
                    // style={{float: "right"}}
                    onClick={exportToExcel}
                  >
                    {t('general.exportToExcel')}
                  </Button>
                </Grid>
              </Grid>
            </ValidatorForm>
          </CardContent>
        </Card>
        <Grid container spacing={2} className={"mt-16"}>
          <Grid item md={3} xs={12}>
            <div className="asset_department treeview-bao-cao overflow-auto MuiPaper-elevation2">
              <div className="treeview-title">
                <b>{titleTree}</b>
              </div>
              {assetGroupTreeView && (
                <CustomizedTreeView
                  t={t}
                  defaultCollapseIcon={<ExpandMoreIcon />}
                  defaultExpanded={["all"]}
                  defaultExpandIcon={<ChevronRightIcon />}
                  onNodeSelect={getAssetBydepartment}
                  style={{ background: "white", borderRadius: "4px" }}
                  treeViewItem={assetGroupTreeView}
                />
              )}
            </div>
          </Grid>

          <Grid item md={9} xs={12} style={{ ...styles.positionRelative }}>
            <Grid item md={12} xs={12}>
              <div
                className="flex flex-end"
                style={{ ...styles.positionAbsolue, ...styles.zIndex, ...styles.right10, ...styles.top30 }}>
                {isShowExactSearch &&
                  <OptionSummaryReport
                    checked={isExactSearch}
                    handleChange={() => this.props.handleToggleTable()}
                    label={appConst.optionExactSearch.XDDKP.name}
                    noPosition="true"
                    name="dd"
                  />
                }
              </div>
            </Grid>
            <MaterialTable
              title={
                <div>
                  {
                    assetGroup !== null &&
                      assetGroup.id &&
                      assetGroup.id !== "all"
                      ? <h4>
                        <span className="text-red-italic mt-10">{assetGroup.name}</span>
                      </h4>
                      : <h4>
                        {tabValue === appConst.tabAggregateAssetsByAssetGroup.tabTSCD ? t("AggregateAssetsByAssetGroup.tabTSCD") : t("AggregateAssetsByAssetGroup.tabCCDC")}
                      </h4>
                  }
                  <div className="pr-10 flex">
                    <div className="mr-20">Tổng số lượng:
                      <span className="text-red-italic inputPrice"> {totalAsset || 0}</span>
                    </div>
                    <div className="mr-20">Tổng GTCL:
                      <span className="text-red-italic"> {convertNumberPrice(totalCarryingAmount || 0) || 0}</span>
                    </div>
                    <div >Tổng nguyên giá:
                      <span className="text-red-italic"> {convertNumberPrice(totalOriginalCost || 0) || 0}</span>
                    </div>
                  </div>
                </div>
              }
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                draggable: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                minBodyHeight: "435px",
                maxBodyHeight: "435px",
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              style={{overflow: "visible"}}
              className=""
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default ComponentAggregateAssetsByAssetGroupTable;
