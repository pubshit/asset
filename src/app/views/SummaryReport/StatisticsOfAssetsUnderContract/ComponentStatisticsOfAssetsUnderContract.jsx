import { Button, Card, Grid, TablePagination, TextField } from "@material-ui/core";
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import MaterialTable, { MTableToolbar } from "material-table";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { getListManagementDepartmentOrg } from "app/views/Asset/AssetService";
import { searchByPage as statusSearchByPage } from "../../AssetStatus/AssetStatusService";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import { getPageBySearchTextAndOrgStore } from "app/views/Store/StoreService";
import { appConst } from "app/appConst";
import {convertNumberPrice, defaultPaginationProps, NumberFormatCustom} from "app/appFunction";
import CardContent from "@material-ui/core/CardContent";
import CustomizedTreeView from "../../Asset/TreeViewAsset";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import ViewCustomMaterialTable from "./ViewCustomMaterialTable";


const styles = {
  positionRelative: {
    position: "relative"
  },
  positionAbsolute: {
    position: "absolute"
  },
  marginBottom60: {
    marginBottom: 60
  },
  top40: {
    top: 40
  },
  width80: {
    width: "80%"
  }
}


class ComponentStatisticsOfAssetsUnderContract extends Component {
  render() {

    let {
      t,
      selectManagementDepartment,
      searchObject,
      selectStatus,
      selectUseDepartment,
      selectAssetSource,
      searchObjectStore,
      searchObjectManagementDepartment,
      searchObjectUseDepartment,
      searchObjectAssetSource,
      selectStore,
      selectOrganizationOrg,
      columns,
      handleChangePage,
      setRowsPerPage,
      exportToExcel,
      handleSetDataSelect,
      handleKeyDown,
      handleKeyUp,
      titleTree,
      handleSelectAssetType,
      columnsTreeview,
      getRowData,
      setRowsPerPageContract,
      handleChangePageContract,
      isRoleAssetManager
    } = this.props
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      assetGroupTreeView,
      status,
      managementDepartment,
      useDepartment,
      assetSource,
      store,
      totalAsset,
      totalOriginalCost,
      totalCarryingAmount,
      organizationOrg,
      listOrganizationOrg,
      yearPutIntoUse,
      originalCost,
      keyword,
      assetType,
      fromContractYear,
      toContractYear,
      contractYear,
      rowPerPageContract,
      pageContract,
      totalElementContracts
    } = this.props.item;
    searchObjectStore.isActive = 1;
    let listAssetType = appConst.assetType.filter(x => x.code !== appConst.assetClass.VT);
    
    return (
      <div>
        <Card elevation={3} className="mb-20">
          <CardContent>
            <ValidatorForm ref="form" onSubmit={() => { }}>
              <Grid container spacing={2} className="p-10 bg-white pt-0" alignItems="flex-end" style={{ borderRadius: "10px" }}>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("general.filterStatus")}
                    searchFunction={statusSearchByPage}
                    multiple={true}
                    searchObject={searchObject}
                    defaultValue={status}
                    displayLable={"name"}
                    value={status}
                    onSelect={selectStatus}
                  />
                </Grid>
                
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("Asset.filter_manage_department")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectManagementDepartment}
                    typeReturnFunction={"category"}
                    defaultValue={managementDepartment}
                    displayLable={"name"}
                    value={managementDepartment}
                    onSelect={selectManagementDepartment}
                    disabled={isRoleAssetManager}
                  />
                </Grid>

                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("StatisticsOfAssetsUnderContract.filterUseDepartment")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectUseDepartment}
                    defaultValue={useDepartment}
                    typeReturnFunction={"category"}
                    displayLable={"name"}
                    value={useDepartment}
                    onSelect={selectUseDepartment}
                  />
                </Grid>

                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("StatisticsOfAssetsUnderContract.filterAsserOrigin")}
                    searchFunction={searchByPageAssetSource}
                    searchObject={searchObjectAssetSource}
                    defaultValue={assetSource}
                    displayLable={"name"}
                    typeReturnFunction="category"
                    value={assetSource}
                    onSelect={selectAssetSource}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("StatisticsOfAssetsUnderContract.filterStore")}
                    searchFunction={getPageBySearchTextAndOrgStore}
                    searchObject={searchObjectStore}
                    defaultValue={store}
                    displayLable={"name"}
                    value={store}
                    onSelect={selectStore}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("StatisticsOfAssetsUnderContract.originalCost")}
                    value={originalCost}
                    name="originalCost"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("StatisticsOfAssetsUnderContract.labelInputYear")}
                    value={yearPutIntoUse}
                    name="yearPutIntoUse"
                    type="number"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                    validators={[
                      "matchRegexp:^[1-9][0-9]{3}$", 
                      `minNumber: 1900`, 
                      `maxNumber: ${new Date().getFullYear()}`
                    ]}
                    errorMessages={[
                      t("general.yearOfManufactureError"),
                      t("general.minYearDefault"),
                      t("general.maxYearNow")
                    ]}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("StatisticsOfAssetsUnderContract.filterAssetType")}
                    searchFunction={() => {
                    }}
                    searchObject={{}}
                    listData={listAssetType}
                    displayLable={"name"}
                    value={assetType || null}
                    name="assetType"
                    onSelect={handleSelectAssetType}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                 {/* Từ ngày */}
                 <Grid item xs={12} sm={12} md={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("StatisticsOfAssetsUnderContract.dxFrom")}
                      format="dd/MM/yyyy"
                      value={fromContractYear ?? null}
                      onChange={(data) => handleSetDataSelect(
                          data,
                          "fromContractYear"
                      )}
                      KeyboardButtonProps={{ "aria-label": "change date", }}
                      maxDate={toContractYear ? new Date(toContractYear) : undefined}
                      minDateMessage={t("general.minDateMessage")}
                      maxDateMessage={toContractYear ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* Đến ngày */}
                <Grid item xs={12} sm={12} md={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("StatisticsOfAssetsUnderContract.dxTo")}
                      format="dd/MM/yyyy"
                      value={toContractYear ?? null}
                      onChange={(data) => handleSetDataSelect(
                          data,
                          "toContractYear"
                      )}
                      KeyboardButtonProps={{ "aria-label": "change date", }}
                      minDate={fromContractYear ? new Date(fromContractYear) : undefined}
                      minDateMessage={fromContractYear ? t("general.minDateToDate") : t("general.minYearDefault")}
                      maxDateMessage={t("general.maxDateMessage")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("StatisticsOfAssetsUnderContract.filterContractYear")}
                    value={contractYear}
                    name="contractYear"
                    type="number"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                    validators={[
                      "matchRegexp:^[1-9][0-9]{3}$", 
                      `minNumber: 1900`, 
                      `maxNumber: ${new Date().getFullYear()}`
                    ]}
                    errorMessages={[
                      t("general.yearOfManufactureError"),
                      t("general.minYearDefault"),
                      t("general.maxYearNow")
                    ]}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="combo-box"
                    options={listOrganizationOrg}
                    value={organizationOrg}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label={t("StatisticsOfAssetsUnderContract.filterOrg")}
                        value={organizationOrg}
                      />
                    )}
                    getOptionLabel={(option) => option.name}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    onChange={(event, value) => {
                      selectOrganizationOrg(value);
                    }}
                  />
                </Grid>
                <Grid item md={2} xs={4}>
                  <Button
                    className="mt-12"
                    variant="contained"
                    color="primary"
                    onClick={exportToExcel}
                  >
                    {t('general.exportToExcel')}
                  </Button>
                </Grid>
              </Grid>
            </ValidatorForm>
          </CardContent>
        </Card>
        <Grid container spacing={2} className={"mt-16"}>
          <Grid item md={4} xs={12}>
            <ViewCustomMaterialTable
              assetGroupTreeView={assetGroupTreeView}
              columnsTreeview={columnsTreeview}
              titleTree={titleTree}
              t={t}
              totalElements={totalElementContracts}
              rowsPerPage={rowPerPageContract}
              page={pageContract}
              handleChangePage={handleChangePageContract}
              setRowsPerPage={setRowsPerPageContract}
              getRowData={getRowData}
            />
          </Grid>

          <Grid item md={8} xs={12} >
            <MaterialTable
              title={
                <div>
                  <div className="pr-10 flex">
                    <div className="mr-20">Tổng số lượng:
                      <span className="text-red-italic inputPrice"> {totalAsset || 0}</span>
                    </div>
                    <div className="mr-20">Tổng GTCL:
                      <span className="text-red-italic"> {convertNumberPrice(totalCarryingAmount || 0) || 0}</span>
                    </div>
                    <div>Tổng nguyên giá:
                      <span className="text-red-italic"> {convertNumberPrice(totalOriginalCost || 0) || 0}</span>
                    </div>
                  </div>
                </div>
              }
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                draggable: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                minBodyHeight: "435px",
                maxBodyHeight: "435px",
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default ComponentStatisticsOfAssetsUnderContract;
