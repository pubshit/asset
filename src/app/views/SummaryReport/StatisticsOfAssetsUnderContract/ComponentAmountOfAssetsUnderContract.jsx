import React, {Component} from "react";
import {
    Button,
    Card,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TablePagination,
    TextField,
} from "@material-ui/core";
import MaterialTable, {MTableToolbar} from "material-table";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "react-toastify/dist/ReactToastify.css";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import TreeDepartmentDialog from "../AmountOfAssetsGroupedByAssetGroup/TreeDepartmentDialog";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {getListManagementDepartmentOrg} from "app/views/Asset/AssetService";
import {searchByPage as searchByPageAssetSource} from "../../AssetSource/AssetSourceService";
import CardContent from "@material-ui/core/CardContent";
import ViewAssetsUnderContractDialog from "./ViewAssetsUnderContractDialog";
import { appConst } from "app/appConst";
import { getPageBySearchTextAndOrgStore } from "../../Store/StoreService";
import {defaultPaginationProps} from "../../../appFunction";


class ComponentAmountOfAssetsUnderContract extends Component {
    render() {
        let {
            t,
            i18n,
            columns,
            handleChange,
            listTemporary,
            listManageAccountant,
            handleSelectAssetSources,
            handleSelectUseDepartment,
            exportToExcel,
            handleSelectReceiverDepartment,
            handleDepartmentPopupClose,
            handleDialogClose,
            handleOKEditClose,
            selectManagementDepartment,
            selectOrganizationOrg,
            searchObjectManagementDepartment,
            searchObjectUseDepartment,
            searchObjectAssetSource,
            handleSetDataSelect,
            handleKeyUp,
            handleKeyDown,
            setRowsPerPage,
            handleChangePage,
            searchObjectStore,
            selectStore,
            isRoleAssetManager
        } = this.props
        let {
            managementDepartment,
            assetSource,
            shouldOpenDepartmentPopup,
            department,
            shouldOpenViewAssetByAssetGroupDialog,
            isTemporary,
            isManagedByAccountant,
            item,
            contract,
            itemList,
            isTSCD,
            listOrganizationOrg,
            organizationOrg,
            useDepartment,
            yearPutIntoUse,
            totalElements,
            page,
            rowsPerPage,
            assetType,
            contractYear,
            fromContractYear,
            toContractYear,
            store
        } = this.props.item;
        let listAssetType = appConst.assetType.filter(x => x.code !== appConst.assetClass.VT);

        return (
            <div>
                <Grid className={"asset_department summary_report"}>
                    <Card>
                        <CardContent>
                            <ValidatorForm>
                                <Grid container spacing={2}>
                                    <Grid item md={3} sm={6} xs={12}>
                                        <FormControl fullWidth={true}>
                                            <InputLabel htmlFor="gender-simple">
                                                {t("Asset.filter_temporary")}
                                            </InputLabel>
                                            <Select
                                                isClearable={true}
                                                onChange={(isTemporary) =>
                                                    handleChange(isTemporary, "isTemporary")
                                                }
                                                inputProps={{
                                                    name: "isTemporary",
                                                    id: "gender-simple",
                                                }}
                                            >
                                                {listTemporary.map((item) => {
                                                    return (
                                                        <MenuItem key={item.name} value={item.value}>
                                                            {item.value != null ? item.name : "Tất cả"}
                                                        </MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item md={3} sm={6} xs={12}>
                                        <FormControl fullWidth={true}>
                                            <InputLabel htmlFor="isManagedByAccountant">
                                                {t("StatisticsOfAssetsUnderContract.filterManageAccountant")}
                                            </InputLabel>
                                            <Select
                                                onChange={(isManagedByAccountant) =>
                                                    handleChange(isManagedByAccountant, "isManagedByAccountant")
                                                }
                                                inputProps={{
                                                    name: "isManagedByAccountant",
                                                    id: "isManagedByAccountant",
                                                }}
                                            >
                                                {listManageAccountant.map((item) => {
                                                    return (
                                                        <MenuItem key={item.name} value={item.value}>
                                                            {item.name}
                                                        </MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item md={3} sm={6} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("StatisticsOfAssetsUnderContract.filterManagedepartment")}
                                            searchFunction={getListManagementDepartmentOrg}
                                            // multiple={true}
                                            searchObject={searchObjectManagementDepartment}
                                            typeReturnFunction={"category"}
                                            defaultValue={managementDepartment}
                                            displayLable={"name"}
                                            value={managementDepartment}
                                            onSelect={selectManagementDepartment}
                                            disabled={isRoleAssetManager}
                                        />
                                    </Grid>
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("StatisticsOfAssetsUnderContract.filterUseDepartment")}
                                            searchFunction={getListManagementDepartmentOrg}
                                            searchObject={searchObjectUseDepartment}
                                            defaultValue={useDepartment}
                                            typeReturnFunction={"category"}
                                            displayLable={"name"}
                                            value={useDepartment}
                                            onSelect={handleSelectUseDepartment}
                                        />
                                    </Grid>
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("StatisticsOfAssetsUnderContract.filterAsserOrigin")}
                                            searchFunction={searchByPageAssetSource}
                                            searchObject={searchObjectAssetSource}
                                            displayLable={"name"}
                                            typeReturnFunction="category"
                                            name="assetSource"
                                            value={assetSource}
                                            onSelect={handleSelectAssetSources}
                                        />
                                    </Grid>
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("StatisticsOfAssetsUnderContract.filterStore")}
                                            searchFunction={getPageBySearchTextAndOrgStore}
                                            searchObject={searchObjectStore}
                                            defaultValue={store}
                                            displayLable={"name"}
                                            value={store}
                                            onSelect={selectStore}
                                        />
                                    </Grid>
                                    <Grid item md={3} xs={4}>
                                        <TextValidator
                                            fullWidth
                                            label={t("StatisticsOfAssetsUnderContract.labelInputYear")}
                                            value={yearPutIntoUse}
                                            name="yearPutIntoUse"
                                            type="number"
                                            onKeyUp={handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            validators={[
                                                "matchRegexp:^[1-9][0-9]{3}$", 
                                                `minNumber: 1900`, 
                                                `maxNumber: ${new Date().getFullYear()}`
                                            ]}
                                            errorMessages={[
                                                t("general.yearOfManufactureError"),
                                                t("general.minYearDefault"),
                                                t("general.maxYearNow")
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("StatisticsOfAssetsUnderContract.filterAssetType")}
                                            searchFunction={() => {
                                            }}
                                            searchObject={{}}
                                            listData={listAssetType}
                                            displayLable={"name"}
                                            isNoRenderChildren
                                            isNoRenderParent
                                            value={assetType || null}
                                            name="assetType"
                                            onSelect={this.props.handleSelectAssetType}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Từ ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("StatisticsOfAssetsUnderContract.dxFrom")}
                                                format="dd/MM/yyyy"
                                                value={fromContractYear ?? null}
                                                onChange={(data) => handleSetDataSelect(
                                                    data,
                                                    "fromContractYear"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                maxDate={toContractYear ? new Date(toContractYear) : undefined}
                                                minDateMessage={t("general.minDateMessage")}
                                                maxDateMessage={toContractYear ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Đến ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("StatisticsOfAssetsUnderContract.dxTo")}
                                                format="dd/MM/yyyy"
                                                value={toContractYear ?? null}
                                                onChange={(data) => handleSetDataSelect(
                                                    data,
                                                    "toContractYear"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                minDate={fromContractYear ? new Date(fromContractYear) : undefined}
                                                minDateMessage={fromContractYear ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                maxDateMessage={t("general.maxDateMessage")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item md={3} xs={4}>
                                        <TextValidator
                                            fullWidth
                                            label={t("StatisticsOfAssetsUnderContract.filterContractYear")}
                                            value={contractYear}
                                            name="contractYear"
                                            type="number"
                                            onKeyUp={handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => handleSetDataSelect(
                                            e?.target?.value,
                                            e?.target?.name,
                                            )}
                                            validators={[
                                            "matchRegexp:^[1-9][0-9]{3}$", 
                                            `minNumber: 1900`, 
                                            `maxNumber: ${new Date().getFullYear()}`
                                            ]}
                                            errorMessages={[
                                            t("general.yearOfManufactureError"),
                                            t("general.minYearDefault"),
                                            t("general.maxYearNow")
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item md={3} sm={6} xs={12}>
                                        <Autocomplete
                                            className="mt-3"
                                            size="small"
                                            id="combo-box"
                                            options={listOrganizationOrg}
                                            value={organizationOrg}
                                            renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                label={t("StatisticsOfAssetsUnderContract.filterOrg")}
                                                value={organizationOrg}
                                            />
                                            )}
                                            getOptionLabel={(option) => option.name}
                                            getOptionSelected={(option, value) =>
                                            option.value === value.value
                                            }
                                            onChange={(event, value) => {
                                                selectOrganizationOrg(value);
                                            }}
                                        />
                                    </Grid>
                                    <Grid item md={3} sm={2} xs={12}>
                                        <Button
                                            className="mt-12"
                                            size="small"
                                            variant="contained"
                                            color="primary"
                                            onClick={exportToExcel}
                                        >
                                            {t("general.exportToExcel")}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </ValidatorForm>
                        </CardContent>
                    </Card>
                </Grid>
                <div>
                    {shouldOpenDepartmentPopup && (
                        <TreeDepartmentDialog
                            open={shouldOpenDepartmentPopup}
                            handleSelect={handleSelectReceiverDepartment}
                            selectedItem={department != null ? department : {}}
                            handleClose={handleDepartmentPopupClose}
                            t={t}
                            i18n={i18n}
                        />
                    )}
                    {shouldOpenViewAssetByAssetGroupDialog && (
                        <ViewAssetsUnderContractDialog
                            t={t}
                            i18n={i18n}
                            handleClose={handleDialogClose}
                            open={shouldOpenViewAssetByAssetGroupDialog}
                            handleOKEditClose={handleOKEditClose}
                            item={item}
                            isTSCD={isTSCD}
                            isTemporary={isTemporary}
                            isManagedByAccountant={isManagedByAccountant}
                            managementDepartment={managementDepartment}
                            assetSource={assetSource}
                            contract={contract}
                            organizationOrg={organizationOrg}
                        />
                    )}
                </div>
                <MaterialTable
                    title={t("general.list")}
                    data={itemList}
                    columns={columns}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                        },
                    }}
                    options={{
                        sorting: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        draggable: false,
                        defaultExpanded: true,
                        rowStyle: (rowData) => ({
                            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "450px",
                        minBodyHeight: "450px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                />
                <TablePagination
                    {...defaultPaginationProps()}
                    rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                    count={totalElements}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={setRowsPerPage}
                />
            </div>
        );
    }

}

export default ComponentAmountOfAssetsUnderContract;
