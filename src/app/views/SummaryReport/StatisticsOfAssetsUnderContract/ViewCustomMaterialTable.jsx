import React from 'react'
import MaterialTable, { MTableToolbar } from "material-table";
import { TablePagination } from "@material-ui/core";
import {defaultPaginationProps} from "../../../appFunction";
import {appConst} from "../../../appConst";
import Scrollbar from "react-perfect-scrollbar";


const ViewCustomMaterialTable = (props) => {
  let {
    assetGroupTreeView,
    columnsTreeview,
    titleTree,
    t,
    totalElements,
    rowsPerPage,
    page,
    handleChangePage,
    setRowsPerPage,
    getRowData 
  } = props;  
  return (
    <div>
        <MaterialTable
          onRowClick={(e, rowData) => {
            getRowData(rowData)
          }}
          title={
            <div className="mb-16 w-100 mt-16" >
              <div className="treeview-title">
                <b style={{fontSize: "16px"}}>{titleTree}</b>
              </div>
            </div>
          }
          data={assetGroupTreeView.contracts}
          columns={columnsTreeview}
          localization={{
            body: {
              emptyDataSourceMessage: `${t(
                "general.emptyDataMessageTable"
              )}`,
            },
          }}
          options={{
            sorting: false,
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            draggable: false,
            rowStyle: (rowData) => ({
              backgroundColor:
                rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            minBodyHeight: "435px",
            maxBodyHeight: "435px",
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
      <Scrollbar options={{suppressScrollY: true}} className="position-relative">
        <TablePagination
          {...defaultPaginationProps()}
          style={{overflow: "visible"}}
          className=""
          rowsPerPageOptions={appConst.rowsPerPageOptions.table}
          count={totalElements}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={setRowsPerPage}
        />
      </Scrollbar>
    </div>
  )
}

export default ViewCustomMaterialTable
