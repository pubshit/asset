import axios from "axios";
import ConstantList from "../../../appConfig";

const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report";


export const getYearUnderContractOrg = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/contracts/report";
  return axios.get(url, {params: searchObject});
};

export const getAssetUnderContractOrg = (searchObject) => {
  let url = API_PATH + "/contract/assets";
  return axios.get(url, {params: searchObject});
};

export const getUnderContractQuantity = (searchObject) => {
  let url = API_PATH + "/contract/assets/quantity";
  return axios.get(url, {params: searchObject})
};

export const getUnderContractAssetSummary = (searchObject) => {
  let url = API_PATH + "/contract/assets/summary";
  return axios.get(url, {params: searchObject});
};

export const getUnderContractStatistics = (searchObject) => {
  let url = API_PATH + "/contract/statistics";
  return axios.get(url, {params: searchObject});
};

export const exportToExcel = (params) => {
  return axios({
    method: "get",
    url: API_PATH + "/contract/assets/export-excel",
    params: params,
    responseType: "blob",
  });
};

// lay hop dong cung ung
export const searchByPageListContract = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE  + "/api/contracts/search";
  return axios.get(url, {params: searchObject});
}

export const exportToExcelContractStatistic = (params) => {
  return axios({
    method: "get",
    url: API_PATH + "/contract/statistic/export-excel",
    params: params,
    responseType: "blob",
  });
};

