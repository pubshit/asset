import React from "react";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import {
    convertNumberPrice,
    formatTimestampToDate,
    getTheHighestRole,
    getUserInformation, handleKeyDown, handleKeyUp,
    handleThrowResponseMessage,
    isSuccessfulResponse,
    removeFalsy,
    validateYear
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import OptionSummaryReport from "../Components/OptionSummaryReport"
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import ComponentStatisticsOfAssetsUnderContract from "./ComponentStatisticsOfAssetsUnderContract";
import AmountOfAssetsUnderContract from "./AmountOfAssetsUnderContract";
import {
    exportToExcel,
    getAssetUnderContractOrg,
    getUnderContractAssetSummary,
    getUnderContractQuantity,
    getYearUnderContractOrg
} from "./StatisticsOfAssetsUnderContractService";
import {formatDateDto, isValidDate} from "../../../appFunction";

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});

let { isRoleAssetManager, departmentUser, isRoleAdmin } = getTheHighestRole();

let { organization } = getUserInformation();
class StatisticsOfAssetsUnderContract extends React.Component {
    constructor(props) {
        super(props);
        this.keyPress = this.keyPress.bind(this);
    }

    state = {
        keyword: "",
        status: [],
        assetGroup: null,
        rowsPerPage: 10,
        rowPerPageContract: 10,
        pageContract:0,
        page: 0,
        assetGroupId: "",
        assetGroupTreeView: {},
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        selectAllItem: false,
        totalElements: 0,
        totalElementContracts: 0,
        shouldOpenConfirmationDeleteAllDialog: false,
        managementDepartment: null,
        useDepartment: null,
        originalCost: null,
        assetSource: null,
        store: null,
        isTSCD: true,
        useDepartmentId: null,
        organizationOrg: null,
        listOrganizationOrg: [],
        isGetSumary: true,
        optionSummaryReport: {
            name: appConst.optionSummaryReport.TDCT.name,
            code: appConst.optionSummaryReport.TDCT.code
        },
        assetType: null,
        contractYear: null,
        contractId: null,
        contract: null
    };

    setPage = (page) => {
        this.setState({ page, isGetSumary: true }, this.updatePageData);
    };

    setRowsPerPage = (event) => {
        this.setState({ rowsPerPage: event.target.value, page: 0, isGetSumary: false }, () => {
            this.updatePageData();
        });
    };

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage, isGetSumary: false }, () => {
            this.updatePageData();
        })
    };

    setPageContract = (pageContract) => {
        this.setState({ pageContract, isGetSumary: true }, this.getTreeView);
    };

    setRowsPerPageContract = (event) => {
        this.setState({ 
            rowPerPageContract: event.target.value,
            pageContract: 0, 
            isGetSumary: false 
        }, () => {
            this.getTreeView();
        });
    };

    handleChangePageContract = (event, newPage) => {
        this.setState({ pageContract: newPage, isGetSumary: false }, () => {
            this.getTreeView();
        })
    };

    updatePageData = async () => {
        let { setPageLoading } = this.context;
        let { t } = this.props
        let {
            useDepartment,
            keyword,
            originalCost,
            yearPutIntoUse,
            assetSource,
            store,
            useDepartmentId,
            isGetSumary,
            managementDepartment,
            status,
            page,
            rowsPerPage,
            assetType,
            contractYear,
            contractId,
            fromContractYear,
            toContractYear
        } = this.state;

        if(yearPutIntoUse && !validateYear(yearPutIntoUse)) return;
        setPageLoading(true)
        let searchObject = {};
        if (useDepartmentId !== "all") {
            searchObject.useDepartmentId = this.state.useDepartmentId;
        }
        if (useDepartment?.id && useDepartment?.id !== "all") {
            searchObject.useDepartmentId = useDepartment?.id;
        }

        searchObject.assetStatusIds = status ? status?.map(i => i?.id)?.join(",") : null;
        searchObject.pageIndex = page + 1;
        searchObject.pageSize = rowsPerPage;
        searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
        searchObject.managementDepartmentId = managementDepartment?.id;
        searchObject.assetClass = assetType?.code;
        searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
        if (keyword && keyword.trim().length > 0) searchObject.keyword = keyword?.trim();
        if (originalCost) searchObject.originalCost = originalCost;
        if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
        if (assetSource && assetSource.id) searchObject.assetSourceId = assetSource?.id;
        if (store && store.id) searchObject.storeId = store?.id;
        if (contractYear !== "all") searchObject.contractYear = contractYear;
        if (contractId !== "all") searchObject.contractId = contractId;
        if (fromContractYear) searchObject.fromContractYear = formatDateDto(fromContractYear);
        if (toContractYear) searchObject.toContractYear = formatDateDto(toContractYear);

        try {
            setPageLoading(true);
            const res = await getAssetUnderContractOrg(searchObject);
            const { code, data } = res?.data;
            if (isSuccessfulResponse(code)) {
                this.setState({
                    itemList: data?.content?.length > 0 ? [...data.content] : [],
                    totalElements: data?.totalElements,
                });
            } else {
                handleThrowResponseMessage(res);
            }
            if (isGetSumary) {
                const resTotal = await getUnderContractAssetSummary(searchObject);
                const codeTotal = resTotal?.data?.code;
                const dataTotal = resTotal?.data?.data;

                if (isSuccessfulResponse(codeTotal)) {
                    this.setState({
                        totalAsset: dataTotal?.assetQty,
                        totalOriginalCost: dataTotal?.totalOriginalCost,
                        totalCarryingAmount: dataTotal?.totalCarryingAmount,
                    })
                } else {
                    handleThrowResponseMessage(resTotal);
                }
            }
        } catch (error) {
            toast.error(t("toastr.error"))
        } finally {
            setPageLoading(false);
        }
    };

     formatContract = (contract, dataMatch) => ({
        ...contract,
        name: `${contract?.contractCode} - ${contract?.contractName} - ${formatTimestampToDate(contract?.contractDate)}`,
        id: contract?.contractId,
        assetQty: dataMatch?.contracts.find(asset => asset?.contractId === contract?.contractId)?.assetQty || 0
    });

    convertAssetsUnderContractTreeView = (contractData, quantity) => {
        let contracts = [];
        
        if (contractData && quantity) {
            contractData?.map(contract => {
                let  dataMatch = quantity.find(item => item?.contractId === contract?.contractId);
                contracts.push({
                    ...contract,
                    assetQty: dataMatch?.assetQty
                })
            });
        }
        
        return contracts;
      };

    getTreeView = async () => {
        let { organizationOrg } = this.state;
        let { setPageLoading } = this.context;
        let { t } = this.props;
        let objectSearch = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            assetClass: this.state.assetType?.code,
            orgId: organizationOrg?.id || organization?.org?.id,
            contractYear: this.state.contractYear || null,
            pageIndex: this.state.pageContract + 1,
            pageSize: this.state.rowPerPageContract,
            managementDepartmentId: this.state.managementDepartment?.id,
            useDepartmentId: this.state.useDepartmentId || this.state.useDepartment?.id,
            assetSourceId: this.state.assetSource?.id,
            storeId: this.state.store?.id,
            yearPutIntoUse: this.state.yearPutIntoUse,
            originalCost: this.state.originalCost,
            fromContractYear: formatDateDto(this.state.fromContractYear),
            toContractYear: formatDateDto(this.state.toContractYear),
            contractTypeCode: appConst.TYPES_CONTRACT.CU
        }
        setPageLoading(true);
        try {
            let res = await getYearUnderContractOrg(objectSearch);
            let resQuantity = await getUnderContractQuantity(objectSearch);
            
            const data = res?.data?.data;
            const dataQuantity = resQuantity?.data?.data;
            
            if (res?.status === appConst.CODE.SUCCESS && res?.data?.data) {
                let assetGroupTreeView = {
                    id: "all",
                    name: <input type="text" />,
                    contracts: this.convertAssetsUnderContractTreeView(data?.content, dataQuantity) 
                };
                this.setState({ 
                    assetGroupTreeView: assetGroupTreeView,
                    totalElementContracts: data?.totalElements 
                }, () => { });
            }
        } catch (e) {
            toast.error(t("toastr.error"))
        } finally {
            setPageLoading(false);
        }
    }


    componentDidMount() {
        let { t } = this.props;

        searchByPageOrg(organization?.org?.id).then((data) => {
            this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
        }).catch(() => {
            toast.error(t("toastr.error"))
        })

        this.setState({ managementDepartment: isRoleAssetManager ? departmentUser : null }, () => {
            this.updatePageData();
            this.getTreeView();
        })
    }

    selectManagementDepartment = (item) => {
        this.setState({ managementDepartment: item }, () => {
            this.search();
        });
    };

    handleEditItem = (item) => {
        this.setState({
            item: item,
            shouldOpenEditorDialog: true,
        });
    };

    handleClick = (event, item) => {
        let { Asset } = this.state;
        if (item.checked == null) {
            item.checked = true;
        } else {
            item.checked = !item.checked;
        }
        let selectAllItem = true;
        for (let i = 0; i < Asset.length; i++) {
            if (Asset[i].checked == null || Asset[i].checked === false) {
                selectAllItem = false;
            }
            if (Asset[i].id === item.id) {
                Asset[i] = item;
            }
        }
        this.setState({ selectAllItem: selectAllItem, Asset: Asset });
    };
    handleSelectAllClick = () => {
        let { Asset } = this.state;
        for (let i = 0; i < Asset.length; i++) {
            Asset[i].checked = !this.state.selectAllItem;
        }
        this.setState({ selectAllItem: !this.state.selectAllItem, Asset: Asset });
    };

    handleDelete = (id) => {
        this.setState({
            id,
            shouldOpenConfirmationDialog: true,
        });
    };

    selectStatus = (statusSelected) => {
        this.setState({ status: statusSelected }, () => {
            this.search();
        });
    };

    handleSelectAssetType = (assetType) => {
        this.setState({ assetType }, () => {
            this.search();
        });
    }

    search = async () => {
        this.setPage(0);
        this.setPageContract(0);
    };

    getRowAssetContract = (rowData) => {
       this.setState({ contractId: rowData?.contractId },() => this.setPage(0));
    }

    selectUseDepartment = (selected) => {
        this.setState({ useDepartment: selected }, () => {
            this.search();
        })
    }

    selectAssetSource = (selected) => {
        this.setState({ assetSource: selected }, () => {
            this.search();
        })
    }

    selectOrganizationOrg = (selected) => {
        this.setState({
            organizationOrg: selected,
            managementDepartment: null,
            useDepartment: null,
            assetSource: null,
            useDepartmentId: null,
            store: null
        }, () => {
            this.search();
            this.getTreeView();
        })
    }

    selectStore = (selected) => {
        this.setState({ store: selected }, () => {
            this.search();
        })
    }

    keyPress(e, value, nameState) {
        if (e.key === appConst.KEY.ENTER) {
            this.setState({ [nameState]: value }, () => this.search())
        }
    }

    selectYear = (value) => {
        if (value) {
            this.setState({ yearPutIntoUse: value.year }, () => this.search())
        } else {
            this.setState({ yearPutIntoUse: value }, () => this.search())
        }
    }
    handleToggle = () => {
        let optionReport = {
            [appConst.optionSummaryReport.TDCT.code]: appConst.optionSummaryReport.TDTH,
            [appConst.optionSummaryReport.TDTH.code]: appConst.optionSummaryReport.TDCT,
        }
        this.setState({
            optionSummaryReport: optionReport[this.state.optionSummaryReport.code],
            contractId: null,
            contractYear: null
        }, () => this.updatePageData());
    }

    handleKeyDown = (e) => handleKeyDown(e, this.search);

    handleKeyUp = (e) => handleKeyUp(e, this.search);

    handleSetDataSelect = (data, name) => {
        if (["fromContractYear", "toContractYear"].includes(name)) {
            this.setState({ [name]: data }, () => {
                if (data === null || isValidDate(data)) {
                    this.search();
                }
            });
            return;
        }
        this.setState({
            [name]: data
        });
    };

    /* Export to excel */
    exportToExcel = async () => {
        const { setPageLoading } = this.context;

        try {
            setPageLoading(true);

            const searchObject = this.buildSearchObject();
            let filename = "Thongketaisantheohopdong.xlsx";
            const res = await exportToExcel(removeFalsy(searchObject));

            const blob = new Blob([res.data], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            });

            saveAs(blob, filename);
        } catch (error) {
            console.error(error);
        } finally {
            setPageLoading(false);
        }
    };

    buildSearchObject = () => {
        let {
            useDepartment,
            keyword,
            originalCost,
            yearPutIntoUse,
            assetSource,
            store,
            useDepartmentId,
            managementDepartment,
            status,
            page,
            assetType,
            contractYear,
            contractId,
            fromContractYear,
            toContractYear
        } = this.state;

        let searchObject = {};
        if (useDepartmentId !== "all") {
            searchObject.useDepartmentId = this.state.useDepartmentId;
        }
        if (useDepartment?.id && useDepartment?.id !== "all") {
            searchObject.useDepartmentId = useDepartment?.id;
        }

        searchObject.assetStatusIds = status ? status?.map(i => i?.id)?.join(",") : null;
        searchObject.pageIndex = page + 1;
        searchObject.pageSize = appConst.OBJECT_SEARCH_MAX_SIZE.pageSize;
        searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
        searchObject.managementDepartmentId = managementDepartment?.id;
        searchObject.assetClass = assetType?.code;
        searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
        if (keyword && keyword.trim().length > 0) searchObject.keyword = keyword?.trim();
        if (originalCost) searchObject.originalCost = originalCost;
        if (yearPutIntoUse) searchObject.yearPutIntoUse = yearPutIntoUse;
        if (assetSource && assetSource.id) searchObject.assetSourceId = assetSource?.id;
        if (store && store.id) searchObject.storeId = store?.id;
        if (contractYear !== "all") searchObject.contractYear = contractYear;
        if (contractId !== "all") searchObject.contractId = contractId;
        if (fromContractYear) searchObject.fromContractYear = formatDateDto(fromContractYear);
        if (toContractYear) searchObject.toContractYear = formatDateDto(toContractYear);

        return searchObject;
    };

    renderNameContract = data => {
        return (
          <span>
            {`${data?.contractCode} - ${data?.contractName}`}
            {data?.assetQty >= 0 && (
              <span className="text-red-italic inputPrice">
                &nbsp;({data.assetQty})
              </span>
            )}
          </span>
        )
    }

    render() {
        const { t, i18n } = this.props;
        const { rowsPerPage, page, organizationOrg } = this.state;
        const currentDepartment = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
        const config_org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
        let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
        let searchObjectAssetSource = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            orgId: organizationOrg?.id || organization?.org?.id,
        };
        let searchObjectStore = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            departmentId: getTheHighestRole().isRoleAdmin || config_org.configDto.isGetAllStore
                ? ""
                : (currentDepartment?.id || ""),
            orgId: organizationOrg?.id || organization?.org?.id
        };
        let searchObjectManagementDepartment = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            orgId: organizationOrg?.id || organization?.org?.id,
            isAssetManagement: true
        }
        let searchObjectUseDepartment = {
            ...appConst.OBJECT_SEARCH_MAX_SIZE,
            orgId: organizationOrg?.id || organization?.org?.id,
        }
        let dateNow = new Date().getFullYear();
        let listYear = Array.from({ length: dateNow - 1970 + 1 }, (_, i) => (dateNow - i));
        let TitlePage = t("Dashboard.summary_report.statistics_of_assets_under_contract");
        let columns = [
            {
                title: t("general.index"),
                field: "code",
                width: "50px",
                cellStyle: {
                    textAlign: "center"
                },
                render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
            },
            {
                title: this.state.isTSCD
                    ? t("Asset.code")
                    : t("InstrumentToolsList.code"),
                field: "code",
                cellStyle: {
                    minWidth: "120px",
                    textAlign: "center"
                },
            },
            {
                title: this.state.isTSCD
                    ? t("Asset.name")
                    : t("InstrumentToolsList.name"),
                field: "name",
                align: "left",
                width: "150",
                cellStyle: {
                    minWidth: "270px",
                },
            },
            {
                title: t("Asset.yearOfManufacture"),
                field: "yearOfManufacture",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.yearIntoUse"),
                field: "yearPutIntoUse",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.serialNumber"),
                field: "serialNumber",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.model"),
                field: "model",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("general.Quantity"),
                field: "quantity",
                minWidth: 80,
                hidden: this.state.isTSCD,
                cellStyle: {
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.manufacturer"),
                field: "manufacturerName",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.originalCost"),
                field: "originalCost",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "right"
                },
                render: (rowData) => rowData?.originalCost
                    ? convertNumberPrice(rowData?.originalCost)
                    : ""
            },
            {
                title: t("Asset.carryingAmountTable"),
                field: "carryingAmount",
                hidden: !this.state.isTSCD,
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "right"
                },
                render: (rowData) => rowData?.carryingAmount
                    ? convertNumberPrice(rowData?.carryingAmount)
                    : ""
            },
            {
                title: t("Asset.status"),
                field: "statusName",
                width: "150",
                cellStyle: {
                    minWidth: "150px",
                    textAlign: "center"
                },
            },
            {
                title: t("Asset.assetSource"),
                field: "assetSourceNames",
                align: "left",
                minWidth: "200px",
            },
            {
                title: t("Asset.store"),
                field: "storeName",
                align: "left",
                minWidth: "200px",
            },
            {
                title: t("Asset.useDepartment"),
                field: "useDepartmentName",
                align: "left",
                width: "150",
                cellStyle: {
                    minWidth: "200px",
                },
            },
        ];
        let columnsTreeview = [
            {
                title: t("StatisticsOfAssetsUnderContract.contract"),
                field: "name",
                align: "left",
                minWidth: "280px",
                render: (rowData) => this.renderNameContract(rowData)
                
            },
            {
                title: t("StatisticsOfAssetsUnderContract.contractDate"),
                field: "contractDate",
                align: "left",
                minWidth: "120px",
                cellStyle: {
                    textAlign: "center"
                },
                render: (rowData) => formatTimestampToDate(rowData?.contractDate)
            },
        ]
        return (
            <div className="m-sm-30">
                <Helmet>
                    <title>
                        {TitlePage} | {t("web_site")}
                    </title>
                </Helmet>
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            {
                                name: t("Dashboard.summary_report.title"),
                                path: "/summary_report/statistics_of_assets_under_contract",
                            },
                            { name: TitlePage },
                        ]}
                    />
                </div>
                <div className="flex flex-end">
                    <OptionSummaryReport
                        checked={this.state.optionSummaryReport?.code === appConst.optionSummaryReport.TDTH.code}
                        handleChange={() => this.handleToggle()}
                        label={this.state.optionSummaryReport?.name}
                        noPosition="true"
                    />
                </div>
                {
                    this.state.optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? (
                        <ComponentStatisticsOfAssetsUnderContract
                            t={t}
                            i18n={i18n}
                            exportToExcel={this.exportToExcel}
                            item={this.state}
                            titleTree={t("StatisticsOfAssetsUnderContract.titleTree")}
                            selectManagementDepartment={this.selectManagementDepartment}
                            searchObject={searchObject}
                            selectStatus={this.selectStatus}
                            selectUseDepartment={this.selectUseDepartment}
                            selectAssetSource={this.selectAssetSource}
                            searchObjectStore={searchObjectStore}
                            searchObjectManagementDepartment={searchObjectManagementDepartment}
                            searchObjectUseDepartment={searchObjectUseDepartment}
                            searchObjectAssetSource={searchObjectAssetSource}
                            selectStore={this.selectStore}
                            listYear={listYear}
                            keyPress={this.keyPress}
                            selectYear={this.selectYear}
                            columns={columns}
                            columnsTreeview={columnsTreeview}
                            handleChangePage={this.handleChangePage}
                            setRowsPerPage={this.setRowsPerPage}
                            selectOrganizationOrg={this.selectOrganizationOrg}
                            isShowExactSearch={true}
                            handleSetDataSelect={this.handleSetDataSelect}
                            handleKeyDown={this.handleKeyDown}
                            handleKeyUp={this.handleKeyUp}
                            handleSelectAssetType={this.handleSelectAssetType}
                            getRowData={(rowData) => this.getRowAssetContract(rowData)}
                            handleChangePageContract={this.handleChangePageContract}
                            setRowsPerPageContract={this.setRowsPerPageContract}
                            isRoleAssetManager={isRoleAssetManager}
                        />
                    ) : (
                        <AmountOfAssetsUnderContract t={this.props.t}/>
                    )
                }
            </div>
        )
    }
}

StatisticsOfAssetsUnderContract.contextType = AppContext
export default StatisticsOfAssetsUnderContract;
