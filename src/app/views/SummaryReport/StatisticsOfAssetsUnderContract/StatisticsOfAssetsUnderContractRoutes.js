import { EgretLoadable } from "egret";
import ConstantList from "../../../appConfig";
import { withTranslation } from 'react-i18next';
const StatisticsOfAssetsUnderContract = EgretLoadable({
  loader: () => import("./StatisticsOfAssetsUnderContract"),
});
const ViewComponent = withTranslation()(StatisticsOfAssetsUnderContract);

const StatisticsOfAssetsUnderContractRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "summary_report/statistics_of_assets_under_contract",
    exact: true,
    component: ViewComponent
  }
];

export default StatisticsOfAssetsUnderContractRoutes;