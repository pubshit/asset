import React, { Component } from "react";
import {
  IconButton,
  Icon,
} from "@material-ui/core";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { STATUS_DEPARTMENT, appConst, keySearch } from "app/appConst";
import { searchByPage } from "../../Department/DepartmentService";
import { debounce } from "lodash";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import {
  convertNumberPriceRoundUp, formatDateDto, functionExportToExcel,
  getTheHighestRole,
  getUserInformation, handleKeyDown, handleKeyUp,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate,
  validateYear
} from "../../../appFunction";
import ComponentAmountOfAssetsUnderContract from "./ComponentAmountOfAssetsUnderContract";
import { exportToExcelContractStatistic, getUnderContractStatistics } from "./StatisticsOfAssetsUnderContractService";
import localStorageService from "../../../services/localStorageService";
import {LightTooltip} from "../../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

let { isRoleAssetManager, departmentUser, isRoleAdmin } = getTheHighestRole();
let {organization} = getUserInformation();
class AmountOfAssetsUnderContract extends Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, () => {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    rowsPerPage: 10,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByAssetGroupDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    managementDepartmentId: "",
    managementDepartment: null,
    listManageDepartment: [],
    assetSources: [],
    assetSourceId: "",
    assetSource: null,
    isTemporary: null,
    isManagedByAccountant: null,
    ranger: [0, 500],
    useDepartment: {},
    isInverted: null, // lấy từ 0 đến rangerValue
    query: {
      pageIndex: 0,
      pageSize: 10,
      keyword: "",
      keySearch: ""
    },
    listDepartment: [],
    isTSCD: true,
    itemList: [],
    organizationOrg: null,
    listOrganizationOrg: [],
    assetType: null
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  yearPutIntoUse = Array.from(new Array(20), (val, index) => (new Date()).getFullYear() - index);
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    let {
      contractId,
      page,
      rowsPerPage,
      keyword,
      managementDepartment,
      assetSource,
      useDepartment,
      managementDepartmentId,
      yearPutIntoUse,
      isTemporary,
      isManagedByAccountant,
      organizationOrg,
      assetType,
      fromContractYear,
      toContractYear,
      store
    } = this.state;
    if(this.state?.yearPutIntoUse && !validateYear(this.state?.yearPutIntoUse)) return;
    try {
      let searchObject = {};
      setPageLoading(true);

      searchObject.pageIndex = page + 1;
      searchObject.pageSize = rowsPerPage;
      searchObject.keyword = keyword || null;
      searchObject.managementDepartmentId = managementDepartment?.id || null;
      searchObject.assetSourceId = assetSource?.id;
      searchObject.useDepartmentId = useDepartment?.id;
      searchObject.managementDepartmentId = managementDepartmentId;
      searchObject.yearPutIntoUse = yearPutIntoUse;
      searchObject.isTemporary = isTemporary;
      searchObject.isManagedByAccountant = isManagedByAccountant;
      if (store && store.id) searchObject.storeId = store?.id;
      searchObject.orgId = organizationOrg?.id || organization?.org?.id;
      searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
      searchObject.assetClass = assetType?.code;
      if (contractId !== "all") searchObject.contractId = contractId?.contractId;
      if (fromContractYear) searchObject.fromContractYear = formatDateDto(fromContractYear);
      if (toContractYear) searchObject.toContractYear = formatDateDto(toContractYear);

      let res = await getUnderContractStatistics(searchObject);
      const {code, data} = res?.data;
      
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: data?.content,
          totalElements: data.totalElements
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
      contract: null
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    let { t } = this.props

    searchByPageOrg(organization?.org?.id).then((data) => {
      this.setState({listOrganizationOrg: [...data?.data?.data?.content]})
    }).catch(() => {
      toast.error(t("toastr.error"))
    });

    this.getListDepartment();
    this.updatePageData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state?.query !== prevState.query) {
      if (this.state?.query.keySearch === keySearch.asset) {
        this.getListDepartment()
      }
    }
  }

  componentWillMount() {
    if (isRoleAssetManager) {
        this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
    };
  }

  debouncedSetSearchTerm = debounce((newTerm, source) => {
    this.setState({
      query: {
        ...this.state.query,
        keyword: newTerm,
        keySearch: source
      }
    });
  }, 300);

  handleSearchDepartment = (e, source) => {
    this.debouncedSetSearchTerm(e.target.value, source)
  };

  getListDepartment = async () => {
    let searchObject = {};
    searchObject.keyword = this.state.query.keyword.trim();
    searchObject.pageIndex = this.state.query.pageIndex + 1;
    searchObject.pageSize = this.state.query.pageSize;
    searchObject.isActive=STATUS_DEPARTMENT.HOAT_DONG.code
    try {
      let res = await searchByPage(searchObject);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.content) {
        this.setState({
          listDepartment: [...res?.data?.content]
        });
      } else {
        this.setState({
          listDepartment: [],
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  search = () => {
    this.setPage(0);
  }

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      () => {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  /* Export to excel */
  exportToExcel = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      contractId,
      managementDepartment,
      assetSource,
      useDepartment,
      managementDepartmentId,
      yearPutIntoUse,
      isTemporary,
      isManagedByAccountant,
      organizationOrg,
      assetType,
      keyword, 
      fromContractYear,
      toContractYear,
      contractYear,
      page,
      store     
    } = this.state;
    let searchObject = {};

    searchObject.keyword = keyword || null;
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = appConst.OBJECT_SEARCH_MAX_SIZE.pageSize;
    searchObject.assetSourceId = assetSource?.id;
    searchObject.useDepartmentId = useDepartment?.id;
    searchObject.managementDepartmentId = managementDepartment?.id || managementDepartmentId || null;
    searchObject.yearPutIntoUse = yearPutIntoUse;
    searchObject.isTemporary = isTemporary;
    searchObject.isManagedByAccountant = isManagedByAccountant;
    searchObject.orgId = organizationOrg?.id || organization?.org?.id;
    searchObject.contractTypeCode = appConst.TYPES_CONTRACT.CU;
    searchObject.assetClass = assetType?.code;
    if (store && store.id) searchObject.storeId = store?.id;
    if (contractId !== "all") searchObject.contractId = contractId;
    if (contractYear !== "all") searchObject.contractYear = contractYear;
    if (fromContractYear) searchObject.fromContractYear = formatDateDto(fromContractYear);
    if (toContractYear) searchObject.toContractYear = formatDateDto(toContractYear);

    try {
      setPageLoading(true);
      let fileName = "Thongketaisantheohopdong.xlsx";
      await functionExportToExcel(
        exportToExcelContractStatistic,
        searchObject,
        fileName,
      )
    } catch (e) {
      console.error(e);
    } finally {
      setPageLoading(false);
    }
  };

  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      () => {
        this.search();
      }
    );
  };

  selectOrganizationOrg = (selected) => {
    this.setState({
        organizationOrg: selected,
        managementDepartment: null,
        useDepartment: null,
        assetSource: null,
        assetSourceId: null,
        store: null
    }, () => {
      this.search();
    })
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary === "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, () => {
        this.search();
      });
      return;
    }
    if (source === "isManagedByAccountant") {
      let { isManagedByAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant === "all") {
        isManagedByAccountant = null;
      } else {
        isManagedByAccountant = ManageAccountant;
      }
      this.setState({ isManagedByAccountant: isManagedByAccountant }, () => {
        this.search();
      });
      return;
    }
    if (source === "yearPutIntoUse") {
      let yearPutIntoUse = event.target.value;
      if (yearPutIntoUse !== 0) {
        this.setState({ yearPutIntoUse: yearPutIntoUse }, () => {
          this.search();
        });
      } else {
        this.setState({ yearPutIntoUse: null })
        this.search();
      }
    }
  };

  handleKeyDown = (e) => handleKeyDown(e, this.search);

  handleKeyUp = (e) => handleKeyUp(e, this.search);

  handleSelectAssetType = (assetType) => {
    this.setState({ assetType }, () => {
        this.search();
    });
}

  handleSetDataSelect = (data, name) => {
    if (["fromContractYear", "toContractYear"].includes(name)) {
      this.setState({ [name]: data }, () => {
          if (data === null || isValidDate(data)) {
              this.search();
          }
      });
      return;
    }
    this.setState({
      [name]: data
    });
  };

  handleSelectUseDepartment = (item) => {
    let department = item ? { id: item?.id, name: item?.name, text: item?.name } : null;
    this.setState({ useDepartment: department }, () => {
      this.search();
    });
  }

  handleSelectAssetSources = (value) => {
    this.setState({
      assetSource: value,
    },
      this.search
    )
  }

  selectStore = (selected) => {
    this.setState({ store: selected }, () => {
        this.search();
    })
}

  render() {
    const { t, i18n } = this.props;
    const { organizationOrg} = this.state;
    const currentDepartment = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
    const config_org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
    let searchObjectAssetSource = { ...appConst.OBJECT_SEARCH_MAX_SIZE, orgId: organizationOrg?.id, };
    let searchObjectManagementDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
        orgId: organizationOrg?.id || organization?.org?.id,
        isAssetManagement: true
      }
      let searchObjectUseDepartment = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        orgId: organizationOrg?.id || organization?.org?.id,
      }
      let searchObjectContract = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        orgId: organizationOrg?.id || organization?.org?.id,
        contractTypeCode: appConst.TYPES_CONTRACT.CU
      }
      let searchObjectStore = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        departmentId: isRoleAdmin || config_org.configDto.isGetAllStore
            ? ""
            : (currentDepartment?.id || ""),
        orgId: organizationOrg?.id || organization?.org?.id
      };
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        cellStyle: {
          minWidth: "120px",
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.view) {
                this.setState(
                  {
                    contract: rowData,
                    shouldOpenViewAssetByAssetGroupDialog: true,
                  }
                );
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("statistics_of_assets_under_contract.contract"),
        field: "name",
        width: "300",
        align: "left",
        cellStyle: {
          minWidth: "300px",
          textAlign: "left",
        },
        render: (rowData) => rowData?.contractName || rowData?.contractYear 
      },
      {
        title: t("statistics_of_assets_under_contract.quantity"),
        field: "quantity",
        align: "left",
        cellStyle: {
          minWidth: "100px",
          textAlign: "center",
        },
        render: (rowData) => rowData?.assetQty || 0
      },
      {
        title: t("statistics_of_assets_under_contract.remainQuantity"),
        field: "totalCarryingAmount",
        hidden: !this.state.isTSCD,
        align: "left",
        cellStyle: {
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.totalCarryingAmount) || 0,
      },
      {
        title: t("statistics_of_assets_under_contract.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "left",
        cellStyle: {
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.totalOriginalCost) || 0,
      },
    ];


    return (
      <div className="">
          <ComponentAmountOfAssetsUnderContract
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            handleChange={this.handleChange}
            listTemporary={this.listTemporary}
            listManageAccountant={this.listManageAccountant}
            yearPutIntoUse={this.yearPutIntoUse}
            searchObjectAssetSource={searchObjectAssetSource}
            searchObjectManagementDepartment={searchObjectManagementDepartment}
            searchObjectUseDepartment={searchObjectUseDepartment}
            searchObjectContract={searchObjectContract}
            searchObjectStore={searchObjectStore}
            handleSelectUseDepartment={this.handleSelectUseDepartment}
            handleSearchDepartment={this.handleSearchDepartment}
            selectOrganizationOrg={this.selectOrganizationOrg}
            exportToExcel={this.exportToExcel}
            handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
            handleDepartmentPopupClose={this.handleDepartmentPopupClose}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            selectManagementDepartment={this.selectManagementDepartment}
            handleKeyDown={this.handleKeyDown}
            handleKeyUp={this.handleKeyUp}
            handleSetDataSelect={this.handleSetDataSelect}
            isShowExactSearch={true}
            handleSelectAssetSources={this.handleSelectAssetSources}
            setRowsPerPage={this.setRowsPerPage}
            handleChangePage={this.handleChangePage}
            handleSelectAssetType={this.handleSelectAssetType}
            selectStore={this.selectStore}
            isRoleAssetManager={isRoleAssetManager}
          />
      </div>
    );
  }
}
AmountOfAssetsUnderContract.contextType = AppContext
export default AmountOfAssetsUnderContract;
