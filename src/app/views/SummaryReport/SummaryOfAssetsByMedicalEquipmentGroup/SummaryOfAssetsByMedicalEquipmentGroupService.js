import axios from "axios";
import ConstantList from "../../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/medical-equipment";
const API_PATH_ORG =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/v2/organizations";
const API_PATH_COMMON =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/common-objects";

export const exportToExcel = (params) => {
  return axios({
    method: "get",
    url: API_PATH + "/assets/export-excel",
    params: params,
    responseType: "blob",
  });
};

export const exportToExcelLissAsset = (params) => {
  return axios({
    method: "get",
    url: API_PATH + "/statistics/export-excel",
    params: params,
    responseType: "blob",
  });
};

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/assets";
  return axios.get(url, { params: searchObject });
};

export const getSummary = (searchObject) => {
  let url = API_PATH + "/summary";
  return axios.get(url, { params: searchObject });
};

export const getStatisticsInformation = (searchObject) => {
  let url = API_PATH + "/statistics";
  return axios.get(url, { params: searchObject });
};

export const searchByPageOrg = (parentId) => {
  let url = API_PATH_ORG + `/${parentId}/children/search`;
  return axios.get(url);
};

export const searchMedicalEquipment = (searchObject) => {
  let url = API_PATH_COMMON + "/roots";
  return axios.get(url, { params: searchObject });
};
