import { EgretLoadable } from "egret";
import ConstantList from "../../../appConfig";
import {withTranslation} from 'react-i18next';
const SummaryOfAssetsByMedicalEquipmentGroup = EgretLoadable({
  loader: () => import("./SummaryOfAssetsByMedicalEquipmentGroup")
});
const ViewComponent = withTranslation()(SummaryOfAssetsByMedicalEquipmentGroup);

const SummaryOfAssetsByMedicalEquipmentGroupRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "summary-report/summary-of-assets-by-medical-equipment",
    exact: true,
    component: ViewComponent
  }
];

export default SummaryOfAssetsByMedicalEquipmentGroupRoutes;
