import {Button, Card, Grid, TablePagination, TextField} from "@material-ui/core";
import React, {Component} from "react";
import {ValidatorForm, TextValidator} from "react-material-ui-form-validator";
import AsynchronousAutocomplete from "../../utilities/AsynchronousAutocomplete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import MaterialTable, {MTableToolbar} from "material-table";
import {getListManagementDepartmentOrg} from "app/views/Asset/AssetService";
import {searchByPage as searchByPageAssetSource} from "../../AssetSource/AssetSourceService";
import {getPageBySearchTextAndOrgStore} from "app/views/Store/StoreService";
import { appConst} from "app/appConst";
import {NumberFormatCustom, convertNumberPriceRoundUp} from "app/appFunction";
import CardContent from "@material-ui/core/CardContent";
import CustomizedTreeView from "../../Asset/TreeViewAsset";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import ViewAssetByMedicalEquipment from "./ViewAssetByAssetGroupDialog";

class ComponentSummaryOfAssetsByMedicalEquipmentGroup extends Component {
  render() {
    let {
      t,
      selectManagementDepartment,
      searchObject,
      selectStatus,
      selectUseDepartment,
      selectAssetSource,
      searchObjectStore,
      selectStore,
      listYear,
      keyPress,
      selectYear,
      columns,
      handleChangePage,
      setRowsPerPage,
      handleSelectMedicalEquipment,
      handleExportToExcel,
      selectOrganizationOrg,
      searchObjectAssetSource,
      searchObjectManagementDepartment,
      searchObjectUseDepartment,
      handleKeyUp,
      handleSetDataSelect,
      handleKeyDown,
      isRoleAssetManager
    } = this.props
    let {
      itemList,
      medicalEquipment,
      medicalEquipmentTreeView,
      managementDepartment,
      useDepartment,
      assetSource,
      store,
      assetType,
      totalAsset,
      totalOriginalCost,
      totalCarryingAmount,
      optionSummaryReport,
      shouldOpenViewAssetByMedicalEquipment,
      organizationOrg,
      listOrganizationOrg,
      originalCost,
      yearPutIntoUse,
      keyword,
      totalElements,
      rowsPerPage,
      page,
      medicalEquipmentId
    } = this.props.item;
    searchObjectStore.isActive = 1;
    let listAssetType = appConst.assetType.filter(x => x.code !== appConst.assetClass.VT);
    return (
      <div>
        <Card elevation={3} className="mb-20">
          <CardContent>
            <ValidatorForm ref="form" onSubmit={() => {
            }}>
              <Grid container spacing={2} className="p-10 bg-white pt-0" alignItems="flex-end"
                    style={{borderRadius: "10px"}}>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("Asset.filter_manage_department")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectManagementDepartment}
                    typeReturnFunction={"category"}
                    defaultValue={managementDepartment}
                    displayLable={"name"}
                    value={managementDepartment}
                    onSelect={selectManagementDepartment}
                    disabled={isRoleAssetManager}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("SummaryByMedicalEquipment.filterUseDepartment")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectUseDepartment}
                    defaultValue={useDepartment}
                    displayLable={"name"}
                    typeReturnFunction={"category"}
                    value={useDepartment}
                    onSelect={selectUseDepartment}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("SummaryByMedicalEquipment.filterAsserOrigin")}
                    searchFunction={searchByPageAssetSource}
                    searchObject={searchObjectAssetSource}
                    defaultValue={assetSource}
                    displayLable={"name"}
                    typeReturnFunction="category"
                    value={assetSource}
                    onSelect={selectAssetSource}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("SummaryByMedicalEquipment.filterStore")}
                    searchFunction={getPageBySearchTextAndOrgStore}
                    searchObject={searchObjectStore}
                    defaultValue={store}
                    displayLable={"name"}
                    value={store}
                    onSelect={selectStore}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("SummaryByMedicalEquipment.labelInputNameOrCode")}
                    value={keyword}
                    name="keyword"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                        e?.target?.value,
                        e?.target?.name,
                    )}
                   />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("SummaryByMedicalEquipment.originalCost")}
                    value={originalCost}
                    name="originalCost"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                        e?.target?.value,
                        e?.target?.name,
                    )}
                    InputProps={{
                        inputComponent: NumberFormatCustom,
                    }}
                   />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("SummaryByMedicalEquipment.labelInputYear")}
                    value={yearPutIntoUse}
                    name="yearPutIntoUse"
                    type="number"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => handleSetDataSelect(
                        e?.target?.value,
                        e?.target?.name,
                    )}
                   />
                </Grid>
                <Grid item md={3} xs={4}>
                  <AsynchronousAutocompleteSub
                    label={t("general.filterAssetType")}
                    searchFunction={() => {
                    }}
                    searchObject={{}}
                    listData={listAssetType}
                    displayLable={"name"}
                    isNoRenderChildren
                    isNoRenderParent
                    value={assetType || null}
                    name="assetType"
                    onSelect={this.props.handleSelectAssetType}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="combo-box"
                    options={listOrganizationOrg}
                    value={organizationOrg}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label={t("SummaryByMedicalEquipment.filterOrg")}
                        value={organizationOrg}
                      />
                    )}
                    getOptionLabel={(option) => option.name}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    onChange={(event, value) => {
                      selectOrganizationOrg(value);
                    }}
                  />
                </Grid>
              </Grid>
            </ValidatorForm>
          </CardContent>
        </Card>

        <Grid container spacing={2} className={"mt-16"}>
          {shouldOpenViewAssetByMedicalEquipment && (
            <ViewAssetByMedicalEquipment
              t={t}
              handleClose={this.props.handleDialogClose}
              open={shouldOpenViewAssetByMedicalEquipment}
              handleOKEditClose={this.props.handleDialogClose}
              // item={this.props.item.item}
              medicalEquipmentId={medicalEquipmentId}
              state={this.props.item}
            />
          )}

          {optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? (
            <>
              <Grid item md={3} xs={12}>
                <div className="asset_department treeview-bao-cao overflow-auto MuiPaper-elevation2">
                  <div className="treeview-title">
                    <b>{t("SummaryByMedicalEquipment.titleTree")}</b>
                  </div>
                  {medicalEquipmentTreeView && (
                    <CustomizedTreeView
                      t={t}
                      defaultCollapseIcon={<ExpandMoreIcon/>}
                      defaultExpanded={["all"]}
                      defaultExpandIcon={<ChevronRightIcon/>}
                      onNodeSelect={handleSelectMedicalEquipment}
                      style={{background: "white", borderRadius: "4px"}}
                      treeViewItem={medicalEquipmentTreeView}
                    />
                  )}
                </div>
              </Grid>
              <Grid item md={9} xs={12}>
                <MaterialTable
                  title={
                    <div>
                      {
                        medicalEquipment?.id !== "all"
                          ? <h4>
                            <span className="text-red-italic">{medicalEquipment?.name}</span>
                          </h4>
                          : <h4>
                            {assetType?.code === appConst.assetClass.TSCD ? t("SummaryByMedicalEquipment.tabTSCD") : t("SummaryByMedicalEquipment.tabCCDC")}
                          </h4>
                      }
                      <div className="pr-10">
                    <span className="mr-20">Tổng số lượng: <span
                      className="text-red-italic inputPrice">{totalAsset || 0}</span></span>
                        <span className="mr-20">Tổng nguyên giá: <span
                          className="text-red-italic">{convertNumberPriceRoundUp(totalOriginalCost || 0)}</span> </span>
                        <span>Tổng GTCL: <span
                          className="text-red-italic">{convertNumberPriceRoundUp(totalCarryingAmount || 0)}</span></span>
                      </div>
                    </div>
                  }
                  data={itemList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    sorting: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense",
                    // minBodyHeight: "450px",
                    maxBodyHeight: "450px",
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                  labelRowsPerPage={t("general.rows_per_page")}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                    }`
                  }
                  component="div"
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                  }}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={setRowsPerPage}
                />
              </Grid>
            </>
          ) : (
            <Grid item md={12} xs={12}>
              <MaterialTable
                title={t("general.list")}
                data={itemList}
                columns={columns}
                parentChildData={(row, rows) => {
                  return rows.find((a) => a.id === row.parentId);
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                  },
                }}
                options={{
                  sorting: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  defaultExpanded: true,
                  rowStyle: (rowData) => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: "450px",
                  minBodyHeight: "450px",
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                  padding: "dense",
                  toolbar: false,
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                component="div"
                count={totalElements}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={setRowsPerPage}
              />
            </Grid>
          )}
        </Grid>
      </div>
    );
  }

}

export default ComponentSummaryOfAssetsByMedicalEquipmentGroup;
