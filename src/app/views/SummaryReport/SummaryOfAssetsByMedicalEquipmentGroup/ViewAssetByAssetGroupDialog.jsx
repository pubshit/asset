import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { ValidatorForm } from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";
import "react-toastify/dist/ReactToastify.css";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  searchByPage,
} from "./SummaryOfAssetsByMedicalEquipmentGroupService";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import {
  convertNumberPrice,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../../appFunction";
import {appConst} from "../../../appConst";
import {PaperComponent} from "../../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class ViewAssetByMedicalEquipment extends Component {
  state = {
    departmentId: "",
    keyword: "",
    assetGroupId: "",
    rowsPerPage: 50,
    page: 0,
    assets: [],
    item: {},
    totalElements: 0,
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {});
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    let {organization} = getUserInformation();
    let {
      keyword,
    } = this.state;
    setPageLoading(true)
    let searchObject = {};
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.orgId = organization?.org?.id;

    if (keyword && keyword.trim().length > 0) {
      searchObject.keyword = keyword.trim();
    }
    searchObject.medicalEquipmentId = this.props.medicalEquipmentId;

    try {
      setPageLoading(true);
      let res = await searchByPage(searchObject);
      let {code, data} = res?.data;
      //asset
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: data?.content || [],
          // totalElements: data?.totalElements,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  search = () => {
    this.setPage(0);
  }

  componentDidMount = () => {
    this.updatePageData();
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let { keyword, rowsPerPage, page, totalElements, itemList } = this.state;
    let columns = [
      {
        title: t("general.index"),
        field: "code", width: "50px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.code"),
        field: "code", 
        minWidth: "120px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: "270px",
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.model"),
        field: "model",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("general.Quantity"),
        field: "quantity",
        // hidden: isTSCD && isTDCT,
        minWidth: 80,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        minWidth: "150px",
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => rowData?.originalCost ? convertNumberPrice(rowData?.originalCost) : ""
      },
      {
        title: t("Asset.carryingAmountTable"),
        field: "carryingAmount",
        minWidth: "150px",
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => rowData?.carryingAmount ? convertNumberPrice(rowData?.carryingAmount) : ""
      },
      {
        title: t("Asset.status"),
        field: "statusName", width: "150",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.assetSource"),
        field: "assetSourceNames",
        align: "left", width: "150",
        minWidth: "200px",
      },
      {
        title: t("Asset.store"),
        field: "storeName",
        align: "left", width: "150",
        minWidth: "200px",
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left", width: "150",
        minWidth: "200px",
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle className="mb-0 cursor-move"
            id="draggable-dialog-title"
          >
            <span className="mb-20">
              {t(
                "amount_of_assets_grouped_by_asset_group.list_asset_by_asset_group"
              )}
            </span>
          </DialogTitle>
          <DialogContent style={{ overflow: "hidden" }}>
            <Grid container justify="flex-start">
              <Grid item md={4} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    placeholder={t("Asset.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment position="end">
                        <SearchIcon onClick={() => this.search()} className="searchTable"/>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={"mt-12"}>
                <MaterialTable
                  data={itemList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`, 
                    },
                    pagination: appConst.localizationVi.pagination
                  }}
                  options={{
                    sorting: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: true,
                    search: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "500px",
                    minBodyHeight: "273px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense", 
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>

            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

ViewAssetByMedicalEquipment.contextType = AppContext
export default ViewAssetByMedicalEquipment;
