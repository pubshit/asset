import React from "react";
import { Icon, IconButton, Button } from "@material-ui/core";
import {
  exportToExcel,
  exportToExcelLissAsset,
  getSummary,
  getStatisticsInformation,
  searchByPage,
  searchByPageOrg,
  searchMedicalEquipment,
} from "./SummaryOfAssetsByMedicalEquipmentGroupService";
import FileSaver, { saveAs } from "file-saver";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import {
  convertNumberPrice,
  getTheHighestRole,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentAggregateAssetsByAssetGroupTable from "./ComponentSummaryOfAssetsByMedicalEquipmentGroup";
import OptionSummaryReport from "../Components/OptionSummaryReport";
import { useTranslation } from "react-i18next";
import { LightTooltip } from "../../Component/Utilities";


toast.configure({
  autoClose: 2000, draggable: false, limit: 3,
});


function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

let { organization } = getUserInformation();
let { isRoleAssetManager, departmentUser, isRoleAdmin } = getTheHighestRole();

class SummaryOfAssetsByMedicalEquipmentGroup extends React.Component {
  constructor(props) {
    super(props);
    this.keyPress = this.keyPress.bind(this);
  }

  state = {
    keyword: "",
    status: [],
    medicalEquipmentId: null,
    rowsPerPage: 10,
    page: 0,
    assetGroupId: "",
    medicalEquipmentTreeView: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    managementDepartment: null,
    useDepartment: null,
    originalCost: null,
    assetSource: null,
    store: null,
    assetType: null,
    organizationOrg: null,
    listOrganizationOrg: [],
    optionSummaryReport: {
      name: appConst.optionSummaryReport.TDCT.name, code: appConst.optionSummaryReport.TDCT.code
    },
    isGetSumary: true
  };

  setPage = (page) => {
    this.setState({ page, isGetSumary: true }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0, isGetSumary: false }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage, isGetSumary: false }, () => {
      this.updatePageData();
    })
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      useDepartment,
      keyword,
      originalCost,
      yearPutIntoUse,
      assetSource,
      store,
      managementDepartment,
      optionSummaryReport,
    } = this.state;
    setPageLoading(true)
    let searchObject = {};
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = this.state.assetType?.code;
    if (keyword && keyword.trim().length > 0) {
      searchObject.keyword = keyword.trim();
    }
    if (this.state.medicalEquipmentId !== variable.listInputName.all) {
      searchObject.medicalEquipmentId = this.state.medicalEquipmentId;
    }
    if (useDepartment) {
      searchObject.useDepartmentId = useDepartment.id;
    }
    if (originalCost) {
      searchObject.originalCost = originalCost;
    }
    if (yearPutIntoUse) {
      searchObject.yearPutIntoUse = yearPutIntoUse;
    }
    if (assetSource && assetSource.id) {
      searchObject.assetSourceId = assetSource.id;
    }
    if (store && store.id) {
      searchObject.storeId = store.id;
    }
    if (managementDepartment?.id || isRoleAssetManager) {
      searchObject.managementDepartmentId = isRoleAssetManager ? departmentUser?.id : managementDepartment?.id;
    }
    searchObject.orgId = this.state?.organizationOrg?.id || organization?.org?.id

    try {
      setPageLoading(true);
      if (optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code) {
        await this.handleGetDetailInformation(searchObject);
      } else {
        await this.handleGetSummaryInformation(searchObject);
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleGetDetailInformation = async (searchObject) => {
    let { isGetSumary } = this.state;
    let res = await searchByPage(searchObject);
    let { code, data } = res?.data;
    //asset
    if (isSuccessfulResponse(code)) {
      this.setState({
        itemList: data?.content || [],
        totalElements: data?.totalElements,
      });
    } else {
      handleThrowResponseMessage(res);
    }
    if (isGetSumary) {
      //summary
      let resSatistics = await getSummary(searchObject);
      if (isSuccessfulResponse(resSatistics?.data?.code)) {
        let { data } = resSatistics?.data;
        this.setState({
          totalAsset: data?.quantity,
          totalOriginalCost: data?.totalOriginalCost,
          totalCarryingAmount: data?.totalCarryingAmount,
        });
      } else {
        handleThrowResponseMessage(resSatistics);
      }
    }
  }

  flattenArray = (arr) => {
    const result = [];

    function recurse(items) {
      for (const item of items) {
        const { children, ...rest } = item;
        result.push(rest);
        if (children && children.length > 0) {
          recurse(children);
        }
      }
    }

    recurse(arr);
    return result;
  }

  handleGetSummaryInformation = async (searchObject) => {
    try {
      let res = await getStatisticsInformation(searchObject);
      let { code, data } = res?.data;

      let newArr = this.flattenArray(data);

      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: newArr,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      console.log(error);
    }
  }


  /* Export to excel */
  handleExportToExcel = async (e, type) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let {
      managementDepartment,//moi
      useDepartment,
      assetSource,
      store,
      keyword,
      yearPutIntoUse,
      assetType,
      optionSummaryReport,
      organizationOrg,
      originalCost,
    } = this.state;
    let searchObject = {};
    searchObject.assetClass = assetType?.code;
    searchObject.keyword = keyword.trim();
    searchObject.medicalEquipmentId = this.state?.medicalEquipmentId;
    searchObject.useDepartmentId = useDepartment?.id;
    searchObject.originalCost = originalCost;
    searchObject.yearPutIntoUse = yearPutIntoUse;
    searchObject.assetSourceId = assetSource?.id;
    searchObject.storeId = store?.id;
    searchObject.managementDepartmentId = managementDepartment?.id;
    searchObject.orgId = organizationOrg?.id || organization?.org?.id
    // searchObject.medicalEquipmentCode = keyword; 

    setPageLoading(true);
    let statusGeneral = optionSummaryReport.code
      === appConst.optionSummaryReport.TDCT.code
    try {
      let functionExportToExcel = statusGeneral
        ? exportToExcel
        : exportToExcelLissAsset
      let res = await functionExportToExcel(searchObject);

      try {
        //Nếu call api bị lỗi 500 thì convert được sang object để check lỗi
        const data = JSON.parse(res?.data)
        toast.warning(data.message)
      }
      catch (e) {
        // Nếu call api thành công data ở dạng blob chỉ có thể lưu
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        statusGeneral
          ? FileSaver.saveAs(blob, "AssetListReport.xlsx")
          : FileSaver.saveAs(blob, "AssetAmountReport.xlsx");
        toast.success(t("general.successExport"));
      }
    }
    catch (e) {
      console.log(e)
    }
    finally {
      setPageLoading(false);
    }
  };

  getTreeView = async () => {
    let { t } = this.props;
    let objectSearch = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      typeCode: appConst.listCommonObject.LOAI_THIET_BI_Y_TE.code,
      orgId: this.state.organizationOrg?.id || organization?.org?.id
    }
    try {
      let res = await searchMedicalEquipment(objectSearch)
      if (isSuccessfulResponse(res?.data?.code)) {
        let medicalEquipmentTreeView = {
          id: "all",
          name: t("Dashboard.subcategory.medicalEquipmentType"),
          children: res?.data?.data,
        };
        this.setState({ medicalEquipmentTreeView });
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    }
  }

  componentDidMount() {
    let { t } = this.props;

    if (isRoleAssetManager) {
      this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
    };

    searchByPageOrg(organization?.org?.id).then((data) => {
      this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
    }).catch(() => {
      toast.error(t("toastr.error"))
    })
    this.updatePageData();
    this.getTreeView();
  }

  componentWillMount() { }

  selectManagementDepartment = (item) => {
    this.setState({ managementDepartment: item }, function () {
      this.search();
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item, shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked === false) {
        selectAllItem = false;
      }
      if (Asset[i].id === item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };
  handleSelectAllClick = () => {
    let { Asset } = this.state;
    for (let i = 0; i < Asset.length; i++) {
      Asset[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, Asset: Asset });
  };

  handleDelete = (id) => {
    this.setState({
      id, shouldOpenConfirmationDialog: true,
    });
  };

  selectStatus = (statusSelected) => {
    this.setState({ status: statusSelected }, function () {
      this.search();
    });
  };

  search = () => {
    this.setPage(0);
  }
  selectUseDepartment = (selected) => {
    this.setState({ useDepartment: selected }, () => {
      this.search();
    })
  }

  selectAssetSource = (selected) => {
    this.setState({ assetSource: selected }, () => {
      this.search();
    })
  }

  selectStore = (selected) => {
    this.setState({ store: selected }, () => {
      this.search();
    })
  }

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
      managementDepartment: null,
      medicalEquipmentId: null,
      useDepartment: null,
      assetSource: null,
      store: null
    }, () => {
      this.search();
      this.getTreeView();
    })
  }

  keyPress(e, value, nameState) {
    if (e.key === appConst.KEY.ENTER) {
      this.setState({ [nameState]: value }, () => this.search())
    }
  }

  selectYear = (value) => {
    if (value) {
      this.setState({ yearPutIntoUse: value.year }, () => this.search())
    } else {
      this.setState({ yearPutIntoUse: value }, () => this.search())
    }
  }

  handleToggle = () => {
    const newOption = {
      [appConst.optionSummaryReport.TDCT.code]: appConst.optionSummaryReport.TDTH,
      [appConst.optionSummaryReport.TDTH.code]: appConst.optionSummaryReport.TDCT,
    }
    this.setState({
      optionSummaryReport: newOption[this.state.optionSummaryReport.code] || appConst.optionSummaryReport.TDCT,
      medicalEquipmentId: "",
    }, () => {
      this.updatePageData()
    })
  }

  handleKeyDown = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleSetDataSelect = (data, name) => {
    this.setState({
      [name]: data
    });
  };

  handleSelectAssetType = assetType => {
    this.setState({ assetType }, this.search)
  }

  handleSelectMedicalEquipment = (e, value) => {
    this.setState({
      medicalEquipmentId: value,
    }, this.search)
  }

  handleOpenDialog = () => {
    this.setState({
      shouldOpenViewAssetByMedicalEquipment: true,
    })
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByMedicalEquipment: false,
    });
  };

  handleShouleOpenViewAssetByMedicalEquipment = (rowData) => {
    this.setState({
      medicalEquipmentId: rowData?.id,
    }, () => {
      this.handleOpenDialog();
    })
  }
  render() {
    const { t, i18n } = this.props;
    const { tabValue, rowsPerPage, page, assetType, optionSummaryReport, organizationOrg } = this.state;
    let searchObject = { pageIndex: 0, pageSize: 10000000 };
    let searchObjectStore = {
      pageIndex: 0,
      pageSize: 10000000,
      departmentId: isRoleAdmin ? "" : departmentUser?.id || "",
      orgId: organizationOrg?.id || organization?.org?.id
    };
    let searchObjectManagementDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
      isAssetManagement: true
    }
    let searchObjectUseDepartment = {
      pageIndex: 1,
      pageSize: 1000000,
      orgId: organizationOrg?.id || organization?.org?.id,
    }
    let searchObjectAssetSource = {
      pageIndex: 0,
      pageSize: 10000000,
      orgId: organizationOrg?.id || organization?.org?.id
    }
    let dateNow = new Date().getFullYear();
    let listYear = [];
    for (let i = dateNow; i >= 1970; i--) {
      listYear.push({ year: i });
    }
    let isTSCD = assetType?.code === appConst.assetClass.TSCD;
    let isTDCT = optionSummaryReport?.code === appConst.optionSummaryReport.TDCT?.code;
    let TitlePage = t("Dashboard.summary_report.summaryByMedicalEquipment");

    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        hidden: isTDCT,
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.view) {
                this.handleShouleOpenViewAssetByMedicalEquipment(rowData)
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "code",
        minWidth: "50px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.code"),
        field: "code",
        minWidth: "120px",
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: "270px",
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.model"),
        field: "model",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("general.Quantity"),
        field: "quantity",
        hidden: isTSCD && isTDCT,
        minWidth: 80,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        minWidth: "150px",
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => isTDCT
          ? rowData?.originalCost ? convertNumberPrice(rowData?.originalCost) : ""
          : rowData?.totalOriginalCost ? convertNumberPrice(rowData?.totalOriginalCost) : ""
      },
      {
        title: t("Asset.carryingAmountTable"),
        field: "carryingAmount",
        minWidth: "150px",
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => isTDCT
          ? rowData?.carryingAmount ? convertNumberPrice(rowData?.carryingAmount) : ""
          : rowData?.totalOriginalCost ? convertNumberPrice(rowData?.totalOriginalCost) : ""
      },
      {
        title: t("Asset.status"),
        field: "statusName",
        width: "150",
        minWidth: "150px",
        hidden: !isTDCT,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.assetSource"),
        field: "assetSourceNames",
        align: "left",
        hidden: !isTDCT,
        minWidth: "200px",
      },
      {
        title: t("Asset.store"),
        field: "storeName",
        align: "left",
        hidden: !isTDCT,
        minWidth: "200px",
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        hidden: !isTDCT,
        minWidth: "200px",
      },
    ];

    return (<div className="m-sm-30">
      <Helmet>
        <title>
          {TitlePage} | {t("web_site")}
        </title>
      </Helmet>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[{ name: t("Dashboard.summary_report.title"), path: "/" },
          { name: TitlePage },]}
        />
      </div>
      <div className="flex mb-16 spaceBetween">
        <Button
          className=""
          variant="contained"
          color="primary"
          onClick={this.handleExportToExcel}
        >
          {t("general.exportToExcel")}
        </Button>
        <OptionSummaryReport
          checked={this.state.optionSummaryReport?.code === appConst.optionSummaryReport.TDTH.code}
          handleChange={this.handleToggle}
          name="optionSumaryReport"
          label={this.state.optionSummaryReport?.name}
          noPosition={true}
        />
      </div>
      {/*{optionSummaryReport.code === appConst.optionSummaryReport.TDCT.code ? (*/}
      <ComponentAggregateAssetsByAssetGroupTable
        t={t}
        item={this.state}
        selectManagementDepartment={this.selectManagementDepartment}
        searchObject={searchObject}
        selectStatus={this.selectStatus}
        selectUseDepartment={this.selectUseDepartment}
        selectAssetSource={this.selectAssetSource}
        searchObjectStore={searchObjectStore}
        searchObjectAssetSource={searchObjectAssetSource}
        searchObjectManagementDepartment={searchObjectManagementDepartment}
        searchObjectUseDepartment={searchObjectUseDepartment}
        selectStore={this.selectStore}
        listYear={listYear}
        keyPress={this.keyPress}
        selectYear={this.selectYear}
        search={this.search}
        columns={columns}
        handleChangePage={this.handleChangePage}
        setRowsPerPage={this.setRowsPerPage}
        handleSelectAssetType={this.handleSelectAssetType}
        handleSelectMedicalEquipment={this.handleSelectMedicalEquipment}
        handleDialogClose={this.handleDialogClose}
        selectOrganizationOrg={this.selectOrganizationOrg}
        handleKeyDown={this.handleKeyDown}
        handleKeyUp={this.handleKeyUp}
        handleSetDataSelect={this.handleSetDataSelect}
        isRoleAssetManager={isRoleAssetManager}
      />
      {/*) : (<AmountOfAssetsGroupedByAssetGroup t={this.props.t}/>)}*/}
    </div>);
  }
}

SummaryOfAssetsByMedicalEquipmentGroup.contextType = AppContext
export default SummaryOfAssetsByMedicalEquipmentGroup;
