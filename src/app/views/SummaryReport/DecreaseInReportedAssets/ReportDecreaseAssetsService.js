import axios from "axios";
import ConstantList from "../../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/summary_report" + ConstantList.URL_PREFIX;
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report";
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  var url =
    API_PATH +
    "/amount_of_assets_grouped_by_asset_group/decreaseInReportedAssets";
  return axios.post(url, searchObject);
};

export const searchByPageNew = (searchObject) => {
  var url = API_PATH_NEW + "/so-ghi-giam";
  return axios.post(url, searchObject);
};

export const getByPage = (searchObject) => {
  var url =
    API_PATH +
    "/amount_of_assets_grouped_by_asset_group/getPageReportIncreaseOrDecreaseAssets";
  return axios.post(url, searchObject);
};

export const searchByPageAsset = (searchObject) => {
  var url =
    API_PATH +
    "/amount_of_assets_grouped_by_asset_group/searchByPageDepartment";
  return axios.post(url, searchObject);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_NEW + "/excel/so-ghi-giam",
    data: searchObject,
    responseType: "blob",
  });
};
