import {
  Button,
  Grid,
  Slider,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField
} from "@material-ui/core";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import moment from "moment";
import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import {
  exportToExcel,
  searchByPageNew,
} from "./ReportDecreaseAssetsService";
import ViewAssetByDepartmentDialog from "./ViewAssetByDepartmentDialog";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi"
import {
  convertFromToDate,
  convertNumberPrice,
  formatDateDto,
  formatTimestampToDate, getUserInformation, handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import Autocomplete from "@material-ui/lab/Autocomplete";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
let { organization } = getUserInformation()
class ReportIncreaseOrDecreaseAssets extends React.Component {
  state = {
    year: moment(new Date()).format("YYYY").toString(),
    keyword: "",
    status: [],
    assetGroupId: "",

    assetDepartmentId: "",
    rowsPerPage: 9999,
    page: 0,
    Asset: [],
    item: {},
    itemList: [],
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByDepartmentDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    fromToDate: moment().endOf("year"),
    toDate: new Date(moment().endOf("month")),
    fromDate: new Date(moment().startOf("month")),
    statusIndex: 5,
    statusIndexTT07: 6,
    listGroup: [],
    group: null,
    assetGroup: null,
    managementDepartment: null,
    managementDepartmentId: "",
    listManageDepartment: [],
    isTemporary: null,
    isManageAccountant: null,
    assetSource: null,
    useDepartment: null,
    ranger: [0, 500],
    rangerValue: 500,
    organizationOrg: null,
    listOrganizationOrg: []
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    if (!this.state.fromDate || !this.state.toDate) return;
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
    searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id
    try {
      setPageLoading(true);
      let result = await searchByPageNew(searchObject);
      const { code, data } = result?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: data?.length > 0 ? [...data] : [],
        });
      } else {
        handleThrowResponseMessage(result);
      }
    } catch (error) {
      toast.error("general.error");
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  componentDidMount = () => {
    let { t } = this.props;
    this.setState({ organizationOrg: organization?.org }, () => {
      searchByPageOrg(organization?.org?.id).then((data) => {
        this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
      }).catch(() => {
        toast.error(t("toastr.error"))
      })
      this.updatePageData();
    })
  }

  search = () => {
    this.setPage(0);
  }

  handleSetDate = (data, name) => {
    this.setState({
      [name]: data,
    }, function () {
      if (!isValidDate(data)) {
        return;
      }
      this.search()
    });
  };

  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected
    }, () => {
      this.search();
    })
  }

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        fromDate: fromDate ? formatDateDto(fromDate) : "",
        toDate: toDate ? formatDateDto(toDate) : "",
        orgId: this.state.organizationOrg?.id || organization?.org?.id
      };
      const res = await exportToExcel(searchObject);
      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "DecreaseInReportedAssets.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  valueLabelFormat = (value) => {
    return `${value}`;
  };

  render() {
    const { t, i18n } = this.props;
    let {
      itemList,
      item,
      shouldOpenViewAssetByDepartmentDialog,
      assetDepartmentId,
      assetSource,
      toDate,
      fromDate,
      organizationOrg,
      listOrganizationOrg
    } = this.state;
    let TitlePage = "Báo cáo giảm tài sản";

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.summary_report.title") },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2} justifyContent="flex-start">
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                className="mb-16 mr-16 align-bottom"
                margin="none"
                id="mui-pickers-date"
                label={t("ReceivingAsset.fromDate")}
                inputVariant="standard"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"fromDate"}
                value={fromDate}
                minDate={new Date("01/01/1900")}
                minDateMessage={t("general.minDateDefault")}
                maxDate={toDate ? toDate : new Date("01/01/2100")}
                maxDateMessage={
                  toDate
                    ? t("ReceivingAsset.maxFilterDate")
                    : t("general.maxDateDefault")
                }
                invalidDateMessage={t("general.invalidDateFormat")}
                onChange={(date) => this.handleSetDate(date, "fromDate")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t("ReceivingAsset.toDate")}
                inputVariant="standard"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"toDate"}
                value={toDate}
                maxDate={new Date("01/01/2100")}
                maxDateMessage={t("general.maxDateDefault")}
                minDate={fromDate ? fromDate : new Date("01/01/1900")}
                minDateMessage={
                  toDate
                    ? t("ReceivingAsset.minFilterDate")
                    : t("general.minDateDefault")
                }
                invalidDateMessage={t("general.invalidDateFormat")}
                onChange={(date) => this.handleSetDate(date, "toDate")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <Autocomplete
              className="mt-3"
              size="small"
              id="combo-box"
              options={listOrganizationOrg}
              value={organizationOrg}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t("WarehouseInventory.filterOrg")}
                  value={organizationOrg}
                />
              )}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              onChange={(event, value) => {
                this.selectOrganizationOrg(value);
              }}
            />
          </Grid>
          <Grid item md={2} sm={6} xs={12} style={{ textAlign: "center" }}>
            <Button
              className="mt-12"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-12">
          <div>
            {shouldOpenViewAssetByDepartmentDialog &&
              this.state.assetDepartmentId && (
                <ViewAssetByDepartmentDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenViewAssetByDepartmentDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  assetSourceId={assetSource?.id}
                  departmentId={assetDepartmentId}
                />
              )}
          </div>

          <div className="MuiPaper-elevation2 MuiPaper-rounded overflow-auto">
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center" rowSpan="2">STT</TableCell>
                    <TableCell align="center" colSpan="2">Chứng từ</TableCell>
                    <TableCell align="center" rowSpan="2">Tên, đặc điểm, ký hiệu TSCĐ</TableCell>
                    <TableCell align="center" rowSpan="2">Nước sản xuất</TableCell>
                    <TableCell align="center" rowSpan="2">Năm sản xuất</TableCell>
                    <TableCell align="center" rowSpan="2">Năm sử dụng</TableCell>
                    <TableCell align="center" rowSpan="2">Số hiệu TSCĐ</TableCell>
                    <TableCell align="center" rowSpan="2">Giá trị ghi giảm</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="center">Số</TableCell>
                    <TableCell align="center">Ngày</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {itemList?.map((entity, index) => (
                    <Fragment key={index}>
                      {entity.totalQuantityIncrease !== 0 && (
                        <TableRow>
                          <TableCell align="left" colSpan={9} className="font-weight-bold" style={{ borderTopWidth: 2 }}>
                            Loại: {entity?.assetGroupName}
                          </TableCell>
                        </TableRow>
                      )}

                      {entity.assets?.map((entityAssetDetail, entityAssetDetailIndex) => (
                        <TableRow key={entityAssetDetailIndex}>
                          <TableCell align="center">
                            {entityAssetDetailIndex + 1}
                          </TableCell>
                          <TableCell align="left">
                            {entityAssetDetail?.voucherCode || ""}
                          </TableCell>
                          <TableCell align="center">
                            {entityAssetDetail?.voucherDate
                              ? formatTimestampToDate(entityAssetDetail?.voucherDate)
                              : ""
                            }
                          </TableCell>
                          <TableCell align="left">
                            {entityAssetDetail?.assetName || ""}
                          </TableCell>
                          <TableCell align="center">
                            {entityAssetDetail?.manufacturingCountry || ""}
                          </TableCell>
                          <TableCell align="center">
                            {entityAssetDetail?.yearOfManufacturing || ""}
                          </TableCell>
                          <TableCell align="center">
                            {entityAssetDetail?.yearPutIntoUse || ""}
                          </TableCell>
                          <TableCell align="center">
                            {entityAssetDetail?.assetCode || ""}
                          </TableCell>
                          <TableCell align="right">
                            {convertNumberPrice(entityAssetDetail?.value) || 0}
                          </TableCell>
                        </TableRow>
                      ))}

                      <TableRow>
                        <TableCell colSpan="8" align="left" className="font-weight-bold">
                          Cộng loại: {entity?.assetGroupName || ""}
                        </TableCell>
                        <TableCell align="right" className="font-weight-bold">
                          {convertNumberPrice(entity?.totalCost) || 0}
                        </TableCell>
                      </TableRow>
                    </Fragment>
                  ))}

                  <TableRow>
                    <TableCell colSpan="9" align="left" className="font-weight-bold">
                      <span >Tổng cộng:</span>
                      <span className="float-right">
                        {convertNumberPrice(itemList?.reduce((acc, item) => acc + item?.totalCost, 0))}
                      </span>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </Grid>
      </div>
    );
  }
}
ReportIncreaseOrDecreaseAssets.contextType = AppContext;
export default ReportIncreaseOrDecreaseAssets;
