import { EgretLoadable } from "egret";
import ConstantList from "../../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const ToolAllocationReport = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./ToolAllocationReport")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(ToolAllocationReport);

const ToolAllocationReportRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "summary_report/tool_allocation_report",
    exact: true,
    component: ViewComponent
  }
];

export default ToolAllocationReportRoutes;
