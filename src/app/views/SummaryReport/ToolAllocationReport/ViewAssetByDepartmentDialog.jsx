import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  TablePagination,
  DialogActions,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { ValidatorForm } from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  exportToExcel,
  searchByPageAsset as searchByPage,
} from "./ToolAllocationReportService";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class ViewAssetByAssetGroupDialog extends Component {
  state = {
    departmentId: "",
    keyword: "",
    assetGroupId: "",
    rowsPerPage: 10,
    page: 0,
    assets: [],
    item: {},
    totalElements: 0,
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {});
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId != "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.assetGroup = { id: this.state.assetGroupId };
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetGroupId = this.state.assetGroupId;
    searchByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword;
      if (this.state.departmentId != "all") {
        searchObject.departmentId = this.state.departmentId;
      }
      searchObject.assetGroup = { id: this.state.assetGroupId };
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.assetGroupId = this.state.assetGroupId;
      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      });
    });
  }

  handleFormSubmit = () => {};

  componentWillMount() {
    let { assetGroupId, departmentId } = this.props;

    this.setState({ assetGroupId, departmentId }, function () {
      this.setState(
        {
          ...this.props.item,
        },
        function () {
          if (this.state.departmentId) {
            this.search();
          }
        }
      );
    });
  }

  componentDidMount() {}

  render() {
    let { open, t, i18n } = this.props;
    let { rowsPerPage, page, totalElements, itemList } = this.state;

    let columns = [
      {
        title: t("general.index"),
        field: "code",
        width: "50px",
        align: "center",
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        minWidth: "200px",
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        align: "left",
        minWidth: "150",
      },
      {
        title: t("AssetSource.title"),
        field: "asset.assetSource.name",
        align: "left",
        minWidth: "150",
      },
      {
        title: t("Asset.originalCost"),
        field: "asset.originalCost",
        align: "left",
        // width: '250px'
      },
      {
        title: t("Asset.carryingAmount"),
        field: "asset.carryingAmount",
        align: "left",
      },
      {
        title: t("Asset.useDepartment"),
        field: "asset.useDepartment.name",
        align: "left",
        minWidth: "180px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
        },
      },

      //   {
      //     title: t('Asset.usePerson'),
      //     field: 'originalCost',
      //     align: 'left',
      //     width: '180px',
      //     headerStyle: {
      //       paddingLeft: '10px',
      //       paddingRight: '10px',
      //     },
      //   },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            <span className="mb-20">
              {t(
                "amount_of_assets_grouped_by_asset_group.list_asset_by_department"
              )}
            </span>
          </DialogTitle>
          <DialogContent>
            <Grid container justify="flex-end">
              <Grid item md={6} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    placeholder={t("Asset.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment>
                        <Link>
                          {" "}
                          <SearchIcon
                            onClick={() => this.search()}
                            style={{
                              position: "absolute",
                              top: "0",
                              right: "0",
                            }}
                          />
                        </Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={"mt-36"}>
                <MaterialTable
                  data={itemList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                  component="div"
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t("general.of")} ${
                      count !== -1 ? count : `more than ${to}`
                    }`
                  }
                  page={page}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                  }}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                  }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-36"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>

              {/* <Button
                                variant="contained"
                                color="primary"
                                type="submit"
                            >
                                {t('general.save')}
                            </Button> */}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default ViewAssetByAssetGroupDialog;
