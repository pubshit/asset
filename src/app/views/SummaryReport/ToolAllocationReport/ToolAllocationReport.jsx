import {
  Button,
  Grid,
  Slider,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@material-ui/core";
import { Breadcrumb } from "egret";
import { saveAs } from "file-saver";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { getListManagementDepartment } from "../../Asset/AssetService";
import { getByRoot } from "../../AssetGroup/AssetGroupService";
import { getTreeView as getAllDepartmentTreeView, } from "../../Department/DepartmentService";
import {
  exportToExcel,
  getIncreaseDecreaseReport,
} from "./ToolAllocationReportService";
import ViewAssetByDepartmentDialog from "./ViewAssetByDepartmentDialog";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { DatePicker } from "@material-ui/pickers";
import { formatDateDtoMore } from "app/appFunction";
import DateFnsUtils from "@date-io/date-fns";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContext from "app/appContext";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
class ReportIncreaseOrDecreaseAssets extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    year: moment(new Date()).format("YYYY").toString(),
    keyword: "",
    status: [],
    assetGroupId: "",
    assetDepartmentId: "",
    rowsPerPage: 10000,
    page: 0,
    Asset: [],
    item: {},
    itemList: [],
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByDepartmentDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    fromToDate: moment().endOf("year"),
    fromDate: formatDateDtoMore(moment(new Date()).subtract(1, "months"), "start"),
    toDate: formatDateDtoMore(new Date()),
    listGroup: [],
    group: null,
    assetGroup: null,
    managementDepartment: null,
    managementDepartmentId: "",
    listManageDepartment: [],
    isTemporary: null,
    isManageAccountant: null,
    assetSource: null,
    useDepartment: null,
    ranger: [0, 500],
    rangerValue: 500,
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;

  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let { departmentId, status, page, rowsPerPage } = this.state;
    let { setPageLoading } = this.context
    setPageLoading(true)
    let { t } = this.props;
    var searchObject = {};
    if (departmentId != "all") {
      searchObject.departmentId = departmentId;
    }
    searchObject.status = status ? status : null;
    searchObject.fromToDate = this.state.fromToDate;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromDate = this.state.fromDate;
    searchObject.toDate = this.state.toDate;
    searchObject.assetGroupId = this.state.assetGroupId;
    searchObject.managementDepartmentId = this.state.useDepartment?.id;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.year = this.state.year;
    if (this.state.rangerValue) {
      searchObject.rangerValue = this.state.rangerValue * 1000000;
    }
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    let start = page * rowsPerPage;
    let end = (page + 1) * rowsPerPage;

    getIncreaseDecreaseReport(searchObject).then(({ data }) => {
      this.setState({
        itemList: data?.data?.length > 0 ? [...data.data] : [],
        totalElements: data?.total
      });
    }).catch(() => {
      toast.error(t("general.error"))
    }).finally(() => {
      setPageLoading(false)
    })
      ;
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByDepartmentDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    this.updatePageData();
  }

  componentWillMount() {
    var searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 1000;
    getByRoot(searchObject).then(({ data }) => {
      this.setState({ listGroup: [...data.content] });
    });

    getListManagementDepartment().then(
      (data) => {
        this.setState({ listManageDepartment: [...data.data] });
      },
      function () { }
    );
  }

  handleFormSubmit = () => { };

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  search() {
    this.setState({ page: 0 }, function () {
      let { setPageLoading } = this.context
      setPageLoading(true)
      let { t } = this.props;
      let { departmentId, status, page, rowsPerPage } = this.state;
      let searchObject = {};
      if (departmentId !== "all") {
        searchObject.departmentId = departmentId;
      }
      searchObject.status = status ? status : null;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromDate = this.state.fromDate;
      searchObject.toDate = this.state.toDate;
      searchObject.assetGroupId = this.state.assetGroupId;
      searchObject.managementDepartmentId = this.state.useDepartment?.id;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      searchObject.year = this.state.year;
      if (this.state.rangerValue) {
        searchObject.rangerValue = this.state.rangerValue * 1000000;
      }
      if (this.state.isInverted != null) {
        searchObject.isInverted = this.state.isInverted;
      }
      let start = page * rowsPerPage;
      let end = (page + 1) * rowsPerPage;
      getIncreaseDecreaseReport(searchObject).then(({ data }) => {
        this.setState({
          itemList: data?.data?.length > 0 ? [...data.data] : [],
          totalElements: data?.total
        });
      }).catch(() => {
        toast.error(t("general.error"))
      }).finally(() => {
        setPageLoading(false)
      })
    });
  }
  getAssetBydepartment = (event, departmentId) => {
    if (departmentId) {
      this.setState({ departmentId: departmentId }, function () {
        this.search();
      });
    }
  };
  handleDateChange = (dateValue, name) => {
    this.setState({
      [name]: name === "toDate" ? formatDateDtoMore(dateValue) : formatDateDtoMore(dateValue, "start")
    }, () => {
      this.search()
    })
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () { }
      );
    });
  };
  // end Tree view

  handleChangeSlider = (event, newValue) => {
    if (newValue >= 10000) {
      newValue = 10000;
    }
    this.setState({ rangerValue: newValue }, () => {
      // this.search();
    });
  };
  handleDeleteButtonClick = (item) => {
    this.setState({
      item: item,
      shouldOpenDepartmentPopup: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { departmentId, status, page, rowsPerPage } = this.state;
    var searchObject = {};
    if (departmentId != "all") {
      searchObject.departmentId = departmentId;
    }
    searchObject.status = status ? status : null;
    searchObject.fromToDate = this.state.fromToDate;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromDate = this.state.fromDate;
    searchObject.toDate = this.state.toDate;
    searchObject.assetGroupId = this.state.assetGroupId;
    searchObject.managementDepartmentId = this.state.useDepartment?.id;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.year = this.state.year;
    if (this.state.rangerValue) {
      searchObject.rangerValue = this.state.rangerValue * 1000000;
    }
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    let start = page * rowsPerPage;
    let end = (page + 1) * rowsPerPage;
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "ReportIncreaseOrDecreaseAssets.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  selectGroup = (item) => {
    this.setState(
      { assetGroupId: item ? item.id : "", assetGroup: item ? item : null },
      function () {
        this.search();
      }
    );
  };
  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
  };

  romanize = (num) => {
    var lookup = { "M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40, "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1 }, roman = '', i;
    for (i in lookup) {
      while (num >= lookup[i]) {
        roman += i;
        num -= lookup[i];
      }
    }
    return roman;
  }

  selectYear = (value) => {
    if (value) {
      this.setState({ year: value.year }, () => this.search())
    } else {
      this.setState({ year: value }, () => this.search())
    }
  }
  valueLabelFormat = (value) => {
    return `${value}`;
  };

  renderUI = (data, prefix = "", index = 1) => {
    let { page, rowsPerPage } = this.state
    let ui = data.map((item, index) => {
      let stt = prefix ? prefix + "." + this.romanize(page * rowsPerPage + (index + 1)) : this.romanize(page * rowsPerPage + (index + 1))
      return <>
        <TableRow  >
          {/* <TableCell className="px-0" align="left">
            <span style={{ fontWeight: "bold" }}> {stt} </span>
          </TableCell> */}
          <TableCell className="px-0" align="left">
            <span style={{ fontWeight: "bold" }}>{moment().format("TMM.YY")}</span>
          </TableCell>
          <TableCell className="px-0" align="left">
            <span style={{ fontWeight: "bold" }}>{item?.assetGroupName}</span>
          </TableCell>
          <TableCell className="px-0" align="center">
            {item.soLuongDauKy === 0 ? 0 : item.soLuongDauKy}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy).toLocaleString("en-US") === 0 ? 0 : Number(item.nguyenGiaDauKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="center">
            {item.soLuongTangTrongKy === 0 ? 0 : item.soLuongTangTrongKy}
          </TableCell>
          <TableCell className="px-0" align="right">

            {Number(item.nguyenGiaTangTrongKy).toLocaleString("en-US") === 0 ? 0 : Number(item.nguyenGiaTangTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="center">
            {item.soLuongGiamTrongKy === 0 ? 0 : item.soLuongGiamTrongKy}
          </TableCell>
          <TableCell className="px-0" align="right">

            {Number(item.nguyenGiaGiamTrongKy).toLocaleString("en-US") === 0 ? 0 : Number(item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}

          </TableCell>
          <TableCell className="px-0" align="center">
            {item.soLuongDauKy + item.soLuongTangTrongKy - item.soLuongGiamTrongKy}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}

          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
          <TableCell className="px-0" align="right">
            {Number(item.nguyenGiaDauKy + item.nguyenGiaTangTrongKy - item.nguyenGiaGiamTrongKy).toLocaleString("en-US")}
          </TableCell>
        </TableRow>
        {/* {item?.products != null && item.products.map((entityProduct, entityProductIndex) => (
          <>
            <TableRow>
              <TableCell className="px-0">
                <span style={{ fontWeight: "bold" }}>{stt}</span>.{entityProductIndex + 1}
              </TableCell>
              <TableCell className="px-0" align="left">
                <span style={{ paddingLeft: 10 }}> {entityProduct.productName}   </span>
              </TableCell>
              <TableCell className="px-0" align="center">
                {entityProduct.soLuongDauKy === 0 ? 0 : entityProduct.soLuongDauKy}
              </TableCell>
              <TableCell className="px-0" align="right">
                {Number(entityProduct.nguyenGiaDauKy).toLocaleString("en-US") === 0 ? 0 : Number(entityProduct.nguyenGiaDauKy).toLocaleString("en-US")}
              </TableCell>
              <TableCell className="px-0" align="center">
                {entityProduct.soLuongTangTrongKy === 0 ? 0 : entityProduct.soLuongTangTrongKy}
              </TableCell>
              <TableCell className="px-0" align="right">

                {Number(entityProduct.nguyenGiaTangTrongKy).toLocaleString("en-US") === 0 ? 0 : Number(entityProduct.nguyenGiaTangTrongKy).toLocaleString("en-US")}
              </TableCell>
              <TableCell className="px-0" align="center">
                {entityProduct.soLuongGiamTrongKy === 0 ? 0 : entityProduct.soLuongGiamTrongKy}
              </TableCell>
              <TableCell className="px-0" align="right">

                {Number(entityProduct.nguyenGiaGiamTrongKy).toLocaleString("en-US") === 0 ? 0 : Number(entityProduct.nguyenGiaGiamTrongKy).toLocaleString("en-US")}

              </TableCell>
              <TableCell className="px-0" align="center">
                {entityProduct.soLuongDauKy + entityProduct.soLuongTangTrongKy - entityProduct.soLuongGiamTrongKy}
              </TableCell>
              <TableCell className="px-0" align="right">
                {Number(entityProduct.nguyenGiaDauKy + entityProduct.nguyenGiaTangTrongKy - entityProduct.nguyenGiaGiamTrongKy).toLocaleString("en-US")}

              </TableCell>
            </TableRow>
          </>
        ))}
        {item?.childGroups && this.renderUI(item?.childGroups, stt)} */}
      </>
    })
    return ui
  }
  render() {
    const { t, i18n } = this.props;
    let dateNow = new Date().getFullYear();
    let listYear = [];
    for (let i = dateNow; i >= 1970; i--) {
      listYear.push({ year: i });
    }

    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenViewAssetByDepartmentDialog,
      assetDepartmentId,
      managementDepartment,
      assetSource,
      assetGroup,
      useDepartment,
      year,
      rangerValue,
      fromDate,
      toDate
    } = this.state;

    let TitlePage = t(
      "Dashboard.summary_report.tool_allocation_report"
    );

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.summary_report.tool_allocation_report") },
              { name: TitlePage },
            ]}
          />
        </div>
        <Grid container spacing={2} justify="flex-start">
          <Grid item md={2} sm={2} xs={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span style={{ color: "red" }}> </span>
                    {t("WarehouseInventory.fromDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"fromDate"}
                value={fromDate}
                onChange={(date) => this.handleDateChange(date, "fromDate")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={2} xs={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span style={{ color: "red" }}> </span>
                    {t("WarehouseInventory.toDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"toDate"}
                value={toDate}
                onChange={(date) => this.handleDateChange(date, "toDate")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={6} sm={6} xs={12} justify="flex-end" style={{ textAlign: "right" }}></Grid>
          <Grid item md={2} sm={6} xs={12} justify="flex-end" style={{ textAlign: "right" }}>
            <Button
              className="mt-12"
              size="small"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-12">
          <div>
            {shouldOpenViewAssetByDepartmentDialog &&
              this.state.assetDepartmentId && (
                <ViewAssetByDepartmentDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenViewAssetByDepartmentDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  assetSourceId={assetSource?.id}
                  departmentId={assetDepartmentId}
                />
              )}
          </div>

          <TableContainer className="bao-cao">
            <Table className="crud-table table-report">
              <TableHead>
                <TableRow >
                  <TableCell align="center" rowSpan="2">Thời gian nhập</TableCell>
                  <TableCell align="center" rowSpan="2"> Tên tài sản </TableCell>
                  <TableCell align="center" rowSpan="2">ĐVT</TableCell>
                  <TableCell align="center" rowSpan="2">Khoa</TableCell>
                  <TableCell align="center" rowSpan="2">SL</TableCell>
                  <TableCell align="center" colSpan="3">Số đầu kỳ ({moment().startOf('month').format("DD/MM/YYYY")})</TableCell>
                  <TableCell align="center" colSpan="3">Phát sinh tăng trong kỳ</TableCell>
                  <TableCell align="center" rowSpan="2">Số năm KH</TableCell>
                  <TableCell align="center" rowSpan="2">KH cơ bản/ tháng</TableCell>
                  <TableCell align="center" rowSpan="2">Số tháng tính KH</TableCell>
                  <TableCell align="center" rowSpan="2">Khấu hao trong kỳ</TableCell>
                  <TableCell align="center" colSpan="2">Số CK ({moment().endOf('month').format("DD/MM/YYYY")})</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center">Đơn giá</TableCell>
                  <TableCell align="center">Thành tiền</TableCell>
                  <TableCell align="center">Giá trị còn lại</TableCell>
                  <TableCell align="center">Đơn giá</TableCell>
                  <TableCell align="center">Thành tiền</TableCell>
                  <TableCell align="center">Giá trị còn lại</TableCell>
                  <TableCell align="center">Nguyên giá</TableCell>
                  <TableCell align="center">Giá trị còn lại</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>

                {this.renderUI(itemList).map(item => item)}

              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </div>
    );
  }
}

ReportIncreaseOrDecreaseAssets.contextType = AppContext;
export default ReportIncreaseOrDecreaseAssets;
