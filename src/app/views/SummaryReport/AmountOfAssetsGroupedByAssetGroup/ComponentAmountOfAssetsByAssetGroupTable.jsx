import React, { Component } from "react";
import { Button, Card, FormControl, Grid, InputLabel, MenuItem, Select, TextField, } from "@material-ui/core";

import MaterialTable, { MTableToolbar } from "material-table";
import ViewAssetByAssetGroupDialog from "./ViewAssetByAssetGroupDialog";
import TreeDepartmentDialog from "./TreeDepartmentDialog";
import Autocomplete, { createFilterOptions } from "@material-ui/lab/Autocomplete";
import "react-toastify/dist/ReactToastify.css";
import { appConst, keySearch } from "app/appConst";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import CardContent from "@material-ui/core/CardContent";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { getListManagementDepartmentOrg } from "app/views/Asset/AssetService";
import { searchByPage as searchByPageAssetSource } from "../../AssetSource/AssetSourceService";
import { searchByPage as statusSearchByPage } from "../../AssetStatus/AssetStatusService";
import { getTheHighestRole } from "app/appFunction";
class ComponentAmountOfAssetsByAssetGroupTable extends Component {
  render() {
    let {
      t,
      i18n,
      columns,
      handleChange,
      listTemporary,
      listManageAccountant,
      selectAssetSources,
      exportToExcel,
      handleSelectReceiverDepartment,
      handleDepartmentPopupClose,
      handleDialogClose,
      handleOKEditClose,
      selectManagementDepartment,
      selectOrganizationOrg,
      searchObjectManagementDepartment,
      searchObjectAssetSource,
      searchObjectUseDepartment,
      selectUseDepartment,
      handleSelectData,
      handleSetDataSelect,
      handleKeyUp,
      handleKeyDown
    } = this.props
    let {
      managementDepartment,
      assetSource,
      shouldOpenDepartmentPopup,
      department,
      shouldOpenViewAssetByAssetGroupDialog,
      assetGroupId,
      item,
      departmentId,
      rangerValue,
      isInverted,
      itemList,
      status,
      organizationOrg,
      listOrganizationOrg,
      useDepartment,
      yearPutIntoUse
    } = this.props.item;
    let { isRoleAssetManager } = getTheHighestRole();
    return (
      <div>
        <ValidatorForm>
          <Card elevation={4} className="mb-20">
            <CardContent>
              <Grid container spacing={2}>
                <Grid item md={3} sm={6} xs={12}>
                  <FormControl fullWidth={true}>
                    <InputLabel htmlFor="gender-simple">
                      {t("Asset.filter_temporary")}
                    </InputLabel>
                    <Select
                      isClearable={true}
                      onChange={(isTemporary) =>
                        handleChange(isTemporary, "isTemporary")
                      }
                      inputProps={{
                        name: "isTemporary",
                        id: "gender-simple",
                      }}
                    >
                      {listTemporary.map((item) => {
                        return (
                          <MenuItem key={item.name} value={item.value}>
                            {item.value != null ? item.name : "Tất cả"}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("general.filterStatus")}
                    searchFunction={statusSearchByPage}
                    multiple={true}
                    searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                    displayLable={"name"}
                    value={status}
                    name="status"
                    onSelect={handleSelectData}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <FormControl fullWidth={true}>
                    <InputLabel htmlFor="isManageAccountant">
                      {t("Asset.filter_manage_accountant")}
                    </InputLabel>
                    <Select
                      onChange={(isManageAccountant) =>
                        handleChange(isManageAccountant, "isManageAccountant")
                      }
                      inputProps={{
                        name: "isManageAccountant",
                        id: "isManageAccountant",
                      }}
                    >
                      {listManageAccountant.map((item) => {
                        return (
                          <MenuItem key={item.name} value={item.value}>
                            {item.name}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("Asset.filter_manage_department")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectManagementDepartment}
                    typeReturnFunction={"category"}
                    defaultValue={managementDepartment}
                    displayLable={"name"}
                    value={managementDepartment}
                    onSelect={selectManagementDepartment}
                    disabled={isRoleAssetManager}
                  />
                </Grid>
                <Grid item md={3} xs={4}>
                  <TextValidator
                    fullWidth
                    label={t("Asset.yearPutIntoUse")}
                    value={yearPutIntoUse}
                    name="yearPutIntoUse"
                    onKeyUp={handleKeyUp}
                    onKeyDown={handleKeyDown}
                    type="number"
                    onChange={(e) => handleSetDataSelect(
                      e?.target?.value,
                      e?.target?.name,
                    )}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("Asset.assetSource")}
                    searchFunction={searchByPageAssetSource}
                    searchObject={searchObjectAssetSource}
                    defaultValue={assetSource}
                    displayLable={"name"}
                    typeReturnFunction="category"
                    value={assetSource}
                    onSelect={selectAssetSources}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("Asset.useDepartment")}
                    searchFunction={getListManagementDepartmentOrg}
                    searchObject={searchObjectUseDepartment}
                    defaultValue={useDepartment}
                    displayLable={"name"}
                    typeReturnFunction={"category"}
                    value={useDepartment}
                    onSelect={selectUseDepartment}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="combo-box"
                    options={listOrganizationOrg}
                    value={organizationOrg}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label={t("WarehouseInventory.filterOrg")}
                        value={organizationOrg}
                      />
                    )}
                    getOptionLabel={(option) => option.name}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    onChange={(event, value) => {
                      selectOrganizationOrg(value);
                    }}
                  />
                </Grid>
                <Grid item md={2} sm={2} xs={12}>
                  <Button
                    className="mt-12"
                    size="small"
                    variant="contained"
                    color="primary"
                    onClick={exportToExcel}
                  >
                    {t("general.exportToExcel")}
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </ValidatorForm>
        <div>
          {shouldOpenDepartmentPopup && (
            <TreeDepartmentDialog
              open={shouldOpenDepartmentPopup}
              handleSelect={handleSelectReceiverDepartment}
              selectedItem={department != null ? department : {}}
              handleClose={handleDepartmentPopupClose}
              t={t}
              i18n={i18n}
            />
          )}
          {shouldOpenViewAssetByAssetGroupDialog && assetGroupId && (
            <ViewAssetByAssetGroupDialog
              t={t}
              state={this.props.item}
              i18n={i18n}
              handleClose={handleDialogClose}
              open={shouldOpenViewAssetByAssetGroupDialog}
              handleOKEditClose={handleOKEditClose}
              item={item}
              assetGroupId={assetGroupId}
              departmentId={departmentId}
              rangerValue={rangerValue ? rangerValue : null}
              isInverted={isInverted != null ? isInverted : null}
            />
          )}
        </div>
        <MaterialTable
          title={t("general.list")}
          data={itemList}
          columns={columns}
          parentChildData={(row, rows) => {
            return rows.find((a) => a.assetGroupId === row.parentId);
          }}
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
          }}
          options={{
            sorting: false,
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            defaultExpanded: true,
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            maxBodyHeight: "450px",
            minBodyHeight: "450px",
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
      </div>
    );
  }

}

export default ComponentAmountOfAssetsByAssetGroupTable;
