import axios from "axios";
import ConstantList from "../../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT +
  "/api/summary_report" +
  ConstantList.URL_PREFIX;
const API_PATH_ASSET =
  ConstantList.API_ENPOINT + "/api/asset" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;

const API_PATH_ASSETGROUP = ConstantList.API_ENPOINT + "/api/assetgroup" + ConstantList.URL_PREFIX;
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE; // api mới

export const getByRootAndQuantity = (searchObject) => {
  var url = API_PATH_ASSETGROUP + "/getByRootAndQuantity/"+searchObject.pageIndex+"/"+searchObject.pageSize;
  return axios.post(url, searchObject)
};

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/amount_of_assets_grouped_by_asset_group/searchByPage";
  return axios.post(url, searchObject);
};

export const searchByPageAsset = (searchObject) => {
  var url = API_PATH_ASSET + "/searchByPage";
  return axios.post(url, searchObject);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      API_PATH_EXPORTTOEXCEL +
      "/exportAmountOfAssetsGroupedByAssetGroupToExcel",
    data: searchObject,
    responseType: "blob",
  });
};
export const exportExcelForFixedAssetStatisticsByAssetGroup = (searchObject) => {
  return axios({
    method: "get",
    url:
      API_PATH_NEW +
      "/api/report/asset-group/fixed-assets/statistics/export-excel",
    params: searchObject,
    responseType: "blob",
  });
}
export const exportExcelForIatStatisticsByAssetGroup = (searchObject) => {
  return axios({
    method: "get",
    url:
      API_PATH_NEW +
      "/api/report/asset-group/instruments-and-tools/statistics/export-excel",
    params: searchObject,
    responseType: "blob",
  });
}