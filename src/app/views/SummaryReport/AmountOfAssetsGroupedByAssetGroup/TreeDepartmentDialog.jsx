import React, { Component } from 'react'
import {
  Dialog,
  Button,
  Grid,
  TablePagination,
  DialogActions,
  Input, InputAdornment,
  Checkbox
} from '@material-ui/core';
import { Link } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';
import { ValidatorForm } from 'react-material-ui-form-validator'
// import { searchByPage } from '../../Asset/AssetService'
import { searchByPage } from '../../Department/DepartmentService'
import FormControl from '@material-ui/core/FormControl'
import DialogTitle from '@material-ui/core/DialogTitle'
import Draggable from 'react-draggable'
import Paper from '@material-ui/core/Paper'
import DialogContent from '@material-ui/core/DialogContent';
import MaterialTable, {
  MTableToolbar,
} from 'material-table'

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  )
}
class TreeDepartmentDialog extends Component {

  state = {
    departmentId: '',
    keyword: '',
    assetGroupId: '',
    rowsPerPage: 10,
    page: 0,
    assets: [],
    item: {},
    totalElements: 0,
    selectedItem: {},
  }

  handleChange = (event, source) => {
    event.persist()
    if (source === 'switch') {
      this.setState({ isActive: event.target.checked })
      return
    }
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { })
  }

  handleKeyDownEnterSearch = (e) => {
    if (e.key === 'Enter') {
      this.search()
    }
  }

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData()
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId != 'all') {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.assetGroup = { id: this.state.assetGroupId };
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      })
    });
  }

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword;
      if (this.state.departmentId != 'all') {
        searchObject.departmentId = this.state.departmentId;
      }
      searchObject.assetGroup = { id: this.state.assetGroupId };
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchByPage(searchObject).then(({ data }) => {

        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        })
      });
    })
  }

  handleFormSubmit = () => {
  }

  componentWillMount() { }

  componentDidMount() {
    this.updatePageData();
  }

  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.id = item.id;
    root.parentId = item.parentId;
    result.push(root);
    if (item.children) {
      item.children.forEach(child => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  onClickRow = (item) => {

    document.querySelector(`#radio${item.id}`).click();
    // if (item.id != null) {
    //   this.setState({ selectedValue: item.id, selectedItem: item },function(){
    //       console.log(this.state.selectedItem);
    //   })
    // } else {
    //   this.setState({ selectedValue: null, selectedItem: null },function(){
    //     console.log(this.state.selectedItem);
    // })
    // }
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item }, function () { })
    } else {
      this.setState({ selectedValue: null, selectedItem: null })
    }
  }
  render() {
    let { open, handleSelect, t } = this.props
    let columns = [
      {
        title: t('general.select'),
        field: 'custom',
        align: 'left',
        width: '250',
        render: (rowData) => (
          <Checkbox
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={this.state.selectedValue === rowData.id}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t('component.department.text'),
        field: 'text',
        align: 'left',
        width: '150',
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth={true}
      >
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} class="validator-form-scroll-dialog">
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            <span className="mb-20">{t('amount_of_assets_grouped_by_asset_group.list_asset_by_asset_group')}</span>
          </DialogTitle>
          <DialogContent>
            <Grid container justifyContent="flex-end">
              <Grid item md={6} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className='search_box w-100'
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    placeholder={t("Asset.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment >
                        <Link> <SearchIcon
                          onClick={() => this.search()}
                          style={{
                            position: "absolute",
                            top: "0",
                            right: "0"
                          }} /></Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={'mt-36'}>
                <MaterialTable
                  data={this.state.itemList}
                  columns={columns}
                  onRowClick={((evt, selectedRow) => this.onClickRow(selectedRow))}

                  parentChildData={(row, rows) => {
                    var list = rows.find((a) => a.id === row.parentId)
                    return list
                  }}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ width: '100%' }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
                  component="div"
                  labelRowsPerPage={t('general.rows_per_page')}
                  labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
                  count={this.state.totalElements}
                  rowsPerPage={this.state.rowsPerPage}
                  page={this.state.page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mb-16 mr-16 align-bottom"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.close')}
              </Button>

              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => handleSelect(this.state.selectedItem)}
              >
                {t('general.select')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    )
  }
}

export default TreeDepartmentDialog
