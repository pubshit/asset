import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Typography,
  Slider,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  searchByPage,
  exportToExcel,
} from "./AmountOfAssetsGroupedByAssetGroupService";
import ViewAssetByAssetGroupDialog from "./ViewAssetByAssetGroupDialog";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import TreeDepartmentDialog from "./TreeDepartmentDialog";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { getListManagementDepartment } from "../../Asset/AssetService";
import {LightTooltip} from "../../Component/Utilities";

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class AmountOfAssetsGroupedByAssetGroup extends React.Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    rowsPerPage: 100000,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByAssetGroupDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    managementDepartmentId: "",
    managementDepartment: null,
    listManageDepartment: [],
    isTemporary: null,
    isManageAccountant: null,
    ranger: [0, 500],
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {});
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId != "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchByPage(searchObject).then(({ data }) => {
      data.map((item) => {
        if (item.remainQuantity) {
          item.remainQuantity = item.remainQuantity.toFixed(3);
        }
        if (item.totalOriginalCost) {
          item.totalOriginalCost = item.totalOriginalCost.toFixed(3);
        }
      });
      this.setState({
        itemList: [...(data || [])],
        totalElements: data?.totalElements,
      });
    });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    this.updatePageData();
  }

  componentWillMount() {
    getListManagementDepartment().then((data) => {
      this.setState({ listManageDepartment: [...data.data] });
    });
  }

  handleFormSubmit = () => {};

  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword;
      if (this.state.departmentId != "all") {
        searchObject.departmentId = this.state.departmentId;
      }
      searchObject.status = this.state.status ? this.state.status : null;
      searchObject.assetGroup = this.state.assetGroup
        ? this.state.assetGroup
        : null;
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.managementDepartmentId = this.state.managementDepartmentId;
      searchObject.isTemporary = this.state.isTemporary;
      searchObject.isManageAccountant = this.state.isManageAccountant;
      if (this.state.ranger) {
        searchObject.ranger = this.state.ranger;
      }
      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...(data || [])],
          totalElements: data?.totalElements,
        });
      });
    });
  }

  getAssetBydepartment = (event, departmentId) => {
    if (departmentId) {
      this.setState({ departmentId: departmentId }, function () {
        this.search();
      });
    }
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () {}
      );
    });
  };
  // end Tree view

  handleDeleteButtonClick = (item) => {
    this.setState({
      item: item,
      shouldOpenDepartmentPopup: true,
    });
  };
  /* Export to excel */
  exportToExcel = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId != "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "AmountOfAssetsGroupedByAssetGroup.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };

  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
  };

  handleChangeSlider = (event, newValue) => {
    this.setState({ ranger: newValue }, () => {
      this.search();
    });
  };

  valueLabelFormat = (value) => {
    return `${value}`;
  };

  render() {
    const { t, i18n } = this.props;
    let searchObject = { pageIndex: 0, pageSize: 1000000 };

    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      assetGroupId,
      departmentTreeView,
      item,
      departmentId,
      shouldOpenConfirmationDialog,
      shouldOpenViewAssetByAssetGroupDialog,
      shouldOpenImportExcelDialog,
      shouldOpenConfirmationDeleteAllDialog,
      expanded,
      defaultExpanded,
      shouldOpenDepartmentPopup,
      department,
      managementDepartment,
      managementDepartmentId,
      ranger,
    } = this.state;

    let TitlePage = t(
      "Dashboard.summary_report.amount_of_assets_grouped_by_asset_group"
    );
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "120px",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                if (rowData.assetGroupId) {
                  this.setState({
                    assetGroupId: rowData.assetGroupId,
                    item: {},
                    shouldOpenViewAssetByAssetGroupDialog: true,
                  });
                }
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("AssetGroup.title"),
        field: "groupName",
        width: "300",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "300px",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.quantity"),
        field: "quantity",
        align: "right",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "100px",
        },
        render: (rowData) => {
          let number = new Number(rowData.quantity);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.remainQuantity"),
        field: "remainQuantity",
        align: "right",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "200px",
        },
        render: (rowData) => {
          let number = new Number(rowData.remainQuantity);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "right",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "200px",
        },
        render: (rowData) => {
          let number = new Number(rowData.totalOriginalCost);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
    ];

    const marks = [
      {
        value: 0,
        label: "0",
      },
      {
        value: 200,
        label: "200 triệu",
      },
      {
        value: 500,
        label: "500 triệu",
      },
      {
        value: 1000,
        label: "1 tỷ",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.summary_report.title") },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid className={"asset_department"}>
          <Grid container spacing={2}>
            <Grid item md={2} sm={6} xs={12} style={{ paddingLeft: "0px" }}>
              <Button
                className="mt-12"
                variant="contained"
                color="primary"
                onClick={this.exportToExcel}
              >
                {t("general.exportToExcel")}
              </Button>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <FormControl fullWidth={true}>
                <InputLabel htmlFor="gender-simple">
                  {t("Asset.filter_temporary")}
                </InputLabel>
                <Select
                  // value={isTemporary}
                  onChange={(isTemporary) =>
                    this.handleChange(isTemporary, "isTemporary")
                  }
                  inputProps={{
                    name: "isTemporary",
                    id: "gender-simple",
                  }}
                >
                  {this.listTemporary.map((item) => {
                    return (
                      <MenuItem key={item.name} value={item.value}>
                        {item.value != null ? item.name : "Tất cả"}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <FormControl fullWidth={true}>
                <InputLabel htmlFor="isManageAccountant">
                  {t("Asset.filter_manage_accountant")}
                </InputLabel>
                <Select
                  // value={isManageAccountant}
                  onChange={(isManageAccountant) =>
                    this.handleChange(isManageAccountant, "isManageAccountant")
                  }
                  inputProps={{
                    name: "isManageAccountant",
                    id: "isManageAccountant",
                  }}
                >
                  {this.listManageAccountant.map((item) => {
                    return (
                      <MenuItem key={item.name} value={item.value}>
                        {item.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Autocomplete
                className="mt-3"
                size="small"
                id="combo-box"
                options={this.state?.listManageDepartment}
                value={managementDepartment}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label={t("Asset.filter_manage_department")}
                    value={managementDepartment}
                    // variant="outlined"
                  />
                )}
                getOptionLabel={(option) => option.name}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                onChange={(event, value) => {
                  this.selectManagementDepartment(value);
                }}
              />
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Typography id="range-slider" gutterBottom>
                Lọc theo giá
              </Typography>
              <Slider
                value={ranger}
                onChange={this.handleChangeSlider}
                valueLabelDisplay="auto"
                aria-labelledby="discrete-slider-custom"
                max={1000}
                step={100}
                valueLabelFormat={this.valueLabelFormat}
                marks={marks}
              />
            </Grid>
          </Grid>
        </Grid>
        <div>
          {shouldOpenDepartmentPopup && (
            <TreeDepartmentDialog
              open={shouldOpenDepartmentPopup}
              handleSelect={this.handleSelectReceiverDepartment}
              selectedItem={department != null ? department : {}}
              handleClose={this.handleDepartmentPopupClose}
              t={t}
              i18n={i18n}
            />
          )}
          {shouldOpenViewAssetByAssetGroupDialog && assetGroupId && (
            <ViewAssetByAssetGroupDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenViewAssetByAssetGroupDialog}
              handleOKEditClose={this.handleOKEditClose}
              item={item}
              assetGroupId={assetGroupId}
              departmentId={departmentId}
              ranger={ranger ? ranger : null}
            />
          )}
        </div>

        <MaterialTable
          title={t("general.list")}
          data={itemList}
          columns={columns}
          parentChildData={(row, rows) => {
            var list = rows.find(
              (a) => a.assetGroupId === row.assetGroupParentId
            );
            return list;
          }}
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
          }}
          options={{
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            defaultExpanded: true,
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            maxBodyHeight: "450px",
            minBodyHeight: "450px",
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
        {/* <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            /> */}
      </div>
    );
  }
}

export default AmountOfAssetsGroupedByAssetGroup;
