import React, { Component } from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import {
  exportExcelForFixedAssetStatisticsByAssetGroup,
  exportExcelForIatStatisticsByAssetGroup,
} from "./AmountOfAssetsGroupedByAssetGroupService";
import { getTreeView as getAllDepartmentTreeView } from "../../Department/DepartmentService";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, keySearch } from "app/appConst";
import { searchByPage } from "../../Department/DepartmentService";
import { debounce } from "lodash";
import ComponentAmountOfAssetsByAssetGroupTable from "./ComponentAmountOfAssetsByAssetGroupTable";
import { convertNumberPrice, functionExportToExcel, getTheHighestRole, getUserInformation } from "../../../appFunction";
import { searchByPageOrg } from "../SummaryOfAssetsByMedicalEquipmentGroup/SummaryOfAssetsByMedicalEquipmentGroupService";
import { LightTooltip, TabPanel } from "../../Component/Utilities";
import { Helmet } from "react-helmet";
import { Breadcrumb } from "egret";
import {
  getListFixedAssetStatistics,
  getListIatStatistics
} from "../AggregateAssetsByAssetGroup/AggregateAssetsByAssetGroupService";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

let { isRoleAssetManager, departmentUser } = getTheHighestRole();

class AmountOfAssetsGroupedByAssetGroup extends Component {
  constructor(props) {
    super(props);
    getAllDepartmentTreeView().then((result) => {
      let departmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: result.data.content,
      };
      let { expanded } = this.state;
      expanded = [];
      expanded.push("all");
      this.setState({ departmentTreeView, expanded }, function () {
        this.treeView = this.state.departmentTreeView;
      });
    });
  }

  state = {
    keyword: "",
    status: [],
    assetGroupId: "",
    rowsPerPage: 1000000,
    page: 0,
    Asset: [],
    item: {},
    departmentId: "",
    departmentTreeView: {},
    shouldOpenViewAssetByAssetGroupDialog: false,
    shouldOpenImportExcelDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    expanded: [],
    defaultExpanded: ["all"],
    shouldOpenDepartmentPopup: false,
    department: null,
    managementDepartmentId: "",
    managementDepartment: null,
    listManageDepartment: [],
    assetSources: [],
    assetSourceId: "",
    assetSource: null,
    isTemporary: null,
    isManageAccountant: null,
    ranger: [0, 500],
    isInverted: null, // lấy từ 0 đến rangerValue
    query: {
      pageIndex: 0,
      pageSize: 10,
      keyword: "",
      keySearch: ""
    },
    listDepartment: [],
    tabValue: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD,
    isTSCD: true,
    organizationOrg: null,
    listOrganizationOrg: [],
    useDepartment: null
  };
  numSelected = 0;
  rowCount = 0;
  treeView = null;
  listManageAccountant = [
    { value: true, name: "Kế Toán Quản Lý" },
    { value: false, name: "Kế toán không Quản Lý" },
    { value: "all", name: "Tất cả" },
  ];
  listTemporary = [
    { value: true, name: "Tạm giao" },
    { value: false, name: "Tài sản đã giao nhận" },
    { value: "all", name: "Tất cả" },
  ];
  yearPutIntoUse = Array.from(new Array(20), (val, index) => (new Date()).getFullYear() - index);
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      keyword: "",
      assetGroup: {},
      assetGroupId: "",
      yearPutIntoUse: null,
      isTemporary: null,
      isManageAccountant: null,
      assetSource: null,
      useDepartment: null,
      managementDepartmentId: isRoleAssetManager ? departmentUser?.id : "",
      managementDepartment: isRoleAssetManager ? departmentUser : null,
      tabValue: newValue,
      isTSCD: appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD === newValue
    }, () => this.updatePageData());
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props
    let { organization } = getUserInformation();
    let searchObject = {};
    setPageLoading(true)
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId !== "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.assetStatusIds = this.state.status ? this.state.status.map(i => i?.id).join(",") : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = isRoleAssetManager ? departmentUser?.id : this.state?.managementDepartmentId;
    searchObject.useDepartmentId = this.state?.useDepartment?.id;
    searchObject.assetSourceId = this.state?.assetSource?.id;
    searchObject.yearPutIntoUse = this.state?.yearPutIntoUse;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state?.isManageAccountant;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    searchObject.assetClass = this.state.isTSCD ? appConst.assetClass.TSCD
      : appConst.assetClass.CCDC;

    let data;
    let response;
    try {
      if (this.state.isTSCD) {
        response = await getListFixedAssetStatistics(searchObject);
      } else {
        response = await getListIatStatistics(searchObject);
      }

      data = response?.data?.data.map((item) => {
        if (item.remainQuantity) {
          item.remainQuantity = item.remainQuantity.toFixed(3);
        }
        if (item.totalOriginalCost) {
          item.totalOriginalCost = item.totalOriginalCost.toFixed(3);
        }
        return item
      });
      var treeValues = [];

      let itemListClone = [...data];

      itemListClone.forEach((item) => {
        var items = this.getListItemChild(item);
        treeValues.push(...items);
      });

      this.setState({
        itemList: treeValues,
        totalElements: data.totalElements,
      });
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false)
    }
  };

  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.assetGroupId = item.id;
    root.parentId = item.parentId;
    root.quantity = item.quantity;
    root.remainQuantity = item.totalCarryingAmount;
    root.totalOriginalCost = item.totalOriginalCost;
    root.totalCarryingAmount = item.totalCarryingAmount;
    result.push(root);
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenViewAssetByAssetGroupDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  componentDidMount() {

    if (this.props?.item?.organizationOrg?.id) {
      this.setState({ organizationOrg: this.props?.item?.organizationOrg }, () => {
        this.updatePageData();
      })
    } else {
      this.updatePageData();
    }
  }

  componentWillMount() {
    let { t } = this.props;
    let { organization } = getUserInformation();
    if (isRoleAssetManager) {
      this.setState({ managementDepartment: departmentUser?.id ? departmentUser : null })
    };
    let orgId = this.props?.item?.organizationOrg?.id || organization?.org?.id;

    searchByPageOrg(orgId).then((data) => {
      this.setState({ listOrganizationOrg: [...data?.data?.data?.content] })
    }).catch(() => {
      toast.error(t("toastr.error"))
    })

  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state?.query !== prevState.query) {
      if (this.state?.query.keySearch === keySearch.asset) {
        this.getListDepartment()
      }
    }
  }


  handleClick = (event, item) => {
    let { Asset } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Asset.length; i++) {
      if (Asset[i].checked == null || Asset[i].checked == false) {
        selectAllItem = false;
      }
      if (Asset[i].id == item.id) {
        Asset[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Asset: Asset });
  };

  debouncedSetSearchTerm = debounce((newTerm, source) => {
    this.setState({
      query: {
        ...this.state.query,
        keyword: newTerm,
        keySearch: source
      }
    });
  }, 300);

  handleSearchDepartment = (e, source) => {
    this.debouncedSetSearchTerm(e.target.value, source)
  };

  getListDepartment = async () => {
    let searchObject = {};
    searchObject.keyword = this.state.query.keyword.trim();
    searchObject.pageIndex = this.state.query.pageIndex + 1;
    searchObject.pageSize = this.state.query.pageSize;
    searchObject.orgId = this.state.organizationOrg?.id
    try {
      let res = await searchByPage(searchObject);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.content) {
        this.setState({
          listDepartment: [...res?.data?.content]
        });
      } else {
        this.setState({
          listDepartment: [],
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  search = () => {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    })
  }

  selectUseDepartment = (selected) => {
    this.setState({ useDepartment: selected }, () => {
      this.search();
    })
  }

  selectOrganizationOrg = (selected) => {
    this.setState({
      organizationOrg: selected,
      managementDepartment: null,
      managementDepartmentId: null,
      assetSource: null,
      assetSourceId: null,
      useDepartment: null,
    }, () => {
      this.search();
    })
  }

  filterItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.assetGroupId = item.id;
    root.parentId = item.parentId;
    root.quantity = item.quantity;
    root.remainQuantity = item.remainQuantity;
    root.totalOriginalCost = item.totalOriginalCost;
    // chỉ lọc những nhóm có tài sản
    if (item.quantity && item.quantity !== 0) {
      result.push(root);
    }
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.filterItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  handleKeyDown = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleSetDataSelect = (data, name) => {
    this.setState({
      [name]: data
    });
  };

  // Tree view
  contains = (name, term) => {
    return name.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  };

  searchTree = (items, term) => {
    if (items instanceof Array) {
      return items.reduce((acc, item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
          this.state.expanded.push(item.id);
        } else if (item.children && item.children.length > 0) {
          let newItems = this.searchTree(item.children, term);
          if (newItems && newItems.length > 0) {
            acc.push({ id: item.id, name: item.name, children: newItems });
            this.state.expanded.push(item.id);
          }
        }
        return acc;
      }, []);
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };
  handleSelectReceiverDepartment = (item) => {
    this.setState(
      { department: { id: item.id, name: item.text }, departmentId: item.id },
      function () {
        this.search();
        this.handleDepartmentPopupClose();
      }
    );
  };

  handleSearch = () => {
    let { expanded } = this.state;
    expanded = [];
    expanded.push("all");
    this.setState({ expanded }, function () {
      let value = document.querySelector("#search_box_department").value;

      let newData = this.searchTree(this.treeView.children, value);
      let newDepartmentTreeView = {
        id: "all",
        name: "Toàn bộ",
        children: newData,
      };
      this.setState(
        { departmentTreeView: newDepartmentTreeView },
        function () { }
      );
    });
  };

  converAssetStatus = (dataStatus) => {
    return dataStatus?.map(status => status?.id?.toString())?.join(',')
  }
  /* Export to excel */
  exportToExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { organization } = getUserInformation();
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    if (this.state.departmentId !== "all") {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.assetStatusIds = this.state.status
      ? this.converAssetStatus(this.state.status)
      : null;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state.managementDepartmentId;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.isManageAccountant = this.state.isManageAccountant;
    searchObject.yearPutIntoUse = this.state.yearPutIntoUse;
    searchObject.assetSourceId = this.state.assetSourceId;
    searchObject.useDepartmentId = this.state.useDepartment?.id;
    searchObject.assetClass = this.state.tabValue;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    if (this.state.rangerValue) {
      searchObject.rangerValue = this.state.rangerValue;
    }
    if (this.state.isInverted != null) {
      searchObject.isInverted = this.state.isInverted;
    }
    try {
      setPageLoading(true);
      const exportFuntion = this.state.isTSCD
        ? exportExcelForFixedAssetStatisticsByAssetGroup
        : exportExcelForIatStatisticsByAssetGroup
      await functionExportToExcel(
        exportFuntion,
        searchObject,
        "AmountOfAssetsGroupedByAssetGroup.xlsx"
      )
    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  selectManagementDepartment = (item) => {
    this.setState(
      {
        managementDepartmentId: item ? item.id : "",
        managementDepartment: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };
  selectAssetSources = (item) => {
    this.setState(
      {
        assetSourceId: item ? item.id : "",
        assetSource: item ? item : null,
      },
      function () {
        this.search();
      }
    );
  };
  handleChange = (event, source) => {
    if (source === "isTemporary") {
      let { isTemporary } = this.state;
      let Temporary = event.target.value;
      if (Temporary == "all") {
        isTemporary = null;
      } else {
        isTemporary = Temporary;
      }
      this.setState({ isTemporary: isTemporary }, function () {
        this.search();
      });
      return;
    }
    if (source === "isManageAccountant") {
      let { isManageAccountant } = this.state;
      let ManageAccountant = event.target.value;
      if (ManageAccountant == "all") {
        isManageAccountant = null;
      } else {
        isManageAccountant = ManageAccountant;
      }
      this.setState({ isManageAccountant: isManageAccountant }, function () {
        this.search();
      });
      return;
    }
    if (source === "yearPutIntoUse") {
      let yearPutIntoUse = event.target.value;
      if (yearPutIntoUse != 0) {
        this.setState({ yearPutIntoUse: yearPutIntoUse }, function () {
          this.search();
        });
      } else {
        this.setState({ yearPutIntoUse: null })
        this.search();
      }
      return;
    }
  };

  valueLabelFormat = (value) => {
    return `${value}`;
  };
  handleSelectUseDepartment = (item) => {
    let department = item ? { id: item?.id, name: item?.text, text: item?.text } : null;
    this.setState({ useDepartment: department }, function () {
      this.search();
    });
  }

  handleSelectData = (value, name) => {
    this.setState({ [name]: value }, this.search);
  }

  render() {
    const { t, i18n, isShowBreadcrumb = true } = this.props;
    let { organization } = getUserInformation()
    let {
      tabValue,
      organizationOrg
    } = this.state;
    let searchObjectManagementDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      orgId: organizationOrg?.id || organization?.org?.id,
      isAssetManagement: true
    }
    let searchObjectUseDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      orgId: organizationOrg?.id || organization?.org?.id,
    }
    let searchObjectAssetSource = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      orgId: organizationOrg?.id || organization?.org?.id
    }

    let TitlePage = t(
      "Dashboard.summary_report.amount_of_assets_grouped_by_asset_group"
    );
    let columns = [
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "120px",
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                if (rowData.assetGroupId) {
                  this.setState(
                    {
                      assetGroupId: rowData.assetGroupId,
                      item: {},
                      shouldOpenViewAssetByAssetGroupDialog: true,
                    }
                  );
                }
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("AssetGroup.title"),
        field: "name",
        width: "300",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "300px",
          textAlign: "left",
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.quantity"),
        field: "quantity",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "100px",
          textAlign: "center",
        },
        render: (rowData) => {
          let number = new Number(rowData.quantity);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.remainQuantity"),
        field: "remainQuantity",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData?.remainQuantity || 0) || 0,
      },
      {
        title: t("amount_of_assets_grouped_by_asset_group.totalOriginalCost"),
        field: "totalOriginalCost",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "10px",
          minWidth: "200px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData?.totalOriginalCost || 0) || 0,
      },
    ];

    return (
      <div className={isShowBreadcrumb && "m-sm-30"}>
        {isShowBreadcrumb && <>
          <Helmet>
            <title>
              {TitlePage} | {t("web_site")}
            </title>
          </Helmet>
          <div className="mb-sm-30">
            <Breadcrumb
              routeSegments={[
                {
                  name: t("Dashboard.summary_report.title"),
                  path: "/summary_report/amount_of_assets_grouped_by_asset_group",
                },
                { name: TitlePage },
              ]}
            />
          </div>
        </>}
        <AppBar position="static" color="default" className="mb-10">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              value={appConst.tabAggregateAssetsByAssetGroup.tabTSCD}
              className="tab"
              label={t("AggregateAssetsByAssetGroup.tabTSCD")}
            />
            <Tab
              value={appConst.tabAggregateAssetsByAssetGroup.tabCCDC}
              className="tab"
              label={t("AggregateAssetsByAssetGroup.tabCCDC")}
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAggregateAssetsByAssetGroup?.tabTSCD}
          className="mp-0"
        >
          <ComponentAmountOfAssetsByAssetGroupTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            handleChange={this.handleChange}
            listTemporary={this.listTemporary}
            listManageAccountant={this.listManageAccountant}
            yearPutIntoUse={this.yearPutIntoUse}
            searchObjectManagementDepartment={searchObjectManagementDepartment}
            searchObjectAssetSource={searchObjectAssetSource}
            searchObjectUseDepartment={searchObjectUseDepartment}
            selectAssetSources={this.selectAssetSources}
            handleSelectUseDepartment={this.handleSelectUseDepartment}
            selectUseDepartment={this.selectUseDepartment}
            handleSearchDepartment={this.handleSearchDepartment}
            exportToExcel={this.exportToExcel}
            handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
            handleDepartmentPopupClose={this.handleDepartmentPopupClose}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            selectManagementDepartment={this.selectManagementDepartment}
            selectOrganizationOrg={this.selectOrganizationOrg}
            handleSelectData={this.handleSelectData}
            handleSetDataSelect={this.handleSetDataSelect}
            handleKeyUp={this.handleKeyUp}
            handleKeyDown={this.handleKeyDown}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAggregateAssetsByAssetGroup?.tabCCDC}
          className="mp-0"
        >
          <ComponentAmountOfAssetsByAssetGroupTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            handleChange={this.handleChange}
            listTemporary={this.listTemporary}
            listManageAccountant={this.listManageAccountant}
            yearPutIntoUse={this.yearPutIntoUse}
            searchObjectManagementDepartment={searchObjectManagementDepartment}
            searchObjectAssetSource={searchObjectAssetSource}
            searchObjectUseDepartment={searchObjectUseDepartment}
            selectAssetSources={this.selectAssetSources}
            handleSelectUseDepartment={this.handleSelectUseDepartment}
            handleSearchDepartment={this.handleSearchDepartment}
            exportToExcel={this.exportToExcel}
            handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
            handleDepartmentPopupClose={this.handleDepartmentPopupClose}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            selectManagementDepartment={this.selectManagementDepartment}
            selectOrganizationOrg={this.selectOrganizationOrg}
            selectUseDepartment={this.selectUseDepartment}
            handleSelectData={this.handleSelectData}
            handleSetDataSelect={this.handleSetDataSelect}
            handleKeyUp={this.handleKeyUp}
            handleKeyDown={this.handleKeyDown}
          />
        </TabPanel>
      </div>
    );
  }
}
AmountOfAssetsGroupedByAssetGroup.contextType = AppContext
export default AmountOfAssetsGroupedByAssetGroup;
