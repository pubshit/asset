import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  TablePagination,
  DialogActions,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";
import "react-toastify/dist/ReactToastify.css";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  exportToExcel,
  getListFixedAsset,
  getListIat,
  searchByPage,
} from "../AggregateAssetsByAssetGroup/AggregateAssetsByAssetGroupService";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import { getUserInformation } from "app/appFunction";
import { appConst } from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class ViewAssetByAssetGroupDialog extends Component {
  state = {
    departmentId: "",
    keyword: "",
    assetGroupId: "",
    rowsPerPage: 50,
    page: 0,
    assets: [],
    item: {},
    totalElements: 0,
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value || e.target.value === "") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context
    let { t } = this.props;
    let { organization } = getUserInformation();

    setPageLoading(true);

    let {
      assetSourceId,
      isTSCD,
      assetGroupId
    } = this.state;

    var searchObject = {};
    let { isInverted } = this.props;
    searchObject.keyword = this.state.keyword?.trim();
    searchObject.assetStatusIds = this.state.status ? this.state.status.map(i => i?.id).join(",") : null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementDepartmentId = this.state?.managementDepartmentId;
    searchObject.useDepartmentId = this.state?.useDepartment?.id;
    searchObject.assetSourceId = assetSourceId;
    searchObject.yearPutIntoUse = this.state?.yearPutIntoUse;
    searchObject.isTemporary = this.state.isTemporary;
    searchObject.assetGroupId = assetGroupId;
    searchObject.isManageAccountant = this.state?.isManageAccountant;
    searchObject.orgId = this.state.organizationOrg?.id || organization?.org?.id;
    searchObject.assetClass = isTSCD ? appConst.assetClass.TSCD
      : appConst.assetClass.CCDC;
    if (isInverted != null) {
      searchObject.isInverted = isInverted;
    }
    try {
      let data
      if (isTSCD) {
        data = await getListFixedAsset(searchObject);
      } else {
        data = await getListIat(searchObject);
      }
      this.setState({
        itemList: data?.data?.data?.content?.length > 0 ? [...data?.data?.data?.content] : [],
        totalElements: data?.data?.data?.totalElements,
      })
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }
  };

  search() {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    });
  }

  handleFormSubmit = () => { };

  componentWillMount() {
    let { open, handleClose, item, assetGroupId, departmentId } = this.props;
    let {
      assetSourceId,
      isManageAccountant,
      isTSCD,
      isTemporary,
      managementDepartmentId,
      organizationOrg,
      status,
      yearPutIntoUse,
      useDepartment,
    } = this.props.state;

    this.setState({
      ...this.props.item,
      assetGroupId,
      departmentId,
      assetSourceId,
      isManageAccountant,
      isTSCD,
      isTemporary,
      managementDepartmentId,
      organizationOrg,
      status,
      yearPutIntoUse,
      useDepartment,
    }, () => {
      if (this.state.assetGroupId) {
        this.setState({ page: 0 }, () => {
          this.updatePageData();
        })
      }
    });
  }

  componentDidMount() { }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let { keyword, rowsPerPage, page, totalElements, itemList, isTSCD } = this.state;

    let columns = [
      {
        title: t("general.index"),
        field: "code",
        width: "50px",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: isTSCD ? t("Asset.code") : t("InstrumentsToolsInventory.code"),
        field: "code",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "120px",
          textAlign: "center",
        },
      },
      {
        title: isTSCD ? t("Asset.code") : t("InstrumentsToolsInventory.name"),
        field: "name",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "200px"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "120px",
          textAlign: "center",
        },
      },
      {
        title: t("Asset.assetSource"),
        field: "assetSourceNames",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px"
        },
      },
      {
        title: t("Asset.quantityReceipt"),
        field: "quantity",
        align: "center",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "150px"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "left",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "150px",
          textAlign: "right",
        },
        render: (rowData) => {
          let number = new Number(rowData?.originalCost ?? 0);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("Asset.carryingAmount"),
        field: "carryingAmount",
        align: "left",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          minWidth: "155px",
          textAlign: "right",
        },
        render: (rowData) => {
          let number = new Number(rowData?.carryingAmount ?? 0);
          if (number != null) {
            let plainNumber = number
              .toFixed(1)
              .replace(/\d(?=(\d{3})+\.)/g, "$&,");
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        width: "180px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
          minWidth: "180px"
        },
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="mb-20">
              {t(
                "amount_of_assets_grouped_by_asset_group.list_asset_by_asset_group"
              )}
            </span>
          </DialogTitle>
          <DialogContent style={{ overflow: "hidden" }}>
            <Grid container justify="flex-start">
              <Grid item md={4} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUp}
                    placeholder={isTSCD ? t("Asset.enterSearch") : t("InstrumentToolsList.enterSearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment>
                        <Link>
                          {" "}
                          <SearchIcon
                            onClick={() => this.search()}
                            style={{
                              position: "absolute",
                              top: "0",
                              right: "0",
                            }}
                          />
                        </Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} xs={12} className={"mt-12"}>
                <MaterialTable
                  data={itemList}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  options={{
                    sorting: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "380px",
                    minBodyHeight: "380px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense",
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                  component="div"
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  labelRowsPerPage={t("general.rows_per_page")}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                    }`
                  }
                  page={page}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                  }}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                  }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>

            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

ViewAssetByAssetGroupDialog.contextType = AppContext
export default ViewAssetByAssetGroupDialog;
