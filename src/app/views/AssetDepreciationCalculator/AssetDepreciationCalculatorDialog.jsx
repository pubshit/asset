import React, {Component} from "react";
import {Button, Dialog, Grid, TablePagination} from "@material-ui/core";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import MaterialTable, {MTableToolbar} from "material-table";
import {
  checkDepreciationYear,
  DepreciationCalculator,
  getMaxDepreciationYear
} from "./AssetDepreciationCalculatorService";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import {withStyles} from "@material-ui/core/styles";
import {ConfirmationDialog} from "egret";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_loadding.scss";
import {convertNumberPriceRoundUp, handleThrowResponseMessage, isSuccessfulResponse} from "app/appFunction";
import debounce from 'lodash.debounce';
import AppContext from "app/appContext";
import {PaperComponent} from "../Component/Utilities/PaperComponent";


class AssetDepreciationCalculatorDialog extends Component {
  constructor(props) {
    super(props);
    this.debouncedCheckDepreciation = debounce(this.checkDepreciationByYear, 400);
  }
  state = {
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    assets: [],
    dataAssetsSupport:[],
    title: null,
    inventoryCountDate: new Date(),
    inventoryCountPerson: null,
    department: null,
    inventoryCountStatus: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    departmentId: "",
    asset: {},
    depreciationYear: new Date().getFullYear(),
    shouldOpenEditorDialog: true,
    isView: false,
    shouldOpenNotificationPopup: false,
    loading: false,
    isDepreciationYear: false,
    assetsInfoRelate:{
      totalCost: 0,
      totalAnnualDepreciation: 0,
      totalRemainingValue:0
    },
    lastDepreciationYear: null,
    isProcessing: false
  };
  handleChangeDepreciationYear = (event) => {
    event.persist();
     this.setState(
      {
        [event.target.name]: event.target.value,
      },
      () => {
        this.debouncedCheckDepreciation();
      });
  };
  handleAssetsInfoRelate = (data) => {
    let totalCost = 0;
    let totalAnnualDepreciation = 0;
    let totalRemainingValue = 0;
    data.forEach(item => {
      totalCost += item?.originalCost;
      totalAnnualDepreciation += item?.annualDepreciationAmount;
      totalRemainingValue += item?.carryingAmount;
    });
    this.setState({
      assetsInfoRelate: {
        totalCost,
        totalAnnualDepreciation,
        totalRemainingValue
      }
    });
  }
  handleDepreciationCalculator = async (isView) => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    const {department, depreciationYear,} = this.state;

    try {
      setPageLoading(true);

      const res = await DepreciationCalculator(department, depreciationYear, isView);
      const {data, code, total} = res?.data;

      if (isSuccessfulResponse(code)) {
        const updatedAssets = data?.length > 0 ? data : [];
        this.setState(
          {
            assets: updatedAssets,
            totalElements: total,
            dataAssetsSupport: updatedAssets,
            // loading: false,
          },
          () => {
            if (!isView) {
              this.props.handleClose();
            }
          }
        );
        this.handleAssetsInfoRelate(updatedAssets);
      } else {
        handleThrowResponseMessage(res);
        // toast.warning(this.props.t("Asset.depreciationError"));
        // this.setState({ loading: false })
      }
    } catch (error) {
      toast.warning(t("general.error"));
    } finally {
      setPageLoading(false);
      // this.setState({page: 0, rowsPerPage: 10}, () => {
      //   this.handleUpdateElementInTable();
      // });
    }
  };

   formatCurrency=(value)=> {
    const intValue = Math.floor(value);
    const formatter = new Intl.NumberFormat('vi-VN', {
      style: 'currency',
      currency: 'VND',
      minimumFractionDigits: 0,
    });
     return formatter.format(intValue).replace(/₫/g, 'VNĐ');
  }

  handleCloseForm = () => {
    this.props.handleClose();
  };
  componentDidMount() {
    let { t } = this.props;
    getMaxDepreciationYear().then(res => {
      if (isSuccessfulResponse(res?.data?.code)) {
        this.setState({ lastDepreciationYear: res.data.data }, () => {
          if (this.state.lastDepreciationYear) {
            this.setState({
              depreciationYear: this.state.lastDepreciationYear,
              isDepreciationYear:false
            }, () => {
              this.checkDepreciationByYear()
            })
          }
          else{
            this.setState({
              depreciationYear: new Date().getFullYear(),
              isDepreciationYear: true,
            })
          }
       })
      } else {
        handleThrowResponseMessage(res);
      }
    }).catch(error => {
      toast.warning(t("general.error"))
    });
  }

  checkDepreciationByYear = () => {
    let { depreciationYear } = this.state;
    if (depreciationYear > 0) {
      checkDepreciationYear(depreciationYear).then((res) => {
        if (isSuccessfulResponse(res?.data?.code)) {
          this.setState({ isDepreciationYear: true });
        }
        else  {
          handleThrowResponseMessage(res);
          this.setState({ isDepreciationYear: false });
        }
      });
    }
  };
  handleConfirmationResponse = async () => {
    let {t} = this.props;
    this.handleDepreciationCalculator(false).then(() => {
      this.props.updateListYearHasDepreciationAndYear();
    }).catch((err) => {
      toast.warning(t("general.error"));
    });
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.handleUpdateElementInTable();
    });
  };
  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage }, function () {
      this.handleUpdateElementInTable();
    });
  };
  handleUpdateElementInTable = () => {
    const { dataAssetsSupport, page, rowsPerPage } = this.state;
    const startIndex = page * rowsPerPage;
    const endIndex = startIndex + rowsPerPage;
    const assets = dataAssetsSupport.slice(startIndex, endIndex);
    this.setState({ assets });
  }
  render() {
    let { open, t } = this.props;
    let {
      totalElements,
      rowsPerPage,
      page,
      assets = [],
      depreciationYear,
      shouldOpenNotificationPopup,
      isDepreciationYear,
      lastDepreciationYear
    } = this.state;

    let columns = [
      {
        title: t("general.index"),
        field: "",
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.name"),
        field: "assetName",
        cellStyle: {
          Width: "700px"
        },
      },
      {
        title: t("Asset.code"),
        field: "assetCode",
        cellStyle: {
          Width: "150px"
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "right",
        cellStyle: {
          textAlign: "center",
          Width: "155px"
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.originalCost)
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "assetYearPutIntoUse",
        width: "",
        cellStyle: {
          textAlign: "center",
          Width: "130px"
        },
      },
      {
        title: t("Asset.depreciationRate"),
        field: "depreciationRate",
        minWidth: 70,
        align: "right",
      },
      {
        title: t("Asset.annualDepreciationAmount"),
        field: "annualDepreciationAmount",
        width: "",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.annualDepreciationAmount)
      },
      {
        title: t("Asset.carryingAmount"),
        field: "carryingAmount",
        width: "",
        align: "right",
        cellStyle: {
          Width: "150px"
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.carryingAmount)
      },
      {
        title: t("Asset.accumulatedDepreciation"),
        field: "depreciatedAmount",
        width: "",
        align: "right",
        render: (rowData) => convertNumberPriceRoundUp(rowData.depreciatedAmount)
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="xl"
        fullWidth
        aria-labelledby="customized-dialog-title"
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <ConfirmationDialog
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("general.confirmCalculator")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        <DialogTitle className="cursor-move pb-0" id="draggable-dialog-title">
          {t("Asset.depreciationDiaglog")}
        </DialogTitle>

        <DialogContent>
          <Grid container spacing={1} alignItems="center" justifyContent="center">
            <Grid item md={1} sm={2} xs={12}>
              <ValidatorForm onSubmit={()=>{}}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Asset.depYear")}
                    </span>
                  }
                  onChange={this.handleChangeDepreciationYear}
                  type="number"
                  name="depreciationYear"
                  value={depreciationYear}
                  validators={["required", "matchRegexp:^[1-9][0-9]{3}$"]}
                  errorMessages={[
                    t("general.required"),
                    t("general.yearPutIntoUseError"),
                  ]}
                  disabled={Boolean(lastDepreciationYear)}
                  // disabled={true}
                />
              </ValidatorForm>
            </Grid>
            <Grid item md={1} sm={6} xs={12}>
              <Button
                size="small"
                className="mt-12"
                variant="contained"
                color="primary"
                // disabled={this.state.isProcessing}
                onClick={() => {
                  this.handleDepreciationCalculator(true);
                }}
              >
                {t("Tạm tính")}
              </Button>
            </Grid>
            <Grid item md={5} sm={6} xs={12}>
              <Typography variant="subtitle1">
                <strong>{t("Asset.lastYearOfDeduction")}: </strong>
                {lastDepreciationYear ? lastDepreciationYear : "Chưa có"}</Typography>
              <Typography variant="subtitle1">
                <strong>{t("Asset.totalCost")}:</strong>
                {this.formatCurrency(this.state.assetsInfoRelate.totalCost)}</Typography>
            </Grid>
            <Grid item md={5} sm={6} xs={12}>
              <Typography variant="subtitle1">
                <strong>{t("Asset.totalAnnualDepreciation")}: </strong>
                {this.formatCurrency(this.state.assetsInfoRelate.totalAnnualDepreciation)}</Typography>
              <Typography variant="subtitle1">
                <strong>{t("Asset.totalRemainingValue")}: </strong>
                {this.formatCurrency(this.state.assetsInfoRelate.totalRemainingValue)}</Typography>
            </Grid>
            <Grid container spacing={2} className="mt-12">
              <Grid item xs={12}>
                <MaterialTable
                  data={assets}
                  columns={columns}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    padding: "dense",
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "320px",
                    minBodyHeight: "320px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ width: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
                <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                  component="div"
                  labelRowsPerPage={t("general.rows_per_page")}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                    }`
                  }
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                  }}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                  }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              onClick={this.props.handleClose}
            >
              {t("general.close")}
            </Button>
            {isDepreciationYear && (
              <Button
                variant="contained"
                className="ml-12"
                color="primary"
                disabled={assets?.length === 0}
                onClick={() =>
                  this.setState({ shouldOpenNotificationPopup: true })
                }
              >
                {t("general.save")}
              </Button>
            )}
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}

AssetDepreciationCalculatorDialog.contextType = AppContext
export default AssetDepreciationCalculatorDialog;
