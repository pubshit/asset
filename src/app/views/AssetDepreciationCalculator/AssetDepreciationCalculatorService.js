import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_Depreciation_Calculator =
  ConstantList.API_ENPOINT +
  "/api/asset_depreciation" +
  ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;

const API_FIX_V1 =
  ConstantList.API_ENPOINT + "/api/v1/fixed-assets/depreciation";

export const deleteItem = (id) => {
  return axios.delete(API_PATH_voucher + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH_voucher + "/" + id);
};

export const searchByPage = (searchObject) => {
  let url = API_PATH_Depreciation_Calculator + "/searchByPage";
  return axios.post(url, searchObject);
};

export const assetDepreciationSearchByPage = (searchObject) => {
  let url = API_PATH_Depreciation_Calculator + "/getByPage";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT + "/api/fileDownload/assetDepreciationToExcel",
    data: searchObject,
    responseType: "blob",
  });
};
export const searchByPageAssetDepreciation = (searchObject) => {
  let url = API_PATH_Depreciation_Calculator + "/searchByPageAssetDepreciation";
  return axios.post(url, searchObject);
};

export const checkDepreciationYear = (depreciationYear) => {
  let url = `${API_FIX_V1}/valid-year-can-depreciation/${depreciationYear}`;
  return axios.get(url);
};

export const getMaxDepreciationYear = () => {
  let url = API_FIX_V1 + "/max-depreciation-year";
  return axios.get(url);
};
export const getAllYearHasDepreciation = () => {
  let url = API_FIX_V1 + "/years-has-depreciation";
  return axios.get(url);
};
export const getDepreciationByPage = (
  useDepartmentId,
  depreciationYear,
  keyword,
  pageIndex,
  pageSize
) => {
  let url = `${API_FIX_V1}/page`;
  let config = {
    params: {
      pageIndex: pageIndex + 1,
      pageSize,
      keyword: keyword,
      useDepartmentId,
      depreciationYear,
    },
  };
  return axios.get(url, config);
};
export const DepreciationCalculator = (
  department,
  depreciationYear,
  isView
) => {
  let url = API_FIX_V1;
  let config = {
    params: {
      isView,
    },
  };
  return axios.post(url, {}, config);
};
