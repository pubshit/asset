import React from "react";
import {Grid, TablePagination, Button, TextField, FormControl, Input, InputAdornment, Divider} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  deleteItem,
  exportToExcel,
  getAllYearHasDepreciation,
  getDepreciationByPage
} from "./AssetDepreciationCalculatorService";
import AssetDepreciationCalculatorDialog from "./AssetDepreciationCalculatorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import { Helmet } from "react-helmet";
import { searchByPage as searchByPageUserDepartment } from "../Department/DepartmentService";
import {convertMoney, convertNumberPriceRoundUp, functionExportToExcel} from "app/appFunction";
import {appConst, variable} from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import GroupActionIcons from "../Component/Utilities/GroupActionIcons";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {Label} from "../Component/Utilities";
import {monthColumns, quarterColumns} from "./columns";


class AssetDepreciationCalculatorTable extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    rowsPerPage: 50,
    page: 0,
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedList: [],
    totalElements: 0,
    departmentId: null,
    dateSearch: null,
    thisYear: null,
    useDepartment: null,
    keyword: null,
    listYearHasDepreciation: [],
    timeUnit: null,
    yearOfQuarter: new Date(),
    monthOfTimeUnit: new Date(),
    yearOfTimeUnit: null,
  };
  
  updatePageData = () => {
    const {
      useDepartment,
      thisYear,
      keyword,
      page,
      rowsPerPage
    } = this.state;

    getDepreciationByPage(useDepartment?.id, thisYear?.id, keyword, page, rowsPerPage).then(data => {
      this.setState({
        itemList: data?.data?.data?.content ?? [],
        totalElements: data?.data?.data?.totalElements ?? 0,

      });
    })
  };
  
  handleFilterDepartment = async (event, departmentSearch) => {
    this.setState({
      useDepartment: departmentSearch,
      page: 0,
      rowsPerPage: 50,
    }, () => {
      this.updatePageData();
    });
  };
  handleFilterYear = async (event, dateSearch) => {
    this.setState({
      thisYear: dateSearch,
      page: 0,
      rowsPerPage: 50,
    }, () => {
      this.updatePageData()
    })
  };
  handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      this.setState({
        page: 0,
        rowsPerPage: 50,
      }, () => {
        this.updatePageData()
      })
    }
  };
  handleSearchIconClick = (event) => {
    event.preventDefault();
    this.setState({
      page: 0,
      rowsPerPage: 50,
    }, () => {
      this.updatePageData()
    })
  }
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => {
      if (!this.state.keyword) { this.updatePageData(); }

    });
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };
  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
      },
      () => this.updatePageData()
    );
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    setPageLoading(true);
    try {
      let res = await deleteItem(this.state.id);
      const { code, message } = res?.data;

      if (appConst.CODE.SUCCESS === code || appConst.CODE.SUCCESS === res?.status) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      }
      else {
        toast.warning(message);
      }
    }
    catch (e) {
      toast.success(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  };
  updateListYearHasDepreciationAndYear = () => {
    getAllYearHasDepreciation().then(res => {
      const dataYear = res?.data?.data?.sort?.((a, b) => b - a) ?? [];
      const newDataYear = dataYear.map((year, index) => {
        return { id: year, year: year.toString() };
      });
      this.setState({
        listYearHasDepreciation: newDataYear,
        thisYear: Array.isArray(dataYear) && dataYear.length ? Math.max(...dataYear) : 0
      }, () => {
        this.updatePageData()
      })
    }).catch(error => {
      console.error(error);
    });
  }
  componentDidMount() {
    this.updateListYearHasDepreciationAndYear()
    searchByPageUserDepartment({ pageIndex: 0, pageSize: 100 }).then(res => {
      this.setState({ listDepartment: res?.data?.content })
    })
  }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  exportToExcel = async () => {
    let { useDepartment } = this.state;
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let searchObject = {};
    if (this.state.departmentId !== variable.listInputName.all) {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.dateSearch = this.state.dateSearch;
    searchObject.useDepartmentId = useDepartment?.id;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportToExcel,
        searchObject,
        t("exportToExcel.assetDepreciationCalculator")
      )
    } catch (e) {
      toast.success(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleSetData = (value, name) => {
    this.setState({
      [name]: value,
    })
  }
  render() {
    const { t, i18n } = this.props;
    let searchObject = { pageIndex: 0, pageSize: 100 };
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      timeUnit,
      quarter,
      monthOfTimeUnit,
      yearOfQuarter,
      yearOfTimeUnit,
    } = this.state;
    let TitlePage = t("Asset.asset_depreciation_calculator");
    const {MONTH, QUARTER, YEAR} = appConst.OBJECT_CHART.TIME_UNITS;
    const isMonth = timeUnit?.code === MONTH.code;
    const isYear = timeUnit?.code === YEAR.code;
    const isQuarter = timeUnit?.code === QUARTER.code;
    let columnsByType = {
      [MONTH.code]: monthColumns,
      // [YEAR.code]: monthColumns,
      [QUARTER.code]: quarterColumns,
    }
    let additionalColumns = columnsByType[timeUnit?.code]?.({t}) || []
    let columns = [
      {
        title: t("general.action"),
        field: "",
        textAlign: "center",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => <GroupActionIcons
          item={rowData}
          onSelect={(item, method) => {
          
          }}
          editIcon
          deleteIcon
        />,
      },
      {
        title: t("general.index"),
        field: "",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.name"),
        field: "assetName",
        align: "left",
        minWidth: 250,
      },
      {
        title: t("Asset.code"),
        field: "assetCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "assetManagementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertMoney(rowData.originalCost)
      },
      {
        title: t("DepreciationCalculation.startOfTermCarryingAmount"),
        field: "",
        width: "",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
      },
      ...additionalColumns,
      {
        title: t("Asset.yearIntoUseTable"),
        field: "assetYearPutIntoUse",
        width: "",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.depYear"),
        field: "depreciationYear",
        width: "",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.depreciationRateTable"),
        field: "depreciationRate",
        width: "",
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.annualDepreciationAmount"),
        field: "annualDepreciationAmount",
        width: "",
        minWidth: 150,
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.annualDepreciationAmount)
      },
      {
        title: t("Asset.carryingAmountTable"),
        field: "carryingAmount",
        width: "",
        minWidth: 150,
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.carryingAmount)
      },
      {
        title: t("Asset.depreciatedAmount"),
        field: "depreciatedAmount",
        width: "",
        minWidth: 180,
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.depreciatedAmount)
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.assetManagement") },
              { name: t("Asset.asset_depreciation_calculator") },
            ]}
          />
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                });
              }}
            >
              {t("Asset.asset_depreciation_calculator")}
            </Button>
            <Button
              className="mb-16 align-bottom"
              variant="contained"
              color="primary"
            >
              {t("DepreciationCalculation.closingEntries")}
            </Button>
          </Grid>
          <Grid item xs={4}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                style={{ marginTop: "8px" }}
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("MaintainPlaning.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.handleSearchIconClick} />
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          
          
          <Grid item xs={12}>
            <ValidatorForm onSubmit={()=>{}}>
              <Grid container spacing={1}>
                <Grid item xs={2}>
                  <AsynchronousAutocompleteSub
                    label={t("Tháng/Quý/Năm")}
                    searchFunction={() => {}}
                    displayLable="name"
                    listData={appConst.OBJECT_CHART.listTimeUnits}
                    name="timeUnit"
                    value={timeUnit || null}
                    onSelect={this.handleSetData}
                  />
                </Grid>
                {timeUnit?.code === appConst.OBJECT_CHART.TIME_UNITS.MONTH.code && (
                  <Grid item xs={2}>
                    <ValidatePicker
                      label={"Chọn tháng"}
                      views={['year', 'month']}
                      openTo={"month"}
                      format={"MM/yyyy"}
                      name="monthOfTimeUnit"
                      value={monthOfTimeUnit || null}
                      onChange={this.handleSetData}
                    />
                  </Grid>
                )}
                {timeUnit?.code === appConst.OBJECT_CHART.TIME_UNITS.QUARTER.code && (
                  <Grid item xs={2}>
                    <AsynchronousAutocompleteSub
                      label={t("Chọn quý")}
                      searchFunction={() => {}}
                      displayLable="name"
                      listData={appConst.OBJECT_CHART.listQuarter}
                      name="quarter"
                      value={quarter || null}
                      onSelect={this.handleSetData}
                    />
                  </Grid>
                )}
                {timeUnit?.code === appConst.OBJECT_CHART.TIME_UNITS.QUARTER.code && (
                  <Grid item xs={2}>
                    <ValidatePicker
                      label={"Năm"}
                      views={['year']}
                      openTo={"year"}
                      format={"yyyy"}
                      name="yearOfQuarter"
                      value={yearOfQuarter || null}
                      onChange={this.handleSetData}
                    />
                  </Grid>
                )}
                {timeUnit?.code === appConst.OBJECT_CHART.TIME_UNITS.YEAR.code && (
                  <Grid item xs={2}>
                    <TextValidator
                      fullWidth
                      label={"Năm"}
                      name="yearOfTimeUnit"
                      value={yearOfTimeUnit || ""}
                      onChange={(e) => this.handleSetData(e?.target?.value, e?.target?.name)}
                    />
                  </Grid>
                )}
                {timeUnit?.code && (
                  <Grid item>
                    <Button
                      className="mt-12"
                      variant="contained"
                      color="primary"
                    >
                      {t("DepreciationCalculation.temporaryCalculation")}
                    </Button>
                  </Grid>
                )}
              </Grid>
              <Grid container spacing={1} justifyContent="flex-end">
                <Grid item>
                  Tổng KH kỳ: ...
                </Grid>
                <Divider orientation="vertical" flexItem className="my-4"/>
                <Grid item className="text-success">
                  Trạng thái: Tạm tính/Đã chốt khấu hao
                </Grid>
              </Grid>
            </ValidatorForm>
          </Grid>
        </Grid>
        <Grid container spacing={2} className={"mt-16"}>
          <Grid item md={12} xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <AssetDepreciationCalculatorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  updateListYearHasDepreciationAndYear={this.updateListYearHasDepreciationAndYear}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: "450px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

AssetDepreciationCalculatorTable.contextType = AppContext;
export default AssetDepreciationCalculatorTable;
