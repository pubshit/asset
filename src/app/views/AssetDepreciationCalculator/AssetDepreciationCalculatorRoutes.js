import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
import {ROUTES_PATH} from "../../appConst";

const AssetAllocationTable = EgretLoadable({
  loader: () => import("./AssetDepreciationCalculatorTable")
});
const ViewComponent = withTranslation()(AssetAllocationTable);

const AssetDepreciationCalculatorRoutes = [
  {
    path:  ConstantList.ROOT_PATH + ROUTES_PATH.FIXED_ASSET_MANAGEMENT.DEPRECIATION,
    exact: true,
    component: ViewComponent
  }
];

export default AssetDepreciationCalculatorRoutes;
