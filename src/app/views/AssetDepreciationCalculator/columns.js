
export const monthColumns = (props) => {
  const {t} = props;
  return [
    {
      title: t("DepreciationCalculation.months.Jan"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Feb"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Mar"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Apr"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.May"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Jun"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Jul"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Aug"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Sep"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Oct"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Nov"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.months.Dec"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    }
  ]
};

export const quarterColumns = (props) => {
  const {t} = props;
  return [
    {
      title: t("DepreciationCalculation.quarters.Q1"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.quarters.Q2"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.quarters.Q3"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("DepreciationCalculation.quarters.Q4"),
      field: "",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    }
  ];
};