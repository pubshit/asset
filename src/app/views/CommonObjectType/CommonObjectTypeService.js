import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/commonobjecttype" + ConstantList.URL_PREFIX;
export const searchByPage = (text, pageIndex, pageSize) => {
  var url = API_PATH + "/searchByText/" + pageIndex + "/" + pageSize;
  return axios.post(url, { keyword: text });
};

export const searchByPageSelect = (searchObject) => {
  var url =
    API_PATH +
    "/searchByText/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.post(url, { keyword: searchObject.text });
};

export const searchByPageObject = (searchObject) => {
  var params = "/" + searchObject.pageIndex + "/" + searchObject.pageSize;
  var url = API_PATH + params;
  return axios.get(url, { keyword: searchObject?.keyword });
};

export const getByPage = (page, pageSize) => {
  var pageIndex = page + 1;
  var params = "/" + pageIndex + "/" + pageSize;
  var url = API_PATH + params;
  return axios.get(url);
};

export const getItemById = (id) => {
  var url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = (item) => {
  var url = API_PATH;
  return axios.post(url, item);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
