import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  InputLabel,
  FormControl,
  MenuItem,
  Select,
  Checkbox,
  TextField,
  FormControlLabel,
  DialogActions,
  DialogTitle,
  DialogContent,
} from "@material-ui/core";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import Autocomplete from "@material-ui/lab/Autocomplete";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  getUserByUsername,
  saveUser,
  saveUserDepartment,
  addNewUser,
  getAllRoles,
  saveUserDepartments,
  listByUserId,
  deleteById,
  checkEmail,
  deleteUserDepartmentId,
} from "./UserService";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import { find } from "lodash";
import { searchByPage as searchByPageOrg } from "../Organization/OrganizationService";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      {/* <IconButton onClick={() => props.onSelect(item, 0)}>
      <Icon color="primary">edit</Icon>
    </IconButton> */}
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}
class UserEditorDialog extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    userDepartmentId: "",
    department: {},
    isAddNew: false,
    listRole: [],
    roles: [],
    active: true,
    email: "",
    person: {},
    username: "",
    changePass: true,
    password: "",
    confirmPassword: "",
    userDepartments: [],
    id: "",
    toastId: null,
    listDepartment: [],
    org: null,
    countRoles: ["ROLE_ORG_ADMIN", "ROLE_SUPER_ADMIN", "ROLE_ADMIN"], // 1 là org_admin, 2 là super admin, 3 laf admin
  };

  listGender = [
    { id: "M", name: "Nam" },
    { id: "F", name: "Nữ" },
    { id: "U", name: "Không rõ" },
  ];

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    if (source === "changePass") {
      this.setState({ changePass: event.target.checked });
      return;
    }
    if (source === "active") {
      this.setState({ active: event.target.checked });
      return;
    }
    if (source === "displayName") {
      let { person } = this.state;
      person = person ? person : {};
      person.displayName = event.target.value;
      this.setState({ person: person });
      return;
    }
    if (source === "gender") {
      let { person } = this.state;
      person = person ? person : {};
      person.gender = event.target.value;
      this.setState({ person: person });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    });
  };
  handleSelectDepartmentPopupClose = () => {
    this.setState({
      openSelectDepartmentPopup: false,
    });
  };
  handleSelectUseDepartment = (item) => {
    if (Object.keys(item).length > 0) {
      let { userDepartments } = this.state;
      let department = { id: item.id, name: item.text, text: item.text };
      if (userDepartments.map((el) => el.department.id).indexOf(item.id) < 0) {
        userDepartments.push({ department, isMainDepartment: false });
      }
      this.setState({ userDepartments });
      // this.setState({ department }, function () {
      // });
    }
    this.handleSelectDepartmentPopupClose();
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  handleFormSubmit = async () => {
    await this.openCircularProgress();
    let { id, userDepartments, email, roles, countRoles } = this.state;

    const checkMainDepartmentNull = userDepartments.find((el) => {
      return el.isMainDepartment === true;
    });
    let isAdmin = false;
    roles.forEach((role) => {
      if (countRoles.indexOf(role.name) !== -1) {
        isAdmin = true;
      }
    });
    if (isAdmin) {
      if (userDepartments && userDepartments.length > 0) {
        toast.warning("Tài khoản này không được chọn phòng ban");
        toast.clearWaitingQueue();
        this.setState({ loading: false });
        return;
      }
    }

    if (!checkMainDepartmentNull && !isAdmin) {
      toast.warning("Chưa chọn phòng ban chính");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return;
    }
    if (!id) {
      id = -1;
    }
    checkEmail(email, id).then(({ data }) => {
      if (data) {
        toast.warning("Địa chỉ email đã tồn tại!");
        toast.clearWaitingQueue();
        this.setState({ loading: false });
        return;
      }

      getUserByUsername(this.state.username).then(({ data }) => {
        if (data && data.id) {
          if (!id || (id && data.id != id)) {
            // alert('Tên đăng nhập đã tồn tại!')
            toast.warning("Tên đăng nhập đã tồn tại!");
            toast.clearWaitingQueue();
            this.setState({ loading: false });
            return;
          }
        }
        let { listDepartment } = this.state;
        listDepartment.map((id) => {
          deleteUserDepartmentId(id)
            .then(({ data }) => { })
            .catch((err) => {
              toast.warning("Có lỗi khi xoá phòng ban của tài khoản");
              toast.clearWaitingQueue();
              this.setState({ loading: false });
              return;
            });
        });

        saveUser({
          ...this.state,
        })
          .then((user) => {
            if (user.data != null && user.data.id != null) {
              let userDepartment = {};
              userDepartment.id = this.state.userDepartmentId;
              userDepartment.user = user.data;
              userDepartment.department = this.state.department;
              userDepartment.isMainDepartment = true;
              // saveUserDepartment(userDepartment).then(() => {
              //   this.props.handleOKEditClose();
              // });

              saveUserDepartments(
                userDepartments.map((element) => {
                  return {
                    id: element.id,
                    user: { id: user.data.id },
                    department: element.department,
                    isMainDepartment: element.isMainDepartment,
                  };
                })
              )
                .then(({ data }) => {
                  toast.success(this.props.t("general.success"));
                  this.props.handleOKEditClose();
                })
                .catch((err) => {
                  toast.warning(
                    "Có lỗi xảy ra khi lưu phòng ban của người dùng"
                  );
                  toast.clearWaitingQueue();
                  this.setState({ loading: false });
                  return;
                });
            }

            // toast.info(this.props.t('general.success'));
            // this.props.handleOKEditClose();
          })
          .catch((err) => {
            toast.warning("Có lỗi xảy ra khi lưu tài khoản");
            toast.clearWaitingQueue();
            this.setState({ loading: false });
            return;
          });
      });
    });
  };

  selectRoles = (rolesSelected) => {
    this.setState({ roles: rolesSelected }, function () {
    });
  };

  componentWillMount() {
    let { open, handleClose, item, department, userDepartmentId } = this.props;
    this.setState(item);
    this.setState({ department, userDepartmentId });
    if (item && item.id) {
      listByUserId(item.id).then(({ data }) => {
        this.setState({ userDepartments: data, password: null }, () => {
        });
      });
    }
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    getAllRoles().then(({ data }) => {
      this.setState({
        listRole: data,
      });
    });
  }

  selectOrganization = (item) => {
    if (item !== null) {
      this.setState({ org: item });
    }
  };

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let {
      id,
      isAddNew,
      listRole,
      roles,
      active,
      email,
      person,
      username,
      changePass,
      password,
      confirmPassword,
      openSelectDepartmentPopup,
      department,
      userDepartments,
      listDepartment,
      org,
      loading,
    } = this.state;

    let orgSearchObject = { pageIndex: 1, pageSize: 1000000 };

    let columnsUserDepartment = [
      {
        title: t("manage.isMainDepartment"),
        field: "custom",
        width: "210px",
        align: "center",
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Checkbox
            checked={rowData.isMainDepartment}
            onChange={(event, checked) => {
              if (checked == true) {
                userDepartments.forEach((el) => (el.isMainDepartment = false));
                const finder = userDepartments.find(
                  (el) =>
                    el.department && el.department.id === rowData.department.id
                );
                if (finder) {
                  finder.isMainDepartment = true;
                  this.setState({ userDepartments });
                }
              }
            }}
          />
        ),
      },
      { title: t("manage.department"), field: "department.name", width: "150" },
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        width: "120px",
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
              } else if (method === 1) {
                for (let index = 0; index < userDepartments.length; index++) {
                  const item = userDepartments[index];
                  if (
                    rowData.department &&
                    item.department &&
                    rowData.department.id === item.department.id
                  ) {
                    if (rowData.id) {
                      // deleteById(rowData.id).then(({ data }) => {
                      // })
                      listDepartment.push(rowData.id);
                    }
                    userDepartments.splice(index, 1);
                    this.setState({ userDepartments });
                    break;
                  }
                }
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    return (
      <Dialog
        onClose={handleClose}
        open={open}
        maxWidth={"md"}
        fullWidth={true}
      >
        {/* <div className="p-24"> */}
        {/* <h4 className="mb-20">{t('general.saveUpdate')}</h4> */}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("user.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent>
            <Grid className="" container spacing={2}>
              <Grid item sm={3} xs={12}>
                <TextValidator
                  InputProps={{
                    readOnly: !isAddNew,
                  }}
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("user.username")}
                    </span>
                  }
                  // label={t('user.username')}
                  onChange={this.handleChange}
                  type="text"
                  name="username"
                  value={username}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={3} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed"></span>
                      {t("organization.name")}
                    </span>
                  }
                  disabled={true}
                  type="text"
                  name="name"
                  value={org?.name}
                />
                {/* <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("organization.name")}
                    </span>
                  }
                  disabled = {true}
                  searchFunction={searchByPageOrg}
                  searchObject={orgSearchObject}
                  defaultValue={org}
                  displayLable={"name"}
                  value={org ? org : null}
                  onSelect={this.selectOrganization}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                /> */}
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("user.displayName")}
                    </span>
                  }
                  onChange={(displayName) =>
                    this.handleChange(displayName, "displayName")
                  }
                  type="text"
                  name="name"
                  value={person ? person.displayName : ""}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="gender-simple">
                    {t("user.gender")}
                  </InputLabel>
                  <Select
                    value={person ? person.gender : ""}
                    onChange={(gender) => this.handleChange(gender, "gender")}
                    inputProps={{
                      name: "gender",
                      id: "gender-simple",
                    }}
                  >
                    {this.listGender.map((item) => {
                      return (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6} xs={12}>
                {listRole && (
                  <Autocomplete
                    style={{ width: "100%" }}
                    multiple
                    id="combo-box-demo"
                    defaultValue={roles}
                    options={listRole}
                    getOptionSelected={(option, value) =>
                      option.id === value.id
                    }
                    getOptionLabel={(option) => option.authority}
                    onChange={(event, value) => {
                      this.selectRoles(value);
                    }}
                    renderInput={(params) =>
                      this.state.roles.length === 0 ? (
                        <TextValidator
                          style={{ marginTop: "9.5px" }}
                          {...params}
                          value={roles}
                          // label={t('user.role')}
                          label={
                            <span>
                              <span className="colorRed">*</span>
                              {t("user.role")}
                            </span>
                          }
                          fullWidth
                          validators={["required"]}
                          errorMessages={[t("user.please_select_permission")]}
                        />
                      ) : (
                        <TextValidator
                          {...params}
                          value={roles}
                          // label={t('user.role')}
                          label={
                            <span>
                              <span className="colorRed">*</span>
                              {t("user.role")}
                            </span>
                          }
                          fullWidth
                          validators={["required"]}
                          errorMessages={[t("user.please_select_permission")]}
                        />
                      )
                    }
                  />
                )}
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("user.email")}
                    </span>
                  }
                  style={{ marginTop: "9.25px" }}
                  onChange={this.handleChange}
                  type="email"
                  name="email"
                  value={email}
                  validators={["required", "isEmail", 'matchRegexp:^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$']}
                  errorMessages={[t("general.required"), t("general.emailNotValid"), t("general.emailNotValid")]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <div style={{ marginTop: "24.25px" }}>
                  <FormControlLabel
                    value={active}
                    className=""
                    name="active"
                    onChange={(active) => this.handleChange(active, "active")}
                    control={<Checkbox checked={active} />}
                    label={t("user.active")}
                  />
                </div>
              </Grid>

              {!isAddNew && (
                <Grid item sm={6} xs={12}>
                  <FormControlLabel
                    value={changePass}
                    className=""
                    name="changePass"
                    onChange={(changePass) =>
                      this.handleChange(changePass, "changePass")
                    }
                    control={<Checkbox checked={changePass} />}
                    label={t("user.changePass")}
                  />
                </Grid>
              )}

              {changePass != null && changePass == true ? (
                <Grid container spacing={2}>
                  <Grid item sm={6} xs={12}>
                    <TextValidator
                      className=" w-100"
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("user.pass")}
                        </span>
                      }
                      variant="outlined"
                      onChange={this.handleChange}
                      name="password"
                      type="password"
                      value={password}
                      validators={["required", "matchRegexp:^.{6,}$"]}
                      errorMessages={[
                        t("general.required"),
                        "Mật khẩu ít nhất có 6 kí tự",
                      ]}
                    />
                  </Grid>
                  <Grid item sm={6} xs={12}>
                    <TextValidator
                      className=" w-100"
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("user.re_pass")}
                        </span>
                      }
                      variant="outlined"
                      onChange={this.handleChange}
                      name="confirmPassword"
                      type="password"
                      value={confirmPassword}
                      validators={["required", "isPasswordMatch"]}
                      errorMessages={[
                        t("general.required"),
                        "Mật khẩu không khớp",
                      ]}
                    />
                  </Grid>
                </Grid>
              ) : (
                <div></div>
              )}

              <Grid item md={12} sm={12} xs={12}>
                {/* <TextValidator
                  InputProps={{
                    readOnly: true,
                  }}
                  label={t("user.department")}
                  className="w-80  mr-16"
                  value={department != null ? department.name : ''}
                /> */}
                <Button
                  className=" mt-10 mb-10"
                  variant="contained"
                  size="small"
                  color="primary"
                  onClick={() =>
                    this.setState({ openSelectDepartmentPopup: true, item: {} })
                  }
                >
                  {t("general.select")}
                </Button>
                {openSelectDepartmentPopup && (
                  <SelectDepartmentPopup
                    open={openSelectDepartmentPopup}
                    handleSelect={this.handleSelectUseDepartment}
                    selectedItem={department != null ? department : {}}
                    handleClose={this.handleSelectDepartmentPopupClose}
                    orgFromUserDialog={this.state.org}
                    t={t}
                    i18n={i18n}
                  />
                )}
              </Grid>
            </Grid>
            <MaterialTable
              title={t("manage.departmentList")}
              data={userDepartments}
              columns={columnsUserDepartment}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: 0,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "223px",
                minBodyHeight: "223px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
                sorting: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-10">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                style={{ marginRight: "15px" }}
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
        {/* </div> */}
      </Dialog>
    );
  }
}

export default UserEditorDialog;
