import React, { Component } from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  TextField,
  FormControl,
  Input,
  InputAdornment,
  MenuItem,
  Select,
  InputLabel,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  findUserByUserName,
  getItemById,
  deleteById,
  getListManagementDepartment,
  searchByPageNew,
  exportExampleImportExcel,
  exportExcelFileError,
  importExcelUserURL,
} from "./UserService";
import UserEditorDialog from "./UserEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Autocomplete } from "@material-ui/lab";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import {functionExportToExcel, handleThrowResponseMessage, isSuccessfulResponse} from "app/appFunction";

toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">
          edit
        </Icon>
      </IconButton>

      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

class User extends Component {
  state = {
    userDepartmentId: "",
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    eQAHealthOrgType: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isActive: null,
    shouldOpenOrganizationPopup: false,
    org: null,
    departmentToSearch: null,
    listDepartmentToSearch: []
  };
  numSelected = 0;
  rowCount = 0;

  listActive = [
    { value: true, name: "Đã kích hoạt" },
    { value: false, name: "Chưa kích hoạt" },
    { value: "all", name: "Tất cả" },
  ];

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {
      if (this.state?.keyword === "") {
        this.search()
      }
    });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    })
  }

  updatePageData = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context
    setPageLoading(true)
    let searchObject = {};
    searchObject.mainDepartmentId = this.state.departmentToSearch?.id;
    searchObject.isActive = this.state.isActive;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPageNew(searchObject).then((res) => {
      const {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        if (!data.content?.length && data.totalPages) {
          this.search();
          return;
        }

        this.setState({
          itemList: data?.content?.length > 0 ? [...data?.content] : [],
          totalElements: data?.totalElements,
        });
      } else {
        handleThrowResponseMessage(res);
        this.setState({
          itemList: [],
        });
        toast.error(t("toastr.error"));
      }
    })
      .catch((err) => {
        toast.error(t("toastr.error"));
      })
      .finally(() => {
        setPageLoading(false)
      })
      ;
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false
    });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  exportExampleImportExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcel,
        {},
        t("exportToExcel.UserImportExample")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false
    });
    this.updatePageData();
  };

  handleDeleteUser = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditUser = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteById(this.state.userId).then((data) => {
      if (!data.data) {
        toast.warning("Không thể xóa.");
      } else {
        toast.success("Xoá thành công.");
      }
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  componentDidMount() {
    let { t } = this.props;
    const departmentSearchObject = {
      pageIndex: 1,
      pageSize: 100000
    };
    getListManagementDepartment(departmentSearchObject)
      .then(res => {
        this.setState({
          listDepartmentToSearch: res?.data?.content
        })
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      })

    this.updatePageData();
  }
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (row) => {
    this.setState({
      userId: row?.id || row?.user?.id,
      id: row.id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    // for (var i = 0; i < list.length; i++) {
    //   await deleteById(list[i].user.id);
    // }

    let listAlert = [];
    let { t } = this.props;
    for (var i = 0; i < list.length; i++) {
      await deleteById(list[i]?.id).then(({ data }) => {
        if (!data) {
          listAlert.push(list[i]?.id);
        }
      });
    }
    this.handleDialogClose();
    if (listAlert.length === list.length) {
      toast.warning(t("Tất cả tài khoản đều đã sử dụng"));
    } else if (listAlert.length > 0) {
      toast.warning(t("Đã xoá tài khoản chưa sử dụng"));
    } else {
      toast.success(t("general.deleteSuccess"));
    }
  }

  handleDeleteAll = (event) => {
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  checkData = () => {
    let { t } = this.props;
    if (!this.data || this.data.length === 0) {
      toast.warn(t("general.noti_check_data"));
      toast.clearWaitingQueue();
      // this.setState({shouldOpenNotificationPopup: true,
      //   Notification:"general.noti_check_data"})
      //  ("Chưa chọn dữ liệu");
    } else if (this.data.length === this.state.itemList.length) {
      // alert("Bạn có muốn xoá toàn bộ");

      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };

  handleChange = (event, source) => {
    if (source === "isActive") {
      let { isActive } = this.state;
      let Active = event.target.value;
      if (Active == "all") {
        isActive = null;
      } else {
        isActive = Active;
      }
      this.setState({ isActive: isActive }, function () {
        this.search();
      });
      return;
    }
  };

  handleSelectOrganization = (item) => {
    this.setState({ org: { id: item.id, name: item.name } }, function () {
      this.handleOrganizationPopupClose();
      this.search();
    });
  };

  handleOrganizationPopupClose = () => {
    this.setState({ shouldOpenOrganizationPopup: false });
  };

  handleSelectDepartmentToSearch(department) {
    this.setState({
      departmentToSearch: department
    }, function () {
      this.search();
    });
  }

  render() {
    const { t, i18n } = this.props;
    let {
      id,
      userDepartmentId,
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      department,
      shouldOpenConfirmationDialog,
      shouldOpenImportExcelDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenOrganizationPopup,
      org,
      departmentToSearch,
      listDepartmentToSearch
    } = this.state;
    let TitlePage = t("user.title");
    let importUrl = importExcelUserURL();

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        headerStyle: {
          paddingLeft: "10px",
        },
        cellStyle: {
          paddingLeft: "10px",
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data != null && data.id != null) {
                    this.setState(
                      {
                        item: data,
                      },
                      function () {
                        this.setState({ shouldOpenEditorDialog: true });
                      }
                    );
                  }
                });
              } else if (method === 1) {
                this.handleDelete(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "code",
        width: "150",
        align: "left",
        headerStyle: {
          paddingLeft: "7px",
        },
        cellStyle: {
          paddingLeft: "10px",
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("user.username"),
        field: "username",
        width: "150",
        align: "left",
        headerStyle: {
          paddingLeft: "10px",
        },
        cellStyle: {
          paddingLeft: "10px",
        },
      },
      { title: t("user.displayName"), field: "displayName", width: "150" },
      {
        title: t("user.email"),
        field: "email",
        align: "left",
        width: "150",
      },
      {
        title: t("user.isMainDepartment"),
        field: "departmentName",
        align: "left",
        width: "150",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('user.title') }]} /> */}
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.manage"), path: "/manage/user" },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2}>
          <Grid item md={4} xs={12}>
            <Button
              className=" align-bottom mr-12"
              style={{ marginTop: "9px" }}
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                  isAddNew: true,
                });
              }}
            >
              {t("general.add")}
            </Button>

            <Button
              className="align-bottom mr-16"
              variant="contained"
              color="primary"
              onClick={() => this.importExcel()}
            >
              {t("general.importExcel")}
            </Button>

            <Button
              className=" mr-36 align-bottom"
              variant="contained"
              color="primary"
              style={{ marginTop: "9px" }}
              onClick={() => this.checkData()}
            >
              {t("general.delete")}
            </Button>
            {shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t("DeleteAllConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}
          </Grid>

          {/* <Grid item md={2} sm={6} xs={12} className="">
            <ValidatorForm>
              <TextValidator
                InputProps={{
                  readOnly: true,
                }}
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("organization.name")}
                  </span>
                }
                className="w-100"
                // style={{width:"60%"}}
                value={org != null ? org.name : ""}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />

              {shouldOpenOrganizationPopup && (
                <SelectOrganizationPopup
                  open={shouldOpenOrganizationPopup}
                  handleSelect={this.handleSelectOrganization}
                  handleClose={this.handleOrganizationPopupClose}
                  t={t}
                  i18n={i18n}
                />
              )}
            </ValidatorForm>
          </Grid>
          <Grid item md={1} sm={6} xs={12} className="">
            <Button
              size={"small"}
              className=" align-bottom mr-12"
              style={{ marginTop: "12px" }}
              variant="contained"
              color="primary"
              onClick={() =>
                this.setState({ shouldOpenOrganizationPopup: true })
              }
            >
              {t("general.select")}
            </Button>
          </Grid> */}

          <Grid item md={3} xs={12}>
            <Autocomplete
              value={departmentToSearch ?? null}
              options={listDepartmentToSearch || []}
              getOptionLabel={option => option.text}
              onChange={(e, value) => this.handleSelectDepartmentToSearch(value)}
              renderInput={(params) => <TextField
                {...params}
                label={"Lọc theo phòng ban"}
                value={departmentToSearch?.text ?? ""}
              />}
            />
          </Grid>
          <Grid item md={2} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="isActive">
                {t("Lọc tài khoản kích hoạt")}
              </InputLabel>
              <Select
                // style={{ marginRight: '12px'}}
                onChange={(isActive) => this.handleChange(isActive, "isActive")}
                inputProps={{
                  name: "isActive",
                  id: "isActive",
                }}
              >
                {this.listActive.map((item) => {
                  return (
                    <MenuItem key={item.name} value={item.value}>
                      {item.value != null ? item.name : "Tất cả"}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <FormControl fullWidth style={{ marginTop: "16px" }}>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("user.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment>
                    <Link to="#">
                      {" "}
                      <SearchIcon
                        onClick={() => this.search(keyword)}
                        style={{ position: "absolute", top: "0", right: "0" }}
                      />
                    </Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <UserEditorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  department={department}
                  userDepartmentId={userDepartmentId}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("DeleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}

              {shouldOpenImportExcelDialog && (
                <ImportExcelDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenImportExcelDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  exportExampleImportExcel={this.exportExampleImportExcel}
                  exportFileError={exportExcelFileError}
                  url={importUrl}
                />
              )}
            </div>
            <MaterialTable
              title={t("user.userList")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                sorting: false,
                selection: true,
                actionsColumnIndex: 0,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: "450px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
                // this.setState({selectedItems:rows});
              }}
              actions={[
                {
                  hidden: true,
                  isFreeAction: true,
                  tooltip: "Remove All Selected Users",
                  icon: "delete",
                  onClick: (evt, data) => {
                    this.handleDeleteAll(data);
                    alert("You want to delete " + data.length + " rows");
                  },
                },
              ]}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
User.contextType = AppContext
export default User;
