import axios from "axios";
import ConstantList from "../../appConfig";
import {STATUS_DEPARTMENT} from "app/appConst";

const API_PATH = ConstantList.API_ENPOINT + "/api/users";
const API_PATH_USER_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_USER_DEPARTMENT_V2 = ConstantList.API_ENPOINT + "/api/v1/user-departments";
const API_PATH_USER_DEPARTMENT_V1 = ConstantList.API_ENPOINT + "/api/v1/user-departments";
const API_PATH_USER_DEPARTMENT_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/user-departments";
const API_PATH_ROLE = ConstantList.API_ENPOINT + "/api/assetroles";
const API_PATH_USER = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/users";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_TEMPLATE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

export const searchByPage = (pageIndex, pageSize) => {
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH + "/" + params;
  return axios.get(url);
};
// org_admin
export const findUserByUserName = (searchObject) => {
  var url = API_PATH_USER_DEPARTMENT + "/searchByPage";
  return axios.post(url, {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

export const searchByPageNew = (searchObject) => {
  let config = {
    params: {
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      ...searchObject,
    }
  }
  return axios.get(API_PATH_USER, config);
};

export const searchByPageUsersDepartmentNew = (searchObject) => {
  let config = {
    params: {
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      ...searchObject,
    }
  }
  return axios.get(API_PATH_USER + "/search-by-page", config);
};

// get User Department
export const getUserDepartment = (searchObject) => {
  const config = {
    params: {
      isUserDepartmentManagement: searchObject?.isUserDepartmentManagement,
      departmentId: searchObject?.departmentId,
      isNotAdminUser: searchObject?.isNotAdminUser,
      isMainDepartment: searchObject?.isMainDepartment,
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
    },
  };
  let url = API_PATH_USER_DEPARTMENT_V1 + "/page";
  return axios.get(url, config);
};

export const getAllRoles = () => {
  var url = API_PATH_ROLE + "/all";
  return axios.get(url);
};

export const getItemById = (id) => {
  var url = API_PATH + "/" + id;
  return axios.get(url);
};

export const getUserByUsername = (username) => {
  const config = { params: { username: username } };
  var url = API_PATH_USER_DEPARTMENT;
  return axios.get(url, config);
};

export const checkEmail = (email, id) => {
  var url = API_PATH_USER_DEPARTMENT + "/checkEmail/" + email + "/" + id;
  return axios.get(url);
};

export const saveUser = (user) => {
  // return axios.post(API_PATH, user);
  return axios.post(API_PATH_USER_DEPARTMENT + "/saveUser", user);
};

export const saveUserCurrent = (user) => {
  return axios.post(API_PATH, user);
};

export const saveUserDepartment = (UserDepartment) => {
  if (UserDepartment.id) {
    return axios.put(
      API_PATH_USER_DEPARTMENT + "/" + UserDepartment.id,
      UserDepartment
    );
  } else {
    return axios.post(API_PATH_USER_DEPARTMENT, UserDepartment);
  }
};
export const saveUserDepartments = (UserDepartments) => {
  if (UserDepartments) {
    return axios.post(
      API_PATH_USER_DEPARTMENT + "/" + "saveUserDepartments",
      UserDepartments
    );
  }
};
export const listByUserId = (userId) => {
  if (userId) {
    return axios.get(API_PATH_USER_DEPARTMENT + "/" + "listByUserId/" + userId);
  }
};
export const deleteById = (userId) => {
  return axios.delete(API_PATH_USER_DEPARTMENT + "/" + userId);
};

export const deleteUserDepartmentId = (userDepartmentId) => {
  return axios.delete(API_PATH_USER_DEPARTMENT + "/delete/" + userDepartmentId);
};

export const getUserDepartmentByUserId = (id) => {
  var url = API_PATH_USER_DEPARTMENT + "/getUserDepartmentByUserId/" + id;
  return axios.get(url);
};

export const getByPageUserDepartment = (searchObject) => {
  var url = API_PATH_USER_DEPARTMENT_V2 + "/page?";
  for (const [key, value] of Object.entries(searchObject)) {
    if (value) {
      url += `${key}=${value}&`;
    }
  }
  return axios.get(url);
};

export const getListManagementDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

export const exportExampleImportExcel = () => {
  return axios({
    method: 'post',
    url: API_PATH_TEMPLATE + "/import-user",
    responseType: 'blob',
  })
};

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + linkFile,
    responseType: "blob",
  });
};

export const importExcelUserURL = () => {
  return ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/users/excel/import";
};

export const getUserByDepartment = (params) => {
  return axios.get(API_PATH_USER_DEPARTMENT_NEW + "/users", {params});
}
