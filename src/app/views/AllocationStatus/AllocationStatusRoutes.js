import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AllocationStatusTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./AllocationStatusTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(AllocationStatusTable);

const AllocationStatusRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/allocation_status",
    exact: true,
    component: ViewComponent
  }
];

export default AllocationStatusRoutes;