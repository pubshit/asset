import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  DialogContent
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkDuplicateCode, createAllocationStatus, updateAllocationStatus } from "./AllocationStatusService";
import Draggable from 'react-draggable';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { handleKeyDownNameSpecialExcept } from "../../appFunction";
import {appConst, variable} from "app/appConst";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});

function PaperComponent(props) {
    return (
      <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
      </Draggable>
    );
  }

class AllocationStatusDialog extends Component {
  state = {
      id:"",
    name: "",
    code: "",
    indexOrder: 0,
    isActive: false,
    shouldOpenNotificationPopup:false,
    Notification:"",
  };

  listIndexOder = [
    { id: 0, name: 'Tiến một bước' },
    { id: 1, name: 'Lùi một bước' }
  ]

  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    if (source === "indexOrder") {
      const value = Number(event.target.value);

      let newIndexOrder;
      if (value === this.listIndexOder[0].id) {
        newIndexOrder = this.props.item?.indexOrder 
                         ? this.props.item.indexOrder + 1 
                         : this.state.indexOrder + 1;
      } else if (value === this.listIndexOder[1].id) {
        newIndexOrder = this.props.item?.indexOrder 
                         ? this.props.item.indexOrder - 1 
                         : this.state.indexOrder - 1;
      }

      this.setState({ [event.target.name]: newIndexOrder });
      return;
    }
    this.setState({
        [event.target.name]:  event.target.value
      }
    )
  };
  
  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;

    checkDuplicateCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(this.props.t('AllocationStatus.noti.dupli_code'))
      } else {
        if (id) {
          updateAllocationStatus({
            ...this.state
          }).then(() => {
            toast.info('Sửa trạng thái thành công.')
            this.props.handleOKEditClose();
          });
        } else {
          createAllocationStatus({
            ...this.state
          }).then(() => {
            toast.info('Thêm mới trạng thái thành công.')
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentWillMount() {
    let { item } = this.props;
    this.setState(item);
  }

  render() {
    let {
      name,
      code,
      shouldOpenNotificationPopup,
    } = this.state;
    let { open, t } = this.props;
    return (
      <Dialog   open={open}  PaperComponent={PaperComponent} maxWidth="sm" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <h4 className="">{t("AllocationStatus.dialog")}</h4>
        </DialogTitle>
          
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={7} xs={12}>
                <TextValidator
                  className="w-100 "
                  
                  label={<span><span className="colorRed">*</span>{t('AllocationStatus.code')}</span>}
                  onChange={(e) => this.handleChange(e, "code")}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>
              <Grid item sm={5} xs={12} >
                <FormControl fullWidth={true} >
                  <InputLabel htmlFor="indexOrderId" >{t("AllocationStatus.order")}</InputLabel>
                  <Select
                    onChange={indexOrder => this.handleChange(indexOrder, "indexOrder")}
                    inputProps={{
                      name: "indexOrder",
                      id: "indexOrderId"
                    }}
                  >
                    {this.listIndexOder.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span className="colorRed">*</span>{t('AllocationStatus.name')}</span>}

                  onChange={this.handleChange}
                  onKeyDown={(e) => {
                    handleKeyDownNameSpecialExcept(e) 
                  }}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
              
            </div>
        </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AllocationStatusDialog;
