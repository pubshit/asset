import React, { Component } from "react";
import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  FormControlLabel,
  Grid,
  Icon,
  IconButton,
  InputLabel,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AppContext from "app/appContext";
import {
  checkInvalidDate,
  convertNumberPriceRoundUp,
  filterOptions,
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { getStockKeepingUnit } from "../StockKeepingUnit/StockKeepingUnitService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListWarehouseByDepartmentId as getListWarehouse } from "../AssetTransfer/AssetTransferService";
import { createFilterOptions } from "@material-ui/lab";
import MaterialTable, { MTableToolbar } from "material-table";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import { getAllListManagementDepartment } from "../MaintainResquest/MaintainRequestService";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
toast.configure();

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

class RadixDialog extends Component {
  state = {
    rowsPerPage: 10000,
    page: 0,
    item: {
      createdDate: new Date(),
    },
    listDepartment: [],
    listUnit: [],
    shouldOpenAssetPopup: false,
  };

  getAllManagementDepartment = async () => {
    let { t } = this.props;
    try {
      let managementDepartmentSearchObject = {
        pageIndex: 1,
        pageSize: 1000000,
        keyword: "",
      };
      let res = await getAllListManagementDepartment(
        managementDepartmentSearchObject
      );
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.content) {
        this.setState({
          listDepartment: res?.data?.content,
        });
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    }
  };
  componentDidMount() {
    this.getAllManagementDepartment();
  }

  componentDidUpdate(prevProps, prevState) {}

  handleFormSubmit = (event) => {};
  handleChange = (e, name) => {
    e.persist();
    if (name === "isManageAccountant") {
      this.setState({
        item: {
          ...this.state.item,
          isManageAccountant: e.target.checked,
        },
      });
      return;
    }
    this.setState({
      item: {
        ...this.state.item,
        [name]: e.target.value,
      },
    });
  };
  handleDateChange = (date, name) => {
    this.setState({
      item: {
        ...this.state.item,
        [name]: date,
      },
    });
  };

  handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      this.handleDateChange(null, name);
    }
  };
  handleSetDataSelect = (data, source) => {
    if (source === "store" && this.state.item.store?.id !== data.id) {
      this.setState({
        item: {
          ...this.state.item,
          [source]: data,
          assetVouchers: [],
        },
      });
      return;
    }
    this.setState({
      item: {
        ...this.state.item,
        [source]: data,
      },
    });
  };
  selectUnit = (unitSelected) => {
    if (unitSelected?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogUnit: true,
      });
      return;
    }

    this.setState({
      item: {
        ...this.state.item,
        unit: unitSelected,
      },
    });
  };
  handleSetListData = (data, source) => {
    this.setState({
      [source]: data,
    });
  };
  openPopupSelectAsset = () => {
    let {t} = this.props
    let { item } = this.state;
    if (item?.store) {
      this.setState({
        storeId: item?.store?.id,
        shouldOpenAssetPopup: true,
      });
    } else {
      toast.info(t("Radix.noti.missingStore"));
    }
  };
  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state.item;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      item: {
        ...this.state.item,
        assetVouchers: assetVouchers,
      },
    });
  };
  handleSelectAsset = (items) => {
    let {t} = this.props
    if (items?.length > 0) {
      this.setState(
        { item: { ...this.state.item, assetVouchers: items } },
        function () {
          this.handleAssetPopupClose();
        }
      );
    } else {
      this.setState(
        { item: { ...this.state.item, assetVouchers: items } },
        function () {
          this.handleAssetPopupClose();
        }
      );
      toast.warning(t("Radix.noti.missingAssetVouchers"));
    }
  };
  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  render() {
    let { open, t, handleClose } = this.props;
    let {
      item,
      loading,
      listDepartment,
      shouldOpenAssetPopup,
      isView,
      listUnit,
    } = this.state;
    let searchObjectUnit = { pageIndex: 1, pageSize: 1000000 };
    let searchObjectWarehouse = {
      pageIndex: 1,
      pageSize: 1000000,
      isAssetManagement: true,
    };

    const filterAutocomplete = createFilterOptions();
    let columns = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: "50px",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        minWidth: 120,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "asset.serialNumber",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) =>
          rowData?.asset?.serialNumber || rowData?.assetSerialNumber,
      },
      {
        title: t("Asset.model"),
        field: "asset.model",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => rowData?.asset?.model || rowData?.assetModel,
      },
      {
        title: t("Asset.yearIntoUseTable"),
        field: "asset.yearPutIntoUse",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "asset.yearOfManufacture",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "asset.manufacturer.name",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => rowData?.asset?.model || rowData?.manufacturerName,
      },
      {
        title: t("Asset.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 150,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.asset?.originalCost
            ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
            : "",
      },
      {
        title: t("Asset.carryingAmount"),
        field: "asset.carryingAmount",
        align: "left",
        minWidth: 150,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.asset?.carryingAmount
            ? convertNumberPriceRoundUp(rowData?.asset?.carryingAmount)
            : "",
      },
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          !isView && (
            <MaterialButton
              item={rowData}
              onSelect={(rowData, method) => {
                if (appConst.active.delete === method) {
                  this.removeAssetInlist(rowData?.asset?.id);
                } else {
                  alert("Call Selected Here:" + rowData?.id);
                }
              }}
            />
          ),
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth={true}
        scroll={"paper"}
      >
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="parent"
          onSubmit={(event) => this.handleFormSubmit(event)}
          className="validator-form-scroll-dialog"
          onKeyDown={this.handlePressEnter}
        >
          <DialogTitle
            id="draggable-dialog-title"
            style={{
              cursor: "move",
              paddingTop: "5px",
              paddingBottom: "5px",
              background: "#f5f5f5",
            }}
          >
            <span className="mb-1">{t("Radix.create")}</span>
            <IconButton
              size="small"
              onClick={() => handleClose()}
              className="position-top-right mt-8 mr-8"
            >
              <Icon fontSize="small">close</Icon>
            </IconButton>
          </DialogTitle>

          <DialogContent
            style={{
              paddingTop: "0px",
              paddingBottom: "0px",
              background: "#f5f5f5",
            }}
          >
            <Grid
              container
              spacing={1}
              className=""
              style={{ background: "#f5f5f5" }}
            >
              <Grid item xs={12} sm={4} md={4}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("Radix.code")}
                    </span>
                  }
                  onChange={(e) => this.handleChange(e, "code")}
                  type="text"
                  name="code"
                  defaultValue={item?.code ? item?.code : ""}
                  value={item?.code ? item?.code : ""}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item xs={12} sm={4} md={4}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("Radix.name")}
                    </span>
                  }
                  onChange={(e) => this.handleChange(e, "name")}
                  type="text"
                  name="name"
                  defaultValue={item?.name ? item?.name : ""}
                  value={item?.name ? item?.name : ""}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item xs={12} sm={4} md={2}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("Radix.createdDate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={item?.createdDate}
                  onChange={(date) =>
                    this.handleDateChange(date, "createdDate")
                  }
                  validators={["required"]}
                  errorMessages={t("general.required")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  maxDate={new Date()}
                  minDate={new Date("1/1/1900")}
                  maxDateMessage={t("Radix.noti.errMaxDate")}
                  minDateMessage={t("Radix.noti.errMinDate")}
                  onBlur={() =>
                    this.handleBlurDate(item?.createdDate, "createdDate")
                  }
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={2}
                style={{ transform: "translateY(15px)" }}
              >
                <FormControlLabel
                  className="mr-4"
                  value={item?.isManageAccountant}
                  name="isManageAccountant"
                  onChange={(isManageAccountant) =>
                    this.handleChange(isManageAccountant, "isManageAccountant")
                  }
                  control={<Checkbox checked={item?.isManageAccountant} />}
                  label={
                    <span style={{ fontSize: "14px" }}>
                      {t("Radix.isManageAccountant")}
                    </span>
                  }
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <AsynchronousAutocompleteTransfer
                  isFocus={true}
                  fullWidth
                  label={t("InstrumentToolsList.unit")}
                  searchFunction={getStockKeepingUnit}
                  searchObject={searchObjectUnit}
                  listData={listUnit}
                  setListData={(data) =>
                    this.handleSetListData(data, "listUnit")
                  }
                  defaultValue={item?.unit}
                  displayLable={"name"}
                  value={item?.unit}
                  onSelect={this.selectUnit}
                  filterOptions={(options, params) =>
                    filterOptions(options, params, true, "name")
                  }
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("Radix.storeName")}
                    </span>
                  }
                  searchFunction={getListWarehouse}
                  searchObject={searchObjectWarehouse}
                  defaultValue={item?.store ?? ""}
                  displayLable="name"
                  value={item?.store ?? ""}
                  onSelect={(store) => this.handleSetDataSelect(store, "store")}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    let filtered = filterAutocomplete(options, params);
                    return filtered;
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={4}
                style={{ transform: "translateY(3px)" }}
              >
                <Autocomplete
                  id="combo-box"
                  fullWidth
                  size="small"
                  name="room"
                  options={listDepartment}
                  onChange={(event, useDepartment) =>
                    this.handleSetDataSelect(useDepartment, "useDepartment")
                  }
                  getOptionLabel={(option) => option.name}
                  value={item?.useDepartment || null}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    let filtered = filterAutocomplete(options, params);
                    return filtered;
                  }}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      label={
                        <>
                          <span style={{ color: "#e52d00" }}>*</span>
                          <span>{t("Radix.useDepartment")}</span>
                        </>
                      }
                      variant="standard"
                      inputProps={{
                        ...params.inputProps,
                      }}
                      value={item?.useDepartment || null}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  )}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <TextValidator
                  className="w-100"
                  label={<span>{t("Radix.note")}</span>}
                  onChange={(e) => this.handleChange(e, "note")}
                  type="text"
                  name="note"
                  defaultValue={item?.note ? item?.note : ""}
                  value={item?.note ? item?.note : ""}
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={1}
              justifyContent="space-between"
              className="pb-20 pt-20"
            >
              <Grid item md={12} sm={12} xs={12} className="mb-10">
                <InputLabel htmlFor="isManageAccountant">
                  <span className="pt-10 font-weight-600 font-size-16 text-black">
                    {t("Radix.list_asset")}:
                  </span>
                </InputLabel>
              </Grid>
              <Grid item md={3} sm={3} xs={3}>
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={this.openPopupSelectAsset}
                >
                  {t("Radix.select_asset")}
                </Button>
              </Grid>
              <Grid item md={12} sm={12} xs={12} className="mt-16">
                <MaterialTable
                  data={item?.assetVouchers ? item.assetVouchers : []}
                  columns={columns}
                  options={{
                    draggable: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                    }),
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                      paddingLeft: 10,
                      paddingRight: 10,
                      textAlign: "center",
                    },
                    padding: "dense",
                    maxBodyHeight: "220px",
                    minBodyHeight: "220px",
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div className="w-100">
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>

            {item?.store && shouldOpenAssetPopup && (
              <SelectAssetAllPopup
                open={shouldOpenAssetPopup}
                handleSelect={this.handleSelectAsset}
                storeId={item?.store?.id}
                handleClose={this.handleAssetPopupClose}
                t={t}
                assetVouchers={
                  item?.assetVouchers != null ? item?.assetVouchers : []
                }
                type="warehouseAssetTransfer"
                assetClass={this.props?.assetClass}
              />
            )}
          </DialogContent>
          <DialogActions>
            <div
              className="flex flex-space-between flex-middle"
              style={{ background: "#f5f5f5" }}
            >
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => {
                  this.props.handleClose();
                }}
              >
                {t("general.cancel")}
              </Button>
              <Button
                className="mr-12"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

RadixDialog.contextType = AppContext;
export default RadixDialog;
