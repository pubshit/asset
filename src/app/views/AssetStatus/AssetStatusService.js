import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/assetstatus" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getAllAssetStatuss = () => {
  return axios.get(API_PATH + "/1/100000");
};

export const getAssetStatusByPage = (pageIndex, pageSize) => {
  let params = pageIndex + "/" + pageSize;
  let url = API_PATH + "/" + params;
  return axios.get(url);
};
export const searchByText = (keyword, pageIndex, pageSize) => {
  let params = pageIndex + "/" + pageSize;
  return axios.post(API_PATH + "/searchByText/" + params, keyword);
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  let url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateAssetStatus = (AssetStatus) => {
  return axios.post(API_PATH, AssetStatus);
};

export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
