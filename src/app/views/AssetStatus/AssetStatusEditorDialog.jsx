import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { addNewOrUpdateAssetStatus, checkCode } from './AssetStatusService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {appConst, variable} from "app/appConst";
import {PaperComponent} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
});

class AssetStatusEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    indexOrder:0,
    shouldOpenNotificationPopup: false
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    let {t} = this.props;
    checkCode(id, code).then((result) => {
      if (result.data) {
        this.setState({shouldOpenNotificationPopup: true,
          Notification:"AssetStatus.noti.dupli_code"})
          toast.clearWaitingQueue();
      } else {
        if (id) {
          addNewOrUpdateAssetStatus({
            ...this.state
          }).then(() => {
            toast.info(t('general.updateSuccess'));
            this.props.handleOKEditClose();
          });
        } else {
          addNewOrUpdateAssetStatus({
            ...this.state
          }).then(() => {
            toast.info(t('general.addSuccess'));
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentDidMount() {
    let { open, handleClose, item } = this.props;
    this.setState({
      ...this.props.item
    });
  }

  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }

  render() {
    let {open, t} = this.props;
    let {
      id,
      name,
      indexOrder,
      code,
      shouldOpenNotificationPopup
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xs" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )} 
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t('AssetStatus.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
        <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  InputLabelProps={{ shrink: true }}
                  className="w-100"                  
                  label={<span><span className="colorRed">*</span>{t('AssetStatus.code')}</span>}                  
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code || ""}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>
            </Grid>
            <Grid className="" container spacing={2}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  InputLabelProps={{ shrink: true }}
                  className="w-100"
                  label={<span><span className="colorRed">*</span>{t('AssetStatus.name')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name || ""}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
                </Grid>
            </Grid>
            <Grid className="" container spacing={2}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t('AssetStatus.indexOrder')}
                  onChange={this.handleChange}
                  type="number"
                  name="indexOrder"
                  value={indexOrder || ""}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle mt-12">
            <Button
              className="mr-12"
              variant="contained"
              color="secondary"
              onClick={() => this.props.handleClose()}
            >
              {t('general.cancel')}
            </Button>
            <Button
              variant="contained"
              className="mr-15"
              color="primary"
              type="submit"
            >
              {t('general.save')}
            </Button>
            
          </div>
        </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AssetStatusEditorDialog;
