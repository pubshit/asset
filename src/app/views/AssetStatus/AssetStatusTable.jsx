import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { getAssetStatusByPage, getItemById, searchByText, deleteCheckItem } from "./AssetStatusService";
import AssetStatusEditorDialog from "./AssetStatusEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from 'react-i18next';
import { saveAs } from 'file-saver';
import { Helmet } from 'react-helmet';
import { withStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router-dom";
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ConstantList from "../../appConfig";
import {appConst, DEFAULT_TOOLTIPS_PROPS} from "app/appConst";
import {defaultPaginationProps, getTheHighestRole, getUserInformation, handleKeyDown} from "app/appFunction";
import {LightTooltip} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { hasDeletePermission } = props
  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} {...DEFAULT_TOOLTIPS_PROPS}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    {
      hasDeletePermission && <LightTooltip title={t('general.deleteIcon')} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">delete</Icon>
        </IconButton>
      </LightTooltip>
    }
  </div>;
}

class AssetStatusTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    AssetStatus: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    hasDeletePermission: false,
  };
  numSelected = 0;
  rowCount = 0;

  handleTextChange = event => {
    this.setState({ keyword: event.target.value })
  };

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  handleKeyUp = e => handleKeyUp(e, this.search);

  setPage = page => {
    this.setState({ page }, () => {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, () => {
      let searchObject = {};
      searchObject.keyword = this.state.keyword;
      searchByText(searchObject, this.state.page + 1, this.state.rowsPerPage).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      });
    });
  }

  updatePageData = () => {
    getAssetStatusByPage(this.state.page + 1, this.state.rowsPerPage).then(({ data }) => {
      this.setState({ itemList: [...data.content], totalElements: data.totalElements })
    }
    );
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false,

    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleDeleteAssetStatus = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditAssetStatus = item => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteCheckItem(this.state.id)
      .then((res) => {
        toast.info(this.props.t('general.deleteSuccess'));
        this.handleDialogClose()
        this.updatePageData()
      })
      .catch((err) => {
        console.error(err);
      })
  };

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser()
  }

  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleClick = (event, item) => {
    let { AssetStatus } = this.state;
    if (item.checked === null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < AssetStatus.length; i++) {
      if (AssetStatus[i].checked == null || AssetStatus[i].checked === false) {
        selectAllItem = false;
      }
      if (AssetStatus[i].id === item.id) {
        AssetStatus[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, AssetStatus: AssetStatus });

  };
  handleSelectAllClick = (event) => {
    let { AssetStatus } = this.state;
    for (let i = 0; i < AssetStatus.length; i++) {
      AssetStatus[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, AssetStatus: AssetStatus });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  getRoleCurrentUser = () => {
    let {isRoleOrgAdmin} = getTheHighestRole();
    
    this.setState({
      hasDeletePermission: isRoleOrgAdmin,
    });
  };

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenNotificationPopup,
      hasDeletePermission
    } = this.state;
    let TitlePage = t("AssetStatus.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        cellStyle: {
          padding: '0px',
          textAlign: "center",
        },
        render: rowData => <MaterialButton item={rowData} hasDeletePermission={hasDeletePermission}
          onSelect={(rowData, method) => {
            if (method === 0) {
              getItemById(rowData.id).then(({ data }) => {
                if (data === null) {
                  data = {};
                }
                this.setState({
                  item: data,
                  shouldOpenEditorDialog: true
                });
              })
            } else if (method === 1) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      {
        title: t("AssetStatus.code"), field: "code", minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetStatus.priority"), field: "indexOrder", align: "left", width: "150",
        cellStyle: {
          textAlign: "center",
        }
      },
      { title: t("AssetStatus.name"), field: "name", align: "left", width: "150" },

    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.category"), path: "/list/asset_status" },
            { name: TitlePage }]} />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} xs={12} >
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({ startDate: new Date(), endDate: new Date() });
              }}
            >
              {t('general.add')}
            </Button>
          </Grid>
          {shouldOpenNotificationPopup && (
            <NotificationPopup
              title={t('general.noti')}
              open={shouldOpenNotificationPopup}
              onYesClick={this.handleDialogClose}
              text={t(this.state.Notification)}
              agree={t('general.agree')}
            />
          )}


          <Grid item md={6} sm={12} xs={12} >
            <FormControl fullWidth>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("AssetStatus.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={() => this.search(keyword)} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <AssetStatusEditorDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default AssetStatusTable;
