import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetStatusTable = EgretLoadable({
  loader: () => import("./AssetStatusTable")
});
const ViewComponent = withTranslation()(AssetStatusTable);

const AssetStatusRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/asset_status",
    exact: true,
    component: ViewComponent
  }
];

export default AssetStatusRoutes;
