import React, { memo } from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import PropTypes from "prop-types";
import {useTranslation} from "react-i18next";
const CustomMaterialTable = memo((props) => {
	const {t} = useTranslation();
	const {
		title,
		data = [],
		columns = [],
		parentChildData,
		localization,
		options,
		onSelectionChange,
		columnActions = [],
		isLoading = false,
		onRowClick = (event = React.MouseEvent, rowData = {}, toggleDetailPanel) => {},
		selectionProps = (rowData) => ({}),
		...rest
	} = props;
	const defaultLocalization = {
		body: {
			emptyDataSourceMessage: "Không có dữ liệu",
		},
		toolbar: {
			nRowsSelected: "{0} hàng được chọn",
		},
	};
	const defaultOptions = {
		selection: false,
		actionsColumnIndex: -1,
		paging: false,
		search: false,
		draggable: false,
		rowStyle: (rowData) => ({
			backgroundColor: rowData?.id === props?.selectedItem?.id && props?.selectedItem?.id
				? "#ccc"
				: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
		}),
		headerStyle: {
			color: "#fff",
			border: "1px solid #E4E7EC",
		},
		selectionProps,
		padding: "dense",
		toolbar: false,
		sorting: false,
	};
	const mergedLocalization = { ...defaultLocalization, ...localization };
	const mergedOptions = { ...defaultOptions, ...options };
	const mergedColumns = [...columnActions, ...columns]
	
	const handleSelectionChange = (selectedRows, rowData) => {
		if (typeof onSelectionChange === "function") {
			onSelectionChange(selectedRows, rowData);
		}
	};
	
	return (
		<MaterialTable
			title={t?.(title)}
			data={[...(data || [])]}
			columns={mergedColumns}
			parentChildData={parentChildData}
			localization={mergedLocalization}
			options={mergedOptions}
			components={{
				Toolbar: (props) => <MTableToolbar {...props} />,
			}}
			onSelectionChange={handleSelectionChange}
			isLoading={isLoading}
			onRowClick={onRowClick}
			{...rest}
		/>
	);
});
export default CustomMaterialTable;
CustomMaterialTable.propTypes = {
	data: PropTypes.array,
	columns: PropTypes.array,
	title: PropTypes.string,
	components: PropTypes.object,
	onRowClick: PropTypes.func,
	onSelectionChange: PropTypes.func,
	options: PropTypes.shape({
		selection: PropTypes.bool,
		actionsColumnIndex: PropTypes.number,
		paging: PropTypes.bool,
		search: PropTypes.bool,
		draggable: PropTypes.bool,
		rowStyle: PropTypes.oneOfType([
      PropTypes.shape({
        backgroundColor: PropTypes.string,
        color: PropTypes.string,
        border: PropTypes.string,
        paddingLeft: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        paddingRight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        textAlign: PropTypes.string,
      }),
      PropTypes.func,
    ]),
		headerStyle: PropTypes.shape({
			backgroundColor: PropTypes.string,
			color: PropTypes.string,
			border: PropTypes.string,
			paddingLeft: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			paddingRight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			textAlign: PropTypes.string,
		}),
		selectionProps: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
		padding: PropTypes.string,
		toolbar: PropTypes.bool,
		sorting: PropTypes.bool,
		showSelectAllCheckbox: PropTypes.bool,
		headerSelectionProps: PropTypes.object,
		pageSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		pageSizeOptions: PropTypes.array,
	})
}
