import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const InventoryTable = EgretLoadable({
    loader: () => import("./InventoryTable")
});
const ViewComponent = withTranslation()(InventoryTable);

const InventoryRoutes = [
    {
        path: ConstantList.ROOT_PATH + "warehouse_inventory",
        exact: true,
        component: ViewComponent
    }
];

export default InventoryRoutes;