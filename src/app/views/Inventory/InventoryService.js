import axios from "axios";
import ConstantList from "../../appConfig";
import {removeFalsy} from "../../appFunction";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/stores" + ConstantList.URL_PREFIX;
const API_EXCEL_REPORT = ConstantList.API_ENPOINT_ASSET_MAINTANE;
const API_PATH_TON_KHO = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/ton-kho";

export const getWarehouse = (searchObject) => {
  return axios.post(API_PATH + "/searchByPage", searchObject);
};

export const getInventoryNew = (searchObject) => {
  const config = {
    params: { ...searchObject },
  };
  return axios.get(
    API_PATH_TON_KHO,
    config
  );
};

export const getInventorySummary = (searchObject) => {
  let config = {
    params: removeFalsy(searchObject),
  };
  return axios.get(API_PATH_TON_KHO + "/summary", config);
};

export const exportToExcelReport = (searchObject) => {
  const config = {
    params: { ...searchObject },
    responseType: "blob",
  };
  let url = API_EXCEL_REPORT + searchObject?.excelUrl;
  return axios(url, config);
};
