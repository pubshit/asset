import {TableCell, TableRow} from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";
import {convertMoney} from "app/appFunction";

function SummaryInformation(props) {
  const {moneySeparation, reportSummary, showUnitPrice, showDetail} = props;
  
  return (
    <TableRow
      className="position-sticky left-0"
      style={{
        zIndex: 2,
        backgroundColor: "rgb(228 240 246)",
        bottom: -1,
      }}
    >
      <TableCell align="left" colSpan={showDetail ? 15 : showUnitPrice ? 5 : 4}>
        <b>Tổng</b> (Tất cả sản phẩm)
      </TableCell>
      <TableCell align="center" className="font-weight-bold" colSpan={1}>
        {convertMoney(reportSummary?.soLuongDauKy, moneySeparation) || 0}
      </TableCell>
      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
        {convertMoney(reportSummary?.thanhTienDauKy, moneySeparation) || 0}
      </TableCell>
      <TableCell align="center" className="font-weight-bold" colSpan={1}>
        {convertMoney(reportSummary?.soLuongNhapTrongKy, moneySeparation) || 0}
      </TableCell>
      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
        {convertMoney(reportSummary?.thanhTienNhapTrongKy, moneySeparation) || 0}
      </TableCell>
      <TableCell align="center" className="font-weight-bold" colSpan={1}>
        {convertMoney(reportSummary?.soLuongXuatTrongKy, moneySeparation) || 0}
      </TableCell>
      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
        {convertMoney(reportSummary?.thanhTienXuatTrongKy, moneySeparation) || 0}
      </TableCell>
      <TableCell align="center" className="font-weight-bold" colSpan={1}>
        {convertMoney(reportSummary?.soLuongCuoiKy, moneySeparation) || 0}
      </TableCell>
      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
        {convertMoney(reportSummary?.thanhTienCuoiKy, moneySeparation) || 0}
      </TableCell>
    </TableRow>
  )
}
export default SummaryInformation;
SummaryInformation.propTypes = {
  moneySeparation: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  reportSummary: PropTypes.object,
  showUnitPrice: PropTypes.bool,
  showDetail: PropTypes.bool,
}