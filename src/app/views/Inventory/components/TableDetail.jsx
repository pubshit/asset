import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import React from 'react'
import {formatTimestampToDate, showTextWithSeparator} from "../../../appFunction";
import SummaryInformation from "./SummaryInformation";

function TableDetail(props) {
  let {
    t,
    itemList = [],
    reportSummary = {},
    convertMoney,
    moneySeparation,
    page,
    rowsPerPage,
  } = props;

    return (
        <Table stickyHeader aria-label="sticky table">
            <TableHead className="position-sticky top-0 left-0" style={{ zIndex: 2 }}>
                <TableRow>
                    <TableCell
                        align="center"
                        rowSpan="2"
                        style={{ width: "30px" }}
                    >
                        STT
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-180px">
                        {t("WarehouseInventory.bidsPackage")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-250px">
                        {t("WarehouseInventory.supplier")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-180px">
                        {t("WarehouseInventory.decisionInfo")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-180px">
                        {t("WarehouseInventory.contractInfo")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-120px">
                        {t("WarehouseInventory.codeAllType")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-250px">
                        {t("WarehouseInventory.nameAllType")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-50px">
                        {t("WarehouseInventory.sku")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-120px">
                        {t("WarehouseInventory.unitPrice")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-150px">
                        {t("WarehouseInventory.sign")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-120px">
                        {t("WarehouseInventory.model")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-120px">
                        {t("WarehouseInventory.seri")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-150px">
                        {t("WarehouseInventory.manufacture")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-50px">
                        {t("Asset.yearOfManufactureTable")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2" className="min-w-250px">
                        {t("WarehouseInventory.specifications")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.remainingQuantityFromDate")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.duringPeriod")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.exportPeriod")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.remainingQuantityToDate")}
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {itemList?.length > 0 && itemList?.map((item, index) => (
                    <TableRow key={index}>
                      <TableCell align="center">{page * rowsPerPage + (index + 1)}</TableCell>
                      <TableCell align="center">
                        {item.biddingPackage}{showTextWithSeparator(item.bidName, " | ")}
                      </TableCell>
                      <TableCell align="left">{item.supplierName}</TableCell>
                      <TableCell align="center">
                        {item.decisionCode}{showTextWithSeparator(formatTimestampToDate(item.decisionDate), " | ")}
                      </TableCell>
                      <TableCell align="center">
                        {item.contractCode}{showTextWithSeparator(formatTimestampToDate(item.contractDate), " | ")}
                      </TableCell>
                      <TableCell align="center">{item?.productCode}</TableCell>
                      <TableCell align="left">{item?.productName}</TableCell>
                      <TableCell align="center">{item?.skuName}</TableCell>
                      <TableCell align="right">{convertMoney(item.unitPrice, moneySeparation)}</TableCell>
                      <TableCell align="center">{item.symbolCode}</TableCell>
                      <TableCell align="center">{item.model}</TableCell>
                      <TableCell align="center">{item.serialNumber}</TableCell>
                      <TableCell align="center">
                        {item.manufacturerName}{showTextWithSeparator(item.madeIn, " | ")}
                      </TableCell>
                      <TableCell align="center">{item.yearOfManufacture}</TableCell>
                      <TableCell align="center">{item.specifications}</TableCell>
                      <TableCell align="center">
                        {convertMoney(item?.soLuongDauKy) || 0}
                      </TableCell>
                      <TableCell align="left" className="text-align-right">
                        {convertMoney(item?.thanhTienDauKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell align="center">
                        {convertMoney(item?.soLuongNhapTrongKy) || 0}
                      </TableCell>
                      <TableCell align="left" className="text-align-right">
                        {convertMoney(item?.thanhTienNhapTrongKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell align="center">
                        {convertMoney(item?.soLuongXuatTrongKy) || 0}
                      </TableCell>
                      <TableCell align="left" className="text-align-right">
                      </TableCell>
                      <TableCell align="center">
                        {convertMoney(item?.soLuongCuoiKy) || 0}
                      </TableCell>
                      <TableCell align="left" className="text-align-right">
                        {convertMoney(item?.thanhTienCuoiKy, moneySeparation) || 0}
                      </TableCell>
                    </TableRow>
                ))}
              
              <SummaryInformation
                reportSummary={reportSummary}
                moneySeparation={moneySeparation}
                showUnitPrice={false}
                showDetail
              />
            </TableBody>
        </Table>
    )
}

export default TableDetail;

