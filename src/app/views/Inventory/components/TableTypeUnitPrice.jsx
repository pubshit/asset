import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import React from 'react'
import SummaryInformation from "./SummaryInformation";

function TableTypeUnitPrice(props) {
    let {
        t,
        style,
        itemList = [],
        convertMoney,
        moneySeparation,
        page,
        rowsPerPage,
      reportSummary,
    } = props;

    return (
        <Table stickyHeader aria-label="sticky table">
            <TableHead className="position-sticky top-0 left-0" style={{ zIndex: 2 }}>
                <TableRow>
                    <TableCell
                        align="center"
                        rowSpan="2"
                        style={{ width: "30px" }}
                    >
                        STT
                    </TableCell>
                    <TableCell align="center" rowSpan="2" style={{ width: 120 }}>
                        {t("WarehouseInventory.codeAllType")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2">
                        {t("WarehouseInventory.nameAllType")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2">
                        {t("WarehouseInventory.sku")}
                    </TableCell>
                    <TableCell align="center" rowSpan="2">
                        {t("WarehouseInventory.unitPrice")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.remainingQuantityFromDate")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.duringPeriod")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.exportPeriod")}
                    </TableCell>
                    <TableCell align="center" colSpan="2">
                        {t("WarehouseInventory.remainingQuantityToDate")}
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell align="center" style={style.headTable}>
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center" style={style.headTable}>
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center" style={style.headTable}>
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                    <TableCell align="center" style={style.headTable}>
                        {t("WarehouseInventory.quantity")}
                    </TableCell>
                    <TableCell align="center">
                        {t("WarehouseInventory.total")}
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {itemList?.length > 0 && itemList?.map((item, index) => (
                    <TableRow key={index}>
                        <TableCell align="center">
                            {page * rowsPerPage + (index + 1)}
                        </TableCell>
                        <TableCell align="center">
                            {item?.productCode}
                        </TableCell>
                        <TableCell align="left">
                            {item?.productName}
                        </TableCell>
                        <TableCell align="center">
                            {item?.skuName}
                        </TableCell>
                        <TableCell className="text-align-right">
                            {convertMoney(item?.unitPrice) || 0}
                        </TableCell>
                        <TableCell align="center">
                            {convertMoney(item?.soLuongDauKy) || 0}
                        </TableCell>
                        <TableCell className="text-align-right">
                            {convertMoney(item?.thanhTienDauKy) || 0}
                        </TableCell>
                        <TableCell align="center">
                            {convertMoney(item?.soLuongNhapTrongKy) || 0}
                        </TableCell>
                        <TableCell className="text-align-right">
                            {convertMoney(item?.thanhTienNhapTrongKy) || 0}
                        </TableCell>
                        <TableCell align="center">
                            {convertMoney(item?.soLuongXuatTrongKy) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                            {convertMoney(item?.thanhTienXuatTrongKy) || 0}
                        </TableCell>
                        <TableCell align="center">
                            {convertMoney(item?.soLuongCuoiKy) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                            {convertMoney(item?.thanhTienCuoiKy) || 0}
                        </TableCell>
                    </TableRow>
                ))}
              
              <SummaryInformation
                reportSummary={reportSummary}
                moneySeparation={moneySeparation}
                showUnitPrice
              />
            </TableBody>
        </Table>
    )
}

export default TableTypeUnitPrice
