import React, { useContext, useEffect, useMemo, useState } from "react";
import { Helmet } from "react-helmet";
import { Breadcrumb } from "egret";
import { ValidatorForm } from "react-material-ui-form-validator";
import DateFnsUtils from "@date-io/date-fns";
import {
    Button, Card,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TableContainer,
    TablePagination,
} from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import SearchIcon from "@material-ui/icons/Search";
import Title from "../FormCustom/Component/Title";
import { exportToExcelReport, getInventoryNew, getWarehouse, getInventorySummary } from "./InventoryService";
import moment from "moment";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  checkMinDate,
  convertFromToDate, convertMoney,
  functionExportToExcel, getTheHighestRole, handleThrowResponseMessage, isSuccessfulResponse,
} from "app/appFunction";
import AppContext from "app/appContext";
import {STATUS_STORE, appConst, variable} from "app/appConst";
import viLocale from "date-fns/locale/vi";
import { isValidDate } from "app/appFunction"
import Collapse from "@material-ui/core/Collapse";
import CardContent from "@material-ui/core/CardContent";
import { getMedicalEquipment } from "../Asset/AssetService";
import { searchByPage as assetGroupSearchByPage } from "../AssetGroup/AssetGroupService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { TableTypeUnitPrice, TableTypeCode } from "./components";
import { defaultPaginationProps, handleGetTemplatesByModel, handleKeyUp } from "../../appFunction";
import { PRINT_TEMPLATE_MODEL } from "../../appConst";
import TableDetail from "./components/TableDetail";

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});

const InventoryTable = (props) => {
  const {t} = props;
  const {isRoleAssetManager, departmentUser} = getTheHighestRole();
  const [keyword, setKeyword] = useState("");
  const [itemList, setItemList] = useState([]);
  const {setPageLoading} = useContext(AppContext)
  const [date, setDate] = useState({
    toDate: moment(new Date()).format("YYYY-MM-DD") + "T23:59:59.999Z",
    fromDate: moment(new Date()).subtract(1, "months").format("YYYY-MM-DD") + "T00:00:00.000Z",
  });
  const [store, setStore] = useState(null);
  const [reloadData, setReloadData] = useState(false);
  const [isSearch, setIsSearch] = useState(true);
  const [listMedicalEquipment, setListMedicalEquipment] = useState([]);
  const [filterData, setFilterData] = useState({
    medicalEquipment: null,
    assetType: null,
    assetGroup: null,
  });
  const [reportType, setReportType] = useState(appConst.INVENTORY_TYPE.UNIT_PRICE);
  const [paging, setPaging] = useState({
    totalElements: 0,
    rowsPerPage: 10,
    page: 0
  });
  const [state, setState] = useState({})
  
  const isTypeSupplies = filterData?.assetType?.code === appConst.assetClass.VT;
  const TitlePage = t("WarehouseInventory.warehouse_inventory");
  const moneySeparation = appConst.TYPES_MONEY.COMMA.code;
  const searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isActive: STATUS_STORE.HOAT_DONG.code,
    managementDepartmentId: isRoleAssetManager ? departmentUser?.id : null,
  };
  const searchObjectAssetGroup = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    assetClass: filterData?.assetType?.code,
  };
  const ComponentByType = {
    [appConst.INVENTORY_TYPE.PRODUCT.code]: TableTypeCode,
    [appConst.INVENTORY_TYPE.UNIT_PRICE.code]: TableTypeUnitPrice,
    [appConst.INVENTORY_TYPE.DETAIL.code]: TableDetail,
  }
  const TableComponent = ComponentByType[reportType?.code] || TableTypeUnitPrice;
  
  const startOfTermQuantity = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.soLuongDauKy || 0), 0)
        , [itemList]
    );
    const startOfTermAmount = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.thanhTienDauKy || 0), 0)
        , [itemList]
    );
    const quantityOfImportedDuringTerm = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.soLuongNhapTrongKy || 0), 0)
        , [itemList]
    );
    const amountOfImportedDuringTerm = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.thanhTienNhapTrongKy || 0), 0)
        , [itemList]
    );
    const quantityOfExportedDuringTerm = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.soLuongXuatTrongKy || 0), 0)
        , [itemList]
    );
    const amountOfExportedDuringTerm = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.thanhTienXuatTrongKy || 0), 0)
        , [itemList]
    );
    const endOfTermQuantity = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.soLuongCuoiKy || 0), 0)
        , [itemList]
    );
    const endOfTermAmount = useMemo(
        () => itemList?.reduce((total, item) => total + Number(item.thanhTienCuoiKy || 0), 0)
        , [itemList]
    );

    const style = {
      headTable: {
        border: "1px solid black",
        width: "20px",
      },
    };

    const selectStore = (storeSelected) => {
        setStore(storeSelected);
    };

    const selectSumaryType = (type) => {
        setReportType(type);
        setPaging((pre) => ({ ...pre, page: 0 }))
    }

    const updatePageData = async () => {
        if (!reportType?.code) return;
        try {
            setPageLoading(true)
            const searchObject = {};
            searchObject.storeId = store?.id;
            searchObject.keyword = keyword.trim();
            searchObject.pageIndex = paging.page + 1;
            searchObject.pageSize = paging.rowsPerPage;
            searchObject.fromDate = moment(date.fromDate).format("YYYY-MM-DDT00:00:00");
            searchObject.toDate = moment(date.toDate).format("YYYY-MM-DDT23:59:59");
            searchObject.assetGroupId = filterData?.assetGroup?.id;
            searchObject.medicalEquipmentId = filterData?.medicalEquipment?.id;
            searchObject.assetClass = filterData?.assetType?.code;
            searchObject.departmentId = departmentUser?.id;
            searchObject.reportType = reportType?.code;

            let isAllowSearch = isValidDate(date?.fromDate)
                && isValidDate(date?.toDate)
                && checkMinDate(date?.fromDate, date?.toDate)

            if (isAllowSearch) {
                const res = await getInventoryNew(searchObject);
                if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
                    let data = res?.data?.data
                    setItemList(data?.content?.length > 0 ? data?.content : [])
                    setPaging((pre) => ({
                        ...pre,
                        totalElements: data?.totalElements,
                        pages: data?.totalPages
                    }))
                }
            }
        } catch (error) {
            console.error(t("general.error"));
        } finally {
            setPageLoading(false)
        }
    };

    const handleTextChange = (event) => {
        setKeyword(event.target.value);
    };
    const handleKeyDownEnterSearch = (e) => {
        if (e.key === appConst.KEY.ENTER) {
            setPaging((pre) => ({ ...pre, page: 0 }))
            updatePageData();
        }
    };

    const handleDateChange = (dateValue, name) => {
      if ([variable.listInputName.fromDate, variable.listInputName.toDate].includes(name)) {
        setDate((pre) => ({...pre, [name]: dateValue}))
        if (!isValidDate(dateValue) && dateValue) {
        } else if (paging.page !== 0) {
          handleChangePage(null, 0);
        } else {
          setReloadData(!reloadData);
        }
      }
    };
  
  useEffect(() => {
    if (paging.page !== 0) {
      handleChangePage(null, 0);
    } else {
      setReloadData(!reloadData);
    }
  }, [store, filterData, paging?.rowsPerPage, reportType?.code]);
  
  
  useEffect(() => {
    updatePageData();
  }, [paging.page, reloadData]);
  
  const handleSetData = (value, name) => {
      if (name === 'assetType') {
        setFilterData({
          ...filterData,
          [name]: value,
          assetGroup: null,
        })
        return;
      }
      
      setFilterData({
        ...filterData,
        [name]: value,
      })
    }

    const exportToExcel = async () => {
        if (!reportType?.code) return;
        try {
            setPageLoading(true)
            const searchObject = {};
            searchObject.storeId = store?.id;
            searchObject.keyword = keyword.trim();
            searchObject.pageIndex = appConst.OBJECT_SEARCH_MAX_SIZE.pageIndex;
            searchObject.pageSize = appConst.OBJECT_SEARCH_MAX_SIZE.pageSize;
            searchObject.fromDate = convertFromToDate(date.fromDate).fromDate;
            searchObject.toDate = convertFromToDate(date.toDate).toDate;
            searchObject.assetGroupId = filterData?.assetGroup?.id;
            searchObject.medicalEquipmentId = filterData?.medicalEquipment?.id;
            searchObject.assetClass = filterData?.assetType?.code;
            searchObject.departmentId = departmentUser?.id;
            searchObject.reportType = reportType?.code;
            let templateExport = state?.listDocumentTemplates?.find(item => item?.code === reportType?.templateCode);
            if (templateExport) {
                searchObject.templateId = templateExport?.id;
                searchObject.excelUrl = templateExport?.excelUrl;
            }
            let isAllowSearch = isValidDate(date?.fromDate)
                && isValidDate(date?.toDate)
                && checkMinDate(date?.fromDate, date?.toDate)

            if (isAllowSearch) {
                await functionExportToExcel(
                    exportToExcelReport,
                    searchObject,
                    templateExport?.name || t("exportToExcel.inventoryReport")
                );
            }
        } catch (error) {
            console.log(error)
            toast.error(t("general.error"));
        } finally {
            setPageLoading(false)
        }
    }

    let propsState = {
      t,
      style,
      itemList,
      startOfTermQuantity,
      startOfTermAmount,
      quantityOfImportedDuringTerm,
      amountOfImportedDuringTerm,
      quantityOfExportedDuringTerm,
      amountOfExportedDuringTerm,
      endOfTermQuantity,
      endOfTermAmount,
      convertMoney,
      moneySeparation,
      page: paging.page,
      rowsPerPage: paging.rowsPerPage,
      reportSummary: state.reportSummary,
    }

    const setRowsPerPage = event => {
        setPaging((pre) => ({ ...pre, rowsPerPage: event.target.value, page: 0 }))
    };

    const handleChangePage = (event, newPage) => {
      setPaging((pre) => ({ ...pre, page: newPage }));
    };

    const handleGetDocumentTemplates = async () => {
      try {
        setPageLoading(true);
        let data = await handleGetTemplatesByModel(PRINT_TEMPLATE_MODEL.SUPPLIES_MANAGEMENT.INVENTORY);
        if (data) {
          setState((pre) => ({...pre, listDocumentTemplates: data}))
        }
      } catch (e) {
        console.error(e)
      } finally {
        setPageLoading(false);
      }
    }
  
  useEffect(() => {
    handleGetDocumentTemplates();
  }, []);
  
  useEffect(() => {
    handleGetInventorySummary();
  }, [store?.id, date, filterData, keyword]);
    
    const handleGetInventorySummary = async () => {
      if (!isValidDate(date.fromDate) || !isValidDate(date.toDate)) {
        return;
      }
      try {
        setPageLoading(true);
        let searchObject = {};
        searchObject.storeId = store?.id;
        searchObject.keyword = keyword.trim();
        if (isValidDate(date.fromDate)) {
          searchObject.fromDate = convertFromToDate(date.fromDate).fromDate;
        }
        if (isValidDate(date.toDate)) {
          searchObject.toDate = convertFromToDate(date.toDate).toDate;
        }
        searchObject.assetGroupId = filterData?.assetGroup?.id;
        searchObject.medicalEquipmentId = filterData?.medicalEquipment?.id;
        searchObject.assetClass = filterData?.assetType?.code;
        searchObject.departmentId = departmentUser?.id;
        
        await getInventorySummary(searchObject).then((res) => {
          let {code, data} = res?.data;
          if (isSuccessfulResponse(code)) {
            setState(prevState => ({
              ...prevState,
              reportSummary: data,
            }));
          } else {
            handleThrowResponseMessage(res);
          }
        });
      } catch (error) {
        toast.error(t("InventoryReport.errorWhenGetSummary"))
      } finally {
        setPageLoading(false);
      }
    }
    
    return (
        <div className="m-sm-30">
            <Helmet>
                <title>
                    {TitlePage} | {t("web_site")}
                </title>
            </Helmet>
            <div className="mb-sm-30">
              <Breadcrumb
                routeSegments={[{name: t("Dashboard.warehouseManagement")}, {
                  name: TitlePage,
                  path: "warehouse_inventory",
                },]}
              />
            </div>
            <Grid container spacing={1}>
                <Grid item md={4} sm={12} xs={12}>
                    <Button
                        className="mt-12 mr-16"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            setIsSearch(!isSearch);
                        }}
                    >
                        {t("Tìm kiếm nâng cao")}
                        <ArrowDropDownIcon />
                    </Button>
                    <Button
                        className="mt-12 mr-16"
                        variant="contained"
                        color="primary"
                        onClick={exportToExcel}
                    >
                        {t("general.exportToExcel")}
                    </Button>
                </Grid>
                <Grid item container md={4} sm={12} xs={12} spacing={1}>
                    <Grid item md={6} sm={6} xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                            <KeyboardDatePicker
                                fullWidth
                                margin="none"
                                id="mui-pickers-date"
                                label={
                                    <span>
                                        <span style={{ color: "red" }}> </span>
                                        {t("WarehouseInventory.fromDate")}
                                    </span>
                                }
                                inputVariant="standard"
                                type="text"
                                autoOk={false}
                                format="dd/MM/yyyy"
                                name={"fromDate"}
                                value={date.fromDate}
                                maxDate={date.toDate}
                                invalidDateMessage={t("general.invalidDateFormat")}
                                minDateMessage={t("general.minDateDefault")}
                                maxDateMessage={date.toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                clearLabel={t("general.remove")}
                                cancelLabel={t("general.cancel")}
                                okLabel={t("general.select")}
                                onChange={(date) => handleDateChange(date, "fromDate")}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={6} sm={6} xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                            <KeyboardDatePicker
                                fullWidth
                                margin="none"
                                id="mui-pickers-date"
                                label={
                                    <span>
                                        <span style={{ color: "red" }}> </span>
                                        {t("WarehouseInventory.toDate")}
                                    </span>
                                }
                                inputVariant="standard"
                                type="text"
                                autoOk={false}
                                format="dd/MM/yyyy"
                                name={"toDate"}
                                value={date.toDate}
                                minDate={date.fromDate}
                                invalidDateMessage={t("general.invalidDateFormat")}
                                minDateMessage={date.fromDate ? t("general.minDateToDate") : t("general.minDateDefault")}
                                maxDateMessage={t("general.maxDateDefault")}
                                clearLabel={t("general.remove")}
                                cancelLabel={t("general.cancel")}
                                okLabel={t("general.select")}
                                onChange={(date) => handleDateChange(date, "toDate")}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                    <div style={{ marginTop: "16px" }}>
                        <FormControl fullWidth>
                            <Input
                                className="search_box w-100"
                                onChange={handleTextChange}
                                onKeyDown={handleKeyDownEnterSearch}
                                onKeyUp={(event) => handleKeyUp(event, updatePageData)}
                                placeholder={t("WarehouseInventory.filter")}
                                id="search_box"
                                startAdornment={
                                    <InputAdornment position="end">
                                        <SearchIcon
                                            onClick={(e) => {
                                              handleChangePage(e, 0);
                                            }}
                                            className="searchTable"
                                        />
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </div>
                </Grid>
            </Grid>

            <ValidatorForm onSubmit={() => { }}>
                <Collapse in={isSearch}>
                    <Card elevation={1} className="mt-30 mt-16">
                        <CardContent>
                            <Grid container spacing={2} alignItems="flex-end" >
                                <Grid item md={12} xs={12} className="text-center text-uppercase py-0">
                                    <b >Tìm kiếm nâng cao</b>
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocompleteSub
                                        label={t("general.filterAssetType")}
                                        searchFunction={() => { }}
                                        searchObject={{}}
                                        listData={appConst.assetType}
                                        setListData={() => { }}
                                        displayLable={"name"}
                                        isNoRenderChildren
                                        isNoRenderParent
                                        value={filterData?.assetType || null}
                                        name="assetType"
                                        onSelect={handleSetData}
                                        noOptionsText={t("general.noOption")}
                                    />
                                </Grid>
                                {!isTypeSupplies && (
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={
                                                filterData?.assetType?.code === appConst.assetClass.TSCD
                                                    ? t("general.filterAssetGroup")
                                                    : t("general.filterIatGroup")
                                            }
                                            searchFunction={assetGroupSearchByPage}
                                            searchObject={searchObjectAssetGroup}
                                            isNoRenderChildren
                                            isNoRenderParent
                                            displayLable={"name"}
                                            name="assetGroup"
                                            value={filterData?.assetGroup || null}
                                            onSelect={handleSetData}
                                            noOptionsText={t("general.noOption")}
                                            disabled={!filterData?.assetType?.code}
                                        />
                                    </Grid>
                                )}

                                {!isTypeSupplies && (
                                    <Grid item md={3} xs={12}>
                                        <AsynchronousAutocompleteSub
                                            label={t("MedicalEquipmentType.title")}
                                            searchFunction={getMedicalEquipment}
                                            searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                                            listData={listMedicalEquipment}
                                            setListData={setListMedicalEquipment}
                                            typeReturnFunction="category"
                                            displayLable={"name"}
                                            name="medicalEquipment"
                                            value={filterData?.medicalEquipment || null}
                                            onSelect={handleSetData}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                )}
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        label={
                                            <span>
                                                <span className="colorRed"></span>
                                                {t("WarehouseInventory.filterName")}
                                            </span>
                                        }
                                        searchFunction={getWarehouse}
                                        searchObject={searchObjectStore}
                                        defaultValue={store}
                                        displayLable={"name"}
                                        value={store}
                                        onSelect={selectStore}
                                    />
                                </Grid>
                                <Grid item md={4} sm={6} xs={12} lg={3}>
                                    <AsynchronousAutocompleteSub
                                        searchFunction={() => { }}
                                        listData={appConst.listInventoryType}
                                        searchObject={{}}
                                        label={t('InventoryReport.summaryType')}
                                        displayLable={"name"}
                                        name="reportType"
                                        onSelect={selectSumaryType}
                                        value={reportType}
                                        noOptionsText={t("general.noOption")}
                                    />
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Collapse>
            </ValidatorForm>
          
          <Grid item md={12} sm={12} xs={12} className="mt-12" style={{fontFamily: " 'Times New Roman', Times, serif"}}>
            <Title title={t("WarehouseInventory.title")}></Title>
            <div className="MuiPaper-root MuiPaper-elevation2 MuiPaper-rounded">
              <TableContainer style={{
                minHeight: 420,
                maxHeight: `calc(100vh - ${isSearch ? "600px" : "400px"})`,
              }}>
                <TableComponent {...propsState}/>
              </TableContainer>
            </div>
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={paging.totalElements}
              rowsPerPage={paging.rowsPerPage}
              page={paging.page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </div>
    );
};

export default InventoryTable;
