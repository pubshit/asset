import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  checkCode,
  addNewData,
  updateData,
  getAllProductTypes,
} from "./ProductService";
import * as productAttributeService from "../ProductAttribute/ProductAttributeService";
import * as stockKeepingUnitService from "../StockKeepingUnit/StockKeepingUnitService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import "../../../styles/views/_loadding.scss";
import ProductScrollableTabsButtonForce from "./ProductScrollableTabsButtonForce";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import CommonObjectDialog from "../CommonObject/CommonObjectDialog";
import AddNewProductCategoryPopup from "../Component/ProductCategory/AddNewProductCategoryPopup";
import AppContext from "app/appContext";
import { getTheHighestRole, handleThrowResponseMessage, isSuccessfulResponse, isTypeNumber } from "app/appFunction";
import { PaperComponent } from "../Component/Utilities";
import StockKeepingUnitEditorDialog from "../StockKeepingUnit/StockKeepingUnitEditorDialog";


toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
});

class ProductDialog extends Component {
  state = {
    name: "",
    code: "",
    description: "",
    productType: {},
    productTypeSelect: [],
    template: {},
    templateSelect: [],
    stockKeepingUnit: {},
    stockKeepingUnitSelect: [],
    attributes: [],
    valueBoolean: false,
    isActive: false,
    enableAddSKu: false,
    totalElements: 0,
    rowsPerPage: 10,
    page: 0,
    cost: 0,
    isDefault: false,
    skus: [],
    madeIn: "",
    yearOfManufacture: 0,
    model: "",
    warrantyExpiryDate: 0,
    manufacturer: {},
    serialNumber: "",
    warrantyMonth: 0,
    supplyUnit: null,
    isEditCode: false,
    productCategory: null,
    commonObject: null,
    shouldOpenNotificationPopup: false,
    notificationList: null,
    managementPurchaseDepartment: null,
    productTypeCode: appConst.productTypeCode.VTHH,
    loading: false,
    listProductCategory: [],
    listManufacturer: [],
    shouldOpenDialogManufacturer: false,
    shouldOpenAddNewSkuDialog: false,
    prodManageType: null,
    isSubmitting: false
  };

  handleChangeProductType = (typeProduct) => {
    let { SINGLE_MANAGEMENT, GROUP_MANAGEMENT } = appConst.PRODUCT_MANAGEMENT_TYPE;
    let managementTypeByProductType = {
      [appConst.productTypeCode.TSCĐ]: SINGLE_MANAGEMENT,
      [appConst.productTypeCode.VTHH]: GROUP_MANAGEMENT,
      [appConst.productTypeCode.CCDC]: null,
    }

    this.setState({
      productType: typeProduct,
      prodManageType: managementTypeByProductType[typeProduct?.code],
      productCategory: null
    });
  };

  handleChangeProductSku = (stockKeepingUnit) => {
    if (variable.listInputName.New === stockKeepingUnit?.code) {
      this.setState({ shouldOpenAddNewSkuDialog: true });
      return;
    }
    this.setState({ stockKeepingUnit });
  };

  handleAddSku = () => {
    let { skus, stockKeepingUnit } = this.state;
    let { t } = this.props;
    let existItem = skus?.find(sku => sku.sku.id === stockKeepingUnit?.id)
    let newArray = skus ? skus : []

    if (existItem) {
      toast.warning(t("general.existItem"))
    }
    else {
      let ps = {};
      ps.sku = stockKeepingUnit
      ps.isDefault = skus === null;
      ps.cost = 0;
      //eslint-disable-next-line
      newArray?.push(ps);
    }

    this.setState({
      skus: newArray,
      stockKeepingUnit: null,
    });
  };

  handleAddAttributes = () => {
    let { attributes, attribute } = this.state;
    let { t } = this.props;
    let existItem = attributes?.find(item => item.attribute?.id === attribute?.id)
    let newArray = attributes ? attributes : []

    if (existItem) {
      toast.warning(t("general.existItem"))
    }
    else {
      let item = {}
      item.attribute = attribute
      newArray?.push(item);
    }
    this.setState({
      attributes: newArray,
      attribute: null,
    });
  };

  handleDefaultChange = (rowData, checked) => {
    let { skus } = this.state;

    //check if lenght sku is < 1 then set toggle checkbox
    if (skus.length === 1) {
      if (skus[0].isDefault === false) {
        skus.forEach((el) => (el.isDefault = true));
      } else {
        skus.forEach((el) => (el.isDefault = false));
      }
      this.setState({ skus });
      return;
    }
    if (checked == true) {
      skus.forEach((el) => (el.isDefault = false));
      const finder = skus.find((el) => el.sku && el.sku.id === rowData.sku.id);
      if (finder) {
        finder.isDefault = true;
        this.setState({ skus });
      }
    }
  };
  deleteProductSkuDetail = (skusSelected) => {
    let index = null;
    let { skus } = this.state;
    if (skus != null && skus.length > 0) {
      skus.forEach((element) => {
        if (
          element != null &&
          element.sku.id != null &&
          skusSelected != null &&
          skusSelected.sku.id != null &&
          element.sku.id == skusSelected.sku.id
        ) {
          index = 0;
        } else {
          index++;
        }
        skus.splice(index, 1);
      });

      this.setState({ skus });
    }
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  selectManufacturer = (manufacturerSelected) => {
    this.setState({ manufacturer: manufacturerSelected }, function () { });
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  handleFormSubmit = async (e) => {
    const { setPageLoading } = this.context;
    if (e.target.id !== "IdProductDialog") return;
    let { t, indexLinhKien = 0, } = this.props;
    let { id, code, isSubmitting, name = "" } = this.state;

    try {
      setPageLoading(true);
      if (this.validateSubmit()) return;

      this.setState({ isSubmitting: true }); // Đánh dấu đang trong quá trình submit

      const sendData = this.convertData();
      await checkCode(id, code).then(async (result) => {
        //Nếu trả về true là code đã được sử dụng
        if (result?.data) {
          return toast.warning(t("general.duplicateCode"));
        }

        let res = id ? await updateData(sendData) : await addNewData(sendData);
        if (
          !isSuccessfulResponse(res?.status)
          || (isTypeNumber(res?.data?.code) && !isSuccessfulResponse(res?.data?.code))
        ) {
          return handleThrowResponseMessage(res);
        }

        if (!id) {
          toast.success(t("Product.noti.addSuccess"))
        } else {
          toast.success(t("Product.noti.updateSuccess"))
        }
        this.props.selectProduct?.(res?.data || {}, indexLinhKien || 0, variable.listInputName.product);
        this.props.handleOKEditClose(name);
      })
    } catch (e) {
      console.error(e);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
      this.setState({ isSubmitting: false }); // Đánh dấu đã hoàn thành quá trình submit
    }
  };

  componentDidUpdate(prevProps) {
    if (
      this.props?.type === "addFromRateIncident" &&
      this.props?.item !== prevProps.item
    ) {
      if (this.props.item?.id) {
        this.setState({
          ...this.props.item
        });
      }
      else {
        this.setState({
          code: this.props.item?.code,
        });
      }
    }
  }

  handleGetListUnit = () => {
    stockKeepingUnitService.getAllStockKeepingUnits().then((result) => {
      let stockKeepingUnitSelect = result?.data?.content;
      this.setState({ stockKeepingUnitSelect: stockKeepingUnitSelect });
    });
  };

  UNSAFE_componentWillMount() {
    let { assetClass, item, type } = this.props;
    const roles = getTheHighestRole();
    getAllProductTypes().then((result) => {
      let productTypeSelect = result?.data?.content;
      
      this.setState({
        productTypeSelect,
        productType: type === "addFromRateIncident"
          ? productTypeSelect?.find(item => item.code === appConst.productPurchaseTypeCode.VTHH)
          : item?.productType
      });
    });

    this.handleGetListUnit();

    productAttributeService.getAllProductAttributes().then((result) => {
      let propertySelect = result?.data?.content;
      this.setState({ propertySelect: propertySelect });
    });

    if (item?.id) {
      this.setState({
        ...item,
        ...roles,
        prodManageType: appConst.listProductManagementType.find(x => x.value === item.prodManageType),
      });
    } else {
      this.setState({
        ...item,
        roles,
        managementPurchaseDepartment: roles?.isRoleAssetManager && roles?.departmentUser?.id
          ? roles?.departmentUser
          : item?.managementPurchaseDepartment || null
      });
    }
  }
  handleSelectSupplyPopupClose = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: false,
    });
  };
  handleSelectSupply = (item) => {
    this.setState(
      { supplyUnit: { id: item.id, name: item.name } },
      function () {
        this.handleSelectSupplyPopupClose();
      }
    );
  };

  handleSelectProductTemplate = (template) => {
    this.setState({ template });
  };

  handleSelectProductCategory = (productCategory) => {
    if (variable.listInputName.New === productCategory?.code) {
      this.setState({ shouldOpenDialogProductCategory: true });
    }
    else {
      this.setState({ productCategory });
    }
  };

  handleSelectCommonObject = (manufacturer) => {
    if (variable.listInputName.New === manufacturer?.code) {
      this.setState({ shouldOpenDialogManufacturer: true });
    }
    else {
      this.setState({ manufacturer });
    }
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleConfficientChange = (rowData, event) => {
    let { skus } = this.state;
    const item = skus.find((el) => el.sku.id === rowData.sku.id);

    if (item != null) {
      item.coefficient = event.target.value / 1;
      this.setState({ skus });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleSelectManagementPurchaseDepartment = (item) => {
    this.setState({ managementPurchaseDepartment: item });
  };

  handleSetListProductCategory = (listProductCategory) => {
    this.setState({ listProductCategory });
  };

  handleClosePopup = () => {
    this.setState({
      shouldOpenDialogManufacturer: false,
      shouldOpenDialogProductCategory: false,
      shouldOpenDialogProductType: false,
      shouldOpenAddNewSkuDialog: false,
      keySearch: "",
    });
  };

  handleSetDataSelect = (data, source, sourceList) => {
    this.setState({
      [source]: data,
      [sourceList]: [],
    });
  };

  handleSetListData = (data, source) => {
    this.setState({
      [source]: data,
    });
  };

  convertData = () => {

    return {
      id: this.state.id,
      code: this.state.code,
      name: this.state.name,
      productType: this.state.productType,
      productCategory: this.state.productCategory,
      managementPurchaseDepartment: this.state.managementPurchaseDepartment,
      yearOfManufacture: this.state.yearOfManufacture,
      model: this.state.model,
      manufacturer: this.state.manufacturer,
      madeIn: this.state.madeIn,
      attributes: this.state.attributes,
      skus: this.state.skus,
      defaultSku: this.state.defaultSku,
      org: this.state.org,
      description: this.state.description,
      prodManageType: this.state.prodManageType?.value,
    }
  };

  validateSubmit = () => {
    let { t } = this.props;
    const isEmptyProductManageType = appConst.listProductManagementType.every(
      x => x.value !== this.state.prodManageType?.value
    );
    const isEmptyDefaultSku = this.state.skus?.every((el) => !el.isDefault);

    if (this.state.productCategory === null) {
      toast.warning("Chưa chọn danh mục sản phẩm");
      return true;
    }
    if (this.state.managementPurchaseDepartment === null || !this.state.managementPurchaseDepartment?.id) {
      toast.warning("Chưa chọn phòng ban mua sắm");
      return true;
    }
    if (this.state.productType === null) {
      toast.warning("Chưa chọn loại sản phẩm");
      return true;
    }
    if (this.state.skus === null || this.state.skus.length === 0) {
      toast.warning(t("Product.noti.emtpySkus"));
      return true;
    }
    if (isEmptyDefaultSku) {
      toast.warning(t("Product.noti.emtpySkuDefault"));
      return true;
    }
    if (
      this.state.productType?.code === appConst.productTypeCode.CCDC
      && isEmptyProductManageType
    ) {
      toast.warning(t("Product.noti.emptyManagementType"));
      return true;
    }

    return false;
  }

  handleDeleteAttributeInList = (rowData) => {
    let { attributes = [] } = this.state;
    if (attributes.length > 0) {
      attributes.splice(rowData?.tableData?.id, 1);

      this.setState({ attributes });
    }
  };

  render() {
    let {
      productTypeSelect,
      shouldOpenDialogManufacturer,
      shouldOpenDialogProductCategory,
      productType,
      keySearch,
      shouldOpenAddNewSkuDialog,
    } = this.state;
    let { open, t, i18n } = this.props;
    let assetClassByType = {
      [appConst.productTypeCode.TSCĐ]: appConst.assetClass.TSCD,
      [appConst.productTypeCode.CCDC]: appConst.assetClass.CCDC,
      [appConst.productTypeCode.VTHH]: appConst.assetClass.VT,
    };
    let productCategorySearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      assetClass: assetClassByType[productType?.code] || appConst.assetClass.TSCD,
    };

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth={true}
      >
        <ValidatorForm
          id="IdProductDialog"
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            {t("Product.productDialog")}
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={1} className="">
              <ProductScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleChange={this.handleChange}
                selectManufacturer={this.selectManufacturer}
                productTypeSelect={productTypeSelect}
                handleChangeProductType={this.handleChangeProductType}
                handleDateChange={this.handleDateChange}
                handleSelectProductCategory={this.handleSelectProductCategory}
                handleSelectProductTemplate={this.handleSelectProductTemplate}
                handleSelectManagementPurchaseDepartment={
                  this.handleSelectManagementPurchaseDepartment
                }
                handleSelectCommonObjectPopupClose={
                  this.handleSelectCommonObjectPopupClose
                }
                handleSelectCommonObject={this.handleSelectCommonObject}
                handleSetDataSelect={this.handleSetDataSelect}
                handleChangeProductSku={this.handleChangeProductSku}
                handleAddSku={this.handleAddSku}
                handleAddAttributes={this.handleAddAttributes}
                deleteProductSkuDetail={this.deleteProductSkuDetail}
                handleDeleteAttributeInList={this.handleDeleteAttributeInList}
                handleDefaultChange={this.handleDefaultChange}
                handleConfficientChange={this.handleConfficientChange}
                type={this.props?.type}
                handleSetListProductCategory={this.handleSetListProductCategory}
                handleSetListData={this.handleSetListData}
                productCategorySearchObject={productCategorySearchObject}
                assetClass={this.props?.assetClass}
                isFromPurchaseRequest={this.props?.isFromPurchaseRequest}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-1">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                style={{ marginRight: "32px" }}
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
        {shouldOpenDialogManufacturer && (
          <CommonObjectDialog
            t={t}
            i18n={i18n}
            item={{
              name: keySearch,
            }}
            handleClose={this.handleClosePopup}
            handleOKEditClose={this.handleClosePopup}
            typeCode={appConst.listCommonObject.HANG_SAN_XUAT.code} //code hãng sản xuất
            open={shouldOpenDialogManufacturer}
            selectManufacturer={(data) => {
              this.handleSetDataSelect(
                data,
                "manufacturer",
                "listManufacturer",
              )
            }}
          />
        )}
        {shouldOpenDialogProductCategory && (
          <AddNewProductCategoryPopup
            t={t}
            i18n={i18n}
            item={{
              name: keySearch,
              productType: productType
            }}
            handleClose={this.handleClosePopup}
            handleOKEditClose={this.handleClosePopup}
            open={shouldOpenDialogProductCategory}
            selectProductCategory={(data) => {
              this.handleSetDataSelect(
                data,
                "productCategory",
                "listProductCategory",
              )
            }}
          />
        )}
        {shouldOpenAddNewSkuDialog && (
          <StockKeepingUnitEditorDialog
            t={t}
            i18n={i18n}
            keySearch={keySearch}
            handleClose={this.handleClosePopup}
            handleOKEditClose={this.handleClosePopup}
            open={shouldOpenAddNewSkuDialog}
            selectUnit={(data) => {
              this.handleSetDataSelect(data, "stockKeepingUnit");
              this.handleGetListUnit();
            }}
          />
        )}
      </Dialog>
    );
  }
}

ProductDialog.contextType = AppContext;
export default ProductDialog;