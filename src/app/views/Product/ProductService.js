import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/product" + ConstantList.URL_PREFIX;
const API_PATH_VOUCHER = ConstantList.API_ENPOINT + "/api/voucher";
const API_PATH_XUAT_KHO =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/inventory";
const API_PATH_PRODUCT =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/products";

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const searchProductInVoucherByPage = (searchObject) => {
  var url = API_PATH_VOUCHER + "/searchProductInVoucherByPage";
  return axios.post(url, searchObject);
};
export const getListRemainingQuantity = (searchObject) => {
  const config = {
    params: {
      ...searchObject,
    },
  };
  var url = API_PATH_XUAT_KHO + `/get-dsvt-tonkho/${searchObject.storeId}`;
  return axios.get(url, config);
};

export const getRemainProduct = (searchObject) => {
  const config = {
    params: {
      keyword: searchObject?.keyword,
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      date: searchObject?.date,
      productIds: searchObject?.productId,
    },
  };
  var url = API_PATH_XUAT_KHO + `/vat-tu-ton-kho/${searchObject.storeId}`;
  return axios.get(url, config);
};

export const getAllProductTypes = () => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/producttype/1/100000";
  return axios.get(API_PATH);
};

export const getAllTemplates = () => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/product_template/1/100000";
  return axios.get(API_PATH);
};
export const getByPage = (page, pageSize) => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/product";
  var pageIndex = page + 1;
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH + params;
  return axios.get(url);
};

export const getItemById = (id) => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/product";
  var url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/product";
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const checkCode = (id, code) => {
  // var API_PATH = ConstantList.API_ENPOINT + "/org/api/product";
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const addNewData = (product) => {
  return axios.post(API_PATH, product);
};

export const updateData = (product) => {
  return axios.put(API_PATH + "/" + product.id, product);
};
export const getNewCode = () => {
  return axios.get(API_PATH + "/" + "addNew/getNewCode");
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};

export const getRemainProductIat = (searchObject) => {
  const config = {
    params: { ...searchObject },
  };
  var url = API_PATH_XUAT_KHO + `/ton-kho/vat-tu`;
  return axios.get(url, config);
};

export const getListSkuByProduct = (productId) => {
  let url = `${API_PATH_PRODUCT}/${productId}/skus`;
  return axios(url);
};

export const getListsProduct = (searchObject) => {
  const config = {
    params: { ...searchObject },
  };
  let url = API_PATH_PRODUCT + "/search-by-page";
  return axios.get(url, config);
};

export const getListsProductAsset = (searchObject) => {
  const config = {
    params: { ...searchObject },
  };
  let url = API_PATH_PRODUCT + "/bids/assets";
  return axios.get(url, config);
};

export const getListsProductSupplies = (searchObject) => {
  const config = {
    params: { ...searchObject },
  };
  let url = API_PATH_PRODUCT + "/bids/supplies";
  return axios.get(url, config);
};
