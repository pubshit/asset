import {
  Button,
  Checkbox,
  Grid,
  Icon,
  IconButton,
  Radio,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import MaterialTable, {
  MTableToolbar
} from "material-table";
import React, { useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { searchByPage as productTypeSearchByPage } from "../ProductType/ProductTypeService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListManagementDepartment } from "../Asset/AssetService";
import { a11yProps, filterOptions, getRoleCategory, handleKeyDownIntegerOnly } from "app/appFunction";
import { searchByTextHSX } from "../CommonObject/CommonObjectService";
import { appConst } from "app/appConst";
import { useEffect } from "react";
import {
  getByRootForDropList,
} from "../ProductCategory/ProductCategoryService";
import NumberFormatCustom from "../Component/Utilities/NumberFormatCustom";
import { Label, LightTooltip } from "../Component/Utilities";
import { TabPanel } from "../Component/Utilities";
import { searchByPage } from "../ProductAttribute/ProductAttributeService";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ProductScrollableTabsButtonForce(props) {
  const { t, i18n, item } = props;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [listProductType, setListProductType] = useState([]);
  const [listManagementDepartment, setListManagementDepartment] = useState([]);

  const searchObject = props.type === "addFromRateIncident"
    || props.type === appConst.productPurchaseTypeCode.VTHH
    ? { keyword: appConst.productTypeCode.VTHH, ...appConst.OBJECT_SEARCH_MAX_SIZE }
    : { ...appConst.OBJECT_SEARCH_MAX_SIZE };
  const manufacturerSearchObject = {
    pageIndex: 1,
    pageSize: 10000,
    keyword: "",
  }
  const isIat = props?.type === appConst.productTypeCode.CCDC
  const isRequiredManagementType = appConst.productTypeCode.CCDC === item?.productType?.code
  const isDisabledManagementType = !isRequiredManagementType && item?.productType?.code;

  useEffect(() => {
    let listProductTypeFiltered = [...listProductType]
    if (
      props?.type === appConst.productPurchaseTypeCode.TSCD_CCDC
      || props?.type === appConst.productTypeCode.CCDC
    ) {
      listProductTypeFiltered = listProductType.filter(item => item.code !== appConst.productPurchaseTypeCode.VTHH)
    }
    setListProductType(listProductTypeFiltered)
  }, [listProductType.length])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let columnsAttribute = [
    {
      title: t("general.action"),
      align: "center",
      width: "120px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.handleDeleteAttributeInList(rowData)}
          >
            <Icon fontSize="small" color="error">delete</Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
    {
      title: t("general.stt"),
      field: "code",
      width: "50px",
      align: "center",
      cellStyle: {
        paddingLeft: "10px",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("ProductAttribute.code"),
      field: "attribute.code",
      align: "left",
      width: "150",
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("ProductAttribute.name"),
      field: "attribute.name",
      align: "left",
      width: "150",
    },
  ];
  let columnSkus = [
    {
      title: t("general.action"),
      align: "center",
      width: "120px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="top"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.deleteProductSkuDetail(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    },
    { title: t("Sku.code"), field: "sku.code", width: "150" },
    { title: t("Sku.name"), field: "sku.name", align: "left", width: "150" },
    {
      title: t("Sku.coefficient"),
      align: "left",
      width: "150",
      render: (rowData) => (
        <TextValidator
          className="w-100"
          onChange={(event) => props.handleConfficientChange(rowData, event)}
          name="coefficient"
          id="formatted-numberformat-originalCost"
          value={rowData.coefficient}
          InputProps={{
            inputComponent: NumberFormatCustom,
          }}
        />
      ),
    },
    {
      title: t("Sku.isDefault"),
      field: "sku.isDefault",
      align: "center",
      width: "150",
      render: (rowData) => (
        <Radio
          name={rowData.tableData.id}
          onClick={(event) => {
            props.handleDefaultChange(rowData, event.target.checked);
          }}
          checked={rowData.isDefault}
        />
      ),
    },
  ];
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          // variant="fullWidth"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("Product.general_information")} {...a11yProps(0)} />
          <Tab label={t("Product.properties")} {...a11yProps(1)} />
          <Tab label={t("Product.dialog.stockKeepingUnit")} {...a11yProps(2)} />
        </Tabs>
      </AppBar>

      <TabPanel value={value} index={0} style={{ height: "350px" }}>
        <Grid container spacing={1} >
          {/* Dòng 1: Mã, tên */}
          <Grid item md={6} sm={12} xs={12}>
            <div>
              <label className="font-weight-bold">
                {t("Product.code")}:{" "}
              </label>{" "}
              {props.item.code}
            </div>
            {props.item.isEditCode && (
              <TextValidator
                className="w-100"
                onChange={props.handleChange}
                type="text"
                name="code"
                value={props.item.code}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            )}
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>{" "}
                  <span> {t("Product.name")}</span>
                </span>
              }
              onChange={props.handleChange}
              type="text"
              name="name"
              value={props.item.name}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>

          {/* Dòng 2: DMSP, loại SP*/}
          <Grid item md={6} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>{" "}
                  <span> {t("Product.productType")}</span>
                </span>
              }
              listData={listProductType}
              setListData={setListProductType}
              searchFunction={productTypeSearchByPage}
              searchObject={searchObject}
              displayLable={"name"}
              readOnly={
                props?.assetClass === appConst.assetClass.TSCD
                || isIat
                || props.type === "addFromRateIncident"
              }
              value={props.item.productType}
              onSelect={props.handleChangeProductType}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>{" "}
                  <span> {t("Dashboard.subcategory.productCategory")}</span>
                </span>
              }
              searchObject={props.productCategorySearchObject}
              searchFunction={getByRootForDropList}
              onInputChange={(event) => props.handleSetDataSelect(event.target.value, "keySearch")}
              // listData={props?.item?.listProductCategory}
              // setListData={(data) => props.handleSetListData(
              //   data,
              //   "listProductCategory"
              // )}
              value={props?.item?.productCategory ?? null}
              displayLable={'name'}
              onSelect={props.handleSelectProductCategory}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "name")
              }
              disabled={!props.item.productType?.code}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>

          {/* Dòng 3: PB mua sắm, loại QL */}
          <Grid item md={6} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>{" "}
                  <span> {t("Product.managementPurchaseDepartment")}</span>
                </span>
              }
              listData={listManagementDepartment}
              setListData={setListManagementDepartment}
              searchFunction={getListManagementDepartment}
              searchObject={{}}
              defaultValue={props.item.managementPurchaseDepartment ?? null}
              displayLable={"name"}
              typeReturnFunction="list"
              isNoRenderChildren
              value={props.item.managementPurchaseDepartment || null}
              onSelect={props.handleSelectManagementPurchaseDepartment}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              disabled={getRoleCategory().isRoleAssetManager || (props?.isFromPurchaseRequest && props.item.managementPurchaseDepartment?.id)}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired={isRequiredManagementType}>
                  {t("Product.managementType")}
                </Label>
              }
              searchFunction={() => { }}
              searchObject={{}}
              listData={appConst.listProductManagementType}
              displayLable={"name"}
              name="prodManageType"
              value={props.item.prodManageType || null}
              onSelect={props.handleSetDataSelect}
              disabled={isDisabledManagementType}
              validators={isRequiredManagementType ? ["required"] : []}
              errorMessages={[t("general.required")]}
              noOptionsText={t("general.noOption")}
            />
          </Grid>

          {/* Dòng 4: NSX, model HSX, xuất xứ */}
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              className="w-100 h-5"
              label={t("Product.yearOfManufacture")}
              onKeyDown={handleKeyDownIntegerOnly}
              onChange={props.handleChange}
              type="number"
              name="yearOfManufacture"
              value={props?.item?.yearOfManufacture}
              validators={[
                "matchRegexp:^[1-9][0-9]{3}$",
                `minNumber: 1900`,
                `maxNumber: ${new Date().getFullYear()}`
              ]}
              errorMessages={[
                t("general.yearOfManufactureError"),
                t("general.minYearDefault"),
                t("general.maxYearNow")
              ]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              className="w-100 h-5"
              label={t("Product.model")}
              onChange={props.handleChange}
              type="text"
              name="model"
              value={props.item.model || ""}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("Asset.manufacturer")}
              searchFunction={searchByTextHSX}
              searchObject={manufacturerSearchObject}
              listData={props?.item?.listManufacturer}
              setListData={(data) => props.handleSetListData(
                data,
                "listManufacturer"
              )}
              typeReturnFunction="category"
              displayLable={"name"}
              value={props.item.manufacturer ?? null}
              onSelect={props.handleSelectCommonObject}
              onInputChange={(e) => props.handleSetDataSelect(e?.target?.value, "keySearch")}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "name")
              }
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              className="w-100 h-5"
              label={t("Product.madeIn")}
              onChange={props.handleChange}
              type="text"
              name="madeIn"
              value={props.item.madeIn || ""}
            />
          </Grid>

          {/* Dòng 5: mô tả*/}
          <Grid item md={12} sm={12} xs={12}>
            <TextValidator
              className="w-100 h-5"
              label={t("Product.description")}
              onChange={props.handleChange}
              type="text"
              name="description"
              value={props.item.description || ""}
            />
          </Grid>
        </Grid>
      </TabPanel>

      <TabPanel
        value={value} index={1}
        style={{ height: "350px" }}
        className="oveflow-hidden"
      >
        <Grid container spacing={1}>
          {/* Mẫu sản phẩm */}
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("Product.attribute")}
              searchFunction={searchByPage}
              searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
              displayLable="name"
              name="attribute"
              value={props.item.attribute || null}
              onSelect={props.handleSetDataSelect}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <Button
              className="mt-12"
              size="small"
              spacing={0}
              variant="contained"
              color="primary"
              onClick={props.handleAddAttributes}
              disabled={!props.item?.attribute?.id}
            >
              {t("Sku.btnAdd")}
            </Button>
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={props.item?.attributes || []}
              columns={columnsAttribute}
              options={{
                sorting:false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel
        value={value} index={2}
        className="oveflow-hidden"
        style={{ height: "350px" }}
      >
        <Grid container spacing={1}>
          {/* Đơn vị tính - trường hợp loại sản phẩm là Vật Tư */}
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("Product.stockKeepingUnit")}
                </span>
              }
              searchFunction={() => { }}
              listData={props.item.stockKeepingUnitSelect}
              displayLable="name"
              filterOptions={(options, params) => filterOptions(options, params, true, "name")}
              value={props.item.stockKeepingUnit}
              onSelect={props.handleChangeProductSku}
              onInputChange={(e) => props.handleSetDataSelect(e?.target?.value, "keySearch")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <Button
              className="mt-12"
              size="small"
              spacing={0}
              variant="contained"
              color="primary"
              onClick={props.handleAddSku}
              disabled={!props.item?.stockKeepingUnit?.id}
            >
              {t("Sku.btnAdd")}
            </Button>
          </Grid>

          <Grid item md={12} sm={12} xs={12}>
            <MaterialTable
              fullWidth={true}
              title={t("Sku.list")}
              data={props.item.skus ? props.item.skus : []}
              columns={columnSkus}
              options={{
                sorting: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  // nRowsSelected: '${t('general.selects')}',
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
    </div>
  );
}
