import React, { Component } from "react";
import {
  IconButton,
  Grid,
  Icon,
  TablePagination,
  Button,
  InputAdornment,
  FormControl,
  Input,
  Collapse,
  Card
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  searchByPage,
  getItemById,
  getNewCode,
  deleteCheckItem,
} from "./ProductService";
import ProductDialog from "./ProductDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AppContext from "app/appContext";
import { ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { searchByPage as searchByPageProductCategory } from "../ProductCategory/ProductCategoryService";
import { getRoleCategory, handleKeyDown, handleKeyUp } from "app/appFunction";
import { searchByPage as productTypeSearchByPage } from "../ProductType/ProductTypeService";
import localStorageService from "app/services/localStorageService";
import { appConst } from "../../appConst";
import { getListManagementDepartment } from "../Asset/AssetService";
import { LightTooltip } from "../Component/Utilities";

toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
});


function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { isRoleAssetManager } = props
  return (
    <div className="none_wrap">
      <LightTooltip
        style={{ width: "5" }}
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "0px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      {
        !isRoleAssetManager &&
        <LightTooltip
          title={t("general.delete")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class Product extends Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    qualificationList: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    listProductCategory: [],
    productCategory: null,
    totalElements: 0,
    shouldOpenConfirmationDeleteListDialog: false,
    shouldOpenNotificationPopup: false,
    keyword: "",
    Notification: "",
    isRoleAssetManager: getRoleCategory().isRoleAssetManager,
    openAdvanceSearch: false,
    managementPurchaseDepartment: null,
    productType: null,
  };
  numSelected = 0;
  rowCount = 0;
  department = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  handleKeyUpSearch = (e) => handleKeyUp(e, this.search);

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  }

  updatePageData = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.productCategoryCode = this.state.productCategory?.code;
    searchObject.managementPurchaseDepartment = this.state.managementPurchaseDepartment;
    searchObject.productTypeCode = this.state.productType?.code;

    setPageLoading(true);
    searchByPage(searchObject)
      .then(({ data }) => {
        if (data) {
          this.setState({
            itemList: data?.content?.length > 0 ? [...data.content] : [],
            totalElements: data?.totalElements,
          });
        }
        setPageLoading(false);
      })
      .catch((err) => {
        toast.error(t("general.error"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenNotificationPopup: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  handleDeleteItem = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditItem = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props;
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count,
      });
    } else if (this.state.itemList.length === 1 && this.state.page === 1) {
      this.setState({
        page: 1,
      });
    }
    try {
      let res = await deleteCheckItem(this.state.id);
      if (res) {
        toast.success(t("Product.noti.deleteSuccess"));
        this.updatePageData();
      } else {
        toast.warn(t("Product.noti.use"));
      }
    } catch (error) {
    } finally {
      this.handleDialogClose();
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleAddItem = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result.data && result.data.code) {
          let item = result.data;
          if (this.state.isRoleAssetManager) {
            item.managementPurchaseDepartment = this.department
          }
          this.setState({
            item: item,
            shouldOpenEditorDialog: true,
          });
        }
      })
      .catch(() => {
        alert(this.props.t("general.error_reload_page"));
      });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { productList } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < productList.length; i++) {
      if (productList[i].checked == null || productList[i].checked === false) {
        selectAllItem = false;
      }
      if (productList[i].id === item.id) {
        productList[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, productList: productList });
  };
  handleSelectAllClick = (event) => {
    let { productList } = this.state;
    for (var i = 0; i < productList.length; i++) {
      productList[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      productList: productList,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props;
    for (var i = 0; i < list.length; i++) {
      try {
        const data = await deleteCheckItem(list[i].id);
        if (!data) {
          listAlert.push(list[i].name);
        }
      } catch (error) {
      }
    }
    this.handleDialogClose();
    if (listAlert.length === list.length) {
      toast.warning(t("Product.noti.use_all"));
    } else if (listAlert.length > 0) {
      toast.warning(t("Product.noti.deleted_unused"));
    } else if (listAlert.length <= 0) {
      toast.success(t("general.deleteSuccess"));
    }
  }

  handleDeleteAll = (event) => {
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.data = null;
      // this.handleDialogClose();
    });
  };

  handleSetListProductCategory = (listProductCategory) => {
    this.setState({ listProductCategory });
  };

  handleSelectProductCategory = (productCategory) => {
    this.setState({ productCategory }, () => {
      this.updatePageData();
    });
  };

  handleSelectManagementPurchaseDepartment = (managementPurchaseDepartment) => {
    this.setState({ managementPurchaseDepartment }, () => {
      this.updatePageData();
    });
  };
  handleChangeProductType = (productType) => {
    this.setState({ productType }, () => {
      this.updatePageData();
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteListDialog,
      shouldOpenNotificationPopup,
      listProductCategory,
      productCategory,
      isRoleAssetManager,
      openAdvanceSearch,
      managementPurchaseDepartment,
      productType
    } = this.state;
    let TitlePage = t("Product.title");
    let productCategorySearchObject = {
      pageIndex: 1,
      pageSize: 10000,
      // assetClass: appConst.assetClass.TSCD,
    };

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            isRoleAssetManager={isRoleAssetManager}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.id).then(({ data }) => {
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  });
                });
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("Product.code"),
        field: "code",
        minWidth: "140px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "name",
        align: "left",
        minWidth: "280px",
      },
      {
        title: t("Product.productCategory"),
        field: "productCategory.name",
        align: "left",
        minWidth: "250px",
      },
      {
        title: t("Product.productType"),
        field: "productType.name",
        align: "left",
        minWidth: "150px",
      },
      {
        title: t("Product.managementPurchaseDepartment"),
        field: "managementPurchaseDepartment.name",
        align: "left",
        minWidth: "200px",
      },
    ];

    return (
      <div className="m-sm-30">
        <ValidatorForm>
          <Helmet>
            <title>
              {TitlePage} | {t("web_site")}
            </title>
          </Helmet>
          <div className="mb-sm-30">
            <Breadcrumb
              routeSegments={[
                { name: t("Dashboard.category"), path: "/list/product" },
                { name: TitlePage },
              ]}
            />
          </div>

          <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={6} sm={12} xs={12}>
              <Button
                className="mt-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.handleAddItem}
              >
                {t("general.add")}
              </Button>
              <Button
                className="mt-16 mr-36 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleOpenAdvanceSearch()}
              >
                {t("general.advancedSearch")}
                <ArrowDropDownIcon />
              </Button>
              {shouldOpenConfirmationDeleteListDialog && (
                <ConfirmationDialog
                  open={shouldOpenConfirmationDeleteListDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDeleteAll}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                  title={t("general.deleteSelectConfirm")}
                />
              )}
            </Grid>
            <Grid item container md={6} sm={12} xs={12} spacing={2}>
              <Grid item md={12} sm={12} xs={12}>
                <FormControl fullWidth>
                  <Input
                    className="search_box w-100 mt-16"
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUpSearch}
                    placeholder={t("Product.enterKeySearch")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment>
                        <Link to="#"> 
                          <SearchIcon
                            onClick={this.search}
                            style={{
                              position: "absolute",
                              top: "0",
                              right: "0",
                            }}
                          />
                        </Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={12} className="pt-0 pb-0">
              <Collapse in={openAdvanceSearch}>
                <Card elevation={0} className="pt-8 pb-12 pl-16 pr-0">
                  <Grid container xs={12} spacing={2}>
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteSub
                        label={t("Dashboard.subcategory.productCategory")}
                        searchObject={productCategorySearchObject}
                        searchFunction={searchByPageProductCategory}
                        listData={listProductCategory}
                        setListData={this.handleSetListProductCategory}
                        value={productCategory ?? null}
                        defaultValue={productCategory ?? null}
                        displayLable={"name"}
                        onSelect={this.handleSelectProductCategory}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteSub
                        label={t("Product.managementPurchaseDepartment")}
                        listData={[]}
                        setListData={() => { }}
                        searchFunction={getListManagementDepartment}
                        searchObject={{}}
                        displayLable={"name"}
                        typeReturnFunction="list"
                        isNoRenderParent={true}
                        isNoRenderChildren={true}
                        value={managementPurchaseDepartment}
                        onSelect={this.handleSelectManagementPurchaseDepartment}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteSub
                        label={t("Product.productType")}
                        listData={[]}
                        setListData={() => { }}
                        searchFunction={productTypeSearchByPage}
                        searchObject={{ pageIndex: 0, pageSize: 1000000 }}
                        displayLable={"name"}
                        value={productType}
                        onSelect={this.handleChangeProductType}
                      />
                    </Grid>
                  </Grid>
                </Card>
              </Collapse>
            </Grid>
            <Grid item xs={12}>
              <div>
                {shouldOpenEditorDialog && (
                  <ProductDialog
                    t={t}
                    i18n={i18n}
                    handleClose={this.handleDialogClose}
                    open={shouldOpenEditorDialog}
                    handleOKEditClose={this.handleOKEditClose}
                    item={item}
                  />
                )}

                {shouldOpenNotificationPopup && (
                  <NotificationPopup
                    title={t("general.noti")}
                    open={shouldOpenNotificationPopup}
                    onYesClick={this.handleDialogClose}
                    text={t(this.state.Notification)}
                    agree={t("general.agree")}
                  />
                )}

                {shouldOpenConfirmationDialog && (
                  <ConfirmationDialog
                    title={t("general.confirm")}
                    open={shouldOpenConfirmationDialog}
                    onConfirmDialogClose={this.handleDialogClose}
                    onYesClick={this.handleConfirmationResponse}
                    text={t("general.deleteConfirm")}
                    agree={t("general.agree")}
                    cancel={t("general.cancel")}
                  />
                )}
              </div>
              <MaterialTable
                title={t("general.list")}
                data={itemList}
                columns={columns}
                options={{
                  sorting: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: "450px",
                  minBodyHeight: this.state.itemList?.length > 0 ? 0 : 250,
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                  padding: "dense",
                  toolbar: false,
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                  toolbar: {
                    nRowsSelected: `${t("general.selects")}`,
                  },
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                component="div"
                count={totalElements}
                rowsPerPage={rowsPerPage}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                page={page}
                labelRowsPerPage={t("general.rows_per_page")}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </Grid>
        </ValidatorForm>
      </div>
    );
  }
}
Product.contextType = AppContext;
export default Product;
