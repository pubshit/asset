import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  Select,
  Input,
  InputLabel,
  FormControl,
  MenuItem,
  FormHelperText
} from "@material-ui/core";
import PropTypes from "prop-types";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import { ValidatorForm, TextValidator, TextField } from "react-material-ui-form-validator";
import { getByPage, deleteItem, saveItem, getAllReagent, getAllTechnician, getAllEQARound, getEQASampleTubeByHealthOrgEQARoundId, getListHealthOrgEQARoundByEQARoundId } from "./EQAResultReportFastTestService";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
// import EQAPlanningSearchDialog from './EQAPlanningSearchDialog';
import EQARoundSearchDialog from './EQARoundSearchDialog';
// import EQASampleSearchDialog from './EQASampleSearchDialog';
import { el } from "date-fns/locale";
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

class EQAResultReportFastTestEditorDialog extends Component {
  constructor(props) {
    super(props);

    getAllEQARound().then((result) => {
      let listEQARound = result.data.content;
      this.setState({ listEQARound: listEQARound });
    });
    getAllReagent().then((result) => {
      let listReagent = result.data;
      this.setState({ listReagent: listReagent });
    });
    getAllTechnician().then((result) => {
      let listTechnician = result.data;
      this.setState({ listTechnician: listTechnician });
    });
  }

  state = {
    hasErrorHealthOrgRound: false,
    hasErrorEQARound: false,
    isUsingIQC: false,
    isUsingControlLine: false,
    eqaRound: '',
    healthOrgRound: '',
    reagentLot: '',
    order: '',
    reagent: '',
    technician: '',
    personBuyReagent: '',
    details: [],
    supplyOfReagent: '',
    timeToResult: '',
    reagentExpiryDate: new Date(),
    testDate: new Date(),
    shouldOpenSearchDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenSearchEQASampleSearchDialog: false,
    listHealthOrgRound: [],
    listReagent: [],
    listTechnician: [],
    listEQARound: []
  };

  //positive(1),//Dương tính
  //indertermine(0),//Không xác định
  //negative(-1),//Âm tính
  //none(-2)//Không thực hiện
  Results = [
    { id: -1, name: "Âm tính" },
    { id: 0, name: "Không thực hiện" },
    { id: 1, name: "Dương tính" }
  ];

  listResult_C_T_Line = [
    { id: 1, name: "Dương tính" },
    { id: -1, name: "Âm tính" }
  ];

  listChooseBoolean = [
    { id: 0, value: false, name: "Không" },
    { id: 1, value: true, name: "Có" }
  ];

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleChooseBooleanChange = (value, name) => {
    this.setState({
      [name]: value.target.value
    });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleHealthOrgRoundChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }

    this.setState({ [event.target.name]: event.target.value }, function () {
      let { details } = this.state;
      let { healthOrgRound } = this.state;
      let listEQAResultReportDetail = [];
      if (healthOrgRound != null) {
        getEQASampleTubeByHealthOrgEQARoundId(healthOrgRound).then((result) => {
          let listEQASampleTube = result.data;
          if (listEQASampleTube != null && listEQASampleTube.length > 0) {
            listEQASampleTube.forEach(tube => {
              let eQAResultReportDetail = {};
              eQAResultReportDetail.sampleTube = tube;
              eQAResultReportDetail.cLine = '';
              eQAResultReportDetail.tLine = '';
              eQAResultReportDetail.result = '';
              listEQAResultReportDetail.push(eQAResultReportDetail);
            });
          }
          this.setState({ details: listEQAResultReportDetail });
        });
      }
      else {
        this.setState({ details: listEQAResultReportDetail });
      }
    });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { details } = this.state;
    if (details != null && details.length > 0) {
      details.forEach(element => {
        if (element.tableData != null && rowData != null && rowData.tableData != null
          && element.tableData.id == rowData.tableData.id) {
          if (event.target.name == "cLine") {
            element.cLine = event.target.value;
          }
          else if (event.target.name == "tLine") {
            element.tLine = event.target.value;
          }
          else if (event.target.name == "result") {
            element.result = event.target.value;
          }
        }
      });
      this.setState({ details: details });
    }
  };

  handleFormSubmit = () => {
    this.setState({ hasErrorHealthOrgRound: false, hasErrorEQARound: false });
    if (!this.state.eqaRound || this.state.eqaRound <= 0) {
      this.setState({ hasErrorEQARound: true });
      return;
    }
    else if (!this.state.healthOrgRound || this.state.healthOrgRound <= 0) {
      this.setState({ hasErrorHealthOrgRound: true });
      return;
    }
    else {
      let { id } = this.state;
      let { healthOrgRound } = this.state;
      let { reagent } = this.state;
      let { technician } = this.state;
      let { listHealthOrgRound } = this.state;
      let { listReagent } = this.state;
      let { listTechnician } = this.state;

      let objHealthOrgRound = listHealthOrgRound.find(item => item.id == healthOrgRound);
      let objReagent = null;
      let objTechnician = null;
      if (reagent != null && reagent != '') {
        objReagent = listReagent.find(item => item.id == reagent);
      }
      if (technician != null && technician != '') {
        objTechnician = listTechnician.find(item => item.id == technician);
      }

      this.setState({ healthOrgRound: objHealthOrgRound, reagent: objReagent ? objReagent : null, technician: objTechnician ? objTechnician : null }, function () {
        saveItem({
          ...this.state
        }).then(() => {
          this.props.handleOKEditClose();
        });
      });
    }
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;
    this.setState({
      ...this.props.item
    }, function () {
      let { healthOrgRound } = this.state;
      if (healthOrgRound != null && healthOrgRound.round != null && healthOrgRound.round.id != null) {

        this.setState({ eqaRound: healthOrgRound.round.id }, function () {
          getListHealthOrgEQARoundByEQARoundId(healthOrgRound.round.id).then((result) => {
            let listHealthOrgRound = result.data;
            this.setState({ listHealthOrgRound: listHealthOrgRound, healthOrgRound: healthOrgRound.id });
          });
        });
      }

      let { reagent } = this.state;
      let { technician } = this.state;
      if (reagent != null && reagent.id != null) {
        this.setState({ reagent: reagent.id });
      }
      if (technician != null && technician.id != null) {
        this.setState({ technician: technician.id });
      }
    });
  }

  handleSearchDialogClose = () => {
    this.setState({
      shouldOpenSearchDialog: false
    });
  };

  handleSearchEQARoundDialogClose = () => {
    this.setState({
      shouldOpenSearchEQARoundSearchDialog: false
    });
  };

  handleSelectEQARound = (itemSelected) => {
    let item = itemSelected.target.value;
    this.setState({ eqaRound: item }, function () {
      let listHealthOrgRound = [];
      if (item != null) {
        getListHealthOrgEQARoundByEQARoundId(item).then((result) => {
          listHealthOrgRound = result.data;
          this.setState({ listHealthOrgRound: listHealthOrgRound, healthOrgRound: '', details: [] });
        });
      } else {
        this.setState({ listHealthOrgRound: listHealthOrgRound, healthOrgRound: '', details: [] });
      }
    });
  }

  render() {
    const { classes } = this.props;
    const { selected, hasErrorHealthOrgRound, hasErrorEQARound } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let {
      isView,
      isUsingIQC,
      isUsingControlLine,
      eqaRound,
      healthOrgRound,
      reagentLot,
      order,
      reagent,
      technician,
      reagentExpiryDate,
      personBuyReagent,
      details,
      supplyOfReagent,
      testDate,
      timeToResult,
      listHealthOrgRound,
      listReagent,
      listTechnician,
      listEQARound
    } = this.state;

    let columns = [
      {
        title: t("EQAResultReportFastTest.sample_list.sample_code"), field: "sampleTube.code", align: "left", width: "150"
      },
      {
        title: t("EQAResultReportFastTest.cLine"),
        field: "cLine",
        width: "150",
        render: rowData =>
          <FormControl className="w-80">
            <Select
              value={rowData.cLine}
              disabled={isView}
              onChange={cLine => this.handleRowDataCellChange(rowData, cLine)}
              inputProps={{
                name: "cLine",
                id: "cLine-simple"
              }}
            >
              {this.listResult_C_T_Line.map(item => {
                return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
      },
      {
        title: t("EQAResultReportFastTest.tLine"),
        field: "tLine",
        width: "150",
        render: rowData =>
          <FormControl className="w-80">
            <Select
              value={rowData.tLine}
              disabled={isView}
              onChange={tLine => this.handleRowDataCellChange(rowData, tLine)}
              inputProps={{
                name: "tLine",
                id: "tLine-simple"
              }}
            >
              {/* <MenuItem value=''><em>None</em> </MenuItem> */}
              {this.listResult_C_T_Line.map(item => {
                return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
      },
      {
        title: t("EQAResultReportFastTest.sample_list.result"), field: "result", align: "left", width: "150",
        render: rowData =>
          <FormControl className="w-80">
            <Select
              value={rowData.result}
              disabled={isView}
              onChange={result => this.handleRowDataCellChange(rowData, result)}
              inputProps={{
                name: "result",
                id: "result-simple"
              }}
            >
              {/* <MenuItem value=''><em>None</em> </MenuItem> */}
              {this.Results.map(item => {
                return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
      }
    ];
    return (
      <Dialog  open={open} PaperComponent={PaperComponent} maxWidth={'lg'} fullWidth={true} >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t("SaveUpdate")}</span>
        </DialogTitle>
        <DialogContent>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="mb-16" container spacing={2}>
              <Grid item sm={12} xs={12}>
                <FormControl fullWidth={true} error={hasErrorEQARound}>
                  <InputLabel htmlFor="eQARound-simple">{t('EQARound.title')}</InputLabel>
                  <Select
                    value={eqaRound ? eqaRound : ''}
                    disabled={isView}
                    onChange={value => this.handleSelectEQARound(value)}
                    required={true}
                    inputProps={{
                      name: "eQARound",
                      id: "eQARound-simple"
                    }}
                  >
                    {listEQARound.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                  {hasErrorEQARound && <FormHelperText>This is required!</FormHelperText>}
                </FormControl>
                {/* <TextValidator disabled={true} placeholder={t("EQARound.title")} id="eqaRound" className="w-80 mt-16 mr-16" value={eqaRound != null ? eqaRound.name : ''}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <Button
                  className="mt-16"
                  variant="contained"
                  color="primary"
                  onClick={() => this.setState({ shouldOpenSearchEQARoundSearchDialog: true, item: {} })}
                >
                  {t('Select')}
                </Button>
              {shouldOpenSearchEQARoundSearchDialog && (
                <EQARoundSearchDialog
                  open={this.state.shouldOpenSearchEQARoundSearchDialog}
                  handleSelect={this.handleSelectEQARound}
                  selectedItem={eqaRound != null ? eqaRound : {}}
                  handleClose={this.handleSearchEQARoundDialogClose} t={t} i18n={i18n} />
              )
              } */}
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true} error={hasErrorHealthOrgRound}>
                  <InputLabel htmlFor="healthOrgRound-simple">{t('EQAResultReportFastTest.healthOrgEQARoundCode')}</InputLabel>
                  <Select
                    value={healthOrgRound ? healthOrgRound : ''}
                    disabled={isView}
                    onChange={this.handleHealthOrgRoundChange}
                    required={true}
                    inputProps={{
                      name: "healthOrgRound",
                      id: "healthOrgRound-simple"
                    }}
                  >
                    {listHealthOrgRound.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.healthOrg.code}</MenuItem>;
                    })}
                  </Select>
                  {hasErrorHealthOrgRound && <FormHelperText>This is required!</FormHelperText>}
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAResultReportFastTest.reagentLot")}
                  onChange={this.handleChange}
                  type="text"
                  name="reagentLot"
                  value={reagentLot ? reagentLot : ''}
                  disabled={isView}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAResultReportFastTest.order")}
                  onChange={this.handleChange}
                  type="number"
                  name="order"
                  value={order ? order : ''}
                  disabled={isView}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={t('EQAResultReportFastTest.reagentExpiryDate')}
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    value={reagentExpiryDate}
                    disabled={isView}
                    onChange={date => this.handleDateChange(date, "reagentExpiryDate")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="reagent-simple">{t('EQAResultReportFastTest.reagentName')}</InputLabel>
                  <Select
                    value={reagent ? reagent : ''}
                    disabled={isView}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "reagent",
                      id: "reagent-simple"
                    }}
                  >
                    {listReagent.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="technician-simple">{t('EQAResultReportFastTest.technicianName')}</InputLabel>
                  <Select
                    value={technician ? technician : ''}
                    disabled={isView}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "technician",
                      id: "technician-simple"
                    }}
                  >
                    {listTechnician.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAResultReportFastTest.supplyOfReagent")}
                  onChange={this.handleChange}
                  type="text"
                  name="supplyOfReagent"
                  value={supplyOfReagent ? supplyOfReagent : ''}
                  disabled={isView}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={t('EQAResultReportFastTest.testDate')}
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    value={testDate}
                    disabled={isView}
                    onChange={date => this.handleDateChange(date, "testDate")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAResultReportFastTest.personBuyReagent")}
                  onChange={this.handleChange}
                  type="text"
                  name="personBuyReagent"
                  value={personBuyReagent ? personBuyReagent : ''}
                  disabled={isView}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAResultReportFastTest.timeToResult")}
                  onChange={this.handleChange}
                  type="number"
                  name="timeToResult"
                  value={timeToResult ? timeToResult : ''}
                  disabled={isView}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="isUsingIQC-simple">{t('EQAResultReportFastTest.isUsingIQC')}</InputLabel>
                  <Select
                    value={(isUsingIQC || isUsingIQC == false) ? isUsingIQC : ''}
                    disabled={isView}
                    onChange={value => this.handleChooseBooleanChange(value, "isUsingIQC")}
                    required={true}
                    inputProps={{
                      name: "isUsingIQC",
                      id: "isUsingIQC-simple"
                    }}
                  >
                    {this.listChooseBoolean.map(item => {
                      return <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="isUsingControlLine-simple">{t('EQAResultReportFastTest.isUsingControlLine')}</InputLabel>
                  <Select
                    value={(isUsingControlLine || isUsingControlLine == false) ? isUsingControlLine : ''}
                    disabled={isView}
                    onChange={value => this.handleChooseBooleanChange(value, "isUsingControlLine")}
                    required={true}
                    inputProps={{
                      name: "isUsingControlLine",
                      id: "isUsingControlLine-simple"
                    }}
                  >
                    {this.listChooseBoolean.map(item => {
                      return <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <MaterialTable title={t("EQAResultReportFastTest.list_tube")} data={details} columns={columns}
                options={{
                  selection: false,
                  paging: false,
                  search: false
                }}
                components={{
                  Toolbar: props => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              {/* <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
                component="div"
                count={this.state.totalElements}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                backIconButtonProps={{
                  "aria-label": "Previous Page"
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page"
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.setRowsPerPage}
              /> */}
            </Grid>
            <div className="flex flex-space-between flex-middle mt-16">
              {(!isView && <Button variant="contained" color="primary" type="submit" >
                {t('Save')}
              </Button>
              )}
              <Button variant="contained" color="secondary" type="button" onClick={() => handleClose()}> {t('Cancel')}</Button>
            </div>
          </ValidatorForm>
        </DialogContent>
      </Dialog >
    );
  }
}

export default EQAResultReportFastTestEditorDialog;
