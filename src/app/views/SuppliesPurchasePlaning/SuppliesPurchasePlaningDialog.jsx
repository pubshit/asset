import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Dialog,
  DialogActions,
  Grid,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import {appConst, formatDate, variable} from "app/appConst";
import history from "history.js";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import moment from "moment";
import React from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { getListManagementDepartment, getShoppingForm } from "../Asset/AssetService";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import {
  updatePurchasePlaningById, createPurchasePlaning, assetPuchasePlanningExportToExcel
} from "../PurchasePlaning/PurchasePlaningService";
import {searchByPage as SupplierSearchByPage, searchByTextNew} from '../Supplier/SupplierService';
import { getListUserByDepartmentId, getByPlanId, } from "./SuppliesPurchasePlaningService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListProductByIds } from "../SuppliesPurchaseRequestCount/PurchaseRequestCountService";
import {
  NumberFormatCustom,
  convertNumberPrice,
  convertNumberPriceRoundUp,
  formatDateDto,
  formatTimestampToDate,
  convertFromToDate, functionExportToExcel, filterOptions, getTheHighestRole
} from "app/appFunction";
import AppContext from "app/appContext";
import ValidatedDatePicker from "../Component/ValidatePicker/ValidatePicker";
import clsx from "clsx";
import { Label, PaperComponent } from "../Component/Utilities"; 
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import SupplierDialog from "../Supplier/SupplierDialog";
import ShoppingFormDialog from "../ShoppingForm/ShoppingFormDialog";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class SuppliesPlaningDialog extends React.Component {
  state = {
    rowsPerPage: 1000,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    keyword: "",
    Notification: "",
    planDate: Date.now(),
    approvedDate: null,
    managementDepartment: null,
    status: null,
    note: null,
    name: null,
    totalEstimatedCost: null,
    planer: null,
    shouldOpenProductPopup: false,
    product: null,
    department: null,
    listDepartments: [],
    listProducts: [],
    planingDepartment: null,
    listProduct: [],
    listSupplier: [],
    supplier: null,
    type: 1,
    formToDate: moment().endOf("month"),
    formDate: moment().startOf("month"),
    checkDate: false,
    listUnit: [],
    listHTMS: [],
    searchParamUserByRequestDepartment: { departmentId: null },
    unit: null,
    htms: null,
    statusObj: null,
    listStatus: [],
    HTMSFilter: null,
    supplierFilter: null,
    quotationApprovalDate: null,
  };

  handleChangeFilter = (value, name) => {
    this.setState({ [name]: value }, () => {
      this.updateProductPlan();
    });
  };

  updateProductPlan = async () => {
    let { t } = this.props
    let { setPageLoading } = this.context
    setPageLoading(true)
    let searchObject = {
      purchasePlanId: this.props?.item?.id,
      shoppingFormId: this.state?.HTMSFilter?.id,
      supplierId: this.state.supplierFilter?.id,
      productType: this.state.productType?.id
    }
    try {
      let res = await getByPlanId(searchObject)
      if (res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listProducts: res?.data?.data?.content?.length > 0 ? [...res?.data?.data?.content] : []
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
    finally {
      setPageLoading(false)
    }
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  selectPurchaseStatus = (item) => {
    this.setState({ status: item, statusObj: item })
  };

  handleSelectDepartment = (department) => {
    this.setState({ department, searchParamUserByRequestDepartment: { departmentId: department?.id, pageSize: 1000 }, planer: null });
  };

  selectPurchaseDepartment = (listDepartments) => {
    this.setState({ listDepartments });
  };

  handleDateChange = (date, name) => {
    this.setState({ [name]: date });
  };

  handleContractDateChange = (rowData, date) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.contractDate = formatDateDto(date)
        }
      });
      this.setState({ listProducts });
    }
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { planDate } = this.state;
    let { t } = this.props

    let payload = {
      approvedDate: formatDateDto(this.state.approvedDate),
      name: this.state.name,
      note: this.state.note,
      planDate: convertFromToDate(this.state.planDate).toDate,
      planDepartmentId: this.state?.department?.id,
      planDepartmentName: this.state?.department?.name,
      planPersonId: this.state.planer?.personId,
      planPersonName: this.state.planer?.personDisplayName,
      purchasePreparePlanIds: id ? this.state?.purchasePreparePlanIds : this.props?.purchaseRequests?.map((item) => { return item?.id }),
      purchasePreparePlanId: this.state?.purchasePreparePlanId,
      purchaseProducts: this.state?.listProducts,
      quotationApprovalDate: formatDateDto(this.state.quotationApprovalDate),
      status: this.state.statusObj.indexOrder,
      totalEstimatedCost: this.state.totalEstimatedCost,
      type: appConst.TYPE_PURCHASE.VT
    }

    if (this.state.listProducts == [] || this.state.listProducts.length === 0) {
      toast.warning("Không có sản phẩm");
      toast.clearWaitingQueue();
      return;
    }
    if (id) {
      updatePurchasePlaningById(
        {
          ...payload,
        },
        id
      ).then((res) => {
        if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.success"))
          this.props.handleOKEditClose();
        }
        else {
          toast.warning(t(res?.data?.message))
        }
      }).catch(() =>
        toast.error(t("general.error"))
      );
    } else {
      createPurchasePlaning(payload).then((res) => {
        if (res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.success"))
          history.push(ConstantList.ROOT_PATH + "list/supplies_purchase_planing");
        } else {
          toast.warning(t(res?.data?.message))
        }
      }).catch(() =>
        toast.error(t("general.error"))
      );
    }
  };

  handleGetListProducts = async () => {
    let { t } = this.props
    let { purchaseRequests } = this.props;
    let searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      purchasePreparePlanIds: purchaseRequests?.map((item) => { return item?.id }).join(),
      status: appConst.listStatusProductInPurchaseRequest.DA_DUYET.code,
    }

    try {
      if (purchaseRequests) {
        const res = await getListProductByIds(searchObject)
        if (res?.data?.code === appConst?.CODE?.SUCCESS) {
          this.setState({ listProducts: res?.data?.data?.content })
        } else {
          toast.error(t("toastr.error"))
        }
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    }
  }

  componentDidMount = () => {
    let {
      item,
      fromToDate,
      fromDate,
      planingDepartment,
    } = this.props;
    let roles = getTheHighestRole();
    let totalEstimatedCost = item?.purchaseProducts?.reduce(
      (acc, cur) => acc + cur?.estimatedUnitPrice * cur?.approvedQuantity,
      0
    )
    this.setState({
      listStatus: appConst.listStatusPurchasePlaning.slice(0, appConst.listStatusPurchasePlaning.length - 1),
      minPlantDate: item?.planDate,
      ...item,
      ...roles,
      fromToDate,
      fromDate,
      planingDepartment,
    })
    if (item?.id) {
      this.setState({
        listProducts: item?.purchaseProducts?.map(item => ({
          ...item,
          supplier: item.supplierId
            ? {
              id: item.supplierId,
              name: item.supplierName,
            }
            : null,
          shoppingForm: item.shoppingFormId
            ? {
              id: item.shoppingFormId,
              name: item.shoppingFormName,
            }
            : null,
        })),
        department: {
          name: item?.planDepartmentName,
          id: item?.planDepartmentId
        },
        planer: {
          personDisplayName: item?.planPersonName,
          personId: item?.planPersonId
        },
        statusObj: appConst.listStatusPurchasePlaning?.find((status) => status.indexOrder === item?.status),
        approvedDate: item?.approvedDate ? moment(item?.approvedDate).format(formatDate) : null,
        totalEstimatedCost: Number(totalEstimatedCost),
      })
    }
    else {
      this.setState({ totalEstimatedCost: Number(totalEstimatedCost), planDate: new Date() })
      this.handleGetListProducts();
    }
  }

  handleChangeTotalEstimatedCosts() {
    let { listProducts } = this.state
    let totalEstimatedCost = listProducts?.reduce((acc, cur) => acc + cur?.estimatedUnitPrice * cur?.approvedQuantity, 0)
    this.setState({ totalEstimatedCost: Number(totalEstimatedCost) })
  }


  handlePriceChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.map((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.estimatedUnitPrice = Number(event.target.value);
          this.handleChangeTotalEstimatedCosts()
        }
      });
      this.setState({ listProducts });
    }
  };

  handleHTMSChange = (rowData, value) => {
    let { listProducts } = this.state;
    if (value?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenDialogShoppingForm: true,
        currentRowData: rowData,
        listHTMS: [],
      });
      return;
    }
    if (listProducts && listProducts.length > 0) {
      listProducts.map((vd) => {
        if (vd?.productId === rowData?.productId) {
          vd.shoppingForm = value || null;
          vd.shoppingFormId = value?.id || "";
          vd.shoppingFormName = value?.name || "";
        }
      });

      this.setState({ listProducts });
    }
  };

  handleSupplierChange = (rowData, value) => {
    let { listProducts } = this.state;
    if (value?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenAddSupplierDialog: true,
        currentRowData: rowData,
        listSupplier: [],
      });
      return;
    }
    if (listProducts && listProducts.length > 0) {
      listProducts.map((vd) => {
        if (vd?.productId === rowData?.productId) {
          vd.supplier = value || null;
          vd.supplierName = value?.name || "";
          vd.supplierId = value?.id || "";
        }
      });

      this.setState({ listProducts });
    }
  };

  handleChangeNotesProduct = (rowData, value) => {
    let { listProducts } = this.state;
    if (listProducts && listProducts.length > 0) {
      listProducts.map((vd) => {
        if (vd?.productId === rowData?.productId) {
          vd.note = value || null;
        }
      });

      this.setState({ listProducts });
    }
  };

  handleSelectRequestPerson = (item) => {
    this.setState({
      planer: item ? item : null,
    });
  };

  handleGetProductStatus = (rowData) => {
    const { DA_DUYET, CHO_DUYET, KHONG_DUYET } = appConst.listStatusProductInPurchaseRequest;
    const statusIndex = rowData?.status;
    const classByStatusIndex = {
      [CHO_DUYET.code]: "status-warning",
      [DA_DUYET.code]: "status-success",
      [KHONG_DUYET.code]: "status-error",
      [null]: "",
    }
    const nameByStatusIndex = {
      [CHO_DUYET.code]: CHO_DUYET.name,
      [DA_DUYET.code]: DA_DUYET.name,
      [KHONG_DUYET.code]: KHONG_DUYET.name,
      [null]: "",
    }

    return (
      <span className={clsx("status", classByStatusIndex[statusIndex])}>
        {nameByStatusIndex[statusIndex]}
      </span>
    )
  }

  exportToExcel = async () => {
    const {setPageLoading} = this.context;
    const {t} = this.props;

    try {
      setPageLoading(true);
      let searchObject = {
        type: appConst.TYPE_PURCHASE.VT,
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        purchasePlanId: this.state.id,
      };

      await functionExportToExcel(
        assetPuchasePlanningExportToExcel,
        searchObject,
        t("exportToExcel.AssetPuchasePlanning"),
      )
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  handleSetData = (value, name) => {
    this.setState({[name]: value});
  }

  handleClosePopup = () => {
    this.setState({
      shouldOpenAddSupplierDialog: false,
      shouldOpenDialogShoppingForm: false,
      currentRowData: null,
      keySearch: "",
    });
  }

  render() {
    const {
      t,
      open,
      isView
    } = this.props;
    let {
      rowsPerPage,
      page,
      department,
      note,
      name,
      totalEstimatedCost,
      planer,
      listProducts,
      listSupplier,
      planDate,
      approvedDate,
      shouldOpenNotificationPopup,
      listHTMS,
      searchParamUserByRequestDepartment,
      statusObj,
      listStatus,
      quotationApprovalDate,
      listDepartments,
      minPlantDate,
      isRoleAssetManager,
      isRoleOrgAdmin,
      shouldOpenAddSupplierDialog,
      shouldOpenDialogShoppingForm,
      currentRowData,
      keySearch,
    } = this.state;
    let checkStatus = [
      appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder,
      appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder
    ].includes(statusObj?.indexOrder)
    let isShowApprovedDate = statusObj?.indexOrder
      >= appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder;
    let isShowQuotationApprovalDate = statusObj?.indexOrder
      >= appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder;
    let isEndStatus = statusObj?.indexOrder
      === appConst?.listStatusPurchasePlaningObject.KET_THUC.indexOrder;
    let isCategoryApproved = statusObj?.indexOrder
      === appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder;
    let isConfirmedTheQuotation = statusObj?.indexOrder
      === appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder;
    let searchObject = { keyword: "", ...appConst.OBJECT_SEARCH_MAX_SIZE, };

    const searchObjectOfSupplier = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      typeCodes: [appConst.TYPE_CODES.NCC_CU],
    }
		const planerSearchObject = {
			...appConst.OBJECT_SEARCH_MAX_SIZE,
			departmentId: department?.id,
		}

    let columns = [
      {
        title: t("Product.STT"),
        field: "code",
        maxWidth: 50,
        align: "center",
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "productName",
        minWidth: 150,
        align: "left",
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "center",
        minWidth: 120,
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "center",
        minWidth: 120,
      },
      {
        title: t("SparePart.unit"),
        field: "skuName",
        align: "left",
        minWidth: 70,
      },
      ...(isConfirmedTheQuotation ? [
        {
          title: t("purchaseRequestCount.contractDate"),
          field: "contractDate",
          align: "left",
          cellStyle: {
            textAlign: "center",
          },
          minWidth: 200,
          render: (rowData) => (
            isView ? formatTimestampToDate(rowData?.contractDate)
              :
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  inputVariant="standard"
                  type="text"
                  autoOk={false}
                  format="dd/MM/yyyy"
                  name={"contractDate"}
                  value={rowData?.contractDate}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  onChange={(date) =>
                    this.handleContractDateChange(rowData, date)
                  }
                  minDate={this.state.approvedDate || new Date()}
                  minDateMessage={t(
                    "purchasePlaning.minContractDateMessage"
                  )}
                  disabled={isView}
                />
              </MuiPickersUtilsProvider>
          )
        }
      ] : []),
      {
        title: t("Product.estimatedUnitPrice"),
        field: "estimatedUnitPrice",
        align: "right",
        minWidth: 200,
        render: (rowData) => (
          isView ? convertNumberPrice(rowData?.estimatedUnitPrice || 0)
            : <TextValidator

              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  className: "text-align-right",
                },
                readOnly: isView
              }}
              type="text"
              onChange={(event) => this.handlePriceChange(rowData, event)}
              id="formatted-numberformat-carryingAmount"
              name="estimatedUnitPrice"
              value={convertNumberPriceRoundUp(Number(rowData?.estimatedUnitPrice || 0))}
            />
        ),
      },
      {
        title: t("Product.estimatedCosts"),
        field: "estimatedCosts",
        align: "right",
        minWidth: 200,
        render: (rowData) => (
          convertNumberPriceRoundUp(rowData.estimatedUnitPrice * rowData.approvedQuantity)
        ),
      },
      {
        title: t('Product.formShopping'),
        field: "HTMS",
        align: "left",
        minWidth: 200,
        render: (rowData) => isView
          ? rowData?.shoppingFormName
          : (
            <AsynchronousAutocompleteSub
              searchFunction={getShoppingForm}
              searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
              listData={listHTMS}
              nameListData="listHTMS"
              setListData={this.handleSetData}
              displayLable="name"
              typeReturnFunction="category"
              value={rowData.shoppingForm || null}
              onSelect={(value) => this.handleHTMSChange(rowData, value)}
              onInputChange={(e) => this.handleSetData(e.target.value, "keySearch")}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                isRoleAssetManager || isRoleOrgAdmin,
                "name",
              )}
              noOptionsText={t("general.noOption")}
              validators={isCategoryApproved ? ["required"] : []}
              errorMessages={isCategoryApproved ? [t("general.required")] : []}
            />
          )

      },
      {
        title: t("Product.supplier"),
        field: "supplierName",
        align: "left",
        hidden: isCategoryApproved,
        minWidth: 200,
        render: (rowData) => isView
          ? rowData?.supplierName
          : (
            <AsynchronousAutocompleteSub
              searchFunction={searchByTextNew}
              searchObject={searchObjectOfSupplier}
              listData={listSupplier}
              nameListData="listSupplier"
              setListData={this.handleSetData}
              displayLable="name"
              value={rowData.supplier || null}
              onSelect={(value) => this.handleSupplierChange(rowData, value)}
              onInputChange={(e) => this.handleSetData(e.target.value, "keySearch")}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                isRoleAssetManager || isRoleOrgAdmin,
                "name",
              )}
              noOptionsText={t("general.noOption")}
              validators={isConfirmedTheQuotation ? ["required"] : []}
              errorMessages={isConfirmedTheQuotation ? [t("general.required")] : []}
            />
          )
      },
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        hidden: isCategoryApproved,
        minWidth: 150,
        render: this.handleGetProductStatus,
      },
      {
        title: t("purchase_request_count.note"),
        field: "description",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          return (
            <TextValidator
              className="w-100"
              onChange={(event) =>
                this.handleChangeNotesProduct(rowData, event.target.value)
              }
              InputProps={{
                readOnly: isView,
              }}
              name="description"
              value={rowData.note || ""}
            />
          );
        },
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        scroll="paper"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <CustomValidatorForm
          ref="formPlan"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            {t("purchasePlaning.title")}
          </DialogTitle>
          <DialogContent style={{ minHeight: "400px" }}>
            <Grid container spacing={1}>
              <Grid item md={4} sm={12} xs={12}>
                <ValidatedDatePicker
                  fullWidth
                  margin="none"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planingDate")}
                    </span>
                  }
                  inputVariant="standard"
                  format="dd/MM/yyyy"
                  name={"planDate"}
                  value={planDate}
                  minDate={minPlantDate}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  onChange={(date) =>
                    this.handleDateChange(date, "planDate")
                  }
                  maxDate={new Date()}
                  minDateMessage={minPlantDate ? t("general.invalidPlanDateFormat") : t("purchasePlaning.minDateMessage")}
                  maxDateMessage={t("purchasePlaning.maxDatePlanMessage")}
                  disabled={isView}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              {isShowApprovedDate && (
                <Grid item md={4} sm={12} xs={12}>
                  <ValidatedDatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog mt-2"
                    label={
                      <Label isRequired>{t("purchasePlaning.approvalDate")}</Label>
                    }
                    name={'approvedDate'}
                    inputVariant="standard"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={approvedDate}
                    onChange={date => this.handleDateChange(date, "approvedDate")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    disabled={isView || !checkStatus}
                    validators={checkStatus ? ['required'] : []}
                    errorMessages={t('general.required')}
                    minDate={planDate}
                    maxDate={quotationApprovalDate || new Date()}
                    minDateMessage={t("purchasePlaning.minDateMessage")}
                    maxDateMessage={quotationApprovalDate ? t("purchasePlaning.maxDateApproveMessage") : t("purchasePlaning.maxDateMessage")}
                  />
                </Grid>
              )}
              {isShowQuotationApprovalDate && (
                <Grid item md={4} sm={12} xs={12}>
                  <ValidatedDatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog mt-2"
                    label={
                      <Label isRequired>{t("purchasePlaning.quoteApprovedDate")}</Label>
                    }
                    name={'quotationApprovalDate'}
                    autoOk
                    format="dd/MM/yyyy"
                    value={quotationApprovalDate}
                    onChange={date => this.handleDateChange(date, "quotationApprovalDate")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    readOnly={isView}
                    disabled={isEndStatus}
                    validators={isShowQuotationApprovalDate ? ['required'] : []}
                    errorMessages={t('general.required')}
                    minDate={approvedDate || planDate}
                    maxDate={new Date()}
                    minDateMessage={approvedDate ? t("purchasePlaning.minDateApproveMessage") : t("purchasePlaning.minDateMessage")}
                    maxDateMessage={t("purchasePlaning.maxDateMessage")}
                  />
                </Grid>
              )}
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span>
                    <span className="colorRed">* </span>
                    <span>{t("maintainRequest.name")}</span>
                  </span>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  inputProps={{
                    readOnly: isView
                  }}
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  searchFunction={() => { }}
                  searchObject={{}}
                  label={
                    <Label isRequired>{t("maintainRequest.status")}</Label>
                  }
                  listData={listStatus}
                  readOnly={isView}
                  displayLable={"name"}
                  value={statusObj ? statusObj : null}
                  onSelect={this.selectPurchaseStatus}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                {isView ?
                  <TextValidator
                    className="w-100 "
                    label={<span>
                      <span className="colorRed">* </span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>}
                    type="text"
                    name="planingDepartmentName"
                    value={this.state.planDepartmentName}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  /> :
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>{" "}
                        <span>{t("purchasePlaning.planingDepartment")}</span>
                      </span>
                    }
                    searchFunction={getListManagementDepartment}
                    searchObject={searchObject}
                    displayLable={"name"}
                    typeReturnFunction="list"
                    readOnly={true}
                    value={department ?? null}
                    defaultValue={department ?? null}
                    listData={listDepartments}
                    setListData={(data) => this.selectPurchaseDepartment(data)}
                    onSelect={this.handleSelectDepartment}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    noOptionsText={t("general.noOption")}
                  />}
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                {isView ?
                  <TextValidator
                    className="w-100 "
                    label={<span>
                      <span className="colorRed">* </span>
                      <span>{t("purchaseRequest.requestPerson")}</span>
                    </span>}
                    type="text"
                    name="planPersonName"
                    value={this.state.planPersonName}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  /> :
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchaseRequest.requestPerson")}
                      </span>
                    }
                    disabled={!department}
                    searchFunction={getListUserByDepartmentId}
                    searchObject={planerSearchObject}
                    defaultValue={planer ?? ''}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    value={planer ?? ''}
                    onSelect={requestPerson => this?.handleSelectRequestPerson(requestPerson)}
                    noOptionsText={t("general.noOption")}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />}
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.totalEstimatedCosts")}
                    </span>
                  }
                  // onChange={this.handleChange}
                  type="text"
                  inputProps={{
                    readOnly: true
                  }}
                  name="totalEstimatedCost"
                  value={convertNumberPriceRoundUp(Number(totalEstimatedCost)) || ""}
                  validators={["minNumber:1", "required"]}
                  errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
                />
              </Grid>
              <Grid item md={checkStatus ? 8 : 12} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("maintainRequest.note")}
                  onChange={this.handleChange}
                  type="text"
                  name="note"
                  inputProps={{
                    readOnly: isView
                  }}
                  value={note}
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} justifyContent="flex-end" className="mt-12">
              <Grid item xs={12}>
                <MaterialTable
                  data={listProducts ? listProducts : []}
                  columns={columns}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    padding: "dense",
                    sorting: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "280px",
                    minBodyHeight: "280px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ witdth: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!isView && (
                <Button variant="contained" color="primary" type="submit" className="mr-12">
                  {t("general.save")}
                </Button>
              )}
              {this.state.id && (
                <Button
                  className="align-bottom mr-12"
                  variant="contained"
                  color="primary"
                  onClick={this.exportToExcel}
                >
                  {t("general.exportToExcel")}
                </Button>
              )}
            </div>
          </DialogActions>
        </CustomValidatorForm>
        {shouldOpenAddSupplierDialog && (
          <SupplierDialog
            t={t}
            item={{
              name: keySearch,
            }}
            disabledType
            typeCodes={[appConst.TYPE_CODES.NCC_CU]}
            open={shouldOpenAddSupplierDialog}
            handleSelect={(value) => this.handleSupplierChange(currentRowData, value)}
            handleClose={this.handleClosePopup}
          />
        )}
        {shouldOpenDialogShoppingForm && (
          <ShoppingFormDialog
            t={t}
            item={{
              name: keySearch,
            }}
            handleClose={this.handleClosePopup}
            handleOKEditClose={this.handleClosePopup}
            open={shouldOpenDialogShoppingForm}
            selectShoppingForm={(value) => this.handleHTMSChange(currentRowData, value)}
          />
        )}
      </Dialog>
    );
  }
}
SuppliesPlaningDialog.contextType = AppContext;
export default SuppliesPlaningDialog;
