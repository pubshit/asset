import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/purchase_planing" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_PRODUCTS =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-products";
const API_PATH_MAINTAIN =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-plan";

export const searchByPage = (purchasePlaning) => {
  return axios.post(API_PATH_MAINTAIN + "/search", purchasePlaning);
};

export const createPurchasePlaning = (purchasePlaning) => {
  return axios.post(API_PATH, purchasePlaning);
};

export const updatePurchasePlaningById = (
  purchasePlaning,
  purchasePlaningId
) => {
  return axios.put(API_PATH + "/" + purchasePlaningId, purchasePlaning);
};

export const getPurchasePlaningById = (purchasePlaningId) => {
  return axios.get(API_PATH_MAINTAIN + "/" + purchasePlaningId);
};

export const getListProductByIds = (object) => {
  return axios.get(API_PATH_PRODUCTS + "/plan", { params: object });
};

export const deletePurchasePlaningById = (purchasePlaningId) => {
  return axios.delete(API_PATH_MAINTAIN + "/" + purchasePlaningId);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/purchaseRequestToExcel",
    data: searchObject,
    responseType: "blob",
  });
};

// api tạm dùng bên điều chuyển để kiếm tra ui
// export const searchByPage = (searchObject) => {
//   let url = API_PATH_TRANSFER + `/page`;
//   let config = {
//     params: {
//       pageIndex: searchObject?.pageIndex,
//       pageSize: searchObject?.pageSize,
//       keyword: searchObject?.keyword,
//       statusIndex: searchObject?.statusIndex,
//     },
//   };
//   return axios.get(url, config);
// };

const API_PATH_TRANSFER =
  ConstantList.API_ENPOINT + "/api/v1/fixed-assets/transfer-vouchers";

export const updateStatus = (data) => {
  let config = {
    params: {
      note: data?.note,
      issueDate: data?.issueDate,
    },
  };
  let url = API_PATH_TRANSFER + "/" + data?.id + "/status/" + data?.status;
  return axios.put(url, data?.dataState, config);
};
export const getCountStatus = (type) => {
  let url = `${API_PATH_MAINTAIN}/count-by-status?type=${type}`;
  return axios.get(url);
};

export const createTransfer = (data) => {
  return axios.post(API_PATH_TRANSFER, data);
};

export const updateTransfer = (id, data) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.put(url, data);
};
export const getStatus = () => {
  return axios.get(ConstantList.API_ENPOINT + "/api/asset_transfer_status");
};

//get phong ban bàn giao
export const getListManagementDepartment = (searchObject) => {
  return axios.post(
    ConstantList.API_ENPOINT +
      "/api/assetDepartment" +
      ConstantList.URL_PREFIX +
      "/searchByPage",
    searchObject
  );
};

// get người theo id phòng
export const getListUserByDepartmentId = (searchObject) => {
  let config = { params: searchObject };
  let url = ConstantList.API_ENPOINT + "/api/v1/user-departments/page";
  return axios.get(url, config);
};

export const supplierPuchasePlanningExportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/purchase-plan",
    data: searchObject,
    responseType: "blob",
  });
};

export const getByPlanId = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  let url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-products/plan";
  return axios.get(url, config);
};
