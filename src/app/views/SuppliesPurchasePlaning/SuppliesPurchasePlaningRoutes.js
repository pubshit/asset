import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const SuppliesPurchasePlaning = EgretLoadable({
  loader: () => import("./SuppliesPurchasePlaningTable")
});
const ViewComponent = withTranslation()(SuppliesPurchasePlaning);

const SuppliesPurchasePlaningRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/supplies_purchase_planing",
    exact: true,
    component: ViewComponent
  }
];

export default SuppliesPurchasePlaningRoutes;