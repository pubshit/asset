import React, { useEffect } from "react";
import {
  IconButton,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";

import { searchByPage as ShoppingFormSearchByPage} from "../ShoppingForm/ShoppingFormService";
import {searchByPage as SupplierSearchByPage} from '../Supplier/SupplierService'
import { getAllStockKeepingUnits } from "../StockKeepingUnit/StockKeepingUnitService";
import { getStatus, getListManagementDepartment, getListUserByDepartmentId } from './SuppliesPurchasePlaningService';
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import viLocale from "date-fns/locale/vi";
import { useState } from "react";
import { createFilterOptions } from "@material-ui/lab";
import { LightTooltip, TabPanel, a11yProps, checkInvalidDate, convertNumberPriceRoundUp, isCheckLenght } from "app/appFunction";
import { appConst } from "app/appConst";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function TransferScrollableTabsButtonForce(props) {
  const t = props.t
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  let transferStatus = appConst.listStatusTransfer.find(status => status.indexOrder === props?.item?.transferStatus?.indexOrder) || "";

  const searchObject = { pageIndex: 1, pageSize: 1000000 };
  const filterAutocomplete = createFilterOptions();
  const [listPurchaseForm, setListPurchaseForm] = useState(props?.item?.listReceiverPerson || []);
  const [listSuppiler, setListSuppiler] = useState(props?.item?.listReceiverPerson || []);
  const [listStockKeepingUnit, setListStockKeepingUnit] = useState(props?.item?.listReceiverPerson || []);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? ''
    });


  }, [props?.item?.handoverDepartment]);



  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255)
    })
  }, [props?.item?.assetVouchers]);

  useEffect(() => {
    setListPurchaseForm(props?.item?.listPurchaseForm)
    setListSuppiler(props?.item?.listSuppiler)
    setListStockKeepingUnit(props?.item?.listStockKeepingUnit)
  }, [props?.item]);

  let columns = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData?.asset?.id);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    },
    {
      title: t("purchaseRequestCount.productPortfolio"),
      field: "asset.code",
      minWidth: 120,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("Product.approvedQuanlity"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("Product.actualQuantity"),
      field: "asset.originalCost",
      align: "center",
      minWidth: 150,
      maxWidth: 400,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
       render: rowData =>
        <TextValidator
          className="w-100"
          name="originalCost"
        /> 
    },
    {
      title: t("Product.stockKeepingUnit"),
      field: "asset.originalCost",
      align: "center",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
        (props?.item?.isView ?
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData.stockKeepingUnit ? rowData.stockKeepingUnit?.displayName : ''}
          /> : <AsynchronousAutocompleteTransfer
            searchFunction={getAllStockKeepingUnits}
            searchObject={searchObject}
            listData={listStockKeepingUnit || []}
            setListData={(data) => props.handleSetDataSelect(data, "listStockKeepingUnit")}
            displayLable={'name'}
            typeReturnFunction="category"
            value={rowData ? rowData?.stockKeepingUnit : null}
            onSelect={props.selectStockKeepingUnit}
            onSelectOptions={rowData}
          />
      )
    },
    {
      title: t("Product.estimatedUnitPrice"),
      field: "asset.carryingAmount",
      align: "center",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) =>
        rowData?.asset?.carryingAmount
          ? convertNumberPriceRoundUp(rowData?.asset?.carryingAmount)
          : "",
    },
    {
      title: 'HTMS',
      minWidth: 230,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
      ((props?.item?.isView || props?.item?.isStatus) ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
          value={rowData.purchaseForm ? rowData.purchaseForm?.displayName : ''}
        /> : <AsynchronousAutocompleteTransfer
          searchFunction={ShoppingFormSearchByPage}
          searchObject={searchObject}
          listData={listPurchaseForm || []}
          setListData={(data) => props.handleSetDataSelect(data, "listPurchaseForm")}
          displayLable={'name'}
          typeReturnFunction="category"
          value={rowData ? rowData?.purchaseForm : null}
          onSelect={props.selectPurchaseForm}
          onSelectOptions={rowData}
        />
      )
    },
    {
      title: t("Product.supplier"),
      field: "supplier",
      align: "left",
      minWidth: 180,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
      ((props?.item?.isView || props?.item?.isStatus) ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
          value={rowData.suppiler ? rowData.suppiler?.displayName : ''}
        /> : <AsynchronousAutocompleteTransfer
          searchFunction={SupplierSearchByPage}
          searchObject={searchObject}
          listData={listSuppiler || []}
          setListData={(data) => props.handleSetDataSelect(data, "listSuppiler")}
          displayLable={'name'}
          typeReturnFunction="category"
          value={rowData ? rowData?.suppiler : null}
          onSelect={props.selectSuppiler}
          onSelectOptions={rowData}
        />
      )
    },
    {
      title: t("maintainRequest.status"),
      field: "status",
      align: "center",
      minWidth: 180,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      }
    }    
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData =>
      // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  }
  
  return (
    <form>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} type="submit" />
            <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={1} justifyContent="space-between">
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <DateTimePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("purchasePlaning.planingDate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={'createDate'}
                  value={props?.item.planDate}
                  InputProps={{
                    readOnly: true,
                  }}
                  readOnly={true}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                  <span className="w-100">
                    <span className="colorRed">*</span>
                    {t("purchasePlaning.approvalDate")}
                  </span>
                }
                  name={'approvedDate'}
                  inputVariant="standard"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.item?.approvalDate}                  
                  onChange={date => props?.handleDateChange(date, "approvedDate")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  InputProps={{
                    readOnly: props?.item?.isView || props?.item?.isStatus
                  }}
                  open={isPickerOpen}
                  onOpen={() => {
                    if (props?.item?.isView || props?.item?.isStatus) {
                      setIsPickerOpen(false);
                    }
                    else {
                      setIsPickerOpen(true);
                    }
                  }}
                  onClose={() => setIsPickerOpen(false)}
                  validators={['required']}
                  errorMessages={t('general.required')}
                  minDate={new Date("01/01/1900")}
                  minDateMessage={"Ngày duyệt không được nhỏ hơn ngày 01/01/1900"}
                  maxDate={new Date()}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                  onBlur={() => handleBlurDate(props?.item.issueDate, "issueDate")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={6} xs={6}>
             <TextValidator
                className="w-100"
                label={
                    <span>
                      <span className="colorRed">* </span>
                      <span>{t("purchasePlaning.name")}</span>
                    </span>
                  }
                name="handoverPersonName"
                value={props?.item?.name}
              />
            </Grid>
              <Grid item md={4} sm={12} xs={12}>
              {props?.item?.isView || props?.item?.isStatus ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  value={transferStatus ? transferStatus?.name : ""}
                /> : <AsynchronousAutocompleteTransfer
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  searchFunction={getStatus}
                  searchObject={{}}
                  listData={props?.item?.listStatus || []}
                  setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                  defaultValue={transferStatus ? transferStatus : null}
                  value={transferStatus ? transferStatus : null}
                  displayLable={'name'}
                  onSelect={props.selectAssetTransferStatusStatus}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  typeReturnFunction="status"
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.item?.isStatus || props?.isRoleAssetUser) ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>
                  }
                  value={props?.item?.handoverDepartment ? props?.item?.handoverDepartment?.text : ''}
                /> : <AsynchronousAutocompleteTransfer
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>
                  }
                  searchFunction={getListManagementDepartment}
                  searchObject={searchObject}
                  listData={props?.item.listHandoverDepartment || []}
                  setListData={(data) => props?.handleSetDataSelect(data, "listHandoverDepartment")}
                  defaultValue={props?.item?.handoverDepartment ? props?.item?.handoverDepartment : ''}
                  displayLable={"text"}
                  value={props?.item?.handoverDepartment ? props?.item?.handoverDepartment : ''}
                  onSelect={handoverDepartment => props?.handleSelectHandoverDepartment(handoverDepartment)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.item?.isStatus) ?
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planer")}
                    </span>
                  }
                  name="handoverPerson"
                  value={props?.item?.handoverPersonName ?? ''}
                  InputProps={{
                    readOnly: props?.item?.isView || props?.item?.isStatus,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planer")}
                    </span>
                  }
                  disabled={!props?.item?.handoverDepartment?.id || props.item?.isView || props.item?.isStatus}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByHandoverDepartment}
                  defaultValue={props?.item?.handoverPerson ?? ''}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={props?.item?.handoverPerson ?? ''}
                  onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />}
            </Grid>
            {/* <Grid item md={4} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                name="totalEstimateCost"
                label={t("purchasePlaning.totalEstimatedCosts")}
                value={props?.item?.totalEstimatedCost}
              />
            </Grid> */}
            <Grid item md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                name="note"
                label={t("maintainRequest.note")}
                value={props?.item?.note}
              />
            </Grid>
            </Grid>
          <Grid spacing={2}>
            <MaterialTable
              style={{marginTop: 10}}
              data={props?.item?.assetVouchers ? props?.item?.assetVouchers : []}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF',
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props?.item?.documents || []}
              columns={columnsVoucherFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF',
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '285px',
                minBodyHeight: '285px',
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div >
    </form>
  );
}