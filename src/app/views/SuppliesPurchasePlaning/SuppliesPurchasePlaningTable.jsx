import {
  AppBar,
  Icon,
  IconButton,
  Tab,
  Tabs,
} from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  TabPanel,
  classStatus,
  convertFromToDate,
  convertNumberPriceRoundUp,
  formatDateDto,
  isValidDate, getTheHighestRole, formatTimestampToDate
} from "app/appFunction";
import { Breadcrumb } from "egret";
import FileSaver from "file-saver";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import ComponentSuppliesPurchasePlaningTable from "./ComponentSuppliesPurchasePlaningTable";
import {
  deletePurchasePlaningById,
  getCountStatus,
  getListProductByIds,
  getPurchasePlaningById,
  searchByPage,
  supplierPuchasePlanningExportToExcel,
} from "./SuppliesPurchasePlaningService";
import { formatDateDtoMore } from "app/appFunction";
import clsx from "clsx";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let {
    hasDeletePermission,
    isRoleAdmin,
  } = props;
  const isDangDuyetDanhMuc = item?.status
    === appConst.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder
  const isDangXuLy = item?.status
    === appConst.listStatusPurchasePlaningObject.DANG_XU_LY.indexOrder
  const isDaDuyetBaoGia = item?.status
    === appConst.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder


  return (
    <div className="none_wrap">
      {(isDangXuLy || isDangDuyetDanhMuc || isDaDuyetBaoGia)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission &&
        (item?.isOwner || isRoleAdmin) &&
        (isRoleAdmin
          ? isRoleAdmin
          : (item?.transferStatusIndex ===
            appConst.listStatusTransfer[0].indexOrder)) && (
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        )}
      <LightTooltip
        title={t("InstrumentToolsTransfer.watchInformation")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <VisibilityIcon size="small" className="iconEye" />
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class SuppliesPurchasePlaningTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    status: null,
    fromPlanDate: null,
    toPlanDate: null,
    tabValue: 0,
    isRoleAdmin: false,
    isRoleAssetManager: false,
    isCheckReceiverDP: true,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataPuschasePlaningTable: [],
    rowDataClick: null,
    type: appConst.TYPE_PURCHASE.VT,
  };
  numSelected = 0;
  rowCount = 0;
  type = ConstantList.VOUCHER_TYPE.Transfer;

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });

    const statusMap = {
      [appConst.tabPurchasePlaning.tabAll]: null,
      [appConst.tabPurchasePlaning.tabPlan]: appConst.tabPurchasePlaning.tabPlan,
      [appConst.tabPurchasePlaning.tabProcessed]: appConst.tabPurchasePlaning.tabProcessed,
      [appConst.tabPurchasePlaning.tabQuoteApproved]: appConst.tabPurchasePlaning.tabQuoteApproved,
      [appConst.tabPurchasePlaning.tabEnd]: appConst.tabPurchasePlaning.tabEnd,
    };
    this.search({ status: statusMap[newValue] });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  setPage = (page) => {
    this.search({ page });
  };

  setRowsPerPage = (event) => {
    this.search({ rowsPerPage: event.target.value, page: 0 });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = (searchObject) => {
    const newObject = searchObject ? { ...searchObject } : { page: 0 };
    this.setState({ ...newObject, rowDataClick: null }, () => this.updatePageData())
  }

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim() || '';
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.status = this.state?.status;
    searchObject.fromPlanDate = formatDateDtoMore(this.state?.fromPlanDate, "start");
    searchObject.toPlanDate = formatDateDtoMore(this.state?.toPlanDate);
    searchObject.type = appConst.TYPE_PURCHASE.VT;
    searchByPage(searchObject)
      .then(({ data }) => {
        this.setState(
          {
            rowDataClick: null,
            itemList: [...data?.data?.content],
            totalElements: data?.data?.totalElements,
            rowDataPuschasePlaningTable: []
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isPrint: false,
      isView: false
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isView: false
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    deletePurchasePlaningById(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(this.props.t("general.deleteSuccess"));
          this.handleDialogClose();
          this.updatePageData();
        } else {
          toast.warning(data?.message);
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    this.search({ status: null });
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCreatePermission,
      hasPrintPermission,
    } = this.state;
    const roles = getTheHighestRole();
    const {
      isRoleAssetUser,
      isRoleAssetManager,
      isRoleOrgAdmin,
      isRoleAdmin,
    } = roles;

    hasDeletePermission = isRoleOrgAdmin || isRoleAdmin || isRoleOrgAdmin || isRoleAssetUser;
    hasEditPermission = isRoleOrgAdmin || isRoleAdmin || isRoleOrgAdmin || isRoleAssetUser;
    hasWatchPermission = isRoleOrgAdmin || isRoleAdmin || isRoleOrgAdmin || isRoleAssetUser;
    hasCreatePermission = isRoleOrgAdmin || isRoleAdmin || isRoleOrgAdmin || isRoleAssetUser;
    hasPrintPermission = isRoleOrgAdmin || isRoleAdmin || isRoleOrgAdmin;

    this.setState({
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCreatePermission,
      hasPrintPermission,
      isRoleAdmin,
      isRoleOrgAdmin,
      isRoleAssetManager
    });
  };

  handleButtonAdd = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deletePurchasePlaningById(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  setStateTransfer = (item) => {
    this.setState(item);
  };

  handleEdit = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          isView: false,
          item: data?.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleView = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          isView: true,
          item: data?.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handlePrint = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          item: data?.data,
          isPrint: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.edit === method) {
      this.handleEdit(rowData?.id);
    } else if (appConst.active.delete === method) {
      this.handleDelete(rowData?.id);
    } else if (appConst.active.view === method) {
      this.handleView(rowData?.id);
    } else if (appConst.active.print === method) {
      this.handlePrint(rowData?.id);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0
  }

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus(this.state.type)
      .then(({ data }) => {
        let countStatusProcessing,
          countStatusApprovedCategory,
          countStatusApproved,
          countStatusEnd;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusVerificationPlanning[0].code === item?.trangThai) {
            countStatusProcessing = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[1].code === item?.trangThai) {
            countStatusApprovedCategory = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[2].code === item?.trangThai) {
            countStatusApproved = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[3].code === item?.trangThai) {
            countStatusEnd = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusProcessing,
            countStatusApprovedCategory,
            countStatusApproved,
            countStatusEnd
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  checkStatus = (status, name) => {
    let itemStatus = appConst[name].find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  handleGetRowData = async (rowData) => {
    let { t } = this.props
    try {
      const res = await getListProductByIds({
          ...appConst.OBJECT_SEARCH_MAX_SIZE,
          purchasePlanId: rowData?.id
      })
      if (res?.data?.code === appConst?.CODE?.SUCCESS) {
        this.setState({
          rowDataPuschasePlaningTable: res?.data?.data?.content ?? [],
          rowDataClick: rowData
        })
      } else {
        toast.error(t("toastr.error"))
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    }

  }

  handleDateChange = (date, name) => {
    this.setState({ [name]: formatDateDto(date) },
      () => {
        if (isValidDate(date) || date === null) {
          this.search()
        }
      });
  };

  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromPlanDate,
      toPlanDate,
      keyword,
      status
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        type: appConst.TYPE_PURCHASE.VT,
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      };

      if (fromPlanDate) {
        searchObject.fromPlanDate = convertFromToDate(new Date(fromPlanDate)).fromDate;
      }
      if (toPlanDate) {
        searchObject.toPlanDate = convertFromToDate(new Date(toPlanDate)).toDate;
      }
      if (keyword) {
        searchObject.keyword = keyword;
      }
      if (status) {
        searchObject.status = status;
      }

      const res = await supplierPuchasePlanningExportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "Kế hoạch mua sắm vật tư.xlsx");
        toast.success(t("general.successExport"));
      }
    }
    catch (error) {
      toast.error(t("general.failExport"));
    }
    finally {
      setPageLoading(false);
    }
  };

  handleGetProductStatus = (rowData) => {
    const { DA_DUYET, CHO_DUYET, KHONG_DUYET } = appConst.listStatusProductInPurchaseRequest;
    const statusIndex = rowData?.status;
    const classByStatusIndex = {
      [CHO_DUYET.code]: "status-warning",
      [DA_DUYET.code]: "status-success",
      [KHONG_DUYET.code]: "status-error",
      [null]: "",
    }
    const nameByStatusIndex = {
      [CHO_DUYET.code]: CHO_DUYET.name,
      [DA_DUYET.code]: DA_DUYET.name,
      [KHONG_DUYET.code]: KHONG_DUYET.name,
      [null]: "",
    }

    return (
      <span className={clsx("status", classByStatusIndex[statusIndex])}>
        {nameByStatusIndex[statusIndex]}
      </span>
    )
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasPrintPermission,
      isRoleAdmin,
      isRoleAssetManager,
      rowDataPuschasePlaningTable,
      rowDataClick,
      isRoleOrgAdmin
    } = this.state;
    let TitlePage = t("purchasePlaning.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        minWidth: "180px",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasWatchPermission={hasWatchPermission}
            hasPrintPermission={hasPrintPermission}
            isRoleAdmin={isRoleAdmin || isRoleOrgAdmin}
            isRoleAssetManager={isRoleAssetManager}
            onSelect={(rowData, method) =>
              this.handleCheckIcon(rowData, method)
            }
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("SuggestedMaterials.ballotCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
      },
      {
        title: t("purchaseRequestCount.name"),
        field: "name",
        align: "left",
        minWidth: "150px",
      },
      {
        title: t("purchasePlaning.planingDate"),
        field: "planDate",
        minWidth: 150,
        align: "center",
        render: (rowData) =>
          rowData?.planDate
            ? (<span>{moment(rowData?.planDate).format("DD/MM/YYYY")}</span>)
            : (""),
      },
      {
        title: t("purchasePlaning.approvalDate"),
        field: "approvedDate",
        minWidth: 150,
        align: "center",
        render: (rowData) => formatTimestampToDate(rowData?.approvedDate) || "",
      },
      {
        title: t("purchasePlaning.quoteApprovedDate"),
        field: "approvedDate",
        minWidth: 150,
        align: "center",
        render: (rowData) => formatTimestampToDate(rowData?.quotationApprovalDate) || "",
      },
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        minWidth: 180,
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span
              className={classStatus(statusIndex)}
            >
              {this.checkStatus(statusIndex, "listStatusPurchasePlaning")}
            </span>
          )
        }
      },
      {
        title: t("purchasePlaning.name"),
        field: "name",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.planingDepartment"),
        field: "planDepartmentName",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.planer"),
        field: "planPersonName",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.totalEstimatedCosts"),
        field: "totalEstimatedCost",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: 200,
        render: (rowData) => (convertNumberPriceRoundUp(rowData?.totalEstimatedCost || 0))
      },
      {
        title: t("purchasePlaning.note"),
        field: "note",
        minWidth: 200,
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: '50px',
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "productName",
        minWidth: 250,
      },
      {
        title: t("SparePart.unit"),
        field: "skuName",
        align: "center",
        minWidth: 70,
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "center",
        minWidth: 90,
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "center",
        minWidth: 90,
      },
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.estimatedUnitPrice"),
          field: "estimatedUnitPrice",
          align: "right",
          minWidth: 200,
          render: (rowData) => convertNumberPriceRoundUp(rowData?.estimatedUnitPrice || 0)
        },
      ] : []),
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.estimatedCosts"),
          field: "estimatedCosts",
          align: "right",
          minWidth: 200,
          render: (rowData) => (
            convertNumberPriceRoundUp(rowData.estimatedUnitPrice * rowData.approvedQuantity)
          ),
        },] : []),
      {
        title: t('Product.formShopping'),
        field: "shoppingFormName",
        minWidth: 200,
      },
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.supplier"),
          field: "supplierName",
          minWidth: 200,
        },] : []),
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        hidden: rowDataClick?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder,
        minWidth: 150,
        render: this.handleGetProductStatus,
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.purchase"), path: "/list/purchase_planing" },
              { name: t("Dashboard.Purchase.purchase_supplies") },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("purchasePlaning.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>
                    <span>{t("purchasePlaning.processing")}</span>
                    <div className="tabQuantity tabQuantity-warning">
                      {this.state?.countStatusProcessing || 0}
                    </div>
                  </span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.approved")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusApprovedCategory || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.quoteApproved")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusApproved || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.finished")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countStatusEnd || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <TabPanel
          value={tabValue}
          index={appConst.tabPurchasePlaning.tabAll}
          className="mp-0"
        >
          <ComponentSuppliesPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchasePlaning.tabPlan}
          className="mp-0"
        >
          <ComponentSuppliesPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchasePlaning.tabProcessed}
          className="mp-0"
        >
          <ComponentSuppliesPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchasePlaning.tabQuoteApproved}
          className="mp-0"
        >
          <ComponentSuppliesPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchasePlaning.tabEnd}
          className="mp-0"
        >
          <ComponentSuppliesPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataPuschasePlaningTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '350px',
              minBodyHeight: '260px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}
SuppliesPurchasePlaningTable.contextType = AppContext;
export default SuppliesPurchasePlaningTable;
