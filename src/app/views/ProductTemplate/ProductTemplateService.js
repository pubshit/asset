import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/product_template" + ConstantList.URL_PREFIX;
const API_PATH_ProductAttribute = ConstantList.API_ENPOINT + "/api/productAttribute/" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const searchByText = (text, pageIndex, pageSize) => {
  var url = API_PATH + "/searchByText/"+ pageIndex + '/' +  pageSize;
  return axios.post(url, {keyword: text});
};

export const getByPage = (page, pageSize) => {
  var pageIndex = page + 1;
  var params = '/'+ pageIndex + "/" + pageSize;
  var url = API_PATH + params;
  return axios.get(url);
};

export const getItemById = id => {
  var url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = id => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = item => {
  var url = API_PATH;
  return axios.post(url, item);
};

export const getAllProperties = () => {
  return axios.get(API_PATH_ProductAttribute + "/1/100000");  
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const deleteCheckItem = id => {
  return axios.delete(API_PATH  + "/delete/" + id);
};