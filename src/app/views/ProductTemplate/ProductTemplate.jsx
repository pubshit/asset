import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { useTranslation } from "react-i18next";
import {
  getItemById,
  searchByPage,
  deleteCheckItem,
} from "./ProductTemplateService";
import ProductTemplateDialog from "./ProductTemplateDialog";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import Tooltip from "@material-ui/core/Tooltip";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import localStorageService from "app/services/localStorageService";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { withRouter } from "react-router-dom";
import { defaultPaginationProps, handleKeyUp } from "../../appFunction";
toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: "-10px",
    left: "-25px",
    width: "80px",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const { hasDeletePermission } = props
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      {
        hasDeletePermission && <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}
function PropertyAtt(props) {
  let attributeName = "";
  let index = 0;
  if (
    props.item != null &&
    props.item.properties != null &&
    props.item.properties.length > 0
  ) {
    props.item.properties.forEach((a) => {
      if (a.attribute.name != null) {
        attributeName += a.attribute.name;
        if (index < props.item.properties.length - 1) {
          attributeName += ", ";
        }
        index++;
      }
    });
  }
  return attributeName;
}
class ProductTemplate extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    Notification: "",
    hasDeletePermission: false
  };
  constructor(props) {
    super(props);
    //this.state = {keyword: ''};
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }
  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  search = () => {
    this.setPage(0);
  }

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser()
  }
  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    try {
      setPageLoading(true)
      const data = await searchByPage(searchObject);
      if (data?.status === appConst.CODE.SUCCESS) {
        this.setState({
          itemList: [...data?.data?.content],
          totalElements: data?.data?.totalElements,
        });
      }
    } catch (error) {

    } finally {
      setPageLoading(false)
    }
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };
  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
      },
      () => {
        this.updatePageData();
      }
    );
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, () => {
      this.search();
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    deleteCheckItem(this.state.id)
      .then((res) => {
        toast.info(this.props.t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      })
      .catch((err) => {
        toast.info(this.props.t("ProductTemplate.noti.use"));
      });
  };
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser.roles.forEach((role) => {
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
      });
    }
  };

  handleKeyUpSearch = e => handleKeyUp(e, this.search);

  render() {
    const { t, i18n } = this.props;
    let { keyword, hasDeletePermission } = this.state;
    let TitlePage = t("ProductTemplate.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "100px",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data.properties === null) {
                    data.properties = [];
                  }
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  });
                });
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("ProductTemplate.name"),
        field: "name",
        width: "300px",
        headerStyle: {
          paddingRight: "10px",
        },
        cellStyle: {
          paddingRight: "10px",
        },
      },
      {
        title: t("ProductTemplate.code"),
        field: "code",
        align: "left",
        width: "180px",
        headerStyle: {
          paddingRight: "10px",
        },
        cellStyle: {
          paddingRight: "10px",
        },
      },
      {
        title: t("ProductTemplate.properties"),
        field: "",
        align: "left",
        width: "140",
        headerStyle: {
          paddingRight: "10px",
        },
        cellStyle: {
          paddingRight: "10px",
        },
        render: (rowData) => <PropertyAtt item={rowData} />,
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.category"), path: "/list/product_template" },
              { name: TitlePage },
            ]}
          />
        </div>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item container md={3} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.handleEditItem(null)}
            >
              {t("general.add")}
            </Button>
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUpSearch}
                placeholder={t("ProductTemplate.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#">
                      <SearchIcon
                        onClick={() => this.search(keyword)}
                        style={{ position: "absolute", top: "0", right: "0" }}
                      />
                    </Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {this.state.shouldOpenEditorDialog && (
                <ProductTemplateDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={this.state.item}
                />
              )}

              {this.state.shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={this.state.shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={this.state.itemList}
              columns={columns}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
// export default ProductTemplate;

ProductTemplate.contextType = AppContext;
export default withRouter(ProductTemplate);

