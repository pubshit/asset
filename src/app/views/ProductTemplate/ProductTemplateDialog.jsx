import {
  Grid,
  DialogActions,
  Button,
  Dialog,
  IconButton,
  Icon,
} from "@material-ui/core";
import React from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { useTranslation } from "react-i18next";
import { getByPage, saveItem, checkCode } from "./ProductTemplateService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import {
  deleteItem,
} from "../ProductAttribute/ProductAttributeService";
import ProductAttributePopup from "./ProductAttributePopup";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { handleKeyDownNameSpecialExcept } from "app/appFunction";
import {appConst, variable} from "../../appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}
class ProductTemplateDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    totalElements: 0,
    shouldOpenProductAttributePopup: false,
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    keyword: "",
    properties: [],
    code: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
    this.updatePageData();
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };
  updatePageData = () => {
    var searchObject = {};
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };
  componentDidMount() {
  }

  componentWillMount() {
    let { item } = this.props;
    this.setState(item);
  }
  search = (keyword) => {
    var searchObject = {};
    searchObject.text = keyword;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  handleChange(event) {
    this.setState({ keyword: event.target.value });
  }
  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };
  handleChangeCode = (event) => {
    this.setState({ code: event.target.value });
  };
  handleProductAttributePopupClose = () => {
    this.setState({
      shouldOpenProductAttributePopup: false,
    });
  };
  handleFormSubmit = (e) => {
    if (e?.target?.id !== "product-template-dialog") return;
    let { id } = this.state;
    let { code } = this.state;
    let { properties } = this.state;
    if (properties.length === 0) {
      toast.warning("Vui lòng chọn thuộc tính sản phẩm.");
      toast.clearWaitingQueue();
      return;
    }

    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(this.props.t("ProductTemplate.noti.dupli_code"));
        toast.clearWaitingQueue();
      } else {
        if (id) {
          saveItem({
            ...this.state,
          }).then(() => {
            toast.info(this.props.t('general.updateSuccess'))
            this.props.handleOKEditClose();
          });
        } else {
          saveItem({
            ...this.state,
          }).then(() => {
            toast.info(this.props.t('general.addSuccess'))
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };
  handleSelectProperties = (items) => {
    if (items != null && items.length > 0) {
      this.setState({ properties: items }, function () {
        this.handleProductAttributePopupClose();
      });
    }
  };
  selectProductAttribute = (attribute) => {
    this.setState({ properties: attribute }, function () { });
  };
  render() {
    const { t, i18n, handleClose, open } = this.props;
    let {
      keyword,
      name,
      code,
      defaultValue,
      properties,
      shouldOpenProductAttributePopup,
      shouldOpenNotificationPopup,
    } = this.state;
    let index = properties.map((item, index) => index);

    let columns = [
      {
        title: t("general.index"),
        field: " ",
        align: "left",
        width: "250",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("ProductAttribute.name"),
        field: "attribute.name",
        width: "150",
      },
      {
        title: t("ProductAttribute.code"),
        field: "attribute.code",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center",
        },
      },

      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        headerStyle: {
          padding: "0px",
        },
        cellStyle: {
          padding: "0px",
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
              } else if (method === 1) {
                for (let index = 0; index < properties.length; index++) {
                  const item = properties[index];
                  if (
                    rowData.attribute &&
                    item.attribute &&
                    rowData.attribute.id === item.attribute.id
                  ) {
                    if (rowData.id) {
                      deleteItem(rowData.id).then(({ data }) => { });
                    }
                    properties.splice(index, 1);
                    this.setState({ properties });
                    break;
                  }
                }
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
          id="product-template-dialog"
        >
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            {t("ProductTemplate.saveUpdate")}
          </DialogTitle>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={4} sx={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductTemplate.code")}
                    </span>
                  }
                  onChange={this.handleChangeCode}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>

              <Grid item md={8} sm={12} sx={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductTemplate.name")}
                    </span>
                  }
                  onChange={this.handleChangeName}
                  onKeyDown={(e) => {
                    handleKeyDownNameSpecialExcept(e)
                  }}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item md={12} sm={12} sx={12}>
                <Button
                  className=" mt-10"
                  variant="contained"
                  color="primary"
                  onClick={() =>
                    this.setState({
                      shouldOpenProductAttributePopup: true,
                      item: {},
                    })
                  }
                >
                  {t("ProductAttribute.title")}
                </Button>
                {shouldOpenProductAttributePopup && (
                  <ProductAttributePopup
                    open={shouldOpenProductAttributePopup}
                    handleSelect={this.handleSelectProperties}
                    selectedItem={properties != null ? properties : []}
                    handleClose={this.handleProductAttributePopupClose}
                    t={t}
                    i18n={i18n}
                  />
                )}
              </Grid>
              <Grid item sm={12} sx="12">
                <MaterialTable
                  title={t("ProductAttribute.title")}
                  data={properties}
                  columns={columns}
                  options={{
                    sorting: false,
                    selection: false,
                    actionsColumnIndex: 0,
                    paging: false,
                    search: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "253px",
                    minBodyHeight: "253px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                    padding: "dense",
                    toolbar: false,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-15"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
export default ProductTemplateDialog;
