import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';

const AssetRepair = EgretLoadable({
  loader: () => import("./AssetRepair")
});
const ViewComponent = withTranslation()(AssetRepair);

const AssetRepairRoutes = [
  {
    path: ConstantList.ROOT_PATH + "asset/maintain_request",
    exact: true,
    component: ViewComponent
  }
];

export default AssetRepairRoutes;