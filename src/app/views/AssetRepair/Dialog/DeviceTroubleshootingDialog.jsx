import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
  IconButton, Menu, MenuItem,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import {
  BO_PHAN_CAN_SUA,
  STATUS_REPORT,
  appConst,
  formatDate,
  TRACKER_CODE,
  ROUTES_PATH,
  variable, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL
} from "app/appConst";
import AppContext from "app/appContext";
import { deleteArAttachmentById, getArAttachmentById } from "app/views/Asset/AssetService";
import { ConfirmationDialog } from "egret";
import MaterialTable from "material-table";
import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AssetAcceptanceForm from "../Component/AssetAcceptanceForm";
import AttachmentPopup from "../Component/AttachmentPopup";
import RateIncidentForm from "../Component/RateIncidentForm";
import ReportIncidentForm from "../Component/ReportIncidentForm";
import {
  checkTotalCost,
  createArSuCo,
  getDetailArSuCo,
  updateArSuCo,
  updateArSuCoNghiemThu,
  updateArSuCoTiepNhan,
} from "../AssetRepairService";
import moment from "moment";
import {
  PaperComponent,
  TabPanel,
  a11yProps,
  getTheHighestRole,
  LightTooltip,
  isSuccessfulResponse,
  handleThrowResponseMessage,
  getUserInformation, convertFromToDate, isInValidForSubmit,
} from "app/appFunction";
import { PrintMultipleFormDialog } from "../../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import { LIST_PRINT_FORM_BY_ORG } from "../../FormCustom/constant";
import CustomValidatorForm from "app/views/Component/ValidatorForm/CustomValidatorForm";
import {
  convertBM01PrintData,
  convertBM0606PrintData,
  convertBM0607PrintData,
  convertKTTS01PrintData
} from "../../FormCustom/Repair";
import WfButton from "../../workflows/Button";
import { getWfWorkflow } from "../../workflows/services";
import { TRACKER_STATUS_CODE } from "../../workflows/constant";
import { getWfFields } from "../../ApprovalRequest/ApprovalRequestService";
import ApprovalRequestDialog from "../../ApprovalRequest/ApprovalRequestDialog";
import ApprovalProcessHistoryPopup from "../../Component/SubmissionForm/ApprovalProcessHistoryPopup";
import { defaultPermission } from "../constant";
import { PrintPreviewTemplateDialog } from "../../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function DeviceTroubleshootingDialog(props) {
  let { setPageLoading, currentOrg } = useContext(AppContext);
  let { departmentUser, currentUser, addressOfEnterprise } = getUserInformation();
  let { isRoleAssetManager, isRoleAssetUser, isRoleAdmin, isRoleUser } = getTheHighestRole();
  let organization = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
  const { REPAIRING } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
  let {
    open,
    t,
    i18n,
    handleClose = () => { },
    itemEdit = {},
    setItemEdit,
    isAdd,
    isView
  } = props;
  const form = useRef();
  const [isSubmitNghiemThuSuCo, setIsSubmitNghiemThuSuCo] = useState(false);
  const [isAllowedSubmit, setIsAllowedSubmit] = useState(true);
  const [tabDeviceTroubleshooting, setTabDeviceTroubleshooting] = React.useState(
    appConst.tabDeviceTroubleshootingDialog.INFO.valueTab
  );
  const [arTrangThai, setArTrangThai] = useState(null);
  const [arTrangThaiMoTa, setArTrangThaiMoTa] = useState("");
  const [disable, setDisable] = useState(false)
  const [state, setState] = useState({
    reportDate: moment().format(formatDate),
    status: "",
    arTnPhongBanId: "",
    arTnPhongBanText: "",
    arTnPhongBan: null,
    arTgBaoCao: moment().format(formatDate),
  });
  const [itemTiepNhan, setItemTiepNhan] = useState({ arLinhKienVatTus: [], ...itemEdit });
  const [openBM01, setOpenBM01] = useState(false);
  const [openBM0601, setOpenBM0601] = useState(false);
  const [openBM0607, setOpenBM0607] = useState(false);
  const [openBM0606, setOpenBM0606] = useState(false);
  const [openBMKTTS01, setOpenBMKTTS01] = useState(false);
  const [openMAU_DNMTS, setOpenMAU_DNMTS] = useState(false);
  const [dataNghiemThuSuCo, setDataNghiemThuSuCo] = useState({});
  const [totalCost, setTotalCost] = useState(0);

  const objConfirmCost = {
    text: t("ReportIncident.AssetAcceptanceForm.confirmTotalCost"),
    agree: t("general.saveAndNext"),
    cancel: t("general.edit"),
    isConfirmCheckingCost: true
  }
  const objConfirmClose = {
    text: t("ReportIncident.AssetAcceptanceForm.confirmClose"),
    agree: t("general.confirm"),
    cancel: t("general.edit"),
    isConfirmCheckingCost: false
  }
  const [shouldOpenConfirmObj, setShouldOpenConfirmObj] = useState({
    open: false,
    ...objConfirmCost
  });
  const isEdit = (itemEdit?.arTrangThai ?
    (departmentUser?.id === itemTiepNhan?.tsPhongBanId || departmentUser?.id === itemTiepNhan?.arTnPhongBanId) &&
    itemEdit?.arTrangThai === STATUS_REPORT.WAITING :
    true);
  const [idArSuCo, setIdArSuCo] = useState("");
  const [documents, setDocuments] = useState({})
  const [listAssetDocumentIdState, setListAssetDocumentId] = useState([]);
  const [item, setItem] = useState({});
  const [listDocument, setListDocument] = useState([]);
  const [shouldOpenPopupAssetFile, setShouldOpenPopupAssetFile] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [listPrintArSucoOptions, setListPrintArSucoOptions] = useState(appConst.listPrintArSucoOptions);
  const [dataArSuCo, setDataArSuCo] = useState({
    arPhongBanText: "..".repeat(20),
    addressOfEnterprise,
    currentUser,
    organization,
    titleName: organization,
    organizationName: organization?.org?.name,
  })
  const [arLinhKienVatTus, setArLinhKienVatTus] = useState([])
  const [workflows, setWorkflows] = useState([]);
  const [wfFields, setWfFields] = useState(null);
  const [shouldOpenSubmissionForm, setShouldOpenSubmissionForm] = useState(false);
  const [shouldOpenApprovalHistoryPopup, setShouldOpenApprovalHistoryPopup] = useState(false);
  const [permission, setPermission] = useState({ ...defaultPermission });
  const [display, setDisplay] = useState({
    shouldOpenPreviewTemplatePrint: false,
  });
  const [currentWf, setCurrentWf] = useState(null);

  const isTabAttachment = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab;
  const isTabInfo = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab;
  const isTabAcceptance = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab;
  const isTabEvaluate = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab;

  useEffect(() => {
    let { MAU_DNMTS } = appConst.PRINT_AR_SUCO_OPTIONS;
    const {
      PROCESSED,
    } = TRACKER_STATUS_CODE.REPAIR;
    if (isRoleAssetUser || isRoleUser) {
      setDisable(isTabEvaluate || isTabAcceptance);
    } else {
      // setDisable(arTrangThaiMoTa === PROCESSED.code && !isTabAttachment)
    }

    if (itemEdit?.arTrangThaiMoTa !== PROCESSED.code) {
      setListPrintArSucoOptions(listPrintArSucoOptions.filter(({ code }) =>
        ![PROCESSED.code, MAU_DNMTS?.code].includes(code)
      ));
    } else if (itemEdit?.arHinhThuc === appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code) {
      setListPrintArSucoOptions(appConst.listPrintArSucoOptions)
    } else {
      setListPrintArSucoOptions(listPrintArSucoOptions.filter(i => i.code !== MAU_DNMTS?.code))
    }
  }, [itemEdit?.arTrangThaiMoTa, arTrangThaiMoTa, tabDeviceTroubleshooting]);

  useEffect(() => {
    let newPermission = { ...permission };
    if (itemEdit?.id && isTabInfo) {
      let newItem = {
        ...itemEdit,
        arTnPhongBan: itemEdit?.arTnPhongBanId
          ? {
            id: itemEdit?.arTnPhongBanId,
            name: itemEdit?.arTnPhongBanText,
          }
          : null
      }

      setIdArSuCo(itemEdit.id);
      setItemTiepNhan(newItem)
      setState({ ...newItem });
      setListDocument(itemEdit?.arAttachments);
      setArTrangThai(itemEdit?.arTrangThai);
      setArTrangThaiMoTa(itemEdit?.arTrangThaiMoTa);
    }

    if (isView) {
      Object.keys(permission)?.forEach(key => {
        newPermission[key].isView = true;
        newPermission[key].disabled = false;
      })
    } else {
      Object.keys(permission)?.forEach(key => {
        newPermission[key].isView = false;
      })
      handleGetWorkFlow();
    }

    setPermission(newPermission);
    if (props?.isFromApprovalRequest) {
      setDisable(props?.isFromApprovalRequest);
    }
  }, [itemEdit?.id]);

  const handleGetWorkFlow = async (objectId, trackerCode) => {
    try {
      const params = {
        trackerCode: trackerCode || TRACKER_CODE.REPAIR,
        objectId: itemEdit?.id || state?.id || itemTiepNhan?.id || objectId,
      }
      let res = await getWfWorkflow(params);
      let { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setWorkflows(data);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  const getAssetDocument = async (document) => {
    let listAssetDocumentId = listAssetDocumentIdState ? listAssetDocumentIdState : [];
    let documents = listDocument ? listDocument : [];
    if (document?.id) {
      listAssetDocumentId.push(document?.id);
      documents.push(document);
    }
    setListAssetDocumentId(listAssetDocumentId)
    setListDocument(documents)
    try {
      const attachmentIds = documents?.map((item) => {
        return item?.id
      })
      setPageLoading(true);
      if (arTrangThai === STATUS_REPORT.WAITING) {
        const result = await updateArSuCo({ ...state, arAttachmentIds: attachmentIds });
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"));
          setState((pre) => ({ ...pre, ...result?.data?.data }))
        } else {
          toast.error(result?.data?.message);
        }
      }
      else if (arTrangThai === STATUS_REPORT.PROCESSING) {
        const result = await updateArSuCoTiepNhan({ ...itemTiepNhan, arAttachmentIds: attachmentIds });
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"));
          setState((pre) => ({ ...pre, ...result?.data?.data }))
        } else {
          toast.error(result?.data?.message);
        }
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const handleUpdateAssetDocument = (document) => {
    let documents = listDocument
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    setListDocument(documents);
  };

  const handleAddAssetDocumentItem = () => {
    setDocuments({})
    setShouldOpenPopupAssetFile(true)
    setItem({ isView: false, isEditAssetDocument: false });
  };

  const handleRowDataCellEditAssetFile = (rowData) => {
    getArAttachmentById(rowData.id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        setItem({ isView: false, isEditAssetDocument: true });
        setDocuments(data?.data);
        setShouldOpenPopupAssetFile(true);
      } else {
        toast.error(t("general.error"))
      }
    }).catch(() => {
      toast.error(t("general.error"))
    });
  };

  const handleRowDataCellViewAssetFile = (rowData) => {
    getArAttachmentById(rowData.id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        setItem({ isView: true, isEditAssetDocument: false });
        setDocuments(data?.data);
        setShouldOpenPopupAssetFile(true);
      } else {
        toast.error(t("general.error"))
      }
    }).catch(() => {
      toast.error(t("general.error"))
    });
  };

  const handleRowDataCellDeleteAssetFile = async (id) => {

    try {
      const result = await deleteArAttachmentById(id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        const newListDocument = [...listDocument];
        const newListAssetDocumentId = [...listAssetDocumentIdState]
        let index = listDocument?.findIndex(document => document?.id === id);
        let indexId = listAssetDocumentIdState?.findIndex(documentId => documentId === id);
        newListDocument.splice(index, 1);
        newListAssetDocumentId.splice(indexId, 1);
        setListDocument(newListDocument)
        setListAssetDocumentId(newListAssetDocumentId)
        toast.success(t("general.success"))
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  const handleAssetFilePopupClose = () => {
    setShouldOpenPopupAssetFile(false)
  };

  const handleChangeTabsDeviceTroubleshooting = (event, newValue) => {
    const { INFO, ATTACHMENTS, EVALUATE, ACCEPTANCE } = appConst.tabDeviceTroubleshootingDialog;
    const {
      WAITING,
      PROCESSING,
      PENDING_APPROVAL,
      APPROVAL_HISTORY,
      PROCESSED,
      APPROVAL,
      REQUEST_FOR_ADDITIONAL_INFORMATION
    } = TRACKER_STATUS_CODE.REPAIR;
    const fullTabsAccessPermission = [INFO.valueTab, EVALUATE.valueTab, ATTACHMENTS.valueTab, ACCEPTANCE.valueTab];
    const validStatus = {
      [""]: [INFO.valueTab],
      [WAITING.code]: [INFO.valueTab, EVALUATE.valueTab, ATTACHMENTS.valueTab],
      [PROCESSING.code]: fullTabsAccessPermission,
      [PENDING_APPROVAL.code]: fullTabsAccessPermission,
      [APPROVAL_HISTORY.code]: fullTabsAccessPermission,
      [PROCESSED.code]: fullTabsAccessPermission,
      [APPROVAL.code]: fullTabsAccessPermission,
      [REQUEST_FOR_ADDITIONAL_INFORMATION.code]: fullTabsAccessPermission,
    }

    if ((isRoleAssetUser || isRoleUser) && newValue === INFO.valueTab) {
      setDisable(false);
    }
    if (newValue) {
      setTabDeviceTroubleshooting(newValue);
      handleCheckPermissionOnChagneTab();
    } else if (isTabAttachment) {
      setTabDeviceTroubleshooting(INFO.valueTab)
    } else {
      setTabDeviceTroubleshooting(tabDeviceTroubleshooting + 1)
    }
  };

  const handleUpdateArSuCo = async (props) => {
    if (isInValidForSubmit()) { return; }

    try {
      setPageLoading(true);
      const sendData = {
        ...state,
        objectId: props?.status?.objectId || state?.id || null,
        id: props?.status?.objectId || state?.id || null,
        arTrangThai: props?.statusNew?.statusId || state?.arTrangThai,
        arTnPhongBanId: state?.arTnPhongBan?.id,
        arTnPhongBanText: state?.arTnPhongBanText,
        assetClass: appConst.assetClass.TSCD,
      };
      let res;
      if (props?.statusNew?.statusId) {
        res = state?.id ? await updateArSuCo(sendData) : await createArSuCo(sendData);
      } else {
        res = await props?.submit?.(sendData);
      }
      const { code, data } = res?.data;

      if (!isSuccessfulResponse(code)) {
        handleThrowResponseMessage(res);
        return;
      }
      toast.success(t(!state.id ? "general.addSuccess" : "general.updateSuccess"));
      setItemEdit(data);
      setItemTiepNhan(data);
      setState({
        ...data,
        arTnPhongBan: data?.arTnPhongBanId
          ? {
            id: data?.arTnPhongBanId,
            name: data?.arTnPhongBanText
          }
          : null,
      });
      setArTrangThai(props?.statusNew?.statusId || arTrangThai);
      setArTrangThaiMoTa(props?.statusNew?.statusCode || arTrangThaiMoTa);
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const handleUpdateTiepNhan = async (props) => {
    if (isInValidForSubmit()) { return; }
    let statusId = props?.statusNew?.statusId || state?.arTrangThai;

    try {
      const sendData = {
        ...itemTiepNhan,
        objectId: props?.status?.objectId || state?.id || null,
        id: props?.status?.objectId || state?.id || null,
        arTrangThai: statusId,
        donViThucHienId: itemTiepNhan?.unit?.id,
        donViThucHienText: itemTiepNhan?.unit?.name,
        donViThucHienCode: itemTiepNhan?.unit?.code,
        chiPhiThucHien: itemTiepNhan?.chiPhiThucHien,
        arAttachments: [],
        assetClass: appConst.assetClass.TSCD,
        arTgXacNhan: convertFromToDate(itemTiepNhan?.arTgXacNhan).toDate,
        arTgCanCoVt: convertFromToDate(itemTiepNhan?.arTgCanCoVt).toDate,
        arHanSuaChuaXong: convertFromToDate(itemTiepNhan?.arHanSuaChuaXong).toDate,
        arTgBaoCao: convertFromToDate(itemTiepNhan?.arTgBaoCao).toDate,
      }
      setPageLoading(true);
      if (validatorTiepNhan(itemTiepNhan)) {
        let res;
        if (props?.statusNew?.statusId) {
          res = await updateArSuCoTiepNhan(sendData);
        } else {
          res = await props?.submit?.(sendData);
        }
        let { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          if (props?.statusNew?.statusId) {
            handleGetWorkFlow();
          }
          toast.success(t(sendData?.id ? "general.updateSuccess" : "general.addSuccess"))
          setIdArSuCo(data?.id);
          setArTrangThai(data?.arTrangThai || state?.arTrangThai);
          setArTrangThaiMoTa(data?.arTrangThaiMoTa);
          setState({ ...data, arTrangThai: data?.arTrangThai || statusId });
          setItemTiepNhan({ ...data, arTrangThai: data?.arTrangThai || statusId });
          setArLinhKienVatTus(data?.arLinhKienVatTus || []);
        } else {
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  const handleSave = async (props) => {
    const { PROCESSED, PENDING_APPROVAL, APPROVAL_HISTORY } = TRACKER_STATUS_CODE.REPAIR;
    let submitFuc = {
      [PROCESSED.code]: handleCheckTotalCost,
      [PENDING_APPROVAL.code]: handleGetWfFields,
      [APPROVAL_HISTORY.code]: handleOpenApprovalHistory,
    }

    if (isAllowedSubmit) {
      if (props?.componentName === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab) {
        await handleUpdateArSuCo(props);
        setPageLoading(false);
      } else if (props?.componentName === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab) {
        await handleUpdateTiepNhan(props);
        setPageLoading(false);
      } else if (props?.componentName === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab) {
        if (isInValidForSubmit()) { return; }
        await submitFuc[props?.statusNew?.statusCode || props.code]?.(props);
      } else if (props?.componentName === appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab) {
      }
    }
  };

  const handleGetDataPrintArSuCo = async (id) => {
    try {
      const result = await getDetailArSuCo(id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        let data = result?.data?.data;

        let newDate = new Date(data?.arTgBaoCao);
        const day = data?.arTgBaoCao ? String(newDate?.getDate())?.padStart(2, '0') : "....";
        const month = data?.arTgBaoCao ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : "....";
        const year = data?.arTgBaoCao ? String(newDate?.getFullYear()) : "......";
        let arNghiemThusTxt = data?.arNghiemThus?.map(item => item?.arNtHangMuc)?.join(", ");

        setDataArSuCo((pre) => ({
          ...pre,
          ...data,
          day,
          month,
          year,
          dataPrint: {
            phongBanLapText: data?.arPhongBanText,
            day,
            month,
            year,
            assets: [{
              index: 1,
              tsSerialNoTxt: data?.tsSerialNo ? data?.tsSerialNo : "....",
              tsModelTxt: data?.tsModel ? data?.tsModel : "....",
              tsHangsxTxt: data?.tsHangsx ? data?.tsHangsx : "....",
              tsNamsxTxt: data?.tsNamsx ? data?.tsNamsx : "....",
              tsNamsdTxt: data?.tsNamsd ? data?.tsNamsd : "....",
              ckSoluong: 1,
              arNghiemThusTxt
            }],
            totalQuantity: 1
          },
          arMa: data?.arMa || "..".repeat(10),
          arPhongBanText: data?.arPhongBanText || "..".repeat(20),
          textQuyCach: data?.tsModel && data?.tsSerialNo
            ? `${data.tsModel}/${data.tsSerialNo}`
            : data?.tsModel
              ? `${data.tsModel}`
              : data?.tsSerialNo
                ? `${data.tsSerialNo}`
                : "",
          arNtTsTinhTrangSdTxt: appConst.typeOfWarehouseAssets.find(x => x.id === data?.arNtTsTinhTrangSd)?.name,
          arMoTa: data?.arMoTa?.replace("\n", "<br>"),
          arNoiDung: data?.arNoiDung?.replace("\n", "<br>"),
          arPhanTichSoBo: data?.arPhanTichSoBo?.replace("\n", "<br>"),
          arDeXuatBienPhap: data?.arDeXuatBienPhap?.replace("\n", "<br>"),
          isCCDC: false,
          isTSCD: true,
        }))
      }
    } catch (error) {
      toast.error(t("general.error"));
    }
  }

  const handleCheckTotalCost = async (props) => {
    try {
      setPageLoading(true);
      const result = await checkTotalCost({
        idAsset: state?.tsId,
        totalCost: totalCost,
      });
      const { data } = result?.data;
      if (!isSuccessfulResponse(result?.data?.code)) {
        return handleThrowResponseMessage(result);
      }
      if (data === false) {
        setCurrentWf(props);
        setShouldOpenConfirmObj((pre) => ({
          open: true,
          ...objConfirmCost,
        }));
      } else {
        await handleUpdateNghiemThu(props);
      }
    } catch (error) {
    } finally {
      setPageLoading(false);
    }
  };

  const handleUpdateNghiemThu = async (props) => {
    if (validatorAcceptance(dataNghiemThuSuCo)) {
      let statusId = props?.statusNew?.statusId || currentWf?.statusNew?.statusId;
      try {
        setPageLoading(true);
        let sendData = {
          ...dataNghiemThuSuCo,
          arTrangThai: statusId,
          assetClass: appConst.assetClass.TSCD,
          arNtThoiGian: convertFromToDate(dataNghiemThuSuCo?.arNtThoiGian).toDate,
        }
        let result;
        if (statusId) {
          result = await updateArSuCoNghiemThu(sendData);
        } else {
          result = await props?.submit?.(sendData);
        }
        if (isSuccessfulResponse(result?.data?.code)) {
          toast.success(t("general.updateSuccess"));
          setDataNghiemThuSuCo(result?.data?.data);
          setDisable(true);
          setItemEdit(result?.data?.data);
          setItemTiepNhan(result?.data?.data);
          handleGetWorkFlow();
          setCurrentWf(null);
        } else {
          handleThrowResponseMessage(result);
        }
      } catch (error) {
        toast.error(t("general.error"));
      } finally {
        setPageLoading(false);
      }
    }
  };

  const handleGetWfFields = async (status) => {
    try {
      setPageLoading(true);
      let params = {
        wfWorkflowId: status?.wfId,
        objectId: status?.objectId,
      }

      let res = await getWfFields(params);
      let { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        setWfFields(data);
        setState(prevState => ({
          ...prevState,
          item: {
            firstLevelSubmitter: isRoleAssetManager
              ? {
                personId: currentUser?.person?.id,
                personDisplayName: currentUser?.person?.displayName,
              }
              : null,
            firstLevelSubmittingDepartment: isRoleAssetManager
              ? prevState.arTnPhongBan
              : null,
            uri: ROUTES_PATH.FIXED_ASSET_MANAGEMENT.REPAIR.AR_SU_CO + "/" + itemEdit?.id
          }
        }))
        handleOpenApprovalProcesses();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const handleOpenApprovalProcesses = () => {
    setShouldOpenSubmissionForm(true);
  }

  const handleOpenApprovalHistory = () => {
    setShouldOpenApprovalHistoryPopup(true);
  }

  const handleClosePopup = () => {
    setShouldOpenSubmissionForm(false);
    setShouldOpenApprovalHistoryPopup(false);
    setDisplay(prevState => ({
      ...prevState,
      shouldOpenPreviewTemplatePrint: false,
    }))
  }

  const handleCloseApprovalProcessesOnSuccess = (data) => {
    setShouldOpenSubmissionForm(false);
    handleGetWorkFlow();
  }

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      cellStyle: {
        textAlign: "center"
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "note",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 100,
      minWidth: 70,
      cellStyle: {
        textAlign: "center"
      },
      hidden: props?.isFromApprovalRequest,
      render: rowData =>
        (!props?.item?.isView && !props?.item?.isStatusConfirmed && !disable) ? (
          <div className="none_wrap">
            <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellEditAssetFile(rowData)}>
                <Icon fontSize="small" color="primary">edit</Icon>
              </IconButton>
            </LightTooltip>
            <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellDeleteAssetFile(rowData?.id)}>
                <Icon fontSize="small" color="error">delete</Icon>
              </IconButton>
            </LightTooltip>
          </div>
        ) : (
          <div className="none_wrap">
            <LightTooltip title={t('general.viewIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellViewAssetFile(rowData)}>
                <Icon fontSize="small" color="primary">visibility</Icon>
              </IconButton>
            </LightTooltip>
          </div>
        )
    },
  ];

  const validatorTiepNhan = (sendData) => {
    if (!sendData?.arType) {
      toast.warning(t("maintainRequest.required.arType"))
      return false;
    }
    else if (
      [BO_PHAN_CAN_SUA.SUA_CA_HAI.code, BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code].includes(sendData?.arType)
      && sendData?.arPhuTungs?.length <= 0
    ) {
      toast.warning(t("maintainRequest.required.arPhuTungs"))
      return false;
    }
    else if (!sendData?.arHinhThuc) {
      toast.warning(t("maintainRequest.required.arHinhThuc"))
      return false;
    }
    else if (
      sendData?.arHinhThuc === appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.code
      && sendData?.arUsers?.length <= 0
    ) {
      toast.warning(t("maintainRequest.required.arUsers"))
      return false
    }
    else if (!sendData?.arScope) {
      toast.warning(t("maintainRequest.required.arScope"));
      return false;
    }
    else if (sendData?.arType !== BO_PHAN_CAN_SUA.SUA_TAI_SAN.code && sendData?.arPhuTungs?.length <= 0) {
      toast.warning(t("maintainRequest.required.arPhuTungId"));
      return sendData?.arPhuTungs?.some(pt => !pt.product);
    }
    let itemLinhKienVatTuError = sendData?.arLinhKienVatTus?.find(x => (!x?.productId || x?.productQty <= 0))
    if (itemLinhKienVatTuError) return false;
    return true;
  }

  const validatorAcceptance = (sendData) => {
    if (!sendData?.arNtTinhTrang) {
      toast.warning(t("maintainRequest.required.arNtTinhTrang"))
      return false;
    }
    else if (!sendData?.arYeuCauXuLy) {
      toast.warning(t("maintainRequest.required.arYeuCauXuLy"));
      return false;
    }
    return true;
  }

  const handleOpenMenuPrint = (event) => {
    let shouldOpenPreviewPrintDialog = [
        LIST_ORGANIZATION.BVDK_BA_VI.code,
        LIST_ORGANIZATION.BVDK_MY_DUC.code,
    ]?.includes(currentOrg?.code);
    if (shouldOpenPreviewPrintDialog) {
      return setDisplay(prevState => ({
        ...prevState,
        shouldOpenPreviewTemplatePrint: true,
      }))
    }
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenuPrint = () => {
    setAnchorEl(null);
  };

  const handleConfirmPrint = async (itemValue, valueTab) => {
    await handleGetDataPrintArSuCo(itemEdit?.id);
    if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_DNMTS.code === itemValue) {
      setOpenMAU_DNMTS(true)
    } else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code === itemValue) {
      setOpenBM01(true);
    } else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_KTTS01.code === itemValue) {
      setOpenBMKTTS01(true);
    } else if (valueTab === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab) {
      setOpenBM0601(true);
    } else if (
      valueTab === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab
    ) {
      setOpenBM0606(true);
    } else if (
      valueTab === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab
    ) {
      setOpenBM0607(true);
    }
  };

  const handlePrint = (e, valueTab) => {
    const itemValue = e?.target?.value || e;

    handleConfirmPrint(itemValue, valueTab);
  };

  const handleCheckPermissionOnChagneTab = () => {
    let newPermission = { ...permission };
    if (isRoleAssetManager) {
      let permissionField = (state.arTnPhongBan?.id || state?.arTnPhongBanId) !== departmentUser?.id;
      newPermission.arTgXacNhan.isDisabled = permissionField;
      newPermission.arTgCanCoVt.isDisabled = permissionField;
      newPermission.arHanSuaChuaXong.isDisabled = permissionField;
      newPermission.arType.isDisabled = permissionField;
      newPermission.arHinhThuc.isDisabled = permissionField;
      newPermission.arScope.isDisabled = permissionField;
      newPermission.arLinhKienVatTus.isDisabled = permissionField;
      newPermission.submit.isDisabled = permissionField;
      newPermission.arPhanTichSoBo.isDisabled = permissionField;
      newPermission.arDeXuatBienPhap.isDisabled = permissionField;
      newPermission.arMoTa.isDisabled = permissionField;
      newPermission.arChiPhi.isDisabled = permissionField;
      newPermission.arNtThoiGian.isDisabled = permissionField;
      newPermission.arNghiemThus.isDisabled = permissionField;
      newPermission.arNtTinhTrang.isDisabled = permissionField;
      newPermission.arYeuCauXuLy.isDisabled = permissionField;
      newPermission.arNtTsTinhTrangSd.isDisabled = permissionField;
    } else if (isRoleAssetUser || isRoleUser) {
      newPermission.arTgXacNhan.isDisabled = true;
      newPermission.arTgCanCoVt.isDisabled = true;
      newPermission.arHanSuaChuaXong.isDisabled = true;
      newPermission.arType.isDisabled = true;
      newPermission.arHinhThuc.isDisabled = true;
      newPermission.arScope.isDisabled = true;
      newPermission.arLinhKienVatTus.isDisabled = true;
      newPermission.submit.isDisabled = true;
      newPermission.arPhanTichSoBo.isDisabled = true;
      newPermission.arDeXuatBienPhap.isDisabled = true;
      newPermission.arMoTa.isDisabled = true;
      newPermission.arChiPhi.isDisabled = true;
      newPermission.arNtThoiGian.isDisabled = true;
      newPermission.arNghiemThus.isDisabled = true;
      newPermission.arNtTinhTrang.isDisabled = true;
      newPermission.arYeuCauXuLy.isDisabled = true;
      newPermission.arNtTsTinhTrangSd.isDisabled = true;
    }

    setPermission(newPermission);
  }

  let dataViewBM01 = useMemo(() => convertBM01PrintData(dataArSuCo), [openBM01]);

  let dataViewKTTS01 = useMemo(() => convertKTTS01PrintData(dataArSuCo), [openBMKTTS01]);

  let dataViewBM0606 = useMemo(() => convertBM0606PrintData(dataArSuCo), [openBM0606]);

  const dataViewBM0607 = useMemo(() => convertBM0607PrintData(dataArSuCo), [openBM0607]);

  let isHasNewEquipment = itemTiepNhan?.arLinhKienVatTus?.some(i => i?.storeId && !i?.id)
    && tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab;

  const handleValidateCloseDialog = () => {
    const isShowConfirmCloseDialog = itemTiepNhan?.arLinhKienVatTus?.some(item => item?.status === appConst.EXPORT_SUPPLY_STATUS.DA_XOA.indexOrder)
    if (isShowConfirmCloseDialog) {
      setShouldOpenConfirmObj((pre) => ({ open: true, ...objConfirmClose }));
    } else {
      handleClose();
    }
  }

  const handleCloseConfirm = () => {
    setShouldOpenConfirmObj((pre) => ({ open: false, ...objConfirmCost }));
  }

  const handleYesClickConfirm = () => {
    if (shouldOpenConfirmObj.isConfirmCheckingCost) {
      setShouldOpenConfirmObj((pre) => ({ open: false, ...objConfirmCost }));
      handleUpdateNghiemThu(tabDeviceTroubleshooting);
    } else {
      setShouldOpenConfirmObj((pre) => ({ open: false, ...objConfirmClose }));
      handleClose();
    }
  }
  
  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth disableEscapeKeyDown={false}>
      <DialogTitle className="pb-0 cursor-move">
        {t("ReportIncident.titleDialog")}
      </DialogTitle>
      <CustomValidatorForm
        ref={form}
        id="device-form"
        onSubmit={() => { }}
        className="validator-form-scroll-dialog m-0"
      >
        <DialogContent>
          <AppBar position="static" color="default">
            <Tabs
              value={tabDeviceTroubleshooting}
              onChange={handleChangeTabsDeviceTroubleshooting}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="simple tabs example"
            >
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.INFO.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.INFO.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.INFO.valueTab)}
              />
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.EVALUATE.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab)}
              />
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab)}
              />
              {itemEdit?.id && <Tab
                value={appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab)}
              />}
            </Tabs>
          </AppBar>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.INFO.valueTab}
          >
            <ReportIncidentForm
              t={t}
              i18n={i18n}
              isEdit={isEdit}
              itemEdit={itemEdit}
              state={state}
              setState={setState}
              isRoleAssetManager={isRoleAssetManager}
              isRoleAdmin={isRoleAdmin}
              disable={disable}
              isAdd={isAdd}
              permission={permission}
            />
          </TabPanel>

          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab}
          >
            <RateIncidentForm
              t={t}
              i18n={i18n}
              setItemTiepNhan={setItemTiepNhan}
              itemTiepNhan={itemTiepNhan}
              isAllowedSubmit={isAllowedSubmit}
              setIsAllowedSubmit={setIsAllowedSubmit}
              disable={disable}
              arLinhKienVatTus={arLinhKienVatTus}
              permission={permission}
            />
          </TabPanel>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab}
          >
            <AssetAcceptanceForm
              t={t}
              i18n={i18n}
              itemEdit={state}
              isSubmitNghiemThuSuCo={isSubmitNghiemThuSuCo}
              setIsSubmitNghiemThuSuCo={setIsSubmitNghiemThuSuCo}
              dataNghiemThuSuCo={dataNghiemThuSuCo}
              setDataNghiemThuSuCo={setDataNghiemThuSuCo}
              totalCost={totalCost}
              setTotalCost={setTotalCost}
              disable={disable}
              permission={permission}
            />
          </TabPanel>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab}
          >
            <Grid item md={12} sm={12} xs={12}>
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={handleAddAssetDocumentItem}
                disabled={disable}
              >
                {t('AssetFile.addFileAttachment')}
              </Button>
              {shouldOpenPopupAssetFile && (
                <AttachmentPopup
                  open={shouldOpenPopupAssetFile}
                  handleClose={handleAssetFilePopupClose}
                  itemAssetDocument={documents}
                  getAssetDocument={getAssetDocument}
                  handleUpdateAssetDocument={handleUpdateAssetDocument}
                  documentType={appConst.documentType.ASSET_DOCUMENT}
                  item={item}
                  t={t}
                  i18n={i18n}
                  idArSuCo={idArSuCo}
                />)
              }
            </Grid>
            <Grid item md={12} sm={12} xs={12} className="mt-16">
              <MaterialTable
                data={listDocument}
                columns={columnsVoucherFile}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  }
                }}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  padding: 'dense',
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                  maxBodyHeight: '285px',
                  minBodyHeight: '285px',
                }}
              />
            </Grid>
          </TabPanel>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleValidateCloseDialog()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            disabled={isTabAttachment}
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={handleOpenMenuPrint}
          >
            {t("general.print")}
          </Button>
          {/* Menu Print */}
          <Menu
            id="simple-menu"
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            keepMounted
            elevation={0}
            onClose={handleCloseMenuPrint}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            {listPrintArSucoOptions.map((item, index) => (
              <MenuItem
                key={index}
                id={`button-print-${item.code}`}
                onClick={(e) => handlePrint(e, tabDeviceTroubleshooting)}
                value={item.code}
              >
                {item.name}
              </MenuItem>
            ))}
          </Menu>

          {workflows?.map((item, index) => {
            return (
              <WfButton
                key={index}
                noOldStatus
                wfItem={item}
                componentName={tabDeviceTroubleshooting}
                handleClick={handleSave}
                additionalProps={{
                  typeCodesSubBtn: [TRACKER_STATUS_CODE.REPAIR.APPROVAL_HISTORY.code],
                  typeCodes: [TRACKER_STATUS_CODE.REPAIR.PENDING_APPROVAL.code]
                }}
                type={
                  item.newStatusCode !== TRACKER_STATUS_CODE.REPAIR.PENDING_APPROVAL.code
                    && item.code !== TRACKER_STATUS_CODE.REPAIR.APPROVAL_HISTORY.code
                    ? "submit"
                    : "button"
                }
              />
            )
          })}
        </DialogActions>
      </CustomValidatorForm>
      {shouldOpenSubmissionForm && (
        <ApprovalRequestDialog
          t={t}
          handleClose={handleClosePopup}
          open={shouldOpenSubmissionForm}
          handleOKEditClose={handleCloseApprovalProcessesOnSuccess}
          item={wfFields}
          defaultValue={state?.item}
          componentName={variable.componentName.APPROVAL_REQUEST}
        />
      )}

      {shouldOpenApprovalHistoryPopup && (
        <ApprovalProcessHistoryPopup
          t={t}
          handleClose={handleClosePopup}
          open={shouldOpenApprovalHistoryPopup}
          objectId={state?.id}
          trackerCodeRelate={TRACKER_CODE.REPAIR}
        />
      )}

      {display?.shouldOpenPreviewTemplatePrint && (
        <PrintPreviewTemplateDialog
          t={t}
          handleClose={handleClosePopup}
          open={display?.shouldOpenPreviewTemplatePrint}
          item={state}
          title={t("Phiếu in sửa chữa")}
          model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.REPAIR.INCIDENT}
        />
      )}

      {openBM0601 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0601(false)
          }}
          open={openBM0601}
          item={dataArSuCo}
          title={t("Phiếu báo cáo sự cố")}
          urls={[
            ...REPAIRING.INCIDENT_REPORT_PVT.GENERAL,
            ...REPAIRING.INCIDENT_REPORT_PVT[currentOrg?.printCode],
          ]}
        />
      )}
      {openMAU_DNMTS && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenMAU_DNMTS(false)
          }}
          open={openMAU_DNMTS}
          item={dataArSuCo}
          title={t("Phiếu đề nghị mang tài sản ra ngoài viện")}
          urls={[
            ...REPAIRING.TAKE_ASSET_OUT_OF_ORG.GENERAL,
            ...REPAIRING.TAKE_ASSET_OUT_OF_ORG[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM01 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM01(false)
          }}
          open={openBM01}
          item={dataViewBM01}
          title={t("Phiếu yêu cầu sửa chữa")}
          urls={[
            ...REPAIRING.INCIDENT_REPORT_HCQT.GENERAL,
            ...REPAIRING.INCIDENT_REPORT_HCQT[currentOrg?.printCode],
          ]}
        />
      )}

      {openBMKTTS01 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBMKTTS01(false)
          }}
          open={openBMKTTS01}
          item={dataViewKTTS01}
          title={t("Biên bản xử lý vật tư thay thế, sửa chữa")}
          urls={[
            ...REPAIRING.MINUTES_OF_HANDLING_SUPPLIES.GENERAL,
            ...REPAIRING.MINUTES_OF_HANDLING_SUPPLIES[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM0606 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0606(false)
          }}
          open={openBM0606}
          item={dataViewBM0606}
          title={t("Phiếu đánh giá sự cố")}
          urls={[
            ...REPAIRING.INCIDENT_RATING.GENERAL,
            ...REPAIRING.INCIDENT_RATING[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM0607 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0607(false)
          }}
          open={openBM0607}
          item={dataViewBM0607}
          title={t("Phiếu nghiệm thu")}
          urls={[
            ...REPAIRING.ACCEPTANCE.GENERAL,
            ...REPAIRING.ACCEPTANCE[currentOrg?.printCode],
          ]}
        />
      )}

      {shouldOpenConfirmObj?.open && (
        <ConfirmationDialog
          title={t("general.confirm")}
          open={shouldOpenConfirmObj?.open}
          onConfirmDialogClose={() => handleCloseConfirm()}
          onYesClick={() => handleYesClickConfirm()}
          text={shouldOpenConfirmObj.text}
          agree={shouldOpenConfirmObj.agree}
          cancel={shouldOpenConfirmObj.cancel}
        />
      )}
    </Dialog>
  );
}

export default DeviceTroubleshootingDialog;
