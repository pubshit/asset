import React, { useEffect, useState } from 'react';
import {
  Dialog,
  Grid,
  DialogActions,
  DialogTitle,
  DialogContent,
  Button,
  TablePagination,
  InputAdornment,
  Input,
  Checkbox,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CustomMaterialTable from '../../CustomMaterialTable';
import { appConst, variable } from '../../../appConst';
import { searchByPageUser } from "../AssetRepairService";
import SearchIcon from '@material-ui/icons/Search';
import { PaperComponent } from "../../Component/Utilities";
import {defaultPaginationProps, getUserInformation} from "../../../appFunction";
import {searchByPageUsersDepartmentNew} from "../../User/UserService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const SelectUserTodoDialog = (props) => {
  const { t } = useTranslation();
  const { open, handleClose, onAddUser, arUsers = [] } = props;
  const [listUser, setListUser] = useState([]);
  const [selectedValue, setSelectedValue] = useState([])
  const [totalElements, setTotalElements] = useState(0);
  const [keyword, setKeyword] = useState("")
  const [query, setQuery] = useState({
    pageIndex: 0,
    pageSize: 10,
    keyword: "",
  });
  const [state, setState] = useState({})
  const [selectedList, setSelectedList] = useState(arUsers.map((item) => ({ ...item, id: item?.userId })) || [])

  useEffect(() => {
    handleSearchByPageUser();
  }, [query]);

  useEffect(() => {
    setSelectedValue(arUsers.map((item) => ({ ...item, id: item?.userId })))
  }, [])

  const handleSearchByPageUser = () => {
    const {organization} = getUserInformation();
    const sendData = {
      pageIndex: query.pageIndex + 1,
      pageSize: query.pageSize,
      keyword: keyword,
      orgId: organization?.org?.id,
    };
    try {
      searchByPageUsersDepartmentNew(sendData).then(({ data }) => {
        if (data?.code === appConst.CODE.SUCCESS) {
          let clone = data?.data?.content?.map((item) => {
            return {
              ...item,
              checked: !!selectedList?.find(checkedItem => checkedItem?.id === item.id),
            }
          });
          setListUser([...clone]);
          setTotalElements(data.data.totalElements);
        }
      });
      
    } catch (error) {
      console.error(error);
    }
  };

  const handleChangePage = (event, newPage) => {
    setQuery({
      ...query,
      pageIndex: newPage,
    });
  };
  const setRowsPerPage = (event) => {
    setQuery({
      ...query,
      pageSize: event.target.value,
      pageIndex: 0
    });
  };

  const handleCheck = (event, item) => {
    let clone = [...selectedList]
    let existItem = selectedList.find(x => (x?.id || x?.userId) === item?.id);
    item.checked = event.target.checked;
    !existItem ?
      clone.push(item) :
      clone.map((item, index) =>
        item === existItem ?
          clone.splice(index, 1) :
          null
      );
    setSelectedList([...clone])
    setState({
      selectAllItem: [...clone],
      selectedList,
    });
  };

  let columns = [
    {
      title: t("general.select"),
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <Checkbox
          name={rowData.id}
          className="p-0"
          checked={rowData.checked ? rowData.checked : false}
          onChange={(e) => {
            handleCheck(e, rowData);
          }}
        />
      ),
    },
    {
      title: t('MaintainPlaning.displayName'),
      field: 'displayName',
      align: 'left',
      minWidth: '120px',
      maxWidth: 400,
      cellStyle: {
        wordBreak: 'break-word',
      }
    },
    {
      title: t('MaintainPlaning.departmentName'),
      field: 'departmentName',
      align: 'left',
      minWidth: '120px',
    },
  ];

  const handleChange = (e) => {
    setKeyword(e.target.value)
  }

  const handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      setQuery({
        ...query,
        pageIndex: 0,
      });
    }
  };

  const handleKeyUp = (e) => {
    !e.target.value && handleSearchByPageUser()
  }

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle className="pb-0 cursor-move">
        {t('MaintainResquest.RateIncident.repairPerson')}
      </DialogTitle>

      <DialogContent>
        <Grid container justifyContent="flex-end" alignItems="flex-end">
          <Input
            label={t('general.enterSearch')}
            type="text"
            name="keyword"
            value={keyword}
            onChange={handleChange}
            onKeyDown={handleKeyDownEnterSearch}
            onKeyUp={handleKeyUp}
            style={{ width: '50%' }}
            className="mb-16"
            id="search_box"
            placeholder={t('general.enterSearch')}
            startAdornment={
              <InputAdornment position="end">
                <SearchIcon
                  onClick={() => setQuery({ ...query, pageIndex: 0 })}
                  className="searchTable"
                />
              </InputAdornment>
            }
          />
        </Grid>
        <div className="mp-0">
          <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={12} xs={12}>
              <CustomMaterialTable
                className="customPlaning"
                data={listUser || []}
                columns={columns}
                // onRowClick={((evt, selectedRow) => handleRowClick(selectedRow))}
                options={{
                  rowStyle: (rowData) => ({
                    backgroundColor: selectedValue?.id === rowData.id ? "#EEE" : "#FFF",
                  }),
                  selection: false,
                }}
                onSelectionChange={(rows) => {
                  setSelectedValue([...rows])
                }}
              />
              <TablePagination
                {...defaultPaginationProps()}
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                count={totalElements}
                rowsPerPage={query.pageSize}
                page={query.pageIndex}
                onPageChange={handleChangePage}
                onRowsPerPageChange={setRowsPerPage}
              />
            </Grid>
          </Grid>
        </div>
      </DialogContent>
      <DialogActions>
        <div className="flex flex-space-between flex-middle mt-12">
          <Button variant="contained" className="mr-12" color="secondary" onClick={() => handleClose()}>
            {t('general.close')}
          </Button>
          <Button
            variant="contained"
            className="mr-15"
            color="primary"
            onClick={() => {
              onAddUser(selectedList);
            }}
          >
            {t('general.select')}
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
};

export default SelectUserTodoDialog;
