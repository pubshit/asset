import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  Grid,
  Input,
  InputAdornment,
  TablePagination,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, {MTableToolbar,} from "material-table";
import {getByPageUserDepartment} from "../../User/UserService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {appConst} from "app/appConst";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {createFilterOptions} from "@material-ui/lab";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {searchByPageListUserDepartment} from "../AssetRepairService";
import {PaperComponent} from "../../Component/Utilities";
import {defaultPaginationProps} from "../../../appFunction";

class SelectUserDepartment extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    listManageDepartment: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    receiverDepartmentId: "",
    department: null,
    persons: [],
  };

  setPage = (page) => {
    this.setState({ page : page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  convertUsers = itemList =>{
    return itemList?.map(item => {
      item.user = {
        id: item.userId,
        displayName: item.personDisplayName,
        username: item.username,
        person: {
          userId: item.userId,
          displayName: item.personDisplayName,
          id: item.personId,
        },
      }

      item.id = item.personId,
      item.department = {
        id: item.departmentId,
        name: item.departmentName,
        code: item.departmentCode,
        view: item.departmentView,
        type: item.departmentType,
      }
      return item
    })
  }

  updatePageData = () => {
    let searchObject = {};
    searchObject.isMainDepartment = true;
    searchObject.isNotAdminUser = this.props.isNotAdminUser;
    searchObject.departmentId = this.state.departmentId;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPageUserDepartment(searchObject).then(({ data }) => {
      let itemListClone = this.convertUsers(data?.data?.content);
      itemListClone?.map((item) => {
        item.isCheck = false;
        this.state?.persons?.forEach((person) => {
          if (person?.id === item?.user?.person?.id || person?.user?.person?.id === item?.user?.person?.id) {
            item.isCheck = true;
          }
        })
      })
      this.setState({
        itemList: [...itemListClone],
        totalElements: data?.data.totalElements,
      });
    });
  };

  componentDidMount() {
    let { selectedItem } = this.props;
    
    this.setState({persons: this.props?.persons, selectedValue: selectedItem?.id},
      () => this.updatePageData()
    );
  }

  handleClick = (event, item) => {
    let { persons } = this.state;
    let foundPerson = persons.find(person => person.departmentName ? person?.id === item?.user?.person?.id : person?.id === item?.id);
    item.isCheck = event.target.checked;
    if (item?.isCheck) {
      if(!foundPerson?.isCheck){
        persons = persons.concat(item);
      }
    } else {
      persons = persons.filter(
        (person) => {
          if(person?.departmentName){
            return (person?.id !== item?.user?.person?.id)
          }else{
            return (person?.id !== item?.id)
          }
        }
      );
    }
    this.setState({ persons: persons });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  search() {
    this.setPage(0)
  }
  handleKeyUp = (e)=>{
    !e.target.value && this.search()
  }

  handleChange = (event, source) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFilterDepartment = (item) => {
    this.setState({
      departmentId: item?.id || "",
      department: item || null,
      page: 0,
    }, () => {
      this.updatePageData();
    });
  };

  handleSubmit = (persons) => {
    if(persons.length > 0){
      this.props.handleSelect(persons)
    }else{
      toast.dismiss()
      toast.warning("Chưa chọn người thanh lý")
      toast.clearWaitingQueue();
    }
  }

  handleSetListData = (data)=>{
    this.setState({listManageDepartment: data});
  }

  render() {
    const { t, handleClose, open, } = this.props;
    const filterAutocomplete = createFilterOptions();
    let { keyword, department } = this.state;
    let SearchDepartment = { pageIndex: this.state.page, pageSize: 1000000 };
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "90px",
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Checkbox
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={rowData?.isCheck}
            onClick={(event) => this.handleClick(event, rowData)}
          />),
      },
      { title: t("asset_liquidate.persons"), field: "user.displayName", width: "270px" },
      {
        title: t("component.department.text"),
        field: "department.name",
        width: "150",
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle className="pb-0 cursor-move">
          <span className="">{t("asset_liquidate.persons")}</span>
        </DialogTitle>
        <DialogContent>
          <Grid item container spacing={2} xs={12}>
            <Grid item md={6} sm={12} xs={12}>
              <Input
                label={t("general.enterSearch")}
                style={{ marginTop: "16px" }}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                className="w-100 mb-16"
                id="search_box"
                placeholder={t("general.enterSearch")}
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={() => this.search(keyword)} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </Grid>
            <Grid item md={3} xs={12}></Grid>
            <Grid item md={3} xs={12}>
              <AsynchronousAutocompleteSub
                    label={t("general.filterDerpartment")}
                    searchFunction={searchByPageListUserDepartment}
                    searchObject={SearchDepartment}
                    listData={this.state.listManageDepartment}
                    setListData={this.handleSetListData}
                    defaultValue={department || null}
                    displayLable={'text'}
                    value={department || null}
                    onSelect={this.handleFilterDepartment}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      return filterAutocomplete(options, params)
                    }}
                    noOptionsText={t("general.noOption")}
                  />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
              }}
              components={{
                Toolbar: (props) => (
                  <div>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mr-12"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mr-16"
            variant="contained"
            style={{ marginLeft: "0px" }}
            color="primary"
            onClick={() => this.handleSubmit(this.state.persons)}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectUserDepartment;
