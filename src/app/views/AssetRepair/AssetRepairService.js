import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH = ConstantList.API_ENPOINT + "/api/maintainRequest" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_DEPARTMENT = ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_AR = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
const API_PATH_WORK_FLOW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/wf-workflows";
const API_PATH_APPROVAL_PROCESSES = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/approval-processes";
const API_PATH_SU_CO = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/su-co";

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_AR + "/download/excel/ar-su-co",
    data: searchObject,
    responseType: "blob",
  });
};

export const getCountStatus = (payload) => {
  let config = {
    params: { ...payload },
  };
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/count-by-status",
    config
  );
};

export const searchByPageArSuco = (searchObject) => {
  let url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/search-by-page";
  return axios.post(url, searchObject);
};

export const updateArSuCo = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco";
  return axios.put(url, searchObject);
};

export const createArSuCo = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco";
  return axios.post(url, searchObject);
};

export const getDetailArSuCo = (id) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/" + id
  );
};

export const deleteArSuCo = (id) => {
  return axios.delete(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/" + id
  );
};

export const searchByPageAssetSuco = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/assets";
  return axios.post(url, searchObject);
};


export const updateArSuCoTiepNhan = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/tiep-nhan";
  return axios.put(url, searchObject);
};

export const searchByPageProductOrgV2 = (searchObject) => {
  let config = {
    params: searchObject
  }
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/products/search-by-page";
  return axios.get(url, config);
};;

export const updateArSuCoNghiemThu = (searchObject) => {
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/nghiem-thu";
  return axios.put(url, searchObject);
};
export const checkTotalCost = (data) => {
  let url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
    "/api/ar-suco/kiem-tra-tong-chi-phi";
  return axios.post(url, null, { params: data });
};
export const searchByPageUser = (searchObject) => {
  let url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/users/search-by-page";
  return axios.post(url, searchObject);
};

export const getAllDepartmentByPage = (searchObject) => {
  let url =
    ConstantList.API_ENPOINT + "/api/assetDepartment/org/searchByPageDeparment";
  return axios.post(url, {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  let url =
    ConstantList.API_ENPOINT +
    "/api/assetDepartment/org/management-departments";
  return axios.get(url, config);
};
export const searchByPageListUserDepartment = (SeachObjectct) => {
  let url = API_PATH_ASSET_DEPARTMENT + "/searchByPage";
  return axios.post(url, SeachObjectct);
};

export const downLoadAttachment = (id) => {
  const url = API_PATH_AR + "/download/document/" + id;
  return axios.get(url, { responseType: "arraybuffer" });
};

export const getPhuTungByAsset = (searchObject) => {
  // list phụ tùng
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset/phu-tung";
  return axios.post(url, searchObject);
};

export const checkIsValidTimes = (params) => {
  //Check cảnh báo nếu ts đã sửa >= 3 lần/ năm
  return axios.get(API_PATH_AR + `/ar-suco/is-valid-times`, {
    params,
  });
};

export const uploadIncidentImages = (formData) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(
    API_PATH_AR + "/ar-attachment/upload",
    formData,
    config
  );
};

export const deleteIncidentImage = (id) => {
  return axios.delete(`${API_PATH_AR}/ar-attachment/${id}`);
};

export const deleteSupplies = (id) => {
  return axios.delete(`${API_PATH_AR}/ar-suco/${id}/supplies`);
};

export const getWfWorkflow = (params) => {
  return axios.get(API_PATH_WORK_FLOW, {params});
};

export const searchByPageProductOrgNew = (searchObject) => {
  let config = {
    params: searchObject
  }
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/products/search-by-page";
  return axios.get(url, config);
};
