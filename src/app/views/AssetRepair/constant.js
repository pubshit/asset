import {appConst} from "../../appConst";


export const defaultPermission = {
  arTgBaoCao: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arTnPhongBan: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arPhongBanId: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arNoiDung: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arTgXacNhan: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arTgCanCoVt: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arHanSuaChuaXong: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arMoTa: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arPhanTichSoBo: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arDeXuatBienPhap: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arType: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arScope: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arHinhThuc: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arLinhKienVatTus: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arPhuTungs: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  submit: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arChiPhi: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arNtThoiGian: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arNghiemThus: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arNtTinhTrang: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arYeuCauXuLy: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  arNtTsTinhTrangSd: { ...appConst.OBJECT_PERMISSION_DEFAULT },
}