import React from "react";
import clsx from "clsx";

export const Table = ({ children, ...props }) => (
  <table {...props} className="MuiTable-root">
    {children}
  </table>
);
export const Tr = ({ children, ...props }) => (
  <tr {...props} className={clsx("MuiTableRow-root", props.className)}>
    {children}
  </tr>
);
export const Th = ({ children, ...props }) => (
  <th
    {...props}
    style={{
      backgroundColor: "rgb(53, 134, 0)",
      color: "rgb(255, 255, 255)",
      paddingLeft: "10px",
      paddingRight: "10px",
      textAlign: "center",
      boxSizing: "border-box",
      padding: `${props.padding}`,
    }}
    className="MuiTableCell-root MuiTableCell-head MTableHeader-header-656 MuiTableCell-alignLeft"
  >
    {children}
  </th>
);
export const Td = ({ children, textAlign, ...rest }) => (
  <td
    {...rest}
    style={{ textAlign: textAlign }}
    className="MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft"
  >
    {children}
  </td>
);
