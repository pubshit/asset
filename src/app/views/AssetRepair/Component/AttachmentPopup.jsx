import React, { Component } from "react";
import { Button, Card, Dialog, DialogActions, Divider, Grid, Icon, IconButton } from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { EgretProgressBar } from 'egret'
import ImportExcelDialog from '../../Component/ImportExcel/ImportExcelDialog'
import FileSaver from 'file-saver';
import GetAppSharpIcon from '@material-ui/icons/GetAppSharp';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { createArAttachment, updateArAttachmentById, uploadFile } from "../../Asset/AssetService";
import AppContext from "app/appContext";
import { appConst, MEGABYTE } from "app/appConst";
import moment from "moment";
import { downLoadAttachment } from "../AssetRepairService";
import { PaperComponent } from "../../Component/Utilities";
import { openFileInNewWindow } from "../../../appFunction";

class AttachmentPopup extends Component {
  state = {
    name: "",
    code: "",
    note: "",
    shouldOpenImportExcelDialog: false,
    shouldOpenNotificationPopup: false,
    dragClass: "",
    attachments: [],
    files: [],
    statusList: [],
    queProgress: 0,
    progress: 0,
    arScId: null,
    fileId: "",
    maintainRequestId: null,
    voucherId: null,
    id: null,
    createdBy: "",
    modifiedBy: "",
    modifyDate: moment().format("YYYY-MM-DDTHH:mm:ss"),
    attachmentId: [],
    isUpload: false,
  };

  convertDto = (state) => {
    return {
      arScId: state?.arScId,
      createDate: moment().format("YYYY-MM-DDTHH:mm:ss"),
      createdBy: state?.createdBy,
      fileId: state?.fileId,
      id: state?.id,
      modifiedBy: state?.modifiedBy,
      modifyDate: state?.modifyDate,
      name: state?.name,
      note: state?.note,
      type: this?.props?.type
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { id, files } = this.state;
    let { t } = this.props

    if (files.length <= 0) {
      toast.warning("Chưa có hoặc chưa tải tập tin");
      return;
    }
    setPageLoading(true);
    let dataState = this.convertDto(this.state)
    if (id) {
      try {
        const result = await updateArAttachmentById(dataState)
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"))
          this.props.handleUpdateAssetDocument(result?.data?.data)
          this.props.handleClose();
        }
      } catch (error) {

      } finally {
        setPageLoading(false);
      }

    } else {
      try {
        const result = await createArAttachment(dataState)
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          this.props.getAssetDocument({ ...result?.data?.data, code: this.props.itemAssetDocument?.code })
          this.props.handleClose();
        }
      } catch (error) {

      } finally {
        setPageLoading(false);
      }
    }
  };

  componentDidMount() {
    let { itemAssetDocument, item } = this.props;
    let { isView, isEditAssetDocument } = item;

    if (isView || isEditAssetDocument) {
      this.setState({
        ...itemAssetDocument,
        arScId: this.props.idArSuCo,
        files: itemAssetDocument?.file ? [{ file: itemAssetDocument?.file }] : [],
      });
    }
    else {
      this.setState({
        ...itemAssetDocument,
        arScId: this.props.idArSuCo,
        files: [],
      });
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
    this.updatePageData()
  }

  handleFileUploadOnSelect = event => {
    let files = event.target.files;
    this.fileUpload(files[0]).then(() => {
      toast.info("Tải tập tin thành công");
    });
  }

  handleFileSelect = event => {
    let files = event.target.files;
    let list = [...this.state.files];

    for (const iterator of files) {
      const maxSizeFile = MEGABYTE * 10;

      if (iterator?.size >= maxSizeFile) {
        toast.warn("Kích thước của file không được vượt quá 10MB")
        return;
      }

      list = []
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
      this.setState({ isUpload: true })
    }

    this.setState({
      files: [...list],
      fileDescriptionIds: list,
    }, () => this.uploadSingleFile(this.state.files.length - 1));
  };

  handleSingleRemove = index => {
    // this.props.getAssetDocument({})

    this.setState({
      files: [],
      fileId: "",
    });
  };

  fileUpload = async (file) => {
    let { setPageLoading } = this.context;
    let formData = new FormData();
    formData.append("uploadfile", file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    setPageLoading(true);
    try {
      const result = await uploadFile(formData)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        let newFile = {
          file: { ...result?.data?.data },
          uploading: true,
          success: true,
          error: false
        };
        this.setState({ fileId: result?.data?.data?.id, files: [newFile] });
        toast.success("Tải tập tin thành công");
      } else {
        toast.error(result?.data?.message);
      }
    } catch (error) {
      toast.error("Lỗi tải file");
    } finally {
      setPageLoading(false)
    }
  }

  uploadSingleFile = async (index) => {
    let file = this.state.files[index];
    await this.fileUpload(file?.file);
  };

  handleViewDocument = async index => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    let file = this.state.files[index];
    let contentType = file?.file?.contentType;
    let fileName = file?.file?.name;

    try {
      setPageLoading(true)
      const result = await downLoadAttachment(file?.file?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = null;
        file = new Blob([document], { type: contentType });

        openFileInNewWindow(file, fileName);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    }
  }

  handleDownloadDocument = async index => {
    const { setPageLoading } = this.context
    const { t } = this.props
    let file = this.state.files[index];
    let contentType = file?.file?.contentType;
    let fileName = file?.file?.name;
    try {
      setPageLoading(true)
      const result = await downLoadAttachment(file?.file?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = new Blob([document], { type: contentType });
        return FileSaver.saveAs(file, fileName);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    };
  }

  render() {
    let { open, t, i18n, item } = this.props;
    let { files, isUpload } = this.state;
    let isEmpty = files.length === 0;
    let { isView = false, isEditAssetDocument } = item
    let {
      name,
      note,
      shouldOpenImportExcelDialog,
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
          <h4 style={{ marginBottom: '0px' }} className="">{t('general.saveUpdate')}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          {shouldOpenImportExcelDialog && (
            <ImportExcelDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenImportExcelDialog}
              handleOKEditClose={this.handleOKEditClose}
            />
          )}
          <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }}>
            <Grid className="" container spacing={1}>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('AssetFile.name')}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  InputProps={{
                    readOnly: isView,
                  }}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>

              <Grid item md={8} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t('AssetFile.description')}
                  onChange={this.handleChange}
                  type="text"
                  name="note"
                  value={note}
                  InputProps={{
                    readOnly: isView,
                  }}
                />
              </Grid>

            </Grid>
            <div className="mt-12">
              <div className="flex flex-wrap mb-15">
                <label htmlFor={!files.length && "upload-single-file"}>
                  {!isView && <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                    disabled={files.length}
                  >
                    <div className="flex flex-middle">
                      <span>{t('general.select_file')}</span>
                    </div>
                  </Button>}
                </label>
                <input
                  className="display-none"
                  onChange={this.handleFileSelect}
                  id="upload-single-file"
                  type="file"
                />
                <div className="px-16"></div>
              </div>
              <Card className="mb-24" elevation={2}>
                <div className="p-16">
                  <Grid
                    container
                    spacing={2}
                    justifyContent="center"
                    alignItems="center"
                    direction="row"
                  >
                    <Grid item lg={4} md={4}>
                      {t('general.file_name')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.size')}
                    </Grid>
                    <Grid item lg={2} md={2}>
                      {t('general.action')}
                    </Grid>
                  </Grid>
                </div>
                <Divider></Divider>

                {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

                {files.map((item, index) => {
                  let { file, success, error, progress } = item;
                  return (
                    <div className="px-16 py-8" key={file?.name}>
                      <Grid
                        container
                        spacing={2}
                        justifyContent="center"
                        alignItems="center"
                        direction="row"
                      >
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          {file?.name}
                        </Grid>
                        {(file?.contentSize) ? (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file?.contentSize / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        ) : (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file?.size / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        )}
                        {((isEditAssetDocument || isView) || success) ? (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={100}></EgretProgressBar>
                          </Grid>
                        ) : (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={progress}></EgretProgressBar>
                          </Grid>
                        )}
                        <Grid item lg={1} md={1} sm={12} xs={12}>
                          {error && <Icon fontSize="small" color="error">error</Icon>}
                          {/* {uploading && <Icon className="text-green">done</Icon>} */}
                        </Grid>
                        <Grid item lg={2} md={2} sm={12} xs={12}>
                          <div className="flex">
                            {(!isEditAssetDocument || isUpload) && !isView && (
                              <IconButton disabled={success} size="small" title={t('general.upload')}
                                onClick={() => this.uploadSingleFile(index)}>
                                <Icon color={success ? "disabled" : "primary"} fontSize="small">cloud_upload</Icon>
                              </IconButton>
                            )}
                            {((isEditAssetDocument || isView) && !isEmpty) && (<IconButton size="small" title={t('general.viewDocument')}
                              onClick={() => this.handleViewDocument(index)}>
                              <Icon fontSize="small" color="primary">visibility</Icon>
                            </IconButton>
                            )}

                            {((isEditAssetDocument || isView) && !isEmpty) && (<IconButton size="small" title={t('general.downloadDocument')}
                              onClick={() => this.handleDownloadDocument(index)}>
                              <Icon fontSize="small" color="default"><GetAppSharpIcon /></Icon>
                            </IconButton>
                            )}
                            <IconButton size="small" title={t('general.removeDocument')}
                              onClick={() => this.handleSingleRemove(index)}>
                              <Icon fontSize="small" color="error">delete</Icon>
                            </IconButton>

                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  );
                })}
              </Card>
            </div>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                className="mr-16"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>

            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AttachmentPopup.contextType = AppContext;
export default AttachmentPopup;
