import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  Tooltip,
  FormControl,
  Input, InputAdornment, TextField,
} from '@material-ui/core'
import FileSaver from "file-saver";
import React from 'react'
import MaterialTable, {
  MTableToolbar,
} from 'material-table'
import {
  getItemById,
  deleteCheckItem,
  exportExampleImportExcelSupplier,
  searchByTextNew
} from './SupplierService'
import SupplierDialog from './SupplierDialog'
import { Breadcrumb, ConfirmationDialog } from 'egret'
import { Helmet } from 'react-helmet'
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import ImportExcelDialog from "./ImportExcelDialog";
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchByPage as searchCommonObject } from '../CommonObject/CommonObjectService'
import AppContext from 'app/appContext';
import { ValidatorForm } from 'react-material-ui-form-validator';
import { Autocomplete, createFilterOptions } from '@material-ui/lab';
import { STATUS_SUPPLIER, TYPE_CODE_SUPPLIER, appConst } from '../../appConst';
import {LightTooltip} from "../Component/Utilities";
import {functionExportToExcel} from "../../appFunction";


function MaterialButton(props) {
  const { t, } = useTranslation()
  const item = props.item
  return (
    <div className="none_wrap">
      <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">edit</Icon>
        </IconButton>
      </LightTooltip>
    </div>
  )
}
class Supplier extends React.Component {
  state = {
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    keyword: '',
    shouldOpenNotificationPopup: false,
    Notification: '',
    listCommonObjectType: [],
    commonObjectType: null,
    isActive: {}
  }
  constructor(props) {
    super(props)
    //this.state = {keyword: ''};
    this.handleTextChange = this.handleTextChange.bind(this)
  }
  handleTextChange(event) {
    this.setState({ keyword: event.target.value })
  }
  search() {
    this.setPage(0);
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search()
    }
  }
  componentDidMount() {
    this.updatePageData()
  }
  updatePageData = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let typeCodes = []
    let searchObject = {}

    this.state.commonObjectType?.code
      && typeCodes.push(this.state.commonObjectType?.code)
    searchObject.keyword = this.state?.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    if (typeCodes?.length > 0) {
      searchObject.typeCodes = typeCodes;
    }
    searchObject.isActive = this.state.isActive?.value;
    try {
      setPageLoading(true);
      let res = await searchByTextNew(searchObject)
      const { status, data } = res;
      if (appConst.CODE.SUCCESS === status) {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        })
      }
      else {
        toast.error(t("general.error"))
      }
    }
    catch (e) {
      toast.error(t("general.error"))
    }
    finally {
      setPageLoading(false);
    }

  }
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData()
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }

  exportExampleImportExcel = async () => {
    await functionExportToExcel(
      exportExampleImportExcelSupplier,
      {},
      "Mẫu excel nhà cung cấp"
    )
  }

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenImportExcelDialog: false,
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        shouldOpenConfirmationDeleteAllDialog: false,
        shouldOpenNotificationPopup: false,
        data: [],
      },
      () => {
        this.updatePageData()
      }
    )
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
    }, () => {
      this.setPage(0);
    })
  }

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    })
  }

  handleConfirmationResponse = () => {
    if (this.state.itemList.length === 1 && this.state.page === 1) {
      let count = this.state.page - 1
      this.setState({
        page: count,
      })
    }
    deleteCheckItem(this.state.id).then((res) => {
      this.updatePageData();
      this.handleDialogClose()
    }).catch((err) => {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "Supplier.noti.use"
      })
      // alert('Nhà cung cấp đang sử dụng, không thể xóa')
      // this.handleDialogClose()
    })
  }
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    })
  }
  async handleDeleteList(list) {
    let listAlert = [];
    for (var i = 0; i < list.length; i++) {

      try {
        await deleteCheckItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    this.handleDialogClose()
    if (listAlert.length === list.length) {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "Supplier.noti.use_all"
      })
      // alert("Nhà cung cấp đều đã sử dụng");
    } else if (listAlert.length > 0) {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "Supplier.noti.deleted_unused"
      })
      // alert("Đã xoá nhà cung cấp chưa sử dụng");
    }
  }
  handleDeleteButtonClick = () => {
    if (
      typeof this.state.data === 'undefined' ||
      this.state.data.length === 0
    ) {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "general.noti_check_data"
      })
      // alert('Chưa chọn dữ liệu')
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true })
    }
  }
  handleDeleteAll = (event) => {
    this.handleDeleteList(this.state.data).then(() => {
      this.updatePageData()
      // this.handleDialogClose()
    })
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  handleSetListCommonObjectType = (listCommonObjectType) => {
    this.setState({ listCommonObjectType });
  };

  handleSelectCommonObjectType = (commonObjectType) => {
    this.setState({ commonObjectType }, () => {
      // this.updatePageData();
      this.search();
    });
  };

  searchByStatus = (e, value) => {
    this.setState((preState) => ({ ...preState, isActive: value }), () => {
      this.search();
    });
  }

  handleKeyUp = (e) => {
    if (!e?.target?.value) {
      this.updatePageData();
    }
  }

  render() {
    const { t, i18n } = this.props
    let {
      keyword,
      shouldOpenNotificationPopup,
      commonObjectType,
      listCommonObjectType,
    } = this.state
    let TitlePage = t('Supplier.title')
    let commonObjectTypeSearchObject = {
      pageIndex: 1,
      pageSize: 10000,
      typeCode: TYPE_CODE_SUPPLIER.NCC.code,
    }

    let columns = [
      {
        title: t('general.action'),
        field: 'custom',
        align: 'left',
        minWidth: '100px',
        cellStyle: {
          textAlign: 'center',
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data.parent === null) {
                    data.parent = {}
                  }
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  })
                })
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id)
              } else {
                alert('Call Selected Here:' + rowData.id)
              }
            }}
          />
        ),
      },
      { title: t('Supplier.name'), field: 'name', minWidth: 450 },
      {
        title: t('Supplier.status'),
        field: 'isActive',
        minWidth: 140,
        align: 'center',
        render: (rowData) => <div>
          {rowData.isActive === STATUS_SUPPLIER.HOAT_DONG.code ? t("Supplier.active") : rowData.isActive === STATUS_SUPPLIER.KHONG_HOAT_DONG.code ? t("Supplier.unactive") : ""}
        </div>
      },
      {
        title: t('Supplier.code'),
        field: 'code',
        align: 'left',
        minWidth: '150px',
        cellStyle: {
          textAlign: 'center',
        }
      },
      {
        title: t('Supplier.type'),
        field: 'types',
        align: 'left',
        minWidth: '350px',
        render: (rowData) => rowData?.types
          ? (rowData?.types?.map(t => t.typeName)?.join(" | ") ?? "")
          : "",
      },
    ]

    const filterAutocomplete = createFilterOptions();
    return (
      <div className="m-sm-30">
        <ValidatorForm onSubmit={() => { }}>
          <Helmet>
            <title>{TitlePage} | {t('web_site')}</title>
          </Helmet>
          <div className="mb-sm-30">
            <Breadcrumb
              routeSegments={[
                { name: t('Dashboard.category'), path: '/list/supplier' },
                { name: TitlePage },
              ]}
            />
          </div>
          <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={4} xs={12} >
              <Button
                className="mt-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleEditItem(null)}
              >
                {t('general.add')}
              </Button>
              {/* <Button
                className="mt-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.handleDeleteButtonClick}
              >
                {t('general.delete')}
              </Button> */}
              <Button
                className="mt-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.importExcel}
              >
                {t("general.importExcel")}
              </Button>
              <Button
                className="mt-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.exportExampleImportExcel}
              >
                Mẫu Excel
              </Button>
              {this.state.shouldOpenImportExcelDialog && (
                <ImportExcelDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenImportExcelDialog}
                  handleOKEditClose={this.handleOKEditClose}
                />
              )}

              {this.state.shouldOpenConfirmationDeleteAllDialog && (
                <ConfirmationDialog
                  open={this.state.shouldOpenConfirmationDeleteAllDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDeleteAll}
                  text={t('general.deleteAllConfirm')}
                  cancel={t('general.cancel')}
                  agree={t('general.agree')}
                />
              )}
              {/* <TextField
                label={t('Supplier.search')}
                className="mb-16 mr-10"
                style={{ width: '20%' }}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
              />
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.search(keyword)}
              >
                {t('general.search')}
              </Button> */}
            </Grid>
            <Grid item container md={8} sm={12} xs={12} spacing={2}>
              <Grid item md={3} sm={12} xs={12} >
                <div >
                  <Autocomplete
                    className="search_box w-100"
                    fullWidth
                    options={[
                      {
                        name: t('Supplier.unactive'),
                        value: 0
                      },
                      {
                        name: t('Supplier.active'),
                        value: 1
                      },

                    ]}
                    value={this.state.isActive}
                    onChange={(e, value) => this.searchByStatus(e, value)}
                    getOptionLabel={(option) => option?.name || ""}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label={t('Supplier.filterStatus')}
                        variant="standard"
                      />
                    )}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                </div>
              </Grid>
              <Grid item md={3} sm={12} xs={12} >
                <AsynchronousAutocompleteSub
                  label={t("Supplier.type")}
                  searchObject={commonObjectTypeSearchObject}
                  searchFunction={searchCommonObject}
                  listData={listCommonObjectType}
                  setListData={this.handleSetListCommonObjectType}
                  value={commonObjectType ?? null}
                  defaultValue={commonObjectType ?? null}
                  displayLable={'name'}
                  onSelect={this.handleSelectCommonObjectType}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item md={6} sm={12} xs={12} >
                <FormControl fullWidth>
                  <Input
                    className='search_box w-100 mt-16'
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUp}
                    placeholder={t("Supplier.search")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment position='end'>
                        <Link to="#"> <SearchIcon
                          onClick={() => this.search(keyword)}
                          style={{
                            position: "absolute",
                            top: "0",
                            right: "0"
                          }} /></Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <div>
                {this.state.shouldOpenEditorDialog && (
                  <SupplierDialog
                    t={t}
                    i18n={i18n}
                    handleClose={this.handleDialogClose}
                    open={this.state.shouldOpenEditorDialog}
                    handleOKEditClose={this.handleOKEditClose}
                    item={this.state.item}
                  />
                )}

                {shouldOpenNotificationPopup && (
                  <NotificationPopup
                    title={t('general.noti')}
                    open={shouldOpenNotificationPopup}
                    // onConfirmDialogClose={this.handleDialogClose}
                    onYesClick={this.handleDialogClose}
                    text={t(this.state.Notification)}
                    agree={t('general.agree')}
                  />
                )}

                {this.state.shouldOpenConfirmationDialog && (
                  <ConfirmationDialog
                    title={t('general.confirm')}
                    open={this.state.shouldOpenConfirmationDialog}
                    onConfirmDialogClose={this.handleDialogClose}
                    onYesClick={this.handleConfirmationResponse}
                    text={t('general.deleteConfirm')}
                    agree={t('general.agree')}
                    cancel={t('general.cancel')}
                  />
                )}
              </div>
              <MaterialTable
                title={t('general.list')}
                data={this.state.itemList}
                columns={columns}
                //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  },
                  toolbar: {
                    // nRowsSelected: '${t('general.selects')}',
                    nRowsSelected: `${t('general.selects')}`
                  }
                }}

                options={{
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: '450px',
                  minBodyHeight: this.state.itemList?.length > 0
                    ? 0 : 250,
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                  padding: 'dense',
                  toolbar: false
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.setState({ data: rows })
                }}
              // actions={[
              //   {
              //     tooltip: 'Remove All Selected Users',
              //     icon: 'delete',
              //     onClick: (evt, data) => {
              //       this.handleDeleteAll(data);
              //       alert('You want to delete ' + data.length + ' rows');
              //     }
              //   },
              // ]}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
                component="div"
                labelRowsPerPage={t('general.rows_per_page')}
                labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
                count={this.state.totalElements}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </Grid>
        </ValidatorForm>
      </div>
    )
  }
}
Supplier.contextType = AppContext;
export default Supplier
