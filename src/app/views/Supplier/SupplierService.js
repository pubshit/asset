import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/supplier" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const searchByText = (text, pageIndex, pageSize, seacrhObject) => {
  let url = API_PATH + "/searchByText/" + pageIndex + "/" + pageSize;
  return axios.post(url, { keyword: text, ...seacrhObject });
};

export const getByPage = (searchObject) => {
  let url =
    API_PATH + "/" + searchObject.pageIndex + "/" + searchObject.pageSize;
  return axios.get(url);
};

export const getItemById = (id) => {
  var url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = (item) => {
  var url = API_PATH;
  return axios.post(url, item);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
export const exportExampleImportExcelSupplier = (supplier) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/fileDownload/exportExampleSupplier",
    data: supplier,
    responseType: "blob",
  });
};

export const searchByTextNew = (searchObject = {}) => {
  let url =
    API_PATH +
    "/searchByText/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  
  return axios.post(url, searchObject);
};

// http://192.168.1.27:8065/asvn/api/supplier/searchByPage
export const searchByTextDVBH = (searchObject) => {
  let url =
    API_PATH +
    "/searchByText/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.post(url, {
    keyword: searchObject.keyword || null,
    typeCode: searchObject.typeCode || null,
    typeCodes: searchObject.typeCodes || null,
    isActive: searchObject.isActive || null,
  });
};

export const searchByTextAndOrg = (searchObject) => {
  let url =
    API_PATH +
    "/searchByText/" +
    searchObject.pageIndex +
    "/" +
    searchObject.pageSize;
  return axios.post(url, {
    keyword: searchObject.keyword,
    typeCodes: searchObject.typeCodes,
  });
};
