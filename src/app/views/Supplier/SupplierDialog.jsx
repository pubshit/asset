import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  FormControlLabel,
  Grid,
} from '@material-ui/core'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {STATUS_SUPPLIER, TYPE_CODE_SUPPLIER, appConst, variable} from 'app/appConst'
import AppContext from 'app/appContext'
import React from 'react'
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { searchByPage as searchCommonObject } from '../CommonObject/CommonObjectService'
import { checkCode, getByPage, saveItem } from './SupplierService'
import { PaperComponent } from "../Component/Utilities";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import PropTypes from "prop-types";
import { handleKeyDownIntegerOnly, handleKeyDownSpecial } from 'app/appFunction'
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class SupplierDialog extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    listType: [],
    types: [],
    supplierType: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: '',
    active: true,
    isActive: STATUS_SUPPLIER.HOAT_DONG.code
  }
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData()
    })
  }
  handleChangeCommonObjectType = (event, source) => {
    let { commonObjectTypes } = this.state
    this.setState({
      type: commonObjectTypes.find((item) => item.id === event.target.value),
      typeId: event.target.value,
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 })
    this.updatePageData()
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }
  updatePageData = () => {
    var searchObject = {}
    searchObject.pageIndex = this.state.page
    searchObject.pageSize = this.state.rowsPerPage
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      })
    })
  }

  handleClick = (event, item) => {
    //alert(item);
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item })
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null })
    }
  }


  componentDidMount = () => {
    let { item, typeCodes} = this.props;

    this.setState({...item}, () =>{
      if (item?.id) {
        let types = [], supplierType = [];
        item?.types?.map(item => {
          supplierType.push({
            id: item?.typeId,
            code: item?.typeCode,
            name: item?.typeName,
          })
          types.push({
            typeId: item?.typeId,
            typeCode: item?.typeCode,
            typeName: item?.typeName,
          })
        })
        this.setState({ supplierType, types });
      } else {
        searchCommonObject({ pageIndex: 1, pageSize: 9999, typeCode: TYPE_CODE_SUPPLIER.NCC.code })
          .then(data => {
            data?.data?.content?.map(supplier => {
              let isExist = typeCodes?.some(code => code === supplier.code);
              if (isExist) {
                this.setState({
                  supplierType: [{
                    id: supplier.id,
                    code: supplier?.code,
                    name: supplier.name
                  }],
                  types: [{
                    typeId: supplier.id,
                    typeCode: supplier?.code,
                    typeName: supplier.name
                  }]
                })
              }
            })
          })
          .catch(error => {
            console.error(error);
          });
      }
    })
  }

  search = (keyword) => {
    var searchObject = {}
    searchObject.text = keyword
    searchObject.pageIndex = this.state.page
    searchObject.pageSize = this.state.rowsPerPage
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      })
    })
  }

  handleChange(event) {
    this.setState({ keyword: event.target.value })
  }
  handleChangeValue = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value })
  }
  handleChangeStatus = () => {
    this.setState({ isActive: this.state.isActive === STATUS_SUPPLIER.HOAT_DONG.code ? STATUS_SUPPLIER.KHONG_HOAT_DONG.code : STATUS_SUPPLIER.HOAT_DONG.code })
  }

  convertDataSubmit = (value) => {
    return {
      address: value?.address,
      code: value?.code,
      commune: value?.commune,
      contactEmail: value?.contactEmail,
      contactInformation: value?.contactInformation,
      contactPhoneNumber: value?.contactPhoneNumber,
      contactPosition: value?.contactPosition,
      createDate: value?.createDate,
      createdBy: value?.createdBy,
      district: value?.district,
      email: value?.email,
      id: value?.id,
      isActive: value?.isActive || STATUS_SUPPLIER.KHONG_HOAT_DONG.code,
      landlinePhone: value?.landlinePhone,
      modifiedBy: value?.modifiedBy,
      modifyDate: value?.modifyDate,
      name: value?.name,
      national: value?.national,
      phoneNumber: value?.phoneNumber,
      province: value?.province,
      taxCode: value?.taxCode,
      types: value?.types,
      voided: value?.voided,
      website: value?.website,
    }
  }

  handleFormSubmit = async () => {
    let { id, code } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let {
      t,
      handleClose,
      handleOKEditClose = () => { },
      handleSelectDVBH = () => { },
      handleSelect = () => { },
    } = this.props;
    let resCheckCode = await checkCode(id, code)

    //Nếu trả về true là code đã được sử dụng
    if (resCheckCode?.data) {
      toast.warning(t("Supplier.noti.dupli_code"))
      setPageLoading(false);
    }
    else {
      try {
        let dataSubmit = this.convertDataSubmit({ ...this.state })
        let res = await saveItem(dataSubmit)
        const { message } = res.data;
        if (appConst.CODE.SUCCESS === res.status) {
          handleSelectDVBH(res?.data);
          handleSelect(res?.data);
          id ? toast.success(t("general.updateSuccess"))
            : toast.success(t("general.addSuccess"))
          handleOKEditClose();
          handleClose()
        } else {
          toast.warning(message);
          handleClose();
        }
      }
      catch (e) {
        console.error(e)
        toast.error(t("general.error"))
      }
      finally {
        setPageLoading(false);
      }
    }
  }
  
  handleSelectType = (supplierType) => {
    let { isMaintainRequest, t } = this.props;
    let types = [];
    let isCheck = supplierType?.some(item => item.code === appConst.TYPE_CODES.NCC_BDSC)

    if (isMaintainRequest && !isCheck) {
      toast.warning(t("maintainRequest.isMaintainRequestType"));
      return;
    }
    //eslint-disable-next-line
    supplierType?.map(item => {
      types.push({
        typeId: item?.id,
        typeName: item?.name,
        typeCode: item?.code,
      });
      return item
    })
    this.setState({ types, supplierType })
  }
  setListTypeData = (listType = []) => {
    this.setState({ listType, })
  }
  render() {
    const {
      t,
      handleClose,
      open,
      disabledType,
    } = this.props
    let {
      name,
      code,
      email,
      isActive,
      taxCode,
      website,
      address,
      commune,
      province,
      district,
      national,
      phoneNumber,
      supplierType,
      contactEmail,
      contactPosition,
      contactPhoneNumber,
    } = this.state;

    let typesSearchObject = { pageIndex: 1, pageSize: 9999, typeCode: TYPE_CODE_SUPPLIER.NCC.code }
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {this.props?.title ? this.props?.title : t('Supplier.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} id="formm">
          <DialogContent>
            <Grid container spacing={1} style={{ fontSize: 16 }}>
              <Grid item sm={12} xs={12} >
                <b>
                  {t("Asset.tab_info_asset")}:
                </b>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed"> *</span>
                      {t('Supplier.name')}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  onKeyDown={(e) => {
                    handleKeyDownSpecial(e)
                  }}
                  type="text"
                  name="name"
                  value={name || ""}
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed"> *</span>

                      {t('Supplier.code')}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="code"
                  value={code || ""}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode"),]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <TextValidator
                  fullWidth
                  label={t('Supplier.taxCode')}
                  onChange={this.handleChangeValue}
                  type="text"
                  name="taxCode"
                  value={taxCode || ""}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <TextValidator
                  fullWidth
                  label={t('Supplier.nation')}
                  onChange={this.handleChangeValue}
                  onKeyDown={handleKeyDownSpecial}
                  type="text"
                  name="national"
                  value={national || ""}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("general.email")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="email"
                  value={email || ""}
                  validators={["isEmail", `matchRegexp:${variable.regex.emailValid}`]}
                  errorMessages={[t("general.emailNotValid"), t("general.emailNotValid")]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {'Website'}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="website"
                  value={website || ""}
                  validators={[`matchRegexp:${variable.regex.urlWebsiteValid}`]}
                  errorMessages={[t("Supplier.noti.error_url")]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t('Supplier.phone')}
                  onChange={this.handleChangeValue}
                  type="text"
                  name="phoneNumber"
                  value={phoneNumber || ""}
                  validators={["isNumber", `matchRegexp:${variable.regex.phoneValid}`]}
                  errorMessages={[t("Supplier.noti.is_number"), t("Supplier.noti.is_numberphone")]}
                />
              </Grid>
              {/* <Grid item sm={2} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.mobilePhone")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="landlinePhone"
                  value={landlinePhone || ""}
                  validators={["isNumber", `matchRegexp:${variable.regex.phoneValid}`]}
                  errorMessages={[t("Supplier.noti.is_number"), t("Supplier.noti.is_numberphone")]}
                />
              </Grid> */}
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.address")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="address"
                  value={address || ""}
                  validators={[
                   `matchRegexp:${variable.regex.addressValid}`
                  ]}
                  errorMessages={[t("general.addressNotValid")]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.province")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="province"
                  value={province || ""}
                  validators={[
                    `matchRegexp:${variable.regex.addressValid}`
                   ]}
                   errorMessages={[t("general.addressNotValid")]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.district")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="district"
                  value={district || ""}
                  validators={[
                    `matchRegexp:${variable.regex.addressValid}`
                   ]}
                   errorMessages={[t("general.addressNotValid")]}
                />
              </Grid>
              <Grid item sm={2} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.commune")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  type="text"
                  name="commune"
                  value={commune || ""}
                  validators={[
                    `matchRegexp:${variable.regex.addressValid}`
                   ]}
                   errorMessages={[t("general.addressNotValid")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  multiple
                  options={[]}
                  listData={this.state.listType}
                  setListData={this.setListTypeData}
                  value={supplierType ?? []}
                  label={
                    <span>
                      <span className="colorRed"> *</span>
                      {t("Supplier.type")}
                    </span>
                  }
                  searchObject={typesSearchObject}
                  searchFunction={searchCommonObject}
                  onSelect={(value) => this.handleSelectType(value)}
                  defaultValue={supplierType ?? []}
                  displayLable={'name'}
                  noOptionsText={t("general.noOption")}
                  disabled={disabledType}
                  validators={["required"]}
                  errorMessages={[t("general.required"),]}
                />
              </Grid>
              <Grid item sm={12} xs={12} style={{ marginTop: 30 }} >
                <b>
                  {t("organization.personContact")}:
                </b>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.contactInformation")}
                    </span>
                  }
                  onChange={this.handleChangeValue}
                  onKeyDown={(e) => {
                    handleKeyDownSpecial(e)
                  }}
                  type="text"
                  name='contactPosition'
                  value={contactPosition || ""}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("Supplier.contactMobilePhone")}
                    </span>
                  }
                  type="text"
                  name='contactPhoneNumber'
                  value={contactPhoneNumber || ""}
                  onChange={this.handleChangeValue}
                  validators={["isNumber", `matchRegexp:${variable.regex.phoneValid}`]}
                  errorMessages={[t("Supplier.noti.is_number"), t("Supplier.noti.is_numberphone")]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t('Supplier.contactEmail')}
                    </span>
                  }
                  type="text"
                  name='contactEmail'
                  value={contactEmail || ""}
                  onChange={this.handleChangeValue}
                  validators={["isEmail", `matchRegexp:${variable.regex.emailValid}`]}
                  errorMessages={[t("general.emailNotValid"), t("general.emailNotValid")]}
                />
              </Grid>
              <Grid item sm={8} xs={12} style={{ marginTop: 12 }}>
                <FormControlLabel
                  control={<Checkbox checked={isActive && true} onChange={this.handleChangeStatus} />}
                  label={`${t("Supplier.status")}: ${isActive ? t("Supplier.active") : t("Supplier.unactive")}`}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={handleClose}
              >
                {t('general.cancel')}
              </Button>
              <Button variant="contained" color="primary" type="submit" className="mr-15">
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    )
  }
}
SupplierDialog.contextType = AppContext;
export default SupplierDialog
SupplierDialog.propTypes = {
  t: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  disabledType: PropTypes.bool,
}
