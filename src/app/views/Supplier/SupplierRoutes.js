import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const Supplier = EgretLoadable({
  loader: () => import("./Supplier")
});
const ViewComponent = withTranslation()(Supplier);

const SupplierRoutes = [
  {
    path: ConstantList.ROOT_PATH + "list/supplier",
    exact: true,
    component: ViewComponent
  }
];

export default SupplierRoutes;
