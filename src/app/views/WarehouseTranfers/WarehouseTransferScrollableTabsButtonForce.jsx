import React, { useEffect } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {
  getListUserByDepartmentId,
  getListWarehouseByDepartmentId
} from '../AssetTransfer/AssetTransferService';
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { useState } from "react";
import { createFilterOptions } from "@material-ui/lab";
import {
  LightTooltip,
  TabPanel,
  checkInvalidDate,
  convertNumberPriceRoundUp,
  isCheckLenght,
  a11yProps
} from "app/appFunction";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { STATUS_STORE, appConst, variable } from "app/appConst";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import { searchByPageAsset } from "../WarehouseReceiving/WarehouseReceivingService";
import { COLUMNS_SubTableVT } from "../WarehouseDelivery/constants";
import SelectMultiProductPopup from "../Component/Product/SelectMultiProductPopup";
import ConstantList from "../../appConfig";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function WarehouseTransferScrollableTabsButtonForce(props) {
  const { isView } = props.item;
  const { assetClass, i18n, t } = props;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [listItem, setListItem] = useState([])
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [isEdit, setIsEdit] = useState(false);
  const isTSCĐ = assetClass === appConst.assetClass.TSCD
  const isCCDC = assetClass === appConst.assetClass.CCDC
  let transferStatus = appConst.listStatusInventoryStore.find(
    status => status.indexOrder === props?.item?.status
      || status.indexOrder === props?.item?.transferStatus?.indexOrder
  )
    || appConst.STATUS_WAREHOUSE_TRANSFER.MOI_TAO;
  const [searchObject, setSearchObject] = useState({ pageAssets: 0, rowsPerPageAssets: 5, totalElementAssets: 0 })
  const searchObjectHandoverWarehouse = {
    isActive: STATUS_STORE.HOAT_DONG.code,
    pageIndex: 1, pageSize: 1000000,
    departmentId: props?.item?.department
      ? props?.item?.department?.id
      : null,
    isAssetManagement: true,
  };
  let personExportSearchObject = {
    pageIndex: 0,
    pageSize: 1000000,
    isAssetManagement: true,
    departmentId: props?.item?.department
      ? props?.item?.department.id
      : null,
  };
  const searchObjectDepartment = { pageIndex: 1, pageSize: 1000000, checkPermissionUserDepartment: false }
  const filterAutocomplete = createFilterOptions();
  const isKhoTaiSan = assetClass === appConst.assetClass.TSCD || assetClass === appConst.assetClass.CCDC
  const isDangThucHien = (props?.item?.transferStatus?.indexOrder || props?.item?.transferStatus?.transferStatusIndex)
    === appConst.STATUS_WAREHOUSE_TRANSFER.DANG_THUC_HIEN.indexOrder;
  const isKetThuc = (props?.item?.transferStatus?.indexOrder || props?.item?.transferStatus?.transferStatusIndex)
    === appConst.STATUS_WAREHOUSE_TRANSFER.KET_THUC.indexOrder;

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.department?.id ?? '',
      pageIndex: 0,
      pageSize: 100000,
    });
    //eslint-disable-next-line
  }, [props?.item?.department, props?.item?.departmentReceipt]);

  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255)
    })
    //eslint-disable-next-line
  }, [props?.item?.assetVouchers]);

  useEffect(() => {
    if (props?.item?.id && (isTSCĐ || isCCDC)) {
      setIsEdit(true)
      handleGetDataAsset(props?.item?.id)
    }
    //eslint-disable-next-line
  }, [props?.item?.id])

  const handleGetDataAsset = async (id) => {
    const dataSearch = {}
    dataSearch.pageIndex = searchObject.pageAssets + 1;
    dataSearch.pageSize = searchObject.rowsPerPageAssets;
    try {
      const { data } = await searchByPageAsset(id, dataSearch)
      if (appConst.CODE.SUCCESS === data?.code) {
        setListItem(data?.data?.content || [])
        let assetVouchersUpdate = data?.data?.content?.map(item => {
          item.asset = {
            id: item?.assetId,
          }
          return item
        })

        props.setAssetVouchers(assetVouchersUpdate)
        setSearchObject({ ...searchObject, totalElementAssets: data?.data?.totalElements })
      } else {
        props.setAssetVouchers([])
        setListItem([])
      }
    } catch (e) {
      console.error(e)
    }
  }
  let columns = props.assetClass === appConst.assetClass.VT ? [
    ...(!props?.item?.isView
      ? [
        {
          title: t("Asset.action"),
          field: "custom",
          align: "left",
          minWidth: 80,
          hidden: isDangThucHien || isKetThuc || props.isView,
          cellStyle: {
            textAlign: "center",
          },
          render: (rowData) =>
            !props?.item?.isView && (
              <MaterialButton
                item={rowData}
                onSelect={(rowData, method) => {
                  if (appConst.active.delete === method) {
                    props.removeAssetInlist(
                      rowData?.asset?.id ||
                      rowData?.assetId ||
                      rowData?.productId
                    );
                  } else {
                    alert("Call Selected Here:" + rowData?.id);
                  }
                }}
              />
            ),
        },
      ]
      : []),
    ...COLUMNS_SubTableVT(t, props, true)
  ] : [
    ...(!isView ? [{
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 120,
      hidden: isDangThucHien || isKetThuc || props.isView,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData?.asset?.id || rowData?.assetId);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    }] : []),
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.code"),
      field: isView || isEdit ? "assetCode" : "asset.code",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetCode || rowData?.asset?.code

    },
    {
      title: t("Asset.managementCode"),
      field: isView || isEdit ? "assetManagementCode" : "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetManagementCode || rowData?.asset?.managementCode
    },
    {
      title: t("Asset.name"),
      field: isView || isEdit ? "assetName" : "asset.name",
      align: "left",
      minWidth: 250,
      maxWidth: 400,
      render: (rowData) => rowData?.assetName || rowData?.asset?.name
    },
    ...(props.assetClass === appConst.assetClass.CCDC ? [
      {
        title: t("AssetWarehouseTransfer.quantity"),
        field: isView || isEdit ? "totalQuantityExport" : "asset.totalQuantityExport",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center"
        },
        render: rowData =>
          <TextValidator
            className="w-100"
            type="number"
            onChange={totalQuantityExport => props.handleRowDataCellChange(rowData, totalQuantityExport)}
            name="totalQuantityExport"
            value={rowData?.totalQuantityExport || rowData?.asset?.quantity}
            inputProps={{
              readOnly: props?.item?.isView || props?.item?.isStatus,
              className: "text-center"
            }}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
      },
    ] : []),
    {
      title: t("Asset.serialNumber"),
      field: isView || isEdit ? "assetSerialNumber" : "asset.serialNumber",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.assetSerialNumber || rowData?.asset?.serialNumber

    },
    {
      title: t("Asset.model"),
      field: isView || isEdit ? "assetModel" : "asset.model",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.assetModel || rowData?.asset?.model
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: isView || isEdit ? "assetYearOfManufacture" : "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetYearOfManufacture || rowData?.asset?.yearOfManufacture
    },
    {
      title: t("Asset.manufacturer"),
      field: isView || isEdit ? "assetManufacturerName" : "asset.manufacturerName",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
      render: (rowData) => rowData?.assetManufacturerName || rowData?.asset?.manufacturerName
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "assetOriginalCost",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPriceRoundUp(rowData?.assetOriginalCost || rowData?.asset?.originalCost),
    },
    {
      title: t("Asset.carryingAmount"),
      field: isView || isEdit ? "assetCarryingAmount" : "asset.carryingAmount",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPriceRoundUp(rowData?.assetCarryingAmount || rowData?.asset?.carryingAmount),
    },
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      headerStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      headerStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
      headerStyle: {
      },
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
      headerStyle: {
      },
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      headerStyle: {
        textAlign: "center",
      },
      render: rowData =>
        // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  }
  let isViewIssueDate = props?.item?.isView || props?.item?.isStatus || ((isDangThucHien || isKetThuc) && props?.item?.id);
  return (
    <form>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} />
            {/* <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} /> */}
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={1}>
            {/* <Grid item md={2} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <DateTimePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('Ngày tạo phiếu')}
                    </span>}
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={'createDate'}
                  value={props?.item?.createDate}
                  InputProps={{
                    readOnly: true,
                  }}
                  readOnly={true}
                />
              </MuiPickersUtilsProvider>
            </Grid> */}
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("Store.issueDate")}
                    </span>
                  }
                  name={'inputDate'}
                  inputVariant="standard"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.item.inputDate}
                  onChange={date => props?.handleDateChange(date, "inputDate")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  InputProps={{
                    readOnly: isViewIssueDate
                  }}
                  open={isPickerOpen}
                  onOpen={() => {
                    if (isViewIssueDate) {
                      setIsPickerOpen(false);
                    }
                    else {
                      setIsPickerOpen(true);
                    }
                  }}
                  onClose={() => setIsPickerOpen(false)}
                  validators={transferStatus.indexOrder === appConst?.listStatusInventoryStore[2].indexOrder ? ['required'] : []}
                  errorMessages={transferStatus.indexOrder === appConst?.listStatusInventoryStore[2].indexOrder ? t('general.required') : []}
                  minDate={new Date("01/01/1900")}
                  minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
                  onBlur={() => handleBlurDate(props?.item.issueDate, "issueDate")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  maxDate={new Date()}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={6} xs={12}>
              {/* {(!props?.item?.isNew && !props?.item?.isStatus) || !props?.item?.id ? */}
              {
                // props?.item?.isView || props?.item?.isStatus || 
                //   (props?.isRoleAssetUser ? true : (props?.isRoleAssetManager ? false : !props?.item?.id)) 
                (props?.item?.isView || props?.item?.isStatus || props?.isRoleAssetUser)
                  ?
                  <TextValidator
                    className="w-100"
                    InputProps={{
                      readOnly: true,
                    }}
                    label={
                      <span>
                        {t("maintainRequest.status")}
                      </span>
                    }
                    value={transferStatus ? transferStatus?.name : ""}
                  /> : <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("maintainRequest.status")}
                      </span>
                    }
                    listData={appConst.listStatusInventoryStore}
                    setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                    defaultValue={transferStatus ? transferStatus : null}
                    value={transferStatus ? transferStatus : null}
                    displayLable={'name'}
                    isNoRenderParent
                    isNoRenderChildren
                    onSelect={props.selectAssetTransferStatus}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    noOptionsText={t("general.noOption")}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                    typeReturnFunction="status"
                  />}
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.item?.isStatus || props?.isRoleAssetUser) ?
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("allocation_asset.handoverDepartment")}
                  value={props.item?.department ? props.item?.department?.name : null}
                  InputProps={{
                    readOnly: true
                  }}
                />
                :
                <AsynchronousAutocompleteTransfer
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      <span> {t("allocation_asset.handoverDepartment")}</span>
                    </span>}
                  searchFunction={getListOrgManagementDepartment}
                  searchObject={personExportSearchObject}
                  listData={props.item.listHandoverDepartment}
                  setListData={(data) => props?.handleSetDataSelect(data, "listHandoverDepartment")}
                  defaultValue={props?.item?.department ? props?.item?.department : ''}
                  displayLable={'name'}
                  value={props?.item?.department ? props?.item?.department : ''}
                  valueTextValidator={props.item.department}
                  onSelect={department => props?.handleSelectHandoverDepartment(department)}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  typeReturnFunction="list"
                  showCode={"showCode"}
                  InputProps={{
                    readOnly: true
                  }}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  disabled={props.isRoleAssetManager}
                />
              }
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.item?.isStatus || props?.isRoleAssetUser) ?
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("AssetWarehouseTransfer.handoverWarehouse")}
                  value={props?.item?.storeExportName ? props?.item?.storeExportName : ''}
                  InputProps={{
                    readOnly: true
                  }}
                />
                :
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("AssetWarehouseTransfer.handoverWarehouse")}
                    </span>
                  }
                  searchFunction={getListWarehouseByDepartmentId}
                  searchObject={searchObjectHandoverWarehouse}
                  defaultValue={props?.item?.storeExport ?? ''}
                  displayLable="name"
                  value={props?.item?.storeExport ?? ''}
                  onSelect={storeExport => props?.handleSelectHandoverWarehouse(storeExport)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  disabled={!props?.item?.department?.id}
                />
              }
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                className="w-100"
                label={
                  <span>
                    {/* <span className="colorRed">* </span> */}
                    {t("allocation_asset.handoverPerson")}
                  </span>
                }
                readOnly={isView}
                disabled={!props?.item?.department?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByHandoverDepartment}
                defaultValue={props?.item?.personExport ?? ''}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                value={props?.item?.personExport ?? ''}
                onSelect={personExport => props?.handleSelectHandoverPerson(personExport)}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
              // validators={["required"]}
              // errorMessages={[t('general.required')]}
              />
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.item?.isView || props?.item?.isStatus || props?.isRoleAssetUser) ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      {t("AssetWarehouseTransfer.receiverWarehouse")}
                    </span>
                  }
                  value={props?.item?.storeReceiptName ? props?.item?.storeReceiptName : ''}
                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("AssetWarehouseTransfer.receiverWarehouse")}
                    </span>
                  }
                  searchFunction={getListWarehouseByDepartmentId}
                  // searchObject={searchObjectreceiverWarehouse}
                  searchObject={searchObjectHandoverWarehouse}
                  defaultValue={props?.item?.storeReceipt ?? ''}
                  displayLable="name"
                  value={props?.item?.storeReceipt ?? ''}
                  onSelect={receiverWarehouse => props?.handleSelectReceiverWarehouse(receiverWarehouse)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  // disabled={!props?.item?.departmentReceipt?.id}
                  disabled={!props?.item?.department?.id}
                />}
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                className="w-100"
                label={
                  <span>
                    {/* <span className="colorRed">* </span> */}
                    {t("AssetTransfer.receiverPerson")}
                  </span>
                }
                readOnly={isView}
                disabled={!props?.item?.department?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByHandoverDepartment}
                // searchObject={searchParamUserByReceiverDepartment}
                defaultValue={props?.item?.personReceipt ?? ''}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                value={props?.item?.personReceipt ?? ''}
                onSelect={personReceipt => props?.handleSelectReceiverPerson(personReceipt)}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
              // validators={["required"]}
              // errorMessages={[t('general.required')]}
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12}></Grid>
            <Grid item md={3} sm={12} xs={12}></Grid>
            <Grid item md={3} sm={12} xs={12}>
              {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
                <Button variant="contained" color="primary"
                  size="small" className="mb-16 w-100"
                  onClick={props?.openPopupSelectAsset}
                  disabled={!props?.item?.storeExport?.id || ((isDangThucHien || isKetThuc) && props?.item?.id)}
                >
                  {
                    props.assetClass === appConst.assetClass.VT
                      ? t('AssetWarehouseTransfer.select_supplies')
                      : props.assetClass === appConst.assetClass.CCDC
                        ? t('InstrumentToolsTransfer.choose')
                        : t('AssetWarehouseTransfer.select_asset')
                  }
                </Button>}
              {props?.item?.department
                && props?.item?.shouldOpenAssetPopup
                && isKhoTaiSan
                && (
                  <SelectAssetAllPopup
                    open={props.item.shouldOpenAssetPopup && isKhoTaiSan}
                    handleSelect={props.handleSelectAsset}
                    departmentId={props.item.departmentId}
                    storeId={props?.item?.storeExport?.id}
                    statusIndexOrders={props.item.statusIndexOrders}
                    handleClose={props.handleAssetPopupClose}
                    t={t} i18n={i18n}
                    departmentReceiptId={props.item.department != null ? props.item.department.id : ''}
                    assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                    issueDateTop={props.item?.issueDate}
                    isWarehouseAssetTransfer={true}
                    type={variable.listInputName.warehouseTransfer}
                    assetClass={props?.assetClass}
                    ngayChuyenKho={props?.item.inputDate}
                  />
                )}
              {props?.item?.department
                && props?.item?.shouldOpenAssetPopup
                && !isKhoTaiSan
                && (
                  <SelectMultiProductPopup
                    open={props?.item?.shouldOpenAssetPopup}
                    handleSelect={props.handleSelectAsset}
                    handleClose={props.handleAssetPopupClose}
                    assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                    t={t}
                    i18n={i18n}
                    voucherType={ConstantList.VOUCHER_TYPE.Store_Transfer}
                    selectedItem={{ product: null }}
                    storeId={
                      props?.item?.storeExport?.id
                    }
                    date={props?.item.inputDate}
                  />
                )
              }
            </Grid>
          </Grid>
          <Grid item md={10} sm={12} xs={12}></Grid>
          <Grid spacing={2}>
            <MaterialTable
              data={props?.item?.assetVouchers}
              // data={props?.item?.id ? listItem : props?.item?.assetVouchers}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12}>
            {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={
                  props.handleAddAssetDocumentItem
                }
              >
                {t('AssetFile.addAssetFile')}
              </Button>
            }
            {props?.item?.shouldOpenPopupAssetFile && (
              <VoucherFilePopup
                open={props?.item.shouldOpenPopupAssetFile}
                handleClose={props?.handleAssetFilePopupClose}
                itemAssetDocument={props?.itemAssetDocument}
                getAssetDocument={props?.getAssetDocument}
                handleUpdateAssetDocument={props?.handleUpdateAssetDocument}
                documentType={props?.item?.documentType}
                item={props?.item?.item}
                configItem={(item) => ({
                  name: item?.name,
                  isEditAssetDocument: Boolean(item?.id),
                })}
                t={t}
                i18n={i18n}
              />)
            }
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props?.item?.documents || []}
              columns={columnsVoucherFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '285px',
                minBodyHeight: '285px',
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div >
    </form>
  );
}