import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const WarehouseTransferMaterialManagementTable = EgretLoadable({
  loader: () => import("./WarehouseTransferMaterialManagementTable")
});
const ViewComponent = withTranslation()(WarehouseTransferMaterialManagementTable);

const WarehouseTransferMaterialManagementRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "material-management/warehouse-tranfers",
    exact: true,
    component: ViewComponent
  }
];

export default WarehouseTransferMaterialManagementRoutes;