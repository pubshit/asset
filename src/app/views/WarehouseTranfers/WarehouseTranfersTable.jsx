import {
  AppBar,
  CircularProgress,
  Icon,
  IconButton,
  Tab,
  TablePagination,
  Tabs,
} from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { appConst, typeWarehouseVoucher } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  TabPanel,
  convertNumberPrice,
  convertNumberPriceRoundUp,
  formatDateTimeDto,
  formatDateTypeArray,
  getTheHighestRole,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isValidDate, functionExportToExcel
} from "app/appFunction";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { saveAs } from "file-saver";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentTransferTable from "./ComponentTransferTable";
import {
  deleteItem,
  endInventoryCCDC,
  endInventoryTSCD,
  endInventoryVT,
  exportToExcelMaterials,
  exportToExcelNew,
  getCountStatus,
  getItemById,
  searchByPage,
  updateStatus,
} from "./WarehouseTranfersService";
import MaterialTable, { MTableToolbar } from "material-table";
import { searchByPageAsset } from "../WarehouseDelivery/WarehouseDeliveryService";
import ReceiptOfDeliveryForm from "../FormCustom/BBGN";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import { convertFromToDate } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const isMoiTao = item?.status === appConst.STATUS_WAREHOUSE_TRANSFER.MOI_TAO.indexOrder;
  const isDangThucHien = item?.status === appConst.STATUS_WAREHOUSE_TRANSFER.DANG_THUC_HIEN.indexOrder;

  return (
    <div className="none_wrap" onClick={(event) => event?.stopPropagation()}>
      {(isMoiTao || isDangThucHien) && (
        <LightTooltip
          title={t("AssetWarehouseTransfer.status.end")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.check)}
          >
            <CheckCircleIcon className="iconCheck" />
          </IconButton>
        </LightTooltip>
      )}
      {(isMoiTao || isDangThucHien) && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {(isMoiTao || isDangThucHien) && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {(
        <LightTooltip
          title={t("InstrumentToolsTransfer.watchInformation")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <VisibilityIcon size="small" className="iconEye" />
          </IconButton>
        </LightTooltip>
      )}
      {(
        <LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

class WarehouseTransferTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    subRowsPerPage: 5,
    subPage: 0,
    subTotalElements: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    hasCreatePermission: true,
    status: null,
    tabValue: 0,
    isRoleAdmin: false,
    isCheckReceiverDP: true,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataAssetVouchersTable: [],
    countStatusWaitConfirmation: 0,
    countStatusConfirmation: 0,
    countStatusTransferred: 0,
    isLoadingSubTable: false,
    toDate: null,
    fromDate: null,
    listStore: [],
    statusFilter: null,
    advancedSearch: true,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = typeWarehouseVoucher.Transfer; //Điều chuyển

  handleSetDate = (data, name) => {
    if (data === null) {
      this.setState({ [name]: data }, () => { this.search() });
    } else {
      this.setState({ [name]: data }, () => { isValidDate(data) && this.search() });
    }
  };

  handleSetListStore = (value) => {
    this.setState({
      listStore: value
    })
  }

  handleSelectStore = (store, name) => {
    this.setState({ [name]: store }, () => {
      this.search();
    });
  };

  selectAssetTransferStatus = (item) => {
    this.setState({
      statusFilter: item,
    }, () => {
      this.search()
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabWarehouseTransfer?.tabAll === newValue) {
      this.setState({
        status: null,
      }, () => this.updatePageData());
    }
    if (appConst?.tabWarehouseTransfer?.tabNew === newValue) {
      this.setState({
        status: appConst.listStatusWarehouseTransfer[0].indexOrder,
      }, () => this.updatePageData());
    }
    if (appConst?.tabWarehouseTransfer?.tabProcessing === newValue) {
      this.setState({
        status: appConst.listStatusWarehouseTransfer[1].indexOrder,
      }, () => this.updatePageData()
      );
    }
    if (appConst?.tabWarehouseTransfer?.tabEnd === newValue) {
      this.setState({
        status: appConst.listStatusWarehouseTransfer[2].indexOrder,
      }, () => this.updatePageData());
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleChangeSubPage = (event, newPage) => {
    this.setSubPage(newPage);
  };

  setSubPage = (subPage) => {
    this.setState({ subPage }, function () {
      this.updateSubTablePageData();
    });
  };

  setSubRowsPerPage = (event) => {
    this.setState({ subRowsPerPage: event.target.value, subPage: 0 }, function () {
      this.updateSubTablePageData();
    });
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword.trim() || null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.status = this.state?.status || this.state?.statusFilter?.indexOrder;
    searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
    searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
    searchObject.assetClass = this.props?.assetClass;
    searchObject.storeExportId = this.state.storeExport?.id
    searchObject.storeReceiptId = this.state.storeReceipt?.id
    searchByPage(searchObject)
      .then((res) => {
        const { data, code } = res?.data;
        if (isSuccessfulResponse(code)) {
          this.setState(
            {
              voucherId: null,
              itemList: [...data?.content],
              totalElements: data?.totalElements,
              rowDataAssetVouchersTable: [],
            },
            () => setPageLoading(false)
          );
        }
        else {
          handleThrowResponseMessage(res);
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  exportToExcel = async () => {
    let { setPageLoading } = this.context;
    const { toDate, fromDate } = this.state;
    const { t } = this.props;
    const searchObject = {};
    let name = '';
    let apiFunction;

    try {
      setPageLoading(true);
      searchObject.type = this.voucherType;
      searchObject.keyword = this.state.keyword.trim() || null;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.status = this.state?.status || this.state?.statusFilter?.indexOrder;
      searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
      searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
      searchObject.assetClass = this.props.assetClass;
      searchObject.storeExportId = this.state.storeExport?.id;
      searchObject.storeReceiptId = this.state.storeReceipt?.id;

      switch (this.props.assetClass) {
        case appConst.assetClass.TSCD:
          name = t("exportToExcel.fixedAssetWarehouseTransfer");
          apiFunction = exportToExcelNew;
          break;
        case appConst.assetClass.CCDC:
          name = t("exportToExcel.iatWarehouseTransfer");
          apiFunction = exportToExcelNew;
          break;
        case appConst.assetClass.VT:
          name = t("exportToExcel.suppliesWarehouseTransfer");
          apiFunction = exportToExcelMaterials;
          break;
        default:
          name = "Chuyển kho";
          apiFunction = exportToExcelNew;
          break;
      }

      await functionExportToExcel(
        apiFunction,
        searchObject,
        name,
      );
    } catch (err) {
      toast.error(t("toastr.error"));
      toast.clearWaitingQueue();
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationEndTransferDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationEndTransferDialog: false,
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  // handleEditAssetTransfer = (item) => {
  //   getItemById(item.id).then(({ data }) => {
  //     this.setState({
  //       item: data?.data,
  //       shouldOpenEditorDialog: true,
  //     });
  //   });
  // };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    deleteItem(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(this.props.t("general.deleteSuccess"));
          this.handleDialogClose();
          this.updatePageData();
        } else {
          toast.warning(data?.message);
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    this.setState(
      {
        status: null,
      },
      () => this.updatePageData()
    );
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasWatchPermission,
      hasCreatePermission,
    } = this.state;
    let { assetClass } = this.props;
    const isVT = assetClass === appConst.assetClass.VT;
    const isTSCĐ = assetClass === appConst.assetClass.TSCD;
    const isCCDC = assetClass === appConst.assetClass.CCDC;
    const roles = getTheHighestRole();

    if (roles.currentUser) {
      if (roles.isRoleUser) {
        if (!hasWatchPermission) {
          hasWatchPermission = true;
        }
        if (hasCreatePermission) {
          hasCreatePermission = false;
        }
      }
      this.setState({
        ...roles,
        isVT,
        isTSCĐ,
        isCCDC,
        hasWatchPermission,
        hasCreatePermission,
      });
    }
  };

  handleButtonAdd = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    if (item.checked === null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetTransfer.length; i++) {
      if (
        AssetTransfer[i].checked === null ||
        AssetTransfer[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (AssetTransfer[i].id === item.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (var i = 0; i < AssetTransfer.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  setStateTransfer = (item) => {
    this.setState(item);
  };

  handleConfirmationReceive = () => {
    let { setPageLoading } = this.context;
    let status = appConst.listStatusWarehouseTransfer[2].indexOrder;
    let issueDate = formatDateTimeDto(this.state.item?.issueDate, 23, 59, 59);
    let dataTranfer = {
      dataState: this.state?.item,
      id: this.state?.item?.id,
      status: status,
      issueDate: issueDate,
    };

    setPageLoading(true);
    updateStatus(dataTranfer)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info("Cập nhật thành công");
          this.handleOKEditClose();
        } else {
          toast.warning(data?.message);
          this.handleOKEditClose();
        }
      })
      .catch(() => {
        setPageLoading(false);
        toast.error(this.props.t("toastr.error"));
        this.handleOKEditClose();
      });
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t, assetClass } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let type = typeWarehouseVoucher?.Transfer

    getCountStatus(type, assetClass)
      .then(({ data }) => {
        let countStatusWaitConfirmation, countStatusConfirmation, countStatusTransferred;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.STATUS_WAREHOUSE_TRANSFER.MOI_TAO.indexOrder === item?.trangThai) {
            countStatusWaitConfirmation = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.STATUS_WAREHOUSE_TRANSFER.DANG_THUC_HIEN.indexOrder === item?.trangThai) {
            countStatusConfirmation = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.STATUS_WAREHOUSE_TRANSFER.KET_THUC.indexOrder === item?.trangThai) {
            countStatusTransferred = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState({
          countStatusWaitConfirmation,
          countStatusConfirmation,
          countStatusTransferred,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  formatDateEdit = (transfer) => {
    let listAssetDocumentId = [];

    transfer.createDate = formatDateTypeArray(transfer?.createDate);
    transfer.transferStatus = {
      id: transfer?.transferStatusId,
      indexOrder: transfer?.transferStatusIndex,
    };

    transfer.handoverDepartment = {
      id: transfer?.handoverDepartmentId,
      code: transfer?.handoverDepartmentCode,
      name: transfer?.handoverDepartmentName,
      text:
        transfer?.handoverDepartmentName +
        " - " +
        transfer?.handoverDepartmentCode,
    };

    transfer.receiverDepartment = {
      id: transfer?.receiverDepartmentId,
      code: transfer?.receiverDepartmentCode,
      name: transfer?.receiverDepartmentName,
      text:
        transfer?.receiverDepartmentName +
        " - " +
        transfer?.receiverDepartmentCode,
    };

    transfer.receiverPerson = {
      displayName: transfer?.receiverPersonName,
    };
    transfer.handoverPerson = {
      displayName: transfer?.receiverPersonName,
    };
    if (transfer?.assetVouchers && transfer?.assetVouchers?.length > 0) {
      transfer.assetVouchers.map((item) => {
        let data = {
          id: item?.assetId,
          name: item?.assetName,
          code: item?.assetCode,
          yearPutIntoUse: item?.assetYearPutIntoUse,
          managementCode: item?.assetManagementCode,
          carryingAmount: item?.assetCarryingAmount,
          dateOfReception: item?.assetDateOfReception,
          madeIn: item?.assetMadeIn,
          originalCost: item?.assetOriginalCost,
          yearOfManufacture: item?.assetYearOfManufacture,
        };

        item.usePerson = {
          id: item?.usePersonId,
          displayName: item?.usePersonDisplayName,
        };

        item.managementDepartment = {
          id: item?.assetManagementDepartmentId,
          code: item?.assetManagementDepartmentCode,
          name: item?.assetManagementDepartmentName,
        };

        item.receiverDepartment = {
          code: item?.receiveDepartmentCode,
          id: item?.receiveDepartmentId,
          name: item?.receiveDepartmentName,
        };

        item.asset = data;
        return item;
      });
    }

    // eslint-disable-next-line no-unused-expressions
    transfer?.documents?.map((item) => {
      listAssetDocumentId.push(item?.id);
    });
    transfer.listAssetDocumentId = listAssetDocumentId;
    return transfer;
  };

  handleView = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getItemById(id)
      .then(({ data }) => {
        let assetTransfer = data?.data ? data?.data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        assetTransfer.isView = true;
        assetTransfer.isStatus = false;
        this.setState({
          item: assetTransfer,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      }).finally(() => {
        setPageLoading(false);
      });
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.view === method) {
      this.handleView(rowData?.id);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusWarehouseTransfer.find(
      (item) => item.transferStatusIndex === status
    );
    return itemStatus?.name;
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleGetRowData = (rowData) => {
    this.setState({
      voucherId: rowData.id,
    }, () => {
      this.updateSubTablePageData();
    })
  }

  updateSubTablePageData = async () => {
    let {
      voucherId,
      subPage = 0,
      subRowsPerPage = 10,
    } = this.state;
    let { t } = this.props;
    let searchObject = {};
    searchObject.pageIndex = subPage + 1;
    searchObject.pageSize = subRowsPerPage;
    this.setState({ isLoadingSubTable: true });
    try {
      let res = await searchByPageAsset(voucherId, searchObject)
      const { data, code, message } = res.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          rowDataAssetVouchersTable: data?.content
            ? data?.content
            : [],
          subTotalElements: data?.totalElements ?? 0,
        })
      }
      else {
        toast.warning(message)
      }
    }
    catch (err) {
      toast.error(t("general.error"));
    }
    finally {
      this.setState({ isLoadingSubTable: false });
    }
  }

  handleEdit = async (id, method) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { t } = this.props;
    try {
      const { data } = await getItemById(id);
      const isSuccess = data?.code === appConst.CODE.SUCCESS;
      if (isSuccess) {
        let assetTransfer = data?.data ? data?.data : {};
        assetTransfer = this.formatDateEdit(assetTransfer);
        this.setState({
          item: assetTransfer,
          shouldOpenEditorDialog: true,
        });
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  convertPrintData = (state) => {
    let { rowDataAssetVouchersTable } = this.state;
    let data = {
      handoverPersonName: state?.personExportName,
      handoverDepartmentName: state?.departmentName,
      storeExport: state?.storeExport,
      storeReceipt: state?.storeReceipt,
      receiverDepartment: state?.departmentReceiptName,
      receiverPerson: state?.personReceiptName,
      issueDate: state?.inputDate,
      voucherCode: state?.code,
      assetVouchers: state?.items?.map((item) => {
        let p = {
          name: item?.productName,
          code: item?.productCode,
          serialNumber: item?.assetSerialNumber,
          model: item?.assetModel,
          managementCode: item?.assetManagementCode,
          manufacturer: item?.assetManufacturerName,
          yearPutIntoUse: item?.assetYearPutIntoUse,
          yearOfManufacture: item?.assetYearOfManufacture,
          originalCost: item?.assetOriginalCost || item?.unitPrice,
          madeIn: item?.assetManufacturerName,
        }
        return {
          asset: p,
          quantity: item?.totalQuantityExport
        }
      })
    }
    return data
  }

  handlePrint = async (item) => {
    let { t } = this.props;

    try {
      const { data } = await getItemById(item?.id);
      const isSuccess = data?.code === appConst.CODE.SUCCESS;
      if (isSuccess) {
        let assetTransfer = data?.data ? data?.data : {};
        let searchObject = {
          id: item?.id,
          pageIndex: 1,
          pageSize: 1000,
        };
        let res = await searchByPageAsset(item?.id, searchObject)
        if (appConst.CODE.SUCCESS === res?.data?.data?.code) {
          assetTransfer.items = res?.data?.data?.content
        }
        assetTransfer = this.convertPrintData({
          ...assetTransfer,
          items: res?.data?.data?.content,
        });
        this.setState({
          item: assetTransfer,
          isPrint: true,
        });
      }
    } catch (error) {
      toast.error(t("toast.error"));
    }
  }

  handlePrintDialogClose = () => {
    this.setState({ isPrint: false });
  }

  handleEndTransfer = async (id) => {
    let item = {
      id: id,
    }

    this.setState({
      item,
      shouldOpenConfirmationEndTransferDialog: true,
    });
  };

  handleConfirmEndTransfer = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      item,
      isVT,
      isCCDC,
      isTSCĐ,
    } = this.state;

    const dataState = {
      ...item,
      status: appConst.STATUS_WAREHOUSE_TRANSFER.KET_THUC.indexOrder
    }

    setPageLoading(true);
    if (isTSCĐ) {
      endInventoryTSCD(dataState)
        .then(({ data }) => {
          setPageLoading(false);
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            this.handleOKEditClose();
          }
          else {
            toast.warning(data?.message);
          }
        })
        .catch(() => {
          setPageLoading(false);
          toast.error(t("toastr.error"));
          toast.clearWaitingQueue();
        });
    }
    else if (isCCDC) {
      endInventoryCCDC(dataState)
        .then(({ data }) => {
          setPageLoading(false);
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            this.handleOKEditClose();
          }
          else {
            toast.warning(data?.message);
          }
        })
        .catch(() => {
          setPageLoading(false);
          toast.error(t("toastr.error"));
          toast.clearWaitingQueue();
        });
    }
    else if (isVT) {
      endInventoryVT(dataState)
        .then(({ data }) => {
          setPageLoading(false);
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("general.updateSuccess"));
            this.handleOKEditClose();
          }
          else {
            toast.warning(data?.message);
          }
        })
        .catch(() => {
          setPageLoading(false);
          toast.error(t("toastr.error"));
          toast.clearWaitingQueue();
        });
    }
  }

  handleOpenAdvanceSearch = () => this.setState({ advancedSearch: !this.state.advancedSearch })
  render() {
    const { t, i18n, breadcrumb, assetClass } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      rowDataAssetVouchersTable,
      isLoadingSubTable,
      isPrint,
      isCCDC,
      isTSCĐ,
      isVT,
      item,
      subPage,
      subRowsPerPage,
    } = this.state;
    let TitlePage = isTSCĐ
      ? t("AssetWarehouseTransfer.warehouseTransfer_fixedAsset")
      : isCCDC
        ? t("AssetWarehouseTransfer.warehouseTransfer_instrumentAndTools")
        : isVT
          ? t("AssetWarehouseTransfer.warehouseTransfer_supplies")
          : ""
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        maxWidth: "170px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData?.id);
              } else if (appConst.active.check === method) {
                this.handleEndTransfer(rowData.id);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.view) {
                this.handleCheckIcon(rowData, appConst.active.view);
              } else if (method === appConst.active.print) {
                this.handlePrint(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },

      {
        title: t("Store.issueDate"),
        field: "inputDate",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.inputDate ? moment(rowData.inputDate).format("DD/MM/YYYY") : "",
      },
      {
        title: t("allocation_asset.status"),
        field: "status",
        width: "220px",
        minWidth: "220px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const status = rowData?.status;
          const listStatusWarehouseTransfer = appConst.listStatusWarehouseTransfer;
          let className = "";
          if (status === listStatusWarehouseTransfer[0].transferStatusIndex) {
            className = "status status-info";
          } else if (status === listStatusWarehouseTransfer[1].transferStatusIndex) {
            className = "status status-warning";
          } else if (status === listStatusWarehouseTransfer[2].transferStatusIndex) {
            className = "status status-success";
          } else {
            className = "";
          }
          return (
            <span
              className={className}
            >
              {this.checkStatus(status)}
            </span>
          )
        }
      },
      {
        title: t("general.code"),
        field: "code",
        width: "220px",
        minWidth: "220px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetWarehouseTransfer.handoverWarehouse"),
        field: "storeExportName",
        width: "220px",
        minWidth: "220px",
      },
      {
        title: t("AssetWarehouseTransfer.receiverWarehouse"),
        field: "storeReceiptName",
        width: "220px",
        minWidth: "220px",
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: '60px',
        maxWidth: '60px',
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => subPage * subRowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: isCCDC ? t("InstrumentToolsList.code") : isVT ? t("SuggestedMaterials.code") : t("Asset.code"),
        field: "assetCode",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => rowData?.assetCode || rowData?.productCode
      },
      {
        title: t("Asset.managementCode"),
        field: "assetManagementCode",
        hidden: isVT,
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: isCCDC ? t("InstrumentToolsList.name") : isVT ? t("SuggestedMaterials.nameMaterial") : t("Asset.name"),
        field: "assetName",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
        render: (rowData) => rowData?.assetName || rowData?.productName
      },
      {
        title: t("Asset.serialNumber"),
        field: "assetSerialNumber",
        hidden: isVT,
        align: "left",
        minWidth: 120,
      },
      {
        title: t("Asset.model"),
        field: "assetModel",
        hidden: isVT,
        align: "left",
        minWidth: 120,
      },
      {
        title: t("Asset.yearIntoUseTable"),
        field: "assetYearPutIntoUse",
        hidden: isVT,
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "assetYearOfManufacture",
        hidden: isVT,
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "assetManufacturerName",
        hidden: isVT,
        align: "left",
        minWidth: 150,
        maxWidth: 200,
      },
      {
        title: t("InstrumentToolsList.originalCost"),
        field: "assetOriginalCost",
        hidden: isVT,
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.assetOriginalCost
            ? convertNumberPrice(rowData?.assetOriginalCost)
            : "",
      },
      {
        title: t("Asset.carryingAmount"),
        field: "assetCarryingAmount",
        hidden: isVT,
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.assetCarryingAmount
            ? convertNumberPrice(rowData?.assetCarryingAmount)
            : "",
      },
      {
        title: t("Asset.receiverPerson"),
        field: "usePersonDisplayName",
        hidden: isVT,
        minWidth: 230,
        align: "left",
      },
      {
        title: t("AssetWarehouseTransfer.quantity"),
        field: "note",
        hidden: isVT,
        align: "left",
        minWidth: 180,
      },
      {
        title: t("Asset.stockKeepingUnitTable"),
        field: "unitName",
        hidden: !isVT,
        align: "left",
        minWidth: 180,
      },
      {
        title: t("general.Quantity"),
        field: "totalQuantityExport",
        hidden: !isVT,
        align: "center",
        minWidth: 180,
      },
    ];

    const getTypeNameByCode = () => {
      if (assetClass === appConst.assetClass.CCDC) {
        return {
          name: "CCDC",
          code: "Mã CCDC",
          nameInTable: "Tên CCDC"
        }
      }
      if (assetClass === appConst.assetClass.TSCD) {
        return {
          name: "tài sản",
          code: "Mã tài sản",
          nameInTable: "Tên tài sản"
        }
      }
      if (assetClass === appConst.assetClass.VT) {
        return {
          name: "vật tư",
          code: "Mã vật tư",
          nameInTable: "Tên vật tư"
        }
      }
      return {
        name: "",
        code: "",
      }
    }

    const currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
    const organizationName = currentUser?.org?.name;
    let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
    let addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");

    let now = new Date(item?.issueDate);
    const day = now?.getDate() < 10 ? `0${now.getDate()}` : now.getDate();
    const month = now?.getMonth() + 1 < 10 ? `0${now.getMonth() + 1}` : now.getMonth() + 1;
    const year = now?.getFullYear();

    let dataView = {
      organizationName,
      titleName,
      addressOfEnterprise,
      day: item?.issueDate ? day : ".....",
      month: item?.issueDate ? month : ".....",
      year: item?.issueDate ? year : ".....",
      typeName: getTypeNameByCode()?.name,
      tableAssetCode: getTypeNameByCode()?.code,
      tableAssetName: getTypeNameByCode()?.nameInTable,
      dataPrint: {
        ...item,
        arrOriginalCost: convertNumberPrice(item?.assetVouchers?.reduce((total, item) => total + item?.asset?.originalCost, 0)) || 0,
        assetVouchers: item?.assetVouchers?.map((i, x) => {
          return {
            ...i,
            index: x + 1,
            madeIn: i?.asset?.madeIn || i?.asset?.assetManufacturerName,
            yearOfManufacture: i?.asset?.yearOfManufacture || i?.asset?.assetYearOfManufacture,
            yearPutIntoUse: i?.asset?.yearPutIntoUse || i?.assetYearPutIntoUser,
            quantity: i?.quantity || 1,
            originalCost: convertNumberPriceRoundUp(i?.asset?.originalCost || 0)
          }
        })
      }
    }

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t(breadcrumb), },
              {
                name: t("Dashboard.subcategory.warehouseTransfer"),
                path: "material-management/warehouse-tranfers",
              },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("InstrumentToolsTransfer.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>
                    {t("AssetWarehouseTransfer.status.new")}
                  </span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusWaitConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("AssetWarehouseTransfer.status.processing")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("AssetWarehouseTransfer.status.end")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusTransferred || 0}
                  </div>
                </div>
              }
            // label={t("InstrumentToolsTransfer.tabWarehouseTransferred")}
            />
          </Tabs>
        </AppBar>
        {this.state?.shouldOpenConfirmationReceiveDialog && (
          <ConfirmationDialog
            title={t("InstrumentToolsTransfer.titleConfirmationReceive")}
            open={this.state?.shouldOpenConfirmationReceiveDialog}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationReceive}
            text={t("InstrumentToolsTransfer.confirmationReceive")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}

        <TabPanel
          value={tabValue}
          index={appConst.tabWarehouseTransfer.tabAll}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            assetClass={this.props?.assetClass}
            handlePrint={this.handlePrint}
            handleConfirmEndTransfer={this.handleConfirmEndTransfer}
            handleSetDate={this.handleSetDate}
            handleSetListStore={this.handleSetListStore}
            handleSelectStore={this.handleSelectStore}
            handleSelectStatus={this.selectAssetTransferStatus}
            handleSetDataSelect={this.handleSetDataSelect}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabWarehouseTransfer.tabNew}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            assetClass={this.props?.assetClass}
            handlePrint={this.handlePrint}
            handleConfirmEndTransfer={this.handleConfirmEndTransfer}
            handleSetDate={this.handleSetDate}
            handleSetListStore={this.handleSetListStore}
            handleSelectStore={this.handleSelectStore}
            handleSelectStatus={this.selectAssetTransferStatus}
            handleSetDataSelect={this.handleSetDataSelect}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabWarehouseTransfer.tabProcessing}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            assetClass={this.props?.assetClass}
            handlePrint={this.handlePrint}
            handleConfirmEndTransfer={this.handleConfirmEndTransfer}
            handleSetDate={this.handleSetDate}
            handleSetListStore={this.handleSetListStore}
            handleSelectStore={this.handleSelectStore}
            handleSelectStatus={this.selectAssetTransferStatus}
            handleSetDataSelect={this.handleSetDataSelect}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabWarehouseTransfer.tabEnd}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={this.handleGetRowData}
            assetClass={this.props?.assetClass}
            handlePrint={this.handlePrint}
            handleConfirmEndTransfer={this.handleConfirmEndTransfer}
            handleSetDate={this.handleSetDate}
            handleSetListStore={this.handleSetListStore}
            handleSelectStore={this.handleSelectStore}
            handleSelectStatus={this.selectAssetTransferStatus}
            handleSetDataSelect={this.handleSetDataSelect}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>

        <div className="position-relative">
          {
            isLoadingSubTable && (
              <div className="sub-table-loading w-100 h-100 flex flex-middle flex-center">
                <CircularProgress />
              </div>
            )
          }
          <MaterialTable
            data={rowDataAssetVouchersTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '350px',
              minBodyHeight: '260px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div className="w-100">
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
        <TablePagination
          align="left"
          className="px-16"
          rowsPerPageOptions={appConst.rowsPerPageOptions.table}
          labelRowsPerPage={t("general.rows_per_page")}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
          }
          component="div"
          count={this.state.subTotalElements}
          rowsPerPage={this.state.subRowsPerPage}
          page={this.state.subPage}
          backIconButtonProps={{ "aria-label": "Previous Page", }}
          nextIconButtonProps={{ "aria-label": "Next Page", }}
          onPageChange={this.handleChangeSubPage}
          onRowsPerPageChange={this.setSubRowsPerPage}
        />

        {isPrint && (
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={this.handlePrintDialogClose}
            open={isPrint}
            item={dataView}
            title={t('TransferToAnotherUnit.transferSlip')}
            urls={[
              {
                // tabLabel: "Mẫu BBGNTB",
                url: '/assets/form-print/receiptOfDeliveryFormTemplate.txt',
                config: {
                  layout: "portrait",
                  margin: "horizontal",
                },
              },
            ]}
          />
        )}

        {/* {isPrint && (
          <ReceiptOfDeliveryForm
            t={t}
            assetClass={this.props.assetClass}
            item={this.state.item}
            open={isPrint}
            handleClose={this.handlePrintDialogClose}
            isStore
          />
        )} */}
      </div>
    );
  }
}
WarehouseTransferTable.contextType = AppContext;
export default WarehouseTransferTable;
