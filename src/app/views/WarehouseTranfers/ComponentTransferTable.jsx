import {
    Button,
    Card,
    Collapse,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    Link,
    TablePagination
} from "@material-ui/core";
import React, { useState } from "react";
import WarehouseTransferDialog from "./WarehouseTransferDialog";
import MaterialTable, { MTableToolbar } from "material-table";
import { STATUS_STORE, appConst } from "app/appConst";
import { filterOptions, getTheHighestRole } from "app/appFunction";
import { ConfirmationDialog } from "egret";
import { searchByPage as storesSearchByPage } from "../Store/StoreService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ValidatedDatePicker from "../Component/ValidatePicker/ValidatePicker"
import SearchIcon from "@material-ui/icons/Search";
import CardContent from "@material-ui/core/CardContent";
import {getListWarehouseByDepartmentId} from "../AssetTransfer/AssetTransferService";

function ComponentTransferTable(props) {
    let { t, i18n, getRowData } = props;
    let {
        rowsPerPage,
        page,
        totalElements,
        itemList,
        item,
        shouldOpenEditorDialog,
        hasCreatePermission,
        shouldOpenConfirmationDialog,
        shouldOpenConfirmationEndTransferDialog,
        toDate,
        fromDate,
        listStore,
        storeExport,
        storeReceipt,
        statusFilter,
        voucherId,
        advancedSearch,
    } = props?.item;
    const { isRoleUser, departmentUser } = getTheHighestRole()
    const searchObjectStore = {
        isActive: STATUS_STORE.HOAT_DONG.code,
        pageIndex: 1,
        pageSize: 1000000,
        departmentId: departmentUser?.id,
        isAssetManagement: true,
    };

    return (
        <>
            <Grid container spacing={2} className="mt-10">
                <Grid item md={6} xs={12}>
                    {(hasCreatePermission && !isRoleUser) && (
                        <Button
                            className="mb-12 mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleButtonAdd({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("general.add")}
                        </Button>
                    )}
                    <Button
                        className="mb-12 mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.exportToExcel}
                    >
                        {t("general.exportToExcel")}
                    </Button>
                    <Button
                        className="mb-12 mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>

                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                    {/* Ẩn đi do đợi api */}
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("AssetTransfer.filterCode")}
                            id="search_box"
                            startAdornment={
                                <InputAdornment>
                                    <Link>
                                        {" "}
                                        <SearchIcon
                                            onClick={() => props.search()}
                                            className="searchTable"
                                        />
                                    </Link>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>


                <Grid item xs={12} className="pt-0 pb-0">
                    <ValidatorForm onSubmit={() => { }}>
                        <Collapse in={advancedSearch} className="pt-0 pb-0">
                            <Card elevation={2}>
                                <CardContent>
                                    <Grid container spacing={1}>
                                        <Grid item md={2} sm={6} xs={12}>
                                            <ValidatedDatePicker
                                                fullWidth
                                                className="mb-16 mr-16 align-bottom"
                                                margin="none"
                                                id="mui-pickers-date"
                                                label={t("AssetWarehouseTransfer.fromDate")}
                                                inputVariant="standard"
                                                type="text"
                                                autoOk={false}
                                                format="dd/MM/yyyy"
                                                name={"fromDate"}
                                                value={fromDate || null}
                                                maxDate={toDate}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                minDateMessage={t("general.minDateMessage")}
                                                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                onChange={(date) => props?.handleSetDate(date, "fromDate")}
                                            />
                                        </Grid>
                                        <Grid item md={2} sm={6} xs={12}>
                                            <ValidatedDatePicker
                                                fullWidth
                                                margin="none"
                                                id="mui-pickers-date"
                                                label={t("AssetWarehouseTransfer.toDate")}
                                                inputVariant="standard"
                                                type="text"
                                                autoOk={false}
                                                format="dd/MM/yyyy"
                                                name={"toDate"}
                                                value={toDate || null}
                                                minDate={fromDate}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                maxDateMessage={t("general.maxDateMessage")}
                                                onChange={(date) => props?.handleSetDate(date, "toDate")}
                                            />
                                        </Grid>
                                        <Grid item md={3} xs={12}>
                                            <AsynchronousAutocompleteSub
                                                isFocus={true}
                                                label={t("AssetWarehouseTransfer.storeExport")}
                                                searchFunction={getListWarehouseByDepartmentId}
                                                searchObject={searchObjectStore}
                                                listData={listStore}
                                                setListData={(listStore) => props.handleSetListStore(listStore)}
                                                displayLable={"name"}
                                                value={storeExport || null}
                                                onSelect={(value) => props.handleSelectStore(value, "storeExport")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        <Grid item md={3} xs={12}>
                                            <AsynchronousAutocompleteSub
                                                isFocus={true}
                                                label={t("AssetWarehouseTransfer.storeImport")}
                                                searchFunction={getListWarehouseByDepartmentId}
                                                searchObject={searchObjectStore}
                                                listData={listStore}
                                                setListData={(listStore) => props.handleSetListStore(listStore)}
                                                displayLable={"name"}
                                                value={storeReceipt || null}
                                                onSelect={(value) => props.handleSelectStore(value, "storeReceipt")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        <Grid item md={2} xs={12}>
                                            <AsynchronousAutocompleteSub
                                                searchFunction={() => { }}
                                                label={t("AssetWarehouseTransfer.statusFilter")}
                                                listData={appConst.listStatusInventoryStore}
                                                setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
                                                value={statusFilter ? statusFilter : null}
                                                displayLable={'name'}
                                                onSelect={props.handleSelectStatus}
                                                filterOptions={filterOptions}
                                                noOptionsText={t("general.noOption")}
                                                validators={["required"]}
                                                errorMessages={[t('general.required')]}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Collapse>
                    </ValidatorForm>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {shouldOpenEditorDialog && (
                            <WarehouseTransferDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                assetClass={props?.assetClass}
                                // subAssetsVouchers={rowDataAssetVouchersTable}
                                handlePrint={props.handlePrint}
                            />
                        )}
                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props?.handleDialogClose}
                                onYesClick={props?.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {shouldOpenConfirmationEndTransferDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationEndTransferDialog}
                                onConfirmDialogClose={props?.handleDialogClose}
                                onYesClick={props?.handleConfirmEndTransfer}
                                text={t("AssetWarehouseTransfer.confirmEndTransfer")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                    </div>
                    <MaterialTable
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        // parentChildData={(row, rows) => {
                        //     var list = rows.find((a) => a.id === row.parentId);
                        //     return list;
                        // }}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: voucherId === rowData?.id
                                    ? "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "490px",
                            minBodyHeight: "260px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            this.data = rows;
                        }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        labelRowsPerPage={t("general.rows_per_page")}
                        labelDisplayedRows={({ from, to, count }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                            }`
                        }
                        component="div"
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                            "aria-label": "Previous Page",
                        }}
                        nextIconButtonProps={{
                            "aria-label": "Next Page",
                        }}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default ComponentTransferTable;
