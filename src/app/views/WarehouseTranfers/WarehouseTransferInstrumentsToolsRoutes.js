import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const WarehouseTransferInstrumentsToolsTable = EgretLoadable({
  loader: () => import("./WarehouseTransferInstrumentsToolsTable")
});
const ViewComponent = withTranslation()(WarehouseTransferInstrumentsToolsTable);

const WarehouseTransferInstrumentsToolsRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "instruments-and-tools/warehouse-tranfers",
    exact: true,
    component: ViewComponent
  }
];

export default WarehouseTransferInstrumentsToolsRoutes;