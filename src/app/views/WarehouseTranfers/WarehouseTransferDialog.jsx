import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  getAssetDocumentById,
  getNewCodeDocument,
} from "../Asset/AssetService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkInvalidDate,
  checkMinDate,
  getTheHighestRole,
  handleThrowResponseMessage,
  isCheckLenght,
  isSuccessfulResponse,
  validateAndFormatDateArr,
} from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import localStorageService from "app/services/localStorageService";
import WarehouseTransferScrollableTabsButtonForce from "./WarehouseTransferScrollableTabsButtonForce";
import {
  createInventoryVT,
  updateInventoryVT,
  createInventoryCCDC,
  createInventoryTSCD,
  updateInventoryCCDC,
  updateInventoryTSCD,
} from "./WarehouseTranfersService";
import moment from "moment";
import { getListRemainingQuantity } from "../Product/ProductService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const theHighestRole = getTheHighestRole();
const isRoleAssetUser = theHighestRole.isRoleAssetUser;
const isRoleAssetManager = theHighestRole.isRoleAssetManager;
const departmentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER);

class WarehouseReceivingDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Transfer,
    personExport: null,
    department: null,
    departmentReceipt: null,
    personReceipt: null,
    inputDate: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    departmentName: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: [2], // cấp phát
    personExportClone: false,
    assetDocumentList: [],
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER,
    shouldOpenPopupAssetFile: false,
    transferStatus: null,
    loading: false,
    note: null,
    storeExport: null,
    storeReceipt: null,
    personReceiptId: null,
    personReceiptName: null,
    personExportId: null,
    personExportName: null,
    departmentReceiptId: null,
    departmentReceiptName: null,
    typeStore: null
  };
  voucherType = ConstantList.VOUCHER_TYPE.Transfer; //Điều chuyển

  convertDto = (state) => {
    let assetVouchersConvert = []
    if (state?.assetVouchers && state?.assetVouchers?.length > 0) {
      state.assetVouchers.map(asset => {
        let item = {
          productId: asset?.asset?.productId || asset?.productId,
          productName: asset?.asset?.productName || asset?.productName,
          serialNumber: asset?.asset?.serialNumber || asset?.serialNumber,
          unitId: asset?.asset?.unitId || asset?.unitId,
          unitName: asset?.asset?.unitName || asset?.unitName,
          unitPrice: asset?.asset?.unitPrice || asset?.unitPrice,
          lotNumber: asset?.asset?.lotNumber || asset?.lotNumber,
          assetId: asset?.assetId || asset?.assetId,
          totalQuantityExport: +asset?.asset?.totalQuantityExport || +asset?.totalQuantityExport,
          assetItemId: asset?.asset?.assetItemId || asset?.assetItemId,
          receiptDate: asset?.inputDate || asset?.receiptDate,
          expiryDate: asset?.expiryDate
        }
        assetVouchersConvert.push(item)
      })
    }
    let data = {
      id: state?.id,
      inputDate: state?.inputDate,
      status: state?.transferStatusIndex || state?.status,
      type: state?.type,
      assetClass: this.props.assetClass,
      storeExportId: state?.storeExport.id || state?.storeExportId,
      storeExportName: state?.storeExport.name || state?.storeExportId,
      storeReceiptId: state?.storeReceipt.id || state?.storeReceiptId,
      storeReceiptName: state?.storeReceipt.name || state?.storeReceiptName,
      items: assetVouchersConvert,
      personReceiptId: state?.personReceipt?.personId || state?.personReceiptId,
      personReceiptName: state?.personReceipt?.personDisplayName || state?.personReceiptName,
      personExportId: state?.personExport?.personId || state?.personExportId,
      personExportName: state?.personExport?.personDisplayName || state?.personExportName,
      departmentId: state?.departmentId,
      departmentReceiptId: state?.departmentReceiptId,
    }
    return data;
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable.listInputName.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowData.tableData?.id) {
        assetVoucher.totalQuantityExport = valueText.target.value;
        assetVoucher.assetOriginalCost = ((rowData?.unitPrice || rowData?.asset?.unitPrice) * +valueText.target.value) || 0
      }
    });
    this.setState({ assetVouchers: assetVouchers, personExportClone: true });
  };

  selectUsePerson = (item, rowdata) => {
    let { assetVouchers } = this.state;
    assetVouchers.map(async (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.usePerson = item ? item : null;
        assetVoucher.usePersonId = item?.id;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, personExportClone: true }
    );
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  validateForm = () => {
    let {
      assetVouchers,
      department,
      personExport,
      transferStatus,
      departmentReceipt,
      inputDate,
      storeExport,
      storeReceipt
    } = this.state;
    if (checkInvalidDate(inputDate)) {
      toast.warning('Ngày chứng từ không tồn tại (Thông tin phiếu).');
      return false;
    }
    if (checkMinDate(inputDate, "01/01/1900")) {
      toast.warning(`Ngày chứng từ không được nhỏ hơn ngày "01/01/1900"(Thông tin phiếu).`);
      return false;
    }
    if (!department) {
      toast.warning("Vui lòng chọn phòng ban bàn giao(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (!transferStatus) {
      toast.warning("Vui lòng chọn trạng thái điều chuyển(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    // if (!personExport) {
    //   toast.warning("Vui lòng nhập người bàn giao(Thông tin phiếu).");
    //   toast.clearWaitingQueue();
    //   this.setState({ loading: false });
    //   return false;
    // }
    // if (!departmentReceipt) {
    //   toast.warning("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).");
    //   toast.clearWaitingQueue();
    //   this.setState({ loading: false });
    //   return false;
    // }
    if (storeExport?.id === storeReceipt?.id) {
      toast.warning("Kho bàn giao không được trùng với kho tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (storeExport?.type !== storeReceipt?.type) {
      toast.warning("Kho bàn giao khác loại với kho tiếp nhận(Thông tin phiếu).");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn tài sản");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }
    if (this.validateAssetVouchers()) return;
    return true;
  }
  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning("Ghi chú không nhập quá 255 ký tự.");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    if (e.target.id !== "assetTransferDialog") return;
    if (!this.validateForm()) return;
    let dataState = this.convertDto(this.state);

    let apiFunc = {
      [appConst.assetClass.TSCD]: !dataState?.id ? createInventoryTSCD : updateInventoryTSCD,
      [appConst.assetClass.CCDC]: !dataState?.id ? createInventoryCCDC : updateInventoryCCDC,
      [appConst.assetClass.VT]: !dataState?.id ? createInventoryVT : updateInventoryVT,
    }

    try {
      setPageLoading(true);
      let res = await apiFunc[this.props.assetClass]?.(dataState);
      let { code } = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(
          dataState?.id
            ? t("AssetWarehouseTransfer.noti_create_success")
            : t("AssetWarehouseTransfer.noti_edit_success")
        );
        this.props.handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date ? moment(date).format("YYYY-MM-DDTHH:mm:ss") : null,
      assetVouchers: [],
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  handleSelectAsset = (items) => {
    // eslint-disable-next-line no-unused-expressions
    items?.forEach((element) => {
      if (element?.asset) {
        element.asset.useDepartment = this.state?.departmentReceipt
        element.assetId = element?.asset?.id || element?.assetId;
      }
      element.receiptDate = element?.inputDate
      element.expiryDate = element?.expiryDate
      element.remainQuantity = element?.remainQuantity
      element.totalQuantityExport = element?.totalQuantityExport
        ? element?.totalQuantityExport
        : element?.asset?.quantity
          ? element?.asset?.quantity
          : element?.remainQuantity
    });
    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning('Chưa có tài sản nào được chọn');
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x?.asset?.id === id || x?.assetId === id || x?.productId === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  setAssetVouchers = (assetVouchers) => {
    this.setState({
      assetVouchers: assetVouchers
    })
  }

  componentWillMount() {
    let { item } = this.props;
    const { currentUser } = getTheHighestRole();
    if (item.id) {
      this.setState({
        ...this.props.item,
        personExportClone: true,
        transferStatus: {
          transferStatus: {},
          transferStatusIndex: item?.status
        },
        department: {
          id: item?.departmentId,
          text: item?.departmentName,
          name: item?.departmentName
        },
        personExport: {
          personId: item?.personExportId,
          personDisplayName: item?.personExportName,
        },
        departmentReceipt: {
          id: item?.departmentReceiptId,
          text: item?.departmentReceiptName,
        },
        personReceipt: {
          personId: item?.personReceiptId,
          personDisplayName: item?.personReceiptName,
        },
        storeExport: {
          id: item?.storeExportId,
          name: item?.storeExportName,
        },
        storeReceipt: {
          id: item?.storeReceiptId,
          name: item?.storeReceiptName,
        },
        assetVouchers: item.items?.map(item => {
          return {
            ...item,
            expiryDate: validateAndFormatDateArr(item?.expiryDate, true)
          }
        }),
      }, () => {
        this.handleGetSuppliesRemainQuantity();
      });
    } else {
      let statusCreate = appConst?.STATUS_WAREHOUSE_TRANSFER.MOI_TAO;
      this.setState({
        ...this.props.item,
        personExport: isRoleAssetManager
          ? {
            personId: currentUser?.person?.id,
            personDisplayName: currentUser?.displayName,
          }
          : null,
        department: departmentUser,
        departmentId: departmentUser?.id,
        transferStatus: statusCreate,
        transferStatusIndex: statusCreate?.indexOrder
      });
    }
  }
  componentDidMount() { }

  openPopupSelectAsset = () => {
    let { voucherType, department } = this.state;
    if (department) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
          departmentId: department?.id,
        },
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.info("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleReceiverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  selectReceiverDepartment = (item) => {
    this.setState({
      departmentReceipt: item,
      departmentReceiptId: item?.id,
    })
  };

  handleSelectReceiverDepartment = (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );

    let departmentReceipt = {
      id: item?.id,
      name: item?.text,
      text: item?.text,
    };

    this.setState(
      {
        departmentReceipt: item ? departmentReceipt : null,
        departmentReceiptId: item?.id,
        assetVouchers: assetVouchers,
        listReceiverPerson: [],
        storeReceipt: null,
        personReceipt: null,
        personReceiptId: null,
        isCheckReceiverDP: item ? false : true,
      }
    );
  };

  handleSelectReceiverWarehouse = (item) => {
    let storeReceipt = {
      id: item?.id,
      name: item?.name,
    };

    this.setState(
      {
        storeReceipt: item ? storeReceipt : null,
        storeReceiptId: item?.id,
        storeReceiptName: item?.name,
        typeStore: item?.type

      },
    );
  };

  handleSelectHandoverWarehouse = (item) => {
    let storeExport = {
      id: item?.id,
      name: item?.name,
    };

    if (item?.type !== this.state.typeStore) {
      this.setState(
        {
          storeExport: item ? storeExport : null,
          storeExportId: item?.id,
          storeExportName: item?.name,
          assetVouchers: [],
          typeStore: item?.type,
          storeReceipt: null,
          storeReceiptId: null,
          storeReceiptName: null
        },
      );
      return
    }

    this.setState(
      {
        storeExport: item ? storeExport : null,
        storeExportId: item?.id,
        storeExportName: item?.name,
        assetVouchers: [],
      },
    );
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({
      personReceipt: item ? item : null,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let { isRoleAssetManager } = this.state;
    let department = {
      id: item?.id,
      name: item?.text,
      text: item?.text,
    };

    this.setState({
      department: item ? department : null,
      departmentId: item?.id,
      departmentName: item?.name,
      assetVouchers: [],
      listHandoverPerson: [],
      storeExport: null,
      storeReceipt: null,
    }, () => {
      if (!isRoleAssetManager) {
        this.setState({
          personExport: null,
          personExportId: null,
        })
      }
    });
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      personExport: item ? item : null,
    });
  };

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleStatus = (dataStatus) => {
    this.props.handleOKEditClose();
    // if(typeof dataStatus !== "number"){
    //   if (dataStatus?.target?.id !== "assetTransferDialog") return;
    // }

    // let { setPageLoading } = this.context;
    // setPageLoading(true);
    // let { t } = this.props;
    // let status = this.state.isStatus ? this.state?.transferStatus?.indexOrder : dataStatus;
    // let inputDate = formatDateTimeDto(this.state.inputDate, 23, 59, 59);
    // let dataState = this.convertDto(this.state)
    // let dataTranfer = {
    //   dataState: dataState,
    //   id: this.state?.id,
    //   note: this.state?.note,
    //   status: status,
    //   inputDate: inputDate,
    // }
    // updateStatus(dataTranfer)
    //   .then(({ data }) => {
    //     if (appConst.CODE.SUCCESS === data?.code) {
    //       toast.info(t("InstrumentToolsTransfer.notification.confirmSucces"));
    //       this.props.handleOKEditClose();
    //     }
    //     else {
    //       toast.warning(data?.message);
    //     }
    //     setPageLoading(false);
    //   })
    //   .catch(() => {
    //     toast.info(t("InstrumentToolsTransfer.notification.faliConfirm"));
    //     setPageLoading(false);
    //   });
  }

  selectAssetTransferStatus = (item) => {
    if (item?.indexOrder !== appConst?.listStatusInventoryStore[2].indexOrder && !this.state.inputDate) {
      this.setState({
        inputDate: new Date(),
      });
    }
    this.setState({
      status: item?.indexOrder,
      transferStatus: item,
      transferStatusIndex: item?.indexOrder
    });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleGetSuppliesRemainQuantity = async () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { assetVouchers = [], storeExport } = this.state;
    let { t } = this.props;
    let searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 100000;
    searchObject.storeId = storeExport?.id;
    searchObject.productIds = assetVouchers?.map(item => item.productId).join(',');

    if (assetVouchers?.length === 0) return; // suppliesItems rỗng thì k cần gọi api

    try {
      let res = await getListRemainingQuantity(searchObject)
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        assetVouchers?.map(x => {
          ;
          let existItem = data?.find(
            item => item?.productId === x?.productId
              && item?.inputDate === x?.receiptDate
              && item?.lotNumber === x?.lotNumber
              && item?.unitPrice === x?.unitPrice
              && item?.expiryDate === x?.expiryDate
          )
          if (existItem) {
            x.remainQuantity = existItem.remainQuantity > 0
              ? existItem.remainQuantity
              : 0;
          }
          return x
        });
        this.setState({
          assetVouchers,
        });
      }
      else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("InventoryDeliveryVoucher.getRemainQuantityError"));
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    let { open, t, i18n } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let personReceiptSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state?.departmentReceipt
        ? this.state.departmentReceipt?.id
        : null,
    };
    let personExportSearchObject = {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.department
        ? this.state.department?.id
        : null,
    };
    let departmentReceiptSearchObject = {
      pageIndex: 1,
      pageSize: 100,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state?.departmentReceipt
        ? this.state.departmentReceipt?.id
        : null,
    };
    let {
      isView,
      shouldOpenNotificationPopup,
      loading,
      isStatus,
    } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={isStatus ? this.handleStatus : this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("AssetWarehouseTransfer.dialog")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <WarehouseTransferScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAssetTransferStatus={
                this.selectAssetTransferStatus
              }
              handleHandoverDepartmentPopupOpen={
                this.handleHandoverDepartmentPopupOpen
              }
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleSelectHandoverPerson={
                this.handleSelectHandoverPerson
              }
              handleHandoverDepartmentPopupClose={
                this.handleHandoverDepartmentPopupClose
              }
              personExportSearchObject={personExportSearchObject}
              personReceiptSearchObject={personReceiptSearchObject}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupOpen={
                this.handleReceiverDepartmentPopupOpen
              }
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleSelectReceiverPerson={
                this.handleSelectReceiverPerson
              }
              handleReceiverDepartmentPopupClose={
                this.handleReceiverDepartmentPopupClose
              }
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              selectUsePerson={this.selectUsePerson}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              getAssetDocument={this.getAssetDocument}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              selectReceiverDepartment={this.selectReceiverDepartment}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              departmentReceiptSearchObject={departmentReceiptSearchObject}
              isRoleAssetUser={isRoleAssetUser}
              isRoleAssetManager={isRoleAssetManager}
              handleSelectReceiverWarehouse={this.handleSelectReceiverWarehouse}
              handleSelectHandoverWarehouse={this.handleSelectHandoverWarehouse}
              assetClass={this.props?.assetClass}
              setAssetVouchers={this.setAssetVouchers}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <>
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={() => this.props.handleClose()}
                >
                  {t("general.cancel")}
                </Button>
                {this.state.id &&
                  <Button
                    variant="contained"
                    className="mr-12"
                    color="primary"
                    disabled={!this.state.id}
                    onClick={() => this.props?.handlePrint(this.state)}
                  >
                    {t("general.print")}
                  </Button>}
                {!isView && <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>}
              </>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
WarehouseReceivingDialog.contextType = AppContext;
export default WarehouseReceivingDialog;
