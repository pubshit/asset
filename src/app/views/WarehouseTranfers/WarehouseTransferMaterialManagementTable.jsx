import React from "react";
import WarehouseTransferTable from "./WarehouseTranfersTable";
import { appConst } from "app/appConst";

function WarehouseTransferMaterialManagementTable(props) {
    return (<>
        <WarehouseTransferTable 
            {...props} 
            breadcrumb="Dashboard.materialManagement"
            assetClass={appConst.assetClass.VT}
        />
    </>);
}

export default WarehouseTransferMaterialManagementTable;