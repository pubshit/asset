import React from "react";
import WarehouseTransferTable from "./WarehouseTranfersTable";
import { appConst } from "app/appConst";

function WarehouseTransferInstrumentsToolsTable(props) {
    return (<>
        <WarehouseTransferTable {...props} breadcrumb={ "Dashboard.toolsManagement"} assetClass={appConst.assetClass.CCDC}/>
    </>);
}

export default WarehouseTransferInstrumentsToolsTable;