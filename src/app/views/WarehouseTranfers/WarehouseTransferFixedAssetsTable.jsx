import React from "react";
import WarehouseTransferTable from "./WarehouseTranfersTable";
import { appConst } from "app/appConst";

function WarehouseTranferFixedAssetsTable(props) {
    return (<>
        <WarehouseTransferTable {...props} breadcrumb={ "Dashboard.assetManagement"} assetClass={appConst.assetClass.TSCD} />
    </>);
}

export default WarehouseTranferFixedAssetsTable;