import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const WarehouseTransferFixedAssetsTable = EgretLoadable({
  loader: () => import("./WarehouseTransferFixedAssetsTable")
});
const ViewComponent = withTranslation()(WarehouseTransferFixedAssetsTable);

const WarehouseTransferFixedAssetsRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/warehouse-tranfers",
    exact: true,
    component: ViewComponent
  }
];

export default WarehouseTransferFixedAssetsRoutes;