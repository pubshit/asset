import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/asset_transfer_status";
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_TRANSFER =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/inventory";
const API_PATH_USER =
  ConstantList.API_ENPOINT + "/api/v1/user-departments/page";
const API_PATH_INVENTORY =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/inventory";

//api mới
export const searchByPage = (searchObject) => {
  let url = API_PATH_TRANSFER + `/search-by-page`;
  // let config = {
  //   params: {
  //     pageIndex: searchObject?.pageIndex,
  //     pageSize: searchObject?.pageSize,
  //     keyword: searchObject?.keyword,
  //     statusIndex: searchObject?.statusIndex
  //   }
  // };
  return axios.post(url, searchObject);
};

export const getItemById = (id) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.get(url);
};
export const createTransfer = (data) => {
  return axios.post(API_PATH_TRANSFER, data);
};
export const updateTransfer = (id, data) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.put(url, data);
};
export const deleteItem = (id) => {
  let url = API_PATH_TRANSFER + "/" + id;
  return axios.delete(url);
};
export const getCountStatus = (type, assetClass) => {
  let url = API_PATH_TRANSFER + "/count-by-status";
  return axios.get(url, { params: { type, assetClass } });
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};
//update trạng thái  sau sửa thành của tài sản điều chuyển
export const updateStatus = (data) => {
  let config = {
    params: {
      note: data?.note,
      issueDate: data?.issueDate,
    },
  };
  let url = API_PATH_TRANSFER + "/" + data?.id + "/status/" + data?.status;
  return axios.put(url, data?.dataState, config);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/assetTransferToExcel",
    data: searchObject,
    responseType: "blob",
  });
};

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};

export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const searchReceiverDepartment = (searchObject) => {
  var url = API_PATH_user_department + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getStatus = () => {
  return axios.get(API_PATH);
};

//get phong ban bàn giao
export const getListManagementDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", searchObject);
};
//get phòng ban theo id người
export const findDepartmentById = (id) => {
  let config = { params: { userId: id } };
  let url = API_PATH_department + "/main-department";
  return axios.get(url, config);
};

// get người theo id phòng
export const getListUserByDepartmentId = (searchObject) => {
  let config = { params: searchObject };
  let url = API_PATH_USER;
  return axios.get(url, config);
};

export const createInventoryTSCD = (data) => {
  //Thêm mới Phiếu TSCĐ
  return axios.post(API_PATH_INVENTORY + "/fixed-asset", data);
};

export const updateInventoryTSCD = (data) => {
  //Sửa mới TSCĐ
  return axios.put(API_PATH_INVENTORY + "/fixed-asset", data);
};

export const endInventoryTSCD = (params) => {
  //Kết thúc phiếu TSCĐ
  return axios.patch(
    API_PATH_INVENTORY + `/fixed-asset/${params?.id}?status=${params?.status}`
  );
};

export const createInventoryCCDC = (data) => {
  //Thêm mới Phiếu CCDC
  return axios.post(API_PATH_INVENTORY + "/iat", data);
};

export const updateInventoryCCDC = (data) => {
  //Sửa Phiếu CCDC
  return axios.put(API_PATH_INVENTORY + "/iat", data);
};

export const endInventoryCCDC = (params) => {
  //Kết thúc phiếu CCDC
  return axios.patch(
    API_PATH_INVENTORY + `/iat-asset/${params?.id}?status=${params?.status}`
  );
};

export const createInventoryVT = (data) => {
  //Thêm mới Phiếu VT
  return axios.post(API_PATH_INVENTORY + "/supplies", data);
};

export const updateInventoryVT = (data) => {
  //Sửa Phiếu VT
  return axios.put(API_PATH_INVENTORY + "/supplies", data);
};

export const endInventoryVT = (params) => {
  //Kết thúc phiếu VT
  return axios.patch(
    API_PATH_INVENTORY + `/supplies/${params?.id}?status=${params?.status}`
  );
};

export const exportToExcelNew = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/fixed-asset-iat-warehouse-transfer",
    data: searchObject,
    responseType: "blob",
  });
};
export const exportToExcelMaterials = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/chuyen-kho-vat-tu",
    data: searchObject,
    responseType: "blob",
  });
};
