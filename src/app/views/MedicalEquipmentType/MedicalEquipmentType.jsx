import {
  Button,
  FormControl,
  Grid,
  Icon,
  IconButton, Input, InputAdornment, TablePagination, Tooltip, withStyles
} from '@material-ui/core';
import { Breadcrumb, ConfirmationDialog } from 'egret';
import React, { useContext, useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable from 'material-table';
import MedicalEquipmentTypeDialog from './MedicalEquipmentTypeDialog';
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import { deleteItem, getById, searchByPage } from './MedicalEquipmentTypeService';
import AppContext from 'app/appContext';
import { appConst } from "../../appConst"
import { defaultPaginationProps } from 'app/appFunction';



toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: "-15px",
    left: "-30px",
    width: "80px",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

function MedicalEquipmentType() {
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const TitlePage = t("MedicalEquipmentType.title");

  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] = useState(false)
  const [shouldOpenConfirmationDeleteAllDialog, setShouldOpenConfirmationDeleteAllDialog] = useState(false)
  const [shouldOpenEditorDialog, setShouldOpenEditorDialog] = useState(false)

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalElements, setTotalElements] = useState(0);
  const [keyword, setKeyword] = useState("");

  const [data, setData] = useState([]);
  const [itemMedicalEquipment, setItemMedicalEquipment] = useState({});
  const [itemList, setItemList] = useState([]);

  const updatePageData = () => {
    let searchObject = {};
    searchObject.keyword = keyword?.trim();
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchByPage(searchObject)
      .then(({ data }) => {
        let itemList = [...data.data.content];
        let list = []
        itemList.forEach(item => {
          let items = getListItemChild(item);
          list.push(...items);
        })
        list.sort((a, b) => a.code - b.code)
        setItemList([...list]);
        setTotalElements(data.data.totalElements);
      }).catch(error => {
        toast.error(t("toastr.error"))
      });
  }

  const getListItemChild = (item) => {
    let result = []
    result.push(item);
    // eslint-disable-next-line no-unused-expressions
    item?.children && item?.children.forEach(child => {
      let childs = getListItemChild(child);
      result.push(...childs)
    });
    return result;
  };
  useEffect(() => {
    updatePageData();
  }, [page, rowsPerPage])

  const search = () => {
    updatePageData();
  }

  const handleDelete = (item) => {
    setShouldOpenConfirmationDialog(true);
    setItemMedicalEquipment(item);
  }
  const handleDialogClose = () => {
    setShouldOpenConfirmationDialog(false);
    setShouldOpenEditorDialog(false);
    setShouldOpenConfirmationDeleteAllDialog(false);
    updatePageData();
  }
  const handleConfirmationResponse = () => {
    deleteItem(itemMedicalEquipment?.id)
      .then((res) => {
        if (appConst.CODE.SUCCESS === res?.data?.code) {
          toast.success(t("general.deleteSuccess"));
          if (itemList.length - 1 === 0 && page !== 0) {
            setPage(page - 1)
            setShouldOpenConfirmationDialog(false);
            return;
          }
          handleDialogClose();
        }
        else {
          toast.warning(res?.data?.message);
          handleDialogClose();
          updatePageData();
        }
      })
      .catch((err) => {
        toast.warning(t("MedicalEquipmentType.notification.use"));
        toast.clearWaitingQueue();
      });
  }
  const handleDeleteList = async (list) => {
    let listAlert = [];
    let count = 0;
    for (let i = 0; i < list.length; i++) {
      let res = await deleteItem(list[i].id);
      if (appConst.CODE.SUCCESS === res?.data?.code) {
        count++;
      }
      else {
        listAlert.push(list[i].name);
      }
    }
    if (listAlert.length === list.length) {
      toast.warning(t("MedicalEquipmentType.notification.useAll"));
    } else if (listAlert.length > 0) {
      toast.warning(t("MedicalEquipmentType.notification.deletedUnused"));
    } else {
      toast.success(t("general.deleteSuccess"));
    }

    if (count > 0 && (itemList.length - count === 0) && page !== 0) {
      setPage(page - 1)
      setShouldOpenConfirmationDeleteAllDialog(false);
      return;
    }
    handleDialogClose();
  }
  const handleDeleteAll = () => {
    setShouldOpenConfirmationDeleteAllDialog(false);
    setPageLoading(true)
    handleDeleteList(data)
      .then(() => {
        setPageLoading(false)
        setData([]);
      })
      .catch(() => {
        setPageLoading(false)
        toast.error("Lỗi");
      })
  }
  const handleEditItem = async (item) => {
    try {
      if (item?.parentId) {
        const data = await getById(item?.parentId);
        if (data?.status === appConst.CODE.SUCCESS) {
          item.parent = { ...(data?.data || item?.parent) }
        }
      }
      setItemMedicalEquipment(item);
      setShouldOpenEditorDialog(true);
    } catch (error) {

    }
  };
  const handleChangePage = (e, newPage) => {
    setPage(newPage);
  }
  const handleChangeRowsPerPage = (e) => {
    setRowsPerPage(e.target.value);
    setPage(0);
  }
  const handleTextChange = (e) => {
    setKeyword(e.target.value);
  }
  const handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      setPage(0);
      updatePageData();
    }
  }
  const handleKeyUp = (e) => {
    !e.target.value && search()
  }
  const columns = [
    {
      title: t("general.action"),
      field: "custom",
      align: "center",
      minWidth: "100px",
      maxWidth: "100px",
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
              handleEditItem(rowData)
            } else if (method === 1) {
              handleDelete(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    { title: t("MedicalEquipmentType.code"), field: "code", minWidth: "610px" },
    { title: t("MedicalEquipmentType.name"), field: "name", minWidth: "610px" },
  ]
  return (
    <div className="m-sm-30">
      <Helmet>
        <title>
          {TitlePage} | {t("web_site")}
        </title>
      </Helmet>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            { name: t("Dashboard.category"), path: "/list/medicalEquipmentType" },
            { name: TitlePage },
          ]}
        />
      </div>
      <Grid container spacing={2} justifyContent="space-between">
        <Grid item container md={3} xs={12}>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleEditItem(null)}
          >
            {t("general.add")}
          </Button>
          {shouldOpenEditorDialog && (
            <MedicalEquipmentTypeDialog
              t={t}
              open={shouldOpenEditorDialog}
              handleClose={handleDialogClose}
              item={itemMedicalEquipment}
            />
          )}
          {shouldOpenConfirmationDeleteAllDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDeleteAllDialog}
              onConfirmDialogClose={handleDialogClose}
              onYesClick={handleDeleteAll}
              text={t("general.deleteConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box w-100"
              placeholder={t("MedicalEquipmentType.search")}
              id="search_box"
              name="keyword"
              value={keyword}
              onKeyDown={handleKeyDownEnterSearch}
              onChange={handleTextChange}
              onKeyUp={handleKeyUp}
              startAdornment={
                <InputAdornment position='end'>
                  <Link to="#">
                    <SearchIcon
                      onClick={() => search()}
                      style={{ position: "absolute", top: "0", right: "0" }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>

        <Grid item xs={12}>

          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={handleDialogClose}
              onYesClick={handleConfirmationResponse}
              text={t("general.deleteConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}

          <MaterialTable
            title="Danh sách công cụ dụng cụ"
            columns={columns}
            data={itemList}
            // data={[]}
            parentChildData={(row, rows) => {
              let list = rows.find((item) => item.id === row.parentId)
              return list
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              selection: false,
              sorting: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: itemList?.length > 0
                ? 0 : 250,
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            onSelectionChange={(rows) => {
              setData(rows)
            }}
          />

          <TablePagination
            {...defaultPaginationProps()}
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            count={totalElements}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
    </div>
  )
}

export default MedicalEquipmentType