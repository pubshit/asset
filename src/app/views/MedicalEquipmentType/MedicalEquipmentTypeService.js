import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/commonobject" + ConstantList.URL_PREFIX;


export const searchByPage = (searchObject) => {
      let config = { params: { keyword: searchObject.keyword } };
      let url = API_PATH + "/ltbyt/page/" + searchObject.pageIndex + '/' + searchObject.pageSize;
      return axios.get(url, config);
};
export const getById = id => {
      return axios.get(API_PATH + "/"+ id);
};
export const saveItem = item => {
      return axios.post(API_PATH + "/ltbyt", item);
};
export const updateItem = item => {
      return axios.put(API_PATH + "/ltbyt/" + item.id, item);
}
export const deleteItem = id => {
      let url = API_PATH + "/ltbyt/" + id;
      return axios.delete(url);
};
