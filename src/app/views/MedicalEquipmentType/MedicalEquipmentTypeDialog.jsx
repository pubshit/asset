import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    Paper
} from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import Draggable from 'react-draggable';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import SelectInstrumentToolsParentPopup from './SelectInstrumentToolsParentPopup';
import { saveItem, updateItem } from './MedicalEquipmentTypeService';
import {appConst, variable} from "../../appConst"
import { PaperComponent } from "../Component/Utilities";
import { handleKeyDownNameSpecialExcept } from "../../appFunction";



toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});

function MedicalEquipmentTypeDialog(props) {
    const {
        open,
        t,
        item,
        handleClose
    } = props

    const [itemMedicalEquipment, setMedicalEquipment] = useState({});
    const [shouldOpenSelectParentPopup, SetShouldOpenSelectParentPopup] = useState(false);

    useEffect(() => {
        item && setMedicalEquipment({ ...item })
    }, [item])

    const openParentPopup = () => {
        SetShouldOpenSelectParentPopup(true)
    }

    const handleFormSubmit = () => {
        let { id } = itemMedicalEquipment

        if (id) {
            updateItem(itemMedicalEquipment)
                .then((res) => {
                    if (appConst.CODE.SUCCESS === res?.data?.code) {
                        toast.success(t("MedicalEquipmentType.notification.updateSuccess"));
                        handleClose();
                    }
                    else {
                        toast.error(res?.data?.message)
                    }
                })
                .catch(() => {
                    toast.error(t("MedicalEquipmentType.notification.updateFail"))
                })
        } else {
            saveItem(itemMedicalEquipment)
                .then((res) => {
                    if (appConst.CODE.SUCCESS === res?.data?.code) {
                        handleSetDataAsset(res?.data?.data);
                        toast.success(t("MedicalEquipmentType.notification.addSuccess"));
                        handleClose();
                    }
                    else {
                        toast.error(res?.data?.message)
                    }
                })
                .catch(() => {
                    toast.error(t("MedicalEquipmentType.notification.addFali"))
                })
        }
    }

    const handleSetDataAsset = (data) => {
        if (props.selectMedicalEquipment) props.selectMedicalEquipment(data);
        if (props.handleSetDataSelect) props.handleSetDataSelect([]);
    }

    const handleChangeCode = (e) => {
        setMedicalEquipment({
            ...itemMedicalEquipment,
            code: e.target.value,
        })
    }
    const handleChangeName = (e) => {
        setMedicalEquipment({
            ...itemMedicalEquipment,
            name: e.target.value
        })
    }
    const handleClosePopup = () => {
        SetShouldOpenSelectParentPopup(false);
    }
    const handleSelectParent = (parent) => {
        setMedicalEquipment({
            ...itemMedicalEquipment,
            parentId: parent.id,
            parent: parent
        })
        SetShouldOpenSelectParentPopup(false)
    }
    return (
        <Dialog
            open={open}
            PaperComponent={PaperComponent}
            maxWidth="sm"
            fullWidth
        >
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                {t("MedicalEquipmentType.saveUpdate")}
            </DialogTitle>
            <ValidatorForm onSubmit={handleFormSubmit}>
                <DialogContent>
                    <Grid container spacing={1}>
                        <Grid item sm={12} xs={12}>
                            <Button
                                size="small"
                                className="float-right mt-10"
                                variant="contained"
                                color="primary"
                                onClick={openParentPopup}
                            >
                                {t('general.select')}
                            </Button>
                            <TextValidator
                                size="small"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label={<span><span className="colorRed"></span>{t("MedicalEquipmentType.parent")}</span>}
                                className="w-85"
                                value={itemMedicalEquipment?.parent !== null ? itemMedicalEquipment?.parent?.name || '' : ''}
                            />

                            {shouldOpenSelectParentPopup && (
                                <SelectInstrumentToolsParentPopup
                                    t={t}
                                    open={shouldOpenSelectParentPopup}
                                    handleClose={handleClosePopup}
                                    handleSelect={handleSelectParent}
                                    item={itemMedicalEquipment}
                                />
                            )}
                        </Grid>
                        <Grid item sm={12} xs={12}>
                            <TextValidator
                                className="w-100 "
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("MedicalEquipmentType.codeEdit")}</span>
                                    </span>
                                }
                                onChange={handleChangeCode}
                                type="text"
                                name="code"
                                value={itemMedicalEquipment?.code}
                                validators={["required", "matchRegexp:^[a-zA-Z0-9_.-]*$"]}
                                errorMessages={[t("general.required"), "Mã loại TBYT chỉ bao gồm các ký tự A-Z, a-z, 0-9, -, _"]}
                            />
                        </Grid>

                        <Grid item sm={12} xs={12}>
                            <TextValidator
                                className="w-100"
                                label={
                                    <span>
                                        <span className="colorRed">* </span>{" "}
                                        <span>{t("MedicalEquipmentType.nameEdit")}</span>
                                    </span>
                                }
                                onChange={handleChangeName}
                                type="text"
                                name="name"
                                value={itemMedicalEquipment?.name}
                                validators={["required", "matchRegexp:^.{1,255}$", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                                errorMessages={[t("general.required"), "Tối đa 255 ký tự", t("general.invalidFormat")]}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-12"
                            onClick={() => handleClose()}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            className="mr-15"
                        >
                            {t("general.save")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
}

export default MedicalEquipmentTypeDialog