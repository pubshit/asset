import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Input,
  InputAdornment,
  Radio,
  TablePagination,
} from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { ValidatorForm } from "react-material-ui-form-validator";
import { searchByPage } from "./MedicalEquipmentTypeService";
import { appConst } from "../../appConst"
import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function SelectInstrumentToolsParentPopup(props) {
  const { t, open, item, handleClose, handleSelect } = props;

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [totalElements, setTotalElements] = useState(0);
  const [keyword, setKeyword] = useState("");

  const [itemList, setItemList] = useState([]);
  const [itemSelect, setItemSelect] = useState({});

  const refRadio = useRef({});

  const updatePageData = async () => {
    let searchObject = {};
    searchObject.keyword = keyword;
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchByPage(searchObject).then(
      ({ data }) => {
        let itemList = [...data.data.content];
        let list = []
        itemList.forEach(item => {
          let items = getListItemChild(item);
          list.push(...items)
        })
        list.sort((a, b) => a.code - b.code)
        setItemList([...list])
        setTotalElements(data.data.totalElements)
      }
    );
  }
  const search = () => {
    updatePageData();
  }

  const getListItemChild = (item) => {
    let result = []
    result.push(item);
    // eslint-disable-next-line no-unused-expressions
    item?.children && item?.children.forEach(child => {
      let childs = getListItemChild(child);
      result.push(...childs)
    });
    return result;
  };
  useEffect(() => {
    updatePageData();
  }, [page, rowsPerPage]);

  useEffect(() => {
    item?.parent && setItemSelect(item?.parent)
  }, [item?.parent])

  const handleClick = (e, item) => {
    if (item?.id === itemSelect?.id) {
      setItemSelect({});
    } else {
      setItemSelect(item);
    }
  }
  const onClickRow = (selectedRow) => {
    let index = selectedRow?.tableData?.id;
    refRadio.current[index].click();
  };
  const handleChangePage = (e, newPage) => {
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (e) => {
    setRowsPerPage(e.target.value)
    setPage(0)
  }
  const handleTextChange = (e) => {
    setKeyword(e.target.value);
  }
  const handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      setPage(0);
      search();
    }
  }
  const handleKeyUp = (e) => {
    !e.target.value && search()
  }
  const handleSubmit = () => {
    handleSelect(itemSelect);
  }

  let columns = [
    {
      title: t("general.select"),
      field: "custom",
      align: "center",
      width: "250",
      render: (rowData) => (
        <Radio
          ref={(element) => refRadio.current[rowData?.tableData?.id] = element}
          name="radSelected"
          value={rowData?.id}
          checked={itemSelect?.id === rowData?.id}
          onClick={(e) => handleClick(e, rowData)}
        />
      ),
    },
    { title: t("MedicalEquipmentType.code"), field: "code", align: "left", width: "150" },
    { title: t("MedicalEquipmentType.name"), field: "name", width: "150" },
  ];

  return (
    <Dialog open={open} maxWidth={"md"} fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("MedicalEquipmentType.choose")}</span>
      </DialogTitle>
      <ValidatorForm>
        <DialogContent>
          <Grid item xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onKeyDown={handleKeyDownEnterSearch}
              onChange={handleTextChange}
              onKeyUp={handleKeyUp}
              style={{ width: "50%" }}
              className="mb-16 mr-12"
              id="search_box"
              placeholder={t("MedicalEquipmentType.searchPopup")}
              startAdornment={
                <InputAdornment position="end">
                  <SearchIcon
                    onClick={() => search(keyword)}
                    className="searchTable"
                  />
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={itemList}
              columns={columns}
              onRowClick={(e, selectedRow) => onClickRow(selectedRow)}
              parentChildData={(row, rows) => {
                return rows.find((a) => a.id === row.parentId);
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                draggable: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[5, 10, 20, 30]}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-36 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={handleSubmit}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

export default SelectInstrumentToolsParentPopup;
