import axios from "axios";
import {variable} from "../../appConst";


export function wfAxiosGenerator(baseEndpoint = "", configString = "") {
  const axiosFunc = {
    [variable.axiosMethod.GET]: (url, config) => axios.get(url, config),
    [variable.axiosMethod.POST]: (url, payload) => axios.post(url, payload),
    [variable.axiosMethod.PUT]: (url, payload) => axios.put(url, payload),
    [variable.axiosMethod.DELETE]: (url, config) => axios.delete(url),
  };
  const convertToJSON = (configString) => {
    try {
      return JSON.parse(configString);
    } catch (error) {
      console.error("Error parsing JSON:", error);
      return null;
    }
  };

  const getValueFromTemplate = (keyStr = '', props = '') => {
    return keyStr.split('.').reduce((acc, currKey) => acc?.[currKey], props);
  };

  const fillTemplate = (templates, props) => {
    const result = {};

    templates?.forEach((template) => {
      if (template?.includes(":")) {
        Object.keys(props)?.map((key) => {
          if (template?.includes(`{${key}}`)) {
            template = template?.replace(`{${key}}`, props[key]);
          }
        })
        let item = JSON.parse(template)
        Object.assign(result, item);
      } else {
        const [key, value] = template?.includes('=') ?
          template?.split('=') :
          template?.split('-');
        result[key] = template.includes('=') ? value : getValueFromTemplate(value, props);
      }
    });
    return {...props, ...result};
  };

  const generateAxiosRequest = (buttonConfig) => {
    const {code, name, componentName, action, url, payloadTemplate, paramsTemplate, pathVariables, t} = buttonConfig;

    const handleClick = async (sendData) => {

      try {
        const method = action;
        const payload = payloadTemplate ? fillTemplate(payloadTemplate?.split(","), sendData) : sendData;
        const params = fillTemplate(paramsTemplate, sendData); // Chưa cần sử dụng
        let finalUrl = url;

        if (pathVariables) {
          const newPathVariables = fillTemplate(pathVariables, sendData);
          for (const [key, value] of Object.entries(newPathVariables)) {
            finalUrl = finalUrl.replace(`{${key}}`, value);
          }
        }

        if (Object.values(variable.axiosMethod).includes(method?.toUpperCase())) {
          return await axiosFunc[method]?.(`${baseEndpoint}${finalUrl}`, payload);
        }

      } catch (error) {
        console.error(`[${code}] ${name} - Error:`, error);
      } finally {
        if (buttonConfig?.actions) {
          const lstActions = generateActions(buttonConfig);
          const actions = lstActions?.[code];
          actions?.forEach((func = (props) => {}) => func(props));
        }
      }
    };

    return {
      code,
      name,
      componentName,
      handleClick,
      type: Boolean(url) ? "submit" : "button",
    };
  };

  const generateActions = (actionsConfig = {}) => {
    let { code, actions } = actionsConfig;

    actions = actions?.map((action) => {
      const { method, url, payloadTemplate, pathVariables } = action;
      return async (props) => {
        try {
          let finalUrl = url;
          let payload = fillTemplate(payloadTemplate, props);

          if (pathVariables) {
            for (const pathInfo of pathVariables) {
              const pathKey = pathInfo?.split('-');
              finalUrl = finalUrl?.replace(`{${pathKey[0]}}`, getValueFromTemplate(pathKey[1], props));
            }
          }
          if (Object.values(variable.axiosMethod).includes(method?.toUpperCase())) {
            await axiosFunc[method]?.(`${baseEndpoint}${finalUrl}`, payload)
          }
        } catch (error) {
          console.error(`[${code}] - Error:`, error);
        }
      };
    })

    return { [code]: actions };
  };

  if (convertToJSON(configString)) {
    const axiosRequests = convertToJSON(configString)?.additionalButton?.map(generateAxiosRequest);
    let actions = {};
    convertToJSON(configString)?.additionalAction?.forEach((actionConfig) => {
      const action = generateActions(actionConfig);
      actions = { ...actions, ...action}
    });
    return { axiosRequests, actions };
  }
}

const convertPathVariables = (pathVariables) => {
  if (Array.isArray(pathVariables)) {
    let pathObject = {};
    pathVariables?.forEach(item => {
      let arr = item.split('-');
      // 'id-objectId'  =>  ["id", "objectId"]  =>  id: objectId
      pathObject[arr[0]] = arr[1];
    })
    return pathObject
  }
}

export const paramsConfig = (searchObject = {}) => {
  return {params: searchObject}
}