
export const defaultStatus = {
  statusId: null,
  statusCode: null,
  statusName: null,
}

export const defaultWfWorkflowsReq = {
  wfId : "",
  objectId : "",
  statusNew: defaultStatus,
  statusOld: defaultStatus,
}

const WfAction = {
  componentName: "",
  submit: (any) => new Promise((any) => {}),
  ...defaultWfWorkflowsReq,
}

export const defaultWfProps = {
  id: "",
  className: "",
  componentName: "",
  wfItem: "",
  type: "",
  handleClick: (props = WfAction) => {},
  additionalProps: null,
}

export const defaultAdditionalProps = {
  typeCodes: [""],
  typeCodesSubBtn: [""],
}


export const defaultWfWorkflowsRes = {
  trackerCode: "",
  oldStatusId: null,
  oldStatusCode: "",
  oldStatusName: "",
  currStatusId: null,
  currStatusCode: "",
  newStatusId: null,
  newStatusCode: "",
  newStatusName: "",
  authorityId: null,
  assignee: null,
  author: null,
  type: "",
  additionalConfiguration: "",
  rule: "",
  endpoint: "",
  id: "",
}

export const defaultWfSummaryStatusRes = {
  name: "",
  id: null,
  position: null,
  code: "",
  trackerCode: "",
  isClosed: false,
  color: "",
  endpoint: null
}

export const defaultWfSummaryStatusProps = {
  summaryStatus: [defaultWfSummaryStatusRes],
  value: null,
  onChange: (data = defaultWfSummaryStatusRes) => {},
}

export const TRACKER_STATUS_CODE = {
  REPAIR: {
    WAITING: { code: "waiting", name: "Chờ xử lý" },
    PROCESSING: { code: "processing", name: "Đang xử lý" },
    PENDING_APPROVAL: { code: "pending_approval", name: "Chờ duyệt" },
    APPROVAL: { code: "approved", name: "Đã duyệt" },
    REQUEST_FOR_ADDITIONAL_INFORMATION: { code: "rejected", name: "Yêu cầu bổ sung" },
    PROCESSED: { code: "processed", name: "Đã xử lý" },
    APPROVAL_HISTORY: { code: "approvalHistory", name: "Thông tin trình ký" },
  },
  APPROVAL_PROCESSES: {
    FIRST_LV_PENDING_APPROVAL: { code: "first_level_pending_approval", name: "Trình ký cấp 1" },
    SECOND_LEVEL_APPROVAL_PROCESS: { code: "second_level_pending_approval", name: "Trình ký cấp 2" },
    SECOND_LEVEL_APPROVED: { code: "second_level_approved", name: "Duyệt cấp 2" },
    SECOND_LEVEL_REJECTED: { code: "second_level_rejected", name: "Từ chối cấp 2" },
    REQUESTED_ADDITION: { code: "requested_addition", name: "Yêu cầu bổ sung" },
  },
}