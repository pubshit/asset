import axios from "axios";
import { paramsConfig } from "./helpers";
import ConstantList from "app/appConfig";

const BASE_URL = ConstantList.API_ENPOINT_ASSET_MAINTANE;
const API_PATH_WORKFLOWS = BASE_URL + "/api/wf-workflows";
const API_PATH_WORKFLOWS_STATUS = BASE_URL + "/api/wf-statuses";
const API_PATH_WORKFLOWS_TRACKER_FIELD = BASE_URL + "/api/wf-tracker-fields";


export const getWfWorkflow = (params) => {
  return axios.get(API_PATH_WORKFLOWS, {params});
};

export const getWfStatus = (params) => {
  let url = `${API_PATH_WORKFLOWS_STATUS}/search`
  return axios.get(url, { params });
}

export const getWfRelate = (params) => {
  let url = `${API_PATH_WORKFLOWS}/relate`
  return axios.get(url, { params });
}

export const getWfTrackerFields = (params) => {
  return axios.get(API_PATH_WORKFLOWS_TRACKER_FIELD, { params });
}