import {memo, useEffect, useState} from "react";
import {wfAxiosGenerator} from "./helpers";
import {defaultAdditionalProps, defaultWfProps, defaultWfWorkflowsReq, defaultWfWorkflowsRes} from "./constant";
import {useTranslation} from "react-i18next";
import {Button} from "@material-ui/core";
import ConstantList from "app/appConfig";
import PropTypes from "prop-types";
import clsx from "clsx";

const WfButton = memo((props = defaultWfProps) => {
  const {t} = useTranslation();
  const {wfItem, additionalProps = defaultAdditionalProps} = props;
  const {typeCodes, typeCodesSubBtn} = additionalProps;

  const [statusCode, setStatusCode] = useState(null);
  const [config, setConfig] = useState({componentName: ""});
  const [wfWorkflowsReq, setWfWorkflowsReq] = useState({...defaultWfWorkflowsReq});
  const [wfAxiosRequests, setWfAxiosRequests] = useState(null);

  const BASE_ENDPOINT = ConstantList.API_ENPOINT_ASSET_MAINTANE;
  const hasNoPermission = wfItem?.role?.split(",").includes(ConstantList.ROLES.NO_PERMISSION);
  const isShowStatus = Boolean(wfItem?.newStatusName)
    && props.componentName === config?.componentName
    && !hasNoPermission;

  useEffect(() => {
    updateStates(props?.wfItem)
  }, [props?.wfItem]);

  const updateStates = (wfItem = defaultWfWorkflowsRes || null) => {
    wfItem.additionalConfiguration && setConfig(JSON.parse(wfItem?.additionalConfiguration));
    setStatusCode({
      old: wfItem?.oldStatusCode,
      new: wfItem?.newStatusCode
    });
    setWfWorkflowsReq({
      statusNew: {
        statusId: wfItem?.newStatusId,
        statusCode: wfItem?.newStatusCode,
        statusName: wfItem?.newStatusName,
      },
      statusOld: {
        statusId: wfItem?.oldStatusId,
        statusCode: wfItem?.oldStatusCode,
        statusName: wfItem?.oldStatusName,
      },
      wfId: wfItem?.id,
      objectId: props?.wfItem?.objectId,
    })
    setWfAxiosRequests(wfAxiosGenerator(BASE_ENDPOINT, wfItem?.additionalConfiguration));
  }

  const handleButtonClick = (additionalButton) => {         // todo: handle click of sub button
    const submit = async (props) => {
      return await additionalButton?.handleClick?.(props)
    }

    if (additionalButton?.componentName && props?.handleClick) {
      props?.handleClick({
        ...defaultWfWorkflowsReq,
        code: additionalButton.code,
        componentName: additionalButton?.componentName,
        submit,
      });
    }
  };

  const handleReq = async (                                        // todo: handle click of main button
    statusReq = defaultWfWorkflowsReq,
    codeReq = "",
    componentName = ""
  ) => {
    const actions = wfAxiosRequests?.actions?.[codeReq];
    const submit = async () => {
      actions?.forEach((func = (props) => {}) => func(props));
    }

    if (props?.handleClick) {
      props?.handleClick?.({
        ...statusReq,
        componentName,
        submit: actions?.length ? submit : null,
      });
    } else {
      await submit();
    }
  };
  
  return (
    <>
      {!hasNoPermission && wfAxiosRequests?.axiosRequests?.map((additionalButton) => {
        return props.componentName === additionalButton.componentName && (
          <Button
            className={clsx(
              "mb-16 mr-16 align-bottom",
              props.className,
            )}
            variant="contained"
            color="primary"
            key={additionalButton.code}
            type={typeCodesSubBtn.includes(additionalButton.code) ? "button" : "submit"}
            id={additionalButton.type === "submit" ? "button-submit" : props?.id}
            onClick={() => handleButtonClick(additionalButton)}
          >
            {additionalButton.name}
          </Button>
        )
      })}
      {isShowStatus && (
        <Button
          className={clsx(
            "mb-16 mr-16 align-bottom",
            props.className,
          )}
          variant="contained"
          color="primary"
          type={typeCodes.includes(wfItem.newStatusCode) ? "button" : "submit"}
          id="button-submit"
          onClick={() => handleReq(wfWorkflowsReq, statusCode?.new, config?.componentName)}
        >
          <span>{config?.newStatusName || wfItem?.newStatusName}</span>
        </Button>
      )}
    </>
  );
});

export default WfButton;
WfButton.propTypes = {
  wfItem: PropTypes.any.isRequired,
  componentName: PropTypes.string,
  handleClick: (props) => {},
  additionalProps: PropTypes.any,
}
