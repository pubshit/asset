import React, {memo, useCallback} from 'react'
import {defaultWfSummaryStatusProps, defaultWfSummaryStatusRes} from "./constant";
import {Tab, Tabs} from "@material-ui/core";


const WfStatusSummary = memo((props = defaultWfSummaryStatusProps) => {
  const { data = [defaultWfSummaryStatusRes], value, onChange } = props;
  const renderLabelTab = useCallback((item) => {
    if (item?.summary) {
      return (
        <div className="tabLable">
          <span>{item?.name}</span>
          <div className={`tabQuantity`} style={{color: item?.color, borderColor: item?.color}}>
            {Number(item?.summary)}
          </div>
        </div>
      )
    } else {
      return item?.name;
    }
  }, [data])

  return (
    <Tabs
      className="tabsStatus"
      value={value}
      onChange={onChange}
      variant="scrollable"
      scrollButtons="on"
      indicatorColor="primary"
      textColor="primary"
      aria-label="scrollable force tabs example"
    >
      {data?.map((item = defaultWfSummaryStatusRes, index) => (
        <Tab
          className="tab"
          key={item.id}
          value={item.id}
          label={renderLabelTab(item)}
        />
      ))}
    </Tabs>
  )
})

export default WfStatusSummary