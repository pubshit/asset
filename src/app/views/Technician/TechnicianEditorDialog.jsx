import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  InputLabel,
  FormControl,
  MenuItem,
  Select,
  FormHelperText
} from "@material-ui/core";

import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import { ValidatorForm, TextValidator, TextField } from "react-material-ui-form-validator";
import { getAllQualification, getAllHealthOrg, saveItem, getItemById } from "./TechnicianService";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { el } from "date-fns/locale";
import EQAHealthOrgSearchDialog from './EQAHealthOrgSearchDialog';
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class TechnicianEditorDialog extends Component {
  constructor(props) {
    super(props);
    getAllQualification().then((result) => {
      let listQualification = result.data;
      this.setState({ listQualification: listQualification });
    });
    getAllHealthOrg().then((result) => {
      let listHealthOrg = result.data;
      this.setState({ listHealthOrg: listHealthOrg });
    });
  }

  state = {
    firstName: '',
    lastName: '',
    displayName: '',
    gender: '',
    qualification: '',
    healthOrg: '',
    birthDate: null,
    phoneNumber:'',
    idNumber:'',
    email:'',
    address:[],
    listQualification: [],
    listHealthOrg: [],
    shouldOpenSearchDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenSearchEQAHealthOrgSearchDialog: false,
  };

  listGender = [
    { id: 1, name: 'Nam' },
    { id: 2, name: 'Nữ' },
    { id: 3, name: 'Không xác định' }
  ]

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    if (source === "firstName") {
      let { lastName } = this.state;
      let displayName = event.target.value.trim() + " " + (lastName ? lastName : '');
      this.setState({ firstName: event.target.value, displayName: displayName });
      return;
    }
    else if (source === "lastName") {
      let { firstName } = this.state;
      let displayName = firstName.trim() + " " + (event.target.value.trim() ? event.target.value.trim() : '');
      this.setState({ lastName: event.target.value, displayName: displayName });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    this.setState({ hasErrorEQAHealthOrg: false, hasErrorQualification: false });
    if (!this.state.healthOrg || this.state.healthOrg <= 0) {
      this.setState({ hasErrorEQAHealthOrg: true });
      return;
    }
    else if (!this.state.qualification || this.state.qualification <= 0) {
      this.setState({ hasErrorQualification: true });
      return;
    }else{
      let { qualification } = this.state;
      let { listQualification } = this.state;
      let objQualification = listQualification.find(item => item.id == qualification);

      this.setState({ qualification: objQualification }, function () {
        saveItem({
          ...this.state
        }).then(() => {
          this.props.handleOKEditClose();
        });
      });

    }
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;
    this.setState({
      ...this.props.item
    }, function () {
      let { qualification } = this.state;
      if (qualification != null && qualification.id != null) {
        this.setState({ qualification: qualification.id });
      }
    });
  }

  handleSearchDialogClose = () => {
    this.setState({
      shouldOpenSearchDialog: false
    });
  };

  deleteTechnicianDetail(eQASampleSetDetail) {
    let index = null;
    let { details } = this.state;
    if (details != null && details.length > 0) {
      details.forEach(element => {
        if (element != null && element.sample != null && eQASampleSetDetail.sample != null && element.sample.id == eQASampleSetDetail.sample.id) {
          if (index == null) {
            index = 0;
          }
          else {
            index++;
          }
        }
      });

      details.splice(index, 1);
      this.setState({ details });
    }
  };
  
  handleSearchEQAHealthOrgDialogClose = () => {
    this.setState({
      shouldOpenSearchEQAHealthOrgSearchDialog: false
    });
  };
  handleSelectEQAHealthOrg = (item) => {
    this.setState({ healthOrg: item });
    this.handleSearchEQAHealthOrgDialogClose();
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    let {
      id,
      firstName,
      lastName,
      displayName,
      qualification,
      gender,
      listQualification,
      healthOrg,
      listHealthOrg,
      birthDate,
      phoneNumber,
      idNumber,
      email,
      address,
      hasErrorEQAHealthOrg,
      hasErrorQualification,
      shouldOpenSearchEQAHealthOrgSearchDialog
    } = this.state;

    return (
      <Dialog  open={open} PaperComponent={PaperComponent} maxWidth={'lg'} fullWidth={true} >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t("SaveUpdate")}</span>
        </DialogTitle>
        <DialogContent>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="mb-16" container spacing={2}>
              <Grid item sm={8} xs={12}>
                <TextValidator 
                disabled={true} 
                placeholder={t("EQAHealthOrg.title")} 
                id="healthOrg" 
                className="w-80 mt-16 mr-16" 
                value={healthOrg != null ? healthOrg.name : ''} />
                <Button
                  className="mt-16"
                  variant="contained"
                  color="primary"
                  onClick={() => this.setState({ shouldOpenSearchEQAHealthOrgSearchDialog: true, item: {} })}
                >
                  {t('Select')}
                </Button>
              </Grid>
              {shouldOpenSearchEQAHealthOrgSearchDialog && (
                <EQAHealthOrgSearchDialog
                  open={this.state.shouldOpenSearchEQAHealthOrgSearchDialog}
                  handleSelect={this.handleSelectEQAHealthOrg}
                  selectedItem={healthOrg != null ? healthOrg : {}}
                  handleClose={this.handleSearchEQAHealthOrgDialogClose} t={t} i18n={i18n} />
              )
              }
              {/* <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true} error={hasErrorEQAHealthOrg}>
                  <InputLabel htmlFor="healthOrg-simple">{t('EQAHealthOrg.title')}</InputLabel>
                  <Select
                    value={healthOrg ? healthOrg : ''}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "healthOrg",
                      id: "healthOrg-simple"
                    }}
                  >
                    {listHealthOrg.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                  {hasErrorEQAHealthOrg && <FormHelperText>This is required!</FormHelperText>}
                </FormControl>
              </Grid> */}
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true} error={hasErrorQualification}>
                  <InputLabel htmlFor="qualification-simple">{t('Qualification.title')}</InputLabel>
                  <Select
                    value={qualification ? qualification : ''}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "qualification",
                      id: "qualification-simple"
                    }}
                  >
                    {listQualification.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                  {hasErrorQualification && <FormHelperText>This is required!</FormHelperText>}
                </FormControl>
              </Grid>
            </Grid>
            <Grid className="mb-16" container spacing={2}>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.firstName")}
                  onChange={value => this.handleChange(value, 'firstName')}
                  type="text"
                  name="firstName"
                  value={firstName}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.lastName")}
                  onChange={value => this.handleChange(value, 'lastName')}
                  type="text"
                  name="lastName"
                  value={lastName}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  disabled={true}
                  className="w-100 mb-16"
                  label={t("general.displayName")}
                  type="text"
                  name="displayName"
                  value={displayName}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="gender-simple">{t('general.gender')}</InputLabel>
                  <Select
                    value={gender ? gender : ''}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "gender",
                      id: "gender-simple"
                    }}
                  >
                    {this.listGender.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={t('general.birthDate')}
                    inputVariant="standard"
                    type="text"
                    format="dd/MM/yyyy"
                    value={birthDate}
                    onChange={date => this.handleDateChange(date, "birthDate")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.phoneNumber")}
                  onChange={this.handleChange}
                  type="text"
                  name="phoneNumber"
                  value={phoneNumber}
                  validators={['isNumber']}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.idNumber")}
                  onChange={this.handleChange}
                  type="text"
                  name="idNumber"
                  value={idNumber}
                  validators={['isNumber']}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.email")}
                  onChange={this.handleChange}
                  placeholder='example@gmail.com'
                  type="email"
                  name="email"
                  value={email}
                  validators={['isEmail']}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              {/* <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("general.address")}
                  onChange={this.handleChange}
                  type="text"
                  name="address"
                  value={address}
                />
              </Grid> */}
            </Grid>
            <div className="flex flex-space-between flex-middle mt-16">
              <Button variant="contained" color="primary" type="submit">
                {t('Save')}
              </Button>
              <Button variant="contained" color="secondary" type="button" onClick={() => handleClose()}> {t('Cancel')}</Button>
            </div>
          </ValidatorForm>
        </DialogContent>
      </Dialog >
    );
  }
}

export default TechnicianEditorDialog;
