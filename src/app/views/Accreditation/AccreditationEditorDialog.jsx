import {
  Button,
  Dialog,
  DialogActions,
  Icon,
  IconButton,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import { Component } from "react";
import Draggable from "react-draggable";
import { useTranslation } from "react-i18next";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  createAccreditation,
  updateAccreditation,
} from "./AccreditationService";

import CircularProgress from "@material-ui/core/CircularProgress";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import localStorageService from "app/services/localStorageService";
import clsx from "clsx";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_loadding.scss";
import { formatDateDto } from "../../appFunction";
import {
  getAssetDocumentById,
  getNewCodeDocument,
} from "../Asset/AssetService";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import AccreditationScrollableTabsButtonForce from "./AccreditationScrollableTabsButtonForce";

toast.configure();

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

class AccreditationEditorDialog extends Component {
  state = {
    type: appConst.TYPE_KD_HC.KIEM_DINH.code,
    rowsPerPage: 10,
    page: 0,
    assetVouchers: [],
    handoverDepartment: null,
    receiverDepartment: null,
    ngayDuyet: null,
    ngayDuyetBaoGia: null,
    ngayLap: null,
    shouldOpenAssetPopup: false,
    totalElements: 0,
    asset: {},
    shouldOpenNotificationPopup: false,
    textNotificationPopup: "",
    shouldOpenPopupAssetFile: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_ALLOCATION,
    kdAttachments: [],
    loading: false,
    keyword: "",
    assetId: "",
    usePerson: null,
    trangThai: appConst.listStatuscalibrationObject.MOI_TAO,
    documents: [],
    isViewAssetFile: false,
    implementingAgencies: null,
    ghiChu: null,
    khTen: null,
    thongTuNghiDinh: null,
    kdDvThucHien: null,
    khHinhThuc: null
  };

  convertDto = (state) => {
    const { id, trangThai, clone } = this.state;
    let data = {
      kdTaiSans: state?.assetVouchers?.map((item, index) => {
        let items = item?.asset;
        let cloneItem = clone && clone[index];
        if (
          id &&
          trangThai?.code !==
          appConst.listStatuscalibrationObject.MOI_TAO.indexOrder
        ) {
          return {
            kdDvThucHienId: items?.kdDvThucHien?.id,
            kdDvThucHienText: items?.kdDvThucHien?.name,
            kdNgayKyHopDong: formatDateDto(items?.kdNgayKyHopDong),
            kdChiPhi: parseInt(items?.kdChiPhi, 10),
            kdAttachments: [],
            kdNtId: items?.kdNtId,
            kdSoTem: items?.kdSoTem,
            kdThoiHan: items?.hanKiemDinh,
            kdTrangThai: items?.kdTrangThai?.code,
            kdTrangThaiText: items?.kdTrangThai?.name,
            khId: items?.khId,
            tsDvtId: items?.unit?.id,
            tsDvtTen: items?.unit?.name,
            tsPhongQlId: items?.managementDepartment?.id,
            tsPhongQlText: items?.managementDepartment?.name,
            tsPhongSdId: items?.useDepartment?.id,
            tsPhongSdText: items?.useDepartment?.name,
            tsSerialNo: items?.serialNumber,
            tsStatusId: items?.status?.id,
            tsTen: items?.name,
            tsId: cloneItem?.tsId ? cloneItem?.tsId : items?.id,
            id: cloneItem?.id,
            kdHinhThuc: items?.kdHinhThucObject?.code,
            type: state?.type,
            usePerson: items?.usePerson,
          };
        } else {
          return {
            kdChiPhi: parseInt(items?.kdChiPhi, 10),
            kdAttachments: [],
            kdNtId: items?.kdNtId,
            kdSoTem: items?.kdSoTem,
            kdThoiHan: items?.hanKiemDinh,
            kdTrangThai: items?.kdTrangThai?.code,
            kdTrangThaiText: items?.kdTrangThai?.name,
            kdDvThucHienId: items?.kdDvThucHien?.id,
            kdDvThucHienText: items?.kdDvThucHien?.name,
            khId: items?.khId,
            tsDvtId: items?.unit?.id,
            tsDvtTen: items?.unit?.name,
            tsHangSx: items?.tenDonViCapPhep,
            tsMa: items?.code,
            tsMaQl: items?.batchCode,
            tsModel: items?.model,
            tsNamSd: items?.tsNamSd,
            tsNuocSx: items?.madeIn,
            tsPhongQlId: items?.managementDepartment?.id,
            tsPhongQlText: items?.managementDepartment?.name,
            tsPhongSdId: items?.useDepartment?.id,
            tsPhongSdText: items?.useDepartment?.name,
            tsSerialNo: items?.serialNumber,
            tsStatusId: items?.status?.id,
            tsTen: items?.name,
            tsId: items?.tsId || items?.id,
            kdHinhThuc: items?.kdHinhThucObject?.code,
            type: state?.type,
            usePerson: items?.usePerson,
            kdNgayKyHopDong: formatDateDto(items?.kdNgayKyHopDong),
          };
        }
      }),
      id: state?.id,
      trangThai: state?.trangThai?.code,
      trangThaiText: state?.trangThai?.name,
      phongBanId: state?.handoverDepartment?.id,
      phongBanText: state?.handoverDepartment?.name,
      thongTuNghiDinh: state?.thongTuNghiDinh,
      ghiChu: state?.ghiChu,
      khTen: state?.khTen,
      ngayDuyet: formatDateDto(state?.ngayDuyet),
      ngayDuyetBaoGia: formatDateDto(state?.ngayDuyetBaoGia),
      ngayLap: formatDateDto(state?.ngayLap),
    };
    return data;
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  checkDataApi = async (id, dataState) => {
    if (id) {
      return await updateAccreditation(id, dataState);
    } else {
      return await createAccreditation(dataState);
    }
  };
  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let { t } = this.props;
    let dataState = this.convertDto(this.state);
    if (!dataState?.kdTaiSans?.length) {
      toast.warning(t("Asset.please_choose_product"));
      return;
    }
    setPageLoading(true);
    try {
      let res = await this.checkDataApi(id, dataState);
      setPageLoading(false);
      if (res?.data && res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.props.handleOKEditClose();
        toast.success(t("general.success"));
      } else {
        toast.warning(res?.data?.data[0]?.errorMessage);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
      setPageLoading(false);
    }
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };
  componentWillMount() {
    let { item } = this.props;
    let departmentUser = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER
    );

    this.setState({
      ...item,
      handoverDepartment:
        this.props?.item?.handoverDepartment || departmentUser,
      handoverDepartmentId:
        this.props?.item?.handoverDepartment?.id || departmentUser?.id,
    });
  }

  componentDidMount() { }

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleAssetPopupOpen = () => {
    this.setState({
      shouldOpenAssetPopup: true,
    });
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item != null && item.id != null) {
      if (assetVouchers == null) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      } else {
        let hasInList = false;
        assetVouchers.forEach((element) => {
          if (element.asset.id == item.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState(
      { assetVouchers, totalElements: assetVouchers.length },
      function () {
        this.handleAssetPopupClose();
      }
    );
  };

  handleSelectAssetAll = (items) => {
    let { t } = this.props;
    items.forEach((element) => {
      element.assetId = element?.asset?.id;
    });
    if (items.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning(t("general.noAssetSelected"));
    }
  };

  handleStatusChange = (trangThai) => {
    this.setState({ trangThai: trangThai });
  };

  selectUsePerson = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.usePerson = item;
          assetVoucher.usePerson = item ?? null;
          assetVoucher.usePersonId = item?.id ?? null;
          assetVoucher.usePersonId = item?.personId ?? null;
          assetVoucher.usePersonDisplayName = item?.personDisplayName;
        }
      }
      this.setState({ assetVouchers: assetVouchers });
    }
  };
  selectType = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.kdHinhThucObject = item;
        }
      }
      this.setState({ assetVouchers: assetVouchers });
    }
  };
  selectAssetKd = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.kdTrangThai = item;
        }
      }
      this.setState({ assetVouchers: assetVouchers });
    }
  };
  handlekdChiPhiChange = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      let sum = 0
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.kdChiPhi = item?.target?.value;
        }
        sum = sum + Number(assetVoucher.asset.kdChiPhi) 
      };
      this.setState({ assetVouchers: assetVouchers ,tongChiPhi: sum});
    }
  };

  selectkdDvThucHien = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.kdDvThucHien = item;
        }
      }
      this.setState({ assetVouchers: assetVouchers });
    }
  };

  handleChangeDateRowData = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      for (const assetVoucher of assetVouchers) {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.asset.kdNgayKyHopDong = formatDateDto(item);
        }
      }
      this.setState({ assetVouchers: assetVouchers });
    }
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item ? item : null,
      handoverDepartmentId: item?.id,
      managementDepartmentId: item?.id,
      assetVouchers: [],
      listHandoverPerson: [],
    });
  };

  selectReceiverDepartment = (item) => {
    let { assetVouchers } = this.state;
    assetVouchers?.length > 0 &&
      assetVouchers.forEach((assetVoucher) => {
        assetVoucher.usePerson = null;
        assetVoucher.usePersonId = null;
      });
    this.setState({
      supplierId: item?.id,
      listReceiverPerson: [],
      assetVouchers: assetVouchers,
      isCheckReceiverDP: item ? false : true,
      receiverPersonId: null,
      usePerson: null,
    });
  };
  selectImplementingAgencies = (item) => {
    let { assetVouchers } = this.state;
    assetVouchers?.length > 0 &&
      assetVouchers.forEach((assetVoucher) => {
        assetVoucher.usePerson = null;
        assetVoucher.usePersonId = null;
      });
    this.setState({
      implementingAgencies: item ? item : null,
      implementingAgenciesId: item?.id,
      listReceiverPerson: [],
      assetVouchers: assetVouchers,
      isCheckReceiverDP: item ? false : true,
      receiverPersonId: null,
      usePerson: null,
    });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes != null && attributes.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (
          attributes[index].attribute &&
          attributes[index].attribute.id == rowData.attribute.id
        ) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () { });
  };

  getAssetDocument = (document) => {
    let kdAttachments = this.state?.kdAttachments
      ? this.state?.kdAttachments
      : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && kdAttachments.push(document?.id);
    document && documents.push(document);
    this.setState({ kdAttachments, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id);
      });

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState({
        item: document,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true,
      });
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id);
      });

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState({
        item: document,
        shouldOpenPopupAssetFile: true,
        isViewAssetFile: true,
      });
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, kdAttachments } = this.state;
    let index = documents?.findIndex((document) => document?.id === id);
    let indexId = kdAttachments?.findIndex((documentId) => documentId === id);
    documents.splice(index, 1);
    kdAttachments.splice(indexId, 1);
    this.setState({ kdAttachments, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(
      (item) => item?.id === document?.id
    );
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        assetVoucher.ghiChu = valueText.target.value;
      }
    });
    this.setState({ assetVouchers: assetVouchers });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data,
    });
  };

  render() {
    let { open, t, i18n, isVisibility, isStatus, isHideButton } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      isAssetManagement: true,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };

    let receiverPersonSearchObject = {
      pageIndex: 1,
      pageSize: 1000,
      keyword: "",
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };

    let receiverDepartmentSearchObject = {
      pageIndex: 1,
      pageSize: 100,
      keyword: "",
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let {
      receiverDepartment,
      shouldOpenNotificationPopup,
      textNotificationPopup,
      loading,
    } = this.state;

    let searchObject = { pageIndex: 1, pageSize: 1000000, departmentId: "" };
    if (receiverDepartment) {
      searchObject = {
        pageIndex: 1,
        pageSize: 1000000,
        departmentId: receiverDepartment.id,
      };
    }

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}

        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>

        <ValidatorForm
          id="parentAssetAllocation"
          ref="form"
          name={variable.listInputName.formAllocation}
          f
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "3px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("Verification.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent style={{ height: "550px" }}>
            <AccreditationScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              SearchObject={searchObject}
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              selectReceiverDepartment={this.selectReceiverDepartment}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              handleDateChange={this.handleDateChange}
              handleAssetPopupOpen={this.handleAssetPopupOpen}
              handleSelectAssetAll={this.handleSelectAssetAll}
              handleAssetPopupClose={this.handleAssetPopupClose}
              removeAssetInlist={this.removeAssetInlist}
              selectUsePerson={this.selectUsePerson}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleRowDataCellChange={this.handleRowDataCellChange}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              getAssetDocument={this.getAssetDocument}
              isVisibility={isVisibility}
              isStatus={isStatus}
              handleFormSubmit={this.handleFormSubmit}
              isAllocation={this.props.isAllocation}
              isHideButton={isHideButton}
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              handleRowDataCellViewAssetFile={
                this.handleRowDataCellViewAssetFile
              }
              selectImplementingAgencies={this.selectImplementingAgencies}
              handleStatusChange={this.handleStatusChange}
              selectType={this.selectType}
              selectAssetKd={this.selectAssetKd}
              handlekdChiPhiChange={this.handlekdChiPhiChange}
              selectkdDvThucHien={this.selectkdDvThucHien}
              handleChangeDateRowData={this.handleChangeDateRowData}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() =>
                  this.props?.isAllocation
                    ? this.props?.isUpdateAsset
                      ? this.props.handleCloseAllocationEditorDialog()
                      : this.props.handleOKEditClose()
                    : this.props.handleClose()
                }
              >
                {t("general.cancel")}
              </Button>
              {(!this.props.isVisibility || this.props.isStatus) && (
                <Button
                  id="save"
                  variant="contained"
                  className="mr-12"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AccreditationEditorDialog.contextType = AppContext;
export default AccreditationEditorDialog;
