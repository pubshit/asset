import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_PERSON =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/kd-ke-hoach";
const API_PATH_PREFIX =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/kd-ke-hoach" + ConstantList.URL_PREFIX;

//api mới
export const searchByPage = (searchObject) => {
  let url = API_PATH + `/get-list`;
  return axios.get(url, { params: searchObject });
};

export const createAccreditation = (data) => {
  return axios.post(API_PATH, data);
};
export const updateAccreditation = (id, data) => {
  let url = API_PATH + "/" + id;
  return axios.put(url, data);
};
export const getItemById = (id) => {
  let url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const updateToReturn = (id, data, status) => {
  let url = API_PATH + "/" + id + "/status/" + status;
  return axios.put(url, data);
};
export const getCountStatus = () => {
  let url = API_PATH + "/count-by-statuses";
  return axios.get(url);
};

export const isLastRecord = (id) => {
  return axios.get(API_PATH_voucher + "/isLastRecord/" + id);
};

export const addNewOrUpdate = (asset) => {
  if (asset.id) {
    return axios.put(API_PATH_voucher + "/" + asset.id, asset);
  } else {
    return axios.post(API_PATH_voucher, asset);
  }
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/cap-phat-tai-san-co-dinh",
    data: searchObject,
    responseType: "blob",
  });
};
export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};
export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartment/" + userId);
};
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};
export const searchReceiverDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};
export const searchReceiverPerson = (searchObject) => {
  var url = API_PATH_PERSON + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

const API_PATH_VOUCHERSALLOCATION =
  ConstantList.API_ENPOINT + "/api/instruments-and-tool/allocation-vouchers";

export const addAllocation = (allocation) => {
  return axios.post(API_PATH_VOUCHERSALLOCATION, allocation);
};
export const getListAssetByOrgNotInPhieuDx = (searchObject) => {
  return axios.post(API_PATH_PREFIX + "/asset", searchObject);
};
