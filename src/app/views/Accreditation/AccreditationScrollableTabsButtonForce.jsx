import DateFnsUtils from "@date-io/date-fns";
import { Button, Grid, Icon, IconButton } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { createFilterOptions } from "@material-ui/lab";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { appConst, keySearch, variable } from "app/appConst";
import { NumberFormatCustom, checkInvalidDate, convertNumberPrice, filterOptions, isCheckLenght } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import SupplierDialog from "../Supplier/SupplierDialog";
import { searchByTextNew } from "../Supplier/SupplierService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchReceiverDepartment } from "./AccreditationService";
import AssetFilePopup from "./AssetFilePopup";
import moment from "moment";

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => props.onSelect(item, appConst.active.delete)}
      >
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: "-10px",
    left: "-25px",
    width: "80px",
  },
}))(Tooltip);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AccreditationScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const filterAutocomplete = createFilterOptions();
  const [listData, setListData] = useState([]);
  const [listStatus, setListStatus] = useState([]);
  const [listSupply, setListSupply] = useState([]);
  const [query, setQuery] = useState({
    pageIndex: 0,
    pageSize: 10,
    keyword: "",
    keySearch: "",
    typeCodes: appConst.TYPE_CODES.NCC_KD
  });
  const [shouldOpenAddSupplierDialog, setShouldOpenAddSupplierDialog] = useState({ open: false, rowData: {} })

  useEffect(() => {
    if (query.keySearch === keySearch.supply) {
      handleGetListSupply();
    }
  }, [query]);

  useEffect(() => {
    handleGetListSupply();
  }, []);

  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255);
    });
  }, [props?.item?.assetVouchers]);

  useEffect(() => {
    let status = [];
    const { item } = props;
    const { listStatuscalibrationObject, listStatuscalibration } = appConst;

    if (
      item?.statusIndex?.indexOrder === listStatuscalibrationObject.MOI_TAO.indexOrder ||
      !item?.id
    ) {
      status = listStatuscalibration.filter(
        (i) => i.indexOrder !== listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder &&
          i.indexOrder !== listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder
      );
    } else if (item?.statusIndex?.indexOrder === listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder) {
      status = listStatuscalibration.filter(
        (i) => i.indexOrder === listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder);
    } else if (item?.statusIndex?.indexOrder === listStatuscalibrationObject.DA_DUYET_DANH_MUC.indexOrder) {
      status = listStatuscalibration.filter(
        (i) => i.indexOrder !== listStatuscalibrationObject.MOI_TAO.indexOrder &&
          i.indexOrder !== listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder
      );
    } else {
      status = listStatuscalibration.filter((i) => i.indexOrder !== listStatuscalibrationObject.MOI_TAO.indexOrder);
    }

    setListStatus(status);
  }, [props?.item?.statusIndex?.indexOrder]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleGetListSupply = async () => {
    try {
      var searchObject = {};
      searchObject.keyword = query.keyword.trim();
      searchObject.pageIndex = 1;
      searchObject.pageSize = 1000000;
      searchObject.typeCodes = [appConst.TYPE_CODES.NCC_KD];
      const res = await searchByTextNew(searchObject);
      setListSupply([]);
      if (res?.data?.content?.length > 0) {
        setListSupply(res?.data?.content);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearch = (e, source) => {
    setQuery({ ...query, keyword: e.target.value, keySearch: source });
  };

  let columns = [
    ...((props.isVisibility || (props?.item?.id && !(props?.item?.statusIndex?.indexOrder === appConst.listStatuscalibrationObject.MOI_TAO.indexOrder)))
      ? [] : [
        {
          title: t("Asset.action"),
          field: "custom",
          align: "left",
          maxWidth: 200,
          minWidth: 120,
          cellStyle: {
            paddingLeft: 10,
            paddingRight: 10,
            textAlign: "center",
          },
          render: (rowData) =>
            !props.isVisibility && (
              <MaterialButton
                item={rowData}
                onSelect={(rowData, method) => {
                  if (appConst.active.delete === method) {
                    props.removeAssetInlist(rowData.asset.id);
                  } else {
                    alert("Call Selected Here:" + rowData.id);
                  }
                }}
              />
            ),
        },
      ]
    ),
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
        wordBreak: "break-word",
      },
      render: (rowData) => {
        return (
          props?.item?.page * props?.item?.rowsPerPage +
          (rowData?.tableData?.id + 1)
        );
      },
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
        wordBreak: "break-word",
      },
      render: (rowData) => rowData?.asset?.tsMa || rowData?.asset?.code,
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) => rowData?.asset?.tsTen || rowData?.asset?.name,
    },
    {
      title: t("Calibration.status"),
      field: "asset.kdTrangThai",
      minWidth: 180,
      align: "center",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) => {
        // return props.isVisibility ? (
        return rowData?.asset?.kdTrangThai?.name
          ? rowData?.asset?.kdTrangThai?.name
          : appConst.listStatusAssetsKDObject.CHO_XU_LY.name
        // ) 
        // : (
        //   <Autocomplete
        //     id="combo-box"
        //     fullWidth
        //     size="small"
        //     value={rowData?.asset?.kdTrangThai}
        //     options={appConst.listStatusAssetsKD}
        //     onChange={(event, value) => props.selectAssetKd(rowData, value)}
        //     getOptionLabel={(option) => option.name}
        //     filterOptions={(options, params) => {
        //       params.inputValue = params.inputValue.trim();
        //       return filterAutocomplete(options, params);
        //     }}
        //     renderInput={(params) => (
        //       <TextValidator
        //         {...params}
        //         variant="standard"
        //         value={rowData?.asset?.kdTrangThai ? rowData?.asset?.kdTrangThai?.name : null}
        //         inputProps={{
        //           ...params.inputProps,
        //         }}
        //         validators={["required"]}
        //         errorMessages={[t("general.required")]}
        //       />
        //     )}
        //     noOptionsText={t("general.noOption")}
        //   />
        // );
      },
    },
    {
      title: t("AssetType.type"),
      field: "asset.kdHinhThucText",
      minWidth: 180,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) => {
        return (props.isVisibility || props?.item?.trangThai?.code === appConst.listStatusVerificationPlanningObject.DA_DUYET_BAO_GIA.code && props?.item?.kdTaiSans?.some((i)=>i?.asset?.asset?.id===rowData?.assetId&&i?.kdChiPhi)) ? (rowData?.asset?.kdHinhThucText ? rowData?.asset?.kdHinhThucText : null
        ) : (
          <Autocomplete
            id="combo-box"
            fullWidth
            size="small"
            value={rowData?.asset?.kdHinhThucObject || { code: rowData?.asset?.kdHinhThuc, name: rowData?.asset?.kdHinhThucText }}
            options={appConst.OPTION_TYPE_KH_KIEM_DINH}
            onChange={(event, value) => props.selectType(rowData, value)}
            getOptionLabel={(option) => option.name}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              return filterAutocomplete(options, params);
            }}
            renderInput={(params) => (
              <TextValidator
                {...params}
                variant="standard"
                value={rowData?.asset?.kdHinhThucObject?.name ? rowData?.asset?.kdHinhThucObject?.name : null}
                inputProps={{
                  ...params.inputProps,
                }}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            )}
            noOptionsText={t("general.noOption")}
          />
        );
      },
    },
    ...(props?.item?.trangThai?.code ===
      appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder ||
      props?.item?.trangThai?.code ===
      appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder
      ? [
        {
          title: t("VerificationCalibration.regDateContact"),
          field: "asset.kdNgayKyHopDong",
          minWidth: 180,
          align: "left",
          cellStyle: {
            paddingLeft: 10,
            paddingRight: 10,
          },
          render: (rowData) => {
            return (props.isVisibility || props?.item?.trangThai?.code === appConst.listStatusVerificationPlanningObject.DA_DUYET_BAO_GIA.code && props?.item?.kdTaiSans?.some((i) => i?.asset?.asset?.id === rowData?.assetId && i?.kdChiPhi)) ? (
              rowData?.asset?.kdNgayKyHopDong
                ? moment(rowData?.asset?.kdNgayKyHopDong).format("DD/MM/YYYY")
                : null
            ) : (
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog"
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={rowData?.asset?.kdNgayKyHopDong}
                  onChange={(date) => props.handleChangeDateRowData(rowData, date)}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  disableFuture
                  maxDate={new Date()}
                  maxDateMessage={t(
                    "VerificationCalibration.maxDateMessage"
                  )}
                  minDate={props?.item?.ngayDuyet}
                  minDateMessage={t("VerificationCalibration.minDateContactMessage")}
                />
              </MuiPickersUtilsProvider>
            );
          },
        },
      ]
      : []),
    ...(props?.item?.trangThai?.code ===
      appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder ||
      props?.item?.trangThai?.code ===
      appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder
      ? [
        {
          title: t("Calibration.implementingAgencies"),
          field: "asset.kdDvThucHien",
          minWidth: 250,
          align: "left",
          cellStyle: {
            paddingLeft: 10,
            paddingRight: 10,
          },
          render: (rowData) => {
            return (props.isVisibility || props?.item?.trangThai?.code === appConst.listStatusVerificationPlanningObject.DA_DUYET_BAO_GIA.code && props?.item?.kdTaiSans?.some((i) => i?.asset?.asset?.id === rowData?.assetId && i?.kdChiPhi)) ? (
              rowData?.asset?.kdDvThucHien?.name
                ? rowData?.asset?.kdDvThucHien?.name
                : null
            ) : (
              <Autocomplete
                id="combo-box"
                size="small"
                options={listSupply}
                onChange={(event, value) => {
                  props.selectkdDvThucHien(rowData, value)
                  value?.code === "New" && setShouldOpenAddSupplierDialog({ open: true, rowData: rowData })
                }}
                value={
                  rowData?.asset?.kdDvThucHien
                    ? rowData?.asset?.kdDvThucHien
                    : null
                }
                getOptionLabel={(option) => option.name || ""}
                filterOptions={(options, params) =>
                  filterOptions(options, params, true, "name")
                }
                renderInput={(params) => (
                  <TextValidator
                    {...params}
                    variant="standard"
                    value={
                      (rowData?.asset?.kdDvThucHien?.code || rowData?.asset?.kdDvThucHien?.id)
                        ? rowData?.asset?.kdDvThucHien?.name
                        : null
                    }
                    onChange={(event) => handleSearch(event, keySearch.supply)}
                    validators={rowData?.asset?.kdNgayKyHopDong ? ["required"] : []}
                    errorMessages={[t("general.required")]}
                  />
                )}

                noOptionsText={t("general.noOption")}
              />
            );
          },
        },
      ]
      : []),
    ...([
      appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder,
      appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder].includes(props?.item?.trangThai?.code)
      ? [{
        title: t("AssetType.cost"),
        field: "asset.kdChiPhi",
        minWidth: 180,
        align: "right",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return (props.isVisibility || props?.item?.trangThai?.code === appConst.listStatusVerificationPlanningObject.DA_DUYET_BAO_GIA.code && props?.item?.kdTaiSans?.some((i) => i?.asset?.asset?.id === rowData?.assetId && i?.kdChiPhi)) ? (rowData?.asset?.kdChiPhi ? convertNumberPrice(rowData?.asset?.kdChiPhi) : null
          ) : (
            <TextValidator
              className="w-100"
              onChange={(kdChiPhi) =>
                props.handlekdChiPhiChange(rowData, kdChiPhi)
              }
              type="text"
              name="kdChiPhi"
              value={rowData?.asset?.kdChiPhi ? rowData?.asset?.kdChiPhi : null}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  style: { textAlign: "right" },
                },
                // readOnly: props?.item?.trangThai?.code === appConst.listStatusVerificationPlanningObject.DA_DUYET_BAO_GIA.code && props?.item?.kdTaiSans?.some((i)=>i?.asset?.asset?.id===rowData?.assetId&&i?.kdChiPhi)
              }}
              validators={rowData?.asset?.kdNgayKyHopDong ? ["required", "isLengthValid", "minNumber:0"] : ["isLengthValid", "minNumber:0"]}
              errorMessages={rowData?.asset?.kdNgayKyHopDong ? [t("general.required"), t("general.errorInput255"), t("general.minNumberError"),] : [t("general.errorInput255"), t("general.minNumberError"),]}
            />
          );
        },
      }] : []),
    {
      title: t("Asset.serialNumber"),
      field: "tsSerialNo",
      align: "left",
      minWidth: 110,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.tsSerialNo || rowData?.asset?.serialNumber || rowData?.serialNumber || "",
    },
    {
      title: t("Asset.model"),
      field: "assetModel",
      align: "left",
      minWidth: 110,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.tsModel || rowData.asset?.model || rowData?.model || "",
    },
    {
      title: t("Asset.manufacturer"),
      field: "manufacturerName",
      align: "left",
      minWidth: 110,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) =>
        rowData?.asset?.tsNuocSx || rowData?.asset?.manufacturer?.name || "",
    },
  ];

  let columnsAssetFile = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
        wordBreak: "break-word",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 300,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 100,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => (
        // !props.isVisibility ?
        <div className="none_wrap">
          <LightTooltip
            title={t("general.editIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() =>
                props.handleRowDataCellDeleteAssetFile(rowData?.id)
              }
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
  ];
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  };

  return (
    <form id="firstChild">
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              label={t(
                "ReceivingScrollableTabsButtonForce.tab.InformationVoting"
              )}
              {...a11yProps(0)}
            />
            <Tab
              label={t(
                "ReceivingScrollableTabsButtonForce.tab.AttachedProfile"
              )}
              {...a11yProps(1)}
            />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container>
            <Grid container spacing={3}>
              <Grid item md={4} sm={4} xs={12}>
                <CustomValidatePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={
                    <span className="w-100">
                      <span className="colorRed">* </span>
                      {t("Calibration.planDate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={"ngayLap"}
                  value={props?.item?.ngayLap}
                  onChange={(date) => props.handleDateChange(date, "ngayLap")}
                  // readOnly={props?.item?.id}
                  disableFuture
                  maxDate={new Date()}
                  maxDateMessage={t(
                    "VerificationCalibration.maxDateMessage"
                  )}
                  InputProps={{
                    readOnly: props.isVisibility,
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  onBlur={() => handleBlurDate(props?.item?.ngayLap, "ngayLap")}
                  clearable
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid item md={4} sm={4} xs={12} className="">
                {props.isVisibility ? (
                  <TextValidator
                    className={"w-100"}
                    label={<span>{t("Calibration.plantName")}</span>}
                    name="khTen"
                    value={props.item?.khTen ? props.item?.khTen : null}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                ) : (
                  <TextValidator
                    className={props.className ? props.className : "w-100"}
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("Calibration.plantName")}
                      </span>
                    }
                    name="khTen"
                    value={props.item?.khTen ? props.item?.khTen : null}
                    onChange={props?.handleChange}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                )}
              </Grid>

              <Grid className="" item md={4} sm={4} xs={12}>
                {props.isVisibility && !props.isStatus ? (
                  <TextValidator
                    className={props.className ? props.className : "w-100"}
                    label={<span>{t("allocation_asset.voucherStatus")}</span>}
                    value={
                      props?.item?.trangThai ? props?.item?.trangThai?.name : ""
                    }
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                ) : (
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        <span> {t("TransferToAnotherUnit.status")}</span>
                      </span>
                    }
                    listData={listStatus}
                    displayLable={"name"}
                    value={
                      props?.item?.trangThai ? props?.item?.trangThai : null
                    }
                    onSelect={props.handleStatusChange}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    InputProps={{
                      readOnly: true,
                    }}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      let filtered = filterAutocomplete(options, params);
                      return filtered;
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                )}
              </Grid>
              {[
                appConst.listStatuscalibrationObject.DA_DUYET_DANH_MUC.indexOrder,
                appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder,
                appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder].includes(props?.item?.trangThai?.code)
                &&
                <Grid item md={4} sm={4} xs={12}>
                  <CustomValidatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span className="w-100">
                        <span className="colorRed">* </span>
                        {t("purchasePlaning.approvalDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    name={"ngayDuyet"}
                    value={props?.item?.ngayDuyet}
                    onChange={(date) => props.handleDateChange(date, "ngayDuyet")}
                    readOnly={props.isVisibility}
                    disabled={props?.item?.id && props?.item?.statusIndex?.indexOrder !== appConst.listStatuscalibrationObject.MOI_TAO.indexOrder}
                    minDate={new Date(props?.item?.ngayLap)}
                    disableFuture
                    maxDate={new Date()}
                    maxDateMessage={t(
                      "VerificationCalibration.maxDateMessage"
                    )}
                    minDateMessage={t("VerificationCalibration.minDateCategoryMessage")}
                    InputProps={{
                      readOnly: props.isVisibility,
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onBlur={() =>
                      handleBlurDate(props?.item?.ngayDuyet, "ngayDuyet")
                    }
                    clearable
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                  />
                </Grid>
              }
              {([
                appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder,
                appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder].includes(props?.item?.trangThai?.code))
                && <Grid item md={4} sm={4} xs={12}>
                  <CustomValidatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span className="w-100">
                        {t("Calibration.planDateCost")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    name={"ngayDuyetBaoGia"}
                    value={props?.item?.ngayDuyetBaoGia}
                    onChange={(date) =>
                      props.handleDateChange(date, "ngayDuyetBaoGia")
                    }
                    readOnly={props.isVisibility}
                    minDate={new Date(props?.item?.ngayDuyet)}
                    disableFuture
                    maxDate={new Date()}
                    maxDateMessage={t(
                      "VerificationCalibration.maxDateMessage"
                    )}
                    minDateMessage={t("VerificationCalibration.minDatePriceMessage")}
                    InputProps={{
                      readOnly: props.isVisibility,
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onBlur={() =>
                      handleBlurDate(
                        props?.item?.ngayDuyetBaoGia,
                        "ngayDuyetBaoGia"
                      )
                    }
                    clearable
                    // validators={["required"]}
                    // errorMessages={t("general.required")}
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                  />
                </Grid>}

              {[
                appConst.listStatuscalibrationObject.DA_DUYET_KET_THUC.indexOrder,
                appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA.indexOrder].includes(props?.item?.trangThai?.code)
                && <Grid item md={4} sm={4} xs={12} className="">
                  <TextValidator
                    className={"w-100"}
                    label={<span>{t("Tổng chi phí")}</span>}
                    type="text"
                    name="tongChiPhi"
                    value={props?.item?.tongChiPhi || 0}
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                      inputProps: {
                        style: { textAlign: "right" },
                      },
                      readOnly: true,
                    }}
                  />
                </Grid>
              }

              <Grid item md={4} sm={12} xs={12} className="">
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={<span>{t("Verification.thongTuNghiDinh")}</span>}
                  name="thongTuNghiDinh"
                  value={
                    props.item?.thongTuNghiDinh
                      ? props.item?.thongTuNghiDinh
                      : null
                  }
                  InputProps={{
                    readOnly: props.isVisibility,
                  }}
                  onChange={props?.handleChange}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12} className="">
                {props.isVisibility ? (
                  <TextValidator
                    className={props.className ? props.className : "w-100"}
                    label={t("Calibration.performingDepartment")}
                    value={
                      props.item?.handoverDepartment
                        ? `${props.item?.handoverDepartment?.name}`
                        : null
                    }
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                ) : (
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("Calibration.performingDepartment")}
                      </span>
                    }
                    searchFunction={getListOrgManagementDepartment}
                    searchObject={props.handoverPersonSearchObject}
                    listData={props.item.listHandoverDepartment}
                    setListData={(data) =>
                      props.handleSetDataSelect(data, "listHandoverDepartment")
                    }
                    defaultValue={
                      props.item?.handoverDepartment
                        ? props.item?.handoverDepartment
                        : null
                    }
                    displayLable={"name"}
                    value={
                      props.item?.handoverDepartment
                        ? props.item?.handoverDepartment
                        : null
                    }
                    onSelect={props.selectHandoverDepartment}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    typeReturnFunction="list"
                    showCode={"showCode"}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      let filtered = filterAutocomplete(options, params);
                      return filtered;
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                )}
              </Grid>
              {props?.item?.trangThai ===
                appConst.listStatuscalibrationObject.DA_DUYET_BAO_GIA
                  .indexOrder && (
                  <>
                    <Grid item md={4} sm={12} xs={12} className="">
                      {props.isVisibility ? (
                        <TextValidator
                          className={props.className ? props.className : "w-100"}
                          label={t("Calibration.implementingAgencies")}
                          value={
                            props.item?.receiverDepartment
                              ? props.item?.receiverDepartment?.text
                              : null
                          }
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      ) : (
                        <AsynchronousAutocompleteSub
                          label={
                            <span>
                              <span className="colorRed">* </span>
                              <span>{t("Calibration.implementingAgencies")}</span>
                            </span>
                          }
                          searchFunction={searchReceiverDepartment}
                          searchObject={props.receiverDepartmentSearchObject}
                          listData={props.item.listReceiverDepartment}
                          setListData={(data) =>
                            props.handleSetDataSelect(
                              data,
                              "implementingAgencies"
                            )
                          }
                          defaultValue={
                            props.item.implementingAgencies
                              ? props.item.implementingAgencies
                              : null
                          }
                          displayLable={"text"}
                          value={
                            props.item.implementingAgencies
                              ? props.item.implementingAgencies
                              : null
                          }
                          onSelect={props.selectImplementingAgencies}
                          validators={["required"]}
                          errorMessages={[t("general.required")]}
                          filterOptions={(options, params) => {
                            params.inputValue = params.inputValue.trim();
                            let filtered = filterAutocomplete(options, params);
                            return filtered;
                          }}
                          noOptionsText={t("general.noOption")}
                        />
                      )}
                    </Grid>
                    <Grid item md={4} sm={12} xs={12}>
                      <TextValidator
                        className={props.className ? props.className : "w-100"}
                        label={t("Calibration.implementationCosts")}
                        name="receiverPersonName"
                        onChange={props?.handleChange}
                        value={
                          props.item?.receiverPersonName
                            ? props.item?.receiverPersonName
                            : null
                        }
                        InputProps={{
                          readOnly: props.isVisibility,
                        }}
                      />
                    </Grid>
                  </>
                )}
              <Grid item className="flex-auto">
                <TextValidator
                  className={"w-100"}
                  label={t("Calibration.note")}
                  name="ghiChu"
                  onChange={props?.handleChange}
                  value={props.item?.ghiChu ? props.item?.ghiChu : null}
                  InputProps={{
                    readOnly: props.isVisibility,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} className="mt-10">
              <Grid className="" item md={4} sm={12} xs={12}>
                {(!props.isVisibility) && (
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className="mb-16 w-100"
                    disabled={props?.item?.id && !(props?.item?.statusIndex?.indexOrder === appConst.listStatuscalibrationObject.MOI_TAO.indexOrder)}
                    onClick={props.handleAssetPopupOpen}
                  >
                    {t("general.select_asset")}
                  </Button>
                )}
                {props.item.shouldOpenAssetPopup && (
                  <SelectAssetAllPopup
                    isGetAll={true}
                    open={props.item.shouldOpenAssetPopup}
                    handleSelect={props.handleSelectAssetAll}
                    type={variable.listInputName.isAccreditation}
                    amDexuatIds={props.item?.handoverDepartment?.id}
                    assetVouchers={
                      props.item.assetVouchers != null
                        ? props.item.assetVouchers
                        : []
                    }
                    handleClose={props.handleAssetPopupClose}
                    t={t}
                    i18n={i18n}
                  />
                )}
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <MaterialTable
                  data={
                    props.item.assetVouchers ? props.item.assetVouchers : []
                  }
                  columns={columns}
                  options={{
                    draggable: false,
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    padding: "dense",
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                    }),
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                      paddingLeft: 10,
                      paddingRight: 10,
                      textAlign: "center",
                    },
                    maxBodyHeight: "225px",
                    minBodyHeight: "200px",
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ witdth: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12}>
            {!props?.isVisibility && (
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={props.handleAddAssetDocumentItem}
              >
                {t("AssetFile.addAssetFile")}
              </Button>
            )}
            {props.item.shouldOpenPopupAssetFile && (
              <AssetFilePopup
                open={props.item.shouldOpenPopupAssetFile}
                handleClose={props.handleAssetFilePopupClose}
                itemAssetDocument={props.itemAssetDocument}
                getAssetDocument={props.getAssetDocument}
                handleUpdateAssetDocument={props.handleUpdateAssetDocument}
                documentType={props.item?.documentType}
                item={props.item}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item.documents || []}
              columns={columnsAssetFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                maxBodyHeight: "290px",
                minBodyHeight: "290px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        {shouldOpenAddSupplierDialog &&
          <SupplierDialog
            t={t}
            i18n={i18n}
            open={shouldOpenAddSupplierDialog.open}
            handleSelectDVBH={(data) => props.selectkdDvThucHien(shouldOpenAddSupplierDialog.rowData, { code: data?.code, name: data?.name })}
            handleClose={() => setShouldOpenAddSupplierDialog({ open: false, rowData: {} })}
            title={t("VerificationCalibration.saveUpdate")}
          />}
      </div>
    </form>
  );
}
