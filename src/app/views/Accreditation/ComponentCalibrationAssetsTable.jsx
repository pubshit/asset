import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Card,
  Collapse,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  Link,
  TablePagination,
  TextField,
} from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SearchIcon from "@material-ui/icons/Search";
import { Autocomplete } from "@material-ui/lab";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { appConst, variable } from "app/appConst";
import { filterOptions } from "app/appFunction";
import { ConfirmationDialog } from "egret";
import { MTableToolbar } from "material-table";
import { useEffect } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import CustomMaterialTable from "../CustomMaterialTable";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AccreditationEditorDialog from "./AccreditationEditorDialog";
import AccreditationPrint from "./AccreditationPrint";
import {
  getListManagementDepartment
} from "./AccreditationService";
import ImportExcelDialog from "./ImportExcelDialog";

function ComponentCalibrationAssetsTable(props) {
  let { t, i18n } = props;
  let {
    item,
    page,
    toDate,
    itemList,
    isStatus,
    fromDate,
    statusIndex,
    rowsPerPage,
    isVisibility,
    isHideButton,
    totalElements,
    originalCostTo,
    shouldOpenPrint,
    isRoleAssetUser,
    originalCostFrom,
    allocationStatus,
    hasEditPermission,
    openAdvanceSearch,
    hasPrintPermission,
    handoverDepartment,
    receiverDepartment,
    hasCreatePermission,
    hasDeletePermission,
    listHandoverDepartment,
    listReceiverDepartment,
    shouldOpenEditorDialog,
    shouldOpenImportExcelDialog,
    shouldOpenNotificationPopup,
    shouldOpenConfirmationDialog,
    shouldOpenConfirmationDeleteAllDialog,
    ngayLap,
    ngayDuyetBaoGia,
    ngayDuyet,
    trangThai,
  } = props?.item;
  let isButtonAll = statusIndex === null ? true : false;
  const searchObject = { pageIndex: 1, pageSize: 1000000 };
  const searchObjectDepartment = {
    ...searchObject,
    checkPermissionUserDepartment: false,
  };
  useEffect(() => {
    let value = appConst.listStatusAllocation.find(
      (item) => item.indexOrder === statusIndex
    );
    props.handleSetDataSelect(value, "allocationStatus");
  }, [statusIndex]);

  const handleKeyDown = (e) => {
    if (appConst.decimalNumberExceptThisSymbols.includes(e?.key)) {
      e.preventDefault();
    }
    props.handleKeyDownEnterSearch(e);
  };

  return (
    <Grid container spacing={2} justify="space-between" className="mt-10">
      <Grid item md={6} xs={12} spacing={2}>
        {hasCreatePermission && isButtonAll && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => {
              props.handleEditItem({
                startDate: new Date(),
                endDate: new Date(),
                isCheckReceiverDP: true,
                isCheckHandoverDP: true,
              });
              props.setState({
                isVisibility: false,
                isStatus: false,
                isHideButton: true,
              });
            }}
          >
            {t("general.add")}
          </Button>
        )}
        {/* {!isRoleAssetUser && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.exportToExcel}
          >
            {t("general.exportToExcel")}
          </Button>
        )} */}
        <Button
          className="mb-16 mr-16 align-bottom"
          variant="contained"
          color="primary"
          onClick={props.handleOpenAdvanceSearch}
        >
          {t("general.advancedSearch")}
          <ArrowDropDownIcon />
        </Button>

        {shouldOpenImportExcelDialog && (
          <ImportExcelDialog
            t={t}
            i18n={i18n}
            open={shouldOpenImportExcelDialog}
            handleClose={props?.handleDialogClose}
            handleOKEditClose={props?.handleOKEditClose}
          />
        )}

        {shouldOpenConfirmationDeleteAllDialog && (
          <ConfirmationDialog
            open={shouldOpenConfirmationDeleteAllDialog}
            onConfirmDialogClose={props.handleDialogClose}
            onYesClick={props.handleDeleteAll}
            text={t("general.deleteAllConfirm")}
          />
        )}

        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={props?.handleNotificationPopup}
            text={t("allocation_asset.not_edit")}
            agree={t("general.agree")}
          />
        )}
      </Grid>
      <Grid item md={6} sm={12} xs={12}>
        <FormControl fullWidth>
          <Input
            className="search_box w-100"
            onChange={props.handleTextChange}
            onKeyDown={props.handleKeyDownEnterSearch}
            onKeyUp={props?.handleKeyUp}
            placeholder={t("Calibration.search")}
            id="search_box"
            startAdornment={
              <InputAdornment>
                <Link>
                  {" "}
                  <SearchIcon
                    onClick={() => props.search()}
                    className="searchTable"
                  />
                </Link>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>

      {/* Bộ lọc Tìm kiếm nâng cao */}
      <Grid item xs={12}>
        <Collapse in={openAdvanceSearch}>
          <ValidatorForm>
            <Card elevation={0} className="pt-8 pb-12 pl-16 pr-0">
              <Grid container xs={12} spacing={2}>
                {/* ngayLap */}
                <Grid item xs={12} sm={12} md={2}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("Calibration.planDate")}
                      format="dd/MM/yyyy"
                      value={ngayLap ?? null}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "ngayLap")
                      }
                      KeyboardButtonProps={{ "aria-label": "change date" }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* ngayDuyetBaoGia */}
                <Grid item xs={12} sm={12} md={2}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("Calibration.planDateCost")}
                      format="dd/MM/yyyy"
                      value={ngayDuyetBaoGia ?? null}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "ngayDuyetBaoGia")
                      }
                      KeyboardButtonProps={{ "aria-label": "change date" }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} sm={12} md={2}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("Calibration.approvedDate")}
                      format="dd/MM/yyyy"
                      value={ngayDuyet ?? null}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "ngayDuyet")
                      }
                      KeyboardButtonProps={{ "aria-label": "change date" }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>

                {/* Phòng bàn giao */}
                <Grid item xs={12} sm={12} md={3}>
                  <AsynchronousAutocompleteTransfer
                    label={t("Calibration.performingDepartment")}
                    searchFunction={getListManagementDepartment}
                    searchObject={searchObject}
                    listData={listHandoverDepartment || []}
                    typeReturnFunction="list"
                    setListData={(data) =>
                      props?.handleSetDataSelect(
                        data,
                        "listHandoverDepartment",
                        variable.listInputName.listData
                      )
                    }
                    defaultValue={
                      handoverDepartment ? handoverDepartment : null
                    }
                    displayLable={"text"}
                    value={handoverDepartment ? handoverDepartment : null}
                    onSelect={(data) =>
                      props?.handleSetDataSelect(data, "handoverDepartment")
                    }
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>

                {/* Trạng thái */}
                <Grid item xs={12} sm={12} md={3}>
                  <Autocomplete
                    fullWidth
                    options={appConst.listStatusAllocation}
                    defaultValue={trangThai ? trangThai : null}
                    value={trangThai ? trangThai : null}
                    onChange={(e, value) =>
                      props.handleSetDataSelect(value, "trangThai")
                    }
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterOptions(options, params);
                    }}
                    getOptionLabel={(option) => option.name}
                    disabled={!isButtonAll}
                    noOptionsText={t("general.noOption")}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        value={allocationStatus?.name || ""}
                        label={t("maintainRequest.status")}
                      />
                    )}
                  />
                </Grid>
                {/* Button xuất excel
                                    <Grid item container xs={12} sm={12} md={3} justifyContent="flex-end">
                                        <Grid item>
                                            
                                        </Grid>
                                    </Grid> */}
              </Grid>
            </Card>
          </ValidatorForm>
        </Collapse>
      </Grid>

      <Grid item xs={12}>
        <div>
          {shouldOpenEditorDialog && (
            <AccreditationEditorDialog
              t={t}
              i18n={i18n}
              open={shouldOpenEditorDialog}
              handleClose={props.handleDialogClose}
              handleOKEditClose={props.handleOKEditClose}
              item={props?.item?.item}
              isVisibility={isVisibility}
              isStatus={isStatus}
              isAllocation={false}
              isHideButton={isHideButton}
              exportToExcel={props.exportToExcel}
            />
          )}

          {shouldOpenPrint && (
            <AccreditationPrint
              t={t}
              i18n={i18n}
              open={shouldOpenPrint}
              handleClose={props.handleDialogClose}
              handleOKEditClose={props.handleOKEditClose}
              item={item}
            />
          )}

          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={props.handleDialogClose}
              onYesClick={props.handleConfirmationResponse}
              text={t("general.cancel_plan")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}
        </div>
        <CustomMaterialTable
          title={t("general.list")}
          data={itemList}
          onRowClick={(e, rowData) => {
            return props?.setItemState(rowData);
          }}
          columns={
            hasDeletePermission || hasEditPermission || hasPrintPermission
              ? props?.columns
              : props?.columnsNoAction
          }
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
          }}
          options={{
            draggable: false,
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            sorting: false,
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
            }),
            maxBodyHeight: "490px",
            minBodyHeight: "260px",
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
              paddingLeft: 10,
              paddingRight: 10,
              textAlign: "center",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
        <TablePagination
          align="left"
          className="px-16"
          rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
          component="div"
          labelRowsPerPage={t("general.rows_per_page")}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} ${t("general.of")} ${
              count !== -1 ? count : `more than ${to}`
            }`
          }
          count={totalElements}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={props.handleChangePage}
          onChangeRowsPerPage={props.setRowsPerPage}
        />
      </Grid>
    </Grid>
  );
}

export default ComponentCalibrationAssetsTable;
