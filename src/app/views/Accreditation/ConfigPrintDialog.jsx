import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  TextField
} from "@material-ui/core";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import {
  searchByPage,
} from "./AssetAllocationService";
import moment from "moment";
import AssetAllocationPrint from "../AssetAllocation/AssetAllocationPrint"


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

class BDSListDiaglog extends Component {
  state = {
    name: "",
    code: "",
    listCategory: [],
    category: [],
    description: "",
    totalElements: 0,
    rowsPerPage: 10,
    page: 0,
    signatureId: "",
    keyword: '',
    shouldOpenConfirmationAssign: false,
    shouldOpenShowStatusDialog: false,
    agencyId: '',
    BDStaffId: '',
    date: new Date(),
    listSign: [],
    unit: "", part: "", MST: "", numberExample: "", nameExport: "",
    lineOne: "", lineTwo: "", lineThree: "", signName: "", isFormal: false
  };


  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  // handleFormSubmit = () => {
  //   let { id } = this.state;
  //   if (id) {
  //     updateData({
  //       ...this.state
  //     }).then(() => {
  //       this.props.handleOKEditClose1();
  //     });
  //   } else {
  //     addNewData({
  //       ...this.state
  //     }).then(() => {
  //       this.props.handleOKEditClose1();
  //     });
  //   }
  // };

  selectCategory = (categorySelected) => {
    this.setState({ category: categorySelected }, function () {
    });
  }

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData()
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    })
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }


  handleTextChange = (event) => {
    this.setState({ [event.target.name]: event.target.value }, function () { })
  }

  handleChangeFormal = (event) => {
    this.setState({ ...this.state, [event.target.name]: event.target.checked });
  }

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenExportDialog: false,
        shouldOpenConfirmationAssign: false,
      },
    )
  }
  handleOKEditClose = () => {
    this.setState({
      shouldOpenExportDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };


  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      this.setState({
        listSign: [...data.content]
      })
    });
  };


  exportExcel = () => {

    this.setState({ loading: true })
    var searchObject = {};
    searchObject.id = this.props.id
    searchObject.dateExport = moment(this.state.date).format('DD/MM/YYYY');
    searchObject.lineOne = this.state.lineOne
    searchObject.lineTwo = this.state.lineTwo
    searchObject.lineThree = this.state.lineThree
    searchObject.signName = this.state.signName
    searchObject.isFormal = this.state.isFormal
    searchObject.fromUploadedFile = this.state.fromUploadedFile
    // getExcel(searchObject).then((result) => {
    //   const url = window.URL.createObjectURL(new Blob([result.data]))
    //   const link = document.createElement('a')
    //   link.href = url
    //   link.setAttribute('download', this.props.code + ".xls")
    //   document.body.appendChild(link)
    //   link.click();
    //   this.setState({ loading: false })
    // })
    this.props.handleClose()
  }

  getFileExport = () => {
    this.setState({ loading: true })
    var searchObject = {}
    searchObject.id = this.props.id

    this.setState({ shouldOpenExportDialog: true, isPrint: true })
  }




  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {}
      searchObject.keyword = this.state.keyword
      searchObject.pageIndex = this.state.page + 1
      searchObject.pageSize = this.state.rowsPerPage
      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      });
    })
  }

  componentWillMount() {
    this.setState({
      ...this.props.item,
    })


    console.log(this.state)

    // this.updatePageData();
    this.setState({
      listSign: [
        {
          "lineOne": "TL. HIỆU TRƯỞNG", "lineTwo": "KT.TRƯỞNG PHÒNG ĐÀO TẠO",
          "lineThree": "PHÓ TRƯỞNG PHÒNG", "signName": "TS. Nguyễn Đình Dũng"
        },
        {
          "lineOne": "KT. HIỆU TRƯỞNG", "lineTwo": "PHÓ HIỆU TRƯỞNG",
          "lineThree": "", "signName": "TS. Đỗ Đình Cường"
        },
        {
          "lineOne": "HIỆU TRƯỞNG", "lineTwo": "",
          "lineThree": "", "signName": "GS. TS. Nguyễn Văn A"
        },
        {
          "lineOne": "TL. HIỆU TRƯỞNG", "lineTwo": "TRƯỞNG PHÒNG ĐÀO TẠO",
          "lineThree": "", "signName": "PGS. TS. Phùng Trung Nghĩa"
        }
      ]
    })
  }

  handleDateChange(date) {
    this.setState({ date: date })
  }


  componentDidMount() {

  }

  render() {
    let { unit, part, MST, numberExample, nameExport, shouldOpenExportDialog, dateExport
    } = this.state;
    let { open, handleClose,  t, i18n } = this.props;

    return (
      <Dialog open={open} onClose={handleClose} PaperComponent={PaperComponent} maxWidth="md" fullWidth={true}>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Cấu Hình Export
        </DialogTitle>
        <DialogContent >
          <Grid className="mb-10" container style={{ display: 'flex' }} spacing={2}>
            <Grid style={{ flexGrow: 3, marginRight: '20px' }}>
              <Grid item md={12} sm={12} xs={12}>

                <TextField
                  label="Đơn vị:"
                  className=" w-100 mb-16 mr-20"
                  type="text"
                  name="unit"
                  InputLabelProps={{ shrink: true }}
                  value={unit}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                />  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextField
                  label="Bộ phận:"
                  className="w-100 mb-16 mr-20"
                  type="text"
                  name="part"
                  InputLabelProps={{ shrink: true }}
                  value={part}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                /></Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextField
                  label="MST:"
                  className="w-100 mb-16 mr-20"
                  type="text"
                  name="MST"
                  InputLabelProps={{ shrink: true }}
                  value={MST}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                /></Grid>
            </Grid>
            <Grid style={{ flexGrow: 3 }}>
              <Grid item md={12} sm={12} xs={12}>
                <TextField
                  label="Mẫu số:"
                  className="w-100 mb-16 mr-20"
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  name="numberExample"
                  value={numberExample}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextField
                  label="Tên báo cáo:"
                  className="w-100 mb-16 mr-20"
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  name="nameExport"
                  value={nameExport}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextField
                  label="Ngày tháng năm:"
                  className="w-100 mb-16 mr-20"
                  type="date"
                  InputLabelProps={{ shrink: true }}
                  name="dateExport"
                  value={dateExport}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onChange={this.handleTextChange}
                />
              </Grid>
            </Grid>

          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle mt-36">

            <Button
              variant="contained"
              color="primary"
              className="mr-36"
              onClick={() => this.getFileExport()}>
              In
            </Button>
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={() => this.props.handleClose()}>
              {t('general.cancel')}
            </Button>

          </div>
        </DialogActions>


        {shouldOpenExportDialog && (
          <AssetAllocationPrint
            t={t}
            i18n={i18n}
            handleClose={this.handleDialogClose}
            open={this.state.isPrint}
            productName={this.props.productName}
            config={this.state}
            handleOKEditClose={this.handleOKEditClose}
            item={this.props.item}

          />
        )}
      </Dialog >
    )
  }
}

export default BDSListDiaglog;