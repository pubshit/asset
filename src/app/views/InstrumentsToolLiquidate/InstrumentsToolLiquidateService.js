import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_liquidation_voucher = ConstantList.API_ENPOINT + "/api/v1/instruments-and-tools/liquidate-vouchers";
const API_PATH_person = ConstantList.API_ENPOINT + "/api/user_department";
const API_PATH_person_org = ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department = ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORT_TO_EXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel"
const API_GET_USER_DEPARTMENT_V1 = ConstantList.API_ENPOINT + "/api/v1/user-departments";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
export const deleteItem = id => {
  return axios.delete(API_PATH_liquidation_voucher + "/" + id);
};
export const deleteList = list => {
  return axios.delete(API_PATH_liquidation_voucher + `?ids=${list}`);
};
export const getItemById = id => {
  return axios.get(API_PATH_liquidation_voucher + "/" + id);
};
export const addNewOrUpdate = asset => {
  return asset.id ?
    axios.put(API_PATH_liquidation_voucher + "/" + asset.id, asset) :
    axios.post(API_PATH_liquidation_voucher, asset);
};
export const getByPage = (searchObject) => {
  let url = API_PATH_liquidation_voucher + `/page?`;
  for (const [key, value] of Object.entries(searchObject)) {
    if (value) {
      url += `${key}=${value}&`;
    }
  }
  return axios.get(url);
};
export const getCountStatus = () => {
  return axios.get(API_PATH_liquidation_voucher + "/count-by-statuses");
};
export const getNewCode = () => {
  return axios.get(API_PATH_liquidation_voucher + "/new-code");
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: 'post',
    url: API_PATH_EXPORT_TO_EXCEL + "/iat-liquidation",
    data: searchObject,
    responseType: 'blob',
  });
};

export const userDepartmentSearchByPage = (searchObject) => {
  return axios.post(API_PATH_person + "/searchByPage", searchObject);
};
export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};
export const personSearchByPage = (searchObject) => {
  return axios.post(API_PATH_person_org + "/searchByPageWithUserDepartment", searchObject);
};
export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const getAllUserDepartment = ({ departmentId, keyword, pageIndex, pageSize }) => {
  let config = {
    params: {
      departmentId,
      keyword,
      pageIndex,
      pageSize,
      isNotAdminUser: true,
      isMainDepartment: true
    }
  };
  return axios.get(API_GET_USER_DEPARTMENT_V1 + "/page", config);
};
export const searchByPageListUserDepartment = (SeachObjectct) => {
  let url = API_PATH_ASSET_DEPARTMENT + "/searchByPage";
  return axios.post(url, SeachObjectct);
};