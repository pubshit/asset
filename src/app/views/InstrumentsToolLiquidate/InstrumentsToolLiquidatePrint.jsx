/* eslint-disable no-unused-expressions */
import React from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { convertNumberPrice } from "app/appFunction";
import localStorageService from "app/services/localStorageService";

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center"
  },
  title: {
    fontSize: '0.975rem',
    fontWeight: 'bold',
    marginBottom: '0px'
  },
  text: {
    fontSize: '0.8rem',
    fontWeight: 'bold',
    marginTop: '0px'
  },
  textAlignLeft: {
    textAlign: "left",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: 'bold'
  },
  table: {
    width: '100%',
    border: '1px solid',
    borderCollapse: 'collapse'
  },
  w_9: {
    border: '1px solid',
    width: '9%',
    textAlign: 'center'
  },
  w_12: {
    border: '1px solid',
    width: '12%',
    textAlign: 'center'
  },
  w_90: {
    border: '1px solid',
    width: '90px',
    textAlign: 'center'
  },
  w_30px: {
    border: '1px solid',
    width: '30px',
    textAlign: 'center'
  },
  w_30: {
    border: '1px solid',
    width: '30%',
  },
  border: {
    border: '1px solid',
    paddingLeft: "5px"
  },
  name: {
    border: '1px solid',
    paddingLeft: "5px",
    textAlign: 'left'
  },
  sum: {
    border: '1px solid',
    paddingLeft: "5px",
    fontWeight: 'bold',
    textAlign: 'left'
  },
  represent: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 12%'
  }
};

export default function InstrumentsToolTransferToAnotherUnitPrint(props) {
  let { open, handleClose, t, item } = props;
  let newDate = new Date(item?.issueDate);
  let currentUser = localStorageService.getSessionItem("currentUser");

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write(content.innerHTML);
    pri.document.close();
    pri.focus();
    pri.print();
  };

  const sumResult = () => {
    let sum = 0;
    item.assetVouchers?.length > 0 ? item.assetVouchers?.map(item => sum += Number(Math.ceil(item?.asset?.originalCost))) : 0; //no-unused-expressions
    return convertNumberPrice(sum);
  };
  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">{t('Phiếu thanh lý')}</span>
      </DialogTitle>
      <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute', print: { size: 'auto', margin: '0mm' } }}></iframe>

      <ValidatorForm onSubmit={handleFormSubmit}>
        <DialogContent id='divcontents'>
          <Grid>
            <div style={style.textAlignCenter}>
              <div style={{ display: 'flex' }}>
                <div style={{ flexGrow: 2 }}>
                  <p style={{ ...style.title }}>{currentUser?.org?.name}</p>
                  <p style={style.text}>Số phiếu: {item?.code}</p>
                </div>
                <div style={{ flexGrow: 2 }}>
                  <p style={style.title} >CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
                  <p style={style.text}>Độc lập - Tự do - Hạnh phúc</p>
                </div>
              </div>

              <div>
                <p style={style.title}>BIÊN BẢN THANH LÝ THIẾT BỊ</p>
                {item?.issueDate 
                ? <p style={style.text}>
                    Hà nội, ngày {newDate.getDate()} tháng {newDate.getMonth() + 1} năm {newDate.getFullYear()}
                  </p>
                : <p> Hà nội, ngày....tháng....năm......</p>
                }
              </div>

              <div style={style.textAlignLeft}>
                <p style={style.forwarder}>BÊN GIAO:</p>
                <ul mr-4>
                  <li><p>Phòng bàn giao: {item?.handoverDepartment?.name}</p> </li>
                  <li><p>Người bàn giao: {item?.handoverPerson?.displayName}</p>  </li>
                </ul>
              </div>

              <div style={style.textAlignLeft}>
                <p style={style.forwarder}>BÊN NHẬN:</p>
                <ul mr-4>
                  <li> <p>Địa chỉ thanh lý: {item?.transferAddress}</p> </li>
                </ul>
              </div>

              <div>
                {
                  <table style={style.table} className="table-report">
                    <tr>
                      <th style={style.w_30px} rowSpan="1">STT</th>
                      <th style={style.w_30} rowSpan="1">Tên, ký hiệu, quy cách, cấp hạng CCDC</th>
                      <th style={style.w_90} rowSpan="1">Số hiệu CCDC</th>
                      <th style={style.w_9} rowSpan="1">Nước sản xuất</th>
                      <th style={style.w_9} rowSpan="1">Năm sản xuất</th>
                      <th style={style.w_9} rowSpan="1">Năm SD</th>
                      <th style={style.w_12} rowSpan="1">Nguyên giá (VNĐ)</th>
                      <th style={style.w_9} rowSpan="1">Số lượng</th>
                      <th style={style.border} rowSpan="1">Ghi chú</th>
                    </tr>

                    {(item.assetVouchers?.length > 0) ? item.assetVouchers.map((row, index) => {
                      return (
                        <tbody>
                          <tr>
                            <td style={style.border}>{index !== null ? (index + 1) : ''}</td>
                            <td style={style.name}>{row?.asset?.name}</td>
                            <td style={style.border}>{row?.asset?.code}</td>
                            <td style={style.border}>{row?.asset?.madeIn}</td>
                            <td style={style.border}>{row?.asset?.yearOfManufacture}</td>
                            <td style={style.border}>{row?.asset?.yearPutIntoUse}</td>
                            <td style={style.border}>{convertNumberPrice(Math.ceil(row?.asset?.originalCost))}</td>
                            <td style={style.border}>{row?.quantity}</td>
                            <td style={style.border}>{row?.note}</td>
                          </tr>
                        </tbody>
                      );
                    }) : ''}
                    <tr>
                      <td style={style.border}></td>
                      <td style={style.sum}>Tổng cộng:</td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}></td>
                      <td style={style.border}>{sumResult()}</td>
                      <td style={style.border}></td>
                    </tr>
                  </table>
                }

              </div>
              <div style={style.represent}>
                <div><p>ĐẠI DIỆN BÊN GIAO</p></div>
                <div><p>ĐẠI DIỆN BÊN NHẬN</p></div>
              </div>
            </div>
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-16"
              onClick={() => handleClose()}
            >
              {t('general.cancel')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
            >
              {t('In')}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

