import ConstantList from "../../appConfig";
import React from "react";
import {
  IconButton,
  Icon,
  Tabs, Tab, AppBar, Box, Typography, Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  getCountStatus,
  getByPage, deleteList, getNewCode, exportToExcel,
} from "./InstrumentsToolLiquidateService";
import { Breadcrumb } from "egret";
import { useTranslation } from 'react-i18next';
import { Helmet } from 'react-helmet';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import moment from "moment";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst, variable } from "../../appConst";
import ComponentLiquidateTable from "./ComponentLiquidateTable";
import AppContext from "../../appContext";
import {
  LightTooltip,
  TabPanel,
  checkInvalidDate,
  convertFromToDate,
  convertNumberPriceRoundUp,
  formatDateDto,
  formatDateDtoMore,
  formatDateTypeArray, handleKeyDown, handleKeyUp
} from "../../appFunction";
import FileSaver from "file-saver";

toast.configure({
  autoClose: 8000,
  draggable: false,
  limit: 3
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;

  return (
    <div className="none_wrap">
      {appConst.listLiquidateStatus[2].code !== item.liquidationStatus && <>
        <LightTooltip
          className="p-5"
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
        <LightTooltip
          className="p-5"
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      </>
      }
      <LightTooltip
        className="p-5"
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.print)}>
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      {appConst.listLiquidateStatus[2].code === item.liquidationStatus &&
        <LightTooltip
          className="p-5"
          title={t("general.viewIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
            <Icon fontSize="small" color="primary">
              visibility
            </Icon>
          </IconButton>
        </LightTooltip>}
    </div>
  );
}

class InstrumentsToolLiquidateTable extends React.Component {
  state = {
    keyword: '',
    createDateBottom: null,
    createDateTop: null,
    issueDateTop: null,
    issueDateBottom: null,
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetLiquidate: [],
    itemList: [],
    item: {},
    checkAll: false,
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    checkDelete: false,
    isRoleManager: false,
    tabValue: 0,
    statusLiquidation: appConst.tabLiquidate.tabAll,
    statuses: [],
    assetVouchers: [],
    liquidationStatus: null
  };
  voucherType = ConstantList.VOUCHER_TYPE.Liquidate;

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      AssetLiquidate: [],
      checkAll: false,
      tabValue: newValue,
      keyword: '',
      createDateBottom: null,
      createDateTop: null,
      issueDateTop: null,
      issueDateBottom: null,
    });
    if (appConst?.tabLiquidate?.tabAll === newValue) {
      this.setState({
        statusLiquidation: appConst.tabLiquidate.tabAll,
        statuses: [],
        liquidationStatus: null
      }, () => this.updatePageData());
    }
    if (appConst?.tabLiquidate?.tabPlan === newValue) {
      this.setState({
        statusLiquidation: appConst.tabLiquidate.tabPlan,
        statuses: [appConst.listLiquidateStatus[0].code],
        liquidationStatus: appConst.listLiquidateStatus[0],
      }, () => this.updatePageData());
    }
    if (appConst?.tabLiquidate?.tabIsLiquiDating === newValue) {
      this.setState({
        statusLiquidation: appConst.tabLiquidate.tabIsLiquiDating,
        statuses: [appConst.listLiquidateStatus[1].code],
        liquidationStatus: appConst.listLiquidateStatus[1],
      }, () => this.updatePageData());
    }
    if (appConst?.tabLiquidate?.tabLiquidated === newValue) {
      this.setState({
        statusLiquidation: appConst.tabLiquidate.tabLiquidated,
        statuses: [appConst.listLiquidateStatus[2].code],
        liquidationStatus: appConst.listLiquidateStatus[2],
      }, () => this.updatePageData());
    }
  };

  checkCount = (count) => {
    return (count ? (count > 99 ? '99+' : count) : 0);
  };

  getAllStatusCount = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countStatusPlan, countStatusIsLiquidating, countStatusIsLiquidated;
        let foundStatusPlan = data?.data?.find(item => appConst?.listLiquidateStatus[0].code === item?.status);
        let foundStatusIsLiquidating = data?.data?.find(item => appConst?.listLiquidateStatus[1].code === item?.status);
        let foundStatusIsLiquidated = data?.data?.find(item => appConst?.listLiquidateStatus[2].code === item?.status);

        if (foundStatusPlan) {
          countStatusPlan = this.checkCount(foundStatusPlan?.count);
        }
        if (foundStatusIsLiquidating) {
          countStatusIsLiquidating = this.checkCount(foundStatusIsLiquidating?.count);
        }
        if (foundStatusIsLiquidated) {
          countStatusIsLiquidated = this.checkCount(foundStatusIsLiquidated?.count);
        }
        this.setState({ countStatusPlan, countStatusIsLiquidating, countStatusIsLiquidated },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleTextChange = event => {
    this.setState({ tempKeyword: event.target.value });
  };

  handleDateChange = (event, source) => {
    let newDate = variable.listInputName.createDateBottom === source ?
      formatDateDtoMore(event, "start") : formatDateDtoMore(event, "end");
    this.setState({ [source]: newDate }, () => {
      !checkInvalidDate(event) && this.validationFilter() && this.updatePageData();
    });
  };

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  handleKeyUpSearch = e => handleKeyUp(e, this.search);

  setPage = page => {
    this.setState({ page, item: null }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setState({ keyword: this.state.tempKeyword }, () => {
      this.setPage(0);
    })
  }

  searchFilter = () => {
    this.validationFilter() && this.setPage(0);
  };
  validationFilter = () => {
    let { createDateBottom, createDateTop, issueDateBottom, issueDateTop } = this.state;
    if (
      checkInvalidDate(createDateBottom)
      || checkInvalidDate(createDateTop)
      || checkInvalidDate(issueDateBottom)
      || checkInvalidDate(issueDateTop)
    ) {
      return false;
    }
    if (createDateTop && createDateBottom && createDateBottom > createDateTop) {
      return false;
    }
    if (issueDateTop && issueDateBottom && issueDateBottom > issueDateTop) {
      return false;
    }
    return true;
  };

  resetFilter = () => {
    this.setState({
      createDateBottom: null,
      createDateTop: null,
      issueDateTop: null,
      issueDateBottom: null,
      statuses: [],
    });
  };

  handleFilteringSelectChange = (value) => {
    this.setState({
      statuses: [value?.code],
      liquidationStatus: value
    }, () => {
      this.updatePageData();
    });
  };

  updatePageData = () => {
    const { setPageLoading } = this.context;
    const { AssetLiquidate } = this.state;
    const { t } = this.props;
    const { page, rowsPerPage, keyword, statuses, createDateTop, createDateBottom, issueDateTop, issueDateBottom } = this.state;
    const searchObject = {
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      type: this.voucherType,
      keyword,
      statuses: statuses?.length > 0 ? statuses : null,
      createDateTop: convertFromToDate(createDateTop).toDate,
      createDateBottom: convertFromToDate(createDateBottom).fromDate,
      issueDateTop: convertFromToDate(issueDateTop).toDate,
      issueDateBottom: convertFromToDate(issueDateBottom).fromDate,
    };
    setPageLoading(true);
    getByPage(searchObject)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code) {
          let countItemChecked = 0
          const itemList = data?.data?.content?.map(item => {
            item.createDate = formatDateTypeArray(item?.createDate);
            if (AssetLiquidate?.find(checkedItem => checkedItem === item.id)) {
              countItemChecked++
            }
            return {
              ...item,
              checked: !!AssetLiquidate?.find(checkedItem => checkedItem === item.id),
            }
          }
          )

          this.setState({
            selectedItem: null,
            itemList,
            totalElements: data?.data?.totalElements,
            ids: [],
            checkAll: itemList?.length === countItemChecked && itemList?.length > 0
          })
        } else {
          toast.warning(data.message);
        }
        setPageLoading(false);
      })
      .catch(err => {
        toast.error(t("asset_liquidate.noti.dataError"));
        setPageLoading(false);
      }
      );
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      isPrint: false
    });
    this.getAllStatusCount();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      assetVouchers: []
    });
    this.updatePageData();
    this.getAllStatusCount();
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    deleteItem(this.state.id).then(({ data }) => {
      if (appConst.CODE.SUCCESS === data.code) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
      }
      else {
        toast.warning(data.message);
      }
      this.handleDialogClose();
    }).catch(error => {
      toast(t("general.error"));
    });
  };

  componentDidMount() {
    this.updatePageData();
    this.getAllStatusCount();
    this.getRoleCurrentUser();
  }

  getRoleCurrentUser = () => {
    let { isRoleManager } = this.state;
    let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    let isRoleAdmin = currentUser?.roles?.some((role) => role.name === ConstantList.ROLES.ROLE_ORG_ADMIN || role.name === ConstantList.ROLES.ROLE_ADMIN);
    if (isRoleAdmin) {
      return;
    }
    isRoleManager = currentUser?.roles?.some((role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER);

    this.setState({
      isRoleManager,
      currentUser
    });
  };

  handleEditItem = item => {
    getNewCode()
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenEditorDialog: true,
          });
        }
      })
      .catch(() => {
        alert(this.props.t("general.error_reload_page"));
      });
  };

  handleCheck = (event, item) => {
    let { AssetLiquidate, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        AssetLiquidate.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = AssetLiquidate.filter(item => !itemListIds.includes(item));

        AssetLiquidate = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, AssetLiquidate });
    }
    else {
      let existItem = AssetLiquidate.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        AssetLiquidate.push(event.target.name) :
        AssetLiquidate.map((item, index) =>
          item === existItem ?
            AssetLiquidate.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: AssetLiquidate,
        AssetLiquidate,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  handleSelectAllClick = (event) => {
    let { AssetLiquidate, itemList } = this.state;

    itemList.map(item => {
      item.checked = event.target.checked;
      AssetLiquidate.push(item.id);
      return item;
    });
    if (!event.target.checked) {
      AssetLiquidate = [];
    }
    this.setState({ checkAll: event.target.checked, AssetLiquidate: AssetLiquidate });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEdit = id => {
    getItemById(id).then(({ data }) => {
      let liquidate = data?.data ? this.formatDataDto(data?.data) : null;
      this.setState({
        checkDelete: true,
        item: liquidate,
        shouldOpenEditorDialog: true
      });
    }).catch(err => {
      toast.error("Xảy ra lỗi, không thể lấy thông tin phiếu");
    });
  };

  handleView = id => {
    getItemById(id).then(({ data }) => {
      let liquidate = data?.data ? this.formatDataDto(data?.data) : null;
      liquidate.isView = true;
      this.setState({
        item: liquidate,
        shouldOpenEditorDialog: true
      });
    });
  };

  handlePrint = id => {
    getItemById(id).then(({ data }) => {
      let liquidate = data?.data ? this.formatDataDto(data?.data) : null;
      this.setState({
        item: liquidate,
        isPrint: true
      });
    });
  };

  handleDeleteAllItem = id => {
    let { AssetLiquidate } = this.state;
    let { t } = this.props;
    if (AssetLiquidate.length === 0) {
      toast.warning(t("general.emptyDataSelected"));
    }
    else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };

  async handleDeleteList(list) {
    let { t } = this.props;
    let errList = [];
    deleteList(list).then(({ data }) => {
      if (appConst.CODE.SUCCESS === data.code) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
      }
      else { toast.warning(data.message); }
      this.getAllStatusCount();
    }).catch(err => errList.push(err));
    if (errList.length !== 0) {
      toast.error(t("asset_liquidate.noti.deleteAllError"));
    }

    this.handleDialogClose();
  }

  handleDeleteAll = () => {
    let { AssetLiquidate } = this.state;
    this.handleDeleteList(AssetLiquidate);

  };

  formatDataDto = (liquidate) => {
    let personIds = [];
    let listAssetDocumentId = [];

    liquidate.createDate = formatDateTypeArray(liquidate?.createDate);

    liquidate.handoverPerson = {
      displayName: liquidate?.handoverPersonName,
    }
    liquidate.handoverDepartment = {
      id: liquidate?.handoverDepartmentId,
      code: liquidate?.handoverDepartmentCode,
      name: liquidate?.handoverDepartmentName,
    };

    liquidate.handoverPerson = {
      handoverPersonId: liquidate?.handoverPersonId,
      personDisplayName: liquidate?.handoverPersonName,
    };

    liquidate.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse,
        originalCost: item?.assetOriginalCost,
        carryingAmount: item?.assetCarryingAmount,
        managementCode: item?.assetManagementCode,
        yearOfManufacture: item?.assetYearOfManufacture,
        madeIn: item?.assetMadeIn,
        quantity: item?.quantity,
      };
      item.unitPrice = (item?.assetOriginalCost / item?.quantity) || 0;
      item.asset = data;
      return item;
    });

    liquidate?.persons?.map((item) => personIds?.push(item?.id));
    liquidate.personIds = personIds;

    liquidate?.documents?.map((item) => listAssetDocumentId?.push(item?.id));
    liquidate.listAssetDocumentId = listAssetDocumentId;

    return liquidate;
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listLiquidateStatus.find(item => item.code === status);
    switch (status) {
      case appConst.listLiquidateStatus[0].code:
        return (
          <span className="status status-success">
            {appConst.listLiquidateStatus[0].name}
          </span>
        );
      case appConst.listLiquidateStatus[1].code:
        return (
          <span className="status status-warning">
            {appConst.listLiquidateStatus[1].name}
          </span>
        );
      case appConst.listLiquidateStatus[2].code:
        return (
          <span className="status status-error">
            {appConst.listLiquidateStatus[2].name}
          </span>
        );

      default:
        break;
    }
    return itemStatus.name;
  };

  setAssetVouchersTable = (rowData = {}) => {
    let assetVouchers = this.formatDataDto(rowData)?.assetVouchers ?? [];
    this.setState({ assetVouchers, selectedItem: rowData })
  };

  handleExportToExcel = async () => {
    const { setPageLoading } = this.context;
    setPageLoading(true);
    const { t } = this.props;
    try {
      let {
        issueDateBottom,
        issueDateTop,
        liquidationStatus,
        createDateTop,
        createDateBottom
      } = this.state
      let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
      let searchObject = {}
      searchObject.fromDate = issueDateBottom ? convertFromToDate(new Date(issueDateBottom)).fromDate : null;
      searchObject.toDate = issueDateTop;
      searchObject.orgId = currentUser?.org?.id;
      searchObject.assetClass = appConst.assetClass.CCDC;
      searchObject.type = ConstantList.VOUCHER_TYPE.Liquidate;
      searchObject.liquidationStatus = liquidationStatus?.indexOrder;
      searchObject.ids = this.state?.AssetLiquidate;
      // searchObject.createDateTop = formatDateDtoMore(createDateTop)
      // searchObject.createDateBottom = formatDateDtoMore(createDateBottom, "start")

      const res = await exportToExcel(searchObject);
      try {
        // Kiểm tra nếu data là JSON
        const data = JSON.parse(res?.data);
        toast.warning(data.message);
      } catch (e) {
        // Nếu call api thành công data ở dạng blob chỉ có thể lưu
        if (res?.data) {
          const blob = new Blob([res.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(blob, "InstrumentsToolLiquidate.xlsx");
          toast.success(t("general.successExport"));
        } else {
          toast.error(t("general.errorExport"));
        }
      } finally {
        setPageLoading(false);
      }
    } catch (error) {
      toast.error(t("general.errorExport"));
    }
  }
  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      checkAll,
      tabValue,
    } = this.state;
    let TitlePage = t("InstrumentToolsType.liquidate");
    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        field: "custom",
        align: "left",
        maxWidth: 60,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: rowData => <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            switch (method) {
              case appConst.active.edit:
                this.handleEdit(rowData.id);
                break;
              case appConst.active.delete:
                this.handleDelete(rowData.id);
                break;
              case appConst.active.view:
                this.handleView(rowData.id);
                break;
              case appConst.active.print:
                this.handlePrint(rowData.id);
                break;
              default:
                alert('Call Selected Here: ' + rowData.id);
                break;
            }
          }}
        />
      },
      {
        title: t("general.index"),
        field: "",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: rowData => ((page) * rowsPerPage) + (rowData.tableData.id + 1)
      },
      {
        title: t("asset_liquidate.createDate"),
        field: "createDate",
        align: "left",
        minWidth: 100,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: rowData => rowData.createDate
          ? moment(rowData.createDate).format('DD/MM/YYYY')
          : ''
      },
      {
        title: t("TransferToAnotherUnit.status"),
        field: "code",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData.liquidationStatus)
      },
      {
        title: t("asset_liquidate.liquidateNumber"),
        field: "code",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("asset_liquidate.liquidationDate"),
        field: "issueDate",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: rowData =>
          (rowData.issueDate) ?
            moment(rowData.issueDate).format('DD/MM/YYYY') : ''
      },
      {
        title: t("asset_liquidate.name"),
        field: "name",
        align: "left",
        minWidth: 230,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("asset_liquidate.handoverDepartment"),
        field: "handoverDepartmentName",
        align: "left",
        minWidth: 200,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    let AssetColumns = [
      {
        title: t("Asset.stt"),
        field: "asset.code",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("InstrumentToolsList.code"),
        field: "asset.code",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.name"),
        field: "asset.name",
        align: "left",
        minWidth: 250,
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.model"),
        field: "assetModel",
        align: "left",
        minWidth: 130,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "assetSerialNumber",
        align: "left",
        minWidth: 130,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "assetYearPutIntoUse",
        align: "left",
        minWidth: 90,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.yearOfManufacture"),
        field: "assetYearOfManufacture",
        align: "left",
        minWidth: 90,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturerTable"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "center"
        }
      },
      {
        title: t("InstrumentToolsList.quantity"),
        field: "asset.quantity",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.asset?.originalCost
          ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
          : 0
      },
      {
        title: t("Asset.useDepartment"),
        field: "assetUseDepartmentName",
        align: "left",
        minWidth: 150,
        cellStyle: {
        },
      },
      {
        title: t("Asset.store"),
        field: "assetStoreName",
        align: "left",
        minWidth: 120,
        cellStyle: {
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.toolsManagement"), path: "/instruments-and-tools/liquidate-vouchers" },
            { name: t('InstrumentToolsType.liquidate') }]} />
        </div>

        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("asset_liquidate.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.plan")}</span>
                  <div className="tabQuantity tabQuantity-success">{this.state?.countStatusPlan || 0}</div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.isLiquidating")}</span>
                  <div className="tabQuantity tabQuantity-warning">{this.state?.countStatusIsLiquidating || 0}</div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.liquidated")}</span>
                  <div className="tabQuantity tabQuantity-error">{this.state?.countStatusIsLiquidated || 0}</div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel className="mp-0" value={tabValue} index={appConst?.tabLiquidate?.tabAll} >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            AssetColumns={AssetColumns}
            setAssetVouchersTable={this.setAssetVouchersTable}
            handleExportToExcel={this.handleExportToExcel}
          />
        </TabPanel>
        <TabPanel className="mp-0" value={tabValue} index={appConst?.tabLiquidate?.tabPlan} >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            AssetColumns={AssetColumns}
            setAssetVouchersTable={this.setAssetVouchersTable}
            handleExportToExcel={this.handleExportToExcel}
          />
        </TabPanel>
        <TabPanel className="mp-0" value={tabValue} index={appConst?.tabLiquidate?.tabIsLiquiDating} >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            AssetColumns={AssetColumns}
            setAssetVouchersTable={this.setAssetVouchersTable}
            handleExportToExcel={this.handleExportToExcel}
          />
        </TabPanel>
        <TabPanel className="mp-0" value={tabValue} index={appConst?.tabLiquidate?.tabLiquidated} >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setAssetVouchersTable={this.setAssetVouchersTable}
            handleExportToExcel={this.handleExportToExcel}
          />
        </TabPanel>
      </div>
    );
  }
}

InstrumentsToolLiquidateTable.contextType = AppContext;
export default InstrumentsToolLiquidateTable;
