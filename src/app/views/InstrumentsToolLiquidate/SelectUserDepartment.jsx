/* eslint-disable no-unused-expressions */
import {
  Grid,
  InputAdornment,
  Input,
  Button,
  TablePagination,
  Dialog,
  DialogActions,
  Checkbox,
  TextField,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { findUserByUserName } from "../User/UserService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { appConst } from "app/appConst";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getUserDepartmentAll } from "../Department/DepartmentService";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { createFilterOptions } from "@material-ui/lab/Autocomplete";
import { getAllUserDepartment } from "./InstrumentsToolLiquidateService";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class SelectUserDepartment extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    receiverDepartmentId: "",
    department: null,
    persons: [],
    listManageDepartment: []
  };

  setPage = (page) => {
    this.setState({ page: page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };
  handleFilterDepartment = (event, departmentSearch) => {
    this.setState({
      departmentId: departmentSearch?.id,
      department: departmentSearch
    }, () => {
      this.updatePageData();
    });
  };
  convertUsers = itemList => {
    return itemList?.map(item => {
      item.user = {
        id: item.userId,
        displayName: item.personDisplayName,
        username: item.username,
        person: {
          userId: item.userId,
          displayName: item.personDisplayName,
          id: item.personId,
        },
      };
      item.id = item.personId;
      item.department = {
        id: item.departmentId,
        name: item.departmentName,
        code: item.departmentCode,
        view: item.departmentView,
        type: item.departmentType,
      };
      return item;
    });
  };
  updatePageData = async () => {
    const { isUserDepartmentManagement, departmentId, keyword, page, rowsPerPage, persons } = this.state;
    const searchObject = {
      ...(isUserDepartmentManagement && { isUserDepartmentManagement }),
      departmentId,
      keyword: keyword.trim(),
      pageIndex: page + 1,
      pageSize: rowsPerPage,
    };
    const { data } = await getAllUserDepartment(searchObject);
    let itemList = this.convertUsers(data?.data?.content);
    const itemListClone = itemList.map((item) => ({
      ...item,
      isCheck: persons?.some((person) => person?.id === item?.user?.person?.id || person?.user?.person?.id === item?.user?.person?.id),
    }));

    this.setState({
      itemList: itemListClone || [],
      totalElements: data?.data?.totalElements,
    });
  };

  componentDidMount() {
    this.setState({
      persons: this.props?.persons
    }, () => this.updatePageData());
  };


  handleClick = (event, item) => {
    let { persons } = this.state;
    let foundPerson = persons.find(person => person.departmentName ? person?.id === item?.user?.person?.id : person?.id === item?.id);
    item.isCheck = event.target.checked;
    if (item?.isCheck) {
      if (!foundPerson?.isCheck) {
        persons = persons.concat(item);
      }
    } else {
      persons = persons.filter(
        (person) => {
          if (person?.departmentName) {
            return (person?.id !== item?.user?.person?.id);
          } else {
            return (person?.id !== item?.id);
          }
        }
      );
    }
    this.setState({ persons: persons });
  };

  componentWillMount() {
    let { selectedItem } = this.props;
    this.setState({ selectedValue: selectedItem.id });
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  search() {
    this.setPage(0);
  }
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleChange = (event, source) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  selectDepartment = (item) => {
    this.setState(
      {
        department: item ? item : null,
        departmentId: item ? item?.id : null,
        page: 0
      },
      () => this.updatePageData()
    );
  };
  handleSubmit = (persons) => {
    if (persons.length > 0) {
      this.props.handleSelect(persons);
    } else {
      toast.dismiss();
      toast.warning("Chưa chọn người thanh lý");
      toast.clearWaitingQueue();
    }
  };
  handleSetDataSelect = (data) => {
    this.setState({
      listManageDepartment: data
    });
  };
  render() {
    const {
      t,
      handleClose,
      open,
    } = this.props;
    let { keyword, department } = this.state;
    const filterAutocomplete = createFilterOptions();
    let SearchDepartment = { pageIndex: this.state.page, pageSize: 1000000 };
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "90px",
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Checkbox
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={rowData?.isCheck}
            onClick={(event) => this.handleClick(event, rowData)}
          />),
      },
      { title: t("asset_liquidate.persons"), field: "user.displayName", width: "270px" },
      {
        title: t("component.department.text"),
        field: "department.name",
        width: "150",
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <span className="">{t("asset_liquidate.persons")}</span>
        </DialogTitle>
        <DialogContent>
          <Grid item container spacing={2} xs={12} justifyContent="space-between">
            <Grid item md={6} sm={12} xs={12}>
              <Input
                label={t("general.enterSearch")}
                style={{ marginTop: "16px" }}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                className="w-100 mb-16"
                id="search_box"
                placeholder={t("general.enterSearch")}
                startAdornment={
                  <InputAdornment>
                    <Link to="#">
                      {" "}
                      <SearchIcon
                        onClick={() => this.search(keyword)}
                        className="searchTable"
                      />
                    </Link>
                  </InputAdornment>
                }
              />
            </Grid>
            <Grid item md={5} xs={12}>
              <AsynchronousAutocompleteTransfer
                label={t("general.filterDerpartment")}
                searchFunction={getUserDepartmentAll}
                searchObject={SearchDepartment}
                listData={this.state.listManageDepartment}
                setListData={this.handleSetDataSelect}
                defaultValue={department}
                displayLable={"text"}
                value={department}
                onSelect={this.selectDepartment}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim();
                  let filtered = filterAutocomplete(options, params);
                  return filtered;
                }}
                noOptionsText={t("general.noOption")}
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={this.state.page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mr-12"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mr-16"
            variant="contained"
            style={{ marginLeft: "0px" }}
            color="primary"
            onClick={() => this.handleSubmit(this.state.persons)}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectUserDepartment;
