import ConstantList from "../../appConfig";
import React, {Component} from "react";
import {Button, Dialog, DialogActions,} from "@material-ui/core";
import {ValidatorForm} from "react-material-ui-form-validator";
import {addNewOrUpdate} from "./InstrumentsToolLiquidateService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import LiquidateScrollableTabsButtonForce from "./LiquidateScrollableTabsButtonForce";
import {
  deleteAssetDocumentById,
  getAssetDocumentById,
  getListOrgManagementDepartment,
  getNewCodeAssetDocumentLiquidate,
} from "../Asset/AssetService";
import {STATUS_THANH_LY_CCDC, appConst} from "../../appConst";
import {PaperComponent, getTheHighestRole, formatDateDto} from "app/appFunction";
import AppContext from "app/appContext";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class InstrumentsToolTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Liquidate,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.Liquidate,
    handoverPersonName: null,
    handoverPersonList: [],
    code: '',
    name: '',
    liquidationStatus: '',
    liquidationCost: '',
    recoveryValue: '',
    conclude: '',
    persons: [],
    personIds: [],
    listAssetDocumentId: [],
    liquidateDepartmentUserList: [],
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    handoverDepartmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_THANH_LY_CCDC,
    managementDepartment: null,
    managementDepartmentList: [],
    transferAddress: "",
    assetDocumentList: [],
    documentType: 3,
    shouldOpenPopupAssetFile: false,
    shouldOpenSelectPersonPopup: false,
    reason: "",
    decisionCode: ""
  };
  voucherType = ConstantList.VOUCHER_TYPE.Liquidate;

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (value, name, rowData) => {
    let {assetVouchers = []} = this.state;
    let existItem = assetVouchers.find(
      item => item.assetItemId === rowData?.asset?.id
        || item.asset?.id === rowData?.asset?.id
    ) || {};
    let newItem = {
      asset: {
        ...existItem?.asset,
        [name]: +value,
        originalCost: (rowData?.asset?.unitPrice * +value) || 0,
      }
    }

    assetVouchers.splice(
      assetVouchers.indexOf(existItem),
      1,
      newItem
    );

    this.setState({
      assetVouchers: [...assetVouchers],
    });
  };

  convertStateData = state => {
    return {
      id: state?.id || "",
      assetVouchers: state?.assetVouchers?.map(item => {
        if (!item?.assetId) {
          return {
            assetId: item?.asset?.assetId,
            assetItemId: item?.asset?.id,
            note: item?.asset?.note,
            quantity: item?.asset?.quantity
          };
        } else {
          return {
            assetId: item?.assetId,
            assetItemId: item?.assetItemId,
            note: item?.note,
            quantity: item?.quantity
          };
        }
      }),
      code: state?.code,
      conclude: state?.conclude,
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverPersonName: state.handoverPerson?.personDisplayName,
      handoverPersonId: state.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      liquidationCost: state?.liquidationCost,
      liquidationStatus: state?.liquidationStatus,
      listAssetDocumentId: state?.listAssetDocumentId,
      name: state?.name,
      personIds: state?.persons?.map(item => item?.personId),
      recoveryValue: state?.recoveryValue,
      transferAddress: state?.transferAddress,
      hoiDongId: this.state?.hoiDongId,
      decisionCode: state?.decisionCode,
      reason: state?.reason,
      persons: state?.persons ? state?.persons?.map(item => {
        return {
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }
      }) : [],
    };
  };

  validation = () => {
    let {
      assetVouchers,
      issueDate,
      persons,
      name,
      liquidationStatus,
      transferAddress,
      handoverDepartment,
      handoverPerson,
    } = this.state;
    console.log(assetVouchers);
    let check = false;
    if (!name) {
      toast.warning("Vui lòng nhập tên phiếu.");
      return true;
    }
    if (!liquidationStatus) {
      toast.warning("Trạng thái phiếu không được để trống.");
      return true;
    }
    if (!handoverDepartment) {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
      return true;
    }
    if (!handoverPerson) {
      toast.warning("Vui lòng chọn người bàn giao.");
      return true;
    }
    if (!liquidationStatus) {
      toast.warning("Trạng thái phiếu không được để trống.");
      return true;
    }
    if (!transferAddress) {
      toast.warning("Vui lòng nhập địa chỉ thanh lý.");
      return true;
    }
    if (Object.keys(assetVouchers).length === 0) {
      toast.warning("Chưa chọn công cụ dụng cụ");
      return true;
    } else {
      let invalid = (liquidationStatus !== appConst.listLiquidateStatusObject.LIQUIDATED.code)
        && issueDate
        && assetVouchers?.some(x => new Date(x.asset?.dateOfReception).getTime() > new Date(issueDate).getTime())
      if (invalid) {
        toast.warning("Ngày thanh lý không được nhỏ hơn ngày tiếp nhận của CCDC");
        return true;
      }
    }
    if (this.state?.hoiDongId && persons?.length === 0) {
      toast.warning("Hội đồng không có thành viên");
      toast.clearWaitingQueue();
      return true;
    }
    return check;
  };


  handleFormSubmit = (e) => {
    if (e.target.id !== "formAssetLiquidateDialog") return;
    let {assetVouchers, id} = this.state;
    let {t, handleOKEditClose} = this.props;
    let {setPageLoading} = this.context;
    if (this.validation()) {
      return;
    } else {
      setPageLoading(true);
      addNewOrUpdate(this.convertStateData(this.state))
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data.code) {
            id ? toast.success(t('general.updateSuccess')) : toast.success(t('general.addSuccess'));
            handleOKEditClose();
          }
          else {
            toast.warning(data.message);
          }
        })
        .catch(() => {
          toast.error(t("general.error"));
        }).finally(() => {
          setPageLoading(false);
        });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true
    });
  };

  handleSelectPerson = (listPerson) => {
    let personIds = [];
    let persons = [];
    if (listPerson?.length > 0) {
      listPerson.forEach((item) => {
        let person = item?.user?.person ? item?.user?.person : item;
        if (item?.user?.person) {
          person.departmentName = item?.department?.name;
        }
        let personId = item?.person?.id ? item?.person?.id : person?.id;

        persons.push(person);
        personIds.push(personId);
      });
      this.setState({
        persons: persons,
        personIds: personIds
      });
    } else {
      this.setState({
        persons: [],
        personIds: []
      });
    }
    this.handleSelectPersonPopupClose();
  };

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false
    });
  };

  handleSelectAsset = (items) => {
    let {t} = this.props;
    let assetVouchers = [];
    if (items.length <= 0) toast.warning(t("general.noCCDCSelected"));

    items?.map(item => {
      let asset = {
        ...item.asset,
        quantity: item.asset?.quantity || 1,
        inventoryQuantity: item.asset.inventoryQuantity
          ? item.asset.inventoryQuantity
          : item.asset.quantity,
      }
      assetVouchers.push({asset})
    })

    this.setState({assetVouchers}, function () {
      this.handleAssetPopupClose();
    });
  };

  removeAssetInlist = (id) => {
    let {assetVouchers} = this.state;
    let {t} = this.props;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({assetVouchers}, () => {
      toast.dismiss();
      toast.info(t("asset_liquidate.deleteFromList"));
    });
  };

  removePersonInlist = (personId) => {
    let {persons, personIds} = this.state;
    let index = persons?.findIndex(x => x.id === personId);
    let indexId = personIds?.findIndex(personIds => personIds === personId);
    persons.splice(index, 1);
    personIds.splice(indexId, 1);

    this.setState({persons, personIds}, () => {
      toast.dismiss();
      toast.info(this.props.t("asset_liquidate.deleteFromList"));
    });
  };

  getManagementDepartment = () => {
    getListOrgManagementDepartment({}).then(({data}) => {
      let handoverDepartment = data ? data[0] : null;
      this.setState({
        handoverDepartment: handoverDepartment,
        handoverDepartmentId: handoverDepartment?.id,
        managementDepartmentId: handoverDepartment?.id
      });
    });
  };

  handleSetListLiquidateDepartmentUserList = (item) => {
    this.setState({persons: item || []})
  }
  handleSetHoiDongId = (id) => {
    this.setState({hoiDongId: id})
  }

  componentDidMount() {
    let {item = {}} = this.props;
    let {isFromRecall} = item;
    const {isRoleAssetManager} = getTheHighestRole();
    let assetVouchers = [];

    item?.assetVouchers?.map(item => {

      let p = {
        asset: {
          ...item,
          assetId: item.assetId,
          name: item.assetName || item.name,
          carryingAmount: item.assetCarryingAmount || item.carryingAmount,
          code: item.assetCode || item?.code,
          dateOfReception: item?.assetYearPutIntoUse || item.yearPutIntoUse,
          id: item.assetItemId || item.id,
          inventoryQuantity: isFromRecall
            ? item?.assetItemQty
            : item.inventoryQuantity || item?.assetItemQty || item?.quantity,
          managementCode: item.assetManagementCode || item.managementCode,
          originalCost: item.assetOriginalCost || item.originalCost,
          quantity: item.quantity,
        }
      };
      return assetVouchers.push(p);
    });

    this.setState({
      ...item,
      assetVouchers,
    }, () => {
      if (isRoleAssetManager) {
        this.getManagementDepartment();
      }
    });
  }

  handleSetDataHandoverDepartment = (data) => {
    this.setState({
      listHandoverDepartment: data
    });
  };

  handleSetDataHandoverPerson = (data) => {
    this.setState({
      listHandoverPerson: data
    });
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item,
      handoverDepartmentId: item?.id,
      listHandoverPerson: [],
      assetVouchers: [],
      managementDepartmentId: item?.id,
      handoverPerson: null
    });
  };

  handleChangeSelect = (item) => {
    this.setState({
      liquidationStatus: item?.code
    });
  };

  openPopupSelectAsset = () => {
    let {voucherType, handoverDepartment} = this.state;
    if (handoverDepartment) {
      this.setState({item: {}, voucherType},
        function () {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };
  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
      item: {},
    });
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  handleConfirmationResponse = () => {
    this.setState({shouldOpenNotificationPopup: false});
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({data}) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => fileDescriptionIds.push(item?.file?.id));

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = appConst.documentType.ASSET_DOCUMENT_LIQUIDATE;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let {documents, listAssetDocumentId} = this.state;
    let {t} = this.props;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({listAssetDocumentId, documents}, () => {
      toast.dismiss();
      toast.info(t("asset_liquidate.deleteFromList"));
    });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeAssetDocumentLiquidate().then(({data}) => {
      if (appConst.CODE.SUCCESS === data?.code) {
        let item = {};
        item.code = data.data;
        this.setState({
          item,
          shouldOpenPopupAssetFile: true,
        });
      } else {
        toast.warning(data.message);
      }
    }).catch(err => toast.error("Không thể tạo mã hồ sơ"));
  };

  updatePageAssetDocument = (newValue) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(item => item?.id === newValue?.id);
    documents[indexDocument] = newValue;
    this.setState({documents});
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({listAssetDocumentId, documents});
  };

  deleteAssetDocumentById = () => {
    let {listAssetDocumentId} = this.state;
    if (listAssetDocumentId && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach((id) => deleteAssetDocumentById(id));
    }
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  render() {
    let {open, t, i18n} = this.props;

    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state.handoverDepartment?.id || null,
    };
    let {shouldOpenNotificationPopup} = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn công cụ dụng cụ")}
            agree={t("general.agree")}
          />
        )}
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          id='formAssetLiquidateDialog'
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            className="pb-0 cursor-move"
            id="draggable-dialog-title"
          >
            <span className="">{t("asset_liquidate.dialog")}</span>
          </DialogTitle>
          <DialogContent className="min-height-450">
            <LiquidateScrollableTabsButtonForce
              t={t}
              id={this.props.item?.data?.id}
              i18n={i18n}
              item={this.state}
              handleHandoverDepartmentPopupOpen={
                this.handleHandoverDepartmentPopupOpen
              }
              handleHandoverDepartmentPopupClose={
                this.handleHandoverDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleSelectPerson={this.handleSelectPerson}
              openSelectPersonPopup={this.openSelectPersonPopup}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleChange={this.handleChange}
              handleDateChange={this.handleDateChange}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              removePersonInlist={this.removePersonInlist}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              updatePageAssetDocument={this.updatePageAssetDocument}
              getAssetDocument={this.getAssetDocument}
              handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
              handleSetDataHandoverDepartment={this.handleSetDataHandoverDepartment}
              handleSetDataHandoverPerson={this.handleSetDataHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              handleChangeSelect={this.handleChangeSelect}
              handleSetListLiquidateDepartmentUserList={this.handleSetListLiquidateDepartmentUserList}
              handleSetHoiDongId={this.handleSetHoiDongId}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!this.props?.item?.isView &&
                <Button
                  variant="contained"
                  color="primary"
                  className="mr-16"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

InstrumentsToolTransferDialog.contextType = AppContext;
export default InstrumentsToolTransferDialog;
