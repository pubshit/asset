import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const AssetLiquidateTable = EgretLoadable({
  loader: () => import("./InstrumentsToolLiquidateTable")
});
const ViewComponent = withTranslation()(AssetLiquidateTable);

const InstrumentsToolLiquidateRoutes = [
  {
    path: ConstantList.ROOT_PATH + "instruments-and-tools/liquidate-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default InstrumentsToolLiquidateRoutes;