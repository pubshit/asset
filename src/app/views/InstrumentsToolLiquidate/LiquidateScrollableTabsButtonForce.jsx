import React, { useEffect, useState } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {
  useTranslation,
} from "react-i18next";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import SelectAssetAllPopup from "../Component/InstrumentTools/SelectInstrumentToolsAllPopup";
import MaterialTable, { MTableToolbar, } from "material-table";
import VoucherFilePopup from "./VoucherFilePopup";
import { LISTLIQUIDATE, appConst } from "../../appConst";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {
  LightTooltip,
  NumberFormatCustom,
  TabPanel,
  a11yProps,
  convertNumberPriceRoundUp,
  handleKeyDownIntegerOnly,
  filterOptions
} from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import { searchByPage } from "../Council/CouncilService";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function LiquidateScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const { createDate } = props.item;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [dataTableLiqui, setDataTableLiqui] = useState(props?.item?.persons || []);
  const [listLiqui, setListLiqui] = useState(null);
  let searchObject = {
    keyword: "",
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    type: appConst.OBJECT_HD_TYPE.THANH_LY.code,
  }
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  let selectLiquidationStatus = appConst.listLiquidateStatus.find(status => status.code === props?.item?.liquidationStatus) || "";
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeSelectLiquidation = (item) => {
    setListLiqui(item);
    props.handleSetListLiquidateDepartmentUserList(item?.hdChiTiets)
    props.handleSetHoiDongId(item?.id)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setDataTableLiqui(item?.hdChiTiets || []);
  };

  const handleRowDataCellDelete = (item) => {
    let newDataRow = dataTableLiqui.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListLiquidateDepartmentUserList(newDataRow)
    setDataTableLiqui(newDataRow);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? ''
    });
  }, [props?.item?.handoverDepartment?.id]);

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView) &&
        <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData.asset.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      )
    },
    {
      title: t("Asset.stt"),
      field: "asset.assetCode",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props.item.page) * props.item.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("InstrumentToolsList.code"),
      field: "asset.assetCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.asset?.assetCode || rowData?.asset?.code
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.assetManagementCode",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.asset?.assetManagementCode || rowData?.asset?.managementCode
    },
    {
      title: t("InstrumentToolsList.name"),
      field: "asset.assetName",
      align: "left",
      minWidth: 150,
      render: rowData => rowData?.asset?.assetName || rowData?.asset?.name
    },
    {
      title: t("InstrumentsToolsLiquidate.quantity"),
      field: "asset.quantity",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          fullWidth
          value={rowData.asset?.quantity || 0}
          onKeyDown={handleKeyDownIntegerOnly}
          onChange={(event) => props.handleRowDataCellChange(
            event?.target?.value,
            event?.target?.name,
            rowData,
          )}
          type="number"
          name="quantity"
          InputProps={{
            readOnly: props?.item?.isView,
            inputProps: {
              className: "text-center",
            },
          }}
          validators={[
            "minNumber:1",
            "required",
            `maxNumber:${rowData.asset?.inventoryQuantity}`
          ]}
          errorMessages={[
            t("general.minNumberError"),
            t("general.required"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
          ]}
        />
      ),
    },
    {
      title: t("InstrumentToolsList.availableQuantity"),
      field: "asset.inventoryQuantity",
      align: "left",
      minWidth: 60,
      hidden: props?.item?.isView,
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("InstrumentToolsList.model"),
      field: "asset.assetModel",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData => rowData?.asset?.assetModel || rowData?.asset?.model
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.assetSerialNumber",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData => rowData?.asset?.assetSerialNumber || rowData?.asset?.serialNumber
    },
    {
      title: t("Asset.yearIntoUse"),
      field: "asset.assetYearPutIntoUse",
      align: "left",
      minWidth: 90,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData => rowData?.asset?.assetYearPutIntoUse || rowData?.asset?.yearPutIntoUse
    },
    {
      title: t("InstrumentToolsList.yearOfManufacture"),
      field: "asset.assetYearOfManufacture",
      align: "left",
      minWidth: 90,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData => rowData?.asset?.assetYearOfManufacture || rowData?.asset?.yearOfManufacture
    },
    {
      title: t("Asset.manufacturerTable"),
      field: "asset.manufacturerName",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center"
      }
    },
    {
      title: t("Asset.useDepartment"),
      field: "asset.useDepartmentName",
      align: "left",
      minWidth: 150,
      render: (rowData) => rowData?.asset?.assetUseDepartmentName || rowData?.asset?.useDepartmentName
    },
    {
      title: t("Asset.store"),
      field: "asset.storeName",
      align: "left",
      minWidth: 150,
      render: (rowData) => rowData?.asset?.assetStoreName || rowData?.asset?.storeName
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPriceRoundUp(rowData?.asset?.originalCost)
    },
  ];

  let columnsVoucherFile = [
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <div className="none_wrap">
          {!props?.item?.isView ? <>
            <LightTooltip
              title={t("general.editIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
              >
                <Icon fontSize="small" color="primary">
                  edit
                </Icon>
              </IconButton>
            </LightTooltip>
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </> :
            <LightTooltip
              className="p-5"
              title={t("general.viewIcon")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
                <Icon fontSize="small" color="primary">
                  visibility
                </Icon>
              </IconButton>
            </LightTooltip>}
        </div>
      ),
    },
    {
      title: t("general.stt"),
      field: "code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1 || '',
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 350,
      cellStyle: {
        textAlign: "left",
      },
    },
  ];


  let columnPerson = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    ...(!props?.item?.isView && props?.item?.liquidationStatus !== appConst.listLiquidateStatusObject.LIQUIDATED.code ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          !props?.item?.isView && (
            <div className="none_wrap">
              <LightTooltip
                title={t("general.deleteIcon")}
                placement="top"
                enterDelay={300}
                leaveDelay={200}
              >
                <IconButton
                  size="small"
                  onClick={() => handleRowDataCellDelete(rowData)}
                >
                  <Icon fontSize="small" color="error">
                    delete
                  </Icon>
                </IconButton>
              </LightTooltip>
            </div>
          ),
      },
    ] : []),
    {
      title: t("asset_liquidate.persons"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    },
  ];
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("Thông tin phiếu")} {...a11yProps(0)} />
          <Tab label={t("Ban thanh lý")} {...a11yProps(1)} />
          <Tab label={t("Hồ sơ đính kèm")} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={12} xs={12} >
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DateTimePicker
                fullWidth
                readOnly={true}
                margin="none"
                onChange={() => { }}
                id="mui-pickers-date"
                label={t("asset_liquidate.createDate")}
                format="dd/MM/yyyy"
                value={createDate || new Date()}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            <ValidatePicker
              fullWidth
              margin="none"
              label={
                <span>
                  {selectLiquidationStatus.indexOrder ===
                    LISTLIQUIDATE.LIQUIDATED.indexOrder ? (
                    <span className="colorRed">*</span>
                  ) : (
                    ""
                  )}
                  {t("asset_liquidate.liquidationDate")}
                </span>
              }
              KeyboardButtonProps={{ "aria-label": "change date" }}
              autoOk
              format="dd/MM/yyyy"
              name={"issueDate"}
              inputProps={{ readOnly: props?.item?.isView }}
              value={props.item?.issueDate || null}
              onChange={(date) => props.handleDateChange(date, "issueDate")}
              readOnly={props?.item?.isView}
              invalidDateMessage={t("general.invalidDateFormat")}
              maxDate={new Date()}
              maxDateMessage={t("allocation_asset.maxDateMessage")}
              validators={
                selectLiquidationStatus.indexOrder ===
                  LISTLIQUIDATE.LIQUIDATED.indexOrder
                  ? ["required"]
                  : []
              }
              errorMessages={selectLiquidationStatus.indexOrder ===
                LISTLIQUIDATE.LIQUIDATED.indexOrder
                ? [t("general.required")] : []}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("asset_liquidate.name")}
                </span>
              }
              name="name"
              value={props.item.name || ''}
              onChange={props?.handleChange}
              InputProps={{ readOnly: props?.item?.isView }}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {props?.item?.isView ?
              <TextValidator
                className="w-100"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    <span> {t("TransferToAnotherUnit.status")}</span>
                  </span>
                }
                value={selectLiquidationStatus ? selectLiquidationStatus?.name : null}
                InputProps={{
                  readOnly: true,
                }}
              /> : <AsynchronousAutocompleteSub
                label={
                  <span>
                    <span className="colorRed">* </span>
                    <span> {t("TransferToAnotherUnit.status")}</span>
                  </span>
                }
                searchFunction={() => { }}
                listData={appConst.listLiquidateStatus}
                setListData={props.handleSetDataHandoverPerson}
                defaultValue={selectLiquidationStatus ? selectLiquidationStatus : null}
                displayLable={"name"}
                value={selectLiquidationStatus ? selectLiquidationStatus : null}
                onSelect={props.handleChangeSelect}
                validators={["required"]}
                errorMessages={[t('general.required')]}
                InputProps={{
                  readOnly: true
                }}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
              />
            }
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            <TextValidator
              fullWidth
              disabled
              label={t("asset_liquidate.number")}
              value={props.item.code || ''}
            />
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            <TextValidator
              fullWidth
              name='decisionCode'
              label={t("asset_liquidate.decisionCode")}
              onChange={props?.handleChange}
              value={props.item.decisionCode || ''}
              InputProps={{ readOnly: props?.item?.isView }}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  <span> {t("allocation_asset.handoverDepartment")}</span>
                </span>
              }
              searchFunction={getListOrgManagementDepartment}
              searchObject={props.handoverPersonSearchObject}
              listData={props.item.listHandoverDepartment}
              setListData={props.handleSetDataHandoverDepartment}
              displayLable={'name'}
              readOnly={props?.isRoleManager || props?.item?.isView}
              value={props.item?.handoverDepartment ? props.item?.handoverDepartment : null}
              onSelect={props.selectHandoverDepartment}
              typeReturnFunction="list"
              showCode={"showCode"}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              isNoRenderChildren
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("asset_liquidate.handoverPerson")}
                </span>
              }
              disabled={!props?.item?.handoverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={{
                ...searchParamUserByHandoverDepartment,
                pageSize: 1000,
                pageIndex: 1
              }}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.handoverPerson ?? ''}
              onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>

          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("asset_liquidate.liquidationCost")}
              name="liquidationCost"
              id="formatted-numberformat-originalCost"
              value={props.item?.liquidationCost || ''}
              onChange={props.handleChange}
              InputProps={{
                readOnly: props?.item?.isView,
                inputComponent: NumberFormatCustom,
              }}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("asset_liquidate.recoveryValue")}
              name="recoveryValue"
              id="formatted-numberformat-originalCost"
              value={props.item?.recoveryValue || ''}
              onChange={props.handleChange}
              InputProps={{
                readOnly: props?.item?.isView,
                inputComponent: NumberFormatCustom,
              }}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("asset_liquidate.address")}
                </span>
              }
              onChange={props.handleChange}
              type="text"
              name="transferAddress"
              value={props.item.transferAddress || ''}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={t("asset_liquidate.conclude")}
              onChange={props.handleChange}
              type="text"
              name="conclude"
              value={props.item.conclude || ''}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={t("asset_liquidate.reason")}
              onChange={props.handleChange}
              type="text"
              name="reason"
              value={props.item.reason || ''}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item container md={12} sm={12} xs={12} spacing={1} className="mb-4">
            <Grid item md={2} sm={12} xs={12}>
              <Button
                variant="contained"
                color="primary"
                className="mt-16 w-100"
                size="small"
                onClick={props.openPopupSelectAsset}
                disabled={props?.item?.isView}
              >
                {t("InstrumentToolsAllocation.choose")}
              </Button>

              {
                // props.item.handoverDepartment && 
                props.item.shouldOpenAssetPopup && (
                  <SelectAssetAllPopup
                    t={t} i18n={i18n}
                    open={props.item.shouldOpenAssetPopup}
                    handleSelect={props.handleSelectAsset}
                    isAssetTransfer={props.item.isAssetTransfer}
                    statusIndexOrders={props.item.statusIndexOrders}
                    handleClose={props.handleAssetPopupClose}
                    handoverDepartment={props.item?.handoverDepartment}
                    assetVouchers={props.item.assetVouchers ? props.item.assetVouchers : []}
                    dateOfReceptionTop={props.item?.issueDate}
                    isLiquidate={true}
                  />
                )}
            </Grid>
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={props.item?.assetVouchers ? props.item?.assetVouchers : []}
              columns={columns}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                draggable: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  textAlign: "center",
                },
                padding: "dense",
                maxBodyHeight: "450px",
                minBodyHeight: "250px",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction='category'
            searchObject={searchObject}
            label={t("asset_liquidate.liquidation_board")}
            displayLable={"name"}
            onSelect={handleChangeSelectLiquidation}
            value={listLiqui}
            InputProps={{
              readOnly: true,
            }}
            disabled={props?.item?.isView}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="pt-16">
          <MaterialTable
            data={dataTableLiqui || []}
            columns={columnPerson}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Grid item md={12} sm={12} xs={12}>
          {!props?.item?.isView &&
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("InstrumentToolsTransfer.addFile")}
            </Button>
          }
          {props.item?.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item?.shouldOpenPopupAssetFile}
              updatePageAssetDocument={props.updatePageAssetDocument}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocument={props.getAssetDocument}
              item={props.item}
              t={t}
              i18n={i18n}
              isView={props?.item?.isView}
            />
          )}
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item?.documents || []}
            columns={columnsVoucherFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div>
  );
}
