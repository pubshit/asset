import PropTypes from "prop-types";
import { Button, Dialog, DialogActions, DialogTitle, Grid, Link } from "@material-ui/core";
import {
  formatDateDto,
  handleThrowResponseMessage,
  isInValidDateForSubmit,
  isInValidForSubmit,
  isSuccessfulResponse,
  PaperComponent
} from "../../appFunction";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import DialogContent from "@material-ui/core/DialogContent";
import { APPROVAL_LEVEL, EXECUTE_ATTRIBUTE, ROUTES_PATH, TRACKER_CODE, variable } from "../../appConst";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { defaultListData } from "./constant";
import AppContext from "../../appContext";
import { toast } from "react-toastify";
import { getWfWorkflow } from "../workflows/services";
import WfButton from "../workflows/Button";
import { addApprovalProcesses, getDetailVoucherByUri, updateApprovalProcesses } from "./ApprovalRequestService";
import { defaultState, defaultPermission } from "./constant";
import {
  approvalnDateWaterfall,
  approverWaterfall,
  approvingDepartmentWaterfall,
  convertItem,
  refreshPermission, submissionDateWaterfall, submitterWaterfall,
  submittingDepartmentWaterfall,
  validateSubmit
} from "./helper";
import { TRACKER_STATUS_CODE } from "../workflows/constant";
import { ViewComponent } from "./component/ViewComponent";
import DeviceTroubleshootingDialog from "../AssetRepair/Dialog/DeviceTroubleshootingDialog";
import { useLocation } from "react-router-dom";

function ApprovalRequestDialog(props) {
  const {
    open,
    handleClose = () => { },
    handleOKEditClose = () => { },
    item = {},
    defaultValue = {},
    componentName = "",
  } = props;
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const location = useLocation();

  const [permission, setPermission] = useState({ ...defaultPermission });
  const [state, setState] = useState({ ...defaultState, ...defaultValue });
  const [listData, setListData] = useState({ ...defaultListData });
  const [workflows, setWorkflows] = useState();
  const [voucherDetail, setVoucherDetail] = useState(null);
  const [shouldOpenDetailDialog, setShouldOpenDetailDialog] = useState(false);

  const approvalRoutePath = "/" + ROUTES_PATH.APPROVAL_REQUEST;
  const detailDialog = {
    [TRACKER_CODE.REPAIR]: DeviceTroubleshootingDialog,
  }

  useEffect(() => {
    if (item?.id) {
      handleGetValueByObs();
    }
    handleGetWorkFlow(item?.id, item?.trackerCode);
    handleCheckPermission();

    return () => {
      refreshPermission(permission, setPermission);
    }
  }, []);



  const handleGetValueByObs = () => {
    const newItem = {};
    const obs = { ...item?.obs };
    let newState = { ...defaultState };

    Object.keys(obs).map(key => {
      newItem[key] = obs[key]?.valueText
        || obs[key]?.valueBoolean
        || obs[key]?.valueComplex
        || obs[key]?.valueDatetime
        || obs[key]?.valueNumeric
    })

    newState = { ...convertItem(newItem, item) }

    setState(prevState => ({ ...prevState, ...newState }));
  }

  const handleCheckPermission = () => {
    const permission = { ...defaultPermission };
    const obs = { ...item?.obs };

    Object.keys(obs).map(key => {
      if (permission[key]) {
        permission[key].isShow = Boolean(obs[key]);
        if (obs[key].attribute === EXECUTE_ATTRIBUTE.READ) {
          permission[key].isView = true;
          permission[key].isDisabled = false;
          permission[key].isRequired = false;
        } else if (obs[key].attribute === EXECUTE_ATTRIBUTE.WRITE) {
          permission[key].isRequired = false;
        }
      }
    })

    setPermission({ ...permission });
  }

  const handleGetWorkFlow = async (objectId, trackerCode) => {
    try {
      const params = {
        trackerCode,
        objectId,
      }
      let res = await getWfWorkflow(params);
      let { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setWorkflows(data);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  const handleChangeText = (e) => {
    setState(prevState => ({
      ...prevState,
      [e?.target?.name]: e?.target?.value
    }))
  }

  const handleChangeSelect = (value, name) => {
    setState(prevState => ({
      ...prevState,
      [name]: value
    }))
  }

  const handleChangeListData = (value, name) => {
    setListData(prevState => ({
      ...prevState,
      [name]: value
    }))
  }

  const handleSelectDepartment = (value, name) => {
    const {
      firstLevelApprovingDepartment,
      firstLevelSubmittingDepartment,
      secondLevelApprovingDepartment,
      firstLevelApprover,
      firstLevelSubmitter,
      secondLevelApprover,
    } = variable.listInputName;
    let personField = {
      [firstLevelApprovingDepartment]: firstLevelApprover,
      [firstLevelSubmittingDepartment]: firstLevelSubmitter,
      [secondLevelApprovingDepartment]: secondLevelApprover,
    };

    setState(prevState => ({
      ...prevState,
      [name]: value,
      [personField[name] || ""]: null,
    }));

    setListData(prevState => ({
      ...prevState,
      listFirstLevelApprover: [],
      listFirstLevelSubmitter: [],
      listSecondLevelApprover: [],
    }))
  }

  const handleSubmit = async (wfData) => {
    try {
      if (isInValidForSubmit() || isInValidDateForSubmit()) { return; }
      if (validateSubmit(state, wfData)) return;

      const sendData = convertPayload(wfData);
      let res;
      if (wfData?.statusNew?.statusId) {
        res = item?.id
          ? await updateApprovalProcesses(item?.id, sendData)
          : await addApprovalProcesses(sendData);
      } else {
        res = await wfData?.submit?.(sendData);
      }

      const { code } = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(t("general.success"));
        handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const convertPayload = (wfData) => {
    const {
      FIRST_LV_PENDING_APPROVAL,
      SECOND_LEVEL_APPROVAL_PROCESS,
      REQUESTED_ADDITION,
    } = TRACKER_STATUS_CODE.APPROVAL_PROCESSES;
    const statusCode = wfData?.statusNew?.statusCode;
    let approverId = {
      [FIRST_LV_PENDING_APPROVAL.code]: state?.firstLevelApprover?.personId,
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: state?.secondLevelApprover?.personId,
    }
    let submitterId = {
      [FIRST_LV_PENDING_APPROVAL.code]: state?.firstLevelSubmitter?.personId,
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: state?.firstLevelApprover?.personId,
    }
    let submittingDepartmentId = {
      [FIRST_LV_PENDING_APPROVAL.code]: state?.firstLevelSubmittingDepartment?.id,
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: state?.firstLevelApprovingDepartment?.id,
    }
    let approvingDepartmentId = {
      [FIRST_LV_PENDING_APPROVAL.code]: state?.firstLevelApprovingDepartment?.id,
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: state?.secondLevelApprovingDepartment?.id,
    }
    let wfWorkflowId = {
      [FIRST_LV_PENDING_APPROVAL.code]: item?.wfWorkflowId,
      [REQUESTED_ADDITION.code]: wfData?.statusNew?.wfId,
    }
    let submissionDate = {
      [FIRST_LV_PENDING_APPROVAL.code]: formatDateDto(state?.firstLevelSubmissionDate),
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: formatDateDto(state?.firstLevelApprovalDate),
    }
    let approvalDate = {
      [FIRST_LV_PENDING_APPROVAL.code]: formatDateDto(state?.firstLevelApprovalDate),
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: formatDateDto(state?.secondLevelSubmissionDate),
    }
    let level = {
      [FIRST_LV_PENDING_APPROVAL.code]: APPROVAL_LEVEL.FIRST_LEVEL,
      [SECOND_LEVEL_APPROVAL_PROCESS.code]: item?.level + 1,
    }
    let obs = {
      [FIRST_LV_PENDING_APPROVAL.code]: {
        firstLevelApprovalContent: null,
        firstLevelApprovalDate: null,
        secondLevelApproverId: null,
        secondLevelApproverName: null,
        secondLevelApprovingDepartmentId: null,
        secondLevelApprovingDepartmentName: null,
        secondLevelSubmissionDate: null,
        secondLevelApprovalDate: null,
        secondLevelApprovalContent: null,
        oldStatusId: state?.newStatusId,
        oldStatusCode: state?.newStatusCode,
        oldStatusName: state?.newStatusName,
      },
    };

    return ({
      statusId: wfData?.statusNew?.statusId,
      objectId: item?.objectId,
      wfWorkflowId: wfWorkflowId[statusCode] || wfData?.wfId,
      submissionDate: submissionDate[statusCode] || formatDateDto(submissionDateWaterfall(state)),
      submitterId: submitterId[statusCode] || submitterWaterfall(state),
      submittingDepartmentId: submittingDepartmentId[statusCode] || submittingDepartmentWaterfall(state),
      approverId: approverId[statusCode] || approverWaterfall(state),
      approvingDepartmentId: approvingDepartmentId[statusCode] || approvingDepartmentWaterfall(state),
      trackerCode: item?.trackerCode,
      trackerCodeRelate: item?.trackerCodeRelate,
      level: level[statusCode] || item?.level + 1,
      submissionNumber: item?.submissionNumber,
      approvalDate: approvalDate[statusCode] || formatDateDto(approvalnDateWaterfall(state)),
      obs: {
        oldStatusId: state?.oldStatusId,
        oldStatusCode: state?.oldStatusCode,
        oldStatusName: state?.oldStatusName,
        newStatusId: wfData?.statusNew?.statusId,
        newStatusCode: wfData?.statusNew?.statusCode,
        newStatusName: wfData?.statusNew?.statusName,
        firstLevelSubmitterId: state?.firstLevelSubmitter?.personId,
        firstLevelSubmitterName: state?.firstLevelSubmitter?.displayName,
        firstLevelSubmittingDepartmentId: state?.firstLevelSubmittingDepartment?.id,
        firstLevelSubmittingDepartmentName: state?.firstLevelSubmittingDepartment?.name,
        firstLevelSubmissionDate: formatDateDto(state?.firstLevelSubmissionDate),
        firstLevelSubmissionContent: state?.firstLevelSubmissionContent,
        firstLevelApprovalContent: state?.firstLevelApprovalContent,
        firstLevelApproverId: state?.firstLevelApprover?.personId,
        firstLevelApproverName: state?.firstLevelApprover?.displayName,
        firstLevelApprovingDepartmentId: state?.firstLevelApprovingDepartment?.id,
        firstLevelApprovingDepartmentName: state?.firstLevelApprovingDepartment?.name,
        firstLevelApprovalDate: formatDateDto(state?.firstLevelApprovalDate),
        secondLevelApproverId: state?.secondLevelApprover?.personId,
        secondLevelApproverName: state?.secondLevelApprover?.displayName,
        secondLevelApprovingDepartmentId: state?.secondLevelApprovingDepartment?.id,
        secondLevelApprovingDepartmentName: state?.secondLevelApprovingDepartment?.name,
        secondLevelSubmissionDate: formatDateDto(state?.secondLevelSubmissionDate),
        secondLevelApprovalDate: formatDateDto(state?.secondLevelApprovalDate),
        secondLevelApprovalContent: state?.secondLevelApprovalContent,
        ...obs[statusCode],
        uri: state?.uri,
      }
    })
  };

  const handleOpenDetailDialog = () => {
    setShouldOpenDetailDialog(true);
  }

  const handleCloseDetailDialog = () => {
    setShouldOpenDetailDialog(false);
  }

  const handleGetDetailVoucher = async () => {
    try {
      let res = await getDetailVoucherByUri(state?.uri);

      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setVoucherDetail(data);
        handleOpenDetailDialog();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const renderDetailDialog = () => {
    const DetailDialogComponent = detailDialog[item?.trackerCodeRelate];
    let props = {};

    switch (item?.trackerCodeRelate) {
      case TRACKER_CODE.REPAIR:
        props = {
          t,
          handleClose: handleCloseDetailDialog,
          itemEdit: voucherDetail,
          open: shouldOpenDetailDialog,
          isView: true,
          isAdd: false,
          isFromApprovalRequest: true,
        }
        break;
      default:
        break;
    }

    return (
      <>
        {shouldOpenDetailDialog && DetailDialogComponent && (
          <DetailDialogComponent {...props} />
        )}
      </>
    )
  }

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth>
      <DialogTitle className="pb-0">
        <div className="flex flex-space-between">
          <span>{t("ApprovalRequest.titleDialog")}</span>
          {location.pathname === approvalRoutePath && (
            <Link className="fz-xm" component="button" onClick={handleGetDetailVoucher}>Xem thông tin đính kèm</Link>
          )}
        </div>
      </DialogTitle>
      <CustomValidatorForm onSubmit={() => { }}>
        <DialogContent className="mb-12">
          <Grid container spacing={2}>
            <ViewComponent
              t={t}
              item={state}
              listData={listData}
              permission={permission}
              handleChangeListData={handleChangeListData}
              handleChangeSelect={handleChangeSelect}
              handleChangeText={handleChangeText}
              handleSelectDepartment={handleSelectDepartment}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={handleClose}
          >
            {t("general.cancel")}
          </Button>
          {workflows?.map(item => {
            return (
              <WfButton
                key={item.id}
                wfItem={item}
                componentName={componentName}
                handleClick={handleSubmit}
              />
            )
          })}
        </DialogActions>
      </CustomValidatorForm>

      {renderDetailDialog()}

    </Dialog>
  )
}

export default ApprovalRequestDialog;
ApprovalRequestDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  item: PropTypes.any,
  defaultValue: PropTypes.any,
  componentName: PropTypes.string,
}
