import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/approval-processes";


export const searchByPage = (params) => {
  return axios.get(API_PATH + "/search-by-page", {params});
};

export const getAllRequest = (params) => {
  return axios.get(API_PATH, {params});
};

export const getItemById = (id, params) => {
  return axios.get(API_PATH + "/" + id, {params});
};

export const addApprovalProcesses = (payload) => {
  return axios.post(API_PATH, payload);
};

export const updateApprovalProcesses = (id, payload) => {
  return axios.put(API_PATH + `/${id}`, payload);
};

export const getWfFields = (params) => {
  let url = `${API_PATH}/fields`
  return axios.get(url, { params });
}

export const getWfProcessHistory = (params) => {
  let url = `${API_PATH}/history`
  return axios.get(url, { params });
}

export const getDetailVoucherByUri = (uri) => {
  return axios.get(ConstantList.API_ENPOINT_ASSET_MAINTANE + uri);
}
