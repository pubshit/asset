import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  AppBar,
  Tabs,
  Tab, Card, CardContent,
} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { useTranslation } from "react-i18next";
import {
  getAllRequest,
  getItemById,
  searchByPage,
} from "./ApprovalRequestService";
import ApprovalRequestDialog from "./ApprovalRequestDialog";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {appConst} from "app/appConst";
import {Label, LightTooltip} from "../Component/Utilities";
import AppContext from "../../appContext";
import {
  defaultPaginationProps, filterOptions, formatDateDto,
  formatTimestampToDate, getUserInformation,
  handleThrowResponseMessage, isInValidDateForSubmit,
  isSuccessfulResponse
} from "../../appFunction";
import ApprovalProcessHistoryPopup from "../Component/SubmissionForm/ApprovalProcessHistoryPopup";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {getListOrgManagementDepartment} from "../Asset/AssetService";
import {searchByPageDepartmentNew} from "../Department/DepartmentService";
import {ValidatorForm} from "react-material-ui-form-validator";
import {getListUserByDepartmentId} from "../AssetTransfer/AssetTransferService";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
function MaterialButton(props) {
  const { t } = useTranslation();
  const {item = {}, tabValue} = props;
  const isTabHistory = tabValue === appConst.TABS_APPROVAL_REQUEST.HISTORY.code;
  const isTabRequest = tabValue === appConst.TABS_APPROVAL_REQUEST.PENDING_REQUEST.code;
  return (
    <div className="none_wrap">
      {isTabRequest && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isTabHistory && (
        <LightTooltip
          title={t("general.view")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <Icon fontSize="small" color="primary">
              visibility
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

function ApprovalRequest() {
  const {t} = useTranslation();
  const {setPageLoading} = useContext(AppContext);
  const {organization} = getUserInformation();

  const [item, setItem] = useState(null);
  const [state, setState] = useState({
    rowsPerPage: appConst.rowsPerPage.voucher,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenApprovalRequestDialog: false,
    shouldOpenApprovalHistory: false,
    keyword: null,
    Notification: "",
    hasDeletePermission: false,
    isView: null,
    tabValue: appConst.TABS_APPROVAL_REQUEST.PENDING_REQUEST.code,
    objectId: "",
    approvingDepartment: null,
    listApprovingDepartment: [],
    submittingDepartment: null,
    listSubmittingDepartment: [],
    approver: null,
    listApprover: [],
    submitter: null,
    listSubmitter: [],
    searchTrigger: false,
  });
  const [filter, setFilter] = useState({
    fromDate: null,
    toDate: null,
    fromApprovalDate: null,
    toApprovalDate: null,
    approvingDepartment: null,
    submittingDepartment: null,
    approver: null,
    submitter: null,
  });

  const TitlePage = t("ApprovalRequest.title");
  const searchDepartmentObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  }

  const columns = [
    {
      title: t("general.action"),
      field: "custom",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center"
      },
      render: (rowData) => (
        <MaterialButton
          tabValue={state?.tabValue}
          item={rowData}
          onSelect={(rowData, method) => {
            let actionByMethod = {
              [appConst.active.edit]: handleEdit,
              [appConst.active.view]: handleView,
            }
            if (Object.keys(actionByMethod)?.includes(method.toString())) {
              actionByMethod[method]?.(rowData)
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    {
      title: t("ApprovalRequest.status"),
      field: "statusName",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center"
      },
    },
    {
      title: t("ApprovalRequest.submittingDate"),
      field: "code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData => formatTimestampToDate(rowData.submissionDate),
    },
    {
      title: t("ApprovalRequest.submittingDepartment"),
      field: "submittingDepartmentName",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("ApprovalRequest.submitter"),
      field: "submitterName",
      align: "left",
      minWidth: 180,
    },
    {
      title: t("ApprovalRequest.approvingDepartment"),
      field: "approvingDepartmentName",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("ApprovalRequest.approver"),
      field: "approverName",
      align: "left",
      minWidth: 180,
    },
    // {
    //   title: t("ApprovalRequest.requestType"),
    //   field: "code",
    //   align: "left",
    //   minWidth: 120,
    // },
  ];

  useEffect(() => {
    if (!state.shouldOpenApprovalRequestDialog) {
      updatePageData();
    }
  }, [
    state?.tabValue,
    state?.rowsPerPage,
    state?.page,
    state.shouldOpenApprovalRequestDialog,
    state.searchTrigger,
  ]);

  useEffect(() => {
    if (!isInValidDateForSubmit()) {
      search();
    }
  }, [
    filter?.fromDate,
    filter?.toDate,
    filter?.fromApprovalDate,
    filter?.toApprovalDate,
    filter?.approvingDepartment,
    filter?.submittingDepartment,
    filter?.approver,
    filter?.submitter,
  ]);

  const search = async () => {
    setPage(0);
  }

  const updatePageData = async () => {
    const {HISTORY, PENDING_REQUEST} = appConst.TABS_APPROVAL_REQUEST;
    try {
      setPageLoading(true);
      const searchObject = {};
      const apiFunc = {
        [HISTORY.code]: getAllRequest,
        [PENDING_REQUEST.code]: searchByPage,
      };

      searchObject.keyword = state?.keyword;
      searchObject.pageIndex = state?.page + 1;
      searchObject.pageSize = state?.rowsPerPage;
      searchObject.fromDate = formatDateDto(filter?.fromDate);
      searchObject.toDate = formatDateDto(filter?.toDate);
      searchObject.fromApprovalDate = formatDateDto(filter?.fromApprovalDate);
      searchObject.toApprovalDate = formatDateDto(filter?.toApprovalDate);
      searchObject.approvingDepartmentId = filter?.approvingDepartment?.id;
      searchObject.submittingDepartmentId = filter?.submittingDepartment?.id;
      searchObject.approverId = filter?.approver?.id;
      searchObject.submitterId = filter?.submitter?.id;

      const res = await apiFunc[state?.tabValue || HISTORY.code](searchObject);
      const {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setState(prevState => ({
          ...prevState,
          itemList: [...data.content],
          totalElements: data.totalElements,
        }));
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  const handleGetDetailRequest = async (id, trackerCode) => {
    try {
      const params = {
        trackerCode,
      }
      const res = await getItemById(id, params);
      const {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setItem({...(data || {})});
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  const handleEdit = (rowData) => {
    handleGetDetailRequest(rowData?.id, rowData?.trackerCode).then((data) => {
      handleOpenApprovalRequestDialog();
    })
  }

  const handleOpenApprovalRequestDialog = () => {
    setState({...state, shouldOpenApprovalRequestDialog: true})
  }

  const handleDialogClose = () => {
    setState(prevState => ({
      ...prevState,
      shouldOpenApprovalRequestDialog: false,
      shouldOpenConfirmationDialog: false,
      isView: null,
    }));
    setItem(null);
  };

  const setPage = (page) => {
    setState(prevState => ({
      ...prevState,
      page,
      searchTrigger: !prevState.searchTrigger,
    }));
  };

  const setRowsPerPage = (event) => {
    setState(prevState => ({
      ...prevState,
      rowsPerPage: event.target.value,
      page: 0,
      searchTrigger: !prevState.searchTrigger,
    }));
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleOKEditClose = async () => {
    setState(prevState => ({
      ...prevState,
      shouldOpenApprovalRequestDialog: false,
      shouldOpenConfirmationDialog: false,
      isView: null,
    }));
    setItem(null);
    await search();
  };

  const handleChangeTabValue = async (event, newValue) => {
    setState(prevState => ({
      ...prevState,
      itemList: [],
      tabValue: newValue,
      page: 0,
      rowsPerPage: state?.rowsPerPage,
    }));
  };

  const handleView = (rowData) => {
    handleOpenApprovalHistory(rowData?.objectId, rowData?.trackerCodeRelate);
  }

  const handleOpenApprovalHistory = (objectId, trackerCodeRelate) => {
    setState(prevState => ({
      ...prevState,
      objectId,
      trackerCodeRelate,
      shouldOpenApprovalHistory: true,
    }))
  }

  const handleCloseApprovalHistory = () => {
    setState(prevState => ({
      ...prevState,
      objectId: "",
      trackerCodeRelate: "",
      shouldOpenApprovalHistory: false,
    }))
  }

  const handleSelectData = (value, name) => {
    setFilter(prevState => ({
      ...prevState,
      [name]: value,
    }))
  }

  return (
    <div className="m-sm-30">
      <Helmet>
        <title>
          {TitlePage} | {t("web_site")}
        </title>
      </Helmet>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            { name: TitlePage },
          ]}
        />
      </div>
      <Grid container spacing={2} justifyContent="space-between">
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={state?.tabValue}
            onChange={handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            {appConst.listTabsApprovalRequest.map(tab => (
              <Tab
                className="tab"
                value={tab.code}
                label={tab.name}
              />
            ))}
          </Tabs>
        </AppBar>

        <Card elevation={2} className="my-12 w-100">
          <CardContent>
            <ValidatorForm onSubmit={() => {}}>
              <Grid container spacing={2}>
                <Grid item md={3} xs={12}>
                  <ValidatePicker
                    label={t("ApprovalRequest.fromDate")}
                    name="fromDate"
                    value={filter?.fromDate || null}
                    onChange={handleSelectData}
                    maxDate={filter?.toDate}
                    maxDateMessage={"Phải nhỏ hơn đến ngày (Ngày duyệt)"}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <ValidatePicker
                    label={t("ApprovalRequest.toDate")}
                    name="toDate"
                    value={filter?.toDate || null}
                    onChange={handleSelectData}
                    minDate={filter?.fromDate}
                    maxDateMessage={"Phải lớn hơn từ ngày (Ngày duyệt)"}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <ValidatePicker
                    label={t("ApprovalRequest.fromApprovalDate")}
                    name="fromApprovalDate"
                    value={filter?.fromApprovalDate || null}
                    onChange={handleSelectData}
                    maxDate={filter?.toApprovalDate}
                    maxDateMessage={"Phải nhỏ hơn đến ngày (Ngày duyệt)"}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <ValidatePicker
                    label={t("ApprovalRequest.toApprovalDate")}
                    name="toApprovalDate"
                    value={filter?.toApprovalDate || null}
                    onChange={handleSelectData}
                    minDate={filter?.fromApprovalDate}
                    maxDateMessage={"Phải lớn hơn từ ngày (Ngày duyệt)"}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("ApprovalRequest.approvingDepartment")}
                    searchFunction={searchByPageDepartmentNew}
                    searchObject={searchDepartmentObject}
                    typeReturnFunction="category"
                    listData={filter?.listApprovingDepartment}
                    setListData={handleSelectData}
                    nameListData="listApprovingDepartment"
                    displayLable={"name"}
                    isNoRenderChildren
                    isNoRenderParent
                    name="appovingDepartment"
                    showCode="code"
                    value={filter?.approvingDepartment}
                    onSelect={handleSelectData}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("ApprovalRequest.submittingDepartment")}
                    searchFunction={searchByPageDepartmentNew}
                    searchObject={searchDepartmentObject}
                    typeReturnFunction="category"
                    listData={filter?.listSubmittingDepartment}
                    setListData={handleSelectData}
                    nameListData="listSubmittingDepartment"
                    displayLable={"name"}
                    isNoRenderChildren
                    isNoRenderParent
                    name="submittingDepartment"
                    showCode="code"
                    value={filter?.submittingDepartment || null}
                    onSelect={handleSelectData}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("ApprovalRequest.approver")}
                    searchFunction={getListUserByDepartmentId}
                    searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                    nameListData="listApprover"
                    listData={filter?.listApprover}
                    setListData={handleSelectData}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    name="approver"
                    value={filter?.approver || null}
                    onSelect={handleSelectData}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("ApprovalRequest.submitter")}
                    searchFunction={getListUserByDepartmentId}
                    searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                    nameListData="listSubmitter"
                    listData={filter?.listApprover || null}
                    setListData={handleSelectData}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    name="submitter"
                    value={filter?.submitter || null}
                    onSelect={handleSelectData}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              </Grid>
            </ValidatorForm>
          </CardContent>
        </Card>

        <Grid item xs={12}>
          <div>
            {state?.shouldOpenApprovalRequestDialog && (
              <ApprovalRequestDialog
                t={t}
                handleClose={handleDialogClose}
                open={state?.shouldOpenApprovalRequestDialog}
                handleOKEditClose={handleOKEditClose}
                item={item}
              />
            )}

            {state?.shouldOpenApprovalHistory && (
              <ApprovalProcessHistoryPopup
                t={t}
                handleClose={handleCloseApprovalHistory}
                open={state?.shouldOpenApprovalHistory}
                objectId={state?.objectId}
                trackerCodeRelate={state?.trackerCodeRelate}
              />
            )}

          </div>
          <MaterialTable
            title={t("general.list")}
            data={state?.itemList}
            columns={columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              sorting: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: state?.itemList?.length > 0
                ? 0 : 250,
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
          />
          <TablePagination
            {...defaultPaginationProps()}
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            count={state?.totalElements}
            rowsPerPage={state?.rowsPerPage}
            page={state?.page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={setRowsPerPage}
          />
        </Grid>
      </Grid>
    </div>
  )
}
export default ApprovalRequest;
