import {EgretLoadable} from "egret";
import ConstantList from "../../appConfig";
import {withTranslation} from 'react-i18next';
import {ROUTES_PATH} from "../../appConst";

const ProductType = EgretLoadable({
  loader: () => import("./ApprovalRequest")
});
const ViewComponent = withTranslation()(ProductType);

const ApprovalRequestRoutes = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.APPROVAL_REQUEST,
    exact: true,
    component: ViewComponent
  }
];

export default ApprovalRequestRoutes;
