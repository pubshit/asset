import {appConst} from "../../appConst";


export const defaultState = {
  firstLevelSubmissionDate: null,
  firstLevelSubmittingDepartment: null,
  firstLevelSubmitter: null,
  firstLevelSubmissionContent: null,
  firstLevelApprovingDepartment: null,
  firstLevelApprover: null,
  firstLevelApprovalContent: null,
  firstLevelApprovalDate: null,
  newStatusCode: "",
  newStatusId: null,
  newStatusName: "",
  oldStatusCode: null,
  oldStatusId: null,
  oldStatusName: null,
  secondLevelApprover: null,
  secondLevelApproverId: null,
  secondLevelApproverName: null,
  secondLevelApprovingDepartment: null,
  secondLevelApprovingDepartmentId: null,
  secondLevelApprovingDepartmentName: null,
  secondLevelSubmissionDate: null,
  secondLevelApprovalDate: null,
  secondLevelApprovalContent: null,
}

export const defaultPermission = {
  firstLevelSubmissionDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmittingDepartment: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmittingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmitter: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmitterId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmissionContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovingDepartment: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprover: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApproverId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovalDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovalContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovingDepartment: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprover: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApproverId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelSubmissionDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovalDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovalContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
};

export const defaultListData = {
  listDepartment: [],
  listPerson: [],
};
