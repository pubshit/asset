import React, {memo} from "react";
import {Grid} from "@material-ui/core";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import {Label} from "../../Component/Utilities";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {searchByPageDepartmentNew} from "../../Department/DepartmentService";
import {filterOptions, getUserInformation} from "../../../appFunction";
import {TextValidator} from "react-material-ui-form-validator";
import {defaultPermission, defaultState} from "../constant";
import {appConst} from "../../../appConst";
import {searchByPageNew} from "../../User/UserService";

export const ViewComponent = memo((props) => {
  const {organization} = getUserInformation();
  const {
    t,
    permission = {...defaultPermission},
    item = {...defaultState},
    listData = {},
    handleChangeSelect,
    handleChangeListData,
    handleChangeText,
    handleSelectDepartment,
  } = props;
  const {
    firstLevelSubmissionDate,
    firstLevelSubmittingDepartment,
    firstLevelSubmitter,
    firstLevelSubmissionContent,
    firstLevelApprovalDate,
    firstLevelApprovingDepartment,
    firstLevelApprover,
    firstLevelApprovalContent,
    secondLevelSubmissionDate,
    secondLevelApprovingDepartment,
    secondLevelApprover,
    secondLevelApprovalDate,
    secondLevelApprovalContent,
  } = item;
  const submittingDepartmentSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  };
  const submitterSearchObject = {
    departmentId: firstLevelSubmittingDepartment?.id ?? '',
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };
  const approvingDepartmentSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  };
  const approverSearchObject = {
    departmentId: firstLevelApprovingDepartment?.id ?? '',
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };
  const secondApproverSearchObject = {
    // departmentId: secondLevelApprovingDepartment?.id ?? '',
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };

  return (
    <>
      {/*Thông tin trình ký*/}
      <Grid hidden={!permission.firstLevelSubmissionDate.isShow} item xs={4}>
        <ValidatePicker
          label={
            <Label isRequired={permission.firstLevelSubmissionDate.isRequired}>
              {t("ApprovalRequest.submittingDate")}
            </Label>
          }
          name="firstLevelSubmissionDate"
          readOnly={permission.firstLevelSubmissionDate.isView}
          value={firstLevelSubmissionDate || null}
          onChange={handleChangeSelect}
          maxDate={new Date()}
          maxDateMessage={t("general.maxDateNow")}
          disabled={permission.firstLevelSubmissionDate.isDisabled}
          validators={
            permission.firstLevelSubmissionDate.isRequired ? ['required'] : []
          }
          errorMessages={t('general.required')}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmittingDepartmentId.isShow} item xs={4}>
        <AsynchronousAutocompleteSub
          searchFunction={searchByPageDepartmentNew}
          searchObject={submittingDepartmentSearchObject}
          label={
            <Label isRequired={permission.firstLevelSubmittingDepartmentId.isRequired}>
              {t("ApprovalRequest.submittingDepartment")}
            </Label>
          }
          listData={listData?.listDepartment}
          nameListData="listDepartment"
          setListData={handleChangeListData}
          typeReturnFunction="category"
          isNoRenderChildren
          isNoRenderParent
          displayLable="name"
          readOnly={permission.firstLevelSubmittingDepartmentId.isView}
          name="firstLevelSubmittingDepartment"
          value={firstLevelSubmittingDepartment || null}
          onSelect={handleSelectDepartment}
          filterOptions={filterOptions}
          disabled={permission.firstLevelSubmittingDepartmentId.isDisabled}
          validators={
            permission.firstLevelSubmittingDepartmentId.isRequired ? ['required'] : []
          }
          errorMessages={[t("general.required")]}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmitterId.isShow} item xs={4}>
        <AsynchronousAutocompleteSub
          label={
            <Label isRequired={permission.firstLevelSubmitterId.isRequired}>
              {t("ApprovalRequest.submitter")}
            </Label>
          }
          listData={listData?.listFirstLevelSubmitter || []}
          setListData={handleChangeListData}
          nameListData="listFirstLevelSubmitter"
          searchFunction={searchByPageNew}
          searchObject={submitterSearchObject}
          displayLable="displayName"
          typeReturnFunction="category"
          name="firstLevelSubmitter"
          readOnly={permission.firstLevelSubmitterId.isView}
          value={firstLevelSubmitter || null}
          onSelect={handleChangeSelect}
          disabled={permission.firstLevelSubmitterId.isDisabled}
          validators={
            permission.firstLevelSubmitterId.isRequired ? ['required'] : []
          }
          errorMessages={[t("general.required")]}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmissionContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={
            <Label isRequired={permission.firstLevelSubmissionContent.isRequired}>
              {t("ApprovalRequest.submissionContent")}
            </Label>
          }
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: permission.firstLevelSubmissionContent.isView
          }}
          value={firstLevelSubmissionContent || ""}
          name="firstLevelSubmissionContent"
          onChange={handleChangeText}
          disabled={permission.firstLevelSubmissionContent.isDisabled}
          validators={
            permission.firstLevelSubmissionContent.isRequired ? ['required'] : []
          }
          errorMessages={[t("Asset.please_select_asset")]}
        />
      </Grid>

      {/*Thông tin duyệt*/}
      <Grid hidden={!permission.firstLevelApprovalDate.isShow} item xs={4}>
        <ValidatePicker
          label={
            <Label isRequired={permission.firstLevelApprovalDate.isRequired}>
              {t("ApprovalRequest.approvalDate")}
            </Label>
          }
          name="firstLevelApprovalDate"
          readOnly={permission.firstLevelApprovalDate.isView}
          value={firstLevelApprovalDate || null}
          onChange={handleChangeSelect}
          maxDate={new Date()}
          maxDateMessage={t("general.maxDateNow")}
          disabled={permission.firstLevelApprovalDate.isDisabled}
          validators={
            permission.firstLevelApprovalDate.isRequired ? ['required'] : []
          }
          errorMessages={t('general.required')}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApprovingDepartmentId.isShow} item xs={4}>
        <AsynchronousAutocompleteSub
          searchFunction={searchByPageDepartmentNew}
          searchObject={approvingDepartmentSearchObject}
          label={
            <Label isRequired={permission.firstLevelApprovingDepartmentId.isRequired}>
              {t("ApprovalRequest.approvingDepartment")}
            </Label>
          }
          listData={listData?.listDepartment}
          nameListData="listDepartment"
          setListData={handleChangeListData}
          typeReturnFunction="category"
          isNoRenderChildren
          isNoRenderParent
          displayLable="name"
          readOnly={permission.firstLevelApprovingDepartmentId.isView}
          name="firstLevelApprovingDepartment"
          value={firstLevelApprovingDepartment || null}
          onSelect={handleSelectDepartment}
          filterOptions={filterOptions}
          disabled={permission.firstLevelApprovingDepartmentId.isDisabled}
          validators={
            permission.firstLevelApprovingDepartmentId.isRequired ? ['required'] : []
          }
          errorMessages={[t("general.required")]}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApproverId.isShow} item xs={4}>
        <AsynchronousAutocompleteSub
          label={
            <Label isRequired={permission.firstLevelApproverId.isRequired}>
              {t("ApprovalRequest.approver")}
            </Label>
          }
          listData={listData?.listFirstLevelApprover || []}
          setListData={handleChangeListData}
          nameListData="listFirstLevelApprover"
          searchFunction={searchByPageNew}
          searchObject={approverSearchObject}
          displayLable="displayName"
          typeReturnFunction="category"
          name="firstLevelApprover"
          readOnly={permission.firstLevelApproverId.isView}
          value={firstLevelApprover || null}
          onSelect={handleChangeSelect}
          disabled={permission.firstLevelApproverId.isDisabled}
          validators={
            permission.firstLevelApproverId.isRequired ? ['required'] : []
          }
          errorMessages={[t("general.required")]}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApprovalContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={
            <Label isRequired={permission.firstLevelApprovalContent.isRequired}>
              {t("ApprovalRequest.approvalContent")}
            </Label>
          }
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: permission.firstLevelApprovalContent.isView
          }}
          value={firstLevelApprovalContent || ""}
          name="firstLevelApprovalContent"
          onChange={handleChangeText}
          disabled={permission.firstLevelApprovalContent.isDisabled}
          validators={
            permission.firstLevelApprovalContent.isRequired ? ['required'] : []
          }
          errorMessages={[t("Asset.please_select_asset")]}
        />
      </Grid>

      {/* Trình cấp 2*/}
      <Grid hidden={!permission.secondLevelSubmissionDate.isShow} item xs={3}>
        <ValidatePicker
          label={
            <Label isRequired={permission.secondLevelSubmissionDate.isRequired}>
              {t("ApprovalRequest.secondLevelSubmissionDate")}
            </Label>
          }
          name="secondLevelSubmissionDate"
          readOnly={permission.secondLevelSubmissionDate.isView}
          value={secondLevelSubmissionDate || null}
          onChange={handleChangeSelect}
          minDate={secondLevelSubmissionDate}
          minDateMessage={"Không được nhỏ hơn ngày trình ký"}
          maxDate={new Date()}
          maxDateMessage={t("general.maxDateNow")}
          disabled={permission.secondLevelSubmissionDate.isDisabled}
          validators={
            permission.secondLevelSubmissionDate.isRequired ? ['required'] : []
          }
          errorMessages={t('general.required')}
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApprovingDepartmentId.isShow} item xs={3}>
        <AsynchronousAutocompleteSub
          searchFunction={searchByPageDepartmentNew}
          searchObject={approvingDepartmentSearchObject}
          label={t("ApprovalRequest.secondLevelApprovingDepartment")}
          listData={listData?.listDepartment}
          nameListData="listDepartment"
          setListData={handleChangeListData}
          typeReturnFunction="category"
          isNoRenderChildren
          isNoRenderParent
          displayLable="name"
          readOnly={permission.secondLevelApprovingDepartmentId.isView}
          name="secondLevelApprovingDepartment"
          value={secondLevelApprovingDepartment || null}
          onSelect={handleSelectDepartment}
          filterOptions={filterOptions}
          disabled={permission.secondLevelApprovingDepartmentId.isDisabled}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApproverId.isShow} item xs={3}>
        <AsynchronousAutocompleteSub
          label={
            <Label isRequired={permission.secondLevelApproverId.isRequired}>
              {t("ApprovalRequest.secondLevelApprover")}
            </Label>
          }
          listData={listData?.listSecondLevelApprover || []}
          setListData={handleChangeListData}
          nameListData="listSecondLevelApprover"
          searchFunction={searchByPageNew}
          searchObject={secondApproverSearchObject}
          displayLable="displayName"
          typeReturnFunction="category"
          name="secondLevelApprover"
          readOnly={permission.secondLevelApproverId.isView}
          value={secondLevelApprover || null}
          onSelect={handleChangeSelect}
          disabled={permission.secondLevelApproverId.isDisabled}
          validators={
            permission.secondLevelApproverId.isRequired ? ['required'] : []
          }
          errorMessages={[t("general.required")]}
          noOptionsText={t("general.noOption")}
        />
      </Grid>

      {/* Duyệt cấp 2 */}
      <Grid hidden={!permission.secondLevelApprovalDate.isShow} item xs={3}>
        <ValidatePicker
          label={
            <Label isRequired={permission.secondLevelApprovalDate.isRequired}>
              {t("ApprovalRequest.secondLevelApprovalDate")}
            </Label>
          }
          readOnly={permission.secondLevelApprovalDate.isView}
          name="secondLevelApprovalDate"
          value={secondLevelApprovalDate || null}
          onChange={handleChangeSelect}
          minDate={secondLevelSubmissionDate}
          minDateMessage={"Không được nhỏ hơn ngày trình cấp 2"}
          maxDate={new Date()}
          maxDateMessage={t("general.maxDateNow")}
          disabled={permission.secondLevelApprovalDate.isDisabled}
          errorMessages={t('general.required')}
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApprovalContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={
            <Label isRequired={permission.secondLevelApprovalContent.isRequired}>
              {t("ApprovalRequest.secondLevelApprovalContent")}
            </Label>
          }
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: permission.secondLevelApprovalContent.isView
          }}
          value={secondLevelApprovalContent || ""}
          name="secondLevelApprovalContent"
          onChange={handleChangeText}
          disabled={permission.secondLevelApprovalContent.isDisabled}
          validators={
            permission.secondLevelApprovalContent.isRequired ? ['required'] : []
          }
          errorMessages={[t("Asset.please_select_asset")]}
        />
      </Grid>
    </>
  )
})