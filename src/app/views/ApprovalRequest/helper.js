import {defaultPermission, defaultState} from "./constant";
import {checkMinDate, formatTimestampToDate} from "../../appFunction";
import {TRACKER_STATUS_CODE} from "../workflows/constant";
import {toast} from "react-toastify";

export const convertItem = (value, item) => {

  return ({
    ...value,
    oldStatusId: item?.statusId,
    oldStatusCode: item?.statusCode,
    oldStatusName: item?.statusName,
    firstLevelSubmittingDepartment: value?.firstLevelSubmittingDepartmentId
      ? {
        id: value?.firstLevelSubmittingDepartmentId,
        name: value?.firstLevelSubmittingDepartmentName,
      }
      : null,
    firstLevelSubmitter: value?.firstLevelSubmitterId
      ? {
        personId: value?.firstLevelSubmitterId,
        displayName: value?.firstLevelSubmitterName,
      }
      : null,
    firstLevelApprovingDepartment: value?.firstLevelApprovingDepartmentId
      ? {
        id: value?.firstLevelApprovingDepartmentId,
        name: value?.firstLevelApprovingDepartmentName,
      }
      : null,
    firstLevelApprover: value?.firstLevelApproverId
      ? {
        personId: value?.firstLevelApproverId,
        displayName: value?.firstLevelApproverName,
      }
      : null,
    secondLevelApprovingDepartment: value?.secondLevelApprovingDepartmentId
      ? {
        id: value?.secondLevelApprovingDepartmentId,
        name: value?.secondLevelApprovingDepartmentName,
      }
      : null,
    secondLevelApprover: value?.secondLevelApproverId
      ? {
        personId: value?.secondLevelApproverId,
        displayName: value?.secondLevelApproverName,
      }
      : null,
  })
}

export const refreshPermission = (
  permission = {...defaultPermission},
  setPermission = (permission = ((prevState = defaultPermission) => defaultPermission) || defaultPermission) => {}
) => {
  let newPermission = {...permission};
  Object.keys(newPermission)?.map((key) => {
    newPermission[key].isShow = false;
    newPermission[key].isDisabled = false;
    newPermission[key].isView = false;
    newPermission[key].isRequired = false;
  })

  setPermission?.(newPermission);
}

export const validateSubmit = (state = {...defaultState}, wfData = {}) => {
  const {
    FIRST_LV_PENDING_APPROVAL,
    SECOND_LEVEL_APPROVAL_PROCESS,
    SECOND_LEVEL_APPROVED,
    REQUESTED_ADDITION,
  } = TRACKER_STATUS_CODE.APPROVAL_PROCESSES;
  const {
    firstLevelSubmissionDate,
    firstLevelSubmittingDepartment,
    firstLevelSubmitter,
    firstLevelSubmissionContent,
    firstLevelApprovalDate,
    firstLevelApprovingDepartment,
    firstLevelApprover,
    firstLevelApprovalContent,
    secondLevelSubmissionDate,
    secondLevelApprovingDepartment,
    secondLevelApprover,
    secondLevelApprovalDate,
    secondLevelApprovalContent,
  } = state;
  let is1stLevelSubmitting = wfData?.statusNew?.statusCode === FIRST_LV_PENDING_APPROVAL.code;
  let is2ndLevelSubmitting = wfData?.statusNew?.statusCode === SECOND_LEVEL_APPROVAL_PROCESS.code;
  let is2ndLevelApproval = wfData?.statusNew?.statusCode === SECOND_LEVEL_APPROVED.code;

  if (is2ndLevelSubmitting && checkMinDate(firstLevelApprovalDate, firstLevelSubmissionDate)) {
    toast.warning("Ngày duyệt không đuợc nhỏ hơn ngày trình ký");
    return true;
  } else if (is1stLevelSubmitting && !firstLevelSubmissionDate) {
    toast.warning("Chưa nhập ngày trình ký");
    return true;
  } else if (is1stLevelSubmitting && firstLevelSubmittingDepartment?.id === firstLevelApprovingDepartment?.id) {
    toast.warning("Phòng ban duyệt không được trùng với phòng ban trình");
    return true;
  } else if (is1stLevelSubmitting && firstLevelSubmitter?.personId === firstLevelApprover?.personId) {
    toast.warning("Người duyệt không được trùng với người trình");
    return true;
  } else if (checkMinDate(secondLevelSubmissionDate, firstLevelApprovalDate)) {
    toast.warning("Ngày trình ký cấp 2 không đuợc nhỏ hơn ngày duyệt");
    return true;
  } else if ((is2ndLevelSubmitting || is2ndLevelApproval) && !firstLevelApprovalDate) {
    toast.warning("Chưa nhập ngày duyệt");
    return true;
  } else if (
    (is2ndLevelSubmitting || is2ndLevelApproval)
    && secondLevelApprovingDepartment?.id === firstLevelApprovingDepartment?.id
  ) {
    toast.warning("Phòng ban duyệt không được trùng với phòng ban duyệt cấp 2");
    return true;
  } else if ((is2ndLevelSubmitting || is2ndLevelApproval) && secondLevelApprover?.id === firstLevelApprover?.id) {
    toast.warning("Người duyệt không được trùng với người duyệt cấp 2");
    return true;
  } else if (is2ndLevelApproval && checkMinDate(secondLevelApprovalDate, secondLevelSubmissionDate)) {
    toast.warning("Ngày duyệt cấp 2 không đuợc nhỏ hơn ngày trình cấp 2");
    return true;
  } else if ((is2ndLevelSubmitting || is2ndLevelApproval) && !secondLevelSubmissionDate) {
    toast.warning("Chưa nhập ngày trình ký cấp 2");
    return true;
  } else if (is2ndLevelApproval && !secondLevelApprovalDate) {
    toast.warning("Chưa nhập ngày duyệt cấp 2");
    return true;
  } else if (is1stLevelSubmitting && !firstLevelSubmissionContent) {
    toast.warning("Chưa nhập nội dung trình ký");
    return true;
  } else if (is2ndLevelSubmitting && !firstLevelApprovalContent) {
    toast.warning("Chưa nhập nội dung duyệt");
    return true;
  } else if (is2ndLevelApproval && !secondLevelApprovalContent) {
    toast.warning("Chưa nhập nội dung duyệt cấp 2");
    return true;
  } else if (is1stLevelSubmitting && !firstLevelSubmitter?.personId) {
    toast.warning("Chưa chọn người trình ký");
    return true;
  } else if (is2ndLevelSubmitting && !firstLevelApprover?.personId) {
    toast.warning("Chưa chọn người trình duyệt");
    return true;
  } else if (is2ndLevelApproval && !secondLevelApprover) {
    toast.warning("Chưa chọn người trình duyệt cấp 2");
    return true;
  }
  return false;
}

export const submittingDepartmentWaterfall = (state) => {
  return state?.firstLevelApprovingDepartment?.id || state?.firstLevelSubmittingDepartment?.id
}

export const approvingDepartmentWaterfall = (state) => {
  return state?.firstLevelApprovingDepartment?.id || state?.secondLevelApprovingDepartment?.id
}

export const submitterWaterfall = (state) => {
  return state?.firstLevelApprover?.personId || state?.firstLevelSubmitter?.personId
}

export const approverWaterfall = (state) => {
  return state?.secondLevelApprover?.personId || state?.firstLevelApprover?.personId
}

export const submissionDateWaterfall = (state) => {
  return state?.secondLevelSubmissionDate
    || state?.firstLevelApprovalDate
    || state?.firstLevelSubmissionDate
}

export const approvalnDateWaterfall = (state) => {
  return state?.secondLevelApprovalDate
    || state?.secondLevelSubmissionDate
    || state?.firstLevelApprovalDate
}
