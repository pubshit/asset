import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  TablePagination,
  IconButton,
  Icon,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { getPurchaseRequest, getItemById } from "./PurchaseRequestCountService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import moment from "moment";
import PurchaseRequestEditorDialog from "../PurchaseRequest/PurchaseRequestEditorDialog";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import PurchasePlaningPrint from "./PurchasePlaningPrint"
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: "-1.5em",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}
class PurchaseRequestCountDialog extends Component {
  state = {
    name: "",
    code: "",
    shouldOpenNotificationPopup: false,
    page: 0,
    rowsPerPage: 10,
    totalElements: 0,
    productName: "",
    shouldOpenViewDialog: false,
    item: {},
    type: 1,
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.type = 1;
    searchObject.keyword = this.state.productName;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.formToDate = this.state.formToDate;
    searchObject.formDate = this.state.formDate;
    getPurchaseRequest(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  exportExcel = () => {

    this.setState({ loading: true })
    var searchObject = {};
    searchObject.id = this.props.id
    searchObject.dateExport = moment(this.state.date).format('DD/MM/YYYY');
    searchObject.lineOne = this.state.lineOne
    searchObject.lineTwo = this.state.lineTwo
    searchObject.lineThree = this.state.lineThree
    searchObject.signName = this.state.signName
    searchObject.isFormal = this.state.isFormal
    searchObject.fromUploadedFile = this.state.fromUploadedFile
    // getExcel(searchObject).then((result) => {
    //   const url = window.URL.createObjectURL(new Blob([result.data]))
    //   const link = document.createElement('a')
    //   link.href = url
    //   link.setAttribute('download', this.props.code + ".xls")
    //   document.body.appendChild(link)
    //   link.click();
    //   this.setState({ loading: false })
    // })
    this.props.handleClose()
  }

  getFileExport = () => {
    this.setState({ loading: true })
    var searchObject = {}
    searchObject.id = this.props.id

    this.setState({ shouldOpenExportDialog: true, isPrint: true })
  }

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.type = 1;
      searchObject.keyword = this.state.productName;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.fromDate = this.state.fromDate;
      getPurchaseRequest(searchObject).then(({ data }) => {
        let itemListClone = [...data.content];
        itemListClone.forEach((el) => {
          el.purchaseRequest.listProducts.forEach((item) => {
            if (
              el.product.id == item.product.id &&
              el.purchaseRequest.id == item.purchaseRequest.id
            ) {
              el.quantity = item.quantity;
            }
          });
        });
        this.setState({
          itemList: [...itemListClone],
          totalElements: data.totalElements,
        });
      });
    });
  }
  componentDidMount() {
    this.updatePageData();
  }
  componentWillMount() {
    let { open, handleClose, item, productName, type } = this.props;
    this.setState({ productName, type }, function () {
      this.setState(
        {
          ...this.props.item,
        },
        function () {
          if (this.state.productName) {
            this.search();
          }
        }
      );
    });
  }
  componentDidMount() { }
  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenViewDialog: false,
      shouldOpenExportDialog: false,
    });
  };

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;

    let {
      name,
      code,
      shouldOpenNotificationPopup,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      productName,
      shouldOpenViewDialog,
      item, loading, shouldOpenExportDialog,
    } = this.state;
    let TitlePage = t("purchaseRequest.title");
    let columns = [
      {
        title: t("purchaseRequest.requestDepartment"),
        field: "purchaseRequest.requestDepartment.name",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
      },
      {
        title: t("purchaseRequest.receptionDepartment"),
        field: "purchaseRequest.receptionDepartment.name",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "0px",
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
      },
      {
        title: t("purchaseRequest.requestDate"),
        field: "purchaseRequest.requestDate",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        render: (rowData) =>
          rowData.purchaseRequest.requestDate ? (
            <span>
              {moment(rowData.purchaseRequest.requestDate).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("purchaseRequest.status"),
        field: "purchaseRequest.status.name",
        align: "left",
        width: "150",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0px",
        },
      },
      {
        title: t("purchase_request_count.quantity"),
        field: "quantity",
        align: "right",
        width: "90px",
        headerStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
        },
        cellStyle: {
          paddingLeft: "0px",
          paddingRight: "10px",
          textAlign: "right",
        },
      },
      {
        title: t("Asset.action"),
        field: "custom",
        align: "left",
        width: "100px",
        headerStyle: {
          paddingLeft: "10px",
          paddingRight: "0",
        },
        cellStyle: {
          paddingLeft: "10px",
          paddingRight: "0",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.purchaseRequest.id).then(({ data }) => {
                  if (data === null) {
                    data = {};
                  }
                  this.setState({
                    item: data,
                    shouldOpenViewDialog: true,
                  });
                });
              }
            }}
          />
        ),
      },
    ];
    return (
      <Dialog open={open} maxWidth="md" PaperComponent={PaperComponent}>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px", overflow: "hidden" }}
          id="draggable-dialog-title"
        >
          {t(this.state.productName)}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <div>
              {shouldOpenViewDialog && (
                <PurchaseRequestEditorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenViewDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  productName={productName}
                  isCheck={false}
                />
              )}
            </div>
            <Grid item xs={12}>
              <MaterialTable
                title={t("general.list")}
                data={itemList}
                columns={columns}
                //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
                parentChildData={(row, rows) => {
                  var list = rows.find((a) => a.id === row.parentId);
                  return list;
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                  toolbar: {
                    // nRowsSelected: '${t('general.selects')}',
                    nRowsSelected: `${t("general.selects")}`,
                  },
                }}
                options={{
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  padding: "dense",
                  toolbar: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                  }),
                  maxBodyHeight: "350px",
                  minBodyHeight: "350px",
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                  // this.setState({selectedItems:rows});
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                component="div"
                count={totalElements}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.setRowsPerPage}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="primary"
                className="mr-36"
                onClick={() => this.getFileExport()}>
                In
              </Button>
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>
            </div>
          </DialogActions>
          {shouldOpenExportDialog && (
            <PurchasePlaningPrint
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={this.state.isPrint}
              productName={this.props.productName}
              config={this.state}
              handleOKEditClose={this.props.handleOKEditClose}
              item={this.props.item}

            />
          )}
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default PurchaseRequestCountDialog;
