import React, { useContext } from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Grid,
  TablePagination,
} from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { ConfirmationDialog } from "egret";
import MaterialTable, { MTableToolbar } from "material-table";
import PurchaseRequestCountDialog from "./Dialog/PurchaseRequestCountDialog";
import { appConst } from "app/appConst";
import { ValidatorForm } from "react-material-ui-form-validator";
import viLocate from "date-fns/locale/vi";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import PlaningDialog from "../PurchasePlaning/PlaningDialog";
import {purchaseRequestCountPrintData} from "../FormCustom/PurchaseRequestCount";
import {defaultPaginationProps} from "../../appFunction";

function ComponentPurchaseRequestCountTable(props) {
  let {
    t,
    i18n,
    handleAddPlanning,
    handleSetData,
    handleDialogClose,
    handleOKEditClose,
    handleConfirmationResponse,
    columns,
    handleChangePage,
    setRowsPerPage,
    getRowData
  } = props;
  let {
    item,
    page,
    toDate,
    isView,
    isPrint,
    itemList,
    fromDate,
    rowsPerPage,
    totalElements,
    shouldOpenEditorDialog,
    shouldOpenConfirmationDialog,
    purchasePreparePlan
  } = props?.item;
  const { ASSET_PURCHASING } = LIST_PRINT_FORM_BY_ORG.SHOPPING_MANAGEMENT;
  const { currentOrg } = useContext(AppContext);
  const dataView = purchaseRequestCountPrintData(item);

  return (
    <>
      <ValidatorForm onSubmit={() => { }}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocate}>
              <KeyboardDatePicker
                margin="none"
                fullWidth
                autoOk
                label={t("purchase_request_count.formDate")}
                format="dd/MM/yyyy"
                value={fromDate ?? null}
                maxDate={toDate || undefined}
                name={"fromDate"}
                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                onChange={(date) => handleSetData(date, "fromDate")}
                KeyboardButtonProps={{ "aria-label": "change date" }}
                invalidDateMessage={t("general.invalidDateFormat")}
                minDateMessage={t("general.minDateMessage")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocate}>
              <KeyboardDatePicker
                margin="none"
                fullWidth
                autoOk
                label={t("purchase_request_count.formToDate")}
                format="dd/MM/yyyy"
                value={toDate ?? null}
                minDate={fromDate || undefined}
                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                onChange={(date) => handleSetData(date, "toDate")}
                KeyboardButtonProps={{ "aria-label": "change date" }}
                invalidDateMessage={t("general.invalidDateFormat")}
                maxDateMessage={t("general.maxDateDefault")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item xs={12}>

            {isPrint && (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={dataView || item}
                title={t("Phiếu tổng hợp nhu cầu đầu tư trang thiết bị")}
                urls={[
                  ...ASSET_PURCHASING.PURCHASE_REQUEST_COUNT.GENERAL,
                  ...(ASSET_PURCHASING.PURCHASE_REQUEST_COUNT[currentOrg?.printCode] || []),
                ]}
              />
            )}

            <div>
              {shouldOpenEditorDialog && (
                <PurchaseRequestCountDialog
                  t={t}
                  i18n={i18n}
                  handleClose={handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={handleOKEditClose}
                  item={item}
                  isView={isView}
                  handleAddPlanning={handleAddPlanning}
                  type={appConst.TYPE_PURCHASE.TSCD_CCDC}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={handleDialogClose}
                  onYesClick={handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                toolbar: false,
                sorting: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    purchasePreparePlan?.id === rowData.id
                      ? "#ccc" :
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: "dense",
              }}
              onRowClick={(e, rowData) => {
                getRowData(rowData)
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </Grid>
      </ValidatorForm>
    </>
  )
}

export default ComponentPurchaseRequestCountTable;
