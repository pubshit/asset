import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-prepare-plan";
const API_PATH_PRODUCTS =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-products";

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/search";
  return axios.post(url, searchObject);
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const getPurchaseRequest = (searchObject) => {
  var url = API_PATH + "/getPurchaseRequest";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT +
      "/api/fileDownload/purchaseRequestCountToExcel",
    data: searchObject,
    responseType: "blob",
  });
};
export const exportToExcelV2 = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/list-product-purchase-prepare-plan",
    data: searchObject,
    responseType: "blob",
  });
};

export const addNewPurchaseRequestCount = (PurchaseRequest) => {
  return axios.post(API_PATH, PurchaseRequest);
};
export const updateNewPurchaseRequestCount = (PurchaseRequest) => {
  return axios.put(API_PATH + `/${PurchaseRequest.id}`, PurchaseRequest);
};

export const getRequestsProducts = (searchObject) => {
  var url = API_PATH_PRODUCTS + "/requests";
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      purchaseRequestIds: searchObject?.purchaseRequestIds.toString(),
    },
  };
  return axios.get(url, config);
};

export const getPreparePlanProducts = (searchObject) => {
  var url = API_PATH_PRODUCTS + "/prepare-plans";
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      purchasePreparePlanIds: searchObject?.purchasePreparePlanIds.toString(),
      status: searchObject?.status,
      requestDepartmentId: searchObject?.requestDepartmentId,
    },
  };
  return axios.get(url, config);
};

export const getByPrepareId = (searchObject) => {
  var url = API_PATH_PRODUCTS + "/prepare";
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      purchasePreparePlanId: searchObject?.purchasePreparePlanId,
    },
  };
  return axios.get(url, config);
};

export const getCountStatus = (type) => {
  let  url = `${API_PATH}/count-by-status?type=${type}`
  return axios.get(url)
}
