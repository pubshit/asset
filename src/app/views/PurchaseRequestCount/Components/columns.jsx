import {classStatus, formatTimestampToDate} from "../../../appFunction";
import React from "react";

export const PurchaseRequestColumns = (props) => {
	const {t} = props;
	
	return ([
		{
			title: t("purchaseRequest.requestDate"),
			field: "requestDate",
			align: "left",
			minWidth: "150px",
			render: (rowData) => formatTimestampToDate(rowData?.requestDate)
			
		},
		{
			title: t("SuggestedMaterials.ballotCode"),
			field: "code",
			align: "left",
			minWidth: "150px",
			cellStyle: {
				textAlign: "center"
			},
		},
		{
			title: t("purchaseRequest.status"),
			field: "status",
			align: "left",
			minWidth: "150px",
			cellStyle: {
				textAlign: "center"
			},
			render: (rowData) => {
				const statusIndex = rowData?.status;
				return (
					<span className={classStatus(statusIndex)}>
            {props.checkStatus(statusIndex)}
          </span>
				)
			}
		},
		{
			title: t("purchaseRequest.requestDepartment"),
			field: "requestDepartmentName",
			align: "left",
			minWidth: "250px",
		},
		{
			title: t("purchaseRequest.receptionDepartment"),
			field: "receptionDepartmentName",
			align: "left",
			minWidth: "250px",
		},
		{
			title: t("purchaseRequest.requestPersonName"),
			field: "requestPersonName",
			align: "left",
			minWidth: "150px",
		},
		{
			title: t("purchaseRequest.procurementPlan"),
			field: "procurementPlanName",
			align: "left",
			minWidth: "150px",
		},
	])
}
