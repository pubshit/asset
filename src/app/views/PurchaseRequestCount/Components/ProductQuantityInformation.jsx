import PropTypes from "prop-types";
import {Divider, Grid} from "@material-ui/core";

function ProductQuantityInformation(props) {
	const {item = {}} = props;
	const isInvalidApprovalQuantity = item.quota && item.totalApprovalQuantity
		&& Number(item.quota) < Number(item.totalApprovalQuantity)

	return (
		<Grid container spacing={2} className="mb-12" justifyContent="flex-end">
			<Grid item>
				<span>Số lượng định mức:</span>&nbsp;&nbsp;&nbsp;
				<b>{item.quota || 0}</b>
			</Grid>
			<Divider orientation="vertical" flexItem className="my-4"/>
			<Grid item>
				<span>SL tổng hợp hiện tại:</span>&nbsp;&nbsp;&nbsp;
				<b className={isInvalidApprovalQuantity ? "text-error" : "text-primary"}>
					{item.totalApprovalQuantity || 0}
				</b>
			</Grid>
		</Grid>
	)
}

export default ProductQuantityInformation;
ProductQuantityInformation.propTypes = {
	item: PropTypes.object.isRequired
}
