import {
	Grid,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import React, { memo } from "react";
import { appConst } from "../../../appConst";

function CountTable(props) {
	let {
		t,
		columns,
		selectedRow = {},
		itemList = [],
		invalidProductIds = [],
		handleRowClick = (e, rowData) => { },
	} = props;

	return (
		<Grid item xs={12}>
			<MaterialTable
				title={t("general.list")}
				data={itemList?.length ? itemList : []}
				columns={columns}
				localization={{
					body: {
						emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
					},
					toolbar: {
						nRowsSelected: `${t("general.selects")}`,
					},
					pagination: appConst.localizationVi.pagination
				}}
				options={{
					selection: false,
					actionsColumnIndex: -1,
					paging: true,
					search: false,
					draggable: false,
					sorting: false,
					rowStyle: (rowData) => ({
						backgroundColor: selectedRow?.tableData?.id === rowData?.tableData?.id
							? "#ccc"
							: rowData?.tableData?.id % 2 === 1 ? "#EEE" : "#FFF",
						border: invalidProductIds?.includes(rowData.alterProductId || rowData.productId)
							? "1.5px solid var(--signal-error)"
							: "none"
					}),
					maxBodyHeight: "450px",
					minBodyHeight: 250,
					headerStyle: {
						backgroundColor: "#358600",
						color: "#fff",
					},
					padding: "dense",
					toolbar: false,
				}}
				components={{
					Toolbar: (props) => <MTableToolbar {...props} />,
				}}
				onSelectionChange={(rows) => {
					this.data = rows;
				}}
				onRowClick={handleRowClick}
			/>
		</Grid>
	);
}

export default memo(CountTable);
