import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const PurchaseRequestCountTable = EgretLoadable({
  loader: () => import("./PurchaseRequestCountTable")
});
const ViewComponent = withTranslation()(PurchaseRequestCountTable);

const PurchaseRequestCountRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/purchase_request_count",
    exact: true,
    component: ViewComponent
  }
];

export default PurchaseRequestCountRoutes;