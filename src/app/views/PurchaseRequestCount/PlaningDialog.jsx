import {
  Grid,
  Button,
  Dialog,
  DialogActions,
  IconButton,
  Icon,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  DateTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import React from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import DateFnsUtils from "@date-io/date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import moment from "moment";
import history from "history.js";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { searchByPage as PurchaseRequestSearchByPage } from "../PurchaseRequestStauts/PurchaseRequestStautsService";
import {searchByPage as SupplierSearchByPage} from '../Supplier/SupplierService'
import {
  createPurchasePlaning,
  updatePurchasePlaningById,
} from "../PurchasePlaning/PurchasePlaningService";
import { getListManagementDepartment } from "../Asset/AssetService";
import { searchByPage } from "./PurchaseRequestCountService";
import { appConst } from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}
const helperTextRequired = {
  color: "#f44336",
  margin: "0",
  fontSize: "0.75rem",
  marginTop: "3px",
  textAlign: "left",
  fontWeight: "400",
  lineHeight: "1.66",
  letterSpacing: "0.03333em",
};
class PlaningDialog extends React.Component {
  state = {
    rowsPerPage: 1000,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    keyword: "",
    Notification: "",
    planingDate: Date.now(),
    approvedDate: null,
    managementDepartment: null,
    status: null,
    note: null,
    name: null,
    totalEstimatedCosts: null,
    planer: null,
    shouldOpenProductPopup: false,
    product: null,
    department: [],
    listProducts: [],
    planingDepartment: null,
    listProduct: [],
    listSupplier: [],
    supplier: null,
    type: 1,
    formToDate: moment().endOf("month"),
    formDate: moment().startOf("month"),
    dupApplyDate: false,
    checkDate: false,
  };
  PURCHASE = {
    name: "Đã duyệt",
    indexOrder: 2, //Đã duyệt
  };

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name };

    this.setState(
      { managementDepartment: department, planingDepartment: department },
      function () {
        this.search();
        this.handleSelectManagementDepartmentPopupClose();
      }
    );
    if (Object.keys(item).length === 0) {
      this.setState({ managementDepartment: null, planingDepartment: null });
    }
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectMultiProduct = () => {
    let { listProducts } = this.state;
    let { listProduct } = this.state;
    if (!listProducts) {
      listProducts = [];
    }
    if (!listProduct) {
      listProduct = [];
    }
    listProduct.forEach((element) => {
      let p = {};
      p.quantity = element.quantity;
      p.product = element.product;
      p.purchaseRequestId=element.purchaseRequestId
      listProducts.push(p);
    });
    this.setState({ listProducts: listProducts });
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.type = this.state.type;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.fromDate = this.state.fromDate;
      searchObject.managementDepartmentId = this.state.planingDepartment
        ? this.state.planingDepartment.id
        : "";
      // searchByPage(searchObject).then(({ data }) => {
      //   let listProduct = [...data?.content];
      //   let { listProducts } = this.state;
      //   listProducts = [];
      //   listProduct.forEach((element) => {
      //     let p = {};
      //     p.quantity = element.quantity;
      //     p.product = element.product;
      //     p.purchaseRequestId = element.purchaseRequestId
      //     listProducts.push(p);
      //   });
      //   listProducts.sort((a, b) =>
      //     a.product.name.localeCompare(b.product.name)
      //   );

      //   this.setState(
      //     {
      //       listProducts: listProducts,
      //       totalElements: data.totalElements,
      //     },
      //   );
      // });
    });
  }

  handleProductPopupClose = () => {
    this.setState({ shouldOpenProductPopup: false });
  };

  selectPurchaseStatus = (item) => {
    this.setState({ status: item }, function () { });
  };
  selectSupplier = (item) => {
    this.setState({ supplier: item }, function () { });
  };
  handleSelectDepartment = (department = [] ) => {
    this.setState({ department });
  };
  selectPurchaseDepartment = (department = [] ) => {
    this.setState({ department });
  };

  handleDateChange = (date, name) => {
    this.setState(
      {
        [name]: date,
      },
      () => { }
    );
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { planingDate } = this.state;
    if (
      !planingDate ||
      planingDate === null ||
      (typeof planingDate === "object" && !planingDate.getTime())
    ) {
      this.setState({ dupApplyDate: true });
      return;
    }
    if (this.state.listProducts == [] || this.state.listProducts.length === 0) {
      toast.warning("Không có sản phẩm");
      toast.clearWaitingQueue();
      return;
    }
    if (id) {
      updatePurchasePlaningById(
        {
          ...this.state,
        },
        id
      ).then(() => {
        this.props.handleOKEditClose();
      });
    } else {
      createPurchasePlaning({
        ...this.state,
      }).then(() => {
        history.push(ConstantList.ROOT_PATH + "list/purchase_planing");
      });
    }
  };
  componentWillMount() {
    let {
      item,
      fromToDate,
      fromDate,
      planingDepartment,
    } = this.props;
    this.setState({ fromToDate, fromDate, planingDepartment }, function () { });
    this.setState(item);
  }

  componentDidMount() {
    this.search();
    ValidatorForm.addValidationRule("isMaxDate", (value) => {
      if (value.getTime() > Date.now()) {
        return false;
      }
      return true;
    });
  }
  handleQuantityChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.quantity = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleStockKeepingUnitChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.stockKeepingUnit = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handlePriceChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.estimatedUnitPrice = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleCostsChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.estimatedCosts = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleHTMSChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.HTMS = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleSupplierChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.supplier = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleNoteChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.noteProduct = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };

  render() {
    const {
      t,
      i18n,
      open,
    } = this.props;
    let {
      rowsPerPage,
      page,
      status,
      department,
      note,
      name,
      totalEstimatedCosts,
      planer,
      listProducts,
      listSupplier,
      supplier,
      planingDate,
      approvalDate,
      shouldOpenNotificationPopup,
      approvedDate,
    } = this.state;
    let searchObject = { keyword: "", pageIndex: 0, pageSize: 10000 };
    let now = Date.now();

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        minWidth: 100,
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 1) {
                listProducts.find((item, index) => item.product.id === rowData.product.id ? listProducts.splice(index, 1) : "")
                this.setState({ listProducts });
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("Product.STT"),
        field: "code",
        maxWidth: 50,
        align: "center",
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        minWidth: 130,
        field: "product.code",
      },
      {
        title: t("Product.name"),
        field: "name",
        minWidth: 150,
        align: "left",
      },
      {
        title: t("Product.quantity"),
        field: "quantity",
        align: "center",
        minWidth: 120,
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="quantity"
            validators={["minNumber:1", "required"]}
            errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
            value={rowData.quantity}
          />
        ),
      },
      {
        title: t("Product.actualQuantity"),
        field: "quantity",
        align: "center",
        minWidth: 200,
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="quantity"
            validators={["minNumber:1", "required"]}
            errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
            value={rowData.quantity}
          />
        ),
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "stockKeepingUnit",
        align: "center",
        minWidth: 200,
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleStockKeepingUnitChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="stockKeepingUnit"
            value={rowData.stockKeepingUnit}
          />
        ),
      },
      {
        title: t("Product.estimatedUnitPrice"),
        field: "estimatedUnitPrice",
        align: "center",
        minWidth: 200,
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handlePriceChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="estimatedUnitPrice"
            value={rowData.estimatedUnitPrice}
          />
        ),
      },
      {
        title: t("Product.estimatedCosts"),
        field: "estimatedCosts",
        align: "center",
        minWidth: 200,
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleCostsChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="estimatedCosts"
            value={rowData.estimatedCosts}
          />
        ),
      },
      {
        title: 'HTMS',
        field: "HTMS",
        align: "center",
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleHTMSChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={this.state.productName ? true : false}
            name="HTMS"
            value={rowData.HTMS}
          />
        ),
      },
      {
        title: t("Product.supplier"),
        field: "supplier",
        align: "center",
        minWidth: 200,
        render: (rowData) => (
          <AsynchronousAutocomplete
            searchFunction={SupplierSearchByPage}
            searchObject={searchObject}
            defaultValue={listSupplier}
            displayLable={"name"}
            value={supplier}
            onSelect={this.selectSupplier}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
          ),
      },
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        minWidth: 150,
      },
    ];

    let dupApplyDate;
    if (this.state.dupApplyDate === true) {
      dupApplyDate = (
        <span style={helperTextRequired}> {t("general.required")} </span>
      );
    } else {
      dupApplyDate = "";
    }

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        scroll="paper"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            {t("purchasePlaning.title")}
          </DialogTitle>
          <DialogContent style={{ minHeight: "400px" }}>
            <Grid container spacing={1}>
              <Grid item md={4} sm={12} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchasePlaning.planingDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"planingDate"}
                    value={planingDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleDateChange(date, "planingDate")
                    }
                  />
                  {dupApplyDate}
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchasePlaning.approvalDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"approvalDate"}
                    value={approvalDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleDateChange(date, "approvalDate")
                    }
                  />
                  {dupApplyDate}
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                className="w-100 "
                label={t("maintainRequest.name")}
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      <span>{t("maintainRequest.status")}</span>
                    </span>
                  }
                  searchFunction={PurchaseRequestSearchByPage}
                  searchObject={searchObject}
                  defaultValue={status}
                  displayLable={"name"}
                  value={status}
                  onSelect={this.selectPurchaseStatus}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                 <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed">* </span>{" "}
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>
                  }
                  searchFunction={getListManagementDepartment}
                  searchObject={searchObject}
                  displayLable={"name"}
                  typeReturnFunction="list"
                  value={department ?? null}
                  defaultValue={department ?? null}
                  listData={department}
                  setListData={this.selectPurchaseDepartment}
                  onSelect={this.handleSelectDepartment}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
               <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                className="w-100"
                label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planer")}
                    </span>
                }
                onChange={this.handleChange}
                type="planer"
                name="planer"
                value={planer}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
            <Grid container spacing={1}>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                className="w-100 "
                label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.totalEstimatedCosts")}
                    </span>
                }
                onChange={this.handleChange}
                type="text"
                name="totalEstimatedCosts"
                value={totalEstimatedCosts}
                validators={["minNumber:1", "required"]}
                errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
                />
              </Grid>               
              {this.state.status?.indexOrder === this.PURCHASE.indexOrder && (
                <Grid item md={4} sm={12} xs={12}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DateTimePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("purchasePlaning.approvedDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={"approvedDate"}
                      value={approvedDate}
                      maxDate={now}
                      onChange={(date) =>
                        this.handleDateChange(date, "approvedDate")
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
              )}
              {this.state.status?.indexOrder === this.PURCHASE.indexOrder ? (
                <Grid item md={12} sm={12} xs={12}>
                  <TextValidator
                    className="w-100 "
                    label={t("maintainRequest.note")}
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    value={note}
                  />
                </Grid>
              ) : (
                <Grid item md={8} sm={12} xs={12}>
                  <TextValidator
                    className="w-100 "
                    label={t("maintainRequest.note")}
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    value={note}
                  />
                </Grid>
              )}
            </Grid>
            <Grid className="mb-16" container spacing={1}></Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <MaterialTable
                  data={listProducts ? listProducts : []}
                  columns={columns}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    padding: "dense",
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                    }),
                    maxBodyHeight: "280px",
                    minBodyHeight: "280px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ witdth: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
export default PlaningDialog;
