import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { useTranslation } from "react-i18next";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  convertFromToDate,
  filterOptions,
  formatDateDto,
  functionExportToExcel,
  getTheHighestRole, getUserInformation,
  handleThrowResponseMessage, isSuccessfulResponse
} from "../../../appFunction";
import { appConst, keySearch, variable } from "../../../appConst";
import { getAllManagementDepartmentByOrg } from "../../Department/DepartmentService";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { getListUserByDepartmentId } from "app/views/AssetTransfer/AssetTransferService";
import CountTable from "../Components/CountTable";
import { getNewCode, searchByPage } from "app/views/Product/ProductService";
import { getStockKeepingUnit } from "../../StockKeepingUnit/StockKeepingUnitService";
import ProductDialog from "app/views/Product/ProductDialog";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import {
  addNewPurchaseRequestCount,
  exportToExcelV2, getRequestsProducts,
  updateNewPurchaseRequestCount
} from "../PurchaseRequestCountService";
import AppContext from "app/appContext";
import { LightTooltip, PaperComponent, TabPanel } from "../../Component/Utilities";
import StockKeepingUnitEditorDialog from "../../StockKeepingUnit/StockKeepingUnitEditorDialog";
import vi from "date-fns/locale/vi";
import { searchByPage as searchByPagePurchaseRequest } from "../../PurchaseRequest/PurchaseRequestService";
import { getItemById } from "../../ShoppingPlan/PurchaseRequestService";
import ProductQuantityInformation from "../Components/ProductQuantityInformation";
import ErrorOutlineRoundedIcon from '@material-ui/icons/ErrorOutlineRounded';
import { filterArrayByProduct, findProductInArray, sumApprovedQtyByProduct, sumApprovedQuantity } from "../helper";
import PlaningDialog from "../../PurchasePlaning/PlaningDialog";
import { PurchaseRequestSelectionPopup } from "../../Component/PurchasingManagement";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { value } = props;

  return (
    <div>
      {value !== appConst.tabPurchaseReqCountDialog.tabProductNotCount && (
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      )}
      {value !== appConst.tabPurchaseReqCountDialog.tabHasProduct && (
        <LightTooltip
          title={t("purchaseRequestCount.count")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.receive)}
          >
            <ArrowForwardOutlinedIcon className="iconArrow" />
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

class PurchaseRequestCountDialog extends Component {
  state = {
    countDepartment: {},
    listCountDepartment: [],
    tabValue: appConst.tabPurchaseReqCountDialog.tabHasProduct,
    listAsset: [],
    query: {
      pageIndex: 0,
      pageSize: 10,
      keyword: "",
      keySearch: "",
      purchaseRequestId: ""
    },
    listUnit: [],
    itemProduct: {},
    keywordSearch: {
      index: null,
      keywordSearch: "",
    },
    shouldOpenDialogProduct: false,
    indexLinhKien: null,
    listProductNotYet: [],
    listHasProduct: [],
    listProductNotCount: [],
    isCounted: false,
    nameList: "listHasProduct",
    statusFilter: null,
    listRequestDepartment: [],
    requestDepartmentFilter: null,
    invalidProductIds: [],
    isSubmitApprove: true,
  };

  getAllItemProduct = () => {
    let { item, isView } = this.props;
    let { departmentUser } = getUserInformation();
    const roles = getTheHighestRole();
    let { isRoleAssetManager, isRoleOrgAdmin, currentUser } = roles;
    let productList = [];
    if (item?.id) {
      productList = item?.purchaseProducts?.length > 0 ? [...item?.purchaseProducts] : [];

      this.setState({
        ...item,
        ...roles,
        isView,
        countDepartment: item?.departmentId
          ? {
            name: item?.departmentName,
            id: item?.departmentId
          }
          : null,
        status: appConst.listStatusPurchaseReqCount?.find(
          p => p.code === item.status
        ),
        countPerson: item?.personId ? {
          personId: item?.personId,
          personDisplayName: item?.personName,
        } : null,
        productList,
        isCounted: item?.status === appConst.STATUS_PURCHASE_REQUEST_COUNT.DA_TONG_HOP.code,
        productRequestIds: item?.purchaseRequestIds || [],
      }, () => {
        this.filterItemList();
      });
    } else {
      productList = item?.requestsProducts?.map(product => ({
        ...product,
      }))
      let countDepartment = isRoleAssetManager
        ? departmentUser
        : item.purchaseRequests?.[0].receptionDepartmentId
          ? {
            name: item.purchaseRequests?.[0].receptionDepartmentName,
            id: item.purchaseRequests?.[0].receptionDepartmentId,
          }
          : null;

      let countPerson = (!isRoleOrgAdmin && currentUser?.person?.id) ? {
        personId: currentUser?.person?.id,
        personDisplayName: currentUser?.person?.displayName,
      } : null;

      this.setState({
        ...item,
        ...roles,
        isView,
        countDepartment: countDepartment,
        status: appConst.listStatusPurchaseReqCountObject.DANG_XU_LY,
        productList,
        countPerson,
      }, () => {
        this.filterItemList();
      });
    }
  };

  exportToExcel = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      statusFilter,
      requestDepartmentFilter
    } = this.state
    setPageLoading(true)
    let searchObject = {
      pageIndex: 1,
      pageSize: 1000000,
      purchasePreparePlanIds: [this.props?.item?.id],
      status: statusFilter?.code,
      requestDepartmentId: requestDepartmentFilter?.id
    }
    try {
      functionExportToExcel(exportToExcelV2, searchObject, "PurchaseRequestCount.xlsx");
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  componentDidMount() {
    this.getAllItemProduct();
    this.handleGetListManagementDepartment();
    this.getListStockKeepingUnit();
    this.handleGetProcurementPlan(this.props.item?.procurementPlanId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.query !== this.state.query) {
      if (this.state.query.keySearch === keySearch.asset) {
        this.handleGetListProduct(this.state.query.purchaseRequestId);
      }
    }
  }

  filterItemList = () => {
    let { item } = this.props;
    let { productList = [] } = this.state;
    let listProductNotYetUpdated = []
    let listHasProductUpdated = []
    let listProductNotCountUpdated = []

    productList?.map((product) => {
      product.unit = product?.skuId
        ? {
          name: product?.skuName,
          id: product?.skuId
        }
        : null;
      product.productSelected = product?.alterProductId
        ? {
          name: product?.alterProductName,
          id: product?.alterProductId
        }
        : null
      product.approvedQuantity = product?.approvedQuantity || product?.quantity;
      if (item?.status === appConst.STATUS_PURCHASE_REQUEST_COUNT.DANG_TONG_HOP.code) {
        if (product?.status === appConst.listStatusProductInPurchaseRequest.KHONG_DUYET.code) { // Danh sách không tổng hợp
          listProductNotCountUpdated.push(product);
        } else if ((product?.alterProductId && item.id) || product?.productId) { // Danh sách đã được chọn sản phẩm
          listHasProductUpdated.push(product);
        } else { // Danh sách chưa được chọn sản phẩm
          listProductNotYetUpdated.push(product);
        }
      }
      else {
        if ( // Danh sách sản phẩm được tổng hợp
          product?.alterProductId
          && product?.status !== appConst.listStatusProductInPurchaseRequest.KHONG_DUYET.code
        ) {
          listHasProductUpdated.push(product);
        } else { // Danh sách sản phẩm không được tổng hợp
          listProductNotCountUpdated.push(product)
        }
      }
    });

    this.setState({
      listProductNotCount: listProductNotCountUpdated,
      listProductNotYet: listProductNotYetUpdated,
      listHasProduct: listHasProductUpdated,
    });
  };

  setKeyword = (value) => {
    this.setState({
      keywordSearch: value,
    });
  };

  setItemProduct = (value, index) => {
    this.setState({
      itemProduct: value,
    }, () => {
      this.setIndexLinhKien(index);
      this.setShouldOpenDialogProduct(true);
    });
  };

  setShouldOpenDialogProduct = (value) => {
    this.setState({
      shouldOpenDialogProduct: value,
      listAsset: [],
    });
  };

  handleOKEditClose = () => {
    this.setShouldOpenDialogProduct(false);
  };

  getListStockKeepingUnit = async () => {
    let searchObjectUnit = { pageIndex: 1, pageSize: 1000000 };
    try {
      let res = await getStockKeepingUnit(searchObjectUnit);
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({
          listUnit: res.data?.content,
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  handleSelectUnit = (value, index, nameList) => {
    if (variable.listInputName.New === value?.code) {
      this.setState({
        shouldOpenAddNewSkuDialog: true,
        keywordSearch: {
          ...this.state.keywordSearch,
          index,
          nameList,
        }
      });
      return;
    }
    let itemListUpdated = this.state[nameList]?.map((row, idx) => {
      if (index === idx) {
        row.unit = value;
        row.skuId = value?.id;
        row.skuName = value?.name;
        return row;
      }
      return row;
    });
    this.setState({
      [nameList]: itemListUpdated,
    });
  };

  handleGetListProduct = async () => {
    let { t } = this.props;
    let { countDepartment } = this.state;
    let searchObject = {};

    searchObject.keyword = this.state.query.keyword;
    searchObject.pageIndex = 1;
    searchObject.pageSize = 10;
    searchObject.managementPurchaseDepartment = null;
    searchObject.productTypeCodes = this.props?.type === appConst.TYPE_PURCHASE.TSCD_CCDC
      ? [appConst.productTypeCode.TSCĐ, appConst.productTypeCode.CCDC]
      : [appConst.productTypeCode.VTHH];
    searchObject.managementPurchaseDepartment = countDepartment?.id
      ? {
        id: countDepartment?.id,
        name: countDepartment?.name
      }
      : null;

    await searchByPage(searchObject)
      .then(({ data }) => {
        this.setState({
          listAsset: [...data.content],
        });
      })
      .catch((e) => {
        console.error(e);
        toast.error(t("toastr.error"));
      });
  };

  handleSearch = (e, source, index, rowData) => {
    this.setState({
      query: {
        ...this.state.query,
        keyword: e.target.value,
        keySearch: source,
        purchaseRequestId: rowData.purchaseRequestId,
        listAsset: [],
      },
    });
    this.setKeyword({
      index: index,
      keywordSearch: e.target.value,
    });
  };

  setIndexLinhKien = (value) => {
    this.setState({
      indexLinhKien: value,
    });
  };

  handleMapDataAsset = (value, index) => {
    const { t } = this.props;
    const { departmentUser } = getUserInformation();
    if (value?.code === variable.listInputName.New) {
      getNewCode()
        .then((result) => {
          if (result != null && result?.data && result?.data?.code) {
            let item = result.data;
            this.setItemProduct({
              ...item,
              name: this.state.keywordSearch.keywordSearch,
              managementPurchaseDepartment: departmentUser
            }, index)
          }
        })
        .catch(() => {
          toast.error(t("general.error"));
        });
    } else {
      let { nameList } = this.state
      let itemListUpdate = this.state[nameList]?.map((item, idx) => {
        if (item?.id === index) {
          if (!value) {
            this.setState({
              query: { ...this.state.query, keyword: "" },
            });
          }
          item.productSelected = value;
          item.alterProductId = value?.id;
          item.alterProductName = value?.name;
          item.skuName = value?.defaultSku?.sku?.name;
          item.skuId = value?.defaultSku?.sku?.id;
          item.unit = { name: item.skuName, id: item.skuId }

          this.setKeyword({
            index: null,
            keywordSearch: "",
          });
          return item;
        }
        return item;
      });
      this.setState({
        query: { ...this.state.query, keyword: "" },
        [nameList]: itemListUpdate,
      });
    }
  };

  handleChangeTabValue = (event, newValue) => {
    let nameList = "";
    switch (newValue) {
      case appConst.tabPurchaseReqCountDialog.tabProductNotYet:
        nameList = "listProductNotYet";
        break;

      case appConst.tabPurchaseReqCountDialog.tabHasProduct:
        nameList = "listHasProduct";
        break;

      case appConst.tabPurchaseReqCountDialog.tabProductNotCount:
        nameList = "listProductNotCount";
        break;

      default:
        break;
    }

    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      tabValue: newValue,
      nameList: nameList,
    });
  };

  handleSelectCountDepartment = (value) => {
    if (value?.id !== this.state.countDepartment?.id) {
      this.setState({
        countDepartment: value ? value : null,
        searchParamUserByCountDepartment: { departmentId: value?.id ?? "" },
        countPerson: null
      });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  selectStatus = (statusSelected) => {
    this.setState({ status: statusSelected });
    if (Object.keys(statusSelected || {}).length === 0) {
      this.setState({ status: null });
    }
  };

  handleGetListManagementDepartment = async () => {
    let { t } = this.props;
    try {
      const result = await getAllManagementDepartmentByOrg();
      if (result?.status === appConst.CODE.SUCCESS) {
        this.setState({ listCountDepartment: result?.data });
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
    }
  };

  handleSelectCountPerson = (item) => {
    this.setState({
      countPerson: item ? item : null,
    });
  };

  handleChangeDataTable = (e, index, nameList) => {
    let listRowTableUpdated = this.state[nameList]?.map((row, idx) => {
      if (index === idx) {
        row[e.target.name] = e.target.value;
        return row;
      }
      return row;
    });
    this.setState({
      [nameList]: listRowTableUpdated,
    });
  };

  handleQuantityChange = (rowData, e, index, nameList) => {
    let { invalidProductIds = [] } = this.state;
    const listData = [...(this.state[nameList] || [])];
    let isInvalidProd = invalidProductIds?.includes(rowData.alterProductId || rowData.productId);
    let value =  +e.target.value;
    listData[index].approvedQuantity = value <= 0 ? 1 : value;
    if (isInvalidProd) {
      invalidProductIds?.splice(invalidProductIds?.indexOf(rowData.alterProductId || rowData.productId), 1)
    }

    this.setState({
      [nameList]: listData,
      invalidProductIds: [...invalidProductIds],
    }, () => {
      this.handleRowClick(e, rowData);
      this.handleValidateAprovalQuantity(false);
    });
  };

  handleProductDialogClose = () => {
    this.setShouldOpenDialogProduct(false);
    if (!this.state?.itemProduct?.id) {
      this.setKeyword({
        index: null,
        keywordSearch: "",
      });
    }
  };

  handleCloseAddNewSkuDialog = () => {
    this.setState({ shouldOpenAddNewSkuDialog: false });
    this.setKeyword({
      index: null,
      keywordSearch: "",
    });
  };

  handleDelete = (id, nameList) => {
    let {productRequestIds = []} = this.state;
    let listProductNotCount = [...this.state.listProductNotCount]
    let itemListUpdated = this.state[nameList]?.filter((item) => {
      if (item.id === id) {
        let existProdInTheSameVoucher = this.state[nameList]?.some(
          x => x.purchaseRequestId === item.purchaseRequestId
          &&  x.id !== item.id
        );

        if (!existProdInTheSameVoucher) {
          productRequestIds.splice(productRequestIds.indexOf(item.purchaseRequestId), 1);
        }
        item.status = appConst.listStatusProductInPurchaseRequest.KHONG_DUYET.code;
        listProductNotCount.push(item);
        return false;
      }
      return true;
    });
    this.setState({
      [nameList]: itemListUpdated,
      productRequestIds,
        listProductNotCount,
    });
  };

  handleCount = (id, nameList) => {
    let { t } = this.props;
    let itemListUpdated = this.state[nameList]?.filter((item) => {
      if (item.id === id) {
        if (!item?.productSelected?.id) {
          toast.warning(t("Chưa chọn sản phẩm"));
        } else {
          item.alterProductId = item?.productSelected?.id;
          item.status = appConst.listStatusProductInPurchaseRequest.DA_DUYET.code;
          this.setState({
            listHasProduct: [...this.state.listHasProduct, item],
          });
          return false;
        }
      }
      return true;
    });
    this.setState({
      [nameList]: itemListUpdated,
    });
  };

  setSubmitApprove = (status) => {
    this.setState({ isSubmitApprove: status })
  }
  
  formatDataDto = (data, openPurchasePlan) => {
    let purchaseRequestsUpdated = []
    purchaseRequestsUpdated = [
      ...data?.listHasProduct?.map(prod => ({
        ...prod,
        status: openPurchasePlan
          ? appConst.listStatusProductInPurchaseRequest.DA_DUYET.code
          : prod.status,
      })),
      ...data?.listProductNotYet,
      ...data?.listProductNotCount
    ]
    
    return {
      id: data?.id,
      purchaseRequestIds: data?.purchaseRequestIds,
      date: formatDateDto(data?.date),
      departmentId: data?.countDepartment?.id,
      departmentName: data?.countDepartment?.name,
      note: data?.note,
      personId: data?.countPerson?.personId,
      personName: data?.countPerson?.personDisplayName,
      status: data?.status?.code || data?.status,
      type: this.props?.type,
      name: data?.name,
      purchaseProducts: purchaseRequestsUpdated
    }
  }

  handleFormSubmit = async (e, openPurchasePlan) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    if (this.handleValidateAprovalQuantity()) { return; }
    try {
      setPageLoading(true);
      let dataSubmit = this.formatDataDto(this.state, openPurchasePlan);
      if (dataSubmit?.id) {
        let res = await updateNewPurchaseRequestCount(dataSubmit)
        if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS && res?.data?.data) {
          if (openPurchasePlan) { return; }
          toast.success(t("general.success"));
          this.setSubmitApprove(true);
          this.props.handleOKEditClose();
          if (this.props?.setSelectedItems) {
            this.props.setSelectedItems([])
          }
        } else {
          this.setSubmitApprove(false);
          handleThrowResponseMessage(res)
        }
      } else {
        let res = await addNewPurchaseRequestCount(dataSubmit)
        if (isSuccessfulResponse(res?.data?.code)) {
          toast.success(t("general.success"));
          this.setSubmitApprove(true);
          this.props.handleOKEditClose();
          if (this.props?.setSelectedItems) {
            this.props.setSelectedItems([])
          }
        } else {
          this.setSubmitApprove(false);
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      this.setSubmitApprove(false);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }
  };

  handleOpenGeneralSelectionPopup = () => {
    this.setState({
      shouldOpenGeneralSelectionPopup: true,
    });
  }

  handleClosePopup = () => {
    this.setState({
      shouldOpenGeneralSelectionPopup: false,
      shouldOpenPlaningEditorDialog: false,
    });
  }

  handleGetProductsByRequestIds = async (ids = []) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    let searchObjects = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      purchaseRequestIds: ids,
    }
    try {
      if (ids?.length === 0) {
        return this.setState({
          productList: [],
          purchaseRequestIds: [],
        }, () => {
          this.filterItemList();
        })
      }
      
      setPageLoading(true);
      let res = await getRequestsProducts(searchObjects)
      let { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        let newArray = data?.content || [];
        newArray?.map(item => {
          let existItem = this.state.productList?.find(x => x.id === item.id);
          if (existItem) {
            item.quantity = existItem.quantity || item.quantity;
            item.approvedQuantity = existItem.approvedQuantity || item.approvedQuantity;
            item.availableQuantity = existItem.availableQuantity || item.availableQuantity;
            item.status = existItem.status || item.status;
            item.unit = existItem.unit || item.unit || null;
          }

          return item;
        })
        this.setState({
          productList: newArray,
          purchaseRequestIds: ids,
        }, () => {
          this.filterItemList();
        })
        return data;
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  handleSelectPurchaseRequestVoucher = (ids = []) => {
    this.handleClosePopup();
    this.handleGetProductsByRequestIds(ids).then(data => {
      this.setState({ productRequestIds: ids });
    });
  }

  handleValidateOnSelectPurchaseRequest = (item = {}, selectedList = [], itemList = []) => {
    let firstSelectedId = selectedList[0]?.id;
    let purchaseRequest = itemList?.find(x => x.id === firstSelectedId);
    let productRequestIds = this.props.item.purchaseRequestIds;
    let isProccessing = item.status === appConst.listStatusPurchaseRequestObject.DANG_TONG_HOP.code;
    
    if (firstSelectedId && purchaseRequest && purchaseRequest?.procurementPlanId !== item?.procurementPlanId) {
      toast.warning("Các phiếu đã chọn không cùng danh mục kế hoạch");
      return false;
    } else if (isProccessing && item?.tableData?.checked === false && !productRequestIds?.includes(item?.id)) {
      return false
    }
    return item?.status !== appConst.listStatusPurchaseRequestObject.DA_TONG_HOP.code;
  }

  handleFilterSelectedPurchaseRequest = (item, rowData) => {
    return item === rowData.id || item.id === rowData.id;
  }

  handleValidForSelect = (item = {}, selectedList = [], itemList = []) => {
    let firstSelectedId = selectedList[0]?.id;
    let purchaseRequest = itemList?.find(x => x.id === firstSelectedId);
    let productRequestIds = this.props.item.purchaseRequestIds;
    let isProccessing = item.status === appConst.listStatusPurchaseRequestObject.DANG_TONG_HOP.code;
    
    if (firstSelectedId && purchaseRequest && purchaseRequest?.procurementPlanId !== item?.procurementPlanId) {
      return false;
    } else if (isProccessing && item?.tableData?.checked === false && !productRequestIds?.includes(item?.id)) {
      return false
    }
    return item?.status !== appConst.listStatusPurchaseRequestObject.DA_TONG_HOP.code;
  }

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusPurchaseRequest.find(
      (item) => item.code === status
    );
    return itemStatus?.name;
  };

  convertPurchaseRequestSelected = (item) => item.id;

  handleRowClick = (e, rowData) => {
    // if((rowData.alterProductId || rowData.productId) === (this.state?.selectedRow?.alterProductId || this.state?.selectedRow?.productId)) return;
    let { procurementPlanProducts, listHasProduct = [], invalidProductIds = [] } = this.state;
    let procurementProduct = findProductInArray(procurementPlanProducts, rowData);
    let listProdByProcurementProd = filterArrayByProduct(listHasProduct, procurementProduct)
    let totalApprovalQuantity = sumApprovedQuantity(listProdByProcurementProd);
    // let isInvalidProd = invalidProductIds?.includes(rowData.alterProductId || rowData.productId);

    // if (isInvalidProd) {
    //   invalidProductIds?.splice(invalidProductIds?.indexOf(rowData.alterProductId || rowData.productId), 1);
    // }

    this.setState({
      selectedRow: rowData,
      quota: procurementProduct?.quantity,
      totalApprovalQuantity,
      // invalidProductIds: [...invalidProductIds],
    })
  }

  handleValidateAprovalQuantity = (isShowMessage = true) => {
    let { listHasProduct = [], procurementPlanProducts = [] } = this.state;
    const invalidProducts = listHasProduct?.filter(prod => {
      let sumApprovedQty = sumApprovedQtyByProduct(listHasProduct, prod);
      let procurementProdQty = findProductInArray(procurementPlanProducts, prod)?.quantity;
      if (sumApprovedQty > procurementProdQty) {
        return prod;
      }
    });
    let invalidProductIds = [...invalidProducts?.map(prod => prod.alterProductId || prod.productId)];

    this.setState({
      invalidProductIds,
    });

    if (invalidProductIds?.length > 0 && isShowMessage) {
      toast.warning("Số lượng duyệt của các sản phẩm vượt quá số lượng định mức của kế hoạch");
      return true;
    } else {
      return false;
    }
  }

  handleGetProcurementPlan = async (id) => {
    const { t } = this.props;
    let { setPageLoading } = this.context
    try {
      setPageLoading(true)
      let res = await getItemById(id)
      const { code, data } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          procurementPlanProducts: data?.products,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false)
    }
  }

  handleAddPlanning = async () => {
    let { countDepartment, date, listProductNotYet = [], id } = this.state;
    if (listProductNotYet?.length > 0) {
      toast.warning('Xử lý sản phẩm ở tab "SẢN PHẨM CHƯA CÓ" trước khi tổng hợp danh mục duyệt');
      return true;
    }

    await this.handleFormSubmit(null, true);
    if (!this.state.isSubmitApprove) return;
    this.setState({
      item: {
        statusObj: appConst.listStatusPurchasePlaningObject.DANG_XU_LY,
        department: countDepartment?.id
          ? countDepartment
          : null,
        planDate: date,
        purchasePreparePlanId: id,
      },
      shouldOpenPlaningEditorDialog: true,
    });
  };

  render() {
    let { open, t, i18n } = this.props;
    let {
      id,
      note,
      name,
      status,
      isView,
      listUnit,
      tabValue,
      isCounted,
      date,
      countPerson,
      countDepartment,
      listCountDepartment,
      isRoleAssetManager,
      purchaseRequest,
      isRoleOrgAdmin,
      shouldOpenAddNewSkuDialog,
      keywordSearch,
      listAsset,
      shouldOpenPlaningEditorDialog,
      item,
    } = this.state;
    let searchParamUserByCountDepartment = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: countDepartment?.id
    }
    let searchObjectProduct = {
      keyword: this.state.query.keyword,
      pageIndex: 1,
      pageSize: 10,
      managementPurchaseDepartment: purchaseRequest?.receptionDepartmentId || countDepartment?.id ? {
        id: purchaseRequest?.receptionDepartmentId || countDepartment?.id,
        name: purchaseRequest?.receptionDepartmentName || countDepartment?.name
      } : null,
      productTypeCodes: this.props?.type === appConst.TYPE_PURCHASE.TSCD_CCDC
        ? [appConst.productTypeCode.TSCĐ, appConst.productTypeCode.CCDC]
        : [appConst.productTypeCode.VTHH],
    }
    const searchObjectPurchaseRequest = {
      type: appConst.TYPE_PURCHASE.TSCD_CCDC,
    }
    let nameListByTab = {
      [appConst.tabPurchaseReqCountDialog.tabHasProduct]: "listHasProduct",
      [appConst.tabPurchaseReqCountDialog.tabProductNotCount]: "listProductNotCount",
      [appConst.tabPurchaseReqCountDialog.tabProductNotYet]: "listProductNotYet",
    };
    let nameList = nameListByTab[tabValue];

    let columns = [
      {
        title: "",
        field: "requestDate",
        align: "left",
        hidden: this.state.invalidProductIds?.length <= 0,
        minWidth: 40,
        render: (rowData) => this.state?.invalidProductIds?.includes(rowData.alterProductId || rowData.productId) && (
          <ErrorOutlineRoundedIcon fontSize="small" color="error" />
        )
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        hidden: isCounted || isView,
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (method === appConst.active.receive) {
                this.handleCount(rowData.id, nameList);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id, nameList);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => (
          <AsynchronousAutocompleteSub
            placeholder={t("purchaseRequest.searchProductAvailabile")}
            searchFunction={searchByPage}
            searchObject={searchObjectProduct}
            listData={listAsset}
            displayLable="name"
            readOnly={isView || isCounted}
            onSelect={(value) => this.handleMapDataAsset(value, rowData.id)}
            value={rowData?.productSelected || null}
            onInputChange={(e) => this.handleSearch(
              e,
              keySearch.asset,
              rowData?.tableData?.id,
              rowData,
            )}
            filterOptions={(options, params) => filterOptions(
              options,
              params,
              isRoleAssetManager || isRoleOrgAdmin,
              "name",
            )}
            onBlur={() => {
              this.setState({
                query: {
                  ...this.state.query,
                  keyword: "",
                  purchaseRequestId: ""
                },
              });
            }}
          />
        ),
      },
      {
        title: t("purchaseRequestCount.nameProduct"),
        field: "alterProductName",
        align: "left",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => {
          return (
            <TextValidator
              className="w-100"
              onChange={(event) =>
                this.handleChangeDataTable(
                  event,
                  rowData?.tableData?.id,
                  nameList
                )
              }
              InputProps={{
                readOnly: isView || isCounted || Boolean(rowData?.productSelected?.id),
              }}
              name="productName"
              validators={isCounted ? ["required"] : []}
              errorMessages={[t("general.required")]}
              value={rowData?.productName || ""}
            />
          );
        },
      },
      {
        title: t("purchaseRequestCount.unit"),
        field: "skuName",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) =>
          isCounted || isView ? (
            <TextValidator
              fullWidth
              InputProps={{
                readOnly: isView || isCounted,
              }}
              value={rowData.unit?.name || ""}
            />
          ) : (
            <Autocomplete
              id="combo-box"
              fullWidth
              style={{ transform: "translateY(1px)" }}
              size="small"
              name="unit"
              options={listUnit}
              onChange={(e, value) =>
                this.handleSelectUnit(value, rowData?.tableData?.id, nameList)
              }
              value={rowData.unit ? rowData.unit : null}
              getOptionLabel={(option) => {
                return option?.name || "";
              }}
              getOptionSelected={(option, value) => option?.id === value?.id}
              noOptionsText={t("general.noOption")}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                isRoleAssetManager || isRoleOrgAdmin,
                "name",
              )}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  onChange={(e) => this.handleSearch(
                    e,
                    keySearch.asset,
                    rowData?.tableData?.id,
                    rowData,
                  )}
                  value={rowData?.unit?.name || ""}
                />
              )}
            />
          ),
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          if (rowData?.quantity == null || rowData?.quantity === 0) {
            rowData.quantity = 1;
          }
          return (
            <TextValidator
              className="w-100"
              onChange={(event) =>
                this.handleQuantityChange(
                  rowData,
                  event,
                  rowData?.tableData?.id,
                  nameList
                )
              }
              id="formatted-numberformat-carryingAmount"
              InputProps={{
                readOnly: isView || isCounted,
                inputProps: {
                  className: "text-center",
                },
              }}
              type="number"
              name="approvedQuantity"
              validators={["minNumber:1", "required"]}
              errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
              value={
                !isNaN(rowData?.approvedQuantity)
                  ? rowData?.approvedQuantity
                  : rowData?.quantity
              }
            />
          );
        },
      },
      {
        title: t("purchaseRequestCount.requestDepartment"),
        field: "requestDepartmentName",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.requestParameters"),
        field: "productSpecs",
        align: "left",
        width: "240px",
        minWidth: "240px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.note"),
        field: "note",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => {
          return (
            <TextValidator
              className="w-100"
              onChange={(event) =>
                this.handleChangeDataTable(
                  event,
                  rowData?.tableData?.id,
                  nameList
                )
              }
              InputProps={{
                readOnly: isView || tabValue !== appConst.tabPurchaseReqCountDialog.tabProductNotCount,
              }}
              name="note"
              value={rowData.note}
            />
          );
        },
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        scroll={"paper"}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle className="cursor-move">
            <span className="">{t("purchase_request_count.title")}</span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <Grid container spacing={1} className="">
              <Grid item md={3} sm={12} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={vi}>
                  <KeyboardDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("purchaseRequestCount.countDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    disabled={isCounted || isView}
                    format="dd/MM/yyyy"
                    name={"date"}
                    value={date || new Date()}
                    maxDate={new Date()}
                    maxDateMessage={t(
                      "purchaseRequestCount.noti.maxDateMessage"
                    )}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleDateChange(date, "date")
                    }
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={3} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("purchaseRequestCount.name")}
                    </span>
                  }
                  name="name"
                  value={name || ""}
                  InputProps={{
                    readOnly: isView || isCounted,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid item md={3} sm={12} xs={12}>
                {isView || isCounted ? (
                  <TextValidator
                    className="w-100"
                    InputProps={{
                      readOnly: isView || isCounted,
                    }}
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("purchaseRequestCount.countDepartment")}
                      </span>
                    }
                    value={countDepartment ? countDepartment?.name : ""}
                  />
                ) : (
                  <Autocomplete
                    style={{ transform: "translateY(3px)" }}
                    id="combo-box-reception-department"
                    fullWidth
                    size="small"
                    name="countDepartment"
                    disabled={!isRoleOrgAdmin || true}
                    options={listCountDepartment}
                    onChange={(event, value) =>
                      this.handleSelectCountDepartment(value)
                    }
                    getOptionLabel={(option) => option.name}
                    getOptionSelected={(option, value) => option?.id === value?.id}
                    value={countDepartment ? countDepartment : null}
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={
                          <span>
                            <span className="colorRed">*</span>
                            {t("purchaseRequestCount.countDepartment")}
                          </span>
                        }
                        variant="standard"
                        value={countDepartment ? countDepartment?.name : null}
                        validators={["required"]}
                        errorMessages={[t("general.required")]}
                        inputProps={{
                          ...params.inputProps,
                        }}
                      // onChange={this.handleSearchDepartment}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                  />
                )}
              </Grid>
              <Grid item md={3} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchaseRequestCount.countPerson")}
                    </span>
                  }
                  disabled={!countDepartment?.id || isView || !isRoleOrgAdmin}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByCountDepartment}
                  defaultValue={countPerson ?? ""}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  readOnly={isView || isCounted}
                  value={countPerson ?? ""}
                  onSelect={(value) => this.handleSelectCountPerson(value)}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={3} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("purchaseRequestCount.status")}
                  displayLable="name"
                  searchFunction={() => { }}
                  listData={appConst.listStatusPurchaseReqCountDialog}
                  onSelect={this.selectStatus}
                  selectedOptionKey="code"
                  readOnly={isView || isCounted}
                  value={status}
                  disabled
                />
              </Grid>
              <Grid item md={9} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span>{t("purchaseRequestCount.note")}</span>}
                  name="note"
                  value={note || ""}
                  InputProps={{
                    readOnly: isView || isCounted,
                  }}
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <Button
                  className="mt-12"
                  size="small"
                  variant="contained"
                  color="primary"
                  onClick={this.handleOpenGeneralSelectionPopup}
                >
                  {t("purchaseRequestCount.selectPurchaseRequest")}
                </Button>
                {this.state.shouldOpenGeneralSelectionPopup && (
                  <PurchaseRequestSelectionPopup
                    t={t}
                    open={this.state.shouldOpenGeneralSelectionPopup}
                    handleClose={this.handleClosePopup}
                    searchFunction={searchByPagePurchaseRequest}
                    searchObject={searchObjectPurchaseRequest}
                    selectedProducts={this.state.purchaseRequestIds}
                    handleSelect={this.handleSelectPurchaseRequestVoucher}
                    filterSelected={this.handleFilterSelectedPurchaseRequest}
                    validOnSelect={this.handleValidateOnSelectPurchaseRequest}
                    convertSelectedItem={this.convertPurchaseRequestSelected}
                    validForSelect={this.handleValidForSelect}
                    procurementPlanId={this.props.item?.procurementPlanId}
                    params={{
                      receptionDepartmentId: this.state.countDepartment?.id,
                    }}
                  />
                )}
              </Grid>
              <div className="w-100 mt-20">
                <AppBar position="static" color="default">
                  <Tabs
                    className="tabsStatus"
                    value={tabValue}
                    onChange={this.handleChangeTabValue}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                  >
                    <Tab
                      className="tab"
                      label={
                        <div className="tabLable">
                          <span>{t("purchaseRequestCount.tabHasProduct")}</span>
                          <div className="tabQuantity tabQuantity-info">
                            {this.state?.listHasProduct?.length || 0}
                          </div>
                        </div>
                      }
                    />
                    <Tab
                      className="tab"
                      label={
                        <div className="tabLable">
                          <span>{t("purchaseRequestCount.tabProductNotYet")}</span>
                          <div className="tabQuantity tabQuantity-info">
                            {this.state?.listProductNotYet?.length || 0}
                          </div>
                        </div>
                      }
                    />
                    <Tab
                      className="tab"
                      label={
                        <div className="tabLable">
                          <span>{t("purchaseRequestCount.tabProductNotCount")}</span>
                          <div className="tabQuantity tabQuantity-info">
                            {this.state?.listProductNotCount?.length || 0}
                          </div>
                        </div>
                      }
                    />
                  </Tabs>
                </AppBar>
              </div>
              <TabPanel
                value={tabValue}
                index={appConst?.tabPurchaseReqCountDialog?.tabHasProduct}
                className="w-100"
              >
                <ProductQuantityInformation
                  item={this.state}
                />
                <CountTable
                  t={t}
                  invalidProductIds={this.state.invalidProductIds}
                  columns={columns}
                  selectedRow={this.state.selectedRow}
                  handleRowClick={this.handleRowClick}
                  itemList={this.state.listHasProduct}
                />
              </TabPanel>
              <TabPanel
                value={tabValue}
                index={appConst?.tabPurchaseReqCountDialog?.tabProductNotYet}
                className="w-100"
              >
                <CountTable
                  t={t}
                  columns={columns}
                  selectedRow={this.state.selectedRow}
                  handleRowClick={this.handleRowClick}
                  itemList={this.state.listProductNotYet}
                />
              </TabPanel>
              <TabPanel
                value={tabValue}
                index={appConst?.tabPurchaseReqCountDialog?.tabProductNotCount}
                className="w-100"
              >
                <CountTable
                  t={t}
                  columns={columns}
                  selectedRow={this.state.selectedRow}
                  handleRowClick={this.handleRowClick}
                  itemList={this.state.listProductNotCount}
                />
              </TabPanel>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => {
                  this.props.handleClose();
                }}
              >
                {t("general.close")}
              </Button>
              {!isView && (
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  className="mr-12"
                  disabled={isCounted}
                >
                  {t("general.save")}
                </Button>
              )}
              {!isView && id && (
                <Button
                  className="mr-12"
                  variant="contained"
                  color="primary"
                  onClick={this.handleAddPlanning}
                >
                  {t("purchase_request_count.add")}
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>
        {this.state.shouldOpenDialogProduct && (
          <ProductDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleProductDialogClose}
            open={this.state?.shouldOpenDialogProduct}
            handleOKEditClose={this.handleOKEditClose}
            item={this.state.itemProduct}
            type={appConst.productPurchaseTypeCode.TSCD_CCDC}
            selectProduct={this.handleMapDataAsset}
            indexLinhKien={this.state.indexLinhKien}
          />
        )}
        {shouldOpenAddNewSkuDialog && (
          <StockKeepingUnitEditorDialog
            t={t}
            i18n={i18n}
            keySearch={keywordSearch.keywordSearch}
            handleClose={this.handleCloseAddNewSkuDialog}
            handleOKEditClose={this.handleCloseAddNewSkuDialog}
            open={shouldOpenAddNewSkuDialog}
            selectUnit={(data) => this.handleSelectUnit(data, keywordSearch.index, keywordSearch.nameList)}
          />
        )}
        {shouldOpenPlaningEditorDialog && (
          <PlaningDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleClosePopup}
            open={shouldOpenPlaningEditorDialog}
            handleOKEditClose={() => {
              this.handleClosePopup();
              this.props.handleClose();
            }}
            item={item}
            fromDate={convertFromToDate(date).fromDate}
            toDate={convertFromToDate(date).toDate}
            planingDepartment={countDepartment}
            purchaseRequests={[{ ...this.formatDataDto(this.state) }]}
          />
        )}
      </Dialog>
    );
  }
}

PurchaseRequestCountDialog.contextType = AppContext;
export default PurchaseRequestCountDialog;
