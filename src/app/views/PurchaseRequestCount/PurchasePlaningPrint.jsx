import React, { Component, useRef } from "react";
import {
  Dialog,
  Button,
  Grid, Checkbox,
  IconButton,
  Icon,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table'; import {
  getPurchaseRequest,
  getItemById,
} from "./PurchaseRequestCountService";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import DateToText from "../FormCustom/Component/DateToText";
import HeaderLeft from "../FormCustom/Component/HeaderLeft";
import Title from "../FormCustom/Component/Title";


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}



function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return <div>
    <IconButton onClick={() => props.onSelect(item, 1)}>
      <Icon color="error">delete</Icon>
    </IconButton>
  </div>;
}
export default class PurchasePlaningPrint extends Component {
  state = {
    type: 2,
    rowsPerPage: 999,
    page: 0,
    totalElements: 0,
    departmentId: '',
    asset: {},
    isView: false
  };

  handleFormSubmit = () => {

    // install();
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);
    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() { 
    let { open, handleClose, item, productName, type } = this.props;
    this.setState({ productName, type }, function () {
      this.setState(
        {
          ...this.props.item,
        },
        function () {
          if (this.state.productName) {
            this.search();
          }
        }
      );
    });

    this.setState({
      ...this.props.item
    }, function () {
    }
    );
   
  }

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.type = 1;
      searchObject.keyword = this.state.productName;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.fromDate = this.state.fromDate;
      getPurchaseRequest(searchObject).then(({ data }) => {
        let itemListClone = [...data.content];
        itemListClone.forEach((el) => {
          el.purchaseRequest.listProducts.forEach((item) => {
            if (
              el.product.id == item.product.id &&
              el.purchaseRequest.id == item.purchaseRequest.id
            ) {
              el.quantity = item.quantity;
            }
          });
        });
        this.setState({
          itemList: [...itemListClone],
          totalElements: data.totalElements,
        }, () => {
        });
      });
    });
  }
  componentDidMount() {
   
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n, item, productName } = this.props;
    let now = new Date();
    let {
      rowsPerPage,
      page,
      assetVouchers, itemList
    } = this.state;
    return (


      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="">{t('Phiếu tổng hợp yêu cầu mua sắm')}</span>
          {/* <IconButton onClick={handleClose} style={{ position: "absolute", top: 0, right:0 }}>
            <Icon className="text-black">clear</Icon>
          </IconButton> */}
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute', print: { size: 'auto', margin: '0mm' } }} title="ifmcontentstoprint"></iframe>

        <ValidatorForm className="validator-form-scroll-dialog" ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent id='divcontents' className="dialog-print">
            <Grid className="form-print">
              
              <div style={{ textAlign: 'center', fontFamily: "'Times New Roman', Times, serif", fontSize: "large" }}>
                
                <div style={{ display: 'flex', textAlign:"center" }}>
                  <div style={{ flexGrow: 1 }}>
                    <HeaderLeft textAlign={{textAlign:"left"}} unit={item?.product?.org?.name} />
                  </div>
                  <div style={{  flexGrow: 1, display:"flex", justifyContent:"end", alignItems: "center"}}>
                    <i>{item?.product?.code}</i>
                  </div>
                </div>
                <Title title="PHIẾU XÁC ĐỊNH NHU CẦU CẦN MUA" />
                <div style={{ flexGrow: 3, textAlign: 'left', marginTop: 40 }}>
                  <p>Căn cứ vào nhu cầu công tác.</p>
                  <p>Khoa, phòng: <span style={{textDecoration:"underline" ,textDecorationStyle: "dotted"}}>{item?.product?.managementPurchaseDepartment?.name}</span></p>
                  <p>Cần trang bị các trang thiết bị sau:  </p>
                </div>
                <div>
                  {
                    <table style={{ width: '100%', border: '1px solid', borderCollapse: 'collapse' }} className="table-report">
                    <tr>
                        <th style={{ border: '1px solid', width: '5%' }}>TT</th>
                        <th style={{ border: '1px solid', width: '65%' }} >Danh mục</th>
                        <th style={{ border: '1px solid', width: '15%' }} >ĐVT</th>
                        <th style={{ border: '1px solid', width: '15%', textAlign: 'center' }} >Số lượng</th>
                      </tr>


                      {itemList !== null ? itemList?.map((row, index) => {
                        // let number;
                        // let unitPrice = new Number(row.asset.unitPrice);
                        // if (unitPrice != null) {
                        //   let plainNumber = unitPrice.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        //   number = plainNumber.substr(0, plainNumber.length - 2);
                        // }
                        return (
                          <tbody>
                            <tr>
                              <td style={{ border: '1px solid', textAlign: "center" }}>{index !== null ? index + 1 : ''}</td>
                              <td style={{ border: '1px solid', textAlign: 'left', paddingLeft: 3 }}>{row?.product?.name}</td>
                              <td style={{ border: '1px solid', textAlign: 'left' , paddingLeft: 3 }}>{row?.skuName}</td>
                              <td style={{ border: '1px solid', textAlign: 'center' }}>{row.quantity}</td>
                            </tr>
                          </tbody>
                        )
                      }) : ''}
                    </table>
                  }
                </div>
                <div style={{ textAlign: 'left' }}>
                  <p style={{ textAlign: 'left', marginLeft:'3%'}}>Vậy kính đề xuất lãnh đạo Bệnh viện xét duyệt.</p>
                </div>
                 <div style={{ display: 'flex', textAlign:"center" }}>
                  <div style={{ width: "50%" }}>
                  </div>
                  <div style={{ width: "50%"}}>
                    <DateToText date={new Date()} noUppercase={true} isLocation={true} location={"Hà Nội"} />
                  </div>
                </div>
                 <div style={{ display: 'flex', textAlign:"center" }}>
                  <div style={{ width: "50%" }}>
                    <b>Trưởng Khoa Dược - VTTBYT</b>
                  </div>
                  <div style={{ width: "50%" }}>
                    <b>{item?.product?.managementPurchaseDepartment?.name}</b>
                  </div>
                </div>
                 <div style={{ display: 'flex', justifyContent:"center", marginTop: 80 }}>
                    <b>Lãnh đạo Bệnh viện</b>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleOKEditClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>

        </ValidatorForm>

      </Dialog>
    );
  }
}


// export default AssetTransferPrint;
