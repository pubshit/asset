import {
  AppBar,
  Icon,
  IconButton,
  Tab,
  TablePagination,
  Tabs,
} from "@material-ui/core";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  classStatus,
  convertFromToDate, formatTimestampToDate,
  getRole,
  getTheHighestRole, getUserInformation,
  isValidDate,
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { Breadcrumb } from "egret";
import { saveAs } from "file-saver";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import ComponentPurchaseRequestCountTable from "./ComponentPurchaseRequestCountTable";
import {
  deleteItem,
  exportToExcel,
  getByPrepareId,
  getCountStatus,
  getItemById,
  getPreparePlanProducts,
  searchByPage,
} from "./PurchaseRequestCountService";
import { LightTooltip, TabPanel } from "../Component/Utilities";

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let {
    hasEditPermission,
    isRoleAdmin,
    departmentUser
  } = props;
  const isDaTongHop = item?.status === appConst.tabPurchaseReqCount.tabCounted;
  const isDaLapKeHoach = item?.status === appConst.tabPurchaseReqCount.tabPlaned;
  const isDelete = !item?.purchasePlanId && (item?.departmentId === departmentUser?.id || isRoleAdmin);
  return (
    <div className="none_wrap">
      {hasEditPermission && !isDaTongHop && !isDaLapKeHoach && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {/* {((hasDeletePermission && !isDaTongHop && !isDaLapKeHoach)) && ( */}
      {isDelete && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class PurchaseRequestCountTable extends React.Component {
  state = {
    keyword: "",
    productName: "",
    rowsPerPage: appConst.rowsPerPageOptions.table[0],
    page: 0,
    PurchaseRequestCount: [],
    item: {},
    id: "",
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationDeleteListDialog: false,
    shouldOpenNotificationPopup: false,
    toDate: moment(new Date()).endOf("month"),
    fromDate: null,
    shouldOpenPlaningEditorDialog: false,
    managementDepartment: null,
    listManageDepartment: [],
    tabValue: appConst.tabPurchaseReqCount.tabAll,
    rowDataProductsTable: [],
    type: appConst.TYPE_PURCHASE.TSCD_CCDC,
    productPage: 0,
    productTotalElements: 0,
    productRowsPerPage: appConst.rowsPerPageOptions.table[0],
    hasEditPermission: false,
    hasDeletePermission: false,
    isCheckAll: false,
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state?.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });

    const statusMap = {
      [appConst.tabPurchaseReqCount.tabAll]: null,
      [appConst.tabPurchaseReqCount.tabCounting]: appConst.tabPurchaseReqCount.tabCounting,
      [appConst.tabPurchaseReqCount.tabCounted]: appConst.tabPurchaseReqCount.tabCounted,
      [appConst.tabPurchaseReqCount.tabPlaned]: appConst.tabPurchaseReqCount.tabPlaned,
    };
    this.search({ statusIndex: statusMap[newValue] });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.search({ page });
  };

  setProductPage = (productPage) => {
    this.setState({ productPage }, function () {
      this.updatePageProductData();
    });
  };

  setRowsPerPage = (event) => {
    this.search({ rowsPerPage: event.target.value, page: 0 });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  setProductRowsPerPage = (event) => {
    this.setState({
      productRowsPerPage: event.target.value,
      productPage: 0
    }, this.updatePageProductData);
  };

  handleChangeProductPage = (event, newPage) => {
    this.setProductPage(newPage);
  };

  search = (searchObject) => {
    const newObject = searchObject ? { ...searchObject } : { page: 0 };
    this.setState({ ...newObject, purchasePreparePlan: null }, () => this.updatePageData());
  };

  checkData = () => {
    if (!this.data || this.data.length === 0) {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "general.noti_check_data",
      });
    } else if (this.data.length === this.state.itemList.length) {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    } else {
      this.setState({ shouldOpenConfirmationDeleteListDialog: true });
    }
  };

  updatePageData = async () => {
    const {
      type,
      page,
      toDate,
      keyword,
      fromDate,
      statusIndex,
      rowsPerPage,
      managementDepartment,
      purchasePreparePlanVoucherIds = [],
    } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);
    let searchObject = {
      type,
      keyword,
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      toDate: convertFromToDate(toDate).toDate,
      fromDate: convertFromToDate(fromDate).fromDate,
    };
    if (statusIndex) {
      searchObject.status = statusIndex;
    }
    if (managementDepartment) {
      searchObject.managementDepartmentId = managementDepartment?.id;
    }

    try {
      const res = await searchByPage(searchObject);
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        const itemList = data?.content;

        if (itemList?.length > 0) {
          itemList.map((item) => {
            let checkItem = purchasePreparePlanVoucherIds?.some(
              (p) => p === item.id
            );
            item.checked = checkItem;
            return item;
          });
        }

        this.setState({
          itemList: data?.content,
          totalElements: data?.totalElements,
          rowDataProductsTable: [],
        });
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenNotificationPopup: false,
      shouldOpenPlaningEditorDialog: false,
      isPrint: false,
    }, () => {
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenPlaningEditorDialog: false,
      isPrint: false,
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleGetRowData = (rowData) => {
    this.setState(
      {
        purchasePreparePlanId: rowData?.id,
        purchasePreparePlan: rowData,
      },
      () => {
        this.updatePageProductData();
      }
    );
  };

  handleDeletePurchaseRequestStauts = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  componentDidMount() {
    let managementDepartment = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER
    );
    if (managementDepartment) {
      managementDepartment.text =
        managementDepartment.name + " - " + managementDepartment.code;
    }

    this.setState({ managementDepartment }, () => {
      this.updatePageData();
      this.getRoleCurrentUser();
    });
    this.getCountStatus();
  }

  handleSetData = (data, name) => {
    this.setState({ [name]: data }, () => {
      if (isValidDate(data) || data === null) {
        this.search();
      }
    }
    );
  };

  getRoleCurrentUser = () => {
    let { hasDeletePermission, hasEditPermission } = this.state;
    let {
      currentUser,
      isRoleAssetUser,
      isRoleAdmin,
      isRoleOrgAdmin,
      isRoleAssetManager,
      departmentUser
    } = getTheHighestRole();
    if (currentUser) {
      // người quản lý vật tư
      if (isRoleAssetManager) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }
      // người đại diện phòng ban
      if (isRoleAssetUser) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }
      // org
      if (isRoleOrgAdmin || isRoleAdmin) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }

      this.setState({
        hasDeletePermission,
        hasEditPermission,
        currentUser,
        isRoleAssetUser,
        isRoleAdmin,
        isRoleOrgAdmin,
        isRoleAssetManager,
        departmentUser
      });
    }
  };

  /* Export to excel */
  exportToExcel = () => {
    var searchObject = {};
    searchObject.formToDate = this.state.formToDate
      ? this.state.formToDate
      : moment().endOf("month");
    searchObject.formDate = this.state.formDate
      ? this.state.formDate
      : moment().startOf("month");
    searchObject.managementDepartmentId = this.state.managementDepartment
      ? this.state.managementDepartment.id
      : "";
    exportToExcel(searchObject)
      .then((res) => {
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "PurchaseRequestCount.xlsx");
      })
      .catch((err) => { });
  };

  checkStatus = (status, name) => {
    let itemStatus = appConst[name].find((item) => item.code === status);
    return itemStatus?.name;
  };

  handleEdit = (rowData) => {
    this.handleGetPreparePlanVoucher(rowData).then(() => {
      this.setState({
        isView: false,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handlePrint = (rowData) => {
    this.handleGetPreparePlanVoucher(rowData).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          currentUser: getRole()?.currentUser
        },
        isPrint: true,
      })
    });
  }

  handleConfirmationResponse = async () => {
    let { purchasePreparePlanId } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;
    try {
      setPageLoading(true);
      let res = await deleteItem(purchasePreparePlanId);
      const { code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      this.handleDialogClose();
      setPageLoading(false);
    }
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleView = async (rowData) => {
    this.handleGetPreparePlanVoucher(rowData).then(() => {
      this.setState({
        isView: true,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleGetPreparePlanVoucher = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);
    try {
      const res = await getItemById(rowData?.id);
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          item: data,
        });
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.edit === method) {
      this.handleEdit(rowData);
    } else if (appConst.active.delete === method) {
      this.handleDelete(rowData?.id);
    } else if (appConst.active.view === method) {
      this.handleView(rowData);
    } else if (appConst.active.print === method) {
      this.handlePrint(rowData);
    } else if (appConst.active.check === method) {
      this.handleToggleRowTable(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  handleToggleRowTable = (rowData, source) => {
    if (variable.listInputName.all === source) {
      this.handleCheckAll(rowData);
    } else {
      this.handleSingleCheck(rowData);
    }
  };

  handleAddPlanning = () => {
    let { purchaseRequests = [] } = this.state;
    let firstDepartment = purchaseRequests[0];
    let hasOtherDepartment = purchaseRequests?.some(
      item => item.departmentId !== firstDepartment?.departmentId
    );

    if (hasOtherDepartment) {
      return toast.warning("Các phiếu đã chọn không cùng phòng ban tổng hợp");
    }

    this.setState({
      item: {
        statusObj: appConst.listStatusPurchasePlaningObject.DANG_XU_LY,
        department: firstDepartment?.departmentId
          ? {
            name: firstDepartment?.departmentName,
            id: firstDepartment?.departmentId
          }
          : null,
        planDate: firstDepartment?.date
      },
      shouldOpenPlaningEditorDialog: true,
    });
  };

  updatePageProductData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    const {
      productPage,
      productRowsPerPage,
      purchasePreparePlanId,
      purchasePreparePlan,
    } = this.state;
    let searchObject = {
      pageIndex: productPage + 1,
      pageSize: productRowsPerPage,
      purchasePreparePlanIds: [purchasePreparePlanId],
      purchasePreparePlanId: purchasePreparePlanId,
    };
    try {
      setPageLoading(true)
      let apiGetProduct =
        purchasePreparePlan?.status !==
          appConst.listStatusPurchaseReqCountObject.DANG_XU_LY.code
          ? getPreparePlanProducts
          : getByPrepareId;

      let res = await apiGetProduct(searchObject);

      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          rowDataProductsTable: data?.content
            ? data?.content.sort((a, b) => a.status - b.status)
            : [],
          productTotalElements: data?.totalElements,
        });
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0
  }

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus(this.state.type)
      .then(({ data }) => {
        let countStatusProcessing,
          countStatusProcessed,
          countStatusPlanned;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusPurchaseReqCount[0].code === item?.trangThai) {
            countStatusProcessing = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusPurchaseReqCount[1].code === item?.trangThai) {
            countStatusProcessed = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusPurchaseReqCount[2].code === item?.trangThai) {
            countStatusPlanned = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusProcessing,
            countStatusProcessed,
            countStatusPlanned,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleSingleCheck = (rowData) => {
    let { purchaseRequests = [], itemList = [], } = this.state;
    let isDaTongHop = this.handleCheckVisibleColumns().isDaTongHop;
    let existItem = purchaseRequests?.find((x) => x.id === rowData?.id);
    let itemListDaTongHop = itemList?.filter(x => isDaTongHop(x));
    let count = 0;

    if (!existItem) {
      purchaseRequests.push(rowData);
    } else {
      purchaseRequests.map((item, index) =>
        item.id === existItem?.id ? purchaseRequests.splice(index, 1) : null
      );
    }

    itemList.map((item) => {
      if (item.id === rowData?.id) {
        item.checked = !existItem;
      }
      return item;
    });
    purchaseRequests?.forEach(x => {
      if (itemListDaTongHop?.map(item => item.id).includes(x.id)) {
        count++;
      }
    })

    this.setState({
      isCheckAll: count === itemListDaTongHop?.length,
      itemList,
      purchaseRequests,
      purchasePreparePlanVoucherIds: purchaseRequests?.map(x => x.id),
    });
  }

  handleCheckAll = (rowData) => {
    let { purchaseRequests = [], isCheckAll, itemList } = this.state;
    let isDaTongHop = this.handleCheckVisibleColumns().isDaTongHop;
    const isExist = (x) => itemList?.some(item => item.id === x.id);
    if (isCheckAll) {
      purchaseRequests = purchaseRequests?.filter(x => !isExist(x) && isDaTongHop(x))
    } else {
      let itemListDaTongHop = itemList.filter(item => isDaTongHop(item));
      purchaseRequests = purchaseRequests.concat(itemListDaTongHop);
    }

    this.setState({
      isCheckAll: !isCheckAll,
      itemList: itemList.map((item) => ({ ...item, checked: !isCheckAll })),
      purchaseRequests,
      purchasePreparePlanVoucherIds: purchaseRequests?.map(x => x.id),
    });
  }

  handleCheckVisibleColumns = () => {
    let { departmentUser } = getUserInformation();

    return {
      isDepartmentCreated: item => item?.requestDepartmentId === departmentUser?.id,
      isDaTongHop: item => item?.status === appConst.tabPurchaseReqCount.tabCounted,
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      productPage = 0,
      productRowsPerPage = 5,
      rowDataProductsTable,
      productTotalElements,
      isRoleAssetManager,
      isRoleAdmin,
      isRoleOrgAdmin,
      hasDeletePermission,
      hasEditPermission,
      currentUser,
      isCheckAll,
      departmentUser
    } = this.state;
    let TitlePage = t("purchase_request_count.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            isRoleAssetManager={isRoleAssetManager}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            isRoleAdmin={isRoleAdmin || isRoleOrgAdmin}
            currentUser={currentUser}
            departmentUser={departmentUser}
            onSelect={(rowData, method) => {
              this.handleCheckIcon(rowData, method);
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("purchase_request_count.countDate"),
        field: "",
        align: "left",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatTimestampToDate(rowData.date)
      },
      {
        title: t("SuggestedMaterials.ballotCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
      },
      {
        title: t("purchaseRequestCount.name"),
        field: "name",
        align: "left",
        minWidth: "150px",
      },
      {
        title: t("purchase_request_count.status"),
        field: "",
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span className={classStatus(statusIndex)}>{this.checkStatus(statusIndex, "listStatusPurchaseReqCount")}</span>
          );
        },
      },
      {
        title: t("purchase_request_count.countDepartment"),
        field: "departmentName",
        align: "left",
        minWidth: 150,
        cellStyle: {
        },
      },
      {
        title: t("purchase_request_count.countPerson"),
        field: "personName",
        align: "left",
        minWidth: 100,
        cellStyle: {
        },
      },
      {
        title: t("purchase_request_count.note"),
        field: "note",
        align: "left",
        minWidth: 150,
        cellStyle: {
        },
      },
    ];
    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: "50px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          productPage * productRowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "alterProductName",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.nameProduct"),
        field: "productName",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.unit"),
        field: "skuName",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchase_request_count.status"),
        field: "",
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span className={classStatus(statusIndex)}>{this.checkStatus(statusIndex, "listStatusProductInPurchaseRequestArray")}</span>
          );
        },
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.requestDepartment"),
        field: "requestDepartmentName",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.requestParameters"),
        field: "productSpecs",
        align: "left",
        width: "240px",
        minWidth: "240px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("purchaseRequestCount.note"),
        field: "note",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.purchase"),
                path: "list/purchase_request_count",
              },
              { name: t("Dashboard.Purchase.purchase_asset") },
              { name: TitlePage },
            ]}
          />
        </div>

        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("purchase_request_count.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchase_request_count.tabCounting")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusProcessing || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchase_request_count.tabCounted")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusProcessed || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchaseReqCount.tabAll}
          className="mp-0"
        >
          <ComponentPurchaseRequestCountTable
            t={t}
            i18n={i18n}
            columns={columns}
            item={this.state}
            handleSetData={this.handleSetData}
            getRowData={this.handleGetRowData}
            setRowsPerPage={this.setRowsPerPage}
            handleAddPlanning={this.handleAddPlanning}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            handleConfirmationResponse={this.handleConfirmationResponse}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchaseReqCount.tabCounting}
          className="mp-0"
        >
          <ComponentPurchaseRequestCountTable
            t={t}
            i18n={i18n}
            columns={columns}
            item={this.state}
            handleAddPlanning={this.handleAddPlanning}
            handleSetData={this.handleSetData}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            getRowData={this.handleGetRowData}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchaseReqCount.tabCounted}
          className="mp-0"
        >
          <ComponentPurchaseRequestCountTable
            t={t}
            i18n={i18n}
            columns={columns}
            item={this.state}
            handleAddPlanning={this.handleAddPlanning}
            handleSetData={this.handleSetData}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            getRowData={this.handleGetRowData}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabPurchaseReqCount.tabPlaned}
          className="mp-0"
        >
          <ComponentPurchaseRequestCountTable
            t={t}
            i18n={i18n}
            columns={columns}
            item={this.state}
            handleAddPlanning={this.handleAddPlanning}
            handleSetData={this.handleSetData}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            getRowData={this.handleGetRowData}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataProductsTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            component="div"
            count={productTotalElements}
            labelRowsPerPage={t("general.rows_per_page")}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            rowsPerPage={productRowsPerPage}
            page={productPage}
            backIconButtonProps={{
              "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page",
            }}
            onPageChange={this.handleChangeProductPage}
            onRowsPerPageChange={this.setProductRowsPerPage}
          />
        </div>
      </div>
    );
  }
}
PurchaseRequestCountTable.contextType = AppContext;
export default PurchaseRequestCountTable;
