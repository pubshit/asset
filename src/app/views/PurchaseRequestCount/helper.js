
export const sumApprovedQuantity = (array = []) => {
	return array?.reduce(
		(total, item) => total + Number(item.approvedQuantity || 0),
		0
	)
}

export const sumApprovedQtyByProduct = (array = [], prod) => {
	let newArray = filterArrayByProduct(array, prod);
	
	return newArray?.reduce(
		(total, item) => total + Number(item.approvedQuantity || 0),
		0
	)
}

export const findProductInArray = (array = [], prod = {}) => {
	return array?.find(
		pd => pd.productId === prod.alterProductId || pd.productId === prod.productId
	);
}

export const filterArrayByProduct = (array = [], prod = {}) => {
	return array?.filter(
		x => x.productId === prod.alterProductId || x.productId === prod.productId
	);
}
