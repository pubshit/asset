import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import {

  KeyboardDatePicker  
} from "@material-ui/pickers";
import { ValidatorForm, TextValidator, TextField, SelectValidator } from "react-material-ui-form-validator";
import { getByPage, deleteItem, saveItem, getItemById, checkCode } from "./EQAHealthOrgService";
import { getAllTestPurposes } from "../TestPurpose/TestPurposeService";
import { getAllEQAhealthOrgLevels } from "../HealthOrgLevel/HealthOrgLevelService"

import { getAllQualifications,getQualificationById } from '../Qualification/QualificationService';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";

import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import EQAPlanningSearchDialog from './EQAPlanningSearchDialog';
import EQAPOrgTypeSearchDialog from './EQAPOrgTypeSearchDialog';
import EQAHealthOrg from "./EQAHealthOrg";
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class EQAHealthOrgEditorDialog extends Component {
  state = {
    name: "",
    taxCodeOfTheUnit:"",
    code: "",specifyTestPurpose:"",
    specifyLevel: "",
    positiveAffirmativeRight:true,
    shouldOpenSearchDialog: false,
    shouldOpenConfirmationDialog: false,
    qualificationSelect: [],
    qualification: {},officerPosion:"",unitCodeOfProgramPEQAS:"",
    testpurposeSelect: [],
    testPurpose1: {},
    testPurpose2: {},
    testPurpose3: {},
    testPurpose4: {},
    levelHealOrg: [],
    fax:"",
    level: "",sampleReceiptDate : new Date(),
    sampleRecipient:"",specifySampleStatus:"",specifyQualification:""
  };
  handleDateChange = date => {
    this.setState({ sampleReceiptDate: date });
  };
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
 

  };
  handleChangeQualification=(event, source)=>{
    let{qualificationSelect}=this.state;
    this.setState({qualification:qualificationSelect.find(item=>item.id==event.target.value),
      qualificationId:event.target.value })
  }
  handleChangetestpurpose1=(event, source)=>{
    let{testpurposeSelect}=this.state;
    this.setState({testPurpose1:testpurposeSelect.find(item=>item.id==event.target.value),
      testPurpose1Id:event.target.value })
  }
  handleChangetestpurpose2=(event, source)=>{
    let{testpurposeSelect}=this.state;
    this.setState({testPurpose2:testpurposeSelect.find(item=>item.id==event.target.value),
      testPurpose2Id:event.target.value })
  }
  handleChangetestpurpose3=(event, source)=>{
    let{testpurposeSelect}=this.state;
    this.setState({testPurpose3:testpurposeSelect.find(item=>item.id==event.target.value),
      testPurpose3Id:event.target.value })
  } 
  handleChangetestpurpose4=(event, source)=>{
    let{testpurposeSelect}=this.state;
    this.setState({testPurpose4:testpurposeSelect.find(item=>item.id==event.target.value),
      testPurpose4Id:event.target.value })
  }
  handleChangelevel=(event, source)=>{
    let{levelHealOrg}=this.state;
    this.setState({level:levelHealOrg.find(item=>item.id==event.target.value),
      levelId:event.target.value })
  }
  
  handleFormSubmit = () => {
    let{id,code}=this.state;
    
    checkCode(id, code).then((result) => {
      if (result.data) {
        alert("Code đã sử dụng");
      } else {
        saveItem({
          ...this.state
        }).then(() => {
          this.props.handleOKEditClose();
        });
      }
    });
  }

  componentWillMount() {
    let { open, handleClose, item } = this.props;
    this.props)
    this.setState({
      ...this.props.item
    },function(){
      let {qualification,testPurpose1,testPurpose2,testPurpose3,testPurpose4,level} =this.state;
      if(qualification!=null&&qualification.id!=null){
        this.setState({qualificationId:qualification.id})
      }
      if(testPurpose1!=null&&testPurpose1.id!=null){
        this.setState({testPurpose1Id:testPurpose1.id})
      }
      if(testPurpose2!=null&&testPurpose2.id!=null){
        this.setState({testPurpose2Id:testPurpose2.id})
      }
      if(testPurpose3!=null&&testPurpose3.id!=null){
        this.setState({testPurpose3Id:testPurpose3.id})
      }
      if(testPurpose4!=null&&testPurpose4.id!=null){
        this.setState({testPurpose4Id:testPurpose4.id})
      }
      if(level!=null&&level.id!=null){
        this.setState({levelId:level.id})
      }
    }
    );


  

  }
  componentDidMount() {
    getAllQualifications().then(( data ) =>{
     let qualificationSelect=data.data;
      this.setState({qualificationSelect:qualificationSelect});
    });
    getAllTestPurposes().then(( data ) => {
    let testpurposeSelect=data.data;
    this.setState({ testpurposeSelect: testpurposeSelect })});

    getAllEQAhealthOrgLevels().then(( data ) => {
      let levelHealOrg=data.data;
    this.setState({ levelHealOrg : levelHealOrg })});
  }
  

  handleSearchDialogClose = () => {
    this.setState({
      shouldOpenSearchDialog: false
    });
  };
  handleSearchOrgTypeDialogClose = () => {
    this.setState({
      shouldOpenSearchOrgTypeDialog: false
    });
  };
  handleSelectHealthOrgType = (item) => {
    //alert('Test');
    this.setState({ healthOrgType: item });
    this.handleSearchOrgTypeDialogClose();
  }
  handleSelectAdministrativeUnitType = (item) => {
    //alert('Test');
    this.setState({ administrativeUnit: item });
    this.handleSearchDialogClose();
  }



  render() {
    let {
      id,
      name,
      publishDate,
      code,
      description,
      address,
      healthOrgType,
      contactName, levelId,
      contactPhone, levelHealOrg,
      shouldOpenSearchOrgTypeDialog,
      shouldOpenConfirmationDialog,
      shouldOpenSearchDialog, sampleReceiptDate,
      administrativeUnit, testPurpose1Id, testPurpose2Id, testPurpose3Id, testPurpose4Id,
      email, qualificationId, qualificationSelect, testpurposeSelect,specifyQualification,officerPosion,unitCodeOfProgramPEQAS,fax,
      sampleStatus,
      technician, specifyTechnician, positiveAffirmativeRight, sampleRecipient, specifySampleStatus,specifyTestPurpose,specifyLevel,taxCodeOfTheUnit

    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;

    return (
      <Dialog  open={open} PaperComponent={PaperComponent} maxWidth="md">
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <h4 className="mb-20">{t("SaveUpdate")}</h4>
        </DialogTitle>
        <DialogContent>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="" container spacing={2}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  disabled={true}
                  label={t("EQAHealthOrg.HealthOrgType")}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                  id="healthOrgType" className="w-80  mr-16" value={healthOrgType != null ? healthOrgType.name : ''} />
                <Button
                  className=""
                  className=" align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => this.setState({ shouldOpenSearchOrgTypeDialog: true, item: {} })}
                >
                  {t('Select')}
                </Button>
              </Grid>
              {shouldOpenSearchOrgTypeDialog && (
                <EQAPOrgTypeSearchDialog
                  open={this.state.shouldOpenSearchOrgTypeDialog}
                  handleSelect={this.handleSelectHealthOrgType}
                  selectedItem={healthOrgType != null ? healthOrgType : {}}
                  handleClose={this.handleSearchOrgTypeDialogClose} t={t} i18n={i18n} />
              )
              }
            <Grid item sm={4} xs={12}>
              <TextValidator
                className="w-100 "
                label={t("EQAHealthOrg.taxCodeOfTheUnit")}
                onChange={this.handleChange}
                type="text"
                name="taxCodeOfTheUnit"
                value={taxCodeOfTheUnit}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
            </Grid>
            <Grid item sm={4} xs={12}>
              <TextValidator
                className="w-100 "
                label={t("EQAHealthOrg.unitCodeOfProgramPEQAS")}
                onChange={this.handleChange}
                type="text"
                name="unitCodeOfProgramPEQAS"
                value={unitCodeOfProgramPEQAS}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
            </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.Name")}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.Code")}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.Address")}
                  onChange={this.handleChange}
                  type="text"
                  name="address"
                  value={address}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.fax")}
                  onChange={this.handleChange}
                  type="fax"
                  name="fax"
                  value={fax}
                />
              </Grid>

              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.ContactName")}
                  onChange={this.handleChange}
                  type="text"
                  name="contactName"
                  value={contactName}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.officerPosion")}
                  onChange={this.handleChange}
                  type="text"
                  name="officerPosion"
                  value={officerPosion}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.contactPhone")}
                  onChange={this.handleChange}
                  type="number"
                  name="contactPhone"
                  value={contactPhone}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.email")}
                  onChange={this.handleChange}
                  type="text"
                  name="email"
                  value={email}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="qualification">{t("EQAHealthOrg.qualification")}</InputLabel>
                  <Select
                    value={qualificationId}
                    onChange={event => this.handleChangeQualification(event)}
                    inputProps={{
                      name: "qualification",
                      id: "qualification"
                    }} 
                    
                    >
                    {qualificationSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                      
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.specifyQualification")+"("+t("if")+")"}
                  onChange={this.handleChange}
                  type="text"
                  name="specifyQualification"
                  value={specifyQualification}
                />
              </Grid>
              
              
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                    <InputLabel htmlFor="positiveAffirmativeRight">{t("EQAHealthOrg.positiveAffirmativeRight")}</InputLabel>
                  <Select
                    name="positiveAffirmativeRight"
                    value={positiveAffirmativeRight}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="positiveAffirmativeRight" />}
                  >
                    <MenuItem value={true}>{t("Yes")}</MenuItem>
                    <MenuItem value={false}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                    <InputLabel htmlFor="levelHealOrg">{t("EQAHealthOrg.levelHealOrg")}</InputLabel>
                  <Select
                   value={levelId}
                   onChange={event => this.handleChangelevel(event)}
                   inputProps={{
                     name: "level",
                     id: "level"
                   }}      
                  >{levelHealOrg.map(type => (
                    <MenuItem key={type.id} value={type.id}>
                      {type.name}
                    </MenuItem>
                  ))}
                
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.specifyLevel")}
                  name="specifyLevel"
                  onChange={this.handleChange}
                  value={specifyLevel}

                />
              </Grid>

             

              <Grid item sm={6} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="testurpose">{t("EQAHealthOrg.testurpose1")}</InputLabel>
                  <Select
                    value={testPurpose1Id}
                    onChange={event => this.handleChangetestpurpose1(event)}
                    inputProps={{
                      name: "testPurpose1",
                      id: "testPurpose1"
                    }}                   >
                    {testpurposeSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="testurpose">{t("EQAHealthOrg.testurpose3")}</InputLabel>
                  <Select
                     value={testPurpose3Id}
                     onChange={event => this.handleChangetestpurpose3(event)}
                     inputProps={{
                       name: "testPurpose3",
                       id: "testPurpose3"
                     }}   
                  >
                    {testpurposeSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item sm={6} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="testurpose">{t("EQAHealthOrg.testurpose2")}</InputLabel>
                  <Select
                     value={testPurpose2Id}
                     onChange={event => this.handleChangetestpurpose2(event)}
                     inputProps={{
                       name: "testPurpose2",
                       id: "testPurpose2"
                     }}   
                  >
                    {testpurposeSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="testurpose">{t("EQAHealthOrg.testurpose4")}</InputLabel>
                  <Select
                     value={testPurpose4Id}
                     onChange={event => this.handleChangetestpurpose4(event)}
                     inputProps={{
                       name: "testPurpose4",
                       id: "testPurpose4"
                     }}   
                  >
                    {testpurposeSelect.map(type => (
                      <MenuItem key={type.id} value={type.id}>
                        {type.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.specifyTestPurpose")}
                  name="specifyTestPurpose"
                  value={specifyTestPurpose}
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="none"
                    // id="mui-pickers-date"
                    label={t("EQAHealthOrg.sampleReceiptDate")}
                    inputVariant="standard"
                    type="text"
                    // autoOk={true}
                    fullWidth
                    value={sampleReceiptDate}
                    format="dd - MM - yyyy"
                    onChange={this.handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "change date"
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("EQAHealthOrg.sampleRecipient")}
                  onChange={this.handleChange}
                  name="sampleRecipient"
                  value={sampleRecipient}
                />
              </Grid>
              <Grid item sm={6} xs={12} >
                <FormControl className="w-100">
                  <InputLabel htmlFor="sampleStatus">{t("EQAHealthOrg.sampleStatus")}</InputLabel>
                  <Select
                    name="sampleStatus"
                    value={sampleStatus}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="sampleStatus" />}
                  >
                    <MenuItem value={0}>{t("good")}</MenuItem>
                    <MenuItem value={1}>{t("not good")}</MenuItem>

                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label="Tình trạng mẫu khác (nếu không tốt hoặc khác)"
                  onChange={this.handleChange}
                  name="specifySampleStatus"
                  value={specifySampleStatus}
                />
              </Grid>


              {shouldOpenSearchDialog && (
                <EQAPlanningSearchDialog
                  open={this.state.shouldOpenSearchDialog}
                  handleSelect={this.handleSelectAdministrativeUnit}
                  selectedItem={administrativeUnit != null ? administrativeUnit : {}}
                  handleClose={this.handleSearchDialogClose} t={t} i18n={i18n} />
              )
              }
            </Grid>
            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="primary" type="submit">
                {t('Save')}
              </Button>
              <Button variant="contained" color="secondary" onClick={() => handleClose()}> {t('Cancel')}</Button>
            </div>
          </ValidatorForm>
        </DialogContent>
      </Dialog >
    );
  }
}

export default EQAHealthOrgEditorDialog;
