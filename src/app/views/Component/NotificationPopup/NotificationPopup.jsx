import React from "react";
import { Dialog, Button,DialogActions } from "@material-ui/core";
const NotificationPopup = ({
  open,
  onConfirmDialogClose,
  text,
  title,
  agree,
  cancel,
  onYesClick,
  onNoClick
}) => {
  return (
    <Dialog
      maxWidth="xs"
      fullWidth={true}
      open={open}
      onClose={onConfirmDialogClose}
    >
      <div className="pt-24 px-20 pb-8">
        <h4 className="capitalize">{title}</h4>
        <p>{text}</p>
        <DialogActions>
            <div className="flex flex-space-between flex-middle">
              {cancel&&<Button
                variant="contained"
                color="secondary"
                onClick={onNoClick}
              >
                {cancel}
              </Button>}
             {agree&&<Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={onYesClick}
              >
               {agree}
              </Button>}
            </div>
          </DialogActions>

      </div>
    </Dialog>
  );
};

export default NotificationPopup;
