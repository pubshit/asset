import { Icon, IconButton, Popover, Typography, } from '@material-ui/core';
import React from 'react'
import HelpIcon from '@material-ui/icons/Help';
import { appConst } from 'app/appConst';

function NotePopup(props) {
  const { typeIndex, assetClass } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const renderMessage = () => {
    let item = {}
    if(appConst.assetClass.TSCD === assetClass) {
      item = appConst.TSCD_NOTE_MESSAGE.find(item => item.index === typeIndex);
    }
    else if(appConst.assetClass.CCDC === assetClass) {
      item = appConst.CCDC_NOTE_MESSAGE.find(item => item.index === typeIndex);
    }
    else {
      item = {}
    }

    if(item?.message instanceof Array) { // Đối với message muốn xuống dòng hoặc tách đoạn thì để dạng array
      return (
        <span>
          {item?.message?.map((ms, index) => (
            <div key={index}>
              {ms ?? ""}
              {index < item?.message?.length - 1 && <div>&nbsp;</div>}
            </div>
          ))}
        </span>
      )
    }
    else {
      return <span>{item?.message ?? ""}</span> // render 1 đoạn message
    }
  }

  return (
    <>
      <span onClick={handleClick}>
        <HelpIcon color='disabled' fontSize="small"/>
      </span>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <div className="max-width-400">

          <div className='flex justifyContentEnd'>
            <IconButton size="small" onClick={handleClose}>
              <Icon fontSize="small">
                close
              </Icon>
            </IconButton>
          </div>

          <Typography className="px-16 pb-16">
          {renderMessage()}
          </Typography>
        </div>
      </Popover>
    </>
  )
}

export default NotePopup
