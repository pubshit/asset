import {
  Grid,
  Button,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
  InputAdornment,
  Input,
} from "@material-ui/core";
import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { searchByPage, getByRoot, getByRootByAssetClass } from "../../AssetGroup/AssetGroupService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectAssetGroupPopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { assetClass, t } = this.props;
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = assetClass
      ? assetClass
      : appConst.assetClass.TSCD;
    try {
      setPageLoading(true);
      let res = await getByRootByAssetClass(searchObject)
      const { data } = res;
      let treeValues = [];

      let itemListClone = [...data?.content];

      itemListClone.forEach((item) => {
        var items = this.getListItemChild(item);
        treeValues.push(...items);
      });
      this.setState({
        itemList: treeValues,
        totalElements: data?.totalElements,
      });
    }
    catch (e) {
      toast.error(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: null, selectedItem: null });
    }
  };

  componentWillMount() {
    let { selectedItem } = this.props;
    //this.setState(item);
    this.setState({ selectedValue: selectedItem.id });
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  search() {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject)
      .then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      })
      .catch((err) => alert("ERROR"));
  }

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => {
    this.search();
  };
  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.id = item.id;
    root.depreciationRate = item.depreciationRate;
    root.yearApply = item.yearApply;
    root.parentId = item.parentId;
    root.namSuDung = item.namSuDung;
    root.viewIndex = item?.viewIndex;
    result.push(root);
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };
  render() {
    const { t, handleClose, handleSelect, open } = this.props;
    let { keyword, totalElements, rowsPerPage, page, selectedItem } =
      this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: 250,
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Radio
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={this.state.selectedValue === rowData.id}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("component.assetGroup.name"),
        field: "name",
        align: "left",
        width: "auto",
        style: "display-inline",
      },
      {
        title: t("component.assetGroup.depreciationRate"),
        field: "depreciationRate",
        align: "left",
        width: "auto",
      },
      {
        title: t("component.assetGroup.yearApply"),
        field: "yearApply",
        align: "left",
        width: "auto",
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        maxBodyHeight
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("component.assetGroup.title")}</span>
        </DialogTitle>
        <DialogContent style={{ overflow: "hidden" }}>
          <Grid item md={6} sm={12} xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDownEnterSearch}
              onKeyUp={this.handleKeyUp}
              className="w-100 mb-16"
              id="search_box"
              placeholder={t("general.enterSearch")}
              startAdornment={
                <InputAdornment position="start">
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
              columns={columns}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                // maxBodyHeight: "350px",
                // minBodyHeight: "253px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleSelect(selectedItem)}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

SelectAssetGroupPopup.contextType = AppContext;
export default SelectAssetGroupPopup;
