import React, { Component } from "react";
import { Button, Card, Dialog, DialogActions, Divider, Grid, Icon, IconButton } from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { EgretProgressBar } from 'egret'
import ImportExcelDialog from '../../Component/ImportExcel/ImportExcelDialog'
import FileSaver from 'file-saver';
import GetAppSharpIcon from '@material-ui/icons/GetAppSharp';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { createAssetDocument, updateAssetDocumentById, uploadFile, uploadFileOldApi } from "../../Asset/AssetService";
import AppContext from "app/appContext";
import { downLoadAttachment } from "app/views/MaintainResquest/MaintainRequestService";
import { appConst } from "app/appConst";
import { PaperComponent } from "../Utilities";
import PropTypes from "prop-types";
import { handleCheckFilesSize, handleThrowResponseMessage, isSuccessfulResponse } from "../../../appFunction";


class VoucherFilePopup extends Component {
  constructor(props) {
    super(props);
    this.filesInput = React.createRef();
  }

  state = {
    name: "",
    code: this.props.item?.code || "",
    description: "",
    shouldOpenImportExcelDialog: false,
    shouldOpenNotificationPopup: false,
    dragClass: "",
    attachments: [],
    files: [],
    statusList: [],
    queProgress: 0,
    progress: 0,
    assetId: null,
    documentType: this.props.documentType,
    fileDescriptionIds: [],
    maintainRequestId: null,
    voucherId: null,
    idVoucherFile: null,
    inputRef: React.createRef()
  };

  convertDto = (state) => {
    const { item, assetId } = this.props;
    return {
      idHoSoDK: state?.idVoucherFile || state?.id,
      name: state?.name || item?.name,
      code: state?.code || item?.code,
      description: state?.description || item?.description,
      assetId: state?.assetId || assetId,
      documentType: state?.documentType,
      fileDescriptionIds: state?.fileDescriptionIds,
      maintainRequestId: state?.maintainRequestId,
      voucherId: state?.voucherId
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    const { idVoucherFile, id } = this.state;

    if (this.state?.fileDescriptionIds.length <= 0) {
      toast.warning("Chưa có hoặc chưa tải tập tin");
      return;
    }
    if (this.state?.fileDescriptionIds.length <= 0) {
      toast.warning("Chưa có hoặc chưa tải tập tin");
      return;
    }

    try {
      setPageLoading(true);
      let dataState = this.convertDto(this.state)
      let res = idVoucherFile || id
        ? await updateAssetDocumentById({ ...dataState })
        : await createAssetDocument({ ...dataState });

      if (isSuccessfulResponse(res?.status)) {
        toast.success(res?.data?.message)
        this.props.handleClose();
        (idVoucherFile || id)
          ? this.props.handleUpdateAssetDocument(res?.data?.data)
          : this.props.getAssetDocument(res?.data?.data);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };


  componentDidMount() {
    let { item, configItem = () => { } } = this.props;
    let isEditAssetDocument = item?.isEditAssetDocument || this.props?.isEditAssetDocument;
    let attachments = item?.item?.attachments || item?.attachments || [];
    let fileDescriptionIds = attachments.map(attachment => attachment?.file?.id);
    let files = [];
    let convertedItem = {
      ...this.convertItem(), // todo: Trường hợp configItem không convert đủ trường thì vẫn phải rải value converted default
      ...(configItem(item) || {})
    };
    attachments?.forEach(element => {
      let rest = Object.assign(element, { 'isEditAssetDocument': convertedItem?.isEditAssetDocument });
      files.push(rest);
    });

    this.setState({
      ...convertedItem,
      ...attachments,
      fileDescriptionIds,
      files: files ? files : item?.item?.attachments || item?.attachments,
    });
  }

  convertItem = () => {
    let { item, isIAT, isTSCD } = this.props;

    return {
      code: item?.item?.code || item?.code,
      idVoucherFile: item?.item?.id,
      id: isIAT ? item?.item?.id : isTSCD ? item?.id : null,
      voucherId: isIAT ? item?.id : null,
      name: isTSCD ? item?.name : item?.item?.name || (typeof item?.name === "string" ? item?.name : ""),
      description: item?.item?.description || item?.description,
      isEditAssetDocument: Boolean(item?.item?.id || item?.id)
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
    this.updatePageData()
  }

  handleFileUploadOnSelect = event => {
    let files = event.target.files;
    this.fileUpload(files[0]).then(() => {
      toast.info("Tải tập tin thành công");
    });
  }

  handleFileSelect = event => {
    let files = event.target.files;
    let arrayFromFiles = Array.from(files);
    let list = [...this.state.files];

    if (handleCheckFilesSize(arrayFromFiles)) {
      return this.filesInput.current.value = null;
    }

    for (const iterator of files) {
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      files: [...list]
    }, () => this.uploadSingleFile(this.state.files?.length - 1));
    // Đặt lại giá trị input thi upload
    this.filesInput.current.value = null;
  };

  handleSingleRemove = index => {
    let files = [...this.state.files];
    let attachments = [...this.state.attachments];
    let fileDescriptionIds = [...this.state.fileDescriptionIds];
    files.splice(index, 1);
    attachments.splice(index, 1);
    fileDescriptionIds.splice(index, 1);
    this.setState({
      files: [...files],
      attachments: [...attachments],
      fileDescriptionIds: [...fileDescriptionIds]
    });
  };

  fileUpload = (file) => {
    let { isTSCD, t, isIAT } = this.props;
    let { setPageLoading } = this.context;
    let formData = new FormData();
    formData.append('uploadfile', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    setPageLoading(true);

    let uploadFunction = (isTSCD || isIAT) ? uploadFileOldApi(formData) : uploadFile(formData)

    return uploadFunction.then((res) => {
      const data = res.data;
      if (isSuccessfulResponse(res.status)) {
        setPageLoading(false)
        let attachment = (isTSCD || isIAT) ? data.file : data?.data;
        let { attachments, fileDescriptionIds } = this.state;
        attachments.push(attachment);
        fileDescriptionIds.push(
          isIAT
            ? attachment?.id
            : attachment?.id
        );
        this.setState({ attachments, fileDescriptionIds });
        toast.success(t("general.uploadFileSucess"));
      }
    })
      .catch(() => {
        toast.error(t("general.error"));
        setPageLoading(false)
      })
  }

  uploadSingleFile = async (index) => {
    let { t } = this.props;
    let allFiles = [...this.state.files];
    let file = this.state.files[index];

    try {
      if (file) {
        await this.fileUpload(file.file)
        allFiles[index] = { ...file, uploading: true, success: true, error: false };
        this.setState({
          files: [...allFiles]
        });
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      this.filesInput.current.value = null;
    }
  };

  handleViewDocument = async (index) => {
    const { setPageLoading } = this.context
    const { t } = this.props;
    const { TYPES_FILE } = appConst;
    let file = this.state.files[index];
    let contentType = file.file.contentType;
    let fileName = file.file.name;

    try {
      const result = await downLoadAttachment(file?.file?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = new Blob([document], { type: contentType });
        if (
          file.type === TYPES_FILE.PDF
          || file.type === TYPES_FILE.TEXT
          || TYPES_FILE.IMAGE_ARRAY.includes(file.type)
        ) {
          let fileURL = URL.createObjectURL(file, fileName);
          window.open(fileURL, '_blank');
          URL.revokeObjectURL(fileURL);
        } else {
          toast.warning(t("general.viewAttachmentError"))
        }
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    }
  };

  handleDownloadDocument = async index => {
    const { setPageLoading } = this.context
    const { t } = this.props
    let file = this.state.files[index];
    let contentType = file?.file?.contentType;
    let fileName = file?.file?.name;
    try {
      setPageLoading(true)
      const result = await downLoadAttachment(file?.file?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = new Blob([document], { type: contentType });
        return FileSaver.saveAs(file, fileName);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    }
  };

  render() {
    let { open, t, i18n, item } = this.props;
    let { files } = this.state;
    let isEmpty = files.length === 0;
    const isViewItem = item?.isView ? item?.isView : item?.isVisibility;
    const isShowItem = item?.isView ? !item?.isView : !item?.isVisibility;
    let {
      name,
      description,
      shouldOpenImportExcelDialog,
    } = this.state;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
          <h4 style={{ marginBottom: '0px' }} className="">{t('general.saveUpdate')}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          {shouldOpenImportExcelDialog && (
            <ImportExcelDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenImportExcelDialog}
              handleOKEditClose={this.handleOKEditClose}
            />
          )}
          <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }}>
            <Grid className="" container spacing={2}>
              <Grid item md={3} sm={12} xs={12} hidden={true}>
                {/* Mã hồ sơ tài sản */}
                <div className="mt-24"><label
                  style={{ fontWeight: 'bold' }}>{t('AssetFile.code')} : </label> {this.state.code || item?.code}</div>
              </Grid>

              <Grid item md={5} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('AssetFile.name')}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name || ""}
                  InputProps={{
                    readOnly: isViewItem
                  }}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>

              <Grid item md={7} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t('AssetFile.description')}
                  onChange={this.handleChange}
                  type="text"
                  name="description"
                  value={description || ""}
                  InputProps={{
                    readOnly: isViewItem
                  }}
                />
              </Grid>

            </Grid>
            <div className="mt-12">
              <div className="flex flex-wrap">
                <label htmlFor="upload-single-file">
                  {isShowItem && (
                    <Button
                      size="small"
                      className="capitalize"
                      component="span"
                      variant="contained"
                      color="primary"

                    >
                      <div className="flex flex-middle">
                        <span>{t('general.select_file')}</span>
                      </div>
                    </Button>
                  )}
                </label>
                <input
                  ref={this.filesInput}
                  className="display-none"
                  onChange={this.handleFileSelect}
                  id="upload-single-file"
                  type="file"
                />
                <div className="px-16"></div>
              </div>
              <Card className="mb-24" elevation={2}>
                <div className="p-16">
                  <Grid
                    container
                    spacing={2}
                    justifyContent="center"
                    alignItems="center"
                    direction="row"
                  >
                    <Grid item lg={4} md={4}>
                      {t('general.file_name')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.size')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.action')}
                    </Grid>
                  </Grid>
                </div>
                <Divider></Divider>

                {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

                {(files || item?.attachments)?.map((item, index) => {
                  let { file, success, error, progress, isEditAssetDocument, id } = item;
                  return (
                    <div className="px-16 py-8" key={file.name}>
                      <Grid
                        container
                        spacing={2}
                        justifyContent="center"
                        alignItems="center"
                        direction="row"
                      >
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          <div className="word-break">{file?.name}</div>
                        </Grid>
                        {(file?.id) ? (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file?.contentSize / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        ) : (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file.size / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        )}
                        {(isEditAssetDocument || success) ? (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={100}></EgretProgressBar>
                          </Grid>
                        ) : (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={progress}></EgretProgressBar>
                          </Grid>
                        )}
                        <Grid item lg={1} md={1} sm={12} xs={12}>
                          {error && <Icon fontSize="small" color="error">error</Icon>}
                          {/* {uploading && <Icon className="text-green">done</Icon>} */}
                        </Grid>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          <div className="flex">
                            {!isEditAssetDocument && (
                              <IconButton disabled={success} size="small" title={t('general.upload')}
                                onClick={() => this.uploadSingleFile(index)}>
                                <Icon color={success ? "disabled" : "primary"} fontSize="small">cloud_upload</Icon>
                              </IconButton>
                            )}
                            {isEditAssetDocument && (<IconButton size="small" title={t('general.viewDocument')}
                              onClick={() => this.handleViewDocument(index)}>
                              <Icon fontSize="small" color="primary">visibility</Icon>
                            </IconButton>
                            )}

                            {isEditAssetDocument && (<IconButton size="small" title={t('general.downloadDocument')}
                              onClick={() => this.handleDownloadDocument(index)}>
                              <Icon fontSize="small" color="default"><GetAppSharpIcon /></Icon>
                            </IconButton>
                            )}
                            {isShowItem && (
                              <IconButton size="small" title={t('general.removeDocument')}
                                onClick={() => this.handleSingleRemove(index)}>
                                <Icon fontSize="small" color="error">delete</Icon>
                              </IconButton>
                            )}
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  );
                })}
              </Card>
            </div>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                className="mr-16"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>

            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

VoucherFilePopup.contextType = AppContext;
VoucherFilePopup.propTypes = {
  t: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  getAssetDocument: PropTypes.func.isRequired,
  handleUpdateAssetDocument: PropTypes.func.isRequired,
  configItem: PropTypes.func,                                     // (item) => ({...item, ...})
  open: PropTypes.bool.isRequired,
  item: PropTypes.object,
  itemAssetDocument: PropTypes.object,
  documentType: PropTypes.any,
}
export default VoucherFilePopup;
