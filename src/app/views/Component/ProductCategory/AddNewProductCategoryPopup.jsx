import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  CircularProgress,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  getAllProductCategorys,
  checkParent,
  addNewOrUpdateProductCategory,
  checkCode,
} from "../../ProductCategory/ProductCategoryService";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import clsx from "clsx";
import NotificationPopup from "../NotificationPopup/NotificationPopup";
import "../../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import { appConst } from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class AddNewProductCategoryPopup extends Component {
  state = {
    name: "",
    code: "",
    parent: {},
    parentSelect: {},
    assetClass: "",
    assetClassParent: appConst.assetType,
    shouldOpenNotificationPopup: false,
    Notification: "",
    loading: false,
  };

  handleChangeParent = (event, source) => {
    let { parentSelect } = this.state;
    this.setState({
      parent: parentSelect.find((item) => item.id === event.target.value),
      parentId: event.target.value,
    });
  };

  handleChangeAssetClass = (event, source) => {
    this.setState({ assetClass: event.target.value });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  }
  handleFormSubmit = async () => {
    await this.openCircularProgress();
    let { id, code } = this.state;
    let { setPageLoading } = this.context;
    let { t, selectProductCategory } = this.props;
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(t("ProductCategory.noti.dupli_code"));
        toast.clearWaitingQueue();
        this.setState({ loading: false });
      } else {
        if (id) {
          checkParent({ ...this.state }).then((isCheck) => {
            if (isCheck.data) {
              this.setState({ loading: false });
              toast.error(t("ProductCategory.noti.updateFailParent"));
              toast.clearWaitingQueue();
            } else {
              setPageLoading(true);
              addNewOrUpdateProductCategory({
                ...this.state,
              }).then(({ data }) => {
                toast.success(t("ProductCategory.noti.updateSuccess"));
                this.props.handleOKEditClose();
                setPageLoading(false);
              }).catch(err => {
                toast.error(t("general.error"));
                setPageLoading(false);
              });
            }
          });
        } else {
          setPageLoading(true);
          addNewOrUpdateProductCategory({
            ...this.state,
          }).then(({ data }) => {
            toast.success(t("ProductCategory.noti.addSuccess"));
            if (data) {
              selectProductCategory(data);
            }
            this.props.handleOKEditClose();
            setPageLoading(false);
          }).catch(err => {
            toast.error(t("general.error"));
            setPageLoading(false);
          });
        }
      }
    });
  };

  handleCheckAssetType = (type) => {
    let { item } = this.props;
    let { assetClassParent } = this.state;
    let { VT } = appConst.assetTypeObject;
    let code = null;
    if (type === VT.code || type === "VTHH") {
      code = VT.code;
    } else {
      code = appConst.assetTypeObject[type]?.code;
    }
    if (!code) {
      return assetClassParent.find(x => x.code === item?.productType?.code);
    }
    return code;
  };

  componentWillMount() {
    let { item } = this.props
    getAllProductCategorys().then((result) => {
      let parentSelect = result.data.content;
      this.setState({ parentSelect: parentSelect });
    });
    this.setState(
      {
        ...this.props.item,
        assetClass: item?.productType
          ? this.handleCheckAssetType(item?.productType?.code)
          : null
      },
      function () {
        let { parent } = this.state;
        if (parent != null && parent.id != null) {
          this.setState({ parentId: parent.id });
        }
      }
    );
  }
  componentDidMount() { }

  render() {
    let {
      open,
      t,
    } = this.props;
    let {
      name,
      code,
      parentId,
      parentSelect,
      assetClass,
      assetClassParent,
      shouldOpenNotificationPopup,
      loading,
    } = this.state;
    let { productType } = this.props.item;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xs">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle className="cursor-move pb-0" id="draggable-dialog-title">
          <h4 className="">{t("ProductCategory.saveUpdate")}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductCategory.code")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductCategory.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControl className=" w-100" style={{ width: "400px" }}>
                  <InputLabel htmlFor="assetClass">
                    {t("ProductCategory.type")}
                  </InputLabel>
                  <Select
                    value={assetClass}
                    onChange={(event) => this.handleChangeAssetClass(event)}
                    inputProps={{
                      name: "assetClass",
                      id: "assetClass",
                    }}
                    disabled={productType?.code}
                  >
                    {assetClassParent &&
                      assetClassParent.length > 0 &&
                      assetClassParent.map((type) => (
                        <MenuItem key={type.code} value={type.code}>
                          {type.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControl className=" w-100" style={{ width: "400px" }}>
                  <InputLabel htmlFor="parent">
                    {t("ProductCategory.parent")}
                  </InputLabel>
                  <Select
                    value={parentId}
                    onChange={(event) => this.handleChangeParent(event)}
                    inputProps={{
                      name: "parent",
                      id: "parent",
                    }}
                  >
                    {parentSelect &&
                      parentSelect.length > 0 &&
                      parentSelect.map((type) => (
                        <MenuItem key={type.id} value={type.id}>
                          {type.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                className="mr-14"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AddNewProductCategoryPopup.contextType = AppContext;
export default AddNewProductCategoryPopup;
