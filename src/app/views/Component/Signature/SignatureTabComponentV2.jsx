import { Grid } from "@material-ui/core";
import CustomMaterialTable from "../../CustomMaterialTable";
import { MTableToolbar } from "material-table";
import React, { useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { appConst, DATA_TYPE, variable } from "../../../appConst";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { TextValidator } from "react-material-ui-form-validator";
import { searchByPageUsersDepartmentNew } from "../../User/UserService";

export const SignatureTabComponentV2 = (props) => {
  const { t } = useTranslation();
  const {
    isExport,                       // true for receipt, allocation, delivery
    data = {},                      // get value only 1 time when it mounted
    permission = {},                // get value only 1 time when it mounted
    handleChange = (data) => { },   // onChange and return data (type []) outside
  } = props;
  const [itemList, setItemList] = useState([
    { code: variable.listInputName.headOfDepartment, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.deputyDepartment, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.director, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.deputyDirector, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.chiefAccountant, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.storekeeper, value: null, type: DATA_TYPE.COMPLEX },
    { code: variable.listInputName.createdPerson, value: null, type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX },
    { code: variable.listInputName.handoverPerson, value: null, type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX },
    { code: variable.listInputName.receiverPerson, value: null, type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX },
  ]);
  const [listUser, setListUser] = useState([]);

  const positionNames = {
    [variable.listInputName.headOfDepartment]: t("Department.headOfDepartment"),
    [variable.listInputName.deputyDepartment]: t("Department.deputyDepartment"),
    [variable.listInputName.director]: t("organization.hospitalDirector"),
    [variable.listInputName.deputyDirector]: t("organization.deputyDirector"),
    [variable.listInputName.chiefAccountant]: t("organization.chiefAccountant"),
    [variable.listInputName.storekeeper]: t("Store.inventoryController"),
    [variable.listInputName.createdPerson]: t("allocation_asset.creator"),
    [variable.listInputName.handoverPerson]: t("allocation_asset.handoverPerson"),
    [variable.listInputName.receiverPerson]: t("allocation_asset.receiverPerson"),
  }
  const columns = [
    {
      title: t("general.index"),
      field: "stt",
      minWidth: 50,
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.tableData.id + 1,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
      render: rowData => positionNames[rowData.code] || ""
    },
    {
      title: t("Reception.columns.personName"),
      field: "value",
      align: "left",
      minWidth: 150,
      render: rowData => {
        return rowData?.type === DATA_TYPE.TEXT
          ? (
            <TextValidator
              fullWidth
              size="small"
              name="value"
              value={rowData.value || ""}
              onChange={(e) => handleChangeRowData(e.target.value, rowData)}
              InputProps={{
                readOnly: Boolean(rowData?.isView),
                disableUnderline: Boolean(rowData?.isView),
              }}
            />
          ) : (
            <AsynchronousAutocompleteSub
              searchFunction={searchByPageUsersDepartmentNew}
              searchObject={{ ...appConst.OBJECT_SEARCH_MAX_SIZE }}
              listData={listUser}
              setListData={setListUser}
              displayLable="displayName"
              typeReturnFunction="category"
              onSelect={(value) => handleChangeRowData(value, rowData)}
              value={rowData?.value || null}
              noOptionsText={t("general.noOption")}
              readOnly={Boolean(rowData?.isView)}
            />
          )
      }
    },
  ];

  const dataProp = useMemo(() => ({ ...(data || {}) }), []);
  const permissionProp = useMemo(() => ({ ...(permission || {}) }), []);

  useEffect(() => {
    let newData = [
      {
        code: variable.listInputName.director,
        isView: permissionProp[variable.listInputName.director],
        value: dataProp?.directorId
          ? {
            displayName: dataProp?.directorName,
            personId: dataProp?.directorId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.deputyDirector,
        isView: permissionProp[variable.listInputName.deputyDirector],
        value: dataProp?.deputyDirectorId
          ? {
            displayName: dataProp?.deputyDirectorName,
            personId: dataProp?.deputyDirectorId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.chiefAccountant,
        isView: permissionProp[variable.listInputName.chiefAccountant],
        value: dataProp?.chiefAccountantId
          ? {
            displayName: dataProp?.chiefAccountantName,
            personId: dataProp?.chiefAccountantId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.headOfDepartment,
        isView: permissionProp[variable.listInputName.headOfDepartment],
        value: dataProp?.headOfDepartmentId
          ? {
            displayName: dataProp?.headOfDepartmentName,
            personId: dataProp?.headOfDepartmentId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.deputyDepartment,
        isView: permissionProp[variable.listInputName.deputyDepartment],
        value: dataProp?.deputyDepartmentId
          ? {
            displayName: dataProp?.deputyDepartmentName,
            personId: dataProp?.deputyDepartmentId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.storekeeper,
        isView: permissionProp[variable.listInputName.storekeeper],
        value: dataProp?.storekeeperId
          ? {
            displayName: dataProp?.storekeeperName,
            personId: dataProp?.storekeeperId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.createdPerson,
        isView: permissionProp[variable.listInputName.createdPerson],
        value: isExport ? dataProp?.createdPersonName : dataProp?.createdPersonId
          ? {
            displayName: dataProp?.createdPersonName,
            personId: dataProp?.createdPersonId
          }
          : null,
        type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.handoverPerson,
        isView: permissionProp[variable.listInputName.handoverPerson],
        value: isExport ? dataProp?.handoverPersonName : dataProp?.handoverPersonId
          ? {
            displayName: dataProp?.handoverPersonName,
            personId: dataProp?.handoverPersonId
          }
          : dataProp?.handoverPersonName || null,
        type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX,
      },
      {
        code: variable.listInputName.receiverPerson,
        isView: permissionProp[variable.listInputName.receiverPerson],
        value: isExport ? dataProp?.receiverPersonName : dataProp?.receiverPersonId
          ? {
            displayName: dataProp?.receiverPersonName,
            personId: dataProp?.receiverPersonId
          }
          : dataProp?.receiverPersonName || null,
        type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX,
      }
    ];
    setItemList([
      ...newData,
    ])
  }, [dataProp]);

  useEffect(() => {
    const newData = convertOutputData();
    handleChange(newData);
  }, [itemList]);

  const convertOutputData = () => {
    let data = {};
    itemList?.forEach(item => {
      if (item.type === DATA_TYPE.TEXT) {
        data[item.code + "Name"] = item.value;
      } else if (item.type === DATA_TYPE.COMPLEX) {
        data[item.code + "Id"] = item.value?.personId;
        data[item.code + "Name"] = item.value?.displayName;
      }
    })
    return data;
  }

  const handleChangeRowData = (value, row) => {
    let newArray = itemList.map(x => {
      if (x.code === row?.code) {
        x.value = value;
      }
      return x;
    })

    setItemList([...newArray]);
  }

  return (
    <Grid container>
      <Grid item xs={12} className="mt-16">
        <CustomMaterialTable
          data={itemList || []}
          columns={columns}
          options={{
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            minBodyHeight: 300,
          }}
          components={{
            Toolbar: (props) => (
              <div>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        />
      </Grid>
    </Grid>
  )
}

SignatureTabComponentV2.propTypes = {
  data: PropTypes.array,
  permission: PropTypes.object.isRequired,
  isExport: PropTypes.bool,
  handleChange: PropTypes.func,
}
