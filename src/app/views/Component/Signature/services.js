import axios from "axios";
import ConstantList from "../../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/voucher-attributes";

export const createSignature = (data) => {
  return axios.post(API_PATH, data);
};

export const getSignature = (voucherId) => {
  let config = {
    params: { voucherId },
  };
  return axios.get(API_PATH, config);
};
