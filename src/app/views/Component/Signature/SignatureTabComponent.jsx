import {Grid} from "@material-ui/core";
import CustomMaterialTable from "../../CustomMaterialTable";
import {MTableToolbar} from "material-table";
import React, {useEffect, useMemo, useState} from "react";
import PropTypes from "prop-types";
import {useTranslation} from "react-i18next";
import {appConst, DATA_TYPE, variable} from "../../../appConst";
import {getUserInformation} from "../../../appFunction";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {TextValidator} from "react-material-ui-form-validator";
import {searchByPageUsersDepartmentNew} from "../../User/UserService";

export const SignatureTabComponent = (props) => {
  const {t} = useTranslation();
  const {
    isExport,                       // true for receipt, allocation, delivery
    data = {},                 // get value only 1 time when it mounted
    handleChange = () => {},   // onChange and return data (type []) outside
    managementDepartmentId
  } = props;
  const {organization} = getUserInformation();
  const [itemList, setItemList] = useState([
    {code: variable.listInputName.headOfDepartment, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.deputyDepartment, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.director, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.deputyDirector, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.chiefAccountant, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.storekeeper, value: null, type: DATA_TYPE.COMPLEX},
    {code: variable.listInputName.handoverPerson, value: null, type: !isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX},
    {code: variable.listInputName.receiverPerson, value: null, type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX},
    {code: variable.listInputName.createdPerson, value: null, type: isExport ? DATA_TYPE.TEXT : DATA_TYPE.COMPLEX},
  ]);
  const [listUserByDepartment, setListUserByDepartment] = useState([]);
  const [listAllUser, setListAllUser] = useState([]);
  const [isView, setIsView] = useState({
    headOfDepartment: false,
    deputyDepartment: false,
    director: false,
    deputyDirector: false,
    chiefAccountant: false,
    storekeeper: false,
    handoverPerson: false,
    receiverPerson: false,
    createdPerson: false,
  });
  
  const userByDepartmentSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    mainDepartmentId: managementDepartmentId,
    orgId: organization?.org?.id,
  }
  const positionNames = {
    [variable.listInputName.headOfDepartment]: t("Department.headOfDepartment"),
    [variable.listInputName.deputyDepartment]: t("Department.deputyDepartment"),
    [variable.listInputName.director]: t("organization.hospitalDirector"),
    [variable.listInputName.deputyDirector]: t("organization.deputyDirector"),
    [variable.listInputName.chiefAccountant]: t("organization.chiefAccountant"),
    [variable.listInputName.storekeeper]: t("Store.inventoryController"),
    [variable.listInputName.handoverPerson]: t("allocation_asset.handoverPerson"),
    [variable.listInputName.receiverPerson]: t("allocation_asset.receiverPerson"),
    [variable.listInputName.createdPerson]: t("MaintenanceProposal.person"),
  }
  const columns = [
    {
      title: t("general.index"),
      field: "stt",
      minWidth: 50,
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.tableData.id + 1,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
      render: rowData => positionNames[rowData.code] || ""
    },
    {
      title: t("Reception.columns.personName"),
      field: "value",
      align: "left",
      minWidth: 150,
      render: rowData => rowData?.type === DATA_TYPE.TEXT
        ? (
          <TextValidator
            fullWidth
            size="small"
            name="value"
            value={rowData.value || ""}
            disabled={isView[rowData.code]}
            onChange={(e) => handleChangeRowData(e.target.value, rowData)}
          />
        ) : (
          <AsynchronousAutocompleteSub
            searchFunction={searchByPageUsersDepartmentNew}
            searchObject={
              rowData.isGetAllUser
              ? {...appConst.OBJECT_SEARCH_MAX_SIZE}
              : userByDepartmentSearchObject
            }
            listData={rowData.isGetAllUser ? listAllUser : listUserByDepartment}
            setListData={rowData.isGetAllUser ? setListAllUser : setListUserByDepartment}
            nameListData={rowData.isGetAllUser ? "listAllUser" : "listUserByDepartment"}
            displayLable="displayName"
            typeReturnFunction="category"
            selectedOptionKey="personId"
            disabled={isView[rowData.code]}
            onSelect={(value) => handleChangeRowData(value, rowData)}
            value={rowData?.value || null}
            noOptionsText={t("general.noOption")}
          />
        )
    },
  ];
  
  const dataProp = useMemo(() => ({...(data || {})}), []);

  useEffect(() => {
    let {
      headOfDepartment,
      deputyDepartment,
      director,
      deputyDirector,
      chiefAccountant,
      storekeeper,
      handoverPerson,
      receiverPerson,
      createdPerson,
    } = variable.listInputName;
    let newData = [
      {
        code: director,
        value: dataProp?.directorId
          ? {
            displayName: dataProp?.directorName,
            personId: dataProp?.directorId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: true
      },
      {
        code: deputyDirector,
        value: dataProp?.deputyDirectorId
          ? {
            displayName: dataProp?.deputyDirectorName,
            personId: dataProp?.deputyDirectorId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: true
      },
      {
        code: chiefAccountant,
        value: dataProp?.chiefAccountantId
          ? {
            displayName: dataProp?.chiefAccountantName,
            personId: dataProp?.chiefAccountantId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: true
      },
      {
        code: headOfDepartment,
        value: dataProp?.headOfDepartmentId
          ? {
            displayName: dataProp?.headOfDepartmentName,
            personId: dataProp?.headOfDepartmentId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: false,
      },
      {
        code: deputyDepartment,
        value: dataProp?.deputyDepartmentId
          ? {
            displayName: dataProp?.deputyDepartmentName,
            personId: dataProp?.deputyDepartmentId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: false,
      },
      {
        code: storekeeper,
        value: dataProp?.storekeeperId
          ? {
            displayName: dataProp?.storekeeperName,
            personId: dataProp?.storekeeperId
          }
          : null,
        type: DATA_TYPE.COMPLEX,
        isGetAllUser: false,
      },
      {
        code: handoverPerson,
        value: dataProp?.handoverPersonId
          ? {
            displayName: dataProp?.handoverPersonName,
            personId: dataProp?.handoverPersonId
          }
          : dataProp?.handoverPersonName || null,
        type: isExport ? DATA_TYPE.COMPLEX : DATA_TYPE.TEXT,
      },
      {
        code: variable.listInputName.createdPerson,
        value: dataProp?.createdPersonId
          ? {
            displayName: dataProp?.createdPersonName,
            personId: dataProp?.createdPersonId
          }
          : dataProp?.createdPersonName || null,
        type: DATA_TYPE.COMPLEX
      },
    ];
    let newView = {
      ...isView,
      [headOfDepartment]: false,
      [deputyDepartment]: false,
      [director]: false,
      [deputyDirector]: false,
      [chiefAccountant]: false,
      [storekeeper]: false,
      [handoverPerson]: true,
      [receiverPerson]: true,
      [createdPerson]: true,
    }
    
    if (isExport) {
      newData.push({
        code: receiverPerson,
        value: dataProp?.receiverPersonId
          ? {
            displayName: dataProp?.receiverPersonName,
            personId: dataProp?.receiverPersonId
          }
          : dataProp?.receiverPersonName || null,
        type: DATA_TYPE.COMPLEX,
      })
    }
    setIsView(newView);
    setItemList([
      ...newData,
    ])
  }, [dataProp]);
  
  useEffect(() => {
    const newData = convertOutputData();
    handleChange(newData);
  }, [itemList]);
  
  const convertOutputData = () => {
    let data = {};
    itemList?.forEach(item => {
      if (item.type === DATA_TYPE.TEXT) {
        data[item.code + "Name"] = item.value || null;
      } else if (item.type === DATA_TYPE.COMPLEX) {
        data[item.code + "Id"] = item.value?.personId || null;
        data[item.code + "Name"] = item.value?.displayName || null;
      }
    })
    return data;
  }
  
  const handleChangeRowData = (value, row) => {
    let newArray = itemList.map(x => {
      if (x.code === row?.code) {
        x.value = value;
      }
      return x;
    })
    
    setItemList([...newArray]);
  }
  
  return (
    <Grid container>
      <Grid item xs={12} className="mt-16">
        <CustomMaterialTable
          data={itemList || []}
          columns={columns}
          options={{
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            minBodyHeight: 300,
          }}
          components={{
            Toolbar: (props) => (
              <div>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        />
      </Grid>
    </Grid>
  )
}

SignatureTabComponent.propTypes = {
  data: PropTypes.object,
  isExport: PropTypes.bool,
  handleChange: PropTypes.func,
  managementDepartmentId: PropTypes.string,
}
