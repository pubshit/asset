import {
  Grid,
  Button,
  Radio,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import React from "react";
import { getByPage, saveItem, checkCode } from "../../ProductType/ProductTypeService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import NotificationPopup from "../NotificationPopup/NotificationPopup";
import "../../../../styles/views/_loadding.scss";
import clsx from "clsx";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class AddNewProductType extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    keyword: "",
    Notification: "",
    loading: false,
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
    this.updatePageData();
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };
  updatePageData = () => {
    var searchObject = {};
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  componentDidMount() {
    // this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null });
    }
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };
  componentWillMount() {
    let { item } = this.props;
    this.setState(item);
  }
  search = (keyword) => {
    var searchObject = {};
    searchObject.text = keyword;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  handleChange(event) {
    this.setState({ keyword: event.target.value });
  }
  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };
  handleChangeCode = (event) => {
    this.setState({ code: event.target.value });
  };

  handleFormSubmit = () => {
    this.setState({loading: true});
    let { id, code } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;
    checkCode(id, code).then((result) => {
      if (result.data) {
        toast.warning(t("ProductType.noti.dupli_code"));
        toast.clearWaitingQueue();
        this.setState({loading: false});
      } else {
        if(id) {
          setPageLoading(true);
          saveItem({ ...this.state }).then(() => {
            toast.success(t("general.updateSuccess"));
            this.props.handleClose();
            setPageLoading(false);
          }).catch(err => {
            toast.error(t("general.error"));
            setPageLoading(false);
          });
        } else {
          setPageLoading(true);
          saveItem({ ...this.state }).then(({data}) => {
            toast.success(t("general.addSuccess"));
            this.props.handleClose();
            setPageLoading(false);
          }).catch(err => {
            toast.error(t("general.error"));
            setPageLoading(false);
          });
        }
      }
    });
  };
  render() {
    const {
      t,
      open,
    } = this.props;
    let {
      name,
      code,
      shouldOpenNotificationPopup,
      loading
    } = this.state;
    return (
      <Dialog open={open} maxWidth="md" PaperComponent={PaperComponent}>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <DialogTitle className="cursor-move" id="draggable-dialog-title">
          {t("ProductType.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent className="pt-0">
            <Grid className="mb-16" container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">* </span>{" "}
                      <span> {t("ProductType.code")}</span>
                    </span>
                  }
                  disabled={this.props?.item?.id}
                  onChange={this.handleChangeCode}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>

              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>{" "}
                      <span> {t("ProductType.name")}</span>
                    </span>
                  }
                  onChange={this.handleChangeName}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                className="mr-13"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
AddNewProductType.contextType = AppContext;
export default AddNewProductType;