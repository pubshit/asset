import { Icon, Card, Grid, Divider, Button, DialogActions, Dialog } from "@material-ui/core";
import React, { useContext } from "react";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import AppContext from "app/appContext";
import {PaperComponent} from "../Utilities";
import {functionExportToExcel} from "../../../appFunction";
import PropTypes from "prop-types";
import axios from "axios";
import ConstantList from "../../../appConfig";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function ImportExcelDialogError(props) {
  const { t, open, handleClose } = props;
  const { setPageLoading } = useContext(AppContext);

  const handleDownloadFileError = async () => {
    const {exportFileError} = props;
    try {
      console.log(props?.file?.urlFileError)
      console.log(exportFileError)
      setPageLoading(true);
      let exportFunction = exportFileError ? exportFileError : exportExcelFileError;
      await functionExportToExcel(
        exportFunction,
        props?.file?.urlFileError,
        props.fileName,
      )
    } catch (e) {
      toast.error(t("general.failImport"));
    } finally {
      setPageLoading(false);
      handleClose();
    }
  }

  const exportExcelFileError = (linkFile) => {
    return axios({
      method: "get",
      url: ConstantList.API_ENPOINT + linkFile,
      responseType: "blob",
    });
  };

  return (
    <Dialog onClose={handleClose} open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">{t('general.failImport')}</span>
      </DialogTitle>
      <DialogContent>
        <Card className="mb-24" elevation={2}>
          <div className="p-16">
            <Grid
              container
              spacing={2}
              justifyContent="center"
              alignItems="center"
              direction="row"
            >
              <Grid item lg={3} md={3}>
                {t('general.file_name')}
              </Grid>
              <Grid item lg={6} md={6}>
                {t('general.messageError')}
              </Grid>
              <Grid item lg={3} md={3}>
                {t('general.showError')}
              </Grid>
            </Grid>
          </div>
          <Divider></Divider>
          <div className="px-16 py-16">
            <Grid
              container
              spacing={2}
              justifyContent="center"
              alignItems="center"
              direction="row"
            >
              <Grid item lg={3} md={3} sm={12} xs={12}>
                {props?.fileName}
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                {props?.file?.messageError}
              </Grid>
              <Grid item lg={3} md={3} sm={12} xs={12}>
                <div className="flex">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleDownloadFileError}
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">download</Icon>
                      <span>File lỗi</span>
                    </div>
                  </Button>
                </div>
              </Grid>
            </Grid>
          </div>
        </Card>
      </DialogContent>
      <DialogActions>
        <Button
          className="mr-16 mb-16"
          variant="contained"
          color="secondary"
          onClick={handleClose}
        >
          {t('general.close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ImportExcelDialogError;
ImportExcelDialogError.propTypes = {
  t: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  file: PropTypes.object,
  exportFileError: PropTypes.func,
  fileName: PropTypes.string,
}
