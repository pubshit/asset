import { Icon, Card, Grid, Divider, Button, DialogActions, Dialog } from "@material-ui/core";
import React from "react";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { EgretProgressBar } from "egret";
import axios from "axios";
import ConstantList from "../../../appConfig";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst } from "../../../appConst";
import ImportExcelDialogError from "./ImportExcelDialogError";
import AppContext from "../../../appContext";
import { PaperComponent } from "../Utilities";
import PropTypes from "prop-types";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class ImportExcelDialog extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    dragClass: "",
    files: [],
    statusList: [],
    queProgress: 0,
    error: false,
    urlFileError: ""
  };

  handleFileUploadOnSelect = event => {
    let files = event.target.files;
    this.fileUpload(files[0]).then(res => {
      alert("File uploaded successfully.")
    });
  }

  handleFileSelect = event => {
    let files = event.target.files;
    let list = [];

    for (const iterator of files) {
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      files: [...list]
    }, () => {
      this.uploadSingleFile(this.state.files.length - 1);
      if (this.inputRef.current) {
        this.inputRef.current.value = null;
      }
    });
  };

  handleDragOver = event => {
    event.preventDefault();
    this.setState({ dragClass: "drag-shadow" });
  };

  handleDrop = event => {
    event.preventDefault();
    event.persist();

    let files = event.dataTransfer.files;
    let list = [];

    for (const iterator of files) {
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      dragClass: "",
      files: [...list]
    });

    return false;
  };

  handleDragStart = event => {
    this.setState({ dragClass: "drag-shadow" });
  };

  handleSingleRemove = index => {
    let files = [...this.state.files];
    files.splice(index, 1);
    this.setState({
      files: [...files]
    });
  };

  handleAllRemove = () => {
    this.setState({ files: [] });
  };

  fileUpload(file) {
    const { url } = this.props;
    const defaultUrl = ConstantList.API_ENPOINT + "/api/excel/import";
    const urlPath = url || defaultUrl;
    let formData = new FormData();
    formData.append('file', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    return axios.post(urlPath, formData, config)
  }

  uploadSingleFile = index => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    let allFiles = [...this.state.files];
    let file = this.state.files[index];
    this.fileUpload(file.file).then(res => {
      setPageLoading(false);
      if (appConst.CODE.SUCCESS === res?.data?.code) {
        toast.success(t("general.importSuccess"));
        this.props.handleOKEditClose();
      }
      else {
        this.setState({
          urlFileError: res?.data?.data?.[0].urlGetFile
            || res?.data?.apiSubErrors?.[0].urlGetFile,
          messageError: res?.data?.message,
          error: true
        })
      }
    })
      .catch(res => {
        toast.error(this.props.t("general.failImport"));
        setPageLoading(false);
      })

    allFiles[index] = { ...file, uploading: true, error: false };

    this.setState({
      files: [...allFiles]
    });
  };

  uploadAllFile = () => {
    let allFiles = [];

    this.state.files.map(item => {
      allFiles.push({
        ...item,
        uploading: true,
        error: false
      });

      return item;
    });

    this.setState({
      files: [...allFiles],
      queProgress: 35
    });
  };

  handleSingleCancel = index => {
    let allFiles = [...this.state.files];
    let file = this.state.files[index];

    allFiles[index] = { ...file, uploading: false, error: true };

    this.setState({
      files: [...allFiles]
    });
  };

  handleCancelAll = () => {
    let allFiles = [];

    this.state.files.map(item => {
      allFiles.push({
        ...item,
        uploading: false,
        error: true
      });

      return item;
    });

    this.setState({
      files: [...allFiles],
      queProgress: 0
    });
  };

  handleCloseDialogError = () => {
    this.setState({
      error: false
    })
  }

  render() {
    const { t, handleClose, open } = this.props;
    let { files } = this.state;
    let isEmpty = files.length === 0;

    return (
      <Dialog onClose={handleClose} open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t("general.importExcel")}</span>
        </DialogTitle>
        <DialogContent>
          <div className="upload-form m-sm-30">
            <div className="flex flex-wrap mb-24">
              <div>
                <label htmlFor="upload-single-file">
                  <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">cloud_upload</Icon>
                      <span>{t('general.select_file')}</span>
                    </div>
                  </Button>
                </label>
                <input
                  ref={this.inputRef}
                  className="display-none"
                  onChange={this.handleFileSelect}
                  id="upload-single-file"
                  type="file"
                  accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                /> </div>
              <br></br>
              <div style={{ marginLeft: '10px' }} >
                <label>
                  <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                    onClick={this.props.exportExampleImportExcel}
                  >
                    <div className="flex flex-middle">
                      <Icon className="pr-8">download</Icon>
                      <span>Mẫu nhập Excel</span>
                    </div>
                  </Button>
                </label></div>
              <div className="px-16"></div>
            </div>
            <Card className="mb-24" elevation={2}>
              <div className="p-16">
                <Grid
                  container
                  spacing={2}
                  justifyContent="center"
                  alignItems="center"
                  direction="row"
                >
                  <Grid item lg={4} md={4}>
                    {t('general.file_name')}
                  </Grid>
                  <Grid item lg={4} md={4}>
                    {t('general.size')}
                  </Grid>
                  <Grid item lg={4} md={4}>
                    {t('general.action')}
                  </Grid>
                </Grid>
              </div>
              <Divider></Divider>

              {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

              {files.map((item, index) => {
                let { file, uploading, error, progress } = item;
                return (
                  <div className="px-16 py-16" key={file.name}>
                    <Grid
                      container
                      spacing={2}
                      justifyContent="center"
                      alignItems="center"
                      direction="row"
                    >
                      <Grid item lg={4} md={4} sm={12} xs={12}>
                        {file.name}
                      </Grid>
                      <Grid item lg={1} md={1} sm={12} xs={12}>
                        {(file.size / 1024 / 1024).toFixed(1)} MB
                      </Grid>
                      <Grid item lg={2} md={2} sm={12} xs={12}>
                        <EgretProgressBar value={progress}></EgretProgressBar>
                      </Grid>
                      <Grid item lg={1} md={1} sm={12} xs={12}>
                        {error && <Icon color="error">error</Icon>}
                        {/* {uploading && <Icon className="text-green">done</Icon>} */}
                      </Grid>
                      <Grid item lg={4} md={4} sm={12} xs={12}>
                        <div className="flex">
                          <Button
                            variant="contained"
                            color="primary"
                            disabled={uploading}
                            onClick={() => this.uploadSingleFile(index)}
                          >
                            {t('general.upload')}
                          </Button>
                          <Button
                            className="mx-8"
                            variant="contained"
                            disabled={!uploading}
                            color="secondary"
                            onClick={() => this.handleSingleCancel(index)}
                          >
                            {t('general.cancel')}
                          </Button>
                          <Button
                            variant="contained"
                            className="bg-error"
                            onClick={() => this.handleSingleRemove(index)}
                          >
                            {t('general.remove')}
                          </Button>
                        </div>
                      </Grid>
                    </Grid>
                    {this.state.error && (
                      <ImportExcelDialogError
                        t={t}
                        open={this.state.error}
                        file={this.state}
                        fileName={file.name}
                        handleClose={this.handleCloseDialogError}
                        exportFileError={this.props.exportFileError}
                      />
                    )}
                  </div>
                );
              })}
            </Card>
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-36 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}>{t('general.close')}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

ImportExcelDialog.contextType = AppContext;
export default ImportExcelDialog;
ImportExcelDialog.propTypes = {
  t: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  i18n: PropTypes.any,
  handleOKEditClose: PropTypes.func,
  exportExampleImportExcel: PropTypes.func,
  exportFileError: PropTypes.func,
  url: PropTypes.string,
}
