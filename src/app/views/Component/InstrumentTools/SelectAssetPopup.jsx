import {
  Grid,
  InputAdornment,
  Input,
  Button,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from "material-table";
import { searchByPage } from "../../Asset/AssetService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { appConst } from "app/appConst";
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectAssetPopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    departmentId: "",
    userDepartmentId: "",
    managementDepartmentId: "",
    voucherType: null,
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.voucherType = this.props.voucherType;
    searchObject.userDepartmentId = this.props.userDepartmentId;
    searchObject.managementDepartmentId = this.props.managementDepartmentId;
    searchObject.departmentId = this.props.departmentId;
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  componentDidMount() {
    this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    this.setState({
      selectedValue: item?.id ? item?.id : null,
      selectedItem: item ? item : null
    });
  };

  componentWillMount() {
    let {
      selectedItem,
      voucherType,
      userDepartmentId,
      managementDepartmentId,
      departmentId,
    } = this.props;
    this.setState({
      selectedValue: selectedItem?.id,
      voucherType: voucherType,
      userDepartmentId: userDepartmentId,
      managementDepartmentId: managementDepartmentId,
      departmentId: departmentId,
    });
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  search() {
    this.setPage(0, function () {
      var searchObject = {};
      searchObject.voucherType = this.props.voucherType;
      searchObject.userDepartmentId = this.props.userDepartmentId;
      searchObject.managementDepartmentId = this.props.managementDepartmentId;
      searchObject.departmentId = this.props.departmentId;
      searchObject.keyword = this.state.keyword;
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      });
    });
  }

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  render() {
    const {
      t,
      handleClose,
      handleSelect,
      open,
    } = this.props;
    let { keyword } = this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "100px",
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Radio
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={this.state.selectedValue === rowData.id}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "left",
        width: "150px",
      },
      {
        title: t("component.asset.code"),
        field: "code",
        align: "left",
        width: "150px",
      },
      {
        title: t("component.asset.name"),
        field: "name",
        align: "left",
        width: "150",
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "left",
        width: "150",
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "left",
        width: "150",
        render: (rowData) => {
          let number = new Number(rowData.originalCost);
          if (number != null) {
            let plainNumber = number.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move", paddingBottom: '0px' }} id="draggable-dialog-title">
          <span className="mb-20">{t("component.asset.title")}</span>
        </DialogTitle>
        <DialogContent style={{ minHeight: "370px" }}>
          <Grid item md={6} sm={12} xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onKeyUp={this.handleKeyUp}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDownEnterSearch}
              className="w-100 mb-16"
              id="search_box"
              placeholder={t("general.enterSearch")}
              startAdornment={
                <InputAdornment>
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>

            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(this.state.selectedItem)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectAssetPopup;
