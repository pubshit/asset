import {
  Grid,
  InputAdornment,
  Input,
  Button,
  Checkbox,
  TablePagination,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from "material-table";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { appConst } from "../../../appConst";
import { searchByPageCCDCV2 } from "app/views/InstrumentToolsList/InstrumentToolsListService";
import { convertNumberPriceRoundUp, formatDateDto, formatDateDtoMore, formatTimestampToDate, getTheHighestRole, getUserInformation } from "app/appFunction";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import AsynchronousAutocompleteTransfer from "../../utilities/AsynchronousAutocompleteTransfer";

import { searchByPage as SearchByPageDepartment, searchByPageDepartmentNew } from "../../Department/DepartmentService";
import { TextValidator } from "react-material-ui-form-validator";
import { Autocomplete } from "@material-ui/lab";
import { createFilterOptions } from "@material-ui/lab";
import { searchByPage } from "../../Store/StoreService";
import { filterOptions } from "../../../appFunction";
import { getMedicalEquipment } from "../../Asset/AssetService";
import { getListInstrumentsVouchers } from "app/views/InstrumentsToolsInventory/InstrumentsToolsInventoryService";
import PropTypes from "prop-types";
import { PaperComponent } from "../Utilities";
import AppContext from "../../../appContext";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class SelectAssetAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    total: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    assetList: [],
    assetVouchers: [],
    listDepartment: [],
    department: null,
    medicalEquipment: null,
    store: null,
    useDepartment: null,
    listUseDepartment: [],
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  handleSetListDepartment = (listDepartment) => {
    this.setState({ listDepartment: listDepartment || [] });
  };
  handleSetListUseDepartment = (listUseDepartment) => {
    this.setState({ listUseDepartment: listUseDepartment || [] });
  };
  handleGetListManagementDepartment = async () => {
    let { t } = this.props
    try {
      const { organization } = getUserInformation();
      let departmentSearchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        isAssetManagement: true,
        orgId: organization?.org?.id,
      };
      const result = await searchByPageDepartmentNew(departmentSearchObject)
      if (result?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listDepartment: result?.data?.data?.content || []
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
    }
  }

  updatePageData = async () => {
    let { t, voucherType, isNotUseDepartment, isFilterDepartment, filteredSelected } = this.props;
    let { store, medicalEquipment, useDepartment } = this.state;
    let { setPageLoading } = this.context;
    let status = 0;
    let searchObject = {};
    setPageLoading(true);
    searchObject.voucherType = voucherType ? appConst.REQUIREMENTS_CATEGORY.REPOSSESSION.code : null;
    searchObject.managementDepartmentId = this.props?.handoverDepartment
      ? this.props?.handoverDepartment.id
      : "";
    if (!isFilterDepartment) {
      searchObject.useDepartmentId = this.state?.departmentId || this.state?.department?.id;
    }
    if (this.props?.isAssetTransfer || this.props?.isAssetAllocation || this.props?.isAssetInventory) {
      searchObject.statusIndexOrders = this.props?.statusIndexOrders;
    } else {
      searchObject.indexOrder = status;
    }
    if (
      this.props.isFilterManagementDepartment
    ) {
      searchObject.managementDepartmentId = this.state.department?.id
    }
    searchObject.issueDateTop = formatDateDto(this.props?.issueDateTop);
    searchObject.dateOfReceptionTop = formatDateDtoMore(this.props?.dateOfReceptionTop, "start");
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    if (this.props?.isSearchForTransfer) {
      searchObject.isSearchForTransfer = this.props?.isSearchForTransfer;
    }
    if (store) {
      searchObject.storeId = store ? store?.id : null;
    }
    if (medicalEquipment) {
      searchObject.medicalEquipmentId = medicalEquipment ? medicalEquipment?.id : null;
    }
    if (isNotUseDepartment) {
      searchObject.notUseDepartmentId = isNotUseDepartment;
      searchObject.isManagedByDepartment = true;
    }
    if (useDepartment?.id) {
      searchObject.useDepartmentId = useDepartment?.id;
    }
    let res, total;
    let itemListClone = []
    try {
      if (isNotUseDepartment) {
        res = await getListInstrumentsVouchers(searchObject);
        itemListClone = res?.data?.data ? [...res?.data?.data] : [];
        total = res?.data?.total;
      } else {
        res = await searchByPageCCDCV2(searchObject);
        itemListClone = res?.data?.data?.content ? [...res?.data?.data?.content] : [];
        total = res?.data?.data?.totalElements;
      }

      itemListClone?.map((item) => {
        let isExist = this.state.assetVouchers?.some(assetVoucher => isNotUseDepartment || filteredSelected
          ? filteredSelected(item, assetVoucher)
          : assetVoucher.asset?.assetId === item.assetId
        )

        item.carryingAmount = Math.ceil(item.carryingAmount)
        if (!this.props.isLiquidate && this.props.convertAssetIdToId) {
          item.id = item.assetId
        }

        item.isCheck = isExist;
        return item;
      });
      this.setState(
        { itemList: itemListClone, total }
      );
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }

  };

  componentDidMount() {
    let { departmentId } = this.props;
    this.setState({
      assetVouchers: this.props.assetVouchers,
      departmentId: departmentId ? departmentId : null,
    },
      () => this.updatePageData()
    )
  }

  handleClick = (event, item) => {
    let { isNotUseDepartment, filteredSelected = () => false } = this.props;
    let { assetVouchers, itemList } = this.state;

    let existItem = assetVouchers?.find(x => {x
      if (isNotUseDepartment || filteredSelected) {
        return filteredSelected
        ? filteredSelected(item, x)
        : (x.asset.id === item.id)
          && (x.asset.dateOfReception === item?.dateOfReception)
          && (x?.departmentId === item?.useDepartmentId)
      } else {
        return (x.asset.id === item.id) || (x.asset?.assetId  === item?.assetId)
      }
    });

    if (!existItem) {
      let p = {};
      p.asset = item;
      assetVouchers = [...assetVouchers, p];
    } else {
      assetVouchers.splice(assetVouchers.indexOf(existItem), 1);
    }

    itemList?.map(x => {
      if (x.id === item.id) {
        item.isCheck = event.target.checked;
      }
    })
    this.setState({ assetVouchers: assetVouchers, itemList });
  };

  componentWillMount() {
    let {
      handoverDepartment,
      managementDepartmentId,
    } = this.props;
    let roles = getTheHighestRole();
    if (this.props?.isFilterManagementDepartment) {
      this.handleGetListManagementDepartment()
    }
    this.setState({
      handoverDepartment,
      managementDepartmentId,
      ...roles,
    });
  }

  search() {
    this.setState({ page: 0 }, () => {
      this.updatePageData();
    })
  }

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
    this.updatePageData();
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleSelectDepartment = (department, source) => {
    this.setState({ [source]: department }, () => {
      this.setPage(0);
    });
  };

  handleSelectData = (value, source) => {
    this.setState({
      [source]: value
    }, () => this.setPage(0))
  }

  handleFilterSelectedItem = (item, assetVoucher) => {
    let { filteredSelected } = this.props;
  }
  render() {
    const filterAutocomplete = createFilterOptions();
    const {
      t,
      handleClose,
      handleSelect,
      open,
      isLiquidate,
      handoverDepartment,
      isNotUseDepartment,
      isRecallSlip,
    } = this.props;

    let {
      keyword,
      assetVouchers,
      isRoleAssetUser,
      isRoleUser,
      department,
      listDepartment,
      store,
      medicalEquipment,
      useDepartment,
      listUseDepartment
    } = this.state;

    let searchObjectDepartment = { pageIndex: 1, pageSize: 9999 };
    let searchObjectStore = {
      pageSize: 100000,
      pageIndex: 1,
      managementDepartmentId: handoverDepartment?.id,
    };
    let searchObjectUnit = { pageSize: 100000, pageIndex: 1 }
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        minWidth: "50px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => (
          <Checkbox
            id={`radio${rowData.id}`}
            style={{ padding: "0px" }}
            name="radSelected"
            value={rowData.id}
            checked={rowData.isCheck}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("InstrumentToolsList.code"),
        field: "code",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.name"),
        field: "name",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("InstrumentToolsList.model"),
        field: "model",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.receive_date"),
        field: "dateOfReception",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "center"
        },
        render: rowData => formatTimestampToDate(rowData?.dateOfReception)
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "left",
        hidden: this.props?.voucherType,
        minWidth: 100,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturerTable"),
        field: "manufacturerName",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InventoryCountVoucher.store"),
        field: "storeName",
        align: "left",
        hidden: this.props?.voucherType,
        minWidth: 160,
      },
      {
        title: t("InstrumentToolsList.availableQuantity"),
        field: "quantity",
        align: "left",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InstrumentToolsList.originalPrice"),
        field: "originalCost",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => rowData.originalCost ? convertNumberPriceRoundUp(rowData.originalCost) : '',
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("Asset.managementDepartment"),
        field: "managementDepartmentName",
        hidden: this.props?.isNotUseDepartment,
        align: "left",
        minWidth: 200,
      },
      {
        title: t("Asset.usingStatus"),
        field: "statusName",
        align: "left",
        minWidth: 200,
      },

    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("InstrumentToolsAllocation.choose")}</span>
        </DialogTitle>
        <DialogContent>
          <Grid container xs={12}>
            <Grid item container spacing={2} xs={12} className="mb-16">
              <Grid item md={8} sm={6} xs={6} >
                <Input
                  label={t("general.enterSearch")}
                  type="text"
                  name="keyword"
                  value={keyword}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  className="w-100 mt-16"
                  id="search_box"
                  placeholder={t("general.enterSearch")}
                  startAdornment={
                    <InputAdornment>
                      <Link to="#">
                        {" "}
                        <SearchIcon
                          onClick={() => this.search(keyword)}
                          style={{
                            position: "absolute",
                            top: "0",
                            right: "0",
                          }}
                        />
                      </Link>
                    </InputAdornment>
                  }
                />
              </Grid>
              {((isLiquidate || isNotUseDepartment) && (!isRoleUser || !isRoleAssetUser)) && (
                <Grid item md={4} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span> {t("receipt.store")}</span>
                      </span>
                    }
                    searchFunction={searchByPage}
                    searchObject={searchObjectStore}
                    displayLable={'name'}
                    value={store ? store : null}
                    onSelect={(value) => this.handleSelectData(value, "store")}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              )}
              {(isLiquidate && (!isRoleUser || !isRoleAssetUser)) && (
                <Grid item xs={12} sm={12} md={4}>
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={t("MedicalEquipmentType.title")}
                    searchFunction={getMedicalEquipment}
                    searchObject={searchObjectUnit}
                    typeReturnFunction="category"
                    displayLable={"name"}
                    value={medicalEquipment ? medicalEquipment : null}
                    onSelect={(value) => this.handleSelectData(value, "medicalEquipment")}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              )}
              {/* dialog chon ccdc thu hồi */}
              {(isRecallSlip && (!isRoleUser || !isRoleAssetUser)) && (
                <Grid item md={4} sm={12} xs={12}>
                  <AsynchronousAutocompleteTransfer
                    label={t("Asset.useDepartment")}
                    searchFunction={SearchByPageDepartment}
                    searchObject={searchObjectDepartment}
                    listData={listUseDepartment}
                    setListData={this.handleSetListUseDepartment}
                    displayLable={"text"}
                    value={useDepartment?.id ? useDepartment : null}
                    onSelect={(value) => this.handleSelectDepartment(value, "useDepartment")}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              )}
              {/* dialog chọn ccdc thanh lý */}
              <Grid item md={4} sm={12} xs={12}>
                {(isLiquidate && (!isRoleUser || !isRoleAssetUser)) && (
                  <AsynchronousAutocompleteTransfer
                    label={t("Asset.useDepartment")}
                    searchFunction={SearchByPageDepartment}
                    searchObject={searchObjectDepartment}
                    listData={listDepartment}
                    setListData={this.handleSetListDepartment}
                    defaultValue={department ? department : null}
                    displayLable={"text"}
                    value={department ? department : null}
                    onSelect={(value) => this.handleSelectDepartment(value, "department")}
                    noOptionsText={t("general.noOption")}
                  />
                )}
                {this.props?.isFilterManagementDepartment &&
                  <Autocomplete
                    id="combo-box-reception-department"
                    fullWidth
                    size="small"
                    name='ManagementDepartment'
                    options={this.state?.listDepartment}
                    onChange={(event, value) => this.handleSelectDepartment(value, "department")}
                    getOptionLabel={(option) => option.name}
                    value={department ? department : null}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      let filtered = filterAutocomplete(options, params);
                      return filtered;
                    }}
                    renderInput={(params) => (
                      <TextValidator
                        {...params}
                        label={<>
                          <span>{t("Asset.managementDepartment")}</span>
                        </>}
                        variant="standard"
                        value={department?.name ? department?.name : null}
                        inputProps={{
                          ...params.inputProps,
                        }}
                        placeholder="Chọn phòng ban"
                        onChange={this.handleSearchDepartment}
                      />
                    )}
                    noOptionsText={t("general.noOption")}
                  />
                }
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <MaterialTable
                data={this.state.itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                options={{
                  sorting: false,
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                  maxBodyHeight: "300px",
                  minBodyHeight: "300px",
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                component="div"
                count={this.state.total}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(assetVouchers)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}

export default SelectAssetAllPopop;
SelectAssetAllPopop.contextType = AppContext;
SelectAssetAllPopop.propTypes = {
  open: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  filteredSelected: PropTypes.func,
  convertAssetIdToId: PropTypes.bool,
}
