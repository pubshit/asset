/* eslint-disable no-unused-expressions */
import {
    Grid,
    InputAdornment,
    Input,
    Button,
    TablePagination,
    Dialog,
    DialogActions,
    Checkbox,
  } from "@material-ui/core";
  import React from "react";
  import SearchIcon from "@material-ui/icons/Search";
  import { Link } from "react-router-dom";
  import MaterialTable, {
    MTableToolbar,
  } from "material-table";
  import { findUserByUserName } from "../../User/UserService";
  import AsynchronousAutocompleteTransfer from "../../utilities/AsynchronousAutocompleteTransfer";
  import DialogContent from "@material-ui/core/DialogContent";
  import DialogTitle from "@material-ui/core/DialogTitle";
  import Draggable from "react-draggable";
  import Paper from "@material-ui/core/Paper";
  import { getUserDepartmentAll } from "../../Department/DepartmentService";
  import { appConst } from "app/appConst";
  import { toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
import { createFilterOptions } from "@material-ui/lab";
  toast.configure({
    autoClose: 2000,
    draggable: false,
    limit:3
    //etc you get the idea
  });
  function PaperComponent(props) {
    return (
      <Draggable
        handle="#draggable-dialog-title"
        cancel={'[class*="MuiDialogContent-root"]'}
      >
        <Paper {...props} />
      </Draggable>
    );
  }
  class SelectUserDepartmentInventoryCountVoucher extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
    }
    state = {
      rowsPerPage: 5,
      page: 0,
      data: [],
      totalElements: 0,
      itemList: [],
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      selectedItem: {},
      keyword: "",
      shouldOpenProductDialog: false,
      receiverDepartmentId: "",
      department: null,
      inventoryCountPersons: [],
    };
  
    setPage = (page) => {
      this.setState({ page : page }, function () {
        this.updatePageData();
      });
    };
  
    setRowsPerPage = (event) => {
      this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
        this.updatePageData();
      });
    };
  
    handleChangePage = (event, newPage) => {
      this.setPage(newPage);
    };
  
    updatePageData = () => {
      let searchObject = {};
      if (this.props.isUserDepartmentManagement) {
        searchObject.isUserDepartmentManagement = this.props.isUserDepartmentManagement;
      }
      searchObject.departmentId = this.state.departmentId;
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      findUserByUserName(searchObject).then(({ data }) => {
        let itemListClone = [...data?.content];
        itemListClone?.map((item) => {
          item.isCheck = false;
          this.state?.inventoryCountPersons?.forEach((inventoryCountPerson) => {
            if (inventoryCountPerson?.person?.id === item?.user?.person?.id) {
              item.isCheck = true;
              return;
            }else if(inventoryCountPerson?.user?.person?.id === item?.user?.person?.id){
              item.isCheck = true;
              return;
            }
          })
        })
        this.setState({
          itemList: [...itemListClone],
          totalElements: data.totalElements,
        });
      });
    };
  
    componentDidMount() {
      this.setState({ inventoryCountPersons: this.props?.inventoryCountPersons },
        () => this.updatePageData());
    }
  
    handleClick = (event, item) => {
      document.querySelector(`#radio${item.id}`).click();
      item.isCheck = event.target.checked;
      let { inventoryCountPersons } = this.state;
      if (item.isCheck) {
        let check = inventoryCountPersons.filter(inventoryCountPerson => inventoryCountPerson?.person?.id === item?.user?.person?.id);
        if (check?.length === 0) {
          inventoryCountPersons = inventoryCountPersons.concat(item);
        }
      } else {
        inventoryCountPersons = inventoryCountPersons.filter(
          (inventoryCountPerson) => {
            if(inventoryCountPerson?.person?.id){
              return (inventoryCountPerson?.person?.id !== item?.user?.person?.id)
            }else{
              return (inventoryCountPerson?.id !== item?.id)
            }
          }
        );
      }
      this.setState({ inventoryCountPersons: inventoryCountPersons });
    };
  
    componentWillMount() {
      let { selectedItem } = this.props;
      //this.setState(item);
      this.setState({ selectedValue: selectedItem.id });
    }
  
    handleKeyDownEnterSearch = (e) => {
      if (appConst.KEY.ENTER === e.key) {
        this.search();
      }
    };
  
    search() {
      this.setPage(0)
    }
    handleKeyUp = (e)=>{
      !e.target.value && this.search()
    }
  
    handleChange = (event, source) => {
      this.setState({
          [event.target.name]: event.target.value,
        });
    };
  
    onClickRow = (selectedRow) => {
      document.querySelector(`#radio${selectedRow.id}`).click();
    };
  
    selectDepartment = (item) => {
      this.setState(
        {departmentId : item ? item?.id : null},
        ()=> this.updatePageData()
      )
    };

    handleSubmit = (inventoryCountPersons) => {
      if(inventoryCountPersons.length > 0){
        this.props.handleSelect(inventoryCountPersons)
      }else{
        toast.warning("Chưa chọn người kiểm kê")
        toast.clearWaitingQueue();
      }
    }

    handleSetDataSelect = (data) => {
        this.setState({
            listDepartment: data
        })
    }
  
    render() {
      const {
        t,
        handleClose,
        open,
      } = this.props;
      let { keyword, department } = this.state;
      let SearchDepartment = { pageIndex: 0, pageSize: 1000000 };
      const filterAutocomplete = createFilterOptions();

      let columns = [
        {
          title: t("general.select"),
          field: "custom",
          align: "left",
          width: "90px",
          cellStyle: {
            padding: "0px",
          },
          render: (rowData) => (
            <Checkbox
              id={`radio${rowData.id}`}
              name="radSelected"
              value={rowData.id}
              checked={rowData?.isCheck}
              onClick={(event) => this.handleClick(event, rowData)}
            />),
        },
        { title: t("InventoryCountVoucher.name"), field: "user.displayName", width: "270px" },
        {
          title: t("component.department.text"),
          field: "department.name",
          width: "150",
        },
      ];
      return (
        <Dialog
          onClose={handleClose}
          open={open}
          PaperComponent={PaperComponent}
          maxWidth={"md"}
          fullWidth
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">
              {this.props.inventoryCount
                ? t("Người kiểm kê")
                : t("Asset.receiverPerson")}
            </span>
          </DialogTitle>
          <DialogContent>
            <Grid item container spacing={2} xs={12}>
              <Grid item md={6} sm={12} xs={12}>
                <Input
                  label={t("general.enterSearch")}
                  style={{ marginTop: "16px" }}
                  type="text"
                  name="keyword"
                  value={keyword}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  className="w-100 mb-16"
                  id="search_box"
                  placeholder={t("general.enterSearch")}
                  startAdornment={
                    <InputAdornment>
                      <Link to="#">
                        {" "}
                        <SearchIcon
                          onClick={() => this.search(keyword)}
                          style={{
                            position: "absolute",
                            top: "0",
                            right: "0",
                          }}
                        />
                      </Link>
                    </InputAdornment>
                  }
                />
              </Grid>
              <Grid item md={3} xs={12}></Grid>
              <Grid item md={3} xs={12}>
                <AsynchronousAutocompleteTransfer
                  label={t("general.filterDerpartment")}
                  searchFunction={getUserDepartmentAll}
                  searchObject={SearchDepartment}
                  listData={this.state.listDepartment}
                  setListData={this.handleSetDataSelect}
                  defaultValue={department}
                  displayLable={"text"}
                  value={department}
                  onSelect={this.selectDepartment}
                  filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <MaterialTable
                data={this.state.itemList}
                columns={columns}
                onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
                parentChildData={(row, rows) => {
                  var list = rows.find((a) => a.id === row.parentId);
                  return list;
                }}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  maxBodyHeight: "253px",
                  minBodyHeight: "253px",
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                component="div"
                count={this.state.totalElements}
                rowsPerPage={this.state.rowsPerPage}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${
                    count !== -1 ? count : `more than ${to}`
                  }`
                }
                page={this.state.page}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              className="mr-12"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16"
              variant="contained"
              style={{ marginLeft: "0px" }}
              color="primary"
              onClick={() => this.handleSubmit(this.state.inventoryCountPersons)}
            >
              {t("general.select")}
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
  }
  export default SelectUserDepartmentInventoryCountVoucher;
  