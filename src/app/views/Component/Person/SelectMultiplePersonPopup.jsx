import {
  Grid,
  Button,
  InputAdornment,
  Input,
  TablePagination,
  Dialog,
  DialogActions, Checkbox,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";

import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { searchByPage } from "../../Staff/StaffService";
import { appConst } from "../../../appConst";
import { toast } from "react-toastify";
import { findUserByUserName } from "app/views/User/UserService";
import { handleKeyDown, handleKeyUp } from "app/appFunction";
import AppContext from "app/appContext";
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectMultiplePersonPopup extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedList: [],
    keyword: "",
    shouldOpenProductDialog: false,
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let { selectedList } = this.state;
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.isActive = this.state.isActive;

    setPageLoading(true);
    findUserByUserName(searchObject).then((data) => {
      if (appConst.CODE.SUCCESS === data.status) {
        data.data.content.map(item => {
          let existItem = selectedList?.find(
            checkedItem => item?.id === checkedItem?.id 
              && item?.department?.id === checkedItem.department?.id
          );

          item.isCheck = Boolean(existItem);
          return item
        })
        this.setState({
          itemList: [...data?.data?.content],
          totalElements: data?.data?.totalElements,
        });
      } else {
        toast.error(t("general.error"));
      }
      setPageLoading(false);
    }).catch(err => {
      toast.error(t("general.error"));
      setPageLoading(false);
    })
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleClick = (event, item) => {
    let { selectedList = [], itemList = [] } = this.state;
    let existItem = selectedList?.find(item => item.id === event?.target?.name)

    if (existItem) {
      selectedList.splice(selectedList.indexOf(existItem), 1);
    }
    else {
      selectedList.push(item);
    }

    itemList.map(user => {
      if (user.id === item.id) {
        user.isCheck = !Boolean(existItem);
      }
      return item;
    })

    this.setState({ selectedList, itemList });
  };

  componentWillMount() {
    let { selectedList } = this.props;
    this.setState({ selectedList });
  }

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search)

  handleKeyUp = (e) => handleKeyUp(e, this.updatePageData);

  handleChange = (event) => {
    this.setState({
      [event?.target?.name]: event?.target?.value,
    });
  };

  render() {
    const {
      t,
      handleClose,
      handleSelect,
      open,
    } = this.props;
    let {
      keyword,
      itemList,
      selectedList
    } = this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        maxWidth: 100,
        cellStyle: {
          padding: "0px",
          paddingLeft: "10px",
        },
        render: (rowData) => (
          <>
            <Checkbox
              id={`checkBox${rowData.id}`}
              name={rowData.id}
              value={rowData.id}
              checked={rowData.isCheck}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          </>
        ),
      },
      {
        title: t("Council.userName"),
        field: "user.displayName",
        align: "left",
        minWidth: 350,
      },
      {
        title: t("staff.department"),
        field: "department.name",
        align: "left",
        minWidth: 450,
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Council.select")}</span>
        </DialogTitle>
        <DialogContent style={{ height: "400px" }}>
          <Grid item md={6} sm={12} xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyUp={this.handleKeyUp}
              onKeyDown={this.handleKeyDownEnterSearch}
              className="w-100 mb-16"
              id="search_box"
              placeholder={t("general.enterSearch")}
              startAdornment={
                <InputAdornment>
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "273px",
                minBodyHeight: "273px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions className="pt-0">
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mb-16 mr-12"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mb-16 mr-16"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(selectedList)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}

SelectMultiplePersonPopup.contextType = AppContext;
export default SelectMultiplePersonPopup;
