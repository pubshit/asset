import React from 'react'
import { Popover, Typography } from '@material-ui/core'
import { Help, } from '@material-ui/icons'

function DialogNote(props) {
    const { content, marginTop = 0, className } = props
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>
            <Help className={`${className} cursor-pointer`} fontSize='small' color="disabled" onClick={handleClick} />
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                className='w-80 p-12 '
            >
                <Typography style={{
                    padding: '12px'
                }} >{content}</Typography>
            </Popover>
        </div>
    )
}

DialogNote.propTypes = {}

export default DialogNote
