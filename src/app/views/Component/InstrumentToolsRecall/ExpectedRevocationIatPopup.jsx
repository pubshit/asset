import React, { Component } from 'react';
import { 
    Dialog, 
    DialogContent,
    Grid,
    DialogActions,
    Checkbox,
    Button,
    TablePagination   
} from '@material-ui/core';
import MaterialTable, { MTableToolbar } from 'material-table';
import moment from 'moment';
import { appConst, variable } from 'app/appConst';
import { formatDateTypeArray, getTheHighestRole } from 'app/appFunction';
import { toast } from "react-toastify";
import AppContext from 'app/appContext';
import { searchByPageRecallSlipIat, getAssetVorcher } from 'app/views/RecallSlipIat/RecallSlipIatService';
import PropTypes from "prop-types";


export class ExpectedRevocationIatPopup extends Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenAddDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    totalPages: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    statusIndex: null,
    tabValue: 0,
    isRoleAdmin: false,
    isRoleAssetManager: false,
    isCheckReceiverDP: true,
    isChecked: [],
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataAssetVouchersTable: [],
    openAdvanceSearch: false,
    listStatus: [],
    listHandoverDepartment: [],
    listReceiverDepartment: [],
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    fromToData: null,
    openPrintQR: false,
    products: [],
    checkAll: false,
    isCheckValueTab: true,
    itemData: {},
    dxIds: [],
    assetVouchers: [],
    listData: []
  };

  checkStatus = (status) => {
    let {isCheckValueTab} = this.state
    const functionStatus = isCheckValueTab 
      ? appConst.STATUS_EXPECTED_RECOVERY 
      : appConst.STATUS_REVOKED

      const itemStatus = Object.values(functionStatus).find((item) => item?.id === status);
      return itemStatus?.name || '';
  }; 

  updatePageData = () => {
    let {t} = this.props;
    let {
      selectedList,
      status,
      rowsPerPage,
      page,
      keyword,
      voucherType,
      receiverDepartment
    } = this.state;
    let { setPageLoading } = this.context;

    setPageLoading(true);
    let searchObject = {};
    searchObject.type = voucherType;
    searchObject.keyword = keyword?.trim() || null;
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchObject.status = status?.indexOrder || status?.id;
    searchObject.receiptDepartmentId = this.props.receiptDepartmentId;
    searchObject.departmentId = receiverDepartment?.id;
    searchObject.notInIds = this.props.item.dxIds?.join(', ');

    searchByPageRecallSlipIat(searchObject)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code && data.data !== null) {
          let countItemChecked = 0
          const itemList = data?.data?.content?.map(item => {
            item.createDate = formatDateTypeArray(item?.createDate)
            let isChecked = !!selectedList?.find(checkedItem => checkedItem === item.id)
              || !!this.props.item?.assetVouchers.find(asset => asset?.dxId === item?.id)

            if (selectedList?.find(checkedItem => checkedItem === item.id)) {
              countItemChecked++
            }
            return {
              ...item,
              checked: isChecked,
            }
          }
          )
          this.setState({
            itemList,
            listData: itemList,
            totalElements: data?.data?.totalElements,
            ids: [],
            rowDataAssetVouchersTable: [],
            checkAll: itemList?.length === countItemChecked && itemList?.length > 0
          })
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleCheck = (event, item) => {
    let { selectedList, itemList, isChecked, dxIds } = this.state;
    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        isChecked.push(item)
        dxIds.push(item?.id)
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        this.setState({
          selectedList: filteredArray,
          isChecked: filteredArray,
          dxIds: filteredArray
        })
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);
      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      if (!event.target.checked) {
        isChecked = isChecked.filter(id => id?.id !== item?.id);
        dxIds = dxIds.filter(id => id !== item?.id);
      } else {
        isChecked.push(item);
        dxIds.push(item?.id);
      }
      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
        isChecked,
        dxIds
      });
    }

  };

  hanldeCheckAssetVoucher = () => {
    let {isChecked} = this.state
    let roles = getTheHighestRole();
    let { departmentUser, isRoleOrgAdmin } = roles;
    const firstElement = isChecked[0]?.receiptDepartmentId;

    let isValid = {
      status: false,
      receip: false,
      receiptDepartmentId: false
    } 

    isChecked.forEach(item => {
      if (item.status !== 2) {
        isValid.status = true
      }
      if (item?.receiptDepartmentId !== firstElement) {
        isValid.receip = true
      }
      if (item?.receiptDepartmentId !== departmentUser?.id && !isRoleOrgAdmin) {
        isValid.receiptDepartmentId = true
      }
    })
    if (isValid.status) {
      toast.warning("Chỉ được chọn phiếu ở trạng thái đã thống kê")
      return
    }
    if (isValid.receip) {
      toast.warning("Chỉ được chọn phiếu ở một phòng ban")
      return
    }
    if (isValid.receiptDepartmentId) {
      toast.warning("Bạn không phải phòng thu hồi phiếu này")
      return
    }

    return true
  }

  handleAddAssetVoucher = () => {
    let {dxIds = []} = this.state;
    let {item, t} = this.props;
    let newDxIds = [...new Set(dxIds)];

    if(!this.hanldeCheckAssetVoucher()) return;

    newDxIds = newDxIds.filter(id => !item?.assetVouchers?.some(av => av.dxId === id)); // todo: lọc bỏ những dxId mà đã có CCDC thì sẽ k gọi api nữa.

    if (newDxIds.length > 0) {
      let assetVouchers = [...item?.assetVouchers.filter(item => !item.asset.dxId)] // todo: lấy ra CCDC không nằm trong các phiếu dự kiến.
      const searchObject = {
        dxIds: newDxIds,
      };

      getAssetVorcher(searchObject).then(({ data }) => {
        data?.data?.forEach(item => {
          assetVouchers.push({
            asset: item,
            assetId: item?.assetId,
            assetItemId: item?.assetItemId,
            tableData: item?.tableData,
            dxId: item.dxId,
            useDepartmentId: item?.useDepartmentId,
            id: item?.id
          });
        });
        this.props.handleOkClose?.(assetVouchers, newDxIds);
      })
      .catch((error) => {
        toast.error(t("toastr.error"));
      })
    } else {
      this.props.handleOkClose?.(item?.assetVouchers, newDxIds);
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  componentDidMount() {
    let {assetVouchers, status} = this.props;
    if (assetVouchers.length > 0) {
      let dxIds = assetVouchers?.filter(asset => asset?.dxId)?.map(asset => asset?.dxId);
      this.setState({
        dxIds,
      })
    }
    this.setState({statusIndex: null, status}, this.updatePageData);
  }

  render() {
    let {t} = this.props
    let {
      page,
      rowsPerPage,
      checkAll,
      listData,
      totalElements
    } = this.state;
    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },

      {
        title: t("AssetTransfer.issueDate"),
        field: "suggestDate",
        align: "left",
        width: 140,
        minWidth: 140,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.suggestDate
            ? moment(rowData.suggestDate).format("DD/MM/YYYY")
            : moment(rowData.issueDate).format("DD/MM/YYYY"),
      },
      {
        title: t("allocation_asset.status"),
        field: "status",
        width: "220px",
        minWidth: "220px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          const {DANG_THONG_KE, DA_THONG_KE, DA_THU_HOI, DANG_TONG_HOP} = appConst.STATUS_EXPECTED_RECOVERY;
          const classByStatus = {
            [DANG_THONG_KE.id]: "warning",
            [DA_THONG_KE.id]: "success",
            [DA_THU_HOI.id]: "info",
            [DANG_TONG_HOP.id]: "warning",
          }
          let className = `status status-${classByStatus[statusIndex]}`;
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "departmentName",
        width: "220px",
        minWidth: "220px",
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "personName",
        width: "200px",
        minWidth: "200px",
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiptDepartmentName",
        width: 180,
        minWidth: 180,
        render: (rowData) => rowData?.receiptDepartmentName || rowData?.receiverDepartmentName
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiptPersonName",
        minWidth: 200,
        render: (rowData) => rowData?.receiptPersonName || rowData?.receiverPersonName
      },
    ];
    return (
      <div>
        <Dialog
          open={this.props.open}
          // PaperComponent={PaperComponent}
          maxWidth="lg"
          fullWidth
          scroll="paper"
        >
          <DialogContent>
            <Grid item md={12} sm={12} xs={12} className="mt-16">
              <MaterialTable
                data={listData || []}
                columns={columns}
                localization={{
                  body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  }
                }}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  padding: 'dense',
                  rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  },
                  maxBodyHeight: '285px',
                  minBodyHeight: '285px',
                }}
                components={{
                  Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
               <TablePagination
                  align="left"
                  className="px-16"
                  rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                  labelRowsPerPage={t("general.rows_per_page")}
                  labelDisplayedRows={({ from, to, count, page }) =>
                      `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
                  }
                  component="paper"
                  count={totalElements}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{ "aria-label": "Previous Page", }}
                  nextIconButtonProps={{ "aria-label": "Next Page", }}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              className="mr-12"
              onClick={() => this.props.handleClose()}
            >
                {t("general.cancel")}
            </Button>
            {/* {!isView &&  */}
            <Button 
              variant="contained" 
              color="primary" 
              onClick={() => this.handleAddAssetVoucher()}
            >
              {t("general.save")}
            </Button>
            {/* // } */}
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
ExpectedRevocationIatPopup.contextType = AppContext;
ExpectedRevocationIatPopup.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.bool.isRequired,
  handleOkClose: PropTypes.func.isRequired,
  assetVouchers: PropTypes.array,
  status: PropTypes.object,
  receiptDepartmentId: PropTypes.string,
  item: PropTypes.any,
  t: PropTypes.func,
  isDataPage: PropTypes.bool,
}
