import {
  filterOptions,
  getUserInformation, handleThrowResponseMessage,
  isSuccessfulResponse,
  PaperComponent
} from "../../../appFunction";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Button, Dialog, DialogActions, Grid} from "@material-ui/core";
import {appConst} from "../../../appConst";
import React, {useContext, useState} from "react";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {searchByPageDepartmentNew} from "../../Department/DepartmentService";
import {ValidatorForm} from "react-material-ui-form-validator";
import AppContext from "../../../appContext";
import {toast} from "react-toastify";
import axios from "axios";
import PropTypes from "prop-types";
import ConstantList from "app/appConfig";

function SwitchDepartmentPopup(props) {
  const {
    open,
    t,
    ids = [],
    handleClose = () => {}
  } = props;
  const {setPageLoading} = useContext(AppContext);
  const {organization} = getUserInformation();
  
  const [department, setDepartment] = useState(null);
  
  const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-requests/change-receipt-department";
  const receiverDepartmentSeacrhObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  }
  
  const apiFunction = (params) => {
    return axios.get(API_PATH, {params})
  }
  
  const handleFormSubmit = async (e) => {
    try {
      setPageLoading(true);
      const params = {
        receiptDepartmentId: department?.id,
        ids: ids?.join(','),
      }
      const res = await apiFunction(params);
      let {code} = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(t("general.success"));
        handleClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }
  }
  
  return (
    <Dialog
      onClose={handleClose}
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth
    >
      <ValidatorForm onSubmit={handleFormSubmit} className="validator-form-scroll-dialog">
        <DialogTitle className="cursor-move">
          <span className="mb-20">Đổi phòng ban tổng hợp</span>
        </DialogTitle>
        <DialogContent>
          <Grid>
            <AsynchronousAutocompleteSub
              label={t("purchaseRequest.selectDepartment")}
              searchFunction={searchByPageDepartmentNew}
              searchObject={receiverDepartmentSeacrhObject}
              displayLable="name"
              isNoRenderChildren
              isNoRenderParent
              typeReturnFunction="category"
              value={department || null}
              onSelect={setDepartment}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12"
              variant="contained"
              color="secondary"
              onClick={handleClose}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16"
              variant="contained"
              color="primary"
              type="submit"
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  )
}

export default SwitchDepartmentPopup;
SwitchDepartmentPopup.propTypes = {
  open: PropTypes.bool,
  t: PropTypes.bool,
  ids: PropTypes.bool,
  handleClose: PropTypes.bool,
}