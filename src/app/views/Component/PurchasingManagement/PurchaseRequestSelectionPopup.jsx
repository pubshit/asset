import PropTypes from "prop-types";
import {
	Button,
	Dialog,
	DialogActions,
	Grid,
	Input,
	InputAdornment, TablePagination
} from "@material-ui/core";
import {
	classStatus,
	defaultPaginationProps, formatTimestampToDate,
	handleKeyDown, handleKeyUp,
	handleThrowResponseMessage,
	isSuccessfulResponse,
	PaperComponent
} from "../../../appFunction";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import SearchIcon from "@material-ui/icons/Search";
import {appConst} from "../../../appConst";
import MaterialTable from "material-table";
import React, {memo, useCallback, useContext, useEffect, useMemo, useState} from "react";
import AppContext from "../../../appContext";

function PurchaseRequestSelectionPopup(props) {
	const {setPageLoading} = useContext(AppContext);
	const {
		t,
		open,
		selectedProducts = [],
		filterSelected = (x, y) => {},
		searchFunction = async (searchObject) => {},
		handleClose = () => {},
		validOnSelect = (item, selectedList, itemList) => true,
		validForSelect = (item) => true,
		convertSelectedItem = (item) => {},
		handleSelect = (item) => {},
		title = "",
    procurementPlanId = "",
    params = {},
  } = props;
	const [state, setState] = useState({
		keyword: "",
		page: 0,
		rowsPerPage: 10,
		totalElements: 0,
		toggleSearch: false,
		products: [...selectedProducts], // selected products
		tempSelectedProduct: [],
		itemList: [],
    procurementPlanId: procurementPlanId,
    params: {...params}
  });
	
	useEffect(() => {
		updatePageData();
	}, [state.toggleSearch]);
	
	const columns =  [
		{
			title: t("general.index"),
			field: "",
			maxWidth: 50,
			align: "left",
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => (rowData.tableData.id + 1),
		},
		{
			title: t("purchaseRequest.requestDate"),
			field: "requestDate",
			align: "left",
			minWidth: "150px",
			render: (rowData) => formatTimestampToDate(rowData?.requestDate)
			
		},
		{
			title: t("SuggestedMaterials.ballotCode"),
			field: "code",
			align: "left",
			minWidth: "150px",
			cellStyle: {
				textAlign: "center"
			},
		},
		{
			title: t("purchaseRequest.status"),
			field: "status",
			align: "left",
			minWidth: "150px",
			cellStyle: {
				textAlign: "center"
			},
			render: (rowData) => {
				const statusIndex = rowData?.status;
				return (
					<span className={classStatus(statusIndex)}>
            {checkStatus(statusIndex)}
          </span>
				)
			}
		},
		{
			title: t("purchaseRequest.procurementPlan"),
			field: "procurementPlanName",
			align: "left",
			minWidth: "150px",
		},
		{
			title: t("purchaseRequest.requestDepartment"),
			field: "requestDepartmentName",
			align: "left",
			minWidth: "250px",
		},
		{
			title: t("purchaseRequest.receptionDepartment"),
			field: "receptionDepartmentName",
			align: "left",
			minWidth: "250px",
		},
		{
			title: t("purchaseRequest.requestPersonName"),
			field: "requestPersonName",
			align: "left",
			minWidth: "150px",
		},
	];
	
	const updatePageData = async () => {
    let {page, rowsPerPage} = state;
    let searchObject = {
      ...props.searchObject,
      keyword: state.keyword,
      pageSize: rowsPerPage,
      pageIndex: page + 1,
      // status: appConst.listStatusPurchaseRequestObject.CHO_TONG_HOP.code,
      procurementPlanId: state.procurementPlanId,
      ...state.params,
    };
    
    try {
			setPageLoading(true);
			let res = await searchFunction(searchObject);
			let {data, code} = res?.data;
			if (isSuccessfulResponse(code)) {
				let array = data?.content || [];
				let tempSelectedProduct = [];
				array.map((item) => {
					let isExistItem = state.products?.some((x) => filterSelected(x, item));
					
					if (isExistItem) {
						tempSelectedProduct.push(item);
					}
					
					item.tableData = {
						...item.tableData,
						checked: isExistItem,
					};
					return item;
				});
				
				handleSetState({
					itemList: data?.content?.length > 0 ? [...data?.content] : [],
					totalElements: data.totalElements,
					tempSelectedProduct,
				});
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (e) {
		
		} finally {
			setPageLoading(false);
		}
	};
	
	const handleSetState = (value = {}) => {
		setState(prevState => ({...prevState, ...value}));
	}
	
	const handleChange = (event, source) => {
		handleSetState({
			[event.target.name]: event.target.value,
		});
	};
	
	const setPage = (page) => {
		handleSetState({page, toggleSearch: !state.toggleSearch});
	};
	
	const setRowsPerPage = (event) => {
		handleSetState({rowsPerPage: event.target.value, page: 0});
	};
	
	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};
	
	const search = () => {
		setPage(0)
	}
	
	const handleKeyDownEnterSearch = (e) => handleKeyDown(e, search);
	
	const handleKeyUpSearch = (e) => handleKeyUp(e, search);
  
  const checkStatus = (status) => {
    let itemStatus = appConst.listStatusPurchaseRequest.find(
      (item) => item.code === status
    );
    return itemStatus?.name;
  };
	
	const handleChangeOnSelection = useCallback((rows = [], rowData) => {
		let {products = [], itemList = []} = state;
		let newArray = [...state.products];
		let tempSelectedProduct = [...state.tempSelectedProduct];
		const existItem = products?.find(x => filterSelected(x, rowData));
  
		if (existItem) {
			newArray.splice(products.indexOf(existItem), 1);
		} else if (validOnSelect(rowData, tempSelectedProduct, itemList, products)) {
			newArray.push(convertSelectedItem(rowData));
			tempSelectedProduct.push(rowData);
		}
  
		handleSetState({
			products: newArray,
			tempSelectedProduct
		});
	}, [state.products]);
	
	
	const renderTable = useMemo(() => (
		<MaterialTable
			data={state.itemList || []}
			columns={columns}
			options={{
				sorting: false,
				toolbar: false,
				selection: true,
				actionsColumnIndex: -1,
				paging: false,
				search: false,
				padding: "dense",
				minBodyHeight: "233px",
				maxBodyHeight: "400px",
				showSelectAllCheckbox: false,
				rowStyle: (rowData) => ({
					backgroundColor:
						rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
				}),
				selectionProps: (rowData) => ({
					disabled: !validForSelect(rowData),
					checked: !validForSelect(rowData, state.tempSelectedProduct, state.itemList, state.products)
						? false
						: rowData.tableData.checked,
				}),
			}}
			localization={{
				body: {
					emptyDataSourceMessage: `${t(
						"general.emptyDataMessageTable"
					)}`,
				},
				...appConst.localizationVi,
			}}
			onSelectionChange={handleChangeOnSelection}
		/>
	), [state.itemList, state.products]);
	
	return (
		<Dialog
			onClose={handleClose}
			open={open}
			PaperComponent={PaperComponent}
			maxWidth={"md"}
			fullWidth
		>
			<DialogTitle className="cursor-move">
				<span className="mb-20">{title}</span>
			</DialogTitle>
			<DialogContent>
				<Grid container>
					<Grid item md={6} sm={6} xs={12}>
						<Input
							label={t("general.enterSearch")}
							type="text"
							name="keyword"
							value={state.keyword || ""}
							onChange={handleChange}
							onKeyDown={handleKeyDownEnterSearch}
							onKeyUp={handleKeyUpSearch}
							fullWidth
							className="mb-16 "
							id="search_box"
							placeholder={t("general.seacrhByProductName")}
							startAdornment={
								<InputAdornment position="end">
									<SearchIcon onClick={search} className="searchTable"/>
								</InputAdornment>
							}
						/>
					</Grid>
				</Grid>
				<Grid item xs={12}>
					{renderTable}
					<TablePagination
						{...defaultPaginationProps()}
						rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
						count={state.totalElements}
						rowsPerPage={state.rowsPerPage}
						page={state.page}
						onPageChange={handleChangePage}
						onRowsPerPageChange={setRowsPerPage}
					/>
				</Grid>
			</DialogContent>
			<DialogActions>
				<div className="flex flex-space-between flex-middle">
					<Button
						className="mr-12"
						variant="contained"
						color="secondary"
						onClick={handleClose}
					>
						{t("general.cancel")}
					</Button>
					<Button
						className="mr-16"
						variant="contained"
						color="primary"
						onClick={() => handleSelect(state.products)}
					>
						{t("general.select")}
					</Button>
				</div>
			</DialogActions>
		</Dialog>
	)
}

export default memo(PurchaseRequestSelectionPopup);
PurchaseRequestSelectionPopup.propTypes = {
	t: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired,
	handleClose: PropTypes.func.isRequired,
	handleSelect: PropTypes.func,
	searchFunction: PropTypes.func,
	searchObject: PropTypes.object,
	selectedProducts: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
	filterSelected: PropTypes.func, // (selectedItem, selectedRowData) => fields want to compare. ex: selectedItem.id === selectedRowData.id
	validOnSelect: PropTypes.func,
	validForSelect: PropTypes.func,
	convertSelectedItem: PropTypes.func,
	title: PropTypes.string,
  procurementPlanId: PropTypes.string,
  params: PropTypes.shape({
      receptionDepartmentId: PropTypes.string,
  }),
}
