import {Button, Dialog, DialogActions, DialogTitle, Divider, Grid, Link} from "@material-ui/core";
import {handleThrowResponseMessage, isSuccessfulResponse, PaperComponent} from "../../../appFunction";
import CustomValidatorForm from "../ValidatorForm/CustomValidatorForm";
import DialogContent from "@material-ui/core/DialogContent";
import React, {useContext, useEffect, useState} from "react";
import {getDetailVoucherByUri, getWfProcessHistory} from "../../ApprovalRequest/ApprovalRequestService";
import {toast} from "react-toastify";
import AppContext from "../../../appContext";
import PropTypes from "prop-types";
import {defaultHistory, defaultStep} from "./constant";
import {MODEL_CODE, ROUTES_PATH, TRACKER_CODE} from "../../../appConst";
import TwoLevelApprovalForm from "./TwoLevelApprovalForm";
import Scrollbar from "react-perfect-scrollbar";
import DeviceTroubleshootingDialog from "../../AssetRepair/Dialog/DeviceTroubleshootingDialog";
import {useLocation} from "react-router-dom";

function ApprovalProcessHistoryPopup(props) {
  const {setPageLoading} = useContext(AppContext);
  const location = useLocation();
  const {open, t, handleClose = () => {}, objectId, item = {}, trackerCodeRelate} = props;

  const [history, setHistory] = useState({...defaultHistory});
  const [steps, setSteps] = useState([{}]);
  const [voucherDetail, setVoucherDetail] = useState(null);
  const [shouldOpenDetailDialog, setShouldOpenDetailDialog] = useState(false);

  const approvalRoutePath = "/" + ROUTES_PATH.APPROVAL_REQUEST;
  const detailDialog = {
    [TRACKER_CODE.REPAIR]: DeviceTroubleshootingDialog,
  }

  const form = {
    [MODEL_CODE.TWO_LEVEL_APPROVAL_FORM]: TwoLevelApprovalForm,
  }

  useEffect(() => {
    handleGetWfHistory();
  }, []);

  const handleGetWfHistory = async () => {
    try {
      setPageLoading(true);
      let params = {
        objectId,
      }

      let res = await getWfProcessHistory(params);
      let {data, code} = res?.data;
      if (isSuccessfulResponse(code)) {
        setHistory(data);
        setSteps(data?.steps);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const renderSingleStep = (step = defaultStep) => {
    const Component = form[history?.modelCode];
    return (
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <b>Trình lần {step.submissionNumber} cấp {step.level}:  </b>
          {step.fields?.newStatusName?.valueText}
        </Grid>

        <Grid item container className="p-16">
          {Component && (
            <Component item={step.fields}/>
          )}
        </Grid>
      </Grid>
    )
  }

  const handleOpenDetailDialog = () => {
    setShouldOpenDetailDialog(true);
  }

  const handleCloseDetailDialog = () => {
    setShouldOpenDetailDialog(false);
  }

  const handleGetDetailVoucher = async () => {
    try {
      const uri = history?.steps[0]?.fields?.uri?.valueText;
      let res = await getDetailVoucherByUri(uri);

      const {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setVoucherDetail(data);
        handleOpenDetailDialog();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  const renderDetailDialog = () => {
    const code = item?.trackerCodeRelate || trackerCodeRelate;
    const DetailDialogComponent = detailDialog[code];
    let props = {};

    switch (code) {
      case TRACKER_CODE.REPAIR:
        props = {
          t,
          handleClose: handleCloseDetailDialog,
          itemEdit: voucherDetail,
          open: shouldOpenDetailDialog,
          isView: true,
          isAdd: false,
          isFromApprovalRequest: true,
        }
        break;
      default:
        break;
    }

    return (
      <>
        {shouldOpenDetailDialog && DetailDialogComponent && (
          <DetailDialogComponent {...props}/>
        )}
      </>
    )
  }

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth>
      <DialogTitle className="pb-0">
        <div className="flex flex-space-between">
          <span>{t("ApprovalRequest.history")}</span>
          {location.pathname === approvalRoutePath && (
            <Link className="fz-xm" component="button" onClick={handleGetDetailVoucher}>Xem thông tin đính kèm</Link>
          )}
        </div>
      </DialogTitle>
      <CustomValidatorForm onSubmit={() => {}}>
        <DialogContent>
          <Scrollbar options={{suppressScrollX: true}} className="position-relative" style={{maxHeight: 450}}>
            {steps?.map((step = defaultStep, index) => {
              return (
                <div key={index}>
                  {renderSingleStep(step)}
                  <Divider className="mb-12 mt-30 mr-14 bg-primary"/>
                </div>
              )
            })}
          </Scrollbar>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={handleClose}
          >
            {t("general.close")}
          </Button>
        </DialogActions>
      </CustomValidatorForm>
      {renderDetailDialog()}
    </Dialog>
  )
}

export default ApprovalProcessHistoryPopup;
ApprovalProcessHistoryPopup.propTypes = {
  t: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  objectId: PropTypes.string.isRequired,
  trackerCodeRelate: PropTypes.string.isRequired,
  item: PropTypes.any,
}
