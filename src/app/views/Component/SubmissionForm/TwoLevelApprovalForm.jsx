import React, {memo, useEffect, useState} from "react";
import {TextValidator} from "react-material-ui-form-validator";
import {useTranslation} from "react-i18next";
import {Grid} from "@material-ui/core";
import PropTypes from "prop-types";
import ValidatePicker from "../ValidatePicker/ValidatePicker";
import {DATA_TYPE, variable} from "../../../appConst";
import {defaultState, defaultPermission} from "./constant";

function TwoLevelApprovalForm (props) {
  const {t} = useTranslation();
  const [state, setState] = useState({...defaultState});
  const [permission, setPermission] = useState({...defaultPermission});

  useEffect(() => {
    handleCheckPermission();
    handleGetValue();
  }, [props.item]);

  const handleCheckPermission = () => {
    const permission = {...defaultPermission};
    const item = {...props.item};

    Object.keys(item).map(key => {
      if (permission[key]) {
        permission[key].isShow = Boolean(item[key]);
      }
    })

    setPermission({...permission});
  }

  const handleGetValue = () => {
    const item = {...props.item};
    const newState = {...state};
    const {valueBoolean, valueComplex, valueDatetime, valueNumeric, valueText} = variable.listInputName;
    const value = {
      [DATA_TYPE.TEXT]: valueText,
      [DATA_TYPE.COMPLEX]: valueComplex,
      [DATA_TYPE.BOOLEAN]: valueBoolean,
      [DATA_TYPE.DATETIME]: valueDatetime,
      [DATA_TYPE.NUMBER]: valueNumeric,
    }
    Object.keys(item).map(key => {
      const dataType = item[key].fieldDatatype;
      const valueField = value[dataType];
      const code = item[key].fieldCode || "";
      newState[code] = item[key][valueField];
    });

    setState(newState)
  }

  return (
    <Grid item container spacing={2} xs={12}>
      <Grid hidden={!permission.firstLevelSubmissionDate.isShow} item xs={4}>
        <ValidatePicker
          label={t("ApprovalRequest.submittingDate")}
          readOnly
          value={state?.firstLevelSubmissionDate || null}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmittingDepartmentName.isShow} item xs={4}>
        <TextValidator
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          label={t("ApprovalRequest.submittingDepartment")}
          value={state?.firstLevelSubmittingDepartmentName || ""}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmitterName.isShow} item xs={4}>
        <TextValidator
          label={t("ApprovalRequest.submitter")}
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          value={state?.firstLevelSubmitterName || ""}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelSubmissionContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={t("ApprovalRequest.submissionContent")}
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: true,
          }}
          value={state?.firstLevelSubmissionContent || ""}
        />
      </Grid>

      {/*Thông tin duyệt*/}
      <Grid hidden={!permission.firstLevelApprovalDate.isShow} item xs={4}>
        <ValidatePicker
          label={t("ApprovalRequest.approvalDate")}
          readOnly
          value={state?.firstLevelApprovalDate || null}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApprovingDepartmentName.isShow} item xs={4}>
        <TextValidator
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          label={t("ApprovalRequest.approvingDepartment")}
          value={state?.firstLevelApprovingDepartmentName || ""}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApproverName.isShow} item xs={4}>
        <TextValidator
          label={t("ApprovalRequest.approver")}
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          value={state?.firstLevelApproverName || ""}
        />
      </Grid>
      <Grid hidden={!permission.firstLevelApprovalContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={t("ApprovalRequest.approvalContent")}
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: true,
          }}
          value={state?.firstLevelApprovalContent || ""}
        />
      </Grid>

      {/* Trình cấp 2*/}
      <Grid hidden={!permission.secondLevelSubmissionDate.isShow} item xs={3}>
        <ValidatePicker
          label={t("ApprovalRequest.secondLevelSubmissionDate")}
          name="secondLevelSubmissionDate"
          readOnly
          value={state?.secondLevelSubmissionDate || null}
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApprovingDepartmentName.isShow} item xs={3}>
        <TextValidator
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          label={t("ApprovalRequest.secondLevelApprovingDepartment")}
          value={state?.secondLevelApprovingDepartmentName || ""}
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApproverName.isShow} item xs={3}>
        <TextValidator
          label={t("ApprovalRequest.secondLevelApprover")}
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          value={state?.secondLevelApproverName || ""}
        />
      </Grid>

      {/* Duyệt cấp 2 */}
      <Grid hidden={!permission.secondLevelApprovalDate.isShow} item xs={3}>
        <ValidatePicker
          label={t("ApprovalRequest.secondLevelApprovalDate")}
          value={state?.secondLevelApprovalDate || null}
          readOnly
        />
      </Grid>
      <Grid hidden={!permission.secondLevelApprovalContent.isShow} item xs={12}>
        <TextValidator
          fullWidth
          label={t("ApprovalRequest.secondLevelApprovalContent")}
          multiline
          minRows={2}
          variant="outlined"
          InputProps={{
            readOnly: true,
          }}
          value={state?.secondLevelApprovalContent || ""}
        />
      </Grid>
    </Grid>
  );
};

export default memo(TwoLevelApprovalForm);
TwoLevelApprovalForm.propTypes = {
  item: PropTypes.any,
}
