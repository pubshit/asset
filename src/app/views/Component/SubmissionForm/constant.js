import {appConst} from "../../../appConst";


export const defaultState = {
  firstLevelSubmissionDate: null,
  firstLevelSubmittingDepartmentName: null,
  firstLevelSubmittingDepartmentId: null,
  firstLevelSubmitterId: null,
  firstLevelSubmitterName: null,
  firstLevelSubmissionContent: null,
  firstLevelApprovingDepartmentName: null,
  firstLevelApprovingDepartmentId: null,
  firstLevelApproverId: null,
  firstLevelApproverName: null,
  firstLevelApprovalContent: null,
  firstLevelApprovalDate: null,
  newStatusCode: "",
  newStatusId: null,
  newStatusName: "",
  oldStatusCode: null,
  oldStatusId: null,
  oldStatusName: null,
  secondLevelApproverId: null,
  secondLevelApproverName: null,
  secondLevelApprovingDepartmentId: null,
  secondLevelApprovingDepartmentName: null,
  secondLevelSubmissionDate: null,
  secondLevelApprovalDate: null,
  secondLevelApprovalContent: null,
  uri: null,
}

export const defaultPermission = {
  firstLevelSubmissionDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmittingDepartmentName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmittingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmitterId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmitterName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelSubmissionContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovingDepartmentName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApproverId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApproverName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovalContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
  firstLevelApprovalDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  newStatusCode: {...appConst.OBJECT_PERMISSION_DEFAULT},
  newStatusId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  newStatusName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  oldStatusCode: {...appConst.OBJECT_PERMISSION_DEFAULT},
  oldStatusId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  oldStatusName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApproverId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApproverName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovingDepartmentId: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovingDepartmentName: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelSubmissionDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovalDate: {...appConst.OBJECT_PERMISSION_DEFAULT},
  secondLevelApprovalContent: {...appConst.OBJECT_PERMISSION_DEFAULT},
};


export const defaultField = {
  id: null,
  attribute: null,
  level: 1,
  fieldName: null,
  objectId: "",
  modelCode: "",
  fieldCode: "",
  fieldDatatype: "",
  obsDatetime: "",
  valueNumeric: null,
  valueDatetime: null,
  valueBoolean: null,
  valueComplex: null,
  submissionNumber: 1,
  step: 1,
  valueText: ""
}

export const defaultStep = {
  level: 0,
  submissionNumber: 0,
  fields: {
    oldStatusId: {...defaultField},
    firstLevelApprovingDepartmentName: {...defaultField},
    firstLevelSubmitterName: {...defaultField},
    oldStatusName: {...defaultField},
    uri: {...defaultField},
    firstLevelApproverName: {...defaultField},
    oldStatusCode: {...defaultField},
    firstLevelSubmissionContent: {...defaultField},
    newStatusId: {...defaultField},
    firstLevelApprovingDepartmentId: {...defaultField},
    newStatusName: {...defaultField},
    firstLevelSubmittingDepartmentId: {...defaultField},
    firstLevelSubmittingDepartmentName: {...defaultField},
    firstLevelApproverId: {...defaultField},
    newStatusCode: {...defaultField},
    firstLevelSubmitterId: {...defaultField},
    firstLevelSubmissionDate: {...defaultField}
  },
}

export const defaultHistory = {
  modelCode: "",
  steps: [defaultStep],
}
