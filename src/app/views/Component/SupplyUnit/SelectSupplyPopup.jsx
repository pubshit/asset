import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Grid, Input, InputAdornment, Link, Paper, Radio, TablePagination } from '@material-ui/core'
import MaterialTable, { MTableToolbar } from 'material-table'
import React, { useContext, useEffect, useState } from 'react'
import { searchByText, searchByTextNew } from '../../Supplier/SupplierService';
import Draggable from 'react-draggable';
import SearchIcon from '@material-ui/icons/Search';
import { appConst, STATUS_SUPPLIER } from '../../../appConst';
import { toast } from 'react-toastify';
import SupplierDialog from 'app/views/Supplier/SupplierDialog';
import i18n from 'i18n';
import AppContext from "../../../appContext";

function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

export default function SelectSupplyPopup(props) {
    const { setPageLoading } = useContext(AppContext);
    const {
        t,
        onClose,
        open,
        handleSelect,
        unit, typeCodes,
        isMaintainRequest
    } = props
    const [selectedValue, setSelectedValue] = useState('');
    const [selectedItem, setSelectedItem] = useState('');
    const [listUnit, setListUnit] = useState([]);
    const [shouldOpenSupplierDialog, setShouldOpenSupplierDialog] = useState(false);
    const [totalElements, setTotalElements] = useState(0)
    const [keyword, setKeyword] = useState("")
    const [query, setQuery] = useState({
        keyword: '',
        pageIndex: 0,
        pageSize: 10,
        typeCodes: typeCodes
    });

    useEffect(() => {
        setSelectedValue(unit?.donViThucHienId || unit?.id || '');
        setSelectedItem(unit)
    }, [unit]);

    useEffect(() => {
        handleSearchByText();
    }, [query]);


    const handleSearchByText = async () => {
        setPageLoading(true);
        let searchObject = {
            pageIndex: query.pageIndex + 1,
            pageSize: query.pageSize,
            keyword: keyword?.trim(),
            typeCodes: query.typeCodes,
            isActive: STATUS_SUPPLIER.HOAT_DONG.code
        }
        try {
            const { status, data } = await searchByTextNew(searchObject);
            if (status === appConst.CODE.SUCCESS) {
                setListUnit(data?.content || []);
                setTotalElements(data?.totalElements)
            } else {
                toast.error(t("general.error"));
            }
        } catch (error) {
            toast.error(t("general.error"))
        } finally {
            setPageLoading(false);
        }
    };

    const handleChangeRadio = (event, data) => {
        setSelectedItem(data ? data : {});
        setSelectedValue(event.target.value);
    };

    const handleChange = (e) => {
        setKeyword(e.target.value)
    }

    const handleKeyDownEnterSearch = e => {
        if (e.key === 'Enter') {
            setQuery({
                ...query,
                pageIndex: 0,
            });
        }
    };
    const handleKeyUp = (e) => {
        if (!e.target.value) {
            handleSearchByText()
        }
    }

    const handleChangePage = (event, newPage) => {
        setQuery({
            ...query,
            pageIndex: newPage
        })
    };

    const setRowsPerPage = (event) => {
        setQuery({
            ...query,
            pageSize: event.target.value,
            pageIndex: 0
        })
    }

    const columns = [
        {
            title: t('general.select'),
            field: '',
            align: 'center',
            maxWidth: 50,
            render: (rowData) => (
                <div>
                    <Radio
                        id={`radio${rowData.id}`}
                        name={rowData.id}
                        style={{ padding: '0px' }}
                        value={rowData.id}
                        checked={selectedValue === rowData.id}
                        onChange={(event) => handleChangeRadio(event, rowData)}
                    />
                </div>
            ),
        },
        {
            title: t('SelectSupplyPopup.maDonVi'),
            field: 'code',
            align: 'left',
            maxWidth: 100,
        },
        {
            title: t('SelectSupplyPopup.tenDonVi'),
            field: 'name',
            align: 'left',
            minWidth: 200,
        },
    ];

    const handleOpenSupplierDialog = () => {
        setShouldOpenSupplierDialog(true);
    };

    const handleCloseSupplierDialog = () => {
        setShouldOpenSupplierDialog(false);
    };


    return (
        <Dialog onClose={onClose} open={open} PaperComponent={PaperComponent} fullWidth maxWidth="md">
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                <span className="mb-20">{t("SelectSupplyPopup.chonDonViThucHien")}</span>
            </DialogTitle>
            <DialogContent>
                <Grid container spacing={2} justifyContent="space-between">
                    <Grid item md={6} sm={12} xs={12}>
                        {listUnit?.length <= 0 && (
                            <Button
                                className="mr-16"
                                variant="contained"
                                color="primary"
                                onClick={handleOpenSupplierDialog}
                            // disabled={disable}
                            >
                                {t("general.add")}
                            </Button>
                        )}
                    </Grid>
                    {shouldOpenSupplierDialog && (
                        <SupplierDialog
                            t={t} i18n={i18n}
                            handleClose={handleCloseSupplierDialog}
                            open={shouldOpenSupplierDialog}
                            handleOKEditClose={handleCloseSupplierDialog}
                            item={{ name: keyword }}
                            handleSelect={handleSelect}
                            typeCodes={typeCodes}
                            isMaintainRequest={isMaintainRequest}
                        />
                    )}
                    {/* <Grid item md={2} sm={12} xs={12}>
                        {isHiddenButton && (
                            <Button
                                size="small"
                                className="mt-14 mr-16 align-bottom"
                                variant="contained"
                                color="primary"
                                onClick={() => { }}
                            >
                                {t('general.add_asset')}
                            </Button>
                        )}
                    </Grid> */}
                    <Grid item md={6} sm={12} xs={12}>
                        <FormControl fullWidth>
                            <Input
                                name="keyword"
                                value={keyword}
                                className="search_box"
                                onKeyUp={handleKeyUp}
                                onChange={handleChange}
                                onKeyDown={handleKeyDownEnterSearch}
                                placeholder={t("SelectSupplyPopup.timKiemTheoMaHoacTenDonVi")}
                                id="search_box"
                                startAdornment={
                                    <InputAdornment position="start">
                                        <SearchIcon
                                            className="searchTable"
                                            onClick={() => setQuery({ ...query, pageIndex: 0 })}
                                        />
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <MaterialTable
                            data={listUnit}
                            columns={columns}
                            localization={{
                                body: {
                                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`,
                                },
                            }}
                            options={{
                                toolbar: false,
                                selection: false,
                                actionsColumnIndex: -1,
                                paging: false,
                                search: false,
                                sorting: false,
                                padding: 'dense',
                                rowStyle: (rowData) => ({
                                    backgroundColor: rowData.tableData.id % 2 === 1 ? '#EEE' : '#FFF',
                                }),
                                headerStyle: {
                                    backgroundColor: '#358600',
                                    color: '#fff',
                                },
                                maxBodyHeight: '320px',
                                minBodyHeight: '320px',
                            }}
                            components={{
                                Toolbar: (props) => (
                                    <div style={{ width: '100%' }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                            }}
                            onSelectionChange={(rows) => { }}
                        />
                        <TablePagination
                            align="left"
                            className="px-16"
                            rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                            component="div"
                            labelRowsPerPage={t("general.rows_per_page")}
                            labelDisplayedRows={({ from, to, count }) =>
                                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                                }`
                            }
                            count={totalElements}
                            page={query.pageIndex}
                            rowsPerPage={query.pageSize}
                            backIconButtonProps={{
                                "aria-label": "Previous Page",
                            }}
                            nextIconButtonProps={{
                                "aria-label": "Next Page",
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={setRowsPerPage}
                        />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <div className="flex flex-space-between flex-middle">
                    <Button className="mb-16 mr-12" variant="contained" color="secondary" onClick={() => onClose()}>
                        {t('general.cancel')}
                    </Button>
                    <Button
                        className="mb-16 mr-16"
                        variant="contained"
                        color="primary"
                        onClick={() => handleSelect(selectedItem)}
                    >
                        {t('general.select')}
                    </Button>
                </div>
            </DialogActions>
        </Dialog>
    )
}
