import DateFnsUtils from "@date-io/date-fns";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import React from "react";
import { ValidatorComponent } from "react-material-ui-form-validator";
import viLocale from "date-fns/locale/vi";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";

class ValidatedDatePicker extends ValidatorComponent {
  renderValidatorComponent() {
    const {
      t,
      tReady,
      errorMessages,
      validators,
      requiredError,
      helperText,
      validatorListener,
      ...rest
    } = this.props;
    const { isValid } = this.state;
    const customAttribute = {
      error: !isValid,
      helperText: (!isValid && this.getErrorMessage()) || helperText,
    }

    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale} >
        <KeyboardDatePicker
          fullWidth
          autoOk
          format="dd/MM/yyyy"
          inputVariant="standard"
          clearLabel={t("general.remove")}
          cancelLabel={t("general.cancel")}
          okLabel={t("general.select")}
          InputProps={{
            readOnly: rest.readOnly,
          }}
          KeyboardButtonProps={{
            "aria-label": "change date",
            disabled: rest.readOnly
          }}
          minDateMessage={rest?.minDate ? rest?.minDateMessage : t("general.minDateDefault")}
          maxDateMessage={rest?.maxDate ? rest?.maxDateMessage : t("general.maxDateDefault")}
          invalidDateMessage={t("general.invalidDateFormat")}
          {...rest}
          {...(!this.props.value && customAttribute)}
          istabgeneralinfomation={rest.isTabGeneralInfomation}
          onChange={(value) => rest.onChange(value, rest.name)}
          id="date-picker-dialog"
        />
      </MuiPickersUtilsProvider>
    );
  }
}

ValidatedDatePicker.propTypes = {
  onChange: PropTypes.func,
  validators: PropTypes.array,
  errorMessages: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  format: PropTypes.string,
  inputVariant: PropTypes.oneOf(['standard', "outlined", "filled"]),
  value: PropTypes.any,
  maxDate: PropTypes.any,
  minDate: PropTypes.any,
  maxDateMessage: PropTypes.string,
  minDateMessage: PropTypes.string,
  invalidDateMessage: PropTypes.string,
  clearLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  okLabel: PropTypes.string,
  disabled: PropTypes.bool,
  autoOk: PropTypes.bool,
  clearable: PropTypes.bool,
  KeyboardButtonProps: PropTypes.object,
  InputLabelProps: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  readOnly: PropTypes.bool,
  name: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium"]),
  isTabGeneralInfomation: PropTypes.bool,
  views: PropTypes.arrayOf(PropTypes.oneOf(["date", "month", "year"])),
  openTo:PropTypes.oneOf(["date", "month", "year"]),
  disableFuture: PropTypes.bool,
  disablePast: PropTypes.bool,
  animateYearScrolling: PropTypes.bool,
  className: PropTypes.string,
  disableToolbar: PropTypes.bool,
}
export default withTranslation()(ValidatedDatePicker);
