import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import { Button, CircularProgress, Dialog, DialogActions, Grid, Tab, Tabs } from "@material-ui/core";
import React, {useEffect, useRef, useState} from "react";
import { FadeLayout, PaperComponent, TabPanel } from "../Utilities";
import {
  a11yProps, functionExportToExcel,
  functionExportToWord,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../../appFunction";
import Divider from "@material-ui/core/Divider";
import {PRINT_DOCUMENT_TYPE, variable} from "../../../appConst";
import Scrollbar from "react-perfect-scrollbar";
import {exportExcelFileError, getResourceTemplate, getTemplateByModel} from "../../../appServices";
import {toast} from "react-toastify";
import ConstantList from "app/appConfig";
import axios from "axios";

export const PrintPreviewTemplateDialog = (props) => {
  let {
    t,
    open = false,
    handleClose,
    title,
    model = "",
    item = {},
    externalConfig = {},
  } = props;
  const {organization} = getUserInformation();

  const form = useRef(null);
  const [tabValue, setTabValue] = useState(null);
  const [currentTemplate, setCurrentTemplate] = useState({
    name: "",
    id: null,
    path: "",
    fileName: "",
    order: 0,
    code: "",
    model: "",
    previewPath: "",
    orgId: "",
    exportUrl: "",
    excelUrl: null,
    contentType: ""
  });
  const [template, setTemplate] = useState("");
  const [templates, setTemplates] = useState([]);
  const [loading, setLoading] = useState(false);
  const [display, setDisplay] = useState({
    excelButton: false,
    wordButton: true,
    previewDetail: true,
    previewTemplate: false,
  });
  const [templateCached, setTemplateCached] = useState({
    1: {
      previewTemplate: null, // mẫu phiếu chưa được fill thông tin
      previewDetail: null, // mẫu phiếu đã được fill thông tin
    }
  });

  const baseEndpoint = ConstantList.API_ENPOINT_ASSET_MAINTANE;

  useEffect(() => {
    handleGetTemplatesByModel();
  }, [model]);

  useEffect(() => {
    if (currentTemplate?.id) {
      setDisplay(prevState => ({
        ...prevState,
        previewTemplate: true,
        previewDetail: false,
        excelButton: Boolean(currentTemplate?.excelUrl),
      }))
      handleGetResourceTemplatePreview();
    }
  }, [currentTemplate?.id]);


  const handleGetTemplatesByModel = async () => {
    try {
      setLoading(true);
      let searchObject = {
        orgId: organization?.org?.id,
        model,
      }
      let res = await getTemplateByModel(searchObject);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        setTemplates(data);
        setTabValue(data?.[0]?.id);
        setCurrentTemplate(data?.[0]);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      console.error(e)
    } finally {
      setLoading(false);
    }
  }

  const handleGetResourceTemplatePreview = async () => {
    try {
      if (Boolean(templateCached[currentTemplate?.id]?.previewTemplate)) {
        handleTongglePreviewButton();
        return setTemplate(templateCached[currentTemplate?.id]?.previewTemplate);
      }

      setLoading(true);
      let searchObject = {
        templateId: tabValue,
        ...externalConfig?.params,
      }
      await handleGetTemplateById(
        getResourceTemplate,
        searchObject,
        variable.listInputName.previewTemplate,
      );
    } catch (e) {
      console.error(e);
      toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  const handleChangeTabValue = (event, newValue) => {
    let template = templates?.find(template => template.id === newValue);

    setDisplay(prevState => ({
      ...prevState,
      excelButton: Boolean(template.excelUrl),
    }))
    setTabValue(newValue);
    setCurrentTemplate(template)
  };

  const templateApiFunction = (params) => {
    return axios({
      method: "get",
      url: params?.urlWord || (baseEndpoint + (currentTemplate?.exportUrl || "") + (item?.id || "")),
      params,
      responseType: "blob",
    });
  }

  const excelApiFunction = (params) => {
    return axios({
      method: "get",
      url: baseEndpoint + currentTemplate?.excelUrl + (item?.id || ""),
      params,
      responseType: "blob",
    });
  }

  const handlePrintWord = async () => {
    try {
      const params = {
        templateId: tabValue,
        documentType: PRINT_DOCUMENT_TYPE.WORD,
        ...externalConfig?.params,
        urlWord: externalConfig?.urlWord,
      }
      await functionExportToWord(
        templateApiFunction,
        params,
        params,
        currentTemplate.fileName,
        setLoading,
      );
    } catch (error) {
      console.error("Error exporting Word file:", error);
    }
  };

  const handleExportToExcel = async () => {
    try {
      const params = {
        templateId: tabValue,
        documentType: PRINT_DOCUMENT_TYPE.WORD,
        ...externalConfig?.params,
      }
      await functionExportToExcel(
        excelApiFunction,
        params,
        currentTemplate.fileName,
        setLoading,
      );
    } catch (error) {
      console.error("Error exporting Word file:", error);
    }
  }

  const handleGetTemplateById = async (apiFunc, params, cachedName) => {
    try {
      setLoading(true);
      let res = await apiFunc(params);

      // check nếu lỗi parse thì có thể parse sang json để lấy message
      if (!isSuccessfulResponse(res?.status)) {
        return toast.error(t("general.error"));
      }

      try {
        let newData = new Blob([res?.data], { type: "application/json" });
        let data = JSON.parse(await newData.text());
        if (!isSuccessfulResponse(data?.code)) {
          handleThrowResponseMessage({ data });
        }
      } catch (e) {
        let blob = new Blob([res?.data], {
          type: currentTemplate?.contentType || "application/pdf",
        });
        const url = URL.createObjectURL(blob);
        handleCacheTemplate(url, cachedName);
        handleTongglePreviewButton();
        setTemplate(url);
      }
    } catch (e) {
      console.error(e);
      toast.error(t("general.error"));
    } finally {
      setLoading(false);
    }
  }

  const handleCacheTemplate = (value, name) => {
    const itemCached = (template) => template[currentTemplate?.id] || {};

    setTemplateCached(prevState => ({
      ...prevState,
      [currentTemplate?.id]: {
        ...(itemCached(prevState)),
        [name]: value
      }
    }))
  };

  const handleGetPreviewDetail = async () => {
    try {
      if (Boolean(templateCached[currentTemplate?.id]?.previewDetail)) {
        handleTongglePreviewButton();
        return setTemplate(templateCached[currentTemplate?.id]?.previewDetail);
      }

      let params = {
        templateId: tabValue,
        documentType: PRINT_DOCUMENT_TYPE.PDF,
        ...externalConfig?.params,
      }

      await handleGetTemplateById(
        templateApiFunction,
        params,
        variable.listInputName.previewDetail,
      );
    } catch (e) {
      console.error(e);
    } finally {
    }
  }

  const handleTongglePreviewButton = () => {
    setDisplay(prevState => ({
      ...prevState,
      previewTemplate: !prevState.previewTemplate,
      previewDetail: !prevState.previewDetail,
    }))
  }

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={props?.maxWidth || "xl"}
      fullWidth
    >
      <ValidatorForm ref={form} className="validator-form-scroll-dialog" onSubmit={() => {}}>
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{title || "In phiếu"}</span>
        </DialogTitle>
        <Divider />
        <DialogContent className="p-0" style={{minHeight: "70vh"}}>
          <Grid container>
            <Grid item xs={2} className="bg-white">
              <Tabs
                className="tabs-container"
                orientation="vertical"
                variant="scrollable"
                value={tabValue}
                onChange={handleChangeTabValue}
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                {templates?.map((item, index) => {
                  return (
                    <Tab key={item.id} label={item.name} {...a11yProps(index)} value={item.id} />
                  )
                })}
              </Tabs>
            </Grid>
            <Grid item xs={10} >
              <Scrollbar
                className="m-10 position-relative"
              >
                <FadeLayout
                  show={loading}
                  fullSize
                  blur
                  justifyContent="center"
                  alignItems="center"
                  color="primary"
                >
                  <CircularProgress color="inherit" />
                </FadeLayout>
                {templates?.map((item, index) => (
                  <TabPanel index={item.id} value={tabValue} boxProps={{ className: "p-0" }}>
                    <iframe
                      style={{ minHeight: "71vh", maxHeight: "71vh", }}
                      id="vsxas"
                      src={template}
                      width="100%"
                      height="100%"
                      title="PDF Viewer"
                    />
                  </TabPanel>
                ))}
              </Scrollbar>
            </Grid>
          </Grid>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Grid container spacing={2} justifyContent="center">
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleClose}
              >
                {t("general.cancel")}
              </Button>
            </Grid>
            {display.previewDetail && (
              <Grid item>
                <Button variant="contained" color="primary" onClick={handleGetPreviewDetail}>
                  {t("general.preview")}
                </Button>
              </Grid>
            )}
            {display.previewTemplate && (
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleGetResourceTemplatePreview}
                >
                  {t("general.previewTemplate")}
                </Button>
              </Grid>
            )}
            {display.excelButton && (
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleExportToExcel}
                >
                  {t("general.exportToExcel")}
                </Button>
              </Grid>
            )}
            {display.wordButton && (
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handlePrintWord}
                >
                  {t("general.word")}
                </Button>
              </Grid>
            )}
          </Grid>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

PrintPreviewTemplateDialog.propTypes = {
  t: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  item: PropTypes.shape({
    id: PropTypes.string,
  }),
  externalConfig: PropTypes.shape({
    params: PropTypes.object,
    urlWord: PropTypes.object,
  })
};
