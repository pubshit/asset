import {
  Grid,
  IconButton,
  Icon,
  Button,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Slider,
} from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import React, { Component } from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import ReactDOM from "react-dom";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import SelectDepartmentPopup from "../Department/SelectDepartmentPopup";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { searchByPage } from "../../Asset/AssetService";
import { getListManagementDepartmentOrg } from "../../Asset/AssetService";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const marks = [
  {
    value: 0,
    label: "0",
  },
  {
    value: 500,
    label: "500 tr",
  },
  {
    value: 1000,
    label: "1 tỷ",
  },
  {
    value: 2000,
    label: "2 tỷ",
  },
  {
    value: 3000,
    label: "3 tỷ",
  },
  {
    value: 4000,
    label: "4 tỷ",
  },
  {
    value: 5000,
    label: "5 tỷ",
  },
  {
    value: 6000,
    label: "6 tỷ",
  },
  {
    value: 7000,
    label: "7 tỷ",
  },
  {
    value: 8000,
    label: "8 tỷ",
  },
  {
    value: 9000,
    label: "9 tỷ",
  },
];
class ReportSearchComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenSelectDepartmentPopup: false,
    assetList: [],
    rangerValue : 500 
  };
  yearPutIntoUse = Array.from(new Array(20), (val, index) => (new Date()).getFullYear() - index);
  componentDidMount() {
  }

  componentWillMount() {
  }
  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    });
  };

  handleSelectUseDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    });
  };
  handleComponentSelectUseDepartment = (item) => {
    let useDepartment = { id: item.id, name: item.text, text: item.text };
    this.setState({ useDepartment: useDepartment }, function () {
      this.handleSelectUseDepartmentPopupClose();
    });
  };
  handleChangeSlider = (event, newValue) => {
    if (newValue >= 10000) {
      newValue = 10000;
    }
    this.setState({ rangerValue: newValue }, () => {
      // this.search();
    });
  };
  render() {
    const {
      t,
      i18n,
      handleSelectDepartment,
      selectedItem,
      open,
      handleSelectUseDepartment,
      searchObjectUseDepartment
    } = this.props;
    let {
      shouldOpenSelectDepartmentPopup,
      rangerValue,
      useDepartment
    } = this.state;
    return (
      <>
        <Grid container spacing={1}>
          <Grid item md={6} sm={6} xs={12}>
            <FormControl fullWidth={true}>
              <InputLabel htmlFor="yearPutIntoUse">
                {t("Asset.yearPutIntoUse")}
              </InputLabel>
              <Select
                isClearable={true}
                onChange={(yearPutIntoUse) =>
                  this.props.handleChangeYearPutIntoUse(yearPutIntoUse, "yearPutIntoUse")
                }
                inputProps={{
                  name: "yearPutIntoUse",
                  id: "yearPutIntoUse",
                }}
              >
                <MenuItem key={0} value={0}>
                  {"Tất cả"}
                </MenuItem>
                {this.yearPutIntoUse.map((item) => {
                  return (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          {/* <Grid item md={5} sm={6} xs={12}>
            <TextField
              size="small"
              InputProps={{
                readOnly: true,
                shrink: true
              }}
              fullWidth
              name="useDepartment"
              label={<span> {t("Asset.useDepartment")} </span>}
              className="font-weight-bold mt-3"
              value={useDepartment?.name || ""}
            />
          </Grid>
          <Grid item md={1} sm={6} xs={12}>
            <Button
              size="small"
              className="float-right mt-10"
              variant="contained"
              color="primary"
              onClick={this.openSelectDepartmentPopup}
            >
              {t("general.select")}
            </Button>
            {shouldOpenSelectDepartmentPopup && (
              <SelectDepartmentPopup
                open={shouldOpenSelectDepartmentPopup}
                handleSelect={(value) => {
                  handleSelectDepartment(value);
                  this.handleComponentSelectUseDepartment(value);
                }}
                selectedItem={useDepartment != null
                    ? useDepartment : {}
                }
                handleClose={() => { this.handleSelectUseDepartmentPopupClose() }}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid> */}
          <Grid item md={6} sm={6} xs={12}>
            <ValidatorForm>
              <AsynchronousAutocompleteSub
                label={t("Asset.useDepartment")}
                searchFunction={getListManagementDepartmentOrg}
                // multiple={true}
                searchObject={searchObjectUseDepartment}
                typeReturnFunction={"category"}
                defaultValue={useDepartment}
                displayLable={"name"}
                value={useDepartment}
                onSelect={handleSelectUseDepartment}
              />
            </ValidatorForm>
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item md={8} sm={12} xs={12}>
            <Slider
              className="mt-12"
              track={false}
              value={rangerValue}
              onChange={this.handleChangeSlider}
              valueLabelDisplay="auto"
              aria-labelledby="discrete-slider-custom"
              max={10000}
              step={100}
              valueLabelFormat={this.valueLabelFormat}
              marks={marks}
            />
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <Button
              className="mt-12 mr-12"
              variant="contained"
              size="small"
              color="primary"
              onClick={() => {
                  this.props.searchOriginalPrice(rangerValue, "less_than_or_equal_to");
              }}
            >
              {Math.floor(rangerValue / 1000) > 0
                ? `Nhỏ hơn ${rangerValue / 1000} tỷ`
                : `Nhỏ hơn ${rangerValue} tr`}
            </Button>
            <Button
              className="mt-12"
              variant="contained"
              color="primary"
              size="small"
              onClick={() => { this.props.searchOriginalPrice(rangerValue,"greater_than_or_equal_to");
              }}
            >
              {Math.floor(rangerValue / 1000) > 0
                ? `Lớn hơn ${rangerValue / 1000} tỷ`
                : `Lớn hơn ${rangerValue} tr`}
            </Button>
          </Grid>
        </Grid>
      </>
    );
  }
}
export default ReportSearchComponent;
