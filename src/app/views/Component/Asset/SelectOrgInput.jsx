import {
  Grid,
  MuiThemeProvider,
  TextField,
  InputAdornment,
  Input,
  Button,
  TableHead,
  TableCell,
  TableRow,
  Checkbox,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import React, { Component } from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import ReactDOM from "react-dom";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import SelectDepartmentPopup from "../Department/SelectDepartmentPopup";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { searchByPage } from "../../Asset/AssetService";
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectOrgInput extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenSelectDepartmentPopup: false,
    assetList: [],
  };
  componentDidMount() {
  }

  componentWillMount() {
  }
  openSelectDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: true,
    });
  };

  handleSelectUseDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    });
  };
  handleSelectUseDepartment = (item) => {
    let useDepartment = { id: item.id, name: item.text, text: item.text };
    this.setState({ useDepartment: useDepartment }, function () {
      this.handleSelectUseDepartmentPopupClose();
    });
  };
  // handleSelectDepartment = (item) => {
  //   if (Object.keys(item).length > 0) {
  //     let { userDepartments } = this.state;
  //     let department = { id: item.id, name: item.text, text: item.text };
  //     if (userDepartments.map((el) => el.department.id).indexOf(item.id) < 0) {
  //       userDepartments.push({ department, isMainDepartment: false });
  //     }
  //     this.setState({ userDepartments });
  //     // this.setState({ department }, function () {
  //     // });
  //   }
  //   this.handleSelectDepartmentPopupClose();
  // };
  render() {
    const {
      t,
      i18n,
      useDepartment,
      handleSelectDepartment,
      selectedItem,
      open,
    } = this.props;
    let {
      shouldOpenSelectDepartmentPopup,
    } = this.state;
    return (
      <>
        

        <TextField
          size="small"
          InputProps={{
            readOnly: true,
            shrink: true
          }}
          fullWidth
          name="useDepartment"
          label={<span> {t("Asset.useDepartment")} </span>}
          style={{ width: "80%", fontWeight: "bold" }}
          value={useDepartment?.name ||""}
        />
        <Button
          size="small"
          // style={{ float: "right", transform: "translateY(3px)" }}
          style={{ width: "20%"}}

          className=" mt-10"
          variant="contained"
          color="primary"
          onClick={this.openSelectDepartmentPopup}
        >
          {t("general.select")}
        </Button>

        {shouldOpenSelectDepartmentPopup && (
          <SelectDepartmentPopup
            open={shouldOpenSelectDepartmentPopup}
            handleSelect={(value) => {
              handleSelectDepartment(value);
              this.handleSelectUseDepartmentPopupClose();
            }}
            selectedItem={useDepartment != null
              ? useDepartment : {}
            }
            handleClose={() => { this.handleSelectUseDepartmentPopupClose() }}
            t={t}
            i18n={i18n}
          />
        )}
      </>
    );
  }
}
export default SelectOrgInput;
