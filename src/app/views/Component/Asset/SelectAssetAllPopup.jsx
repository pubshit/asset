import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  Grid,
  Input,
  InputAdornment,
  Radio,
  TablePagination,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import SearchIcon from "@material-ui/icons/Search";
import { appConst, STATUS_STORE, variable } from "app/appConst";
import {
	convertNumberPrice,
	defaultPaginationProps,
	filterOptions,
	formatDateDto, formatTimestampToDate,
	getTheHighestRole,
	getUserInformation,
	handleThrowResponseMessage,
	isSuccessfulResponse,
} from "app/appFunction";
import { getAssetsMaintainProposal } from "app/views/MaintainPlaning/MaintainPlaningService";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React from "react";
import {
  getAssetsByStore,
  getListAssetReevaluate,
  searchByPage,
  searchByPageAsset,
  searchByPageLiquidation,
} from "../../Asset/AssetService";
import {
  getListAssetByOrgNotInPhieuDx
} from "../../AccreditationPlan/AccreditationService";
import {
  searchProductByPage
} from "../../VerificationCalibration/VerificationCalibrationService";
import {
  searchByPage as SearchByPageDepartment,
  searchByPageDepartmentNew,
} from "../../Department/DepartmentService";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import { getListWarehouseByDepartmentId as getListWarehouse } from "app/views/AssetTransfer/AssetTransferService";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { PaperComponent } from "../Utilities";
import PropTypes from "prop-types";
import { searchByPageFixedAsset } from "../../RecallSlip/RecallSlipService";
import { getMedicalEquipment } from "../../Asset/AssetService";
import { searchByPage as searchByPageStore } from "../../Store/StoreService";

class SelectAssetAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    isRoleAssetManager: false,
    assetList: [],
    listDepartment: [],
    listWarehouse: [],
    department: null,
    warehouse: null,
    selectedValue: "",
    medicalEquipment: null,
    store: null
  };
  filterAutocomplete = createFilterOptions();
  searchObjectWarehouse = {
    pageIndex: 1,
    pageSize: 1000000,
    isAssetManagement: true,
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      if (this.props.isMaintenanceProposalDialog) {
        this.getDataAssetMaintenance();
      } else {
        this.updatePageData();
      }
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      if (this.props.isMaintenanceProposalDialog) {
        this.getDataAssetMaintenance();
      } else {
        this.updatePageData();
      }
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };
  getDataAssetMaintenance = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { handoverDepartment, receiverDepartmentId, assetVouchers } = this.state;
    try {
      setPageLoading(true);
      const searchObject = {
        keyword: this.state.keyword,
        pageIndex: this.state.page + 1,
        pageSize: this.state.rowsPerPage,
        statusIndexOrders: this.props.statusIndexOrders,
      };
      searchObject.dxPhongBanId = handoverDepartment?.id;
      if (handoverDepartment?.id === receiverDepartmentId) {
        searchObject.dxPhongBanId = null;
      }
      searchObject.tnPhongBanId = receiverDepartmentId;

      const res = await getAssetsMaintainProposal(searchObject);
      const { data, code } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        //eslint-disable-next-line
        data?.content?.map((item) => {
          let existItem = assetVouchers?.find((x) => x.asset.id === item.id);
          item.isCheck = !!existItem;
          return item;
        });
        this.setState({
          itemList: data?.content || [],
          totalElements: data?.totalElements ?? 0,
        });
      } else {
        handleThrowResponseMessage(res)
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let status = 0;
    let searchObject = {};
    setPageLoading(true);
    searchObject.voucherType = this.props.voucherType;
    searchObject.storeId = this.state.store?.id
    searchObject.medicalEquipmentId = this.state.medicalEquipment?.id
    searchObject.usageState = this.state.usageState?.id
    if (this.props.isRevocationStream) {
      searchObject.isRevocationStream = this.props.isRevocationStream;
    }
    if (
      this.props.isAssetTransfer ||
      this.props.isFilterUseDepartment ||
      this.props.type === variable.listInputName.assetReevaluate
    ) {
      searchObject.useDepartmentId = this.state.department?.id;
    } else {
      searchObject.useDepartmentId = this.props.departmentId;
    }
    searchObject.managementDepartmentId = this.props.handoverDepartment
      ? this.props.handoverDepartment.id
      : null;
    if (this.props.isFilterManagementDepartment) {
      searchObject.managementDepartmentId = this.state.department?.id;
    }
    if (this.props.isAssetTransfer) {
      searchObject.statusIndexOrders = this.props.statusIndexOrders;
    } else if (this.props.isAssetAllocation) {
      searchObject.statusIndexOrders = this.props.statusIndexOrders;
      searchObject.isSearchForAllocation = this.props.isSearchForAllocation;
      searchObject.storeId = this.props.storeId;
    } else if (this.props.isGetAll) {
    } else if (this.props.statusIndexOrders) {
      searchObject.statusIndexOrders = this.props.statusIndexOrders;
    } else {
      searchObject.indexOrder = status;
    }

    if (this.props.type === variable.listInputName.isAccreditation && this.props.amDexuatIds) {
      searchObject.amDexuatIds = [this.props.amDexuatIds]
    }
    if (this.props.ngayChuyenKho) {
      searchObject.ngayChuyenKho = formatDateDto(this.props.ngayChuyenKho)
    }
    if (this.props.isSearchForTransfer) {
      searchObject.isSearchForTransfer = this.props.isSearchForTransfer
    }
    if (this.props.isNotUseDepartment) {
      searchObject.notUseDepartmentId = this.props.isNotUseDepartment;
      searchObject.isManagedByDepartment = true;
    }
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.issueDateTop = this.props.issueDateTop
      ? formatDateDto(this.props.issueDateTop)
      : null;
    searchObject.dateOfReceptionTop = this.props.issueDate
      ? moment(this.props.issueDate).format("YYYY/MM/DD")
      : null;
    searchObject.dateOfReceptionTop = this.props.dateOfReceptionTop
      ? formatDateDto(this.props.dateOfReceptionTop)
      : null;

    try {
      let res;
      if (this.props.type === variable.listInputName.assetReevaluate) {
        searchObject.storeId = this.state.warehouse?.id;
        res = await getListAssetReevaluate(searchObject);
      } else if (
        this.props.type === variable.listInputName.warehouseTransfer
      ) {
        searchObject.storeId = this.props?.storeId;
        searchObject.assetClass = this.props.assetClass;
        res = await getAssetsByStore(searchObject);
      } else if (this.props.type === variable.listInputName.isAccreditation) {
        res = await getListAssetByOrgNotInPhieuDx(searchObject);
      } else if (this.props.type === variable.listInputName.isVerification) {
        res = await searchProductByPage(searchObject);
      } else if (this.props.isRevocationStream) {
        res = await searchByPageFixedAsset(searchObject);
      } else if (this.props.liquidation) {
        res = await searchByPageLiquidation(searchObject)
      } else if (this.props.isNotUseDepartment) {
        res = await searchByPageLiquidation(searchObject)
      } else if (this.props.isRecallSlip) {
        res = await searchByPageAsset(searchObject);
      } else {
        res = await searchByPage(searchObject);

      }

      if (res?.data) {
        let data = res?.data;
        
        if(!data?.data?.content?.length && data?.data?.totalPages) {
          this.search();
          return;
        }
        
        let itemListClone = [...data?.data?.content];
        itemListClone.map((item) => {
          item.carryingAmount = Math.ceil(item.carryingAmount);
          item.isCheck = false;
          if (this.state.assetVouchers && this.state.assetVouchers.length > 0) {
            this.state.assetVouchers.forEach((assetVoucher) => {
              if ((assetVoucher?.asset?.id || assetVoucher?.id) === item.id) {
                item.isCheck = true;
              }
            });
          }
          if (this.props.assetVouchers && this.props.assetVouchers.length > 0) {
            this.props.assetVouchers.forEach((assetVoucher) => {
              if ((assetVoucher?.asset?.assetId || assetVoucher?.assetId) === item.id) {
                item.isCheck = true;
              }
            });
          }
          return item;
        });
        this.setState(
          {
            itemList: [...itemListClone],
            totalElements: data?.data?.totalElements,
          },
          () => { }
        );
      }
      setPageLoading(false);
    } catch (e) {
      setPageLoading(false);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  getListWarehouse = async () => {
    let { t } = this.props;
    try {
      let res = await getListWarehouse(this.searchObjectWarehouse);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.content) {
        this.setState({
          listWarehouse: res.data.content,
        });
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    }
  };

  componentDidMount() {
    if (this.props.dataAsset && this.props.dataAsset?.tsId) {
      this.setState({
        selectedValue: this.props.dataAsset?.tsId,
      });
    }
    if (this.props.isMaintenanceProposalDialog) {
      this.getDataAssetMaintenance();
    } else {
      this.updatePageData();
    }
    if (this.props.type === variable.listInputName.assetReevaluate) {
      this.getListWarehouse();
    }
    // if () {

    // }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.dataAsset?.tsId && this.state.itemList !== prevState.itemList) {
      let item = this.state.itemList.find(
        (item) => item.id === this.props.dataAsset?.tsId
      );
      if (item) {
        this.setState({
          assetVouchers: [{ asset: item }],
        });
      }
    }
  }

  handleClick = (event, item) => {
    if (this.props.isOneSelect) {
      if (item?.id) {
        this.setState({
          selectedValue: item?.id,
          assetVouchers: [{ asset: item }],
        });
      } else {
        this.setState({ selectedValue: null, assetVouchers: [] });
      }
      return;
    }

    document.querySelector(`#radio${item.id}`).click();
    item.isCheck = !event.target.checked;
    let { assetVouchers } = this.state;
    assetVouchers = assetVouchers ? assetVouchers : [];
    if (item.isCheck === true) {
      let check = assetVouchers.filter(
        (assetVoucher) => (assetVoucher?.asset?.id || assetVoucher?.id) === item.id
      );
      if (check.length === 0) {
        let p = {};
        p = { asset: item };
        assetVouchers = assetVouchers.concat(p);
      }
    }
    if (item.isCheck === false) {
      assetVouchers = assetVouchers.filter(
        (assetVoucher) =>
          (assetVoucher?.asset?.id || assetVoucher?.id) !== item.id
      );
    }
    this.setState({ assetVouchers: assetVouchers }, function () { });
  };

  async componentWillMount() {
    let {
      assetVouchers,
      handoverDepartment,
      receiverDepartmentId,
      managementDepartmentId,
    } = this.props;
    let roles = getTheHighestRole();

    if (this.props.type === variable.listInputName.assetReevaluate) {
      let searchObjectDepartment = { pageIndex: 1, pageSize: 9999 };
      try {
        let res = await SearchByPageDepartment(searchObjectDepartment);
        if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
          this.handleSetListDepartment(res?.data?.content);
        }
      } catch (e) { }
    }
    if (this.props.isFilterManagementDepartment) {
      this.handleGetListManagementDepartment();
    }

    this.setState({
      assetVouchers: assetVouchers,
      handoverDepartment,
      receiverDepartmentId,
      managementDepartmentId,
      isMaintenanceProposalDialog: false,
      ...roles,
    });
  }

  search = () => {
    this.setPage(0);
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
    if (this.props.isMaintenanceProposalDialog) {
      this.getDataAssetMaintenance();
    } else {
      this.updatePageData();
    }
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleSetListDepartment = (listDepartment) => {
    this.setState({ listDepartment: listDepartment ? listDepartment : [] });
  };

  handleSelectDepartment = (department) => {
    this.setState({ department: department ? department : null }, () => {
      this.updatePageData();
    });
  };

  handleSelectWarehouse = (warehouse) => {
    this.setState({ warehouse: warehouse ? warehouse : null }, () => {
      this.updatePageData();
    });
  };

  handleSelect = (value, source) => {
    this.setState({
      [source]: value
    }, this.updatePageData)
  }

  handleGetListManagementDepartment = async () => {
    let { t } = this.props;
    try {
      const { organization } = getUserInformation();
      let departmentSearchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        isAssetManagement: true,
        orgId: organization?.org?.id,
      };
      const result = await searchByPageDepartmentNew(departmentSearchObject)
      if (result?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listDepartment: result?.data?.data?.content || []
        })
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
    }
  };
  render() {
    const {
      t,
      open,
      handleClose,
      handleSelect,
      isAssetTransfer,
      isMaintenanceProposalDialog,
      handoverDepartment,
      isNotUseDepartment = false,
      isLiquidate,
      isWarehouseAssetTransfer,
      isFilterUseDepartment,
    } = this.props;
    let {
      keyword,
      department,
      isRoleUser,
      assetVouchers,
      isRoleAssetUser,
      store,
      medicalEquipment,
      usageState
    } = this.state;

    let searchObjectDepartment = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
    let searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      managementDepartmentId: handoverDepartment?.id,
      isActive: STATUS_STORE.HOAT_DONG.code
    };
    let searchObjectUnit = { ...appConst.OBJECT_SEARCH_MAX_SIZE }

    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        maxWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          !this.props.isOneSelect ? (
            <Checkbox
              id={`radio${rowData.id}`}
              style={{ padding: "0px", paddingLeft: "8px" }}
              name="radSelected"
              value={rowData.id}
              checked={rowData.isCheck}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          ) : (
            <Radio
              id={`radio${rowData.id}`}
              style={{ padding: "0px" }}
              name="radSelected"
              value={rowData.id}
              checked={this.state.selectedValue === rowData.id}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          ),
      },
      {
        title: t("Asset.code"),
        field: "code",
        minWidth: 120,
        align: "right",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.code || rowData?.tsMa
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: 200,
        render: (rowData) => rowData?.name || rowData?.tsTen
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.serialNumber || rowData?.tsSerialNo
      },
      {
        title: t("Asset.model"),
        field: "model",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.model || rowData?.tsModel
      },
      {
        title: this.props.isAssetAllocation || this.props.isRecallSlip
          ? t("Asset.dateOfReception")
          : t("Asset.yearIntoUseTable"),
        field: "dateOfReception",
        align: "center",
        hidden: isMaintenanceProposalDialog,
        minWidth: 120,
        render: (rowData) =>
          this.props.isAssetAllocation || this.props.isRecallSlip
            ? rowData.dateOfReception
              ? moment(rowData.dateOfReception).format("DD-MM-YYYY")
              : null
            : rowData.yearPutIntoUse || "",
      },
      {
        title: t("Asset.yearOfManufactureTable"),
        field: "asset.yearOfManufacture",
        align: "left",
        hidden: !isMaintenanceProposalDialog,
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        minWidth: 200,
        hidden: !isMaintenanceProposalDialog || isWarehouseAssetTransfer,
        render: (rowData) =>
          rowData?.useDepartment?.name
            ? rowData?.useDepartment?.name
            : rowData?.useDepartmentName
      },
      {
        title: t("Asset.originalCost"),
        field: "asset.originalCost",
        align: "left",
        hidden: isMaintenanceProposalDialog,
        minWidth: 180,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData.originalCost),
      },
      {
        title: t("Asset.carryingAmount"),
        field: "asset.carryingAmount",
        align: "left",
        hidden: isMaintenanceProposalDialog,
        minWidth: 180,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData.carryingAmount),
      },
      {
        title: t("Asset.store"),
        hidden: !isNotUseDepartment,
        field: "storeName",
        align: "left",
        minWidth: 180,
      },
      {
        title: t("Asset.usingStatus"),
        hidden: isMaintenanceProposalDialog || isWarehouseAssetTransfer,
        field: "statusName",
        align: "left",
        minWidth: 180,
      },
    ];

    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move", paddingBottom: 0 }} id="draggable-dialog-title">
          <span>{t("component.asset.title")}</span>
        </DialogTitle>
        <ValidatorForm onSubmit={() => { }}>
          <DialogContent>
            <Grid container spacing={2} className="mb-16" justifyContent="flex-end">
              <Grid item md={8} sm={6} xs={12}>
                <Input
                  label={t("general.enterSearch")}
                  type="text"
                  name="keyword"
                  value={keyword}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  className="w-100 mt-16"
                  id="search_box"
                  placeholder={t("Asset.enterSearchPopup")}
                  startAdornment={
                    <InputAdornment position="end">
                      {" "}
                      <SearchIcon
                        onClick={() => this.search(keyword)}
                        style={{
                          position: "absolute",
                          top: "0",
                          right: "0",
                        }}
                      />
                    </InputAdornment>
                  }
                />
              </Grid>
              {isLiquidate && (!isRoleUser || !isRoleAssetUser) && <Grid item md={4} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  searchFunction={() => { }}
                  searchObject={{}}
                  className="w-100"
                  label={t("MedicalEquipmentType.status")}
                  listData={appConst.typeOfWarehouseAssets || []}
                  value={usageState ? usageState : null}
                  displayLable={'name'}
                  onSelect={(value) => this.handleSelect(value, "usageState")}
                />
              </Grid>}
              {(isAssetTransfer && (!isRoleUser || !isRoleAssetUser) || isFilterUseDepartment) && (
                <Grid item md={4} sm={4} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("Asset.useDepartment")}
                    searchFunction={SearchByPageDepartment}
                    searchObject={searchObjectDepartment}
                    listData={this.state.listDepartment}
                    setListData={this.handleSetListDepartment}
                    defaultValue={department ? department : null}
                    displayLable={"text"}
                    value={department ? department : null}
                    onSelect={this.handleSelectDepartment}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              )}
              {this.props.isFilterManagementDepartment && <Grid item md={4} sm={4} xs={12}>
                <Autocomplete
                  id="combo-box-reception-department"
                  fullWidth
                  size="small"
                  name="ManagementDepartment"
                  options={this.state.listDepartment}
                  onChange={(event, value) =>
                    this.handleSelectDepartment(value)
                  }
                  getOptionLabel={(option) => option.name}
                  value={department ? department : null}
                  filterOptions={filterOptions}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      label={
                        <>
                          <span>{t("Asset.managementDepartment")}</span>
                        </>
                      }
                      variant="standard"
                      value={department?.name ? department?.name : null}
                      inputProps={{
                        ...params.inputProps,
                      }}
                      placeholder="Chọn phòng ban"
                    />
                  )}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>}
              {(isAssetTransfer || isNotUseDepartment) && (!isRoleUser || !isRoleAssetUser) && <Grid item md={4} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span> {t("receipt.store")}</span>
                    </span>
                  }
                  searchFunction={searchByPageStore}
                  searchObject={searchObjectStore}
                  displayLable={'name'}
                  value={store ? store : null}
                  onSelect={(value) => this.handleSelect(value, "store")}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>}
              {isAssetTransfer && (!isRoleUser || !isRoleAssetUser) && (
                <Grid item xs={12} sm={12} md={4}>
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={t("MedicalEquipmentType.title")}
                    searchFunction={getMedicalEquipment}
                    searchObject={searchObjectUnit}
                    typeReturnFunction="category"
                    displayLable={"name"}
                    value={medicalEquipment ? medicalEquipment : null}
                    onSelect={(value) => this.handleSelect(value, "medicalEquipment")}
                    filterOptions={filterOptions}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
              )}
            </Grid>
            <Grid >
              <MaterialTable
                data={this.state.itemList}
                columns={columns}
                onRowClick={
                  this.props.isOneSelect
                    ? (evt, selectedRow) => this.onClickRow(selectedRow)
                    : null
                }
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  sorting: false,
                  paging: false,
                  search: false,
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    paddingRight: 10,
                    paddingLeft: 10,
                    textAlign: "center",
                  },
                  maxBodyHeight: 280,
                  minBodyHeight: 280,
                }}
                components={{
                  Toolbar: (props) => (
                    <div className="w-100">
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
								{...defaultPaginationProps()}
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                count={this.state.totalElements}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </DialogContent>
        </ValidatorForm>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={(event) => handleSelect(assetVouchers)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
SelectAssetAllPopop.contextType = AppContext;
SelectAssetAllPopop.propTypes = {
  dateOfReceptionTop: PropTypes.any,
  handoverDepartment: PropTypes.object,
  statusIndexOrders: PropTypes.array,
  isAssetTransfer: PropTypes.bool,
  open: PropTypes.bool.isRequired,
  t: PropTypes.any.isRequired,
  handleSelect: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  assetVouchers: PropTypes.array.isRequired,
	isRevocationStream: PropTypes.bool,
	isSearchForTransfer: PropTypes.bool,
	isGetAll: PropTypes.bool,
	liquidation: PropTypes.bool,
	isNotUseDepartment: PropTypes.bool,
	isRecallSlip: PropTypes.bool,
	isFilterUseDepartment: PropTypes.bool,
	isFilterManagementDepartment: PropTypes.bool,
	isAssetAllocation: PropTypes.bool,
	isMaintenanceProposalDialog: PropTypes.bool,
	type: PropTypes.any,
	isOneSelect: PropTypes.any,
	assetClass: PropTypes.number,
	issueDate: PropTypes.any,
	issueDateTop: PropTypes.any,
	voucherType: PropTypes.any,
	ngayChuyenKho: PropTypes.any,
	dataAsset: PropTypes.any,
	departmentId: PropTypes.string,
	amDexuatIds: PropTypes.array,
};
export default SelectAssetAllPopop;
