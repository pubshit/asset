import {
  Grid,
  InputAdornment,
  Input,
  Button,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from "material-table";
import { getListsProductAsset, searchByPage } from "../../Product/ProductService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import ProductDialog from "../../Product/ProductDialog";
import { getNewCode } from "../../Product/ProductService";
import { appConst } from "app/appConst";
import { searchByPage as productTypeSearchByPage } from "../../ProductType/ProductTypeService";
import localStorageService from "app/services/localStorageService";
import { TabPanel } from "app/appFunction";
import AppContext from "app/appContext";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectProductPopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 10,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: this.props.selectedItem,
    keyword: "",
    shouldOpenProductDialog: false,
    item: null,
    productType: {},
    tabValue: 0,

  };

  setPage = (page) => {
    this.setState({ page: page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { isReceivingAsset, productTypeCode, fromAssetDialog } = this.props;
    let { departmentUser, tabValue, decisionCode, supplier } = this.state;
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    if (fromAssetDialog && departmentUser?.id) {
      searchObject.managementPurchaseDepartmentId = departmentUser?.id;
      searchObject.departmentId = departmentUser?.id;
      searchObject.departmentCode = departmentUser?.code;
      searchObject.useDepartmentId = departmentUser?.id;
      searchObject.managementDepartmentId = departmentUser?.id;
      searchObject.isUserDepartmentManagement = fromAssetDialog;
      searchObject.checkPermissionUserDepartment = fromAssetDialog;
    }
    if (isReceivingAsset) {
      searchObject.managementPurchaseDepartmentId = departmentUser?.id
      searchObject.supplierId = supplier?.id
    }
    if (fromAssetDialog || isReceivingAsset) {
      searchObject.decisionCode = decisionCode
    }
    if (productTypeCode && productTypeCode != null) {
      searchObject.productTypeCode = productTypeCode;
    };

    let isTabInside = tabValue === appConst.PRODUCT_BIDDING.INSIDE.code;
    let funSearch = isTabInside
      ? getListsProductAsset
      : searchByPage;

    try {
      setPageLoading(true);
      let res = await funSearch(searchObject);
      if (res?.status === appConst.CODE.SUCCESS) {
        let { data } = res;
        this.setState({
          itemList: isTabInside ? data?.data?.content : data?.content,
          totalElements: isTabInside ? Number(data?.data?.totalElements || 0) : Number(data?.totalElements || 0),
        });
      }

    } catch (error) {
      console.error(error);
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    let departmentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
    if (this.props?.productTypeCode) {
      this.getListProductType()
    }
    if (this.props?.isReceivingAsset && this.props?.decisionCode?.decisionCode) {
      this.setState({
        decisionCode: this.props?.decisionCode?.decisionCode,
        supplier: this.props?.supplyUnit,
        tabValue: appConst.PRODUCT_BIDDING.INSIDE.code
      })
    }

    this.setState({ departmentUser }, () => {
      this.updatePageData();
    });
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: null, selectedItem: null });
    }
  };

  componentWillMount() {
    let { selectedItem } = this.props;
    this.setState({ selectedValue: selectedItem.id });
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  search = () => {
    this.setPage(0);
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = (keySearch = "") => {
    this.setState({
      keyword: keySearch || this.state?.keyword,
      shouldOpenProductDialog: false,
    });
    this.updatePageData();
  };

  selectProduct = (selectedItem) => {
    if (selectedItem?.id != null) {
      this.setState({ selectedValue: selectedItem.id, selectedItem: selectedItem });
    } else {
      this.setState({ selectedValue: null, selectedItem: null });
    }
  }
  getListProductType = async () => {
    let searchObject = { pageIndex: 0, pageSize: 1000000 };
    try {
      let res = await productTypeSearchByPage(searchObject)
      if (res?.data?.content && res?.status === appConst.CODE.SUCCESS) {
        let productTypeCode = res.data.content.find(item => item.code === this.props?.productTypeCode);
        if (productTypeCode) {
          this.setState({
            productType: productTypeCode
          })
        }
      }
    } catch (error) {
    }
  }

  handleAddItem = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result.data && result.data.code) {
          let item = result.data;
          item.name = this.state.keyword;
          if (this.props?.assetClass) {
            item.productType = this.state.productType
          }
          this.setState({
            item: item,
            shouldOpenProductDialog: true,
          });
        }
      })
      .catch(() => {
        alert(this.props.t("general.error_reload_page"));
      });
  };

  onClickRow = (selectedRow) => {
    if (selectedRow?.id != null) {
      this.setState({ selectedValue: selectedRow?.id, selectedItem: selectedRow });
    } else {
      this.setState({ selectedValue: null, selectedItem: null });
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({ tabValue: newValue, itemList: [] }, () => {
      this.setPage(0);
    });
  }
  render() {
    const {
      t, i18n,
      handleClose,
      handleSelect,
      open,
      assetClass,
    } = this.props;
    let { keyword, shouldOpenProductDialog, itemList, decisionCode, tabValue } = this.state;
    const productType = assetClass === appConst.assetClass.TSCD
      ? appConst.productTypeCode.TSCĐ
      : assetClass === appConst.assetClass.CCDC
        ? appConst.productTypeCode.CCDC
        : appConst.productTypeCode.VTHH
    const isTabOutside = tabValue === appConst.PRODUCT_BIDDING.OUTSIDE.code;

    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "center",
        maxWidth: 100,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <Radio
            // id={`radio${rowData?.id}`}
            name="radSelected"
            className="p-0"
            value={rowData?.id}
            checked={this.state.selectedItem?.id === rowData?.id && this.state.selectedItem?.supplierId === rowData?.supplierId && this.state.selectedItem?.decisionCode === rowData?.decisionCode}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("Product.code"),
        field: "code",
        align: "left",
        minWidth: 250,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "name",
        align: "left",
        minWidth: 300,
      },

      {
        title: t("Asset.stockKeepingUnit"),
        field: "unitName",
        hidden: isTabOutside,
        align: "center",
        minWidth: 200,
      },
      {
        title: t("BiddingList.code"),
        field: "decisionCode",
        hidden: isTabOutside,
        align: "center",
        minWidth: 200,
      },
      {
        title: t("Product.madeIn"),
        field: "madeIn",
        align: "center",
        minWidth: 120,
      },
      {
        title: t("Product.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "center",
        minWidth: 120,
      },
      {
        title: t("Product.manufacturer"),
        field: "manufacturer.name",
        align: "center",
        minWidth: 150,
        render: rowData => rowData?.manufacturer?.name || rowData?.manufacturerName,
      },
      {
        title: t("Product.supplier"),
        field: "supplierName",
        hidden: isTabOutside,
        align: "left",
        minWidth: 200,
      },
      {
        title: t("InventoryReport.remainingQuantity"),
        field: "remainQuantity",
        hidden: isTabOutside,
        align: "center",
        minWidth: 200,
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          {t("component.product.title")}
        </DialogTitle>
        <DialogContent>
          <AppBar position="static" color="default">
            <Tabs
              className="tabsStatus"
              value={tabValue}
              onChange={this.handleChangeTabValue}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              {
                this.props?.isReceivingAsset
                  ? !decisionCode ? <Tab
                    className="tab"
                    value={appConst.PRODUCT_BIDDING.OUTSIDE.code}
                    label={
                      <div className="tabLable">
                        <span>{appConst.PRODUCT_BIDDING.OUTSIDE.name}</span>
                      </div>
                    }
                  /> : <Tab
                    className="tab"
                    value={appConst.PRODUCT_BIDDING.INSIDE.code}
                    label={
                      <div className="tabLable">
                        <span>{appConst.PRODUCT_BIDDING.INSIDE.name}</span>
                      </div>
                    }
                  />
                  : ([
                    <Tab
                      className="tab"
                      value={appConst.PRODUCT_BIDDING.OUTSIDE.code}
                      label={
                        <div className="tabLable">
                          <span>{appConst.PRODUCT_BIDDING.OUTSIDE.name}</span>
                        </div>
                      }
                    />,
                    <Tab
                      className="tab"
                      value={appConst.PRODUCT_BIDDING.INSIDE.code}
                      label={
                        <div className="tabLable">
                          <span>{appConst.PRODUCT_BIDDING.INSIDE.name}</span>
                        </div>
                      }
                    />
                  ])
              }
            </Tabs>
          </AppBar>
          <Grid item xs={12} className="mt-16">
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyUp={this.handleKeyUp}
              style={{ width: "50%" }}
              onKeyDown={this.handleKeyDownEnterSearch}
              className=" mb-16 mr-10"
              id="search_box"
              placeholder={t("Product.search")}
              startAdornment={
                <InputAdornment>
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
            {itemList?.length <= 0 && (isTabOutside
              && <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                // onClick={() => this.handleOpenProductDialog()}
                onClick={() => this.handleAddItem()}
              >
                {t("Product.add")}
              </Button>)
            }
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenProductDialog && (
                <ProductDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogProductClose}
                  open={shouldOpenProductDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  fromAssetDialog={this.props.fromAssetDialog}
                  item={this.state.item}
                  handleSelect={handleSelect}
                  selectProduct={this.selectProduct}
                  assetClass={this.props?.assetClass}
                  type={this.props.type
                    ? this.props.type
                    : productType
                  }
                />
              )}
            </div>
            <MaterialTable
              data={itemList}
              columns={columns}
              onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
              // parentChildData={(row, rows) => null}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[5, 10, 20, 30]}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={this.state.page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleSelect(this.state.selectedItem)}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

SelectProductPopup.contextType = AppContext;
export default SelectProductPopup;
