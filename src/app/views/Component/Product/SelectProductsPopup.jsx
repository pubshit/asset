import {
  Grid,
  Button,
  InputAdornment,
  Input,
  TablePagination,
  Dialog,
  DialogActions, Checkbox,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";

import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { appConst, STATUS_STORE } from "../../../appConst";
import { toast } from "react-toastify";
import { convertNumberPrice, formatTimestampToDate, handleKeyDown, handleKeyUp } from "app/appFunction";
import AppContext from "app/appContext";
import { getRemainProductIat } from '../../Product/ProductService';
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { getPageBySearchTextAndOrgStore } from "app/views/Store/StoreService";
import PropTypes from "prop-types";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectProductsPopup extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedList: [],
    keyword: "",
    shouldOpenProductDialog: false,
    store: null,
    listStores: []
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  handleSetListChecked = (lists = [], checkedLists = []) => {
    return lists.map(item => {
      let existItem = checkedLists?.find(checkedItem => this.handleFilterItems(checkedItem, item));

      item.isCheck = Boolean(existItem);
      return item;
    })
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    try {
      setPageLoading(true);
      let { selectedList, store } = this.state;
      let searchObject = {};
      searchObject.keyword = this.state.keyword;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.storeId = store?.id;

      let searchFunc = getRemainProductIat;

      let response = await searchFunc(searchObject);

      let itemList = response?.data?.data?.content || [];
      let code = response.data?.code;
      let totalElements = response?.data?.data?.totalElements;

      if (code === appConst.CODE.SUCCESS) {
        let _itemList = this.handleSetListChecked(itemList, selectedList);
        this.setState({
          itemList: _itemList,
          totalElements,
        });
      } else {
        toast.error(t("general.error"));
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleClick = (event, item) => {
    let { selectedList = [], itemList = [] } = this.state;
    let existItem = selectedList?.find(x => this.handleFilterItems(x, item))

    if (existItem) {
      selectedList.splice(selectedList.indexOf(existItem), 1);
    }
    else {
      selectedList.push(item);
    }

    itemList.map(x => {
      if (this.handleFilterItems(x, item)) {
        x.isCheck = !Boolean(existItem);
      }
      return item;
    })

    this.setState({ selectedList, itemList });
  };

  componentWillMount() {
    let { selectedList } = this.props;
    this.setState({ selectedList: selectedList });
  }

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search)

  handleKeyUp = (e) => handleKeyUp(e, this.updatePageData);

  handleChange = (event) => {
    this.setState({
      [event?.target?.name]: event?.target?.value,
    });
  };

  handleSetData = (data, source) => {
    this.setState({ [source]: data }, () => {
      this.updatePageData();
    })
  }

  handleFilterItems = (x, item) => {
    let {filterSelected} = this.props;
    
    if (filterSelected) {
      return filterSelected(x, item);
    } else {
      return x.productId === item.productId
        && x?.inputDate === item?.inputDate
        && x?.storeId === item?.storeId
        && x?.unitPrice === item?.unitPrice
        && x?.lotNumber === item?.lotNumber
        && x?.expiryDate === item?.expiryDate
    }
  }
  
  render() {
    const {
      t,
      handleClose,
      handleSelect,
      open,
    } = this.props;
    let {
      page,
      store,
      keyword,
      itemList,
      listStores,
      rowsPerPage,
      selectedList,
      totalElements,
    } = this.state;

    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "center",
        minWidth: 80,
        cellStyle: {
          padding: 0
        },
        render: (rowData) => (
          <>
            <Checkbox
              id={`checkBox${rowData.productId}`}
              name={rowData.productId}
              value={rowData.productId}
              checked={rowData.isCheck}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          </>
        ),
      },
      {
        title: t("general.stt"),
        field: "",
        align: "center",
        minWidth: 80,
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        field: "productCode",
        align: "center",
        minWidth: 140,
      },
      {
        title: t("Product.inputDate"),
        field: "inputDate",
        align: "center",
        minWidth: 140,
        render: rowData => formatTimestampToDate(rowData?.inputDate)
      },
      {
        title: t("Product.name"),
        field: "productName",
        align: "left",
        minWidth: 450,
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "unitName",
        align: "center",
        minWidth: 140,
      },
      {
        title: t("Product.remainQuantity"),
        field: "remainQuantity",
        align: "center",
        minWidth: 140,
      },
      {
        title: t("Product.batchCode"),
        field: "lotNumber",
        align: "left",
        minWidth: 140,
      },
      {
        title: t("Product.expiryDate"),
        field: "expiryDate",
        align: "center",
        minWidth: 140,
        render: rowData => formatTimestampToDate(rowData?.expiryDate)
      },
      {
        title: t("Product.price"),
        field: "unitPrice",
        minWidth: 180,
        cellStyle: {
          textAlign: "right"
        },
        render: rowData => convertNumberPrice(rowData?.unitPrice)
      },
      {
        title: t("Product.amount"),
        field: "amount",
        align: "right",
        minWidth: 180,
        cellStyle: {
          textAlign: "right"
        },
        render: rowData => convertNumberPrice(rowData?.unitPrice * rowData?.remainQuantity) || 0
      },
      {
        title: t("Asset.store"),
        field: "storeName",
        align: "left",
        minWidth: 350
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move", paddingBottom: 0 }} id="draggable-dialog-title">
          <span>{t("AssetWarehouseTransfer.select_supplies")}</span>
        </DialogTitle>
        <DialogContent>
          <Grid container justifyContent="flex-end" spacing={2}>
            <Grid item md={3} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                label={t("InventoryReceivingVoucher.store")}
                searchFunction={getPageBySearchTextAndOrgStore}
                searchObject={{
                  ...appConst.OBJECT_SEARCH_MAX_SIZE,
                  isActive: STATUS_STORE.HOAT_DONG.code,
                }}
                listData={listStores}
                setListData={(listStores) => this.setState({ listStores })}
                displayLable={"name"}
                value={store ? store : null}
                onSelect={(value) => this.handleSetData(value, "store")}
                className="w-100"
              />
            </Grid>
            <Grid item md={6} sm={12} xs={12}>
              <Input
                label={t("general.enterSearch")}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyUp={this.handleKeyUp}
                onKeyDown={this.handleKeyDownEnterSearch}
                className="w-100 mt-15"
                id="search_box"
                placeholder={t("general.enterSearch")}
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={() => this.search()} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={12}>
              <MaterialTable
                data={itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  },
                }}
                options={{
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: "273px",
                  minBodyHeight: "273px",
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                  },
                  padding: "dense",
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ width: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
              <TablePagination
                align="left"
                className="px-16"
                rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
                component="div"
                count={totalElements}
                rowsPerPage={rowsPerPage}
                page={page}
                labelRowsPerPage={t("general.rows_per_page")}
                labelDisplayedRows={({ from, to, count }) =>
                  `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                  }`
                }
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                }}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="pt-0">
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mb-16 mr-12"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mb-16 mr-16"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(selectedList)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}

SelectProductsPopup.contextType = AppContext;
SelectProductsPopup.propTypes = {
  t: PropTypes.func,
  open: PropTypes.bool,
  filterSelected: PropTypes.func,
  handleClose: PropTypes.func,
  selectedList: PropTypes.array,
  handleSelect: PropTypes.func,
}
export default SelectProductsPopup;

