import {
  Grid,
  InputAdornment,
  Input,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  FormControlLabel,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  getListRemainingQuantity,
  getRemainProduct,
  searchByPage,
  searchProductInVoucherByPage,
} from "../../Product/ProductService";
import ConstantList from "../../../appConfig";
import {
  PaperComponent,
  convertNumberPrice,
  formatTimestampToDate,
  getRole,
  removeAccents,
  handleKeyDown, handleKeyUp, formatDateDto, convertMoney
} from "app/appFunction";
import ProductDialog from "app/views/Product/ProductDialog";
import { getNewCode } from "app/views/Product/ProductService";
import { appConst } from "app/appConst";
import { toast } from "react-toastify";
import AppContext from "../../../appContext";

class SelectProductAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    products: [],
    voucherType: null,
    voucherId: null,
    productTypeCode: "",
    storeId: "",
    isGetAll: false,
    planingDepartment: null,
    openProductDialog: false,
    itemProduct: null,
    loading: false,
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  componentDidMount() {
    this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    item.isCheck = event.target.checked;
    let { products = [] } = this.state;
    if (products?.length === 0 && item.isCheck === true) {
      let p = {};
      p.Product = item;

      products.push(p);
    } else {
      let itemInList = false;

      products?.forEach((el) => {
        if (el.Product.id === item.id) {
          itemInList = true;
        }
      });
      if (!itemInList && item.isCheck) {
        let p = {};
        p.Product = item;

        products.push(p);
      } else {
        products.forEach((item) => {
          if (item.Product.isCheck) {
            let index = products.indexOf(item);
            products.splice(index, 1);
          }
        });
      }
    }
    this.setState({ products: products });
  };

  componentWillMount() {
    let {
      products,
      productTypeCode,
      voucherType,
      voucherId,
      storeId,
      planingDepartment,
      assetVouchers,
    } = this.props;
    this.setState({
      products,
      productTypeCode,
      voucherType,
      voucherId,
      storeId,
      planingDepartment,
      assetVouchers,
    });
  }

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  search = () => {
    this.setState({ page: 0 }, async () => {
      let {
        t,
        planingDepartment,
        isManagementPurchaseDepartment,
        managementPurchaseDepartment,
        date,
      } = this.props;
      let { setPageLoading } = this.context;

      try {
        setPageLoading(true);
        let searchObject = {};
        let planingDepartmentClone = {};
        if (planingDepartment) {
          if (Object.keys(planingDepartment).length > 0) {
            planingDepartmentClone = { ...planingDepartment };
          }
        }

        searchObject.storeId = this.state.storeId;
        searchObject.productTypeCode = this.state.productTypeCode;
        searchObject.voucherType = this.state.voucherType;
        searchObject.voucherId = this.state.voucherId;
        searchObject.keyword = this.state.keyword.trim();
        searchObject.pageIndex = this.state.page;
        searchObject.date = new Date(date).toISOString();
        searchObject.pageSize = this.state.rowsPerPage;
        searchObject.managementPurchaseDepartment =
          isManagementPurchaseDepartment
            && managementPurchaseDepartment?.id

            ? managementPurchaseDepartment
            : null;
        if (Object.keys(planingDepartmentClone).length > 0 && this.state.isGetAll === false) {
          searchObject.managementPurchaseDepartment = planingDepartmentClone;
        }
        if (this.props?.voucherType === ConstantList.VOUCHER_TYPE.Store_Transfer) {
          await this.handleGetProductForStoreTransfer(searchObject);
        } else if (
          this.props?.voucherType === ConstantList.VOUCHER_TYPE.StockIn
          || this.props?.voucherType === ConstantList.VOUCHER_TYPE.StockOut
        ) {
          await this.handleSearchRemainProduct();
        }
        else if (searchObject.voucherType) {
          searchProductInVoucherByPage(searchObject).then(({ data }) => {
            this.setState({
              itemList: [...data.content],
              totalElements: data.totalElements,
            });
          });
        } else {
          searchByPage(searchObject).then(({ data }) => {
            this.setState({
              itemList: [...data.content],
              totalElements: data.totalElements,
            });
          });
        }
      } catch (e) {
        toast.error(t("general.error"));
      } finally {
        setPageLoading(false);
      }
    });
  }

  handleSearchRemainProduct = async () => {
    let { assetVouchers } = this.state;
    let normalizeValue = removeAccents(this.state.keyword?.toLowerCase()?.trim())
    let newArray = this.state.itemListClone.filter(item => {
      if (
        removeAccents(item.name)?.toLowerCase().includes(normalizeValue)
        || removeAccents(item.code)?.toLowerCase().includes(normalizeValue)
        || !this.state.keyword
      ) {
        let isExistProduct = this.state.products?.some(
          x => x.id === item.id
            && x.inputDate === item.inputDate
            && x.lotNumber === item.lotNumber
            && x.unitPrice === item.unitPrice
            && x.code === item.code
            && x.expiryDate === item.expiryDate
        );
        let isExistAsset = assetVouchers?.some(
          (x) =>
            (x?.product?.id === item?.id || x?.productId === item?.id)
            && (x?.product?.inputDate === item?.inputDate || x?.receiptDate === item?.inputDate)
            && (x?.product?.lotNumber === item?.lotNumber || x?.batchCode === item?.lotNumber)
            && (x?.product?.unitPrice === item?.unitPrice || x?.price === item?.unitPrice || x?.amount === item?.unitPrice)
            && (x?.product?.code === item?.code || x?.productCode === item?.code)
            && (x?.product?.expiryDate === item?.expiryDate || x?.expiryDate === item?.expiryDate)
        );
        item.tableData = {
          ...item.tableData,
          checked: isExistProduct || isExistAsset,
        }
        return item
      }
    })

    this.setState({
      itemList: newArray,
    })
  }

  handleGetProductForStoreTransfer = async (searchObject) => {
    let { assetVouchers } = this.state;

    getListRemainingQuantity(searchObject).then(({ data }) => {
      data?.data?.map((item) => {
        let existItem = assetVouchers?.find(
          (x) =>
            x?.productId === item?.productId &&
            x?.receiptDate === item?.inputDate
        );
        item.isCheck = !!existItem;
        item.tableData = {
          ...item.tableData,
          checked: !!existItem,
        };
        return item;
      });
      this.setState({
        itemList: data?.data?.length > 0 ? [...data.data] : [],
        totalElements: data?.total,
      });
    });
  }

  updatePageData = () => {
    let { assetVouchers = [] } = this.state;
    let { setPageLoading } = this.context;
    let searchObject = {};
    let {
      planingDepartment,
      isManagementPurchaseDepartment,
      managementPurchaseDepartment,
      date,
      isExportAsset
    } = this.props;
    let planingDepartmentClone = {};
    if (planingDepartment) {
      if (Object.keys(planingDepartment).length > 0) {
        planingDepartmentClone = { ...planingDepartment };
      }
    }
    searchObject.storeId = this.state.storeId;
    searchObject.productTypeCode = this.state.productTypeCode;
    searchObject.voucherType = this.state.voucherType;
    searchObject.voucherId = this.state.voucherId;
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.date = formatDateDto(date);
    if (isExportAsset) {
      searchObject.productId = [...assetVouchers]?.map(i => i?.productId).join(",")
    }
    searchObject.managementPurchaseDepartment = isManagementPurchaseDepartment
      && managementPurchaseDepartment?.id
      ? managementPurchaseDepartment
      : null;
    if (
      Object.keys(planingDepartmentClone).length > 0 &&
      this.state.isGetAll === false
    ) {
      searchObject.managementPurchaseDepartment = planingDepartmentClone;
    }
    if (this.props?.voucherType === ConstantList.VOUCHER_TYPE.Store_Transfer) {
      let _products = [];
      getListRemainingQuantity(searchObject).then(({ data }) => {
        //eslint-disable-next-line
        data?.data?.map((item) => {
          let existItem = assetVouchers?.find(
            (x) =>
              x?.productId === item?.productId &&
              (x?.receiptDate === item?.inputDate ||
                x?.inputDate === item?.inputDate)
          );
          item.isCheck = !!existItem;
          item.tableData = {
            ...item.tableData,
            checked: !!existItem,
          };
          if (!!existItem) {
            _products.push(item);
          }
          return item;
        });
        this.setState({
          itemList: data?.data?.length > 0 ? [...data.data] : [],
          totalElements: data?.total,
          products: _products,
        });
      });
    }
    else if (
      this.props?.voucherType === ConstantList.VOUCHER_TYPE.StockOut
      || this.props?.voucherType === ConstantList.VOUCHER_TYPE.StockIn
    ) {
      let products = [];
      getRemainProduct(searchObject).then(({ data }) => {
        data?.data?.map((item) => {
          let existItem = assetVouchers?.find(
            (x) =>
              (x?.product?.id === item?.id || x?.productId === item?.id)
              && (x?.product?.inputDate === item?.inputDate || x?.receiptDate === item?.inputDate)
              && (x?.product?.lotNumber === item?.lotNumber || x?.batchCode === item?.lotNumber)
              && (x?.product?.unitPrice === item?.unitPrice || x?.price === item?.unitPrice || x?.amount === item?.unitPrice)
              && (x?.product?.code === item?.code || x?.productCode === item?.code)
              && (x?.product?.expiryDate === item?.expiryDate || x?.expiryDate === item?.expiryDate)
          );
          if (existItem) {
            products.push(item);
          }
          item.tableData = {
            ...item.tableData,
            checked: Boolean(existItem),
          };
          return item;
        });
        this.setState({
          itemList: data?.data?.length > 0 ? [...data.data] : [],
          itemListClone: data?.data?.length > 0 ? [...data.data] : [],
          totalElements: data?.total,
          products,
        });
      });
    } else if (searchObject.voucherType) {
      searchProductInVoucherByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      });
    } else {
      searchByPage(searchObject).then(({ data }) => {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements,
        });
      });
    }
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "isGetAll") {
      this.setState({ isGetAll: event.target.checked }, () =>
        this.updatePageData()
      );

      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
      openProductDialog: false,
    });
    this.updatePageData();
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => handleKeyUp(e, this.search);

  setItemProduct = (value) => {
    this.setState(
      {
        itemProduct: value,
      },
      () => {
        this.setState({
          openProductDialog: true,
        });
      }
    );
  };

  handleAddNewProduct = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result?.data && result?.data?.code) {
          let item = result.data;
          this.setItemProduct({
            ...item,
            name: this.state.keyword,
          });
        }
      })
      .catch(() => {
        alert(this.props?.t("general.error_reload_page"));
      });
  };

  setOpenProductDialog = (value) => {
    this.setState({
      openProductDialog: value,
    });
  };

  handleProductDialogClose = () => {
    this.setOpenProductDialog(false);
    if (!this.state?.itemProduct?.id) {
      this.setState({
        keyword: "",
      });
    }
  };

  handleChangeOnSelection = (rows, rowData) => {
    let { products = [] } = this.state;
    let newArray = [...products];
    if (this.props?.voucherType === ConstantList.VOUCHER_TYPE.Store_Transfer) {
      newArray = rows;
      // let existRow = newArray.find(x => x.id === rowData?.id && x?.inputDate === rowData?.inputDate); // được chọn cùng 1 sản phẩm nhưng khác ngày nhập kho

      // rows?.forEach(item => {
      //   let isExist = products?.some(x => x.id === item.id && x?.inputDate === rowData?.inputDate);

      //   if (!isExist) {
      //     newArray.push(item);
      //   }
      // });

      // if (existRow?.id) {
      //   newArray.splice(newArray?.indexOf(existRow), 1);
      // }

    } else {
      if (rowData?.id && rowData?.inputDate) {
        newArray = newArray.filter(
          (product) => !(product.id === rowData.id && product.inputDate === rowData.inputDate)
        );
      }

      rows.forEach((item) => {
        const isExist = newArray.some(
          (product) => product.id === item.id && product.inputDate === item.inputDate
        );

        if (!isExist) {
          newArray.push(item);
        }
      });

    }
    this.setState(
      {
        products: newArray,
      }
    );
  };

  render() {
    const {
      t,
      i18n,
      handleClose,
      handleSelect,
      open,
      planingDepartment,
    } = this.props;
    let planingDepartmentClone = {};
    if (planingDepartment) {
      if (Object.keys(planingDepartment).length > 0) {
        planingDepartmentClone = { ...planingDepartment };
      }
    }
    let {
      keyword,
      itemList,
      products,
      voucherType,
      isGetAll,
    } = this.state;
    let columns = [
      {
        title: t("Product.code"),
        field: "code",
        align: "left",
        width: "150px",
      },
      { title: t("Product.name"), field: "name", align: "left", width: "150" },
    ];
    let columnsInSearchByVoucher = [
      {
        title: t("Product.code"),
        field: "code",
        align: "left",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.code || rowData?.productCode,
      },
      {
        title: t("Product.inputDate"),
        field: "inputDate",
        align: "left",
        width: "150px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatTimestampToDate(rowData?.inputDate),
      },
      {
        title: t("Product.name"),
        field: "name",
        align: "left",
        width: "150",
        render: (rowData) => rowData?.name || rowData?.productName,
      },
      {
        title: t("StockKeepingUnit.title"),
        field: "sku.name",
        align: "left",
        width: "150px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.sku?.name || rowData?.unitName,
      },
      {
        title: t("InventoryDeliveryVoucher.remainingQuantity"),
        field: "",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => convertMoney(rowData?.remainingQuantity || rowData?.remainQuantity || 0),
      },
      {
        title: t("Product.price"),
        field: "price",
        align: "left",
        width: "200px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.unitPrice
          ? convertNumberPrice(rowData?.unitPrice)
          : 0,
      },
      {
        title: t("Product.expiryDate"),
        field: "expiryDate",
        align: "left",
        width: "200px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.expiryDate
          ? formatTimestampToDate(rowData.expiryDate)
          : "",
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <span className="mb-20">{t("Product.title")}</span>
        </DialogTitle>
        <DialogContent style={{ overflow: "hidden" }}>
          <Grid container spacing={2}>
            <Grid item md={6} sm={6} xs={12}>
              <Input
                label={t("general.enterSearch")}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                fullWidth
                className="mb-16 "
                id="search_box"
                placeholder={t("Product.searchByName")}
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      onClick={this.search}
                      className="searchTable"
                    />
                  </InputAdornment>
                }
              />
            </Grid>
            {Object.keys(planingDepartmentClone).length > 0 ? (
              <Grid item md={6} sm={6} xs={12}>
                <FormControlLabel
                  style={{ float: "right" }}
                  value={isGetAll}
                  className="mb-16"
                  name="isGetAll"
                  onChange={(isGetAll) =>
                    this.handleChange(isGetAll, "isGetAll")
                  }
                  control={<Checkbox checked={isGetAll} />}
                  label={t("general.allProduct")}
                />
              </Grid>
            ) : (
              ""
            )}
            {this.props?.allowedAddNew &&
              itemList?.length === 0 &&
              getRole().isRoleAssetManager && (
                <Grid item md={6} sm={6} xs={12}>
                  <Button
                    className="mr-12"
                    variant="contained"
                    color="primary"
                    onClick={this.handleAddNewProduct}
                  >
                    {t("general.add")}
                  </Button>
                </Grid>
              )}
            {this.state.openProductDialog && (
              <ProductDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleProductDialogClose}
                open={this.state?.openProductDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={this.state.itemProduct}
                type={appConst.productPurchaseTypeCode.VTHH}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={itemList}
              columns={voucherType ? columnsInSearchByVoucher : columns}
              options={{
                sorting: false,
                toolbar: false,
                selection: true,
                actionsColumnIndex: -1,
                paging: true,
                search: false,
                padding: "dense",
                minBodyHeight: "233px",
                maxBodyHeight: "233px",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: {
                  labelDisplayedRows: "{from}-{to} trong {count}",
                  firstTooltip: "Trang đầu",
                  previousTooltip: "Trang trước",
                  nextTooltip: "Trang tiếp",
                  lastTooltip: "Trang cuối",
                  labelRowsSelect: "hàng mỗi trang",
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={this.handleChangeOnSelection}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(products)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectProductAllPopop;
SelectProductAllPopop.contextType = AppContext;
