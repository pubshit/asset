import React from "react";
import { Box, Typography } from "@material-ui/core";
import PropTypes from "prop-types";
import clsx from "clsx";


export function TabPanel(props) {
  const { children, value, index, boxProps, indexes, ...other } = props;

  if (indexes?.length > 0) {
    return (
      <React.Fragment key={index}>
        <div
          role="tabpanel"
          id={`scrollable-force-tabpanel-${index}`}
          aria-labelledby={`scrollable-force-tab-${index}`}
          {...other}
        >
          <Box className={boxProps?.className}>
            {children}
          </Box>
        </div>
      </React.Fragment>
    );
  }
  return (
    <React.Fragment key={index}>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box className={clsx(boxProps?.className)}>
            {children}
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

TabPanel.propTypes = {
  children: PropTypes.any,
  boxProps: PropTypes.shape({
    className: PropTypes.any,
  }),
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};