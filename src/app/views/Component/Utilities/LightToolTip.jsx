import {withStyles} from "@material-ui/core/styles";
import {Tooltip} from "@material-ui/core";

const CustomTooltip = props => {
  let {popperOptions, title, children, ...rest} = props;
  return (
    <Tooltip
      {...rest}
      title={props.title}
      PopperProps={{
        popperOptions,
      }}
    >
      {children}
    </Tooltip>
  )
}

export const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: "0",
  },
}))(CustomTooltip);