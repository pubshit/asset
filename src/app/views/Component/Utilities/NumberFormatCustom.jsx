import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import React, {memo} from "react";
import { appConst, LIST_ORGANIZATION } from "../../../appConst";
import { CURRENT_ORG } from "app/appConfig";

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  let formatSiteBV199 = [
    LIST_ORGANIZATION.BV199.code,
    LIST_ORGANIZATION.PRODUCT_BV199.code,
  ].includes(CURRENT_ORG.code);
  
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator={formatSiteBV199 ? appConst.TYPES_MONEY.DOT.value : appConst.TYPES_MONEY.COMMA.value}
      decimalSeparator={formatSiteBV199 ? appConst.TYPES_MONEY.COMMA.value : appConst.TYPES_MONEY.DOT.value}
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
export default NumberFormatCustom