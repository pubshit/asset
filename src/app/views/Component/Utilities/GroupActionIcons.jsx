import {appConst, DEFAULT_TOOLTIPS_PROPS} from "../../../appConst";
import {Icon, IconButton} from "@material-ui/core";
import React from "react";
import {useTranslation} from "react-i18next";
import PropTypes from "prop-types";
import {LightTooltip} from "./LightToolTip";

function GroupActionIcons(props) {
  const {t} = useTranslation();
  const {
    item = false,
    editIcon = false,
    deleteIcon = false,
    viewIcon = false,
    checkIcon = false,
    receiveIcon = false,
    printIcon = false,
    proposeIcon = false,
    historyIcon = false,
    compareIcon = false,
    qrcodeIcon = false,
    cardIcon = false,
    copyIcon = false,
    cancelIcon = false,
    onSelect = (item, method) => {}
  } = props;
  
  return (
    <div className="none_wrap">
      {editIcon && (
        <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.edit)}>
            <Icon fontSize="small" color="primary">edit</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {deleteIcon && (
        <LightTooltip title={t("general.deleteIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">delete</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {viewIcon && (
        <LightTooltip title={t("general.viewIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.view)}>
            <Icon fontSize="small" color="default">visibility</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {printIcon && (
        <LightTooltip title={t("general.print")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.print)}>
            <Icon fontSize="small" color="default">print</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {checkIcon && (
        <LightTooltip title={t("general.checkIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.check)}>
            <Icon fontSize="small" color="primary">check_circle</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {receiveIcon && (
        <LightTooltip title={t("general.receiveIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.receive)}>
            <span className="iconArrow flex flex-center flex-middle">
              <Icon fontSize="small" color="primary">arrow_forward_outlined</Icon>
            </span>
          </IconButton>
        </LightTooltip>
      )}
      {historyIcon && (
        <LightTooltip title={t("general.historyIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.history)}>
            <Icon fontSize="small" color="primary">history</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {cardIcon && (
        <LightTooltip title={t("general.cardIconIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.card)}>
            <Icon fontSize="small" color="primary">assignment_icon</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {qrcodeIcon && (
        <LightTooltip title={t("general.qrcodeIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.qrcode)}>
            <Icon fontSize="small" color="primary">filter_center_focus</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {copyIcon && (
        <LightTooltip title={t("general.copyIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.copy)}>
            <Icon fontSize="small" color="primary">content_copy</Icon>
          </IconButton>
        </LightTooltip>
      )}
      {cancelIcon && (
        <LightTooltip title={t("general.cancelIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton size="small" onClick={() => onSelect(item, appConst.active.cancel)}>
            <Icon fontSize="small" color="error">cancel</Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  )
}

export default GroupActionIcons;
GroupActionIcons.propTypes = {
  item: PropTypes.any,
  onSelect: PropTypes.func,
  itemIcon: PropTypes.bool,
  editIcon: PropTypes.bool,
  deleteIcon: PropTypes.bool,
  viewIcon: PropTypes.bool,
  checkIcon: PropTypes.bool,
  receiveIcon: PropTypes.bool,
  printIcon: PropTypes.bool,
  proposeIcon: PropTypes.bool,
  historyIcon: PropTypes.bool,
  compareIcon: PropTypes.bool,
  qrcodeIcon: PropTypes.bool,
  cardIcon: PropTypes.bool,
  copyIcon: PropTypes.bool,
  cancelIcon: PropTypes.bool,
}