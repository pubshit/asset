import {FormControlLabel, styled, Switch} from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";

const MaterialUISwitch = styled(Switch)((props) => {
  let {theme, beforesvg, checkedsvg, thumbcolor = {}} = props;
  const newBeforeSvg = beforesvg.replaceAll('\n', "");
  const newCheckedSvg = checkedsvg.replaceAll('\n', "");

  return ({
    width: 52,
    height: 28,
    padding: 7,
    '& .MuiSwitch-switchBase': {
      margin: 1,
      padding: 0,
      transform: 'translateX(6px)',
      '&.Mui-checked': {
        color: '#fff',
        transform: 'translateX(22px)',
        '& .MuiSwitch-thumb:before': {
          backgroundImage: `url('data:image/svg+xml;utf8,${newCheckedSvg}')`
        },
        '& .MuiSwitch-thumb': {
          backgroundColor: thumbcolor.checked,
        },
        '& + .MuiSwitch-track': {
          opacity: 1,
          backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
        },
      },
    },
    '& .MuiSwitch-thumb': {
      backgroundColor: thumbcolor.default || theme.custom.primary.main,
      width: 22,
      height: 22,
      '&::before': {
        content: `""`,
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundImage: `url('data:image/svg+xml;utf8,${newBeforeSvg}')`
      },
    },
    '& .MuiSwitch-track': {
      opacity: 1,
      backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
      borderRadius: 20 / 2,
    },
  });
});

export default function CustomizedSwitches(props) {
  const {
    beforesvg,
    checkedsvg,
    thumbcolor,
    checked,
    onChange,
    value,
    label,
    ...rest
  } = props;
  return (
    <FormControlLabel
      control={(
        <MaterialUISwitch
          beforesvg={beforesvg} // Truyền svg vào dưới dạng string '' | `` | ""
          checkedsvg={checkedsvg}
          thumbcolor={thumbcolor}
          checked={checked}
          onChange={onChange}
          value={value}
          {...rest}
        />
      )}
      label={label}
    />
  );
}

CustomizedSwitches.propTypes = {
  label: PropTypes.string.isRequired,
  beforesvg: PropTypes.string,
  checkedsvg: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  value: PropTypes.any,
  thumbcolor: PropTypes.objectOf(PropTypes.shape({
    default: PropTypes.string,
    checked: PropTypes.string,
  })),
}
