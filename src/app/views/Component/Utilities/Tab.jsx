import {Tab, Tabs} from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";

export const GeneralTab = (props) => {
  const {tabs, value, onChange = () => {}} = props;
  const renderLabelTab = (item) => {
    if (item?.count) {
      return (
        <div className="tabLable">
          <span>{item?.label}</span>
          <div className={`tabQuantity tabQuantity-${item?.type}`}>
            {item?.count || 0}
          </div>
        </div>
      )
    } else {
      return item?.label;
    }
  }

  return <>
    <Tabs
      className="tabsStatus"
      value={value}
      onChange={onChange}
      variant="scrollable"
      scrollButtons="on"
      indicatorColor="primary"
      textColor="primary"
      aria-label="scrollable force tabs example"
    >
      {tabs?.map((item) => (
        <Tab
          className="tab"
          key={item.key}
          value={item.key}
          label={renderLabelTab(item)}
        />
      ))}
    </Tabs>
  </>
}

GeneralTab.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.any,
      label: PropTypes.string,
      count: PropTypes.any,
      type: PropTypes.oneOf(["info", "warning", "success", "error"]),
    })
  ).isRequired,
}