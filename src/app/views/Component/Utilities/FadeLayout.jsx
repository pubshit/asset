import clsx from "clsx";
import PropTypes from "prop-types";

export const FadeLayout = props => {
  const {fullSize, blur, children, show, alignItems, justifyContent, color, backgroundColor} = props;

  const getAlignItem = align => {
    switch (align) {
      case "center":
        return "flex-middle";
      case "end":
        return "flex-align-items-end";
      case "start":
        return "flex-align-items-start";
      case "top":
        return "flex-top";
      case "bottom":
        return "flex-bottom";
      default:
        return "";
    }
  }

  const getJustifyContent = justify => {
    switch (justify) {
      case "center":
        return "flex-center";
      case "space-around":
        return "flex-space-around";
      case "space-between":
        return "flex-space-between";
      case "space-evenly":
        return "flex-space-evenly";
      default:
        return "";
    }
  }

  const getColor = color => {
    switch (color) {
      case "primary":
        return "text-primary";
      case "secondary":
        return "text-secondary";
      case "pink":
        return "text-pink";
      default:
        return "";
    }
  }

  const getBackgroundColor = color => {
    switch (color) {
      case "light-primary":
        return "bg-light-primary";
      case "fade":
        return "bg-fade";
      case "none":
        return "";
      default:
        return "";
    }
  }


  return (
    <div
      className={clsx(
        "position-absolute zIndex-1000",
        fullSize ? "inset-0" : "",
        blur ? "bg-blur" : "",
        show ? "display-block" : "display-none",
        show && (alignItems || justifyContent) ? "flex" : "",
        alignItems ? getAlignItem(alignItems) : "",
        justifyContent ? getJustifyContent(justifyContent) : "",
        color ? getColor(color) : "",
        backgroundColor ? getBackgroundColor(backgroundColor) : "",
      )}
    >
      {children}
    </div>
  );
};

FadeLayout.propTypes = {
  fullSize: PropTypes.bool,
  blur: PropTypes.bool,
  show: PropTypes.bool,
  children: PropTypes.any,
  alignItems: PropTypes.oneOf(["top", "bottom", "start", "end", "center",]),
  justifyContent: PropTypes.oneOf(["center", "space-between", "space-around", "space-evenly"]),
  color: PropTypes.oneOf(["primary", "secondary", "pink",]),
  backgroundColor: PropTypes.oneOf(["light-primary", "none", "fade"]),
}