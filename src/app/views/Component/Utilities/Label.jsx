import PropTypes from "prop-types";

const Label = (props) => {
  const {isRequired, children} = props;

  return (
    <span>
      {isRequired && (<span className="colorRed">* </span>)}
      {children}
    </span>
  )
}

Label.propTypes = {
  isRequired: PropTypes.bool,
}
export default Label;
