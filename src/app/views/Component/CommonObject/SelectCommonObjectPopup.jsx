import {
  InputAdornment,
  Input,
  Grid,
  MuiThemeProvider,
  TextField,
  Button,
  TableHead,
  TableCell,
  TableRow,
  Checkbox,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import MaterialTable, {
  MTableToolbar,
  Chip,
  MTableBody,
  MTableHeader,
} from "material-table";
import { useTranslation, withTranslation, Trans } from "react-i18next";
// import { searchByPage } from "../../Supplier/SupplierService";
// import { searchByPage } from "../../ProductCategory/ProductCategoryService"
import { searchByTextHSX } from "../../CommonObject/CommonObjectService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import CommonObjectDialog from "../../CommonObject/CommonObjectDialog";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import { appConst } from "../../../appConst";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectCommonObjectPopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    departmentId: "",
    userDepartmentId: "",
    managementDepartmentId: "",
    voucherType: null,
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    code: "",
    name: "",
    manufacturer: null,
    shouldOpenCommonObjectDialog: false,
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    try {
      searchByTextHSX(searchObject).then(({ data }) => {
        this.setState({
          itemList: [],
        });
        if (appConst.CODE.SUCCESS === data?.code) {
          this.setState({
            itemList: [...data.data?.content],
            totalElements: data.data?.totalElements,
          });
        }
      });
    } catch (error) {
      console.error("Error: ", error);
    }
  };

  search() {
    this.setPage(0, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword;
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      console.log(searchObject);
      try {
        searchByTextHSX(searchObject).then(({ data }) => {
          this.setState({
            itemList: [],
          });
          if (appConst.CODE.SUCCESS === data?.code) {
            this.setState({
              itemList: [...data.data?.content],
              totalElements: data.data?.totalElements,
            });
          }
        });
      } catch (error) {
        console.error("Error: ", error);
      }
    });
  }

  componentDidMount() {
    this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    //alert(item);
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: null, selectedItem: null });
    }
  };

  componentWillMount() {
    let { open, handleClose, selectedItem, manufacturer, name, code } =
      this.props;
    //this.setState(item);
    this.setState({
      name: name,
      code: code,
      selectedValue: selectedItem.id,
      manufacturer: manufacturer,
    });
  }

  handleKeyDownEnterSearch = (e) => {
    // if (e.key === 'Enter') {
    this.search();
    // }
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenCommonObjectDialog = () => {
    this.setState({
      shouldOpenCommonObjectDialog: true,
    });
  };

  handleDialogCommonObjectClose = () => {
    this.setState({
      shouldOpenCommonObjectDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.search(this.state.keyword);
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => {
    this.search();
  };

  render() {
    const { t, i18n, handleClose, handleSelect, selectedItem, open } =
      this.props;
    let { keyword, itemList, shouldOpenCommonObjectDialog } = this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "120px",
        cellStyle: {
          padding: "0px",
        },
        render: (rowData) => (
          <Radio
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={this.state.selectedValue === rowData.id}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("CommonObject.code"),
        field: "code",
        align: "left",
        minWidth: "150x",
      },
      {
        title: t("CommonObject.name"),
        field: "name",
        align: "left",
        width: "150",
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        fullWidth
        style={{ height: "100%" }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Product.manufacturer")}</span>
        </DialogTitle>
        <DialogContent style={{ minHeight: "450px" }}>
          <Grid item xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDownEnterSearch}
              onKeyUp={this.handleKeyUp}
              style={{ width: "50%" }}
              className="mb-16 mr-12"
              id="search_box"
              placeholder={t("general.enterSearch")}
              startAdornment={
                <InputAdornment>
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
            {/* <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.search(keyword)}>
              {t('general.search')}
            </Button> */}
            {itemList.length !== 0 ? (
              ""
            ) : (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleOpenCommonObjectDialog()}
              >
                {t("CommonObject.add")}
              </Button>
            )}
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenCommonObjectDialog && (
                <CommonObjectDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogCommonObjectClose}
                  fromCommonObjectPopup={true}
                  open={shouldOpenCommonObjectDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={{}}
                />
              )}
            </div>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "303px",
                minBodyHeight: "303px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>

          <Button
            className="mr-32 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleSelect(this.state.selectedItem)}
          >
            {t("general.select")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectCommonObjectPopup;
