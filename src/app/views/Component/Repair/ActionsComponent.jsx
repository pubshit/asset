import { ICON_OBJECT } from "../../../appConst";
import { LightTooltip } from "../Utilities";
import { Icon, IconButton } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";

function MaterialButton(props) {
  const rowData = props.rowData;
  const listAction = rowData?.additionalIcon?.split(",")?.map(x => x?.trim());

  return (
    <div className="none_wrap">
      {listAction?.map(x => {
        let componentsObject = ICON_OBJECT[x] || {};
        return (
          <span key={x}>
            <LightTooltip
              title={componentsObject?.text}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => props.onSelect(rowData, componentsObject?.iconAction)}
              >
                <Icon fontSize="small" color={componentsObject?.color}>
                  {componentsObject?.icon}
                </Icon>
              </IconButton>
            </LightTooltip>
          </span>
        )
      })}
    </div>
  );
}

export default MaterialButton;
MaterialButton.propTypes = {
  onSelect: PropTypes.func.isRequired,
  rowData: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  hasCreatePermission: PropTypes.bool,
  hasDeletePermission: PropTypes.bool,
}
