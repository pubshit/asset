import React, { useContext } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import {
  Typography,
  Tabs,
  Box,
  Tab,
  AppBar,
  Grid,
  IconButton,
  Icon,
  TablePagination,
  TextField,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import NumberFormat from "react-number-format";
import AppContext from "app/appContext";
import {
  getListMaintainPlaningHistory,
  getListRepairingHistory,
  getMaintainPlaningHistoryLinhKien,
  getRepairingHistoryLinhKien,
  getTotalCostMaintainPlaningHistory,
  getTotalCostRepairingHistory, historyOfAllocation,
  historyOfAllocationTransfer, historyOfTransfer,
} from "app/views/Asset/AssetService";
import { appConst, typeWarehouseVoucher } from "app/appConst";
import { toast } from "react-toastify";
import { useState } from "react";
import { useEffect } from "react";
import {
  TabPanel,
  convertNumberPrice,
  filterOptions,
  handleThrowResponseMessage,
  isSuccessfulResponse, formatTimestampToDate, defaultPaginationProps
} from "app/appFunction";
import { DateTimePicker, MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { searchByTextNew } from "app/views/Supplier/SupplierService";
import { Autocomplete } from "@material-ui/lab";
import viLocale from "date-fns/locale/vi";
import { getTheHighestRole, formatDateDto, formatDateDtoMore } from "app/appFunction";
import Constantlist from "app/appConfig";
import AsynchronousAutocompleteSub from "app/views/utilities/AsynchronousAutocompleteSub";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonForce(props) {
  const { setPageLoading } = useContext(AppContext);
  const { t, item, assetId, setTotalElementsChild, setTotalElements, handleChange, handleReset } = props;
  let { tabValue, fromDate, toDate, donViThucHien } = item;
  const classes = useStyles();
  const [transferHistories, setTransferHistories] = useState([]);
  const [allocationHistories, setAllocationHistories] = useState([]);
  const [maintainPlaningHistories, setMaintainPlaningHistories] = useState([]);
  const [repairingHistories, setRepairingHistories] = useState([]);
  const [repairingLinhKiens, setRepairingLinhKiens] = useState([]);
  const [totalCosts, setTotalCosts] = useState({
    maintainPlaningCost: 0,
    repairCost: 0,
  });
  const [listSupplier, setListSupplier] = useState([]);

  const searchObjectSupplier = {
    pageIndex: 1,
    pageSize: 1000000,
    typeCodes: [appConst.TYPE_CODES.NCC_BDSC]
  }

  useEffect(() => {
    if (appConst.historyTabs.maintainPlaning === tabValue) {
      maintainPlaningUpdatePageData();
    } else if (appConst.historyTabs.repairing === tabValue) {
      repairingUpdatePageData();
    } else if (appConst.historyTabs.allocation === tabValue) {
      allocationUpdatePageData();
    } else if (appConst.historyTabs.transfer === tabValue) {
      transferUpdatePageData();
    }
    props.setRowsPerPage?.(5);
    props.setRowsPerPageChild?.(5);
  }, [tabValue]);

  useEffect(() => {
    if (appConst.historyTabs.maintainPlaning === tabValue) {
      maintainPlaningUpdatePageData();
    } else if (appConst.historyTabs.repairing === tabValue) {
      repairingUpdatePageData();
    }
  }, [item?.page, item?.rowsPerPage, item?.pageChild, item?.rowsPerPageChild]);

  useEffect(() => {
    if (appConst.historyTabs.maintainPlaning === tabValue) {
      maintainPlaningUpdatePageData();
    }
  }, [toDate, fromDate, donViThucHien]);

  const handleChangeValue = (event, newValue) => {
    props.setTabValue(newValue);
    handleReset();
  };

  const maintainPlaningUpdatePageData = async () => {
    let historySearchObject = {
      pageIndex: item.page + 1,
      pageSize: item.rowsPerPage,
      assetId: assetId,
      unitId: donViThucHien?.id,
      toDate: formatDateDtoMore(toDate),
      fromDate: formatDateDtoMore(fromDate, "start"),
    };
    let productSearchObject = {
      pageIndex: item.pageChild + 1,
      pageSize: item.rowsPerPageChild,
      assetId: assetId,
      donViThucHienid: donViThucHien?.id,
      toDate: formatDateDto(toDate),
      fromDate: formatDateDto(fromDate),
    };
    try {
      setPageLoading(true);
      let res = await getListMaintainPlaningHistory(historySearchObject);
      let resTotalCost = await getTotalCostMaintainPlaningHistory(assetId);
      let resLinhKien = await getMaintainPlaningHistoryLinhKien(productSearchObject);
      const { code, data, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        //get ls bảo dưỡng
        setMaintainPlaningHistories(data?.content ?? []);
        setTotalElements(data?.totalElements ?? 0);
      } else {
        toast.warning(message);
      }
      if (appConst.CODE.SUCCESS === resLinhKien?.data?.code) {
        setTotalElementsChild(resLinhKien?.data?.data?.totalElements ?? 0);
      } else {
        toast.warning(resLinhKien?.data?.message);
      }
      if (appConst.CODE.SUCCESS === resTotalCost?.data?.code) {
        setTotalCosts({
          ...totalCosts,
          maintainPlaningCost: resTotalCost?.data?.data ?? 0,
        });
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const renderText = (rowData) => {
    const uniqueStrings = rowData.asset?.assetSources?.map((item) => item?.assetSource?.name)
    const joinedText = uniqueStrings.join(", ")
    return joinedText
  }

  const allocationUpdatePageData = async () => {
    try {
      setPageLoading(true);
      let res = await historyOfAllocation(assetId);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setAllocationHistories(data);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const transferUpdatePageData = async () => {
    try {
      setPageLoading(true);
      let res = await historyOfTransfer(assetId);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setTransferHistories(data);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const repairingUpdatePageData = async () => {
    let historySearchObject = {
      pageIndex: item.page + 1,
      pageSize: item.rowsPerPage,
      assetId: assetId,
    };
    let productSearchObject = {
      pageIndex: item.pageChild + 1,
      pageSize: item.rowsPerPageChild,
      assetId: assetId,
    };
    try {
      setPageLoading(true);
      let res = await getListRepairingHistory(historySearchObject);
      let resTotalCost = await getTotalCostRepairingHistory(assetId);
      let resLinhKien = await getRepairingHistoryLinhKien(productSearchObject);
      const { code, data, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        //get ls bảo dưỡng
        setRepairingHistories(data?.content ?? []);
        setTotalElements(data?.totalElements ?? 0);
      } else {
        toast.warning(message);
      }
      if (appConst.CODE.SUCCESS === resLinhKien?.data?.code) {
        // get ds linh kiện
        setRepairingLinhKiens(resLinhKien?.data?.data?.content ?? []);
        setTotalElementsChild(resLinhKien?.data?.data?.totalElements ?? 0);
      } else {
        toast.warning(resLinhKien?.data?.message);
      }
      if (appConst.CODE.SUCCESS === resTotalCost?.data?.code) {
        //gettổng chi phí
        setTotalCosts({
          ...totalCosts,
          repairCost: resTotalCost?.data?.data ?? 0,
        });
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  let columnsAllocationAsset = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 40,
      minWidth: 40,
      align: "center",
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("allocation_asset.date"),
      field: "asset.dayStartedUsing",
      cellStyle: {
        textAlign: "center",
      },
      maxWidth: 80,
      render: (rowData) => formatTimestampToDate(rowData.issueDate),
    },
    {
      title: t("allocation_asset.assetSource"),
      width: "350px",
      field: "sourceName",
    },
    {
      title: t("allocation_asset.receiverDepartment"),
      field: "receiverDepartmentName",
    },
    {
      title: t("allocation_asset.receiverPerson"),
      field: "receiverPersonName",
    },
    {
      title: t("allocation_asset.usePerson"),
      field: "usePersonName",
    },
  ];

  let columnsAssetTransfer = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 40,
      minWidth: 40,
      align: "center",
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetTransfer.issueDate"),
      field: "voucher.issueDate",
      cellStyle: { textAlign: "center" },
      render: (rowData) => formatTimestampToDate(rowData.issueDate),
    },
    {
      title: t("AssetTransfer.handoverDepartment"),
      field: "handoverDepartmentName",
    },
    {
      title: t("AssetTransfer.handoverPerson"),
      field: "handoverPersonName",
    },
    {
      title: t("allocation_asset.receiverDepartment"),
      field: "receiverDepartmentName",
    },
    {
      title: t("allocation_asset.receiverPerson"),
      field: "receiverPersonName",
    },
    {
      title: t("allocation_asset.usePerson"),
      field: "usePersonName",
    }
  ];

  const columnsRepairHistory = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("maintainRequest.date"),
      field: "arNtThoiGian",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.arNtThoiGian
          ? moment(rowData?.arNtThoiGian).format("DD/MM/YYYY")
          : "",
    },
    {
      title: t("maintainRequest.columns.codeForm"),
      field: "arMa",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("maintainRequest.depRequest"),
      field: "arPhongBanText",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("maintainRequest.form"),
      field: "arHinhThucMoTa",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("maintainRequest.columns.costs"),
      field: "arTongChiPhi",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.arTongChiPhi ? convertNumberPrice(rowData?.arTongChiPhi) : "",
    },
    {
      title: t("maintainRequest.columns.IncidentContent"),
      field: "arNoiDung",
      align: "left",
      minWidth: 250,
    },
  ];

  const columnsRepairHistoryChildren = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("maintainRequest.columns.suppliesUse"),
      field: "productName",
      minWidth: 400,
    },

    {
      title: t("maintainRequest.columns.quantity"),
      field: "quantity",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
    },
  ];

  let columnsMaintainRequest = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("maintainRequest.columns.implementationDate"),
      field: "",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.ckDate ? (
          <span>{moment(rowData?.ckDate).format("DD/MM/YYYY")}</span>
        ) : (
          ""
        ),
    },
    {
      title: t("maintainRequest.columns.implementUnit"),
      field: "donViThucHienText",
    },
    {
      title: t("maintainRequest.columns.costs"),
      field: "ckNtTongChiPhi",
      align: "center",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.ckNtTongChiPhi
          ? convertNumberPrice(rowData?.ckNtTongChiPhi)
          : "",
    },
  ];

  let columnsAccreditationHistory = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("maintainRequest.columns.implementationDate"),
      field: "",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.ckDate ? (
          <span>{moment(rowData?.ckDate).format("DD/MM/YYYY")}</span>
        ) : (
          ""
        ),
    },
    {
      title: t("maintainRequest.columns.inspectionUnit"),
      field: "donViKiemDinh",
    },
    {
      title: t("maintainRequest.columns.certificationPeriod"),
      field: "certificationPeriod",
    },
    {
      title: t("maintainRequest.columns.inspectionResult"),
      field: "certificationPeriod",
    },
  ];
  let columnsCalibrationhistory = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("maintainRequest.columns.implementationDate"),
      field: "",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.ckDate ? (
          <span>{moment(rowData?.ckDate).format("DD/MM/YYYY")}</span>
        ) : (
          ""
        ),
    },
    {
      title: t("maintainRequest.columns.inspectionUnit"),
      field: "donViKiemDinh",
    },
    {
      title: t("maintainRequest.columns.certificationPeriod"),
      field: "certificationPeriod",
    },
    {
      title: t("maintainRequest.columns.inspectionResult"),
      field: "certificationPeriod",
    },
  ];
  const columnsMaintainRequestChildren = [
    {
      title: t("general.stt"),
      field: "code",
      minWidth: 42,
      maxWidth: 42,
      align: "center",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "10px",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },

    {
      title: t("maintainRequest.columns.suppliesUse"),
      field: "product",
      minWidth: 400,
    },

    {
      title: t("maintainRequest.columns.quantity"),
      field: "",
      cellStyle: {
        textAlign: "center",
      },
      minWidth: 110,
      maxWidth: 110,
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={tabValue}
          onChange={handleChangeValue}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("allocation_asset.allocation_asset_histoty")} />
          <Tab label={t("AssetTransfer.asset_transfer_history")} />
          <Tab label={t("maintainRequest.repair_history")} />
          <Tab label={t("maintainRequest.maintenance_history")} />
          {/* <Tab label={t("maintainRequest.maintain_calibration_history")} /> */}
          {/* <Tab label="Item Three" /> */}
        </Tabs>
      </AppBar>
      <TabPanel
        value={tabValue}
        index={appConst.historyTabs.allocation}
        style={{ height: "450px" }}
      >
        <MaterialTable
          title={t("general.list")}
          data={allocationHistories}
          columns={columnsAllocationAsset}
          //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
          options={{
            sorting: false,
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
            // this.setState({selectedItems:rows});
          }}
        />
      </TabPanel>
      <TabPanel
        value={tabValue}
        index={appConst.historyTabs.transfer}
        style={{ height: "450px" }}
      >
        <Grid>
          <MaterialTable
            title={t("general.list")}
            data={transferHistories}
            columns={columnsAssetTransfer}
            //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
            options={{
              sorting: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
              // this.setState({selectedItems:rows});
            }}
          />
        </Grid>
      </TabPanel>

      <TabPanel
        value={tabValue}
        index={appConst.historyTabs.repairing}
        style={{ height: "450px" }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={repairingHistories ?? []}
              columns={columnsRepairHistory}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={props?.item?.totalElements}
              rowsPerPage={props.item?.rowsPerPage}
              page={props?.item?.page}
              onPageChange={props?.handleChangePage}
              onRowsPerPageChange={props?.setRowsPerPage}
            />
          </Grid>

          <Grid item container xs={12}>
            <Grid item xs={6}>
              <MaterialTable
                columns={columnsRepairHistoryChildren}
                data={repairingLinhKiens ?? []}
                options={{
                  sorting: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    color: "#fff",
                  },
                  padding: "dense",
                  toolbar: false,
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
              />
              <TablePagination
                {...defaultPaginationProps()}
                rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                count={props?.item?.totalElementsChild}
                rowsPerPage={props.item?.rowsPerPageChild}
                page={props?.item?.pageChild}
                onPageChange={props?.handleChangePageChild}
                onRowsPerPageChange={props?.setRowsPerPageChild}
              />
            </Grid>

            <Grid container item xs={6} justifyContent="flex-end">
              <Grid
                item
                className="text-uppercase font-weight-bold text-primary"
              >
                {t("maintainRequest.totalCosts")}{" "}
                <span>{convertNumberPrice(totalCosts?.repairCost) ?? ""}</span>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </TabPanel>

      <TabPanel
        value={tabValue}
        index={appConst.historyTabs.maintainPlaning}
        style={{ height: "450px" }}
      >
        <Grid container spacing={2}>
          {/* <Grid item md={3} sm={6} xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <KeyboardDatePicker
                  fullWidth
                  className="mb-16 mr-16 align-bottom"
                  margin="none"
                  id="mui-pickers-date"
                  label={t("maintainRequest.fromDate")}
                  inputVariant="standard"
                  type="text"
                  autoOk={false}
                  format="dd/MM/yyyy"
                  name={"fromDate"}
                  value={fromDate}
                  maxDate={toDate}
                  onChange={(date) => handleChange(date, "fromDate")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <div>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <KeyboardDatePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={t("maintainRequest.toDate")}
                  inputVariant="standard"
                  type="text"
                  autoOk={false}
                  format="dd/MM/yyyy"
                  name={"toDate"}
                  value={toDate}
                  minDate={fromDate}
                  onChange={(date) => handleChange(date, "toDate")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
            </div>
          </Grid> */}
          <Grid item xs={12} sm={6} md={3}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={t("maintainRequest.columns.implementUnit")}
              listData={listSupplier}
              setListData={setListSupplier}
              searchFunction={searchByTextNew}
              searchObject={searchObjectSupplier}
              defaultValue={donViThucHien ? donViThucHien : null}
              displayLable="name"
              value={donViThucHien ? donViThucHien : null}
              onSelect={value => handleChange(value, "donViThucHien")}
              filterOptions={(options, params) => filterOptions(options, params)}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={maintainPlaningHistories}
              columns={columnsMaintainRequest}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={props?.item?.totalElements}
              rowsPerPage={props.item?.rowsPerPage}
              page={props?.item?.page}
              onPageChange={props?.handleChangePage}
              onRowsPerPageChange={props?.setRowsPerPage}
            />
          </Grid>

          <Grid item container xs={12}>
            <Grid item xs={6}></Grid>
            <Grid container item xs={6} justifyContent="flex-end">
              <Grid
                item
                className="text-uppercase font-weight-bold text-primary"
              >
                {t("MaintainPlaning.totalMaintainCost")}{" "}
                <span>
                  {convertNumberPrice(totalCosts?.maintainPlaningCost) ?? ""}
                </span>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </TabPanel>
    </div>
  );
}
