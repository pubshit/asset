import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch,
  DialogActions,
  TablePagination,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import { generateRandomId } from "utils";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import DialogContent from "@material-ui/core/DialogContent";
import ScrollableTabsButtonForce from "./ScrollableTabsButtonForce";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import FileSaver, { saveAs } from "file-saver";
import {
  historyOfAllocationTransfer,
  historyOfBrokenMessageAndRepair,
  exportToExceAllocationHistory,
  exportToExceTransferHistory,
  exportToExceRepairHistory,
  exportToExceMaintenanceHistory
} from "../../Asset/AssetService";
import { toast } from "react-toastify";
import AppContext from "app/appContext";
import { appConst } from "app/appConst";
import { functionExportToExcel, formatDateDtoMore } from "app/appFunction";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class ViewAssetHistoryPopup extends Component {
  state = {
    allocationTransfer: [],
    maintainRequest: [],
    rokenMessageAndRepair: [],
    totalElements: 0,
    page: 0,
    rowsPerPage: 10000,
    totalElementsChild: 0,
    pageChild: 0,
    rowsPerPageChild: 10000,
    tabValue: 0,
    toDate: null,
    fromDate: null,
    donViThucHien: null,
  };

  handleFormSubmit = () => {};

  componentDidMount() {
    this.updatePageData();
  }

  updatePageData = () => {
    let { item, t } = this.props;
    let searchAssetId = {};
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event?.target?.value ?? event, page: 0 });
  };

  handleChangePageChild = (event, pageChild) => {
    this.setState({ pageChild });
  };

  setRowsPerPageChild = (event) => {
    this.setState({
      rowsPerPageChild: event?.target?.value ?? event,
      pageChild: 0,
    });
  };

  setTotalElements = (totalElements) => {
    this.setState({ totalElements });
  };

  setTotalElementsChild = (totalElementsChild) => {
    this.setState({ totalElementsChild });
  };

  setTabValue = (newValue) => {
    this.setState({ tabValue: newValue });
  }
  
  exportToExcelAssetDetail =async () => {
    const { setPageLoading } = this.context;
    const { tabValue, toDate, fromDate, donViThucHien } = this.state;
    const { t, item } = this.props;
    try {

      setPageLoading(true);
      
      const searchObject = {
        assetId: item?.id,
        assetClass : appConst.assetClass.TSCD,
        toDate: toDate ? formatDateDtoMore(toDate) : null,
        fromDate: fromDate ? formatDateDtoMore(fromDate, "start") : null,
        unitId: donViThucHien ? donViThucHien?.id : null,
      };

      if(tabValue === appConst.historyTabs.allocation) {
        functionExportToExcel(exportToExceAllocationHistory, item?.id, t("allocation_asset.allocation_asset_histoty")+'.xlsx' );
      }
      if(tabValue === appConst.historyTabs.transfer) {
        functionExportToExcel(exportToExceTransferHistory, item?.id, t("AssetTransfer.asset_transfer_history")+'.xlsx' );
      }
      if(tabValue === appConst.historyTabs.repairing) {
        functionExportToExcel(exportToExceRepairHistory, searchObject, t("maintainRequest.repair_history")+'.xlsx' );
      }
      if(tabValue === appConst.historyTabs.maintainPlaning) {
        functionExportToExcel(exportToExceMaintenanceHistory, searchObject, t("maintainRequest.maintenance_history")+'.xlsx' );
      }
      if(tabValue === appConst.historyTabs.maintainCalibration) {

      }

    } catch (error) {
      console.log(error)
      toast.warning(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  }
  
  handleChange = (event, source) => {
    this.setState({[source]: event})
  }

  handleReset = () => {
    this.setState({
      toDate: null,
      fromDate: null,
      donViThucHien: null,
    })
  }
  render() {
    let { open, t, i18n, item } = this.props;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth={true}
        scroll={"paper"}
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          style={{
            overflowY: "auto",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            <span className="mb-20">{t("Asset.viewHistory")}</span>
          </DialogTitle>

          <DialogContent>
            <Grid container spacing={1} className="">
              <ScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                assetId={item?.id}
                handleChangePage={this.handleChangePage}
                setRowsPerPage={this.setRowsPerPage}
                handleChangePageChild={this.handleChangePageChild}
                setRowsPerPageChild={this.setRowsPerPageChild}
                setTotalElements={this.setTotalElements}
                setTotalElementsChild={this.setTotalElementsChild}
                setTabValue={this.setTabValue}
                handleChange={this.handleChange}
                handleReset={this.handleReset}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.close")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => this.exportToExcelAssetDetail()}
              >
                {t("general.exportToExcel")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

ViewAssetHistoryPopup.contextType = AppContext;
export default ViewAssetHistoryPopup;
