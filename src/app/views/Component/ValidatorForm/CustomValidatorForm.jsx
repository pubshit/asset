import React, { forwardRef, useImperativeHandle } from 'react';
import { ValidatorForm } from "react-material-ui-form-validator";
import PropTypes from "prop-types";
import {isInValidDateForSubmit} from "../../../appFunction";


//forwardRef truyền ref từ component cha xuống component con
const CustomValidatorForm = forwardRef((props, ref) => {
  const formRef = React.useRef(null);

  /*
    useImperativeHandle cho phép xuất một số phương thức từ component con ra ngoài,
    cho phép gọi các phương thức đó từ component cha.
    Điều này hữu ích khi muốn giao tiếp từ component con ra ngoài mà không cần thông qua props
  */
  useImperativeHandle(ref, () => ({
    onSubmit: () => {
      if (formRef.current) {
        formRef.current.submit();
      }
    }
  }),);

  const handleSubmit = (event) => {
    // Thêm hành động của bạn ở đây trước khi gọi handleSubmit của lớp cơ sở
    if (isInValidDateForSubmit()) {
      return;
    }

    // Gọi handleSubmit của lớp cơ sở (ValidatorForm)
    if (formRef.current) {
      props.onSubmit(event);
    }
  }

  return (
    <ValidatorForm {...props} ref={formRef} onSubmit={handleSubmit}>
      {props.children}
    </ValidatorForm>
  );
});

CustomValidatorForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
}
export default CustomValidatorForm;
