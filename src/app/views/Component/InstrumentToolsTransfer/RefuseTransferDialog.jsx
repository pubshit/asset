import React from "react";
import {
    Dialog,
    Button,
    DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import "../../../../styles/views/_loadding.scss";
import "react-toastify/dist/ReactToastify.css";

export default function RefuseTransferDialog(props) {
    let { t, open, item, handleClose, handleConfirmReason } = props
    return (
        <Dialog
            open={open}
            maxWidth="xs"
            fullWidth
            scroll="paper"
        >
            <ValidatorForm
                id="idRefuseTransfer"
                className="validator-form-scroll-dialog"
                onSubmit={() => handleConfirmReason(item)}
            >
                <DialogTitle
                    style={{ cursor: "move", paddingBottom: "0px" }}
                    id="draggable-dialog-title"
                >
                    <span className="">{t("InstrumentToolsTransfer.refuseTransfer")}</span>
                </DialogTitle>
                <DialogContent >
                    <TextValidator
                        multiline
                        // rowsMax={3}
                        className="w-100"
                        onChange={props?.handleChange}
                        type="text"
                        name="note"
                        defaultValue={props?.item?.note}
                        value={props?.item?.note}
                        validators={['required']}
                        errorMessages={t('general.required')}
                    />
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-12"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            {t("InstrumentToolsTransfer.confirm")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
}