import React, {useState} from "react";
import { Dialog, Button,DialogActions } from "@material-ui/core";
import {TextValidator} from "react-material-ui-form-validator";
import CustomValidatorForm from "../../Component/ValidatorForm/CustomValidatorForm";
import {useTranslation} from "react-i18next";
import {appConst} from "app/appConst";
import PropTypes from "prop-types";

const ConfirmationDuplicateAssetPopup = (props) => {
  let {t} = useTranslation();
  let {
    open,
    handleSelect = (item) => {},
    assetClass,
    handleClose = () => {},
  } = props;
  const assetType = appConst.assetType.find(type => type.code === assetClass);
  const [state, setState] = useState({
    quantity: "",
  });

  const handleChange = (e) => {
    setState({
      ...state,
      [e?.target?.name]: e?.target?.value,
    })
  }

  const handleSubmit = () => {
    handleSelect?.(state);
  }

  return (
    <Dialog
      maxWidth="xs"
      fullWidth={true}
      open={open}
      onClose={handleClose}
    >
      <CustomValidatorForm onSubmit={handleSubmit}>
        <div className="pt-24 px-20 pb-8">
          <h4 className="capitalize">Nhân bản</h4>
          <p>Nhập số lượng {assetType?.name} muốn nhân bản</p>
          <TextValidator
            fullWidth
            label={t("InstrumentsToolsReception.quantity")}
            name="quantity"
            type="number"
            value={state?.quantity}
            onChange={handleChange}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={handleClose}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.agree")}
              </Button>
            </div>
          </DialogActions>

        </div>
      </CustomValidatorForm>
    </Dialog>
  );
};

export default ConfirmationDuplicateAssetPopup;
ConfirmationDuplicateAssetPopup.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleSelect: PropTypes.func.isRequired,
  assetClass: PropTypes.number.isRequired,
}
