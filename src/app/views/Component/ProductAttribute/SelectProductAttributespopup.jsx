import { Grid, Button, InputAdornment, Input, Checkbox, TablePagination, Dialog, DialogActions } from "@material-ui/core";
import React from "react";
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from 'material-table';
import { searchByPage } from "../../ProductAttribute/ProductAttributeService";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import ProductAttributeEditorDialog from '../../ProductAttribute/ProductAttributeEditorDialog'
import { appConst } from "app/appConst";
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class SelectProductAttributespopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: '',
    shouldOpenProductDialog: false,
    attributes: [],
    shouldOpenProductAttributeDialog: false
  };

  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim() || null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      // nếu id của 5 cái này có trong properties thì sẽ thay trạng thái isCheck bằng true
      let itemListClone = [...data.content]
      itemListClone.map(item => {
        item.isCheck = false;
        // eslint-disable-next-line no-unused-expressions
        this.state?.attributes?.forEach(attribute => {
          if (attribute?.attribute?.id === item?.id) {
            item.isCheck = true;
          }
        })
      })
      this.setState({ itemList: [...itemListClone], totalElements: data.totalElements })
    });
  }

  componentDidMount() { }

  handleClick = (event, item) => {
    item.isCheck = event.target.checked;
    let { attributes } = this.state

    if (item?.isCheck) {
      let check = attributes.find(attribute => attribute?.attribute?.id === item?.id);
      if (!check?.isCheck) {
        let itemAttribute = {};
        itemAttribute.attribute = item;
        attributes = attributes.concat(itemAttribute);
      }
    } else {
      attributes = attributes.filter(attribute => attribute?.attribute?.id !== item?.id)
    }
    this.setState({ attributes: attributes });
  }

  componentWillMount() {
    let { attributes } = this.props;
    this.setState({ attributes: attributes },
      () => this.updatePageData()
    );
  }

  handleKeyDownEnterSearch = e => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  handleKeyUp = e => {
    this.search()
  }

  search() {
    this.setPage(0);
  };

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true
    })
  }

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false
    })
  }

  handleOpenProductAttributeDialog = () => {
    this.setState({
      shouldOpenProductAttributeDialog: true
    })
  }

  handleDialogProductAttributeClose = () => {
    this.setState({
      shouldOpenProductAttributeDialog: false
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
      shouldOpenProductAttributeDialog: false
    }, () => {
      this.updatePageData();
    });
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  }

  render() {
    const { t, i18n, handleClose, handleSelect, open } = this.props;
    let { keyword, itemList, attributes, shouldOpenProductAttributeDialog } = this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "center",
        width: "60px",
        cellStyle: {
          paddingTop: '0px',
          paddingBottom: '0px'
        },
        render: rowData => <Checkbox id={`radio${rowData.id}`} name="radSelected" value={rowData.id}
          checked={rowData.isCheck}
          onClick={(event) => this.handleClick(event, rowData)}
        />
      },
      { title: t("ProductAttribute.code"), field: "code", align: "left", width: "180px" },
      { title: t("ProductAttribute.name"), field: "name", width: "150" },
    ];
    return (
      <Dialog onClose={handleClose} open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t("ProductAttribute.title")}</span>
        </DialogTitle>
        <DialogContent>
          <Grid item xs={12}>
            <Input
              label={t('general.enterSearch')}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDownEnterSearch}
              style={{ width: '50%' }}
              className="mb-16 mr-12"
              id="search_box"
              placeholder={t('general.enterSearch')}
              startAdornment={
                <InputAdornment >
                  <Link to="#"> <SearchIcon
                    onClick={() => this.search(keyword)}
                    style={{
                      position: "absolute",
                      top: "0",
                      right: "0"
                    }} /></Link>
                </InputAdornment>
              }
            />
            {itemList.length !== 0 ? '' :
              <Button className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleOpenProductAttributeDialog()}
              >
                {t('ProductAttribute.add')}
              </Button>
            }
          </Grid>
          <div>
            {shouldOpenProductAttributeDialog && (
              <ProductAttributeEditorDialog t={t} i18n={i18n}
                handleClose={this.handleDialogProductAttributeClose}
                open={shouldOpenProductAttributeDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={{
                  name: keyword
                }}
              />
            )}
          </div>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF'
                }),
                maxBodyHeight: '253px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                minBodyHeight: "253px"
              }}
              components={{
                Toolbar: props => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}>{t('general.cancel')}</Button>
          <Button className="mb-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => handleSelect(attributes)}>
            {t('general.select')}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
export default SelectProductAttributespopup;