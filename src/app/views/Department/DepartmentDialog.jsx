import {
  Grid,
  DialogActions,
  Button,
  Checkbox,
  Dialog,
  FormControlLabel,
} from "@material-ui/core";
import React from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { checkCode, getListOfDepartmentAssets, searchByPage } from "./DepartmentService";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import {
  saveItem,
  checkParent,
} from "./DepartmentService";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import {appConst, STATUS_DEPARTMENT} from "app/appConst";
import {
  handleKeyDownFloatOnly,
  handleThrowResponseMessage,
  isSuccessfulResponse,
  isTypeNumber,
} from "app/appFunction";
import {getRoleCategory} from "app/appFunction";
import {PaperComponent} from "../Component/Utilities";
import {getUserByDepartment} from "../User/UserService";

toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
});

class DepartmentDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    id: null,
    rowsPerPage: 50,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenDepartmentPopup: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    parent: "",
    type: 0,
    keyword: "",
    Notification: "",
    isAssetManagement: false,
    viewIndex: null,
    count: 1,
    loading: false,
    listParentDepartment: [],
    isActive: STATUS_DEPARTMENT.KHONG_HOAT_DONG.code,
    permissions: {
      departmentCode: {
        isDisable: false
      }
    },
    listUser: [],
    headOfDepartment: null,
    secondarySignatory: null,
    deputyDepartment: null,
  };

  handleFormSubmit = async (isClose) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let { id, name, code, parent, isAssetManagement, viewIndex, count, isActive } = this.state;
    if (count > 1) {
      return this.props.handleClose();
    }
    setPageLoading(true);
    checkCode(id, code).then(async (data) => {
      if (data?.data) {
        toast.warning(t("Department.noti.dupli_code"));
        toast.clearWaitingQueue();
        setPageLoading(false);
        return;
      }

      let sendData = {
        id,
        name,
        code,
        parent: parent || null,
        isAssetManagement,
        viewIndex,
        isActive,
        headOfDepartmentId: this.state.headOfDepartment?.personId,
        headOfDepartmentName: this.state.headOfDepartment?.displayName,
        deputyDepartmentId: this.state.deputyDepartment?.personId,
        deputyDepartmentName: this.state.deputyDepartment?.displayName,
        secondarySignatoryId: this.state.secondarySignatory?.personId,
        secondarySignatoryName: this.state.secondarySignatory?.displayName,
      }
      this.handleCheckParent(sendData, this.handleSaveItem).then(() => {
        setPageLoading(false);
      });
    }).catch(err => {
      console.log(err)
      setPageLoading(false);
      toast.error(t("general.error"));
    });
  };

  handleCheckParent = async (data, callback = () => { }) => {
    let { t } = this.props;
    try {
      let res = await checkParent(data);

      if (res.data) {
        return toast.error(t("Department.noti.updateFailParent"));
      } else if (isTypeNumber(res?.data?.code) && !isSuccessfulResponse(res?.data?.code)) {
        return handleThrowResponseMessage(res);
      }

      await callback(data);
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  handleSaveItem = async (data) => {
    let { t } = this.props;
    try {
      let res = await saveItem(data);

      if (res?.data && res?.data?.id) {
        toast.success(t("general.success"));
        this.props.handleClose();
      } else {
        handleThrowResponseMessage(res);
      }

    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null });
    }
  };
  componentDidMount() {
    const { item = {} } = this.props;
    let { permissions } = this.state;
    if (item?.parent) {
      item.parent.text = item?.parent?.name
    }
    if (item?.id) {
      permissions.departmentCode.isDisable = true;
      this.setState({ permissions });
    }
    this.setState({
      ...item,
      headOfDepartment: item?.headOfDepartmentId
        ? {
          personId: item.headOfDepartmentId,
          displayName: item?.headOfDepartmentName,
        }
        : null,
      secondarySignatory: item?.secondarySignatoryId
        ? {
          personId: item.secondarySignatoryId,
          displayName: item?.secondarySignatoryName,
        }
        : null,
      deputyDepartment: item?.deputyDepartmentId
        ? {
          personId: item.deputyDepartmentId,
          displayName: item?.deputyDepartmentName,
        }
        : null,
      parentId: item?.parent?.id,
    });
  }

  handleChange(event, source) {
    let { item } = this.props;
    // debugger
    if (source === "isAssetManagement") {
      this.setState({ isAssetManagement: event.target.checked });
      return;
    }
    if (source === 'isActive') {
      let { item } = this.props;
      if (item?.isActive) {
        getListOfDepartmentAssets(item.id).then((res) => {
          if (res?.data === true) {
            this.setState({ shouldOpenNotificationPopup: true });
          } else {
            if (this.state.isActive) {
              this.setState({ isActive: STATUS_DEPARTMENT.KHONG_HOAT_DONG.code });
            } else {
              this.setState({ isActive: STATUS_DEPARTMENT.HOAT_DONG.code });
            }
          }
        }).catch(err => { })
      } else {
        this.setState({ isActive: event.target.checked ? STATUS_DEPARTMENT.HOAT_DONG.code : STATUS_DEPARTMENT.KHONG_HOAT_DONG.code });
      }
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleSelectDepartment = (parent = {}) => {
    let { item, t } = this.props;
    if (
      item?.id
      && (
        item?.id === parent?.id
        || parent?.parent?.id === item?.id
        || parent?.parent === item?.id
      )
    ) {
      toast.warning(t("ProductCategory.noti.updateFailParent"))
    }
    else {
      if (parent) {
        parent.parent = null;
        parent.name = parent?.text
      }
      this.setState({ parent }, function () {
        this.handleDepartmentPopupClose();
      });
    }
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleSelect = (value, name) => {
    this.setState({[name]: value});
  };

  render() {
    let parentDepartmentSearchObject = { pageIndex: 0, pageSize: 1000000 };
    const {
      t,
      open,
    } = this.props;
    let {
      parent,
      name,
      code,
      isAssetManagement,
      shouldOpenNotificationPopup,
      viewIndex,
      loading,
      permissions,
      headOfDepartment,
      secondarySignatory,
      deputyDepartment,
    } = this.state;
    let usersSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: this.state.id,
    }
    return (
      <Dialog open={open} PaperComponent={PaperComponent} className="wrapper">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onNoClick={this.handleDialogClose}
            text={t('Department.messageWarning')}
            cancel={t("general.cancel")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t("Department.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("Department.parent")}
                  searchObject={parentDepartmentSearchObject}
                  searchFunction={searchByPage}
                  value={parent ?? null}
                  defaultValue={parent ?? null}
                  displayLable="text"
                  onSelect={this.handleSelectDepartment}
                  noOptionsText={t("general.noOption")}
                  disabled={getRoleCategory().isRoleAssetManager}
                />
              </Grid>

              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Department.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name || ""}
                  validators={["required", "matchRegexp:^(?!.*\\.\\.).*$"]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Department.code")}
                    </span>
                  }
                  disabled={permissions.departmentCode.isDisable}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code || ""}
                  validators={["required", "matchRegexp:^(?!.*\\.\\.).*$"]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("AssetGroup.viewIndex")}
                    </span>
                  }
                  onChange={this.handleChange}
                  onKeyDown={handleKeyDownFloatOnly}
                  type="number"
                  name="viewIndex"
                  value={viewIndex || ""}
                  validators={["required", "minFloat: 0.0000001", "matchRegexp:^(?!.*\\.\\.).*$"]}
                  errorMessages={[t("general.required"), t("general.minNumberError"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("Department.headOfDepartment")}
                  searchFunction={getUserByDepartment}
                  searchObject={usersSearchObject}
                  listData={this.state.listUser}
                  setListData={this.handleSelect}
                  nameListData="listUser"
                  displayLable="displayName"
                  typeReturnFunction="listData"
                  name="headOfDepartment"
                  value={headOfDepartment || null}
                  onSelect={this.handleSelect}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("Department.deputyDepartment")}
                  searchFunction={getUserByDepartment}
                  searchObject={usersSearchObject}
                  listData={this.state.listUser}
                  setListData={this.handleSelect}
                  nameListData="listUser"
                  displayLable="displayName"
                  typeReturnFunction="category"
                  name="deputyDepartment"
                  value={deputyDepartment || null}
                  onSelect={this.handleSelect}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("Department.secondarySignatory")}
                  searchFunction={getUserByDepartment}
                  searchObject={usersSearchObject}
                  listData={this.state.listUser}
                  setListData={this.handleSelect}
                  nameListData="listUser"
                  displayLable="displayName"
                  typeReturnFunction="category"
                  name="secondarySignatory"
                  value={secondarySignatory || null}
                  onSelect={this.handleSelect}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControlLabel
                  value={isAssetManagement}
                  className="mt-15"
                  name="isAssetManagement"
                  onChange={(isAssetManagement) =>
                    this.handleChange(isAssetManagement, "isAssetManagement")
                  }
                  control={<Checkbox checked={isAssetManagement} />}
                  label={t("Department.isAssetManagement")}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControlLabel
                  name="isActive"
                  onChange={(isActive) =>
                    this.handleChange(isActive, "isActive")
                  }
                  control={<Checkbox checked={this.state.isActive ? true : false}

                  />}
                  label={`${t('Department.status')}: ${this.state.isActive ? t("Department.active") : t("Department.unactive")}`}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                disabled={loading}
                className="mr-15"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
DepartmentDialog.contextType = AppContext;
export default DepartmentDialog;
