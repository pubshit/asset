import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
import { getTheHighestRole } from "../../appFunction";
const API_PATH = ConstantList.API_ENPOINT + "/api/department";
const API_PATH_ASSET =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_DEPARTMENT = ConstantList.API_ENPOINT_ASSET_MAINTANE +  "/api/departments";

export const checkCode = (id, code) => {
  // var API_PATH = ConstantList.API_ENPOINT + "/api/department";
  const config = { params: { id: id, code: code } };
  var url = API_PATH_ASSET + "/checkCode";
  return axios.get(url, config);
};

export const searchByPageDeparment = (searchObject) => {
  var url = API_PATH_ASSET + "/searchByPageDeparment";
  return axios.post(url, searchObject);
};
//hàm cũ
export const searchByPage = (searchObject) => {
  var url = API_PATH_ASSET + "/searchByPage";
  return axios.post(url,
    {isActive:STATUS_DEPARTMENT.HOAT_DONG.code,...searchObject});
};
//admin
export const searchByPageAndAdmin = (searchObject) => {
  var url = API_PATH_ASSET + "/searchByPage/admin";
  return axios.post(url, searchObject);
};

export const searchParentByPage = (searchObject) => {
  var url = API_PATH_ASSET + "/searchParentByPage";
  return axios.post(url, searchObject);
};

export const getAll = () => {
  var url = API_PATH + "/all";
  return axios.get(url);
};

export const getTreeView = () => {
  // var url = API_PATH + "/tree/1/10000000";
  // return axios.get(url);
  var searchObject = {};
  searchObject.page = 1;
  searchObject.pageSize = 10000000;
  var url = API_PATH_ASSET + "/searchParentByPage";
  return axios.post(url, searchObject);
};

export const getByPage = (page, pageSize) => {
  var searchObject = {};
  searchObject.page = page;
  searchObject.pageSize = pageSize;
  var url = API_PATH_ASSET + "/searchByPageDeparment";
  return axios.post(url, searchObject);
};

export const getItemById = (id) => {
  var url = API_PATH_ASSET + "/findDepartmentById/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  var API_PATH = API_PATH_ASSET + "/delete";
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = (item) => {
  return axios.post(API_PATH_ASSET, item);
};
export const checkParent = (dto) => {
  var url = API_PATH_ASSET + "/checkParent";
  return axios.post(url, dto);
};

export const getListManagementDepartment = () => {
  let url = API_PATH_ASSET + "/getListManagementDepartment";
  return axios.get(url);
};
export const exportExampleImportExcelDeparment = (department) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/fileDownload/exportExampleDeparment",
    data: department,
    responseType: "blob",
  });
};

export const getUserDepartmentAll = (searchObject) => {
  let url = API_PATH_ASSET + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getListOfDepartmentAssets = (id) => {
  var url =
    ConstantList.API_ENPOINT +
    "/api/assetDepartment/check-department-have-asset/" +
    id;
  return axios.get(url);
};

export const getAllManagementDepartmentByOrg = () => {
  let config = { params: {isActive:STATUS_DEPARTMENT.HOAT_DONG.code } }
  let url = API_PATH_ASSET + "/management-departments";
  return axios.get(url,config);
};

export const searchByPageDepartmentNew = (params) => {
  let config = {
    params: {
      ...params,
      isActive:STATUS_DEPARTMENT.HOAT_DONG.code
    }
  }
  let url = API_PATH_DEPARTMENT + "/search-by-page";
  return axios.get(url,config);
};
export const searchByPageDepartmentByOrg = (payload) => {
  const { organization } = getTheHighestRole();
  let config = {
    params: {
      pageIndex: 0,
      pageSize: 1000000,
      isActive:STATUS_DEPARTMENT.HOAT_DONG.code,
      orgId: organization?.org?.id,
      ...payload
    }
  }
  let url = API_PATH_DEPARTMENT + "/search-by-page";
  return axios.get(url,config);
};
