import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination, FormControl, Input, InputAdornment, TextField
} from "@material-ui/core";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import { useTranslation, } from "react-i18next";
import {
  deleteItem,
  getItemById,
  searchParentByPage,
  exportExampleImportExcelDeparment
} from "./DepartmentService";
import DepartmentDialog from "./DepartmentDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { Helmet } from "react-helmet";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import ImportExcelDialog from "./ImportExcelDialog";
import { toast } from "react-toastify";
import SearchIcon from "@material-ui/icons/Search";
import "react-toastify/dist/ReactToastify.css";
import AppContext from "app/appContext";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import {
  functionExportToExcel,
  getRoleCategory,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "app/appFunction";
import {STATUS_DEPARTMENT, appConst, DEFAULT_TOOLTIPS_PROPS} from "../../appConst";
import {GroupActionIcons, LightTooltip} from "../Component/Utilities";

toast.configure({
  autoClose: 1500,
  draggable: false,
  limit: 3,
});

const style = {
  absolute: {
    position: "relative"
  },
  autocompletePosition: {
    position: "absolute",
    top: -15
  }
}


class Department extends Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationDeleteListDialog: false,
    shouldOpenNotificationPopup: false,
    keyword: "",
    Notification: "",
    shouldOpenImportExcelDialog: false,
    isActive: {},
    isRoleAssetManager: getRoleCategory().isRoleAssetManager
  };
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }

  search() {
    var searchObject = {};
    this.setState({ page: 0 }, () => {
      searchObject.keyword = this.state.keyword;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.isActive = this.state.isActive?.value;
      searchParentByPage(searchObject).then(({ data }) => {
        var treeValues = [];

        let itemListClone = [...data.content];

        itemListClone.forEach((item) => {
          var items = this.getListItemChild(item, this.state.isActive?.value);
          treeValues.push(...items);
        });
        this.setState({
          itemList: this.state.keyword ? itemListClone : treeValues,
          totalElements: data.totalElements,
        });
      });
    });
  }

  checkData = () => {
    let { t } = this.props;
    if (!this.data || this.data.length === 0) {
      toast.warning(t("general.noti_check_data"));
      // alert('Chưa chọn dữ liệu')
    } else if (this.data.length === this.state.itemList.length) {
      // alert("Bạn có muốn xoá toàn bộ");

      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    } else {
      this.setState({ shouldOpenConfirmationDeleteListDialog: true });
    }
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  updatePageData = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.isActive = this.state.isActive?.value;
    setPageLoading(true);
    searchParentByPage(searchObject).then(({ data }) => {
      if (data) {
        var treeValues = [];

        let itemListClone = [...data?.content];

        itemListClone.forEach((item) => {
          var items = this.getListItemChild(item, this.state.isActive?.value);
          treeValues.push(...items);
        });
        this.setState({
          itemList: this.state.keyword ? itemListClone : treeValues,
          totalElements: data?.totalElements,
        });
      }
      setPageLoading(false);
    }).catch(err => {
      toast.error(t("general.error"));
      setPageLoading(false);
    })
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  exportExampleImportExcel = async () => {
    try {
      await functionExportToExcel(
        exportExampleImportExcelDeparment,
        {},
        "Mẫu excel danh mục phòng ban.xlsx"
      )
    } catch (e) {
      console.error(e);
    }
  }

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.setPage(0);
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    });
    this.updatePageData();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenImportExcelDialog: false
    });
    this.setPage(0);
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    deleteItem(this.state.id)
      .then((res) => {
        if (res?.data) {
          toast.success(t("general.deleteSuccess"));
        } else {
          toast.warning(t("Department.noti.use"));
        }
        this.handleDialogClose();
        this.updatePageData();
      })
      .catch((err) => {
        toast.warning(t("Department.noti.use"));
      });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props;
    for (var i = 0; i < list.length; i++) {
      try {
        await deleteItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    this.handleDialogClose();
    if (listAlert.length === list.length) {
      toast.warning(t("Department.noti.use_all"));
    } else if (listAlert.length > 0) {
      toast.warning(t("Department.noti.deleted_unused"));
    } else {
      toast.info(t("general.deleteSuccess"));
    }
  }
  handleDeleteAll = (event) => {
    if (this.data != null) {
      this.handleDeleteList(this.data).then(() => {
        this.data = null;
      });
    } else {
      this.handleDeleteList(this.data).then(() => {
        this.updatePageData();
        this.data = null;
      });
    }
  };
  getListItemChild(item, isActiveChild) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.id = item.id;
    root.parentId = item.parentId;
    root.isActive = item.isActive;
    result.push(root)
    if (item.children) {
      if ([STATUS_DEPARTMENT.KHONG_HOAT_DONG.code, STATUS_DEPARTMENT.HOAT_DONG.code].includes(isActiveChild)) {
        item.children.filter(i => i.isActive === isActiveChild)?.forEach((child) => {
          var childs = this.getListItemChild(child);
          result.push(...childs)
        })
      } else {
        item.children.forEach((child) => {
          var childs = this.getListItemChild(child);
          result.push(...childs)
        });
      }
    }
    return result;
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  searchByStatus = (e, value) => {
    this.setState((preState) => ({ ...preState, isActive: value }), () => {
      this.search();
    });
  }
  
  handleEdit = async (rowData) => {
    let { setPageLoading } = this.context;
    const { t } = this.props;
    
    try {
      setPageLoading(true);
      const res = await getItemById(rowData?.id);
      
      if (isSuccessfulResponse(res?.status)) {
        if (res?.data?.parent === null) {
          res.data.parent = {};
        }
        
        this.setState({
          item: res?.data,
          shouldOpenEditorDialog: true,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      shouldOpenNotificationPopup,
      shouldOpenImportExcelDialog,
      keyword,
      isRoleAssetManager
    } = this.state;
    let TitlePage = t("Department.title");
    const filterAutocomplete = createFilterOptions();
    let columns = [
      ...(isRoleAssetManager ? [] :
        [{
          title: t("general.action"),
          field: "custom",
          align: "left",
          maxWidth: 120,
          cellStyle: {
            textAlign: "center",
          },
          headerStyle: {
            textAlign: "center",
          },
          render: (rowData) => (
            <GroupActionIcons
              editIcon
              deleteIcon
              item={rowData}
              onSelect={(rowData, method) => {
                if (method === appConst.active.edit) {
                  this.handleEdit(rowData);
                } else if (method === 1) {
                  this.handleDelete(rowData.id);
                } else {
                  alert("Call Selected Here:" + rowData.id);
                }
              }}
            />
          )
        }]
      ),
      {
        title: t("Department.code"),
        field: "code",
        align: "left",
        width: 150,
        cellStyle: {
          textAlign: "center",
        }
      },
      {
        title: t("Department.name"),
        field: "name",
        minWidth: 450,
        render: rowData => rowData?.name
      },
      {
        title: t("Department.status"),
        field: "isActive",
        width: "150",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.isActive === STATUS_DEPARTMENT.HOAT_DONG.code
          ? t("Department.active")
          : t("Department.unactive")
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.category"), path: "/list/department" },
              { name: TitlePage },
            ]}
          />
        </div>
        <Grid container spacing={2}>
          <Grid item md={6} xs={12}>
            {!isRoleAssetManager && (
              <Button
                className="mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => {
                  this.handleEditItem({
                    startDate: new Date(),
                    endDate: new Date(),
                  });
                }}
              >
                {t("general.add")}
              </Button>
            )}
            {!isRoleAssetManager && (
              <Button
                className="mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.importExcel}
              >
                {t("general.importExcel")}
              </Button>
            )}
            {!isRoleAssetManager && (
              <Button
                className=" align-bottom"
                variant="contained"
                color="primary"
                onClick={this.exportExampleImportExcel}
              >
                Mẫu Excel
              </Button>
            )}

            {shouldOpenImportExcelDialog && (
              <ImportExcelDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={shouldOpenImportExcelDialog}
                handleOKEditClose={this.handleOKEditClose}
              />
            )}

            {this.state.shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={this.state.shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t("general.deleteAllConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}

            {shouldOpenNotificationPopup && (
              <NotificationPopup
                title={t("general.noti")}
                open={shouldOpenNotificationPopup}
                onYesClick={this.handleDialogClose}
                text={t(this.state.Notification)}
                agree={t("general.agree")}
              />
            )}

            {this.state.shouldOpenConfirmationDeleteListDialog && (
              <ConfirmationDialog
                title={t("general.confirm")}
                open={this.state.shouldOpenConfirmationDeleteListDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t("general.deleteConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            <div style={{ ...style.absolute }}>
              <Autocomplete
                style={{ ...style.autocompletePosition }}
                className="search_box w-100 statusFilter"
                fullWidth
                options={[
                  {
                    name: t('Department.unactive'),
                    value: 0
                  },
                  {
                    name: t('Department.active'),
                    value: 1
                  },

                ]}
                value={this.state.isActive}
                onChange={(e, value) => this.searchByStatus(e, value)}
                getOptionLabel={(option) => option?.name || ""}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label={t('Department.filterStatus')}
                    variant="standard"
                  />
                )}
                noOptionsText={t("general.noOption")}
              />
            </div>
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("Department.filter")}
                id="search_box"
                value={keyword || ""}
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.search} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {this.state.shouldOpenEditorDialog && (
                <DepartmentDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={this.state.item}
                />
              )}

              {this.state.shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={this.state.shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={this.state.itemList}
              labelRowsPerPage={t("general.rows_per_page")}
              columns={columns}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              labelRowsPerPage={t("general.rows_per_page")}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={this.state.page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
Department.contextType = AppContext;
export default Department;
