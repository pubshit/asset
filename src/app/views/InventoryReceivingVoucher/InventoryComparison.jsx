import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  Input,
  Checkbox,
  TablePagination,
  InputAdornment,
  DialogContent
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
import Draggable from 'react-draggable';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { updatePurchasePlaningById } from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningService";
import {searchByPage as searchByPagePurchasePlaning} from '../SuppliesPurchasePlaning/SuppliesPurchasePlaningService';
import AppContext from "app/appContext";
import {PaperComponent} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
});

class InventoryComparison extends Component {
  state = {
    id:"",
    name: "",
    code: "",
    indexOrder:"",
    isActive: false,
    shouldOpenNotificationPopup:false,
    Notification:"",
    keyword: '',
    productListReceivingVoucher: [],
    productListPurchasePlaning: [],
    purchasePlaning: null,
    totalElements: 0,
    rowsPerPage: 50,
    page: 0,
    totalElementsPurchase: 0,
    rowsPerPagePurchase: 50,
    pagePurchase: 0,
    status: null,
    listStatus:[],
    note: '',
    selectedValue: null,
    purchasePlaningType: 2, // kế hoạch mua sam vật tư
  };

  updateDataPuchasePlaning =()=> {
    // debugger
    let { id } = this.state;
    let {purchasePlaning} = this.state;
    if(purchasePlaning === null || !purchasePlaning.id ) {
      toast.warn("Chưa chọn kế hoạch mua sắm.");
      toast.clearWaitingQueue();
      return;
    }
    console.log(purchasePlaning)
    if (purchasePlaning.id) {
      purchasePlaning.status = this.state.status;
      purchasePlaning.note = this.state.note;
      updatePurchasePlaningById({...purchasePlaning}, purchasePlaning.id).then(data => {
        if(data.data) {
          toast.info("Cập nhật thành công.")
        } else {
          toast.warn("Có lỗi xảy ra.");
        }
      })
    }
  }

  componentWillMount() {
    this.updateData();
  }

  updateData = () => {
    let {purchasePlaning} = this.state;
    let {t} = this.props;
    let {setPageLoading} = this.context;
    let searchObjectStatusPurchase = { pageIndex: 0, pageSize: 1000000 }
    let searchObjectReceiving={};
    searchObjectReceiving.keyword = this.state.keyword.trim();
    searchObjectReceiving.pageIndex = this.state.page + 1;
    searchObjectReceiving.pageSize = this.state.rowsPerPage;

    let searchObjectPurchase={};
    searchObjectPurchase.keyword = this.state.keyword.trim();
    searchObjectPurchase.pageIndex = this.state.pagePurchase + 1;
    searchObjectPurchase.pageSize = this.state.rowsPerPagePurchase;
    searchObjectPurchase.productId = this.state.selectedValue;

    if(purchasePlaning) {
      searchObjectReceiving.purchasePlaningId = purchasePlaning.id;
    }
    
    // if(purchasePlaning) {
    //   searchObjectPurchase.purchasePlaningId = purchasePlaning.id;
    // }    
    // searchProductInVoucherPurchaseByPage(searchObjectPurchase).then(({data}) => {
    //   if(data?.data?.content) {
    //     this.setState({
    //       productListPurchasePlaning : data?.data?.content, 
    //       totalElementsPurchase: data?.data?.totalElements
    //     })
    //   }      
    // })
    
    // getAllStatusPurchase(searchObjectStatusPurchase).then(({data})=>{
    //   this.setState({
    //     listStatus:[...data?.data?.content],
    //   })     
    // })
  };

  setPage = page => {
    this.setState({ page }, function () {
      this.updateData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updateData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleChangePagePurchase = (event, newPage) => {
    this.setPagePurchase(newPage);
  };

  setPagePurchase = pagePurchase => {
    this.setState({ pagePurchase }, function () {
      this.updateData();
    })
  };

  setRowsPerPagePurchase = event => {
    this.setState({ rowsPerPagePurchase: event.target.value, pagePurchase: 0 }, function () {
      this.updateData();
    });
  };

  selectPurchasePlaning = (item) => {
    if(item) {
      this.setState({ purchasePlaning: item, status: item.status, note: item.note ? item.note: "", selectedValue: null }, function () {
        this.updateData()
      });    
    } else {
      this.setState({ purchasePlaning: item, status: '', note: "" }, function () {
        this.updateData()
      });    
    }
  }

  selectStatus = (item) => {
    this.setState({ status: item }, function () {
    });    
  }

  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
      this.updateData();
    })
  };

  handleChange = (event, source) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleClick = (event, item) => {
    let {selectedValue} = this.state;

    if(selectedValue == item.id) {
      this.setState({ selectedValue: null, selectedItem: null }, ()=> this.updateData());   
      return;   
    }

    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item }, () => this.updateData());      
      return;
    } 
  }

  render() {
    let {
      id,
      name,
      code,
      indexOrder,
      shouldOpenNotificationPopup,
      productListReceivingVoucher,
      productListPurchasePlaning,
      purchasePlaningType,
      purchasePlaning,
      totalElements,
      rowsPerPage,
      page,
      keyword,
      totalElementsPurchase,
      rowsPerPagePurchase,
      pagePurchase,
      note
    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n, item } = this.props;
    let searchObjectPurchasePlaning = {pageIndex: 0, pageSize: 1000000, type: purchasePlaningType}
    let columnsReceivingVoucher = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "80px",
        cellStyle:{
          paddingLeft:'20px'
        },
        render: rowData => <Checkbox style={{padding:'0px'}} id={`radio${rowData.id}`} name="radSelected" value={rowData.id} checked={this.state.selectedValue === rowData.id} onClick={(event) => this.handleClick(event, rowData)}
        />
      },
      { title: t("Product.code"), field: "code", width: "100",
        headerStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',
          textAlign:'left',
          minWidth: '120px'
        },
        cellStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',          
          textAlign:'left'
        },
      },
      { title: t("Product.name"), field: "name", width: "180",
        headerStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',
          textAlign:'left'
        },
        cellStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',          
          textAlign:'left'
        },
      },
      { title: t("Product.quantity"), field: "remainingQuantity", width: "180", align: 'right',
        headerStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',
        },
        cellStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',    
        },
        render: (rowData) => {
          let number = new Number(rowData.remainingQuantity);
          if (number != null) {
            let plainNumber = number.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      },
      { title: t("Product.price"), field: "price", width: "180", align: 'right',
        headerStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',
        },
        cellStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',    
        },
        render: (rowData) => {
          let number = new Number(rowData.price);
          if (number != null) {
            let plainNumber = number.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      }
    ];

    let columnsPurchasePlaning = [
      { title: t("Product.code"), field: "code", width: "100",
        headerStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',
          textAlign:'left',
          minWidth: '120px'
        },
        cellStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',          
          textAlign:'left'
        },
      },
      { title: t("Product.name"), field: "name", width: "180",
        headerStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',
          textAlign:'left'
        },
        cellStyle: {
          paddingLeft: '10px',
          paddingRight: '0px',          
          textAlign:'left'
        },
      },
      { title: t("Product.quantity"), field: "remainingQuantity", width: "180", align: 'right',
        headerStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',
        },
        cellStyle: {
          paddingLeft: '0px',
          paddingRight: '10px',    
        },
        render: (rowData) => {
          let number = new Number(rowData.remainingQuantity);
          if (number != null) {
            let plainNumber = number.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            return plainNumber.substr(0, plainNumber.length - 2);
          }
        },
      }
    ]

    return (
      <Dialog   open={open}  PaperComponent={PaperComponent} maxWidth="lg" fullWidth>
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <h4 className="">{t("So sánh vật tư")}</h4>
        </DialogTitle>          
        <ValidatorForm>
        <DialogContent>
        
        <Grid className="" container spacing={1}>   
          <Grid item md={3} sm={12} xs={12} className="">
            <FormControl fullWidth>
              <Input
                className='search_box w-100 mt-16'
                onKeyUp={this.handleTextChange}
                // onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("Tìm kiếm theo tên và mã sản phẩm")}
                id="search_box"
                startAdornment={
                  <InputAdornment >
                    <Link> <SearchIcon
                      // onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>       
          <Grid item md={3} sm={12} xs={12} className="">
            <AsynchronousAutocomplete
              label={t('Chọn kế hoạch mua sắm')}
              className="w-100 mb-6"
              searchFunction={searchByPagePurchasePlaning}
              searchObject={searchObjectPurchasePlaning}
              defaultValue={purchasePlaning}
              displayLable={'name'}
              value={purchasePlaning? purchasePlaning : null}
              onSelect={this.selectPurchasePlaning}
            />
          </Grid>
          
          <Grid item md={2} sm={12} xs={12} className="">  
            <Autocomplete                  
              size="small"
              style={{marginTop:'3px'}}
              id="combo-box"
              options={this.state?.listStatus}
              value={this.state.status}
              renderInput={(params) => <TextValidator {...params}
                label={<span><span className="colorRed">*</span>{t('Chọn trạng thái cập nhật')}</span>}
                value = {this.state.status}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              onChange={(event,value)=>{
                this.selectStatus(value);
              }}
            />
          </Grid>
          <Grid className="" item md={3} sm={12} xs={12}>
            <TextValidator
              className="w-100 "
              label={t("maintainRequest.note")}
              onChange={this.handleChange}
              type="text"
              name="note"
              value={note}
            />
          </Grid>    
          <Grid item md={1} sm={12} xs={12} className="">
            <Button
              variant="contained"
              className="mt-12 w-100"
              size='small'
              color="secondary"
              onClick={this.updateDataPuchasePlaning}
            >
              {t('Cập nhật')}
            </Button>   
          </Grid>      
          
          <Grid item md={6} sm={12} xs={12} className="">
            <MaterialTable
              title={t('Danh sách vật tư nhập kho')}
              data={productListReceivingVoucher}
              columns={columnsReceivingVoucher}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              parentChildData={(row, rows) => {
                var list = rows.find(a => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF'
                }), 
                maxBodyHeight: '380px',
                minBodyHeight: '380px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color:'#fff',
                },
                padding: 'dense',
                toolbar: true
              }}

              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12} className="">
            <MaterialTable
              title={t('Danh sách vật tư kế hoạch mua sắm')}
              data={productListPurchasePlaning}
              columns={columnsPurchasePlaning}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              parentChildData={(row, rows) => {
                var list = rows.find(a => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF'
                }), 
                maxBodyHeight: '380px',
                minBodyHeight: '380px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color:'#fff',
                },
                padding: 'dense',
                toolbar: true
              }}

              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElementsPurchase}
              rowsPerPage={rowsPerPagePurchase}
              page={pagePurchase}
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onChangePage={this.handleChangePagePurchase}
              onChangeRowsPerPage={this.setRowsPerPagePurchase}
            />
          </Grid>          
        </Grid>         
        
        </DialogContent>
        </ValidatorForm>
        <DialogActions>
          <div className="flex flex-space-between flex-middle mt-12">
            <Button
              variant="contained"
              className="mr-12"
              color="secondary"
              onClick={() => this.props.handleClose()}
            >
              {t('general.cancel')}
            </Button>         
          </div>
      </DialogActions>
      </Dialog>
    );
  }
}

InventoryComparison.contextType = AppContext;
export default InventoryComparison;
