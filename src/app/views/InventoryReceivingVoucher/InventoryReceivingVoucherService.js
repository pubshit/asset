import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_purchase_planing =
  ConstantList.API_ENPOINT + "/api/purchase_planing" + ConstantList.URL_PREFIX;
const API_PATH_API_ENPOINT_ASSET_MAINTANE = ConstantList.API_ENPOINT_ASSET_MAINTANE;
const API_PATH_EXCEL = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const addNewOrUpdate = (voucher) => {
  if (voucher.id) {
    return axios.put(API_PATH + "/" + voucher.id, voucher);
  } else {
    return axios.post(API_PATH, voucher);
  }
};

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const getNewCode = () => {
  let config = {
    params: {
      assetClass: appConst.assetClass.VT,
      voucherType: ConstantList.VOUCHER_TYPE.StockIn,
    },
  }; //
  return axios.get(API_PATH + "/addNew/getNewCodeByVoucherType", config);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_API_ENPOINT_ASSET_MAINTANE + "/api/download/excel/nhap-xuat-kho-vat-tu",
    data: searchObject,
    responseType: "blob",
  });
};

export const exportToExcelPrint = (searchObject) => {
  return axios({
    method: "post",
    url:API_PATH_EXCEL + "/nhap-kho-vat-tu/" + searchObject?.id,
    params: searchObject,
    responseType: "blob",
  });
};
