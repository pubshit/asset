import ConstantList from "../../appConfig";
import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  FormControl,
  Input,
  InputAdornment,
  Tabs,
  AppBar,
  Tab,
  Collapse,
  Card
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  deleteItem,
  getItemById,
  searchByPage,
  getNewCode,
  exportToExcel,
} from "./InventoryReceivingVoucherService";
import InventoryReceivingVoucherDialog from "./InventoryReceivingVoucherDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi"
import InventoryComparison from "./InventoryComparison";
import AppContext from "app/appContext";
import { appConst, DEFAULT_TOOLTIPS_PROPS, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, STATUS_STORE } from "app/appConst";
import {
  convertNumberPrice,
  handleKeyUp,
  filterOptions,
  convertFromToDate,
  functionExportToExcel,
  getTheHighestRole,
  getUserInformation,
  NumberFormatCustom, convertMoney,
  isValidDate, handleThrowResponseMessage,
} from "app/appFunction";
import { getManagementDepartment } from "app/appServices";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { getListWarehouseByDepartmentId } from "../AssetTransfer/AssetTransferService";
import CardContent from "@material-ui/core/CardContent";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { inventoryReceivingPrintData } from "../FormCustom/InventoryReceiving";
import { exportToExcelPrint } from "./InventoryReceivingVoucherService";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import { LightTooltip } from "../Component/Utilities";
import CustomTablePagination  from "../CustomTablePagination"

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const isChoXuLy = item?.status
    === appConst.listStatusInventoryReceivingVoucherObject.CHO_XU_LY.code

  return (
    <div className="none_wrap">
      <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.edit)}
        >
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      {isChoXuLy && (
        <LightTooltip title={t("general.delete")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip><LightTooltip
        className="p-5"
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(
            item,
            appConst.active.view
          )}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InventoryReceivingVoucherTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    voucherDetails: [],
    totalElements: 0,
    shouldOpenPrintDialog: false,
    shouldOpenInventoryComparison: false,
    assetClass: appConst.assetClass.VT,
    openAdvanceSearch: false,
    receiverDepartment: null,
    store: null,
    fromDate: null,
    toDate: null,
    userInfo: getUserInformation(),
    tabValue: 0,
    isProcessed: null,
  };
  voucherType = ConstantList.VOUCHER_TYPE.StockIn; //Nhập kho
  productTypeCode = appConst.productTypeCode.VTHH; //lấy sản phẩm theo loại là: Vật tư tiêu hao

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let searchObject = {};

    searchObject.productTypeCode = this.state.productTypeCode;
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state?.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = this.state.assetClass;
    searchObject.statuses = this.state.statuses;
    searchObject.receiverDepartmentId = this.state?.receiverDepartment?.id;
    searchObject.storeId = this.state?.store?.id;
    if (this.state.fromDate) {
      searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
    }
    if (this.state.toDate) {
      searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
    }

    try {
      setPageLoading(true);
      let res = await searchByPage(searchObject);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        let newArray = data?.content?.map((voucher) => {
          let totalAmount = 0;
          if (voucher.voucherDetails) {
            voucher.voucherDetails?.map((vd) => {
              totalAmount += vd?.amount;
            });
          }
          voucher.totalAmount = totalAmount;
          return voucher;
        });

        this.setState({
          itemList: [...newArray],
          totalElements: data?.totalElements,
        });
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenPrintDialog: false,
      shouldOpenInventoryComparison: false,
      isView: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, this.updatePageData);
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { id } = this.state;

    setPageLoading(true);
    try {
      let res = await deleteItem(id);
      const { code, message } = res?.data;

      if (
        appConst.CODE.SUCCESS === code ||
        appConst.CODE.SUCCESS === res?.status
      ) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.success(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
  }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleAddItem = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    setPageLoading(true);
    try {
      let res = await getNewCode();
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code || res?.data?.voucherCode) {
        this.setState({
          item: {
            ...data,
            voucherDetails: data?.voucherDetails ? data?.voucherDetails : [],
            issueDate: data?.issueDate ? data?.issueDate : new Date(),
          },
          shouldOpenEditorDialog: true,
          isView: false,
          isProcessed: false,
        });
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = async () => {
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword;
    searchObject.assetClass = this.state.assetClass;
    searchObject.receiverDepartmentId = this.state?.receiverDepartment?.id;
    searchObject.storeId = this.state?.store?.id;
    if (this.state.fromDate) {
      searchObject.fromDate = convertFromToDate(this.state.fromDate).fromDate;
    }
    if (this.state.toDate) {
      searchObject.toDate = convertFromToDate(this.state.toDate).toDate;
    }
    await functionExportToExcel(exportToExcel, searchObject, "InventoryReceivingVoucher.xlsx");
  };

  handleGetVoucherDetailsById = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);

    try {
      let res = await getItemById(rowData.id);
      const { code, data } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          item: {
            ...data,
            status: appConst.listStatusInventoryReceivingVoucher.find(
              (item) => item?.indexOrder === data?.status
            ),
          },
        });
        return data
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleEdit = (rowData) => {
    this.handleGetVoucherDetailsById(rowData).then((data) => {
      let isProcessed = data?.status === appConst.listStatusInventoryReceivingVoucherObject.DA_XU_LY.code;
      this.setState({
        shouldOpenEditorDialog: true,
        isView: isProcessed,
        isProcessed,
      });
    }).catch((error) => {
      console.log("error", error);
    });
  };

  handleView = (rowData) => {
    this.handleGetVoucherDetailsById(rowData)
      .then(() => {
        this.setState({
          shouldOpenEditorDialog: true,
          isView: true,
        });
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  handleSortListData = (voucherDetails = []) => {
    return [...voucherDetails]?.sort((a, b) => {
      return a?.product?.name.localeCompare(b?.product?.name);
    });
  }

  handlePrint = (rowData) => {
    const { currentOrg } = this.context;
    if ([LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV_VAN_DINH.code].includes(currentOrg?.code)) {
      return this.setState({
        item: {
          ...rowData,
          voucherDetails: this.handleSortListData(rowData?.voucherDetails)
        },
        shouldOpenPrintDialog: true,
        isView: true,
        id: rowData?.id,
      })
    }
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        id: rowData?.id,
        shouldOpenPrintDialog: true,
        isView: true,
      });
    });
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.listStatusInventoryReceivingVoucherObject.CHO_XU_LY
        .indexOrder:
        return (
          <span className="status status-warning">
            {appConst.listStatusInventoryReceivingVoucherObject.CHO_XU_LY.name}
          </span>
        );
      case appConst.listStatusInventoryReceivingVoucherObject.DA_XU_LY
        .indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusInventoryReceivingVoucherObject.DA_XU_LY.name}
          </span>
        );
      default:
        break;
    }
  };

  handleRowDataClick = (e, rowData) => {
    this.setState({
      selectedRow: rowData,
      voucherDetails: rowData.voucherDetails
        ? this.handleSortListData(rowData.voucherDetails)
        : [],
    });
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
      voucherDetails: [],
      selectedRow: null
    });
    if (appConst?.tabInventoryReceivingVoucher?.tabAll === newValue) {
      this.setState({ statuses: null }, () => {
        this.updatePageData()
      });
    }
    // if (appConst?.tabInventoryReceivingVoucher?.tabProcessing === newValue) {
    //   this.setState({
    //     status:
    //       appConst.listStatusInventoryDeliveryVoucherObject.XAC_NHAN.indexOrder,
    //   },() => this.updatePageData());
    // }
    else {
      this.setState({ statuses: [newValue] }, () => {
        this.updatePageData()
      });
    }
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  }

  handleSetData = (value, name) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: value }, () => {
        if (value === null || isValidDate(value)) {
          this.updatePageData();
        }
      })
      return;
    }
    this.setState({ [name]: value }, () => {
      this.updatePageData();
    });
  };

  handleExportToExcelPrint = async (props) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let searchObject = {};
    console.log(props);
    searchObject.id = this.state.id;
    searchObject.exportType = props?.type;

    await functionExportToExcel(
      exportToExcelPrint,
      searchObject,
      t("exportToExcel.inventoryDeliveryPrint"),
      setPageLoading
    );
  }

  render() {
    const { t, i18n } = this.props;
    let {
      page,
      item,
      itemList,
      tabValue,
      selectedRow,
      rowsPerPage,
      totalElements,
      voucherDetails,
      shouldOpenPrintDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDialog,
      shouldOpenInventoryComparison,
      openAdvanceSearch,
      receiverDepartment,
      store,
      fromDate,
      toDate
    } = this.state;
    let TitlePage = t("InventoryReceivingVoucher.title");
    const { currentOrg } = this.context;
    const { INVENTORY_RECEIVING } = LIST_PRINT_FORM_BY_ORG.SUPPLIES_MANAGEMENT;
    const { isRoleAssetManager, isRoleAssetUser, departmentUser, } = getTheHighestRole();

    let dataView = inventoryReceivingPrintData(item, currentOrg);
    let isShowPrintPreview = [
      LIST_ORGANIZATION.BVDK_BA_VI.code,
      LIST_ORGANIZATION.BV_VAN_DINH.code,
      LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code,
    ].includes(currentOrg?.code);

    let searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_STORE.HOAT_DONG.code,
      departmentId: isRoleAssetManager || isRoleAssetUser
        ? departmentUser?.id
        : null,
    };
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        minWidth: "120px",
        maxWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleEdit(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.view) {
                this.handleView(rowData);
              } else if (method === appConst.active.print) {
                this.handlePrint(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryReceivingVoucher.stt"),
        field: "",
        width: "",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("InventoryReceivingVoucher.issueDate"),
        minWidth: "100px",
        maxWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryReceivingVoucher.voucherCode"),
        field: "voucherCode",
        minWidth: "140px",
        maxWidth: "140px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryReceivingVoucher.status"),
        field: "status",
        minWidth: "120px",
        maxWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.status),
      },
      {
        title: t("InventoryReceivingVoucher.billNumber"),
        field: "billNumber",
        minWidth: "140px",
        maxWidth: "140px",
        align: "left",
      },
      {
        title: t("component.Supply.name"),
        field: "supplier.name",
        minWidth: 240,
        maxWidth: 240,
        align: "left",
      },
      {
        title: t("InventoryReceivingVoucher.store"),
        field: "stockReceiptDeliveryStore.name",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("InventoryReceivingVoucher.receiverPerson"),
        field: "receiverPerson.displayName",
        align: "left",
        minWidth: "180px",
      },
      {
        title: t("InventoryReceivingVoucher.total"),
        field: "totalAmount",
        align: "left",
        minWidth: "145px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
          ? (convertMoney(rowData.totalAmount) || 0)
          : (convertNumberPrice(rowData.totalAmount) || 0),
      },
    ];

    let suppliesColumns = [
      {
        title: t("InventoryReceivingVoucher.stt"),
        field: "",
        width: "80px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        field: "product.code",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "product.name",
        align: "left",
        minWidth: "180px",
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "sku.name",
        align: "left",
        minWidth: 40,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.purchasePlaning"),
        minWidth: "140px",
        field: "purchasePlaning.name",
        align: "left",
      },
      {
        title: t("Product.quantityOfVoucher"),
        field: "quantityOfVoucher",
        minWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.quantity"),
        field: "quantity",
        minWidth: 80,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.price"),
        field: "price",
        minWidth: "130px",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <ValidatorForm>
            <TextValidator
              name="price"
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  readOnly: true,
                  style: { textAlign: "right", minWidth: "100px" },
                  className: "text-align-right",
                },
              }}
              value={rowData?.price || 0}
            />
          </ValidatorForm>
        ),
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        minWidth: 140,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.amount"),
        field: "amount",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: 130,
        render: (rowData) => (
          <ValidatorForm>
            <TextValidator
              name="amount"
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  readOnly: true,
                  style: { textAlign: "right", minWidth: "100px" },
                  className: "text-align-right",
                },
              }}
              value={rowData?.amount || 0}
            />
          </ValidatorForm>
        ),
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {" "}
            {t("InventoryReceivingVoucher.title")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.materialManagement") },
              {
                name: TitlePage,
                path: "list/inventory_receiving_voucher",
              },
            ]}
          />
        </div>

        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              value={appConst.tabInventoryReceivingVoucher.tabAll}
              label={t("InventoryReceivingVoucher.tabAll")}
            />
            <Tab
              className="tab"
              value={appConst.tabInventoryReceivingVoucher.tabProcessing}
              label={
                <div className="tabLable">
                  <span>{t("InventoryReceivingVoucher.tabProcessing")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.tabInventoryReceivingVoucher.tabProcessed}
              label={
                <div className="tabLable">
                  <span>{t("InventoryReceivingVoucher.tabProcessed")}</span>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <Grid container spacing={3} justifyContent="space-between" className="mt-20">
          {shouldOpenPrintDialog && (
            isShowPrintPreview ? (
              <PrintPreviewTemplateDialog
                t={t}
                handleClose={this.handleDialogClose}
                open={shouldOpenPrintDialog}
                item={this.state.item}
                title={t("Phiếu nhập kho")}
                model={PRINT_TEMPLATE_MODEL.SUPPLIES_MANAGEMENT.RECEIVING}
              />
            ) : (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={shouldOpenPrintDialog}
                item={dataView}
                title={t("Phiếu nhập kho")}
                urls={[
                  ...INVENTORY_RECEIVING.GENERAL,
                  ...(INVENTORY_RECEIVING[currentOrg?.printCode] || []),
                ]}
                exportExcelFunction={this.handleExportToExcelPrint}
              />
            )
          )}

          {shouldOpenInventoryComparison && (
            <InventoryComparison
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenInventoryComparison}
              item={item}
            />
          )}
          <Grid item md={5} sm={12} xs={12}>
            {!isRoleAssetUser && <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.handleAddItem}
            >
              {t("InventoryReceivingVoucher.add")}
            </Button>}
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.handleOpenAdvanceSearch()}
            >
              {t("general.advancedSearch")}
              <ArrowDropDownIcon />
            </Button>
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button>
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={(e) => handleKeyUp(e, this.updatePageData)}
                value={this.state?.keyword}
                placeholder={t("InventoryReceivingVoucher.enterSearch")}
                id="search_box"
                endAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.search} className="searchTable" />
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} className="pt-0 pb-0">
            <Collapse in={openAdvanceSearch}>
              <Card elevation={2}>
                <CardContent>
                  <ValidatorForm onSubmit={() => { }}>
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} md={3}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                          <KeyboardDatePicker
                            fullWidth
                            margin="none"
                            id="mui-pickers-date"
                            label={t("InventoryReceivingVoucher.fromDate")}
                            inputVariant="standard"
                            type="text"
                            autoOk={false}
                            format="dd/MM/yyyy"
                            name={"fromDate"}
                            value={fromDate}
                            onChange={(date) =>
                              this.handleSetData(date, "fromDate")
                            }
                            invalidDateMessage={t("general.invalidDateFormat")}
                            maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateNow")}
                            minDateMessage={t("general.minDateDefault")}
                            maxDate={toDate ? (new Date(toDate)) : new Date()}
                            clearable
                            clearLabel={t("general.remove")}
                            cancelLabel={t("general.cancel")}
                            okLabel={t("general.select")}
                          />
                        </MuiPickersUtilsProvider>
                      </Grid>
                      <Grid item xs={12} sm={12} md={3}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                          <KeyboardDatePicker
                            fullWidth
                            margin="none"
                            id="mui-pickers-date"
                            label={t("InventoryReceivingVoucher.toDate")}
                            inputVariant="standard"
                            type="text"
                            autoOk={false}
                            format="dd/MM/yyyy"
                            name={"toDate"}
                            value={toDate}
                            onChange={(date) =>
                              this.handleSetData(date, "toDate")
                            }
                            invalidDateMessage={t("general.invalidDateFormat")}
                            maxDateMessage={t("general.maxDateNow")}
                            minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minDateDefault")}
                            minDate={fromDate ? (new Date(fromDate)) : undefined}
                            maxDate={new Date()}
                            clearable
                            clearLabel={t("general.remove")}
                            cancelLabel={t("general.cancel")}
                            okLabel={t("general.select")}
                          />
                        </MuiPickersUtilsProvider>
                      </Grid>
                      <Grid item xs={12} sm={12} md={3}>
                        <AsynchronousAutocompleteSub
                          label={t("allocation_asset.receiverDepartment")}
                          searchFunction={getManagementDepartment}
                          searchObject={{}}
                          displayLable={'name'}
                          typeReturnFunction="list"
                          value={receiverDepartment ? receiverDepartment : null}
                          onSelect={(value) => this.handleSetData(value, "receiverDepartment")}
                          filterOptions={(options, params) => filterOptions(options, params)}
                          noOptionsText={t("general.noOption")}
                        />
                      </Grid>
                      <Grid item xs={12} sm={12} md={3}>
                        <AsynchronousAutocomplete
                          label={t("InventoryReceivingVoucher.store")}
                          searchFunction={getListWarehouseByDepartmentId}
                          searchObject={searchObjectStore}
                          defaultValue={store ? store : null}
                          displayLable={"name"}
                          value={store ? store : null}
                          onSelect={(value) => this.handleSetData(value, "store")}
                        />
                      </Grid>
                    </Grid>
                  </ValidatorForm>
                </CardContent>
              </Card>
            </Collapse>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <InventoryReceivingVoucherDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  handleSortListData={this.handleSortListData}
                  item={item}
                  isProcessed={this.state.isProcessed}
                  isView={this.state?.isView}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    selectedRow?.id === rowData.id
                      ? "#ccc"
                      : rowData.tableData.id % 2 === 1
                        ? "var(--primary-light-hover)"
                        : "#FFF",
                }),
                maxBodyHeight: "350px",
                minBodyHeight: "250px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
              onRowClick={this.handleRowDataClick}
            />
            <CustomTablePagination
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
              }
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>

          <Grid item xs={12}>
            <MaterialTable
              title={t("general.list")}
              data={voucherDetails}
              columns={suppliesColumns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: {
                  labelDisplayedRows: '{from}-{to} trong {count}',
                  labelRowsSelect: `${t("general.rows_per_page_table")}`,
                }
              }}
              options={{
                sorting: false,
                selection: false,
                pageSize: 5,
                actionsColumnIndex: -1,
                paging: true,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "350px",
                minBodyHeight: "250px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
InventoryReceivingVoucherTable.contextType = AppContext;
export default InventoryReceivingVoucherTable;
