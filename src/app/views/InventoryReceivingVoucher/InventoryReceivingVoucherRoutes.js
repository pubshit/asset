import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const InventoryReceivingVoucherTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./InventoryReceivingVoucherTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(InventoryReceivingVoucherTable);

const InventoryReceivingVoucherRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/inventory_receiving_voucher",
    exact: true,
    component: ViewComponent
  }
];

export default InventoryReceivingVoucherRoutes;