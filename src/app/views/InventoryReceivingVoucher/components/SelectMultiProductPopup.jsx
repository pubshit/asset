import {
  Grid,
  InputAdornment,
  Input,
  Button,
  Checkbox,
  TablePagination,
  Radio,
  Dialog,
  DialogActions,
  FormControlLabel,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  getListsProductSupplies,
  searchByPage,
} from "../../Product/ProductService";
import {convertMoney, getRole} from "app/appFunction";
import ProductDialog from "app/views/Product/ProductDialog";
import { getNewCode } from "app/views/Product/ProductService";
import { appConst } from "app/appConst";
import { PaperComponent } from "../../Component/Utilities";
import PropTypes from "prop-types";

class SelectProductAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    products: [],
    voucherType: null,
    voucherId: null,
    productTypeCode: "",
    storeId: "",
    isGetAll: false,
    planingDepartment: null,
    openProductDialog: false,
    itemProduct: null,
  };

  setPage = (page) => {
    this.setState({ page },  () =>  {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 },  () =>  {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  componentDidMount() {
    this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    item.isCheck = event.target.checked;
    let { products } = this.state;
    if (item.isCheck === true) {
      products.push(item);
    }

    if (item.isCheck === false) {
      let index = products.findIndex(assetVoucher =>
        ((assetVoucher?.product?.id || assetVoucher?.id) === item.id) && assetVoucher?.decisionCode === item?.decisionCode)
      products.splice(index, 1);
    }
    this.setState({ products });
  };

  componentWillMount() {
    let {
      products,
      productTypeCode,
      voucherType,
      voucherId,
      storeId,
      planingDepartment,
      assetVouchers,
      isReception,
      item,
    } = this.props;
    this.setState({
      products,
      productTypeCode,
      voucherType,
      voucherId,
      storeId,
      planingDepartment,
      assetVouchers,
      isReception,
      decisionCode: item?.decisionCode?.decisionCode,
      supplierId: item?.supplyUnit?.id,
      isSearchByBiding: isReception && item?.decisionCode?.decisionCode,
    });
  }

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  search() {
    this.setPage(0,  () =>  {
      this.updatePageData()
    })
  }

  updatePageData = async () => {
    let { supplierId, decisionCode, isSearchByBiding } = this.state;
    let searchObject = {};
    let searchObjectReception = {};
    let {
      planingDepartment,
      isManagementPurchaseDepartment,
      managementPurchaseDepartment,
    } = this.props;
    let planingDepartmentClone = {};
    if (planingDepartment) {
      if (Object.keys(planingDepartment).length > 0) {
        planingDepartmentClone = { ...planingDepartment };
      }
    }
    searchObject.storeId = this.state.storeId;
    searchObject.productTypeCode = this.state.productTypeCode;
    searchObject.voucherType = this.state.voucherType;
    searchObject.voucherId = this.state.voucherId;
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.managementPurchaseDepartment = isManagementPurchaseDepartment
      && managementPurchaseDepartment?.id
      ? managementPurchaseDepartment
      : null;
    if (
      Object.keys(planingDepartmentClone).length > 0 &&
      this.state.isGetAll === false
    ) {
      searchObject.managementPurchaseDepartment = planingDepartmentClone;
    }

    //searchobj reception
    searchObjectReception.keyword = this.state.keyword;
    searchObjectReception.pageIndex = this.state.page + 1;
    searchObjectReception.pageSize = this.state.rowsPerPage;
    searchObjectReception.decisionCode = decisionCode;
    searchObjectReception.supplierId = supplierId;
    try {
      const funApi = isSearchByBiding ? getListsProductSupplies : searchByPage;
      const searchObjectApi = isSearchByBiding ? searchObjectReception : searchObject;
      const res = await funApi(searchObjectApi);
      if (res?.status === appConst.CODE.SUCCESS) {
        let listData = isSearchByBiding ? res?.data?.data?.content : res?.data?.content;
        let itemListClone = listData?.length ? [...listData] : [];
        let totalElements = isSearchByBiding ? res?.data?.data?.totalElements : res?.data?.totalElements;
        itemListClone.map((item) => {
          item.isCheck = false;
          if (this.state.products && this.state.products.length > 0) {
            this.state.products.forEach((product) => {
              if (!isSearchByBiding && (product?.product?.id || product?.id) === item.id) {
                item.isCheck = true;
              }
              if (isSearchByBiding
                && product?.product?.id === item.id
                && product?.decisionCode === item.decisionCode
              ) {
                item.isCheck = true;
              }
            });
          }
          return item;
        });
        this.setState(
          {
            itemList: [...itemListClone],
            totalElements,
          },
          () => { }
        );
      }

    } catch (error) {
      console.error(error)
    }
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "isGetAll") {
      this.setState({ isGetAll: event.target.checked }, () =>
        this.updatePageData()
      );

      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = (name) => {
    this.setState({
      shouldOpenProductDialog: false,
      openProductDialog: false,
      keyword: name || this?.state?.keyword || "",
    }, this.updatePageData);
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };

  handleKeyUp = (e) => {
    this.search();
  };

  setItemProduct = (value) => {
    this.setState(
      {
        itemProduct: value,
      },
      () => {
        this.setState({
          openProductDialog: true,
        });
      }
    );
  };

  handleAddNewProduct = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result?.data && result?.data?.code) {
          let item = result.data;
          this.setItemProduct({
            ...item,
            name: this.state.keyword,
          });
        }
      })
      .catch(() => {
        alert(this.props?.t("general.error_reload_page"));
      });
  };

  setOpenProductDialog = (value) => {
    this.setState({
      openProductDialog: value,
    });
  };

  handleProductDialogClose = () => {
    this.setOpenProductDialog(false);
    if (!this.state?.itemProduct?.id) {
      this.setState({
        keyword: "",
      });
    }
  };

  render() {
    const {
      t,
      i18n,
      handleClose,
      handleSelect,
      open,
      planingDepartment,
    } = this.props;
    let planingDepartmentClone = {};
    if (planingDepartment) {
      if (Object.keys(planingDepartment).length > 0) {
        planingDepartmentClone = { ...planingDepartment };
      }
    }
    let {
      keyword,
      shouldOpenProductDialog,
      itemList,
      products,
      voucherType,
      voucherId,
      storeId,
      totalElements,
      rowsPerPage,
      page,
      isGetAll,
      isSearchByBiding,
    } = this.state;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        maxWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          !this.props?.isOneSelect ? (
            <Checkbox
              // id={`radio${rowData.id}`}
              style={{ padding: "0px", paddingLeft: "8px" }}
              name="radSelected"
              // value={rowData.id}
              checked={rowData.isCheck}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          ) : (
            <Radio
              // id={`radio${rowData.id}`}
              style={{ padding: "0px" }}
              name="radSelected"
              // value={rowData.id}
              checked={this.state.selectedValue === rowData.id}
              onClick={(event) => this.handleClick(event, rowData)}
            />
          ),
      },
      {
        title: t("Product.code"),
        field: "code",
        align: "left",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "name",
        align: "left",
        minWidth: 250,
      },
      {
        title: t("Product.model"),
        field: "model",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.stockKeepingUnit"),
        field: "unitName",
        align: "center",
        minWidth: 100,
        hidden: !isSearchByBiding,
      },
      {
        title: t("InventoryReport.remainingQuantity"),
        field: "remainQuantity",
        align: "center",
        minWidth: 120,
        hidden: !isSearchByBiding,
      },
      {
        title: t("BiddingList.code"),
        field: "decisionCode",
        align: "center",
        minWidth: 120,
        hidden: !isSearchByBiding,
      },
      {
        title: t("Product.madeIn"),
        field: "madeIn",
        align: "center",
        minWidth: 150,
        hidden: !isSearchByBiding,
      },
      {
        title: t("Product.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "center",
        minWidth: 100,
        hidden: !isSearchByBiding,
      },
      {
        title: t("Product.manufacturer"),
        field: "manufacturerName",
        align: "center",
        minWidth: 200,
        hidden: !isSearchByBiding,
      },
      {
        title: t("Product.supplier"),
        field: "supplierName",
        align: "left",
        minWidth: 200,
        hidden: !isSearchByBiding,
      },
      {
        title: t("Product.price"),
        field: "supplierName",
        align: "left",
        minWidth: 200,
        hidden: !isSearchByBiding,
        cellStyle: {
          textAlign: "right",
        },
        render: rowData => convertMoney(rowData.unitPrice)
      },
    ];

    let {
      isRoleOrgAdmin,
      isRoleAdmin,
      isRoleAssetManager,
    } = getRole();
    let isAllowAddNewProduct = isRoleAssetManager || isRoleOrgAdmin || isRoleAdmin
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={isSearchByBiding ? "lg" : "md"}
        fullWidth
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <span className="mb-20">{t("Product.title")}</span>
        </DialogTitle>
        <DialogContent style={{ overflow: "hidden" }}>
          <Grid container spacing={2}>
            <Grid item md={6} sm={6} xs={12}>
              <Input
                label={t("general.enterSearch")}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                fullWidth
                className="mb-16 "
                id="search_box"
                placeholder={t("general.enterSearch")}
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      className="searchTable"
                    />
                  </InputAdornment>
                }
              />
            </Grid>
            {Object.keys(planingDepartmentClone).length > 0 ? (
              <Grid item md={6} sm={6} xs={12}>
                <FormControlLabel
                  style={{ float: "right" }}
                  value={isGetAll}
                  className="mb-16"
                  name="isGetAll"
                  onChange={(isGetAll) =>
                    this.handleChange(isGetAll, "isGetAll")
                  }
                  control={<Checkbox checked={isGetAll} />}
                  label={t("general.allProduct")}
                />
              </Grid>
            ) : (
              ""
            )}
            {this.props?.allowedAddNew &&
              itemList?.length === 0 &&
              isAllowAddNewProduct &&
              !isSearchByBiding &&
              (
                <Grid item md={6} sm={6} xs={12}>
                  <Button
                    className="mr-12"
                    variant="contained"
                    color="primary"
                    onClick={this.handleAddNewProduct}
                  >
                    {t("general.add")}
                  </Button>
                </Grid>
              )}
            {this.state.openProductDialog && (
              <ProductDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleProductDialogClose}
                open={this.state?.openProductDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={this.state.itemProduct}
                type={"addFromRateIncident"}
              // selectProduct={this.handleMapDataAsset}
              // indexLinhKien={this.state.indexLinhKien}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={itemList}
              columns={columns}
              options={{
                sorting: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                minBodyHeight: "233px",
                maxBodyHeight: "233px",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) =>
                this.setState({
                  products: rows.map((row) => ({
                    ...row,
                    tableData: {
                      ...row.tableData,
                      checked: false,
                    },
                  })),
                })
              }
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
              onRowClick={(evt, selectedRow) => this.onClickRow(selectedRow)}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(products)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectProductAllPopop;
