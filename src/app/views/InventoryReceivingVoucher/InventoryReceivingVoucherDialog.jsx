import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import {
  addNewOrUpdate,
  personSearchByPage,
} from "./InventoryReceivingVoucherService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import DateFnsUtils from "@date-io/date-fns";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import { searchByPage as searchByPagePurchasePlaning } from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningService";
import {
  appConst,
  STATUS_STORE,
  PURCHASE_PLANING_TYPE_OBJECT, STATUS_SUPPLIER, variable,
} from "app/appConst";
import {
  LightTooltip,
  NumberFormatCustom,
  PaperComponent,
  filterOptions,
  getTheHighestRole,
  formatDateDto,
  checkMinDate,
  checkInvalidDate,
  getUserInformation,
  formatTimestampToDate,
  a11yProps,
  isSuccessfulResponse,
  handleThrowResponseMessage,
} from "app/appFunction";
import AppContext from "app/appContext";
import { getListWarehouseByDepartmentId } from "../AssetTransfer/AssetTransferService";
import SelectMultiProductPopup from "./components/SelectMultiProductPopup"
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import viLocale from "date-fns/locale/vi";
import { Label, TabPanel } from "../Component/Utilities";
import { searchByTextNew } from "../Supplier/SupplierService";
import SupplierDialog from "../Supplier/SupplierDialog";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import { searchByPageDepartmentNew } from "../Department/DepartmentService";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import { SignatureTabComponent } from "../Component/Signature/SignatureTabComponent";
import {getVoucherAttributes, saveVoucherAttributes} from "../../appServices";

class InventoryReceivingVoucherDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    issueDate: new Date(),
    shouldOpenStorePopup: false,
    shouldOpenProductPopup: false,
    shouldOpenHandoverStorePopup: false,
    totalElements: 0,
    stockReceiptDeliveryStore: null,
    voucherCode: "",
    voucherName: "",
    voucherDetails: [],
    type: ConstantList.VOUCHER_TYPE.StockIn,
    product: [],
    amount: 0,
    productTypeCode: appConst.productTypeCode.VTHH, //không được thay đổi
    productSku: {},
    expiryDate: null, //Hạn sử dụng
    batchCode: "", //Mã lô
    note: null,
    handoverPerson: null,
    billDate: null,
    billNumber: "",
    purchasePlaningType: PURCHASE_PLANING_TYPE_OBJECT.VAT_TU.code, // kế hoạch mua sam vật tư
    assetClass: appConst.assetClass.VT,
    statusInventoryReceivingVoucher: appConst.listStatusInventoryReceivingVoucherObject.CHO_XU_LY,
    allowSubmit: true,
    permission: {
      note: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      reason: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      handoverRepresentative: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      billDate: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      issueDate: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      billNumber: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      receiverPerson: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      receiverDepartment: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      supplier: { ...appConst.OBJECT_PERMISSION_DEFAULT },
      store: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    },
    supplier: null,
    tabValue: 0,
    signatures: {},
  };
  voucherType = ConstantList.VOUCHER_TYPE.StockIn; //Nhập kho

  handleChange = (event, source) => {
    event.persist(); // todo: keep the event exist after change

    this.setState({
      [event.target.name]: event.target.value,
    }, () => {
      if (event?.target?.name === variable.listInputName.billNumber) {
        this.handleBidingReason();
      }
    });
  };

  convertDataSubmit = (data) => {
    return {
      type: data?.type,
      id: data?.id,
      voucherCode: data?.voucherCode,
      issueDate: formatDateDto(data?.issueDate),
      billDate: formatDateDto(data?.billDate),
      billNumber: data?.billNumber,
      status: data?.statusInventoryReceivingVoucher?.code,
      receiverDepartment: {
        id: data?.receiverDepartment?.id,
      },
      receiverPerson: data?.receiverPerson,
      stockReceiptDeliveryStore: data?.stockReceiptDeliveryStore,
      supplier: {
        id: data?.supplier?.id,
      },
      handoverRepresentative: data?.handoverRepresentative,
      reason: data?.reason,
      note: data?.note,
      assetClass: appConst.assetClass.VT,
      voucherDetails: data?.voucherDetails?.map(i => {
        return {
          ...i,
          purchasePlaningId: i?.purchasePlaningId || i?.purchasePlaning?.id,
        }
      })
    }
  }

  validateForm = () => {
    let { voucherDetails, issueDate, billDate } = this.state;
    if(!issueDate) {
      toast.warning("Chưa chọn ngày nhập kho");
      return false;
    }
    if (voucherDetails?.length === 0) {
      toast.warning("Chưa chọn sản phẩm");
      return false;
    }
    if (checkMinDate(issueDate, "01/01/1890")) {
      toast.warning(`Ngày nhập kho không được nhỏ hơn ngày "01/01/1890"(Thông tin phiếu).`);
      return false;
    }
    if (checkInvalidDate(issueDate)) {
      toast.warning('Ngày nhập kho không tồn tại (Thông tin phiếu).');
      return false;
    }
    if (checkMinDate(billDate, "01/01/1890")) {
      toast.warning(`Ngày hóa đơn không được nhỏ hơn ngày "01/01/1890"(Thông tin phiếu).`);
      return false;
    }
    return true;
  }

  handleFormSubmit = async () => {
    if (!this.state?.allowSubmit) {
      return
    }
    //todo submit kho vật tư
    let { id } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;

    if (!this.validateForm()) return;
    setPageLoading(true);
    try {
      const res = await addNewOrUpdate({
        ...this.convertDataSubmit(this.state),
      });
      const { code, data, message } = res?.data;

      if (code === appConst.CODE.SUCCESS && data) {
        id
          ? toast.success(t("general.updateSuccess"))
          : toast.success(t("general.addSuccess"));
        this.props.handleOKEditClose();
      } else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    }, () => {
      if (name === variable.listInputName.billDate) {
        this.handleBidingReason();
      }
    });
  };

  handleProductPopupClose = () => {
    this.setState({
      shouldOpenProductPopup: false,
      allowSubmit: true
    });
  };

  handleSelectMultiProduct = (items) => {
    let voucherDetails = []
    if (items?.length > 0) {
      items.forEach((item) => {
        if (!item?.product) {
          let vc = {};
          vc.skus = [];
          // debugger
          if (item.skus != null && item.skus?.length > 0) {
            item.skus.forEach((element) => {
              vc.skus.push(element.sku);
            });
            vc.sku = item.defaultSku?.sku;
          }
          vc.product = item;
          vc.quantity = 0;
          vc.price = "";
          vc.amount = 0;
          voucherDetails.push(vc);
        } else {
          voucherDetails.push(item);
        }
      });

      if (!voucherDetails) {
        voucherDetails = [];
      }

      this.setState({
        voucherDetails: this.props.handleSortListData(voucherDetails)
      }, () => {
        this.handleProductPopupClose();
      });
    }
  };

  componentDidMount() {
    let roles = getTheHighestRole();
    let { item = {} } = this.props;
    let { organization } = getUserInformation();

    this.setState({
      ...item,
      ...roles,
      statusInventoryReceivingVoucher: item?.status,
      voucherDetails: this.props.handleSortListData(item?.voucherDetails),
    }, () => {
      this.handleCheckPermission();
      this.handleBidingReason();
      if (!item?.id) {
        this.setState({
          receiverDepartment: roles?.isRoleAssetManager
            ? roles?.departmentUser
            : null,
          signatures: {
            directorId: organization?.directorId,
            directorName: organization?.directorName,
            deputyDirectorId: organization?.deputyDirectorId,
            deputyDirectorName: organization?.deputyDirectorName,
            chiefAccountantId: organization?.chiefAccountantId,
            chiefAccountantName: organization?.chiefAccountantName,
            headOfDepartmentId: item?.headOfDepartmentId,
            headOfDepartmentName: item?.headOfDepartmentName,
            deputyDepartmentId: item?.deputyDepartmentId,
            deputyDepartmentName: item?.deputyDepartmentName,
            storeKeeperId: item?.storeKeeperId,
            storeKeeperName: item?.storeKeeperName,
            handoverPersonId: null,
            handoverPersonName: item.handoverRepresentative,
            receiverPersonId: item?.receiverPersonId,
            receiverPersonName: item?.receiverPersonName,
          }
        })
      } else {
        // this.handleGetVoucherAttributes();
      }
    });
  }

  handleStorePopupClose = () => {
    this.setState({
      shouldOpenStorePopup: false,
    });
  };

  selectReceiverPerson = (item) => {
    this.setState({ receiverPerson: item }, () => { });
  };

  handleQuantityChange = (rowData, event) => {
    let { voucherDetails } = this.state;
    let value = event.target.value;
    let name = event.target.name;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        let isReCountAmount = value && vd.price && name === 'quantity'
        if (vd?.product?.id === rowData?.product?.id) {
          vd[name] = value || null;
          if (isReCountAmount) {
            vd.amount = (value / 1) * vd.price;
          }
        }
      });
      this.setState({ voucherDetails });
    }
  };
  handleBatchCodeChange = (rowData, event) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (
          vd != null &&
          vd.product &&
          rowData != null &&
          rowData.product &&
          vd.product.id === rowData.product.id
        ) {
          vd.batchCode = event.target.value;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  handleExpiryDateChange = (rowData, expiryDate, name) => {
    // debugger
    let { voucherDetails } = this.state;
    this.setState({ [name]: expiryDate });
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (
          vd != null &&
          vd.product &&
          rowData != null &&
          rowData.product &&
          vd.product.id === rowData.product.id
        ) {
          vd.expiryDate = expiryDate ? formatDateDto(expiryDate) : null;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  handlePriceChange = (rowData, event) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id === rowData.product.id
        ) {
          vd.price = event.target.value;
          if (event.target.value != null && vd.quantity != null) {
            vd.amount = (event.target.value / 1) * vd.quantity;
          }
        }
      });
      this.setState({ voucherDetails });
    }
  };

  selectSku = (item, rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (vd.product.id === rowData.product.id) {
          vd.sku = item;
          vd.skuType = 1;
          return;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  selectPurchasePlaning = (item, rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (vd.product.id === rowData.product.id) {
          vd.purchasePlaning = item;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  handleRowDeleteProduct = (rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      for (let index = 0; index < voucherDetails.length; index++) {
        if (
          voucherDetails &&
          voucherDetails[index].product.id === rowData.product.id
        ) {
          voucherDetails.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ voucherDetails });
  };

  handleChangeStatus = (value) => {
    this.setState({ statusInventoryReceivingVoucher: value });
  };

  handleSetData = (value, name) => {
    this.setState({ [name]: value });

  };

  handleSelectReceiverDepartment = (item) => {
    let receiverDepartment = {
      name: item?.name,
      text: item?.text,
      id: item?.id,
      code: item?.code
    }

    this.setState({
      receiverDepartment: item?.id ? receiverDepartment : null,
      stockReceiptDeliveryStore: null,
      receiverPerson: null,
      voucherDetails: []
    })
  }

  handleClosePopup = () => {
    this.setState({
      shouldOpenAddSupplierDialog: false,
      keySearch: "",
    })
  }

  handleSelectSupplier = (value) => {
    if (value?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenAddSupplierDialog: true,
        supplier: null,
        listSupplier: [],
      });
      return;
    }
    this.setState({
      supplier: value,
    }, () => {
      this.handleClosePopup();
      this.handleBidingReason();
    });
  }

  handleCheckPermission = () => {
    let { permission } = this.state;
    let { isView } = this.props;
    let newPermission = { ...permission };

    if (isView) {
      Object.keys(permission)?.map(key => {
        newPermission[key].isRequired = false;
      })
    }

    this.setState({
      permission: { ...newPermission },
    })
  }

  handleBidingReason = () => {
    let { supplier, billNumber, billDate } = this.state;
    let reason = `Nhập hàng của ${supplier?.name || "                   "
      } theo hóa đơn ${billNumber || "             "
      } ngày ${formatTimestampToDate(billDate) || ""
      }`;

    this.setState({ reason });
  }

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      tabValue: newValue,
    })
  };
  
  handleChangeSignature = (data = []) => {
    this.setState({
      signatures: data,
    })
  }
  
  handleGetVoucherAttributes = async () => {
    let {setPageLoading} = this.context;
    try {
      setPageLoading(true);
      let payload = {
        voucherId: this.state.id,
      };
      let res = await getVoucherAttributes(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          signatures: data,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      this.showToast(t("general.error"), "error");
    } finally {
      setPageLoading(false);
    }
  }
  
  handleSaveVoucherAttributes = async (id) => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    try {
      setPageLoading(true);
      let payload = {
        ...this.state.signatures,
        voucherId: id,
      };
      let res = await saveVoucherAttributes(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          signatures: data,
        })
        return true;
      } else {
        handleThrowResponseMessage(res);
        return false;
      }
    } catch (err) {
      toast.error(t("general.error"));
      return false;
    } finally {
      setPageLoading(false);
    }
  }
  
  render() {
    let { open, t, i18n, isView, isProcessed } = this.props;
    let {
      page,
      note,
      reason,
      handoverRepresentative,
      billDate,
      issueDate,
      billNumber,
      rowsPerPage,
      voucherCode,
      allowSubmit,
      receiverPerson,
      voucherDetails,
      productTypeCode,
      receiverDepartment,
      isRoleAssetManager,
      listReceiverDepartment,
      shouldOpenProductPopup,
      stockReceiptDeliveryStore,
      shouldOpenAddSupplierDialog,
      supplier,
      keySearch,
      listSupplier,
      permission,
      tabValue,
    } = this.state;
    const { organization } = getUserInformation();
    let searchObjectPurchasePlaning = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      type: this.state.purchasePlaningType,
    };
    let searchObjectUser = { ...appConst.OBJECT_SEARCH_MAX_SIZE, departmentId: receiverDepartment?.id, };
    let searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_STORE.HOAT_DONG.code,
      departmentId: receiverDepartment?.id, //todo: phòng ban quản lý đăng nhập,
    };
    let supplierSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_SUPPLIER.HOAT_DONG.code,
      typeCodes: [appConst.TYPE_CODES.NCC_CU],
    };
    let handoverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      orgId: organization?.org?.id,
    }

    let columns = [
      {
        title: t("general.action"),
        field: "valueText",
        width: "100px",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        hidden: isView,
        render: (rowData) => (
          <LightTooltip
            title={t("general.delete")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => this.handleRowDeleteProduct(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("InventoryReceivingVoucher.stt"),
        field: "",
        width: "80px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Product.code"),
        field: "product.code",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.name"),
        field: "product.name",
        align: "left",
        minWidth: "180px",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: "ĐVT",
        align: "center",
        minWidth: "80px",
        cellStyle: {
          textAlign: "center",
        },
        field: "sku.name",
        render: (rowData) =>
          rowData.skus ? (
            <Autocomplete
              options={rowData.skus}
              defaultValue={rowData.sku}
              value={rowData.sku}
              getOptionLabel={(option) => option?.name || option?.skuName}
              onChange={(event, newValue) => {
                this.selectSku(newValue, rowData);
              }}
              renderInput={(params) => <TextValidator
                {...params}
                value={rowData.sku}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                InputProps={{
                  readOnly: isView,
                }}
              />}
              closeIcon={<></>}
            />
          ) : (
            <label>{rowData.sku?.name}</label>
          ),
      },
      {
        title: t("Product.purchasePlaning"),
        minWidth: "140px",
        align: "left",
        render: (rowData) => (
          <AsynchronousAutocompleteSub
            searchFunction={searchByPagePurchasePlaning}
            searchObject={searchObjectPurchasePlaning}
            typeReturnFunction="category"
            noOptionsText={"no data"}
            displayLable={"name"}
            value={rowData.purchasePlaning}
            onSelect={(value) => this.selectPurchasePlaning(value, rowData)}
            onSelectOptions={rowData}
            readOnly={isView}
          />
        ),
      },
      {
        title: t("Product.quantityOfVoucher"),
        field: "quantityOfVoucher",
        minWidth: 100,
        render: (rowData) => (
          <TextValidator
            type="text"
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            InputProps={{
              inputComponent: NumberFormatCustom,
              inputProps: {
                style: { textAlign: "center", minWidth: "50px" },
                decimalScale: 2,
                allowNegative: false,
              },
              readOnly: isView,
            }}
            value={rowData.quantityOfVoucher || ""}
            name="quantityOfVoucher"
            validators={allowSubmit ? ["minFloat:0.000000001", "required", `matchRegexp:${variable.regex.numberQuantityFloatValid}`] : []}
            errorMessages={[t("general.minNumberError"), t("general.required"), t("general.quantityError")]}
          />
        ),
      },
      {
        title: t("Product.quantity"),
        field: "quantity",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            type="text"
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            // onKeyDown={handleKeyDownIntegerOnly}
            id="formatted-numberformat-carryingAmount"
            InputProps={{
              inputComponent: NumberFormatCustom,
              inputProps: {
                style: { textAlign: "center", minWidth: "50px" },
              },
              readOnly: isView,
            }}
            name="quantity"
            validators={allowSubmit ? ["minFloat:0.000000001", "required", `matchRegexp:${variable.regex.numberQuantityFloatValid}`] : []}
            errorMessages={[t("general.minNumberError"), t("general.required"), t("general.quantityError")]}
            value={rowData.quantity || ""}
          />
        ),
      },
      {
        title: t("Product.price"),
        field: "price",
        minWidth: "130px",
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handlePriceChange(rowData, event)}
            name="price"
            id="formatted-numberformat-originalCost"
            InputProps={{
              inputComponent: NumberFormatCustom,
              inputProps: {
                style: { textAlign: "right", minWidth: "100px" },
                className: "text-align-right",
              },
              readOnly: isView,
            }}
            validators={allowSubmit ? ["minNumber:0", `matchRegexp:${variable.regex.numberQuantityFloatValid}`] : []}
            errorMessages={allowSubmit ? [t("general.nonNegativeNumber"), t("general.quantityError")] : []}
            value={rowData.price || ""}
          />
        ),
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        minWidth: "120px",
        align: "left",
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleBatchCodeChange(rowData, event)}
            name="batchCode"
            InputProps={{
              inputProps: {
                style: { textAlign: "center", minWidth: "120px" },
              },
              readOnly: isView,
            }}
            value={rowData.batchCode || ""}
          />
        ),
      },
      {
        title: t("InventoryReceivingVoucher.expiryDate"),
        field: "expiryDate",
        minWidth: "110px",
        align: "left",
        render: (rowData) => (
          <CustomValidatePicker
            fullWidth
            margin="none"
            id="mui-pickers-date"
            inputVariant="standard"
            type="text"
            autoOk={false}
            format="dd/MM/yyyy"
            name={"expiryDate"}
            value={rowData.expiryDate || null}
            invalidDateMessage={t("general.invalidDateFormat")}
            onChange={(expiryDate) =>
              this.handleExpiryDateChange(rowData, expiryDate, "expiryDate")
            }
            InputProps={{
              inputProps: {
                style: { textAlign: "center", minWidth: "80px" },
              },
            }}
            readOnly={isView}
            clearLabel={t("general.remove")}
            cancelLabel={t("general.cancel")}
            okLabel={t("general.select")}
          />
        ),
      },
      {
        title: t("Product.amount"),
        field: "amount",
        align: "left",
        cellStyle: {
          textAlign: "right",
        },
        minWidth: 130,
        render: (rowData) => (
          <TextValidator
            name="amount"
            id="formatted-numberformat-originalCost"
            InputProps={{
              readOnly: true,
              inputComponent: NumberFormatCustom,
              inputProps: {
                style: { textAlign: "right", minWidth: "100px" },
                className: "text-align-right",
              },
            }}
            validators={allowSubmit ? ["minNumber:0"] : []}
            value={rowData.amount || ""}
          />
        ),
      },
    ];

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className="validator-form-scroll-dialog">
          <DialogTitle className="mb-0 cursor-move" id="draggable-dialog-title">
            {t("InventoryReceivingVoucher.dialog")}
          </DialogTitle>

          <DialogContent style={{ minHeight: "420px" }}>
            <AppBar position="static" color="default">
              <Tabs
                value={tabValue}
                onChange={this.handleChangeTabValue}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab label={t("Reception.tab.InformationVoting")} value={0} {...a11yProps(0)} />
                {/*<Tab label={t("Reception.tab.signature")} value={1} {...a11yProps(1)} />*/}
              </Tabs>
            </AppBar>

            <TabPanel value={tabValue} index={0}>
              <Grid container spacing={1}>
                {/* Dòng 1  */}
                <Grid item md={3} sm={12} xs={12}>
                  <p className="font-weight-bold display-block">
                    {t("InventoryReceivingVoucher.voucherCode") +
                      ": " +
                      voucherCode}
                  </p>
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <CustomValidatePicker
                      label={
                        <Label isRequired>
                          {t("InventoryReceivingVoucher.issueDate")}
                        </Label>
                      }
                      type="text"
                      name={"issueDate"}
                      value={issueDate || null}
                      onChange={(date) =>
                        this.handleDateChange(date, "issueDate")
                      }
                      readOnly={isView}
                      disabled={isProcessed}
                      maxDate={new Date()}
                      maxDateMessage={t("general.maxDateNow")}
                      validators={allowSubmit ? ["required"] : []}
                      errorMessages={[t("general.required")]}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <CustomValidatePicker
                    label={t("InventoryReceivingVoucher.billDate")}
                    name={"billDate"}
                    value={billDate || null}
                    onChange={(date) => this.handleDateChange(date, "billDate")}
                    readOnly={isView && !isProcessed}
                    maxDate={new Date()}
                    maxDateMessage={t("allocation_asset.maxDateMessage")}
                  />
                </Grid>
                <Grid item md={3} sm={3} xs={12}>
                  <TextValidator
                    onChange={this.handleChange}
                    type="text"
                    name="billNumber"
                    label={
                      <Label>
                        {t("InventoryReceivingVoucher.billNumber")}
                      </Label>
                    }
                    className="w-100"
                    value={billNumber || ""}
                    InputProps={{
                      readOnly: isView && !isProcessed,
                    }}
                  />
                </Grid>

                {/* Dòng 2 */}
                <Grid item md={3} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <Label isRequired>
                        {t("InventoryReceivingVoucher.status")}
                      </Label>
                    }
                    searchFunction={() => { }}
                    listData={appConst.listStatusInventoryReceivingVoucher}
                    displayLable="name"
                    readOnly={isView && !isProcessed}
                    disabled={isProcessed}
                    value={this.state.statusInventoryReceivingVoucher || null}
                    onSelect={this.handleChangeStatus}
                    noOptionsText={t("general.noOption")}
                    validators={allowSubmit ? ["required"] : []}
                    errorMessages={[t("general.required")]}
                  />
                </Grid>
                <Grid item md={3} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <Label isRequired>
                        {t("InventoryReceivingVoucher.receiverDepartment")}
                      </Label>
                    }
                    searchFunction={
                      isRoleAssetManager
                        ? getListOrgManagementDepartment
                        : searchByPageDepartmentNew
                    }
                    searchObject={
                      isRoleAssetManager
                        ? {}
                        : handoverDepartmentSearchObject
                    }
                    listData={listReceiverDepartment}
                    setListData={(data) => this.handleSetData(data, "listReceiverDepartment")}
                    displayLable={'name'}
                    typeReturnFunction={
                      isRoleAssetManager
                        ? "list"
                        : "category"
                    }
                    isNoRenderChildren
                    isNoRenderParent
                    value={receiverDepartment ? receiverDepartment : null}
                    onSelect={(value) => this.handleSelectReceiverDepartment(value)}
                    filterOptions={(options, params) => filterOptions(options, params)}
                    noOptionsText={t("general.noOption")}
                    validators={allowSubmit && permission.receiverDepartment.isRequired ? ["required"] : []}
                    errorMessages={[t('general.required')]}
                    disabled={isRoleAssetManager || isProcessed}
                    readOnly={isView && !isProcessed}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <Label isRequired>
                        {t("InventoryReceivingVoucher.receiverPerson")}
                      </Label>
                    }
                    searchFunction={personSearchByPage}
                    searchObject={searchObjectUser}
                    displayLable={"displayName"}
                    value={receiverPerson || null}
                    onSelect={this.selectReceiverPerson}
                    validators={allowSubmit && permission.receiverPerson.isRequired ? ["required"] : []}
                    errorMessages={[t("general.required")]}
                    readOnly={isView && !isProcessed}
                    disabled={!receiverDepartment?.id || isProcessed}
                  />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("InventoryReceivingVoucher.store")}
                      </span>
                    }
                    searchFunction={getListWarehouseByDepartmentId}
                    searchObject={searchObjectStore}
                    displayLable={"name"}
                    name="stockReceiptDeliveryStore"
                    value={stockReceiptDeliveryStore || null}
                    onSelect={this.handleSetData}
                    validators={allowSubmit && permission.store.isRequired ? ["required"] : []}
                    errorMessages={[t("general.required")]}
                    disabled={!receiverDepartment?.id || isProcessed}
                    readOnly={isView && !isProcessed}
                  />
                </Grid>

                <Grid item md={3} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={t("component.Supply.name")}
                    searchFunction={searchByTextNew}
                    searchObject={supplierSearchObject}
                    nameListData="listSupplier"
                    listData={listSupplier}
                    setListData={this.handleSetData}
                    displayLable={"name"}
                    name="supplier"
                    value={supplier}
                    onSelect={this.handleSelectSupplier}
                    filterOptions={(options, params) =>
                      filterOptions(options, params, true, "name")
                    }
                    onInputChange={(e) => this.handleSetData(e?.target?.value, "keySearch")}
                    noOptionsText={t("general.noOption")}
                    readOnly={isView && !isProcessed}
                    disabled={isProcessed}
                  />
                </Grid>
                <Grid item md={3} sm={12} xs={12}>
                  <TextValidator
                    onChange={this.handleChange}
                    type="text"
                    name="handoverRepresentative"
                    label={t("ReceivingAsset.handoverRepresentative")}
                    className="w-100"
                    value={handoverRepresentative || ""}
                    inputProps={{
                      readOnly: isView
                    }}
                    disabled={isProcessed}
                  />
                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                  <TextValidator
                    onChange={this.handleChange}
                    type="text"
                    name="reason"
                    label={<span>{t("ReceivingAsset.reason")}</span>}
                    className="w-100"
                    value={reason || ""}
                    inputProps={{
                      readOnly: isView && !isProcessed
                    }}
                    disabled={isProcessed}
                  />
                </Grid>
                <Grid item md={12} sm={12} xs={12}>
                  <TextValidator
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    label={<span>{t("ReceivingAsset.note")}</span>}
                    className="w-100"
                    value={note || ""}
                    inputProps={{
                      readOnly: isView && !isProcessed
                    }}
                    disabled={isProcessed}
                  />
                </Grid>
              </Grid>

              {!isView && <Grid container spacing={2} className="mt-8">
                <Grid item md={3} sm={6} xs={12}>
                  <Button
                    size="small"
                    className="mt-16"
                    variant="contained"
                    color="primary"
                    onClick={() =>
                      this.setState({ shouldOpenProductPopup: true, allowSubmit: false })
                    }
                  >
                    {t("component.product.title")}
                  </Button>
                  {shouldOpenProductPopup && (
                    <SelectMultiProductPopup
                      t={t}
                      i18n={i18n}
                      open={shouldOpenProductPopup}
                      handleSelect={this.handleSelectMultiProduct}
                      products={voucherDetails?.length > 0 ? [...voucherDetails] : []}
                      handleClose={this.handleProductPopupClose}
                      selectedItem={{ product: null }}
                      productTypeCode={
                        productTypeCode != null
                          ? productTypeCode
                          : this.productTypeCode
                      }
                      allowedAddNew={true}
                      isManagementPurchaseDepartment={true}
                      managementPurchaseDepartment={receiverDepartment} //todo: phòng ban quản lý
                    />
                  )}

                </Grid>
              </Grid>}
              <Grid container spacing={2} className="mt-8">
                <Grid item xs={12}>
                  <MaterialTable
                    data={voucherDetails}
                    columns={columns}
                    options={{
                      sorting: false,
                      toolbar: false,
                      selection: false,
                      actionsColumnIndex: -1,
                      paging: false,
                      search: false,
                      padding: "dense",
                      maxBodyHeight: "273px",
                      minBodyHeight: "273px",
                      rowStyle: (rowData) => ({
                        backgroundColor:
                          rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                      }),
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                      },
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div>
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                    onSelectionChange={(rows) => {
                      this.data = rows;
                    }}
                  />
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
              <SignatureTabComponent
                data={this.state.signatures}
                handleChange={this.handleChangeSignature}
                managementDepartmentId={receiverDepartment?.id}
              />
            </TabPanel>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {(!isView || isProcessed) && <Button variant="contained" color="primary" type="submit">
                {t("general.save")}
              </Button>}
            </div>
          </DialogActions>
        </ValidatorForm>

        {shouldOpenAddSupplierDialog && (
          <SupplierDialog
            t={t}
            i18n={i18n}
            item={{
              name: keySearch,
            }}
            disabledType
            typeCodes={[appConst.TYPE_CODES.NCC_CU]}
            open={shouldOpenAddSupplierDialog}
            handleSelect={this.handleSelectSupplier}
            handleClose={this.handleClosePopup}
          />
        )}
      </Dialog>
    );
  }
}

InventoryReceivingVoucherDialog.contextType = AppContext;
export default InventoryReceivingVoucherDialog;
