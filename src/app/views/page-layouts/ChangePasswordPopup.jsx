import { Fab, Icon, Card, Grid, Divider, Button, DialogActions, Dialog, TextField } from "@material-ui/core";
// import {
//   Card,
//   Icon,
//   Avatar,
//   Grid,
//   Select,
//   FormControl,
//   Divider,
//   IconButton,
//   Button,
//   withStyles,
//   InputLabel,
//   TextField,
//   DialogActions,
//   Dialog
// } from "@material-ui/core";

import { createMuiTheme } from "@material-ui/core/styles";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import DateFnsUtils from "@date-io/date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { Breadcrumb, SimpleCard, EgretProgressBar } from "egret";
import axios from "axios";
import ConstantList from "../../appConfig";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import JwtAuthService from "../../services/jwtAuthService";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst } from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class ChangePasswordPopup extends React.Component {

  state = {
    oldPassword: '',
    password: '',
    confirmPassword: '',
  }

  componentDidMount() {
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      if (value !== this.state.password) {
        return false
      }
      return true
    })
  }

  handleChangePassword = async () => {
    let isChangedOK = false;
    try {

      let formData = new FormData();
      formData.append("oldPassword", this.state?.oldPassword);
      formData.append("password", this.state?.password);
      formData.append("confirmPassword", this.state?.confirmPassword);

      const url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/users/change-password";
      const data = await axios.patch(url, formData);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        isChangedOK = true;
        toast.info("Bạn đã đổi mật khẩu thành công.");
      } else {
        isChangedOK = false;
        toast.warn(data?.data?.data?.[0]?.errorMessage)
      }
    } catch (error) {
      toast.warn('Mật khẩu hiện tại không đúng.')
      toast.clearWaitingQueue();
      this.setState({ errorMessage: err.message });
    } finally {
      if (isChangedOK) {
        await JwtAuthService.logout();
      }
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { t, i18n, handleClose, handleSelect, selectedItem, open, user } = this.props;
    return (
      <Dialog onClose={handleClose} open={open} PaperProps={{
        style: {
          width: 500,
          maxHeight: 800,
          alignContent: 'center',
        },
      }} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth={true} >
        <DialogTitle style={{ cursor: 'move', paddingBottom: 0 }} >
          {t("user.changePass")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleChangePassword}>
          <DialogContent>
            <Grid container spacing={1}>

              <Grid item md={12} sm={12} xs={12}>
                <FormControl fullWidth margin="dense">
                  <TextValidator
                    label={<span><span className="colorRed">*</span>{t('user.current_password')}</span>}
                    id="password-current"
                    className="w-100"
                    name="oldPassword"
                    type="password"
                    value={this.state.oldPassword}
                    onChange={this.handleChange('oldPassword')}
                    validators={['required']}
                    errorMessages={[this.props.t('general.required')]}
                  />
                </FormControl>
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <FormControl fullWidth margin="dense">
                  <TextValidator
                    label={<span><span className="colorRed">*</span>{t('user.pass')}</span>}
                    id="password-current"
                    className="w-100"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                    validators={['required', "matchRegexp:^.{6,}$"]}
                    errorMessages={[this.props.t('general.required'), "Mật khẩu ít nhất có 6 kí tự",]}
                  />
                </FormControl>
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <FormControl fullWidth margin="dense">
                  <TextValidator
                    label={<span><span className="colorRed">*</span>{t('user.confirm_password')}</span>}
                    id="confirm-password"
                    className="w-100"
                    name="confirmPassword"
                    type="password"
                    value={this.state.confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                    validators={['required', 'isPasswordMatch']}
                    errorMessages={[
                      this.props.t('general.required'),
                      this.props.t('general.mismatch')
                    ]}
                  />
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Grid container spacing={2} style={{ marginTop: 10 }} justifyContent="center">
              <Button
                className="mb-16 mr-12 align-bottom"
                variant="contained"
                color="secondary"
                style={{ minWidth: 100 }}
                onClick={() => handleClose()}>{t('general.cancel')}
              </Button>
              <Button
                className="mb-16 align-bottom"
                variant="contained"
                color="primary"
                type="submit"
                style={{ minWidth: 100 }}
              >
                {t('general.update')}
              </Button>
            </Grid>
          </DialogActions>
        </ValidatorForm>

      </Dialog>
    );
  }
}
export default ChangePasswordPopup;