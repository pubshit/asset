import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const SuppliesPurchaseRequestTable = EgretLoadable({
  loader: () => import("./SuppliesPurchaseRequestTable")
});
const ViewComponent = withTranslation()(SuppliesPurchaseRequestTable);

const SuppliesPurchaseRequestRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/supplies_purchase_request",
    exact: true,
    component: ViewComponent
  }
];

export default SuppliesPurchaseRequestRoutes;