import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-requests";
const API_PATH_PURCHASE_REQUEST_STATUS =
  ConstantList.API_ENPOINT +
  "/api/purchase_request_status" +
  ConstantList.URL_PREFIX;
const API_PATH_REQUEST_COMMENT =
  ConstantList.API_ENPOINT + "/api/purchase_request_comment";
const API_PATH_PURCHASE_PRODUCT =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-products";
export const searchByPage = (searchObject) => {
  var url = API_PATH + "/search";
  return axios.post(url, searchObject);
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH_PURCHASE_REQUEST_STATUS + "/checkCode";
  return axios.get(url, config);
};

export const addNewPurchaseRequest = (PurchaseRequest) => {
  return axios.post(API_PATH, PurchaseRequest);
};

export const UpdatePurchaseRequest = (PurchaseRequest, id) => {
  return axios.put(API_PATH + "/" + id, PurchaseRequest);
};

export const updatePurchaseRequestComment = (comment, id) => {
  return axios.put(API_PATH_REQUEST_COMMENT + "/update/" + id, comment);
};
export const createPurchaseRequestComment = (comment) => {
  return axios.post(API_PATH_REQUEST_COMMENT + "/create", comment);
};

export const searchByPageRequestComment = (searchObject) => {
  var url = API_PATH_REQUEST_COMMENT + "/searchByPage";
  return axios.post(url, searchObject);
};

export const deleteRequestComment = (id) => {
  return axios.delete(API_PATH_REQUEST_COMMENT + "/delete/" + id);
};
export const deleteCkeckRequestComment = (id) => {
  return axios.delete(API_PATH_REQUEST_COMMENT + "/deleteCheck/" + id);
};

export const getOneRequestComment = (id) => {
  return axios.get(API_PATH_REQUEST_COMMENT + "/getOne/" + id);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/purchase-request",
    data: searchObject,
    responseType: "blob",
  });
};
