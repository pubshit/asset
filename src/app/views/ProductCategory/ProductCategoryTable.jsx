import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  getItemById,
  searchByPage,
  deleteCheckItem,
  getByRoot,
  exportExampleCategoryProduct,
} from "./ProductCategoryService";
import FileSaver from "file-saver";
import ProductCategoryEditorDialog from "./ProductCategoryEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ImportExcelDialog from "./ImportExcelDialog";
import { appConst } from "app/appConst";
import { Link } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';
import {handleKeyUp, isSuccessfulResponse} from "../../appFunction";
import AppContext from "../../appContext";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: "-15px",
    left: "-30px",
    width: "80px",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class ProductCategoryTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    ProductCategory: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenNotificationPopup: false,
  };
  numSelected = 0;
  rowCount = 0;

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };
  exportExampleImportExcel = () => {
    exportExampleCategoryProduct()
      .then((res) => {
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        FileSaver.saveAs(blob, "Mẫu excel danh mục sản phẩm.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  }

  componentDidMount() {
    this.updatePageData();
    // this.search();
  }
  updatePageData = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    let searchObject = {};
    searchObject.keyword = this.state.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    try {
      setPageLoading(false);
      let {data} = await searchByPage(searchObject);
      let treeValues = [];

      let itemListClone = [...data.content];

      itemListClone.forEach((item) => {
        let items = this.getListItemChild(item);
        treeValues.push(...items);
      });

      this.setState({
        itemList: treeValues,
        totalElements: data.totalElements,
      });

    } catch {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenImportExcelDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenNotificationPopup: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenImportExcelDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, () => {
      this.updatePageData();
    });
  };

  handleDeleteProductCategory = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditProductCategory = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count,
      });
    } else if (this.state.itemList.length === 1 && this.state.page === 1) {
      this.setState({
        page: 1,
      });
    }

    let { t } = this.props;
    deleteCheckItem(this.state.id)
      .then((res) => {
        this.handleDialogClose();
        this.updatePageData();
        if (res.data) {
          toast.success(t("general.deleteSuccess"));
        } else {
          toast.warning(t("ProductCategory.noti.use"));
        }
      })
      .catch((err) => {
        toast.warning(t("ProductCategory.noti.use"));

        // alert('Loại sản phẩm đang sử dụng, không thể xóa')
        // this.handleDialogClose()
      });
  };

  // componentDidMount() {
  //   this.updatePageData();
  // }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { ProductCategory } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < ProductCategory.length; i++) {
      if (
        ProductCategory[i].checked === null ||
        ProductCategory[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (ProductCategory[i].id === item.id) {
        ProductCategory[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      ProductCategory: ProductCategory,
    });
  };
  handleSelectAllClick = (event) => {
    let { ProductCategory } = this.state;
    let { t } = this.props;
    for (var i = 0; i < ProductCategory.length; i++) {
      ProductCategory[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      ProductCategory: ProductCategory,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props;
    for (var i = 0; i < list.length; i++) {
      try {
        await deleteCheckItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    if (listAlert.length === list.length) {
      toast.warning(t("ProductCategory.noti.use_all"));
    } else if (listAlert.length > 0) {
      toast.warning(t("ProductCategory.noti.deleted_unused"));
    } else {
      toast.success(t("general.deleteSuccess"));
    }
  }

  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
    });
  };

  getListItemChild(item) {
    var result = [];
    var root = {};
    root.name = item.name;
    root.code = item.code;
    root.id = item.id;
    root.parentId = item.parentId;
    root.assetClass = item.assetClass;
    result.push(root);
    if (item.children) {
      item.children.forEach((child) => {
        var childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };
  checkType = (type) => {
    switch (type) {
      case appConst.assetTypeObject.TSCD.code:
        return appConst.assetTypeObject.TSCD.name;
        break;
      case appConst.assetTypeObject.CCDC.code:
        return appConst.assetTypeObject.CCDC.name;
        break;
      case appConst.assetTypeObject.VT.code:
        return appConst.assetTypeObject.VT.name;
        break;
      default:
        break;
    }
  };
  
  handleKeyUpSearch = e => handleKeyUp(e, this.search);
  
  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenNotificationPopup,
    } = this.state;
    let TitlePage = t("ProductCategory.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: "100px",
        maxWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data === null) {
                    data = {};
                  }
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  });
                });
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("ProductCategory.code"),
        field: "code",
        minWidth: "200px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("ProductCategory.type"),
        field: "name",
        align: "left",
        minWidth: "160px",
        render: (rowData) => this.checkType(rowData.assetClass),
      },
      {
        title: t("ProductCategory.name"),
        field: "name",
        align: "left",
        minWidth: "400px",
      },
      {
        title: t("ProductCategory.parent"),
        field: "parent.name",
        align: "left",
        minWidth: "400px",
        render: (rowData) =>
          rowData?.parentId
            ? this.state.itemList?.find((item) => item.id === rowData.parentId)
              ?.name
            : "",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('ProductCategory.title') }]} /> */}
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.category"), path: "/list/product_category" },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={6} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                });
              }}
            >
              {t("general.add")}
            </Button>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.importExcel}
            >
              {t("general.importExcel")}
            </Button>
            <Button
              className="mb-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.exportExampleImportExcel}
            >
              Mẫu Excel
            </Button>
            {this.state.shouldOpenImportExcelDialog && (
              <ImportExcelDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={this.state.shouldOpenImportExcelDialog}
                handleOKEditClose={this.handleOKEditClose}
              />
            )}

            {shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t("general.deleteAllConfirm")}
              />
            )}
            {/* <TextField
              label={t('ProductCategory.search')}
              className="mb-16 mr-10"
              type="text"
              name="keyword"
              value={keyword}
              onKeyDown={this.handleKeyDownEnterSearch}
              onChange={this.handleTextChange} />
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary" onClick={() => this.search(keyword)}>
              {t('general.search')}
            </Button> */}
          </Grid>
          <Grid item md={6} sm={12} xs={12} >
            <FormControl fullWidth>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUpSearch}
                placeholder={t("ProductCategory.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      onClick={this.search}
                      className="searchTable"
                    />
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <ProductCategoryEditorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenNotificationPopup && (
                <NotificationPopup
                  title={t("general.noti")}
                  open={shouldOpenNotificationPopup}
                  // onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDialogClose}
                  text={t(this.state.Notification)}
                  agree={t("general.agree")}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: this.state.itemList?.length > 0 ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

ProductCategoryTable.contextType = AppContext;
export default ProductCategoryTable;
