import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/productcategory" + ConstantList.URL_PREFIX;

export const getAllProductCategorys = () => {
  return axios.get(API_PATH+"/1/100000");  
};

export const getProductCategoryByPage = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH+"/"+params;
  return axios.get(url);  
};
export const searchByPage =  (productcategory) => {
  return axios.post(API_PATH + "/searchByText",productcategory);
};

export const getUserById = id => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = id => {
  return axios.delete(API_PATH+"/"+id);
};

export const getItemById = id => {
  return axios.get(API_PATH + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: {id: id, code: code } };
  var url = API_PATH+"/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateProductCategory = ProductCategory => {
  return axios.post(API_PATH , ProductCategory);
};
export const deleteCheckItem = id => {
  return axios.delete(API_PATH +"/delete/"+id);
};

export const checkParent = (dto) => {
  var url = API_PATH+"/checkParent";
  return axios.post(url, dto);
};
export const getByRoot = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = "1/"+pageIndex + "/" + pageSize;
  var url = API_PATH+"/getByRoot/"+params;
  return axios.get(url);  
};
export const getByRootForDropList = (searchObject) => {
  let params = searchObject.assetClass + "/" + searchObject.pageIndex + "/" + searchObject.pageSize;
  return axios.get(API_PATH + "/getByRoot/" + params);  
};
export const exportExampleCategoryProduct = (CategoryProduct) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/fileDownload/exportExampleCategory",
    data:CategoryProduct ,
    responseType: "blob",
  }
  );
}