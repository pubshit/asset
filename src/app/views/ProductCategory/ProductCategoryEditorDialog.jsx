import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  getAllProductCategorys,
  checkParent,
  addNewOrUpdateProductCategory,
  searchByPage,
  checkCode,
} from "./ProductCategoryService";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst } from "app/appConst";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { handleThrowResponseMessage } from "app/appFunction";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class ProductCategoryEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    parent: {},
    parentSelect: {},
    assetClass: "",
    assetClassParent: appConst.assetType,
    shouldOpenNotificationPopup: false,
    Notification: "",
    loading: false,
  };
  handleChangeParent = (parent) => {
    this.setState({
      parent,
    })
  };
  handleChangeAssetClass = (event, source) => {
    this.setState({ assetClass: event.target.value });
  };
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };
  handleFormSubmit = async () => {
    await this.openCircularProgress();
    let { id } = this.state;
    let { code } = this.state;
    let { t } = this.props;
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(t("ProductCategory.noti.dupli_code"));
        toast.clearWaitingQueue();
        this.setState({ loading: false });
      } else {
        let sendData = this.convertData();
        if (id) {
          checkParent(sendData).then((isCheck) => {
            if (isCheck.data) {
              this.setState({ loading: false });
              toast.error(t("ProductCategory.noti.updateFailParent"));
              toast.clearWaitingQueue();
            } else {
              addNewOrUpdateProductCategory(sendData).then((data) => {
                if(data?.data?.code === appConst.CODE.SUCCESS) {
                  toast.success(t("ProductCategory.noti.updateSuccess"));
                  this.props.handleOKEditClose();
                } else {
                  this.setState({ loading: false });
                  handleThrowResponseMessage(data)
                }
              });
            }
          });
        } else {
          addNewOrUpdateProductCategory(sendData).then(() => {
            toast.info(t("ProductCategory.noti.addSuccess"));
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  convertData = () => {
    return {
      id: this.state.id,
      name: this.state.name,
      code: this.state.code,
      assetClass: this.state.assetClass,
      parentId: this.state.parent?.id,
    }
  }

  componentWillMount() {
    getAllProductCategorys().then((result) => {
      let parentSelect = result.data.content;
      this.setState({ parentSelect: parentSelect });
    });
    let { item } = this.props;
    this.setState({
      ...this.props.item,
      parent: item?.parentId
        ? {
          id: item?.parentId,
          name: item?.parentName,
          code: item?.parentCode,
        }
        : null,
    });
  }
  componentDidMount() { }

  render() {
    let { open, handleClose, handleOKEditClose, handleDialogClose, t, i18n } =
      this.props;
    let {
      id,
      name,
      code,
      parent,
      parentId,
      parentSelect,
      shouldOpenNotificationPopup,
      loading,
      assetClassParent,
      assetClass,
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="xs">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <DialogTitle className="cursor-move pb-0" id="draggable-dialog-title">
          <h4 className="">{t("ProductCategory.saveUpdate")}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={t("ProductCategory.parent")}
                  searchFunction={getAllProductCategorys}
                  searchObject={{}}
                  displayLable="name"
                  isNoRenderChildren
                  value={parent}
                  onSelect={this.handleChangeParent}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <FormControl className=" w-100" style={{ width: "400px" }}>
                  <InputLabel htmlFor="assetClass">
                    {t("ProductCategory.type")}
                  </InputLabel>
                  <Select
                    value={assetClass}
                    onChange={(event) => this.handleChangeAssetClass(event)}
                    inputProps={{
                      name: "assetClass",
                      id: "assetClass",
                    }}
                  >
                    {assetClassParent &&
                      assetClassParent.length > 0 &&
                      assetClassParent.map((type) => (
                        <MenuItem key={type.code} value={type.code}>
                          {type.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductCategory.code")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", "matchRegexp:^[a-zA-Z0-9-_.]*$"]}
                  errorMessages={[t("general.required"), "Chỉ chứa ký tự a-z, A-Z, 0-9, -, _, ."]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("ProductCategory.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                className="mr-14"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default ProductCategoryEditorDialog;
