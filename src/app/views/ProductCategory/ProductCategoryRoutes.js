import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const ProductCategoryTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./ProductCategoryTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(ProductCategoryTable);

const ProductCategoryRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/product_category",
    exact: true,
    component: ViewComponent
  }
];

export default ProductCategoryRoutes;
