import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { Button, Dialog, DialogActions, } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { createReceipt, updateReceipt, } from "./ReceiptService";

import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import ReceiptScrollableTabsButtonForce from "./ReceiptScrollableTabsButtonForce";
import { getAssetDocumentById, getNewCodeDocument, } from "../Asset/AssetService";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, STATUS_DEPARTMENT, variable } from "app/appConst";
import {
  checkObject,
  formatDateDto,
  getTheHighestRole, getUserInformation,
  handleThrowResponseMessage,
  isCheckLenght,
  PaperComponent
} from "app/appFunction";
import AppContext from "app/appContext";
import localStorageService from "app/services/localStorageService";
import { ConfirmationDialog } from 'egret';
import { convertFromToDate } from "../../appFunction";
import moment from "moment";

toast.configure();

class ReceiptEditorDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Allocation,
    rowsPerPage: 10,
    page: 0,
    assetVouchers: [],
    statusIndexOrders: [0, 1], // cấp phát tài sản lưu kho, nhập kho
    handoverPerson: null,
    managementDepartment: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    suggestDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenManagementDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    totalElements: 0,
    asset: {},
    shouldOpenPersonPopup: false,
    shouldOpenNotificationPopup: false,
    textNotificationPopup: "",
    isAssetAllocation: true,
    shouldOpenPopupAssetFile: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_ALLOCATION,
    assetDocumentList: [],
    listAssetDocumentId: [],
    count: 1,
    allocationStatus: null,
    loading: false,
    keyword: '',
    checkPermissionUserDepartment: true,
    check: false,
    assetId: '',
    usePerson: null,
    managementDepartmentId: '',
    documents: [],
    isConfirm: false,
    shouldOpenRefuseTransfer: false,
    openPrintQR: false,
    isViewAssetFile: false,
  };

  convertDto = (state, status) => {
    return {
      id: state?.id,
      status: status || state?.status?.indexOrder,
      assetVouchers: state?.assetVouchers?.map(i => {
        return {
          assetId: i?.assetId,
          dxRequestQuantity: i?.dxRequestQuantity,
          id: i?.id,
          note: i?.note,
          quantity: i?.approvedQuantity
        }
      }),
      name: state?.name,
      approvedDate: state?.approvedDate ? formatDateDto(state?.approvedDate) : null,
      storeId: state?.store?.id,
      departmentId: state?.handoverDepartment?.id,
      personName: state?.handoverPerson?.personDisplayName,
      personId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      suggestDate: state?.suggestDate ? formatDateDto(state?.suggestDate) : null,
      receiptDepartmentId: state?.receiverDepartment?.id,
      receiptPersonName: state?.receiverPerson?.personDisplayName,
      receiptPersonId: state?.receiverPerson?.personId,
      assetClass: appConst.assetClass.TSCD,
    };
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
    });
  };
  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };
  openCircularProgress = () => {
    this.setState({ loading: true });
  };
  validateSubmit = () => {
    let {
      assetVouchers,
      handoverPerson,
      handoverDepartment,
      receiverDepartment,
      allocationStatus,
      suggestDate
      // issueDate 
    } = this.state;
    const isSuggestDate = assetVouchers.some(asset => moment(asset.dateOfReception).isAfter(suggestDate, 'day'));
    let { isVisibility, isStatus, t } = this.props;
    if (isSuggestDate) { // ngày đề xuất nhỏ hơn ngày tiếp nhận
      toast.warning(t('allocation_asset.minSuggestDate'))
      return false
    }
    if (!handoverDepartment) {
      toast.warning(t('allocation_asset.missingHandoverDepartment'));
      return false;
    }
    if (!assetVouchers || assetVouchers?.length === 0) {
      toast.warning(t('allocation_asset.missingCCDC'));
      return false;
    }
    if (this.validateAssetVouchers()) return;
    if (!receiverDepartment) {
      toast.warning(t('allocation_asset.missingReceiverDepartment'));
      return false;
    }
    return true;
  }
  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    let { t } = this.props;

    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning(t('allocation_asset.errMessageNote'));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  checkQuantityAssets = (arr = []) => {
    return arr.find(x => x?.approvedQuantity > 1);
  };

  handleStatus = async (status) => {
    const { t, handleOKEditClose = () => { }, handleClose = () => { } } = this.props;
    if (!this.state.approvedDate && this.state?.isConfirm) {
      toast.warning(t('allocation_asset.isEmtyApprovedDate'));
      return;
    }
    if(this.checkQuantityAssets(this?.state?.assetVouchers)) {
      toast.warning(t('Asset.invalidQuantityAsset'));
      return;
    }
    let dataState = this.convertDto(this.state, status)
    let { id } = this.state;

    let res = await updateReceipt(id, dataState);
    if (appConst.CODE.SUCCESS === res.data?.code) {
      handleOKEditClose();
      handleClose();
      toast.success(t("general.updateSuccess"));
    } else {
      handleThrowResponseMessage(res);
    }
  }

  handleFormSubmit = async (e) => {
    const { setPageLoading } = this.context;
    const { id } = this.state;
    const { t, handleOKEditClose = () => { }, handleClose = () => { } } = this.props;

    if (e.target.id !== "parentAssetAllocation" || !this.validateSubmit()) return;

    const dataState = this.convertDto(this.state);
    setPageLoading(true);

    try {
      let res;
      if (id) {
        res = await updateReceipt(id, dataState);
        if (appConst.CODE.SUCCESS === res.data?.code) {
          handleOKEditClose();
          handleClose();
          toast.success(t("general.updateSuccess"));
        } else {
          handleThrowResponseMessage(res);
        }
      } else {
        res = await createReceipt(dataState);
        if (appConst.CODE.SUCCESS === res.data?.code) {
          handleOKEditClose();
          handleClose();
          toast.success(t("general.addSuccess"));
        } else {
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      console.log(error)
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    })
  }

  componentDidMount() {
    let { item, isVisibility } = this.props;
    const { currentUser, departmentUser } = getUserInformation();
    const roles = getTheHighestRole();
    const {
      isRoleAssetUser,
      isRoleAssetManager
    } = roles;

    if (item?.id) {
      
      this.setState({
        ...item,
        isVisibility,
        receiverDepartment: {
          text: item?.receiptDepartmentName,
          id: item?.receiptDepartmentId,
        },
        receiverPerson: {
          personId: item?.receiptPersonId,
          personDisplayName: item?.receiptPersonName,
        },
        handoverPerson: {
          personId: item?.personId,
          personDisplayName: item?.personName,
        },
        handoverDepartment: {
          text: item?.departmentName,
          name: item?.departmentName,
          id: item?.departmentId,
        }, store: {
          id: item?.storeId,
          name: item?.storeName,
        },
        assetVouchers: item?.assetVouchers.map((item) => {
          let data = {
            ...item,
            unitName: item?.skuName,
            approvedQuantity: item?.quantity,
          };
          data.asset = {
            ...data,
            code: data.code || data.assetCode || null,
            id: data.assetId,
          };
          return data;
        }),
      });
    } else {
      this.setState({
        ...item,
        isVisibility,
        receiverDepartment: isRoleAssetUser ? departmentUser : null,
        receiverPerson: isRoleAssetUser
          ? {
            personId: currentUser?.person?.id,
            personDisplayName: currentUser?.person?.displayName,
          }
          : null,
        handoverPerson: isRoleAssetManager
          ? {
            personId: currentUser?.person?.id,
            personDisplayName: currentUser?.person?.displayName,
          }
          : null,
        handoverDepartment: isRoleAssetManager ? departmentUser : null,
        status: appConst.STATUS_RECEIPT.waitApprove,
      });
    }
  }


  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleAssetPopupOpen = () => {
    let { t } = this.props;
    let { statusIndexOrders, handoverDepartment, store } = this.state;
    if (handoverDepartment) {
      this.setState({
        shouldOpenAssetPopup: true,
        item: {},
        statusIndexOrders: statusIndexOrders,
      });
    } else if (!handoverDepartment) {
      toast.info(t('allocation_asset.missingHandoverDepartment'));
    } else if (!store) {
      toast.info(t('receipt.missingStore'));
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item != null && item.id != null) {
      if (assetVouchers == null) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      } else {
        let hasInList = false;
        assetVouchers.forEach((element) => {
          if (element.asset.id == item.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState(
      { assetVouchers, totalElements: assetVouchers.length },
      function () {
        this.handleAssetPopupClose();
      }
    );
  };

  handleSelectAssetAll = (items) => {
    let { assetVouchers } = this.state;
    let { t } = this.props;

    items.map((element) => {
      element.asset.useDepartment = this.state.receiverDepartment;
      element.assetId = element.assetId || element?.asset?.id;
      element.name = element.name || element?.asset?.name;
      element.code = element.code || element?.asset?.code;
      element.serialNumber = element.serialNumber || element?.asset?.serialNumber;
      element.model = element.model || element?.asset?.model;
      element.yearOfManufacture = element.yearOfManufacture || element?.asset?.yearOfManufacture;
      element.originalCost = element.originalCost || element?.asset?.originalCost;
      element.dateOfReception = element.dateOfReception || element?.asset?.dateOfReception;
      element.dxRequestQuantity = 1;
      element.approvedQuantity = element.approvedQuantity || 1;
    });
    if (items.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        // this.handleAssetPopupClose();
      });
      toast.warning(t("allocation_asset.missingCCDC"));
    }
  };

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    });
  };
  handlePersonPopupOpen = () => {
    this.setState({
      shouldOpenPersonPopup: true,
    });
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  handleManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: false,
    });
  };
  handleManagementDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = item;
    this.setState({
      handoverDepartment: department,
      handoverDepartmentId: department?.id
    }, function () {
      this.handleManagementDepartmentPopupClose();
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: { id: item?.id, name: item?.text },
      receiptDepartmentId: item?.id,
    },
    );
  };

  selectUsePerson = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      assetVouchers.forEach(async (assetVoucher) => {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.usePerson = item ? item : null;
          assetVoucher.usePersonId = item?.id;
          assetVoucher.usePersonId = item ? item?.personId : null;
          assetVoucher.usePersonDisplayName = item?.personDisplayName;
        }
      });
      this.setState({ assetVouchers: assetVouchers });
    }
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item ? item : null,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      store: null,
      managementDepartmentId: item?.id,
      assetVouchers: [],
      listHandoverPerson: [],
    });
  };

  selectReceiverDepartment = (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );
    this.setState({
      receiverDepartment: item ? item : null,
      receiptDepartmentId: item?.id,
      listReceiverPerson: [],
      assetVouchers: assetVouchers,
      isCheckReceiverDP: item ? false : true,
      receiverPerson: null,
      receiverPersonId: null,
      usePerson: null,
    });
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes != null && attributes.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (
          attributes[index].attribute &&
          attributes[index].attribute.id == rowData.attribute.id
        ) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () { });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isViewAssetFile: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  selectAlocationStatus = (item) => {
    this.setState({
      status: item
    });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        let { name, value } = event.target;
        if (["quantity", "approvedQuantity"].includes(name)) {
          assetVoucher[name] = +value;
        } else {
          assetVoucher[name] = value;
        }
      }
    });
    this.setState(
      { assetVouchers: assetVouchers },
    );
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleSetDataStatus = (data) => {
    let { id } = this.state;

    const statusMappings = {
      [appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder]: id ? [
        // Sửa phiếu ở TT mới tạo 
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
      ] : [
        // Tạo mới phiếu 
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder]: [
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder]: [
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder
      ]
    };

    const allocationStatusIndexOrder = this.state.allocationStatus?.indexOrder;
    const dataNew = data.filter((item) => statusMappings[allocationStatusIndexOrder].includes(item.indexOrder));

    this.setState({
      listStatus: dataNew
    });

  };

  handleChangeValueOpenQR = (newValue) => {
    this.setState({
      openPrintQR: newValue
    });
  };

  handleSelect = (value, name) => {
    this.setState({ [name]: value })
    if (name === "store") {
      this.setState({ assetVouchers: [] })
    }
  }
  render() {
    let { open, t, i18n, isStatus, isHideButton } = this.props;
    let {
      loading,
      isConfirm,
      isVisibility,
      textNotificationPopup,
      shouldOpenNotificationPopup,
      status
    } = this.state;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };

    const isDangDuyet = status?.indexOrder === appConst.listStatusSuggestSuppliesObject.DANG_DUYET.indexOrder;
    const { isRoleOrgAdmin, departmentUser } = getTheHighestRole();
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}
        {this.state?.shouldOpenRefuseTransfer &&
          <ConfirmationDialog
            title={t('general.confirm')}
            open={this.state?.shouldOpenRefuseTransfer}
            onConfirmDialogClose={this.handleCloseRefuseTransferDialog}
            onYesClick={() => this.handleStatus(appConst.STATUS_RECEIPT.refuse.indexOrder)}
            text={t('general.cancel_receive_assets')}
            agree={t('general.agree')}
            cancel={t('general.cancel')}
          />
        }
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>

        <ValidatorForm
          id="parentAssetAllocation"
          ref="form"
          name={variable.listInputName.formAllocation}
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "3px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("receipt.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent>
            <ReceiptScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAlocationStatus={this.selectAlocationStatus}
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleManagementDepartmentPopupClose={
                this.handleManagementDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              selectReceiverDepartment={this.selectReceiverDepartment}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              handleManagementDepartmentPopupOpen={
                this.handleManagementDepartmentPopupOpen
              }
              handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleDateChange={this.handleDateChange}
              handleDepartmentPopupClose={this.handleDepartmentPopupClose}
              handlePersonPopupOpen={this.handlePersonPopupOpen}
              handlePersonPopupClose={this.handlePersonPopupClose}
              handleAssetPopupOpen={this.handleAssetPopupOpen}
              handleSelectAssetAll={this.handleSelectAssetAll}
              handleAssetPopupClose={this.handleAssetPopupClose}
              removeAssetInlist={this.removeAssetInlist}
              selectUsePerson={this.selectUsePerson}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleRowDataCellChange={this.handleRowDataCellChange}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              getAssetDocument={this.getAssetDocument}
              isVisibility={isVisibility}
              isStatus={isStatus}
              check={this.state.check}
              handleFormSubmit={this.handleFormSubmit}
              isAllocation={this.props.isAllocation}
              isHideButton={isHideButton}
              managementDepartmentId={this.state.managementDepartmentId}
              handleSetDataSelect={this.handleSetDataSelect}
              handleSetDataStatus={this.handleSetDataStatus}
              handleChange={this.handleChange}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleChangeValueOpenQR={this.handleChangeValueOpenQR}
              handleRowDataCellViewAssetFile={this.handleRowDataCellViewAssetFile}
              handleSelect={this.handleSelect}
            />

          </DialogContent>
          <DialogActions className="pr-24 pb-16">
            <Button
              className="btnClose ml-12"
              variant="contained"
              color="secondary"
              onClick={() =>
              (this.props?.isAllocation
                ? (this.props?.isUpdateAsset
                  ? this.props.handleCloseAllocationEditorDialog()
                  : this.props.handleOKEditClose())
                : this.props.handleClose())
              }
            >
              {t("InstrumentToolsTransfer.close")}
            </Button>
            {isConfirm ? <>
              {!isDangDuyet && (
                isRoleOrgAdmin
                || (this.state?.departmentId === departmentUser?.id)
              ) && <Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={() => this.handleStatus(appConst.STATUS_RECEIPT.review.indexOrder)}
              >
                  {t("SuggestedSupplies.review")}
                </Button>}
              <Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={() => this.handleStatus(appConst.STATUS_RECEIPT.allocated.indexOrder)}
              >
                {t("InstrumentToolsTransfer.confirm")}
              </Button>
              <Button
                className="btnRefuse ml-12"
                variant="contained"
                onClick={() => {
                  this.setState({
                    shouldOpenRefuseTransfer: true
                  });
                }}
              >
                {t("InstrumentToolsTransfer.refuse")}
              </Button>
            </> : <>
              {(!isVisibility || this.props.isStatus) &&
                <Button
                  id="save"
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
              {
                this.state?.allocationStatusIndex === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder &&
                <Button
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  onClick={() => {
                    this.handleChangeValueOpenQR(true);
                  }}
                >
                  {t("Asset.PrintQRCode")}
                </Button>
              }
            </>}
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

ReceiptEditorDialog.contextType = AppContext;
export default ReceiptEditorDialog;
