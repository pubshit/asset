import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { Icon, IconButton, TextField } from "@material-ui/core";
import { appConst } from "app/appConst";
import moment from "moment";
import { personSearchByPage } from "../AssetTransfer/AssetTransferService";
import { useEffect } from "react";
import { useState } from "react";
import { convertNumberPrice, formatDateDto, formatTimestampToDate } from "app/appFunction";

function MaterialButton(props) {
  const item = props?.item;
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => props?.onSelect(item, appConst.active.delete)}
      >
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

function ListAssetTable(props) {
  const t = props?.t;
  let columns = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1
    },
    {
      title: t("Asset.code"),
      field: "code",
      align: "left",
      maxWidth: 120,
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Asset.serialNumber"),
      field: "serialNumber",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("Asset.model"),
      field: "model",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("Asset.yearOfManufactureTable"),
      field: "yearOfManufacture",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.dateOfReception"),
      field: "custom",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.dateOfReception
        ? formatTimestampToDate(rowData.dateOfReception)
        : ""
    },
    {
      title: t("Asset.originalCost"),
      field: "originalCost",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right",
      },
      render: rowData => rowData.originalCost
        ? convertNumberPrice(rowData.originalCost)
        : ""
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
    },
  ];
  return (
    <MaterialTable
      data={props?.itemAsset?.assetVouchers ? props?.itemAsset?.assetVouchers : []}
      columns={columns}
      options={{
        draggable: false,
        toolbar: false,
        selection: false,
        actionsColumnIndex: -1,
        paging: false,
        search: false,
        sorting: false,
        padding: "dense",
        rowStyle: (rowData) => ({
          backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
        }),
        headerStyle: {
          backgroundColor: "#358600",
          color: "#fff",
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        maxBodyHeight: "225px",
        minBodyHeight: "225px",
      }}
      localization={{
        body: {
          emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
        },
      }}
      components={{
        Toolbar: (props) => (
          <div style={{ witdth: "100%" }}>
            <MTableToolbar {...props} />
          </div>
        ),
      }}
      onSelectionChange={(rows) => {
        this.data = rows;
      }}
    />
  );
}

export default ListAssetTable;
