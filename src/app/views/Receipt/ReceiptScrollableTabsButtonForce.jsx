import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Grid,
  Icon,
  IconButton,
} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { appConst, STATUS_STORE } from "app/appConst";
import {
  checkInvalidDate,
  convertNumberPrice,
  getTheHighestRole,
  isCheckLenght, filterOptions, formatTimestampToDate, handleKeyDownIntegerOnly,
  getUserInformation
} from "app/appFunction";
import MaterialTable, { MTableToolbar } from 'material-table';
import React, { useEffect, useState } from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import { searchByPage } from "../Store/StoreService";
import { getUserDepartmentAll, searchByPageDepartmentNew } from "../Department/DepartmentService";


function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, appConst.active.delete)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ReceiptScrollableTabsButtonForce(props) {
  const { t, i18n } = props;
  let {
    statusEdit
  } = props?.item;

  let roles = getTheHighestRole();
  let { isRoleAssetUser, isRoleAssetManager } = roles;
  const classes = useStyles();

  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });

  let isShowCode = props.item?.id;
  let isNotChangeStatus = props.item?.id && (
    statusEdit === appConst.STATUS_RECEIPT.approved.indexOrder
  );
  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: props?.item?.handoverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code
  };

  let optionsStatus = statusEdit === appConst.STATUS_RECEIPT.refuse.indexOrder ?
    appConst.LIST_RECEIPT.filter(i => [
      appConst.STATUS_RECEIPT.refuse.indexOrder,
      appConst.STATUS_RECEIPT.approved.indexOrder
    ].includes(i?.indexOrder))
    : appConst.LIST_RECEIPT.filter(i => [
      appConst.STATUS_RECEIPT.waitApprove.indexOrder,
      appConst.STATUS_RECEIPT.approved.indexOrder
    ].includes(i?.indexOrder))

  let isShowApproveDate = [
    appConst.STATUS_RECEIPT.refuse.indexOrder,
    appConst.STATUS_RECEIPT.allocated.indexOrder,
  ].includes(statusEdit)
    || props?.item?.isConfirm;

  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255)
    })
  }, [props?.item?.assetVouchers]);

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.handoverDepartment?.id ?? '',
    });

    setSearchParamUserByReceiverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.receiverDepartment?.id ?? '',
    });
  }, [props?.item?.handoverDepartment?.id, props?.item?.receiverDepartment]);


  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      hidden: isNotChangeStatus || (props.isVisibility && props.item?.isEditRefuseStatus),
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      (
        <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData.asset.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />)
    },
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: 50,
      align: 'left',
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => {
        return ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData?.tableData?.id + 1)
      }
    },
    {
      title: t("Asset.code"),
      field: "code",
      align: "left",
      maxWidth: 120,
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Asset.serialNumber"),
      field: "serialNumber",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("Asset.model"),
      field: "model",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("receipt.dxRequestQuantity"),
      field: "quantity",
      minWidth: 100,
      align: "left",
      render: rowData => (
        <TextValidator
          className="w-100"
          type="number"
          name="quantity"
          value={rowData.dxRequestQuantity}
          InputProps={{
            readOnly: props.isVisibility,
            disableUnderline: props.isVisibility,
            inputProps: {
              className: "text-center"
            },
          }}
          disabled={!props.isVisibility}
        />
      )
    },
    {
      title: t("receipt.approvedQuantity"),
      field: "quantity",
      minWidth: 100,
      align: "left",
      render: rowData => (
        <TextValidator
          className="w-100"
          onChange={e => props.handleRowDataCellChange(rowData, e)}
          onKeyDown={handleKeyDownIntegerOnly}
          type="number"
          name="approvedQuantity"
          value={rowData.approvedQuantity}
          InputProps={{
            readOnly: props.isVisibility,
            disableUnderline: props.isVisibility,
            inputProps: {
              className: "text-center"
            },
          }}
          disabled={isRoleAssetUser || !props?.item?.isConfirm}
          validators={["minNumber:0", "maxNumber:1"]}
          errorMessages={[t("general.nonNegativeNumber"), t("receipt.maxQuantityFixedAsset"),]}
        />
      )
    },
    {
      title: t("Asset.yearOfManufactureTable"),
      field: "yearOfManufacture",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.dateOfReception"),
      field: "custom",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData.dateOfReception
        ? formatTimestampToDate(rowData.dateOfReception)
        : ""
    },
    {
      title: t("Asset.originalCost"),
      field: "originalCost",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right",
      },
      render: rowData => rowData.originalCost
        ? convertNumberPrice(rowData.originalCost)
        : ""
    },
    {
      title: t("Product.amount"),
      field: "amount",
      minWidth: "130px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        let unitPrice = Number(rowData?.originalCost) > 0 ? rowData?.originalCost : rowData?.originalCostPerOne;
        return convertNumberPrice(unitPrice * rowData?.approvedQuantity) || "";
      },
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
      render: rowData =>
        <TextValidator
          multiline
          maxRows={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note)}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props.isVisibility && props.item?.isEditRefuseStatus,
            disableUnderline: props.isVisibility && props.item?.isEditRefuseStatus,
          }}
          validators={["isLengthValid"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name)
    }
  }
  const searchDepartmentObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: getUserInformation().organization?.org?.id,
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={1} style={{ marginBottom: "8px" }}>
        {isShowCode && <Grid item md={2} sm={6} xs={6}>
          <TextValidator
            className={props.className ? props.className : "w-100"}
            label={t("receipt.code")}
            value={props.item?.code || ""}
            InputProps={{
              readOnly: true,
            }}
          />
        </Grid>}
        <Grid item md={isShowCode ? 2 : 4} sm={isShowCode ? 6 : 12} xs={isShowCode ? 6 : 12}>
          <CustomValidatePicker
            fullWidth
            margin="none"
            id="mui-pickers-date"
            className="w-100"
            label={
              <span>
                <span className="colorRed">*</span>
                {t('receipt.suggestDate')}
              </span>}
            inputVariant="standard"
            type="text"
            autoOk={true}
            format="dd/MM/yyyy"
            name={'suggestDate'}
            value={props?.item?.suggestDate}
            onChange={date => props.handleDateChange(date, "suggestDate")}
            readOnly={props.isVisibility || isNotChangeStatus}
            minDate={
              !props.isVisibility && (
                props.isAllocation
                  ? (new Date(props?.item?.dateOfReception))
                  : new Date("1/1/1900")
              )
            }
            maxDate={!props.isVisibility && new Date()}
            maxDateMessage={t("allocation_asset.maxDateMessage")}
            InputProps={{
              readOnly: props.isVisibility || props?.isAllocation,
            }}
            invalidDateMessage={t("general.invalidDateFormat")}
            validators={['required']}
            errorMessages={[t('general.required')]}
            minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
            onBlur={() => handleBlurDate(props?.item?.suggestDate, "suggestDate")}
            clearable
            clearLabel={t("general.remove")}
            cancelLabel={t("general.cancel")}
            okLabel={t("general.select")}
          />
        </Grid>
        {isShowApproveDate && <Grid item md={4} sm={12} xs={12}>
          <CustomValidatePicker
            fullWidth
            margin="none"
            id="mui-pickers-date"
            className="w-100"
            label={
              <span>
                <span className="colorRed">*</span>
                {t('Asset.approvedDate')}
              </span>}
            inputVariant="standard"
            type="text"
            autoOk={true}
            format="dd/MM/yyyy"
            name={'approvedDate'}
            InputProps={{
              readOnly: true,
            }}
            value={props?.item?.approvedDate}
            onChange={date => props.handleDateChange(date, "approvedDate")}
            readOnly={props.isVisibility && ![appConst.STATUS_RECEIPT.refuse.indexOrder].includes(statusEdit)}
            minDate={new Date(props?.item?.suggestDate)}
            maxDateMessage={t("general.maxDateDefault")}
            invalidDateMessage={t("general.invalidDateFormat")}
            validators={['required']}
            errorMessages={[t('general.required')]}
            minDateMessage={t("allocation_asset.minApprovedDateMessage")}
            onBlur={() => handleBlurDate(props?.item?.approvedDate, "approvedDate")}
            clearable
            clearLabel={t("general.remove")}
            cancelLabel={t("general.cancel")}
            okLabel={t("general.select")}
          />
        </Grid>}
        <Grid item md={isShowApproveDate ? 4 : 8} sm={12} xs={12}>
          <TextValidator
            className={props.className ? props.className : "w-100"}
            label={
              <span>
                <span className="colorRed">* </span>
                {t("receipt.name")}
              </span>
            }
            InputProps={{
              readOnly: props.isVisibility || isNotChangeStatus,
            }}
            value={props.item?.name}
            name="name"
            onChange={props?.handleChange}
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
        </Grid>

        <Grid item md={4} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span className="colorRed">* </span>
                {t("maintainRequest.status")}
              </span>
            }
            searchFunction={() => { }}
            listData={optionsStatus || []}
            setListData={props.handleSetDataStatus}
            value={props.item?.status ? props.item?.status : null}
            displayLable={'name'}
            readOnly={(props.isVisibility && !props.isStatus) || isNotChangeStatus}
            onSelect={props.selectAlocationStatus}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12} >
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span className="colorRed">* </span>
                <span> {t("allocation_asset.handoverDepartment")}</span>
              </span>
            }
            searchFunction={searchByPageDepartmentNew}
            searchObject={searchDepartmentObject}
            listData={props.item.listHandoverDepartment}
            setListData={(data) => props.handleSetDataSelect(data, "listHandoverDepartment")}
            displayLable={'name'}
            isNoRenderParent={true}
            isNoRenderChildren={true}
            showCode="code"
            readOnly={props.isVisibility || props?.isAllocation || isNotChangeStatus || isRoleAssetManager}
            value={props.item?.handoverDepartment || null}
            onSelect={props.selectHandoverDepartment}
            validators={["required"]}
            errorMessages={[t('general.required')]}
            typeReturnFunction="category"
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>

        <Grid item md={4} sm={12} xs={12} >
          <AsynchronousAutocompleteSub
            className="w-100"
            label={
              <span>
                {t("allocation_asset.handoverPerson")}
              </span>
            }
            disabled={!props?.item?.handoverDepartment?.id}
            searchFunction={getListUserByDepartmentId}
            searchObject={searchParamUserByHandoverDepartment}
            displayLable="personDisplayName"
            typeReturnFunction="category"
            readOnly={props.isVisibility || isNotChangeStatus}
            value={props?.item?.handoverPerson ?? ''}
            onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span style={{ color: "red" }}>* </span>
                <span> {t("receipt.store")}</span>
              </span>
            }
            searchFunction={searchByPage}
            searchObject={searchObjectStore}
            displayLable={'name'}
            readOnly={props.isVisibility || isNotChangeStatus}
            value={props.item?.store ? props.item?.store : null}
            onSelect={(value) => props.handleSelect(value, "store")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
            filterOptions={filterOptions}
            disabled={!props?.item?.handoverDepartment?.id}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12} >
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span className="colorRed">* </span>
                <span> {t("allocation_asset.receiverDepartment")}</span>
              </span>
            }
            searchFunction={getUserDepartmentAll}
            searchObject={props.receiverDepartmentSearchObject}
            listData={props.item.listReceiverDepartment}
            setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
            displayLable={'text'}
            isNoRenderChildren
            isNoRenderParent
            readOnly={props.isVisibility || isNotChangeStatus || isRoleAssetUser}
            value={props.item.receiverDepartment ? props.item.receiverDepartment : null}
            onSelect={props.selectReceiverDepartment}
            validators={["required"]}
            errorMessages={[t('general.required')]}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12} >
          <AsynchronousAutocompleteSub
            className="w-100"
            label={
              <span>
                {t("AssetTransfer.receiverPerson")}
              </span>
            }
            disabled={!props?.item?.receiverDepartment?.id}
            searchFunction={getListUserByDepartmentId}
            searchObject={searchParamUserByReceiverDepartment}
            displayLable="personDisplayName"
            typeReturnFunction="category"
            readOnly={props.isVisibility || isNotChangeStatus}
            value={props?.item?.receiverPerson ?? ''}
            onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
      </Grid>
      <Grid container item md={2} sm={12} xs={12}>
        <Button
          variant="contained"
          color="primary"
          size="small"
          className="mb-16 w-100"
          disabled={statusEdit === appConst.STATUS_RECEIPT.approved.indexOrder || !props.item.store?.id || (props.isVisibility && props.item?.isEditRefuseStatus)}
          onClick={props.handleAssetPopupOpen}
        >
          {t('general.select_asset')}
        </Button>
        {props.item.shouldOpenAssetPopup && (
          <SelectAssetAllPopup
            open={props.item.shouldOpenAssetPopup}
            handleSelect={props.handleSelectAssetAll}
            assetVouchers={props.item.assetVouchers ? props.item.assetVouchers : []}
            handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
            voucherType={props.item.type}
            handoverDepartment={props.item?.handoverDepartment}
            isAssetAllocation={true}
            statusIndexOrders={props.item.statusIndexOrders}
            dateOfReceptionTop={props.item.issueDate}
            getAssetDocumentList={props.getAssetDocumentList}
            managementDepartmentId={props.managementDepartmentId}
            issueDate={props.item?.issueDate}
            storeId={props.item?.store?.id}
            isSearchForAllocation
          />
        )}
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={12}>
          <MaterialTable
            data={props.item.assetVouchers ? props.item.assetVouchers : []}
            columns={columns}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: 'dense',
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              maxBodyHeight: '225px',
              minBodyHeight: '225px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}