import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";
const AssetAllocationTable = EgretLoadable({
  loader: () => import("./ReceiptTable"),
});
const ViewComponent = withTranslation()(AssetAllocationTable);

const ReceiptRoutes = [
  {
    path: ConstantList.ROOT_PATH + "fixed-assets/receipt",
    exact: true,
    component: ViewComponent,
  },
];

export default ReceiptRoutes;
