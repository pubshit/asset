import React, {Component, useContext} from "react";
import {
  Checkbox,
  FormControlLabel,
  Grid,
  Button,
  withStyles,
  CircularProgress,
  InputAdornment,
  IconButton,
  Switch
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { withRouter } from "react-router-dom";
import { Helmet } from 'react-helmet';
import { loginWithEmailAndPassword } from "../../redux/actions/LoginActions";
import { VisibilityOffOutlined, VisibilityOutlined } from "@material-ui/icons";
import Brand from '../../../app/EgretLayout/SharedCompoents/Brand';
import AppContext from "app/appContext";
import {appConst} from "app/appConst";

const styles = theme => ({
  wrapper: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
  },
  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -11,
    marginLeft: -12,
    color: "#212529"
  },
  title: {
    color: "#212529",
    fontFamily: "Inter",
    fontSize: "30px",
    fontWeight: "700",
    lineHeight: "36px",
    letterSpacing: "0em",
    textAlign: "center",
    marginBottom: "20px"
  },
  buttonLogin: {
    width: "fit-content",
    padding: "2.5%",
  },
  container: {
    width: "100%",
    height: "100vh",
    background: "#547fc0",
    backgroundImage: 'url("/assets/images/illustrations/bg-sign-in.jpg")',
    backgroundSize: "cover",
  },
  containerBv199: {
    width: "100%",
    height: "100vh",
    background: "#547fc0",
    backgroundImage: 'url("/assets/images/illustrations/bg-sign-in-199.png")',
    backgroundSize: "cover",
  },
  themeGreenContainer: {
    backgroundImage: "url(/assets/images/illustrations/bg-theme-green.jpg)",
    backgroundSize: "cover",
    width: "100%",
    height: "100vh",
  },
  themeBlueFade: {
    paddingTop: "5%",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "start",
    height: "100%",
    width: "100%",
    background: "rgba(26, 94, 131, 0.41)",
    boxShadow: "5px 4px 4px 0px rgba(0, 0, 0, 0.25)",
  },
  theme199Fade: {
    paddingTop: "5%",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "start",
    gap: 40,
    height: "100%",
    width: "100%",
    boxShadow: "5px 4px 4px 0px rgba(0, 0, 0, 0.25)",
  },
  themeGreenFade: {
    paddingTop: "5%",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "start",
    gap: 40,
    height: "100%",
    width: "100%",
    boxShadow: "inset 0 0 30px 5px rgba(0, 0, 0, .416)",
    filter: "drop-shadow(0 0 26px black)",
    inset: 0,
    position: "fixed",
    zIndex: 1,
  },
  img: {
    display: "flex",
    justifyContent: "center",
    marginBottom: "48px"
  },
  left: {
    background: "#212529",
    width: "100%",
    height: "100vh",
    backgroundImage: 'url("/assets/images/illustrations/foreground-login-1.png")',
    backgroundSize: "50%",
    backgroundRepeat: "no-repeat",
    backgroundPositionX: "center",
    backgroundPositionY: "60%",
  },
  background: {
    background: "#212529",
    height: "100vh",
    overflow: "auto",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  buttonSignUp: {
    fontWeight: "bold",
    textDecoration: "underline white solid 1px",
    color: "#212529"
  },
  text: {
    color: "#212529",
    marginTop: "20px"
  },
  label: {
    color: theme.primary.main,
    marginBottom: "5px",
    fontWeight: "bold",
  },
  inputLabel: {
    color: theme.primary.main,
    '&.MuiFormLabel-root.Mui-focused': {
      color: "#212529",
    },
  },
  input: {
    color: "#212529",
    '& label.Mui-focused': {
      color: '#1C20261A',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#1C20261A',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#1C20261A',
      },
      '&:hover fieldset': {
        borderColor: '#1C20261A',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'rgba(26,94,131,0.6)',
      },
    }
  },
  iframe:{
    width:"100%",
    height:"50%",
    border:"none",
    position: "relative",
    top: "30%"
  }

});

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    agreement: "",
    showPassword: false,
  };
  handleChange = event => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  handleFormSubmit = event => {
    this.props.loginWithEmailAndPassword({ ...this.state });
  };
  handleClickShowPassword = () => this.setState({ showPassword: !this.state.showPassword });
  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  renderLogoSwitch = () => (
    // Open Brand component file to replace logo and text
    <Brand>
    </Brand>
  );
  render() {
    const { t } = this.props;
    let { email, password, showPassword } = this.state;
    let { classes } = this.props;
    const { theme } = this.context;
    const isBV199 = theme.code === appConst.OBJECT_THEME.THEME_199.code;
    const containerClass = {
      [appConst.OBJECT_THEME.THEME_199.code]: classes.containerBv199,
      [appConst.OBJECT_THEME.THEME_BLUE.code]: classes.container,
      [appConst.OBJECT_THEME.THEME_GREEN.code]: classes.themeGreenContainer,
    };
    const fadeClass = {
      [appConst.OBJECT_THEME.THEME_199.code]: classes.theme199Fade,
      [appConst.OBJECT_THEME.THEME_BLUE.code]: classes.themeBlueFade,
      [appConst.OBJECT_THEME.THEME_GREEN.code]: classes.themeGreenFade,
    };

    return (
      <div className="login position-fixed inset-0">
        <Helmet>
          <title>{t('Dashboard.login')} | {t('web_site')} </title>
        </Helmet>
        <div className={classes.background}>
          <Grid container>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <div className={containerClass[theme?.code] || classes.container}>
                <div className={fadeClass[theme?.code] || classes.themeBlueFade}>
                  <div className="title">
                    <div className="unit-name">
                      <img src={"/assets/images/logos/utc.png"} alt="" />
                    </div>
                    <p className="app-name">Phần mềm quản lý tài sản và trang thiết bị y tế</p>
                  </div>
                  <Grid item lg={12} md={12} sm={12} xs={12} className="form-container">
                    <h4 className="text-center pb-12 text-primary">Đăng nhập</h4>
                    <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                      <div className={classes.label}>{t('username')}</div>
                      <TextValidator
                        className={`mb-24 w-100 ${classes.input} userName`}
                        variant="outlined"
                        onChange={this.handleChange}
                        type="text"
                        name="email"
                        value={email}
                        validators={["required"]}
                        errorMessages={t('general.required')}
                      />
                      <div className={classes.label}>{t('password')}</div>
                      <TextValidator
                        className={`mb-16 w-100 ${classes.input} password`}
                        // label={t('password')}
                        variant="outlined"
                        onChange={this.handleChange}
                        name="password"
                        value={password}
                        InputProps={{
                          type: showPassword ? 'text' : 'password',
                          endAdornment:
                            (< InputAdornment position="end" >
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={this.handleClickShowPassword}
                                onMouseDown={this.handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword ? <VisibilityOffOutlined color="#212529" /> : <VisibilityOutlined />}
                              </IconButton>
                            </InputAdornment>)
                        }}
                        validators={["required"]}
                        errorMessages={t('general.required')}
                      />
                      <FormControlLabel className="login-checkbox" control={<Checkbox defaultChecked />} label="Nhớ mật khẩu" />
                      <div className={classes.wrapper}>
                        <Button
                          variant="contained"
                          color="primary"
                          disabled={this.props.login.loading}
                          type="submit"
                          size="large"
                          className={classes.buttonLogin}
                        >
                          {t("sign_in.title")}
                        </Button>
                        {this.props.login.loading && (
                          <CircularProgress
                            size={24}
                            className={classes.buttonProgress}
                          />
                        )}
                      </div>
                    </ValidatorForm>
                  </Grid>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div >
    );
  }
}

SignIn.contextType = AppContext;
const mapStateToProps = state => ({
  loginWithEmailAndPassword: PropTypes.func.isRequired,
  login: state.login
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(
      mapStateToProps,
      { loginWithEmailAndPassword }
    )(SignIn)
  )
);
