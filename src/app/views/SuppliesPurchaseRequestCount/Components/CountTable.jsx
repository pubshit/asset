import {
  Grid,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";

function CountTable(props) {
  let {
    t,
    columns,
    itemList,
  } = props;

  return (
    <Grid item>
      <MaterialTable
        title={t("general.list")}
        data={itemList}
        columns={columns}
        localization={{
          body: {
            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
          },
          toolbar: {
            nRowsSelected: `${t("general.selects")}`,
          },
        }}
        options={{
          selection: false,
          actionsColumnIndex: -1,
          paging: false,
          search: false,
          rowStyle: (rowData) => ({
            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
          }),
          maxBodyHeight: "450px",
          minBodyHeight: itemList?.length > 0
            ? 0 : 250,
          headerStyle: {
            backgroundColor: "#358600",
            color: "#fff",
          },
          padding: "dense",
          toolbar: false,
        }}
        components={{
          Toolbar: (props) => <MTableToolbar {...props} />,
        }}
        onSelectionChange={(rows) => {
          this.data = rows;
        }}
      />
    </Grid>
  );
}

export default CountTable;
