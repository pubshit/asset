import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const SuppliesPurchaseRequestCountTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./SuppliesPurchaseRequestCountTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(SuppliesPurchaseRequestCountTable);

const SuppliesPurchaseRequestCountRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/supplies_purchase_request_count",
    exact: true,
    component: ViewComponent
  }
];

export default SuppliesPurchaseRequestCountRoutes;