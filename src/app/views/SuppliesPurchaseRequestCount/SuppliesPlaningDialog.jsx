import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Dialog,
  DialogActions,
  Grid,
  Icon,
  IconButton,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import { Autocomplete } from "@material-ui/lab";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import { appConst, formatDate } from "app/appConst";
import history from "history.js";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import moment from "moment";
import React from "react";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { getListManagementDepartment, getShoppingForm } from "../Asset/AssetService";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {
  createPurchasePlaning,
  updatePurchasePlaningById,
} from "../PurchasePlaning/PurchasePlaningService";
import { searchByPage as SupplierSearchByPage } from '../Supplier/SupplierService';
import { getListUserByDepartmentId, getByPlanId, } from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListProductByIds } from "./PurchaseRequestCountService";
import { NumberFormatCustom, convertNumberPrice, convertNumberPriceRoundUp, formatDateDto, formatTimestampToDate } from "app/appFunction";
import AppContext from "app/appContext";
import { searchByPage as productTypeSearchByPage } from "../ProductType/ProductTypeService";
import ValidatedDatePicker from "../Component/ValidatePicker/ValidatePicker";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const helperTextRequired = {
  color: "#f44336",
  margin: "0",
  fontSize: "0.75rem",
  marginTop: "3px",
  textAlign: "left",
  fontWeight: "400",
  lineHeight: "1.66",
  letterSpacing: "0.03333em",
};
class SuppliesPlaningDialog extends React.Component {
  state = {
    rowsPerPage: 1000,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    keyword: "",
    Notification: "",
    planDate: Date.now(),
    approvedDate: null,
    managementDepartment: null,
    status: null,
    note: null,
    name: null,
    totalEstimatedCost: null,
    planer: null,
    shouldOpenProductPopup: false,
    product: null,
    department: null,
    listProducts: [],
    planingDepartment: null,
    listProduct: [],
    listSupplier: [],
    supplier: null,
    type: 1,
    formToDate: moment().endOf("month"),
    formDate: moment().startOf("month"),
    dupApplyDate: false,
    checkDate: false,
    listUnit: [],
    listHTMS: [],
    searchParamUserByRequestDepartment: { departmentId: null },
    unit: null,
    htms: null,
    statusObj: null,
    listStatus: [],
    HTMSFilter: null,
    supplierFilter: null,
  };
  PURCHASE = {
    name: "Đã duyệt",
    indexOrder: 2, //Đã duyệt
  };

  setListProductType = (value) => {
    let listProductTypeNoVTHH = value?.filter(item => item.code !== appConst.productTypeCode.VTHH)
    this.setState({
      listProductType: listProductTypeNoVTHH || []
    })
  }
  getListSupplier = async () => {
    let searchObjectSupplier = { pageIndex: 1, pageSize: 1000000 };
    try {
      let res = await SupplierSearchByPage(searchObjectSupplier);
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({ listSupplier: res?.data?.content })
      }
    } catch (error) { console.error(error) }
  }
  getListHTMS = async () => {
    let searchObjectUnit = { pageIndex: 1, pageSize: 1000000 };
    try {
      let res = await getShoppingForm(searchObjectUnit);
      if (res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({ listHTMS: res?.data?.data?.content })
      }
    } catch (error) { console.error(error) }
  }
  handleChangeFilter = (value, name) => {
      this.setState({ [name]: value }, () => {
        this.updateProductPlan();
      });
  };

  updateProductPlan = async () => {
    let {t} = this.props
    let {setPageLoading} = this.context
    setPageLoading(true)
    let searchObject = {
      purchasePlanId: this.props?.item?.id,
      shoppingFormId: this.state?.HTMSFilter?.id,
      supplierId: this.state.supplierFilter?.id,
      productType: this.state.productType?.id
    }
    try {
      let res = await getByPlanId(searchObject)
      if(res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listProducts: res?.data?.data?.content?.length > 0 ? [...res?.data?.data?.content] : []
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
    finally {
      setPageLoading(false)
    }
  }

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name };

    this.setState(
      { managementDepartment: department, planingDepartment: department },
      function () {
        this.search();
        this.handleSelectManagementDepartmentPopupClose();
      }
    );
    if (Object.keys(item).length === 0) {
      this.setState({ managementDepartment: null, planingDepartment: null });
    }
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectMultiProduct = () => {
    let { listProducts } = this.state;
    let { listProduct } = this.state;
    if (!listProducts) {
      listProducts = [];
    }
    if (!listProduct) {
      listProduct = [];
    }
    listProduct.forEach((element) => {
      let p = {};
      p.quantity = element.quantity;
      p.product = element.product;
      p.purchaseRequestId = element.purchaseRequestId
      listProducts.push(p);
    });
    this.setState({ listProducts: listProducts });
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.type = this.state.type;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.fromDate = this.state.fromDate;
      searchObject.managementDepartmentId = this.state.planingDepartment
        ? this.state.planingDepartment.id
        : "";
    });
  }

  handleProductPopupClose = () => {
    this.setState({ shouldOpenProductPopup: false });
  };

  selectPurchaseStatus = (item) => {
    this.setState({ status: item, statusObj: item })
  };
  selectSupplier = (item) => {
    this.setState({ supplier: item }, function () { });
  };
  handleSelectDepartment = (department) => {
    this.setState({ department, searchParamUserByRequestDepartment: { departmentId: department?.id } });

  };
  selectPurchaseDepartment = (department) => {
    this.setState({ department });
    this.setState({ department, searchParamUserByRequestDepartment: { departmentId: department?.id } });
  };

  handleDateChange = (date, name) => {
    this.setState(
      {
        [name]: date,
      },
      () => { }
    );
  };

  handleContractDateChange = (rowData, date) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.contractDate = formatDateDto(date)
        }
      });
      this.setState({ listProducts });
    }
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { planDate } = this.state;
    let { t } = this.props

    let payload = {
      approvedDate: this.state.approvedDate ? moment(this.state.approvedDate).format(formatDate) : null,
      name: this.state.name,
      note: this.state.note,
      planDate: moment(this.state.planDate).format(formatDate),
      planDepartmentId: this.state.planingDepartment?.id,
      planDepartmentName: this.state.planingDepartment?.name,
      planPersonId: this.state.planer?.personId,
      planPersonName: this.state.planer?.personDisplayName,
      purchasePreparePlanIds: id ? this.state?.purchasePreparePlanIds : this.props?.purchaseRequests?.map((item) => { return item?.id }),
      purchaseProducts: this.state?.listProducts,
      status: this.state.statusObj.indexOrder,
      totalEstimatedCost: this.state.totalEstimatedCost,
      type: appConst.TYPE_PURCHASE.VT
    }
    if (
      !planDate ||
      planDate === null ||
      (typeof planDate === "object" && !planDate.getTime())
    ) {
      this.setState({ dupApplyDate: true });
      return;
    }
    if (this.state.listProducts == [] || this.state.listProducts.length === 0) {
      toast.warning("Không có sản phẩm");
      toast.clearWaitingQueue();
      return;
    }
    if (id) {
      updatePurchasePlaningById(
        {
          ...payload,
        },
        id
      ).then((res) => {
        if(res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.success"))
          this.props.handleOKEditClose();
        }
        else {
          toast.warning(t(res?.data?.message))
        }
      }).catch(() =>
        toast.error(t("general.error"))
      );
    } else {
      createPurchasePlaning(payload).then((res) => {
        if (res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.success"))
          history.push(ConstantList.ROOT_PATH + "list/supplies_purchase_planing");
        } else {
          toast.warning(t(res?.data?.message))
        }
      }).catch(() =>
        toast.error(t("general.error"))
      );
    }
  };

  handleGetListProducts = async () => {
    let { t } = this.props
    let { purchaseRequests } = this.props
    let searchObject = {
      purchasePreparePlanIds: purchaseRequests?.map((item) => { return item?.id }).join(),
    }

    try {
      if (purchaseRequests) {
        const res = await getListProductByIds(searchObject)
        if (res?.data?.code === appConst?.CODE?.SUCCESS) {
          this.setState({ listProducts: res?.data?.data?.content})
        } else {
          toast.error(t("toastr.error"))
        }
      }

    } catch (error) {
      toast.error(t("toastr.error"))
    }
  }

  componentWillMount() {
    let {
      item,
      fromToDate,
      fromDate,
      planingDepartment
    } = this.props;

    let totalEstimatedCost = item?.purchaseProducts?.reduce((acc, cur) => acc + cur?.estimatedUnitPrice * cur?.approvedQuantity, 0)
    this.setState({ listStatus: appConst.listStatusPurchasePlaning.slice(0, appConst.listStatusPurchasePlaning.length - 1) })

    if (item?.id) {
      this.setState({
        listProducts: item?.purchaseProducts,
        department: {
          name: item?.planDepartmentName,
          id: item?.planDepartmentId
        },
        planer: {
          personDisplayName: item?.planPersonName,
          personId: item?.planPersonId
        },
        statusObj: appConst.listStatusPurchasePlaning?.find((status) => status.indexOrder === item?.status),
        approvedDate: item?.approvedDate ? moment(item?.approvedDate).format(formatDate) : null,
        totalEstimatedCost: Number(totalEstimatedCost)
      })
    }
    else {
      this.setState({totalEstimatedCost: Number(totalEstimatedCost)})
      this.handleGetListProducts()
    }
    this.setState({ fromToDate, fromDate, planingDepartment }, function () { });
    this.setState(item);

    // this.getListStockKeepingUnit()
    this.getListSupplier()
    this.getListHTMS()
  }
  handleChangeTotalEstimatedCosts() {
   let {listProducts} = this.state
    let totalEstimatedCost = listProducts?.reduce((acc, cur) => acc + cur?.estimatedUnitPrice * cur?.approvedQuantity, 0)
    this.setState({ totalEstimatedCost: Number(totalEstimatedCost) })
  }

  componentDidMount() {
    this.search();
    ValidatorForm.addValidationRule("isMaxDate", (value) => {
      if (value.getTime() > Date.now()) {
        return false;
      }
      return true;
    });
  }
  handleQuantityChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd, index) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.approvedQuantity = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  // handleStockKeepingUnitChange = (rowData, value) => {
  //   let { listProducts } = this.state;
  //   if (listProducts != null && listProducts.length > 0) {
  //     listProducts.forEach((vd) => {
  //       if (
  //         vd != null &&
  //         rowData != null &&
  //         vd?.productId === rowData?.productId
  //       ) {
  //         vd.skuName = value.name;
  //         vd.skuId = value.id;
  //       }
  //     });
  //     this.setState({ listProducts });
  //   }
  // };
  handlePriceChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.estimatedUnitPrice = Number(event.target.value);
          this.handleChangeTotalEstimatedCosts()
        }
      });
      this.setState({ listProducts });
    }
  };
  handleCostsChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.estimatedCosts = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleHTMSChange = (rowData, value) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.shoppingFormId = value?.id;
          vd.shoppingFormName = value?.name;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleSupplierChange = (rowData, value) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.supplierName = value.name;
          vd.supplierId = value.id;
        }
      });

      this.setState({ listProducts });
    }
  };
  handleNoteChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd?.productId === rowData?.productId
        ) {
          vd.noteProduct = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };


  // getListStockKeepingUnit = async () => {
  //   let searchObjectUnit = { pageIndex: 1, pageSize: 1000000 };
  //   try {
  //     let res = await getStockKeepingUnit(searchObjectUnit);
  //     if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
  //       this.setState({ listUnit: res?.data?.content })
  //     }
  //   } catch (error) { console.error(error) }
  // }

  getListSupplier = async () => {
    let searchObjectSupplier = { pageIndex: 1, pageSize: 1000000 };
    try {
      let res = await SupplierSearchByPage(searchObjectSupplier);
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({ listSupplier: res?.data?.content })
      }
    } catch (error) { console.error(error) }
  }
  handleSelectRequestPerson = (item) => {
    this.setState({
      planer: item ? item : null,
    });
  };
  render() {
    const {
      t,
      i18n,
      open,
      isView
    } = this.props;

    let {
      rowsPerPage,
      page,
      department,
      note,
      name,
      totalEstimatedCost,
      planer,
      listProducts,
      listSupplier,

      planDate,
      approvedDate,
      shouldOpenNotificationPopup,
      listHTMS,
      searchParamUserByRequestDepartment,
      statusObj,
      listStatus,
      HTMSFilter,
      productType,
      supplierFilter
    } = this.state;

    let checkStatus = [appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder, appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder].includes(statusObj?.indexOrder)
    let searchObject = { keyword: "", pageIndex: 0, pageSize: 10000 };
    let columns = [
      {
        title: t("Product.STT"),
        field: "code",
        maxWidth: 50,
        align: "center",
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "productName",
        minWidth: 150,
        align: "left",
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "center",
        minWidth: 90,
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "center",
        minWidth: 90,
      },
      {
        title: t("SparePart.unit"),
        field: "skuName",
        align: "left",
        minWidth: 70,
      },
       ...(statusObj?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder ? [
        {
        title: t("purchaseRequestCount.contractDate"),
        field: "contractDate",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        minWidth: 200,
        render: (rowData) => (
          isView ? formatTimestampToDate(rowData?.contractDate)
            : 
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"contractDate"}
                    value={rowData?.contractDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleContractDateChange( rowData, date)
                    }
                    minDate={this.state.approvedDate || new Date()}
                    minDateMessage={t(
                      "purchasePlaning.minContractDateMessage"
                    )}
                    disabled={isView}
                  />
                </MuiPickersUtilsProvider>
        )
      } 
      ] : []),
      {
        title: t("Product.estimatedUnitPrice"),
        field: "estimatedUnitPrice",
        align: "right",
        minWidth: 200,
        render: (rowData) => (
          isView ? convertNumberPrice(rowData?.estimatedUnitPrice || 0)
            : <TextValidator

              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  className: "text-align-right",
                },
                readOnly: isView
              }}
              type="text"
              onChange={(event) => this.handlePriceChange(rowData, event)}
              id="formatted-numberformat-carryingAmount"
              name="estimatedUnitPrice"
              value={convertNumberPriceRoundUp(Number(rowData?.estimatedUnitPrice || 0))}
            />
        ),
      },
      {
        title: t("Product.estimatedCosts"),
        field: "estimatedCosts",
        align: "right",
        minWidth: 200,
        render: (rowData) => (
          convertNumberPriceRoundUp(rowData.estimatedUnitPrice * rowData.approvedQuantity)
        ),
      },
      {
        title: t('Product.formShopping'),
        field: "HTMS",
        align: "left",
        minWidth: 200,
        render: (rowData) => (
          isView ? rowData?.shoppingFormName : <Autocomplete
            id="combo-box"
            fullWidth
            size="small"
            name="HTMS"
            options={listHTMS}
            onChange={(e, newValue) =>
              this.handleHTMSChange(
                rowData, newValue,
              )
            }
            value={{ name: rowData?.shoppingFormName, id: rowData?.shoppingFormId }}
            getOptionLabel={(option) => {
              return option?.name || "";
            }}
            noOptionsText={t("general.noOption")}
            renderInput={(params) => (
              <TextValidator
                {...params}
                variant="standard"
                onChange={() => { }}
                value={rowData?.shoppingFormName || ""}
                validators={statusObj?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? ["required"] : []}
                errorMessages={statusObj?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [t("general.required")] : []}
              />
            )}
          />
        )
      },
      ...(statusObj?.indexOrder !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
      {
        title: t("Product.supplier"),
        field: "supplierName",
        align: "left",
        minWidth: 200,
        render: (rowData) => (
          isView ? rowData?.supplierName : <Autocomplete
            id="combo-box"
            fullWidth
            size="small"
            name="supplier"
            options={listSupplier}
            onChange={(e, newValue) =>
              this.handleSupplierChange(
                rowData, newValue,
              )
            }
            value={{ name: rowData?.supplierName, id: rowData?.supplierId }}
            getOptionLabel={(option) => {
              return option?.name || "";
            }}
            noOptionsText={t("general.noOption")}
            renderInput={(params) => (
              <TextValidator
                {...params}
                variant="standard"
                onChange={() => { }}
                value={rowData?.supplierName || ""}
                validators={statusObj?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder ? ["required"] : []}
                errorMessages={statusObj?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder ? [t("general.required")] : []}
              />
            )}
          />
        )
      }, 
      ] : []),
      ...(statusObj?.indexOrder !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        minWidth: 150,
        render: (rowData) => {
          const statusIndex = rowData?.status;
          if (statusIndex === appConst.statusProduct.DA_MUA) {
            return (
              <span
                className="status status-success"
              >
                {t("purchasePlaning.bought")}
              </span>
            )
          } else {
            return (
              <span
                className="status status-warning"
              >
                {t("purchasePlaning.notBought")}
              </span>
            )
          }
        }
      }
      ] : []),
    ];

    let dupApplyDate;
    if (this.state.dupApplyDate === true) {
      dupApplyDate = (
        <span style={helperTextRequired}> {t("general.required")} </span>
      );
    } else {
      dupApplyDate = "";
    }
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        scroll="paper"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <ValidatorForm
          ref="formPlan"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            {t("purchasePlaning.title")}
          </DialogTitle>
          <DialogContent style={{ minHeight: "400px" }}>
            <Grid container spacing={1}>
              <Grid item md={4} sm={12} xs={12}>
                  <ValidatedDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchasePlaning.planingDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"planDate"}
                    value={planDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleDateChange(date, "planDate")
                    }
                    disableFuture
                    maxDateMessage={t(
                      "purchasePlaning.maxDatePlanMessage"
                    )}
                    disabled={isView}
                  />
                  {dupApplyDate}
              </Grid>
              {checkStatus && <Grid item md={4} sm={12} xs={12}>
                  <ValidatedDatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog mt-2"
                    label={
                       statusObj?.indexOrder === appConst.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? 
                        <span className="w-100">
                          <span className="colorRed">*</span>
                          {t("purchasePlaning.approvalDate")}
                        </span> : 
                        <span className="w-100">
                          <span className="colorRed">*</span>
                          {t("purchasePlaning.quoteApprovedDate")}
                        </span>
                    }
                    name={'approvedDate'}
                    inputVariant="standard"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={approvedDate}
                    onChange={date => this.handleDateChange(date, "approvedDate")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    disabled={isView || !checkStatus}
                    validators={checkStatus ? ['required'] : []}
                    errorMessages={t('general.required')}
                    minDate={planDate}
                    clearable
                    disableFuture
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                    minDateMessage={t(
                      "purchasePlaning.minDateMessage"
                    )}
                    maxDateMessage={t(
                      "purchasePlaning.maxDateMessage"
                    )}
                  />
              </Grid>}
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span>
                    <span className="colorRed">* </span>
                    <span>{t("maintainRequest.name")}</span>
                  </span>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  inputProps={{
                    readOnly: isView
                  }}
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                {isView ?
                  <TextValidator
                    className="w-100 "
                    label={<span>
                      <span className="colorRed">* </span>
                      <span>{t("maintainRequest.status")}</span>
                    </span>}
                    type="text"
                    name="status"
                    value={this.state.statusObj?.name}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  /> :
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        <span>{t("maintainRequest.status")}</span>
                      </span>
                    }
                    listData={listStatus}
                    // setListData={props.handleSetDataHandoverPerson}
                    // defaultValue={selectLiquidationStatus ? selectLiquidationStatus : null}
                    displayLable={"name"}
                    value={statusObj ? statusObj : null}
                    onSelect={this.selectPurchaseStatus}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                    noOptionsText={t("general.noOption")}
                  />}
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                {isView ?
                  <TextValidator
                    className="w-100 "
                    label={<span>
                      <span className="colorRed">* </span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>}
                    type="text"
                    name="planingDepartmentName"
                    value={this.state.planDepartmentName}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  /> :
                  <AsynchronousAutocomplete
                    label={
                      <span>
                        <span className="colorRed">* </span>{" "}
                        <span>{t("purchasePlaning.planingDepartment")}</span>
                      </span>
                    }
                    searchFunction={getListManagementDepartment}
                    searchObject={searchObject}
                    displayLable={"name"}
                    typeReturnFunction="list"
                    value={department ?? null}
                    defaultValue={department ?? null}
                    listData={department}
                    setListData={this.selectPurchaseDepartment}
                    onSelect={this.handleSelectDepartment}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    noOptionsText={t("general.noOption")}
                  />}
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                {isView ?
                  <TextValidator
                    className="w-100 "
                    label={<span>
                      <span className="colorRed">* </span>
                      <span>{t("purchaseRequest.requestPerson")}</span>
                    </span>}
                    type="text"
                    name="planPersonName"
                    value={this.state.planPersonName}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  /> :
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchaseRequest.requestPerson")}
                      </span>
                    }
                    disabled={!department}
                    searchFunction={getListUserByDepartmentId}
                    searchObject={searchParamUserByRequestDepartment}
                    defaultValue={planer ?? ''}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    value={planer ?? ''}
                    onSelect={requestPerson => this?.handleSelectRequestPerson(requestPerson)}
                    noOptionsText={t("general.noOption")}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />}
              </Grid>
              <Grid item md={4} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.totalEstimatedCosts")}
                    </span>
                  }
                  // onChange={this.handleChange}
                  type="text"
                  inputProps={{
                    readOnly: true
                  }}
                  name="totalEstimatedCost"
                  value={convertNumberPriceRoundUp(Number(totalEstimatedCost))}
                  validators={["minNumber:1", "required"]}
                  errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
                />
              </Grid>
              <Grid item md={checkStatus ? 8 : 12} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("maintainRequest.note")}
                  onChange={this.handleChange}
                  type="text"
                  name="note"
                  inputProps={{
                    readOnly: isView
                  }}
                  value={note}
                />
              </Grid>
            </Grid>
            <Grid className="mb-16" container spacing={1}></Grid>
            <Grid container spacing={2} justifyContent="flex-end">
              {
                    (this.props?.item?.status === appConst.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder || 
                    this.props?.item?.status === appConst.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder) &&
                    <Grid item md={3} sm={4} xs={4}>
                        <AsynchronousAutocompleteSub
                          label={
                            <span>
                              <span> {t("Product.formShopping")}</span>
                            </span>
                          }
                          listData={listHTMS || []}
                          setListData={() => {}}
                          searchFunction={() => {}}
                          searchObject={{}}
                          defaultValue={HTMSFilter}
                          displayLable={"name"}
                          value={HTMSFilter}
                          onSelect={(newValue) => this.handleChangeFilter(newValue, "HTMSFilter")}
                        />
                    </Grid>
                }
                {
                    (this.props?.item?.status === appConst.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder || 
                    this.props?.item?.status === appConst.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder)  &&
                    <Grid item md={3} sm={4} xs={4}>
                        <AsynchronousAutocompleteSub
                          label={
                            <span>
                              <span> {t("Product.productType")}</span>
                            </span>
                          }
                          listData={this.state.listProductType}
                          setListData={this.setListProductType}
                          searchFunction={productTypeSearchByPage}
                          searchObject={searchObject}
                          defaultValue={productType}
                          displayLable={"name"}
                          value={productType}
                          onSelect={(newValue) => this.handleChangeFilter(newValue, "productType")}
                        />
                    </Grid>
                }
                {
                    (this.props?.item?.status === appConst.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder)  &&
                    <Grid item md={3} sm={4} xs={4}>
                      <AsynchronousAutocompleteSub
                          label={
                            <span>
                              <span> {t("Product.supplier")}</span>
                            </span>
                          }
                          listData={listSupplier}
                          setListData={() => {}}
                          searchFunction={() => {}}
                          searchObject={{}}
                          defaultValue={supplierFilter}
                          displayLable={"name"}
                          value={supplierFilter}
                          onSelect={(newValue) => this.handleChangeFilter(newValue, "supplierFilter")}
                        />
                    </Grid>
                }
              <Grid item xs={12}>
                <MaterialTable
                  data={listProducts ? listProducts : []}
                  columns={columns}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    padding: "dense",
                    sorting: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "#EEE" : "#FFF",
                    }),
                    maxBodyHeight: "280px",
                    minBodyHeight: "280px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ witdth: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!isView && <Button variant="contained" color="primary" type="submit">
                {t("general.save")}
              </Button>}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
SuppliesPlaningDialog.contextType = AppContext;
export default SuppliesPlaningDialog;
