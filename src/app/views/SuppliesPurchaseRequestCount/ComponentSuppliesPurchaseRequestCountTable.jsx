import {
  Button,
  Grid,
  TablePagination,
} from "@material-ui/core";
import React, { useContext } from "react";
import { ConfirmationDialog } from "egret";
import MaterialTable, { MTableToolbar } from "material-table";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import PurchaseRequestCountDialog from "./Dialog/PurchaseRequestCountDialog";
import { ValidatorForm } from "react-material-ui-form-validator";
import { appConst } from "app/appConst";
import viLocale from "date-fns/locale/vi";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import SuppliesPlaningDialog from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningDialog";
import { purchaseRequestCountPrintData } from "../FormCustom/PurchaseRequestCount";
import {defaultPaginationProps} from "../../appFunction";

function ComponentSuppliesPurchaseRequestCountTable(props) {
  let {
    t,
    i18n,
    columns,
    getRowData,
    handleSetData,
    setRowsPerPage,
    handleChangePage,
    handleAddPlanning,
    handleDialogClose,
    handleOKEditClose,
    handleConfirmationResponse,
  } = props;
  let {
    item,
    page,
    isView,
    isPrint,
    toDate,
    itemList,
    fromDate,
    rowsPerPage,
    totalElements,
    purchaseRequests,
    managementDepartment,
    shouldOpenEditorDialog,
    shouldOpenPlaningEditorDialog,
    shouldOpenConfirmationDialog,
    purchasePreparePlan
  } = props?.item;

  const { SUPPLIES_PURCHASING } = LIST_PRINT_FORM_BY_ORG.SHOPPING_MANAGEMENT;
  const { currentOrg } = useContext(AppContext);

  let dataView = purchaseRequestCountPrintData(item)
  return (
    <>
      <ValidatorForm onSubmit={() => {
      }}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                label={t("purchase_request_count.formDate")}
                format="dd/MM/yyyy"
                name={"fromDate"}
                value={fromDate}
                maxDate={toDate || undefined}
                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                minDateMessage={t("general.minDateMessage")}
                invalidDateMessage={t("general.invalidDateFormat")}
                onChange={(date) => handleSetData(date, "fromDate")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                clearable
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                label={t("purchase_request_count.formToDate")}
                format="dd/MM/yyyy"
                name={"toDate"}
                value={toDate}
                minDate={fromDate || undefined}
                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                maxDateMessage={t("general.maxDateDefault")}
                invalidDateMessage={t("general.invalidDateFormat")}
                onChange={(date) => handleSetData(date, "toDate")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                clearable
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item xs={12}>

            {isPrint && (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={dataView || item}
                title={t("Phiếu tổng hợp nhu cầu đầu tư trang thiết bị")}
                urls={[
                  ...SUPPLIES_PURCHASING.PURCHASE_REQUEST_COUNT.GENERAL,
                  ...(SUPPLIES_PURCHASING.PURCHASE_REQUEST_COUNT[currentOrg?.printCode] || []),
                ]}
              />
            )}

            <div>
              {shouldOpenEditorDialog && (
                <PurchaseRequestCountDialog
                  t={t}
                  i18n={i18n}
                  handleClose={handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={handleOKEditClose}
                  item={item}
                  isView={isView}
                  handleAddPlanning={handleAddPlanning}
                  type={appConst.TYPE_PURCHASE.VT}
                />
              )}
              {shouldOpenPlaningEditorDialog && (
                <SuppliesPlaningDialog
                  t={t}
                  i18n={i18n}
                  handleClose={handleDialogClose}
                  open={shouldOpenPlaningEditorDialog}
                  handleOKEditClose={handleOKEditClose}
                  item={item}
                  fromDate={fromDate}
                  toDate={toDate}
                  planingDepartment={managementDepartment}
                  purchaseRequests={purchaseRequests}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={handleDialogClose}
                  onYesClick={handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                toolbar: false,
                sorting: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    purchasePreparePlan?.id === rowData.id
                      ? "#ccc" :
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: "dense",
              }}
              onRowClick={(e, rowData) => {
                getRowData(rowData)
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                // this.data = rows;
              }}
            />
            <TablePagination
							{...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
							onPageChange={handleChangePage}
							onRowsPerPageChange={setRowsPerPage}
            />
          </Grid>
        </Grid>
      </ValidatorForm>
    </>
  )
}

export default ComponentSuppliesPurchaseRequestCountTable;
