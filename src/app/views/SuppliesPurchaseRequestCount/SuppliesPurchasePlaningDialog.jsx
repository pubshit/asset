import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Dialog,
  DialogActions,
  Grid,
  Icon,
  IconButton
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import {
  DateTimePicker,
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import history from "history.js";
import MaterialTable, {
  MTableToolbar
} from "material-table";
import moment from "moment";
import React from "react";
import Draggable from "react-draggable";
import { useTranslation } from "react-i18next";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import SelectManagementDepartmentPopup from "../Component/Department/SelectManagementDepartmentPopup";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import {
  createPurchasePlaning,
  updatePurchasePlaningById,
} from "../PurchasePlaning/PurchasePlaningService";
import { searchByPage as PurchaseRequestSearchByPage } from "../PurchaseRequestStauts/PurchaseRequestStautsService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { searchByPage } from "./PurchaseRequestCountService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}
const helperTextRequired = {
  color: "#f44336",
  margin: "0",
  fontSize: "0.75rem",
  marginTop: "3px",
  textAlign: "left",
  fontWeight: "400",
  lineHeight: "1.66",
  letterSpacing: "0.03333em",
};
class PurchasePlaningDialog extends React.Component {
  state = {
    rowsPerPage: 1000,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenNotificationPopup: false,
    selectedItem: {},
    keyword: "",
    Notification: "",
    planingDate: Date.now(),
    approvedDate: null,
    managementDepartment: null,
    shouldOpenSelectManagementDepartmentPopup: false,
    status: null,
    note: null,
    shouldOpenProductPopup: false,
    product: null,
    listProducts: [],
    planingDepartment: null,
    listProduct: [],
    type: 1,
    formToDate: moment().endOf("month"),
    formDate: moment().startOf("month"),
    dupApplyDate: false,
    checkDate: false,
  };
  PURCHASE = {
    name: "Đã duyệt",
    indexOrder: 2, //Đã duyệt
  };
  
  openSelectManagementDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectManagementDepartmentPopup: true,
    });
  };

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name };
    this.setState(
      { managementDepartment: department, planingDepartment: department },
      function () {
        this.search();
        this.handleSelectManagementDepartmentPopupClose();
      }
    );
    if (Object.keys(item).length === 0) {
      this.setState({ managementDepartment: null, planingDepartment: null });
    }
  };

  handleSelectManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectManagementDepartmentPopup: false,
    });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectMultiProduct = () => {
    let { listProducts } = this.state;
    let { listProduct } = this.state;
    if (!listProducts) {
      listProducts = [];
    }
    if (!listProduct) {
      listProduct = [];
    }
    listProduct.forEach((element) => {
      let p = {};
      p.quantity = element.quantity;
      p.product = element.product;
      p.purchaseRequestId=p.purchaseRequestId
      listProducts.push(p);
    });
    this.setState({ listProducts: listProducts });
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.type = 1;
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromToDate = this.state.fromToDate;
      searchObject.fromDate = this.state.fromDate;
      searchObject.managementDepartmentId = this.state.planingDepartment
        ? this.state.planingDepartment.id
        : "";
      searchByPage(searchObject).then(({ data }) => {
        let listProduct = [...data.content];
        let { listProducts } = this.state;
        listProducts = [];
        listProduct.forEach((element) => {
          let p = {};
          p.quantity = element.quantity;
          p.product = element.product;
          p.purchaseRequestId = element.purchaseRequestId
          listProducts.push(p);
        });
        listProducts.sort((a, b) =>
          a.product.name.localeCompare(b.product.name)
        );

        this.setState(
          {
            listProducts: listProducts,
            totalElements: data.totalElements,
          },
        );
      });
    });
  }

  handleProductPopupClose = () => {
    this.setState({ shouldOpenProductPopup: false });
  };

  selectPurchaseStatus = (item) => {
    this.setState({ status: item }, function () { });
  };

  handleDateChange = (date, name) => {
    this.setState(
      {
        [name]: date,
      },
      () => { }
    );
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code, planingDate, approvedDate } = this.state;
    if (
      !planingDate ||
      planingDate === null ||
      (typeof planingDate === "object" && !planingDate.getTime())
    ) {
      this.setState({ dupApplyDate: true });
      return;
    }
    if (this.state.listProducts == [] || this.state.listProducts.length === 0) {
      toast.warning("Không có sản phẩm");
      toast.clearWaitingQueue();
      return;
    }
    if (id) {
      updatePurchasePlaningById(
        {
          ...this.state,
        },
        id
      ).then(() => {
        this.props.handleOKEditClose();
      });
    } else {
      createPurchasePlaning({
        ...this.state,
      }).then(() => {
        history.push(ConstantList.ROOT_PATH + "list/purchase_planing");
      });
    }
  };
  componentWillMount() {
    let {
      open,
      handleClose,
      item,
      handleDialogClose,
      fromToDate,
      fromDate,
      planingDepartment,
    } = this.props;
    this.setState({ fromToDate, fromDate, planingDepartment }, function () { });
    this.setState(item);
  }

  componentDidMount() {
    this.search();
    ValidatorForm.addValidationRule("isMaxDate", (value) => {
      if (value.getTime() > Date.now()) {
        return false;
      }
      return true;
    });
  }
  handleQuantityChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id == rowData.product.id
        ) {
          vd.quantity = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };
  handleNoteChange = (rowData, event) => {
    let { listProducts } = this.state;
    if (listProducts != null && listProducts.length > 0) {
      listProducts.forEach((vd) => {
        if (
          vd != null &&
          rowData != null &&
          vd.product.id == rowData.product.id
        ) {
          vd.noteProduct = event.target.value;
        }
      });
      this.setState({ listProducts });
    }
  };

  render() {
    const {
      t,
      i18n,
      open,
    } = this.props;
    let {
      status,
      note,
      listProducts,
      planingDepartment,
      planingDate,
      shouldOpenNotificationPopup,
      approvedDate,
      shouldOpenSelectManagementDepartmentPopup,
    } = this.state;
    let searchObject = { keyword: "", pageIndex: 0, pageSize: 10000 };
    let now = Date.now();

    let columns = [
      {
        title: t("Product.code"),
        field: "product.code",
        width: "140px",
      },
      {
        title: t("Product.name"),
        field: "product.name",
        align: "left",
        width: "150",
        headerStyle: {
          minWidth: "300px",
        },
      },
      {
        title: t("Product.quantity"),
        field: "quantity",
        align: "left",
        width: "100px",
        render: (rowData) => (
          <TextValidator
            className="w-10"
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={!!this.state.productName}
            name="quantity"
            validators={["minNumber:1", "required"]}
            errorMessages={[t("Số lượng lớn hơn 0"), t("general.required")]}
            value={rowData.quantity}
          />
        ),
      },
      {
        title: t("maintainRequest.note"),
        field: "noteProduct",
        align: "left",
        width: "220px",
        render: (rowData) => (
          <TextValidator
            className="w-10"
            onChange={(event) => this.handleNoteChange(rowData, event)}
            id="formatted-numberformat-carryingAmount"
            disabled={!!this.state.productName}
            name="noteProduct"
            value={rowData.noteProduct}
          />
        ),
      },

      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "100px",
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 1) {
                listProducts.find((item, index) => item.product.id === rowData.product.id ? listProducts.splice(index, 1) : "")
                this.setState({ listProducts });
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    let dupApplyDate;
    if (this.state.dupApplyDate === true) {
      dupApplyDate = (
        <span style={helperTextRequired}> {t("general.required")} </span>
      );
    } else {
      dupApplyDate = "";
    }

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"md"}
        scroll="paper"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            {t("purchase_request_count.saveUpdate")}
          </DialogTitle>
          <DialogContent style={{ minHeight: "400px" }}>
            <Grid className="" container spacing={1}>
              <Grid item md={3} sm={12} xs={12} className="">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t("purchasePlaning.planingDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={"planingDate"}
                    value={planingDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    onChange={(date) =>
                      this.handleDateChange(date, "planingDate")
                    }
                  // emptyLabel={[t("general.required")]}
                  />
                  {dupApplyDate}
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid className="" item md={3} sm={12} xs={12}>
                <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed">* </span>{" "}
                      <span> {t("maintainRequest.status")}</span>
                    </span>
                  }
                  searchFunction={PurchaseRequestSearchByPage}
                  searchObject={searchObject}
                  defaultValue={status}
                  displayLable={"name"}
                  value={status}
                  onSelect={this.selectPurchaseStatus}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>

              <Grid item md={6} sm={12} xs={12} className="">
                <Button
                  style={{ float: "right" }}
                  size="small"
                  className=" mt-10"
                  variant="contained"
                  color="primary"
                  onClick={this.openSelectManagementDepartmentPopup}
                >
                  {t("general.select")}
                </Button>
                <TextValidator
                  // InputLabelProps={{ shrink: true }}
                  size="small"
                  // disabled={true}
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planingDepartment")}
                    </span>
                  }
                  className="mr-20"
                  style={{ width: "80%", fontWeight: "bold", marginTop: "3px" }}
                  value={
                    planingDepartment != null ? planingDepartment.name : ""
                  }
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
                {shouldOpenSelectManagementDepartmentPopup && (
                  <SelectManagementDepartmentPopup
                    open={shouldOpenSelectManagementDepartmentPopup}
                    handleSelect={this.handleSelectManagementDepartment}
                    selectedItem={
                      planingDepartment != null ? planingDepartment : {}
                    }
                    handleClose={
                      this.handleSelectManagementDepartmentPopupClose
                    }
                    t={t}
                    i18n={i18n}
                  />
                )}
              </Grid>
            </Grid>
            <Grid className="" container spacing={1}>
              {this.state.status?.indexOrder == this.PURCHASE.indexOrder && (
                <Grid item md={3} sm={12} xs={12} className="">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DateTimePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("purchasePlaning.approvedDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={"approvedDate"}
                      value={approvedDate}
                      maxDate={now}
                      onChange={(date) =>
                        this.handleDateChange(date, "approvedDate")
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
              )}

              {this.state.status?.indexOrder === this.PURCHASE.indexOrder ? (
                <Grid className="" item md={9} sm={12} xs={12}>
                  <TextValidator
                    className="w-100 "
                    label={t("maintainRequest.note")}
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    value={note}
                  />
                </Grid>
              ) : (
                <Grid className="" item md={12} sm={12} xs={12}>
                  <TextValidator
                    className="w-100 "
                    label={t("maintainRequest.note")}
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    value={note}
                  />
                </Grid>
              )}
            </Grid>
            <Grid className="mb-16" container spacing={1}></Grid>

            <Grid container spacing={2}>
              <Grid item xs={12}>
                <MaterialTable
                  data={listProducts ? listProducts : []}
                  columns={columns}
                  options={{
                    toolbar: false,
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    padding: "dense",
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "280px",
                    minBodyHeight: "280px",
                    headerStyle: {
                      backgroundColor: "#358600",
                      color: "#fff",
                    },
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                  }}
                  components={{
                    Toolbar: (props) => (
                      <div style={{ witdth: "100%" }}>
                        <MTableToolbar {...props} />
                      </div>
                    ),
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
export default PurchasePlaningDialog;
