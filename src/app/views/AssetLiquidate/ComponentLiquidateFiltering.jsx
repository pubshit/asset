import React from 'react'
import { useTranslation } from "react-i18next";
import { useState } from "react";
import { appConst } from "../../appConst";
import { Card, Grid, TextField, Collapse } from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Autocomplete, { createFilterOptions } from "@material-ui/lab/Autocomplete";
import viLocale from "date-fns/locale/vi";
import '../../../styles/custom.scss';
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { ValidatorForm } from "react-material-ui-form-validator";
function ComponentLiquidateFiltering(props) {
	const { t } = useTranslation();
	const [statuses, setStatuses] = useState(null);
	const { createDateBottom, createDateTop, issueDateBottom, issueDateTop, handoverDepartment } = props.item;
	const isAll = appConst.tabLiquidate.tabAll === props?.item?.statusLiquidattion
	const isPlan = appConst.tabLiquidate.tabPlan === props?.item?.statusLiquidattion
	const isWaiting = appConst.tabLiquidate.tabIsLiquiDating === props?.item?.statusLiquidattion
	const isLiquidated = appConst.tabLiquidate.tabLiquidated === props?.item?.statusLiquidattion
	const filterAutocomplete = createFilterOptions();

	const handleSelectChange = (event, value) => {
		props.handleFilteringSelectChange(value);
		setStatuses(value);
	};

	return (
		<Collapse in={props.advancedSearch}>
			<Card elevation={2} className="mb-16 px-16 py-16">
				<div>
					<Grid item container xs={12} sm={12} md={12} lg={12} spacing={2}>
						{(isAll || isPlan) && <>
							<Grid item xs={12} sm={6} md={4} lg={3}>
								<MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
									<KeyboardDatePicker
										className="w-100"
										id="mui-pickers-date"
										label={t("liquidate.createDateStart")}
										type="text"
										autoOk={true}
										format="dd/MM/yyyy"
										name={"fromDate"}
										value={createDateBottom}
										invalidDateMessage={t("general.invalidDateFormat")}
										maxDate={createDateTop ? (new Date(createDateTop)) : undefined}
										minDateMessage={t("general.minYearDefault")}
										maxDateMessage={createDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
										clearable
										onChange={(e) => props?.handleDateChange(e, 'createDateBottom')}
										clearLabel={t("general.remove")}
										cancelLabel={t("general.cancel")}
										okLabel={t("general.select")}
									/>
								</MuiPickersUtilsProvider>
							</Grid>
							<Grid item xs={12} sm={6} md={4} lg={3}>
								<MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
									<KeyboardDatePicker
										className="w-100"
										id="mui-pickers-date"
										label={t("liquidate.createDateEnd")}
										type="text"
										autoOk={true}
										format="dd/MM/yyyy"
										name={"fromDate"}
										value={createDateTop}
										invalidDateMessage={t("general.invalidDateFormat")}
										minDate={createDateBottom ? (new Date(createDateBottom)) : undefined}
										minDateMessage={createDateBottom ? t("general.minDateToDate") : t("general.minYearDefault")}
										maxDateMessage={t("general.maxDateDefault")}
										clearable
										onChange={(e) => props?.handleDateChange(e, 'createDateTop')}
										clearLabel={t("general.remove")}
										cancelLabel={t("general.cancel")}
										okLabel={t("general.select")}
									/>
								</MuiPickersUtilsProvider>
							</Grid>
						</>}
						{(isAll || isWaiting || isLiquidated) && <>
							<Grid item xs={12} sm={6} md={4} lg={3}>
								<MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
									<KeyboardDatePicker
										className="w-100"
										id="mui-pickers-date"
										label={t("liquidate.issueDateBottom")}
										type="text"
										autoOk={true}
										format="dd/MM/yyyy"
										name={"fromDate"}
										value={issueDateBottom}
										invalidDateMessage={t("general.invalidDateFormat")}
										maxDate={issueDateTop ? (new Date(issueDateTop)) : undefined}
										minDateMessage={t("general.minYearDefault")}
										maxDateMessage={issueDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
										clearable
										onChange={(e) => props?.handleDateChange(e, 'issueDateBottom')}
										clearLabel={t("general.remove")}
										cancelLabel={t("general.cancel")}
										okLabel={t("general.select")}
									/>
								</MuiPickersUtilsProvider>
							</Grid>
							<Grid item xs={12} sm={6} md={4} lg={3}>
								<MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
									<KeyboardDatePicker
										className="w-100"
										id="mui-pickers-date"
										label={t("liquidate.issueDateTop")}
										type="text"
										autoOk={true}
										format="dd/MM/yyyy"
										name={"fromDate"}
										value={issueDateTop}
										invalidDateMessage={t("general.invalidDateFormat")}
										minDate={issueDateBottom ? (new Date(issueDateBottom)) : undefined}
										minDateMessage={issueDateBottom ? t("general.minDateToDate") : t("general.minYearDefault")}
										maxDateMessage={t("general.maxDateDefault")}
										clearable
										onChange={(e) => props?.handleDateChange(e, 'issueDateTop')}
										clearLabel={t("general.remove")}
										cancelLabel={t("general.cancel")}
										okLabel={t("general.select")}
									/>
								</MuiPickersUtilsProvider>
							</Grid>
						</>}
						{isAll && <>
							<Grid item xs={12} sm={6} md={4} lg={3} >
								<Autocomplete
									options={appConst.listLiquidateStatus}
									fullWidth
									value={statuses || null}
									getOptionLabel={option => option.name}
									onChange={(e, newValue) => handleSelectChange(e, newValue)}
									filterOptions={(options, params) => {
										params.inputValue = params.inputValue.trim()
										let filtered = filterAutocomplete(options, params)
										return filtered
									}}
									renderInput={params => <TextField
										{...params}
										label={t("liquidate.status")}
										value={statuses?.name}
									/>}
								/>
							</Grid>
							<Grid item xs={12} sm={6} md={4} lg={3} >
								<ValidatorForm style={{ width: "100%" }}>
									<AsynchronousAutocompleteSub
										label={
											<span>
												<span> {t("allocation_asset.handoverDepartment")}</span>
											</span>
										}
										className="w-100"
										isNoRenderChildren
										searchFunction={getListOrgManagementDepartment}
										searchObject={{ isActive: 1 }}
										defaultValue={handoverDepartment || null}
										displayLable={"name"}
										value={handoverDepartment || null}
										onSelect={props.handleFilteringHandoverDepartmentChange}
										typeReturnFunction="list"
										showCode={"showCode"}
										noOptionsText={t("general.noOption")}
									/>
								</ValidatorForm>
							</Grid>
						</>}
					</Grid>
				</div>
			</Card>
		</Collapse>
	)
}

export default ComponentLiquidateFiltering
