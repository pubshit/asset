import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const AssetLiquidateTable = EgretLoadable({
  loader: () => import("./AssetLiquidateTable")
});
const ViewComponent = withTranslation()(AssetLiquidateTable);

const AssetLiquidateRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/liquidate-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default AssetLiquidateRoutes;