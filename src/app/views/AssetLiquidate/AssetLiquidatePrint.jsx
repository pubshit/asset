/* eslint-disable no-unused-expressions */
import React, { useEffect, useRef, useState } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {
  PaperComponent,
  convertNumberPrice,
  convertNumberToWords,
  getTheHighestRole
} from "app/appFunction";
import { appConst } from "app/appConst";
import PropTypes from "prop-types";
import Mustache from "mustache"
import localStorageService from "app/services/localStorageService";

export default function AssetTransferToAnotherUnitPrint(props) {
  let { open, handleClose, t, item } = props;
  const [html, setHtml] = useState("");
  const ref = useRef(null);
  const [arr, setArr] = useState([])
  const [arrConclude, setArrConclude] = useState([])
  const { currentUser, addressOfEnterprise } = getTheHighestRole();
  let printDate = new Date(item?.issueDate);
  let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
  const isTSCD = !props.assetClass
    || props.assetClass === appConst.assetClass.TSCD
  const nameByType = isTSCD ? " TSCĐ " : " CCDC ";

  const isReason = "isReason";

  const newDate = new Date()
  const day = String(newDate?.getDate())?.padStart(2, '0');
  const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
  const year = String(newDate?.getFullYear());

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  const renderTemplate = async () => {
    try {
      const res = await fetch("/assets/form-print/assetLiquidateTemplate.txt");
      const data = await res.text();
      let dataView = {
        ...item,
        decisionCode: item?.decisionCode || "......",
        orgName: currentUser?.org?.name,
        budgetCode: titleName?.budgetCode,
        nameByType: nameByType,
        nameByTypeLong: isTSCD ? " tài sản" : " công cụ dụng cụ",
        isTSCD: isTSCD,
        isNotTSCD: !isTSCD,
        border: "1px solid",
        width: "20%",
        textAlign: "center",
        style: {
          w_5: 'border: 1px solid; width: 5%; text-align: center;',
          w_9: 'border: 1px solid; width: 9%; text-align: center;',
          w_11: 'border: 1px solid; width: 11%; text-align: center;',
          w_15: 'border: 1px solid; width: 15%; text-align: center;',
          w_20: 'border: 1px solid; width: 20%; text-align: center;',
        },
        costName: isTSCD ? "Nguyên giá" : "Thành tiền",
        reason: item?.reason
          ? `
              <div style="
                position: relative;
                line-height: 25px;
                word-break: break-word;
                overflow: hidden;
              ">
                <div id="dottedEl">
                  ${item.reason}
                </div>
                <div style="
                  position: absolute;
                  top: -4px;
                  left: 0;
                  right: 0;
                ">
                  ${renderDotted(isReason).length > 0 ? renderDotted(isReason).join('') : ""}
                </div>
              </div>`
          : `
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
            `,
        conclude: item?.conclude
          ?
          `
              <div style="
                position: relative;
                line-height: 25px;
                word-break: break-word;
                overflow: hidden;
              ">
                <div id="dottedElConclude">
                  ${item.conclude}
                </div>
                <div style="
                  position: absolute;
                  top: -4px;
                  left: 0;
                  right: 0;
                ">
                  ${renderDotted().length > 0 ? renderDotted().join('') : ""}
                </div>
              </div>`
          : `
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
            `,
        assetVouchers: item?.assetVouchers?.map(i => {
          return {
            ...i,
            originalCost: convertNumberPrice(i?.asset?.originalCost) || 0,
            KHLK: convertNumberPrice(i?.asset?.originalCost - i?.asset?.carryingAmount) || 0,
            carryingAmount: convertNumberPrice(i?.asset?.carryingAmount) || 0
          }
        }),
        day,
        month,
        year,
        liquidationCost: convertNumberPrice(item?.liquidationCost) || 0,
        liquidationCostToWords: convertNumberToWords(item?.liquidationCost) || "Không",
        recoveryValue: convertNumberPrice(item?.recoveryValue) || 0,
        recoveryValueToWords: convertNumberToWords(item?.recoveryValue) || "Không",
      };
      const renderedHtml = Mustache.render(data, dataView);
      setHtml(renderedHtml);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    renderTemplate();
  }, [item, currentUser, titleName, nameByType, isTSCD, arr]);

  const renderDotted = (type) => {
    let height = 0;
    const dottedEl = document.getElementById('dottedEl');
    const dottedElConclude = document.getElementById('dottedElConclude');

    if (type === isReason) {
      height = dottedEl?.scrollHeight
    } else {
      height = dottedElConclude?.scrollHeight
    }

    const newArr = [];
    const extraLine = 5;
    const rowNumber = Math.floor(height / 25) + extraLine;

    for (let i = 1; i <= rowNumber; i++) {
      newArr.push(`<div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>`);
    }
    return newArr
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">Phiếu thanh lý {nameByType}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        onSubmit={handleFormSubmit}
        class="validator-form-scroll-dialog"
      >
        <DialogContent id="divcontents" className=" dialog-print" ref={ref}>
          <Grid>
            <div dangerouslySetInnerHTML={{ __html: html }} />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-16"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("In")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

AssetTransferToAnotherUnitPrint.propTypes = {
  item: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  assetClass: PropTypes.node.isRequired,
  t: PropTypes.node.isRequired,
}
