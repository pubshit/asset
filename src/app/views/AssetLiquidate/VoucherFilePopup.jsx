import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid, DialogActions,
  Card, Divider, Icon, IconButton
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { EgretProgressBar } from 'egret'
import ImportExcelDialog from '../Component/ImportExcel/ImportExcelDialog'
import axios from "axios";
import ConstantList from "../../appConfig";
import { createAssetDocument, updateAssetDocumentById } from '../Asset/AssetService'
import FileSaver from 'file-saver';
import GetAppSharpIcon from '@material-ui/icons/GetAppSharp';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst } from "../../appConst";
import AppContext from "app/appContext";
import { handleCheckFilesSize } from "../../appFunction";
toast.configure();

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

class VoucherFilePopup extends Component {
  constructor(props) {
    super(props);
    this.filesInput = React.createRef();
  }

  state = {
    name: "",
    code: "",
    description: "",
    shouldOpenImportExcelDialog: false,
    shouldOpenNotificationPopup: false,
    dragClass: "",
    attachments: [],
    files: [],
    statusList: [],
    queProgress: 0,
    progress: 0,
    assetId: null,
    documentType: appConst.documentType.ASSET_DOCUMENT_LIQUIDATE,
    fileDescriptionIds: [],
    maintainRequestId: null,
    voucherId: null
  };

  convertDto = (state) => {
    let data = {
      name: state?.name,
      code: state?.code,
      description: state?.description,
      assetId: state?.assetId,
      documentType: state?.documentType,
      fileDescriptionIds: state?.fileDescriptionIds,
      maintainRequestId: state?.maintainRequestId,
      voucherId: state?.voucherId,
      idHoSoDK: state?.idHoSoDK
    }
    return data;
  }

  handleChange = (event) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  validation = () => {
    let { fileDescriptionIds, files } = this.state;
    if (!files || files?.length <= 0) {
      toast.warning("Vui lòng chọn tệp đính kèm")
      return false;
    }
    else if (files?.length > 0 && !fileDescriptionIds || fileDescriptionIds?.length <= 0) {
      toast.warning("Tệp đính kèm phải được tải lên trước khi lưu")
      return false;
    }
    else if (files?.length !== fileDescriptionIds?.length) {
      toast.warning("Có tệp đính kèm chưa được tải lên")
      return false;
    }
    return true;
  }

  handleFormSubmit = () => {
    let { id } = this.state;
    let { t } = this.props;
    let dataState = this.convertDto(this.state)
    let { setPageLoading } = this.context;
    if (this.validation()) {
      setPageLoading(true);
      if (id) {
        updateAssetDocumentById(dataState, id).then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            this.props.updatePageAssetDocument(this.state);
            this.props.handleClose();
            toast.success(t("general.updateSuccess"));
          }
          else {
            toast.warning(data?.message)
          }
        }).catch(error => {
          toast.error(t("general.error"));
        })
      } else {
        createAssetDocument(dataState).then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            this.props.getAssetDocument(data?.data)
            this.props.handleClose();
            toast.success(t("general.addSuccess"));
          }
          else {
            toast.warning(data?.message)
          }
        }).catch(error => {
          toast.error(t("general.error"));
        })
      }
    }
  };

  componentWillMount() {
    let { itemAssetDocument, item } = this.props;
    let { isEditAssetDocument } = item;
    
    let files = [];
    if (itemAssetDocument?.id) {
      itemAssetDocument.attachments.forEach(element => {
        let rest = Object.assign(element, { 'isEditAssetDocument': isEditAssetDocument });
        files.push(rest);
      });
    }
    this.setState({
      ...this.props.itemAssetDocument,
      files: files
    }, function () { }
    );
  }

  componentDidMount() {
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
    this.updatePageData()
  }

  handleFileUploadOnSelect = event => {
    let files = event.target.files;
    this.fileUpload(files[0]).then(res => {
      this.setState({ progress: 100 })
      toast.success("Tải tập tin thành công");
    });
  }
  handleFileSelect = event => {
    let files = event.target.files;
    let arrayFromFiles = Array.from(files);
    let list = [...this.state.files];

    if (handleCheckFilesSize(arrayFromFiles)) {
      return this.filesInput.current.value = null;
    }

    for (const iterator of files) {
      let MaxSizeFile = 10485760;

      if (iterator?.size > MaxSizeFile) {
        toast.warn("Kích thước của file không được vượt quá 10MB")
        return;
      }
      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0
      });
    }

    this.setState({
      files: [...list]
    }, () => this.uploadSingleFile(this.state.files?.length - 1));
    //đặt lại giá trị cho input khi upload
    this.filesInput.current.value = null;
  };

  handleSingleRemove = index => {
    let files = [...this.state.files];
    let attachments = [...this.state.attachments];
    let fileDescriptionIds = [...this.state.fileDescriptionIds];
    files.splice(index, 1);
    attachments.splice(index, 1);
    fileDescriptionIds.splice(index, 1);
    this.setState({
      files: [...files],
      attachments: [...attachments],
      fileDescriptionIds: [...fileDescriptionIds]
    });
  };

  fileUpload(file) {
    let { setPageLoading } = this.context;
    const url = ConstantList.API_ENPOINT + "/api/fileUpload/asset/assetDocument/uploadattachment";
    let formData = new FormData();
    formData.append('uploadfile', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    setPageLoading(true);
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    return axios.post(url, formData, config).then(({ data }) => {
      let attachment = data;
      let { attachments, fileDescriptionIds } = this.state;
      if (attachment) {
        attachments.push(attachment);
        fileDescriptionIds.push(attachment.file.id);
      }
      this.setState({ attachments, fileDescriptionIds });
    }).catch(() => {
      toast.error(t("general.error"));
    }).finally(() => {
      setPageLoading(false);
    })

  }

  uploadSingleFile = async (index) => {
    let { t } = this.props;
    let allFiles = [...this.state.files];
    let file = this.state.files[index];
    try {
      if (file) {
        await this.fileUpload(file.file).then(res => {
          toast.success("Tải tập tin thành công");
        });
        allFiles[index] = { ...file, uploading: true, success: true, error: false};
        this.setState({
          files: [...allFiles]
        });
      }
    } catch {
      toast.error(t("general.error"));
    } finally {
      this.filesInput.current.value = null;
    }
  };

  handleViewDocument = index => {
    const { TYPES_FILE } = appConst;
    let file = this.state.files[index];
    let contentType = file.file.contentType;
    let fileName = file.file.name;
    const url = ConstantList.API_ENPOINT + "/api/fileDownload/assetDocument/" + file.file.id;
    axios.get(url, { responseType: 'arraybuffer' }).then((successResponse) => {
      let document = successResponse.data;
      let file = null;
      file = new Blob([document], { type: contentType });
      if (
        file.type === TYPES_FILE.PDF
        || file.type === TYPES_FILE.TEXT
        || TYPES_FILE.IMAGE_ARRAY.includes(file.type)
      ) {
        let fileURL = URL.createObjectURL(file, fileName);
        window.open(fileURL, '_blank');
        URL.revokeObjectURL(fileURL);
      } else {
        toast.warning(t("general.viewAttachmentError"))
      }
    })

  }
  handleDownloadDocument = index => {
    let file = this.state.files[index];
    let contentType = file.file.contentType;
    let fileName = file.file.name;
    const url = ConstantList.API_ENPOINT + "/api/fileDownload/assetDocument/" + file.file.id;
    axios.get(url, { responseType: 'arraybuffer' }).then((successResponse) => {
      let document = successResponse.data;
      let file = new Blob([document], { type: contentType });
      return FileSaver.saveAs(file, fileName);
    });
  }
  render() {
    let { open, t, i18n, item } = this.props;
    let { files } = this.state;
    let isEmpty = files.length === 0;
    let {
      name,
      description,
      shouldOpenImportExcelDialog,
    } = this.state;
  
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle className="cursor-move pb-0" id="draggable-dialog-title">
          <h4 className="mb-0" >{t('general.saveUpdate')}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          {shouldOpenImportExcelDialog && (
            <ImportExcelDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenImportExcelDialog}
              handleOKEditClose={this.handleOKEditClose}
            />
          )}
          <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }} >
            <Grid className="" container spacing={1}>
              <Grid item md={3} sm={12} xs={12}>
                {/* Mã hồ sơ tài sản */}
                <div className="mt-24"><label className="mt-24 font-weight-bold">{t('AssetFile.code')} : </label> {this.state.code || ""}</div>
              </Grid>

              <Grid item md={9} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span><span className="colorRed">*</span>{t('AssetFile.name')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  InputProps={{
                    readOnly: item?.isView
                  }}
                />
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={<span><span className="colorRed"></span>{t('AssetFile.description')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="description"
                  value={description}
                  readOnly={item.isView}
                  InputProps={{
                    readOnly: item?.isView
                  }}
                />
              </Grid>

            </Grid>
            <div className="mt-12">
              <div className="flex flex-wrap">
                <label htmlFor="upload-single-file">
                  <Button
                    size="small"
                    className="capitalize"
                    component="span"
                    variant="contained"
                    color="primary"
                    disabled={item.isView}
                  >
                    <div className="flex flex-middle">
                      <span>{t('general.select_file')}</span>
                    </div>
                  </Button>
                </label>
                <input
                  ref={this.filesInput}
                  className="display-none"
                  onChange={this.handleFileSelect}
                  id="upload-single-file"
                  type="file"
                  disabled={item.isView}
                />
                <div className="px-16"></div>
              </div>
              <Card className="mb-24" elevation={2}>
                <div className="p-16">
                  <Grid
                    container
                    spacing={2}
                    justify="center"
                    alignItems="center"
                    direction="row"
                  >
                    <Grid item lg={4} md={4}>
                      {t('general.file_name')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.size')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.action')}
                    </Grid>
                  </Grid>
                </div>
                <Divider></Divider>

                {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

                {files.map((item, index) => {
                  let { file, success, error, progress, isEditAssetDocument, contentSize } = item;
                  return (
                    <div className="px-16 py-8" key={file.name}>
                      <Grid
                        container
                        spacing={2}
                        justify="center"
                        alignItems="center"
                        direction="row"
                      >
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          {file.name}
                        </Grid>
                        {isEditAssetDocument === true ? (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file.contentSize / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        ) : (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {(file.size / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        )}
                          {(isEditAssetDocument || success) ? (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={100}></EgretProgressBar>
                          </Grid>
                        ) : (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={progress}></EgretProgressBar>
                          </Grid>
                        )}
                        <Grid item lg={1} md={1} sm={12} xs={12}>
                          {error && <Icon fontSize="small" color="error">error</Icon>}
                        </Grid>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          <div className="flex">
                            {!isEditAssetDocument && (
                              <IconButton disabled={success} size="small" title={t('general.upload')} onClick={() => this.uploadSingleFile(index)}>
                                <Icon color={success ? "disabled" : "primary"} fontSize="small" >cloud_upload</Icon>
                              </IconButton>
                            )}
                            {isEditAssetDocument && (<IconButton size="small" title={t('general.viewDocument')} onClick={() => this.handleViewDocument(index)}>
                              <Icon fontSize="small" color="primary">visibility</Icon>
                            </IconButton>
                            )}

                            {isEditAssetDocument && (<IconButton size="small" title={t('general.downloadDocument')} onClick={() => this.handleDownloadDocument(index)}>
                              <Icon fontSize="small" color="default"><GetAppSharpIcon /></Icon>
                            </IconButton>
                            )}
                            {!this.props.item.isView && (
                              <IconButton size="small" title={t('general.removeDocument')} onClick={() => this.handleSingleRemove(index)}>
                                <Icon fontSize="small" color="error">delete</Icon>
                              </IconButton>
                            )}
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  );
                })}
              </Card>
            </div>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                className="mr-16"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

VoucherFilePopup.contextType = AppContext;
export default VoucherFilePopup;
