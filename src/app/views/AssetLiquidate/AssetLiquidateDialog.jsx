import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { addNewOrUpdate } from "./AssetLiquidateService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import LiquidateScrollableTabsButtonForce from "./LiquidateScrollableTabsButtonForce";
import {
  deleteAssetDocumentById,
  getAssetDocumentById,
  getListAssetReevaluate,
  getListOrgManagementDepartment,
  getNewCodeAssetDocumentLiquidate,
} from "../Asset/AssetService";
import {
  formatDateDto,
  getTheHighestRole, handleThrowResponseMessage,
  isNumberRegex, isSuccessfulResponse,
} from "../../appFunction";
import { STATUS_THANH_LY_TSCD, appConst } from "../../appConst";
import AppContext from "app/appContext";
import { PaperComponent } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Liquidate,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    assetVouchersList: [],
    voucherType: ConstantList.VOUCHER_TYPE.Liquidate,
    handoverPersonName: null,
    handoverPersonList: [],
    decisionCode: "",
    code: "",
    name: "",
    liquidationStatus: "",
    liquidationCost: "",
    recoveryValue: "",
    conclude: "",
    persons: [],
    personIds: [],
    listAssetDocumentId: [],
    liquidateDepartmentUserList: [],
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    handoverDepartmentId: "",
    useDepartment: null,
    asset: {},
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_THANH_LY_TSCD,
    handoverPersonClone: false,
    managementDepartment: null,
    managementDepartmentList: [],
    transferAddress: "",
    assetDocumentList: [],
    documentType: 3,
    shouldOpenPopupAssetFile: false,
    shouldOpenSelectPersonPopup: false,
    shouldOpenAssetReevaluateDialog: false,
    assetReevaluate: {},
    reason: ""
  };
  voucherType = ConstantList.VOUCHER_TYPE.Liquidate;

  setStateAssetLiquidate = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleNumberChange = (event) => {
    if (isNumberRegex(event.target.value)) {
      this.setState({
        [event.target.name]: Number(event.target.value),
      });
    }
  };

  handleLiquidationDepartmentUserListChange = (event) => {
    if (isNumberRegex(event.target.value)) {
      this.setState({
        [event.target.name]: event.target.value,
      });
    }
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers, assetVouchersList } = this.state;
    let itemFound = assetVouchers.find(
      (item) => item.assetId === rowData?.asset?.id
    );
    let index = assetVouchers.indexOf(itemFound);
    assetVouchers[index].note = valueText?.target?.value;
    assetVouchersList[index].asset.note = valueText?.target?.value;
    this.setState({
      assetVouchers: assetVouchers,
      assetVouchersList,
      handoverPersonClone: true,
    });
  };

  convertStateData = (state) => {
    return {
      id: state.id || "",
      assetVouchers: state.assetVouchers.map((item) => {
        return {
          assetId: item.asset?.id,
          note: item.asset?.note,
        };
      }),
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      code: state.code,
      decisionCode: state?.decisionCode,
      name: state.name,
      liquidationStatus: state.liquidationStatus,
      handoverDepartmentId: state.handoverDepartmentId,
      handoverPersonName: state.handoverPerson?.personDisplayName,
      handoverPersonId: state.handoverPerson?.personId,
      conclude: state.conclude,
      liquidationCost: state.liquidationCost,
      listAssetDocumentId: state.listAssetDocumentId,
      personIds: state.personIds,
      recoveryValue: state.recoveryValue,
      transferAddress: state.transferAddress,
      hoiDongId: this.state?.hoiDongId,
      reason: state?.reason,
      persons: state?.persons
        ? state?.persons?.map((item) => {
          return {
            departmentId: item?.departmentId,
            departmentName: item?.departmentName,
            personId: item?.personId,
            personName: item?.personName,
            position: item?.position,
            role: item?.role,
          };
        })
        : [],
    };
  };

  validation = () => {
    let {
      assetVouchers,
      issueDate,
      persons,
      name,
      liquidationStatus,
      transferAddress,
      handoverDepartment,
      handoverPerson,
    } = this.state;
    let check = false;
    if (!name) {
      toast.warning("Vui lòng nhập tên phiếu.");
      return true;
    }
    if (!liquidationStatus) {
      toast.warning("Trạng thái phiếu không được để trống.");
      return true;
    }
    if (!handoverDepartment) {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
      return true;
    }
    if (!handoverPerson) {
      toast.warning("Vui lòng chọn người bàn giao.");
      return true;
    }
    if (!liquidationStatus) {
      toast.warning("Trạng thái phiếu không được để trống.");
      return true;
    }
    if (!transferAddress) {
      toast.warning("Vui lòng nhập địa chỉ thanh lý.");
      return true;
    }
    if (Object.keys(assetVouchers).length === 0) {
      toast.warning("Chưa chọn tài sản");
      return true;
    }
    if (assetVouchers?.length && issueDate) {
      let invalidAsset = assetVouchers?.some(
        i => new Date(i?.dateOfReception || i?.asset?.dateOfReception) > new Date(issueDate)
      );
      if (invalidAsset) {
        toast.warning("Ngày thanh lý phải lớn hơn ngày tiếp nhận của tài sản");
        return true;
      }
    }
    if (this.state?.hoiDongId && persons?.length === 0) {
      toast.warning("Hội đồng không có thành viên");
      toast.clearWaitingQueue();
      return true;
    }
    return check;
  };

  handleFormSubmit = async (e) => {
    if (e.target.id !== "formAssetLiquidateDialog") return;
    let { setPageLoading } = this.context
    let { id } = this.state;
    let { t, handleOKEditClose } = this.props;
    setPageLoading(true);

    if (this.validation()) {
      return setPageLoading(false);
    }

    try {
      let sendData = this.convertStateData(this.state);
      let res = await addNewOrUpdate(sendData);
      let { code } = res?.data;
      if (isSuccessfulResponse(code)) {
        id
          ? toast.success(t("general.updateSuccess"))
          : toast.success(t("general.addSuccess"));
        handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true,
    });
  };

  handleSelectPerson = (listPerson) => {
    let personIds = [];
    let persons = [];
    if (listPerson?.length > 0) {
      listPerson.forEach((item) => {
        let person = item?.user?.person ? item?.user?.person : item;
        if (item?.user?.person) {
          person.departmentName = item?.department?.name;
        }
        let personId = item?.person?.id ? item?.person?.id : person?.id;

        persons.push(person);
        personIds.push(personId);
      });
      this.setState({
        persons: persons,
        personIds: personIds,
      });
    } else {
      this.setState({
        persons: [],
        personIds: [],
      });
    }
    this.handleSelectPersonPopupClose();
  };

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false,
    });
  };

  handleSelectAsset = (items) => {
    let { inventoryLiquidateAssets, isInventoryLiquidateAsset } = this.props.item;
    if (isInventoryLiquidateAsset) {
      inventoryLiquidateAssets?.map((asset) => {
        let found = items?.find((item) => item?.asset?.id === asset?.asset?.id);
        if (!found) {
          items.push(asset);
        }
      });
    }

    items?.forEach((element) => {
      element.assetId = element.asset?.id;
    });

    if (items?.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
      toast.warning("Chưa có tài sản nào được chọn");
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let { t } = this.props;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({ assetVouchers }, () => {
      toast.dismiss();
      toast.info(t("asset_liquidate.deleteFromList"));
    });
  };

  removePersonInlist = (personId) => {
    let { persons, personIds } = this.state;
    let index = persons?.findIndex((x) => x.id === personId);
    let indexId = personIds?.findIndex((personIds) => personIds === personId);
    persons.splice(index, 1);
    personIds.splice(indexId, 1);

    this.setState({ persons, personIds }, () => {
      toast.dismiss();
      toast.info(this.props.t("asset_liquidate.deleteFromList"));
    });
  };

  getManagementDepartment = () => {
    getListOrgManagementDepartment({}).then(({ data }) => {
      let handoverDepartment = data ? data[0] : null;
      this.setState({
        handoverDepartment: handoverDepartment,
        handoverDepartmentId: handoverDepartment?.id,
        managementDepartmentId: handoverDepartment?.id,
      });
    });
  };

  handleSetListLiquidateDepartmentUserList = (item) => {
    this.setState({ persons: item || [] });
  };

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id });
  };

  async componentDidMount() {
    let { item, checkDelete } = this.props;
    let {
      inventoryLiquidateAssets,
      isInventoryLiquidateAsset,
    } = item;
    let roles = getTheHighestRole();
    let { departmentUser, isRoleAssetManager } = roles;

    if (isInventoryLiquidateAsset) {
      this.setState({
        ...roles,
        checkDelete,
        assetVouchers: inventoryLiquidateAssets || [],
        handoverDepartment: departmentUser || null,
        handoverDepartmentId: departmentUser?.id || "",
      });
    } else {
      this.setState({ ...item, checkDelete, ...roles });
    }

    if (isRoleAssetManager) {
      this.getManagementDepartment();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.type === "reevaluate" &&
      this.props?.item !== prevProps?.item
    ) {
      this.setState({
        ...this.state,
        assetVouchers: this.props?.item?.assetVouchers,
        checkDelete: this.props?.checkDelete,
      });
    }
  }

  handleSetDataHandoverDepartment = (data) => {
    this.setState({
      listHandoverDepartment: data,
    });
  };

  handleSetDataHandoverPerson = (data) => {
    this.setState({
      listHandoverPerson: data,
    });
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item,
      handoverDepartmentId: item?.id,
      listHandoverPerson: [],
      assetVouchers: [],
      assetVouchersList: [],
      managementDepartmentId: item?.id,
      handoverPerson: null
    });
  };

  handleChangeSelect = (item) => {
    this.setState({
      liquidationStatus: item?.code,
    });
  };

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState({ item: {}, voucherType }, function () {
        this.setState({
          shouldOpenAssetPopup: true,
        });
      });
    } else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };
  handleHandoverDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: true,
      item: {},
    });
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    let { setPageLoading } = this.context;
    setPageLoading(false);
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      document?.attachments?.map((item) => {
        return fileDescriptionIds?.push(item?.file?.id);
      });

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = appConst.documentType.ASSET_DOCUMENT_LIQUIDATE;
      document.idHoSoDK = rowData.id,
        this.setState({
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        });
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let { t } = this.props;
    let index = documents?.findIndex((document) => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(
      (documentId) => documentId === id
    );
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents }, () => {
      toast.dismiss();
      toast.info(t("asset_liquidate.deleteFromList"));
    });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeAssetDocumentLiquidate()
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data.data;
          this.setState({
            item,
            shouldOpenPopupAssetFile: true,
          });
        } else {
          toast.warning(data.message);
        }
      })
      .catch((err) => toast.error("Không thể tạo mã hồ sơ"));
  };

  updatePageAssetDocument = (newValue) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(
      (item) => item?.id === newValue?.id
    );
    documents[indexDocument] = newValue;
    this.setState({ documents });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId
      ? this.state?.listAssetDocumentId
      : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;
    if (listAssetDocumentId && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach((id) => deleteAssetDocumentById(id));
    }
  };

  handleOpenAssetReevaluateDialog = async () => {
    let { t } = this.props;
    try {
      let codeAsset =
        this.state.assetReevaluate?.asset?.code ||
        this.state.assetReevaluate?.code;
      if (codeAsset) {
        let searchObject = {
          assetClass: appConst.assetClass.TSCD,
          pageSize: 10,
          pageIndex: 1,
        };
        searchObject.keyword = codeAsset;
        let res = await getListAssetReevaluate(searchObject);
        if (
          res?.data?.code === appConst.CODE.SUCCESS &&
          res?.data?.data?.content &&
          res?.data?.data?.content?.length > 0 &&
          res?.status === appConst.CODE.SUCCESS
        ) {
          if (this.state.assetReevaluate?.asset) {
            this.setAssetReevaluate({
              ...this.state.assetReevaluate.asset,
              ...res?.data?.data?.content[0],
            });
          } else {
            this.setAssetReevaluate({
              ...this.state.assetReevaluate,
              ...res?.data?.data?.content[0],
            });
          }
          this.setState({
            shouldOpenAssetReevaluateDialog: true,
          });
        } else {
          this.setAssetReevaluate({});
          toast.warning(t("AssetReevaluateDialog.noti.notFound"));
        }
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    }
  };

  handleAssetReevaluateDialogClose = () => {
    this.setState({
      shouldOpenAssetReevaluateDialog: false,
    });
  };

  setAssetReevaluate = (value) => {
    this.setState({
      assetReevaluate: value,
    });
  };
  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };
  render() {
    let { open, t, i18n } = this.props;
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: this.state.handoverDepartment?.id || null,
    };

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          onError={() => this.context.setPageLoading(false)}
          id="formAssetLiquidateDialog"
          className="validator-form-scroll-dialog"
        >
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            <span className="">{t("asset_liquidate.dialog")}</span>
          </DialogTitle>
          <DialogContent className="min-height-450">
            <LiquidateScrollableTabsButtonForce
              t={t}
              id={this.props?.item?.id}
              i18n={i18n}
              item={this.state}
              handleHandoverDepartmentPopupOpen={this.handleHandoverDepartmentPopupOpen}
              handleHandoverDepartmentPopupClose={this.handleHandoverDepartmentPopupClose}
              handoverPersonSearchObject={handoverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleSelectPerson={this.handleSelectPerson}
              openSelectPersonPopup={this.openSelectPersonPopup}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleChange={this.handleChange}
              handleNumberChange={this.handleNumberChange}
              handleDateChange={this.handleDateChange}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              removePersonInlist={this.removePersonInlist}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              updatePageAssetDocument={this.updatePageAssetDocument}
              getAssetDocument={this.getAssetDocument}
              handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
              handleSetDataHandoverDepartment={this.handleSetDataHandoverDepartment}
              handleSetDataHandoverPerson={this.handleSetDataHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              handleChangeSelect={this.handleChangeSelect}
              assetReevaluate={this.state.assetReevaluate}
              setAssetReevaluate={this.setAssetReevaluate}
              handleAssetReevaluateDialogClose={this.handleAssetReevaluateDialogClose}
              shouldOpenAssetReevaluateDialog={this.state.shouldOpenAssetReevaluateDialog}
              handleEdit={this.props?.handleEdit}
              assetVouchers={this.state.assetVouchers}
              getDsPhieuDanhGiaOfVoucher={this.props?.getDsPhieuDanhGiaOfVoucher}
              assetVoucherIds={this.props?.assetVoucherIds}
              disabledDgl={this.props?.disabledDgl}
              handleSetListLiquidateDepartmentUserList={this.handleSetListLiquidateDepartmentUserList}
              handleSetHoiDongId={this.handleSetHoiDongId}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={this.props.handleClose}
              >
                {t("general.cancel")}
              </Button>
              {!this.props?.disabledDgl && (
                <Button
                  variant="contained"
                  color="primary"
                  className="mr-12"
                  disabled={
                    !(this.state.assetReevaluate?.assetId || this.state.assetReevaluate?.id)
                  }
                  onClick={() => this.handleOpenAssetReevaluateDialog()}
                >
                  {t("general.reevaluate")}
                </Button>
              )}
              {(!this.props?.item?.isView || this.props?.isInventoryLiquidateAsset) && (
                <Button
                  variant="contained"
                  color="primary"
                  className="mr-16"
                  type="submit"
                  onClick={() => this.context.setPageLoading(true)}
                >
                  {t("general.save")}
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetTransferDialog.contextType = AppContext;
export default AssetTransferDialog;
