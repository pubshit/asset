import React from "react";
import { appConst } from "app/appConst";
import { convertNumberPriceRoundUp } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";

function ListAssetLiquidateTable(props) {
  const { t } = props;
  let columns = [
    {
      title: t("Asset.stt"),
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.code"),
      field: "assetCode",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "assetName",
      align: "left",
      minWidth: 200,
      cellStyle: {
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "assetManagementCode",
      align: "left",
      minWidth: 120,
    },
    {
      title: t("Asset.serialNumber"),
      field: "assetSerialNumber",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.model"),
      field: "assetModel",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.dateOfReception"),
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.dateOfReception
          ? moment(rowData?.dateOfReception).format("DD/MM/YYYY")
          : "",
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "assetYearPutIntoUse",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.statusDgl"),
      field: "assetDglTrangthai",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return appConst.statusAssetReevaluate.find(
          (item) => item.id === rowData?.assetDglTrangthai
        )?.name;
      },
    },
    {
      title: t("TransferToAnotherUnit.originalCost"),
      field: "assetOriginalCost",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        convertNumberPriceRoundUp(rowData?.assetOriginalCost),
    },
    {
      title: t("TransferToAnotherUnit.carryingAmount"),
      field: "assetCarryingAmount",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        convertNumberPriceRoundUp(rowData?.assetCarryingAmount),
    },
    {
      title: t("Asset.useDepartment"),
      field: "assetUseDepartmentName",
      align: "left",
      minWidth: 150,
      cellStyle: {
      },
    },
    {
      title: t("Asset.store"),
      field: "assetStoreName",
      align: "left",
      minWidth: 120,
      cellStyle: {
      },
    },
  ];
  return (
    <div>
      <MaterialTable
        data={props?.itemAsset?.assetVouchers ? props?.itemAsset?.assetVouchers : []}
        columns={columns}
        options={{
          toolbar: false,
          selection: false,
          actionsColumnIndex: -1,
          paging: false,
          search: false,
          sorting: false,
          scrollButtons: true,
          rowStyle: (rowData) => ({
            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
          }),
          headerStyle: {
            backgroundColor: "#358600",
            color: "#fff",
            textAlign: "center",
          },
          padding: "dense",
          maxBodyHeight: "450px",
          minBodyHeight: "250px",
        }}
        localization={{
          body: {
            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
          },
        }}
        components={{
          Toolbar: (props) => (
            <div style={{ witdth: "100%" }}>
              <MTableToolbar {...props} />
            </div>
          ),
        }}
        onSelectionChange={(rows) => {
          this.data = rows;
        }}
      />
    </div>
  );
}

export default ListAssetLiquidateTable;
