import {
	Button,
	ButtonGroup,
	ClickAwayListener,
	FormControl,
	Grid,
	Input,
	InputAdornment,
	MenuItem,
	MenuList,
	Paper,
	Popover,
	TablePagination
} from "@material-ui/core";
import React, { useContext, useEffect, useRef, useState } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import AssetLiquidateDialog from "./AssetLiquidateDialog";
import { OPTIONS_EXCEL_QR_CARD, appConst } from "../../appConst";
import ComponentLiquidateFiltering from "./ComponentLiquidateFiltering";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import AssetsQRPrintNew from "../Asset/ComponentPopups/AssetsQRPrintNew";
import {defaultPaginationProps, getRole} from "app/appFunction";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { convertNumberPrice, convertNumberToWords, getTheHighestRole } from "../../appFunction";
import localStorageService from "../../services/localStorageService";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";

function ComponentLiquidateTable(props) {
	const isAddNew = appConst.statusLiquidate.all === props?.item?.statusLiquidattion
	const isPlan = appConst.tabLiquidate.tabPlan === props?.item?.statusLiquidattion
	const isProcessing = appConst.tabLiquidate.tabIsLiquiDating === props?.item?.statusLiquidattion
	let { t, i18n, setItemState, handleToggleOpenExcel } = props;
	let {
		item,
		page,
		itemList,
		isPrint,
		rowsPerPage,
		totalElements,
		shouldOpenEditorDialog,
		shouldOpenConfirmationDialog,
		shouldOpenConfirmationDeleteAllDialog,
		openPrintQR,
		products,
		advancedSearch,
		openExcel,
		itemAsset
	} = props?.item;
	const { currentOrg } = useContext(AppContext);
	const { LIQUIDATE } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
	const isTSCD = true;
	const nameByType = isTSCD ? " TSCĐ " : " CCDC ";
	let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
	const { currentUser } = getTheHighestRole();
	const isReason = "isReason";
	const newDate = new Date()
	const day = String(newDate?.getDate())?.padStart(2, '0');
	const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
	const year = String(newDate?.getFullYear())

	const [anchorRef, setAnchorRef] = useState(null);
	const [heightReasonEl, setHeightReasonEl] = useState(26);
	const [heightConcludeEl, setHeightConcludeEl] = useState(26);
	let anchorRefF = useRef()
	useEffect(() => {
		setItemState({})
		setAnchorRef(anchorRefF)
	}, [])
	let optionExcels = getRole().isRoleAssetUser ? [OPTIONS_EXCEL_QR_CARD[0]] : OPTIONS_EXCEL_QR_CARD
	// const [openExcel, setOpenExcel] = useState(false);

	const getTemplateDetail = (template) => {
		// Tạo một div tạm thời để chứa HTML string
		const tempDiv = document.createElement('div');
		tempDiv.innerHTML = template;

		// Thêm div tạm thời vào body (hoặc bất kỳ container nào khác)
		document.body.appendChild(tempDiv);

		// Lấy element có id là 'dottedEl' từ div tạm thời
		const dottedEl = tempDiv.querySelector('#dottedEl');
		const dottedElConclude = tempDiv.querySelector('#dottedElConclude');

		// Lấy chiều cao của element này
		const heightReason = dottedEl?.scrollHeight;
		const heightConclude = dottedElConclude?.scrollHeight;

		// Loại bỏ div tạm thời khỏi DOM
		document.body.removeChild(tempDiv);

		// Trả về hoặc log chiều cao của element
		setHeightReasonEl(heightReason);
		setHeightConcludeEl(heightConclude);
	};

	const renderDotted = (type) => {
		let heightReason = 0;
		let heightConclude = 0;

		if (type === isReason) {
			heightReason = heightReasonEl
		} else {
			heightConclude = heightConcludeEl;
		}

		const newArr = [];
		const extraLine = 5;
		let rowNumber = 1;

		if (type === isReason) {
			rowNumber = Math.floor(heightReason / 25) + extraLine;
		} else {
			rowNumber = Math.floor(heightConclude / 25) + extraLine;
		}

		for (let i = 1; i <= (rowNumber || 1); i++) {
			newArr.push(`<div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>`);
		}

		return newArr
	};

	let dataView = {
		...item,
		decisionCode: item?.decisionCode || "......",
		orgName: currentUser?.org?.name,
		budgetCode: titleName?.budgetCode,
		nameByType: nameByType,
		nameByTypeLong: isTSCD ? " tài sản" : " công cụ dụng cụ",
		isTSCD: isTSCD,
		isNotTSCD: !isTSCD,
		border: "1px solid",
		width: "20%",
		textAlign: "center",
		style: {
			w_5: 'border: 1px solid; width: 5%; text-align: center;',
			w_9: 'border: 1px solid; width: 9%; text-align: center;',
			w_11: 'border: 1px solid; width: 11%; text-align: center;',
			w_15: 'border: 1px solid; width: 15%; text-align: center;',
			w_20: 'border: 1px solid; width: 20%; text-align: center;',
		},
		costName: isTSCD ? "Nguyên giá" : "Thành tiền",
		reason: item?.reason
			? `
              <div style="
                position: relative;
                line-height: 25px;
                word-break: break-word;
                overflow: hidden;
              ">
                <div id="dottedEl">
                  ${item.reason}
                </div>
                <div style="
                  position: absolute;
                  top: -4px;
                  left: 0;
                  right: 0;
                ">
                  ${renderDotted(isReason).length > 0 ? renderDotted(isReason).join('') : ""}
                </div>
              </div>`
			: `
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
            `,
		conclude: item?.conclude
			?
			`
              <div style="
                position: relative;
                line-height: 25px;
                word-break: break-word;
                overflow: hidden;
              ">
                <div id="dottedElConclude">
                  ${item.conclude}
                </div>
                <div style="
                  position: absolute;
                  top: -4px;
                  left: 0;
                  right: 0;
                ">
                  ${renderDotted().length > 0 ? renderDotted().join('') : ""}
                </div>
              </div>`
			: `
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
              <div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>
            `,
		assetVouchers: item?.assetVouchers?.map(i => {
			return {
				...i,
				originalCost: convertNumberPrice(i?.asset?.originalCost) || 0,
				KHLK: convertNumberPrice(i?.asset?.originalCost - i?.asset?.carryingAmount) || 0,
				carryingAmount: convertNumberPrice(i?.asset?.carryingAmount) || 0
			}
		}),
		day,
		month,
		year,
		liquidationCost: convertNumberPrice(item?.liquidationCost) || 0,
		liquidationCostToWords: convertNumberToWords(item?.liquidationCost) || "Không",
		recoveryValue: convertNumberPrice(item?.recoveryValue) || 0,
		recoveryValueToWords: convertNumberToWords(item?.recoveryValue) || "Không",
	};

	return (
		<>
			<Grid container spacing={2} justifyContent="space-between" className="pt-12">
				<Grid item md={6} xs={12}>
					{isAddNew && <Button
						className="mb-16 mr-16 align-bottom"
						variant="contained"
						color="primary"
						onClick={props.handleEditItem}
					>
						{t('Asset.asset_liquidate')}
					</Button>}
					{(isProcessing || isPlan) && <Button
						className="mb-16 mr-16 align-bottom"
						variant="contained"
						color="primary"
						onClick={props.handleDeleteAllItem}
					>
						{t('general.delete')}
					</Button>}

					{shouldOpenConfirmationDeleteAllDialog && (
						<ConfirmationDialog
							open={shouldOpenConfirmationDeleteAllDialog}
							onConfirmDialogClose={props.handleDialogClose}
							onYesClick={props.handleDeleteAll}
							text={t('general.deleteAllConfirm')}
							agree={t('general.agree')}
							cancel={t('general.cancel')}
						/>
					)}
					<Button
						className="mb-16 mr-16 align-bottom"
						variant="contained"
						color="primary"
						onClick={props.handleOpenAdvanceSearch}
					>
						{t("general.advancedSearch")}
						{!advancedSearch ? <ArrowDropDownIcon /> : <ArrowDropUpIcon />}
					</Button>
					{/* <Button
				className="mb-16 mr-16 align-bottom"
				variant="contained"
				color="primary"
				onClick={() => props.handleExportToExcel()}
			>
				{t("general.exportToExcel")}
			</Button> */}
					{!getRole().isRoleAssetUser && <>
						<ButtonGroup
							className="align-bottom mb-16 mr-16"
							ref={anchorRef}
							variant="contained"
							aria-label="split button"
						>
							<Button
								aria-controls={openExcel ? "split-button-menu" : undefined}
								aria-expanded={openExcel ? "true" : undefined}
								aria-label="select merge strategy"
								aria-haspopup="menu"
								variant="contained"
								color="primary"
								onClick={(e) => props.handleExportToExcel(e, 0)} // để tạm là 1 trong list optionExcels do những option khác chưa làm
							>
								{t("general.exportToExcel")}
							</Button>
						</ButtonGroup>
						<Popover
							open={openExcel}
							anchorEl={anchorRef?.current}
							transition
							disablePortal
							style={{ zIndex: 999 }}
						>
							<Paper>
								<ClickAwayListener onClickAway={() => { }}>
									<MenuList id="split-button-menu" autoFocusItem>
										{optionExcels.map((option, index) => (
											<MenuItem
												key={option}
												onClick={(event) =>
													props.handleExportToExcel(event, option.code)
												}
											>
												{option.name}
											</MenuItem>
										))}
									</MenuList>
								</ClickAwayListener>
							</Paper>
						</Popover>
					</>}
				</Grid>
				<Grid item md={6} sm={12} xs={12}>
					<FormControl fullWidth className='position-relative'>
						<Input
							className='search_box w-100'
							onChange={props.handleTextChange}
							onKeyDown={props.handleKeyDownEnterSearch}
							onKeyUp={props.handleKeyUpSearch}
							placeholder={t("asset_liquidate.search")}
							id="search_box"
							endAdornment={
								<InputAdornment position="end">
									<SearchIcon
										className="cursor-pointer"
										onClick={props.searchFilter}
									/>
								</InputAdornment>
							}
						/>
					</FormControl>
				</Grid>

				<Grid item xs={12}>
					<div>
						{isPrint && (
							<PrintMultipleFormDialog
								t={t}
								i18n={i18n}
								handleClose={props?.handleDialogClose}
								getTemplate={getTemplateDetail}
								open={isPrint}
								item={dataView}
								title={t("Phiếu thanh lý TSCĐ")}
								urls={[
									...LIQUIDATE.GENERAL,
									...(LIQUIDATE[currentOrg?.printCode] || []),
								]}
							/>
						)}

						{shouldOpenEditorDialog && (<AssetLiquidateDialog
							t={t} i18n={i18n}
							handleClose={props?.handleDialogClose}
							open={shouldOpenEditorDialog}
							handleOKEditClose={props?.handleOKEditClose}
							item={item}
							totalElements={totalElements}
							checkDelete={props?.checkDelete}
							isRoleManager={props.item?.isRoleManager}
							handleEdit={props?.handleEdit}
							type="reevaluate"
							getDsPhieuDanhGiaOfVoucher={props?.getDsPhieuDanhGiaOfVoucher}
							assetVoucherIds={props?.assetVoucherIds}
						/>)}

						{shouldOpenConfirmationDialog && (
							<ConfirmationDialog
								title={t("general.confirm")}
								open={shouldOpenConfirmationDialog}
								onConfirmDialogClose={props?.handleDialogClose}
								onYesClick={props?.handleConfirmationResponse}
								text={t('asset_liquidate.deleteConfirm')}
								agree={t('general.agree')}
								cancel={t('general.cancel')}
							/>
						)}
					</div>
					<div>
						<ComponentLiquidateFiltering
							item={props?.item}
							handleFilteringSelectChange={props?.handleFilteringSelectChange}
							handleFilteringHandoverDepartmentChange={props?.handleFilteringHandoverDepartmentChange}
							resetFilter={props.resetFilter}
							searchFilter={props?.searchFilter}
							handleDateChange={props?.handleDateChange}
							advancedSearch={advancedSearch}
						/>
						{openPrintQR && (
							<AssetsQRPrintNew
								t={t}
								i18n={i18n}
								handleClose={props.handleCloseQrCode}
								open={openPrintQR}
								items={products}
							/>
						)}
					</div>
					<MaterialTable
						title={t("general.list")}
						data={itemList}
						columns={props?.columns}
						localization={{
							body: {
								emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
							},
						}}
						options={{
							draggable: false,
							sorting: false,
							selection: false,
							actionsColumnIndex: -1,
							paging: false,
							search: false,
							rowStyle: (rowData) => ({
								backgroundColor: itemAsset?.id === rowData?.id
									? "#ccc"
									: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
							}),
							maxBodyHeight: "450px",
							minBodyHeight: "250px",
							headerStyle: {
								backgroundColor: "#358600",
								color: "#fff",
							},
							padding: "dense",
							toolbar: false,
						}}
						components={{
							Toolbar: (props) => <MTableToolbar {...props} />,
						}}
						onSelectionChange={(rows) => {
							this.data = rows;
						}}
						onSortChange={(columnId, direction) => {
							return [...itemList].sort((a, b) => {
								if (direction === 'asc') {
									return a[columnId] > b[columnId] ? 1 : -1;
								} else {
									return a[columnId] < b[columnId] ? 1 : -1;
								}
							});
						}}
						onRowClick={(e, rowData) => { return props?.setItemState(rowData) }}
					/>
					<TablePagination
						{...defaultPaginationProps()}
						rowsPerPageOptions={appConst.rowsPerPageOptions.table}
						count={totalElements}
						rowsPerPage={rowsPerPage}
						page={page}
						onPageChange={props.handleChangePage}
						onRowsPerPageChange={props.setRowsPerPage}
					/>
				</Grid>
			</Grid>
		</>
	)
}

export default ComponentLiquidateTable;
