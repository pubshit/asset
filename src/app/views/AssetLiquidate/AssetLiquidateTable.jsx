import {
  AppBar,
  Box,
  Checkbox,
  Icon,
  IconButton,
  Tab,
  Tabs,
} from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import {
  LightTooltip,
  TabPanel,
  checkInvalidDate,
  convertFromToDate,
  formatDateDto,
  formatDateDtoMore,
  formatDateTypeArray, handleKeyDown, handleKeyUp,
} from "app/appFunction";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { OPTIONS_EXCEL_QR_CARD_OBJECT, appConst, variable } from "../../appConst";
import AppContext from "../../appContext";
import {
  deleteItem,
  deleteList,
  exportToExcel,
  getByPage,
  getCountStatus,
  getItemById,
  getListQRCode,
} from "./AssetLiquidateService";
import ComponentLiquidateTable from "./ComponentLiquidateTable";
import { getByPageDsPhieuDanhGiaOfVoucher } from "../AssetReevaluate/AssetReevaluateService";
import ListAssetLiquidateTable from "./ListAssetLiquidateTable";
import appConfig from "../../appConfig";
toast.configure({
  autoClose: 8000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const isView =
    appConst.listLiquidateStatus[0].code === item?.liquidationStatus ||
    appConst.listLiquidateStatus[1].code === item?.liquidationStatus;

  return (
    <div className="none_wrap">
      {appConst.listLiquidateStatus[2].code !== item?.liquidationStatus && (
        <>
          <LightTooltip
            className="p-5"
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>

          <LightTooltip
            className="p-5"
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </>
      )}

      <LightTooltip
        className="p-5"
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>

      {!isView && (
        <LightTooltip
          className="p-5"
          title={t("general.viewIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <Icon fontSize="small" color="primary">
              visibility
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class AssetLiquidateTable extends React.Component {
  constructor(props) {
    super(props)
    this.anchorRef = React.createRef();
  }
  state = {
    keyword: "",
    createDateBottom: null,
    createDateTop: null,
    issueDateTop: null,
    issueDateBottom: null,
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetLiquidate: [],
    itemList: [],
    item: {},
    checkAll: false,
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    checkDelete: false,
    isRoleManager: false,
    tabValue: 0,
    statusLiquidattion: appConst.tabLiquidate.tabAll,
    statuses: [],
    assetVoucherIds: [],
    itemAsset: {},
    liquidationStatus: null,
    openPrintQR: false,
    products: [],
    advancedSearch: false,
    openExcel: false,
    handoverDepartment: null
  };

  handleToggleOpenExcel = (e) => {
    const { openExcel } = this.state;
    this.setState((prevState) => ({
      openExcel: !openExcel,
    }));
  };

  voucherType = ConstantList.VOUCHER_TYPE.Liquidate; //Thanh lý

  getDsPhieuDanhGiaOfVoucher = async (assetVoucherIds) => {
    try {
      let searchObject = {
        assetVoucherIds,
        pageIndex: 1,
        pageSize: 10,
        voucherType: 2,
        voucherId: this.state.item.id,
      };
      let res = await getByPageDsPhieuDanhGiaOfVoucher(searchObject);
      if (
        res?.data?.data?.content &&
        res?.status === appConst.CODE.SUCCESS &&
        res?.data?.code === appConst.CODE.SUCCESS
      ) {
        let listDataDgl = res?.data?.data?.content;
        let assetsUpdate = this.state.item.assetVouchers.map((item) => {
          let itemDgl = listDataDgl.find(
            (itemDgl) => itemDgl.assetId === item.assetId
          );
          if (!itemDgl?.isEvaluated) {
            item.isAllowedReevaluate = true;
          } else if (itemDgl?.dglInfo) {
            if (
              itemDgl?.dglInfo?.dglTrangthai ===
              appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder ||
              itemDgl?.dglInfo?.dglTrangthai ===
              appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder
            ) {
              item.isAllowedReevaluate = false;
            } else {
              item.isAllowedReevaluate = true;
            }
          }
          item.asset.assetVoucherId = itemDgl?.assetVoucherId;
          item.asset.voucherId = this.state.item.id;
          item.asset.voucherType = appConfig.VOUCHER_TYPE.Allocation;
          item.asset.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai;

          return item;
        });

        this.setState({
          item: {
            ...this.state.item,
            assetVouchers: assetsUpdate,
          },
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.assetVoucherIds !== prevState.assetVoucherIds) {
      this.getDsPhieuDanhGiaOfVoucher(this.state.assetVoucherIds);
    }
  }

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      AssetLiquidate: [],
      checkAll: false,
      tabValue: newValue,
      keyword: "",
      createDateBottom: null,
      createDateTop: null,
      issueDateTop: null,
      issueDateBottom: null,
    });
    if (appConst?.tabLiquidate?.tabAll === newValue) {
      this.setState(
        {
          statusLiquidattion: appConst.tabLiquidate.tabAll,
          statuses: [],
          liquidationStatus: null
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabLiquidate?.tabPlan === newValue) {
      this.setState(
        {
          statusLiquidattion: appConst.tabLiquidate.tabPlan,
          statuses: [appConst.listLiquidateStatus[0].code],
          liquidationStatus: appConst.listLiquidateStatus[0],
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabLiquidate?.tabIsLiquiDating === newValue) {
      this.setState(
        {
          statusLiquidattion: appConst.tabLiquidate.tabIsLiquiDating,
          statuses: [appConst.listLiquidateStatus[1].code],
          liquidationStatus: appConst.listLiquidateStatus[1],
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabLiquidate?.tabLiquidated === newValue) {
      this.setState(
        {
          statusLiquidattion: appConst.tabLiquidate.tabLiquidated,
          statuses: [appConst.listLiquidateStatus[2].code],
          liquidationStatus: appConst.listLiquidateStatus[2],
        },
        () => this.updatePageData()
      );
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getAllStatusCount = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countStatusPlan, countStatusIsLiquidating, countStatusIsLiquidated;
        let foundStatusPlan = data?.data?.find(
          (item) => appConst?.listLiquidateStatus[0].code === item?.status
        );
        let foundStatusIsLiquidating = data?.data?.find(
          (item) => appConst?.listLiquidateStatus[1].code === item?.status
        );
        let foundStatusIsLiquidated = data?.data?.find(
          (item) => appConst?.listLiquidateStatus[2].code === item?.status
        );

        if (foundStatusPlan) {
          countStatusPlan = this.checkCount(foundStatusPlan?.count);
        }
        if (foundStatusIsLiquidating) {
          countStatusIsLiquidating = this.checkCount(
            foundStatusIsLiquidating?.count
          );
        }
        if (foundStatusIsLiquidated) {
          countStatusIsLiquidated = this.checkCount(
            foundStatusIsLiquidated?.count
          );
        }
        this.setState(
          {
            countStatusPlan,
            countStatusIsLiquidating,
            countStatusIsLiquidated,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleTextChange = (event) => {
    this.setState({ tempKeyword: event.target.value });
  };

  handleDateChange = (event, source) => {
    let newDate =
      variable.listInputName.createDateBottom === source
        ? formatDateDtoMore(event, "start")
        : formatDateDtoMore(event, "end");
    this.setState({ [source]: newDate, itemAsset: {} }, () => {
      if (!checkInvalidDate(event)) {
        this.updatePageData();
      }
    });
  };

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  handleKeyUpSearch = e => handleKeyUp(e, this.search);

  setPage = (page) => {
    this.setState({ page, itemAsset: {} }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setState({keyword: this.state.tempKeyword}, () => {
      this.setPage(0);
    })
  }

  searchFilter = () => {
    if (this.validationFilter()) {
      this.setPage(0);
    }
  };

  validationFilter = () => {
    let { createDateBottom, createDateTop, issueDateBottom, issueDateTop } =
      this.state;

    if (
      checkInvalidDate(createDateBottom) ||
      checkInvalidDate(createDateTop) ||
      checkInvalidDate(issueDateBottom) ||
      checkInvalidDate(issueDateTop)
    ) {
      return false;
    }
    if (
      (createDateTop && createDateBottom && createDateBottom > createDateTop) ||
      (issueDateTop && issueDateBottom && issueDateBottom > issueDateTop)
    ) {
      return false;
    }
    return true;
  };

  resetFilter = () => {
    this.setState({
      createDateBottom: null,
      createDateTop: null,
      issueDateTop: null,
      issueDateBottom: null,
      statuses: [],
    });
  };

  handleFilteringSelectChange = (value) => {
    this.setState({
      statuses: [value?.code],
      liquidationStatus: value
    }, () => this.updatePageData());
  };
  handleFilteringHandoverDepartmentChange = (value) => {
    this.setState({
      handoverDepartment: value,
    }, () => this.updatePageData());
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { AssetLiquidate } = this.state;
    let { t } = this.props;
    const {
      page,
      rowsPerPage,
      keyword,
      statuses,
      createDateTop,
      createDateBottom,
      issueDateTop,
      issueDateBottom,
      handoverDepartment
    } = this.state;

    const searchObject = {
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      type: this.voucherType,
      keyword,
      statuses: statuses?.length > 0 ? statuses : null,
      createDateTop: convertFromToDate(createDateTop).toDate,
      createDateBottom: convertFromToDate(createDateBottom).fromDate,
      issueDateTop: convertFromToDate(issueDateTop).toDate,
      issueDateBottom: convertFromToDate(issueDateBottom).fromDate,
      handoverDepartmentId: handoverDepartment?.id,
    };
    setPageLoading(true);
    getByPage(searchObject)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code) {
          if (data?.data?.totalElements && data?.data?.content.length <= 0) {
            this.setPage(0);
          } else {
            let countItemChecked = 0
            const totalElements = data?.data?.totalElements || 0;
            const itemList = data?.data?.content?.map(item => {
              item.createDate = formatDateTypeArray(item?.createDate);
              if (AssetLiquidate?.find(checkedItem => checkedItem === item.id)) {
                countItemChecked++
              }
              return {
                ...item,
                checked: !!AssetLiquidate?.find(checkedItem => checkedItem === item.id),
              }
            }
            )

            this.setState({
              itemList,
              totalElements,
              ids: [],
              checkAll: itemList?.length === countItemChecked && itemList?.length > 0
            })
          }
        } else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch((err) => {
        toast.error(t("asset_liquidate.noti.dataError"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      isPrint: false,
    });
    this.getAllStatusCount();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
    });
    this.updatePageData();
    this.getAllStatusCount();
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code) {
          toast.success(t("general.deleteSuccess"));
          this.updatePageData();
        } else {
          toast.warning(data.message);
        }
        this.handleDialogClose();
      })
      .catch((error) => {
        toast(t("general.error"));
      });
  };

  componentDidMount() {
    this.updatePageData();
    this.getAllStatusCount();
    this.getRoleCurrentUser();
  }

  getRoleCurrentUser = () => {
    let { isRoleManager } = this.state;
    let currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    let isRoleAdmin = currentUser?.roles?.some(
      (role) =>
        role.name === ConstantList.ROLES.ROLE_ORG_ADMIN ||
        role.name === ConstantList.ROLES.ROLE_ADMIN
    );
    if (isRoleAdmin) {
      return;
    }
    isRoleManager = currentUser?.roles?.some(
      (role) => role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER
    );

    this.setState({ isRoleManager });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleCheck = (event, item) => {
    let { AssetLiquidate, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        AssetLiquidate.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = AssetLiquidate.filter(item => !itemListIds.includes(item));

        AssetLiquidate = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, AssetLiquidate });
    }
    else {
      let existItem = AssetLiquidate.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        AssetLiquidate.push(event.target.name) :
        AssetLiquidate.map((item, index) =>
          item === existItem ?
            AssetLiquidate.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: AssetLiquidate,
        AssetLiquidate,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  handleSelectAllClick = (event) => {
    let { AssetLiquidate, itemList } = this.state;

    itemList.map((item) => {
      item.checked = event.target.checked;
      AssetLiquidate.push(item.id);
      return item;
    });
    if (!event.target.checked) {
      AssetLiquidate = [];
    }
    this.setState({ checkAll: event.target.checked, AssetLiquidate });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEdit = (id) => {
    let arrAssetVoucherIds = [];
    getItemById(id)
      .then(({ data }) => {
        let liquidate = data?.data ? this.formatDataDto(data?.data) : null;
        if (liquidate?.assetVouchers?.length > 0) {
          liquidate.assetVouchers.map((item) =>
            arrAssetVoucherIds.push(item?.assetId)
          );
        }
        this.setState({
          checkDelete: true,
          item: liquidate,
          shouldOpenEditorDialog: true,
          assetVoucherIds: arrAssetVoucherIds,
        });
      })
      .catch((err) => {
        toast.error("Xảy ra lỗi, không thể lấy thông tin phiếu");
      });
  };

  handleView = (id) => {
    getItemById(id).then(({ data }) => {
      let liquidate = data?.data ? this.formatDataDto(data?.data) : null;
      liquidate.isView = true;
      this.setState({
        item: liquidate,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handlePrint = async (id) => {
    try {
      let result = await getItemById(id);
      if (result?.data?.code === appConst.CODE.SUCCESS && result?.data?.data) {
        let liquidate = result?.data?.data
          ? this.formatDataDto(result?.data?.data)
          : null;
        this.setState({
          item: liquidate,
          isPrint: true,
        });
      }
    } catch (error) { }
  };

  handleDeleteAllItem = (id) => {
    let { AssetLiquidate } = this.state;
    let { t } = this.props;
    if (AssetLiquidate.length === 0) {
      toast.warning(t("general.emptyDataSelected"));
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };

  async handleDeleteList(list) {
    let { t } = this.props;
    let errList = [];
    deleteList(list)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code) {
          toast.success(t("general.deleteSuccess"));
        } else {
          toast.warning(data.message);
        }
      })
      .catch((err) => errList.push(err));
    if (errList.length !== 0) {
      toast.error(t("asset_liquidate.noti.deleteAllError"));
    }
  }

  handleDeleteAll = () => {
    let { AssetLiquidate } = this.state;
    this.handleDeleteList(AssetLiquidate).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  formatDataDto = (liquidate) => {
    let personIds = [];
    let listAssetDocumentId = [];

    liquidate.createDate = formatDateTypeArray(liquidate?.createDate);
    liquidate.handoverDepartment = {
      id: liquidate?.handoverDepartmentId,
      code: liquidate?.handoverDepartmentCode,
      name: liquidate?.handoverDepartmentName,
    };

    liquidate.handoverPerson = {
      handoverPersonId: liquidate?.handoverPersonId,
      personDisplayName: liquidate?.handoverPersonName,
    };

    liquidate.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse,
        originalCost: item?.assetOriginalCost,
        carryingAmount: item?.assetCarryingAmount,
        managementCode: item?.assetManagementCode,
        yearOfManufacture: item?.assetYearOfManufacture,
        madeIn: item?.assetMadeIn,
      };
      item.asset = data;
      return item;
    });

    liquidate?.persons?.map((item) => personIds?.push(item?.id));
    liquidate.personIds = personIds;

    liquidate?.documents?.map((item) => listAssetDocumentId?.push(item?.id));
    liquidate.listAssetDocumentId = listAssetDocumentId;
    return liquidate;
  };

  checkStatus = (status) =>
    appConst.listLiquidateStatus.find((item) => item.code === status)?.name;
  handleSetItemState = async (rowData) => {
    this.setState({
      itemAsset: rowData,
    });
  };

  handleExportToExcel = async (e, type) => {
    const { setPageLoading } = this.context;
    setPageLoading(true);
    const { t } = this.props;
    try {
      let {
        issueDateBottom,
        issueDateTop,
        liquidationStatus,
        createDateBottom,
        createDateTop,
        handoverDepartment,
        statuses
      } = this.state
      let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
      let searchObject = {}
      searchObject.createDateBottom = createDateBottom ? formatDateDtoMore(createDateBottom, "start") : "";
      searchObject.createDateTop = createDateTop ? formatDateDtoMore(createDateTop) : "";
      searchObject.fromDate = issueDateBottom ? formatDateDtoMore(issueDateBottom, "start") : "";
      searchObject.toDate = issueDateTop ? formatDateDtoMore(issueDateTop) : "";
      searchObject.departmentId = handoverDepartment?.id;
      searchObject.statuses = statuses?.length > 0 ? statuses : null;
      searchObject.orgId = currentUser?.org?.id;
      searchObject.assetClass = appConst.assetClass.TSCD;
      searchObject.type = ConstantList.VOUCHER_TYPE.Liquidate;
      searchObject.liquidationStatus = liquidationStatus?.indexOrder;
      searchObject.ids = this.state?.AssetLiquidate;
      const exportToExcelService = type === OPTIONS_EXCEL_QR_CARD_OBJECT.TOAN_BO.code ?
        exportToExcel : type === OPTIONS_EXCEL_QR_CARD_OBJECT.THE_QR.code ?
          () => { } : () => { }
      const res = await exportToExcelService(searchObject);
      try {
        // Kiểm tra nếu data là JSON
        const data = JSON.parse(res?.data);
        toast.warning(data.message);
      } catch (e) {
        // Nếu call api thành công data ở dạng blob chỉ có thể lưu
        if (res?.data) {
          const blob = new Blob([res.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(blob, "AssetLiquidateTable.xlsx");
          toast.success(t("general.successExport"));
        } else {
          toast.error(t("general.errorExport"));
        }
      } finally {
        setPageLoading(false);
      }
    } catch (error) {
      setPageLoading(false);
      toast.error(t("general.errorExport"));
    }
  }
  handlePrintQRCode = async (id) => {
    let { t } = this.props
    try {
      let res = await getListQRCode(id);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          products: res?.data?.data?.length > 0 ? [...res?.data?.data] : [],
          openPrintQR: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }
  handleOpenAdvanceSearch = () => {
    this.setState({ advancedSearch: !this.state.advancedSearch })
  }
  render() {
    const { t, i18n } = this.props;
    let { rowsPerPage, page, checkAll, tabValue } = this.state;
    let TitlePage = t("Asset.asset_liquidate");
    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        field: "custom",
        align: "left",
        width: "60px",
        headerStyle: {},
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },

        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            checked={rowData.checked}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        width: "80px",
        align: "left",
        headerStyle: {},
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              switch (method) {
                case appConst.active.edit:
                  this.handleEdit(rowData.id);
                  break;
                case appConst.active.delete:
                  this.handleDelete(rowData.id);
                  break;
                case appConst.active.view: //view
                  this.handleView(rowData.id);
                  break;
                case appConst.active.print:
                  this.handlePrint(rowData.id);
                  break;
                case appConst.active.qrcode:
                  this.handlePrintQRCode(rowData.id);
                  break;
                default:
                  alert("Call Selected Here: " + rowData.id);
                  break;
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        width: 50,
        align: "left",
        headerStyle: {},
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("asset_liquidate.createDate"),
        width: "200",
        align: "left",
        headerStyle: {},
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.createDate
            ? moment(rowData?.createDate).format("DD/MM/YYYY")
            : "",
      },
      {
        title: t("asset_liquidate.liquidateNumber"),
        field: "code",
        width: "200",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("TransferToAnotherUnit.status"),
        field: "code",
        align: "left",
        headerStyle: {
          minWidth: "200px",
        },
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.liquidationStatus;
          let className = "";
          if (
            statusIndex === appConst.listLiquidateStatus[2].code
          ) {
            className = "status status-error";
          } else if (
            statusIndex === appConst.listLiquidateStatus[1].code
          ) {
            className = "status status-warning";
          } else if (
            statusIndex === appConst.listLiquidateStatus[0].code
          ) {
            className = "status status-success";
          } else {
            className = "";
          }
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("asset_liquidate.liquidationDate"),
        field: "issueDate",
        width: "150",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.issueDate
            ? moment(rowData?.issueDate).format("DD/MM/YYYY")
            : "",
      },
      {
        title: t("asset_liquidate.name"),
        field: "name",
        width: "200",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("asset_liquidate.handoverDepartment"),
        field: "handoverDepartmentName",
        align: "left",
        minWidth: "220px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "/asset/asset_liquidate",
              },
              { name: t("Asset.asset_liquidate") },
            ]}
          />
        </div>

        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("asset_liquidate.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.plan")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusPlan || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.isLiquidating")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusIsLiquidating || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("asset_liquidate.liquidated")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countStatusIsLiquidated || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst?.tabLiquidate?.tabAll}
          className="mp-0"
        >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            handleFilteringHandoverDepartmentChange={this.handleFilteringHandoverDepartmentChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleEdit={this.handleEdit}
            assetVoucherIds={this.state.assetVoucherIds}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetLiquidateTable
            itemAsset={this.state.itemAsset}
            {...this.props}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabLiquidate?.tabPlan}
          className="mp-0"
        >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            handleFilteringHandoverDepartmentChange={this.handleFilteringHandoverDepartmentChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEdit={this.handleEdit}
            assetVoucherIds={this.state.assetVoucherIds}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetLiquidateTable
            itemAsset={this.state.itemAsset}
            {...this.props}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabLiquidate?.tabIsLiquiDating}
          className="mp-0"
        >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            handleFilteringHandoverDepartmentChange={this.handleFilteringHandoverDepartmentChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEdit={this.handleEdit}
            assetVoucherIds={this.state.assetVoucherIds}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetLiquidateTable
            itemAsset={this.state.itemAsset}
            {...this.props}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabLiquidate?.tabLiquidated}
          className="mp-0"
        >
          <ComponentLiquidateTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            handleDeleteAllItem={this.handleDeleteAllItem}
            handleFilteringSelectChange={this.handleFilteringSelectChange}
            handleFilteringHandoverDepartmentChange={this.handleFilteringHandoverDepartmentChange}
            resetFilter={this.resetFilter}
            searchFilter={this.searchFilter}
            handleDateChange={this.handleDateChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUpSearch={this.handleKeyUpSearch}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEdit={this.handleEdit}
            assetVoucherIds={this.state.assetVoucherIds}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            handleExportToExcel={this.handleExportToExcel}
            handleCloseQrCode={this.handleCloseQrCode}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
          <ListAssetLiquidateTable
            itemAsset={this.state.itemAsset}
            {...this.props}
          />
        </TabPanel>
      </div>
    );
  }
}

AssetLiquidateTable.contextType = AppContext;
export default AssetLiquidateTable;
