import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid, Radio } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from "material-table";
import VoucherFilePopup from "./VoucherFilePopup";
import { createFilterOptions } from "@material-ui/lab/Autocomplete";
import { LISTLIQUIDATE, appConst } from "../../appConst";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import viLocale from "date-fns/locale/vi";
import {
  a11yProps,
  convertNumberPriceRoundUp,
  filterOptions,
  LightTooltip,
  TabPanel,
  NumberFormatCustom
} from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import moment from "moment/moment";
import AssetReevaluateDialog from "../AssetReevaluate/AssetReevaluateDialog";
import { searchByPage } from "../Council/CouncilService";
import { Label } from "../Component/Utilities";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function LiquidateScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const {createDate} = props.item;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [selectedValue, setSelectedValue] = useState(null);
  const [listLiqui, setListLiqui] = useState(props?.item?.listLiqui || null);
  const [dataTableLiqui, setDataTableLiqui] = useState(
    props?.item?.persons || []
  );
  const [listLiquidationBoard, setListLiquidationBoard] = useState([]);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  let selectliquidationStatus = appConst.listLiquidateStatus.find(
    (status) => status.code === props?.item?.liquidationStatus
  ) || "";
  const isLiquidated = props?.item?.liquidationStatus === appConst.listLiquidateStatusObject.LIQUIDATED.code
    || selectliquidationStatus.indexOrder === appConst.listLiquidateStatusObject.LIQUIDATED.code;
  const filterAutocomplete = createFilterOptions();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let searchObject = {
    keyword: "",
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    type: appConst.OBJECT_HD_TYPE.THANH_LY.code,
    isActive: appConst.STATUS_HOI_DONG.HOAT_DONG.code
  };

  const handleClick = async (event, item) => {
    if (item?.asset?.id === selectedValue) {
      setSelectedValue(null)
    } else {
      setSelectedValue(item?.asset?.id);
    }
    props.setAssetReevaluate(item);
  };

  const handleRowDataCellDelete = (item) => {
    let newDataRow = dataTableLiqui.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListLiquidateDepartmentUserList(newDataRow);
    setDataTableLiqui(newDataRow);
  };

  const handleChangeSelectLiquidation = (item) => {
    setListLiqui(item);
    props.handleSetListLiquidateDepartmentUserList(item?.hdChiTiets);
    props.handleSetHoiDongId(item?.id);

    item?.hdChiTiets?.map((chitiet) => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName,
      };
      return chitiet;
    });
    setDataTableLiqui(item?.hdChiTiets || []);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? ''
    });
  }, [props?.item?.handoverDepartment?.id]);

  let columns = [
    ...(!props?.disabledDgl &&
      (selectliquidationStatus.indexOrder === LISTLIQUIDATE.NEW.indexOrder ||
        selectliquidationStatus.indexOrder ===
        LISTLIQUIDATE.PROCESSING.indexOrder) ?
      [{
        title: t("general.reevaluateAcronyms"),
        field: "custom",
        align: "left",
        width: "45px",
        render: (rowData) => {
          return (
            rowData?.isAllowedReevaluate && (
              <Radio
                id={`radio${rowData.id}`}
                style={{ padding: "0px" }}
                name="radSelected"
                value={rowData?.assetId}
                checked={selectedValue === rowData?.assetId}
                onClick={(event) => handleClick(event, rowData)}
              />
            )
          );
        },
      }] : []),
    {
      title: t("Asset.action"),
      field: "custom",
      align: "center",
      width: "250",
      headerStyle: {
        whiteSpace: "nowrap",
      },
      render: (rowData) =>
        !props?.item?.isView && (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (appConst.active.delete === method) {
                props.removeAssetInlist(rowData?.asset?.id);
              } else {
                alert("Call Selected Here:" + rowData?.id);
              }
            }}
          />
        ),
    },
    {
      title: t("Asset.stt"),
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props?.item?.page * props?.item?.rowsPerPage +
        (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      minWidth: 120,
      align: "right",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.managementCode || rowData?.assetManagementCode
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetSerialNumber || rowData?.asset?.serialNumber
    },
    {
      title: t("Asset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetModel || rowData?.asset?.model
    },
    {
      title: t("Asset.dateOfReception"),
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return rowData?.asset?.dateOfReception
          ? moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")
          : rowData?.dateOfReception ? moment(rowData?.dateOfReception).format("DD/MM/YYYY") : ""
      }
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "asset.yearPutIntoUse",
      align: "left",
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetYearPutIntoUse || rowData?.asset?.yearPutIntoUse
    },
    {
      title: t("Asset.statusDgl"),
      field: "asset.dglTrangthai",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return appConst.statusAssetReevaluate.find(
          (item) => item.id === rowData?.asset?.dglTrangthai
        )?.name;
      },
    },
    {
      title: t("TransferToAnotherUnit.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        convertNumberPriceRoundUp(rowData?.asset?.originalCost),
    },
    {
      title: t("TransferToAnotherUnit.carryingAmount"),
      field: "asset.carryingAmount",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        convertNumberPriceRoundUp(rowData?.asset?.carryingAmount),
    },
    {
      title: t("Asset.useDepartment"),
      field: "assetUseDepartmentName",
      align: "left",
      minWidth: 150,
      render: (rowData) => rowData?.assetUseDepartmentName || rowData?.asset?.useDepartmentName
    },
    {
      title: t("Asset.store"),
      field: "assetStoreName",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.assetStoreName || rowData?.asset?.storeName
    },
  ]

  let columnsVoucherFile = [
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => 
      (
        <div className="none_wrap">
          {!props.item.isView && (
            <>
              <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
                <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
                  <Icon fontSize="small" color="primary">edit</Icon>
                </IconButton>
              </LightTooltip>
              <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
                <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
                  <Icon fontSize="small" color="error">delete</Icon>
                </IconButton>
              </LightTooltip>
            </>
          )}
          {props.item.isView && (
            <LightTooltip title={t('general.viewIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">visibility</Icon>
              </IconButton>
            </LightTooltip>
          )}
        </div>
      )
    },
    {
      title: t("general.stt"),
      field: "code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1 || '',
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 350,
      cellStyle: {
        textAlign: "left",
      },
    },
  ];

  let columnPerson = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("general.action"),
      field: "",
      align: "center",
      maxWidth: 100,
      hidden: isLiquidated || props?.item?.isView,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <div className="none_wrap">
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => handleRowDataCellDelete(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
    {
      title: t("asset_liquidate.persons"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) =>
        appConst.listHdChiTietsRole.find((item) => item?.code === rowData?.role)
          ?.name,
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("general.seacrhByProductName")} {...a11yProps(0)} />
          <Tab label={t("asset_liquidate.liquidation_board")}{...a11yProps(1)} />
          <Tab label={t("general.tabDocument")} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={12} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                onChange={() => { }}
                fullWidth
                readOnly={true}
                disabled
                margin="none"
                id="mui-pickers-date"
                label={t("asset_liquidate.createDate")}
                format="dd/MM/yyyy"
                value={createDate ? moment(createDate) : new Date()}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                  style: { display: 'none' },
                }}
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={2} sm={12} xs={12}>
            <ValidatePicker
              fullWidth
              margin="none"
              label={
                <Label isRequired={isLiquidated}>{t("asset_liquidate.liquidationDate")}</Label>
              }
              KeyboardButtonProps={{ "aria-label": "change date" }}
              autoOk
              clearable
              format="dd/MM/yyyy"
              name={"issueDate"}
              value={props.item?.issueDate || null}
              onChange={(date) => props.handleDateChange(date, "issueDate")}
              readOnly={props?.item?.isView}
              invalidDateMessage={t("general.invalidDateFormat")}
              maxDate={new Date()}
              maxDateMessage={t("allocation_asset.maxDateMessage")}
              validators={isLiquidated ? ["required"] : []}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={
                <Label isRequired>{t("asset_liquidate.name")}</Label>
              }
              name="name"
              value={props.item.name || ""}
              onChange={props?.handleChange}
              InputProps={{ readOnly: props?.item?.isView }}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired>{t("TransferToAnotherUnit.status")}</Label>
              }
              searchFunction={() => { }}
              listData={appConst.listLiquidateStatus}
              setListData={props.handleSetDataHandoverPerson}
              displayLable={"name"}
              selectedOptionKey="indexOrder"
              value={selectliquidationStatus || null}
              onSelect={props.handleChangeSelect}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              readOnly={props?.item?.isView}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12}>
            <TextValidator
              fullWidth
              disabled
              label={t("asset_liquidate.number")}
              value={props.item.code || ""}
            />
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            <TextValidator
              fullWidth
              name="decisionCode"
              label={
                <span>
                  {[
                    appConst.listLiquidateStatusObject.PROCESSING.indexOrder,
                    appConst.listLiquidateStatusObject.LIQUIDATED.indexOrder
                  ].includes(selectliquidationStatus.indexOrder) && (
                      <span className="colorRed">*</span>
                    )}
                  <span>{t("asset_liquidate.decisionCode")}</span>
                </span>
              }
              onChange={props?.handleChange}
              value={props.item.decisionCode || ""}
              InputProps={{ readOnly: props?.item?.isView }}
              {...([
                appConst.listLiquidateStatusObject.PROCESSING.indexOrder,
                appConst.listLiquidateStatusObject.LIQUIDATED.indexOrder
              ].includes(selectliquidationStatus.indexOrder)
                ? {
                  validators: ["required"],
                  errorMessages: [t("general.required")],
                }
                : {})}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired> {t("allocation_asset.handoverDepartment")}</Label>
              }
              searchFunction={getListOrgManagementDepartment}
              searchObject={props.handoverPersonSearchObject}
              listData={props.item.listHandoverDepartment}
              setListData={props.handleSetDataHandoverDepartment}
              displayLable={"name"}
              readOnly={
                props.item?.isRoleManager
                || props.item?.isView
                || props.item?.isInventoryLiquidateAsset
              }
              isNoRenderParent
              value={props.item?.handoverDepartment || null}
              onSelect={props.selectHandoverDepartment}
              typeReturnFunction="list"
              showCode={"showCode"}
              filterOptions={filterOptions}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              noOptionsText={t("general.noOption")}
              disabled={props.item?.isRoleAssetManager}
              isNoRenderChildren
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("asset_liquidate.handoverPerson")}
                </span>
              }
              disabled={!props?.item?.handoverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={{
                ...searchParamUserByHandoverDepartment,
                pageSize: 1000,
                pageIndex: 1
              }}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.handoverPerson ?? ''}
              onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("asset_liquidate.liquidationCost")}
              name="liquidationCost"
              id="formatted-numberformat-originalCost"
              value={props.item?.liquidationCost || ""}
              onChange={props.handleChange}
              InputProps={{
                readOnly: props?.item?.isView,
                inputComponent: NumberFormatCustom,
              }}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("asset_liquidate.recoveryValue")}
              name="recoveryValue"
              id="formatted-numberformat-originalCost"
              value={props.item?.recoveryValue || ""}
              onChange={props.handleChange}
              InputProps={{
                readOnly: props?.item?.isView,
                inputComponent: NumberFormatCustom,
              }}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("asset_liquidate.address")}
                </span>
              }
              onChange={props.handleChange}
              type="text"
              name="transferAddress"
              value={props.item.transferAddress || ""}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={t("asset_liquidate.conclude")}
              onChange={props.handleChange}
              type="text"
              name="conclude"
              value={props.item.conclude || ""}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              fullWidth
              className="w-100 "
              label={t("asset_liquidate.reason")}
              onChange={props.handleChange}
              type="text"
              name="reason"
              value={props.item.reason || ""}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>

          <Grid item md={2} sm={12} xs={12}>
            <Button
              variant="contained"
              color="primary"
              className="mt-16 w-100"
              size="small"
              onClick={props.openPopupSelectAsset}
              disabled={props?.item?.isView}
            >
              {t("general.select_asset")}
            </Button>

            {props.item.handoverDepartment &&
              props.item.shouldOpenAssetPopup && (
                <SelectAssetAllPopup
                  t={t}
                  i18n={i18n}
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAsset}
                  isAssetTransfer={props.item.isAssetTransfer}
                  statusIndexOrders={props.item.statusIndexOrders}
                  handleClose={props.handleAssetPopupClose}
                  handoverDepartment={props.item?.handoverDepartment}
                  assetVouchers={
                    props.item.assetVouchers ? props.item.assetVouchers : []
                  }
                  liquidation={true}
                />
              )}
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={
                props.item.assetVouchers ? [...props.item.assetVouchers] : []
              }
              columns={columns}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                scrollButtons: true,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: "dense",
                maxBodyHeight: "450px",
                minBodyHeight: "250px",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction="category"
            searchObject={searchObject}
            label={t("asset_liquidate.liquidation_board")}
            listData={listLiquidationBoard}
            displayLable={"name"}
            onSelect={handleChangeSelectLiquidation}
            value={listLiqui}
            InputProps={{
              readOnly: true,
            }}
            disabled={
              props?.item?.isView
            }
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              let filtered = filterAutocomplete(options, params);
              return filtered;
            }}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="pt-16">
          <MaterialTable
            data={dataTableLiqui || []}
            columns={columnPerson}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Grid item md={12} sm={12} xs={12}>
          {!props?.item?.isView && (
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          )}

          {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              updatePageAssetDocument={props.updatePageAssetDocument}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocument={props.getAssetDocument}
              item={props.item}
              t={t}
              i18n={i18n}
            />
          )}
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item?.documents || []}
            columns={columnsVoucherFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      {props?.shouldOpenAssetReevaluateDialog && (
        <AssetReevaluateDialog
          t={t}
          handleClose={() => props?.handleAssetReevaluateDialogClose()}
          open={props?.shouldOpenAssetReevaluateDialog}
          item={props?.assetReevaluate}
          updateData={props?.handleEdit}
          assetVouchers={props?.assetVouchers}
          idItem={props?.id}
          getDsPhieuDanhGiaOfVoucher={props?.getDsPhieuDanhGiaOfVoucher}
          assetVoucherIds={props?.assetVoucherIds}
          type="hasSelectedAsset"
        />
      )}
    </div>
  );
}
