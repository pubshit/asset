import React, { Component } from "react";
import moment from "moment";
import {
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination,
  TableContainer,
  Button,
  Card,
  Checkbox,
  TableSortLabel
} from "@material-ui/core";
import { getAllEQASerumBanks, deleteEQASerumBank, getByPage, getById } from "./EQASerumBankService";
import EQASerumBankEditorDialog from "./EQASerumBankEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import shortid from "shortid";
import { saveAs } from 'file-saver';
class EQASerumBankTable extends Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    eQASerumBankList: [],
    item:{},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem:false,
    selectedList:[],
    totalElements:0,
    shouldOpenConfirmationDeleteAllDialog:false
  };
  numSelected =0;
  rowCount=0;
  
  setPage = page => {
    this.setState({ page },function(){
      this.updatePageData();
     })
  };

  setRowsPerPage = event => {
   this.setState({ rowsPerPage: event.target.value, page:0},function(){
    this.updatePageData();
   })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
      let searchDto = {};
      searchDto.pageIndex = this.state.page;
      searchDto.pageSize = this.state.rowsPerPage;

    getByPage(searchDto).then(({ data }) => this.setState({
      eQASerumBankList: [...data.content],selectAllItem:false, totalElements:data.totalElements
      }));
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "hello world.txt");    
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog:false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleDeleteEQASerumBank = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditEQASerumBank = item => {
    getById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteEQASerumBank(this.state.id).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  componentDidMount() {
    this.updatePageData();
  }

 
  handleClick = (event, item) => {
    let {eQASerumBankList} =  this.state;
    if(item.checked==null){
      item.checked=true;
    }else {
      item.checked=!item.checked;
    }
    var selectAllItem=true;
    for(var i=0;i<eQASerumBankList.length;i++){
      if(eQASerumBankList[i].checked==null || eQASerumBankList[i].checked==false){
        selectAllItem=false;
      }
      if(eQASerumBankList[i].id==item.id){
        eQASerumBankList[i]=item;
      }
    }
    this.setState({selectAllItem:selectAllItem, eQASerumBankList:eQASerumBankList});
    
  };
   handleSelectAllClick = (event) => {
    let {eQASerumBankList} =  this.state;
    for(var i=0;i<eQASerumBankList.length;i++){
      eQASerumBankList[i].checked=!this.state.selectAllItem;
    }
     this.setState({selectAllItem:!this.state.selectAllItem, eQASerumBankList:eQASerumBankList});
  };

  async handleDeleteList(list){
    for(var i=0;i<list.length;i++){
      if(list[i].checked){
        await deleteEQASerumBank(list[i].id);
      }
    }    
  }
  handleDeleteAll = (event) => {
    let {eQASerumBankList} =  this.state;
    this.handleDeleteList(eQASerumBankList).then(()=>{  
      this.updatePageData();
      this.handleDialogClose();
    }
    );
  };

  render() {
    const { t } = this.props;
    let {
      rowsPerPage,
      page,
      eQASerumBankList,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog
    } = this.state;

    return (
      <div className="m-sm-30">
        
        <div  className="mb-sm-30">
          <Breadcrumb routeSegments={[{ name: t('EQASerumBankTable.title') }]} />
        </div>

        <Button
          className="mb-16 mr-16"
          variant="contained"
          color="primary"
          onClick={() => this.setState({ shouldOpenEditorDialog: true,item:{} })}
        >
          {t('button.add')}
        </Button>
        <Button
          className="mb-16 mr-16"
          variant="contained"
          color="primary"
          onClick={() => this.setState({ shouldOpenConfirmationDeleteAllDialog: true})}
        >
          {t('Delete')}
        </Button>
        <TableContainer>
          <Table stickyHeader className="crud-table" style={{ whiteSpace: "pre", minWidth: "750px" }}>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={this.state.selectAllItem}
                    onChange={this.handleSelectAllClick}
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                </TableCell>                
                <TableCell>{t('EQASerumBankTable.dialog.serum_code')}</TableCell>
                <TableCell>{t('EQASerumBankTable.dialog.original_code')}</TableCell>
                <TableCell>{t('Name')}</TableCell>
                <TableCell>{t('EQASerumBankTable.dialog.sampled_date')}</TableCell>
                <TableCell>{t('EQASerumBankTable.dialog.receive_date')}</TableCell>
                <TableCell>{t('Status')}</TableCell>
                <TableCell>{t('Action')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {eQASerumBankList
                .map((item, index) => {
                  return(
                  <TableRow key={shortid.generate()}>
                    <TableCell padding="checkbox">
                      <Checkbox onClick={(event) => this.handleClick(event, item)}
                        checked={item.checked}
                      />
                    </TableCell>
                    <TableCell className="" align="left">{item.serumCode}</TableCell>
                    <TableCell className="" align="left">{item.originalCode}</TableCell>
                    <TableCell className="">{item.name}</TableCell>
                    <TableCell className="" align="left" type="date">{moment(item.sampledDate).format('DD/MM/YYYY')}</TableCell>
                    <TableCell className="" align="left" type="date">{moment(item.receiveDate).format('DD/MM/YYYY')}</TableCell>
                    <TableCell className="">
                      {item.checked ? (
                        <small className="border-radius-4 bg-primary text-white px-8 py-2 ">
                          active
                        </small>
                      ) : (
                        <small className="border-radius-4 bg-light-gray px-8 py-2 ">
                          inactive
                        </small>
                      )}
                    </TableCell>
                    <TableCell className="px-0 border-none">
                      <IconButton
                        onClick={() =>
                          this.handleEditEQASerumBank(item)
                        }
                      >
                        <Icon color="primary">edit</Icon>
                      </IconButton>
                      <IconButton onClick={() => this.handleDeleteEQASerumBank(item.id)}>
                        <Icon color="error">delete</Icon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                  )})}
            </TableBody>
          </Table>
          </TableContainer>
          <TablePagination
            className="px-16"
            rowsPerPageOptions={[1,2,5, 10, 25]}
            component="div"
            count={this.state.totalElements}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              "aria-label": "Previous Page"
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page"
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.setRowsPerPage}
          />

          {shouldOpenEditorDialog && (
            <EQASerumBankEditorDialog
              handleClose={this.handleDialogClose}
              handleOKEditClose={this.handleOKEditClose}
              open={shouldOpenEditorDialog}
              item={this.state.item}
              t={t}
            />
          )}
          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleConfirmationResponse}
              text="Are you sure to delete?"
            />
          )}

          {shouldOpenConfirmationDeleteAllDialog && (
            <ConfirmationDialog
              open={shouldOpenConfirmationDeleteAllDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleDeleteAll}
              text="Are you sure to delete all?"
            />
          )}          
        
      </div>
    );
  }
}

export default EQASerumBankTable;
