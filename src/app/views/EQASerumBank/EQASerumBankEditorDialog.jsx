import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkCode, addNewEQASerumBank, updateEQASerumBank } from "./EQASerumBankService";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
class EQASerumBankEditorDialog extends Component {
  state = {
    name: "",
    serumCode: "",//Mã ngân hàng huyết thanh
    originalCode: "",//Mã nguyên bản
    type: 0,//Loại mẫu:máu, huyết thanh, huyết tương, khác...; giá trị: PIConst.SerumType
    originalVolume: 0,//Dung tích ban đầu
    presentVolume: 0,//Dung tích hiện thời 
    quality: 0,//Chất lượng mẫu; giá trị: PIConst.SerumQuality
    hepatitisBStatus: 0,//Tình trạng nhiễm viêm gan B, âm tính hay dương tính, giá trị: PIConst.SampleStatus
    hepatitisCStatus: 0,//Tình trạng nhiễm viêm gan C, âm tính hay dương tính, giá trị: PIConst.SampleStatus
    sampledDate: new Date(),//Ngày lấy mẫu
    receiveDate: new Date(),//Ngày nhận mẫu
    storeLocation: "",
    isActive: false
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSampledDateChange = sampledDate => {
    this.setState({ sampledDate });
  };

  handleReceiveDateChange = receiveDate => {
    this.setState({ receiveDate });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { originalCode } = this.state;

    checkCode(id, originalCode).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        alert("Mã nguyên bản đã được sử dụng");
      } else {
        //Nếu trả về false là code chưa sử dụng có thể dùng
        if (id) {
          updateEQASerumBank({
            ...this.state
          }).then(() => {
            this.props.handleOKEditClose();
          });
        } else {
          addNewEQASerumBank({
            ...this.state
          }).then(() => {
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentWillMount() {
    //getUserById(this.props.uid).then(data => this.setState({ ...data.data }));
    let { open, handleClose, item } = this.props;
    this.setState(item);
  }

  render() {
    let {
      id,
      name,
      serumCode,
      originalCode,
      labCode,
      type,
      originalVolume,
      presentVolume,
      hivStatus,
      numberBottle,
      hasLipit,
      hemolysis,
      hasHighSpeedCentrifugal,
      dialysis,
      inactivated,
      storeLocation,
      sampledDate,
      receiveDate,
    } = this.state;

    let { t, open, handleClose, handleOKEditClose } = this.props;
    return (
      <Dialog  open={open} maxWidth={"md"}>
        <div className="p-24">
          <h4 className="mb-20">{t("SaveUpdate")}</h4>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="mb-16" container spacing={4}>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.originalCode")}
                  onChange={this.handleChange}
                  type="text"
                  name="originalCode"
                  value={originalCode}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.labCode")}
                  onChange={this.handleChange}
                  type="text"
                  name="labCode"
                  value={labCode}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.name")}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="type">{t("EQASerumBank.type.title")}</InputLabel>
                  <Select
                    name="type"
                    value={type}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="type" />}
                  >
                    <MenuItem value={1}>{t("EQASerumBank.type.Serum")}</MenuItem>
                    <MenuItem value={0}>{t("EQASerumBank.type.Plasma")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.originalVolume")}
                  onChange={this.handleChange}
                  type="number"
                  name="originalVolume"
                  value={originalVolume}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.presentVolume")}
                  onChange={this.handleChange}
                  type="number"
                  name="presentVolume"
                  value={presentVolume}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="result">{t("EQASerumBank.hivStatus")}</InputLabel>
                  <Select
                    name="hivStatus"
                    value={hivStatus}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="hivStatus" />}
                  >
                    <MenuItem value={1}>{t("SampleManagement.sample-list.Result.positive")}</MenuItem>
                    <MenuItem value={0}>{t("SampleManagement.sample-list.Result.indertermine")}</MenuItem>
                    <MenuItem value={-1}>{t("SampleManagement.sample-list.Result.negative")}</MenuItem>
                    <MenuItem value={-2}>{t("SampleManagement.sample-list.Result.none")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.numberBottle")}
                  onChange={this.handleChange}
                  type="number"
                  name="numberBottle"
                  value={numberBottle}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="hasLipit">{t("EQASerumBank.hasLipit")}</InputLabel>
                  <Select
                    name="hasLipit"
                    value={hasLipit? 1:0}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="hasLipit" />}
                  >
                    <MenuItem value={1}>{t("Yes")}</MenuItem>
                    <MenuItem value={0}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="hemolysis">{t("EQASerumBank.hemolysis")}</InputLabel>
                  <Select
                    name="hemolysis"
                    value={hemolysis? 1:0}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="hemolysis" />}
                  >
                    <MenuItem value={1}>{t("Yes")}</MenuItem>
                    <MenuItem value={0}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="hasHighSpeedCentrifugal">{t("EQASerumBank.hasHighSpeedCentrifugal")}</InputLabel>
                  <Select
                    name="hasHighSpeedCentrifugal"
                    value={hasHighSpeedCentrifugal? 1:0}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="hasHighSpeedCentrifugal" />}
                  >
                    <MenuItem value={1}>{t("Yes")}</MenuItem>
                    <MenuItem value={0}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="dialysis">{t("EQASerumBank.dialysis")}</InputLabel>
                  <Select
                    name="dialysis"
                    value={dialysis? 1:0}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="dialysis" />}
                  >
                    <MenuItem value={1}>{t("Yes")}</MenuItem>
                    <MenuItem value={0}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl className="w-100">
                  <InputLabel htmlFor="inactivated">{t("EQASerumBank.inactivated")}</InputLabel>
                  <Select
                    name="inactivated"
                    value={inactivated? 1:0}
                    onChange={event => this.handleChange(event)}
                    input={<Input id="inactivated" />}
                  >
                    <MenuItem value={1}>{t("Yes")}</MenuItem>
                    <MenuItem value={0}>{t("No")}</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    className="w-100"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("EQASerumBank.sampledDate")}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={new Date(sampledDate)}
                    onChange={this.handleSampledDateChange}
                    fullWidth
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    className="w-100"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("EQASerumBank.receiveDate")}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={new Date(receiveDate)}
                    onChange={this.handleReceiveDateChange}
                    fullWidth
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t("EQASerumBank.storeLocation")}
                  onChange={this.handleChange}
                  type="text"
                  name="storeLocation"
                  value={storeLocation}
                />
              </Grid>
            </Grid>

            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
              <Button variant="contained" color="secondary" onClick={() => this.props.handleClose()}>Cancel</Button>
            </div>
          </ValidatorForm>
        </div>
      </Dialog>
    );
  }
}

export default EQASerumBankEditorDialog;
