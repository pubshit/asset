import React, {useState} from "react";
import {Grid, Icon, IconButton, TableCell, TableHead, TableRow,} from "@material-ui/core";
import {TextValidator} from "react-material-ui-form-validator";
import {makeStyles} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import MaterialTable, {MTableToolbar} from "material-table";
import {getAllAssetStatuss as searchAssetStatus} from "../AssetStatus/AssetStatusService";
import {searchByPage as storesSearchByPage} from "../Store/StoreService";
import {getAllInventoryCountStatus} from "../InventoryCountStatus/InventoryCountStatusService";
import clsx from "clsx";
import moment from "moment";
import {createFilterOptions} from "@material-ui/lab/useAutocomplete";
import {appConst, STATUS_STORE, variable} from "../../appConst";
import {a11yProps, checkInvalidDate, convertNumberPrice, correctPrecision, NumberFormatCustom} from "../../appFunction";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {searchByPage} from "../Council/CouncilService";
import {LightTooltip, TabPanel} from "../Component/Utilities"


function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">delete</Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    zIndex: 1,
    background: "#358600",
    position: "sticky",
    top: 0,
    left: 0,
    "& .MuiTableCell-head": {
      padding: "4px" // <-- arbitrary value
    }
  },
  borderRight: {
    borderRight: "1px solid white !important",
    color: "#ffffff",
    textAlign: "center"
  },
  colorWhite: {
    color: "#ffffff",
    textAlign: "center"
  },
  mw_100: {
    minWidth: "90px"
  },
  mw_70: {
    minWidth: "70px"
  },
  tabPanel: {
    "& .MuiBox-root": {
      paddingLeft: "0",
      paddingRight: "0"
    }
  },
  textRight: {
    "& input": {
      textAlign: "right"
    }
  }
}));

export default function ProductScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const searchObjectStore = {
    pageIndex: 0,
    pageSize: 1000000,
    isActive: STATUS_STORE.HOAT_DONG.code
  }; // kho tài sản
  const filterAutocomplete = createFilterOptions();
  const [listInventoryCountBoard, setListInventoryCountBoard] = useState([]);
  const [inventoryBoard, setInventoryBoard] = useState([])
  const [listInventoryPerson, setListInventoryPerson] = useState(props?.item?.inventoryCountPersons || [])
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleSelectHandoverDepartment = (item, rowData) => {
    let newDataRow = listInventoryPerson.map((i) => {
      if (i.tableData.id === rowData.tableData.id) {
        i.handoverDepartment = item;
        i.departmentId = item?.id;
        i.departmentName = item?.name
      }
      return i;
    });
    setListInventoryPerson(newDataRow || []);
  }

  const handleRowDataCellDelete = (item) => {
    let newDataRow = listInventoryPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListInventoryPerson(newDataRow)
    setListInventoryPerson(newDataRow || []);
  };

  let searchObject = { pageIndex: 0, pageSize: 1000000, type: appConst.OBJECT_HD_TYPE.KIEM_KE.code, };
  let columns = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        borderRight: "1px solid white !important",
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.managementCode"),
      field: "managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.managementCode
    },
    {
      title: t("Asset.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.code
    },
    {
      title: t("Asset.name"),
      field: "name",
      align: "left",
      minWidth: 180,
      render: (rowData) => rowData?.asset?.name
    },
    {
      title: t("InventoryCountStatus.CountAsset.currentStatus"),
      field: "currentStatus",
      align: "left",
      minWidth: 140,
      render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
    },
    {
      title: t("InventoryCountStatus.CountAsset.status"),
      align: "left",
      minWidth: 140,
      render: (rowData) =>
      (props?.item?.isView ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
          value={rowData.status ? rowData.status?.name : ''}
        /> : <AsynchronousAutocomplete
          searchFunction={searchAssetStatus}
          searchObject={searchObject}
          defaultValue={rowData.status}
          displayLable={"name"}
          value={rowData.status}
          onSelect={props.selectStatus}
          onSelectOptions={rowData}
          disableClearable={true}
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim()
            let filtered = filterAutocomplete(options, params)
            return filtered
          }}
          noOptionsText={t("general.noOption")}
        />
      ),
    },
    {
      title: t("Asset.receive_date"),
      field: "dateOfReception",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.dateOfReception ? (
          <span>
            {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
          </span>
        ) : (
          ""
        ),
    },
    {
      title: t("InventoryCountVoucher.store"),
      field: "asset.store",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.isStore && (props?.item?.isView ?
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData?.store ? rowData?.store?.name : ''}
          /> : <AsynchronousAutocomplete
            searchFunction={storesSearchByPage}
            searchObject={searchObjectStore}
            defaultValue={rowData?.store}
            displayLable={"name"}
            value={rowData?.store}
            onSelect={props.selectStores}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim()
              return filterAutocomplete(options, params)
            }}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />)
    },
    {
      title: t("InstrumentToolsList.useDepartment"),
      field: "departmentName",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.departmentName
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "accountantQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.accountantQuantity ? rowData.accountantQuantity : 0
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "accountantOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice((rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)) || 0,
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "inventoryQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          name="inventoryQuantity"
          value={rowData?.inventoryQuantity || 0}
          onKeyDown={e => variable.regex.numberExceptThisSymbols.includes(e.key) && e.preventDefault()}
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          InputProps={{
            readOnly: props?.item?.isView,
            inputProps: {
              className: "text-center"
            }
          }}
          validators={["required", "minNumber:0"]}
          errorMessages={[t("general.required"), "Số không được âm"]}
        />
      ),
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "inventoryOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          name="inventoryOriginalCost"
          defaultValue={rowData?.inventoryOriginalCost}
          value={rowData?.inventoryOriginalCost}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: props?.item?.isView
          }}
          validators={["required", "minNumber:0"]}
          errorMessages={[t("general.required"), "Số không được âm"]}
        />
      ),
    },
    {
      title: t("InstrumentsToolsInventory.quantity"),
      field: "differenceQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.differenceQuantity ? rowData.differenceQuantity : 0,
    },
    {
      title: t("InstrumentsToolsInventory.originalCost"),
      field: "differenceOriginalCost",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={classes.textRight}
          name="differenceOriginalCost"
          value={correctPrecision(rowData?.differenceOriginalCost) || 0}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: true,
            disableUnderline: true,
          }}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.note"),
      field: "custom",
      align: "left",
      minWidth: 150,
      render: (rowData) => (
        <TextValidator
          className="w-40"
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          type="text"
          name="note"
          value={rowData.note}
          InputProps={{
            readOnly: props?.item?.isView
          }}
        />
      ),
    },
  ];

  const columnsInventoryCount = [
    {
      title: t("InventoryCountVoucher.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => handleRowDataCellDelete(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      },
    ] : []),
    {
      title: t("InventoryCountVoucher.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    }
  ]


  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  }
  const handleChangeSelectInventoryBoard = (item) => {
    setInventoryBoard(item);
    props.handleSetHoiDongId(item?.id)
    props.handleSetListInventoryPerson(item?.hdChiTiets)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setListInventoryPerson(item?.hdChiTiets || []);
  };

  return (
    <form id="firstChild">
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t("InstrumentsToolsInventory.tabInfo")} {...a11yProps(0)} />
            <Tab label={t("InstrumentsToolsInventory.persons")} {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel className={classes.tabPanel} value={value} index={0} >
          <Grid className="" container spacing={2}>
            <Grid item md={2} sm={12} xs={12}>
              <CustomValidatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InventoryCountVoucher.inventoryCountDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={'inventoryCountDate'}
                value={props.item.inventoryCountDate}
                onChange={(date) =>
                  props.handleDateChange(date, "inventoryCountDate")
                }
                InputProps={{
                  readOnly: props?.item?.isView
                }}
                minDate={new Date("01/01/1900")}
                minDateMessage={"Ngày kiểm kê không được nhỏ hơn ngày 01/01/1900"}
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                invalidDateMessage={"Ngày không tồn tại"}
                validators={['required']}
                errorMessages={t('general.required')}
                open={isPickerOpen}
                onOpen={() => setIsPickerOpen(!props?.item?.isView)}
                onClose={() => setIsPickerOpen(false)}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                onBlur={() => handleBlurDate(props?.item?.inventoryCountDate, "inventoryCountDate")}
              />
            </Grid>

            <Grid item md={2} sm={12} xs={12} className="">
              {props?.item?.isView ?
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed"> * </span>
                      {t("InventoryCountVoucher.status")}
                    </span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  value={props.item.inventoryCountStatus ? props.item.inventoryCountStatus?.name : ''}
                /> : <AsynchronousAutocomplete
                  label={
                    <span>
                      <span className="colorRed"> * </span>
                      {t("InventoryCountVoucher.status")}
                    </span>
                  }
                  searchFunction={getAllInventoryCountStatus}
                  searchObject={searchObject}
                  defaultValue={props.item.inventoryCountStatus}
                  displayLable={"name"}
                  value={props.item.inventoryCountStatus}
                  onSelect={props.selectInventoryCount}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  noOptionsText={t("general.noOption")}
                />
              }
            </Grid>
            <Grid item md={8} sm={12} xs={12}>
              <TextValidator
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InstrumentsToolsInventory.title")}
                  </span>
                }
                className="w-100"
                name="title"
                value={props.item.title}
                onChange={props.handleChange}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                InputProps={{
                  readOnly: props?.item?.isView
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <MaterialTable
                data={props?.item?.assets?.length ? props?.item?.assets : []}
                columns={columns}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  search: false,
                  sorting: false,
                  maxBodyHeight: "500px",
                  minBodyHeight: "273px",
                  padding: "dense",
                  pageSizeOptions: appConst.rowsPerPageOptions.table,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    position: "relative",
                    paddingLeft: 10,
                    paddingRight: 10,
                    textAlign: "center",
                  },
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                  pagination: appConst.localizationVi.pagination
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),

                  Header: () => {
                    return (
                      <TableHead className={clsx(classes.header)}>
                        <TableRow>
                          <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("Asset.stt")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} rowSpan={2} >{t("Asset.managementCode")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentsToolsInventory.code")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentsToolsInventory.name")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.receive_date")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.store")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentToolsList.useDepartment")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={2} >{t("InventoryCountVoucher.deviant")}</TableCell>
                          <TableCell className={clsx(classes.colorWhite, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.note")}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.quantity")}</TableCell>
                          <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InstrumentsToolsInventory.originalCost")}</TableCell>
                        </TableRow>
                      </TableHead>
                    );
                  },
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              searchFunction={searchByPage}
              typeReturnFunction='category'
              searchObject={searchObject}
              label={t('InstrumentsToolsInventory.persons')}
              listData={listInventoryCountBoard}
              setListData={setListInventoryCountBoard}
              displayLable={"name"}
              onSelect={handleChangeSelectInventoryBoard}
              value={inventoryBoard}
              InputProps={{
                readOnly: true,
              }}
              disabled={props?.item?.isView || props?.item?.inventoryCountStatus?.indexOrder === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim();
                let filtered = filterAutocomplete(options, params);
                return filtered;
              }}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={listInventoryPerson}
              columns={columnsInventoryCount}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                },
              }}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingRight: 10,
                  paddingLeft: 10,
                  textAlign: "center",
                },

                maxBodyHeight: "285px",
                minBodyHeight: "285px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div>
    </form>
  );
}
