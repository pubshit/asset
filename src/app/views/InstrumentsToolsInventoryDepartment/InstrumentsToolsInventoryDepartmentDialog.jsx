import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { addAsset, getAsset, updateAsset } from './InstrumentsToolsInventoryDepartmentService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../../../styles/views/_loadding.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InventoryCountScrollableTabsButtonForce from './InventoryCountScrollableTabsButtonForce'
import { appConst, variable } from "../../appConst";
import AppContext from "app/appContext";
import { checkInvalidDate, checkMinDate, handleThrowResponseMessage, isSuccessfulResponse } from "app/appFunction";
import {PaperComponent} from "../Component/Utilities"

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});


class InstrumentsToolsInventoryDepartmentDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    assets: [],
    title: null,
    inventoryCountDate: new Date(),
    department: null,
    inventoryCountStatus: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: '',
    asset: {},
    inventoryCountPersons: [],
    shouldOpenSelectPersonPopup: false,
    shouldOpenPopupAssetFile: false,
    listAssetDocumentId: [],
    loading: false,
    timer: null,
    listItemEdit: []
  };

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true
    })
  }

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false
    })
  }

  handleChange = (event, source) => {
    event.persist();
    if (variable?.listInputName?.switch === source) {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  selectHandoverPerson = (item) => {
    this.setState({ handoverPerson: item }, function () {
    });
  }

  selectReceiverPerson = (item) => {
    this.setState({ receiverPerson: item }, function () {
    });
  }
  validateAsset = () => {
    let { assets } = this.state;
    let check = false;
    assets?.some(asset => {
      let isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder));
      if (isStore && !asset?.store) {
        toast.warning("Chưa chọn kho");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryQuantity && asset?.inventoryQuantity !== 0) {
        toast.warning("Số lượng không được bỏ trống");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryQuantity < 0) {
        toast.warning("Số lượng không được âm");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryOriginalCost && asset?.inventoryOriginalCost !== 0) {
        toast.warning("Thành tiền không được bỏ trống");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryOriginalCost < 0) {
        toast.warning("Thành tiền không được âm");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }
  validateSubmit = () => {
    let {
      assets,
      title,
      inventoryCountStatus,
      inventoryCountPersons,
      inventoryCountDate
    } = this.state;
    if (!inventoryCountDate) {
      toast.warning('Chưa chọn ngày kiểm kê');
      return false;
    }
    if (checkInvalidDate(inventoryCountDate)) {
      toast.warning('Ngày kiểm kê không tồn tại');
      return false;
    }
    if (checkMinDate(inventoryCountDate, "01/01/1900")) {
      toast.warning(`Ngày kiểm kê không được nhỏ hơn ngày "01/01/1900"`);
      return false;
    }
    if (!title) {
      this.setState({ loading: false });
      toast.warn("Chưa nhập kiểm kê CCDC.");
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountStatus) {
      this.setState({ loading: false });
      toast.warn("Chưa chọn trạng thái.");
      toast.clearWaitingQueue();
      return false;
    }
    if (assets?.length === 0) {
      toast.warning("Không có tài sản CCDC kiểm kê");
      toast.clearWaitingQueue();
      return false;
    }
    if (this.state?.hoiDongId && (!inventoryCountPersons || inventoryCountPersons?.length === 0)) {
      toast.warning("Hội đồng không có thành viên")
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountPersons || inventoryCountPersons?.length === 0) {
      toast.warning("Chưa chọn hội đồng kiêm kê")
      toast.clearWaitingQueue();
      return false;
    }
    if (this.validateAsset()) return;
    return true;
  }

  convertDataSubmit = () => {
    return {
      id: this.state?.id,
      assets: this.state?.assets?.map((i) => {
        return {
          assetStatusId: i?.status?.id,
          assetId: i?.assetId,
          accountantQuantity: i?.accountantQuantity,
          accountantOriginalCost: i?.accountantOriginalCost,
          accountantCarryingAmount: i?.accountantCarryingAmount,
          inventoryQuantity: i?.inventoryQuantity,
          inventoryOriginalCost: i?.inventoryOriginalCost,
          inventoryCarryingAmount: i?.inventoryCarryingAmount,
          differenceQuantity: i?.differenceQuantity,
          differenceOriginalCost: i?.differenceOriginalCost,
          differenceCarryingAmount: i?.differenceCarryingAmount,
          departmentId: i?.asset?.useDepartmentId || i?.asset?.departmentId,
          note: i?.note,
          storeId: i?.storeId,
          usedCondition: i?.usedCondition,
        }
      }),
      hoiDongId: this.state?.hoiDongId,
      inventoryCountStatusId: this.state?.inventoryCountStatus?.id || this.state?.inventoryCountStatusId,
      title: this.state?.title,
      inventoryCountDate: this.state?.inventoryCountDate,
      inventoryCountPersons: this.state?.inventoryCountPersons
        ? this.state?.inventoryCountPersons?.map(item => ({
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }))
        : []
    }
  }

  handleFormSubmit = (e) => {
    if (e.target.id !== "instrumentsToolsInventory") return;
    let { setPageLoading } = this.context;
    let { id } = this.state;
    if (!this.validateSubmit()) return;
    let dataSubmit = this.convertDataSubmit();
    setPageLoading(true)
    if (id) {
      updateAsset(dataSubmit)
        .then((res) => {
          if (isSuccessfulResponse(res?.data?.code)) {
            toast.info("Sửa phiếu thành công.");
            this.props.handleOKEditClose();
          } else {
            handleThrowResponseMessage(res)
          }
        })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
        })
        .finally(() => {
          setPageLoading(false)
        })
    } else {
      addAsset(dataSubmit)
        .then((res) => {
          if (isSuccessfulResponse(res?.data?.code)) {
            toast.info("Thêm mới phiếu thành công.");
            this.props.handleOKEditClose();
          } else {
            handleThrowResponseMessage(res)
          }
        })
        .catch((error) => {
          console.log(error)
          toast.error(this.props.t("general.error"))
        })
        .finally(() => {
          setPageLoading(false)
        })
    }
  };
  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false
    });
  };
  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item?.id) {
      if (!assetVouchers) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      }
      else {
        let hasInList = false;
        assetVouchers?.forEach(element => {
          if (element?.asset?.id === item?.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState({ assetVouchers, totalElements: assetVouchers?.length }, function () {
      this.handleAssetPopupClose();
    });
  }

  getAssetsNotInventoried = async () => {
    try {

    } catch (error) {

    }
  }

  componentWillMount() {
    let { item } = this.props;
    if (item?.isAll) {
      this.handleGetAssetVouchersCCDC();
    } else {
      if (item?.assets) {
        this.props.item?.assets.sort((a, b) => (a.id - b.id));
      }
      this.setState({
        ...this.props.item
      });
    }
  }

  handleSetListInventoryPerson = (item) => {
    this.setState({ inventoryCountPersons: item })
  }

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id })
  }


  componentDidMount() {

  }

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState({ item: {}, voucherType: voucherType, departmentId: handoverDepartment?.id }, function () {
        this.setState({
          shouldOpenAssetPopup: true
        });
      });
    }
    else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  }

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true
    });
  };


  handleGetAssetVouchersCCDC = () => {
    let { setPageLoading } = this.context;

    setPageLoading(true)
    let searchObject = {}
    searchObject.assetClass = appConst.assetClass.CCDC
    getAsset(searchObject)
      .then(({ data }) => {
        setPageLoading(false)
        if (appConst.CODE.SUCCESS !== data?.code) {
          toast.warning(data?.message)
          return;
        }
        let inventoryCountAssets = []
        if (data?.data?.length > 0) {
          data.data.forEach(asset => {
            let inventoryCountAsset = {};
            let status = {
              id: asset?.statusId,
              name: asset?.statusName,
              indexOrder: asset?.statusIndexOrder,
            }

            inventoryCountAsset.asset = asset;
            inventoryCountAsset.departmentId = asset?.departmentId;
            inventoryCountAsset.assetId = asset?.assetId;
            inventoryCountAsset.departmentName = asset?.useDepartmentName;
            inventoryCountAsset.differenceOriginalCost = 0;
            inventoryCountAsset.differenceQuantity = 0;
            inventoryCountAsset.currentStatus = status;
            inventoryCountAsset.status = status;
            inventoryCountAsset.assetStatusId = asset?.statusId;
            inventoryCountAsset.note = '';
            inventoryCountAsset.inventoryOriginalCost = (asset?.originalCost) || 0; //thành tiền kiểm kê
            inventoryCountAsset.inventoryQuantity = asset?.quantity || 0; //số lượng kiểm kê
            inventoryCountAsset.price = (asset?.originalCost / asset?.quantity); //số tiền 1 cái
            inventoryCountAsset.accountantOriginalCost = (asset?.originalCost) || 0; // thành tiền kiểm sổ
            inventoryCountAsset.accountantQuantity = asset?.quantity || 0; // số lượng kiểm sổ
            inventoryCountAsset.store = {
              name: asset?.storeName,
              id: asset?.storeId
            };
            inventoryCountAsset.storeId = asset?.storeId;
            inventoryCountAsset.isStore = [
              appConst.listStatusAssets.NHAP_KHO.indexOrder,
              appConst.listStatusAssets.LUU_MOI.indexOrder,
              appConst.listStatusAssets.DA_HONG.indexOrder,
            ].includes(asset?.statusIndexOrder);
            inventoryCountAssets.push(inventoryCountAsset);
          });
          this.setState({
            assets: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
          },);
        }
      })
      .catch((error) => {
        console.log(error)
        toast.error(this.props.t("general.error"))
        setPageLoading(false)
      })
  }

  selectInventoryCount = (item) => {
    this.setState({
      inventoryCountStatus: item,
      inventoryCountStatusId: item?.id
    });
  }
  selectStatus = (assetStatus, rowData) => {
    let { assets } = this.state;
    assets[rowData?.tableData?.id].status = assetStatus
    assets[rowData?.tableData?.id].assetStatusId = assetStatus?.id
    assets[rowData?.tableData?.id].isStore = [
      appConst.listStatusAssets.NHAP_KHO.indexOrder,
      appConst.listStatusAssets.LUU_MOI.indexOrder,
      appConst.listStatusAssets.DA_HONG.indexOrder,
    ].includes(assetStatus?.indexOrder);

    this.setState({ assets });

    this.handleCheckListAsset(assets[rowData?.tableData?.id]);
    this.debounce();
  }

  handleRowDataCellChange = (rowData, event) => {
    let { assets } = this.state;
    let item = assets[rowData?.tableData?.id]
    if (variable.listInputName.note === event.target.name) {
      item.note = event.target.value;
      this.setState({ assets })
    }
    if (variable.listInputName.inventoryOriginalCost === event.target.name) {
      let value = Number(event.target.value);
      let inventoryOriginalCost = (value * item?.inventoryQuantity) - (item?.accountantQuantity * item?.accountantOriginalCost)
      item.inventoryOriginalCost = value;
      item.differenceOriginalCost = (inventoryOriginalCost);
      this.setState({ assets });
    }
    if (variable.listInputName.inventoryQuantity === event.target.name) {
      let value = Number(event.target.value);
      let inventoryOriginalCost = (value * item?.inventoryOriginalCost) - (item?.accountantQuantity * item?.accountantOriginalCost)
      item.inventoryQuantity = value;
      item.differenceQuantity = (value - item?.accountantQuantity);
      item.differenceOriginalCost = (inventoryOriginalCost);
      this.setState({ assets });
    }

    this.handleCheckListAsset(item);
    this.debounce();
  };

  handleCheckListAsset = (assets) => {
    try {
      if (!assets) {
        return;
      }

      const { listItemEdit } = this.state;
      const existingIndex = listItemEdit.findIndex((item) => item?.assetId === assets?.assetId);

      if (existingIndex !== -1) {
        const updatedList = [...listItemEdit];
        updatedList[existingIndex] = assets;
        this.setState({ listItemEdit: updatedList });
      } else {
        this.setState((prevState) => ({
          listItemEdit: [...prevState.listItemEdit, assets],
        }));
      }
    } catch (error) {
    }
  };

  debounce = () => {
    clearTimeout(this.state.timer);
    const newTimer = setTimeout(this.handleSubmitAssetChange, 3000);
    this.setState({ timer: newTimer });
  }

  handleSubmitAssetChange = () => {
    console.log(this.state.listItemEdit)
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true
    });
  }

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false
    });
  }

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes?.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (attributes[index]?.attribute?.id === rowData.attribute?.id) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () {
    });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  selectStores = (store, rowData) => {
    let { assets } = this.state;
    assets[rowData?.tableData?.id].store = store
    assets[rowData?.tableData?.id].storeId = store?.id
    this.setState({ assets })

    this.handleCheckListAsset(assets[rowData?.tableData?.id]);
    this.debounce();
  };

  handleAssetPopupOpen = () => {
    this.setState({
      shouldOpenAssetPopup: true
    });
  };
  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };
  render() {
    let { open, t, i18n, item } = this.props;
    let { loading } = this.state;

    return (
      <Dialog className="w-90 m-auto" open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth scroll="paper" >
        <div className={clsx("wrapperButton", !loading && 'hidden')} >
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm id='instrumentsToolsInventory' ref="form" onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog">
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            <h4 className="">
              {item?.id ? t('InstrumentsToolsInventory.updateDialogDepartment') : t('InstrumentsToolsInventory.addDialogDepartment')}
            </h4>
          </DialogTitle>
          <DialogContent >
            <Grid>
              <InventoryCountScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
                handleChange={this.handleChange}
                selectInventoryCount={this.selectInventoryCount}
                handleDepartmentPopupClose={this.handleDepartmentPopupClose}
                selectStatus={this.selectStatus}
                selectStores={this.selectStores}
                handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
                openSelectPersonPopup={this.openSelectPersonPopup}
                handleSelectPerson={this.handleSelectPerson}
                removePersonInlist={this.removePersonInlist}
                handleRowDataCellChange={this.handleRowDataCellChange}
                handleDateChange={this.handleDateChange}
                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                handleSetListInventoryPerson={this.handleSetListInventoryPerson}
                handleSetHoiDongId={this.handleSetHoiDongId}
                handleAssetPopupOpen={this.handleAssetPopupOpen}
                handleAssetPopupClose={this.handleAssetPopupClose}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => { this.props.handleClose() }}
              >
                {t('general.cancel')}
              </Button>
              {!item?.isView &&
                <Button variant="contained" color="primary" type="submit">
                  {t('general.save')}
                </Button>
              }
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
InstrumentsToolsInventoryDepartmentDialog.contextType = AppContext;
export default InstrumentsToolsInventoryDepartmentDialog;
