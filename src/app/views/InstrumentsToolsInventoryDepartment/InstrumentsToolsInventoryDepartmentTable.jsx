/* eslint-disable no-unused-expressions */
/* eslint-disable array-callback-return */
import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountByStatus,
  getAssetByVoucher,
  getItemByIdPrint,
  getInfoSumary,
} from "./InstrumentsToolsInventoryDepartmentService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import ComponentInventoryTable from "./ComponentInventoryTable";
import localStorageService from "app/services/localStorageService";
import ListAssetInstrumentsToolsInventoryDepartmentTable from "./ListAssetInstrumentsToolsInventoryDepartmentTable";
import { handleThrowResponseMessage, isSuccessfulResponse, isValidDate } from "app/appFunction";
import { convertFromToDate } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: "-1.5em",
  },
}))(Tooltip);

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}
function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const hasDeletePermission = props.hasDeletePermission;
  const hasEditPermission = props.hasEditPermission;

  return (
    <div className="none_wrap">
      {hasEditPermission
        &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder,
        ].includes(item?.statusIndexOrder)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
        ].includes(item?.statusIndexOrder) && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InstrumentsToolsInventoryDepartmentTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    rowsPerPageAsset: appConst.rowsPerPageOptions.popup[0],
    pageAsset: 0,
    totalElementsAsset: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationExcel: false,
    shouldOpenPrintDialog: false,
    hasEditPermission: false,
    hasDeletePermission: false,
    hasCreatePermission: false,
    isSearch: false,
    inventoryCountDateBottom: null,
    inventoryCountDateTop: null,
    ids: [],
    tabValue: 0,
    itemAsset: [],
    checkAll: false,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = 2; //Điều chuyển

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabInventory?.tabAll === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder: null,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabPlan === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[0].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabTakingInventory === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[1].indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabInventory?.tabInventory === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          inventoryCountStatusIndexOrder:
            appConst.listStatusInventory[2].indexOrder,
        },
        () => this.updatePageData()
      );
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleChangePageAsset = (event, newPage) => {
    this.setState({ pageAsset: newPage }, () => {
      this.updatePageDataAsset();
    })
  };

  setRowsPerPageAsset = (event) => {
    this.setState({ rowsPerPageAsset: event.target.value, pageAsset: 0 }, () => {
      this.updatePageDataAsset();
    });
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { selectedList } = this.state;
    setPageLoading(true);
    let searchObject = {};
    searchObject.type = appConst.assetTypeInventoryCountObject.ALL.code;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = convertFromToDate(this.state.inventoryCountDateBottom)?.fromDate;
    searchObject.inventoryCountDateTop = convertFromToDate(this.state.inventoryCountDateTop)?.toDate;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.inventoryCountStatusIndexOrder =
      this.state?.inventoryCountStatusIndexOrder;

    searchByPage(searchObject).then((res) => {
      const { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        let countItemChecked = 0
        const itemList = data?.content?.map(item => {
          if (selectedList?.find(checkedItem => checkedItem === item.id)) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
          }
        })

        this.setState({
          idVoucher: null,
          itemList,
          totalElements: data?.totalElements,
          ids: [],
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0
        })
      }
      else {
        handleThrowResponseMessage(res)
      }
    })
      .catch(() => {
        toast.error(this.props.t("general.error"));
      })
      .finally(() => {
        setPageLoading(false)
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrintDialog: false,
      shouldOpenConfirmationExcel: false,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationExcel: false,
      inventoryCountDateBottom: null,
      inventoryCountDateTop: null,
      keyword: "",
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then(({ data }) => {
      this.setState({
        item: data?.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(this.props.t("general.deleteSuccess"))
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          }
          this.updatePageData();
          this.handleDialogClose();
        } else {
          toast.warning(data.message);
        }
      })
      .catch((error) => {
        toast.error(this.props.t("general.error"));
      });
  };

  componentDidMount() {
    this.setState({
      inventoryCountStatusIndexOrder: null,
    }, () => {
      this.updatePageData();
      this.getRoleCurrentUser();
      this.getCountStatus();
    }
    );
  }

  getRoleCurrentUser = () => {
    let { hasDeletePermission, hasEditPermission, hasCreatePermission } =
      this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser?.roles?.forEach((role) => {
        //user
        if (role.name === ConstantList.ROLES.ROLE_USER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = false;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = false;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = false;
          }
        }
        // người quản lý vật tư
        if (role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = true;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = true;
          }
        }
        // người đại diện phòng ban
        if (role.name === ConstantList.ROLES.ROLE_ASSET_USER) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = false;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = false;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = false;
          }
        }
        // admin hoặc supper_admin
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
          if (hasCreatePermission !== true) {
            hasCreatePermission = true;
          }
          if (hasEditPermission !== true) {
            hasEditPermission = true;
          }
        }
      });
      this.setState(
        {
          hasDeletePermission,
          hasEditPermission,
          hasCreatePermission,
          orgName: currentUser?.org?.name,
        },
        () => {
        }
      );
    }
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    item.checked = !item?.checked;

    let selectAllItem = true;
    for (let i = 0; i < AssetTransfer?.length; i++) {
      if (!AssetTransfer[i].checked) {
        selectAllItem = false;
      }
      if (AssetTransfer[i]?.id === item?.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };
  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (let i = 0; i < AssetTransfer?.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetTransfer: AssetTransfer,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (let i = 0; i < list?.length; i++) {
      await deleteItem(list[i]?.id);
    }
  }

  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };
  handleExcel = () => {
    this.setState({
      shouldOpenConfirmationExcel: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = this.state.inventoryCountDateBottom;
    searchObject.inventoryCountDateTop = this.state.inventoryCountDateTop;
    searchObject.ids = this.state?.selectedList;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.type = appConst.assetTypeInventoryCountObject.ALL.code;
    searchObject.inventoryCountStatusIndexOrder = this.state.inventoryCountStatusIndexOrder;

    exportToExcel(searchObject)
      .then((res) => {
        setPageLoading(false);
        this.setState({ ids: [] });
        toast.success(this.props.t("general.successExport"));
        this.handleOKEditClose();
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "InventoryCountVoucherCCDC.xlsx");
      })
      .catch((err) => {
        setPageLoading(false);
        toast.error(this.props.t("general.failExport"));
      });
  };

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  handleDateChange = (date, name) => {
    this.setState(
      {
        [name]: date,
      },
      () => { isValidDate(date) && this.search() }
    );
  };

  handleEditData = (inventoryCountVoucher, openEdit = false, openPrint = false) => {
    let inventoryCountPersonIds = [];
    let userIds = [];
    inventoryCountVoucher?.assets?.map((asset, index) => {
      let differenceQuantity = (
        asset?.inventoryQuantity - asset?.accountantQuantity
      );
      let differenceOriginalCost = (
        (asset?.inventoryQuantity * asset?.inventoryOriginalCost) - (asset?.accountantQuantity * asset?.accountantOriginalCost)
      );
      asset.asset = {
        ...asset,
        name: asset?.assetName,
        code: asset?.assetCode,
        status: {
          name: asset?.updateStatusName,
          id: asset?.updateStatusId,
          indexOrder: asset?.updateStatusIndexOrder
        },
        store: {
          name: asset?.storeName,
          id: asset?.storeId
        },
      };
      asset.status = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };
      asset.store = {
        name: asset?.storeName,
        id: asset?.storeId
      };
      asset.currentStatus = {
        name: asset?.currentStatusName,
        id: asset?.currentStatusId,
      }
      asset.assetId = asset?.assetId;
      asset.differenceOriginalCost = differenceOriginalCost;
      asset.storeId = asset?.storeId;
      asset.departmentId = asset?.departmentId;
      asset.differenceQuantity = differenceQuantity;
      asset.price = (
        asset?.accountantOriginalCost / asset?.accountantQuantity
      );
      asset.isStore =
        appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder ||
        appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder ||
        appConst.listStatusAssets.DA_HONG.indexOrder === asset?.status?.indexOrder;
    });
    inventoryCountVoucher?.inventoryCountPersons?.map((item) => {
      inventoryCountPersonIds.push(item?.personId);
      userIds.push(item?.personId);
    });

    inventoryCountVoucher = {
      ...inventoryCountVoucher,
      inventoryCountPersonIds: inventoryCountPersonIds,
      userIds: userIds,
      inventoryCountStatus:
        inventoryCountVoucher?.statusId
          ?
          {
            name: inventoryCountVoucher?.statusName,
            id: inventoryCountVoucher?.statusId,
            statusIndexOrder: inventoryCountVoucher?.statusIndexOrder,
          }
          : null,
      inventoryCountStatusId: inventoryCountVoucher?.statusId,
    };

    this.setState({
      item: inventoryCountVoucher,
      shouldOpenEditorDialog: openEdit,
      shouldOpenPrintDialog: openPrint,
    });
  };
  setStateInventory = (item) => {
    this.setState(item);
  };

  updatePageDataAsset = async () => {
    try {
      let {
        rowsPerPageAsset,
        pageAsset,
        idVoucher
      } = this.state;

      let searchObject = {};

      searchObject.pageIndex = pageAsset + 1;
      searchObject.pageSize = rowsPerPageAsset;
      searchObject.id = idVoucher;

      const data = await getAssetByVoucher(searchObject);
      let { content, totalElements } = data?.data?.data;
      this.setState({
        itemAsset: content || [],
        totalElementsAsset: totalElements || 0,
      });
    } catch (error) {
      toast.error("Xảy ra lỗi")
    }
  }

  handleSetItemState = async (rowData) => {
    if (rowData?.id) {
      this.setState({ idVoucher: rowData?.id, pageAsset: 0 }, () => {
        this.updatePageDataAsset();
      })
    }
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.listStatusInventory[0].indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusInventory[0].name}
          </span>
        );
      case appConst.listStatusInventory[1].indexOrder:
        return (
          <span className="status status-warning">
            {appConst.listStatusInventory[1].name}
          </span>
        );
      case appConst.listStatusInventory[2].indexOrder:
        return (
          <span className="status status-error">
            {appConst.listStatusInventory[2].name}
          </span>
        );

      default:
        break;
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountByStatus({ type: 2, assetClass: appConst.assetClass.CCDC })
      .then(({ data }) => {
        let countStatusNew,
          countStatusTakingInventory,
          countStatusInventory;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (
            appConst?.listStatusInventoryObject.NEW.indexOrder ===
            item?.trangThai
          ) {
            countStatusNew = this.checkCount(item?.soLuong);

            return;
          }
          if (
            appConst?.listStatusInventoryObject.DANG_KIEM_KE.indexOrder ===
            item?.trangThai
          ) {
            countStatusTakingInventory = this.checkCount(item?.soLuong);
            return;
          }
          if (
            appConst?.listStatusInventoryObject.DA_KIEM_KE.indexOrder ===
            item?.trangThai
          ) {
            countStatusInventory = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusNew,
            countStatusTakingInventory,
            countStatusInventory,
          }
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      }).finally(() => {
        setPageLoading(false);
      });
  };

  getFullInfoAsset = async (rowData) => {
    let mergeObj = {};
    let { t } = this.props
    try {
      const searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        id: rowData?.id,
      }
      const info = await getInfoSumary(rowData?.id);
      const resInfo = info?.data?.data || {};
      const assets = await getAssetByVoucher(searchObject);
      const resAssets = assets?.data?.data?.content?.length ? assets?.data?.data?.content : [];

      if (isSuccessfulResponse(info?.data?.code)) {
        mergeObj = { ...mergeObj, ...resInfo };
      } else {
        handleThrowResponseMessage(info)
      }
      if (isSuccessfulResponse(assets?.data?.code)) {
        mergeObj = { ...mergeObj, assets: [...resAssets] };
      } else {
        handleThrowResponseMessage(assets)
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      return mergeObj;
    }
  };

  handleEdit = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.isView = false;
      this.handleEditData(data, true, false)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleView = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.isView = true;
      this.handleEditData(data, true, false)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handlePrint = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.orgName = this.state?.orgName
      this.handleEditData(data, false, true)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      tabValue,
      checkAll
    } = this.state;
    let TitlePage = t("InstrumentsToolsInventory.inventoryCountVoucherDepartment");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        // width: "120px",
        maxWidth: 100,
        align: "left",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasEditPermission={hasEditPermission}
            hasDeletePermission={hasDeletePermission}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.view === method) {
                this.handleView(rowData);
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.stt"),
        field: "",
        maxWidth: "40px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("InventoryCountVoucher.inventoryCountDate"),
        maxWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.inventoryCountDate ? (
            <span>
              {moment(rowData.inventoryCountDate).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryCountVoucher.status"),
        field: "inventoryCountStatus.name",
        maxWidth: 100,
        align: "center",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          this.checkStatus(rowData?.statusIndexOrder),
      },
      {
        title: t("InventoryCountVoucher.voucherName"),
        field: "title",
        minWidth: 150,
        align: "left",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/inventory-count-vouchers-department",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("InventoryCountVoucher.tabAll")} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabNew")}</span>
              <div className="tabQuantity tabQuantity-success">
                {this.state?.countStatusNew || 0}
              </div>
            </div>} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabTakingInventory")}</span>
              <div className="tabQuantity tabQuantity-warning">
                {this.state?.countStatusTakingInventory || 0}
              </div>
            </div>} />
            <Tab className="tab" label={<div className="tabLable">
              <span>{t("InventoryCountVoucher.tabInventory")}</span>
              <div className="tabQuantity tabQuantity-error">
                {this.state?.countStatusInventory || 0}
              </div>
            </div>} />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabAll}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryDepartmentTable
            {...this.props}
            item={this.state}
            itemAsset={this.state.itemAsset}
            handleChangePageAsset={this.handleChangePageAsset}
            setRowsPerPageAsset={this.setRowsPerPageAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabPlan}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryDepartmentTable
            {...this.props}
            item={this.state}
            itemAsset={this.state.itemAsset}
            handleChangePageAsset={this.handleChangePageAsset}
            setRowsPerPageAsset={this.setRowsPerPageAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabTakingInventory}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryDepartmentTable
            {...this.props}
            item={this.state}
            itemAsset={this.state.itemAsset}
            handleChangePageAsset={this.handleChangePageAsset}
            setRowsPerPageAsset={this.setRowsPerPageAsset}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabInventory.tabInventory}
          className="mp-0"
        >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
          />
          <ListAssetInstrumentsToolsInventoryDepartmentTable
            {...this.props}
            item={this.state}
            itemAsset={this.state.itemAsset}
            handleChangePageAsset={this.handleChangePageAsset}
            setRowsPerPageAsset={this.setRowsPerPageAsset}
          />
        </TabPanel>
      </div>
    );
  }
}
InstrumentsToolsInventoryDepartmentTable.contextType = AppContext;
export default InstrumentsToolsInventoryDepartmentTable;
