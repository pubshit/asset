import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from "react-i18next";
const InstrumentsToolsInventoryDepartmentTable = EgretLoadable({
  loader: () => import("./InstrumentsToolsInventoryDepartmentTable"),
});
const ViewComponent = withTranslation()(
  InstrumentsToolsInventoryDepartmentTable
);

const InstrumentsToolsInventoryDepartmentRoutes = [
  {
    path:
      ConstantList.ROOT_PATH +
      "instruments-and-tools/inventory-count-vouchers-department",
    exact: true,
    component: ViewComponent,
  },
];

export default InstrumentsToolsInventoryDepartmentRoutes;
