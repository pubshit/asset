import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  getAssetDocumentById,
  getNewCodeDocument,
} from "../Asset/AssetService";
import {
  updatePurchasePlaningById,
  createPurchasePlaning,
} from "./PurchasePlaningService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import TransferScrollableTabsButtonForce from "./PurchasePlaningScrollableTabsButtonForce";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getTheHighestRole } from "app/appFunction";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import localStorageService from "app/services/localStorageService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const theHighestRole = getTheHighestRole();
const departmentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER);

class PurchasePlaningDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Transfer,
    rowsPerPage: 10000,
    page: 0,
    purchaseProducts: [],
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    approvedDate: new Date(),
    planingDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    valueText: null,
    isAssetTransfer: true,
    handoverPersonClone: false,
    shouldOpenPopupAssetFile: false,
    transferStatus: null,
    loading: false,
    note: null,
    name: "",
    planDepartment: "",
    totalEstimatedCost:"",
    status: "",
    plantDepartment: null,
    planDepartmentId:"",
    planDepartmentName: "",
    objPlanDepartment:{},
    objPlaner:{},
    planer: null,
  };
   isRoleAssetUser = theHighestRole.isRoleAssetUser;

   convertDto = (state) => {  
    let { item } = this.props;
    item.purchaseProducts = item?.purchaseProducts?.map(product => ({
      ...product,
      skuId: product.sku?.id,
      skuName: product.sku?.name,
      supplierId: product?.suppilerDepartment?.id,
      supplierName: product.suppilerDepartment?.name,
      shoppingFormId: product.shoppingForm?.id,
      shoppingFormName: product.shoppingForm?.name
    }));
    let data = {
      ...item,
      purchaseProducts: item.purchaseProducts,
      planDepartmentId: state?.plantDepartment?.id,
      planDepartmentName: state?.plantDepartment?.text,
      approvedDate: state?.approvedDate,
      planDate: state?.planDate,
      name: state?.name,
      status: state?.status,
      note: state?.note,
      totalEstimatedCost: state?.totalEstimatedCost,
      type: state?.type,
      planPersonId: state?.planer?.personId,
      planPersonName: state?.planer?.personDisplayName,
      purchasePreparePlanIds: item.purchasePreparePlanIds,
      purchasePreparePlans: item.purchasePreparePlans
    };
    return data;
  }

  handleChange = (event) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleChangeEstimatedCosts = (value, rowdata) => {
    let { purchaseProducts } = this.state;
    purchaseProducts.map( (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.estimatedCosts = value;
      }
    });
    this.setState(
      { purchaseProducts: purchaseProducts, handoverPersonClone: true }
    );
    
  };
  selectPurchaseForm = async (item, rowdata) => {
    let { purchaseProducts } = this.state;
    purchaseProducts.map( (assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.shoppingForm = item;
      }
    });
    this.setState(
      { purchaseProducts: purchaseProducts, handoverPersonClone: true }
    );
  };
  
  selectSuppiler = async (item, rowdata) => {
    let { purchaseProducts } = this.state;
    purchaseProducts.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.suppilerDepartment = item
      }
    });
    this.setState(
      { purchaseProducts: purchaseProducts, handoverPersonClone: true }
    );
  };
  selectStockKeepingUnit = async (item, rowdata) => {
    let { purchaseProducts } = this.state;
    purchaseProducts.map((assetVoucher) => {
      if (assetVoucher?.tableData?.id === rowdata.tableData?.id) {
        assetVoucher.sku = item;
      }
    });
    this.setState({ purchaseProducts: purchaseProducts, handoverPersonClone: true });
    };
  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  handleFormSubmit = (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let dataState = this.convertDto(this.state);
    setPageLoading(true);
    if (id) {
      updatePurchasePlaningById(dataState, id)
        .then(({data}) => {
          setPageLoading(false);
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.info(data?.message);
            this.props.handleOKEditClose();
          }
          else {
            toast.warning(data?.message);
          }
        })
        .catch(() => {
          setPageLoading(false);
          toast.clearWaitingQueue();
      });
    } else {
      createPurchasePlaning(dataState)
        .then(({ data }) => {
          setPageLoading(false);
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.info(data?.message);
            this.props.handleOKEditClose();
          }
          else {
            toast.warning(data?.message);
          }
        })
        .catch(() => {
          setPageLoading(false);
          toast.clearWaitingQueue();
        });
    }
  };

  handleDateChange = (date, name) => {
    if(name === "approvedDate" && date) {
        this.setState({
        [name]: date,
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  removeAssetInlist = (id) => {
    let { purchaseProducts } = this.state;
    console.log(purchaseProducts)
    let index = purchaseProducts.findIndex((x) => x.id === id);
    purchaseProducts.splice(index, 1);
    this.setState({
      purchaseProducts,
    });
  };

  componentWillMount() {
    let { item } = this.props;
    if (item.id) {
      this.setState({
        ...this.props.item,
        handoverPersonClone: true,
        handoverDepartment: {
          id: item?.planDepartmentId,
          text: item?.handoverDepartmentName,
        },
        planer: {
          personId: item?.planPersonId,
          personDisplayName: item?.planPersonName,
        },
        purchaseProducts: item?.purchaseProducts?.map(product => {
          product.sku = {
            name: product.skuName,
            id: product.skuId,
          }
          product.shoppingForm = {
            name: product.shoppingFormName,
            id: product.shoppingFormId
          }
          product.suppilerDepartment = {
            name: product.supplierName,
            id: product.supplierId
          }
          return product
        })
      });
    } else {
      this.setState({
        ...this.props.item,
      });
    }
    this.isRoleAssetUser && this.setState({
      handoverDepartment: {
        ...departmentUser,
        text: departmentUser?.name
      },
      planDepartmentId: departmentUser?.id,
    });
  }
  componentDidMount() {}

  handleSelectHandoverDepartment = (item) => {
    this.setState({ plantDepartment: item ? item: null, planer: null });
  };

  handleSelectStatus = (item) => {
    this.setState({status: item ? item : null}); 
  }

  handleSelectHandoverPerson = (item) => {
    this.setState({
      planer: item ? item : null,
    });

  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  render() {
    let { open, t, i18n, isView } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 1000 };
    let {
      shouldOpenNotificationPopup,
      loading,
    } = this.state;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          id="assetTransferDialog"
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            {t("purchasePlaning.title")}
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <TransferScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleSelectHandoverPerson={
                this.handleSelectHandoverPerson
              }
              handleChangeEstimatedCosts={this.handleChangeEstimatedCosts}
              selectPurchaseForm={this.selectPurchaseForm}
              selectSuppiler={this.selectSuppiler}
              selectStockKeepingUnit={this.selectStockKeepingUnit}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleSetDataSelect={this.handleSetDataSelect}
              handleChange={this.handleChange}
              isRoleAssetUser={this.isRoleAssetUser}
              handleSelectStatus={this.handleSelectStatus}
              isView={isView}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={() => this.props.handleClose()}
                >
                  {t("general.cancel")}
                </Button>
                {<Button variant="contained" color="primary" type="submit">
                  {t("general.save")}
                </Button>}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
PurchasePlaningDialog.contextType = AppContext;
export default PurchasePlaningDialog;
