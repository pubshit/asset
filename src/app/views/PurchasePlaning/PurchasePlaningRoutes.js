import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const PurchasePlaning = EgretLoadable({
  loader: () => import("./PurchasePlaningTable")
});
const ViewComponent = withTranslation()(PurchasePlaning);

const PurchasePlaningRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/purchase_planing",
    exact: true,
    component: ViewComponent
  }
];

export default PurchasePlaningRoutes;