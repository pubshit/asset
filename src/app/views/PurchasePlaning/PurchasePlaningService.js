import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";

const API_PATH_MAINTAIN =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-plan";

export const searchByPage = (purchasePlaning) => {
  return axios.post(API_PATH_MAINTAIN + "/search", purchasePlaning);
};

export const createPurchasePlaning = (purchasePlaning) => {
  return axios.post(API_PATH_MAINTAIN, purchasePlaning);
};

export const updatePurchasePlaningById = (
  purchasePlaning,
  purchasePlaningId
) => {
  return axios.put(
    API_PATH_MAINTAIN + "/" + purchasePlaningId,
    purchasePlaning
  );
};

export const getPurchasePlaningById = (purchasePlaningId) => {
  return axios.get(API_PATH_MAINTAIN + "/" + purchasePlaningId);
};

export const deletePurchasePlaningById = (purchasePlaningId) => {
  return axios.delete(API_PATH_MAINTAIN + "/" + purchasePlaningId);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/purchaseRequestToExcel",
    data: searchObject,
    responseType: "blob",
  });
};

//get phong ban bàn giao
export const getListManagementDepartment = (searchObject) => {
  return axios.post(
    ConstantList.API_ENPOINT +
      "/api/assetDepartment" +
      ConstantList.URL_PREFIX +
      "/searchByPage",
    { isActive: STATUS_DEPARTMENT.HOAT_DONG.code, ...searchObject }
  );
};

// get người theo id phòng
export const getListUserByDepartmentId = (searchObject) => {
  let config = {
    params: { ...searchObject, isActive: STATUS_DEPARTMENT.HOAT_DONG.code },
  };
  let url = ConstantList.API_ENPOINT + "/api/v1/user-departments/page";
  return axios.get(url, config);
};

export const assetPuchasePlanningExportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/purchase-plan",
    data: searchObject,
    responseType: "blob",
  });
};

export const getByPlanId = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  let url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/purchase-products/plan";
  return axios.get(url, config);
};

export const getCountStatus = (type) => {
  let url = `${API_PATH_MAINTAIN}/count-by-status?type=${type}`
  return axios.get(url)
}
 