import {
  AppBar,
  Icon,
  IconButton,
  Tab,
  Tabs,
} from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  TabPanel,
  classStatus,
  convertFromToDate,
  convertNumberPriceRoundUp,
  formatDateDto,
  isValidDate, formatTimestampToDate
} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import {
  assetPuchasePlanningExportToExcel,
  deletePurchasePlaningById,
  getCountStatus,
  getPurchasePlaningById,
  searchByPage,
} from "./PurchasePlaningService";
import ComponentPurchasePlaningTable from "./ComponentPurchasePlaningTable";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import { getListProductByIds } from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningService";
import clsx from "clsx";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let {
    hasDeletePermission,
    isRoleAdmin,
  } = props;
  const isDangDuyetDanhMuc = item?.status
    === appConst.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder
  const isDangXuLy = item?.status
    === appConst.listStatusPurchasePlaningObject.DANG_XU_LY.indexOrder;
  const isDaDuyetBaoGia = item?.status
    === appConst.listStatusPurchasePlaningObject.DA_DUYET_BAO_GIA.indexOrder

  return (
    <div className="none_wrap">
      {(isDangXuLy || isDangDuyetDanhMuc || isDaDuyetBaoGia)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission &&
        (item?.isOwner || isRoleAdmin) &&
        (isRoleAdmin
          ? isRoleAdmin
          : (item?.transferStatusIndex ===
            appConst.listStatusTransfer[0].indexOrder)) && (
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        )}
      {/* {(hasPrintPermission || isRoleAdmin) && (
        <LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )} */}
      <LightTooltip
        title={t("InstrumentToolsTransfer.watchInformation")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <VisibilityIcon size="small" className="iconEye" />
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class PurchasePlaningTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    status: null,
    fromPlanDate: null,
    toPlanDate: null,
    tabValue: 0,
    isRoleAdmin: false,
    isRoleAssetManager: false,
    isCheckReceiverDP: true,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataPuschasePlaningTable: [],
    rowDataClick: null,
    type: appConst.TYPE_PURCHASE.TSCD_CCDC,
  };
  numSelected = 0;
  rowCount = 0;
  type = ConstantList.VOUCHER_TYPE.Transfer;

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });

    const statusMap = {
      [appConst.tabPurchasePlaning.tabAll]: null,
      [appConst.tabPurchasePlaning.tabPlan]: appConst.tabPurchasePlaning.tabPlan,
      [appConst.tabPurchasePlaning.tabProcessed]: appConst.tabPurchasePlaning.tabProcessed,
      [appConst.tabPurchasePlaning.tabQuoteApproved]: appConst.tabPurchasePlaning.tabQuoteApproved,
      [appConst.tabPurchasePlaning.tabEnd]: appConst.tabPurchasePlaning.tabEnd,
    };
    this.search({ status: statusMap[newValue] });
  };

  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromPlanDate,
      toPlanDate,
      keyword,
      status,
      selectedList,
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        type: appConst.TYPE_PURCHASE.TSCD_CCDC,
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
      };
      searchObject.purchasePlanIds = selectedList?.map(x => x.id);
      if (fromPlanDate) {
        searchObject.fromPlanDate = convertFromToDate(new Date(fromPlanDate)).fromDate;
      }
      if (toPlanDate) {
        searchObject.toPlanDate = convertFromToDate(new Date(toPlanDate)).toDate;
      }
      if (keyword) {
        searchObject.keyword = keyword;
      }
      if (status) {
        searchObject.status = status;
      }

      const res = await assetPuchasePlanningExportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "Kế hoạch mua sắm tài sản.xlsx");
        toast.success(t("general.successExport"));
      }
    }
    catch (error) {
      toast.error(t("general.failExport"));
    }
    finally {
      setPageLoading(false);
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.search({ page });
  };

  setRowsPerPage = (event) => {
    this.search({ rowsPerPage: event.target.value, page: 0 });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = (searchObject) => {
    const newObject = searchObject ? { ...searchObject } : { page: 0 };
    this.setState({ ...newObject, rowDataClick: null }, () => this.updatePageData())
  }

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      fromPlanDate, 
      toPlanDate,
      keyword,
      page,
      rowsPerPage,
      status,
      selectedList,
    } = this.state;
    setPageLoading(true);
    let searchObject = {};
    searchObject.keyword = keyword.trim() || '';
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchObject.status = status;
    searchObject.fromPlanDate = fromPlanDate ? formatDateDto(fromPlanDate): null;
    searchObject.toPlanDate = fromPlanDate ? formatDateDto(toPlanDate): null;
    searchObject.type = appConst.TYPE_PURCHASE.TSCD_CCDC;
    searchByPage(searchObject)
      .then(({ data }) => {
        let newArray = data?.data?.content?.map(item => {
          let isExist = selectedList?.some(x => x.id === item.id);
          
          return {
            ...item,
            tableData: {
              ...item.tableData,
              checked: isExist,
            }
          }
        })
        this.setState(
          {
            rowDataClick: null,
            itemList: [...newArray],
            totalElements: data?.data?.totalElements,
            rowDataPuschasePlaningTable: []
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isPrint: false,
      isView: false
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isView: false
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    deletePurchasePlaningById(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(this.props.t("general.deleteSuccess"));
          this.handleDialogClose();
          this.updatePageData();
        } else {
          toast.warning(data?.message);
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    this.setState(
      {
        status: null,
      },
      () => this.updatePageData()
    );
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCreatePermission,
      hasPrintPermission,
      isRoleAdmin,
      isRoleAssetManager
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      // eslint-disable-next-line no-unused-expressions
      currentUser?.roles?.forEach((role) => {
        if (
          role.name === ConstantList.ROLES.ROLE_ORG_ADMIN ||
          role.name === ConstantList.ROLES.ROLE_ADMIN
        ) {
          if (!hasDeletePermission) {
            hasDeletePermission = true;
          }
          if (!hasEditPermission) {
            hasEditPermission = true;
          }
          if (!hasWatchPermission) {
            hasWatchPermission = true;
          }
          if (!hasPrintPermission) {
            hasPrintPermission = true;
          }
          if (!hasCreatePermission) {
            hasCreatePermission = true;
          }
          if (!isRoleAdmin) {
            isRoleAdmin = true;
          }
        }
        // người quản lý vật tư
        if (role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER) {
          if (!hasDeletePermission) {
            hasDeletePermission = true;
          }
          if (!hasEditPermission) {
            hasEditPermission = true;
          }
          if (!hasWatchPermission) {
            hasWatchPermission = true;
          }
          if (!hasPrintPermission) {
            hasPrintPermission = true;
          }
          if (!hasCreatePermission) {
            hasCreatePermission = true;
          }
          if (!isRoleAssetManager) {
            isRoleAssetManager = true
          }
        }
        // người đại diện phòng ban
        if (role.name === ConstantList.ROLES.ROLE_ASSET_USER) {
          if (!hasDeletePermission) {
            hasDeletePermission = true;
          }
          if (!hasEditPermission) {
            hasEditPermission = true;
          }
          if (!hasWatchPermission) {
            hasWatchPermission = true;
          }
          if (!hasCreatePermission) {
            hasCreatePermission = true;
          }
          if (!hasPrintPermission) {
            hasPrintPermission = false;
          }
        }
      });
      this.setState({
        hasDeletePermission,
        hasEditPermission,
        hasWatchPermission,
        hasCreatePermission,
        hasPrintPermission,
        isRoleAdmin,
        isRoleAssetManager
      });
    }
  };

  handleButtonAdd = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (let i = 0; i < list.length; i++) {
      await deletePurchasePlaningById(list[i].id);
    }
  }

  handleDeleteAll = () => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  setStateTransfer = (item) => {
    this.setState(item);
  };

  handleEdit = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          isView: false,
          item: data?.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleView = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          isView: true,
          item: data?.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handlePrint = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getPurchasePlaningById(id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          item: data?.data,
          isPrint: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.edit === method) {
      this.handleEdit(rowData?.id);
    } else if (appConst.active.delete === method) {
      this.handleDelete(rowData?.id);
    } else if (appConst.active.view === method) {
      this.handleView(rowData?.id);
    } else if (appConst.active.print === method) {
      this.handlePrint(rowData?.id);
    } else {
      alert("Call Selected Here:" + rowData?.id);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0
  }

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus(this.state.type)
      .then(({ data }) => {
        let countStatusProcessing,
          countStatusApprovedCategory,
          countStatusApproved,
          countStatusEnd;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusVerificationPlanning[0].code === item?.trangThai) {
            countStatusProcessing = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[1].code === item?.trangThai) {
            countStatusApprovedCategory = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[2].code === item?.trangThai) {
            countStatusApproved = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusVerificationPlanning[3].code === item?.trangThai) {
            countStatusEnd = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusProcessing,
            countStatusApprovedCategory,
            countStatusApproved,
            countStatusEnd
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  checkStatus = (status, name) => {
    let itemStatus = appConst[name].find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  handleGetRowData = async (rowData) => {
    let { t } = this.props
    try {
      const res = await getListProductByIds({
          ...appConst.OBJECT_SEARCH_MAX_SIZE,
          purchasePlanId: rowData?.id
      })
      if (res?.data?.code === appConst?.CODE?.SUCCESS) {
        this.setState({
          rowDataPuschasePlaningTable: res?.data?.data?.content ?? [],
          rowDataClick: rowData
        })
      } else {
        toast.error(t("toastr.error"))
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    }
  }

  handleDateChange = (date, name) => {
    if ((["fromPlanDate", "toPlanDate"].includes(name) && isValidDate(date)) || date === null) {
      this.setState({ [name]: formatDateDto(date) },
        () => {
          this.search();
        });
    } else {
    }
  };

  handleGetProductStatus = (rowData) => {
    const { DA_DUYET, CHO_DUYET, KHONG_DUYET } = appConst.listStatusProductInPurchaseRequest;
    const statusIndex = rowData?.status;
    const classByStatusIndex = {
      [CHO_DUYET.code]: "status-warning",
      [DA_DUYET.code]: "status-success",
      [KHONG_DUYET.code]: "status-error",
      [null]: "",
    }
    const nameByStatusIndex = {
      [CHO_DUYET.code]: CHO_DUYET.name,
      [DA_DUYET.code]: DA_DUYET.name,
      [KHONG_DUYET.code]: KHONG_DUYET.name,
      [null]: "",
    }

    return (
      <span className={clsx("status", classByStatusIndex[statusIndex])}>
        {nameByStatusIndex[statusIndex]}
      </span>
    )
  }
  
  handleSelectRow =(rows, rowData) => {
    let {selectedList = []} = this.state;
    let isExist = selectedList?.some(x => x.id === rowData?.id);
    
    if (isExist) {
      selectedList?.splice(selectedList.indexOf(rowData), 1)
    } else {
      selectedList?.push(rowData);
    }
    
    this.setState({
      selectedList,
    })
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasPrintPermission,
      isRoleAdmin,
      isRoleAssetManager,
      rowDataPuschasePlaningTable,
      rowDataClick
    } = this.state;
    let TitlePage = t("purchasePlaning.title");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasWatchPermission={hasWatchPermission}
            hasPrintPermission={hasPrintPermission}
            isRoleAdmin={isRoleAdmin}
            isRoleAssetManager={isRoleAssetManager}
            onSelect={(rowData, method) =>
              this.handleCheckIcon(rowData, method)
            }
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("SuggestedMaterials.ballotCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
      },
      {
        title: t("purchaseRequestCount.name"),
        field: "name",
        align: "left",
        minWidth: "150px",
      },
      {
        title: t("purchasePlaning.planingDate"),
        field: "planDate",
        minWidth: 150,
        align: "center",
        render: (rowData) =>
          rowData?.planDate
            ? (<span>{moment(rowData?.planDate).format("DD/MM/YYYY")}</span>)
            : (""),
      },
      {
        title: t("purchasePlaning.approvalDate"),
        field: "approvedDate",
        minWidth: 150,
        align: "center",
        render: (rowData) => formatTimestampToDate(rowData?.approvedDate) || "",
      },
      {
        title: t("purchasePlaning.quoteApprovedDate"),
        field: "approvedDate",
        minWidth: 150,
        align: "center",
        render: (rowData) => formatTimestampToDate(rowData?.quotationApprovalDate) || "",
      },
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        minWidth: 180,
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span
              className={classStatus(statusIndex)}
            >
              {this.checkStatus(statusIndex, "listStatusPurchasePlaning")}
            </span>
          )
        }
      },
      {
        title: t("purchasePlaning.name"),
        field: "name",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.planingDepartment"),
        field: "planDepartmentName",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.planer"),
        field: "planPersonName",
        minWidth: 200,
      },
      {
        title: t("purchasePlaning.totalEstimatedCosts"),
        field: "totalEstimatedCost",
        align: "left",
        minWidth: 200,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (convertNumberPriceRoundUp(rowData?.totalEstimatedCost || 0))
      },
      {
        title: t("purchasePlaning.note"),
        field: "note",
        minWidth: 200,
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: '50px',
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "productName",
        minWidth: 250,
      },
      {
        title: t("SparePart.unit"),
        field: "skuName",
        align: "center",
        minWidth: 70,
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "center",
        minWidth: 90,
      },
      {
        title: t("purchaseRequestCount.currentQuantity"),
        field: "approvedQuantity",
        align: "center",
        minWidth: 90,
      },
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.estimatedUnitPrice"),
          field: "estimatedUnitPrice",
          align: "center",
          minWidth: 200,
          render: (rowData) => convertNumberPriceRoundUp(rowData?.estimatedUnitPrice || 0)
        },
      ] : []),
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.estimatedCosts"),
          field: "estimatedCosts",
          align: "right",
          minWidth: 200,
          render: (rowData) => (
            convertNumberPriceRoundUp(rowData.estimatedUnitPrice * rowData.approvedQuantity)
          ),
        },
      ] : []),
      {
        title: t('Product.formShopping'),
        field: "shoppingFormName",
        minWidth: 200,
      },
      ...(rowDataClick?.status !== appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder ? [
        {
          title: t("Product.supplier"),
          field: "supplierName",
          minWidth: 200,
        },
      ] : []),
      {
        title: t("maintainRequest.status"),
        field: "status",
        align: "center",
        hidden: rowDataClick?.indexOrder === appConst?.listStatusPurchasePlaningObject.DA_DUYET_DANH_MUC.indexOrder,
        minWidth: 150,
        render: this.handleGetProductStatus,
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.purchase"), path: "/list/purchase_planing" },
              { name: t("Dashboard.Purchase.purchase_asset") },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" value={appConst.tabPurchasePlaning.tabAll} label={t("purchasePlaning.all")} />
            <Tab
              className="tab"
              value={appConst.tabPurchasePlaning.tabPlan}
              label={
                <div className="tabLable">
                  <span>
                    <span>{t("purchasePlaning.processing")}</span>
                    <div className="tabQuantity tabQuantity-warning">
                      {this.state?.countStatusProcessing || 0}
                    </div>
                  </span>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.tabPurchasePlaning.tabProcessed}
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.approved")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusApprovedCategory || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.tabPurchasePlaning.tabQuoteApproved}
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.quoteApproved")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusApproved || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.tabPurchasePlaning.tabEnd}
              label={
                <div className="tabLable">
                  <span>{t("purchasePlaning.finished")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countStatusEnd || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <TabPanel
          value={tabValue}
          index={tabValue}
          className="mp-0"
        >
          <ComponentPurchasePlaningTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleDateChange={this.handleDateChange}
            exportToExcel={this.exportToExcel}
            handleSelectRow={this.handleSelectRow}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataPuschasePlaningTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '350px',
              minBodyHeight: '260px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}
PurchasePlaningTable.contextType = AppContext;
export default PurchasePlaningTable;
