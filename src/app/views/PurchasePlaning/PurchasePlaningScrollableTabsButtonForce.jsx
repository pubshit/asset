import React, { useEffect } from "react";
import {
  IconButton,
  Icon,
  Grid,
  TextField,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import NumberFormat from 'react-number-format';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";

import { searchByPage as ShoppingFormSearchByPage} from "../ShoppingForm/ShoppingFormService";
import {searchByPage as SupplierSearchByPage} from '../Supplier/SupplierService'
import { getAllStockKeepingUnits } from "../StockKeepingUnit/StockKeepingUnitService";
import { getListManagementDepartment, getListUserByDepartmentId } from './PurchasePlaningService';
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import viLocale from "date-fns/locale/vi";
import { useState } from "react";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { LightTooltip, TabPanel, a11yProps, convertNumberPriceRoundUp, isCheckLenght } from "app/appFunction";
import { appConst } from "app/appConst";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function TransferScrollableTabsButtonForce(props) {
  const t = props.t
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: "" });
  let status = appConst.listStatusPurchasePlaning.find(status => status.indexOrder === props?.item?.status) || "";
  let departmentObj = {text: props.item.planDepartmentName, id: props.item.planDepartmentId}
  const searchObject = { pageIndex: 1, pageSize: 1000000 };
  const filterAutocomplete = createFilterOptions();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.plantDepartment?.id  ?? ''
    });


  }, [props?.item?.plantDepartment]);



  useEffect(() => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !isCheckLenght(value, 255)
    })
  }, [props?.item?.assetVouchers]);

  let columns = [
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.isView)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData?.id);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    },
    {
      title: t("purchaseRequestCount.productPortfolio"),
      field: "productName",
      minWidth: 250,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("Product.approvedQuanlity"),
      field: "quantity",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("Product.actualQuantity"),
      field: "quantity",
      align: "center",
      minWidth: 150,
      maxWidth: 400,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("Product.stockKeepingUnit"),
      field: "skuName",
      align: "center",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
        (props?.isView ?
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData.sku ? rowData.sku : ''}
          /> : <AsynchronousAutocompleteTransfer
            searchFunction={getAllStockKeepingUnits}
            searchObject={searchObject}
            displayLable={'name'}
            typeReturnFunction="category"
            value={rowData ? rowData?.sku : null}
            defaultValue={rowData?.sku}
            onSelect={props.selectStockKeepingUnit}
            onSelectOptions={rowData}
          />
      )
    },
    {
      title: t("Product.estimatedUnitPrice"),
      field: "estimatedUnitPrice",
      align: "center",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) =>
        rowData?.estimatedUnitPrice
          ? convertNumberPriceRoundUp(rowData?.estimatedUnitPrice)
          : "",
    },
     {
      title: t("Product.estimatedCosts"),
      field: "estimatedCosts",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
      ((props?.isView) ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
           value={rowData ? rowData?.estimatedCosts : null}
        /> : <TextValidator
          className="w-100"
          type="number"
          value={rowData?.estimatedCosts || ""}
          onChange={(e) => props.handleChangeEstimatedCosts(e.target.value, rowData)}
          validators={["isNumber"]}
          errorMessages={[t("purchasePlaning.is_number")]}
        />
      ) 
    },
    {
      title: 'HTMS',
      minWidth: 230,
      field: "shoppingForm",
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
      ((props?.isView) ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
          value={rowData?.shoppingForm ? rowData?.shoppingForm : ''}
        /> : <AsynchronousAutocompleteTransfer
          searchFunction={ShoppingFormSearchByPage}
          searchObject={searchObject}
          displayLable={'name'}
          typeReturnFunction="category"
          value={rowData ? rowData?.shoppingForm : null}
          onSelect={props.selectPurchaseForm}
          onSelectOptions={rowData}
        />
      )
    },
    {
      title: t("Product.supplier"),
      field: "suppilerDepartment",
      align: "left",
      minWidth: 180,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: rowData =>
      ((props?.isView) ?
        <TextValidator
          className="w-100"
          InputProps={{
            readOnly: true,
          }}
          value={rowData.suppilerDepartment ? rowData.suppilerDepartment : ''}
        /> : <AsynchronousAutocompleteTransfer
          searchFunction={SupplierSearchByPage}
          searchObject={searchObject}
          displayLable={'name'}
          typeReturnFunction="category"
          value={rowData ? rowData?.suppilerDepartment : null}
          onSelect={props.selectSuppiler}
          onSelectOptions={rowData}
        />
      )
    },
    {
      title: t("maintainRequest.status"),
      field: "status",
      align: "center",
      minWidth: 180,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) => {
        const statusIndex = rowData?.status;
        if (statusIndex === 0) {
          return (
            <span
              className="status status-success"
            >
              {t("purchasePlaning.bought")}
            </span>
          )
        } else {
          return (
            <span
              className="status status-warning"
            >
              {t("purchasePlaning.notBought")}
            </span>
          )
      }}}
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      headerStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData =>
      // ((!props?.isView) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];
  return (
    <form>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} type="submit" />
            <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={1} justifyContent="space-between">
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <DateTimePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("purchasePlaning.planingDate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={'createDate'}
                  value={props?.item.planDate}
                  InputProps={{
                    readOnly: true,
                  }}
                  readOnly={true}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog mt-2"
                  label={
                  <span className="w-100">
                    <span className="colorRed">*</span>
                    {t("purchasePlaning.approvalDate")}
                  </span>
                }
                  name={'approvedDate'}
                  inputVariant="standard"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.item?.approvedDate}                  
                  onChange={date => props?.handleDateChange(date, "approvedDate")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  InputProps={{
                    readOnly: props?.isView
                  }}
                  open={isPickerOpen}
                  onOpen={() => {
                    if (props?.isView) {
                      setIsPickerOpen(false);
                    }
                    else {
                      setIsPickerOpen(true);
                    }
                  }}
                  onClose={() => setIsPickerOpen(false)}
                  validators={['required']}
                  errorMessages={t('general.required')}
                  minDate={new Date("01/01/1900")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={4} sm={6} xs={6}>
             <TextValidator
                className="w-100"
                label={
                    <span>
                      <span className="colorRed">* </span>
                      <span>{t("purchasePlaning.name")}</span>
                    </span>
                  }
                name="name"
                value={props?.item?.name}
                onChange={props?.handleChange}
              />
            </Grid>
              <Grid item md={4} sm={12} xs={12}>
              {props?.isView ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("maintainRequest.status")}
                    </span>
                  }
                  value={props.status ? props?.status?.name : ""}
                /> : 
                    <Autocomplete
                      fullWidth
                      options={appConst.listStatusPurchasePlaning}
                      value={props?.item?.status ? props?.item?.status?.name : ""}
                      defaultValue={status}
                      onSelect={props?.handleSelectStatus}
                      getOptionLabel={(option) => option?.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label={
                            <span>
                              <span className="colorRed">* </span>
                              {t("maintainRequest.status")}
                            </span>
                          }
                          variant="standard"
                        />
                      )}
                      filterOptions={(options, params) => {
                        params.inputValue = params.inputValue.trim()
                        let filtered = filterAutocomplete(options, params)
                        return filtered
                      }}
                      noOptionsText={t("general.noOption")}
                      validators={["required"]}
                      errorMessages={[t('general.required')]}
                    />
                }
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {props?.isView ?
                <TextValidator
                  className="w-100"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>
                  }
                  value={props?.item?.planDepartmentId ? props?.item?.planDepartmentName : ''}
                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      <span>{t("purchasePlaning.planingDepartment")}</span>
                    </span>
                  }
                  searchFunction={getListManagementDepartment}
                  searchObject={searchObject}
                  displayLable={"text"}
                  defaultValue={props?.item?.plantDepartment ? props?.item?.plantDepartment : departmentObj }
                  value={props?.item?.plantDepartment ? props?.item?.plantDepartment : departmentObj }
                  onSelect={handoverDepartment => props?.handleSelectHandoverDepartment(handoverDepartment)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
                }
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
              {(props?.isView) ?
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planer")}
                    </span>
                  }
                  name="handoverPerson"
                  value={props?.item?.handoverPersonName ?? ''}
                  InputProps={{
                    readOnly: props?.isView,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                /> : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("purchasePlaning.planer")}
                    </span>
                  }
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByHandoverDepartment}
                  defaultValue={props?.item?.planer ?? ''}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={props?.item?.planer ?? ''}
                  onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              }
            </Grid>
            {/* <Grid item md={4} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                name="totalEstimatedCost"
                label={t("purchasePlaning.totalEstimatedCosts")}
                value={props?.item?.totalEstimatedCost}
                onChange={props?.handleChange}
              />
            </Grid> */}
            <Grid item md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                name="note"
                label={t("maintainRequest.note")}
                value={props?.item?.note}
                onChange={props?.handleChange}
              />
            </Grid>
            </Grid>
          <Grid spacing={2}>
            <MaterialTable
              style={{marginTop: 10}}
              data={props?.item?.purchaseProducts ? props?.item?.purchaseProducts : []}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF',
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '220px',
                minBodyHeight: '220px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props?.item?.documents || []}
              columns={columnsVoucherFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF',
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '285px',
                minBodyHeight: '285px',
              }}
              components={{
                Toolbar: props => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div >
    </form>
  );
}