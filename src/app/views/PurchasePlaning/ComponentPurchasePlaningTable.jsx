import {
    Button,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination
} from "@material-ui/core";
import React from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import { appConst } from "app/appConst";
import PlaningDialog from "./PlaningDialog";
import { defaultPaginationProps } from "app/appFunction";

function ComponentPurchasePlaningTable(props) {
    let { t, i18n, getRowData } = props;
    let {
        rowsPerPage,
        page,
        totalElements,
        itemList,
        item,
        shouldOpenConfirmationDialog,
        shouldOpenEditorDialog,
        shouldOpenConfirmationDeleteAllDialog,
        isView,
        rowDataClick
    } = props?.item;
    return (
        <>
            <Grid container spacing={2}>
                {shouldOpenConfirmationDeleteAllDialog && (
                    <ConfirmationDialog
                        open={shouldOpenConfirmationDeleteAllDialog}
                        onConfirmDialogClose={props.handleDialogClose}
                        onYesClick={props.handleDeleteAll}
                        text={t("general.deleteAllConfirm")}
                    />
                )}
                <Grid item md={2} sm={3} xs={12} style={{ display: "flex", alignItems: "flex-end" }}>
                    {(
                        <Button
                            className="align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.exportToExcel}
                        >
                            {t("general.exportToExcel")}
                        </Button>
                    )}
                </Grid>

                <Grid item md={2} sm={3} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                            className="w-100"
                            id="mui-pickers-date"
                            label={t("purchase_request_count.formDate")}
                            type="text"
                            autoOk={true}
                            format="dd/MM/yyyy"
                            name={"fromPlanDate"}
                            value={props.item?.fromPlanDate}
                            invalidDateMessage={t("general.invalidDateFormat")}
                            maxDateMessage={props.item?.toPlanDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                            minDateMessage={t("general.minYearDefault")}
                            maxDate={props.item?.toPlanDate ? (new Date(props.item?.toPlanDate)) : undefined}
                            clearable
                            onChange={(date) => props.handleDateChange(date, "fromPlanDate")}
                            clearLabel={t("general.remove")}
                            cancelLabel={t("general.cancel")}
                            okLabel={t("general.select")}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={2} sm={3} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                            className="w-100"
                            id="mui-pickers-date"
                            label={t("purchase_request_count.formToDate")}
                            type="text"
                            autoOk={true}
                            format="dd/MM/yyyy"
                            name={"toPlanDate"}
                            value={props.item?.toPlanDate}
                            invalidDateMessage={t("general.invalidDateFormat")}
                            maxDateMessage={t("general.maxDateDefault")}
                            minDateMessage={props.item?.fromPlanDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                            minDate={props.item?.fromPlanDate ? (new Date(props.item?.fromPlanDate)) : undefined}
                            clearable
                            onChange={(date) => props.handleDateChange(date, "toPlanDate")}
                            clearLabel={t("general.remove")}
                            cancelLabel={t("general.cancel")}
                            okLabel={t("general.select")}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={6} sm={12} xs={12}
                    style={{ display: "flex", alignItems: "end" }}>
                    <FormControl fullWidth >
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("purchasePlaning.search")}
                            id="search_box"
                            startAdornment={
                                <InputAdornment position="end">
                                    <SearchIcon
                                        onClick={() => props.search()}
                                        className="searchTable"
                                    />
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <div>
                        {shouldOpenEditorDialog && (
                            <PlaningDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                isView={isView}
                            />
                        )}
                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                    </div>
                    <MaterialTable
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                          selection: true,
                          actionsColumnIndex: -1,
                          paging: false,
                          search: false,
                          sorting: false,
                          draggable: false,
                          showSelectAllCheckbox: false,
                          rowStyle: (rowData) => ({
                            backgroundColor:
                              rowDataClick?.id === rowData?.id ?
                                "#ccc" :
                                rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                          }),
                          maxBodyHeight: "490px",
                          minBodyHeight: "260px",
                          headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                          },
                          padding: "dense",
                          toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={props.handleSelectRow}
                    />
                    <TablePagination
                        {...defaultPaginationProps()}
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default ComponentPurchasePlaningTable;
