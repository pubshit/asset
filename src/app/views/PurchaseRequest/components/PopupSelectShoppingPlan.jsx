import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton } from '@material-ui/core';
import MaterialTable, { MTableToolbar } from 'material-table'
import React, { useCallback, useContext, useEffect, useState } from 'react'
import { PaperComponent } from '../../../appFunction';
import { getListProductShoppingPlan } from "../../ShoppingPlan/PurchaseRequestService";
import { appConst } from 'app/appConst';
import AppContext from "../../../appContext";

function PopupSelectShoppingPlan(props) {
    let {
        t,
        open,
        handleClose,
        handleSelect,
        item,
    } = props;
    const [state, setState] = useState({
        keyword: "",
        page: 0,
        rowsPerPage: 10,
        totalElements: 0,
        toggleSearch: false,
        data: [],
        products: [],
        productSelectedBefore: [],
        productTypeCodes: [],
        ...item,
    });
    const [pageLoading, setPageLoading] = useState(false);
    console.log(1);
    const updatePageData = async () => {
        try {
            setPageLoading(true);
            const params = {
              ...appConst.OBJECT_SEARCH_MAX_SIZE,
              productTypeCodes: state?.productTypeCodes?.join(","),
              managementPurchaseDepartmentId: state?.managementPurchaseDepartmentId,
            }
            const data = await getListProductShoppingPlan(state?.id, params);
            let _data = data?.data?.data?.content;
            let listProductsInShoppingPlan = [];
            _data = _data?.length ? _data : [];
            _data?.forEach(item => {
                let isExists = state?.productSelected?.find(x => x?.productSelected?.id === item?.id);
                item.tableData = {
                    ...item?.tableData,
                    checked: Boolean(isExists)
                };
                if (isExists) {
                    listProductsInShoppingPlan.push(item);
                }
            })
            setState((pre) => ({ ...pre, data: [..._data], productSelectedBefore: listProductsInShoppingPlan, products: listProductsInShoppingPlan }))
        } catch (error) {
            console.log(error);
        } finally {
            setPageLoading(false);
        }
    };

    useEffect(() => {
        if (state?.id) {
            updatePageData();
        }
    }, [state?.toggleSearch]);

    const handleSelection = (rows) => {
        setState((pre) => ({ ...pre, products: rows }))
    };

    let columns = [
        {
            title: t("general.index"),
            field: "index",
            align: "center",
            maxWidth: "60px",
            render: rowData => rowData.tableData.id + 1,
        },
        {
            title: t("Product.code"),
            field: "code",
            align: "center",
            minWidth: "150px",
            render: rowData => rowData?.code
        },
        {
            title: t("Product.name"),
            field: "name",
            align: "left",
            minWidth: "350px",
        },
        {
            title: t("ShoppingPlan.totalQuantity"),
            field: "requiredQty",
            align: "left",
            minWidth: "100px",
            cellStyle: {
                textAlign: "center",
            },
            render: (rowData) => {
                let remainQty = (rowData?.requiredQty || 0);

                remainQty = remainQty % 1 === 0
                    ? remainQty?.toString()
                    : parseFloat(remainQty?.toFixed(2));

                return remainQty;
            }
        },
        {
            title: t("ShoppingPlan.remainQuantity"),
            field: "remainQuantity",
            align: "left",
            minWidth: "100px",
            cellStyle: {
                textAlign: "center",
            },
            render: (rowData) => {
                let remainQty = (rowData?.quantity || 0) - (rowData?.approvedQty || 0);

                remainQty = remainQty % 1 === 0
                    ? remainQty?.toString()
                    : parseFloat(remainQty?.toFixed(2));

                return remainQty;
            }
        },
    ];
    const titleDialog = state?.name ? `Danh sách sản phẩm trong ${state?.name}` : "Danh sách sản phẩm";

    const handleChangeOnSelection = useCallback((rows = [], rowData) => {
        let { products = [] } = state;
        let newArray = products || [];

        if (rowData) {
            const existItem = products?.some(x => x.id === rowData?.id);

            if (existItem) {
                newArray.splice(products.indexOf(rowData), 1);
            } else {
                newArray.push(rowData);
            }
        } else {
            newArray = rows;
        }

        setState((pre) => ({ ...pre, products: newArray }))
    }, [state.products]);

    return (
        <Dialog
            open={open}
            PaperComponent={PaperComponent}
            maxWidth="lg"
            fullWidth
            scroll={"paper"}
        >
            <DialogTitle className="cursor-move">
                {titleDialog}
                <IconButton
                    onClick={() => handleClose()}
                    style={{ position: "absolute", top: 0, right: 0 }}
                >
                    <Icon className="text-black">clear</Icon>
                </IconButton>
            </DialogTitle>
            <DialogContent style={{ minHeight: "450px" }}>
                <MaterialTable
                    title={t("general.list")}
                    data={state?.data?.length ? state?.data : []}
                    columns={columns}
                    isLoading={pageLoading}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t(
                                "general.emptyDataMessageTable"
                            )}`,
                        },
                        toolbar: {
                            nRowsSelected: `${t("general.selects")}`,
                        },
                        pagination: appConst.localizationVi.pagination
                    }}
                    options={{
                        selection: true,
                        pageSize: 10,
                        pageSizeOptions: appConst.rowsPerPageOptions.popup,
                        actionsColumnIndex: -1,
                        paging: true,
                        search: false,
                        sorting: false,
                        draggable: false,
                        rowStyle: (rowData) => ({
                            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: 400,
                        minBodyHeight: "340px",
                        padding: "dense",
                        toolbar: false,
                        selectionProps: {
                            align: "center",
                            style: {
                                width: 28,
                            }
                        },
                    }}
                    onSelectionChange={(rows, rowData) => {
                        handleSelection(rows);
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    className="mb-16 align-bottom"
                    variant="contained"
                    color="secondary"
                    onClick={() => handleClose()}
                >
                    {t("general.cancel")}
                </Button>
                <Button
                    className="mb-16 mr-12 align-bottom"
                    variant="contained"
                    color="primary"
                    onClick={() => handleSelect(state?.products, state?.productSelectedBefore)}
                >
                    {t("general.select")}
                </Button>
            </DialogActions>
        </Dialog>

    )
}

export default PopupSelectShoppingPlan
