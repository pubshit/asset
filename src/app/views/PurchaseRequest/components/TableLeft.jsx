import { TablePagination } from '@material-ui/core'
import React from 'react';
import {
    defaultPaginationProps,
    formatToDateEndDate
} from "../../../appFunction";
import { appConst } from '../../../appConst';
import MaterialTable, { MTableToolbar } from 'material-table';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles';

const ActiveBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        '&::after': {
            position: 'absolute',
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}))(Badge);

const UnActiveBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#f44336',
        color: '#f44336',
        '&::after': {
            position: 'absolute',
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            border: '1px solid currentColor',
            content: '""',
        },
    },
}))(Badge);

function TableLeft(props) {
    let {
        t,
        item,
        data = [],
        onRowClick = () => { },
        selectedItemPlan = {}
    } = props;
    let {
        subTotalElements,
        subRowsPerPage,
        subPage,
    } = item;
    let columns = [
        {
            title: t("TT"),
            field: "isActive",
            align: "center",
            maxWidth: "40px",
            render: rowData => {
                let ComponentSelect = rowData?.isActive ? ActiveBadge : UnActiveBadge;
                return (
                    <ComponentSelect
                        overlap="circular"
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        variant="dot"
                    >
                    </ComponentSelect>
                )
            }
        },
        {
            title: t("ShoppingPlan.nameSort"),
            field: "name",
            align: "left",
            minWidth: "200px",
        },
        {
            title: t("ShoppingPlan.codeSort"),
            field: "code",
            align: "center",
            minWidth: "200px",
        },
        {
            title: t("ShoppingPlan.time"),
            field: "requestDate",
            align: "center",
            minWidth: "200px",
            render: (rowData) => (
                <span dangerouslySetInnerHTML={formatToDateEndDate(rowData?.applicationPeriodFrom, rowData?.applicationPeriodTo)} />
            )
        },
    ];
    return (
        <>
            <MaterialTable
                title={t("general.list")}
                data={data}
                columns={columns}
                localization={{
                    body: {
                        emptyDataSourceMessage: `${t(
                            "general.emptyDataMessageTable"
                        )}`,
                    },
                    toolbar: {
                        nRowsSelected: `${t("general.selects")}`,
                    },
                }}
                options={{
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    draggable: false,
                    rowStyle: (rowData) => ({
                        backgroundColor:
                            selectedItemPlan?.tableData?.id === rowData?.tableData?.id
                                ? "#ccc" :
                                rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    maxBodyHeight: "340px",
                    minBodyHeight: "340px",
                    headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                    },
                    padding: "dense",
                    toolbar: false,
                }}
                onRowClick={(e, rowData) => {
                    onRowClick(rowData)
                }}
                components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                }}
            />
            <TablePagination
                {...defaultPaginationProps()}
                rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                count={subTotalElements}
                rowsPerPage={subRowsPerPage}
                page={subPage}
                onPageChange={props.handleChangeSubPage}
                onRowsPerPageChange={props.setSubRowsPerPage}
            />
        </>
    )
}

export default TableLeft
