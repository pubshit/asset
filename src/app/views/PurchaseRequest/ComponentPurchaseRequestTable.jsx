import {
  Badge,
  Button,
  Card,
  CardContent,
  ClickAwayListener,
  Collapse,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  TablePagination
} from "@material-ui/core";
import React, { useContext, useState } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import PurchaseRequestEditorDialog from "./PurchaseRequestEditorDialog";
import PurchaseRequestCountDialog from "../PurchaseRequestCount/Dialog/PurchaseRequestCountDialog";
import {
  handleKeyDown,
  handleKeyUp,
  filterOptions,
} from "app/appFunction";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  searchByPage as searchByPageDepartment,
} from "../Department/DepartmentService";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { getUserInformation } from "../../appFunction";
import { LIST_PRINT_FORM_BY_ORG, printStyleText } from "../FormCustom/constant";
import AppContext from "../../appContext";
import { appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL } from "../../appConst";
import { exportToWord } from "./PurchaseRequestService";
import { purchaseRequestPrintData } from "../FormCustom/PurchaseRequest";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import TableLeft from "./components/TableLeft"
import SwitchDepartmentPopup from "../Component/PurchasingManagement/SwitchDepartmentPopup";

function ComponentPurchaseRequestTable(props) {
  let {
    t,
    i18n,
    getRowData,
    handleEditItem,
    handlePurchaseReqCount,
    handleDialogClose,
    handleDeleteAll,
    handleTextChange,
    search,
    handleOKEditClose,
    handleCountDialogClose,
    handleOKCountClose,
    handleConfirmationResponse,
    columns,
    columnsNoAction,
    handleChangePage,
    setRowsPerPage,
    setSelectedItems,
    updatePageData,
    onRowShoppingPlanClick,
    handleChangeSubPage,
    setSubRowsPerPage,
    handleSwitchDepartment,
  } = props;
  let {
    itemList,
    hasDeletePermission,
    hasEditPermission,
    shouldOpenNotificationPopup,
    Notification,
    shouldOpenConfirmationDeleteAllDialog,
    shouldOpenConfirmationDeleteListDialog,
    shouldOpenEditorDialog,
    item,
    shouldOpenCountDialog,
    itemCount,
    shouldOpenConfirmationDialog,
    totalElements,
    rowsPerPage,
    page,
    tabValue,
    isRoleAssetManager,
    purchaseRequests,
    isPrint,
    openAdvanceSearch,
    fromDate,
    toDate,
    selectedList,
    isExportExcelByVoucher,
    selectedItem,
    isRoleOrgAdmin,
    selectedItemPlan,
    listItemsShoppingPlan,
    shouldOpenSwitchDepartmentPopup,
  } = props?.item;
  const { ASSET_PURCHASING } = LIST_PRINT_FORM_BY_ORG.SHOPPING_MANAGEMENT;
  const { currentOrg } = useContext(AppContext);
  let searchObject = {
    keyword: "",
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };
  let [listRequestDepartment, setListRequestDepartment] = useState([])
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSetDataSelect = (data, source) => {
    setListRequestDepartment(data?.length > 0 ? data : [])
  }

  const handleCheckTabValue = (tabValue) => {
    const isMatchTabValue = [
      appConst.tabPurchaseRequest.tabCounted,
      appConst.tabPurchaseRequest.tabPlaned
    ];

    return isMatchTabValue.includes(tabValue);
  }

  let dataView = purchaseRequestPrintData(item);
  return (
    <Grid container spacing={2} justifyContent="space-between" className="mt-10">
      <Grid item md={7} xs={12}>
        {hasEditPermission && !handleCheckTabValue(tabValue) && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => {
              handleEditItem({
                startDate: new Date(),
                endDate: new Date(),
                isView: false,
                shoppingPlan: selectedItemPlan?.isActive && {
                  id: selectedItemPlan?.id,
                  name: selectedItemPlan?.name
                },
              });
            }}
          >
            {t("general.addRequest")}
          </Button>
        )}
        {(isRoleAssetManager || isRoleOrgAdmin) && !handleCheckTabValue(tabValue) && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={!purchaseRequests || purchaseRequests?.length <= 0}
            onClick={handlePurchaseReqCount}
          >
            {t("purchase_request_count.title")}
          </Button>
        )}
        {(isRoleAssetManager || isRoleOrgAdmin) && (
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={!purchaseRequests || purchaseRequests?.length <= 0}
            onClick={handleSwitchDepartment}
          >
            {t("purchaseRequest.switchDepartment")}
          </Button>
        )}
        {!isExportExcelByVoucher ? (
          <Button
            className="mb-16 mr-16"
            variant="contained"
            color="primary"
            onClick={handleClick}
          >
            {t("general.exportToExcel")}
          </Button>
        ) : (
          <Badge
            badgeContent={selectedList?.length ?? 0}
            className="mb-16 mr-30"
            color="secondary"
          >
            <Button
              variant="contained"
              color="primary"
              onClick={() => props.handleExportToExcel(null)}
            >
              {t("general.exportToExcel")}&nbsp;Theo bản ghi
            </Button>
          </Badge>
        )}
        <Button
          className="mb-16 align-bottom"
          variant="contained"
          color="primary"
          onClick={props.handleOpenAdvanceSearch}
        >
          {t("general.advancedSearch")}
          <ArrowDropDownIcon />
        </Button>

        <Popper
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          transition
          style={{ zIndex: 999 }}
        >
          <Paper>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList>
                {appConst.listOptionsExcelPurchaseRequest.map((item, i) => (
                  <MenuItem key={i} onClick={(e) => {
                    handleClose();
                    props.handleExportToExcel(item.code);
                  }}>{item.name}</MenuItem>
                ))}
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Popper>
        
        {shouldOpenSwitchDepartmentPopup && (
          <SwitchDepartmentPopup
            t={t}
            open={shouldOpenSwitchDepartmentPopup}
            handleClose={handleOKEditClose}
            ids={purchaseRequests?.map(x => x.id)}
          />
        )}
        
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={handleDialogClose}
            text={t(Notification)}
            agree={t("general.agree")}
          />
        )}

        {shouldOpenConfirmationDeleteAllDialog && (
          <ConfirmationDialog
            open={shouldOpenConfirmationDeleteAllDialog}
            onConfirmDialogClose={handleDialogClose}
            onYesClick={handleDeleteAll}
            text={t("general.deleteAllConfirm")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
        {shouldOpenConfirmationDeleteListDialog && (
          <ConfirmationDialog
            open={shouldOpenConfirmationDeleteListDialog}
            onConfirmDialogClose={handleDialogClose}
            onYesClick={handleDeleteAll}
            text={t("general.deleteConfirm")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
      </Grid>
      <Grid item md={5} sm={12} xs={12}>
        <FormControl fullWidth>
          <Input
            className="search_box w-100"
            onChange={handleTextChange}
            onKeyDown={(e) => handleKeyDown(e, search)}
            onKeyUp={(e) => handleKeyUp(e, search)}
            placeholder={t("purchaseRequest.search")}
            id="search_box"
            startAdornment={
              <InputAdornment position="end">
                <SearchIcon
                  onClick={() => search()}
                  className="searchTable"
                />
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      {/* Bộ lọc Tìm kiếm nâng cao */}
      <Grid item xs={12} className="pt-0 pb-0">
        <ValidatorForm onSubmit={() => { }}>
          <Collapse in={openAdvanceSearch}>
            <Card elevation={1}>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item lg={3} md={4} xs={12}>
                    <AsynchronousAutocompleteSub
                      className="w-100"
                      label={
                        <span>
                          {t("purchaseRequest.requestDepartment")}
                        </span>
                      }
                      searchFunction={searchByPageDepartment}
                      searchObject={searchObject}
                      listData={listRequestDepartment || []}
                      setListData={(data) =>
                        handleSetDataSelect(data, "listRequestDepartment")
                      }
                      displayLable={"text"}
                      value={props?.item?.requestDepartmentFilter || null}
                      onSelect={(requestDepartmentFilter) =>
                        props.handleSelectRequestDepartment(requestDepartmentFilter)
                      }
                      // readOnly={!roles?.departmentUser?.id}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                  {/* Từ ngày */}
                  <Grid item lg={3} md={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("MaintainPlaning.dxFrom")}
                        autoOk
                        format="dd/MM/yyyy"
                        name={"fromDate"}
                        value={fromDate}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "fromDate"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateNow")}
                        minDateMessage={t("general.minDateDefault")}
                        maxDate={toDate ? (new Date(toDate)) : new Date()}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  {/* Đến ngày */}
                  <Grid item lg={3} md={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                      <KeyboardDatePicker
                        className="w-100"
                        id="mui-pickers-date"
                        label={t("MaintainPlaning.dxTo")}
                        autoOk
                        format="dd/MM/yyyy"
                        name={"fromPlanDate"}
                        value={toDate}
                        onChange={(data) => props.handleSetDataSelect(
                          data,
                          "toDate"
                        )}
                        KeyboardButtonProps={{ "aria-label": "change date", }}
                        invalidDateMessage={t("general.invalidDateFormat")}
                        maxDateMessage={t("general.maxDateNow")}
                        minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minDateDefault")}
                        minDate={fromDate ? (new Date(fromDate)) : undefined}
                        maxDate={new Date()}
                        clearable
                        clearLabel={t("general.remove")}
                        cancelLabel={t("general.cancel")}
                        okLabel={t("general.select")}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item lg={3} md={4} xs={12}>
                    <AsynchronousAutocompleteSub
                      className="w-100"
                      label={t("purchaseRequest.status")}
                      searchFunction={() => { }}
                      searchObject={{}}
                      listData={appConst.listStatusPurchaseRequest || []}
                      displayLable="name"
                      value={props?.item?.statusFilter || null}
                      onSelect={(value) => props.handleSetDataSelect(value, "statusFilter")}
                      disabled={tabValue !== appConst.tabPurchaseRequest.tabAll}
                      filterOptions={filterOptions}
                      noOptionsText={t("general.noOption")}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Collapse>
        </ValidatorForm>
      </Grid>
      <Grid container item xs={12} spacing={2}>
        <div>
          {shouldOpenEditorDialog && (
            <PurchaseRequestEditorDialog
              t={t}
              i18n={i18n}
              handleClose={handleDialogClose}
              open={shouldOpenEditorDialog}
              handleOKEditClose={handleOKEditClose}
              item={item}
              isCheck
              type={appConst.TYPE_PURCHASE.TSCD_CCDC}
              updatePageData={updatePageData}
            />
          )}

          {shouldOpenCountDialog && (
            <PurchaseRequestCountDialog
              t={t}
              i18n={i18n}
              handleClose={handleCountDialogClose}
              open={shouldOpenCountDialog}
              handleOKEditClose={handleOKCountClose}
              item={itemCount}
              type={appConst.TYPE_PURCHASE.TSCD_CCDC}
              setSelectedItems={setSelectedItems}
            />
          )}

          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={handleDialogClose}
              onYesClick={handleConfirmationResponse}
              text={t("general.deleteConfirm")}
              agree={t("general.agree")}
              cancel={t("general.cancel")}
            />
          )}
          {isPrint && (
            [LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV199.code, LIST_ORGANIZATION.PRODUCT_BV199.code].includes(currentOrg?.code) ? (
              <PrintPreviewTemplateDialog
                t={t}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={item}
                title={t("Phiếu xác định nhu cầu cần mua")}
                model={PRINT_TEMPLATE_MODEL.PURCHASE_MANAGEMENT.REQUEST}
              />
            ) : (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={isPrint}
                item={dataView || item}
                title={t("Phiếu xác định nhu cầu cần mua")}
                funcExportWord={exportToWord}
                payloadExport={{
                  id: dataView?.itemEdit?.id,
                  assetClass: dataView?.itemEdit?.assetClass,
                }}
                urlWord={
                  currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code
                    ? ASSET_PURCHASING.PURCHASE_REQUEST.urlPrintWord
                    : null
                }
                urls={[
                  ...ASSET_PURCHASING.PURCHASE_REQUEST.GENERAL,
                  ...ASSET_PURCHASING.PURCHASE_REQUEST[currentOrg?.printCode],
                ]}
              />
            )
          )}
        </div>
        <Grid item lg={4} md={12}>
          <TableLeft
            t={t}
            data={listItemsShoppingPlan}
            item={props?.item}
            onRowClick={onRowShoppingPlanClick}
            selectedItemPlan={selectedItemPlan}
            handleChangeSubPage={handleChangeSubPage}
            setSubRowsPerPage={setSubRowsPerPage}
          />
        </Grid>
        <Grid item lg={8} md={12}>
          <MaterialTable
            title={t("general.list")}
            data={itemList ? [...itemList] : []}
            columns={
              hasEditPermission || hasDeletePermission
                ? columns
                : columnsNoAction
            }
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
              toolbar: {
                nRowsSelected: `${t("general.selects")}`,
              },
            }}
            options={{
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              draggable: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  selectedItem?.id === rowData.id
                    ? "#ccc" :
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "340px",
              minBodyHeight: "340px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            onRowClick={(e, rowData) => {
              getRowData(rowData)
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            component="div"
            count={totalElements}
            labelRowsPerPage={t("general.rows_per_page")}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{ "aria-label": "Previous Page", }}
            nextIconButtonProps={{ "aria-label": "Next Page", }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={setRowsPerPage}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default ComponentPurchaseRequestTable;
