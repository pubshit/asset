import React, { Component } from "react";
import { Button, Dialog, DialogActions, Grid, } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import PurchaseRequestScrollableTabsButtonForce from "./PurchaseRequestScrollableTabsButtonForce";
import {
  addNewPurchaseRequest,
  deleteCkeckRequestComment,
  deleteRequestComment,
  exportToExcelV2,
  getOneRequestComment,
  searchByPageRequestComment,
  UpdatePurchaseRequest,
} from "./PurchaseRequestService";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  formatDateDto,
  functionExportToExcel,
  getRole,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../appFunction";
import { appConst, variable } from "../../appConst";
import localStorageService from "app/services/localStorageService";
import ProductDialog from "../Product/ProductDialog";
import { getNewCode } from "../Product/ProductService";
import { PaperComponent } from "../Component/Utilities";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import AppContext from "../../appContext";
import { convertPurchaseRequestPrintData } from "../FormCustom/AssetPurchasing/purchaseRequest";
import StockKeepingUnitEditorDialog from "../StockKeepingUnit/StockKeepingUnitEditorDialog";
import PopupSelectShoppingPlan from "./components/PopupSelectShoppingPlan"

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class PurchaseRequestEditorDialog extends Component {
  state = {
    shouldOpenNotificationPopup: false,
    shouldOpenRequestDepartmentPopup: false,
    requestDate: new Date(),
    requestDepartment: null,
    shouldOpenSelectDepartmentPopup: false,
    shouldOpenProductPopup: false,
    listProducts: [],
    receptionDepartment: null,
    status: null,
    note: "",
    page: 0,
    isDialog: true,
    comments: [],
    listRequestCommentId: [],
    listStatus: [],
    isView: false,
    shouldOpenCommentDialog: false,
    currentUser: null,
    productTypeCode: appConst.productTypeCode.TSCĐ, // sản phẩm là tài sản
    loading: false,
    listRowTable: [],
    shouldOpenDialogProduct: false,
    itemProduct: {},
    indexLinhKien: null,
    keywordSearch: {
      index: null,
      keywordSearch: ""
    },
    query: {
      pageIndex: 0,
      pageSize: 10,
      keyword: "",
      keySearch: ""
    },
    isSaveAndPrint: false,
    isSaveAndExport: false,
    tabValue: 0,
  };

  currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER)

  setQuery = (value) => {
    this.setState({
      query: value
    })
  }

  setShouldOpenDialogProduct = (value) => {
    this.setState({
      shouldOpenDialogProduct: value
    })
  }

  setKeyword = (value) => {
    this.setState({
      keywordSearch: value
    })
  }

  setIndexLinhKien = (value) => {
    this.setState({
      indexLinhKien: value
    })
  }

  handleOKEditClose = () => {
    this.setShouldOpenDialogProduct(false)
  };

  setItemProduct = (value, index) => {
    this.setState({
      itemProduct: value
    }, () => {
      this.setIndexLinhKien(index)
      this.setShouldOpenDialogProduct(true);
    })
  }

  handleProductDialogClose = () => {
    this.setShouldOpenDialogProduct(false)
    if (!this.state?.itemProduct?.id) {
      this.setKeyword({
        index: null,
        keywordSearch: ""
      });

    }
  }

  addRow = () => {
    if (!this.state.receptionDepartment?.id) {
      toast.info("Vui lòng chọn phòng ban tiếp nhận")
      return
    }
    let row = {
      name: "",
      supplyUnit: "",
      productSelected: {},
      quantity: "",
      requiredParameters: "",
      description: "",
      newProduct: "",
    }
    this.setState({
      listRowTable: [
        row,
        ...this.state.listRowTable,
      ]
    })
  }

  handleOpenPopupProduct = () => {
    this.setState({ shouldOpenDialogShoppingPlan: true })
  };

  handleDialogShoppingPlanClose = () => {
    this.setState({ shouldOpenDialogShoppingPlan: false })
  };

  handleSelectShoppingPlan = (items = [], productSelectedBefore = []) => {
    let { listRowTable = [] } = this.state;

    let _listRowTable = listRowTable.filter(row => {
      return !productSelectedBefore.some(item => item?.id === row?.productSelected?.id);
    });

    items.forEach(item => {
      let itemConvert = {
        productSelected: item ? { ...item } : null,
        newProduct: item?.name || "",
        unit:
          item?.unitId ? {
            id: item?.unitId,
            name: item?.unitName
          } : null,
      }
      _listRowTable.push(itemConvert)
    });

    this.setState({ listRowTable: _listRowTable }, () => {
      this.handleDialogShoppingPlanClose();
    })
  }

  handleSelectUnit = (value, index) => {
    if (variable.listInputName.New === value?.code) {
      this.setState({
        shouldOpenAddNewSkuDialog: true,
        index,

      });
      return;
    }

    let listRowTableUpdated = this.state.listRowTable?.map((row, idx) => {
      if (index === idx) {
        row.unit = value
        return row
      }
      return row
    })
    this.setState({
      listRowTable: listRowTableUpdated
    })
  }

  selectFormPurchase = (value, index) => {
    let listRowTableUpdated = this.state.listRowTable?.map((row, idx) => {
      if (index === idx) {
        row.formPurchase = value
        return row
      }
      return row
    })
    this.setState({
      listRowTable: listRowTableUpdated
    })
  }

  selectSuppiler = (value, index) => {
    let listRowTableUpdated = this.state.listRowTable?.map((row, idx) => {
      if (index === idx) {
        row.suppiler = value
        return row
      }
      return row
    })
    this.setState({
      listRowTable: listRowTableUpdated
    })
  }

  setListRowTable = (item) => {
    this.setState({ listRowTable: item });
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleChangeTab = (tabValue) => {
    this.setState({ tabValue });
  };
  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  formatDataDto = (purchaseRequest) => {
    let purchaseProducts = purchaseRequest.listRowTable?.map(item => {
      return {
        ...item,
        id: item?.id,
        note: item?.description,
        productId: item?.productSelected?.id,
        productName: item?.newProduct || item?.productSelected?.name,
        productSpecs: item?.requiredParameters,
        quantity: item?.quantity,
        skuId: item?.unit?.id,
        skuName: item?.unit?.name,
        status: appConst.listStatusProductInPurchaseRequest.CHO_DUYET.code,
        availableQuantity: item?.availableQuantity
      }
    })
    return {
      id: purchaseRequest?.id,
      note: purchaseRequest?.note,
      purchaseProducts: purchaseProducts,
      receptionDepartmentId: purchaseRequest?.receptionDepartment?.id,
      receptionDepartmentName: purchaseRequest?.receptionDepartment?.name,
      evaluationDepartmentId: purchaseRequest?.evaluationDepartment?.id,
      evaluationDepartmentName: purchaseRequest?.evaluationDepartment?.name,
      requestDate: formatDateDto(purchaseRequest?.requestDate),
      requestDepartmentId: purchaseRequest?.requestDepartment?.id,
      requestDepartmentName: purchaseRequest?.requestDepartment?.name,
      requestPersonId: purchaseRequest?.requestPerson?.personId,
      requestPersonName: purchaseRequest?.requestPerson?.personDisplayName,
      status: purchaseRequest?.status?.code || purchaseRequest?.status,
      type: purchaseRequest?.type,
      evaluationContent: purchaseRequest?.evaluationContent,
      confirmationContent: purchaseRequest?.confirmationContent,
      isEvaluation: purchaseRequest?.isEvaluate,
      procurementPlanId: purchaseRequest?.shoppingPlan?.id
    }
  };

  checkOpenFormPrint = (data) => {
    if (this.state.isSaveAndPrint) {
      this.setState({
        ...data?.data,
        status: appConst.listStatusPurchaseRequest.find(i => i?.code === data?.data?.status),
        openBMQTQT04: true
      }, () => {
        this.props.updatePageData && this.props.updatePageData()
      })
    }
    else {
      this.props.handleOKEditClose();
    }
  }

  checkExportExcel = async (voucherId) => {
    let { t, type } = this.props;
    let { setPageLoading } = this.context;

    setPageLoading(true);
    try {
      let searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        purchaseRequestIds: voucherId ? [voucherId] : null,
        type
      }
      await functionExportToExcel(exportToExcelV2, searchObject, "Yêu cầu mua sắm tài sản.xlsx");
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  handleFormSubmit = async () => {
    let {setPageLoading} = this.context;
    let { id } = this.state;
    let { t } = this.props;
    if (this.state.listRowTable.length <= 0) {
      toast.warning(t("purchaseRequest.noti.unSelectedProduct"))
      return
    }
    try {
      setPageLoading(true);
      let dataSubmit = this.formatDataDto(this.state);
      if (id) {
        const res = await UpdatePurchaseRequest(dataSubmit, id);
        let { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          if (document.activeElement?.id === "btn-excel") {
            return this.checkExportExcel(data?.id);
          }
          toast.success(t("general.updateSuccess"));
          this.checkOpenFormPrint(res?.data);
        }
        else {
          handleThrowResponseMessage(res);
        }
      } else {
        const res = await addNewPurchaseRequest(dataSubmit);
        let { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          if (document.activeElement?.id === "btn-excel") {
             return this.checkExportExcel(data?.id);
          }
          toast.success(t("general.success"));
          this.checkOpenFormPrint(res?.data);
        }
        else {
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount = () => {
    let { item, productName, isCheck, type } = this.props;
    let { departmentUser } = getUserInformation();
    let roles = getRole();

    this.setState({
      ...item,
      ...roles,
      isCheck,
      productName,
      listRowTable: item?.itemList || [],
      type,
    }, () => {
      if (!item?.id && !item?.isCopy) {
        this.setState({
          status: appConst.STATUS_PURCHASE_REQUEST.DANG_TONG_HOP,
          requestPerson: roles?.isRoleAssetUser || roles?.isRoleAssetManager
            ? {
              ...roles.currentUser,
              personDisplayName: roles.currentUser?.person?.displayName,
              personId: roles.currentUser?.person?.id
            }
            : null,
          requestDepartment: roles?.isRoleAssetUser || roles?.isRoleAssetManager
            ? {
              ...departmentUser,
              text: departmentUser?.name,
              name: departmentUser?.name
            }
            : null,
        });
      }
    })
  }

  componentWillMount = () => {
    let { item } = this.props;
    if (item?.isEvaluate || item?.isConfirm) {
      this.setState({ tabValue: appConst.TAB_PURCHASE_DIALOG.INFO_EVALUATE.code })
    }
  };

  handleSelectRequestPerson = (item) => {
    this.setState({
      requestPerson: item ? item : null,
    });
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    })
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleSelectRequestDepartment = (item) => {
    this.setState({
      requestDepartment:
        item?.id
          ? { id: item?.id, name: item?.text, text: item?.text }
          : null,
      requestPerson: null
    });
  };

  handleQuantityChange = (rowData, e, index) => {
    let listRowTableUpdated = this.state.listRowTable?.map((row, idx) => {
      if (index === idx) {
        row[e?.target?.name] = e.target.value
        return row
      }
      return row
    })
    this.setState({
      listRowTable: listRowTableUpdated
    })
  };

  setListProducts = (item) => {
    this.setState({ listProducts: item });
  };

  openRequestCommentDialog = () => {
    this.setState({ shouldOpenCommentDialog: true, comment: null });
  };

  handleRequestCommentClose = () => {
    this.setState({ shouldOpenCommentDialog: false });
  };

  SearchByPageRequestComment = () => {
    let searchObject = {};
    if (this.props.item.id) {
      searchObject.purchaseRequestId = this.props.item.id;
    }
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = 1000000;
    searchByPageRequestComment(searchObject).then(({ data }) => {
      this.setState({ comments: data?.content });
    }).catch(() => { });
  };

  getRequestCommentId = (commentId) => {
    let { listRequestCommentId } = this.state;
    if (!listRequestCommentId) {
      listRequestCommentId = [];
    }
    if (commentId) {
      listRequestCommentId.push(commentId);
    }
    this.setState({ listRequestCommentId: listRequestCommentId });
  };

  deleteRequestCommentById = () => {
    let { listRequestCommentId } = this.state;
    if (listRequestCommentId != null && listRequestCommentId.length > 0) {
      listRequestCommentId.forEach((id) => deleteRequestComment(id));
    }
  };

  deleteCkeckRequestComment = (id) => {
    let { listRequestCommentId } = this.state;

    deleteCkeckRequestComment(id).then(({ data }) => {
      if (data) {
        if (listRequestCommentId) {
          listRequestCommentId.map((commentId, index) => {
            if (commentId === id) {
              listRequestCommentId.splice(index, 1);
            }
          });
        }
        toast.info("Xoá thành công.");
        this.SearchByPageRequestComment();
      } else {
        toast.warning("Bạn không thể xoá bình luận này.");
      }
    });
  };

  handleEditRequestComment = (id) => {
    if (id) {
      getOneRequestComment(id).then(({ data }) => {
        if (data != null) {
          this.setState({ comment: data, shouldOpenCommentDialog: true })
        }
      });
    }
  };

  handleChangeDataTable = (e, index) => {
    let listRowTableUpdated = this.state.listRowTable?.map((row, idx) => {
      if (index === idx) {
        row[e.target.name] = e.target.value
        return row
      }
      return row
    })
    this.setState({
      listRowTable: listRowTableUpdated
    })
  }

  handleSelectReceiveDepartment = (value) => {
    this.setState({
      receptionDepartment: value ? value : null,
      evaluationDepartment: value,
    })
  }

  handleSelectEvaluationDepartment = (value) => {
    this.setState({
      evaluationDepartment: value,
    })
  }

  handleSelectStatus = (value) => {
    this.setState({ status: value })
  }

  handleMapDataAsset = (value, index) => {
    let { t } = this.props;
    if (value?.code === variable.listInputName.New) {
      getNewCode().then((result) => {
        if (result != null && result?.data && result?.data?.code) {
          let item = result.data;
          this.setItemProduct({
            ...item,
            name: this.state.keywordSearch.keywordSearch,
            managementPurchaseDepartment: this.state.receptionDepartment
          }, index);
          this.setState({
            shouldOpenDialogProduct: true,
          })
        }
      }).catch(() => {
        toast.error(t("general.error"));
      });
    }
    else {
      if (!value) {
        this.setQuery({
          ...this.state.query,
          keyword: ""
        })
      }
      let productExist = this?.state.listRowTable?.some(item => item?.productSelected?.id === value?.id);
      if (productExist && value?.id) {
        toast.warning("Thiết bị đã có trong danh sách");
        return;
      }
      
      let listRowTableUpdate = this?.state.listRowTable?.map((asset, idx) => {
        if (idx === index) {
          asset.productSelected = value ? { ...value } : null;
          asset.newProduct = value?.name || "";
          asset.unit = value?.defaultSku?.sku
          this.setKeyword({
            index: null,
            keywordSearch: ""
          })
          return asset
        }
        return asset
      })
      let productUpdated = this?.state.listProducts?.find((product, idx) => {
        return idx === index
      })
      let listProductsUpdate = [...this?.state.listProducts]
      if (productUpdated) {
        listProductsUpdate = this?.state.listProducts?.map((product, idx) => {
          if (idx === index) {
            return value ? { ...value } : null
          }
          return product
        })
      }
      else {
        listProductsUpdate.push(value)
      }

      this.setListRowTable(listRowTableUpdate)
      this.setListProducts(listProductsUpdate)
    }
  }

  handlePrint = () => {
    this.setState({
      openBMQTQT04: true
    })
  };

  handleClosePrintForm = () => {
    this.setState({
      openBMQTQT04: false
    });
  }

  handleCloseAddNewSkuDialog = () => {
    this.setState({
      shouldOpenAddNewSkuDialog: false,
      keySearch: "",
      index: null,
    })
  }

  handleCheckExportExcel = () => {
    this.checkExportExcel(this.state?.id);
  };

  render() {
    let { open, t, i18n, item } = this.props;
    let {
      shouldOpenNotificationPopup,
      isView,
      loading,
      openBMQTQT04,
      shouldOpenAddNewSkuDialog,
      keySearch,
      tabValue
    } = this.state;
    const { ASSET_PURCHASING } = LIST_PRINT_FORM_BY_ORG.SHOPPING_MANAGEMENT;
    const { currentOrg } = this.context;
    const dataView = convertPurchaseRequestPrintData(this.state);
    const isAllowedSaveOrPrint = tabValue === appConst.TAB_PURCHASE_DIALOG.INFO_EVALUATE.code && (item?.isEvaluate || item?.isConfirm) || (this.state.isCheck && !isView);
    const urlsPrint = (item?.isEvaluate || item?.isConfirm)
      ? [
        ...ASSET_PURCHASING.PURCHASE_REQUEST_DIALOG.GENERAL,
        ...(ASSET_PURCHASING.PURCHASE_REQUEST_DIALOG[currentOrg?.printCode] || []),]
      : [
        ...ASSET_PURCHASING.PURCHASE_REQUEST.GENERAL,
        ...(ASSET_PURCHASING.PURCHASE_REQUEST[currentOrg?.printCode] || []),
      ]
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">
              {this.state.isCheck
                ? t("purchaseRequest.saveUpdate")
                : t("purchase_request_count.request_information")}
            </span>
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <Grid container spacing={1} className="">
              <PurchaseRequestScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleDateChange={this.handleDateChange}
                handleSelectRequestDepartment={
                  this.handleSelectRequestDepartment
                }
                handleSelectDepartmentPopupClose={
                  this.handleSelectDepartmentPopupClose
                }
                handleChange={this.handleChange}
                productTypeCode={this.productTypeCode}
                handleQuantityChange={this.handleQuantityChange}
                setListProducts={this.setListProducts}
                handleRequestCommentClose={this.handleRequestCommentClose}
                openRequestCommentDialog={this.openRequestCommentDialog}
                SearchByPageRequestComment={this.SearchByPageRequestComment}
                getRequestCommentId={this.getRequestCommentId}
                deleteCkeckRequestComment={this.deleteCkeckRequestComment}
                handleEditRequestComment={this.handleEditRequestComment}
                isCheck={this.props.isCheck}
                addRow={this.addRow}
                setListRowTable={this.setListRowTable}
                handleChangeDataTable={this.handleChangeDataTable}
                handleSetDataSelect={this.handleSetDataSelect}
                handleSelectRequestPerson={this.handleSelectRequestPerson}
                handleSelectReceiveDepartment={this.handleSelectReceiveDepartment}
                handleSelectStatus={this.handleSelectStatus}
                handleSelectUnit={this.handleSelectUnit}
                setQuery={this.setQuery}
                setKeyword={this.setKeyword}
                handleMapDataAsset={this.handleMapDataAsset}
                selectFormPurchase={this.selectFormPurchase}
                selectSuppiler={this.selectSuppiler}
                handleChangeTab={this.handleChangeTab}
                handleOpenPopupProduct={this.handleOpenPopupProduct}
                handleSelectEvaluationDepartment={this.handleSelectEvaluationDepartment}
              />
            </Grid>
            {openBMQTQT04 && (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleClosePrintForm}
                open={openBMQTQT04}
                item={dataView}
                title={t("Phiếu tổng hợp nhu cầu đầu tư trang thiết bị")}
                urls={urlsPrint}
              />
            )}
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mr-20 mb-8">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => {
                  this.props.handleClose();
                  this.deleteRequestCommentById();
                }}
              >
                {this.state.isCheck
                  ? t("general.cancel")
                  : t("general.close")}
              </Button>
              {isAllowedSaveOrPrint && (
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  className="mr-12"
                  onClick={() => this.setState({ isSaveAndPrint: false })}
                >
                  {t("general.save")}
                </Button>
              )}
              {isAllowedSaveOrPrint && (
                <Button
                  id="btn-saveAndPrint"
                  variant="contained"
                  type="submit"
                  color="primary"
                  className="mr-12"
                  onClick={() => this.setState({ isSaveAndPrint: true })}
                >
                  {t("general.saveAndPrint")}
                </Button>
              )}
              <Button
                id="btn-excel"
                variant="contained"
                type={isView ? "button" : "submit"}
                color="primary"
                onClick={isView ? this.handleCheckExportExcel : ()=>{}}
              >
                {isView ? t("general.exportToExcel") : t("general.saveAndExportToExcel")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
        {shouldOpenAddNewSkuDialog && (
          <StockKeepingUnitEditorDialog
            t={t}
            i18n={i18n}
            keySearch={keySearch}
            handleClose={this.handleCloseAddNewSkuDialog}
            handleOKEditClose={this.handleCloseAddNewSkuDialog}
            open={shouldOpenAddNewSkuDialog}
            selectUnit={(data) => this.handleSelectUnit(data, this.state.index)}
          />
        )}
        {this.state.shouldOpenDialogProduct && (
          <ProductDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleProductDialogClose}
            open={this.state?.shouldOpenDialogProduct}
            handleOKEditClose={this.handleOKEditClose}
            item={this.state.itemProduct}
            type={appConst.productPurchaseTypeCode.TSCD_CCDC}
            selectProduct={this.handleMapDataAsset}
            indexLinhKien={this.state.indexLinhKien}
            isFromPurchaseRequest={true}
          />
        )}
        {this.state.shouldOpenDialogShoppingPlan && (
          <PopupSelectShoppingPlan
            t={t}
            open={this.state.shouldOpenDialogShoppingPlan}
            handleClose={this.handleDialogShoppingPlanClose}
            handleSelect={this.handleSelectShoppingPlan}
            item={{
              id: this.state?.shoppingPlan?.id,
              name: this.state?.shoppingPlan?.name,
              productSelected: this.state?.listRowTable,
              productTypeCodes: [
                appConst.productTypeCode.TSCĐ,
                appConst.productTypeCode.CCDC,
              ],
              managementPurchaseDepartmentId: this.state.receptionDepartment?.id,
            }}
          />
        )}
      </Dialog>
    );
  }
}

PurchaseRequestEditorDialog.contextType = AppContext;
export default PurchaseRequestEditorDialog;
