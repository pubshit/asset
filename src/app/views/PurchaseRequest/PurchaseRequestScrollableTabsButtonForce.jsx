import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid, Card } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useTranslation } from "react-i18next";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import MaterialTable, { MTableToolbar } from "material-table";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { searchByPage } from "../Product/ProductService";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, keySearch, variable } from "app/appConst";
import {
  searchByPage as searchByPageDepartment, searchByPageDepartmentNew,
} from "../Department/DepartmentService";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import { getStockKeepingUnit } from "../StockKeepingUnit/StockKeepingUnitService";
import { TabPanel, filterOptions, getTheHighestRole, getOptionSelected, getUserInformation } from "app/appFunction";
import viLocale from "date-fns/locale/vi";
import { handleKeyDownFloatOnly } from "../../appFunction";
import { getListProductShoppingPlan, searchByPage as searchByPageShoppingPlan } from "../ShoppingPlan/PurchaseRequestService";
import {NumberFormatCustom} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  const isCheck = props.isCheck;

  return (
    <div>
      {!isCheck && (
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function PurchaseRequestScrollableTabsButtonForce(props) {
  const { t, i18n, item } = props;
  const {
    isRoleAssetUser,
    isRoleAssetManager,
    isRoleOrgAdmin,
  } = getTheHighestRole();
  const { organization } = getUserInformation();
  const classes = useStyles();
  const [listAsset, setListAsset] = useState([]);
  const [searchParamUserByRequestDepartment, setsearchParamUserByRequestDepartment,] = useState({
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    departmentId: ""
  });
  let searchObject = {
    keyword: "",
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
  };
  const receiverDepartmentSeacrhObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  }

  const shoppingPlanSeacrhObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isActive: appConst.STATUS_SHOPPING_PLAN.ACTIVE.index
  }

  const handleSetData = props.handleDateChange;

  useEffect(() => {
    setsearchParamUserByRequestDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.requestDepartment?.id ?? "",
    });
  }, [props?.item?.requestDepartment]);

  useEffect(() => {
    handleSearchAsset();
  }, [props?.item?.receptionDepartment?.id]);

  const handleSearchAsset = () => {
    let searchObject = {};
    let planingDepartment =
      props.item.receptionDepartment != null
        ? props.item.receptionDepartment
        : {};
    let planingDepartmentClone = {};
    if (planingDepartment) {
      if (Object.keys(planingDepartment).length > 0) {
        planingDepartmentClone = { ...planingDepartment };
      }
    }
    searchObject.keyword = props?.item.query?.keyword;
    searchObject.pageIndex = 1;
    searchObject.pageSize = 20;
    searchObject.managementPurchaseDepartment = null;
    searchObject.productTypeCodes =
      props?.item?.type === appConst.TYPE_PURCHASE.TSCD_CCDC
        ? [appConst.productTypeCode.TSCĐ, appConst.productTypeCode.CCDC]
        : [appConst.productTypeCode.VTHH];
    if (Object.keys(planingDepartmentClone).length > 0) {
      searchObject.managementPurchaseDepartment = planingDepartmentClone;
    }

    searchByPage(searchObject)
      .then(({ data }) => {
        setListAsset([...data.content]);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  useEffect(() => {
    if (props?.item?.query?.keySearch === keySearch.asset) {
      handleSearchAsset();
    }
  }, [props?.item?.query]);

  const handleSearch = (e, source, index) => {
    props.setQuery({
      ...props?.item?.query,
      keyword: e.target.value,
      keySearch: source,
    });
    props.setKeyword({
      index: index,
      keywordSearch: e.target.value,
    });
  };

  let columns = [
    {
      title: props.item.isCheck ? t("general.action") : "",
      field: "custom",
      align: "center",
      hidden: props?.item?.isView,
      width: "250",
      minWidth: "100px",
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          isCheck={!props?.item?.isCheck}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
            } else if (method === appConst.active.delete) {
              for (
                let index = 0;
                index < props?.item?.listRowTable?.length;
                index++
              ) {
                const item = props?.item?.listRowTable[index];
                if (
                  rowData &&
                  item &&
                  rowData?.tableData?.id === item?.tableData?.id
                ) {
                  props?.item?.listRowTable?.length > 0 &&
                    props.item.listRowTable.splice(index, 1);
                  props?.item?.listProducts?.length > 0 &&
                    props.item.listProducts.splice(index, 1);
                  props.setListRowTable(props.item.listRowTable);
                  props.setListProducts(props.item.listProducts);
                  break;
                }
              }
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    {
      title: t("general.index"),
      field: " ",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("Product.productAvailable"),
      field: "",
      align: "left",
      minWidth: "250px",
      render: (rowData) => {
        return props?.item?.isView ? (
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={
              rowData?.alterProductName // có thông tin khi ở tt đang/đã tổng hợp
                ? rowData?.alterProductName
                : rowData?.productSelected && rowData?.productId
                  ? rowData?.productSelected?.name
                  : ""
            }
          />
        ) : (
          <div className="w-100">
            <Autocomplete
              id="combo-box"
              size="small"
              options={listAsset}
              value={
                rowData?.productSelected && rowData?.productSelected?.id
                  ? rowData?.productSelected
                  : null
              }
              onChange={(event, value) =>
                props?.handleMapDataAsset(value, rowData.tableData.id)
              }
              getOptionLabel={(option) => option.name || ""}
              getOptionSelected={getOptionSelected}
              filterOptions={(options, params) => {
                if (props?.item?.isRoleAssetManager || props?.item?.isRoleOrgAdmin) {
                  return filterOptions(options, params, true, "name")
                }
                else {
                  params.inputValue = params.inputValue.trim();
                  return filterOptions(options, params);
                }
              }}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  placeholder={t("purchaseRequest.searchProductAvailabile")}
                  variant="standard"
                  onChange={(e) =>
                    handleSearch(e, keySearch.asset, rowData.tableData.id)
                  }
                  value={""}
                  onFocus={() => {
                    if(props?.item?.query?.keyword && listAsset?.length) return;
                    props.setQuery({
                      ...props?.item?.query,
                      keyword: "",
                    });
                    handleSearchAsset();
                  }}
                />
              )}
              noOptionsText={t("general.noOption")}
            />
          </div>
        );
      },
    },
    {
      title: t("Product.newProduct"),
      field: "newProduct",
      align: "left",
      maxWidth: 80,
      minWidth: "250px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            className="w-100"
            onChange={(event) =>
              props.handleChangeDataTable(event, rowData.tableData.id)
            }
            disabled={!props.item.isCheck}
            InputProps={{
              readOnly: Boolean(rowData?.productSelected?.id) || props?.item?.isView,
            }}
            name="newProduct"
            validators={["required"]}
            errorMessages={[t("general.required")]}
            value={rowData.newProduct}
          />
        );
      },
    },
    {
      title: t("purchase_request_count.unit"),
      field: "supplyUnit",
      align: "left",
      maxWidth: 150,
      minWidth: 120,

      render: (rowData) => (
        <AsynchronousAutocompleteSub
          searchFunction={getStockKeepingUnit}
          searchObject={searchObject}
          listData={props.item?.listUnit}
          nameListData="listUnit"
          setListData={handleSetData}
          displayLable="name"
          value={rowData.unit || null}
          readOnly={props.item?.isView}
          onSelect={(value) =>
            props.handleSelectUnit(value, rowData?.tableData?.id)
          }
          onInputChange={(e) => handleSetData(e.target.value, "keySearch")}
          filterOptions={(options, params) => filterOptions(
            options,
            params,
            isRoleAssetManager || isRoleOrgAdmin,
            "name",
          )}
          noOptionsText={t("general.noOption")}
        />
      ),
    },
    {
      title: t("purchaseRequestCount.suggestQuantity"),
      field: "quantity",
      align: "left",
      maxWidth: 80,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            fullWidth
            onChange={(event) =>
              props.handleQuantityChange(rowData, event, rowData.tableData.id)
            }
            type="text"
            disabled={!props.item.isCheck}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView,
              inputProps: {
                className: "text-center",
                decimalScale: 2,
                allowNegative: false,
              },
            }}
            name="quantity"
            validators={["minFloat:0.01", "required", `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
            errorMessages={[t("Số lượng lớn hơn 0.01"), t("general.required"), t("general.quantityError")]}
            value={rowData.quantity || ""}
          />
        );
      },
    },
    {
      title: t("purchaseRequestCount.availableQuantity"),
      field: "availableQuantity",
      align: "left",
      maxWidth: 80,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            className="w-100"
            onChange={(event) =>
              props.handleQuantityChange(rowData, event, rowData.tableData.id)
            }
            type="text"
            disabled={!props.item.isCheck}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView,
              inputProps: {
                className: "text-center",
                decimalScale: 2,
                allowNegative: false,
              },
            }}
            name="availableQuantity"
            validators={["minNumber:0", `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
            errorMessages={[t("Số lượng lớn hơn hoặc bằng 0"), t("general.quantityError")]}
            value={rowData.availableQuantity || ""}
          />
        );
      },
    },
    ...(props?.item?.status?.code !== appConst.listStatusPurchaseRequestObject.CHO_TONG_HOP.code ?
      [
        {
          title: t("purchaseRequestCount.approvedQuantity"),
          field: "approvedQuantity",
          align: "left",
          width: "150px",
          minWidth: "150px",
          sorting: false,
          cellStyle: {
            textAlign: "center",
          },
        }
      ] : []
    )
    ,
    {
      title: t("purchase_request_count.required_parameters"),
      field: "requiredParameters",
      align: "left",
      minWidth: 240,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            className="w-100"
            onChange={(event) =>
              props.handleChangeDataTable(event, rowData.tableData.id)
            }
            disabled={!props.item.isCheck}
            InputProps={{
              readOnly: props?.item?.isView,
            }}
            name="requiredParameters"
            value={rowData.requiredParameters}
          />
        );
      },
    },
    {
      title: t("purchase_request_count.note"),
      field: "description",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return (
          <TextValidator
            className="w-100"
            onChange={(event) =>
              props.handleChangeDataTable(event, rowData.tableData.id)
            }
            disabled={!props.item.isCheck}
            InputProps={{
              readOnly: props?.item?.isView,
            }}
            name="description"
            value={rowData.description}
          />
        );
      },
    },
  ];
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={item.tabValue}
          onChange={(e, index) => props?.handleChangeTab(index)}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={appConst.TAB_PURCHASE_DIALOG.INFO.name} {...a11yProps(0)} />
          {(props?.item?.isConfirm
            || props?.item?.isEvaluate
            || props?.item?.isView)
            && <Tab label={props?.item?.isConfirm ? appConst.TAB_PURCHASE_DIALOG.INFO_CONFIRM.name : appConst.TAB_PURCHASE_DIALOG.INFO_EVALUATE.name} {...a11yProps(1)} />}
          {/* {props.item.isCheck && <Tab label={t("Ghi chú")} {...a11yProps(2)} />} */}
        </Tabs>
      </AppBar>
      <TabPanel value={item.tabValue} index={appConst.TAB_PURCHASE_DIALOG.INFO.code}>
        <Grid container spacing={1}>
          <Grid item md={3} sm={12} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                // label={t("purchaseRequest.requestDate")}
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("purchaseRequest.requestDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                disabled={!props.item?.isCheck || props?.item?.isView}
                name={"requestDate"}
                value={props.item.requestDate}
                maxDate={new Date()}
                maxDateMessage={t("purchaseRequest.noti.maxDateMessage")}
                invalidDateMessage={t("general.invalidDateFormat")}
                onChange={(date) => props.handleDateChange(date, "requestDate")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("purchaseRequest.status")}
                </span>
              }
              searchFunction={() => { }}
              searchObject={{}}
              listData={appConst.listStatusPurchaseRequest}
              displayLable="name"
              value={props.item.status || null}
              onSelect={props.handleSelectStatus}
              readOnly
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("purchaseRequest.requestDepartment")}
                </span>
              }
              searchFunction={searchByPageDepartment}
              searchObject={searchObject}
              listData={props?.item.listRequestDepartment || []}
              setListData={(data) => props?.handleSetDataSelect(data, "listRequestDepartment")}
              displayLable="text"
              value={props?.item?.requestDepartment?.id ? props?.item?.requestDepartment : null}
              onSelect={(value) => props.handleSelectRequestDepartment(value)}
              filterOptions={filterOptions}
              readOnly={
                props?.item?.isView
                || props?.item?.isStatus
                || isRoleAssetUser
                || isRoleAssetManager
              }
              disabled={isRoleAssetUser}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("purchaseRequest.requestPerson")}
                </span>
              }
              disabled={!props?.item?.requestDepartment?.id}
              readOnly={props?.item?.isView || props?.item?.isStatus || isRoleAssetUser}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByRequestDepartment}
              defaultValue={props?.item?.requestPerson ?? ""}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              value={props?.item?.requestPerson ?? ""}
              onSelect={(value) => props?.handleSelectRequestPerson(value)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("purchaseRequest.receptionDepartment")}
                </span>
              }
              searchFunction={searchByPageDepartmentNew}
              searchObject={receiverDepartmentSeacrhObject}
              displayLable="name"
              isNoRenderChildren
              isNoRenderParent
              typeReturnFunction="category"
              value={props.item.receptionDepartment || null}
              onSelect={props.handleSelectReceiveDepartment}
              readOnly={props?.item?.isView || props?.item?.isStatus}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("purchaseRequest.evaluationDepartment")}
              searchFunction={searchByPageDepartmentNew}
              searchObject={receiverDepartmentSeacrhObject}
              displayLable="name"
              isNoRenderChildren
              isNoRenderParent
              typeReturnFunction="category"
              value={props.item.evaluationDepartment || null}
              onSelect={props.handleSelectEvaluationDepartment}
              readOnly={props?.item?.isView || props?.item?.isStatus}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Dashboard.Purchase.ShoppingPlan")}
                </span>
              }
              searchFunction={searchByPageShoppingPlan}
              searchObject={shoppingPlanSeacrhObject}
              displayLable="name"
              isNoRenderChildren
              isNoRenderParent
              typeReturnFunction="category"
              value={props.item.shoppingPlan || null}
              onSelect={(data) => props?.handleDateChange(data, "shoppingPlan")}
              readOnly={props?.item?.isView}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              // InputLabelProps={{ shrink: true }}
              label={t("Asset.note")}
              onChange={props.handleChange}
              type="text"
              disabled={!props.item.isCheck}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              name="note"
              value={props.item.note}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-12 mb-12">
            {!props?.item?.isView && (
              <>
                <Button
                  className="mr-12"
                  size="small"
                  variant="contained"
                  color="primary"
                  disabled={!props.item.isCheck}
                  onClick={() => props.addRow()}
                >
                  {t("+")}
                </Button>
                <Button
                  className=" "
                  size="small"
                  variant="contained"
                  color="primary"
                  disabled={!props.item.shoppingPlan?.id}
                  onClick={() => props.handleOpenPopupProduct()}
                >
                  {t("ShoppingPlan.addProductInList")}
                </Button>
              </>
            )}
          </Grid>
          <Grid item md={5} sm={6} xs={12}></Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              title={t("Product.title")}
              data={props.item?.listRowTable ? props.item?.listRowTable : []}
              columns={columns}
              options={{
                selection: false,
                actionsColumnIndex: 0,
                paging: false,
                search: false,
                sorting: false,
                toolbar: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: "dense",
                maxBodyHeight: "210px",
                minBodyHeight: "210px",
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={item.tabValue} index={appConst.TAB_PURCHASE_DIALOG.INFO_EVALUATE.code}>
        <Card
          className="mb-6 w-100 p-8"
          elevation={2}
          style={{ marginBottom: "3px" }}
        >
          <Grid container spacing={2} className="mb-3 h-100">
            <Grid item md={12} sm={12} xs={12}>
              <TextValidator
                multiline
                className="w-100"
                label={t("Asset.evaluationContent")}
                onChange={props.handleChange}
                type="text"
                rows={4}
                rowsMax={8}
                disabled={props?.item?.isConfirm || props.item?.confirmationContent}
                name="evaluationContent"
                value={props.item?.evaluationContent || ""}
                InputProps={{
                  readOnly: props?.item?.isView && !props?.item?.isEvaluate && !props?.item?.isConfirm
                }}
              />
            </Grid>
            <Grid item md={12} sm={12} xs={12}>
              <TextValidator
                multiline
                className="w-100"
                label={t("Asset.confirmationContent")}
                onChange={props.handleChange}
                type="text"
                rows={4}
                rowsMax={8}
                disabled={props?.item?.isEvaluate}
                name="confirmationContent"
                value={props.item?.confirmationContent || ""}
                InputProps={{
                  readOnly: props?.item?.isView && !props?.item?.isEvaluate && !props?.item?.isConfirm
                }}
              />
            </Grid>
          </Grid>
        </Card>
      </TabPanel>
      {/* <TabPanel value={value} index={2}>
        <Grid container spacing={2}>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.openRequestCommentDialog}
            >
              {t("Thêm ghi chú")}
            </Button>
            {props.item.shouldOpenCommentDialog && (
              <RequestCommentDialog
                open={props.item.shouldOpenCommentDialog}
                handleSelectRequestComment={props.handleSelectRequestComment}
                handleCloseComment={props.handleRequestCommentClose}
                SearchByPageRequestComment={props.SearchByPageRequestComment}
                getRequestCommentId={props.getRequestCommentId}
                editComment={props.item.comment}
                item={props.item}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          {comments?.map((item, index) => {
            return (
              <Card
                className="mb-6 w-100 p-8"
                elevation={2}
                style={{ marginBottom: "3px" }}
              >
                <Grid container spacing={2} className="mb-3">
                  <Grid item md={3} sm={12} xs={12}>
                    {item.user?.displayName}
                  </Grid>
                  <Grid item md={8} sm={12} xs={12}>
                    {item?.comment}
                  </Grid>
                  <Grid item md={1} sm={12} xs={12}>
                    {item.user?.id === currentUser?.id && (
                      <div>
                        <IconButton
                          size="small"
                          title={t("Xoá")}
                          onClick={() =>
                            props.deleteCkeckRequestComment(item.id)
                          }
                        >
                          <Icon fontSize="small" color="error">
                            delete
                          </Icon>
                        </IconButton>

                        <IconButton
                          size="small"
                          title={t("Sửa")}
                          onClick={() =>
                            props.handleEditRequestComment(item.id)
                          }
                        >
                          <Icon fontSize="small" color="primary">
                            edit
                          </Icon>
                        </IconButton>
                      </div>
                    )}
                  </Grid>
                </Grid>
              </Card>
            );
          })}
        </Grid>
      </TabPanel> */}
    </div >
  );
}
