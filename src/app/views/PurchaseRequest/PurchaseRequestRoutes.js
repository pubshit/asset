import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const PurchaseRequestTable = EgretLoadable({
  loader: () => import("./PurchaseRequestTable")
});
const ViewComponent = withTranslation()(PurchaseRequestTable);

const PurchaseRequestRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/purchase_request",
    exact: true,
    component: ViewComponent
  }
];

export default PurchaseRequestRoutes;