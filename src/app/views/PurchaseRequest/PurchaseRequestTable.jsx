import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Checkbox, Badge,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import {
  deleteItem,
  searchByPage,
  exportToExcelV2 as exportToExcelService,
  getItemById,
  getCountStatus,
} from "./PurchaseRequestService"
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import { Helmet } from "react-helmet";
import moment from "moment";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, LIST_ORGANIZATION } from "app/appConst";
import VisibilityIcon from "@material-ui/icons/Visibility";
import AppContext from "app/appContext";
import ComponentPurchaseRequestTable from "./ComponentPurchaseRequestTable";
import {
  checkStatusSubTable,
  classStatus,
  classStatusSubTable,
  functionExportToExcel,
  getRole,
  formatDateDtoMore,
  getTheHighestRole, getUserInformation, handleThrowResponseMessage, isSuccessfulResponse,
} from "app/appFunction";
import { getRequestsProducts } from "../PurchaseRequestCount/PurchaseRequestCountService";
import localStorageService from "app/services/localStorageService";
import { TabPanel, LightTooltip } from "../Component/Utilities";
import ClearIcon from "@material-ui/icons/Clear";
import LibraryAddOutlinedIcon from "@material-ui/icons/LibraryAddOutlined";
import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
import { isValidDate } from "../../appFunction";
import { withRouter } from "react-router-dom";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CommentIcon from '@material-ui/icons/Comment';
import { searchByPage as searchByPageShoppingPlan } from "../ShoppingPlan/PurchaseRequestService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  let {
    hasEditPermission,
    isRoleAdmin,
    isRoleOrgAdmin
  } = props;
  const { isRoleAssetManager, isRoleAssetUser } = getTheHighestRole();
  const { departmentUser } = getUserInformation();
  const isChoTongHop = item?.status === appConst.tabPurchaseRequest.tabWaitCount;
  let isReceptionDepartment = item?.receptionDepartmentId === departmentUser?.id;
  const isDepartmentCreated = item?.requestDepartmentId === departmentUser?.id;
  let isDelete = isRoleAdmin || isRoleOrgAdmin || (isChoTongHop && isDepartmentCreated);

  const isAllowedEvaluate = !item?.confirmationContent && (
		!isRoleAssetUser && departmentUser?.id === item?.receptionDepartmentId || isRoleOrgAdmin
	);
  const isAllowedConfirm = departmentUser?.id === item?.requestDepartmentId || isRoleOrgAdmin;

  return (
    <div className="none_wrap">
      {isChoTongHop && (isRoleOrgAdmin || (isRoleAssetManager && isReceptionDepartment)) && (
        <Checkbox
          checked={item?.checked}
          onChange={() => props.onSelect(item, appConst.active.check)}
        />
      )}
      {hasEditPermission && isChoTongHop && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton size="small" onClick={(event) => props.onSelect(item, appConst.active.edit, event)}>
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isDelete && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton size="small" onClick={(event) => props.onSelect(item, appConst.active.delete, event)}>
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={(event) => props.onSelect(item, appConst.active.print, event)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("InstrumentToolsTransfer.watchInformation")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={(event) => props.onSelect(item, appConst.active.view, event)}
        >
          <VisibilityIcon size="small" className="iconEye" />
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("InstrumentToolsTransfer.copy")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={(event) => props.onSelect(item, appConst.active.copy, event)}
        >
          <Icon fontSize="small" color="primary">content_copy</Icon>
        </IconButton>
      </LightTooltip>
      {isAllowedEvaluate && <LightTooltip
        title={t("general.evaluate")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={(event) => props.onSelect(item, appConst.active.propose, event)}
        >
          <CommentIcon />
        </IconButton>
      </LightTooltip>}
      {isAllowedConfirm && <LightTooltip
        title={t("general.confirm")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={(event) => props.onSelect(item, appConst.active.card, event)}
        >
          <CheckCircleIcon />
        </IconButton>
      </LightTooltip>}
    </div>
  );
}

class PurchaseRequestTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.table[0],
    page: 0,
    subPage: 0,
    subRowsPerPage: appConst.rowsPerPageOptions.table[1],
    subTotalElements: 0,
    listItemsShoppingPlan: [],
    PurchaseRequest: [],
    item: {},
    id: "",
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationDeleteListDialog: false,
    shouldOpenNotificationPopup: false,
    type: appConst.TYPE_PURCHASE.TSCD_CCDC,
    hasDeletePermission: false,
    hasEditPermission: false,
    itemCount: {},
    shouldOpenCountDialog: false,
    rowDataProductsTable: [],
    tabValue: appConst.tabPurchaseRequest.tabAll,
    itemList: [],
    selectedItems: [],
    purchaseRequestVoucherIds: [],
    openAdvanceSearch: false,
    requestDepartmentFilter: false,
    toDate: null,
    fromDate: null,
    shouldOpenSwitchDepartmentPopup: false,
  };

  setSelectedItems = (items) => {
    let resetItemList = this.state.itemList.map((item) => {
      item.checked = false
      return item
    })
    this.setState({
      itemList: resetItemList,
      purchaseRequests: [],
      purchaseRequestVoucherIds: [],
      requestsProducts: [],
      purchaseRequestIds: []
    })
  }

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state?.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });

    const statusMap = {
      [appConst.tabPurchaseRequest.tabAll]: null,
      [appConst.tabPurchaseRequest.tabWaitCount]: appConst.tabPurchaseRequest.tabWaitCount,
      [appConst.tabPurchaseRequest.tabCounted]: appConst.tabPurchaseRequest.tabCounted,
      [appConst.tabPurchaseRequest.tabPlaned]: appConst.tabPurchaseRequest.tabPlaned,
    };
    this.search({
      status: statusMap[newValue],
      statusFilter: appConst.listStatusPurchaseRequest.find(item => item?.code === statusMap[newValue])
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  setPage = (page) => {
    this.search({ page });
  };

  setRowsPerPage = (event) => {
    this.search({ rowsPerPage: event.target.value, page: 0 });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleSelectRequestDepartment = (item) => {
    this.setState({ requestDepartmentFilter: item ? { id: item?.id, name: item?.text, text: item?.text } : null }, () => {
      this.search()
    });
  };

  handleSetDataSelect = (data, name) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      this.search();
    });
  };

  getDepartmentAU = async () => {
    let department = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)
    this.setState({
      requestDepartmentFilter: {
        ...department,
        text: department?.name,
        name: department?.name
      }
    })
  }

  search = (searchObject) => {
    const newObject = searchObject ? { ...searchObject } : { page: 0 };
    this.setState({ ...newObject, selectedItem: null }, () => this.updatePageData())
  }

  updatePageData = async () => {
    const {
      type,
      keyword,
      page,
      rowsPerPage,
      status,
      statusFilter,
      purchaseRequestVoucherIds = [],
      requestDepartmentFilter,
      toDate,
      fromDate,
      selectedList,
      selectedItemPlan,
    } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    const searchObject = {
      type,
      keyword,
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      status: status || statusFilter?.code,
      requestDepartmentId: requestDepartmentFilter?.id,
      toDate: toDate ? formatDateDtoMore(toDate) : null,
      fromDate: fromDate ? formatDateDtoMore(fromDate, "start") : null,
      procurementPlanId: selectedItemPlan?.id
    };

    try {
      const res = await searchByPage(searchObject);
      const { data, code } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        if (data?.totalElements && data?.content.length <= 0) {
          return this.setPage(0);
        }
        const itemList = data?.content || [];
        const totalElements = data?.totalElements || 0;

        if (itemList?.length > 0) {
          itemList.map((item) => {
            let checkItem = purchaseRequestVoucherIds?.some(p => p === item.id);
            let isExist = selectedList?.some(x => x.id === item?.id)
            item.checked = checkItem;
            item.isExcelChecked = isExist;
            return item
          })
        }

        this.setState({
          itemList,
          totalElements,
          rowDataProductsTable: []
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleGetRowData = (rowData) => {
    this.setState({ selectedItem: rowData }, () => {
      this.handleGetDataProduct(rowData)
    })
  }

  handleGetDataProduct = async (rowData) => {
    const { pageAssets, rowsPerPageAssets } = this.state
    const searchObject = {}
    searchObject.pageIndex = pageAssets + 1;
    searchObject.pageSize = rowsPerPageAssets;
    try {
      const { data } = await getItemById(rowData?.id);
      if (appConst.CODE.SUCCESS === data?.code) {
        const purchaseProductsUpdated = data?.data?.purchaseProducts?.map(item => {
          return {
            ...item,
            requestDepartmentName: data?.data?.requestDepartmentName
          }
        })
        this.setState({ rowDataProductsTable: purchaseProductsUpdated })
      } else {
        this.setState({ rowDataProductsTable: [] })
      }
    } catch (e) {
      console.error(e)
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenNotificationPopup: false,
      shouldOpenSwitchDepartmentPopup: false,
      isPrint: false,
      isConfirm: false,
      isEvaluate: false,
      selectedItem: null,
      rowDataProductsTable: [],
    });
  };

  handleCountDialogClose = () => {
    this.setState({
      shouldOpenCountDialog: false,
    });
  };

  exportToExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    const {
      type,
      keyword,
      page,
      rowsPerPage,
      status,
      statusFilter,
      purchaseRequestVoucherIds = [],
      requestDepartmentFilter,
      toDate,
      fromDate,
      selectedList,
      isExportExcelByVoucher
    } = this.state;
    setPageLoading(true);
    try {
      const searchObject = {
        type,
        keyword,
        pageIndex: page + 1,
        pageSize: rowsPerPage,
        status: !isExportExcelByVoucher ? status || statusFilter?.code : null,
        requestDepartmentId: requestDepartmentFilter?.id,
        toDate: toDate ? formatDateDtoMore(toDate) : null,
        fromDate: fromDate ? formatDateDtoMore(fromDate, "start") : null,
        purchaseRequestIds: selectedList?.map(i => i?.id)
      };
      await functionExportToExcel(exportToExcelService, searchObject, "Yêu cầu mua sắm tài sản.xlsx");
    } catch (error) {
      console.log(error)
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
      this.closeExportExcelColumns();
    }
  };

  handleExportToExcel = async (type) => {
    if (type === appConst.OBJECT_OPTIONS_EXCEL_PURCHASE_REQUEST.TAT_CA.code) {
      await this.exportToExcel();
    }
    else if (type === appConst.OBJECT_OPTIONS_EXCEL_PURCHASE_REQUEST.THEO_BAN_GHI.code) {
      this.setState({
        isExportExcelByVoucher: true,
      })
    }
    else {
      if (this.state.selectedList?.length === 0) {
        toast.warning("Chưa chọn phiếu để xuất excel");
        return;
      }
      this.setState({
        isExportExcelByVoucher: false,
      })
      await this.exportToExcel();
    }
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenSwitchDepartmentPopup: false,
      isConfirm: false,
      isEvaluate: false,
      selectedItemPlan: null,
      purchaseRequestVoucherIds: [],
      selectedList: [],
      purchaseRequests: [],
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKCountClose = () => {
    this.setState({
      shouldOpenCountDialog: false,
    }, () => {
      this.updatePageData();
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props
    let { setPageLoading } = this.context
    setPageLoading(true)
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count,
      });
    }
    deleteItem(this.state.id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        toast.info(t("general.deleteSuccess"));
        this.updatePageData();
        this.getCountStatus();
        this.handleDialogClose();
      }
      else {
        toast.warning(t(data?.message));
      }
    }).catch(() => {
      toast.error(t("toastr.error"));
    }).finally(() => {
      setPageLoading(false);
    });
  };

  componentDidMount = () => {
    let { location, history } = this.props;

    if (location?.state?.id) {
      this.handleEdit(location?.state?.id)
      history.push(location?.state?.path, null);
    }
    this.updatePageData();
    this.getRoleCurrentUser();
    this.getCountStatus();
    this.updateSubPageData();
  }

  getRoleCurrentUser = () => {
    let { hasDeletePermission, hasEditPermission } = this.state;
    let {
      currentUser,
      isRoleAssetUser,
      isRoleAdmin,
      isRoleOrgAdmin,
      isRoleAssetManager,
    } = getRole();
    if (currentUser) {
      // người quản lý vật tư
      if (isRoleAssetManager) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }
      // người đại diện phòng ban
      if (isRoleAssetUser) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }
      // org
      if (isRoleOrgAdmin || isRoleAdmin) {
        if (hasDeletePermission !== true) {
          hasDeletePermission = true;
        }
        if (hasEditPermission !== true) {
          hasEditPermission = true;
        }
      }

      this.setState({
        hasDeletePermission,
        hasEditPermission,
        currentUser,
        isRoleAssetUser,
        isRoleAdmin,
        isRoleOrgAdmin,
        isRoleAssetManager,
      });
    }
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };
  handleGetProductsByPurchaseRequestIds = async (purchaseRequestVoucherIds, procurementPlanId) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    let searchObjects = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      purchaseRequestIds: purchaseRequestVoucherIds
    }
    try {
      setPageLoading(true);
      let res = await getRequestsProducts(searchObjects);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemCount: {
            date: new Date(),
            status: appConst.STATUS_PURCHASE_REQUEST_COUNT.DANG_TONG_HOP.code,
            type: appConst.TYPE_PURCHASE.TSCD_CCDC,
            purchaseRequests: this.state.purchaseRequests,
            requestsProducts: data?.content ?? [],
            purchaseRequestIds: this.state.purchaseRequestVoucherIds,
            procurementPlanId,
          },
          shouldOpenCountDialog: true,
        });
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  handlePurchaseReqCount = () => {
    let { purchaseRequestVoucherIds, purchaseRequests = [] } = this.state;
    let { t } = this.props;
    let firstReceptionDepartmentId = purchaseRequests[0].receptionDepartmentId;
    let firtsProcurementPlanId = purchaseRequests[0].procurementPlanId;
    let hasOtherReceptionDepartment = purchaseRequests?.some(
      item => item.receptionDepartmentId !== firstReceptionDepartmentId
    );
    let hasOtherProcurementPlan = purchaseRequests?.some(
        item => item.procurementPlanId !== firtsProcurementPlanId
    );

    if (purchaseRequestVoucherIds?.length <= 0) {
      toast.warning(t("general.noti_check_data"));
      return;
    } else if (hasOtherReceptionDepartment) {
      return toast.warning(t("purchaseRequest.noti.asynchronousReceptionDepartment"));
    } else if (hasOtherProcurementPlan) {
      return toast.warning("Các phiếu đã chọn không cùng danh mục kế hoạch");
    }
    this.handleGetProductsByPurchaseRequestIds(purchaseRequestVoucherIds, firtsProcurementPlanId);
  };

  handleDelete = (rowData) => {
    this.setState({
      id: rowData?.id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props
    this.handleDeleteList(this.data).then(() => {
      toast.info(this.props.t("general.deleteSuccess"));
      this.updatePageData();
      this.handleDialogClose();
    }).catch(() => {
      toast.error(t("toastr.error"));
    });
  };

  formatDataDto(data) {
    let listRowTables = data.purchaseProducts.map((item, index) => {
      return {
        ...item,
        newProduct: item?.productName,
        description: item?.note,
        productSelected: {
          id: item?.productId,
          name: item?.productName,
        },
        requiredParameters: item?.productSpecs,
        quantity: item?.quantity,
        unit: {
          id: item?.skuId,
          name: item?.skuName,
        },
      }
    })
    return {
      ...data,
      purchaseProducts: data.purchaseProducts.map((item, index) => {
        return {
          ...item,
          index: index + 1,
        }
      }),
      status: appConst.listStatusPurchaseRequest.find(item => item.code === data?.status),
      requestDate: data?.requestDate,
      requestDepartment: {
        name: data?.requestDepartmentName,
        id: data?.requestDepartmentId,
        text: data?.requestDepartmentName,
      },
      evaluationDepartment: {
        name: data?.evaluationDepartmentName,
        id: data?.evaluationDepartmentId,
        text: data?.evaluationDepartmentName,
      },
      requestPerson: {
        name: data?.requestPersonName,
        id: data?.requestPersonId,
        personDisplayName: data?.requestPersonName,
      },
      receptionDepartment: {
        id: data?.receptionDepartmentId,
        name: data?.receptionDepartmentName,
        text: data?.receptionDepartmentName,
      },
      shoppingPlan: {
        id: data?.procurementPlanId,
        name: data?.procurementPlanName
      },
      note: data?.note,
      isView: data?.isView,
      itemList: listRowTables
    }
  }

  handleEdit = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: false,
        },
        shouldOpenEditorDialog: true,
      })
    });
  }


  handleView = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: true,
        },
        shouldOpenEditorDialog: true,
      })
    });
  }
  handleEvaluate = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: true,
          isEvaluate: true,
          isEvaluation: true
        },
        shouldOpenEditorDialog: true,
      })
    });
  }
  handleConfirm = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          isView: true,
          isConfirm: true,
          isEvaluation: true
        },
        shouldOpenEditorDialog: true,
      })
    });
  }

  handlePrint = (rowData) => {
    const { currentOrg } = this.context;

    if (currentOrg?.code === LIST_ORGANIZATION.BVDK_BA_VI.code) {
      return this.setState({
        item: rowData,
        isPrint: true,
      })
    }
    this.handleGetDetailPurchaseRequest(rowData?.id).then(() => {
      this.setState({
        item: {
          ...this.state.item,
          assetClass: appConst.assetClass.TSCD,
          currentUser: getRole().currentUser,
        },
        isPrint: true,
      })
    });
  }

  handleGetDetailPurchaseRequest = async (id) => {
    const { t } = this.props;
    let { setPageLoading } = this.context
    try {
      setPageLoading(true)
      let res = await getItemById(id)
      const { code, data } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        let dataEdit = {
          ...data,
          isView: false,
          isEvaluation: false
        }
        this.setState({
          item: this.formatDataDto(dataEdit ?? {}),
          selectedItem: dataEdit,
          rowDataProductsTable: dataEdit?.purchaseProducts?.map(item => {
            return  {
              ...item,
              requestDepartmentName: dataEdit?.requestDepartmentName
            }
          }) || [],
        });
        return this.formatDataDto(dataEdit ?? {});
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false)
    }
  }

  handleToggleRowTable = (rowData) => {
    let {
      purchaseRequestVoucherIds,
      purchaseRequests = [],
      itemList = [],
    } = this.state;
    const id = rowData?.id
    let newArray = purchaseRequestVoucherIds ? purchaseRequestVoucherIds : []
    let existItem = newArray?.find(x => x === id)
    if (!existItem) {
      newArray.push(id);
      purchaseRequests.push(rowData);
    } else {
      purchaseRequests.map((item, index) =>
        item.id === existItem ? purchaseRequests.splice(index, 1) : null
      );
      newArray.map((item, index) =>
        item === existItem ? newArray.splice(index, 1) : null
      );
    }
    itemList.map((item) => {
      if (item.id === id) {
        item.checked = !existItem;
      }
      return item
    })
    this.setState({
      itemList,
      purchaseRequests,
      purchaseRequestVoucherIds: newArray,
    })
  }

  handleCheckIcon = (rowData, method, event) => {
    event?.stopPropagation();
    let actions = {
      [appConst.active.delete]: this.handleDelete,
      [appConst.active.edit]: this.handleEdit,
      [appConst.active.view]: this.handleView,
      [appConst.active.print]: this.handlePrint,
      [appConst.active.check]: this.handleToggleRowTable,
      [appConst.active.copy]: this.handleCopy,
      [appConst.active.propose]: this.handleEvaluate,
      [appConst.active.card]: this.handleConfirm,
    };

    if (Object.keys(actions)?.includes(method?.toString())) {
      actions[method]?.(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  }

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus(this.state.type)
      .then(({ data }) => {
        let countStatusWaitCount,
          countStatusSumming,
          countStatusAggregate;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusPurchaseRequest[0].code === item?.trangThai) {
            countStatusWaitCount = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusPurchaseRequest[1].code === item?.trangThai) {
            countStatusSumming = this.checkCount(item?.soLuong);
            return;
          }
          if (appConst?.listStatusPurchaseRequest[2].code === item?.trangThai) {
            countStatusAggregate = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState(
          {
            countStatusWaitCount,
            countStatusSumming,
            countStatusAggregate,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusPurchaseRequest.find(
      (item) => item.code === status
    );
    return itemStatus?.name;
  };
  handleOpenAdvanceSearch = () => {
    if (getRole().isRoleAssetUser) {
      this.getDepartmentAU()
    }
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  closeExportExcelColumns = () => {
    this.setState({
      isExportExcelByVoucher: false, //hide columns
      selectedList: [], // clear selected list
      itemList: this.state.itemList.map(item => ({ // clear excel checkbox
        ...item,
        isExcelChecked: false,
      }))
    });
  };

  handleClick = (data, rowData) => {
    let { selectedList, itemList } = this.state;
    let isExist = selectedList?.some(item => item.id === rowData?.id)

    if (isExist) {
      selectedList = selectedList?.filter(item => item.id !== rowData?.id)
    }
    else {
      selectedList.push(rowData);
    }

    itemList.map(item => {
      if (item.id === rowData?.id) {
        item.isExcelChecked = !isExist;
      }
      return item;
    })
    this.setState({ selectedList, itemList });
  };

  handleCopy = (rowData) => {
    this.handleGetDetailPurchaseRequest(rowData?.id).then((data) => {
      const productItem = product => ({
        ...product,
        id: null,
        status: null,
        quantity: null,
        approvedQuantity: null,
        availableQuantity: null,
        supplierId: null,
        supplierName: null,
        shoppingFormId: null,
        shoppingFormName: null,
      })

      this.setState({
        item: {
          ...data,
          id: null,
          code: null,
          isCopy: true,
          purchasePreparePlanId: null,
          itemList: data?.itemList?.map(productItem),
          purchaseProducts: data?.purchaseProducts?.map(productItem),
          status: appConst.STATUS_PURCHASE_REQUEST.DANG_TONG_HOP,
        },
        shouldOpenEditorDialog: true,
      })
    });
  }

  updateSubPageData = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let { subRowsPerPage, selectedItemPlan, subPage } = this.state;
      const searchObject = {
        pageIndex: subPage + 1,
        pageSize: subRowsPerPage,
      };

      const res = await searchByPageShoppingPlan(searchObject);
      const { data, code, message } = res?.data;
      if (appConst.CODE.SUCCESS === code) {
        const listItemsShoppingPlan = data?.content || [];
        const subTotalElements = data?.totalElements || 0;

        this.setState({
          listItemsShoppingPlan,
          subTotalElements
        });
      } else {
        toast.warning(message);
      }
    } catch (error) {
    } finally {
      setPageLoading(false);
    }
  };

  handleRowShoppingPlanClick = (rowData) => {
    this.setState({ selectedItemPlan: rowData }, () => {
      this.updatePageData();
    })
  }

  handleChangeSubPage = (event, newPage) => {
    this.setState({ subPage: newPage }, () => {
      this.updateSubPageData();
    });
  };

  setSubRowsPerPage = (event) => {
    this.setState({ subRowsPerPage: event.target.value, subPage: 0 }, () => {
      this.updateSubPageData();
    })
  };
  
  handleSwitchDepartment = () => {
    this.setState({
      shouldOpenSwitchDepartmentPopup: true,
    })
  }

  render() {
    const { t, i18n } = this.props;
    let {
      page,
      tabValue,
      rowsPerPage,
      currentUser,
      isRoleAdmin,
      isRoleOrgAdmin,
      hasEditPermission,
      isRoleAssetManager,
      hasDeletePermission,
      rowDataProductsTable,
      isExportExcelByVoucher,
    } = this.state;
    let TitlePage = t("purchaseRequest.title");
    let columns = [
      {
        title: (
          <Badge
            badgeContent={
              <IconButton
                size="small"
                className="cursor-pointer"
                onClick={this.closeExportExcelColumns}
              >
                <ClearIcon color="primary" fontSize="small" />
              </IconButton>
            }
            color="secondary"
            className="my-8"
          >
            <div className="mt-4 mr-4">Xuất excel</div>
          </Badge>
        ),
        field: "custom",
        maxWidth: "120px",
        hidden: !isExportExcelByVoucher,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            icon={<LibraryAddOutlinedIcon />}
            checkedIcon={<LibraryAddCheckIcon />}
            checked={rowData.isExcelChecked}
            onChange={(e) => this.handleClick(e, rowData)}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            isRoleAssetManager={isRoleAssetManager}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            currentUser={currentUser}
            isRoleAdmin={isRoleAdmin}
            isRoleOrgAdmin={isRoleOrgAdmin}
            onSelect={this.handleCheckIcon}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("purchaseRequest.requestDate"),
        field: "requestDate",
        align: "center",
        minWidth: "150px",
        cellStyle: {
        },
        render: (rowData) =>
          rowData?.requestDate
            ? (<span>{moment(rowData?.requestDate).format("DD/MM/YYYY")}</span>)
            : (""),
      },
      {
        title: t("SuggestedMaterials.ballotCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
      },
      {
        title: t("purchaseRequest.status"),
        field: "status",
        align: "left",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span
              className={classStatus(statusIndex)}
            >
              {this.checkStatus(statusIndex)}
            </span>
          )
        }
      },
      {
        title: t("purchaseRequest.requestDepartment"),
        field: "requestDepartmentName",
        minWidth: "250px",
        cellStyle: {
        },
      },
      {
        title: t("purchaseRequest.receptionDepartment"),
        field: "receptionDepartmentName",
        align: "left",
        minWidth: "250px",
        cellStyle: {
        },
      },
      {
        title: t("purchaseRequest.requestPersonName"),
        field: "requestPersonName",
        align: "left",
        minWidth: "150px",
        cellStyle: {
        },
      },
      // {
      //   title: t("purchaseRequest.modifiedBy"),
      //   field: "modifiedBy",
      //   align: "left",
      //   minWidth: "150px",
      //   headerStyle: {
      //     textAlign: "center",
      //   },
      //   cellStyle: {
      //   },
      // },
      {
        title: t("purchaseRequestCount.note"),
        field: "note",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
      }
    ];

    let columnsNoAction = [
      {
        title: t("purchaseRequest.requestDate"),
        field: "requestDate",
        align: "center",
        width: "150",
        cellStyle: {
        },
        render: (rowData) =>
          rowData?.requestDate
            ? (<span>{moment(rowData?.requestDate).format("DD/MM/YYYY")}</span>)
            : (""),
      },
      {
        title: t("SuggestedMaterials.ballotCode"),
        field: "code",
        align: "center",
        minWidth: "150px",
      },
      {
        title: t("purchaseRequest.status"),
        field: "",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span
              className={classStatus(statusIndex)}
            >
              {this.checkStatus(statusIndex)}
            </span>
          )
        }
      },
      {
        title: t("purchaseRequest.requestDepartment"),
        field: "requestDepartmentName",
        width: "150",
        cellStyle: {
        },
      },
      {
        title: t("purchaseRequest.receptionDepartment"),
        field: "receptionDepartmentName",
        align: "left",
        width: "150",
        cellStyle: {
        },
      },
      {
        title: t("purchaseRequest.note"),
        field: "note",
        align: "left",
        width: "150",
        cellStyle: {
        },
      }
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: '50px',
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => (rowData.tableData.id + 1)
      },
      {
        title: t("purchaseRequestCount.productPortfolio"),
        field: "productName",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        render: (rowData) => rowData?.alterProductName ? rowData?.alterProductName : rowData?.productId ? rowData?.productName : ""
      },
      {
        title: t("purchaseRequestCount.nameProduct"),
        field: "productName",
        align: "left",
        minWidth: "300px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        }
      },
      {
        title: t("purchaseRequestCount.unit"),
        field: "skuName",
        align: "left",
        minWidth: 80,
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.suggestQuantity"),
        field: "quantity",
        align: "left",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.approvedQuantity"),
        field: "approvedQuantity",
        align: "left",
        width: "150px",
        minWidth: "130px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.availableQuantity"),
        field: "availableQuantity",
        align: "left",
        width: "150px",
        minWidth: "130px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("purchaseRequestCount.requestDepartment"),
        field: "requestDepartmentName",
        align: "left",
        minWidth: "200px",
        sorting: false,
      },
      {
        title: t("purchaseRequest.status"),
        field: "",
        align: "left",
        minWidth: "150px",
        cellStyle: {
          textAlign: "center"
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          return (
            <span
              className={classStatusSubTable(statusIndex)}
            >
              {checkStatusSubTable(statusIndex)}
            </span>
          )
        }
      },
      {
        title: t("purchaseRequestCount.requestParameters"),
        field: "productSpecs",
        align: "left",
        width: "240px",
        minWidth: "240px",
        sorting: false,
      },
      {
        title: t("purchaseRequestCount.note"),
        field: "note",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
      }
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.purchase"), path: "/list/purchase_request" },
              { name: t("Dashboard.Purchase.purchase_asset") },
              { name: TitlePage },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("purchaseRequest.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>
                    {t("purchaseRequest.tabWaitCount")}
                  </span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusWaitCount || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchaseRequest.tabCounted")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusSumming || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("purchaseRequest.tabPlanned")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusAggregate || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <ComponentPurchaseRequestTable
          t={t}
          i18n={i18n}
          item={this.state}
          getRowData={this.handleGetRowData}
          handleEditItem={this.handleEditItem}
          handlePurchaseReqCount={this.handlePurchaseReqCount}
          handleDialogClose={this.handleDialogClose}
          handleDeleteAll={this.handleDeleteAll}
          onRowShoppingPlanClick={this.handleRowShoppingPlanClick}
          handleChangeSubPage={this.handleChangeSubPage}
          setSubRowsPerPage={this.setSubRowsPerPage}
          handleTextChange={this.handleTextChange}
          search={this.search}
          handleOKEditClose={this.handleOKEditClose}
          handleCountDialogClose={this.handleCountDialogClose}
          handleOKCountClose={this.handleOKCountClose}
          handleConfirmationResponse={this.handleConfirmationResponse}
          columns={columns}
          columnsNoAction={columnsNoAction}
          handleChangePage={this.handleChangePage}
          setRowsPerPage={this.setRowsPerPage}
          setSelectedItems={this.setSelectedItems}
          updatePageData={this.updatePageData}
          handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          handleSelectRequestDepartment={this.handleSelectRequestDepartment}
          handleSetDataSelect={this.handleSetDataSelect}
          handleClick={this.handleClick}
          handleExportToExcel={this.handleExportToExcel}
          handleSwitchDepartment={this.handleSwitchDepartment}
        />
        <div>
          <MaterialTable
            data={rowDataProductsTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '350px',
              minBodyHeight: '260px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}

PurchaseRequestTable.contextType = AppContext;
export default withRouter(PurchaseRequestTable);
