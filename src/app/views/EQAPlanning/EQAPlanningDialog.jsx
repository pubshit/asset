import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  FormControlLabel,
  Switch
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { getUserById, updateUser, addNewEQAPlanning, updateEQAPlanning, checkCode } from "./EQAPlanningService";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import { generateRandomId } from "utils";

class EQAPlanningDialog extends Component {
  state = {
    name: "",
    code: "",
    year: new Date().getFullYear(),
    type: 0,
    objectives: "",
    numberOfRound: 0,
    fee: 0,
    startDate: new Date(),
    endDate: new Date(),
    isActive: false
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }

    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;

    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
      } else {
        //Nếu trả về false là code chưa sử dụng có thể dùng
        if (id) {
          updateEQAPlanning({
            ...this.state
          }).then(() => {
            this.props.handleClose();
          });
        } else {
          addNewEQAPlanning({
            ...this.state
          }).then(() => {
            this.props.handleClose();
          });
        }
      }
    });
  };

  handleStartDateChange = startDate => {
    this.setState({ startDate });
  };

  handleEndDateChange = endDate => {
    this.setState({ endDate });
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;
    this.setState(item);
  }

  render() {
    const { t, i18n } = this.props;
    let {
      code,
      name,
      year,
      type,
      objectives,
      numberOfRound,
      startDate,
      endDate,
      fee,
      isActive
    } = this.state;
    let { open, handleClose } = this.props;

    return (
      <Dialog  open={open}>
        <div className="p-24">
          <h4 className="mb-20">Cập nhật kế hoạch ngoại kiểm</h4>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="mb-16" container spacing={4}>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Code")}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Name")}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Year")}
                  onChange={this.handleChange}
                  type="number"
                  name="year"
                  value={year}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Type")}
                  onChange={this.handleChange}
                  type="text"
                  name="type"
                  value={type}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAPlanning.fee")}
                  onChange={this.handleChange}
                  type="number"
                  name="fee"
                  value={fee}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100 mb-16"
                  label="objectives"
                  label={t("EQAPlanning.objectives")}
                  onChange={this.handleChange}
                  type="text"
                  name="objectives"
                  value={objectives}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("EQAPlanning.numberOfRound")}
                  onChange={this.handleChange}
                  type="number"
                  name="numberOfRound"
                  value={numberOfRound}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    className="w-100 mb-16"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("StartDate")}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={new Date(startDate)}
                    onChange={this.handleStartDateChange}
                    fullWidth
                  />
                  <DateTimePicker
                    className="w-100 mb-16"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("EndDate")}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={new Date(endDate)}
                    onChange={this.handleEndDateChange}
                    fullWidth
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>

            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
              <Button variant="contained" color="secondary" onClick={() => this.props.handleClose()}>Cancel</Button>
            </div>
          </ValidatorForm>
        </div>
      </Dialog>
    );
  }
}

export default EQAPlanningDialog;
