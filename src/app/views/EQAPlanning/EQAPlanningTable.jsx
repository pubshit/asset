import React, { Component } from "react";
import {
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination,
  TableContainer,
  Button,
  TextField,
  Checkbox,
  Card
} from "@material-ui/core";
import { getAllEQAPlannings, deleteEQAPlanning, getById } from "./EQAPlanningService";
import EQAPlanningEditorDialog from "./EQAPlanningDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import shortid from "shortid";
import { saveAs } from 'file-saver';
class EQAPlanningTable extends Component {
  state = {
    text: '',
    pageSize: 10,
    pageIndex: 0,
    totalElements: 0,
    listData: [],
    item: {},
    selectAllItem:false,
    selectedList:[],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false
  };
  numSelected =0;
  rowCount=0;

  handleTextChange = event => {
    this.setState({ text: event.target.value }, function () {
    })
  };

  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.updatePageData();
    }
  };

  setRowsPerPage = event => {
    this.setState({ pageIndex: 0, pageSize: event.target.value }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setState({ pageIndex: newPage }, function () {
      this.updatePageData();
    });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog:false
    });
    this.updatePageData();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleEditEQAPlanning = item => {
    getById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });

  };

  handleDeleteEQAPlanning = id => {
    this.setState({
      pageIndex: 0,
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleConfirmationResponse = () => {
    deleteEQAPlanning(this.state.id).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  componentDidMount() {
    this.updatePageData();
  }

  updatePageData = () => {
    getAllEQAPlannings(this.state).then(({ data }) => this.setState({
      listData: [...data.content], page: data.pageable.pageNumber + 1, totalElements: data.totalElements
    }));
  };
  
  handleClick = (event, item) => {
    let {listData} =  this.state;
    if(item.checked==null){
      item.checked=true;
    }else {
      item.checked=!item.checked;
    }
    var selectAllItem=true;
    for(var i=0;i<listData.length;i++){
      if(listData[i].checked==null || listData[i].checked==false){
        selectAllItem=false;
      }
      if(listData[i].id==item.id){
        listData[i]=item;
      }
    }
    this.setState({selectAllItem:selectAllItem, listData:listData});
    
  };
   handleSelectAllClick = (event) => {
    let {listData} =  this.state;
    for(var i=0;i<listData.length;i++){
      listData[i].checked=!this.state.selectAllItem;
    }
     this.setState({selectAllItem:!this.state.selectAllItem, listData:listData});
  };

  async handleDeleteList(list){
    for(var i=0;i<list.length;i++){
      if(list[i].checked){
        await deleteEQAPlanning(list[i].id);
      }
    }    
  }
  handleDeleteAll = (event) => {
    let {listData} =  this.state;
    this.handleDeleteList(listData).then(()=>{  
      this.updatePageData();
      this.handleDialogClose();
    }
    );
  };

  render() {
    // var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
    // saveAs(blob, "hello world.txt");
    const { t, i18n } = this.props;
    let {
      pageSize,
      pageIndex,
      totalElements,
      listData,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog
    } = this.state;
    return (
      <div className="m-sm-30">

        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[{ name: t('EQAPlanning.title') }]} />
        </div>

        <Button
          className="mb-16 mr-16"
          variant="contained"
          color="primary"
          onClick={() => this.setState({ item: {}, shouldOpenEditorDialog: true })}
        >
          {t('button.add')}
        </Button>
        <Button
          className="mb-16 mr-36"
          variant="contained"
          color="primary"
          onClick={() => this.setState({ shouldOpenConfirmationDeleteAllDialog: true})}
        >
          {t('Delete')}
        </Button>
        <TextField className="mb-16 mr-10" placeholder={t('EnterSearch')} type="text" value={this.state.text} onChange={this.handleTextChange} onKeyDown={this.handleKeyDownEnterSearch} />
        <Button className="mb-16 mr-16" variant="contained" color="primary" onClick={() => this.updatePageData()}>{t('Search')}</Button>
        <Card className="w-100 overflow-auto" elevation={6}>
          <Table className="crud-table" style={{ whiteSpace: "pre", minWidth: "750px" }}>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={this.state.selectAllItem}
                    onChange={this.handleSelectAllClick}
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                </TableCell>  
                <TableCell>{t('general.code')}</TableCell>
                <TableCell>{t('general.name')}</TableCell>
                <TableCell>{t('general.action')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listData
                .map((eQAPlanning, index) => (
                  <TableRow key={shortid.generate()}>
                  <TableCell padding="checkbox">
                    <Checkbox onClick={(event) => this.handleClick(event, eQAPlanning)}
                      checked={eQAPlanning.checked}
                    />
                  </TableCell>
                    <TableCell className="px-0" align="left">
                      {eQAPlanning.code}
                    </TableCell>
                    <TableCell className="px-0" align="left">
                      {eQAPlanning.name}
                    </TableCell>

                    <TableCell className="px-0 border-none">
                      <IconButton
                        onClick={() =>
                          this.handleEditEQAPlanning(eQAPlanning)
                        }
                      >
                        <Icon color="primary">edit</Icon>
                      </IconButton>
                      <IconButton onClick={() => this.handleDeleteEQAPlanning(eQAPlanning.id)}>
                        <Icon color="error">delete</Icon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>

          <TablePagination
            className="px-16"
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={totalElements}
            rowsPerPage={pageSize}
            page={pageIndex}
            backIconButtonProps={{
              "aria-label": "Previous Page"
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page"
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.setRowsPerPage}
          />

          {shouldOpenEditorDialog && (
            <EQAPlanningEditorDialog
              handleClose={this.handleDialogClose}
              open={shouldOpenEditorDialog}
              item={this.state.item}
              t={t} i18n={i18n}
            />
          )}
          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleConfirmationResponse}
              text="Are you sure to delete?"
            />
          )}

          {shouldOpenConfirmationDeleteAllDialog && (
            <ConfirmationDialog
              open={shouldOpenConfirmationDeleteAllDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleDeleteAll}
              text="Are you sure to delete all?"
            />
          )}   
        </Card>
      </div>
    );
  }
}

export default EQAPlanningTable;
