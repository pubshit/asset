import React from "react";
import {Button, Dialog, DialogActions} from "@material-ui/core";
import {ValidatorForm} from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { convertNumberPriceRoundUp} from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center"
  },
  title: {
    fontSize: '0.975rem',
    fontWeight: 'bold',
    marginBottom: '0px'
  },
  text: {
    fontSize: '0.8rem',
    fontStyle: 'italic',
    marginTop: '10px'
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: 'bold'
  },
  table: {
    width: '100%',
    border: '1px solid',
    borderCollapse: 'collapse'
  },
  w_3: {
    border: '1px solid',
    width: '3%',
    textAlign: 'center'
  },
  w_5: {
    border: '1px solid',
    width: '5%',
    textAlign: 'center'
  },
  w_7: {
    border: '1px solid',
    width: '7%',
    textAlign: 'center'
  },
  w_9: {
    border: '1px solid',
    width: '9%',
    textAlign: 'center'
  },
  w_11: {
    border: '1px solid',
    width: '11%',
  },
  w_20: {
    border: '1px solid',
    width: '20%',
  },
  border: {
    border: '1px solid',
    padding: "0 5px",
    fontSize: "0.975rem"
  },
  name: {
    border: '1px solid',
    paddingLeft: "5px",
    textAlign: 'left'
  },
  sum: {
    border: '1px solid',
    paddingLeft: "5px",
    fontWeight: 'bold',
    textAlign: 'left'
  },
  represent: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 12%'
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    textTransform: 'uppercase',
  },
  textCenter: {
    display:"flex",
    justifyContent:"center",
    fontWeight: 'bold',
    width:"33%"
  },
  marginTop25minus : {
    marginTop: -25
  },
  pt_30 : {
    paddingTop: 30,
  },
  pt_40 : {
    paddingTop: 40,
  },
}

class InventorySuggestedSuppliesPrint extends React.Component {
  state = {
    voucherDetails:[],
  }

  componentWillMount() {
    this.setState({
      ...this.props.item
    });
  }

  componentDidMount() {
  }

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write(content.innerHTML);
    
    pri.document.close();
    pri.focus();
    pri.print();
  }

  totalQuantity = (arr = [], name) => {
    let totalQuantity = arr?.reduce((accumulator, item) => accumulator += item[name], 0)
    return totalQuantity || ""
  }

  render() {
    const { t, item } = this.props
    let { open} = this.props;
    let now = new Date(this.state?.ngayDeXuat); 
    let currentUser = localStorageService.getSessionItem("currentUser");
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth={true}>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t('SuggestedSupplies.printSuggestedSupplies')}</span>
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{height: '0px', width: '0px', position: 'absolute'}} title="ifmcontentstoprint"></iframe>
        <ValidatorForm ref="form"  onSubmit={this.handleFormSubmit}>
          <DialogContent id='divcontents' style ={{}} className="dialog-print">
            <div style={{textAlign: 'center'}} className="form-print">

              <div style={{ display:'flex'}}>
                <div style={{flexGrow: 1, textAlign: "left"}}>
                  <p style={{ ...style.title }}>{"BỘ CÔNG AN/" + currentUser?.org?.name}</p>
                </div>
                <div
                    style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-end",
                    flexGrow: 1
                    }}
                >
                {
                    this.state?.status === appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.indexOrder && 
                    <p>Mã xuất: {this.state?.voucherCode || ""}</p>
                }
              </div>
              </div>
              <div>
                <p style={style.title}>PHIẾU ĐỀ XUẤT VẬT TƯ/LINH KIỆN</p>
                <p style={style.text}>
                  {this.state?.ngayDeXuat
                    ? <span style={style.text}>
                      Ngày {now.getDate()} tháng {now.getMonth() + 1} năm {now.getFullYear()}
                    </span>
                    : <span> Ngày....tháng....năm......</span>
                  }
                </p>
              </div>
              <div>
                {
                  <table style={style.table}  className="table-report">
                    <tr>
                      <th style={style.w_3}>STT</th>
                      <th style={style.w_11}>Mã Vật Tư</th>
                      <th style={style.w_20}>Tên Vật Tư</th>
                      <th style={style.w_20}>Tên Tài Sản</th>
                      <th style={style.w_5}>Hãng SX</th>
                      <th style={style.w_5}>ĐVT</th>
                      <th style={style.w_5}>SL yêu cầu</th>
                      <th style={style.w_5}>SL Xuất</th>
                      <th style={style.w_20}>Ghi chú</th>
                    </tr>
                    
                  {this.state.voucherDetails && this.state.voucherDetails?.map((row, index) => {
                    return(
                      <tr>
                        <td style={{ ...style.border, ...style.textAlignCenter}}>{index + 1 || "" }</td>
                        <td style={{ ...style.border, ...style.textAlignCenter}}>{row?.productCode || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.productName || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.assetName || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.product?.manufacturerName || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter}}>{row?.skuName || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter}}>{row?.quantityOfVoucher || ""}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter}}>{row?.amount ? row?.amount : ""}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft}}>{row?.note}</td>
                      </tr>
                    )
                  })}
                  <tr>
                    <td colSpan={5} style={{ ...style.border, ...style.textAlignLeft, ...style.forwarder}}>Tổng cộng: {this.totalQuantity(this.state?.voucherDetails, "quantity")}</td>
                    <td colSpan={1} style={{ ...style.border, ...style.textAlignCenter}}></td>
                    <td colSpan={1} style={{ ...style.border, ...style.textAlignCenter}}>{this.totalQuantity(this.state?.voucherDetails, "quantityExport")}</td>
                    <td colSpan={1} style={{ ...style.border, ...style.textAlignCenter}}></td>
                  </tr>
                  </table>
                }
              </div>
              <div>
                <div style={style.signContainer}>
                  <div style ={{...style.textCenter}}><p>TRƯỞNG PHÒNG VTTBYT</p></div>
                  <div style ={{...style.textCenter}}><p>NGƯỜI PHÁT</p></div>
                  <div style ={{...style.textCenter}}><p>NGƯỜI lĨNH</p></div>
                  <div style ={{...style.textCenter}}><p>TRƯỞNG KHOA TIẾP NHẬN</p></div>
                </div>
              </div>
              <div style={style.marginTop25minus}>
                <div style={style.signContainer}>
                  <div style ={{
                    ...style.textCenter,
                    ...style.pt_40,
                    }}
                  >
                    <p>{this.state?.handoverDepartment?.name}</p>
                  </div>
                  <div style ={{
                    ...style.textCenter,
                    ...style.pt_40,
                  }}>
                    <p>{this.state?.handoverPerson?.displayName}</p>
                  </div>
                  <div style ={{
                    ...style.textCenter,
                    ...style.pt_40,
                  }}>
                    <p>{this.state?.receiverPerson?.displayName}</p>
                  </div>
                  <div style ={{
                    ...style.textCenter,
                    ...style.pt_40,
                    }}
                  >
                    <p>{this.state?.receiverDepartment?.name}</p>
                  </div>
                </div>
              </div>
            </div>
          </DialogContent>
  
            <DialogActions>
              <div className="flex flex-space-between flex-middle mr-16">
                
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-36"
                  onClick={() => this.props.handleClose()}
                >
                  {t('general.cancel')}
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  {t('In')}
                </Button>
              </div>
            </DialogActions>
          </ValidatorForm>
        </Dialog>
     
    )
  }
}

export default InventorySuggestedSuppliesPrint;
