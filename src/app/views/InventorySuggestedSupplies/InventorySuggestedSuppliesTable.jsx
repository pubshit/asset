import React from "react";
import { IconButton, Icon, AppBar, Tabs, Tab } from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  exportToExcel,
  searchByPage,
  countByStatus,
} from "./InventorySuggestedSuppliesService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import ConstantList from "../../appConfig";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import {
  LightTooltip,
  TabPanel,
  checkCount,
  convertFromToDate,
  convertNumberPrice,
  getTheHighestRole,
  handleKeyDown,
  isSuccessfulResponse,
  isValidDate
} from "app/appFunction";
import ComponentDeliveryVoucherTable from "./ComponentDeliveryVoucherTable";
import AppContext from "app/appContext";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { GeneralTab } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const roles = getTheHighestRole();
  const { departmentUser, isRoleOrgAdmin, isRoleAssetManager, isRoleAssetUser } = roles
  let isActionDelete = props?.item?.createByDepartmentId === departmentUser?.id

  if (isRoleOrgAdmin) {
    isActionDelete = true
  }
  const item = props.item;
  const isDeXuat = item?.status === appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code;
  const isXacNhan = [
    appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code,
    appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code
  ].includes(item?.status);

  const isAllowEdit =
    item?.status === appConst.listStatusSuggestSuppliesObject.TU_CHOI.code
    && ((departmentUser?.id === item?.receiptDepartmentId) || departmentUser?.id === item?.createByDepartmentId);

  return (
    <div className="none_wrap">
      {(isAllowEdit || isDeXuat) && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isDeXuat && isActionDelete && (
        <LightTooltip
          title={t("general.delete")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isXacNhan && !isRoleAssetUser && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.hoverCheck")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.check)}
          >
            <CheckCircleIcon />
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        className="p-5"
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InventorySuggestedSuppliesTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    voucherDetails: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenPrintDialog: false,
    tabValue: 0,
    status: null,
    isConfirm: null,
    isSatusRowData: null,
    openAdvanceSearch: false,
    handoverDepartment: null,
    receiverDepartment: null,
    fromDate: null,
    toDate: null,
  };
  voucherType = ConstantList.VOUCHER_TYPE.StockOut; //Xuất kho

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search)

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      toDate,
      fromDate,
      status,
      handoverDepartment,
      receiverDepartment,
    } = this.state;

    let searchObject = {};
    try {
      setPageLoading(true);
      searchObject.type = this.voucherType;
      searchObject.keyword = this.state?.keyword?.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.isDeXuat = true;
      searchObject.status = status?.indexOrder;
      if (handoverDepartment) {
        searchObject.departmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiptDepartmentId = receiverDepartment?.id;
      }
      if (status && !status) {
        searchObject.status = status?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }

      let res = await searchByPage(searchObject);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        let newArray = data?.content?.map((voucher) => {
          let totalAmount = 0;
          if (voucher.voucherDetails) {
            voucher.voucherDetails?.map((vd) => {
              totalAmount += vd?.amount;
            });
          }
          voucher.totalAmount = totalAmount;
          return voucher;
        });

        this.setState({
          selectItem: null,
          itemList: [...newArray],
          totalElements: data?.totalElements,
          voucherDetails: []
        });
      } else {
        toast.warning(message);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      itemList: [],
      tabValue: newValue,
      keyword: "",
    });
    if (appConst?.tabSuggestSupplies?.tabAll === newValue) {
      this.setState({
        status: null,
      }, () => this.updatePageData());
    }
    if (appConst?.tabSuggestSupplies?.tabSuggest === newValue) {
      this.setState({
        status: appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE,
      }, () => this.updatePageData());
    }
    if (appConst?.tabSuggestSupplies?.tabConfirm === newValue) {
      this.setState({
        status: appConst.listStatusSuggestSuppliesObject.DA_THONG_KE,
      }, () => this.updatePageData());
    }
    if (appConst?.tabSuggestSupplies?.tabRefuse === newValue) {
      this.setState({
        status: appConst.listStatusSuggestSuppliesObject.XAC_NHAN,
      }, () => this.updatePageData());
    }
    if (appConst?.tabSuggestSupplies?.tabApproved === newValue) {
      this.setState({
        status: appConst.listStatusSuggestSuppliesObject.TU_CHOI,
      }, () => this.updatePageData());
    }
    if (appConst?.tabSuggestSupplies?.tabReview === newValue) {
      this.setState({
        status: appConst.listStatusSuggestSuppliesObject.DANG_DUYET,
      }, () => this.updatePageData());
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenPrintDialog: false,
      item: null,
      isView: null,
      isConfirm: null,
      isReturnStatus: false,
      isEditRefuseStatus: false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      item: null,
      isView: null,
      isSatusRowData: null,
      isEditRefuseStatus: false
    });
    this.updatePageData();
    this.countByStatusVouchers();
  };

  handleConfirmationResponse = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { id } = this.state;

    setPageLoading(true);
    try {
      let res = await deleteItem(id);
      const { code, message, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        toast.success(t("general.deleteSuccess"));
        this.updatePageData();
        this.handleDialogClose();
      } else {
        toast.warning(data[0]?.errorMessage);
      }
    } catch (e) {
      toast.success(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleViews({
            id: location?.state?.id
          })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleApprove({
            id: location?.state?.id
          })
          break;
        default:
          break;
      }
      history.push(location?.state?.path, null);
    }

    this.updatePageData();
    this.countByStatusVouchers();
  }

  handleEditItem = async () => {
    this.setState({
      shouldOpenEditorDialog: true,
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { t } = this.props;
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword;
    if (this.state.status.length > 0) {
      searchObject.statuses = this.state.status;
    } else {
      searchObject.voucherStatus = this.state.status;
    }
    exportToExcel(searchObject)
      .then((res) => {
        toast.success("general.successExport");
        let blob = new Blob([res.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "InventoryDeliveryVoucher.xlsx");
      })
      .catch((err) => {
        toast.error(t("general.error"));
        toast.clearWaitingQueue();
      });
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.indexOrder:
        return (
          <span className="status status-info">
            {appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.name}
          </span>
        );
      case appConst.listStatusSuggestSuppliesObject.XAC_NHAN.indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusSuggestSuppliesObject.XAC_NHAN.name}
          </span>
        );
      case appConst.listStatusSuggestSuppliesObject.TU_CHOI.indexOrder:
        return (
          <span className="status status-error">
            {appConst.listStatusSuggestSuppliesObject.TU_CHOI.name}
          </span>
        );
      case appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.indexOrder:
        return (
          <span className="status status-warning">
            {appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.name}
          </span>
        );
      case appConst.listStatusSuggestSuppliesObject.DANG_DUYET.indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusSuggestSuppliesObject.DANG_DUYET.name}
          </span>
        );

      default:
        break;
    }
  };

  handleGetVoucherDetailsById = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    setPageLoading(true);

    try {
      let res = await getItemById(rowData.id);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        this.setState({ item: data ? { ...data, statusVoucher: data?.status } : {} });
      } else {
        toast.warning(message);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  // handleGetQuatity

  handleShowQuantity = async (rowData) => {
    let statusRowData = rowData?.status === appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.indexOrder;

    if (statusRowData) {
      this.setState({
        isSatusRowData: true,
      })
    }
  }

  handleEdit = (rowData) => {
    this.handleShowQuantity(rowData)
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
        isEditRefuseStatus: this.state?.item?.status === appConst.listStatusSuggestSuppliesObject.TU_CHOI.code,
      });
    });
  };

  handleView = (rowData) => {
    this.handleShowQuantity(rowData)
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
        isView: true,
        isSatusRowData: false,
        isReturnStatus: false
      });
    });
  };

  handleApprove = async (rowData) => {
    this.handleShowQuantity(rowData)
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenEditorDialog: true,
        isView: true,
        isConfirm: true
      });
    });
  };

  handlePrint = (rowData) => {
    this.handleGetVoucherDetailsById(rowData).then(() => {
      this.setState({
        shouldOpenPrintDialog: true,
        isView: true,
      });
    });
  };

  handleSetDataSelect = (data, name, source) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.originalCostFrom !== name &&
        variable.listInputName.listData !== source
        // && !this.state.statusIndex
      ) {
        this.search();
      }
    });
  };

  handleRowDataClick = async (rowData) => {
    let { t } = this.props;

    try {
      let res = await getItemById(rowData.id);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        this.setState({
          selectItem: data,
          voucherDetails: data?.voucherDetails
            ? data?.voucherDetails
            : []
        });
      } else {
        toast.warning(message);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
    }
  };

  countByStatusVouchers = async () => {
    try {
      const data = await countByStatus();
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        let countIsCounting,
          countIsCounted,
          countIsConfirm,
          countIsRefuse,
          countIsReivew;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.data?.forEach((item) => {
          if (appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code === item?.trangThai) {
            countIsCounting = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code === item?.trangThai) {
            countIsCounted = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code === item?.trangThai) {
            countIsConfirm = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.TU_CHOI.code === item?.trangThai) {
            countIsRefuse = checkCount(item?.soLuong);
            return;
          }
          if (appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code === item?.trangThai) {
            countIsReivew = checkCount(item?.soLuong);
            return;
          }
        });

        this.setState({
          countIsCounting,
          countIsCounted,
          countIsConfirm,
          countIsRefuse,
          countIsReivew
        })
      }

    } catch (error) {
    }
  }

  getListTab = () => {
    let { t } = this.props;
    let { tabAll, tabSuggest, tabConfirm, tabRefuse, tabApproved, tabReview } = appConst.tabSuggestSupplies;
    return [
      {
        key: tabAll,
        label: t("SuggestedSupplies.tab.tabAll")
      },
      {
        key: tabSuggest,
        label: t("SuggestedSupplies.tab.tabSuggest"),
        count: this.state.countIsCounting || 0,
        type: "info",
      },
      {
        key: tabConfirm,
        label: t("SuggestedSupplies.tab.tabConfirm"),
        count: this.state.countIsCounted || 0,
        type: "warning",
      },
      {
        key: tabReview,
        label: t("SuggestedSupplies.tab.review"),
        count: this.state.countIsReivew || 0,
        type: "success",
      },
      {
        key: tabRefuse,
        label: t("SuggestedSupplies.tab.tabRefuse"),
        count: this.state.countIsConfirm || 0,
        type: "success",
      },
      {
        key: tabApproved,
        label: t("SuggestedSupplies.tab.tabApproveds"),
        count: this.state.countIsRefuse || 0,
        type: "error",
      },
    ];
  };


  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasSuperAccess,
      isRoleAssetUser,
      tabValue,
    } = this.state;

    let TitlePage = t("SuggestedMaterials.title");
    const tabs = this.getListTab();
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasPrintPermission={hasPrintPermission}
            isRoleAssetUser={isRoleAssetUser}
            hasSuperAccess={hasSuperAccess}
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData);
              } else if (appConst.active.view === method) {
                this.handleView(rowData);
              } else if (appConst.active.check === method) {
                this.handleApprove(rowData);
              } else if (appConst.active.qrcode === method) {
                this.handlePrintQRCode(rowData.id)
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t('receipt.suggestDate'),
        field: "suggestDate",
        align: "left",
        minWidth: 110,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => {
          const newDate = new Date(rowData?.suggestDate)
          const day = String(newDate?.getDate())?.padStart(2, '0');
          const month = String(newDate?.getMonth() + 1)?.padStart(2, '0')
          const year = String(newDate?.getFullYear());
          return rowData?.suggestDate ? `${day}/${month}/${year}` : ""
        }
      },
      {
        title: t("allocation_asset.status"),
        field: "status",
        minWidth: 150,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.status),
      },
      {
        title: t("receipt.name"),
        field: "name",
        minWidth: 250,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("receipt.code"),
        field: "code",
        minWidth: 150,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "departmentName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "receiptPersonName",
        minWidth: "200px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiptDepartmentName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "personName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    let columnsDetailVourcher = [
      {
        title: t("InventoryDeliveryVoucher.stt"),
        field: "",
        minWidth: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("SuggestedSupplies.codeSupplies"),
        field: "productCode",
        minWidth: "165px",
        align: "left",
      },
      {
        title: t("SuggestedSupplies.nameSupplies"),
        field: "productName",
        align: "left",
        minWidth: "170px",
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "skuName",
        width: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        minWidth: "80px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryReceivingVoucher.dateAdded"),
        field: "receiptDate",
        minWidth: "80px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.receiptDate ? moment(rowData?.receiptDate).format("DD/MM/YYYY") : ""
      },
      {
        title: t("InventoryReceivingVoucher.expiryDate"),
        field: "expiryDate",
        minWidth: "80px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.expiryDate ? moment(rowData?.expiryDate).format("DD/MM/YYYY") : ""
      },
      {
        title: t("Product.recommendedQuantity"),
        field: "quantityOfVoucher",
        align: "left",
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.browsersQuantity"),
        field: "quantity",
        align: "left",
        width: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Product.price"),
        field: "price",
        align: "right",
        width: "130px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.price
          ? convertNumberPrice(rowData.price)
          : "",
      },
      {
        title: t("Product.amount"),
        field: "amount",
        minWidth: "130px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData?.amount)
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.materialManagement") },
              {
                name: TitlePage,
                path: "material-management/suggested-supplies",
              },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <GeneralTab tabs={tabs} value={tabValue} onChange={this.handleChangeTabValue} />
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabAll}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabSuggest}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabConfirm}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabRefuse}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabApproved}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabSuggestSupplies.tabReview}
          className="mp-0"
        >
          <ComponentDeliveryVoucherTable
            t={t}
            i18n={i18n}
            item={this.state}
            columns={columns}
            columnsDetailVourcher={columnsDetailVourcher}
            handleDialogClose={this.handleDialogClose}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            search={this.search}
            handleOKEditClose={this.handleOKEditClose}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleRowDataClick={this.handleRowDataClick}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
        </TabPanel>
      </div>
    );
  }
}
InventorySuggestedSuppliesTable.contextType = AppContext;
export default InventorySuggestedSuppliesTable;
