import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_VOUCHER = ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_DXVT_INVENTORY = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/supplies/phieu-linh";
const API_PATH_person = ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_ASSET =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
export const searchProductInVoucherByPage = (searchObject) => {
  let url = API_PATH_VOUCHER + "/searchProductInVoucherByPage";
  return axios.post(url, searchObject);
};

export const deleteItem = id => {
  return axios.delete(API_PATH_DXVT_INVENTORY + "/" + id);
};

export const getItemById = id => {
  return axios.get(API_PATH_DXVT_INVENTORY + "/" + id);
};

export const addNewOrUpdate = voucher => {
  if (voucher.id) {
    return axios.put(API_PATH_DXVT_INVENTORY + "/" + voucher.id, voucher);
  }
  else{
    return axios.post(API_PATH_DXVT_INVENTORY, voucher);
  }
};

export const searchByPage = (searchObject) => {
  var url = API_PATH_DXVT_INVENTORY + "/search-by-page";
  return axios.post(url, searchObject);
};

export const getNewCode = () => {
  let config = {
    params: {
      assetClass: appConst.assetClass.VT,
      voucherType: ConstantList.VOUCHER_TYPE.Allocation,
    },
  }; //
  return axios.get(API_PATH + "/addNew/getNewCodeByVoucherType", config);
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: 'post',
    url: ConstantList.API_ENPOINT + "/api/fileDownload/org/inventoryReceivingVoucherToExcel",
    data: searchObject,
    responseType: 'blob',
  })
}

export const getListManagementDepartment = (searchObject) => {
  var url = API_PATH_ASSET + "/getListManagementDepartment";
  return axios.post(url, searchObject);
};

export const countByStatus = () => {
  let url = API_PATH_DXVT_INVENTORY + "/count-by-status"
  return axios(url);
}