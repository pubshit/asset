import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  addNewOrUpdate,
  personSearchByPage,
} from "./InventorySuggestedSuppliesService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import SelectMultiProductPopup from "../Component/Product/SelectMultiProductPopup";
import ConstantList from "../../appConfig";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import { toast } from "react-toastify";
import { ConfirmationDialog } from 'egret';
import "react-toastify/dist/ReactToastify.css";
import { STATUS_STORE, appConst } from "app/appConst";
import { searchByPage } from "../Department/DepartmentService";
import { getListRemainingQuantity, getRemainProduct } from "../Product/ProductService";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import {
  LightTooltip,
  NumberFormatCustom,
  PaperComponent,
  convertNumberPrice,
  filterOptions,
  formatDateTimeDto,
  getRole,
  getTheHighestRole,
  handleKeyDownIntegerOnly,
  handleThrowResponseMessage,
  isSuccessfulResponse, getUserInformation,
} from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListWarehouseByDepartmentId } from "../AssetTransfer/AssetTransferService";
import { getManagementDepartment } from "app/appServices";
import AppContext from "app/appContext";
import { createFilterOptions } from "@material-ui/lab";
import AutoComplete from "@material-ui/lab/Autocomplete";
import { searchByPage as SearchByPageAsset } from "../Asset/AssetService"
import moment from "moment";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class InventoryDeliveryVoucherDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    handoverPerson: null,
    handoverDepartment: null,
    listDepartment: [],
    title: null,
    receiptDepartment: null,
    person: null,
    isConfirm: false,
    isSatusRowData: false,
    suggestDate: new Date(),
    shouldOpenStorePopup: false,
    shouldOpenPersonPopup: false,
    shouldOpenProductPopup: false,
    shouldOpenHandoverStorePopup: false,
    shouldOpenreceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenDepartmentPopup: false,
    textNotificationPopup: "",
    stockReceiptDeliveryStore: null,
    code: "",
    voucherName: "",
    voucherDetails: [],
    totalElements: 0,
    product: [],
    listAsset: [],
    amount: 0,
    type: ConstantList.VOUCHER_TYPE.StockOut,
    storeId: "",
    note: null,
    depRequest: null,
    shouldOpenRefuseTransfer: false,
    storeType: null, // kho vật tu
    status: "",
    listStatus: appConst.listStatusSuggestSupplies,
    listHandoverDepartment: []
  };
  voucherType = ConstantList.VOUCHER_TYPE.StockOut; //Xuất kho
  filterAutocomplete = createFilterOptions();

  handleChange = (event, source) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = async () => {
    let { id, voucherDetails } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let dataState = this.convertDataDto(this.state);

    if (voucherDetails && voucherDetails.length > 0) {
      try {
        const res = await addNewOrUpdate(dataState);
        const { code, data, message } = res?.data;

        if (isSuccessfulResponse(code)) {
          id
            ? toast.success(t("general.updateSuccess"))
            : toast.success(t("general.addSuccess"));
          this.props.handleOKEditClose();
        } else {
          toast.warning(data[0]?.errorMessage);
        }
      } catch (error) {
        toast.error(t("toastr.error"));
      } finally {
        setPageLoading(false);
      }
    } else {
      let text = this.props.t(
        "InventoryDeliveryVoucher.check_product_in_voucher"
      );
      this.setState(
        {
          shouldOpenNotificationPopup: true,
          textNotificationPopup: text,
        },
      );
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: formatDateTimeDto(date),
    });
  };

  handleProductPopupClose = () => {
    this.setState({
      shouldOpenProductPopup: false,
    });
  };

  handleGetSuppliesRemainQuantity = async () => {
    let { voucherDetails = [], storeId } = this.state;

    let { t } = this.props;
    let searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 100000;
    searchObject.storeId = storeId;
    searchObject.productIds = voucherDetails?.map(item => item?.productId).join(',');

    if (voucherDetails?.length === 0) return; // suppliesItems rỗng thì k cần gọi api

    try {
      let res = await getListRemainingQuantity(searchObject)
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        voucherDetails?.map(x => {
          let existItem = data?.find(
            item => item?.productId === x?.productId
              && item?.inputDate === x?.receiptDate
              && item?.lotNumber === x?.batchCode
              && item?.unitPrice === x?.price
              && item?.expiryDate === x?.expiryDate
          )
          if (existItem) {
            x.remainingQuantity = existItem.remainQuantity > 0
              ? existItem.remainQuantity
              : 0;
          }
          return x
        });
        this.setState({
          voucherDetails,
        });
      }
      else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("InventoryDeliveryVoucher.getRemainQuantityError"));
    }
  }

  handleSelectMultiProduct = (items = []) => {
    let { voucherDetails } = this.state;

    if (items != null && items.length > 0) {
      let newItems = [];

      items?.forEach(x => {

        let checkExisted = voucherDetails?.find((el) =>
          (el?.product?.id === x?.id || el?.productId === x?.id)
          && (el?.product?.inputDate === x?.inputDate || el?.receiptDate === x?.inputDate)
          && (el?.product?.lotNumber === x?.lotNumber || el?.batchCode === x?.lotNumber)
          && (el?.product?.unitPrice === x?.unitPrice || el?.price === x?.unitPrice || el?.amount === item?.unitPrice)
          && (el?.product?.code === x?.code || el?.productCode === x?.code)
        );
        let vc = {};
        vc.product = { ...x }
        vc.skuName = x?.sku?.name;
        vc.skuId = x?.sku?.id;
        vc.remainingQuantity = x?.remainingQuantity;
        vc.productName = x.name;
        vc.productCode = x.code;
        vc.productId = x.id;
        vc.price = x?.unitPrice;
        vc.quantityOfVoucher = checkExisted?.quantityOfVoucher || 1;
        vc.quantity = checkExisted?.quantity || 1;
        vc.amount = x?.unitPrice;
        vc.receiptDate = x?.inputDate;
        vc.batchCode = x?.lotNumber;
        vc.expiryDate = x?.expiryDate
        newItems.push(vc);
      })

      voucherDetails = newItems;
    }

    this.setState({ voucherDetails }, function () {
      this.handleProductPopupClose();
    });
  };

  handleSelectPerson = (item) => {
    this.setState({ receiptPerson: item });
  };

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    });
  };

  handleSelectReceiverPerson = (item) => {
    this.setState({
      person: item,
    });
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };

  handlereceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenreceiverPersonPopup: false,
    });
  };

  componentWillMount() {
    let { item } = this.props;
    const roles = getTheHighestRole();
    const {
      isRoleAssetUser,
      departmentUser,
      isRoleAssetManager,
      currentUser
    } = roles;
    
    this.setState({
      ...item,
      ...roles,
      currentStatus: item?.status
    }, function () {
      let listStatus = this.handleGetStatusOptions()
      if (item?.id) {
        let newItem = this.convertItemDto(item);
        this.setState({
          ...newItem,
          listStatus
        });
      } else {
        this.setState({
          status: appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE,
          department: isRoleAssetUser
            ? departmentUser
            : null,
          receiptDepartment: isRoleAssetManager
            ? departmentUser
            : null,
          handoverPerson: isRoleAssetManager ? {
            id: currentUser?.person?.id,
            personDisplayName: currentUser?.person?.displayName,
            text: currentUser?.person?.displayName
          } : null,
          person: isRoleAssetUser ? {
            id: currentUser?.person?.id,
            displayName: currentUser?.person?.displayName,
            text: currentUser?.person?.displayName
          } : null,
          stockReceiptDeliveryStore: {
            name: item?.storeName,
            id: item?.storeId,
          },
          listStatus
        });
      }
      this.handleGetAllListListManagementDepartment();
      this.handleGetSuppliesRemainQuantity()
    });
  }

  handleStorePopupClose = () => {
    this.setState({
      shouldOpenStorePopup: false,
    });
  };

  handleSelectStore = (item) => {
    this.setState({
      stockReceiptDeliveryStore: item,
      storeId: item?.id,
      voucherDetails: [],
    });
  };

  handleQuantityChange = (rowData, event) => {
    const { name, value } = event.target;

    const { voucherDetails } = this.state;
    voucherDetails?.forEach((element) => {
      if (element?.productId === rowData?.productId
        && element?.receiptDate === rowData?.receiptDate
        && element?.price === rowData?.price
      ) {
        element[name] = value;
        if (name === "quantityOfVoucher") {
          element.quantity = value;
          element.amount = (Number(value) / 1) * element.price;
        }
        if (name === "quantity") {
          element.amount = (Number(value) / 1) * element.price;
        }
      }
    });
    this.setState({ voucherDetails });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { voucherDetails } = this.state;
    voucherDetails.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        assetVoucher.note = event.target.value
      }
    });
    this.setState(
      { voucherDetails: voucherDetails, handoverPersonClone: true },
    );
  };

  handleConfirmationResponse = () => {
    this.setState(
      { shouldOpenNotificationPopup: false, textNotificationPopup: "" },
      function () { }
    );
  };

  selectSku = (item, rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach((vd) => {
        if (vd.productId == rowData.productId) {
          vd.sku = item;
          vd.skuType = 1;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  handleRowDeleteProduct = (rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      for (let index = 0; index < voucherDetails.length; index++) {
        if (
          voucherDetails &&
          voucherDetails[index].productId == rowData.productId
        ) {
          voucherDetails.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ voucherDetails }, function () { });
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    })
  }

  handleSelectHandOverDepartment = (item) => {
    this.setState({
      receiptDepartment: item,
      receiptDepartmentId: item?.id,
      handoverPerson: null,
      stockReceiptDeliveryStore: null,
      voucherDetails: []
    })
  };

  handleGetAllListListManagementDepartment = async () => {
    const searchObject = {
      pageIndex: 1,
      pageSize: 1000,
      isActive: STATUS_STORE.HOAT_DONG.code,
    };
    try {
      const result = await searchByPage(searchObject);
      if (result?.status === appConst.CODE.SUCCESS) {
        this.setState({ listDepartment: result?.data?.content });
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
    }
  };

  handleChangeSelect = (event, value) => {
    this.setState({
      department: value,
      departmentId: value?.id,
      person: null,
    });
  };

  handleChangeStatus = (event, value) => {
    this.setState({ status: value });
  };

  convertDataDto = (state, status) => {
    return {
      id: state?.id,
      name: state?.name,
      personId: state?.handoverPerson?.personId ?? state?.handoverPerson?.id,
      status: status || state?.status?.code,
      departmentId: state?.receiptDepartment?.id,
      receiptDepartmentId: state?.department?.id,
      receiptPersonId: state?.person?.id,
      storeId: state?.stockReceiptDeliveryStore?.id,
      voucherDetails: state?.voucherDetails,
      suggestDate: state.suggestDate
        ? formatDateTimeDto(state.suggestDate)
        : null,
      approvedDate: state.approvedDate
        ? formatDateTimeDto(state.approvedDate)
        : null,
      note: state.note,
    }
  };

  checkQuantitySupplies = (arr = []) => {
    return arr.find(x => x?.quantity > x?.remainingQuantity);
  };
  
  handleStatus = async (status) => {
    const { t, handleOKEditClose = () => { }, handleClose = () => { }, isConfirm } = this.props;
    if (!this.state?.approvedDate && isConfirm) {
      toast.warning(t('allocation_asset.isEmtyApprovedDate'));
      return;
    }
    
    if(this.checkQuantitySupplies(this.state?.voucherDetails)) {
      toast.warning(t('InventoryDeliveryVoucher.invalidQuantitySupplies'));
      return;
    }

    let dataState = this.convertDataDto(this.state, status)

    let res = await addNewOrUpdate(dataState);
    if (appConst.CODE.SUCCESS === res.data?.code) {
      handleOKEditClose();
      handleClose();
      toast.success(t("general.updateSuccess"));
    } else {
      handleThrowResponseMessage(res);
    }
  }

  handleGetStatusOptions = () => {
    let { currentStatus, id } = this.state;
    let options = appConst.listStatusSuggestSupplies
    if (!id) {
      return [
        appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE,
        appConst.listStatusSuggestSuppliesObject.DA_THONG_KE,
      ]
    }
    switch (currentStatus) {
      case appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code:
        return [
          appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE,
          appConst.listStatusSuggestSuppliesObject.DA_THONG_KE,
          // appConst.listStatusSuggestSuppliesObject.TU_CHOI
        ]
      case appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code:
        return [
          appConst.listStatusSuggestSuppliesObject.DA_THONG_KE,
          appConst.listStatusSuggestSuppliesObject.XAC_NHAN,
          appConst.listStatusSuggestSuppliesObject.TU_CHOI,
        ]
      case appConst.listStatusSuggestSuppliesObject.TU_CHOI.code:
        return [
          appConst.listStatusSuggestSuppliesObject.TU_CHOI,
          appConst.listStatusSuggestSuppliesObject.DA_THONG_KE,
        ];
      case appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code:
        return [appConst.listStatusSuggestSuppliesObject.XAC_NHAN];
      default: return options;
    }
  };

  handleSearchAsset = (e) => {
    let searchObject = {
      keyword: e?.target?.value,
      pageIndex: 1,
      pageSize: 10,
    }
    try {
      SearchByPageAsset(searchObject).then((res) => {
        if (isSuccessfulResponse(res?.data?.code)) {
          this.setState({
            listAsset: res?.data?.data?.content
          })
        }
      });
    } catch (error) {
      console.error(error);
    }
  };

  handleSelectRowDataChange = (value, rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails.length > 0) {
      voucherDetails.forEach((element) => {
        if (element.productId === rowData.productId) {
          element.asset = value;
          element.assetName = value?.name;
          element.assetId = value?.id;
        }
      });
      this.setState({ voucherDetails });
    }
  };

  addValidationRule = () => {
    ValidatorForm.addValidationRule("isLengthValid", (value) => {
      return !this.isCheckLenght(value, 255);
    });
  };

  convertItemDto = (item) => {    
    return {
      status: appConst.listStatusSuggestSupplies.find(
        (i) => i.code === item.status
      ),
      receiptDepartment: {
        name: item?.departmentName,
        id: item?.departmentId,
        text: item?.departmentName,
      },
      department: {
        name: item?.receiptDepartmentName,
        id: item?.receiptDepartmentId,
        text: item?.receiptDepartmentName,
      },
      stockReceiptDeliveryStore: {
        name: item?.storeName,
        id: item?.storeId,
      },
      person: {
        id: item?.receiptPersonId,
        displayName: item?.receiptPersonName,
        text: item?.receiptPersonName
      },
      handoverPerson: {
        personDisplayName: item?.personName,
        id: item?.personId,
        text: item?.personName
      },
      voucherDetails: item?.voucherDetails?.map(product => {
        product.asset = {
          name: product.assetName,
          id: product.assetId,

        }
        return product
      })
    }
  };

  setListData = (data, source) => {
    this.setState({ [source]: data })
  }
  render() {
    let { open, t, i18n, isView, isConfirm, isSatusRowData, isEditRefuseStatus } = this.props;

    let {
      id,
      receiptDepartment,
      suggestDate,
      rowsPerPage,
      page,
      shouldOpenProductPopup,
      isRoleAssetUser,
      isRoleAssetManager,
      product,
      person,
      stockReceiptDeliveryStore,
      voucherDetails,
      shouldOpenNotificationPopup,
      textNotificationPopup,
      storeId,
      listDepartment,
      department,
      status,
      listAsset,
      name,
      handoverPerson,
      listStatus,
      departmentUser,
      isRoleOrgAdmin,
      approvedDate,
      statusVoucher
    } = this.state;

    const isChoXuLy = statusVoucher ? statusVoucher === appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code : true;
    const isDangDuyet = statusVoucher === appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code;

    let objPermission = {
      [appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: {
        rcmQuantity: {
          disable: false,
          readOnly: false,
        },
        approvedQuantity: {
          disable: true,
          readOnly: true,
        },
      },
      [appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: {
        rcmQuantity: {
          disable: true,
          readOnly: true,
        },
        approvedQuantity: {
          disable: false,
          readOnly: false,
        }
      },
      [appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: {
        rcmQuantity: {
          disable: false,
          readOnly: false,
        },
        approvedQuantity: {
          disable: false,
          readOnly: false,
        }
      },
      [appConst.listStatusSuggestSuppliesObject.TU_CHOI.code]: {
        rcmQuantity: {
          disable: false,
          readOnly: false,
        },
        approvedQuantity: {
          disable: true,
          readOnly: true,
        },
        receiptDepartment: {
          disable: true,
          readOnly: true,
        }
      },
      [null]: {
        rcmQuantity: {
          disable: false,
          readOnly: false,
        },
        approvedQuantity: {
          disable: true,
          readOnly: true,
        }
      },
      [undefined]: {
        rcmQuantity: {
          disable: false,
          readOnly: false,
        },
        approvedQuantity: {
          disable: true,
          readOnly: true,
        }
      },
    }
    const permission = objPermission[statusVoucher];

    let isShowApproveDate = [
      appConst.listStatusSuggestSuppliesObject.TU_CHOI.code,
      appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code,
    ].includes(status?.code)
      || isConfirm;

    let searchObjectStore = {
      pageIndex: 1,
      pageSize: 100000,
      isActive: STATUS_STORE.HOAT_DONG.code,
      departmentId: receiptDepartment?.id,
    };
    let searchObjectPerson = {
      pageIndex: 1,
      pageSize: 1000,
      departmentId: department?.id,
    };
    let searchObjectReceiptPerson = {
      pageIndex: 1,
      pageSize: 1000,
      departmentId: receiptDepartment?.id,
    };

    let columns = [
      {
        title: t("general.action"),
        field: "valueText",
        minWidth: "100px",
        hidden: isView,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <LightTooltip
            title={t("general.delete")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => this.handleRowDeleteProduct(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("InventoryDeliveryVoucher.stt"),
        field: "",
        minWidth: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("SuggestedSupplies.codeSupplies"),
        field: "productCode",
        minWidth: "165px",
        align: "left",
      },
      {
        title: t("SuggestedSupplies.nameSupplies"),
        field: "productName",
        align: "left",
        minWidth: "170px",
      },
      {
        title: t("Product.stockKeepingUnit"),
        field: "skuName",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryReceivingVoucher.batchCode"),
        field: "batchCode",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("InventoryReceivingVoucher.dateAdded"),
        field: "receiptDate",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.receiptDate ? moment(rowData?.receiptDate).format("DD/MM/YYYY") : ""
      },
      {
        title: t("InventoryReceivingVoucher.expiryDate"),
        field: "expiryDate",
        minWidth: "120px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.expiryDate ? moment(rowData?.expiryDate).format("DD/MM/YYYY") : ""
      },
      {
        title: t("Product.recommendedQuantity"),
        field: "quantityOfVoucher",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            name="quantityOfVoucher"
            id="formatted-numberformat-originalCost"
            value={rowData.quantityOfVoucher || ""}
            validators={[
              "minFloat:0.000000001",
              "required",
              `maxNumber:${rowData.remainingQuantity}`,
              "matchRegexp:^\\d+(\\.\\d{1,2})?$"
            ]}
            errorMessages={[
              t("general.minNumberError"),
              t("general.required"),
              t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
              t("general.quantityError")
            ]}
            disabled={permission?.rcmQuantity?.disable}
            InputProps={{
              inputProps: {
                style: {
                  textAlign: "center",
                },
              },
              readOnly: permission?.rcmQuantity?.readOnly
            }}
          />
        ),
      },
      {
        title: t("Product.browsersQuantity"),
        field: "quantity",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            onChange={(event) => this.handleQuantityChange(rowData, event)}
            // onKeyDown={handleKeyDownIntegerOnly}
            // type="number"
            name="quantity"
            id="formatted-numberformat-originalCost"
            value={rowData.quantity}
            validators={[
              "minNumber:0",
              "required",
              `maxNumber:${rowData.remainingQuantity}`,
              "matchRegexp:^\\d+(\\.\\d{1,2})?$"
            ]}
            errorMessages={[
              t("general.minNumberError"),
              t("general.required"),
              t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
              t("general.quantityError")
            ]}
            disabled={permission?.approvedQuantity?.disable}
            InputProps={{
              inputProps: {
                style: {
                  textAlign: "center",
                },
              },
              readOnly: permission?.approvedQuantity?.readOnly
            }}
          />
        ),
      },
      {
        title: t("InventoryDeliveryVoucher.remainingQuantity"),
        align: "center",
        hidden: this.state.currentStatus ===  appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code,
        minWidth: "90px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => <span>{rowData?.remainingQuantity || 0}</span>,
      },
      {
        title: t("InventoryDeliveryVoucher.selectAsset"),
        field: "valueText",
        minWidth: 200,
        render: (rowData) => isView || isSatusRowData ? (
          <TextValidator
            fullWidth
            InputProps={{
              readOnly: true,
            }}
            name="note"
            label={t("Asset.searchAsset")}
            value={rowData?.asset?.name || ""}
          />
        ) : (
          <AutoComplete
            id="combo-box"
            size="small"
            options={listAsset}
            onChange={(event, value) => this.handleSelectRowDataChange(
              value,
              rowData,
            )}
            value={rowData?.asset || null}
            getOptionLabel={(option) => option.name || ""}
            filterOptions={(options, params) => filterOptions(options, params)}
            renderInput={(params) => (
              <TextValidator
                {...params}
                label={t("Asset.searchAsset")}
                placeholder={t("Asset.SimilarProperty")}
                variant="standard"
                onChange={this.handleSearchAsset}
                onFocus={this.handleSearchAsset}
                value={""}
              />
            )}
            noOptionsText={t("general.noOption")}
          />
        ),
      },
      {
        title: t("Product.price"),
        field: "price",
        align: "left",
        minWidth: "130px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.price
          ? convertNumberPrice(rowData.price)
          : "",
      },
      {
        title: t("Product.amount"),
        field: "amount",
        minWidth: "130px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData.amount
          ? convertNumberPrice(rowData.amount)
          : "",
      },
      {
        title: t("Asset.note"),
        field: "note",
        minWidth: 180,
        align: "left",
        render: rowData =>
          <TextValidator
            multiline
            maxRows={2}
            className="w-100"
            onChange={(event) => this.handleRowDataCellChange(rowData, event)}
            type="text"
            name="note"
            value={rowData.note || ""}
            InputProps={{
              readOnly: isView && !isConfirm,
            }}
          />
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        <CustomValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "0px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("SuggestedSupplies.dialog")}</span>
          </DialogTitle>
          <DialogContent style={{ minHeight: "450px" }}>
            <Grid container spacing={1}>
              <Grid container spacing={1}>
                <Grid item md={4} sm={12} xs={12}>
                  <CustomValidatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed"> * </span>
                        {t("SuggestedSupplies.issueDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    name={"suggestDate"}
                    value={suggestDate}
                    onChange={(date) =>
                      this.handleDateChange(date, "suggestDate")
                    }
                    disabled={!isChoXuLy}
                    readOnly={isView}
                    InputProps={{
                      readOnly: isView,
                    }}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    maxDate={new Date()}
                    maxDateMessage={t("allocation_asset.maxDateMessage")}
                  />
                </Grid>
                {isShowApproveDate && <Grid item md={4} sm={12} xs={12}>
                  <CustomValidatePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed"> * </span>
                        {t('Asset.approvedDate')}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    name={"approvedDate"}
                    value={approvedDate}
                    InputProps={{
                      readOnly: true,
                    }}
                    onChange={(date) =>
                      this.handleDateChange(date, "approvedDate")
                    }
                    readOnly={!isConfirm}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                    minDate={suggestDate}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    maxDateMessage={t("general.maxDateDefault")}
                    minDateMessage={t("allocation_asset.minApprovedDateMessage")}
                  />
                </Grid>}
                <Grid item md={isShowApproveDate ? 4 : 8} sm={12} xs={12}>
                  <TextValidator
                    onChange={this.handleChange}
                    type="text"
                    className="w-100"
                    name="name"
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("InventoryDeliveryVoucher.name")}
                      </span>
                    }
                    InputProps={{
                      readOnly: !isChoXuLy || isView,
                    }}
                    value={name}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  {isView ? (
                    <TextValidator
                      fullWidth
                      InputProps={{
                        readOnly: true,
                      }}
                      name="note"
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("InventoryReceivingVoucher.status")}
                        </span>
                      }
                      value={status?.name}
                    />
                  ) : (
                    <Autocomplete
                      id="combo-box"
                      className="mt-3"
                      fullWidth
                      size="small"
                      name="status"
                      options={listStatus}
                      onChange={(event, value) =>
                        this.handleChangeStatus(event, value)
                      }
                      getOptionLabel={(option) => option?.name}
                      filterOptions={filterOptions}
                      value={status}
                      renderInput={(params) => (
                        <TextValidator
                          {...params}
                          label={
                            <span>
                              <span className="colorRed">*</span>
                              {t("InventoryReceivingVoucher.status")}
                            </span>
                          }
                          variant="standard"
                          value={status ? status?.name : null}
                          inputProps={{
                            ...params.inputProps,
                          }}
                          validators={["required"]}
                          errorMessages={[t("general.required")]}
                        />
                      )}
                      noOptionsText={t("general.noOption")}
                    // disabled={!isChoXuLy || isView}
                    />
                  )}
                </Grid>
                {/* phòng bàn giao */}
                <Grid item md={4} sm={12} xs={12}>
                  {isView ? (
                    <TextValidator
                      fullWidth
                      InputProps={{
                        readOnly: true,
                      }}
                      name="note"
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("InventoryDeliveryVoucher.handoverDepartment")}
                        </span>
                      }
                      value={receiptDepartment?.name || ""}
                    />
                  ) : (
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          <span className="colorRed">*</span>
                          {t("InventoryDeliveryVoucher.handoverDepartment")}
                        </span>
                      }
                      searchFunction={getManagementDepartment}
                      listData={this.state.listHandoverDepartment}
                      setListData={(value) => this.setListData(value, "listHandoverDepartment")}
                      typeReturnFunction="list"
                      isNoRenderChildren
                      isNoRenderParent
                      displayLable={"name"}
                      value={receiptDepartment || ""}
                      onSelect={this.handleSelectHandOverDepartment}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                      readOnly={!isChoXuLy || isView}
                    />
                  )}
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("InventoryDeliveryVoucher.handoverPerson")}
                      </span>
                    }
                    searchFunction={getListUserByDepartmentId}
                    searchObject={searchObjectReceiptPerson}
                    displayLable="personDisplayName"
                    typeReturnFunction="category"
                    readOnly={!isChoXuLy}
                    value={handoverPerson || ""}
                    onSelect={this.handleSelectHandoverPerson}
                    disabled={!receiptDepartment?.id}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                </Grid>
                <Grid item md={4} sm={6} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("InventoryDeliveryVoucher.store")}
                      </span>
                    }
                    searchFunction={getListWarehouseByDepartmentId}
                    searchObject={searchObjectStore}
                    displayLable="name"
                    readOnly={isView || isEditRefuseStatus}
                    disabled={!receiptDepartment?.id}
                    value={stockReceiptDeliveryStore || null}
                    onSelect={this.handleSelectStore}
                    validators={["required"]}
                    errorMessages={[t("general.required")]}
                  />
                </Grid>
                {/* Phòng tiếp nhận */}
                <Grid item md={4} sm={12} xs={12}>
                  {permission?.receiptDepartment?.readOnly || isView ? (
                    <TextValidator
                      fullWidth
                      InputProps={{
                        readOnly: true,
                      }}
                      name="note"
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("InventoryDeliveryVoucher.receiverDepartment")}
                        </span>
                      }
                      value={department?.text}
                    />
                  ) : (
                    <Autocomplete
                      className="mt-3"
                      id="combo-box"
                      fullWidth
                      size="small"
                      name="room"
                      options={listDepartment}
                      onChange={(event, value) =>
                        this.handleChangeSelect(event, value)
                      }
                      getOptionLabel={(option) => option.text}
                      value={department ? department : null}
                      filterOptions={(options, params) => filterOptions(
                        options, params
                      )}
                      renderInput={(params) => (
                        <TextValidator
                          {...params}
                          label={
                            <span>
                              <span className="colorRed">*</span>
                              {t("InventoryDeliveryVoucher.receiverDepartment")}
                            </span>
                          }
                          variant="standard"
                          value={department?.text || ""}
                          inputProps={{
                            ...params.inputProps,
                          }}
                          validators={["required"]}
                          errorMessages={[t("general.required")]}
                        />
                      )}
                      noOptionsText={t("general.noOption")}
                      disabled={isRoleAssetUser}
                    />
                  )}
                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                  {isView ? (
                    <TextValidator
                      fullWidth
                      InputProps={{
                        readOnly: true,
                      }}
                      name="note"
                      label={t("InventoryDeliveryVoucher.receiverPerson")}
                      value={person?.displayName}
                    />
                  ) : (
                    <AsynchronousAutocompleteSub
                      label={t("InventoryDeliveryVoucher.receiverPerson")}
                      searchFunction={personSearchByPage}
                      searchObject={searchObjectPerson}
                      defaultValue={person}
                      displayLable={"displayName"}
                      value={person ? person : null}
                      readOnly={!isChoXuLy}
                      onSelect={this.handleSelectReceiverPerson}
                      disabled={!department?.id}
                    />
                  )}
                </Grid>
              </Grid>
              <Grid container spacing={1}>
                <Grid item md={3} sm={6} xs={12}>
                  {!isView && <Button
                    size="small"
                    className=" mt-16"
                    variant="contained"
                    color="primary"
                    disabled={!stockReceiptDeliveryStore?.id}
                    onClick={() =>
                      this.setState({
                        shouldOpenProductPopup: true,
                        item: {},
                      })
                    }
                  >
                    {t("component.product.title")}
                  </Button>}
                  {shouldOpenProductPopup && (
                    <SelectMultiProductPopup
                      open={shouldOpenProductPopup}
                      handleSelect={this.handleSelectMultiProduct}
                      product={product != null ? product : []}
                      handleClose={this.handleProductPopupClose}
                      t={t}
                      i18n={i18n}
                      voucherType={ConstantList.VOUCHER_TYPE.StockIn}
                      voucherId={id ? id : null}
                      selectedItem={{ product: null }}
                      storeId={
                        this.state.stockReceiptDeliveryStore != null &&
                          this.state.stockReceiptDeliveryStore.id != null
                          ? this.state.stockReceiptDeliveryStore.id
                          : storeId
                      }
                      assetVouchers={voucherDetails?.length > 0 ? [...voucherDetails] : []}
                      date={this.state?.suggestDate}
                    />
                  )}
                </Grid>
              </Grid>

              <Grid container spacing={2} className="mt-12">
                <Grid item xs={12}>
                  <MaterialTable
                    data={voucherDetails}
                    columns={columns}
                    options={{
                      sorting: false,
                      toolbar: false,
                      selection: false,
                      actionsColumnIndex: -1,
                      paging: false,
                      search: false,
                      rowStyle: (rowData) => ({
                        backgroundColor:
                          rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                      }),
                      maxBodyHeight: "253px",
                      minBodyHeight: "253px",
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                      },
                      padding: "dense",
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div style={{ witdth: "100%" }}>
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                    onSelectionChange={(rows) => {
                      this.data = rows;
                    }}
                  />
                </Grid>
              </Grid>
              {this.state?.shouldOpenRefuseTransfer &&
                <ConfirmationDialog
                  title={t('general.confirm')}
                  open={this.state?.shouldOpenRefuseTransfer}
                  onConfirmDialogClose={this.handleCloseRefuseTransferDialog}
                  onYesClick={() => this.handleStatus(appConst.STATUS_RECEIPT.refuse.indexOrder)}
                  text={t('general.cancel_receive_assets')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              }
            </Grid>
          </DialogContent>
          <DialogActions className="zIndex-9999 position-sticky bottom-0 right-0">
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className=""
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {isConfirm ? <>
                {!isDangDuyet && (
                  isRoleOrgAdmin
                  || (this.state?.departmentId === departmentUser?.id)
                ) && <Button
                  className="ml-12"
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(appConst.listStatusSuggestSuppliesObject.DANG_DUYET.indexOrder)}
                >
                    {t("SuggestedSupplies.review")}
                  </Button>}
                <Button
                  className="ml-12"
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleStatus(appConst.listStatusSuggestSuppliesObject.XAC_NHAN.indexOrder)}
                >
                  {t("InstrumentToolsTransfer.confirm")}
                </Button>
                <Button
                  className="btnRefuse ml-12"
                  variant="contained"
                  onClick={() => {
                    this.setState({
                      shouldOpenRefuseTransfer: true
                    });
                  }}
                >
                  {t("InstrumentToolsTransfer.refuse")}
                </Button>
              </> : <>
                {!isView && (
                  <Button
                    variant="contained"
                    className="ml-12"
                    color="primary"
                    type="submit"
                  >
                    {t("general.save")}
                  </Button>
                )}
              </>}
            </div>
          </DialogActions>
        </CustomValidatorForm>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}
      </Dialog>
    );
  }
}
InventoryDeliveryVoucherDialog.contextType = AppContext;
export default InventoryDeliveryVoucherDialog;
