import React, { useContext } from "react";
import {
  Grid,
  TablePagination,
  Button,
  FormControl,
  Input,
  InputAdornment,
  Collapse,
  Card,
  TextField,
} from "@material-ui/core";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import CardContent from "@material-ui/core/CardContent";
import InventoryDeliveryVoucherDialog from "./InventorySuggestedSuppliesDialog";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { Autocomplete } from '@material-ui/lab';
import {convertNumberPriceRoundUp, defaultPaginationProps, filterOptions, getUserInformation} from "app/appFunction";
import { getListManagementDepartment, searchReceiverDepartment, } from '../Receipt/ReceiptService';
import { ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { handleKeyUp } from "app/appFunction";
import { appConst, variable } from "app/appConst";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import {receiptPrintData} from "../FormCustom/Receipt";


export default function ComponentDeliveryVoucherTable(props) {
  let {
    item,
    t, i18n,
    handleRowDataClick,
  } = props;
  let { voucherDetails } = item;
  let {
    listHandoverDepartment,
    handoverDepartment,
    listReceiverDepartment,
    receiverDepartment,
    toDate,
    fromDate,
    status,
    tabValue,
    selectItem
  } = props.item
  const { currentOrg } = useContext(AppContext);
  const { RECEIPT } = LIST_PRINT_FORM_BY_ORG.SUPPLIES_MANAGEMENT;
  let isButtonAll = tabValue === appConst.OBJECT_TAB_RECEIPT.all;
  const searchObject = { pageIndex: 1, pageSize: 1000000 };
  const searchObjectDepartment = {
    ...searchObject,
    checkPermissionUserDepartment: false
  }

  let dataView = receiptPrintData(item);

  return (
    <div>
      <Grid container spacing={3} justifyContent="space-between">

        {item.shouldOpenPrintDialog && (
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={props?.handleDialogClose}
            open={item.shouldOpenPrintDialog}
            item={dataView || item?.item}
            title={t("Phiếu lĩnh vật tư")}
            urls={[
              ...RECEIPT.GENERAL,
              ...(RECEIPT[currentOrg?.printCode] || []),
            ]}
          />
        )}
        <Grid item md={3} sm={12} xs={12}>
          <Button
            className="mb-12 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleEditItem}
          >
            {t("InventoryDeliveryVoucher.add")}
          </Button>
          <Button
            className="mb-12 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleOpenAdvanceSearch}
          >
            {t("general.advancedSearch")}
            <ArrowDropDownIcon />
          </Button>
          {/* <Button
              className="mb-12 align-bottom"
              variant="contained"
              color="primary"
              onClick={props.exportToExcel}
            >
              {t("general.exportToExcel")}
            </Button> */}
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box w-100"
              onChange={props.handleTextChange}
              onKeyDown={props.handleKeyDownEnterSearch}
              onKeyUp={(e) => handleKeyUp(e, props.search)}
              placeholder={t("SuggestedSupplies.enterSearch")}
              id="search_box"
              startAdornment={
                <InputAdornment>
                  <Link>
                    {" "}
                    <SearchIcon
                      onClick={() => props.search()} //keyword
                      style={{ position: "absolute", top: "0", right: "0" }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>
        {/* Bộ lọc Tìm kiếm nâng cao */}
        <Grid item xs={12} style={{ padding: "0 8px" }}>
          <Collapse in={props.item.openAdvanceSearch} >
            <ValidatorForm onSubmit={() => { }}>
              <Card elevation={2}>
                <CardContent>
                  <Grid container spacing={2}>
                    {/* Phòng bàn giao */}
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteTransfer
                        label={t("allocation_asset.handoverDepartment")}
                        searchFunction={getListManagementDepartment}
                        searchObject={searchObject}
                        listData={listHandoverDepartment || []}
                        typeReturnFunction="list"
                        setListData={(data) => props?.handleSetDataSelect(
                          data,
                          "listHandoverDepartment",
                          variable.listInputName.listData,
                        )}
                        defaultValue={handoverDepartment ? handoverDepartment : null}
                        displayLable={"text"}
                        value={handoverDepartment ? handoverDepartment : null}
                        onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    {/* Phòng tiếp nhận */}
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteTransfer
                        className="w-100"
                        label={t("allocation_asset.receiverDepartment")}
                        searchFunction={searchReceiverDepartment}
                        searchObject={searchObjectDepartment}
                        listData={listReceiverDepartment || []}
                        setListData={(data) => props.handleSetDataSelect(
                          data,
                          "listReceiverDepartment",
                          variable.listInputName.listData,
                        )}
                        defaultValue={receiverDepartment ? receiverDepartment : null}
                        displayLable={"text"}
                        value={receiverDepartment ? receiverDepartment : null}
                        onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim()
                          return filterOptions(options, params)
                        }}
                        // disabled={isRoleAssetUser}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    {/* Từ ngày */}
                    <Grid item xs={12} sm={12} md={3}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("MaintainPlaning.dxFrom")}
                          format="dd/MM/yyyy"
                          value={fromDate ?? null}
                          onChange={(data) => props.handleSetDataSelect(
                            data,
                            "fromDate"
                          )}
                          maxDate={toDate || undefined}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={t("general.maxDateMessage")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    {/* Đến ngày */}
                    <Grid item xs={12} sm={12} md={3}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("MaintainPlaning.dxTo")}
                          format="dd/MM/yyyy"
                          value={toDate ?? null}
                          onChange={(data) => props.handleSetDataSelect(
                            data,
                            "toDate"
                          )}
                          minDate={fromDate || undefined}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={t("general.maxDateMessage")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                          clearLabel={t("general.remove")}
                          cancelLabel={t("general.cancel")}
                          okLabel={t("general.select")}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    {/* Trạng thái */}
                    {<Grid item xs={12} sm={12} md={3}>
                      <Autocomplete
                        fullWidth
                        options={appConst.LIST_RECEIPT}
                        defaultValue={status ? status : null}
                        disabled={!isButtonAll}
                        value={status ? status : null}
                        onChange={(e, value) => props.handleSetDataSelect(value, "status")}
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim()
                          return filterOptions(options, params)
                        }}
                        getOptionLabel={option => option.name}
                        noOptionsText={t("general.noOption")}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            value={status?.name || ""}
                            label={t("maintainRequest.status")}
                          />
                        )}
                      />
                    </Grid>}
                  </Grid>
                </CardContent>
              </Card>
            </ValidatorForm>
          </Collapse>
        </Grid>

        <Grid item xs={12}>
          <div>
            {item.shouldOpenEditorDialog && (
              <InventoryDeliveryVoucherDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={item.shouldOpenEditorDialog}
                handleOKEditClose={props.handleOKEditClose}
                item={item.item}
                isView={item.isView}
                isConfirm={item?.isConfirm}
                isSatusRowData={item?.isSatusRowData}
                isReturnStatus={item?.isReturnStatus}
                isEditRefuseStatus={item?.isEditRefuseStatus}
              />
            )}
            {item.shouldOpenConfirmationDialog && (
              <ConfirmationDialog
                title={t("general.confirm")}
                open={item.shouldOpenConfirmationDialog}
                onConfirmDialogClose={props.handleDialogClose}
                onYesClick={props.handleConfirmationResponse}
                text={t("general.deleteConfirm")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}
          </div>
          <MaterialTable
            title={t("general.list")}
            data={item.itemList}
            columns={props.columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              sorting: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor: selectItem?.id === rowData?.id
                  ? "#ccc"
                  : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "490px",
              minBodyHeight: "260px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
            onRowClick={(e, rowData) => {
              handleRowDataClick(rowData)
            }}
          />
          <TablePagination
            {...defaultPaginationProps()}
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            count={item.totalElements}
            rowsPerPage={item.rowsPerPage}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            page={item.page}
            onChangePage={props.handleChangePage}
            onChangeRowsPerPage={props.setRowsPerPage}
          />
          <MaterialTable
            title={t("general.list")}
            data={voucherDetails}
            columns={props.columnsDetailVourcher}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              sorting: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "490px",
              minBodyHeight: "260px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </Grid>
    </div>
  )
}
