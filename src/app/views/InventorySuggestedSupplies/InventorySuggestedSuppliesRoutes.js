import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";
const InventorySuggestedSuppliesTable = EgretLoadable({
  loader: () => import("./InventorySuggestedSuppliesTable"),
});
const ViewComponent = withTranslation()(InventorySuggestedSuppliesTable);

const InventorySuggestedSuppliesRoutes = [
  {
    path: ConstantList.ROOT_PATH + "material-management/suggested-supplies",
    exact: true,
    component: ViewComponent,
  },
];

export default InventorySuggestedSuppliesRoutes;
