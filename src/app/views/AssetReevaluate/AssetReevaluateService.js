import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/dgl-danhgialai/";
const API_PATH_MAINTAIN =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset";

export const searchByPage = (searchObject) => {
  return axios.post(API_PATH + "search-by-page", searchObject);
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + id);
};

export const saveItem = (item) => {
  return axios.post(API_PATH, item);
};
export const updateItem = (item) => {
  return axios.put(API_PATH + item.id, item);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + id);
};

const API_PATH_TYPE_CODE =
  ConstantList.API_ENPOINT + "/api/commonobject/list/objecttypecode";

export const getListReasons = () => {
  return axios.post(API_PATH_TYPE_CODE, { code: "LD" });
};

export const createReevaluate = (data) => {
  return axios.post(API_PATH, data);
};

export const updateReevaluate = (data) => {
  return axios.put(API_PATH, data);
};

export const deleteReevaluate = (id) => {
  return axios.delete(API_PATH + id);
};

export const getListAssetSourceApi = (searchObject) => {
  let url =
    ConstantList.API_ENPOINT +
    `/api/v1/asset-sources/page?keyword=${searchObject.keyword}&pageIndex=${searchObject.pageIndex}&pageSize=${searchObject.pageSize}`;
  return axios.get(url);
};

export const getByPageDsPhieuDanhGiaOfVoucher = (searchObject) => {
  let url = API_PATH + "asset-voucher/search-by-page";
  return axios.post(url, searchObject);
};

export const getCountStatus = () => {
  return axios.get(API_PATH + "count-by-status");
};

export const getListAssetSourceByAssetId = (id) => {
  return axios(API_PATH_MAINTAIN + `/${id}/asset-sources`)
}