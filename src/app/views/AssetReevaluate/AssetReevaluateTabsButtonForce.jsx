import {
  Button,
  FormControl,
  Grid,
  Icon,
  IconButton,
  MenuItem,
  makeStyles,
} from "@material-ui/core";
import React from "react";
import {
  SelectValidator,
  TextValidator,
} from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AssetCapitalTable from "./components/AssetCapitalTable";
import AssetAtrophyTable from "./components/AssetAtrophyTable";
import { getListReasons } from "./AssetReevaluateService";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { appConst } from "app/appConst";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import MaterialTable, { MTableToolbar } from "material-table";
import { createFilterOptions } from "@material-ui/lab";
import { useState } from "react";
import { searchByPage } from "../Council/CouncilService";
import {LightTooltip, TabPanel} from "../Component/Utilities";
import {a11yProps} from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AssetReevaluateTabsButtonForce(props) {
  const t = props.t;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [listReevaluateBoard, setListReevaluateBoard] = useState(null);
  const [listReevaluatePerson, setListReevaluatePerson] = useState(
    props?.item?.dataAssetReevaluate?.persons || []
  );
  const [reevaluateBoard, setReevaluateBoard] = useState([]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let searchObject = {
    keyword: "",
    pageIndex: 1,
    pageSize: 1000000,
    type: appConst.OBJECT_HD_TYPE.DANH_GIA_LAI.code,
    isActive: appConst.STATUS_HOI_DONG.HOAT_DONG.code
  };
  const filterAutocomplete = createFilterOptions();
  const columns = [
    {
      title: t("AssetReevaluateDialog.STT"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView &&
    props?.item?.dataAssetReevaluate?.dglTrangthai !==
      appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder
      ? [
          {
            title: t("general.action"),
            field: "",
            align: "center",
            maxWidth: 100,
            cellStyle: {
              paddingLeft: 10,
              paddingRight: 10,
              textAlign: "center",
            },
            render: (rowData) =>
              !props?.item?.isView && (
                <div className="none_wrap">
                  <LightTooltip
                    title={t("general.deleteIcon")}
                    placement="top"
                    enterDelay={300}
                    leaveDelay={200}
                  >
                    <IconButton
                      size="small"
                      onClick={() => handleRowDataCellDelete(rowData)}
                    >
                      <Icon fontSize="small" color="error">
                        delete
                      </Icon>
                    </IconButton>
                  </LightTooltip>
                </div>
              ),
          },
        ]
      : []),
    {
      title: t("AssetReevaluateDialog.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetReevaluateDialog.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("AssetReevaluateDialog.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) =>
        appConst.listHdChiTietsRole.find((item) => item?.code === rowData?.role)
          ?.name,
    },
    {
      title: t("AssetReevaluateDialog.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    },
  ];

  const handleRowDataCellDelete = (item) => {
    let newDataRow = listReevaluatePerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListReevaluateUser(newDataRow);
    setListReevaluatePerson(newDataRow || []);
  };

  const handleChangeSelectBoardReevaluate = (item) => {
    setReevaluateBoard(item);
    props.handleSetHoiDongId(item?.id);
    props.handleSetListReevaluateUser(item?.hdChiTiets);
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map((chitiet) => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName,
      };
      return chitiet;
    });
    setListReevaluatePerson(item?.hdChiTiets || []);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("AssetReevaluateDialog.tabInfo")} {...a11yProps(0)} />
          <Tab
            label={t("AssetReevaluateDialog.evaluationBoard")}
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid className="" container spacing={2}>
          <Grid item lg={4} md={4} sm={12} xs={12}>
            <Grid
              container
              spacing={1}
              style={{
                margin: "0",
              }}
            >
              <Grid item lg={6} md={6} sm={5} xs={12} className="mt-10">
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog"
                  style={{ marginTop: "2px" }}
                  disabled={true}
                  label={
                    <span>
                      <span style={{ color: "red" }}>*</span>
                      {t("AssetReevaluateDialog.dateCreate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.dataAssetReevaluate?.dglNgayLap}
                  onChange={(date) =>
                    props.handleChangeDate(date, "dglNgayLap")
                  }
                  validators={["required"]}
                  errorMessages={t("general.required")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={7} xs={12} className="mt-10">
                <CustomValidatePicker
                  margin="none"
                  fullWidth
                  id="date-picker-dialog"
                  style={{ marginTop: "2px" }}
                  disabled={props?.isView}
                  label={
                    <span>
                      <span style={{ color: "red" }}>*</span>
                      {t("AssetReevaluateDialog.dateReevaluate")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  value={props?.dataAssetReevaluate?.dglThoiGian}
                  onChange={(date) =>
                    props.handleChangeDate(date, "dglThoiGian")
                  }
                  validators={["required"]}
                  errorMessages={t("general.required")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  minDate={props?.dataAssetReevaluate?.dglNgayLap}
                  maxDate={new Date()}
                  maxDateMessage={t(
                    "AssetReevaluateDialog.errDateReevaluate"
                  )}
                  minDateMessage={props?.dataAssetReevaluate?.dglNgayLap ? t("AssetReevaluateDialog.errToDateReevaluate") : t("general.minDateDefault")}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid
                item container
                spacing={1} lg={12} md={12} sm={12} xs={12}
                className="mt-10"
              >
                <Grid
                  item
                  lg={
                    props?.dataAssetReevaluate?.id ||
                    props.type === "hasSelectedAsset"
                      ? 12
                      : 9
                  }
                  md={
                    props?.dataAssetReevaluate?.id ||
                    props.type === "hasSelectedAsset"
                      ? 12
                      : 8
                  }
                  sm={
                    props?.dataAssetReevaluate?.id ||
                    props.type === "hasSelectedAsset"
                      ? 12
                      : 10
                  }
                  xs={12}
                >
                  <TextValidator
                    className="w-100"
                    type="text"
                    value={props?.dataAssetReevaluate?.tsTen || ""}
                    name="tsTen"
                    size="small"
                    InputProps={{
                      readOnly: true,
                    }}
                    label={
                      <span className="font">
                        <span style={{ color: "red" }}> * </span>
                        {t("AssetReevaluateDialog.assetName")}
                      </span>
                    }
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    onChange={() => {}}
                  />
                </Grid>

                {!props.dataAssetReevaluate?.id &&
                  props.type !== "hasSelectedAsset" &&
                  !props?.isView && (
                    <Grid
                      item
                      lg={3}
                      md={4}
                      sm={2}
                      xs={12}
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "end",
                        minWidth: "fit-content",
                      }}
                    >
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenSelectAssetPopup}
                        style={{ height: "37px" }}
                      >
                        {t("AssetReevaluateDialog.select")}
                      </Button>
                    </Grid>
                  )}
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={6} className="mt-10">
                <TextValidator
                  className="w-100"
                  type="text"
                  value={props?.dataAssetReevaluate?.tsModel || ""}
                  name="tsModel"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">
                      {t("AssetReevaluateDialog.model")}
                    </span>
                  }
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={6} className="mt-10">
                <TextValidator
                  className="w-100"
                  type="text"
                  value={props?.dataAssetReevaluate?.tsSerialNo || ""}
                  name="tsSerialNo"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">
                      {t("AssetReevaluateDialog.serialNo")}
                    </span>
                  }
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12} className="mt-10">
                <FormControl
                  fullWidth
                  variant="standard"
                  sx={{ m: 1, minWidth: 120 }}
                >
                  <SelectValidator
                    className="w-100"
                    label={
                      <span className="font">
                        <span style={{ color: "red" }}> * </span>
                        {t("AssetReevaluateDialog.dglTrangthai")}
                      </span>
                    }
                    InputProps={{
                      readOnly: props?.isView,
                    }}
                    size="small"
                    value={props?.dataAssetReevaluate?.dglTrangthai || ""}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    name="dglTrangthai"
                    onChange={props.handleChangeInput}
                  >
                    {appConst.statusAssetReevaluate.map((item) => {
                      return (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </SelectValidator>
                </FormControl>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12} className="mt-10">
                <FormControl
                  fullWidth
                  variant="standard"
                  sx={{ m: 1, minWidth: 120 }}
                >
                  {props?.isView ? (
                    <TextValidator
                      className="w-100"
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("AssetReevaluateDialog.dglLydoId")}
                        </span>
                      }
                      InputProps={{
                        readOnly: true,
                      }}
                      value={
                        props?.item.itemLyDo ? props?.item.itemLyDo.name : ""
                      }
                    />
                  ) : (
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          <span className="colorRed"> * </span>
                          {t("AssetReevaluateDialog.dglLydoId")}
                        </span>
                      }
                      listData={props?.listReason}
                      setListData={(value) =>
                        props?.handleSetListDataReason(value)
                      }
                      searchFunction={getListReasons}
                      searchObject={{}}
                      defaultValue={props?.item.itemLyDo}
                      displayLable={"name"}
                      value={props?.item.itemLyDo}
                      onSelect={props.handleChangeInputLyDoDgl}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                      noOptionsText={t("general.noOption")}
                    />
                  )}
                </FormControl>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12} className="mt-10">
                <TextValidator
                  className="w-100"
                  multiline
                  rowsmin={1}
                  rowsmax={1000}
                  label={
                    <span className="font">
                      {t("AssetReevaluateDialog.note")}
                    </span>
                  }
                  type="text"
                  value={props?.dataAssetReevaluate?.ghiChu || ""}
                  name="ghiChu"
                  size="small"
                  InputProps={{
                    readOnly: props?.isView,
                  }}
                  onChange={props?.handleChangeInput}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid
            className="content-right"
            item
            container
            lg={8}
            md={8}
            sm={12}
            xs={12}
          >
            <Grid item lg={12} md={12} sm={12} xs={12} style={{marginTop: 10}}>
              <div className="flex flex-end">
                {!props?.isView && <Button
                  variant="contained"
                  color="primary"
                  disabled={
                    props?.item?.dglTrangthai === appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder
                    || !props?.item?.dataAssetReevaluate?.tsId
                  }
                  onClick={props?.handleResetAssetSource}
                  className="mr-12 mb-10"
                >
                  {t("AssetCapitalTable.deleteAssetSource")}
                </Button>}
                <Button
                  variant="contained"
                  color="primary"
                  disabled={
                    props?.item.dataAssetReevaluate?.nguonVonDtos?.length >=
                      props?.item.listAssetSourceAPILength || props?.isView
                  }
                  onClick={props?.handleAddNewAssetSource}
                  className="mb-10"
                >
                  {t("AssetCapitalTable.addAssetSource")}
                </Button>
              </div>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <AssetCapitalTable
                dataAssetReevaluate={props?.item.dataAssetReevaluate}
                handleChangeAssetSource={props.handleChangeAssetSource}
                handleSelectAssetSource={props.handleSelectAssetSource}
                t={t}
                listRestAssetSource={props?.item.listRestAssetSource}
                isView={props?.isView}
                handleDelete={props.handleDeleteAssetSource}
              />
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} className="mt-30">
              <AssetAtrophyTable
                dataAssetReevaluate={props?.item.dataAssetReevaluate}
                handleChangeAtrophyInput={props?.handleChangeAtrophyInput}
                t={t}
                isView={props?.isView}
                convertNumber={props.convertNumberPrice}
                handleSetDataAsset={props.handleSetDataAsset}
              />
            </Grid>
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction="category"
            searchObject={searchObject}
            label={t("AssetReevaluateDialog.evaluationBoard")}
            listData={listReevaluateBoard}
            displayLable={"name"}
            onSelect={handleChangeSelectBoardReevaluate}
            value={reevaluateBoard}
            InputProps={{
              readOnly: true,
            }}
            disabled={
              props?.item?.isView ||
              props?.item?.dataAssetReevaluate?.dglTrangthai ===
                appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder
            }
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              let filtered = filterAutocomplete(options, params);
              return filtered;
            }}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={listReevaluatePerson}
            columns={columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },

              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div>
  );
}
