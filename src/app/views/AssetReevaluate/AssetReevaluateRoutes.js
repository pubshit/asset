import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetReevaluateTable = EgretLoadable({
  loader: () => import("./AssetReevaluateTable")
});
const ViewComponent = withTranslation()(AssetReevaluateTable);

const AssetReevaluateRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "asset/asset_re_evaluate",
    exact: true,
    component: ViewComponent
  }
];

export default AssetReevaluateRoutes;