import {
  AppBar,
  Box,
  Icon,
  IconButton,
  Tab,
  Tabs,
  Tooltip,
  Typography,
} from "@material-ui/core";
import React from "react";
import { useTranslation } from "react-i18next";
import ConstantList from "../../appConfig";
import { getCurrentUser } from "../page-layouts/UserProfileService";
import {
  deleteItem,
  deleteReevaluate,
  getCountStatus,
  getItemById,
  searchByPage,
} from "./AssetReevaluateService";
import { withStyles } from "@material-ui/core/styles";
import AppContext from "app/appContext";
import { Breadcrumb } from "egret";
import moment from "moment";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst } from "../../appConst";
import { convertNumberPrice, sortDataByDate } from "app/appFunction";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ComponentReevaluateTable from "./ComponentReevaluateTable";
import {LightTooltip, TabPanel} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  return (
    <div className="none_wrap">
      {item?.dglTrangthai !==
        appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder && (
        <>
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </>
      )}
      {item?.dglTrangthai ===
        appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder && (
        <LightTooltip
          title={t("general.viewIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <VisibilityIcon size="small" className="iconEye" />
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}
class AssetReevaluateTable extends React.Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    keyword: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
    isRoleUser: false,
    isRoleManager: false,
    item: [],
    isView: false,
    tabValue: 0,
  };
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };
  componentDidMount() {
    this.setState(
      {
        dglTrangthai: null,
      },
      () => this.updatePageData()
    );
    this.updatePageData();
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = async () => {
    let { setPageLoading } = this.context;
    this.setState({ page: 0 }, async () => {
      let searchObject = {};
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.dglTrangthai = this.state?.dglTrangthai;

      setPageLoading(true);
      try {
        let res = await searchByPage(searchObject);
        if (res?.data?.code === 200) {
          this.setState({
            itemList: [
              ...sortDataByDate(res?.data?.data?.content, "dglNgayLap"),
            ],
            totalElements: res?.data?.data?.totalElements,
          });
        } else {
        }
      } catch (err) {
      } finally {
        setPageLoading(false);
      }
    });
  };

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.dglTrangthai = this.state?.dglTrangthai;
    setPageLoading(true);
    try {
      let res = await searchByPage(searchObject);
      if (res?.data?.code === 200) {
        this.setState({
          itemList: [...sortDataByDate(res?.data?.data?.content, "dglNgayLap")],
          totalElements: res?.data?.data?.totalElements,
        });
      } else {
      }
    } catch (err) {
    } finally {
      setPageLoading(false);
    }
    this.getCountStatus();
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      isView: false,
    });
    this.getCountStatus();
    this.updatePageData();
  };

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        shouldOpenConfirmationDeleteAllDialog: false,
        shouldOpenNotificationPopup: false,
        data: [],
        isView: false,
      },
      () => {
        this.updatePageData();
      }
    );
  };

  getRoleCurrentUser = () => {
    let { isRoleUser, isRoleManager } = this.state;
    getCurrentUser()
      .then(({ data }) => {
        if (data) {
          data.roles.forEach((role) => {
            //user
            if (role.name === ConstantList.ROLES.ROLE_USER) {
              isRoleUser = true;
            }

            // người quản lý vật tư
            if (role.name === ConstantList.ROLES.ROLE_ASSET_MANAGER) {
              isRoleManager = true;
            }
            // người đại diện phòng ban
            if (role.name === ConstantList.ROLES.ROLE_ASSET_USER) {
              isRoleUser = true;
            }
            // org
            if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
              isRoleManager = true;
            }
          });
          this.setState({ isRoleManager, isRoleUser });
        }
      })
      .catch((err) => {});
  };
  handleClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    if (this.state.itemList.length === 1 && this.state.page === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count,
      });
    }
    setPageLoading(true);
    try {
      let res = await deleteReevaluate(this.state?.id);
      if (res?.data?.data && res?.status === 200) {
        this.updatePageData();
        toast.success(t("Xóa thành công"));
      } else {
        toast.warning(res?.data?.message);
      }
    } catch (error) {
      toast.warning(t("Không thể xóa danh mục này"));
    }
    this.handleDialogClose();
    setPageLoading(false);
  };
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };
  handleDeleteButtonClick = () => {
    let { t } = this.props;
    if (!this.data || this.data.length === 0) {
      toast.warning(t("general.noti_check_data"));
    } else if (this.data.length === this.state.itemList.length) {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true });
    }
  };
  async handleDeleteList(list) {
    let listAlert = [];
    var { t } = this.props;
    for (var i = 0; i < list.length; i++) {
      try {
        await deleteItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      } finally {
      }
    }
    this.handleDialogClose();
    if (listAlert.length === list.length) {
      toast.warning(t("Danh mục đã được sử dụng"));
      // alert("Các trạng thái đều đã sử dụng");
    } else if (listAlert.length > 0) {
      toast.warning(t("Đã xoá các danh mục chưa sử dụng"));
      // alert("Đã xoá các trạng thái chưa sử dụng");
    }
  }
  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data)
      .then(() => {
        this.updatePageData();
        toast.success("Xóa thành công");
        this.data = null;
      })
      .catch((err) => {
        toast.error("Xảy ra lỗi");
      });
  };

  checkStatus = (status) => {
    let itemStatus = appConst.statusAssetReevaluate.find(
      (item) => item.id === status
    );
    return itemStatus?.name;
  };

  handleView = async (id) => {
    try {
      let res = await getItemById(id);
      if (res?.data?.data && res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          item: res.data.data,
          shouldOpenEditorDialog: true,
          isView: true,
        });
      }
    } catch (error) {
      toast.error("Không lấy được thông tin phiếu");
    }
  };
  handleEdit = async (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      let res = await getItemById(id);
      if (res?.data?.data && res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          item: res.data.data,
          shouldOpenEditorDialog: true,
        });
      }
    } catch (error) {
      toast.error("Không lấy được thông tin phiếu");
    } finally {
      setPageLoading(false);
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    getCountStatus()
      .then(({ data }) => {
        let countStatusWaitReevaluate,
          countStatusIsReevaluating,
          countStatusReevaluated;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (
            appConst?.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder ===
            item?.trangThai
          ) {
            countStatusWaitReevaluate = this.checkCount(item?.soLuong);
            return;
          }
          if (
            appConst?.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder ===
            item?.trangThai
          ) {
            countStatusIsReevaluating = this.checkCount(item?.soLuong);
            return;
          }
          if (
            appConst?.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder ===
            item?.trangThai
          ) {
            countStatusReevaluated = this.checkCount(item?.soLuong);
            return;
          }
        });
        this.setState({
          countStatusWaitReevaluate,
          countStatusIsReevaluating,
          countStatusReevaluated,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabReevaluate?.tabAll === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          dglTrangthai: null,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabReevaluate?.tabNew === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          dglTrangthai:
            appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabReevaluate?.tabReevaluating === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          dglTrangthai:
            appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabReevaluate?.tabReevaluated === newValue) {
      this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          dglTrangthai:
            appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder,
        },
        () => this.updatePageData()
      );
    }
  };

  render() {
    const { t, i18n } = this.props;
    let { isRoleManager, tabValue } = this.state;
    let TitlePage = t("AssetReevaluateTable.titlePage");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        width: "120px",
        minWidth: "120px",
        sorting: false,
        headerStyle: {
          padding: "0px",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleEdit(rowData.id);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else if (method === appConst.active.view) {
                this.handleView(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("AssetReevaluateTable.documentNumber"),
        field: "dglMa",
        width: "150px",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },

      {
        title: t("AssetReevaluateTable.dayVouchers"),
        field: "dglThoiGian",
        align: "left",
        width: "150px",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <div>
            {rowData.dglThoiGian
              ? moment(rowData.dglThoiGian).format("DD/MM/YYYY")
              : ""}
          </div>
        ),
      },
      {
        title: t("AssetReevaluateTable.status"),
        field: "dglTrangthaiMota",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.dglTrangthai;
          let className = "";
          if (
            statusIndex ===
            appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder
          ) {
            className = "status status-error";
          } else if (
            statusIndex ===
            appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder
          ) {
            className = "status status-warning";
          } else if (
            statusIndex ===
            appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder
          ) {
            className = "status status-success";
          } else {
            className = "";
          }
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("AssetReevaluateTable.assetCode"),
        field: "tsMa",
        align: "left",
        width: "150px",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateTable.accountingCode"),
        field: "tsMaketoan",
        align: "left",
        width: "150px",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateTable.managementCode"),
        field: "tsMaquanly",
        align: "left",
        width: "150px",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateTable.assetName"),
        field: "tsTen",
        align: "left",
        width: "150px",
        minWidth: "200px",
        sorting: false,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("AssetReevaluateDialog.model"),
        field: "tsModel",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateDialog.serialNo"),
        field: "tsSerialNo",
        align: "left",
        width: "150px",
        minWidth: "100px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateTable.remainingYears"),
        field: "sonamconlaiMoi",
        align: "left",
        width: "150px",
        minWidth: "80px",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetReevaluateTable.originalPrice"),
        field: "tongnguonvonMoi",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => {
          return convertNumberPrice(rowData?.tongnguonvonMoi || 0);
        },
      },
      {
        title: t("AssetReevaluateTable.residualValue"),
        field: "giatriconlaiMoi",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => {
          return convertNumberPrice(rowData?.giatriconlaiMoi || 0);
        },
      },
      {
        title: t("AssetReevaluateTable.atrophy"),
        field: "haomonluykeMoi",
        align: "left",
        width: "150px",
        minWidth: "150px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => {
          return convertNumberPrice(rowData?.haomonluykeMoi || 0);
        },
      },
      {
        title: t("AssetReevaluateTable.reason"),
        field: "dglLydoText",
        align: "left",
        minWidth: "200px",
        sorting: false,
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('AssetGroup.title') }]} /> */}
          <Breadcrumb
            routeSegments={[
              { name: "Quản lý tài sản", path: "/list/asset_group" },
              { name: TitlePage },
            ]}
          />
        </div>
        <div>
          <AppBar position="static" color="default">
            <Tabs
              className="tabsStatus"
              value={tabValue}
              onChange={this.handleChangeTabValue}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              <Tab
                className="tab"
                label={t("AssetReevaluateTable.tabAll")}
                value={appConst?.tabReevaluate?.tabAll}
              />
              <Tab
                className="tab"
                label={
                  <div className="tabLable">
                    value={appConst?.tabReevaluate?.tabNew}
                    <span>{t("AssetReevaluateTable.tabWaitReevaluate")}</span>
                    <div className="tabQuantity tabQuantity-success">
                      {this.state?.countStatusWaitReevaluate || 0}
                    </div>
                  </div>
                }
              />
              <Tab
                className="tab"
                value={appConst?.tabReevaluate?.tabReevaluating}
                label={
                  <div className="tabLable">
                    <span>{t("AssetReevaluateTable.tabIsReevaluating")}</span>
                    <div className="tabQuantity tabQuantity-warning">
                      {this.state?.countStatusIsReevaluating || 0}
                    </div>
                  </div>
                }
              />
              <Tab
                className="tab"
                value={appConst?.tabReevaluate?.tabReevaluated}
                label={
                  <div className="tabLable">
                    <span>{t("AssetReevaluateTable.tabReevaluated")}</span>
                    <div className="tabQuantity tabQuantity-error">
                      {this.state?.countStatusReevaluated || 0}
                    </div>
                  </div>
                }
              />
            </Tabs>
          </AppBar>
        </div>
        <TabPanel
          value={tabValue}
          index={tabValue}
          className="mp-0"
        >
          <ComponentReevaluateTable
            t={t}
            isRoleManager={isRoleManager}
            columns={columns}
            shouldOpenEditorDialog={this.state.shouldOpenEditorDialog}
            handleEditItem={this.handleEditItem}
            handleTextChange={this.handleTextChange}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            search={this.search}
            itemList={this.state.itemList}
            totalElements={this.state.totalElements}
            rowsPerPage={this.state.rowsPerPage}
            page={this.state.page}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            updatePageData={this.updatePageData}
            item={this.state.item}
            isView={this.state.isView}
            shouldOpenConfirmationDialog={
              this.state.shouldOpenConfirmationDialog
            }
            handleConfirmationResponse={this.handleConfirmationResponse}
          />
        </TabPanel>
      </div>
    );
  }
}
AssetReevaluateTable.contextType = AppContext;
export default AssetReevaluateTable;
