import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";

import React from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SelectAssetAllPopup from "../../views/Component/Asset/SelectAssetAllPopup";
import {
  createReevaluate,
  getListAssetSourceApi,
  getListAssetSourceByAssetId,
  getListReasons,
  updateReevaluate,
} from "./AssetReevaluateService";
import {
  convertFromToDate,
  formatDateDto,
  removeCommas,
} from "app/appFunction";
import moment from "moment";
import { appConst, variable } from "app/appConst";
import AssetReevaluateTabsButtonForce from "./AssetReevaluateTabsButtonForce";
import { PaperComponent } from "../Component/Utilities/PaperComponent";
import AppContext from "app/appContext";
import { isValidDate } from "../../appFunction";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetReevaluateDialog extends React.Component {
  state = {
    listAsset: [],
    listRestAssetSource: [],
    listAssetSourceAPILength: 0,
    dataAssetReevaluate: {
      createDate: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
      tsId: "",
      dglNgayLap: formatDateDto(new Date()),
      dglThoiGian: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
      dglTrangthai: appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder,

      tongnguonvonMoi: 0,
      tongnguonvonCu: 0,
      dglLydoId: "",
      dglLydoText: "",
      nguonVonDtos: [],

      haomonluykeMoi: null,
      giatriconlaiMoi: null,
      tylehaomonMoi: null,
      sonamconlaiMoi: null,

      haomonluykeCu: null,
      giatriconlaiCu: null,
      tylehaomonCu: null,
      sonamconlaiCu: null,
    },
    listReason: [],
    setShowSelectAssetPopup: false,
    isPickerOpen: false,
    itemLyDo: null,
  };

  getRestListAssetSource = async (listAssetSourceAvailable) => {
    try {
      let searchObject = {
        keyword: "",
        pageSize: 999,
        pageIndex: 1,
      };
      let res = await getListAssetSourceApi(searchObject);
      if (res?.data?.data?.content && res?.status === appConst.CODE.SUCCESS) {
        let restArray = res.data.data.content.filter((parentItem) => {
          return !listAssetSourceAvailable.some(
            (childItem) =>
              childItem?.assetSource?.id === parentItem.id ||
              childItem?.nvId === parentItem.id ||
              childItem?.assetSourceId === parentItem.id
          );
        });

        if (this.state.listAssetSourceAPILength === 0) {
          this.setState({
            listAssetSourceAPILength:
              restArray.length + listAssetSourceAvailable.length,
          });
        }
        this.setState({
          listRestAssetSource: restArray,
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  checkMissingInfo = () => {
    if (
      this.state.dataAssetReevaluate.haomonluykeMoi === "" ||
      this.state.dataAssetReevaluate.giatriconlaiMoi === "" ||
      this.state.dataAssetReevaluate.sonamconlaiMoi === "" ||
      this.state.dataAssetReevaluate.tylehaomonMoi === ""
    ) {
      toast.warning(this.props.t("AssetReevaluateDialog.errMissingInfor"));
      return true;
    }
    let isMissingInfor = this.state.dataAssetReevaluate.nguonVonDtos.find(
      (item) =>
        !("nvGiatrimoi" in item) || item?.nvGiatrimoi === "" || !item?.nvId
    );

    if (isMissingInfor) {
      toast.warning(this.props.t("AssetReevaluateDialog.errMissingInfor"));
    }
    return isMissingInfor;
  };

  handleSetListReevaluateUser = (item) => {
    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        persons: item,
      },
    });
  };
  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id });
  };

  validateSubmit = () => {
    let { t } = this.props;
    let { dataAssetReevaluate, itemLyDo } = this.state;
    let isOkDateReevaluate =
      !isValidDate(dataAssetReevaluate?.dglThoiGian) ||
      dataAssetReevaluate?.dglThoiGian < dataAssetReevaluate?.dglNgayLap;
    if (isOkDateReevaluate) {
      toast.warning(t("AssetReevaluateDialog.invalideDateReevaluate"));
      return true;
    }
    if (!itemLyDo?.id) {
      toast.warning(t("AssetReevaluateDialog.noti.emtyReason"));
      return true;
    }
    if (
      !dataAssetReevaluate?.giatriconlaiMoi ||
      !dataAssetReevaluate?.tylehaomonMoi ||
      !dataAssetReevaluate?.sonamconlaiMoi
    ) {
      toast.warning(t("Chưa nhập đầy đủ thông tin của phiếu"));
      return true;
    }
    return false;
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    if (this.validateSubmit()) return;
    setPageLoading(true);

    try {
      if (this.state.dataAssetReevaluate?.nguonVonDtos.length !== 0) {
        let dataSubmit = {
          ...this.state.dataAssetReevaluate,
          hoiDongId: this.state?.hoiDongId,
          createDate: convertFromToDate(
            this.state.dataAssetReevaluate?.createDate
          )?.fromDate,
          dglNgayLap: convertFromToDate(
            this.state.dataAssetReevaluate?.dglNgayLap
          )?.fromDate,
          dglThoiGian: convertFromToDate(
            this.state.dataAssetReevaluate?.dglThoiGian
          )?.fromDate,
          persons: this.state?.dataAssetReevaluate?.persons
            ? this.state?.dataAssetReevaluate?.persons?.map((item) => {
                return {
                  departmentId: item?.departmentId,
                  departmentName: item?.departmentName,
                  personId: item?.personId,
                  personName: item?.personName,
                  position: item?.position,
                  role: item?.role,
                };
              })
            : [],
        };
        if (dataSubmit?.id) {
          let res = await updateReevaluate(dataSubmit);
          if (
            res?.data?.data &&
            res?.status === appConst.CODE.SUCCESS &&
            res?.data?.code === appConst.CODE.SUCCESS
          ) {
            toast.success("Sửa phiếu thành công");
            this.props.handleClose();
          } else {
            toast.warning(res?.data?.message);
          }
        } else {
          let res = await createReevaluate(dataSubmit);
          if (
            res?.data?.data &&
            res?.data?.code === appConst.CODE.SUCCESS &&
            res?.status === appConst.CODE.SUCCESS
          ) {
            toast.success("Tạo phiếu thành công");
            this.props.handleClose();
          } else {
            toast.warning(res?.data?.message);
          }
        }
        if (
          +dataSubmit.dglTrangthai ===
          appConst.listStatusAssetReevaluate.DA_DANH_GIA.indexOrder
        ) {
          if (this.props?.updateData) {
            this.props.updateData(this.props?.idItem);
          }
        }
        if (this.props?.getDsPhieuDanhGiaOfVoucher) {
          this.props.getDsPhieuDanhGiaOfVoucher(this.props?.assetVoucherIds);
        }
      } else {
        toast.warning(t("AssetReevaluateDialog.noti.missingAssetSources"));
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    } finally {
      setPageLoading(false);
    }
    if (this.props?.setSelectedValue) {
      this.props.setSelectedValue(null);
    }
  };

  getListReasonsReevaluate = async () => {
    try {
      let res = await getListReasons();
      if (res && res?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listReason: res.data,
        });
      }
    } catch (error) {}
  };

  async componentDidMount() {
    let { item } = this.props;
    if (this.props?.item && !this.props.item?.tsTen) {
      this.handleSelectAsset(this.props.item);
    } else if (this.props?.item) {
      this.setState({
        dataAssetReevaluate: {
          ...item,
          nguonVonDtos: item?.nguonVonDtos?.map((nv) => ({
            ...nv,
            name: nv.nvTen,
            nv: {
              name: nv?.nvTen,
              id: nv?.nvId,
            },
          })),
        },
      });
      this.getRestListAssetSource(item?.nguonVonDtos || []);
    }
  }

  componentWillUnmount() {}

  componentWillMount = () => {
    this.getListReasonsReevaluate();
  };

  getTotalNewValueAssetSource = () => {
    return this.state.dataAssetReevaluate?.nguonVonDtos.reduce((acc, asset) => {
      return acc + Number(asset.nvGiatrimoi || 0);
    }, 0);
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.dataAssetReevaluate?.nguonVonDtos !==
      prevState.dataAssetReevaluate?.nguonVonDtos
    ) {
      let isValidInputNewValue =
        this.state.dataAssetReevaluate.nguonVonDtos.find(
          (item) => typeof item?.nvGiatrimoi === "number"
        );
      let isMissingInputOldValue =
        this.state.dataAssetReevaluate.nguonVonDtos.find(
          (item) =>
            typeof item?.nvGiatricu !== "number" && item.type === "number"
        );
      let dataUpdate = { ...this.state.dataAssetReevaluate };

      if (isValidInputNewValue) {
        dataUpdate.tongnguonvonMoi = this.getTotalNewValueAssetSource();
      } else {
        dataUpdate.tongnguonvonMoi = "";
      }

      if (!isMissingInputOldValue) {
        dataUpdate.tongnguonvonCu = this.getTongnguonvonCu(
          this.state.dataAssetReevaluate.nguonVonDtos
        );
        if (
          this.state.dataAssetReevaluate.tongnguonvonCu !==
          dataUpdate.tongnguonvonCu
        ) {
          let getListAssetSourcesNew =
            this.state.dataAssetReevaluate.nguonVonDtos.map((item) => {
              return {
                ...item,
                nvTyleCu: dataUpdate.tongnguonvonCu
                  ? (
                      (item.nvGiatricu / dataUpdate.tongnguonvonCu) *
                      100
                    ).toFixed(2)
                  : "",
              };
            });
          dataUpdate.nguonVonDtos = getListAssetSourcesNew;
        }
      }

      //set lại tỉ lệ giá trị nguồn vốn mới
      dataUpdate.nguonVonDtos.forEach((item) => {
        item.nvTyleMoi = dataUpdate?.tongnguonvonMoi
          ? ((item.nvGiatrimoi / dataUpdate.tongnguonvonMoi) * 100).toFixed(2)
          : "";
      });

      this.setState({
        dataAssetReevaluate: dataUpdate,
      });
    }

    if (
      this.state.listReason !== prevState.listReason &&
      this.props?.item?.dglLydoId
    ) {
      let itemLyDo = this.state.listReason.find(
        (item) => item.id === this.props?.item.dglLydoId
      );
      this.setState({ itemLyDo: itemLyDo });
    }
  }

  getListNguonVonDtos = async (id) => {
    let listNguonVonDtos = [];
    try {
      const data = await getListAssetSourceByAssetId(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        data?.data?.data?.forEach((item) => {
          let itemObject = {
            nv: item?.assetSourceId
              ? {
                  id: item?.assetSourceId,
                  name: item?.assetSourceName,
                }
              : null,
            nvId: item?.assetSourceId || item.assetSource?.id,
            nvTen: item?.assetSourceName || item.assetSource?.name,
            name: item?.assetSourceName || item.assetSource?.name,
            tsNvId: item?.id,
            nvGiatricu: item?.value,
            nvTyleCu: item?.percentage,
            nvGiatrimoi: item?.value,
            nvTyleMoi: item?.percentage,
          };
          listNguonVonDtos.push(itemObject);
        });

        this.setState({
          dataAssetReevaluate: {
            ...this?.state?.dataAssetReevaluate,
            nguonVonDtos: listNguonVonDtos,
          },
        });
      }
    } catch (error) {}
  };

  getTongnguonvonCu = (listAssetSources) => {
    if (listAssetSources) {
      return listAssetSources.reduce((acc, asset) => {
        return acc + Number(asset?.value || asset?.nvGiatricu || 0);
      }, 0);
    }
    return "";
  };

  handleSelectAsset = async (item) => {
    let asset = Array.isArray(item) ? item[0].asset : item;
    if (asset) {
      this.getListNguonVonDtos(asset?.id);
      this.setState({
        dataAssetReevaluate: {
          ...this.state.dataAssetReevaluate,
          tsId: asset?.id,
          tsMa: asset?.code,
          tsModel: asset?.model,
          tsSerialNo: asset?.serialNumber,
          tongnguonvonMoi: "",
          haomonluykeMoi: "",
          giatriconlaiMoi: "",
          tongnguonvonCu: this.getTongnguonvonCu(
            this?.state?.dataAssetReevaluate?.nguonVonDtos || []
          ),
          sonamconlaiMoi: "",
          tylehaomonMoi: "",
          tsTen: asset?.name,
          giatriconlaiCu: asset?.carryingAmount,
          haomonluykeCu: asset?.accumulatedDepreciationAmount || 0,
          tylehaomonCu: asset?.depreciationRate,
          sonamconlaiCu: asset?.soNamKhConLai || 0,
          assetVoucherId: asset?.assetVoucherId,
          voucherId: asset?.voucherId,
          voucherType: asset?.voucherType,
        },
        setShowSelectAssetPopup: false,
      });
      this.getRestListAssetSource(
        this.state.dataAssetReevaluate?.nguonVonDtos || []
      );
    } else {
      toast.warning(this.props.t("AssetReevaluateDialog.errorSelectAsset"));
    }
  };
  handleChangeDate = (date, name) => {
    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        [name]: date ? moment(date).format("YYYY-MM-DDTHH:mm:ss") : null,
      },
    });
  };
  handleChangeAssetSource = (e, index, value) => {
    let listAssetSourcesUpdate = [
      ...this.state.dataAssetReevaluate.nguonVonDtos,
    ];
    let source = e?.target?.name;
    if (value) {
      listAssetSourcesUpdate = this.state.dataAssetReevaluate.nguonVonDtos.map(
        (item, indexItem) => {
          if (indexItem === index) {
            item.nvId = value.id;
            item.nvTen = value.name;
          }
          return item;
        }
      );
      this.getRestListAssetSource(listAssetSourcesUpdate);
    } else if (source) {
      listAssetSourcesUpdate = this.state.dataAssetReevaluate.nguonVonDtos.map(
        (item, indexItem) => {
          if (indexItem === index) {
            let value = e.target.value;
            if (["nvGiatrimoi", "nvGiatricu"].includes(source)) {
              item[source] = Number(value);
            } else {
              item[source] = value;
            }
          }
          return item;
        }
      );
    } else {
      listAssetSourcesUpdate = this.state.dataAssetReevaluate.nguonVonDtos.map(
        (item, indexItem) => {
          if (indexItem === index) {
            item.nvId = "";
            item.nvTen = "";
          }
          return item;
        }
      );
    }

    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        nguonVonDtos: listAssetSourcesUpdate,
      },
    });
  };

  handleChangeAtrophyInput = (e) => {
    let dataAssetReevaluateUpdate = {
      ...this.state.dataAssetReevaluate,
    };
    if (e.target.value === "") {
      if (e.target.name === "giatriconlaiMoi") {
        dataAssetReevaluateUpdate.haomonluykeMoi = "";
      }
      dataAssetReevaluateUpdate[e.target.name] = "";
    } else if (
      e.target.name === "tylehaomonMoi" ||
      e.target.name === "sonamconlaiMoi"
    ) {
      dataAssetReevaluateUpdate[e.target.name] = e.target.value;
    }

    if (e.target.name === "giatriconlaiMoi" && e.target.value !== "") {
      dataAssetReevaluateUpdate.giatriconlaiMoi = e.target.value;
    }
    this.setState({
      dataAssetReevaluate: dataAssetReevaluateUpdate,
    });
  };

  handleSetDataAsset = (source, value) => {
    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        [source]: value,
      },
    });

    if (value === "") {
      if (source === "giatriconlaiMoi") {
        this.setState({
          dataAssetReevaluate: {
            ...this.state.dataAssetReevaluate,
            haomonluykeMoi: "",
          },
        });
      }
    }
  };

  convertNumberPrice = (value, type) => {
    let { DOT, COMMA, SPACE } = appConst.TYPES_MONEY;
    let typeConvert = {
      [DOT.code]: DOT.value,
      [COMMA.code]: COMMA.value,
      [SPACE.code]: SPACE.value,
    };
    let number = Number(value ? value : 0);
    let plainNumber = number
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, "$&" + (typeConvert[type] || ","));
    return value ? plainNumber.substring(0, plainNumber.length - 2) : 0;
  };

  handleChangeInputLyDoDgl = (value) => {
    this.setState({
      itemLyDo: value,
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        dglLydoText: value?.name,
        dglLydoId: value?.id,
      },
    });
  };

  handleChangeInput = (e) => {
    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        [e.target.name]: e.target.value,
      },
      isPickerOpen: false,
    });
  };

  handleCloseSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: false,
    });
  };

  handleOpenSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: true,
    });
  };

  handleAddNewAssetSource = () => {
    this.getRestListAssetSource(this.state.dataAssetReevaluate.nguonVonDtos);
    if (this.state.dataAssetReevaluate?.tsId) {
      this.setState({
        dataAssetReevaluate: {
          ...this.state.dataAssetReevaluate,
          nguonVonDtos: [
            ...this.state.dataAssetReevaluate?.nguonVonDtos,
            {
              nvGiatricu: 0,
              nvId: "",
              nvTen: "",
              nvTyleCu: 0,
              tsNvId: "",
              type: "addnew",
              nv: null,
            },
          ],
        },
      });
    }
  };

  handleDeleteAssetSource = (indexAS) => {
    let nguonVonDtosUpdate = this.state.dataAssetReevaluate.nguonVonDtos.filter(
      (item, index) => index !== indexAS
    );
    this.setState(
      {
        dataAssetReevaluate: {
          ...this.state.dataAssetReevaluate,
          nguonVonDtos: nguonVonDtosUpdate,
        },
      },
      () => {
        this.getRestListAssetSource(
          this.state.dataAssetReevaluate.nguonVonDtos
        );
      }
    );
  };

  handleSetListDataReason = (value) => {
    this.setState({ listReason: value });
  };

  handleSelectAssetSource = (value, name, index) => {
    let listAssetSourcesUpdate = [
      ...this.state.dataAssetReevaluate.nguonVonDtos,
    ];
    listAssetSourcesUpdate = listAssetSourcesUpdate?.map((item, indexItem) => {
      if (indexItem === index) {
        item.nvId = value?.id || null;
        item.nvTen = value?.name || null;
        item.name = value?.name || null;
        if (value?.id) {
          item.nv = {
            id: value?.id,
            name: value?.name,
          };
        } else {
          item.nv = null;
        }
      }
      return item;
    });
    this.getRestListAssetSource(listAssetSourcesUpdate);
    this.setState({
      dataAssetReevaluate: {
        ...this.state.dataAssetReevaluate,
        nguonVonDtos: listAssetSourcesUpdate,
      },
    });
  };

  handleResetAssetSource = () => {
    this?.getListNguonVonDtos(this?.state?.dataAssetReevaluate?.tsId);
  };

  render() {
    let { open, handleClose, t } = this.props;
    let { dataAssetReevaluate } = this.state;
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: this.state.handoverDepartment?.id || null,
    };
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        className="assetReevaluateDialog"
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          {t("AssetReevaluateTable.title")}
        </DialogTitle>
        <CustomValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          style={{
            paddingBottom: "5px",
            paddingTop: "5px",
            overflow: "auto",
            marginBottom: "50px",
          }}
        >
          <DialogContent style={{ overflow: "unset" }}>
            <AssetReevaluateTabsButtonForce
              t={t}
              item={this.state}
              dataAssetReevaluate={dataAssetReevaluate}
              handleChangeDate={this.handleChangeDate}
              isView={this.props?.isView}
              type={this.props.type}
              handleOpenSelectAssetPopup={this.handleOpenSelectAssetPopup}
              handleChangeInput={this.handleChangeInput}
              listReason={this.state.listReason}
              handleChangeInputLyDoDgl={this.handleChangeInputLyDoDgl}
              handleAddNewAssetSource={this.handleAddNewAssetSource}
              handleChangeAssetSource={this.handleChangeAssetSource}
              handleSelectAssetSource={this.handleSelectAssetSource}
              handleDeleteAssetSource={this.handleDeleteAssetSource}
              handleChangeAtrophyInput={this.handleChangeAtrophyInput}
              handleSetListDataReason={this.handleSetListDataReason}
              handoverPersonSearchObject={handoverPersonSearchObject}
              handleSetListReevaluateUser={this.handleSetListReevaluateUser}
              handleSetHoiDongId={this.handleSetHoiDongId}
              convertNumberPrice={this.convertNumberPrice}
              handleResetAssetSource={this?.handleResetAssetSource}
              handleSetDataAsset={this.handleSetDataAsset}
            />
          </DialogContent>
          <DialogActions
            style={{
              position: "absolute",
              bottom: "-8px",
              right: 0,
              width: "100%",
              backgroundColor: "#fff",
              zIndex: 100,
            }}
          >
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            {!this.props?.isView && (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            )}
          </DialogActions>
        </CustomValidatorForm>
        {this.state.setShowSelectAssetPopup && (
          <SelectAssetAllPopup
            t={t}
            open={this.state.setShowSelectAssetPopup}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleCloseSelectAssetPopup}
            isOneSelect={true}
            type={variable.listInputName.assetReevaluate}
            dataAsset={this.state.dataAssetReevaluate}
          />
        )}
      </Dialog>
    );
  }
}
AssetReevaluateDialog.contextType = AppContext;
export default AssetReevaluateDialog;
