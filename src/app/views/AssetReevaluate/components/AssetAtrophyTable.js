import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { TextValidator } from "react-material-ui-form-validator";
import { NumberFormatCustom, convertNumberPrice } from "app/appFunction";
import { appConst, variable } from "app/appConst";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetAtrophyTable extends React.Component {
  state = {
    haomonluykeMoi: "",
    giatriconlaiMoi: "",
    sonamconlaiMoi: "",
    tylehaomonMoi: "",
  };

  componentWillMount() {
    this.setState({
      haomonluykeMoi: this.props.dataAssetReevaluate?.haomonluykeMoi || "",
      giatriconlaiMoi: this.props.dataAssetReevaluate?.giatriconlaiMoi || "",
      sonamconlaiMoi: this.props.dataAssetReevaluate?.sonamconlaiMoi || "",
      tylehaomonMoi: this.props.dataAssetReevaluate?.tylehaomonMoi || "",
    });
  }
  handleChangeValue = (source, value) => {
    let { dataAssetReevaluate } = this.props;
    this.setState({ [source]: value }, () => {
      
      if (source === "giatriconlaiMoi") {
        let updatedValueHaoMonLuyKeMoi = value
          ? Number(dataAssetReevaluate?.tongnguonvonMoi) -
            Number(this.state?.giatriconlaiMoi)
          : "";

        let event = {
          target: {
            name: "haomonluykeMoi",
            value: updatedValueHaoMonLuyKeMoi,
          },
        };

        if (value === "") {
          this.setState({ haomonluykeMoi: "" });
        } else {
          this.setState({
            haomonluykeMoi: updatedValueHaoMonLuyKeMoi,
          });
        }
        this.props.handleChangeAtrophyInput(event);
      }
    });
    // this.props.handleSetDataAsset(source, value);
    let event = {
      target: {
        name: source,
        value,
      },
    };
    this.props.handleChangeAtrophyInput(event);
  };

  checkValueCell = (idRow) => {
    if (idRow === 0) {
      return {
        value: this.state.haomonluykeMoi,
        name: "haomonluykeMoi",
      };
    }
    if (idRow === 1) {
      return {
        value: this.state.giatriconlaiMoi,
        name: "giatriconlaiMoi",
      };
    }
    if (idRow === 2) {
      return {
        value: this.state.sonamconlaiMoi,
        name: "sonamconlaiMoi",
      };
    }
    if (idRow === 3) {
      return {
        value: this.state.tylehaomonMoi,
        name: "tylehaomonMoi",
      };
    }
  };
  render() {
    let { t, dataAssetReevaluate } = this.props;
    let valueCellDefault = [
      {
        cellAtrophy: t("AssetAtrophyTable.accumulatedDepreciation"),
        oldValue: this.props.dataAssetReevaluate?.tsId
          ? convertNumberPrice(this.props.dataAssetReevaluate?.haomonluykeCu) ||
            0
          : "",
        // newValue:
        //   this.props.dataAssetReevaluate?.tsId &&
        //   typeof this.props.dataAssetReevaluate?.haomonluykeMoi === "number"
        //     ? convertNumberPrice(
        //         this.props.dataAssetReevaluate?.haomonluykeMoi
        //       ) || 0
        //     : "",
        difference:
          this.props.dataAssetReevaluate?.haomonluykeMoi !== ""
            ? convertNumberPrice(
                this.props.dataAssetReevaluate?.haomonluykeMoi -
                  this.props.dataAssetReevaluate?.haomonluykeCu
              ) || 0
            : "",
      },
      {
        cellAtrophy: t("AssetAtrophyTable.residualValue"),
        oldValue: this.props.dataAssetReevaluate?.tsId
          ? convertNumberPrice(
              this.props.dataAssetReevaluate?.giatriconlaiCu || 0
            )
          : "",
        // newValue: this.props.dataAssetReevaluate?.tsId ? (
        //   <TextValidator
        //     className="w-100 inputPrice"
        //     variant="standard"
        //     value={
        //       typeof dataAssetReevaluate?.giatriconlaiMoi !== "number"
        //         ? ""
        //         : convertNumberPrice(+dataAssetReevaluate?.giatriconlaiMoi)
        //     }
        //     name="giatriconlaiMoi"
        //     size="small"
        //     placeholder=""
        //     validators={["required", "minNumber:0"]}
        //     errorMessages={[t("general.required"), t("general.errorNumber0")]}
        //     onChange={this.props.handleChangeAtrophyInput}
        //   />
        // ) : (
        //   ""
        // ),
        difference:
          this.props.dataAssetReevaluate?.giatriconlaiMoi !== ""
            ? convertNumberPrice(
                this.props.dataAssetReevaluate?.giatriconlaiMoi -
                  this.props.dataAssetReevaluate?.giatriconlaiCu
              ) || 0
            : "",
      },
      {
        cellAtrophy: t("AssetAtrophyTable.yearsRemainingUse"),
        oldValue: this.props.dataAssetReevaluate?.tsId
          ? this.props.dataAssetReevaluate?.sonamconlaiCu
          : "",
        // newValue: this.props.dataAssetReevaluate?.tsId ? (
        //   <TextValidator
        //     className="w-100 inputPrice"
        //     variant="standard"
        //     type="number"
        //     InputProps={{
        //       readOnly: this.props?.isView,
        //     }}
        //     name="sonamconlaiMoi"
        //     value={this.props.dataAssetReevaluate?.sonamconlaiMoi}
        //     size="small"
        //     placeholder=""
        //     InputLabelProps={{
        //       shrink: true,
        //     }}
        //     validators={[
        //       "required",
        //       "minFloat:0",
        //       `matchRegexp:${variable.regex.onlyTwoCharacterFloat}`,
        //     ]}
        //     errorMessages={[
        //       t("general.required"),
        //       t("general.errorNumber0"),
        //       t("general.quantityError"),
        //     ]}
        //     onChange={this.props.handleChangeAtrophyInput}
        //   />
        // ) : (
        //   ""
        // ),
        difference:
          this.props.dataAssetReevaluate?.sonamconlaiMoi !== ""
            ? this.props.dataAssetReevaluate?.sonamconlaiMoi -
              this.props.dataAssetReevaluate?.sonamconlaiCu
            : "",
      },
      {
        cellAtrophy: t("AssetAtrophyTable.depreciationRates"),
        oldValue: this.props.dataAssetReevaluate?.tsId
          ? this.props.dataAssetReevaluate?.tylehaomonCu
          : "",
        // newValue: this.props.dataAssetReevaluate?.tsId ? (
        //   <TextValidator
        //     className="w-100 inputPrice"
        //     variant="standard"
        //     type="number"
        //     name="tylehaomonMoi"
        //     InputProps={{
        //       readOnly: this.props?.isView,
        //     }}
        //     value={this.props.dataAssetReevaluate?.tylehaomonMoi}
        //     size="small"
        //     placeholder=""
        //     InputLabelProps={{
        //       shrink: true,
        //     }}
        //     onChange={this.props.handleChangeAtrophyInput}
        //     validators={[
        //       "required",
        //       "minFloat:0",
        //       `matchRegexp:${variable.regex.onlyTwoCharacterFloat}`,
        //     ]}
        //     errorMessages={[
        //       t("general.required"),
        //       t("general.errorNumber0"),
        //       t("general.quantityError"),
        //     ]}
        //   />
        // ) : (
        //   ""
        // ),
        difference:
          this.props.dataAssetReevaluate?.tylehaomonMoi !== ""
            ? this.props.dataAssetReevaluate?.tylehaomonMoi -
              this.props.dataAssetReevaluate?.tylehaomonCu
            : "",
      },
    ];
    let columns = [
      {
        title: t("AssetAtrophyTable.atrophy"),
        field: "cellAtrophy",
        align: "left",
        minWidth: "200px",
        sorting: false,
      },
      {
        title: t("AssetAtrophyTable.oldValue"),
        field: "oldValue",
        align: "left",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
      },
      {
        title: t("AssetAtrophyTable.newValue"),
        field: "newValue",
        align: "left",
        minWidth: "130px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => {
          let cell = this.checkValueCell(rowData.tableData.id);
          let readOnlyCell = [0].includes(rowData.tableData.id);
          return (
            <TextValidator
              className="w-100"
              variant="standard"
              type="text"
              name="nvGiatricu"
              value={cell?.value}
              InputProps={{
                readOnly: readOnlyCell,
                inputComponent: NumberFormatCustom,
                inputProps: {
                  className: "text-align-right",
                },
              }}
              size="small"
              onChange={(event) =>
                this.handleChangeValue(cell.name, event.target.value)
              }
            />
          );
        },
      },
      {
        title: t("AssetAtrophyTable.difference"),
        field: "difference",
        align: "left",
        minWidth: "120px",
        sorting: false,
        cellStyle: {
          textAlign: "right",
        },
      },
    ];

    return (
      <div className="assetAtrophyTable">
        <MaterialTable
          data={valueCellDefault}
          columns={columns}
          localization={{
            body: {
              emptyDataSourceMessage: `Không có bản ghi nào`,
            },
          }}
          options={{
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            maxBodyHeight: "1000px",
            headerStyle: {
              color: "#eee",
              fontSize: "14px",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
      </div>
    );
  }
}
export default AssetAtrophyTable;
