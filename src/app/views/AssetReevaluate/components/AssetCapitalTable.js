import {
  Button,
  FormControl,
  Icon,
  IconButton,
  MenuItem,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { appConst } from "app/appConst";
import { LightTooltip, convertNumberPrice } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { useTranslation } from "react-i18next";
import { NumberFormatCustom } from "../../Component/Utilities";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class AssetCapitalTable extends React.Component {
  state = {
    valueCell: [],
  };
  filterAutocomplete = createFilterOptions();
  getValueNewRatio = (index) => {
    let value = "";
    if (
      this.props.dataAssetReevaluate?.tongnguonvonMoi &&
      typeof this.props.dataAssetReevaluate?.nguonVonDtos[index]
        ?.nvGiatrimoi === "number"
    ) {
      value = (
        (this.props.dataAssetReevaluate?.nguonVonDtos[index]?.nvGiatrimoi /
          this.props.dataAssetReevaluate?.tongnguonvonMoi) *
        100
      ).toFixed(2);
      if (value === "100.00") {
        value = 100;
      } else if (value === "0.00") {
        value = 0;
      }
    }
    return value;
  };

  getAssetSources = () => {
    let { t } = this.props;
    let assetSourcesArr = this.props.dataAssetReevaluate.nguonVonDtos.map(
      (item, index) => {
        return {
          difference:
            this.props.dataAssetReevaluate?.tsId &&
            this.props.dataAssetReevaluate.tongnguonvonMoi !== "" &&
            typeof item.nvGiatrimoi === "number" &&
            typeof item.nvGiatricu === "number"
              ? convertNumberPrice(item.nvGiatrimoi - item.nvGiatricu) || 0
              : "",
        };
      }
    );
    return assetSourcesArr;
  };

  render() {
    let { t } = this.props;
    let isFullOfOldPercentage =
      this.props.dataAssetReevaluate.nguonVonDtos?.reduce(
        (total, item) => total + Number(item.nvTyleCu),
        0
      ) >= 100;
    let valueCell = [
      ...this.props.dataAssetReevaluate.nguonVonDtos,
      {
        isTotal: true,
        total: t("AssetCapitalTable.tongnguonvonMoi"),
        nvGiatricu: this.props.dataAssetReevaluate?.tongnguonvonCu,
        nvTyleCu:
          this.props.dataAssetReevaluate?.tsId &&
          this.props.dataAssetReevaluate?.nguonVonDtos?.length > 0 &&
          this.props.dataAssetReevaluate?.tongnguonvonCu !== 0
            ? 100
            : "",
        nvGiatrimoi: this.props.dataAssetReevaluate?.tongnguonvonMoi,
        nvTyleMoi:
          this.props.dataAssetReevaluate?.tsId &&
          this.props.dataAssetReevaluate.tongnguonvonMoi
            ? 100
            : "",
        difference:
          this.props.dataAssetReevaluate?.tsId &&
          this.props.dataAssetReevaluate.tongnguonvonMoi !== ""
            ? convertNumberPrice(
                this.props.dataAssetReevaluate?.tongnguonvonMoi -
                  this.props.dataAssetReevaluate?.tongnguonvonCu
              ) || 0
            : "",
      },
    ];

    let columns = [
      {
        title: t("general.action"),
        field: "action",
        align: "left",
        minWidth: 80,
        maxWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          !rowData.isTotal && (
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
              PopperProps={{
                popperOptions: {
                  modifiers: {
                    offset: { enabled: true, offset: "10px, 0px" },
                  },
                },
              }}
            >
              <IconButton
                size="small"
                onClick={() => this.props.handleDelete(rowData.tableData.id)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          ),
      },
      {
        title: t("AssetCapitalTable.capital"),
        field: "total",
        align: "left",
        minWidth: "180px",
        render: (rowData) =>
          rowData.isTotal ? (
            rowData.total
          ) : (
            <AsynchronousAutocompleteSub
              searchFunction={() => {}}
              listData={this.props.listRestAssetSource}
              displayLable="name"
              onSelect={(value, name) =>
                this.props.handleSelectAssetSource(
                  value,
                  name,
                  rowData.tableData.id
                )
              }
              value={rowData?.nv || null}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              noOptionsText={t("general.noOption")}
            />
          ),
      },
      {
        title: t("AssetCapitalTable.oldValue"),
        field: "oldValue",
        align: "left",
        minWidth: "120px",
        render: (rowData) => (
          <TextValidator
            className="w-100"
            variant="standard"
            type="text"
            name="nvGiatricu"
            InputProps={{
              readOnly:
                this.props?.isView || rowData.isTotal || !rowData?.tsNvId,
              inputComponent: NumberFormatCustom,
              inputProps: {
                className: "text-align-right",
              },
              disableUnderline: rowData.isTotal,
            }}
            value={rowData.nvGiatricu || 0}
            size="small"
            onChange={(e) =>
              this.props.handleChangeAssetSource(e, rowData.tableData.id)
            }
            validators={
              isFullOfOldPercentage && this.state.dataAssetReevaluate?.id
                ? ["required"]
                : []
            }
            errorMessages={[t("general.required")]}
          />
        ),
      },
      {
        title: t("AssetCapitalTable.oldRatio"),
        field: "nvTyleCu",
        minWidth: "120px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetCapitalTable.newValue"),
        field: "newValue",
        minWidth: "120px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className="w-100"
            type="text"
            name="nvGiatrimoi"
            InputProps={{
              readOnly: this.props?.isView || rowData.isTotal,
              inputComponent: NumberFormatCustom,
              inputProps: {
                className: "text-align-right",
              },
              disableUnderline: rowData.isTotal,
            }}
            value={rowData?.nvGiatrimoi ? rowData?.nvGiatrimoi : ""}
            size="small"
            placeholder=""
            onChange={(e) =>
              this.props.handleChangeAssetSource(e, rowData.tableData.id)
            }
            validators={["required"]}
            errorMessages={[t("general.required")]}
          />
        ),
      },
      {
        title: t("AssetCapitalTable.newRatio"),
        field: "nvTyleMoi",
        minWidth: "130px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetCapitalTable.difference"),
        field: "difference",
        align: "left",
        minWidth: "120px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) =>
          convertNumberPrice(rowData.nvGiatrimoi - rowData.nvGiatricu) || 0,
      },
    ];

    return (
      <div>
        <MaterialTable
          data={valueCell}
          columns={columns}
          localization={{
            body: {
              emptyDataSourceMessage: `Không có bản ghi nào`,
            },
          }}
          options={{
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            sorting: false,
            draggable: false,
            maxBodyHeight: "1000px",
            headerStyle: {
              fontSize: "14px",
              color: "#eee",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
      </div>
    );
  }
}
export default AssetCapitalTable;
