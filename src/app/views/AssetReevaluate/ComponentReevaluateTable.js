import {
  Button,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  Link,
  TablePagination,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import React, { useEffect } from "react";
import AssetReevaluateDialog from "./AssetReevaluateDialog";
import { ConfirmationDialog } from "egret";

function ComponentReevaluateTable(props) {
  let {
    t,
    isRoleManager,
    columns,
    shouldOpenEditorDialog,
    handleEditItem,
    handleTextChange,
    handleKeyDownEnterSearch,
    handleKeyUp,
    search,
    itemList,
    totalElements,
    rowsPerPage,
    page,
    handleChangePage,
    setRowsPerPage,
    handleDialogClose,
    handleOKEditClose,
    updatePageData,
    item,
    isView,
    shouldOpenConfirmationDialog,
    handleConfirmationResponse,
  } = props;
  return (
    <Grid container spacing={2} justifyContent="space-between">
      <Grid item md={3} xs={12}>
        {isRoleManager && (
          <>
            <Button
              className="align-bottom mr-16 mb-16"
              variant="contained"
              color="primary"
              onClick={() => handleEditItem(null)}
            >
              {t("general.add")}
            </Button>
          </>
        )}
      </Grid>
      <Grid item md={4} sm={12} xs={12}>
        <FormControl fullWidth style={{ marginTop: "6px" }}>
          <Input
            className="search_box w-100"
            onChange={handleTextChange}
            onKeyDown={handleKeyDownEnterSearch}
            onKeyUp={handleKeyUp}
            placeholder="Tìm kiếm theo tên tài sản hoặc mã tài sản"
            id="search_box"
            startAdornment={
              <InputAdornment>
                <Link>
                  {" "}
                  <SearchIcon
                    onClick={() => search()}
                    style={{ position: "absolute", top: "0", right: "0" }}
                  />
                </Link>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <div></div>
        <MaterialTable
          title={t("general.list")}
          data={itemList}
          columns={columns}
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
            toolbar: {
              nRowsSelected: `${t("general.selects")}`,
            },
          }}
          options={{
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            rowStyle: (rowData) => ({
              backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            maxBodyHeight: "1000px",
            minBodyHeight: "370px",
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
        <TablePagination
          align="left"
          className="px-16"
          rowsPerPageOptions={[1, 2, 3, 5, 10, 25]}
          component="div"
          labelRowsPerPage={t("general.rows_per_page")}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} ${t("general.of")} ${
              count !== -1 ? count : `more than ${to}`
            }`
          }
          count={totalElements}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={setRowsPerPage}
        />
      </Grid>
      {shouldOpenEditorDialog && (
        <AssetReevaluateDialog
          t={t}
          handleClose={() => handleDialogClose()}
          open={shouldOpenEditorDialog}
          handleOKEditClose={handleOKEditClose}
          updatePageData={updatePageData}
          item={item}
          isView={isView}
        />
      )}
      {shouldOpenConfirmationDialog && (
        <ConfirmationDialog
          title={t("general.confirm")}
          open={shouldOpenConfirmationDialog}
          onConfirmDialogClose={handleDialogClose}
          onYesClick={handleConfirmationResponse}
          text={t("DeleteConfirm")}
          agree={t("general.agree")}
          cancel={t("general.cancel")}
        />
      )}
    </Grid>
  );
}

export default ComponentReevaluateTable;
