/* eslint-disable no-unused-expressions */

import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { toast } from 'react-toastify'
function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

export default function Asynchronous(props) {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [valueInitial, setValueInitial] = React.useState();
  const [valueChange, setValueChange] = React.useState();

  //kiểu return trả về của hàm search. Mặc định là return kiểu page. ('list' - trả về 1 list)
  const [typeReturnFunction, setTypeReturnFunction] = React.useState('');
  const [displayLable, setDisplayLable] = React.useState('');   // tên trường để hiển thị ra cho người dùng chọn (mặc định sẽ là code)
  const [defaultValue, setDefaultValue] = React.useState([]); //giá trị mặc định ban đầu
  const [className, setClassName] = React.useState([]); //class của thẻ Autocomplete
  const [variant, setVariant] = React.useState([]);     //kiểu hiển thị của thẻ TextValidator
  const [multiple, setMultiple] = React.useState(false);  //chọn nhiều hay chọn 1
  const [validators, setValidators] = React.useState([]);   // Truyền vào kiểu validators muốn check
  const [errorMessages, setErrorMessages] = React.useState([]); //Text hiển thị khi check validators (theo thứ tự các validators truyền vào)
  const [value, setValue] = React.useState(""); //Text hiển thị khi check validators (theo thứ tự các validators truyền vào)
  const loading = open && options.length == 0;
  //tạm thời không sử dụng selectChildName.
  const [selectChildName, setSelectChildName] = React.useState(''); //Nếu muốn so sánh id object bên trong của value thì truyền tên object bên trong Ex: selectChildName = {'objectName'}
  const getListItemChild = (item) => {
    let result = []
    result.push(item);
    // eslint-disable-next-line no-unused-expressions
    item?.children && item?.children.forEach(child => {
      let childs = getListItemChild(child);
      result.push(...childs)
    });
    return result;
  };
  React.useEffect(() => {
    let active = true;

    if (props?.listData?.length > 0) {
      setOptions([...props?.listData]);
      return;
    }

    if (!loading) {
      return undefined;
    }
    (async () => {
      let dataValue = [];
      //searchFunction: hàm search data
      //searchObject: object chứa các thuộc tính search bên trong
      const response = await props.searchFunction(props.searchObject)
      const data = await ((props.typeReturnFunction && props.typeReturnFunction === 'list') ? response?.data :
        ((props?.typeReturnFunction === 'category') ? response?.data?.data?.content || response?.data?.content :
          ((props?.typeReturnFunction === 'status') ? response?.data?.data?.createStatuses : response?.data?.content)));
      if (data?.length === 0) {
        toast.warning('Không có dữ liệu')
      }
      if (props.children === "children") {
        data && data?.forEach((item) => {
          let items = getListItemChild(item);
            dataValue.push(...items);
        });
      } else {
          data && dataValue.push(...data);
      }

      if (active) {
        if (props?.setListData) props.setListData(dataValue);
        setOptions(dataValue);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading]);

  useEffect(() => {
    setValueInitial(props.value);
    setValueChange("");
  }, [props?.value])

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
      setTypeReturnFunction([]);
      setDisplayLable([]);
      setDefaultValue([]);
      setMultiple([]);
      setVariant([]);
      setSelectChildName([]);
      setClassName([]);
      setValidators([]);
      setErrorMessages([]);
    }
  }, [open]);
  const handleChange = (e) => {
    setValueInitial(null);
    if (props?.setNewValue) {
      props.setNewValue(e.target.value)
    }
    setValueChange(props?.isFocus ? e?.target?.value : null);
  }
  return (
    <Autocomplete
      id="asynchronous-demo"
      className={props.className ? props.className : "w-100"}
      open={open}
      autoComplete
      disabled={props.disabled}
      autoHighlight={true}
      multiple={props.multiple}
      noOptionsText={props?.noOptionsText ? props?.noOptionsText : "no option"}
      filterOptions={props?.filterOptions}
      onChange={(event, value, reason, details) => {
        let list = [];
        if (props.selectChildName) {
          value.forEach(item => {
            if (item.id === details.option.id) {
              list.push({ [props.selectChildName]: item });
            } else {
              list.push(item);
            }
          });
        }
        else {
          list = value;
        }
        props.onSelect(list, props.onSelectOptions);
      }}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      onBlur={() => setValueChange("")}
      getOptionSelected={(option, value) => (props.selectChildName && option[props.selectChildName]) ? option[props.selectChildName].id === value[props.selectChildName].id : option.id === value.id}
      getOptionLabel={(option) =>
        option
          ? ((props.selectChildName && option[props.selectChildName])
            ? ((props.displayLable)
              ? option[props.selectChildName][props.displayLable]
              : option[props.selectChildName].code)
            : ((props.displayLable)
              ? (props?.showCode
                ? option[props.displayLable] + ' - ' + option?.code
                : option[props.displayLable])
              : option.code))
          : ''}
      options={options}
      defaultValue={props.defaultValue ? props.defaultValue : null}
      loading={loading}
      // disableClearable
      renderOption={(option) =>
        option
          ? ((props.selectChildName && option[props.selectChildName])
            ? ((props.displayLable)
              ? option[props.selectChildName][props.displayLable]
              : option[props.selectChildName].code)
            : ((props.displayLable)
              ? (props?.showCode
                ? option[props.displayLable] + ' - ' + option.code
                : option[props.displayLable])
              : option.code))
          : ''}
      renderInput={(params) => (
        <TextValidator
          {...params}
          label={props.label}
          value={valueInitial ? valueInitial : ""}
          validators={props.validators ? props.validators : []}
          errorMessages={props.errorMessages ? props.errorMessages : ""}
          variant={props.variant ? props.variant : "standard"}
          size={props.size ? props.size : "medium"}
          onChange={handleChange}
          inputProps={{
            ...params.inputProps,
            value: valueInitial ? valueInitial[props?.displayLable] : valueChange,
            endadornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={10} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    // renderOption={(option, { inputValue }) => {
    //   const matches = match((option) => (props.selectChildName && option[props.selectChildName]) ? ((props.displayLable) ? option[props.selectChildName][props.displayLable] : option[props.selectChildName].code) : ((props.displayLable) ? option[props.displayLable] : option.code), inputValue);
    //   const parts = parse((option) => (props.selectChildName && option[props.selectChildName]) ? ((props.displayLable) ? option[props.selectChildName][props.displayLable] : option[props.selectChildName].code) : ((props.displayLable) ? option[props.displayLable] : option.code), matches);
    //   return (
    //     <div>
    //       {parts.map((part, index) => (
    //         <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
    //           {part.text}
    //         </span>
    //       ))}
    //     </div>
    //   );
    // }}
    />
  );
}