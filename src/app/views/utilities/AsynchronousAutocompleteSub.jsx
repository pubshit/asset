import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { TextValidator } from "react-material-ui-form-validator";
import { toast } from 'react-toastify';
import PropTypes from "prop-types";
import {filterOptions, getOptionSelected} from "../../appFunction";
import clsx from "clsx";

function Asynchronous(props) {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [valueInitial, setValueInitial] = React.useState(null);
  const [valueChange, setValueChange] = React.useState(null);

  //kiểu return trả về của hàm search. Mặc định là return kiểu page. ('list' - trả về 1 list)
  const loading = open && options.length === 0;

  React.useEffect(() => {
    setValueInitial(props.value);
    setValueChange("");
    let active = true;

    if (props?.listData?.length > 0) {
      setOptions([...props?.listData]);
      return;
    }

    if (!loading) {
      return undefined;
    }
    (async () => {
      //searchFunction: hàm search data
      //searchObject: object chứa các thuộc tính search bên trong
      try {
        const response = await props?.searchFunction(props?.searchObject)
        let data = [];
        switch (props.typeReturnFunction) {
          case "list":
            data = [...response?.data]
            break;
          case "listData":
            data = [...response?.data?.data]
            break;
          case "category":
            data = [...response?.data?.data?.content]
            break;
          case "default":
            data = [...response?.data?.content]
            break;
          default:
            data = [...response?.data?.content]
            break;
        }

        let list = [];
        if (data?.length === 0) {
          toast.warning('Không có dữ liệu')
        }
        try {
          // eslint-disable-next-line no-unused-expressions
          data && data?.forEach(item => {
            if (props.isNoRenderParent) {
              item.parent = null;
            }
            if (props.isNoRenderChildren) {
              item.children = [];
            }
            let items = getListItemChild(item);
            list.push(...items);
          })
        } catch (e) { }
        if (active) {
          if (props?.setListData) {
            props.setListData(list, props.nameListData);
          }
          setOptions([...list]);
        }
      }
      catch (e) {
        console.log(e)
      }
    })();

    return () => {
      active = false;
    };
  }, [loading, props.value]);

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  React.useEffect(() => {
    setOptions([...(props?.listData || [])])
  }, [props.listData]);

  const getListItemChild = (item) => {
    let result = []
    result.push(item);
    // eslint-disable-next-line no-unused-expressions
    item?.children && item?.children.forEach(child => {
      let childs = getListItemChild(child);
      result.push(...childs)
    });
    return result;
  };

  const handleRenderOption = (option) => {
    if (!option) return "";

    if (option[props.displayLable] && option?.[props.showCode]) {                              //Có cả tên và mã => Tên - Mã
      return option[props.displayLable] + ' - ' + (option?.[props.showCode] || option.code);
    } else if (option[props.displayLable] || option.name) {                                    // Chỉ có tên => Tên
      return option[props.displayLable] || option.name;
    } else {                                                                                   //Không có cả tên và mã => ""
      return "";
    }
  }

  return (
    props.readOnly ? (
      <TextValidator
        fullWidth
        className={clsx(
          props.hidden ? "hidden" : "",
          props.className,
        )}
        label={props.label}
        value={(
          valueInitial
            ? (props?.showCode
              ? valueInitial[props.showCode]
                ? valueInitial[props?.displayLable] + " - " + valueInitial[props.showCode]
                : valueInitial?.code
                  ? valueInitial[props?.displayLable] + " - " + valueInitial?.code
                  : valueInitial[props?.displayLable]
              : valueInitial[props?.displayLable])
            : valueChange
        ) || ""
        }
        InputProps={{
          readOnly: true,
        }}
        placeholder={props?.placeholder || null}
        variant={props.variant ? props.variant : "standard"}
        size={props.size ? props.size : "medium"}
      />
    ) : (
      <Autocomplete
        id="asynchronous-demo"
        className={clsx(
            "w-100",
          props.hidden ? "hidden" : "",
          props.className,
        )}
        open={open}
        autoComplete
        closeIcon={props.closeIcon}
        disabled={props.disabled}
        autoHighlight={true}
        multiple={props.multiple}
        onChange={(event, value, reason, details) => {
          let list = [];
          if (props.selectChildName) {
            value.forEach(item => {
              if (item.id === details.option.id) {
                list.push({ [props.selectChildName]: item });
              } else {
                list.push(item);
              }
            });
          }
          else {
            list = value;
          }
          props.onSelect(list, props.name);
        }}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        getOptionSelected={(option, value) => {
          return props.selectedOptionKey
            ? option[props.selectedOptionKey] === value[props.selectedOptionKey]
            : getOptionSelected(option, value)
        }}
        getOptionLabel={handleRenderOption}
        options={options}
        name={props?.name}
        loadingText={props?.noOptionsText || "Đang tải dữ liệu"}
        noOptionsText={props?.noOptionsText ? props?.noOptionsText : "Không có dữ liệu"}
        filterOptions={props?.filterOptions || filterOptions}
        value={props.value ? props.value : null}
        loading={loading}
        // disableClearable
        onBlur={(e) => {
          setValueChange("");
          props.onBlur?.(e);
        }}
        renderOption={handleRenderOption}
        renderInput={(params) => {
          return (
            <TextValidator
              {...params}
              className={props?.className ? props?.className : ""}
              label={props.label}
              value={valueInitial ? valueInitial : ""}
              onChange={(e) => {
                setValueInitial(null); setValueChange(null);
                props.onInputChange?.(e);
              }}
              onFocus={(e) => {
                props?.onInputForcus?.(e);
              }}
              placeholder={props?.placeholder || null}
              validators={props.validators ? props.validators : []}
              errorMessages={props.errorMessages ? props.errorMessages : ""}
              variant={props.variant ? props.variant : "standard"}
              size={props.size ? props.size : "medium"}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? <CircularProgress color="inherit" size={10} /> : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
            />
          )
        }}
      />
    )
  );
}

Asynchronous.propTypes = {
  className: PropTypes.any,
  selectChildName: PropTypes.any,
  displayLable: PropTypes.string.isRequired,
  errorMessages: PropTypes.array,
  noOptionsText: PropTypes.string,
  onSelect: PropTypes.func,
  typeReturnFunction: PropTypes.oneOf(['category', 'list', "default", "listData"]),
  readOnly: PropTypes.bool,
  searchFunction: PropTypes.func.isRequired,
  searchObject: PropTypes.object,
  validators: PropTypes.array,
  isNoRenderParent: PropTypes.bool,
  isNoRenderChildren: PropTypes.bool,
  selectedOptionKey: PropTypes.string,
  listData: PropTypes.array,
  setListData: PropTypes.func,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  disabled: PropTypes.bool,
  name: PropTypes.string,
  onInputChange: PropTypes.func,
  onInputForcus: PropTypes.func,
  onBlur: PropTypes.func,
  nameListData: PropTypes.string,
  hidden: PropTypes.bool,
  showCode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  filterOptions: PropTypes.any,
  multiple: PropTypes.bool,
}
export default Asynchronous;
