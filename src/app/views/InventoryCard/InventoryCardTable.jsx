import React from "react";
import {
  Grid,
  Card,
  CardContent, TableCell, TableRow, TableContainer, Table, TableHead, TableBody,
} from "@material-ui/core";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  searchByPage,
} from "./InventoryCardService";
import { Breadcrumb } from "egret";
import moment from "moment";
import { Helmet } from 'react-helmet';
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { searchByPage as storesSearchByPage } from '../Store/StoreService'
import { ValidatorForm } from 'react-material-ui-form-validator'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  STATUS_STORE,
  appConst,
} from "app/appConst";
import viLocale from "date-fns/locale/vi";
import {
  convertDateToHHMMSSDDMMYYYY,
  convertMoney,
  convertFromToDate,
  formatTimestampToDate,
  handleKeyDown,
  isValidDate
} from "app/appFunction";
import { searchByPage as searchByPageMaterial } from "../Product/ProductService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AppContext from "../../appContext";
import { isSuccessfulResponse, onInputChangeDataSearch } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class InventoryCardTable extends React.Component {
  constructor(props) {
    super(props);
    this.tableHeaderRef = React.createRef();
  }

  state = {
    keyword: '',
    page: 0,
    item: {},
    shouldOpenEditorDialog: false,
    totalElements: 0,
    fromDate: null,
    toDate: null,
    store: null,
    listDataMaterial: [],
    shouldOpenPrintDialog: false,
    dataView: {},
    material: null,
  };

  handleTextChange = event => {
    this.setState({ keyword: event.target.value })
  };

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  setPage = page => {
    this.setState({ page }, () => {
      this.updatePageData();
    })
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = async () => {
    const { t } = this.props;
    let { store } = this.state;
    let { setPageLoading } = this.context;
    if (!this.state?.material?.id) return;
    try {
      setPageLoading(true);
      let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
      if (store != null && store.id) {
        searchObject.storeId = this.state.store.id;
      }
      searchObject.fromDate = convertFromToDate(this.state.fromDate)?.fromDate;
      searchObject.toDate = convertFromToDate(this.state.toDate)?.toDate;
      searchObject.productId = this.state?.material?.id;

      const data = await searchByPage(searchObject);
      if (isSuccessfulResponse(data?.data?.code)) {
        this.setState({
          itemList: data?.data?.data?.content
        })
      }
    } catch (error) {
      console.error(error)
      toast.error(t("toastr.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleSearchSupplier = async (keySearch = "") => {
    try {
      let searchObject = {};
      searchObject.keyword = keySearch.trim();
      searchObject.pageIndex = 1;
      searchObject.pageSize = 10;
      searchObject.productTypeCode = appConst.productTypeCode.VTHH;

      const data = await searchByPageMaterial(searchObject);
      if (isSuccessfulResponse(data?.status)) {
        this.setState({ listDataMaterial: data?.data?.content })
      } else {
        this.setState({ listDataMaterial: [] })
      }

    } catch (error) {
      console.error(error)
    }
  };

  componentDidMount() {
    this.updatePageData();
    this.handleSearchSupplier();
  }

  handleChangeState = (data, source) => {
    this.setState({ [source]: data }, () => {
      this.handleCheckSource(data, source);
    })
  }

  handleCheckSource = (data, source) => {
    if (["material"].includes(source)) {
      this.setState({ itemList: [] })
    }
    if (["material", "store"].includes(source)) {
      this.search();
    }
    if (["fromDate", "toDate"].includes(source)) {
      if (isValidDate(data) || data === null) {
        this.updatePageData();
      }
    }
  }

  checkPrice = (price) => price === 0;
  render() {
    const { t, i18n } = this.props;
    let {
      itemList,
      fromDate,
      toDate,
      listDataMaterial,
      store,
      material
    } = this.state;
    let TitlePage = t("InventoryCard.title");
    const searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_STORE.HOAT_DONG.code,
    };

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.materialManagement") },
            { name: TitlePage, path: "report/inventory_card" }]} />
        </div>
        <Grid container spacing={1} justifyContent="space-between">
          <Card elevation={2} className="w-100 mt-8">
            <CardContent>
              <Grid item container spacing={2} md={12} xs={12} lg={12}>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span style={{ color: 'red' }}>  </span>
                          {t("InventoryReport.fromDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={'fromDate'}
                      value={fromDate}
                      maxDate={toDate ? new Date(toDate) : undefined}
                      onChange={date => this.handleChangeState(date, "fromDate")}
                      minDateMessage={t("general.minDateDefault")}
                      maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span style={{ color: 'red' }}>  </span>
                          {t("InventoryReport.toDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={'toDate'}
                      value={toDate}
                      minDate={fromDate ? new Date(fromDate) : undefined}
                      maxDateMessage={t("general.maxDateMessage")}
                      minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      onChange={date => this.handleChangeState(date, "toDate")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <ValidatorForm onSubmit={() => <i> { }</i>}>
                    <AsynchronousAutocomplete
                      label={t('Asset.store')}
                      searchFunction={storesSearchByPage}
                      searchObject={searchObjectStore}
                      displayLable={'name'}
                      value={store}
                      onSelect={(data) => this.handleChangeState(data, "store")}
                      noOptionsText={t("general.noOption")}
                    />
                  </ValidatorForm>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <ValidatorForm onSubmit={() => <i> { }</i>}>
                    <AsynchronousAutocompleteSub
                      searchFunction={() => { }}
                      searchObject={{}}
                      label={t('InventoryCard.material')}
                      listData={listDataMaterial?.map(x => ({ ...x, name: `${x?.code} - ${x?.name}` }))}
                      setListData={(data) => this.handleChangeState(data, "listDataMaterial")}
                      displayLable={"name"}
                      onSelect={(data) => this.handleChangeState(data, "material")}
                      onInputChange={(event) => {
                        const value = event.target.value;
                        onInputChangeDataSearch(value, this.handleSearchSupplier)
                      }}
                      onInputForcus={(event => {
                        onInputChangeDataSearch("", this.handleSearchSupplier)
                      })}
                      value={material}
                      noOptionsText={t("general.noOption")}
                    />
                  </ValidatorForm>
                </Grid>
              </Grid>
            </ CardContent>
          </Card>
          <Grid item xs={12}>
            <div className="MuiPaper-root MuiPaper-elevation2 MuiPaper-rounded">
              <TableContainer style={{ maxHeight: `calc(100vh - 350px)` }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead innerRef={this.tableHeaderRef} className="position-sticky top-0 left-0" style={{ zIndex: 2 }}>
                    <TableRow>
                      <TableCell align="center" rowSpan="2">
                        STT
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1" style={{ minWidth: 140 }}>
                        {t("InventoryCard.date")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.voucherImport")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.voucherExport")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.batCode")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.expiry")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.desc")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.importQty")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.exportQty")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.remainingQty")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.unitPrice")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("InventoryCard.amount")}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="position-sticky left-0" style={{ zIndex: 2, top: `${this.tableHeaderRef.current?.offsetHeight ? this.tableHeaderRef.current?.offsetHeight : "40"}px`, background: "#fff" }}>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i> { }</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top", minWidth: 100 }}>
                        <i>Ngày tháng nhập/xuất/chuyển</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Mã phiếu nhập đối với phiếu nhập, mã phiếu chuyển đổi với kho tiếp nhận</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Mã phiếu xuất đối với phiếu xuất, mã phiếu chuyển đổi với kho điều chuyển</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Số lô vật tư của lần nhập/xuất/chuyển</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Hạn sử dụng của vật tư lần nhập/xuất/chuyển</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Diễn giải lần nhập/xuất/chuyển như ví dụ dưới đây</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>SL nhập đối với vật tư nhập kho</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Số lượng xuất đối với vật tư xuất kho</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>SL tồn tính sau mỗi lần nhập hoặc xuất</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Đơn giá của vật tư đối với từng lần nhập xuất chuyển kho</i>
                      </TableCell>
                      <TableCell align="left" style={{ verticalAlign: "top" }}>
                        <i>Thành tiền của vật tư đối với từng lần nhập/xuất/chuyển</i>
                      </TableCell>
                    </TableRow>
                    {itemList?.length > 0 && itemList?.map((item, index) => (
                      <TableRow key={item?.productName || index}>
                        <TableCell align="center">
                          {index + 1}
                        </TableCell>
                        <TableCell align="center">
                          {convertDateToHHMMSSDDMMYYYY(item?.inputDate)}
                        </TableCell>
                        <TableCell align="center">
                          {item?.inventoryType === appConst.INVENTORY_CARD_TYPE.NK.code && item?.code}
                        </TableCell>
                        <TableCell align="center">
                          {item?.inventoryType === appConst.INVENTORY_CARD_TYPE.XK.code && item?.code}
                        </TableCell>
                        <TableCell align="center">
                          {item?.lotNumber}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                          {formatTimestampToDate(item?.expiryDate)}
                        </TableCell>
                        <TableCell align="left">
                          {item?.interpretation}
                        </TableCell>
                        <TableCell align="right" className="text-align-right">
                          {item?.inventoryType === appConst.INVENTORY_CARD_TYPE.NK.code && (this.checkPrice(item?.quantity) ? "0" : convertMoney(item?.quantity))}
                        </TableCell>
                        <TableCell align="right" className="text-align-right">
                          {item?.inventoryType === appConst.INVENTORY_CARD_TYPE.XK.code && (this.checkPrice(item?.quantity) ? "0" : convertMoney(item?.quantity))}
                        </TableCell>
                        <TableCell align="right" className="text-align-right">
                          {this.checkPrice(item?.soLuongCuoiKy) ? "0" : convertMoney(item?.soLuongCuoiKy)}
                        </TableCell>
                        <TableCell align="center">
                          {this.checkPrice(item?.unitPrice) ? "0" : convertMoney(item?.unitPrice)}
                        </TableCell>
                        <TableCell align="right" className="text-align-right">
                          {this.checkPrice(item?.amount) ? "0" : convertMoney(item?.amount)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

InventoryCardTable.contextType = AppContext;
export default InventoryCardTable;