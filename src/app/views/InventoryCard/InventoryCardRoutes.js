import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';

const InventoryReportTable = EgretLoadable({
  loader: () => import("./InventoryCardTable")
});
const ViewComponent = withTranslation()(InventoryReportTable);

const InventoryCardRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "report/inventory_card",
    exact: true,
    component: ViewComponent
  }
];

export default InventoryCardRoutes;
