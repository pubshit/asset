import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT_ASSET_MAINTANE +
  "/api/report/supplies-inventory/card";

export const searchByPage = (searchObject) => {
  var url = API_PATH + `/${searchObject?.productId}`;
  let config = {
    params: searchObject,
  };
  return axios.get(url, config);
};
