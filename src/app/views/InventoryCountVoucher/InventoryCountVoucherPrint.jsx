/* eslint-disable no-new-wrappers */
import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import Signature from "../FormCustom/Component/Signature";
import Title from "../FormCustom/Component/Title";
import NationalName from "../FormCustom/Component/NationalName";
import HeaderLeft from "../FormCustom/Component/HeaderLeft";
import { convertNumberPrice, convertNumberPriceRoundUp } from "app/appFunction";
import { appConst } from "app/appConst";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    minWidth: "76px",
    // maxWidth: "80px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60x",
  },
  code: {
    border: "1px solid",
    padding: "2px",
  },
  date: {
    marginRight: "7%",
    textAlign: "right",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px",
  },
  nameText: {
    textAlign: "center",
    border: "1px solid",
    fontWeight: "bold",
    padding: "4px",
    minWidth: "130px",
  },
  w_33: {
    width: "33%",
  },
  d_flex: {
    display: "flex",
    paddingLeft: "20px",
  },
  // alignCenter : {
  //   alignItems: "center",
  // },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
    // padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
  }
};

export default class InventoryCountVoucherPrint extends Component {
  state = {
    sumPrice: 0,
    sumCarryingAmount: 0,
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write('<style>@page { padding: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    this.setState({
      ...this.props.item,
    });
  }
  componentDidMount() { }

  sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return convertNumberPrice(sum);
  };

  sumArrayNumber = (arr) => {
    return arr.reduce((a, b) => a + b);
  };

  renderRole = (role) => {
    switch (role) {
      case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
        return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name;
      case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
        return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name;
      case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
        return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name;
      default:
        break;
    }
  };
  render() {
    let { open, t, item } = this.props;
    let arrAccountantQuantity = [];
    let arrAccountantOriginalCost = [];
    let arrInventoryQuantity = [];
    let arrInventoryOriginalCost = [];
    let arrDifferenceQuantity = [];
    let arrDifferenceOriginalCost = [];
    let arrCarryingAmountAccount = [];
    let arrCarryingAmountCheck = [];
    let arrDifferenceCarryingAmount = [];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <span className="">{t("Phiếu kiểm kê")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
        ></iframe>

        <ValidatorForm
          className="validator-form-scroll-dialog"
          ref="form"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className="dialog-print">
            <Grid container>
              <div className="form-print form-print-landscape" style={{ fontSize: "18px" }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between", ...style.bold }}
                >
                  <div item xs={5}>
                    <div>Đơn vị:.....................</div>
                    <div>Mã QHNS:..........................</div>
                  </div>
                  <div>Mẫu số: C53-HD</div>
                </div>
                <div style={{ textAlign: "center", marginBottom: 20 }}>
                  <Title
                    title={"BIÊN BẢN KIỂM KÊ TÀI SẢN CỐ ĐỊNH"}
                    date={new Date(item?.inventoryCountDate)}
                    timeOfInventory={"Thời điểm kiểm kê"}
                  />
                </div>
                <div>
                  <div>Ban kiểm kê bao gồm: </div>
                  {item?.inventoryCountPersons &&
                    item?.inventoryCountPersons?.sort((a, b) => a?.role - b?.role)?.map((person) => {
                      return (
                        <div style={{ marginBottom: 5, display: "flex" }}>
                          <div style={{ flex: 1 }}>
                            {this.renderRole(person?.role)} {" "}Ông/ Bà: {person?.personName || "............................."}.
                          </div>
                          <div style={{ flex: 1 }}>
                            Chức vụ: {person?.position || "............................."}
                          </div>
                          <div style={{ flex: 1 }}>
                            Đại diện: {person?.departmentName || "............................."}.
                          </div>

                        </div>
                      );
                    })}
                </div>
                <div>
                  <div>Đã kiểm kê kho có những tài sản cố định dưới đây:</div>
                  <div>
                    <table style={{ ...style.tableCollapse, fontSize: "18px" }} className="table-report">
                      <tr>
                        <th style={{ ...style.border, minWidth: "30px" }} rowSpan="2">
                          STT
                        </th>
                        <th style={{ ...style.border, maxWidth: "20px" }} rowSpan="2">
                          {t("InventoryCountVoucher.nameAsset")}
                        </th>
                        <th style={{ ...style.border }} rowSpan="2">
                          {t("InventoryCountVoucher.codeAsset")}
                        </th>
                        <th style={{ ...style.border, minWidth: "20px" }} rowSpan="2">
                          {t("InventoryCountVoucher.placeOfUse")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                          {t("InventoryCountVoucher.accordingAccountingBooks")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                          {t("InventoryCountVoucher.accordingInventory")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                          NG chênh lệch (Giảm ghi âm(-))
                        </th>
                        <th style={style.border} rowSpan="2">
                          Ghi chú
                        </th>
                      </tr>
                      <tr>
                        <th style={{ ...style.alignCenter, ...style.border }}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                        <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
                        <th style={{ ...style.alignCenter, ...style.border }}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                        <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
                        <th style={{ ...style.alignCenter, ...style.border }}>
                          {t("InstrumentsToolsInventory.quantity")}
                        </th>
                        <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                        <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
                      </tr>

                      {item.assets
                        ? item.assets.map((row, index) => {
                          let {
                            accountantQuantity,
                            accountantOriginalCost,
                            accountantCarryingAmount,
                            inventoryQuantity,
                            inventoryOriginalCost,
                            inventoryCarryingAmount,
                          } = row;
                          let carryAmount =
                            inventoryCarryingAmount -
                            accountantCarryingAmount;
                          let originalCost =
                            inventoryOriginalCost - accountantOriginalCost;
                          inventoryCarryingAmount - accountantCarryingAmount;
                          let totalOriginalCost =
                            inventoryOriginalCost - accountantOriginalCost;
                          let totalQuantity =
                            inventoryQuantity - accountantQuantity;
                          arrAccountantOriginalCost.push(
                            accountantOriginalCost
                          );
                          arrAccountantQuantity.push(accountantQuantity);
                          arrInventoryOriginalCost.push(
                            inventoryOriginalCost
                          );
                          arrInventoryQuantity.push(inventoryQuantity);
                          arrDifferenceQuantity.push(totalQuantity);
                          arrDifferenceOriginalCost.push(totalOriginalCost);
                          arrCarryingAmountAccount.push(
                            accountantCarryingAmount
                          );
                          arrCarryingAmountCheck.push(
                            inventoryCarryingAmount
                          );
                          arrDifferenceCarryingAmount.push(carryAmount);

                          return (
                            <tbody style={{ fontSize: "18px" }}>
                              <tr>
                                <td style={{ ...style.alignCenter, ...style.border, textAlign: "center" }}>{index + 1}</td>
                                <td style={{ ...style.code, padding: 5 }}>
                                  {row?.asset?.name}
                                </td>
                                <td style={{ ...style.code, textAlign: "center" }}>{row.asset?.code}</td>
                                <td style={{ ...style.code, padding: 5 }}>
                                  {row.asset?.useDepartment?.name}
                                </td>
                                <td style={style.alignCenterNumber}>1</td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(
                                    accountantOriginalCost
                                  )}
                                </td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(
                                    accountantCarryingAmount
                                  )}
                                </td>
                                <td style={style.alignCenterNumber}>1</td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(
                                    inventoryOriginalCost
                                  )}
                                </td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(
                                    inventoryCarryingAmount
                                  )}
                                </td>
                                <td style={style.alignCenterNumber}>
                                  {convertNumberPriceRoundUp(
                                    totalOriginalCost
                                  ) || 0}
                                </td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(originalCost) || 0}
                                </td>
                                <td style={style.alignRightOriginalCost}>
                                  {convertNumberPriceRoundUp(carryAmount) || 0}
                                </td>
                                <td style={{ ...style.border }}>{row?.note}</td>
                              </tr>
                            </tbody>
                          );
                        })
                        : ""}
                      {
                        <tr>
                          <th colSpan="4">Tổng cộng</th>
                          <th style={{ ...style.alignCenter, border: "1px solid", }}>
                            {item?.assets?.length}
                          </th>
                          <th style={style.alignRight}>
                            {this.sumArray(arrAccountantOriginalCost)}
                          </th>
                          <th style={style.alignRight}>
                            {this.sumArray(arrCarryingAmountAccount)}
                          </th>
                          <th style={style.alignCenter}>
                            {item?.assets?.length}
                          </th>
                          <th style={style.alignRight}>
                            {this.sumArray(arrInventoryOriginalCost)}
                          </th>
                          <th style={style.alignRight}>
                            {this.sumArray(arrCarryingAmountCheck)}
                          </th>
                          <th style={style.alignCenter}>0</th>
                          <th style={style.alignRight}>
                            {convertNumberPrice(
                              this.sumArrayNumber(arrInventoryOriginalCost) - this.sumArrayNumber(arrAccountantOriginalCost)
                            ) || 0}
                          </th>
                          <th style={style.alignRight}>
                            {convertNumberPrice(
                              this.sumArrayNumber(arrCarryingAmountCheck) - this.sumArrayNumber(arrCarryingAmountAccount)
                            ) || 0}
                          </th>
                        </tr>
                      }
                    </table>
                  </div>

                  <div style={{ marginTop: "10px" }}>
                    Ý kiến giải quyết số chênh lệch ..................................................................................................................................................................................
                  </div>
                </div>
                <div style={style.signature}>
                  <Signature
                    titleProps={{ fontSize: 18 }}
                    signProps={{ fontSize: 18 }}
                    title="Thủ trưởng đơn vị"
                    sign="Ký, họ tên, đóng dấu"
                  />
                  <Signature
                    titleProps={{ fontSize: 18 }}
                    signProps={{ fontSize: 18 }}
                    title="Kế toán trưởng" sign="Ký, họ tên" />
                  <Signature
                    titleProps={{ fontSize: 18 }}
                    signProps={{ fontSize: 18 }}
                    title="Trưởng ban kiểm kê"
                    sign="Ký, họ tên"
                  // date={new Date(item?.inventoryCountDate)}
                  />
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
