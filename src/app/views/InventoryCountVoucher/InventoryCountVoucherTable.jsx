import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountByStatus,
  getListQRCode,
} from "./InventoryCountVoucherService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable, OBJECT_PRINT_ASSET } from "app/appConst";
import AppContext from "app/appContext";
import ComponentInventoryTable from "./ComponentInventoryTable";
import { getByPageDsPhieuDanhGiaOfVoucher } from "../AssetReevaluate/AssetReevaluateService";
import ListAssetTable from "./ListAssetTable";
import { convertFromToDate, getTheHighestRole, isValidDate } from "app/appFunction";
import { TabPanel, LightTooltip } from "../Component/Utilities";
import { getAssetByVouchers } from "../InventoryCountVoucherDepartment/InventoryCountVoucherDepartmentService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const hasDeletePermission = props.hasDeletePermission;
  const hasEditPermission = props.hasEditPermission;

  return (
    <div className="none_wrap">
      {hasEditPermission
        &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder,
        ].includes(item?.inventoryCountStatus?.indexOrder)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission
        &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
        ].includes(item?.inventoryCountStatus?.indexOrder)
        && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.print)}>
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
      {
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class InventoryCountVoucherTable extends React.Component {
  constructor(props) {
    super(props)
    this.anchorRef = React.createRef();
  }
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    checkAll: false,
    selectedList: [],
    listDetailItems: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenPrintDialog: false,
    hasEditPermission: false,
    hasDeletePermission: false,
    hasCreatePermission: false,
    isSearch: false,
    inventoryCountDateBottom: null,
    inventoryCountDateTop: null,
    ids: [],
    tabValue: 0,
    assetVoucherIds: [],
    idItem: null,
    itemAsset: [],
    openPrintQR: false,
    products: [],
    openExcel: false,
    openAdvanceSearch: false,
    shouldOpenPrintDialogAll: false,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = 1; //Điều chuyển

  handleToggleOpenExcel = (e) => {
    const { openExcel } = this.state;
    this.setState((prevState) => ({
      openExcel: !openExcel,
    }));
  };

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage
    })
    if (appConst?.tabInventory?.tabAll === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: null,
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabPlan === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[0].indexOrder
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabTakingInventory === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[1].indexOrder
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabInventory === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[2].indexOrder
      },
        () => this.updatePageData()
      )
    }
  }

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search()
  }

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0)
  }

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { selectedList } = this.state;
    let searchObject = {};
    searchObject.type = appConst.assetTypeInventoryCountObject.DEPARTMENT.code;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = convertFromToDate(this.state.inventoryCountDateBottom).fromDate;
    searchObject.inventoryCountDateTop = convertFromToDate(this.state.inventoryCountDateTop).toDate;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.TSCD;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;
    searchObject.departmentId = this.state?.department?.id;
    setPageLoading(true);
    searchByPage(searchObject).then(({ data }) => {
      if (appConst.CODE.SUCCESS === data?.code) {
        if (data?.data?.number && data?.data?.content?.length <= 0) {
          return this.setPage(0);
        }
        let countItemChecked = 0;
        const itemList = data?.data?.content?.map(item => {
          let isExitsItem = selectedList?.find(checkedItem => checkedItem?.id === item.id);
          if (isExitsItem) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!isExitsItem,
          }
        }
        )

        this.setState({
          selectedItem: null,
          itemList,
          totalElements: data?.data?.totalElements,
          ids: [],
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0,
        })
      } else {
        toast.warning(data?.message)
      }
    }).catch(() => {
      toast.error(this.props.t("general.error"))
    }).finally(() => {
      setPageLoading(false)
    })
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrintDialog: false,
      shouldOpenInventoryAllDepartmentsDialog: false,
      shouldOpenPrintDialogAll: false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      inventoryCountDateBottom: null,
      inventoryCountDateTop: null,
      shouldOpenInventoryAllDepartmentsDialog: false,
      shouldOpenPrintDialogAll: false,
      keyword: ""
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then(({ data }) => {
      this.setState({
        item: data?.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info("Xoá thành công.");
          this.search();
          this.getCountStatus();
          this.handleDialogClose();
        } else {
          toast.warning(data.message)
        }
      }).
      catch((error) => {
        toast.error(this.props.t("general.error"))
      })
  };

  componentDidMount() {
    this.setState({
      inventoryCountStatusIndexOrder: null
    },
      () => this.updatePageData()
    );
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getDsPhieuDanhGiaOfVoucher = async (assetVoucherIds) => {
    try {
      let searchObject = {
        assetVoucherIds,
        pageIndex: 1,
        pageSize: 10000,
        voucherType: 1,
        voucherId: this.state.item.id
      }
      let res = await getByPageDsPhieuDanhGiaOfVoucher(searchObject)
      if (res?.data?.data?.content && res?.status === 200 && res?.data?.code === 200) {
        let listDataDgl = res?.data?.data?.content
        let assetsUpdate = this.state.item.assets.map(item => {
          let itemDgl = listDataDgl.find(itemDgl => itemDgl?.assetVoucherId === item?.id)
          if (!itemDgl?.isEvaluated) {
            item.isAllowedReevaluate = true
          }
          else if (itemDgl?.dglInfo) {
            if (itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder || itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder) {
              item.isAllowedReevaluate = false
            }
            else {
              item.isAllowedReevaluate = true
            }
          }
          item.asset.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.asset.assetVoucherId = itemDgl?.assetVoucherId
          item.asset.voucherId = this.state.item.id
          item.asset.voucherType = 1
          return item
        })

        this.setState({
          item: {
            ...this.state.item,
            assets: assetsUpdate
          }
        })
      }
    } catch (error) {
      toast.error("Xảy ra lỗi")
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.assetVoucherIds !== prevState.assetVoucherIds) {
      this.getDsPhieuDanhGiaOfVoucher(this.state.assetVoucherIds)
    }
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasCreatePermission,
    } = this.state;
    const roles = getTheHighestRole();
    let {
      currentUser,
      isRoleAccountant,
      isRoleAdmin,
      isRoleAssetManager,
      isRoleOrgAdmin,
    } = roles;
    if (currentUser) {
      const isAllManagers = isRoleAssetManager
        || isRoleAdmin
        || isRoleOrgAdmin
        || isRoleAccountant;
      hasDeletePermission = isAllManagers;
      hasCreatePermission = isAllManagers;
      hasEditPermission = isAllManagers;

      this.setState({
        ...roles,
        hasDeletePermission,
        hasEditPermission,
        hasCreatePermission,
        orgName: currentUser?.org?.name
      });
    }
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  validateAsset = () => {

    const { t, i18n } = this.props;
    let {
      selectedList
    }
      = this.state;

    if (selectedList <= 0) {
      toast.warning(t("InventoryCountVoucherDepartment.messages.noItemSelected"));
      return false;
    }
    return true;
  }

  convertDataAsset = (value) => {
    value?.map((asset, index) => {
      let differenceQuantity = (
        asset?.inventoryQuantity - asset?.accountantQuantity
      );
      let differenceOriginalCost = (
        asset?.inventoryOriginalCost - asset?.accountantOriginalCost
      );
      asset.assetsId = asset?.assetId;
      asset.asset = {
        ...asset,
        name: asset?.assetName,
        code: asset?.assetCode,
        status: {
          name: asset?.updateStatusName,
          id: asset?.updateStatusId,
          indexOrder: asset?.updateStatusIndexOrder
        }
      };
      asset.status = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };
      asset.currentStatus = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };
      asset.differenceOriginalCost = differenceOriginalCost;
      asset.differenceQuantity = differenceQuantity;
      asset.price = (
        asset?.accountantOriginalCost / asset?.accountantQuantity
      );
      asset.storeId = asset?.storeId;
      asset.store = {
        name: asset?.storeName,
        id: asset?.storeId
      };
      asset.isStore = [
        appConst.listStatusAssets.NHAP_KHO.indexOrder,
        appConst.listStatusAssets.LUU_MOI.indexOrder,
        appConst.listStatusAssets.DA_HONG.indexOrder,
      ].includes(asset?.updateStatusIndexOrder)
    });
    return value;
  }

  handleEditItemInventoryAllDepartment = async () => {
    let { setPageLoading } = this.context;
    const { t } = this.props;
    try {
      setPageLoading(true);
      if (!this.validateAsset()) return;
      const data = await getAssetByVouchers(this.state?.selectedList);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          assets: this.convertDataAsset(data?.data?.data) || [],
          item: {
            isView: false,
          },
          shouldOpenInventoryAllDepartmentsDialog: true,
        });
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true)
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = this.state.inventoryCountDateBottom;
    searchObject.inventoryCountDateTop = this.state.inventoryCountDateTop;
    searchObject.ids = this.state?.selectedList?.map(i => i?.id);
    searchObject.assetClass = appConst.assetClass.TSCD;
    searchObject.departmentId = this.state?.department?.id;
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;
    searchObject.type = appConst.assetTypeInventoryCountObject.DEPARTMENT.code;

    exportToExcel(searchObject)
      .then((res) => {
        setPageLoading(false)
        this.setState({ ids: [] })
        toast.success(t('general.successExport'));
        this.handleOKEditClose();
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, t("InventoryCountVoucher.exportExcelFileName"));
      })
      .catch((err) => {
        setPageLoading(false)
        toast.error(t('general.failExport'));
      });
  };

  handleDateChange = (date, name) => {
    if (date === null) {
      this.setState({ [name]: date }, () => { this.search() });
    } else {
      this.setState({ [name]: date }, () => { isValidDate(date) && this.search() });
    }
  }

  handleChangeDepartment = (value, name) => {
    this.setState({ [name]: value }, () => { this.search() });
  }

  handleEditData = (inventoryCountVoucher) => {
    let inventoryCountPersonIds = [];
    let userIds = [];
    let arrAssetVoucherIds = []
    inventoryCountVoucher?.assets?.map((asset, index) => {
      let differenceQuantity = (
        asset?.accountantQuantity - asset?.inventoryQuantity
      );
      let differenceOriginalCost = (asset?.inventoryOriginalCost - asset?.accountantOriginalCost) || 0;
      let differenceCarryingAmount = (asset?.inventoryCarryingAmount - asset?.accountantCarryingAmount) || 0;
      asset.assetId = asset?.asset?.id
      asset.differenceQuantity = differenceQuantity;
      asset.inventoryQuantity = (asset?.inventoryQuantity === 0 || asset?.inventoryQuantity === null) ? 0 : 1;
      asset.differenceOriginalCost = differenceOriginalCost
      asset.differenceCarryingAmount = differenceCarryingAmount
      asset.storeId = asset?.store?.id
      asset.isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder))

      arrAssetVoucherIds.push(asset?.id)
    })

    inventoryCountVoucher.assets.sort((a, b) => (b?.asset?.code)?.localeCompare(a?.asset?.code))
    inventoryCountVoucher.inventoryCountPersons.map((item) => {
      inventoryCountPersonIds.push(item?.person?.id);
      userIds.push(item?.person?.userId);
    })

    inventoryCountVoucher = {
      ...inventoryCountVoucher,
      assetId: inventoryCountVoucher?.asset?.id,
      departmentId: inventoryCountVoucher?.department?.id,
      inventoryCountPersonIds: inventoryCountPersonIds,
      userIds: userIds,
      inventoryCountStatusId: inventoryCountVoucher?.inventoryCountStatus?.id
    }

    this.setState({
      item: inventoryCountVoucher,
      shouldOpenEditorDialog: true,
      assetVoucherIds: arrAssetVoucherIds
    });
  }
  setStateInventory = (item) => {
    if (item) {
      this.setState(item)
    }
  }

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item);
        return item;
      });

      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item?.id));
        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item?.id === event.target.name);
      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(item) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  updateData = () => {
    getItemById(this.state.item?.id).then(({ data }) => {
      let inventoryCountVoucher = data?.data ? data?.data : {}
      inventoryCountVoucher.isView = false;
      this.handleEditData(inventoryCountVoucher)
    });
  }

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusInventory.find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountByStatus({ type: appConst.assetTypeInventoryCountObject.DEPARTMENT.code })
      .then(({ data }) => {
        let countStatusPlan,
          countStatusTakingInventory,
          countStatusInventory;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder
            === item?.status
          ) {
            countStatusPlan = this.checkCount(item?.count);
            return;
          }
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder
            === item?.status
          ) {
            countStatusTakingInventory = this.checkCount(item?.count);
            return;
          }
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.DA_KIEM_KE.indexOrder
            === item?.status
          ) {
            countStatusInventory = this.checkCount(item?.count);
            return;
          }
        });
        this.setState(
          {
            countStatusPlan,
            countStatusTakingInventory,
            countStatusInventory,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  exportToPrint = async (e, type) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true)
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.type = this.voucherType;
    searchObject.inventoryCountDateBottom = this.state.inventoryCountDateBottom;
    searchObject.inventoryCountDateTop = this.state.inventoryCountDateTop;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = 99999;
    searchObject.assetClass = appConst.assetClass.TSCD;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;

    try {
      if (type === OBJECT_PRINT_ASSET.TOAN_BO.code) {
        const data = await searchByPage(searchObject);
        if (appConst.CODE.SUCCESS === data?.data?.code) {
          if (data?.data?.data?.content?.length <= 0) {
            toast.warning(t("InventoryCountVoucher.messages.emptyList"));
            return;
          }
          this.setState({
            item: data?.data?.data?.content,
            shouldOpenPrintDialogAll: true,
          });
        }
      }
      if (type === OBJECT_PRINT_ASSET.THEO_DANH_SACH.code) {
        if (this.state?.selectedList?.length <= 0) {
          toast.warning(t("InventoryCountVoucher.messages.emptyForm"));
          return;
        }
        this.setState({
          item: this.state?.selectedList,
          shouldOpenPrintDialogAll: true,
        });
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleSetItemState = async (rowData) => {

    let { setPageLoading } = this.context;
    try {
      setPageLoading(true)
      let arrAssetVoucherIds = []

      rowData.assets.map((asset, index) => {
        let differenceOriginalCost = (asset?.inventoryOriginalCost - asset?.accountantOriginalCost);
        let differenceCarryingAmount = (asset?.inventoryCarryingAmount - asset?.accountantCarryingAmount);
        asset.assetId = asset?.asset?.id
        asset.differenceOriginalCost = differenceOriginalCost
        asset.differenceCarryingAmount = differenceCarryingAmount
        asset.storeId = asset?.store?.id
        asset.isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder))

        arrAssetVoucherIds.push(asset?.id)
      })
      let searchObject = {
        assetVoucherIds: arrAssetVoucherIds,
        pageIndex: 1,
        pageSize: 10000,
        voucherType: 1,
        voucherId: this.state.item.id
      }
      let res = await getByPageDsPhieuDanhGiaOfVoucher(searchObject)
      if (res?.data?.data?.content && res?.status === 200 && res?.data?.code === 200) {
        let listDataDgl = res?.data?.data?.content
        let assetsUpdate = rowData.assets.map(item => {
          let itemDgl = listDataDgl?.find(itemDgl => itemDgl?.assetVoucherId === item?.id)
          if (!itemDgl?.isEvaluated) {
            item.isAllowedReevaluate = true
          }
          else if (itemDgl?.dglInfo) {
            if (itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder || itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder) {
              item.isAllowedReevaluate = false
            }
            else {
              item.isAllowedReevaluate = true
            }
          }
          item.asset.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.asset.assetVoucherId = itemDgl?.assetVoucherId
          item.asset.voucherId = this.state.item.id
          item.asset.voucherType = 1
          return item
        })
        this.setState({
          selectedItem: rowData,
          itemAsset: assetsUpdate.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
        });

      }
    } catch (error) {
      console.log(error)
      toast.error("Xảy ra lỗi")
    } finally {
      setPageLoading(false);
    }

  };

  handleStatus = (rowData) => {
    const statusIndex = rowData?.inventoryCountStatus?.indexOrder;
    let className = "";
    switch (statusIndex) {
      case appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder:
        className = "status status-success";
        break;
      case appConst.listStatusInventoryObject.DANG_KIEM_KE.indexOrder:
        className = "status status-warning";
        break;
      case appConst.listStatusInventoryObject.NEW.indexOrder:
        className = "status status-info";
        break;
      default: break;
    }

    return (
      <span className={className}>{this.checkStatus(statusIndex)}</span>
    );

  };

  handlePrintQRCode = async (id) => {
    let { t } = this.props
    try {
      let res = await getListQRCode(id);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          products: res?.data?.data?.length > 0 ? [...res?.data?.data] : [],
          openPrintQR: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handlePrint = async (rowData) => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);

      let listDetailItems = [];
      if (this.state?.selectedList?.length > 0) {
        for (const item of this.state.selectedList) {
          const data = await getItemById(item);
          if (data?.data?.data) {
            listDetailItems.push(data.data.data);
          }
        }
        this.setState({ listDetailItems });
      }

      getItemById(rowData.id).then(({ data }) => {
        let inventoryCountVoucher = data?.data ? data?.data : {};
        inventoryCountVoucher.orgName = this.state?.orgName;
        if (this.state?.selectedList?.length <= 0) {
          this.setState({ listDetailItems: [{ ...inventoryCountVoucher }] });
        }
        this.setState({
          item: inventoryCountVoucher,
          shouldOpenPrintDialog: true,
        });
      });
    } catch (error) {
      console.error(error);
    } finally {
      setPageLoading(false)
    }
  };


  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      tabValue,
      checkAll,
    } = this.state;

    let TitlePage = t("InventoryCountVoucher.title");

    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.stt"),
        field: "",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 60,
        maxWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasEditPermission={hasEditPermission}
            hasDeletePermission={hasDeletePermission}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                getItemById(rowData.id).then(({ data }) => {
                  let inventoryCountVoucher = data?.data ? data?.data : {}
                  inventoryCountVoucher.isView = false;
                  this.handleEditData(inventoryCountVoucher)
                });
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.view === method) {
                getItemById(rowData.id).then(({ data }) => {
                  let inventoryCountVoucher = data?.data ? data?.data : {}
                  inventoryCountVoucher.isView = true;
                  this.handleEditData(inventoryCountVoucher)
                });
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData);

              } else if (appConst.active.qrcode === method) {
                this.handlePrintQRCode(rowData.id)
              }
              else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.inventoryCountDate"),
        maxWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.inventoryCountDate
            ? moment(rowData?.inventoryCountDate).format("DD/MM/YYYY")
            : "",
      },
      {
        title: t("InventoryCountVoucher.status"),
        field: "inventoryCountStatus.name",
        minWidth: "150",
        align: "left",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.handleStatus(rowData),
      },
      {
        title: t("InventoryCountVoucher.voucherName"),
        field: "title",
        minWidth: 200,
        align: "left",
      },
      {
        title: t("InventoryCountVoucher.department"),
        field: "department.name",
        minWidth: 200,
        align: "left",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {" "}
            {t("Asset.inventory_count_voucher")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.assetManagement"), path: 'fixed-assets/inventory-count-vouchers' },
              { name: TitlePage }
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("InventoryCountVoucher.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabNew")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusPlan || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabTakingInventory")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusTakingInventory || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabInventory")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusInventory || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabAll} className="mp-0">
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleChangeDepartment={this.handleChangeDepartment}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            assetVoucherIds={this.state.assetVoucherIds}
            updateData={this.updateData}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            exportToPrint={this.exportToPrint}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabPlan} className="mp-0" >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleChangeDepartment={this.handleChangeDepartment}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            exportToPrint={this.exportToPrint}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabTakingInventory} className="mp-0" >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleChangeDepartment={this.handleChangeDepartment}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            exportToPrint={this.exportToPrint}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabInventory} className="mp-0" >
          <ComponentInventoryTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            handleEditItemInventoryAllDepartment={this.handleEditItemInventoryAllDepartment}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleChangeDepartment={this.handleChangeDepartment}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            exportToPrint={this.exportToPrint}
          />
        </TabPanel>
        <ListAssetTable {...this.props} itemAsset={this.state.itemAsset} />
      </div>
    );
  }
}
InventoryCountVoucherTable.contextType = AppContext;
export default InventoryCountVoucherTable;
