import axios from "axios";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
const API_PATH_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/excel/export/assets/inventory-count-voucher";
const API_PATH_voucher_asset =
  ConstantList.API_ENPOINT + "/api/v2/assets/inventory-count-vouchers";
const API_PATH_asset = ConstantList.API_ENPOINT + "/api/v2/assets/asset-class";
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
export const personSearchByPage = (searchObject) => {
  let url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const getManagementDeparmentByOrg = (searchObject) => {
  let url = API_PATH_department + "/management-departments";
  return axios.get(url);
};

export const getCountByStatus = (payload) => {
  let config = {
    params: { ...payload },
  };
  let url = API_PATH_voucher_asset + "/count-by-status";
  return axios.get(url, config);
};
export const searchByPage = (searchObject) => {
  let url = API_PATH_voucher_asset + "/search-by-page";
  return axios.post(url, searchObject);
};
export const updateAsset = (asset) => {
  let url = API_PATH_voucher_asset + `/${asset?.id}`;
  return axios.put(url, asset);
};
export const addAsset = (asset) => {
  return axios.post(API_PATH_voucher_asset, asset);
};
export const getItemById = (id) => {
  let url = API_PATH_voucher_asset + `/${id}`;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let url = API_PATH_voucher_asset + `/${id}`;
  return axios.delete(url);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL,
    data: searchObject,
    responseType: "blob",
  });
};

//get bản ghi theo id đơn vị kiểm kê
export const getAssetByDepartmentByOrg = (searchObject) => {
  let config = { params: { useDepartmentId: searchObject?.useDepartmentId } };
  let url = API_PATH_asset + `/${appConst.assetClass.TSCD}`;
  return axios.get(url, config);
};

export const getAssetByDepartment = (searchObject) => {
  let config = { params: { useDepartmentId: searchObject?.useDepartmentId } };
  let url = API_PATH_NEW + `/asset/assetClass/${searchObject.assetClass}`;
  return axios.get(url, config);
};

export const getListQRCode = (id) => {
  let url =
    API_PATH_NEW + `/inventory-count-vouchers/print-asset-qr-code/${id}`;
  return axios.get(url);
};
