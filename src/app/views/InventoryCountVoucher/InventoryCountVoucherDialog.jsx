import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { addAsset, getAssetByDepartmentByOrg, updateAsset } from './InventoryCountVoucherService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import {
  searchByPageAssetDocument,
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeAssetDocument,
} from '../Asset/AssetService'
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../../../styles/views/_loadding.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InventoryCountScrollableTabsButtonForce from './InventoryCountScrollableTabsButtonForce'
import { appConst, variable } from "../../appConst";
import AppContext from "app/appContext";
import AssetTransferDialog from "../AssetLiquidate/AssetLiquidateDialog";
import AssetReevaluateDialog from "../AssetReevaluate/AssetReevaluateDialog";
import { PaperComponent } from "../Component/Utilities";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import { formatDateDto } from "app/appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});


class InventoryCountVoucherDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    assets: [],
    inventoryLiquidateAssets: [],
    title: null,
    inventoryCountDate: new Date(),
    department: null,
    inventoryCountStatus: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenSelectAssetPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: '',
    asset: {},
    shouldOpenSelectPersonPopup: false,
    assetDocumentList: [],
    shouldOpenPopupAssetFile: false,
    listAssetDocumentId: [],
    loading: false,
    documentType: 6, // hồ sơ kiểm kê,
    assetReevaluate: {},
    shouldOpenReevaluateDialog: false,
    selectedValue: null,
    isLiquidate: false,
    dataAssetTable: [],
    isEnabledBtn: false,
    inventoryCountPersons: [],
    listAssetsAdded: []
  };

  setSelectedValue = (id) => {
    this.setState({
      selectedValue: id,
    })
  }

  handleSetAssetReevaluate = (item) => {
    this.setState({
      assetReevaluate: item
    })
  }

  handleOpenAssetReevaluateDialog = () => {
    if (this.state.assetReevaluate?.id) {
      this.setState({
        shouldOpenReevaluateDialog: true,
      })
    }
    else {
      toast.warning("Chưa có tài sản nào được chọn")
    }
  }

  handleCloseAssetReevaluateDialog = () => {
    this.setState({
      shouldOpenReevaluateDialog: false,
    })
  }

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true
    })
  }

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false
    })
  }

  handleChange = (event, source) => {
    this.setState({ [event?.target?.name]: event?.target?.value });
  };
  selectHandoverPerson = (item) => {
    this.setState({ handoverPerson: item }, function () {
    });
  }

  selectReceiverPerson = (item) => {
    this.setState({ receiverPerson: item }, function () {
    });
  }

  validateAsset = () => {
    let { assets } = this.state;
    let check = false;
    assets?.some(asset => {
      let isStore = (
        appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder
        || appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder
      );
      if (isStore && !asset?.store) {
        toast.warning(t("InventoryCountVoucher.messages.emptyStore"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryOriginalCost && asset?.inventoryOriginalCost !== 0) {
        toast.warning(t("InventoryCountVoucher.messages.emptyOriginalCost"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryOriginalCost < 0) {
        toast.warning(t("InventoryCountVoucher.messages.originalCostError"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryCarryingAmount && asset?.inventoryCarryingAmount !== 0) {
        toast.warning(t("InventoryCountVoucher.messages.emptyCaryingAmount"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryCarryingAmount < 0) {
        toast.warning(t("InventoryCountVoucher.messages.caryingAmountError"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  validateSubmit = () => {
    let {
      id,
      assets,
      hoiDongId,
      inventoryCountStatus,
      inventoryCountPersons,
      listAssetsAdded,
      title
    } = this.state;
    let { t } = this.props;
    const listAsset = [...(assets?.length ? assets : []), ...(listAssetsAdded?.length ? listAssetsAdded : [])];
    if (!title) {
      toast.warning(t("InventoryCountVoucher.messages.emptyTitle"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountStatus) {
      toast.warning(t("InventoryCountVoucher.messages.emptyStatus"));
      toast.clearWaitingQueue();
      return false;
    }
    if (listAsset?.length <= 0) {
      toast.warning(t("InventoryCountVoucher.messages.emptyAsset"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!id && hoiDongId && inventoryCountPersons?.length === 0) {
      toast.warning(t("InventoryCountVoucher.messages.emptyCouncil"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!hoiDongId && !id) {
      toast.warning(t("InventoryCountVoucher.messages.noCouncilSelected"));
      toast.clearWaitingQueue();
      return false;
    }
    if (this.validateAsset()) return;

    return true;
  }

  handleFormSubmit = () => {
    let { setPageLoading } = this.context;
    let {
      id,
      shouldOpenLiquidateDialog,
      inventoryCountDate,
      inventoryCountStatusId,
      assets = [],
      listAssetsAdded = [],
      title,
      hoiDongId,
      departmentId,
      assetReevaluate
    } = this.state;
    let { t, handleClose } = this.props;
    let newData = [...assets, ...listAssetsAdded]
    let dataSubmit = {
      // ...this.state,
      id,
      inventoryCountDate: inventoryCountDate ? formatDateDto(inventoryCountDate) : null,
      inventoryCountStatusId,
      assets: newData?.map(i => {
        return {
          ...i,
          storeId: i?.store?.id || i?.asset?.storeId,
          departmentId: i?.departmentId || i?.asset?.useDepartmentId,
        }
      }),
      departmentId,
      title,
      hoiDongId,
      assetReevaluate,
      inventoryCountPersons: this.state?.inventoryCountPersons
        ? this.state?.inventoryCountPersons?.map(item => ({
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }))
        : []
    }
    if (!this.validateSubmit() && !shouldOpenLiquidateDialog) return;

    setPageLoading(true)
    if (id) {
      updateAsset(dataSubmit)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("InventoryCountVoucher.updateSuccess"));
            this.setState({
              id: data?.data?.id,
              isEnabledBtn: data.data?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
              isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
            })
            if (this.props?.updatePageData) {
              this.props.updatePageData()
            }
            handleClose();
          } else {
            toast.warning(data.message)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
          setPageLoading(false)
        })
    } else {
      addAsset(dataSubmit).then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          this.setState({
            id: data?.data?.id,
            isEnabledBtn: data.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
            isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
          })
          toast.success(t("InventoryCountVoucher.addSuccess"));
          this.setState({
            id: data?.data?.id,
            isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventory[2].indexOrder
          });
          if (this.props?.updatePageData) {
            this.props.updatePageData()
          }
          handleClose();
        } else {
          toast.warning(data.message)
        }
        setPageLoading(false)
      })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
          setPageLoading(false)
        })
    }
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false
    });
  };
  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item?.id) {
      if (!assetVouchers) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      }
      else {
        let hasInList = false;
        assetVouchers?.forEach(element => {
          if (element?.asset?.id === item?.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState({ assetVouchers, totalElements: assetVouchers?.length }, function () {
      this.handleAssetPopupClose();
    });
  }

  componentWillMount() {
    let { item } = this.props;
    if (item?.assets) {
      this.props.item?.assets.sort((a, b) => (a.id - b.id));
    }
    if (item?.inventoryCountStatus) {
      if (item?.inventoryCountStatus.indexOrder === appConst.listStatusInventory[2].indexOrder) {
        this.setState({
          isEnabledBtn: true,
          isLiquidate: true
        })
      }
    }

    this.setState({
      ...this.props.item,
      assets: this.props?.item?.assets?.filter(i => i?.accountantQuantity),
      dataAssetTable: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 1 || i?.accountantQuantity === null),
      listAssetsAdded: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 0)
    });
  }

  componentDidMount() {

  }

  componentDidUpdate(prevProps) {
    if (this.props?.item !== prevProps.item) {
      if (this.props?.item?.assets) {
        this.props.item?.assets.sort((a, b) => (a.id - b.id));
        this.setState({
          dataAssetTable: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 1 || i?.accountantQuantity === null),
          listAssetsAdded: this.props?.item?.assets?.filter(i => i?.accountantQuantity === 0)
        })
      }
    }
  }

  handlePopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
      shouldOpenSelectAssetPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true
    });
  };
  handleSelectAssetPopupOpen = () => {
    if (!this.state?.department?.id) {
      toast.info("Vui lòng chọn phòng ban được kiểm kê (Thông tin phiếu)")
      return;
    }
    this.setState({
      shouldOpenSelectAssetPopup: true
    });
  };

  handleSelectAssetAdded = (assets = []) => {
    this.setState({
      listAssetsAdded: assets?.map(i => {
        return {
          ...i,
          assetId: i?.asset?.id,
          accountantQuantity: 0,
          differenceOriginalCost: 0,
          differenceCarryingAmount: 0,
          differenceQuantity: 1,
          accountantOriginalCost: i?.asset?.originalCost,
          accountantCarryingAmount: i?.asset?.carryingAmount,
          inventoryOriginalCost: i?.asset?.originalCost,
          inventoryCarryingAmount: i?.asset?.carryingAmount,
          inventoryQuantity: 1,
          currentStatus: i?.asset?.statusName,
          note: "",
          store: {
            id: i?.asset?.storeId || i?.store?.id,
            name: i?.asset?.storeName || i?.store?.name
          },
          status: {
            id: i?.status?.id || i?.asset?.statusId,
            name: i?.status?.name || i?.asset?.statusName,
          },
          assetStatusId: i?.status?.id || i?.asset?.statusId,
          departmentId: i?.asset?.useDepartmentId || i?.departmentId,
          departmentName: i?.asset?.useDepartmentName || i?.departmentName,
        }
      }),
      shouldOpenSelectAssetPopup: false
    })
  }
  handleDeleteItem = (rowData) => {
    let { listAssetsAdded } = this.state;
    const index = listAssetsAdded?.findIndex(item => item?.tableData?.id === rowData?.tableData?.id);

    if (index > -1) {
      listAssetsAdded.splice(index, 1);
    }

    this.setState({ listAssetsAdded });
  }

  handleSelectDepartment = (item) => {
    let { setPageLoading } = this.context;
    let { assets } = this.state;
    this.setState({ department: { id: item?.id, name: item?.name ? item?.name : item?.text }, assets: [] }, function () {
      setPageLoading(true)
      let inventoryCountAssets = [];
      if (item?.id) {
        let searchObject = {}
        searchObject.useDepartmentId = item.id
        searchObject.assetClass = appConst.assetClass.TSCD
        getAssetByDepartmentByOrg(searchObject)
          .then(({ data }) => {
            setPageLoading(false)
            if (appConst.CODE.SUCCESS !== data?.code) {
              toast.warning(data?.message)
              return;
            }
            if (data?.data?.length > 0) {
              data.data.forEach(asset => {
                let inventoryCountAsset = {};
                inventoryCountAsset.asset = asset;
                inventoryCountAsset.assetId = asset?.id;
                inventoryCountAsset.currentStatus = asset.status;
                inventoryCountAsset.status = asset.status;
                inventoryCountAsset.assetStatusId = asset.status?.id;
                inventoryCountAsset.note = '';
                inventoryCountAsset.inventoryQuantity = 1;
                inventoryCountAsset.inventoryOriginalCost = asset?.originalCost || 0;
                inventoryCountAsset.inventoryCarryingAmount = asset?.carryingAmount || 0;
                inventoryCountAsset.accountantOriginalCost = asset?.originalCost || 0;
                inventoryCountAsset.accountantCarryingAmount = asset?.carryingAmount || 0;
                inventoryCountAsset.store = asset?.store ? asset?.store : null;
                inventoryCountAsset.storeId = asset?.store?.id ? asset?.store?.id : null;
                inventoryCountAsset.isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder))
                inventoryCountAssets.push(inventoryCountAsset);
              });
              this.setState({
                dataAssetTable: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
                assets: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
                departmentId: item.id
              },);
            }
            else {
              this.setState({
                dataAssetTable: [],
              },);
            }
          })
          .catch((error) => {
            toast.error(this.props.t("general.error"))
            setPageLoading(false)
          })
      }
      else {
        this.setState({ assets: inventoryCountAssets, dataAssetTable: inventoryCountAssets });
      }
      this.handlePopupClose();
    });
    if (Object.keys(item).length === 0) {
      this.setState({ department: null, assets: null })
    }
    this.setState({
      selectedValue: null
    })
  }

  selectInventoryCount = (item) => {
    if (item?.indexOrder === appConst.listStatusInventoryObject.NEW.indexOrder && !this.state.inventoryCountDate) {
      this.setState({
        inventoryCountDate: new Date()
      });
    } else {
      this.setState({
        inventoryCountDate: this.state.inventoryCountDate
      });
    }
    this.setState({
      inventoryCountStatus: item,
      inventoryCountStatusId: item?.id
    });
  }

  selectStatus = (assetStatus, rowData, isTabNotRecorded) => {
    let { assets = [], listAssetsAdded = [] } = this.state;
    if (isTabNotRecorded) {
      listAssetsAdded[rowData?.tableData?.id].status = assetStatus
      listAssetsAdded[rowData?.tableData?.id].assetStatusId = assetStatus?.id
      listAssetsAdded[rowData?.tableData?.id].isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === assetStatus?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === assetStatus?.indexOrder))

    } else {
      assets[rowData?.tableData?.id].status = assetStatus
      assets[rowData?.tableData?.id].assetStatusId = assetStatus?.id
      assets[rowData?.tableData?.id].isStore = ((appConst.listStatusAssets.NHAP_KHO.indexOrder === assetStatus?.indexOrder) || (appConst.listStatusAssets.LUU_MOI.indexOrder === assetStatus?.indexOrder))
    }
    this.setState({ assets });
  }
  handleRowDataCellChange = (rowData, event, isTabNotRecorded) => {
    let { assets = [], listAssetsAdded = [] } = this.state;
    let item = (isTabNotRecorded ? listAssetsAdded?.[rowData?.tableData?.id] : assets?.[rowData?.tableData?.id]) || {};
    if (variable.listInputName.note === event.target.name) {
      item.note = event.target.value;
      this.setState({ assets })
      return;
    }
    if (variable.listInputName.inventoryOriginalCost === event.target.name) {
      let value = Number(event.target.value);
      item.differenceOriginalCost = (value - (item?.accountantOriginalCost || 0)) || 0;
      item.inventoryOriginalCost = value;
      this.setState({ assets });
      return;
    }
    if (variable.listInputName.inventoryCarryingAmount === event.target.name) {
      let value = event.target.value || 0;
      console.log(value);
      item.differenceCarryingAmount = (value - (item?.accountantCarryingAmount || 0)) || 0;
      item.inventoryCarryingAmount = value;
      this.setState({ assets });
      return;
    }
    if (variable.listInputName.inventoryQuantity === event.target.name) {
      let value = Number(event.target.value);
      item.inventoryQuantity = value;
      item.differenceQuantity = (item?.accountantQuantity || 1) - value;
      if (value === 0) {
        item.inventoryOriginalCost = 0
        item.inventoryCarryingAmount = 0
      }
      else {
        item.inventoryOriginalCost = item?.accountantOriginalCost
        item.inventoryCarryingAmount = item?.accountantCarryingAmount
      }
      this.setState({ assets });
      return;
    }
  };

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true
    });
  }

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false
    });
  }

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes?.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (attributes[index]?.attribute?.id === rowData.attribute?.id) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () {
    });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData?.id).then(({ data }) => {
      if (!data) {
        data = {};
      }
      this.setState({
        item: data,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true
      },
        () => {
          this.updatePageAssetDocument()
        });
    })
  }

  handleRowDataCellDeleteAssetFile = (rowData) => {
    deleteAssetDocumentById(rowData?.id).then(data => {
      this.updatePageAssetDocument()
    })
  }

  handleSetListInventoryPerson = (item) => {
    this.setState({ inventoryCountPersons: item || [] })
  }

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id })
  }

  handleAddAssetDocumentItem = () => {
    getNewCodeAssetDocument().then((result) => {
      if (result?.data?.code) {
        let item = result.data;
        this.setState({
          item: item,
          shouldOpenPopupAssetFile: true,
        });
      }
    }).catch(() => {
      toast.warning(this.props.t("general.error_reload_page"));
    });
  }

  updatePageAssetDocument = () => {
    let searchObject = {}
    searchObject.inventoryCountVoucherId = this.props.item.id;
    searchObject.keyword = this.state.keyword
    searchObject.pageIndex = this.state.page + 1
    searchObject.documentType = this.state.documentType
    searchObject.pageSize = 100000;
    searchByPageAssetDocument(searchObject).then(({ data }) => {
      this.setState({
        assetDocumentList: [...data?.content],
        totalElements: data?.totalElements,
      }, () => {
      })
    })
  }

  getAssetDocumentId = (documentId) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    if (documentId) {
      listAssetDocumentId.push(documentId);
    };
    this.setState({
      listAssetDocumentId: listAssetDocumentId
    }, () => {
    })
  }

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;

    (listAssetDocumentId?.length > 0) && listAssetDocumentId.forEach(id => deleteAssetDocumentById(id));
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  selectStores = (store, rowData, isTabNotRecorded) => {
    const { assets, listAssetsAdded } = this.state;
    const dataList = isTabNotRecorded ? listAssetsAdded : assets;
    const rowId = rowData?.tableData?.id;

    if (rowId !== undefined) {
      dataList[rowId].store = store;
      dataList[rowId].storeId = store?.id;
    }

    this.setState({ assets, listAssetsAdded });
  };


  handleOpenLiquidateDialog = () => {
    let { assets, inventoryLiquidateAssets, isLiquidate } = this.state;
    let { t } = this.props;

    inventoryLiquidateAssets = assets?.filter((asset) =>
      asset?.status?.indexOrder === appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder
      && !inventoryLiquidateAssets?.find(
        item => item?.asset?.id === asset?.asset?.id
      )
    )
    if (isLiquidate) {
      this.setState({
        shouldOpenLiquidateDialog: true,
        isInventoryLiquidateAsset: true,
        inventoryLiquidateAssets,
      });
    }
    else {
      toast.warning(t("InventoryCountVoucher.saveBeforeLiquidate"))
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenLiquidateDialog: false,
      inventoryLiquidateAssets: []
    });
  }
  handlePrintSingleItem = (rowData) => {
    this.setState({
      itemPrint:
      {
        ...rowData,
        asset: {
          ...rowData?.asset,
          detailInfo:
            rowData?.asset?.serialNumber ||
              rowData?.asset?.model ||
              rowData?.asset?.yearPutIntoUse
              ? `${rowData?.asset?.serialNumber || "........."}/ ${rowData?.asset?.model || "........."}/ ${rowData?.asset?.yearPutIntoUse || "........."}`
              : "..".repeat(100),
          useDepartment: {
            ...rowData?.asset?.useDepartment,
            name: rowData?.asset?.useDepartment?.name || "..".repeat(100)
          }
        }
      }
    }, () => {
      this.setState({ shoundOpenPrintSingleItem: true })
    })
  }

  handleClosePrintSingleItem = () => {
    this.setState({ shoundOpenPrintSingleItem: false, itemPrint: null })
  }

  render() {
    let { open, t, i18n, item, handleClose, handleOKEditClose } = this.props;
    let { loading, shouldOpenLiquidateDialog, shoundOpenPrintSingleItem, itemPrint } = this.state;
    let org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
    const newDate = new Date(item?.inventoryCountDate)
    const day = item?.inventoryCountDate ? String(newDate?.getDate())?.padStart(2, '0') : ".....";
    const month = item?.inventoryCountDate ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : ".....";
    const year = item?.inventoryCountDate ? String(newDate?.getFullYear()) : "......";
    return (
      <Dialog className="w-90 m-auto" open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth scroll="paper" >
        <div className={clsx("wrapperButton", !loading && 'hidden')} >
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm ref="form" onSubmit={shouldOpenLiquidateDialog ? () => { } : this.handleFormSubmit}
          class="validator-form-scroll-dialog">
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            <h4 className="">
              {item?.id ? t('InventoryCountVoucher.updateDialog') : t('InventoryCountVoucher.addDialog')}
            </h4>
          </DialogTitle>
          <DialogContent >
            <Grid>
              <InventoryCountScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
                handleSelectAssetPopupOpen={this.handleSelectAssetPopupOpen}
                handleSelectAssetAdded={this.handleSelectAssetAdded}
                handleDeleteItem={this.handleDeleteItem}
                handleChange={this.handleChange}
                selectInventoryCount={this.selectInventoryCount}
                handleSelectDepartment={this.handleSelectDepartment}
                handlePopupClose={this.handlePopupClose}
                selectStatus={this.selectStatus}
                selectStores={this.selectStores}
                handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
                openSelectPersonPopup={this.openSelectPersonPopup}
                handleSelectPerson={this.handleSelectPerson}
                removePersonInlist={this.removePersonInlist}
                handleRowDataCellChange={this.handleRowDataCellChange}
                handleDateChange={this.handleDateChange}
                itemAssetDocument={this.state.item}
                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
                handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                updatePageAssetDocument={this.updatePageAssetDocument}
                getAssetDocumentId={this.getAssetDocumentId}
                assetReevaluate={this.state.assetReevaluate}
                handleSetAssetReevaluate={this.handleSetAssetReevaluate}
                selectedValue={this.state.selectedValue}
                setSelectedValue={this.setSelectedValue}
                dataAssetTable={this.state.dataAssetTable}
                isEnabledBtn={this.state.isEnabledBtn}
                handleSetListInventoryPerson={this.handleSetListInventoryPerson}
                handleSetHoiDongId={this.handleSetHoiDongId}
                handlePrintSingleItem={this.handlePrintSingleItem}
              />
            </Grid>

            {shouldOpenLiquidateDialog && <>
              <AssetTransferDialog
                t={t}
                i18n={i18n}
                item={this.state}
                open={shouldOpenLiquidateDialog}
                handleClose={this.handleDialogClose}
                handleOKEditClose={this.props.handleOKEditClose}
                disabledDgl={true}
                isInventoryLiquidateAsset={true}
              />
            </>}

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={handleOKEditClose}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                disabled={!this.state.isEnabledBtn}
                className="mr-12"
                onClick={() => this.handleOpenAssetReevaluateDialog()}
              >
                {t('Dashboard.subcategory.reEvaluate')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-12"
                disabled={!this.state.isEnabledBtn}
                onClick={this.handleOpenLiquidateDialog}
              >
                {t('Asset.asset_liquidate')}
              </Button>
              {!item?.isView && <>
                {!this.state.isLiquidate && <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  onClick={(e) => {
                    if (!this.validateSubmit()) {
                      e.preventDefault();
                    }
                  }}>
                  {t('general.save')}
                </Button>}
              </>}
            </div>
          </DialogActions>
        </ValidatorForm>
        {
          this.state.shouldOpenReevaluateDialog &&
          <AssetReevaluateDialog
            t={t}
            handleClose={() => this.handleCloseAssetReevaluateDialog()}
            open={this.state.shouldOpenReevaluateDialog}
            handleOKEditClose={this.handleOKEditClose}
            item={this.state.assetReevaluate}
            type="hasSelectedAsset"
            setSelectedValue={this.setSelectedValue}
            getDsPhieuDanhGiaOfVoucher={this.props?.getDsPhieuDanhGiaOfVoucher}
            assetVoucherIds={this.props?.assetVoucherIds}
            updateData={this.props?.updateData}
          />
        }
        {shoundOpenPrintSingleItem && <>
          <PrintMultipleFormDialog
            t={t}
            i18n={i18n}
            handleClose={() => this.handleClosePrintSingleItem()}
            open={shoundOpenPrintSingleItem}
            item={{ ...itemPrint, org, day, month, year }}
            title={t("InventoryCountVoucher.printTitle")}
            urls={[
              {
                // tabLabel: "Mẫu đánh giá trình trạng",
                url: "/assets/form-print/danhGiaTinhTrangThietBi_bvHoeNhai.txt",
                config: {
                  layout: "landscape",
                  margin: "horizontal",
                },
              },
            ]}
          />
        </>}
      </Dialog>
    );
  }
}
InventoryCountVoucherDialog.contextType = AppContext;
export default InventoryCountVoucherDialog;
