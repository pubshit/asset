import React, { useContext, useEffect, useRef, useState } from "react";
import {
    Button,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination,
    Collapse,
    Card,
    CardContent,
    ButtonGroup,
    Popper,
    Paper,
    ClickAwayListener,
    MenuList,
    MenuItem
} from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ConfirmationDialog } from "egret";
import viLocale from "date-fns/locale/vi";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import {STATUS_DEPARTMENT, appConst, OPTIONS_PRINT_ASSET, PRINT_TEMPLATE_MODEL, LIST_ORGANIZATION} from "app/appConst";
import InventoryCountVoucherDialog from "./InventoryCountVoucherDialog";
import InventoryAllDepartmentsDialog from "../InventoryCountVoucherDepartment/InventoryAllDepartmentsDialog";
import AssetsQRPrintNew from "../Asset/ComponentPopups/AssetsQRPrintNew";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { getRole } from "app/appFunction";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { ValidatorForm } from "react-material-ui-form-validator";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { searchByPage } from "../Department/DepartmentService";
import InventoryCountVoucherAll from "../FormCustom/InventoryCountVoucherAll";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { getInventoryCountVoucherPrintData } from "../FormCustom/InventoryCountVoucher";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import CustomTablePagination from "../CustomTablePagination";

function ComponentInventoryTable(props) {
    const { currentOrg } = useContext(AppContext);
    const { INVENTORY_COUNT } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    let isButtonAdd = props?.item?.inventoryCountStatusIndexOrder === null;
    let {
        selectedList = [],
        openAdvanceSearch,
        selectedItem
    } = props?.item;
  let { openPrintQR, products, openExcel, item } = props?.item;
    const { isRoleAdmin, isRoleSuperAdmin, isRoleOrgAdmin, isRoleAccountant } = getRole();
    let isShowBtnInventoryCount = isRoleAdmin || isRoleSuperAdmin || isRoleOrgAdmin || isRoleAccountant;
    const [listData, setListData] = useState([])
    const [anchorRef, setAnchorRef] = useState(null);
    const t = props.t;
    const i18n = props.i18n;
    const isMongCai = currentOrg?.code === LIST_ORGANIZATION.TTYTTP_MONG_CAI.code;
    
    let anchorRefF = useRef();
    let dataView = getInventoryCountVoucherPrintData(props.item, appConst.assetClass.TSCD);

    useEffect(() => {
        setAnchorRef(anchorRefF)
    }, [])
    return (
        <div>
            <Grid container spacing={2} className="alignItemsFlexEnd spaceBetween mt-10">
                <Grid item md={8} sm={12} xs={12}>
                    {props.item?.hasCreatePermission && isButtonAdd && (
                        <Button
                            // size="small"
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleEditItem({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isView: false
                                });
                            }}
                        >
                            {t("InventoryCountVoucher.add")}
                        </Button>
                    )}
                    {isShowBtnInventoryCount && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.handleEditItemInventoryAllDepartment}
                            disabled={selectedList?.length <= 0}
                        >
                            {t("InventoryCountVoucherDepartment.titleBtn")}
                        </Button>
                    )}
                    <Button
                        className="align-bottom mr-16"
                        variant="contained"
                        color="primary"
                        onClick={props.exportToExcel}
                    >
                        {t("general.exportToExcel")}
                    </Button>
                    <Button
                        className="align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>

                </Grid>
                <Grid item md={4} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("InventoryCountVoucher.enterSearch")}
                            id="search_box"
                            value={props.item?.keyword}
                            startAdornment={
                                <InputAdornment position="end">
                                    <SearchIcon
                                        onClick={() => props?.search(props.item?.keyword)}
                                    />
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>

                {/* Bộ lọc Tìm kiếm nâng cao */}
                <Grid item xs={12}>
                    <Collapse in={openAdvanceSearch}>
                        <ValidatorForm>
                            <Card elevation={2}>
                                <CardContent>
                                    <Grid container xs={12} spacing={2}>
                                        <Grid item md={3} sm={6} xs={12} className="pr-16" >
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("InventoryCountVoucher.inventoryFromDate")}
                                                    type="text"
                                                    autoOk={false}
                                                    format="dd/MM/yyyy"
                                                    name={"inventoryCountDateBottom"}
                                                    value={props.item?.inventoryCountDateBottom}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    minDateMessage={t("general.minDateDefault")}
                                                    maxDateMessage={props.item?.inventoryCountDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                    maxDate={props.item?.inventoryCountDateTop ? (new Date(props.item?.inventoryCountDateTop)) : undefined}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "inventoryCountDateBottom")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item md={3} sm={6} xs={12} className="pr-16">
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    className="w-100"
                                                    id="mui-pickers-date"
                                                    label={t("InventoryCountVoucher.inventoryToDate")}
                                                    type="text"
                                                    autoOk={false}
                                                    format="dd/MM/yyyy"
                                                    name={"inventoryCountDateTop"}
                                                    value={props.item?.inventoryCountDateTop}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    minDateMessage={props.item?.inventoryCountDateBottom ? t("general.minDateToDate") : t("general.minDateDefault")}
                                                    maxDateMessage={t("general.maxDateDefault")}
                                                    minDate={props.item?.inventoryCountDateBottom ? (new Date(props.item?.inventoryCountDateBottom)) : undefined}
                                                    clearable
                                                    onChange={(date) => props.handleDateChange(date, "inventoryCountDateTop")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteTransfer
                                                label={t("InventoryCountVoucher.department")}
                                                searchFunction={searchByPage}
                                                searchObject={{
                                                    pageIndex: 1, pageSize: 100000, isActive: STATUS_DEPARTMENT.HOAT_DONG.code
                                                }}
                                                listData={listData}
                                                typeReturnFunction="category"
                                                setListData={setListData}
                                                displayLable={"text"}
                                                value={props.item?.department ? props.item?.department : null}
                                                onSelect={(data) => props?.handleChangeDepartment(data, "department")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        <Grid item md={3} sm={6} xs={12}>
                                            <ButtonGroup
                                                className="align-bottom mr-16 mt-16"
                                                ref={anchorRef}
                                                variant="contained"
                                                aria-label="split button"
                                            >
                                                <Button
                                                    aria-controls={openExcel ? "split-button-menu" : undefined}
                                                    aria-expanded={openExcel ? "true" : undefined}
                                                    aria-label="select merge strategy"
                                                    aria-haspopup="menu"
                                                    variant="contained"
                                                    color="primary"
                                                    style={{
                                                        minWidth: 105
                                                    }}
                                                    onClick={(e) => props?.handleToggleOpenExcel(e)}
                                                >
                                                    {t("general.print")}
                                                </Button>
                                            </ButtonGroup>
                                            <Popper
                                                open={openExcel}
                                                anchorEl={anchorRef?.current}
                                                role={undefined}
                                                transition
                                                disablePortal
                                                style={{ zIndex: 999 }}
                                            >
                                                <Paper>
                                                    <ClickAwayListener onClickAway={() => { }}>
                                                        <MenuList id="split-button-menu" autoFocusItem>
                                                            {OPTIONS_PRINT_ASSET.map((option, index) => (
                                                                <MenuItem
                                                                    key={option}
                                                                    onClick={(event) =>
                                                                        props.exportToPrint(event, option.code)
                                                                    }
                                                                >
                                                                    {option.name}
                                                                </MenuItem>
                                                            ))}
                                                        </MenuList>
                                                    </ClickAwayListener>
                                                </Paper>
                                            </Popper>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </ValidatorForm>
                    </Collapse>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {props.item?.shouldOpenPrintDialog && (
                          isMongCai ? (
                            <PrintPreviewTemplateDialog
                              t={t}
                              handleClose={props.handleDialogClose}
                              open={props.item?.shouldOpenPrintDialog}
                              item={props.item?.item}
                              title={t("InventoryCountVoucher.printTitle")}
                              model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.INVENTORY_COUNT_VOUCHER}
                            />
                          ) : (
                            <PrintMultipleFormDialog
                              t={t}
                              i18n={props.item?.i18n}
                              handleClose={props.handleDialogClose}
                              open={props.item?.shouldOpenPrintDialog}
                              item={dataView || props.item?.item}
                              title={t("InventoryCountVoucher.printTitle")}
                              urls={[
                                ...INVENTORY_COUNT.GENERAL,
                                ...(INVENTORY_COUNT[currentOrg?.printCode] || []),
                              ]}
                            />
                          )
                        )}

                        {props.item?.shouldOpenEditorDialog && (
                            <InventoryCountVoucherDialog
                                t={t}
                                i18n={props.item?.i18n}
                                handleClose={props.handleDialogClose}
                                open={props.item?.shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={props.item?.item}
                                getDsPhieuDanhGiaOfVoucher={props?.getDsPhieuDanhGiaOfVoucher}
                                assetVoucherIds={props?.assetVoucherIds}
                                updateData={props?.updateData}
                                updatePageData={props?.updatePageData}
                            />
                        )}
                        {props.item?.shouldOpenInventoryAllDepartmentsDialog && (
                            <InventoryAllDepartmentsDialog
                                t={t}
                                i18n={props.item?.i18n}
                                handleClose={props.handleDialogClose}
                                open={props.item?.shouldOpenInventoryAllDepartmentsDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={props.item}
                                getDsPhieuDanhGiaOfVoucher={props?.getDsPhieuDanhGiaOfVoucher}
                                assetVoucherIds={props?.assetVoucherIds}
                                updateData={props?.updateData}
                                updatePageData={props?.updatePageData}
                            />
                        )}

                        {props.item?.shouldOpenPrintDialogAll && (
                            <InventoryCountVoucherAll
                                t={t}
                                i18n={props.item?.i18n}
                                assetClass={appConst.assetClass.TSCD}
                                handleClose={props.handleDialogClose}
                                open={props.item?.shouldOpenPrintDialogAll}
                                item={props.item?.item}
                            />
                        )}

                        {props.item?.shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={props.item?.shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {openPrintQR && (
                            <AssetsQRPrintNew
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                    </div>
                    <MaterialTable
                        title={t("general.list")}
                        data={props.item?.itemList}
                        columns={props.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            draggable: false,
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: selectedItem?.id === rowData?.id
                                    ? "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "270px",
                            minBodyHeight: "270px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                paddingLeft: 10,
                                paddingRight: 10,
                                textAlign: "center",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            let listIds = []
                            // eslint-disable-next-line no-unused-expressions
                            rows?.forEach(item => listIds.push(item.id))
                            props.setState({ ids: [...listIds] })
                        }}
                        onRowClick={(e, rowData) => {
                            return props?.setItemState(rowData);
                        }}
                    />
                    <CustomTablePagination
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        count={props.item?.totalElements}
                        rowsPerPage={props.item?.rowsPerPage}
                        labelDisplayedRows={({ from, to, count }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                            }`
                        }
                        page={props.item?.page}
                        onChangePage={props.handleChangePage}
                        onChangeRowsPerPage={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

export default ComponentInventoryTable;
