import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Grid,
  Icon,
  IconButton,
  Radio,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { createFilterOptions } from "@material-ui/lab/useAutocomplete";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { appConst, STATUS_STORE } from "app/appConst";
import { LightTooltip, TabPanel, a11yProps, convertNumberPrice, filterOptions } from "app/appFunction";
import clsx from "clsx";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { TextValidator } from "react-material-ui-form-validator";
import { getAllAssetStatuss as searchAssetStatus } from "../AssetStatus/AssetStatusService";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { getAllInventoryCountStatus } from "../InventoryCountStatus/InventoryCountStatusService";
import { searchByPage as storesSearchByPage } from "../Store/StoreService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchByPage } from "../Council/CouncilService";
import { NumberFormatCustom } from "../Component/Utilities";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import { toast } from "react-toastify";
import { correctPrecision } from "../../appFunction";

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    zIndex: 1,
    background: "#358600",
    position: "sticky",
    top: 0,
    left: 0,
    "& .MuiTableCell-head": {
      padding: "4px" // <-- arbitrary value
    }
  },
  borderRight: {
    borderRight: "1px solid white !important",
    color: "#ffffff",
    textAlign: "center"
  },
  colorWhite: {
    color: "#ffffff",
    textAlign: "center"
  },
  mw_100: {
    minWidth: "90px"
  },
  mw_70: {
    minWidth: "70px"
  },
  tabPanel: {
    "& .MuiBox-root": {
      paddingLeft: "0",
      paddingRight: "0"
    }
  },
  textRight: {
    "& input": {
      textAlign: "right"
    }
  },
  textCenter: {
    "& input": {
      textAlign: "center"
    }
  }
}));

export default function ProductScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [checkTab, setCheckTab] = useState({
    isTabInfo: true,
    isTabCouncil: false,
    isTabNotRecorded: false,
  });
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [listAssetStatus, setListAssetStatus] = useState([]);
  const [listStatus, setListStatus] = useState([]);
  const [listInventoryCountBoard, setListInventoryCountBoard] = useState([]);
  const [listInventoryPerson, setListInventoryPerson] = useState(props?.item?.inventoryCountPersons || []);
  const [inventoryBoard, setInventoryBoard] = useState([])

  const searchObjectStore = {
    pageIndex: 0,
    pageSize: 1000000,
    isActive: STATUS_STORE.HOAT_DONG.code
  }; // kho tài sản
  const filterAutocomplete = createFilterOptions();

  const handleClick = (event, item) => {
    if (props?.setSelectedValue) {
      if (item?.id) {
        props.setSelectedValue(item?.id);
        props.handleSetAssetReevaluate(item)
      }
    }
    else {
      props.setSelectedValue(null);
      props.handleSetAssetReevaluate({})
    }
    return;
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
    setCheckTab({
      isTabInfo: newValue === appConst.tabDialogInventory.tabInfo,
      isTabCouncil: newValue === appConst.tabDialogInventory.tabCouncil,
      isTabNotRecorded: newValue === appConst.tabDialogInventory.tabNotRecorded,
    })
  };
  let searchObject = { pageIndex: 0, pageSize: 1000000, type: appConst.OBJECT_HD_TYPE.KIEM_KE.code, };

  let columns = props?.item?.id && props?.isEnabledBtn ?
    [
      {
        title: t("general.reevaluateAcronyms"),
        field: "custom",
        align: "left",
        minWidth: "45px",
        hidden: checkTab?.isTabNotRecorded,
        cellStyle: {
          padding: "0px",
          paddingLeft: "10px",
        },
        render: (rowData) => {
          return rowData.isAllowedReevaluate && <Radio
            id={`radio${rowData.id}`}
            className="p-0"
            name="radSelected"
            value={rowData.asset.id}
            checked={props?.selectedValue === rowData.asset.id}
            onClick={(event) => handleClick(event, rowData?.asset)}
          />
        }
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 60,
        maxWidth: 150,
        hidden: checkTab?.isTabNotRecorded,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (<LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton size="small" onClick={() => props?.handlePrintSingleItem(rowData)}>
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>)
      },
      {
        title: t("Asset.stt"),
        field: "",
        maxWidth: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        align: "left",
        minWidth: 200,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.seri"),
        field: "asset.serialNumber",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.model"),
        field: "asset.model",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.currentStatus"),
        field: "currentStatus",
        minWidth: "140px",
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
      },
      {
        title: t("InventoryCountStatus.CountAsset.status"),
        minWidth: 180,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => (
          <AsynchronousAutocompleteSub
            listData={listAssetStatus}
            setListData={setListAssetStatus}
            searchFunction={searchAssetStatus}
            searchObject={searchObject}
            defaultValue={rowData.status}
            displayLable={"name"}
            value={rowData.status}
            readOnly={props?.item?.isView || props?.item?.isLiquidate}
            onSelect={(value) => props.selectStatus(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
          />
        ),
      },
      {
        title: t("Asset.statusDgl"),
        field: "asset.dglTrangthai",
        minWidth: "120px",
        hidden: checkTab?.isTabNotRecorded,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => {
          return appConst.statusAssetReevaluate.find(item => item.id === rowData.dglTrangthai)?.name
        }
      },
      {
        title: t("Asset.receive_date"),
        field: "asset.dateOfReception",
        minWidth: 120,
        maxWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.asset?.dateOfReception ? (
            <span>
              {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryCountVoucher.store"),
        field: "asset.store",
        minWidth: "110px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => checkTab?.isTabNotRecorded ? rowData?.store?.name : rowData?.isStore && (
          <AsynchronousAutocompleteSub
            searchFunction={storesSearchByPage}
            searchObject={searchObjectStore}
            defaultValue={rowData?.store}
            displayLable={"name"}
            readOnly={props.item?.isView}
            value={rowData?.store}
            onSelect={(value) => props.selectStores(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />
        )
      },
      {
        title: t("Asset.currentUseByDepartment"),
        field: "asset.departmentName",
        align: "left",
        hidden: !checkTab?.isTabNotRecorded,
        minWidth: 160,
        render: (rowData) => rowData?.departmentName
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.accountantQuantity",
        hidden: checkTab?.isTabNotRecorded,
        minWidth: "100px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => rowData?.accountantQuantity ? rowData.accountantQuantity : 1,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice((rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.accountantCarryingAmount",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice((rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.inventoryQuantity",
        minWidth: "100px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => rowData?.asset?.inventoryQuantity ? rowData?.asset?.inventoryQuantity : 1,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.inventoryOriginalCost",
        minWidth: "100px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            name="inventoryOriginalCost"
            defaultValue={rowData?.inventoryOriginalCost}
            value={rowData?.inventoryOriginalCost}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.inventoryCarryingAmount",
        minWidth: "100px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            name="inventoryCarryingAmount"
            defaultValue={rowData?.inventoryCarryingAmount}
            value={rowData?.inventoryCarryingAmount}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => (rowData?.asset?.accountantQuantity - rowData?.asset?.inventoryQuantity) || 0,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceOriginalCost"
            defaultValue={correctPrecision(rowData?.differenceOriginalCost)}
            value={correctPrecision(rowData?.differenceOriginalCost) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.accountantCarryingAmount",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceCarryingAmount"
            defaultValue={correctPrecision(rowData?.differenceCarryingAmount)}
            value={correctPrecision(rowData?.differenceCarryingAmount) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.note"),
        field: "custom",
        minWidth: "150px",
        headerStyle: {
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: 10,
          paddingRight: "10px",
          textAlign: "left",
        },
        render: (rowData) => (
          <TextValidator
            className="w-40"
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            type="text"
            name="note"
            value={rowData.note}
            InputProps={{
              readOnly: props?.item?.isView
            }}
          />
        ),
      },
    ] :
    ///////////////////////////////////////////////////
    ///when edit
    ///////////////////////////////////////////////////
    [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 60,
        maxWidth: 150,
        hidden: props?.item?.isView && checkTab?.isTabNotRecorded,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <>
            {
              !checkTab?.isTabNotRecorded
                ?
                <LightTooltip
                  title={t("general.print")}
                  placement="right-end"
                  enterDelay={300}
                  leaveDelay={200}
                  PopperProps={{
                    popperOptions: {
                      modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                    },
                  }}
                >
                  <IconButton size="small" onClick={() => props?.handlePrintSingleItem(rowData)}>
                    <Icon fontSize="small" color="inherit">
                      print
                    </Icon>
                  </IconButton>
                </LightTooltip>
                :
                <LightTooltip
                  title={t("general.print")}
                  placement="right-end"
                  enterDelay={300}
                  leaveDelay={200}
                  PopperProps={{
                    popperOptions: {
                      modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                    },
                  }}
                >
                  <IconButton size="small" onClick={() => props?.handleDeleteItem(rowData)}>
                    <Icon fontSize="small" color="error">
                      delete
                    </Icon>
                  </IconButton>
                </LightTooltip>
            }
          </>
        )
      },
      {
        title: t("Asset.stt"),
        field: "",
        maxWidth: "50px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.oldCode"),
        field: "asset.managementCode",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "left",
        },
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        align: "left",
        minWidth: 200,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.seri"),
        field: "asset.serialNumber",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.model"),
        field: "asset.model",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("InventoryCountStatus.CountAsset.currentStatus"),
        field: "currentStatus",
        minWidth: 180,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "left",
        },
        render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
      },
      {
        title: t("InventoryCountStatus.CountAsset.status"),
        minWidth: 180,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "left",
        },
        render: (rowData) => (
          <AsynchronousAutocompleteSub
            listData={listAssetStatus}
            setListData={setListAssetStatus}
            searchFunction={searchAssetStatus}
            searchObject={searchObject}
            displayLable={"name"}
            readOnly={props?.item?.isView || props?.item?.isLiquidate}
            value={rowData.status || ""}
            onSelect={(value) => props.selectStatus(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />
        ),
      },
      {
        title: t("Asset.receive_date"),
        field: "asset.dateOfReception",
        minWidth: 120,
        maxWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.asset?.dateOfReception ? (
            <span>
              {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryCountVoucher.store"),
        field: "asset.store",
        minWidth: 180,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "left",
        },
        render: (rowData) => checkTab?.isTabNotRecorded ? rowData?.store?.name : rowData?.isStore && (
          <AsynchronousAutocompleteSub
            searchFunction={storesSearchByPage}
            searchObject={searchObjectStore}
            defaultValue={rowData?.store}
            displayLable={"name"}
            value={rowData?.store}
            readOnly={props?.item?.isView || checkTab?.isTabNotRecorded}
            onSelect={(value) => props.selectStores(value, rowData, checkTab?.isTabNotRecorded)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={filterOptions}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />
        )
      },
      {
        title: t("Asset.currentUseByDepartment"),
        field: "asset.departmentName",
        align: "left",
        hidden: !checkTab?.isTabNotRecorded,
        minWidth: 160,
        render: (rowData) => rowData?.departmentName
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => 1,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice((rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.accountantCarryingAmount",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice((rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "inventoryQuantity",
        minWidth: "100px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textCenter)}
            name="inventoryQuantity"
            type="number"
            value={rowData?.inventoryQuantity}
            onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
            InputProps={{
              readOnly: props?.item?.isView || checkTab?.isTabNotRecorded,
            }}
            validators={["required", "minNumber:0", "maxNumber:1"]}
            errorMessages={[t("general.required"), "Số không được âm", "Số không được lớn hơn 1"]}
          />
        )
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.inventoryOriginalCost",
        minWidth: "100px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
            name="inventoryOriginalCost"
            defaultValue={rowData?.inventoryOriginalCost}
            value={rowData?.inventoryOriginalCost}
            id="formatted-numberformat-originalCost"
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.inventoryCarryingAmount",
        minWidth: "100px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
            name="inventoryCarryingAmount"
            defaultValue={rowData?.inventoryCarryingAmount}
            value={rowData?.inventoryCarryingAmount}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.differenceQuantity",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "center",
        },
        render: (rowData) => rowData?.differenceQuantity ? rowData.differenceQuantity : 0,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceOriginalCost"
            defaultValue={correctPrecision(rowData?.differenceOriginalCost)}
            value={correctPrecision(rowData?.differenceOriginalCost) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.accountantCarryingAmount",
        minWidth: "100px",
        hidden: checkTab?.isTabNotRecorded,
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: "5px",
          paddingRight: "5px",
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceCarryingAmount"
            defaultValue={correctPrecision(rowData?.differenceCarryingAmount)}
            value={correctPrecision(rowData?.differenceCarryingAmount) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.note"),
        field: "custom",
        minWidth: "150px",
        headerStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        cellStyle: {
          paddingLeft: 10,
          paddingRight: "10px",
          textAlign: "left",
        },
        render: (rowData) => (
          <TextValidator
            className="w-40"
            onChange={(event) => props.handleRowDataCellChange(rowData, event, checkTab?.isTabNotRecorded)}
            type="text"
            name="note"
            value={rowData.note}
            InputProps={{
              readOnly: props?.item?.isView
            }}
          />
        ),
      },
    ];

  let columnPerson = [
    {
      title: t("Asset.stt"),
      field: "",
      width: "50px",
      headerStyle: {
      },
      cellStyle: {
        paddingLeft: "12px",
        paddingRight: 10,
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("InventoryCountVoucher.inventoryCountPerson"),
      field: "personDisplayName",
      width: "250px",
      headerStyle: {
      },
    },
    {
      title: t("Asset.department"),
      field: "departmentView",
      width: "150",
      headerStyle: {
      },
    },
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      width: "120px",
      render: (rowData) => (
        !props?.item?.isView && <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
            } else if (method === 1) {
              props.removePersonInlist(rowData.person.id);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
  ];

  let columnsVoucherFile = [
    {
      title: t("general.stt"),
      field: "code",
      width: "50px",
      align: "center",
      headerStyle: {
        paddingLeft: 10,
        paddingRight: "10px",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      width: "200px",
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      width: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      width: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      width: "150px",
      render: (rowData) => (
        <div className="none_wrap">
          <LightTooltip
            title={t("general.editIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
  ];

  const columnsInventoryCount = [
    {
      title: t("InventoryCountVoucher.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 80,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => handleRowDataCellDelete(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      }
    ] : []),
    {
      title: t("InventoryCountVoucher.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    }
  ]
  const handleChangeSelectInventoryBoard = (item) => {
    setInventoryBoard(item);
    props.handleSetHoiDongId(item?.id)
    props.handleSetListInventoryPerson(item?.hdChiTiets)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setListInventoryPerson(item?.hdChiTiets || []);
  };
  const handleRowDataCellDelete = (item) => {
    let newDataRow = listInventoryPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListInventoryPerson(newDataRow)
    setListInventoryPerson(newDataRow || []);
  };
  const handleSelectHandoverDepartment = (item, rowData) => {
    let newDataRow = listInventoryPerson.map((i) => {
      if (i.tableData.id === rowData.tableData.id) {
        i.handoverDepartment = item;
        i.departmentId = item?.id;
        i.departmentName = item?.name
      }
      return i;
    });

    setListInventoryPerson(newDataRow || []);
  };
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab value={0} label={t("InventoryCountVoucher.tabInfo")} {...a11yProps(0)} />
          <Tab value={1} label={t("InventoryCountVoucher.unrecordedAssets")} {...a11yProps(1)} />
          <Tab label={t("InventoryCountVoucher.inventoryCouncil")} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel className={classes.tabPanel} value={value} indexes={[appConst.tabDialogInventory.tabInfo, appConst.tabDialogInventory.tabNotRecorded]} >
        <Grid className="" container spacing={1}>
          <Grid item md={2} sm={12} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog mt-2"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("InventoryCountVoucher.inventoryCountDate")}
                  </span>
                }
                name={'inventoryCountDate'}
                inputVariant="standard"
                autoOk={true}
                format="dd/MM/yyyy"
                value={props?.item?.inventoryCountDate}
                onChange={(date) => props.handleDateChange(date, "inventoryCountDate")}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                open={isPickerOpen}
                onOpen={() => setIsPickerOpen(!props?.item?.isView)}
                onClose={() => setIsPickerOpen(false)}
                validators={props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventory[0].indexOrder ? ['required'] : []}
                errorMessages={props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventory[0].indexOrder ? t('general.required') : []}
                maxDate={new Date()}
                maxDateMessage={t("InventoryCountVoucher.noti_maxDate")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              label={
                <span>
                  <span className="colorRed"> * </span>
                  {t("InventoryCountVoucher.voucherName")}
                </span>
              }
              className="w-100 mr-16"
              name="title"
              value={props.item.title}
              onChange={props.handleChange}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: props?.item?.isView
              }}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12} >
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed"> * </span>
                  {t("InventoryCountVoucher.status")}
                </span>
              }
              listData={listStatus}
              setListData={setListStatus}
              searchFunction={getAllInventoryCountStatus}
              searchObject={searchObject}
              displayLable={"name"}
              readOnly={props?.item?.isView}
              value={props.item.inventoryCountStatus || null}
              onSelect={props.selectInventoryCount}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid container item md={4} sm={12} xs={12} justifyContent="space-between" spacing={1}>
            <Grid item md={!checkTab?.isTabNotRecorded ? 9 : 12} sm={!checkTab?.isTabNotRecorded ? 9 : 12} xs={!checkTab?.isTabNotRecorded ? 9 : 12}>
              <TextValidator
                className="w-100"
                InputProps={{
                  readOnly: true,
                }}
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("InventoryCountVoucher.department")}
                  </span>
                }
                value={
                  props.item.department ? props.item.department.name : ""
                }
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </Grid>
            <Grid item md={3} sm={3} xs={3}>
              {!checkTab?.isTabNotRecorded && <Button
                size="small"
                className="w-100 mt-15"
                variant="contained"
                color="primary"
                disabled={props.item.id || props?.item?.isView}
                onClick={props.handleDepartmentPopupOpen}
              >
                {t("general.select")}
              </Button>}
            </Grid>
            {props.item.shouldOpenDepartmentPopup && (
              <SelectDepartmentPopup
                open={props.item.shouldOpenDepartmentPopup}
                handleSelect={props.handleSelectDepartment}
                selectedItem={
                  props.item.department != null ? props.item.department : null
                }
                handleClose={props.handlePopupClose}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            {(checkTab?.isTabNotRecorded && !props?.item?.isView) && <Button
              size="small"
              variant="contained"
              color="primary"
              disabled={!checkTab?.isTabNotRecorded}
              onClick={props.handleSelectAssetPopupOpen}
            >
              {t("general.add_asset")}
            </Button>}
            {props.item.shouldOpenSelectAssetPopup && <SelectAssetAllPopup
              t={t}
              i18n={i18n}
              open={props.item.shouldOpenSelectAssetPopup}
              handleSelect={props.handleSelectAssetAdded}
              assetVouchers={props?.item?.listAssetsAdded ? props?.item?.listAssetsAdded : []}
              handleClose={props.handlePopupClose}
              isNotUseDepartment={props?.item?.department?.id}
              dateOfReceptionTop={props?.item?.inventoryCountDate}
            />}
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} className="mt-12">
            <MaterialTable
              data={checkTab?.isTabNotRecorded ? props?.item?.listAssetsAdded : props?.dataAssetTable}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: true,
                search: false,
                maxBodyHeight: "500px",
                minBodyHeight: "273px",
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  position: "relative"
                },
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: {
                  labelDisplayedRows: "{from}-{to} trong {count}",
                  labelRowsPerPage: "Số bản ghi mỗi trang:",
                  firstTooltip: "Trang đầu",
                  previousTooltip: "Trang trước",
                  nextTooltip: "Trang tiếp",
                  lastTooltip: "Trang cuối",
                  labelRowsSelect: "hàng mỗi trang",
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),

                Header: () => {
                  return (
                    <TableHead className={clsx(classes.header)}>
                      <TableRow>
                        {props?.item?.id && props?.isEnabledBtn && !checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("general.reevaluateAcronyms")}</TableCell>}
                        {!(props?.item?.isView && checkTab?.isTabNotRecorded) && <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("general.action")}</TableCell>}
                        <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("Asset.stt")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)} rowSpan={2} >{t("Asset.managementCode")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.code")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.name")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.seri")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.model")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                        {props?.item?.id && props?.isEnabledBtn && !checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.statusDgl")}</TableCell>}
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.receive_date")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.store")}</TableCell>
                        {checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.currentUseByDepartment")}</TableCell>}
                        {!checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>}
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                        {!checkTab?.isTabNotRecorded && <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.deviant")}</TableCell>}
                        <TableCell className={clsx(classes.colorWhite, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.note")}</TableCell>
                      </TableRow>
                      <TableRow>
                        {!checkTab?.isTabNotRecorded &&
                          <>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                            <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>

                          </>
                        }
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                      </TableRow>
                    </TableHead>
                  );
                },
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={appConst.tabDialogInventory.tabCouncil}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction='category'
            searchObject={searchObject}
            label={t('InventoryCountVoucher.InventoryCountBoard')}
            listData={listInventoryCountBoard}
            setListData={setListInventoryCountBoard}
            displayLable={"name"}
            onSelect={handleChangeSelectInventoryBoard}
            value={inventoryBoard}
            InputProps={{
              readOnly: true,
            }}
            disabled={
              props?.item?.isView
              || (
                props?.item?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder
                && props?.item?.id
              )
            }
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              let filtered = filterAutocomplete(options, params);
              return filtered;
            }}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={listInventoryPerson}
            columns={columnsInventoryCount}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },

              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      {/* <TabPanel value={value} index={2}>
        <Grid item md={12} sm={12} xs={12}>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={props.handleAddAssetDocumentItem}
          >
            {t("AssetFile.addAssetFile")}
          </Button>
          {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              updatePageAssetDocument={props.updatePageAssetDocument}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocumentId={props.getAssetDocumentId}
              item={props.item}
              t={t}
              i18n={i18n}
            />
          )}
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item.assetDocumentList}
            columns={columnsVoucherFile}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
          />
        </Grid>
      </TabPanel> */}
    </div>
  );
}
