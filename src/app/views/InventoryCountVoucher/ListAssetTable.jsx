import React from "react";
import { TableCell, TableHead, TableRow, makeStyles } from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import clsx from "clsx";
import { convertNumberPrice } from "app/appFunction";
import moment from "moment";
import { appConst } from "app/appConst";

function ListAssetTable(props) {
    const { t } = props
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            width: "100%",
            backgroundColor: theme.palette.background.paper,
        },
        header: {
            zIndex: 1,
            background: "#358600",
            position: "sticky",
            top: 0,
            left: 0,
            "& .MuiTableCell-head": {
                padding: "4px" // <-- arbitrary value
            }
        },
        borderRight: {
            borderRight: "1px solid white !important",
            color: "#ffffff",
            textAlign: "center"
        },
        colorWhite: {
            color: "#ffffff",
            textAlign: "center"
        },
        mw_100: {
            minWidth: "90px"
        },
        mw_70: {
            minWidth: "70px"
        },
        tabPanel: {
            "& .MuiBox-root": {
                paddingLeft: "0",
                paddingRight: "0"
            }
        },
        textRight: {
            "& input": {
                textAlign: "right"
            }
        }
    }));
    const classes = useStyles();
    let columns = [
        {
            title: t("Asset.stt"),
            field: "",
            maxWidth: "50px",
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) =>
                (rowData.tableData.id + 1),
        },
        {
            title: t("Asset.managementCode"),
            field: "asset.managementCode",
            minWidth: "100px",
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.code"),
            field: "asset.code",
            align: "left",
            minWidth: "100px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("Asset.name"),
            field: "asset.name",
            align: "left",
            minWidth: 200,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.seri"),
            field: "asset.serialNumber",
            align: "left",
            minWidth: 100,
            cellStyle: {
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.model"),
            field: "asset.model",
            align: "left",
            minWidth: 100,
            cellStyle: {
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.currentStatus"),
            field: "currentStatus.name",
            minWidth: "140px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.status"),
            field: "status.name",
            minWidth: 180,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.statusDgl"),
            field: "asset.dglTrangthai",
            minWidth: "160px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
            render: (rowData) => {
                return appConst.statusAssetReevaluate.find(item => item.id === rowData.dglTrangthai)?.name
            }
        },
        {
            title: t("Asset.receive_date"),
            field: "asset.dateOfReception",
            minWidth: 120,
            maxWidth: 120,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) =>
                rowData?.asset?.dateOfReception ? (
                    <span>
                        {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
                    </span>
                ) : (
                    ""
                ),
        },
        {
            title: t("InventoryCountVoucher.store"),
            field: "asset.store",
            minWidth: "110px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => rowData?.asset?.store?.name || ""
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "asset.accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => 1
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "asset.accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "asset.accountantCarryingAmount",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "inventoryQuantity",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => (rowData?.inventoryQuantity === null) || rowData?.inventoryQuantity ? 1 : rowData?.differenceQuantity ? rowData.differenceQuantity : 0
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "asset.inventoryOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryOriginalCost ? rowData?.inventoryOriginalCost : 0)
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "asset.inventoryCarryingAmount",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryCarryingAmount ? rowData?.inventoryCarryingAmount : 0)
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "asset.accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => rowData?.inventoryQuantity === null ? 1 : rowData?.differenceQuantity ? rowData.differenceQuantity : 0
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "asset.accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryOriginalCost - rowData?.accountantOriginalCost),
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "asset.accountantCarryingAmount",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryCarryingAmount - rowData?.accountantCarryingAmount),
        },
        {
            title: t("InventoryCountVoucher.note"),
            field: "note",
            minWidth: "150px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: 10,
                paddingRight: "10px",
                textAlign: "left",
            },
        },
    ];
    return (<div>
        <MaterialTable
            data={props?.itemAsset?.reverse()}
            columns={columns}
            options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: true,
                search: false,
                maxBodyHeight: "500px",
                minBodyHeight: "273px",
                padding: "dense",
                rowStyle: (rowData) => ({
                    backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    position: "relative"
                },
            }}
            localization={{
                body: {
                    emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                    )}`,
                },
                pagination: {
                    labelDisplayedRows: "{from}-{to} trong {count}",
                    labelRowsPerPage: "Số bản ghi mỗi trang:",
                    firstTooltip: "Trang đầu",
                    previousTooltip: "Trang trước",
                    nextTooltip: "Trang tiếp",
                    lastTooltip: "Trang cuối",
                    labelRowsSelect: "hàng mỗi trang",
                },
            }}
            components={{
                Toolbar: (props) => (
                    <div className="w-100">
                        <MTableToolbar {...props} />
                    </div>
                ),

                Header: () => {
                    return (
                        <TableHead className={clsx(classes.header)}>
                            <TableRow>
                                {props?.item?.id && props?.isEnabledBtn && <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("general.reevaluateAcronyms")}</TableCell>}
                                <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("Asset.stt")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} rowSpan={2} >{t("Asset.managementCode")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.code")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.name")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.seri")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.model")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.statusDgl")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.receive_date")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.store")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.deviant")}</TableCell>
                                <TableCell className={clsx(classes.colorWhite, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.note")}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                            </TableRow>
                        </TableHead>
                    );
                },
            }}
            onSelectionChange={(rows) => {
                this.data = rows;
            }}
        />
    </div>);
}

export default ListAssetTable;