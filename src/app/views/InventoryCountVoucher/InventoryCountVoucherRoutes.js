import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetTransferTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./InventoryCountVoucherTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(AssetTransferTable);

const InventoryCountVoucherRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/inventory-count-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default InventoryCountVoucherRoutes;