import axios from "axios";
import ConstantList from "../../appConfig";

export const importExcelUrl =
  ConstantList.API_ENPOINT + "/api/excel/import/asset";
