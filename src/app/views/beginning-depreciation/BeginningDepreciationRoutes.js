import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const BeginningDepreciationTable = EgretLoadable({
  loader: () => import("./BeginningDepreciationTable")
});
const ViewComponent = withTranslation()(BeginningDepreciationTable);

const BeginningDepreciationRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/beginning-depreciation",
    exact: true,
    component: ViewComponent
  }
];

export default BeginningDepreciationRoutes;
