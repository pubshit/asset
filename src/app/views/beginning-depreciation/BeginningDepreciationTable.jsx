import React from "react";
import { Grid, TablePagination, Button, FormControl, Input, InputAdornment, FormControlLabel, FormGroup, Checkbox, IconButton, Icon } from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  importExcelUrl,
} from "./BeginningDepreciationService";
import { Breadcrumb, ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import { Helmet } from "react-helmet";
import { convertNumberPrice, defaultPaginationProps, formatTimestampToDate, functionExportToExcel, handleKeyDownFloatOnly, isValidDate } from "app/appFunction";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { CheckBox, CheckBoxOutlineBlank } from "@material-ui/icons";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { LightTooltip, NumberFormatCustom } from "../Component/Utilities";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { exportExcelFileError } from "app/appServices";

function MaterialButton(props) {
  const { t } = props;
  const item = props.item;
  return <div className="none_wrap">
    <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">delete</Icon>
      </IconButton>
    </LightTooltip>
  </div>;
}

class BeginningDepreciationTable extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    rowsPerPage: 50,
    page: 0,
    item: {},
    totalElements: 0,
    keyword: null,
    isReportAllAsset: false,
    period: null,
    shouldOpenAssetPopup: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenImportExcelDialog: false,
    itemList: [],
    isView: false,
  };
  updatePageData = () => {
    const {
      keyword,
      page,
      rowsPerPage
    } = this.state;


  };
  handleKeyDownEnterSearch = (e) => {
    if (e.key === appConst.KEY.ENTER) {
      this.setState({
        page: 0,
        rowsPerPage: 50,
      }, () => {
        this.updatePageData()
      })
    }
  };
  handleSearchIconClick = (event) => {
    event.preventDefault();
    this.setState({
      page: 0,
      rowsPerPage: 50,
    }, () => {
      this.updatePageData()
    })
  }
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => {
      if (!this.state.keyword) { this.updatePageData(); }

    });
  };
  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenAssetPopup: false,
        shouldOpenConfirmationDialog: false,
        shouldOpenImportExcelDialog: false,
      }
    );
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenImportExcelDialog: false,
    });
    this.updatePageData();
  };
  componentDidMount() {

  }

  handleChange = (event, source) => {
    if (source) {
      this.setState({ [source]: event })
      return;
    }

    if (typeof event.target?.checked === "boolean") {
      let { name, checked } = event.target;
      this.setState({ [name]: checked })
    } else {
      let { name, value } = event.target;
      this.setState({ [name]: value })
    }
  }

  convertDepreciationDate = (date, year = 0) => {
    if (!date || !isValidDate(date)) return "";

    let newDate = new Date(date);

    newDate.setFullYear(newDate.getFullYear() + year)
    newDate.setDate(newDate.getDate() - 1);

    return newDate;
  }

  convertDataTable = (data = []) => {
    return data.map(item => {
      let asset = item?.asset || {};
      return {
        ...item,
        id: asset?.id,
        name: asset?.name,
        code: asset?.code,
        managementCode: asset?.managementCode,
        originalCost: asset?.originalCost,
        carryingAmount: asset?.carryingAmount,
        depreciationDate: asset?.depreciationDate,
        soNamKhConLai: asset?.soNamKhConLai,
        timeEnd: this.convertDepreciationDate(asset?.depreciationDate),
      }
    });
  }
  handleSelectAsset = (data) => {
    this.setState({ itemList: this.convertDataTable(data), shouldOpenAssetPopup: false })
  }

  handleChangeCell = (value, rowData, source) => {
    let { itemList } = this.state;
    itemList.forEach(item => {
      if (item?.id === rowData?.id) {
        item[source] = value;
        this.handleCheckChangeCell(item, source, value);
      }
    });

    this.setState({ itemList });
  }

  handleCheckChangeCell = (item, source, value) => {
    if (source === "carryingAmount") {
      item.accumulated = Number(item?.originalCost - value)
    }
    if (source === "accumulated") {
      item.carryingAmount = Number(item?.originalCost - value)
    }
  }

  handleDelete = (rowData) => {
    let { itemList } = this.state;
    itemList = itemList.filter(item => item?.id !== rowData?.id);
    this.setState({ itemList });
  }

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.delete === method) {
      this.handleDelete(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  }

  handleSave = async () => {

  }

  handleDeleteData = () => {
    this.setState({ shouldOpenConfirmationDialog: true })
  }

  handleLockData = async () => {
    const { t, i18n } = this.props;
    this.setState({ isView: !this.state.isView }, () => {
      if (this.state.isView) {
        console.log(1);

        toast.success(t("beginning_depreciation.successBlock"))
      }
    })
  }

  handleConfirmationResponse = async () => {
    this.setState({ shouldOpenConfirmationDialog: false })
  }

  exportExampleImportExcel = () => {
    let { t } = this.props;
    // exportExampleImportExcel()
    //   .then((res) => {
    //     toast.success(t("general.successExport"));
    //     let blob = new Blob([res.data], {
    //       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    //     });
    //     FileSaver.saveAs(blob, t("Asset.importExcelFileName"));
    //   })
    //   .catch((err) => {
    //     console.error(err);
    //   });
  };


  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      isReportAllAsset,
      period,
      shouldOpenAssetPopup,
      shouldOpenConfirmationDialog,
      shouldOpenImportExcelDialog,
      isView,
    } = this.state;
    let TitlePage = t("Asset.beginning_depreciation");
    let columns = [
      {
        title: t("general.index"),
        field: "",
        align: "center",
        maxWidth: 50,
        cellStyle: {
          background: "#dbeef4"
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        minWidth: 80,
        cellStyle: {
          background: "#dbeef4"
        },
        render: rowData => <MaterialButton
          t={t}
          item={rowData}
          onSelect={(rowData, method) => {
            this.handleCheckIcon(rowData, method);
          }}
          onClick={(event) => {
            event.stopPropagation();
          }}
        />
      },
      {
        title: t("Asset.name"),
        field: "name",
        minWidth: "280px",
        cellStyle: {
          background: "#dbeef4"
        }
      },
      {
        title: t("Asset.code"),
        field: "code",
        align: "center",
        minWidth: "140px",
        cellStyle: {
          background: "#dbeef4"
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "center",
        minWidth: "140px",
        cellStyle: {
          background: "#dbeef4"
        },
      },
      {
        title: t("beginning_depreciation.originalCost"),
        field: "originalCost",
        align: "right",
        minWidth: "200px",
        cellStyle: {
          background: "#dbeef4"
        },
        render: (rowData) => convertNumberPrice(rowData?.originalCost)
      },
      {
        title: t("beginning_depreciation.timeCount"),
        field: "timeCount",
        minWidth: "200px",
        cellStyle: {
          background: "#dbeef4"
        },
      },
      {
        title: t("beginning_depreciation.timeStart"),
        field: "depreciationDate",
        align: "center",
        minWidth: "200px",
        cellStyle: {
          background: "#dbeef4"
        },
        render: rowData => formatTimestampToDate(rowData?.depreciationDate)
      },
      {
        title: t("beginning_depreciation.timeEnd"),
        field: "timeEnd",
        align: "center",
        minWidth: "200px",
        cellStyle: {
          background: "#dbeef4"
        },
        render: rowData => formatTimestampToDate(rowData?.timeEnd)
      },
      {
        title: t("beginning_depreciation.accumulated"),
        field: "accumulated",
        align: "left",
        minWidth: "200px",
        render: rowData => {
          return (
            <TextValidator
              className='w-100'
              onChange={(e) => this.handleChangeCell(e?.target?.value, rowData, "accumulated")}
              type='text'
              name='accumulated'
              value={rowData?.accumulated || ""}
              onKeyPress={handleKeyDownFloatOnly}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  className: "text-align-right",
                },
                readOnly: isView,
                disableUnderline: isView,
              }}
              validators={[
                // "minFloat:0",
                `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
              errorMessages={[
                // t("general.nonNegativeNumber"),
                t("general.quantityError")]}
            />
          )
        }
      },
      {
        title: t("beginning_depreciation.carryingAmount"),
        field: "carryingAmount",
        align: "right",
        minWidth: "200px",
        render: rowData => {
          return (
            <TextValidator
              className='w-100'
              onChange={(e) => this.handleChangeCell(e?.target?.value, rowData, "carryingAmount")}
              type='text'
              name='carryingAmount'
              value={rowData?.carryingAmount || ""}
              onKeyPress={handleKeyDownFloatOnly}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  className: "text-align-right"
                },
                readOnly: isView,
                disableUnderline: isView,
              }}
              validators={[
                // "minFloat:0",
                `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
              errorMessages={[
                // t("general.nonNegativeNumber"),
                t("general.quantityError")]}
            />
          )
        }
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.assetManagement") },
              { name: t("Asset.depreciation") },
              { name: t("Asset.beginning_depreciation"), path: "fixed-assets/beginning-depreciation", },
            ]}
          />
        </div>
        <div>
          {shouldOpenAssetPopup && (
            <SelectAssetAllPopup
              t={t}
              i18n={i18n}
              open={shouldOpenAssetPopup}
              handleSelect={this.handleSelectAsset}
              assetVouchers={itemList.length ? itemList : []}
              handleClose={this.handleDialogClose}
            />
          )}
          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title={t("general.confirm")}
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleConfirmationResponse}
              text={t('beginning_depreciation.confirmDeleteMessage')}
              agree={t('general.agree')}
              cancel={t('general.cancel')}
            />
          )}
          {shouldOpenImportExcelDialog && (
            <ImportExcelDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenImportExcelDialog}
              handleOKEditClose={this.handleOKEditClose}
              exportExampleImportExcel={this.exportExampleImportExcel}
              exportFileError={exportExcelFileError}
              url={importExcelUrl}
            />
          )}
        </div>
        <Grid container spacing={2}>
          <Grid item md={6} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.setState({
                shouldOpenAssetPopup: true
              })}
            >
              {t("general.select_asset")}
            </Button>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => this.setState({
                shouldOpenImportExcelDialog: true
              })}
            >
              {t("general.importExcel")}
            </Button>
            <Button
              className="mb-16 align-bottom pr-0"
              variant="contained"
              color="primary"
            >
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="isReportAllAsset"
                      checked={isReportAllAsset}
                      onChange={(event) => this.handleChange(event)}
                      className="p-0 mr-2"
                      icon={<CheckBoxOutlineBlank style={{ color: "#fff" }} />}
                      checkedIcon={<CheckBox style={{ color: "#fff" }} />}
                    />
                  }
                  label={t("beginning_depreciation.report_asset")}
                />
              </FormGroup>
            </Button>
          </Grid>
          <Grid item md={2} xs={12} style={{ transform: "translateY(-5px)" }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                size="small"
                margin="none"
                fullWidth
                views={['month', 'year']}
                format='MM/yyyy'
                label={t("beginning_depreciation.period")}
                value={period ?? null}
                onChange={(data) =>
                  this.handleChange(data, "period")
                }
                KeyboardButtonProps={{ "aria-label": "change date" }}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={t("general.maxDateDefault")}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                clearable
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={4} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                style={{ marginTop: "8px" }}
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("beginning_depreciation.filter")}
                id="search_box"
                endAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.handleSearchIconClick} />
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <ValidatorForm onSubmit={() => { }}>
              <MaterialTable
                title={t("general.list")}
                data={itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                options={{
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: "450px",
                  minBodyHeight: "450px",
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    paddingLeft: 10,
                    paddingRight: 10,
                  },
                  padding: "dense",
                  toolbar: false,
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </ValidatorForm>
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justifyContent="flex-end">
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={isView}
            style={{ minWidth: "160px" }}
            onClick={() => this.handleSave()}
          >
            {t("general.save")}
          </Button>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={isView}
            style={{ minWidth: "160px" }}
            onClick={() => this.handleDeleteData()}
          >
            {t("beginning_depreciation.deleteData")}
          </Button>
          <Button
            className="mb-16 align-bottom"
            variant="contained"
            color="primary"
            style={{ minWidth: "160px" }}
            onClick={() => this.handleLockData()}
          >
            {isView ? t("beginning_depreciation.unblock") : t("beginning_depreciation.lockData")}
          </Button>
        </Grid>
      </div >
    );
  }
}

BeginningDepreciationTable.contextType = AppContext;
export default BeginningDepreciationTable;
