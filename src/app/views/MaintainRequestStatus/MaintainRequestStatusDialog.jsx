import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  TextField,
  FormControlLabel,
  DialogContent
} from "@material-ui/core";
// import Paper from '@material-ui/core/Paper'
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkCode, addNewMaintanRequestStatus, updateMaintanRequestStatus } from "./MaintainRequestStatusService";
import Draggable from 'react-draggable';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});

function PaperComponent(props) {
    return (
      <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
      </Draggable>
    );
  }

class MaintainRequestStatusDialog extends Component {
  state = {
      id:"",
    name: "",
    code: "",
    indexOrder:"",
    isActive: false,
    shouldOpenNotificationPopup:false,
    Notification:"",
  };

  listIndexOder = [
    { id: 0, name: 'Mới tạo' },
    { id: 1, name: 'Tiếp nhận, sửa chữa' },
    { id: 2, name: 'Đã sửa chữa' },
    { id: -1, name: 'Hỏng, nhập kho' }
  ]

  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;

    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        // this.setState({shouldOpenNotificationPopup: true,
        //   Notification:"maintainRequestStatus.noti.dupli_code"})
        toast.warning(this.props.t('maintainRequestStatus.noti.dupli_code'))
        // alert("Code đã được sử dụng");
      } else {
        //Nếu trả về false là code chưa sử dụng có thể dùng
        if (id) {
          updateMaintanRequestStatus({
            ...this.state
          }).then(() => {
            toast.info('Sửa trạng thái thành công.')
            this.props.handleOKEditClose();
          });
        } else {
          addNewMaintanRequestStatus({
            ...this.state
          }).then(() => {
            toast.info('Thêm mới trạng thái thành công.')
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentWillMount() {
    //getUserById(this.props.uid).then(data => this.setState({ ...data.data }));
    let { open, handleClose,item } = this.props;
    this.setState(item);
  }

  render() {
    let {
      id,
      name,
      code,
      indexOrder,
      shouldOpenNotificationPopup
      
    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    return (
      <Dialog   open={open}  PaperComponent={PaperComponent} maxWidth="xs" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <h4 className="">{t("maintainRequestStatus.saveUpdate")}</h4>
        </DialogTitle>
          
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={7} xs={12}>
                <TextValidator
                  className="w-100 "
                  
                  label={<span><span className="colorRed">*</span>{t('maintainRequestStatus.code')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={5} xs={12} >
                <FormControl fullWidth={true} >
                  <InputLabel htmlFor="indexOrderId" >{t("maintainRequestStatus.order")}</InputLabel>
                  <Select
                    value={indexOrder}
                    onChange={indexOrder => this.handleChange(indexOrder, "indexOrder")}
                    inputProps={{
                      name: "indexOrder",
                      id: "indexOrderId"
                    }}
                  >
                    {this.listIndexOder.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span className="colorRed">*</span>{t('maintainRequestStatus.name')}</span>}

                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
              
            </div>
        </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default MaintainRequestStatusDialog;
