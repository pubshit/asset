import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const MaintainRequestStatusTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./MaintainRequestStatusTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(MaintainRequestStatusTable);

const MaintainRequestStatusRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/maintain_request_status",
    exact: true,
    component: ViewComponent
  }
];

export default MaintainRequestStatusRoutes;