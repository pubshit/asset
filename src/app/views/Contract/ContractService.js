import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/contract" + ConstantList.URL_PREFIX;
const API_PATH_MAINTANE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/contracts";

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByText/"+ searchObject?.pageIndex + '/' +  searchObject?.pageSize;
  return axios.post(url, searchObject);
};

export const searchByPageList = (searchObject) => {
  let url = API_PATH_MAINTANE + "/search";
  let config = {
    params : searchObject
  }
  return axios.get(url, config);
};

export const getByPage = (page, pageSize) => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/contract";
  var pageIndex = page + 1;
  var params = '/'+ pageIndex + "/" + pageSize;
  var url = API_PATH + params;
  return axios.get(url);
};

export const getItemById = id => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/contract";
  var url = API_PATH + "/" + id;
  return axios.get(url);
};
export const deleteItem = id => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/contract";
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};
export const saveItem = item => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/contract";
  var url = API_PATH;
  return axios.post(url, item);
};

export const checkCode = (id, code) => {
  var API_PATH = ConstantList.API_ENPOINT + "/api/contract";
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};
