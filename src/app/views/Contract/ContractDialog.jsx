import {
  Grid,
  DialogActions,
  Button,
  Dialog,
} from '@material-ui/core';
import React from 'react';
import { saveItem, checkCode } from './ContractService';
import * as supplierService from './../Supplier/SupplierService';
import * as commonObjectService from './../CommonObject/CommonObjectService';
import { TextValidator } from 'react-material-ui-form-validator';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import {
  filterOptions,
  formatDateDto,
  handleKeyDownIntegerOnly,
  handleThrowResponseMessage,
  isSuccessfulResponse,
} from 'app/appFunction';
import SupplierDialog from '../Supplier/SupplierDialog';
import { variable, appConst, STATUS_SUPPLIER, keySearch } from 'app/appConst';
import { toast } from 'react-toastify';
import ValidatedDatePicker from '../Component/ValidatePicker/ValidatePicker';
import { liquidateContract } from '../MaintainPlaning/MaintainPlaningService';
import AsynchronousAutocompleteSub from '../utilities/AsynchronousAutocompleteSub';
import { PaperComponent, NumberFormatCustom } from "../Component/Utilities";
import { Label } from "../Component/Utilities";
import PropTypes from "prop-types";
import AppContext from "../../appContext";
import CustomValidatorForm from '../Component/ValidatorForm/CustomValidatorForm';
import ReactQuill from "react-quill";

class ContractDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    selectedItem: {},
    suppliers: [],
    supplier: null,
    contractTypes: [],
    contractType: null,
    contractDate: new Date(),
    contractName: "",
    contractCode: "",
    contractContent: null,
    shouldOpenNotificationPopup: false,
    shouldOpenAddSupplierDialog: false,
    cost: null,
    amountPaid: "",
    performedTimesNumber: null,
  };

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleClick = (event, item) => {
    //alert(item);
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null });
    }
  };

  componentDidMount = async () => {
    const {
      t,
      item,
      ngayKyHopDong,
      contractTypeCode,
      isVisibleType,
    } = this.props;
    const { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await supplierService.searchByText(
        null,
        0,
        10000,
        {
          isActive: STATUS_SUPPLIER.HOAT_DONG.code
        }
      ).then(({ data }) => {
        let suppliers = data?.content;
        this.setState({ suppliers });
      }).catch(() => {
        toast.error(t("general.error"))
      });

      await commonObjectService.getCommonObjectByCode(
        appConst.listCommonObject.HOP_DONG.code
      ).then(({ data }) => {
        let contractType = null
        let contractTypes = data || [];
        if (contractTypeCode) {
          contractType = data?.find(
            item => item.code === contractTypeCode
          ) || null
          if (isVisibleType) {
            contractTypes = contractTypes?.filter(
              item => item.code === appConst.TYPES_CONTRACT.BTBD || item.code === appConst.TYPES_CONTRACT.CU
            )
          }
          this.setState({
            contractTypes,
            contractType,
            contractTypeId: contractType?.id,
          });
        } else {
          this.setState({
            contractTypes: data
          });
        }
      }).catch(() => {
        toast.error(t("general.error"))
      });

      this.setState({
        contractName: item?.name
      })
    } catch {

    } finally {
      setPageLoading(false);
      if (item?.id) {
        this.setState({
          ...item,
          contractTypeId: item?.contractType?.id,
        });
      } else {
        if (ngayKyHopDong) {
          this.setState({
            contractDate: new Date(ngayKyHopDong),
          })
        }
        if (contractTypeCode) {
          this.setState({
            supplier: item?.supplier,
            contractDate: item?.contractDate || new Date(),
            contractName: item?.contractName,
          })
        }
      }
    }
  }

  handleChangeInput = (event, source) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleChangeDate = (date, nameDate) => {
    this.setState({ [nameDate]: date });
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { id } = this.state;
    let { t, isMaintainPlaning } = this.props;
    let code = this.state.contractCode;
    const { handleSelect = () => { }, } = this.props;

    if (isMaintainPlaning) {
      await this.handleEndContractSubmit();
      setPageLoading(false);
      return;
    }

    let dataSubmit = this.convertDataSubmit();
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(t("Contract.noti.dupli_code"));
      } else {
        saveItem(dataSubmit).then(({ data }) => {
          if (data !== null) {
            handleSelect({
              ...data,
              contractText: data?.contractName + " - " + data?.contractCode,
            })
            id
              ? toast.success(t("general.updateSuccess"))
              : toast.success(t("general.addSuccess"));
          }
          this.props.handleClose();
        });
      }
    }).finally(() => {
      setPageLoading(false);
    });
  };

  convertDataSubmit = () => ({
    id: this.state.id,
    supplier: this.state.supplier,
    contractType: this.state.contractType,
    contractDate: this.state.contractDate ? formatDateDto(this.state.contractDate) : null,
    cost: this.state.cost,
    amountPaid: this.state.amountPaid,
    performedTimesNumber: this.state.performedTimesNumber,
    contractName: this.state.contractName,
    contractCode: this.state.contractCode,
    contractContent: this.state.contractContent,
  })

  handleEndContractSubmit = async () => {
    let { contractId, contractLiquidationDate } = this.state;

    let { t, handleOKEditClose = () => { } } = this.props;
    try {
      let searchObject = {
        contractLiquidationDate: formatDateDto(contractLiquidationDate),
      }
      let res = await liquidateContract(contractId, searchObject);
      const { data, code } = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(t("general.success"));
        handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    }
  };

  handChangeSupplier = event => {
    if (variable.listInputName.New === event?.code) {
      this.setState({
        shouldOpenAddSupplierDialog: true,
      });
    }

    this.setState({
      supplier: event,
      supplierId: event?.id,
    });
  };
  handChangeContractType = event => {
    this.setState({
      contractType: event
    });
  };

  handleContentChange = contentHtml => {
    const parser = new DOMParser(); // todo: convert to html tag (value trả về: <p>...</p>)
    const doc = parser?.parseFromString(contentHtml, 'text/html');
    const newValue = doc.body.textContent

    this.setState({
      contractContent: contentHtml,
    });
  };

  handleCloseSupplier = () => {
    const { supplier } = this.state
    this.setState({
      shouldOpenAddSupplierDialog: false
    })
    if (variable.listInputName.New === supplier?.code) {
      this.setState({
        supplier: {},
      });
    }
    const { t } = this.props;
    supplierService.searchByText(null, 0, 1000, { isActive: STATUS_SUPPLIER.HOAT_DONG.code }).then(({ data }) => {
      let suppliers = data.content;
      this.setState({ suppliers });
    }).catch(() => {
      toast.error(t("general.error"))
    });
  }

  render() {
    const {
      t, i18n,
      open,
      isView,
      isMaintainPlaning, // TODO: End contract for maintain planing
      isFromPlaningDialog, // TODO: for Add new contract for maintain planing
      handleClose,
      isVisibleType,
      ngayKyHopDong,
      item,
      isHDCUType
    } = this.props;

    let {
      contractLiquidationDate,
      contractCode,
      contractName,
      contractContent,
      contractDate,
      shouldOpenNotificationPopup,
      suppliers,
      supplier,
      cost,
      performedTimesNumber,
      contractTypes,
      contractType,
      amountPaid,
    } = this.state;
    const descriptionLabel = t('Contract.content');
    const isHDCU = appConst.TYPES_CONTRACT.CU === contractType?.code;
		const toolbarOptions = [
			['bold', 'italic', 'underline', 'strike'],        // toggled buttons
			['blockquote', 'code-block'],
			['link', 'image', 'video', 'formula'],
			
			[{ 'list': 'ordered'}, { 'list': 'bullet' }, { 'list': 'check' }],
			[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
			[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
			[{ 'direction': 'rtl' }],                         // text direction
			
			[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
			[{ 'header': [1, 2, 3, 4, 5, 6, false] }],
			
			[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
			[{ 'font': [] }],
			[{ 'align': [] }],
			
			['clean']                                         // remove formatting button
		];

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id='draggable-dialog-title'>
          {t('Contract.saveUpdate')}
        </DialogTitle>
        <CustomValidatorForm ref='form' onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className='' container spacing={1}>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className='w-100'
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('Contract.name')}
                    </span>
                  }
                  onChange={this.handleChangeInput}
                  type='text'
                  InputProps={{
                    readOnly: isView,
                  }}
                  name='contractName'
                  value={contractName || ""}
                  disabled={isMaintainPlaning && !isView}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className='w-100'
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('Contract.code')}
                    </span>
                  }
                  onChange={this.handleChangeInput}
                  type='text'
                  InputProps={{
                    readOnly: isView,
                  }}
                  name='contractCode'
                  value={contractCode || ""}
                  disabled={isMaintainPlaning && !isView}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={<Label isRequired>{t('Supplier.title')}</Label>}
                  searchFunction={() => { }}
                  listData={suppliers}
                  onSelect={(value) => this.handChangeSupplier(value)}
                  onInputChange={e => this.handleChangeDate(e.target.value, keySearch.supply)}
                  readOnly={isView}
                  displayLable='name'
                  value={supplier || null}
                  filterOptions={(options, params) =>
                    filterOptions(options, params, true, "name")
                  }
                  disabled={
                    (isMaintainPlaning && !isView)
                    || isFromPlaningDialog || item?.supplier?.id
                  }
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>

              <Grid item sm={isMaintainPlaning ? 3 : 6} xs={12}>
                <ValidatedDatePicker
                  margin='none'
                  fullWidth
                  id='date-picker-dialog'
                  label={t('Contract.date')}
                  format='dd/MM/yyyy'
                  value={contractDate || null}
                  readOnly={isView}
                  InputProps={{
                    readOnly: isView
                  }}
                  onChange={date => {
                    this.handleChangeDate(date, 'contractDate');
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  maxDate={ngayKyHopDong ? new Date(ngayKyHopDong) : new Date()}
                  maxDateMessage={ngayKyHopDong ? t("MaintainPlaning.messages.maxDateContract") : t("allocation_asset.maxDateMessage")}
                  invalidDateMessage={t('general.invalidDateFormat')}
                  disabled={isMaintainPlaning && !isView}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>

              {isMaintainPlaning && (
                <Grid item sm={3} xs={12}>
                  <ValidatedDatePicker
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t('MaintainPlaning.contractLiquidationDate')}
                      </span>
                    }
                    value={contractLiquidationDate}
                    readOnly={isView}
                    InputProps={{
                      readOnly: isView
                    }}
                    onChange={date => {
                      this.handleChangeDate(date, 'contractLiquidationDate');
                    }}
										maxDate={new Date()}
										maxDateMessage={t("general.maxDateNow")}
                    invalidDateMessage={t('general.invalidDateFormat')}
                  />
                </Grid>
              )}
              <Grid item sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('Contract.contractType')}
                    </span>
                  }
                  searchFunction={() => { }}
                  listData={contractTypes}
                  onSelect={(value) => this.handChangeContractType(value)}
                  readOnly={isView}
                  displayLable='name'
                  value={contractType || null}
                  filterOptions={filterOptions}
                  disabled={
                    (isMaintainPlaning && !isView)
                    || isFromPlaningDialog || isHDCUType
                  }
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              {!isHDCU && (
                <Grid item sm={3} xs={12}>
                  <TextValidator
                    className='w-100'
                    label={t('Contract.cost')}
                    onChange={this.handleChangeInput}
                    type='text'
                    name='cost'
                    value={cost || ""}
                    disabled={isMaintainPlaning && !isView}
                    InputProps={{
                      readOnly: isView,
                      inputComponent: NumberFormatCustom,
                    }}
                    validators={['minNumber:1']}
                    errorMessages={[t("general.minNumberError")]}
                  />
                </Grid>
              )}
              {!isHDCU && (
                <Grid item sm={3} xs={12}>
                  <TextValidator
                    className='w-100'
                    label={t('Contract.amountPaid')}
                    onChange={this.handleChangeInput}
                    type='text'
                    name='amountPaid'
                    value={amountPaid || ""}
                    disabled={isMaintainPlaning && !isView}
                    InputProps={{
                      readOnly: isView,
                      inputComponent: NumberFormatCustom,
                    }}
                    validators={['minNumber:1']}
                    errorMessages={[t("general.minNumberError")]}
                  />
                </Grid>
              )}
              {!isHDCU && (
                <Grid item sm={3} xs={12}>
                  <TextValidator
                    className='w-100'
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        {t('Contract.times')}
                      </span>
                    }
                    onChange={this.handleChangeInput}
                    onKeyDown={handleKeyDownIntegerOnly}
                    type='text'
                    InputProps={{
                      readOnly: isView,
                      inputComponent: NumberFormatCustom,
                    }}
                    name='performedTimesNumber'
                    value={performedTimesNumber || ""}
                    disabled={isMaintainPlaning && !isView}
                    validators={['required', 'minNumber:1']}
                    errorMessages={[t('general.required'), t("general.minNumberError")]}
                  />
                </Grid>
              )}
              <Grid item sm={12} xs={12}>
                <div className="mt-12 mb-3">
                  <span><b>{descriptionLabel}</b></span>
                </div>
								<ReactQuill
									modules={{
										toolbar: toolbarOptions,
									}}
									theme="snow"
									formats={[
										'header',
										'bold', 'italic', 'underline', 'strike', 'blockquote',
										'list', 'bullet', 'indent',
										'link', 'image'
									]}
									value={contractContent} onChange={this.handleContentChange} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className='flex flex-space-between flex-middle mt-12'>
              <Button
                className='mr-12'
                variant='contained'
                color='secondary'
                onClick={handleClose}
              >
                {t('general.close')}
              </Button>
              {!isView && (
                <Button
                  variant='contained'
                  className="mr-15"
                  color='primary'
                  type='submit'
                >
                  {
                    isMaintainPlaning
                      ? t('MaintainPlaning.endContract')
                      : t('general.save')
                  }
                </Button>
              )}
            </div>
          </DialogActions>
        </CustomValidatorForm>
        {this.state.shouldOpenAddSupplierDialog &&
          <SupplierDialog
            t={t}
            i18n={i18n}
            item={{
              name: this.state[keySearch.supply],
            }}
            typeCodes={this.props?.typeCodesSupplier}
            open={this.state.shouldOpenAddSupplierDialog}
            handleSelectDVBH={this.handChangeSupplier}
            handleClose={this.handleCloseSupplier}
          />
        }
      </Dialog>
    );
  }
}

ContractDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  isVisibleType: PropTypes.bool,
  handleClose: PropTypes.func,
}
ContractDialog.contextType = AppContext;
export default ContractDialog;
