import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import React from "react";
import MaterialTable, { MTableToolbar } from 'material-table';
import { useTranslation } from 'react-i18next';
import { deleteItem, getItemById, searchByPage } from "./ContractService";
import ContractDialog from "./ContractDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import moment from "moment";
import { Helmet } from 'react-helmet';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import localStorageService from "app/services/localStorageService";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
import {LightTooltip} from "../Component/Utilities";
import {defaultPaginationProps, getTheHighestRole, handleKeyDown, handleKeyUp, handleThrowResponseMessage} from "../../appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import {getCommonObjectByCode} from "../CommonObject/CommonObjectService";
import AppContext from "../../appContext";

function MaterialButton(props) {
  const { t } = useTranslation();
  const {isRoleOrgAdmin} = getTheHighestRole();
  const item = props.item;

  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    {
      isRoleOrgAdmin && (
        <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">delete</Icon>
          </IconButton>
        </LightTooltip>
    )}
  </div>;
}

class Contract extends React.Component {
  state = {
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    keyword: '',
    Notification: '',
    hasDeletePermission: false,
    contractType: [],
    listType: [],
  }
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }

  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }

  search = async () => {
    this.setPage(0);
  }

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  handleKeyUp = e => handleKeyUp(e, this.search);

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser()
  }

  updatePageData = async () => {
    let { t } = this.props;
    let {setPageLoading} = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword?.trim() || "";
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.typeId = this.state.contractType?.id;

    try {
      setPageLoading(true);
      let res = await searchByPage(searchObject)
      if (res) {
        this.setState({
          itemList: [...res?.data?.content],
          totalElements: res?.data?.totalElements
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  setPage = page => {
    this.setState({ page }, () => {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, () => {
      this.updatePageData();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,

    }, this.updatePageData);
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    if (this.state.itemList.length === 1 && this.state.page === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count
      })
    }
    deleteItem(this.state.id).then((res) => {
      if (res?.data) {
        toast.success(t("general.success"))
      }
      this.updatePageData();
      this.handleDialogClose()
    });
  };

  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser.roles.forEach((role) => {
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
      });
    }
  };

  handChangeContractType = (contractType) => {
    this.setState({
      contractType,
    }, this.search);
  }

  handleSetData = (value, name) => {
    this.setState({
      [name]: value
    })
  }

  render() {
    const { t, i18n } = this.props;
    let { keyword, contractType, listType } = this.state;
    let TitlePage = t("Contract.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "250",
        cellStyle: {
          textAlign: "center"
        },
        render: rowData => <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
              getItemById(rowData.id).then(({ data }) => {
                if (data.parent === null) {
                  data.parent = {};
                }
                this.setState({
                  item: data,
                  shouldOpenEditorDialog: true
                });
              })
            } else if (method === 1) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      {
        title: t("Contract.name"), field: "contractName", width: "150",
        cellStyle: {
          paddingRight: '10px',
        },
      },
      {
        title: t("Contract.code"), field: "contractCode", align: "left", width: "150",
        cellStyle: {
          paddingRight: '10px',
        },
      },
      {
        title: t("Contract.date"),
        field: "",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center"
        },
        render: rowData =>
          (rowData.contractDate) ? <span>{moment(rowData.contractDate).format('DD/MM/YYYY')}</span> : ''
      },
      {
        title: t("Supplier.name"),
        field: "supplier.name",
        align: "left",
        width: "150",
      },
      {
        title: t("Contract.contractType"),
        field: "contractType.name",
        align: "left",
        width: "150",
      },

    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.category"), path: "/list/contract" },
            { name: TitlePage }]} />
        </div>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} sm={3} xs={12} >
            <Button
              className="mr-16 mt-12"
              variant="contained"
              color="primary"
              onClick={() => this.handleEditItem(null)}
            >
              {t('general.add')}
            </Button>

            {this.state.shouldOpenConfirmationDeleteListDialog && (
              <ConfirmationDialog
                open={this.state.shouldOpenConfirmationDeleteListDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t('general.deleteConfirm')}
                agree={t('general.agree')}
                cancel={t('general.cancel')}
              />
            )}
          </Grid>
          <Grid item md={3} sm={3} xs={12}>
            <CustomValidatorForm onSubmit={() => {}}>
              <AsynchronousAutocompleteSub
                label={t('Contract.contractType')}
                searchFunction={getCommonObjectByCode}
                searchObject={appConst.listCommonObject.HOP_DONG.code}
                listData={listType}
                nameListData="listType"
                typeReturnFunction="list"
                setListData={this.handleSetData}
                displayLable='name'
                value={contractType || null}
                onSelect={(value) => this.handChangeContractType(value)}
              />
            </CustomValidatorForm>
          </Grid>
          <Grid item md={6} sm={6} xs={12}>
            <FormControl fullWidth>
              <Input
                className='search_box mt-16'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("Contract.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#"> <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {this.state.shouldOpenEditorDialog && (
                <ContractDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={this.state.item}
                />
              )}
              {this.state.shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={this.state.shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={this.state.itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>

    )
  }
}
export default Contract;
Contract.contextType = AppContext;
