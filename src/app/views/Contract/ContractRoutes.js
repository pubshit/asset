import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const Contract = EgretLoadable({
  loader: () => import("./Contract")
});
const ViewComponent = withTranslation()(Contract);

const ContractRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/contract",
    exact: true,
    component: ViewComponent
  }
];

export default ContractRoutes;
