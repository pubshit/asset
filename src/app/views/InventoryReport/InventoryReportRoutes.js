import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';

const InventoryReportTable = EgretLoadable({
  loader: () => import("./InventoryReportTable")
});
const ViewComponent = withTranslation()(InventoryReportTable);

const InventoryReportRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "report/inventory_report",
    exact: true,
    component: ViewComponent
  }
];

export default InventoryReportRoutes;
