import axios from "axios";
import ConstantList from "../../appConfig";
import { removeFalsy } from "app/appFunction";

const API_PATH_TON_KHO = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report";
const API_PATH_REPORT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report";
export const API_PATH_EXPORT_WORD = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/report/word/config/inventory-supplies";

export const searchByPage = (searchObject) => {
  let url = API_PATH_TON_KHO + "/ton-kho/vat-tu";
  let config = {
    params: removeFalsy(searchObject),
  };
  return axios.get(url, config);
};

export const getVoucherById = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  return axios.get(API_PATH_TON_KHO + "/ton-kho/vat-tu/chi-tiet", config);
};

export const getInventorySummary = (searchObject) => {
  let config = {
    params: removeFalsy(searchObject),
  };
  return axios.get(API_PATH_TON_KHO + "/ton-kho/vat-tu/summary", config);
};

export const exportToExcel = (searchObject) => {
  let {excelUrl, ...rest} = searchObject;
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + searchObject.excelUrl,
    params: removeFalsy(rest),
    responseType: "blob",
  });
};

export const exportToExcelPrintSuppliesInventory = (params) => {
  return axios({
    method: "get",
    url: API_PATH_REPORT + "/kiem-ke-vat-tu-ton-kho/export-excel",
    params,
    responseType: "blob",
  });
};
