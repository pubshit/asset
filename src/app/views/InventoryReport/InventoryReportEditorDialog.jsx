import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  TablePagination
} from "@material-ui/core";
import moment from "moment";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';
import MaterialTable, { MTableToolbar } from 'material-table';
import {convertMoney, formatTimestampToDate} from "../../appFunction";
import {appConst} from "../../appConst";
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
function TabPanel(props) {
  const { value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
    </div>
  );
}

TabPanel.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,

          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};


class InventoryReportEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    address: "",
    totalElements: 0,
    rowsPerPage: 10,
    page: 0,
    list_import_history: [],
    itemList: [],
    currentTabIndex: 0,
    pageIndexImport: 0,
    pageSizeImport: 1,
    pageIndexExport: 0,
    pageSizeExport: 1,
    value: 0,
    productId: null,
  };
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({ value: newValue });
  }

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
  };
  componentDidMount() {
    let {item} = this.props;
    this.setState({
      itemList: item.details,
      productId: item.productId,
      unitPrice: item.unitPrice,
      soLuongDauKy: item.soLuongDauKy
    });

  }

  render() {
    let { open, t} = this.props;
    let {
      name,
      itemList,
      soLuongDauKy
    } = this.state;
    const moneySeparation = appConst.TYPES_MONEY.COMMA.code;
    let columns = [
      {
        title: t("InventoryReport.date"),
        field: "issueDate",
        minWidth: 120,
        align: 'center',
        render: rowData => formatTimestampToDate(rowData.inputDate)
      },
      {
        title: t("InventoryReport.code"),
        field: "productCode",
        minWidth: 130,
        align: 'left',
        cellStyle: {
          textAlign: 'center'
        },
      },
      {
        title: t("InventoryReport.name"),
        field: "productName",
        align: "left",
        minWidth: '175px',
      },
      {
        title: t("InventoryReport.sku"),
        field: "skuName",
        align: "center",
        minWidth: 80,
      },
      {
        title: t("InventoryReport.importQuantity"),
        field: "soLuongNhapTrongKy",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: 'center'
        },
        render: rowData => convertMoney(rowData.soLuongNhapTrongKy, moneySeparation)
      },
      {
        title: t("InventoryReport.exportQuantity"),
        field: "soLuongXuatTrongKy",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: 'center'
        },
        render: rowData => convertMoney(rowData.soLuongXuatTrongKy, moneySeparation),
      },
      {
        title: t("InventoryReport.remainingQuantity"),
        field: "soLuongCuoiKy",
        align: "left",
        minWidth: 100,
        cellStyle: {
          textAlign: 'center'
        },
        render: rowData => convertMoney(rowData.soLuongCuoiKy, moneySeparation),
      },
      {
        title: t("InventoryReport.price"),
        field: "unitPrice",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: 'right'
        },
        render: rowData => convertMoney(rowData.unitPrice, moneySeparation),
      },
      {
        title: t("InventoryReport.total"),
        field: "thanhTienCuoiKy",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: 'right'
        },
        render: rowData => convertMoney(rowData.thanhTienCuoiKy, moneySeparation),
      },
    ];

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth={true}>
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <span className="">{t('InventoryReport.history_title')}</span>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent className="p-0">
            <Grid item xs={12}>
              <MaterialTable
                title={t('InventoryReport.remainingQuantityFromDate') + ": " + (soLuongDauKy || 0)}
                data={itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  },
                  ...appConst.localizationVi,
                }}
                options={{
                  sorting: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: true,
                  search: false,
                  padding: 'dense',
                  maxBodyHeight: '340px',
                  minBodyHeight: '340px',
                  rowStyle: rowData => ({
                    backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF'
                  }),
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                }}
                components={{
                  Toolbar: props => (
                    <MTableToolbar {...props} />
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                className="mr-12"
                variant="contained"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.close')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>

      </Dialog>
    );
  }
}

export default InventoryReportEditorDialog;
