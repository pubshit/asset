import React, { useMemo } from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input, InputAdornment,
  Card,
  CardContent, TableCell, TableRow, TableContainer, Table, TableHead, TableBody,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  searchByPage,
  getVoucherById,
  exportToExcel,
  exportToExcelPrintSuppliesInventory, API_PATH_EXPORT_WORD, getInventorySummary
} from "./InventoryReportService";
import InventoryReportEditorDialog from "./InventoryReportEditorDialog";
import { Breadcrumb } from "egret";
import { useTranslation } from 'react-i18next';
import { saveAs } from 'file-saver';
import moment from "moment";
import { Helmet } from 'react-helmet';
import SearchIcon from '@material-ui/icons/Search';
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import { searchByPage as storesSearchByPage } from '../Store/StoreService'
import { ValidatorForm } from 'react-material-ui-form-validator'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  STATUS_STORE,
  appConst,
  LIST_ORGANIZATION,
  DEFAULT_TOOLTIPS_PROPS,
  PRINT_TEMPLATE_MODEL,
  PRINT_DOCUMENT_TYPE
} from "app/appConst";
import viLocale from "date-fns/locale/vi";
import {
  convertFromToDate,
  convertMoney, defaultPaginationProps,
  formatDateDto,
  functionExportToExcel, handleGetTemplatesByModel,
  handleKeyDown,
  handleKeyUp, handleThrowResponseMessage, isSuccessfulResponse,
  isValidDate
} from "app/appFunction";
import { searchByPage as searchByPageCouncil } from "../Council/CouncilService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import AppContext from "../../appContext";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import localStorageService from "../../services/localStorageService";
import { convertNumberPrice } from "../../appFunction";
import { LightTooltip } from "../Component/Utilities";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;

  return <div className="none_wrap">
    <LightTooltip title={t('general.viewDetail')}{...DEFAULT_TOOLTIPS_PROPS}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
        <Icon fontSize="small" color="primary">visibility</Icon>
      </IconButton>
    </LightTooltip>
  </div>;
}

class InventoryReportTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 5,
    page: 0,
    InventoryReport: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationDeleteListDialog: false,
    fromDate: moment().startOf('month'),
    toDate: moment().endOf('month'),
    remainingQuantityFromDate: 0,
    store: null,
    listDataHD: [],
    shouldOpenPrintDialog: false,
    dataView: {},
    listDocumentTemplates: [],
    reportType: appConst.INVENTORY_REPORT_TYPE.UNIT_PRICE,
    reportSummary: {
      productId: null,
      productName: null,
      productCode: null,
      objectType: null,
      skuId: null,
      skuName: null,
      soLuongDauKy: null,
      thanhTienDauKy: null,
      soLuongNhapTrongKy: null,
      thanhTienNhapTrongKy: null,
      soLuongXuatTrongKy: null,
      thanhTienXuatTrongKy: null,
      soLuongCuoiKy: null,
      thanhTienCuoiKy: null,
      unitPrice: null,
      inputDate: null
    }
  };
  numSelected = 0;
  rowCount = 0;

  handleTextChange = event => {
    this.setState({ keyword: event.target.value })
  };

  handleKeyDownEnterSearch = e => {
    handleKeyDown(e, this.search);
    this.handleGetInventorySummary();
  };

  setPage = page => {
    this.setState({ page }, () => {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    }, () => {
      if (isValidDate(date)) {
        this.updatePageData();
        this.handleGetInventorySummary();
      }
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  checkData = () => {
    if (!this.data || this.data.length === 0) {
      alert("Chưa chọn dữ liệu");
    } else if (this.data.length === this.state.itemList.length) {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true })
    } else {
      this.setState({ shouldOpenConfirmationDeleteListDialog: true })
    }
  }

  updatePageData = () => {
    let { store } = this.state;
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let searchObject = {};
      if (store != null && store.id) {
        searchObject.storeId = this.state.store.id;
      }
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      searchObject.fromDate = formatDateDto(this.state.fromDate);
      searchObject.toDate = formatDateDto(this.state.toDate);
      searchObject.reportType = this.state.reportType?.code;

      searchByPage(searchObject).then(({ data }) => {
        data?.data?.content?.map(product => {
          product.total = product.remainingQuantity * product.price;
        })
        this.setState({ itemList: [...data?.data?.content], totalElements: data?.data?.totalElements })
      }).catch(() => { })
    } catch (error) {
      console.error(error)
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    let blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenPrintDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleConfirmationResponse = () => {
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count
      })
    } else if (this.state.itemList.length === 1 && this.state.page === 1) {
      this.setState({
        page: 1
      })
    }
  };

  componentDidMount() {
    this.updatePageData();
    this.handleGetInventorySummary();
    this.handleGetDocumentTemplates();
  }

  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleClick = (event, item) => {
    let { InventoryReport } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < InventoryReport.length; i++) {
      if (InventoryReport[i].checked == null || InventoryReport[i].checked == false) {
        selectAllItem = false;
      }
      if (InventoryReport[i].id == item.id) {
        InventoryReport[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, InventoryReport: InventoryReport });

  };
  handleSelectAllClick = (event) => {
    let { InventoryReport } = this.state;
    for (let i = 0; i < InventoryReport.length; i++) {
      InventoryReport[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, InventoryReport: InventoryReport });
  };

  selectStore = (storeSelected) => {
    this.setState({ store: storeSelected }, () => {
      this.search();
      this.handleGetInventorySummary();
    })
  }

  /* Export to excel */
  exportToExcel = async () => {
    let {store, reportType} = this.state;
    let {setPageLoading} = this.context;
    let {t} = this.props;
    let searchObject = {}
    let currentTemplate = this.state.listDocumentTemplates.find(item => item?.code === reportType?.templateCode)
    searchObject.storeId = store?.id;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.fromDate = formatDateDto(this.state.fromDate);
    searchObject.toDate = formatDateDto(this.state.toDate);
    searchObject.templateId = currentTemplate?.id;
    searchObject.reportType = reportType?.code;
    searchObject.excelUrl = currentTemplate.excelUrl;
    
    const fileName = currentTemplate?.fileName
    
    try {
      await functionExportToExcel(
        exportToExcel,
        searchObject,
        fileName,
        setPageLoading,
      )
    } catch (e) {
      toast.error(t("general.error"));
    }
  }

  setListDataHD = (listDataHD) => {
    this.setState({ listDataHD })
  }

  handleChangeSelectInventoryBoard = (value) => {
    this.setState({ inventoryBoard: value })
  }

  formatQuantity = (q) => {
    let str = q?.toString();
    let decimalIndex = str.indexOf('.');
    return q ? str.slice(0, decimalIndex + 3) : 0;
  }

  convertDataPrint = (data = []) => {
    let { currentOrg } = this.context;
    let _response = data?.map((product, x) => {
      let difference = product?.soLuongCuoiKy - product?.soLuongDauKy
      let isExcess = difference > 0;
      let isLack = difference < 0;
      let emptyValue = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code ? "" : 0;
      let _soLuongCuoiKy = product?.soLuongCuoiKy || 0;

      let soLuongCuoiKy =
        _soLuongCuoiKy % 1 === 0
          ? _soLuongCuoiKy.toString()
          : parseFloat(_soLuongCuoiKy.toFixed(2));

      return {
        ...product,
        index: x + 1,
        isExcess: isExcess ? this.formatQuantity(difference) : emptyValue,
        isLack: isLack ? this.formatQuantity(difference) : emptyValue,
        total: product?.remainingQuantity * product?.price,
        unitPrice: convertMoney(product?.unitPrice, appConst.TYPES_MONEY.SPACE.code) || 0,
        thanhTienCuoiKy: convertNumberPrice(product?.thanhTienCuoiKy, appConst.TYPES_MONEY.SPACE.code) || 0,
        soLuongCuoiKy,
      }
    })

    let date = new Date();
    const day = String(date?.getDate())?.padStart(2, '0');
    const month = String(date?.getMonth() + 1)?.padStart(2, '0');
    const year = String(date?.getFullYear());
    const hours = String(date?.getHours());
    const minus = String(date?.getMinutes());
    let org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);

    return {
      dataView:
      {
        day,
        month,
        year,
        hours,
        minus,
        storeName: this.state.store?.name || "..".repeat(20),
        titleName: org?.org?.name,
        budgetCode: org?.budgetCode,
        addressOfEnterprise: org?.province,
        assetVouchers: _response,
        inventoryBoard: this.state?.inventoryBoard?.hdChiTiets?.map((i, x) => {
          return {
            ...i,
            index: x + 1
          }
        }),
        sumAmount: convertNumberPrice(data?.reduce((total, item) => total + item?.thanhTienCuoiKy, 0), appConst.TYPES_MONEY.SPACE.code) || 0
      },
    }
  }

  printVouchers = async () => {
    const { t } = this.props;
    let { setPageLoading, currentOrg } = this.context;
    try {
      setPageLoading(true)
      let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
      let { store } = this.state;
      if (store != null && store.id) {
        searchObject.storeId = this.state.store.id;
      }
      searchObject.keyword = this.state.keyword.trim();
      searchObject.fromDate = formatDateDto(this.state.fromDate);
      searchObject.toDate = formatDateDto(this.state.toDate);

      if (currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code) {
        return this.setState({
          item: {
            externalParams: { ...searchObject },
          },
          shouldOpenPrintDialog: true,
        })
      }

      const response = await searchByPage(searchObject);
      let dataPrint = this.convertDataPrint(response?.data?.data?.content);
      this.setState({
        ...(dataPrint || {}),
        item: {
          externalParams: {
            ...searchObject,
            documentType: PRINT_DOCUMENT_TYPE.WORD,
          },
        },
        shouldOpenPrintDialog: true
      })
    } catch (error) {
      console.error(error)
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleExportExcelPrintSuppliesInventory = async () => {
    let { setPageLoading, currentOrg } = this.context;
    const { t } = this.props;
    const { reportType } = this.state;
    let fileName = {
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: t("exportToExcel.suppliesInventory"),
    };

    try {
      setPageLoading(true);
      let searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
      let { store } = this.state;

      searchObject.reportType = reportType?.code;
      searchObject.storeId = store?.id;
      searchObject.keyword = this.state.keyword.trim();
      searchObject.fromDate = formatDateDto(this.state.fromDate);
      searchObject.toDate = formatDateDto(this.state.toDate);

      await functionExportToExcel(
        exportToExcelPrintSuppliesInventory,
        searchObject,
        fileName[currentOrg?.printCode] || t("exportToExcel.suppliesUtilization"),
        setPageLoading,
      )
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }
  }

  handleGetDocumentTemplates = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let data = await handleGetTemplatesByModel(PRINT_TEMPLATE_MODEL.SUPPLIES_MANAGEMENT.SUPPLIES_INVENTORY);
      if (data) {
        this.setState({
          listDocumentTemplates: data,
        })
      }
    } catch (e) {
      console.error(e)
    } finally {
      setPageLoading(false);
    }
  }

  handleSelect = (value, name) => {
    this.setState({
      [name]: value
    }, () => {
      this.updatePageData();
    })
  }

  handleView = (rowData) => {
    let { setPageLoading } = this.context;
    let { reportType } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {};
      searchObject.productId = rowData?.productId;
      searchObject.reportType = reportType?.code;
      searchObject.fromDate = convertFromToDate(this?.state?.fromDate).fromDate;
      searchObject.toDate = convertFromToDate(this?.state?.toDate).toDate;
      if (reportType?.code === appConst.INVENTORY_REPORT_TYPE.UNIT_PRICE.code) {
        searchObject = {
          ...searchObject,
          unitPrice: rowData?.unitPrice || null
        };
      }

      getVoucherById(searchObject).then((res) => {
        let { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          data.productId = rowData?.productId;
          this.setState({
            item: data,
            shouldOpenEditorDialog: true,
          });
        } else {
          handleThrowResponseMessage(res);
        }
      });
    } catch (error) {
      console.error(error)
    } finally {
      setPageLoading(false);
    }
  }

  handleGetInventorySummary = async () => {
    let {setPageLoading} = this.context;
    let {reportType} = this.state;
    let {t} = this.props;

    try {
      setPageLoading(true);
      let searchObject = {};
      searchObject.storeId = this.state.store?.id;
      searchObject.keyword = this.state.keyword.trim();
      searchObject.fromDate = formatDateDto(this.state.fromDate);
      searchObject.toDate = formatDateDto(this.state.toDate);
      
      await getInventorySummary(searchObject).then((res) => {
        let { code, data } = res?.data;
        if (isSuccessfulResponse(code)) {
          this.setState({
            reportSummary: data,
          });
        } else {
          handleThrowResponseMessage(res);
        }
      });
    } catch (error) {
      toast.error(t("InventoryReport.errorWhenGetSummary"))
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      itemList,
      item,
      shouldOpenEditorDialog,
      fromDate,
      toDate,
      listDataHD,
      shouldOpenPrintDialog,
      dataView,
      totalElements,
      rowsPerPage,
      page,
      reportSummary,
    } = this.state;
    let TitlePage = t("InventoryReport.title");
    const { currentOrg } = this.context;

    const searchObjectStore = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_STORE.HOAT_DONG.code,
    };
    let searchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      type: appConst.OBJECT_HD_TYPE.KIEM_KE.code,
    };
    const { INVENTORY_REPORT } = LIST_PRINT_FORM_BY_ORG.SUPPLIES_MANAGEMENT;
    const isBvc = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code;
    const moneySeparation = isBvc ? appConst.TYPES_MONEY.SPACE.code : appConst.TYPES_MONEY.COMMA.code;
    const isUnitPriceType = this.state.reportType?.code === appConst.INVENTORY_REPORT_TYPE.UNIT_PRICE.code;

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.materialManagement") },
            { name: t("InventoryReport.inventory_report"), path: "report/inventory_report" }]} />
        </div>
        <Grid container spacing={1} justifyContent="space-between">
          <Grid item md={4} sm={12} xs={12} mb-10>
            <Button
              className="mt-12 mr-16"
              variant="contained"
              color="primary"
              onClick={this.exportToExcel}
            >
              {t('general.exportToExcel')}
            </Button>
            <Button
              className="mt-12"
              variant="contained"
              color="primary"
              onClick={this.printVouchers}
            >
              {t('general.print')}
            </Button>
          </Grid>
          <Grid item md={4} sm={12} xs={12} >
            <div style={{ marginTop: '16px' }}>
              <FormControl fullWidth>
                <Input
                  className='search_box w-100'
                  onChange={this.handleTextChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={(e) => handleKeyUp(e, this.updatePageData)}
                  placeholder={t("InventoryReport.filter")}
                  id="search_box"
                  startAdornment={
                    <InputAdornment position="end">
                      <SearchIcon onClick={() => this.search(keyword)} className="searchTable" />
                    </InputAdornment>
                  }
                />
              </FormControl>
            </div>
          </Grid>

          <Card elevation={2} className="w-100 mt-8">
            <CardContent>
              <Grid item container spacing={2} md={12} xs={12} lg={12}>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span style={{ color: 'red' }}>  </span>
                          {t("InventoryReport.fromDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={'fromDate'}
                      value={fromDate}
                      maxDate={toDate ? new Date(toDate) : undefined}
                      onChange={date => this.handleDateChange(date, "fromDate")}
                      minDateMessage={t("general.minDateDefault")}
                      maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                    <KeyboardDatePicker
                      fullWidth
                      margin="none"
                      id="mui-pickers-date"
                      label={
                        <span>
                          <span style={{ color: 'red' }}>  </span>
                          {t("InventoryReport.toDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={false}
                      format="dd/MM/yyyy"
                      name={'toDate'}
                      value={toDate}
                      minDate={fromDate ? new Date(fromDate) : undefined}
                      maxDateMessage={t("general.maxDateMessage")}
                      minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      onChange={date => this.handleDateChange(date, "toDate")}
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <ValidatorForm onSubmit={() => { }}>
                    <AsynchronousAutocomplete
                      label={t('Asset.store')}
                      searchFunction={storesSearchByPage}
                      searchObject={searchObjectStore}
                      defaultValue={this.state.store}
                      displayLable={'name'}
                      value={this.state.store}
                      onSelect={this.selectStore}
                    />
                  </ValidatorForm>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <ValidatorForm onSubmit={() => { }}>
                    <AsynchronousAutocompleteSub
                      searchFunction={searchByPageCouncil}
                      typeReturnFunction='category'
                      searchObject={searchObject}
                      label={t('InventoryCountVoucher.InventoryCountBoard')}
                      listData={listDataHD}
                      setListData={(e) => this.setListDataHD(e)}
                      displayLable={"name"}
                      onSelect={this.handleChangeSelectInventoryBoard}
                      value={this.state?.inventoryBoard}
                      InputProps={{
                        readOnly: true,
                      }}
                      noOptionsText={t("general.noOption")}
                    />
                  </ValidatorForm>
                </Grid>
                <Grid item md={4} sm={6} xs={12} lg={3}>
                  <ValidatorForm onSubmit={() => { }}>
                    <AsynchronousAutocompleteSub
                      searchFunction={() => { }}
                      listData={appConst.listInventoryReportType}
                      searchObject={searchObject}
                      label={t('InventoryReport.summaryType')}
                      displayLable={"name"}
                      name="reportType"
                      onSelect={this.handleSelect}
                      value={this.state?.reportType}
                      noOptionsText={t("general.noOption")}
                    />
                  </ValidatorForm>
                </Grid>
              </Grid>
            </ CardContent>
          </Card>
          <Grid item xs={12}>

            {shouldOpenEditorDialog && (
              <InventoryReportEditorDialog t={t} i18n={i18n}
                handleClose={this.handleDialogClose}
                open={shouldOpenEditorDialog}
                handleOKEditClose={this.handleOKEditClose}
                item={item}
              />
            )}
            {shouldOpenPrintDialog && (
              currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code ? (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenPrintDialog}
                  item={this.state.item}
                  title={t("Phiếu nhập kho")}
                  model={PRINT_TEMPLATE_MODEL.SUPPLIES_MANAGEMENT.INVENTORY_REPORT}
                  externalConfig={{
                    params: this.state.item?.externalParams,
                  }}
                />
              ) : (
                <PrintMultipleFormDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenPrintDialog}
                  item={dataView}
                  title={t("Phiếu in kiểm kê vật tư")}
                  urls={[
                    ...INVENTORY_REPORT.GENERAL,
                    ...(INVENTORY_REPORT[currentOrg?.printCode] || []),
                  ]}
                  exportExcelFunction={this.handleExportExcelPrintSuppliesInventory}
                />
              )
            )}

            <div className="MuiPaper-root MuiPaper-elevation2 MuiPaper-rounded">
              <TableContainer style={{ maxHeight: 400 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead className="position-sticky top-0 left-0" style={{ zIndex: 2 }}>
                    <TableRow>
                      <TableCell align="center" rowSpan="2" style={{ minWidth: 80 }}>
                        {t("general.action")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2">
                        STT
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("Product.name")}
                      </TableCell>
                      <TableCell align="center" rowSpan="2" className="border-1">
                        {t("WarehouseInventory.sku")}
                      </TableCell>
                      {isUnitPriceType && (
                        <TableCell align="center" rowSpan="2" className="border-1">
                          {t("InventoryDeliveryVoucher.price")}
                        </TableCell>
                      )}
                      <TableCell align="center" colSpan="2" className="border-1">
                        {t("WarehouseInventory.remainingQuantityFromDate")}
                      </TableCell>
                      <TableCell align="center" colSpan="2" className="border-1">
                        {t("WarehouseInventory.duringPeriod")}
                      </TableCell>
                      <TableCell align="center" colSpan="2" className="border-1">
                        {t("WarehouseInventory.exportPeriod")}
                      </TableCell>
                      <TableCell align="center" colSpan="2" className="border-1">
                        {t("WarehouseInventory.remainingQuantityToDate")}
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                      </TableCell>
                      <TableCell align="center" className="border-1">
                        {t("WarehouseInventory.total")}
                      </TableCell>
                      <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                      </TableCell>
                      <TableCell align="center" className="border-1">
                        {t("WarehouseInventory.total")}
                      </TableCell>
                      <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                      </TableCell>
                      <TableCell align="center" className="border-1">
                        {t("WarehouseInventory.total")}
                      </TableCell>
                      <TableCell align="center">
                        {t("WarehouseInventory.quantity")}
                      </TableCell>
                      <TableCell align="center" className="border-1">
                        {t("WarehouseInventory.total")}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {itemList?.length > 0 && itemList?.map((item, index) => (
                      <TableRow key={index}>
                        <TableCell align="center">
                          <MaterialButton
                            item={item}
                            onSelect={(item, method) => {
                              if (method === appConst.active.view) {
                                this.handleView(item);
                              }
                            }}
                          />
                        </TableCell>
                        <TableCell align="center">
                          {index + 1}
                        </TableCell>
                        <TableCell align="left">
                          {item?.productName}
                        </TableCell>
                        <TableCell align="center">
                          {item?.skuName}
                        </TableCell>
                        {isUnitPriceType && (
                          <TableCell align="right">
                            {convertMoney(item?.unitPrice, moneySeparation) || 0}
                          </TableCell>
                        )}
                        <TableCell align="center">
                          {convertMoney(item?.soLuongDauKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                          {convertMoney(item?.thanhTienDauKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="center">
                          {convertMoney(item?.soLuongNhapTrongKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                          {convertMoney(item?.thanhTienNhapTrongKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="center">
                          {convertMoney(item?.soLuongXuatTrongKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                          {convertMoney(item?.thanhTienXuatTrongKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="center">
                          {convertMoney(item?.soLuongCuoiKy, moneySeparation) || 0}
                        </TableCell>
                        <TableCell align="left" className="text-align-right">
                          {convertMoney(item?.thanhTienCuoiKy, moneySeparation) || 0}
                        </TableCell>
                      </TableRow>
                    ))}

                    <TableRow
                      className="position-sticky left-0"
                      style={{
                        zIndex: 2,
                        backgroundColor: "rgb(228 240 246)",
                        bottom: -1,
                      }}
                    >
                      <TableCell align="left" colSpan={isUnitPriceType ? 5 : 4}><b>Tổng</b> (Tất cả sản phẩm)</TableCell>
                      <TableCell align="center" className="font-weight-bold" colSpan={1}>
                        {convertMoney(reportSummary?.soLuongDauKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
                        {convertMoney(reportSummary?.thanhTienDauKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell align="center" className="font-weight-bold" colSpan={1}>
                        {convertMoney(reportSummary?.soLuongNhapTrongKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
                        {convertMoney(reportSummary?.thanhTienNhapTrongKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell align="center" className="font-weight-bold" colSpan={1}>
                        {convertMoney(reportSummary?.soLuongXuatTrongKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
                        {convertMoney(reportSummary?.thanhTienXuatTrongKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell align="center" className="font-weight-bold" colSpan={1}>
                        {convertMoney(reportSummary?.soLuongCuoiKy, moneySeparation) || 0}
                      </TableCell>
                      <TableCell className="font-weight-bold text-align-right" colSpan={1}>
                        {convertMoney(reportSummary?.thanhTienCuoiKy, moneySeparation) || 0}
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

InventoryReportTable.contextType = AppContext;
export default InventoryReportTable;