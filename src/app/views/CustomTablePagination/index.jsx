import React from "react";
import { useTranslation } from "react-i18next";
import TablePagination from "@material-ui/core/TablePagination";
import {labelDisplayedRows} from "../../appFunction";
import {appConst} from "../../appConst";
const CustomTablePagination = ({
  totalElements = 0,
  rowsPerPage = 10,
  page = 0,
  handleChangePage = () => {},
  setRowsPerPage = () => {},
  rowsPerPageOptions = appConst.rowsPerPageOptions.table,
  ...props
}) => {
  const { t } = useTranslation();

  return (
    <TablePagination
      align="left"
      className="px-16"
      rowsPerPageOptions={rowsPerPageOptions}
      labelRowsPerPage={t("general.rows_per_page")}
      labelDisplayedRows={labelDisplayedRows}
      component="div"
      count={totalElements}
      rowsPerPage={rowsPerPage}
      page={page}
      backIconButtonProps={{
        "aria-label": "Previous Page",
      }}
      nextIconButtonProps={{
        "aria-label": "Next Page",
      }}
      onPageChange={handleChangePage}
      onRowsPerPageChange={setRowsPerPage}
      {...props}
    />
  );
};
export default CustomTablePagination;
