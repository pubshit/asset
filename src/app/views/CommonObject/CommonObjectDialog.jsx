import { Grid, DialogActions, Button, Radio, Dialog } from "@material-ui/core";
import React from "react";
import { getByPage, saveItem, checkCode, searchByPage } from "./CommonObjectService";
import * as commonObjectTypeService from "./../CommonObjectType/CommonObjectTypeService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { appConst, variable } from "app/appConst";
import { handleKeyDownSpecial, isSuccessfulResponse} from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class CommonObjectDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    type: null,
    keyword: "",
    shouldOpenNotificationPopup: false,
    Notification: "",
    name: null,
    code: null,
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };
  handleChangeCommonObjectType = (value, source) => {
    this.setState({
      [source]: value
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
    this.updatePageData();
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };
  updatePageData = () => {
    var searchObject = {};
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject.pageIndex, searchObject.pageSize).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  componentDidMount() {
    this.updatePageData(this.state.page, this.state.rowsPerPage);
  }

  handleClick = (event, item) => {
    if (item.id != null) {
      this.setState({ selectedValue: item.id, selectedItem: item });
    } else {
      this.setState({ selectedValue: item.id, selectedItem: null });
    }
  };

  async componentWillMount() {

    try {
      let searchObject = {
        pageIndex: 0,
        pageSize: 1000,
        text: "",
      }

      let res =  await commonObjectTypeService.searchByPageSelect(searchObject)
      if (res?.data?.content && isSuccessfulResponse(res?.status)) {
        this.setState({
          listType: res?.data?.content || [],
          type: res?.data?.content?.find(x => x.code === (
            this.props.typeCode || appConst.listCommonObject.HANG_SAN_XUAT.code
          ))
        })
      }

    } catch {
      toast.warning(t("general.error"));
    } finally {
      let { item } = this.props;
      this.setState({ ...item }, () => {
        let { type } = this.state;
        if (type && type.id) {
          this.setState({ typeId: type.id });
        }
      });
    }
  }

  search = (keyword) => {
    var searchObject = {};
    searchObject.text = keyword;
    searchObject.pageIndex = this.state.page;
    searchObject.pageSize = this.state.rowsPerPage;
    getByPage(searchObject).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  handleChange(event) {
    this.setState({ keyword: event.target.value });
  }

  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };
  handleChangeCode = (event) => {
    this.setState({ code: event.target.value });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    let { t, selectManufacturer = () => { } } = this.props;
    let { fromCommonObjectPopup } = this.props;
    if (fromCommonObjectPopup) {
      if (this.state.name === null) {
        toast.warning("Chưa chọn tên.");
        return;
      }
      if (this.state.code === null) {
        toast.warning("Chưa chọn mã.");
        return;
      }
    }
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(t("CommonObject.noti.dupli_code"));
      } else {
        saveItem({ ...this.state }).then((res) => {
          toast.success(t("general.addSuccess"));
          selectManufacturer(res.data);
          this.props.handleClose();
        });
        this.props.handleOKEditClose();
      }
    }).catch((e) => {
      toast.error(t("general.error"));
    });
  };

  handleSetDataAsset = () => {
    if (this.props.handleSetDataSelect) this.props.handleSetDataSelect([]);
  };

  render() {
    const { t, open } = this.props;
    let { name, code, shouldOpenNotificationPopup, type } =
      this.state;
    let columns = [
      { title: t("CommonObject.name"), field: "name", width: "150" },
      {
        title: t("CommonObject.vode"),
        field: "code",
        align: "left",
        width: "150",
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "250",
        render: (rowData) => (
          <Radio
            name="radSelected"
            value={rowData.id}
            checked={this.state.selectedValue === rowData.id}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="sm"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t("CommonObject.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={7} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("CommonObjectType.name")}
                    </span>
                  }
                  onChange={this.handleChangeName}
                  type="text"
                  name="name"
                  value={name || ""}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={5} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("CommonObjectType.code")}
                    </span>
                  }
                  onChange={this.handleChangeCode}
                  type="text"
                  name="code"
                  value={code || ""}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode"),]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("CommonObjectType.title")}
                    </span>
                  }
                  searchObject={{}}
                  searchFunction={()=>{}}
                  listData={this.state.listType}
                  value={type}
                  disabled={this.props?.isTSCD || this.props.typeCode}
                  displayLable="name"
                  onSelect={(value) => this.handleChangeCommonObjectType(value, "type")}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {this.props.fromCommonObjectPopup ? (
                <Button
                  className="mr-15"
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              ) : (
                <Button
                  className="mr-15"
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              )}
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default CommonObjectDialog;
