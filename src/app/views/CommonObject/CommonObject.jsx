import {
  Grid, IconButton, Icon, Button, TablePagination, FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import React from "react";
import MaterialTable, { MTableToolbar } from 'material-table';
import { useTranslation } from 'react-i18next';
import { getItemById, searchByText, deleteCheckItem } from "./CommonObjectService";
import CommonObjectDialog from "./CommonObjectDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { Helmet } from 'react-helmet';
import SearchIcon from '@material-ui/icons/Search';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { LightTooltip } from "../Component/Utilities";
import { ValidatorForm } from "react-material-ui-form-validator";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchByPageObject } from "../CommonObjectType/CommonObjectTypeService";
import { defaultPaginationProps, handleKeyDown, labelDisplayedRows } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">delete</Icon>
      </IconButton>
    </LightTooltip>
  </div>;
}
class CommonObject extends React.Component {
  state = {
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    keyword: '',
    Notification: "",
    commonObjectType: null
  }
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  handleTextChange(event) {
    this.setState({ keyword: event.target.value });
  }
  search = () => {
    this.setPage(0);
  }

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  componentDidMount() {
    this.updatePageData();
  }

  updatePageData = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let searchObject = {};
    searchObject.keyword = this.state?.keyword?.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.typeCode = this.state?.commonObjectType?.code;
    try {
      setPageLoading(true);
      let res = await searchByText(
        searchObject.keyword,
        searchObject.pageIndex,
        searchObject.pageSize,
        searchObject
      )
      const { data } = res;
      if (data) {
        this.setState({
          itemList: [...data.content],
          totalElements: data.totalElements
        })
      }
    }
    catch (e) {
      toast.error(t("general.error"))
    }
    finally {
      setPageLoading(false);
    }
  };
  setPage = page => {
    this.setState({ page }, () => {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, () => {
      this.updatePageData();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    }, () => {
      this.setPage(0);
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props
    if (this.state.itemList.length === 1 && this.state.page === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count
      })
    }
    deleteCheckItem(this.state.id)
      .then((res) => {
        if (res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.deleteSuccess"));
          this.updatePageData();
          this.handleDialogClose()
        }
        else {
          toast.warning(t('CommonObject.noti.use'));
          this.updatePageData();
          this.handleDialogClose()
        }
      }).catch((err) => {
        toast.warning(t('CommonObject.noti.use'));
        this.handleDialogClose()
      })
  };
  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };
  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props
    for (let i = 0; i < list.length; i++) {
      try {
        let res = await deleteCheckItem(list[i].id);
        if (res?.data?.code !== appConst.CODE.SUCCESS) {
          listAlert.push(list[i].name);
        }
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    this.handleDialogClose()
    if (listAlert.length === list.length) {
      toast.warning(t('CommonObject.noti.use_all'));
    } else if (listAlert.length > 0) {
      toast.warning(t('CommonObject.noti.deleted_unused'));
    }
  }
  handleDeleteAll = (event) => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.data = null;
    }
    );
  };

  handleKeyUp = (event) => {
    if (!event?.target?.value) {
      this.updatePageData();
    }
  };
  selectCommonObjectType = (data) => {
    this.setState({ commonObjectType: data }, () => {
      this.updatePageData();
    })
  }
  render() {
    const { t, i18n } = this.props;
    let { keyword, commonObjectType } = this.state;
    let TitlePage = t("CommonObject.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 70,
        maxWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: rowData => <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              getItemById(rowData.id).then(({ data }) => {
                if (data.parent === null) {
                  data.parent = {};
                }
                this.setState({
                  item: data,
                  shouldOpenEditorDialog: true
                });
              })
            } else if (method === appConst.active.delete) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      {
        title: t("CommonObject.code"),
        field: "code",
        align: "left",
        maxWidth: "100px",
        cellStyle: {
          textAlign: "center",
        }
      },
      { title: t("CommonObject.name"), field: "name", maxWidth: "350px" },
      { title: t("CommonObject.type"), field: "type.name", align: "left", maxWidth: "300px" },

    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.category"), path: "/list/CommonObject" },
              { name: TitlePage }
            ]}
          />
        </div>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item container xs={12} spacing={2} >
            <Grid item md={4} xs={12} >
              <Button
                className="mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleEditItem(null)}
              >
                {t('general.add')}
              </Button>
            </Grid>
            <Grid item md={3} xs={12} style={{
              transform: "translateY(-15px)"
            }}>
              <ValidatorForm onSubmit={() => { }}>
                <AsynchronousAutocompleteSub
                  label={t("CommonObjectType.title")}
                  searchFunction={searchByPageObject}
                  searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                  displayLable={'name'}
                  value={commonObjectType ? commonObjectType : null}
                  onSelect={this.selectCommonObjectType}
                  noOptionsText={t("general.noOption")}
                />
              </ValidatorForm>
            </Grid>
            <Grid item md={5} sm={12} xs={12} >
              <FormControl fullWidth>
                <Input
                  className='search_box w-100'
                  onChange={this.handleTextChange}
                  onKeyDown={this.handleKeyDownEnterSearch}
                  onKeyUp={this.handleKeyUp}
                  placeholder={t("CommonObject.filter")}
                  id="search_box"
                  startAdornment={
                    <InputAdornment position="end">
                      <SearchIcon onClick={this.search} className="searchTable" />
                    </InputAdornment>
                  }
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <div>
              {this.state.shouldOpenEditorDialog && (
                <CommonObjectDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={this.state.shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={this.state.item}
                />
              )}

              {this.state.shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={this.state.shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={this.state.itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              labelDisplayedRows={labelDisplayedRows}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>

    )
  }
}
CommonObject.contextType = AppContext;
export default CommonObject;