import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  addNewOrUpdateInventoryCountStatus,
  checkCode,
} from "./InventoryCountStatusService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {appConst, variable} from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class InventoryCountStatusEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    indexOrder: 0,
    shouldOpenNotificationPopup: false,
    Notification: "",
  };
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    let {t} = this.props;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    checkCode(id, code).then((result) => {
      if (result.data) {
        this.setState({
          shouldOpenNotificationPopup: true,
          Notification: "InventoryCountStatus.noti.dupli_code",
        });
      } else {
        if (id) {
          addNewOrUpdateInventoryCountStatus({
            ...this.state,
          }).then(() => {
            toast.info(t("general.updateSuccess"));
            this.props.handleOKEditClose();
          });
        } else {
          addNewOrUpdateInventoryCountStatus({
            ...this.state,
          }).then(() => {
            toast.info(t("general.addSuccess"));
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentDidMount() {
    this.setState(
      {
        ...this.props.item,
      },
      function () {}
    );
  }

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };
  render() {
    let { open, t } = this.props;
    let {
      name,
      code,
      indexOrder,
      shouldOpenNotificationPopup,
    } = this.state;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="xs"
        fullWidth
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <h4 className="">{t("InventoryCountStatus.saveUpdate")}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={8} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("InventoryCountStatus.code")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={t("InventoryCountStatus.indexOrder")}
                  onChange={this.handleChange}
                  type="text"
                  name="indexOrder"
                  value={indexOrder}
                  validators={["minNumber:0", "matchRegexp:^[0-9]{1,9}$"]}
                  errorMessages={[
                    t("general.invalidFormat"),
                    t("general.invalidFormat"),
                  ]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("InventoryCountStatus.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default InventoryCountStatusEditorDialog;
