import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH =
  ConstantList.API_ENPOINT +
  "/api/inventory_count_status" +
  ConstantList.URL_PREFIX;

export const getAllInventoryCountStatus = () => {
  return axios.get(API_PATH + "/1/100000");
};

export const getInventoryCountStatusByPage = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH + "/" + params;
  return axios.get(url);
};
export const searchByPage = (keyword, pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  return axios.post(API_PATH + "/searchByText/" + params, keyword);
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateInventoryCountStatus = (InventoryCountStatus) => {
  return axios.post(API_PATH, InventoryCountStatus);
};
