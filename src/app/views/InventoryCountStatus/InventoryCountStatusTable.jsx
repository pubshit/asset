import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import {
  getInventoryCountStatusByPage,
  getItemById,
  searchByPage,
  deleteCheckItem,
} from "./InventoryCountStatusService";
import InventoryCountStatusEditorDialog from "./InventoryCountStatusEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import Tooltip from "@material-ui/core/Tooltip";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import localStorageService from "app/services/localStorageService";
import ConstantList from "../../appConfig";
import { appConst } from "app/appConst";
import { defaultPaginationProps } from "app/appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: "-1.5em",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { hasDeletePermission } = props
  return (
    <div className="none_wrap">
      <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>
      {
        hasDeletePermission && <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class InventoryCountStatusTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 10,
    page: 0,
    InventoryCountStatus: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    Notification: "",
    hasDeletePermission: false
  };
  numSelected = 0;
  rowCount = 0;

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  handleKeyUp = (e) => {
    if (!e.target.value) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  }

  updatePageData = () => {
    getInventoryCountStatusByPage(
      this.state.page + 1,
      this.state.rowsPerPage
    ).then(({ data }) => {
      this.setState({
        itemList: [...data.content],
        totalElements: data.totalElements,
      });
    });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    });
    this.updatePageData();
  };

  handleDeleteInventoryCountStatus = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditInventoryCountStatus = (item) => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteCheckItem(this.state.id)
      .then((res) => {
        toast.info(this.props.t("general.deleteSuccess"));
        this.handleDialogClose();
        this.updatePageData();
      })
      .catch(() => {
        this.setState({
          shouldOpenNotificationPopup: true,
          Notification: "InventoryCountStatus.noti.use",
        });
      });
  };

  componentDidMount() {
    this.updatePageData();
    this.getRoleCurrentUser()
  }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { InventoryCountStatus } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < InventoryCountStatus.length; i++) {
      if (
        InventoryCountStatus[i].checked == null ||
        InventoryCountStatus[i].checked == false
      ) {
        selectAllItem = false;
      }
      if (InventoryCountStatus[i].id == item.id) {
        InventoryCountStatus[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      InventoryCountStatus: InventoryCountStatus,
    });
  };
  handleSelectAllClick = (event) => {
    let { InventoryCountStatus } = this.state;
    for (var i = 0; i < InventoryCountStatus.length; i++) {
      InventoryCountStatus[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      InventoryCountStatus: InventoryCountStatus,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser.roles.forEach((role) => {
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
      });
    }
  };

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenNotificationPopup,
      hasDeletePermission
    } = this.state;
    let TitlePage = t("InventoryCountStatus.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        headerStyle: {
          padding: "0px",
        },
        cellStyle: {
          padding: "0px",
          textAlign: "center"
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === 0) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data === null) {
                    data = {};
                  }
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  });
                });
              } else if (method === 1) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      { title: t("InventoryCountStatus.code"), field: "code", width: "150" },
      {
        title: t("InventoryCountStatus.name"),
        field: "name",
        align: "left",
        width: "150",
      },
      {
        title: t("InventoryCountStatus.indexOrder"),
        field: "indexOrder",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center"
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {" "}
            {t("Asset.inventory_count_status")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Asset.title"), path: "/asset/inventory_count_status" },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                });
              }}
            >
              {t("general.add")}
            </Button>

            {shouldOpenNotificationPopup && (
              <NotificationPopup
                title={t("general.noti")}
                open={shouldOpenNotificationPopup}
                // onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDialogClose}
                text={t(this.state.Notification)}
                agree={t("general.agree")}
              />
            )}
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("InventoryCountStatus.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.search} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <InventoryCountStatusEditorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                toolbar: {
                  nRowsSelected: `${t("general.selects")}`,
                },
              }}
              options={{
                selection: false,
                sorting: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "450px",
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default InventoryCountStatusTable;
