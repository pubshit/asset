import React, { useState } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {
  LightTooltip,
  TabPanel,
  a11yProps,
  checkInvalidDate,
  convertNumberPriceRoundUp,
  handleKeyDownIntegerOnly, filterOptions
} from "app/appFunction";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { appConst } from "app/appConst";
import {
  DateTimePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import { createFilterOptions } from "@material-ui/lab";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import SelectAssetAllPopup from "../Component/InstrumentTools/SelectInstrumentToolsAllPopup";
import { searchByPage } from "../Council/CouncilService";
import {getListUserByDepartmentId} from "../AssetTransfer/AssetTransferService";
import {searchByPage as searchByPageOrg} from "../Organization/OrganizationService";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, appConst.active.delete)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function TransferToAnotherUnitScrollableTabs(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isCheckStatusNew, setIsCheckStatusNew] = React.useState(false);
  const filterAutocomplete = createFilterOptions();
  const [listTransferPerson, setListTransferPerson] = useState(props?.item?.persons || [])
  let transferToAnotherUnitStatus = appConst.listStatusTransferToAnother.find(status => status.code === props?.item?.transferToAnotherUnitStatus) || "";
  const [listTransferCountBoard, setListTransferCountBoard] = useState(null);
  const [transferBoard, setTransferBoard] = useState([])
  let searchObject = {
    keyword: "",
    pageIndex: 1,
    pageSize: 1000000,
    type: appConst.OBJECT_HD_TYPE.CHUYEN_DI.code,
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleRowDataCellDelete = (item) => {
    let newDataRow = listTransferPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetTransferToAnotherUnitUserList(newDataRow)
    setListTransferPerson(newDataRow || []);
  };

  const handleSelectHandoverDepartment = (item, rowData) => {
    let newDataRow = listTransferPerson.map((i) => {
      if (i.tableData.id === rowData.tableData.id) {
        i.handoverDepartment = item;
        i.departmentId = item?.id;
        i.departmentName = item?.name
      }
      return i;
    });
    setListTransferPerson(newDataRow || []);
  };

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView) &&
        <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData.asset.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      )
    },
    {
      title: t("Asset.stt"),
      field: "asset.code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props.item.page) * props.item.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("InstrumentToolsList.code"),
      field: "asset.code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("TransferToAnotherUnit.quantity"),
      field: "asset.quantity",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          fullWidth
          value={rowData.asset?.quantity || 0}
          onKeyDown={handleKeyDownIntegerOnly}
          onChange={(event) => props.handleRowDataCellChange(
            event?.target?.value,
            event?.target?.name,
            rowData,
          )}
          type="number"
          name="quantity"
          InputProps={{
            readOnly: props?.item?.isView,
            inputProps: {
              className: "text-center",
            },
          }}
          validators={[
            "minNumber:1",
            "required",
            `maxNumber:${rowData.asset?.inventoryQuantity}`
          ]}
          errorMessages={[
            t("general.minNumberError"),
            t("general.required"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
          ]}
        />
      ),
    },
    {
      title: t("InstrumentToolsList.availableQuantity"),
      field: "asset.inventoryQuantity",
      align: "left",
      minWidth: 60,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPriceRoundUp(rowData?.asset?.originalCost)
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 130,
      render: rowData => rowData?.asset?.serialNumber || rowData?.asset?.assetSerialNumber
    },
    {
      title: t("InstrumentToolsList.model"),
      field: "asset.model",
      align: "left",
      minWidth: 130,
      render: rowData => rowData?.asset?.model || rowData?.asset?.assetModel
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "asset.yearPutIntoUse",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => rowData?.asset?.yearPutIntoUse || rowData?.asset?.assetYearPutIntoUse
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: rowData => rowData?.asset?.yearOfManufacture || rowData?.asset?.assetYearOfManufacture
    },
  ];

  const columnsTransferCount = [
    {
      title: t("TransferToAnotherUnit.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.transferToAnotherUnitStatus !== appConst.listStatusTransferToAnotherObject.DA_CHUYEN_DI.code ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                onClick={() => handleRowDataCellDelete(rowData)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      },
    ] : []),
    {
      title: t("TransferToAnotherUnit.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
      cellStyle: {
      }
    }
  ]

  let columnsAssetFile = [
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      (
        // !props?.item?.isView && 
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      )
    },
    {
      title: t("general.stt"),
      field: "code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1 || '',
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 350,
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name)
      return;
    }
  }
  const handleChangeSelecttransferBoard = (item) => {
    setTransferBoard(item);
    props.handleSetTransferToAnotherUnitUserList(item?.hdChiTiets)
    props.handleSetHoiDongId(item?.id)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setListTransferPerson(item?.hdChiTiets || []);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} />
          <Tab label={t("Người chuyển đi")} {...a11yProps(1)} />
          <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={2} sm={6} xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <DateTimePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t('Ngày tạo phiếu')}
                  </span>}
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                name={'createDate'}
                value={props?.item?.createDate}
                InputProps={{
                  readOnly: true,
                }}
                readOnly={true}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={6} xs={6}>
            <CustomValidatePicker
              fullWidth
              margin="none"
              id="mui-pickers-date"
              label={
                <span className="w-100">
                  <span className="colorRed">*</span>
                  {t('TransferToAnotherUnit.issueDate')}
                </span>
              }
              inputVariant="standard"
              type="text"
              autoOk={true}
              format="dd/MM/yyyy"
              name={'issueDate'}
              value={props?.item?.issueDate}
              onChange={date => props.handleDateChange(date, "issueDate")}
              readOnly={props?.item?.isView}
              minDate={new Date("1/1/1900")}
              invalidDateMessage={"Ngày không tồn tại"}
              validators={!isCheckStatusNew && ['required']}
              errorMessages={!isCheckStatusNew && t('general.required')}
              minDateMessage={"Ngày chuyển đi không được nhỏ hơn ngày 01/01/1900"}
              onBlur={() => handleBlurDate(props?.item?.issueDate, "issueDate")}
              clearable
              clearLabel={t("general.remove")}
              cancelLabel={t("general.cancel")}
              okLabel={t("general.select")}
              maxDate={new Date()}
              maxDateMessage={t("allocation_asset.maxDateMessage")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            {(props?.item?.isRoleManager || props?.item?.isView) ?
              < TextValidator
                className="w-100"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    <span> {t("allocation_asset.handoverDepartment")}</span>
                  </span>
                }
                value={props.item?.handoverDepartment ? `${props.item?.handoverDepartment?.name} - ${props.item?.handoverDepartment?.code}` : ""}
                InputProps={{
                  readOnly: true,
                }}
              />
              :
              <AsynchronousAutocompleteSub
                label={
                  <span>
                    <span className="colorRed">*</span>
                    <span> {t("allocation_asset.handoverDepartment")}</span>
                  </span>
                }
                searchFunction={getListOrgManagementDepartment}
                searchObject={props.handoverPersonSearchObject}
                listData={props.item.listHandoverDepartment}
                setListData={props.handleSetDataHandoverDepartment}
                defaultValue={props.item?.handoverDepartment ? props.item?.handoverDepartment : null}
                displayLable={'name'}
                value={props.item?.handoverDepartment ? props.item?.handoverDepartment : null}
                onSelect={props.selectHandoverDepartment}
                validators={["required"]}
                errorMessages={[t('general.required')]}
                typeReturnFunction="list"
                showCode={"showCode"}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
              />
            }
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  <span> {t("allocation_asset.handoverPerson")}</span>
                </span>
              }
              searchFunction={getListUserByDepartmentId}
              searchObject={props.handoverPersonSearchObject}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.handoverPerson ?? null}
              onSelect={props?.handleSelectHandoverPerson}
              filterOptions={filterOptions}
              disabled={!props?.item?.handoverDepartment?.id || props.item?.isView}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              name="code"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.code")}
              InputProps={{
                readOnly: true,
              }}
              disabled={!props.item?.code}
              value={props.item?.code || ""}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              className="w-100"
              name="name"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.name")}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              value={props.item?.name || ""}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  <span> {t("TransferToAnotherUnit.status")}</span>
                </span>
              }
              searchFunction={() => { }}
              listData={appConst.listStatusTransferToAnother}
              setListData={props.handleSetDataHandoverPerson}
              displayLable={"name"}
              readOnly={props?.item?.isView}
              value={transferToAnotherUnitStatus || null}
              onSelect={props.handleChangeSelect}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("TransferToAnotherUnit.receiverOrg")}
              searchFunction={searchByPageOrg}
              searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
              listData={props.item?.listOrganization}
              setListData={props.handleDateChange}
              nameListData="listOrganization"
              displayLable={"name"}
              readOnly={props?.item?.isView}
              typeReturnFunction="category"
              name="receiverOrg"
              value={props.item?.receiverOrg || null}
              onSelect={props.handleDateChange}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              name="transferAddress"
              onChange={props?.handleChange}
              label={
                <span>
                  <span className="colorRed">*</span>
                  <span> {t("TransferToAnotherUnit.transferAddress")}</span>
                </span>
              }
              value={props.item?.transferAddress ? props.item?.transferAddress : null}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
              validators={['required']}
              errorMessages={t('general.required')}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100"
              name="conclude"
              onChange={props?.handleChange}
              label={t("TransferToAnotherUnit.conclude")}
              value={props.item?.conclude ? props.item?.conclude : null}
              InputProps={{
                readOnly: props?.item?.isView,
              }}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            {!props?.item?.isView &&
              <Button variant="contained" color="primary" size="small" className="mb-16 w-100"
                onClick={props.openPopupSelectAsset}
              >
                {t('TransferToAnotherUnit.InstrumentsTool.choose')}
              </Button>
            }

            {props.item.shouldOpenAssetPopup && (
              <SelectAssetAllPopup
                t={t} i18n={i18n}
                open={props.item.shouldOpenAssetPopup}
                handleSelect={props.handleSelectAsset}
                isAssetTransfer={props.item.isAssetTransfer}
                statusIndexOrders={props.item.statusIndexOrders}
                handleClose={props.handleAssetPopupClose}
                handoverDepartment={props.item?.handoverDepartment}
                assetVouchers={props.item.assetVouchers ? props.item.assetVouchers : []}
                isLiquidate={true}
                dateOfReceptionTop={props?.item?.issueDate}
                filteredSelected={props.handleFilterSelected}
              />
            )}
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={props.item.assetVouchers ? props.item.assetVouchers : []}
              columns={columns}
              options={{
                sorting: false,
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                maxBodyHeight: '225px',
                minBodyHeight: '225px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>

      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction='category'
            searchObject={searchObject}
            label={t('TransferToAnotherUnit.transferCountBoard')}
            listData={listTransferCountBoard}
            displayLable={"name"}
            onSelect={handleChangeSelecttransferBoard}
            value={transferBoard}
            InputProps={{
              readOnly: true,
            }}
            disabled={props?.item?.isView || props?.item?.transferToAnotherUnitStatus === appConst.listStatusTransferToAnotherObject.DA_CHUYEN_DI.code}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              let filtered = filterAutocomplete(options, params);
              return filtered;
            }}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={listTransferPerson}
            columns={columnsTransferCount}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },

              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2} >
        <Grid item md={12} sm={12} xs={12}>
          {!props?.item?.isView &&
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={
                props.handleAddAssetDocumentItem
              }
            >
              {t('InstrumentToolsTransfer.addFile')}
            </Button>
          }
          {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocumentId={props.getAssetDocumentId}
              getAssetDocument={props.getAssetDocument}
              documentType={props.item?.documentType}
              handleUpdateAssetDocument={props.handleUpdateAssetDocument}
              item={props.item}
              t={t}
              i18n={i18n}
            />)
          }
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item.documents || []}
            columns={columnsAssetFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              sorting: false,
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              padding: 'dense',
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
              },
              maxBodyHeight: '290px',
              minBodyHeight: '290px',
            }}
            components={{
              Toolbar: props => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div >
  );
}