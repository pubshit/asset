import React from "react";
import {
  Dialog,
  Button,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { convertNumberPrice, convertNumberPriceRoundUp } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import Signature from "../FormCustom/Component/Signature";
import DateToText from "../FormCustom/Component/DateToText";

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center"
  },
  title: {
    fontWeight: 'bold',
    marginBottom: '0px'
  },
  text: {
    fontWeight: 'bold',
    marginTop: '0px'
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: 'bold'
  },
  table: {
    width: '100%',
    border: '1px solid',
    borderCollapse: 'collapse'
  },
  w_3: {
    border: '1px solid',
    width: '3%',
    textAlign: 'center'
  },
  w_5: {
    border: '1px solid',
    width: '5%',
    textAlign: 'center'
  },
  w_7: {
    border: '1px solid',
    width: '7%',
    textAlign: 'center'
  },
  w_9: {
    border: '1px solid',
    width: '9%',
    textAlign: 'center'
  },
  w_11: {
    border: '1px solid',
    width: '11%',
  },
  w_20: {
    border: '1px solid',
    width: '20%',
  },
  mt_25minus: {
    marginTop: -25
  },
  border: {
    border: '1px solid',
    padding: "0 5px"
  },
  name: {
    border: '1px solid',
    paddingLeft: "5px",
    textAlign: 'left'
  },
  sum: {
    border: '1px solid',
    paddingLeft: "5px",
    fontWeight: 'bold',
    textAlign: 'left'
  },
  represent: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 12%'
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px"
  },
  textCenter: {
    display: "flex",
    justifyContent: "center",
    fontWeight: 'bold',
    width: "33%"
  },
  pt_40: {
    paddingTop: 40
  },
  bold: {
    fontWeight: "bold",
  },
  alignCenter: {
    alignItems: "center"
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
  }
}
export default function InstrumentsToolTransferToAnotherUnitPrint(props) {
  let { open, handleClose, t, item } = props;
  let now = new Date(item?.issueDate);
  let currentUser = localStorageService.getSessionItem("currentUser");
  let arrOriginalCost = [];

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };
  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return sum;
  }

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth={true}>
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">{t('TransferToAnotherUnit.transferSlip')}</span>
      </DialogTitle>
      <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute' }} title="ifmcontentstoprint"></iframe>
      <ValidatorForm onSubmit={handleFormSubmit} className="validator-form-scroll-dialog">
        <DialogContent id='divcontents' style={{}} className="dialog-print">
          <div style={{ textAlign: 'center' }} className="form-print">
            <div
              style={{ display: "flex", justifyContent: "space-between", ...style.bold }}
            >
              <div style={{ textAlign: "left" }}>
                <div>Đơn vị:.....................</div>
                <div>Mã QHNS:..........................</div>
              </div>

              <div>Mẫu số: C50-HD</div>
            </div>

            <div style={{ margin: "30px 0" }}>
              <div style={style.title}>BIÊN BẢN GIAO NHẬN THIẾT BỊ</div>
              <DateToText isLocation noUppercase date={item?.issueDate} />
            </div>

            {
              <div>
                <div style={style.textAlignLeft}>Căn cứ Quyết định số: ....... ngày ...... tháng ...... năm ...... của .................. về việc bàn giao CCDC</div>
                <div style={style.textAlignLeft}>
                  <div>Bên giao nhận CCDC gồm:</div>

                  <table style={{
                    width: "100%",
                    borderCollapse: "collapse",
                    border: "none",
                    marginLeft: 20
                  }}>
                    <tbody style={{ border: "none", }}>
                      <tr style={{ border: "none" }}>
                        <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>- Đại diện bên giao: Ông/Bà: {item?.handoverDepartment?.name}.</th>
                        <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>Chức vụ: {"..".repeat(17)} </th>
                      </tr>
                      <tr style={{ border: "none" }}>
                        <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>- Đại diện bên nhận:Ông/Bà: {item?.handoverPersonName}.</th>
                        <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>Chức vụ: {"..".repeat(17)} </th>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div style={style.textAlignLeft}>Địa điểm giao nhận CCDC: {item?.transferAddress}</div>
                <div style={{ ...style.textAlignLeft, marginBottom: 5 }}>Xác nhận việc giao nhận CCDC như sau:</div>
              </div>
            }

            <div>
              {
                <table style={style.table} className="table-report">
                  <tr>
                    <th style={style.w_3}>STT</th>
                    <th style={style.w_11}>Mã CCDC</th>
                    <th style={style.w_20}>Tên CCDC</th>
                    <th style={style.w_5}>Nước SX</th>
                    <th style={style.w_5}>Năm SX</th>
                    <th style={style.w_5}>Năm SD</th>
                    <th style={style.w_5}>SL</th>
                    <th style={style.w_11}>Người tiếp nhận</th>
                    <th style={style.w_9}>Nguyên giá</th>
                  </tr>

                  {(item.assetVouchers?.length > 0) ? item.assetVouchers.map((row, index) => {
                    arrOriginalCost.push(row?.asset?.originalCost)
                    return (
                      <tr>
                        <td style={{ ...style.border, ...style.textAlignCenter }}>{index + 1 || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft }}>{row?.asset?.code || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft }}>{row?.asset?.name || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft }}>{row?.asset?.madeIn || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter }}>{row?.asset?.yearOfManufacture || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter }}>{row?.asset?.yearPutIntoUse || row?.assetYearPutIntoUser || ''}</td>
                        <td style={{ ...style.border, ...style.textAlignCenter }}>{row?.quantity ? row?.quantity : 1}</td>
                        <td style={{ ...style.border, ...style.textAlignLeft }}>{item?.handoverPersonName}</td>
                        <td style={{ ...style.border, ...style.textAlignRight }}>{convertNumberPriceRoundUp(row?.asset?.originalCost || 0)}</td>
                      </tr>
                    )
                  }) : ""}
                  <tr>
                    <td colSpan={2} style={{ ...style.border, ...style.textAlignLeft, ...style.forwarder }}>Tổng cộng:</td>
                    <td colSpan={7} style={{ ...style.border, ...style.textAlignRight }}>{convertNumberPriceRoundUp(sumArray(arrOriginalCost))}</td>
                  </tr>
                </table>
              }
            </div>
            {/* {
              <div>
                <div style={{ ...style.title, marginTop: 20 }}>DỤNG CỤ, PHỤ TÙNG KÈM THEO</div>
                <table style={style.table} className="table-report">
                  <tr>
                    <th style={style.w_3}>STT</th>
                    <th style={style.w_20}>Tên, quy cách dụng cụ, phụ tùng</th>
                    <th style={style.w_11}>Đơn vị tính</th>
                    <th style={style.w_3}>Số lượng</th>
                    <th style={style.w_9}>Giá trị</th>
                  </tr>
                  <tr>
                    <td style={{ ...style.border, ...style.textAlignLeft }}></td>
                    <td style={{ ...style.border, ...style.textAlignLeft }}></td>
                    <td style={{ ...style.border, ...style.textAlignCenter }}></td>
                    <td style={{ ...style.border, ...style.textAlignCenter }}></td>
                    <td style={{ ...style.border, ...style.textAlignRight }}></td>
                  </tr>
                </table>
              </div>
            } */}
            <div>
              <div style={style.signContainer}>
                <Signature
                  title="Thủ trưởng đơn vị"
                  note={"Bên nhận"}
                  sign="Ký, họ tên, đóng dấu"
                />
                <Signature
                  title="Kế toán trưởng"
                  note={"Bên nhận"}
                  sign="Ký, họ tên"
                />
                <Signature
                  title="Người nhận"
                  sign="Ký, họ tên"
                />
                <Signature
                  title="Người giao"
                  sign="Ký, họ tên"
                />
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-16"
              onClick={() => handleClose()}
            >
              {t('general.cancel')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
            >
              {t('In')}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

