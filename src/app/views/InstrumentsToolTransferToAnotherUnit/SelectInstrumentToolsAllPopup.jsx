import {
  Grid,
  InputAdornment,
  Input,
  Button,
  Checkbox,
  TablePagination,
  Dialog,
  DialogActions,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, { MTableToolbar } from "material-table";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { appConst } from "../../appConst";
import { searchByPageCCDC } from "app/views/InstrumentToolsList/InstrumentToolsListService";
import { convertNumberPriceRoundUp, formatDateDto } from "app/appFunction";
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectAssetAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    total: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    assetList: [],
    assetVouchers: []
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  updatePageData = () => {
    let status = 0;
    var searchObject = {};
    searchObject.voucherType = this.props.voucherType;
    searchObject.useDepartmentId = this.props.departmentId;
    searchObject.managementDepartmentId = this.props.handoverDepartment
      ? this.props.handoverDepartment.id
      : "";

    if (this.props.isAssetTransfer || this.props.isAssetAllocation) {
      searchObject.statusIndexOrders = this.props?.statusIndexOrders;
    } else {
      searchObject.indexOrder = status;
    }
    searchObject.dateOfReceptionTop = formatDateDto(this.props?.dateOfReceptionTop);
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPageCCDC(searchObject).then(({ data }) => {
      let itemListClone = [...data?.data];
      itemListClone.map((item) => {
        item.isCheck = false;
        if (this.state?.assetVouchers?.length > 0) {
          this.state.assetVouchers.forEach((assetVoucher) => {
            if (assetVoucher.asset.id === item.id) {
              item.isCheck = true;
              return;
            }
          });
        }
      });
      this.setState(
        { itemList: [...itemListClone], total: data?.total },
        () => { }
      );
    });
  };

  componentDidMount() {
    this.setState({ assetVouchers: this.props.assetVouchers },
      () => this.updatePageData()
    )
  }

  handleClick = (event, item) => {
    document.querySelector(`#radio${item.id}`).click();
    item.isCheck = !event.target.checked;
    let { assetVouchers } = this.state;
    if (item.isCheck) {
      let p = {};
      p.asset = item;
      assetVouchers = assetVouchers.concat(p);
    } else {
      assetVouchers = assetVouchers.filter(
        (assetVoucher) => assetVoucher.asset.id !== item.id
      );
    }
    this.setState({ assetVouchers: assetVouchers });
  };

  componentWillMount() {
    let {
      handoverDepartment,
      managementDepartmentId,
    } = this.props;
    this.setState({
      handoverDepartment,
      managementDepartmentId,
    });
  }

  search() {
    this.updatePageData();
  }

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpenProductDialog = () => {
    this.setState({
      shouldOpenProductDialog: true,
    });
  };

  handleDialogProductClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenProductDialog: false,
    });
    this.updatePageData();
  };

  onClickRow = (selectedRow) => {
    document.querySelector(`#radio${selectedRow.id}`).click();
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  render() {
    const {
      t,
      handleClose,
      handleSelect,
      open,
    } = this.props;
    let {
      keyword,
      assetVouchers,
    } = this.state;
    let columns = [
      {
        title: t("InstrumentToolsList.code"),
        field: "code",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.name"),
        field: "name",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("InstrumentToolsList.model"),
        field: "model",
        align: "left",
        minWidth: 130,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        align: "left",
        minWidth: 130,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.yearIntoUse"),
        field: "yearPutIntoUse",
        align: "left",
        minWidth: 90,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: 90,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("Asset.manufacturerTable"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "center"
        },
      },
      {
        title: t("InstrumentToolsList.originalPrice"),
        field: "originalCost",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => rowData.originalCost ? convertNumberPriceRoundUp(rowData.originalCost) : '',
      },
      {
        title: t("InstrumentToolsList.carryingAmount"),
        field: "carryingAmount",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right"
        },
        render: (rowData) => convertNumberPriceRoundUp(rowData.carryingAmount),
      },
      {
        title: t("InstrumentToolsList.availableQuantity"),
        field: "quantity",
        align: "left",
        minWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.useDepartment"),
        field: "useDepartmentName",
        align: "left",
        minWidth: 200,
      },
      {
        title: t("Asset.managementDepartment"),
        field: "managementDepartmentName",
        align: "left",
        minWidth: 200,
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("InstrumentToolsAllocation.choose")}</span>
        </DialogTitle>
        <DialogContent style={{ height: "370px" }}>
          <Grid item md={6} sm={12} xs={12}>
            <Input
              label={t("general.enterSearch")}
              type="text"
              name="keyword"
              value={keyword}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDownEnterSearch}
              onKeyUp={this.handleKeyUp}
              className="w-100 mb-16"
              id="search_box"
              placeholder={t("general.enterSearch")}
              startAdornment={
                <InputAdornment>
                  <Link to="#">
                    {" "}
                    <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                      }}
                    />
                  </Link>
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              parentChildData={(row, rows) => {
                var list = rows.find((a) => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              component="div"
              count={this.state.total}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => handleSelect(assetVouchers)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectAssetAllPopop;
