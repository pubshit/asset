import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const InstrumentsToolTransferToAnotherUnit = EgretLoadable({
  loader: () => import("./InstrumentsToolTransferToAnotherUnitTable")
});
const ViewComponent = withTranslation()(InstrumentsToolTransferToAnotherUnit);

const InstrumentsToolTransferToAnotherUnitRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "instruments-and-tools/transfer-to-another-unit-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default InstrumentsToolTransferToAnotherUnitRoutes;