import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { Dialog, Button, DialogActions } from "@material-ui/core";
import {
  personSearchByPage,
  addAsset,
  updateAsset,
} from "./InstrumentsToolTransferToAnotherUnitService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TransferToAnotherUnitScrollableTabs from "./TransferToAnotherUnitScrollableTabs";
import {
  getAssetDocumentById,
  getListOrgManagementDepartment,
  getNewCodeDocument,
} from "../Asset/AssetService";
import {
  checkInvalidDate,
  checkMinDate,
  checkObject, formatDateDto,
  getTheHighestRole,
} from "app/appFunction";
import AppContext from "app/appContext";
import { STATUS_CHUYEN_DI_CCDC, appConst } from "app/appConst";
import { PaperComponent } from "../Component/Utilities";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class InstrumentsToolTransferDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.TransferToAnotherUnit,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.TransferToAnotherUnit,
    handoverPersonName: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: "",
    useDepartment: null,
    asset: {},
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenSelectPersonPopup: false,
    valueText: null,
    isAssetTransfer: true,
    statusIndexOrders: STATUS_CHUYEN_DI_CCDC, //đang sử dụng và bảo dưỡng, không rõ
    managementDepartment: null,
    personIds: [],
    persons: [],
    transferAddress: "",
    code: null,
    conclude: null,
    transferToAnotherUnitStatus: null,
    shouldOpenPopupAssetFile: false,
    isRoleManager: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_TRANSFER_TO_ANOTHER_UNIT,
    receiverOrg: null,
    listOrganization: [],
  };

  voucherType = ConstantList.VOUCHER_TYPE.TransferToAnotherUnit; //Điều chuyển

  convertDto = (state) => {
    let data = {
      assetVouchers: state?.assetVouchers?.map((item) => ({
        assetId: item?.assetId || item?.asset?.assetId,
        assetItemId: item?.assetItemId || item?.asset?.id,
        note: item?.note || item?.asset?.note,
        quantity: item?.asset?.quantity,
      })),
      code: state?.code,
      conclude: state?.conclude,
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      handoverPersonId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      name: state?.name,
      personIds: state?.personIds,
      transferAddress: state?.transferAddress,
      transferToAnotherUnitStatus: state?.transferToAnotherUnitStatus,
      hoiDongId: this.state?.hoiDongId,
      receiverOrgId: state?.receiverOrg?.id,
      receiverOrgName: state?.receiverOrg?.name,
      persons: state?.persons
        ? state?.persons?.map((item) => {
          return {
            departmentId: item?.departmentId,
            departmentName: item?.departmentName,
            personId: item?.personId,
            personName: item?.personName,
            position: item?.position,
            role: item?.role,
          };
        })
        : [],
    };
    return data;
  };

  handleSetTransferToAnotherUnitUserList = (item) => {
    this.setState({ persons: item || [] });
  };

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id });
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleRowDataCellChange = (value, name, rowData) => {
    let { assetVouchers = [] } = this.state;
    let existItem =
      assetVouchers.find(
        (item) =>
          item.assetItemId === rowData?.asset?.id ||
          item.asset?.id === rowData?.asset?.id
      ) || {};
    let newItem = {
      asset: {
        ...existItem?.asset,
        [name]: +value,
        originalCost: (rowData?.asset?.unitPrice * +value) || 0,
      },
    };

    assetVouchers.splice(assetVouchers.indexOf(existItem), 1, newItem);

    this.setState({
      assetVouchers: [...assetVouchers],
    });
  };

  validateForm = () => {
    let {
      assetVouchers,
      issueDate,
      transferToAnotherUnitStatus,
      transferAddress,
      handoverDepartment,
      handoverPerson,
      persons,
    } = this.state;

    if (checkInvalidDate(issueDate)) {
      toast.warning("Ngày chuyển đi không tồn tại.");
      return false;
    }

    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(`Ngày chuyển đi không được nhỏ hơn ngày "01/01/1900".`);
      return false;
    }

    if (!handoverDepartment?.id) {
      toast.warning("Chưa chọn phòng ban bàn giao.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (!handoverPerson?.personDisplayName) {
      toast.warning("Chưa chọn người bàn giao.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (!transferToAnotherUnitStatus) {
      toast.warning("Chưa chọn trạng thái phiếu.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (!transferAddress) {
      toast.warning("Chưa nhập địa chỉ chuyển đi.");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (assetVouchers?.length === 0) {
      toast.warning("Chưa chọn CCDC chuyển đi");
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return false;
    }

    if (this.state?.hoiDongId && persons?.length === 0) {
      toast.warning("Hội đồng không có thành viên");
      toast.clearWaitingQueue();
      return false;
    }

    return true;
  };

  handleFormSubmit = (e) => {
    if (e.target.id !== "formTransferToAnotherUnitDialog") return;
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { id } = this.state;
    if (!this.validateForm()) return;
    setPageLoading(true);
    let dataState = this.convertDto(this.state);
    if (id) {
      updateAsset(id, dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.info(data.message);
            this.props.handleOKEditClose();
          } else {
            toast.warning(data.message);
          }
          setPageLoading(false);
        })
        .catch((error) => {
          toast.error(t("general.error"));
          setPageLoading(false);
        });
    } else {
      addAsset(dataState)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.info(data.message);
            this.props.handleOKEditClose();
          } else {
            toast.warning(data.message);
          }
          setPageLoading(false);
        })
        .catch((error) => {
          toast.error(t("general.error"));
          setPageLoading(false);
        });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    }, () => {
      if(name === "issueDate") {
        this.setState({ assetVouchers: [] });
      }
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleSelectAsset = (items) => {
    let { t } = this.props;
    let assetVouchers = [];
    if (items.length <= 0) toast.warning(t("general.noCCDCSelected"));

    items?.map((item) => {
      let asset = {
        ...item.asset,
        inventoryQuantity: item.asset.inventoryQuantity
          ? item.asset.inventoryQuantity
          : item.asset.quantity,
      };
      assetVouchers.push({ asset });
    });

    this.setState({ assetVouchers }, function () {
      this.handleAssetPopupClose();
    });
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState(
      {
        assetVouchers,
      },
      () => {
        toast.info("Xóa CCDC trong danh sách CCDC thành công");
      }
    );
  };

  componentDidMount() {
    let { item } = this.props;
    const { isRoleAssetManager } = getTheHighestRole();
    let assetVouchers = [];

    item?.assetVouchers?.map((item) => {
      let p = {
        asset: {
          ...item,
          assetId: item.assetId,
          name: item.assetName,
          carryingAmount: item.assetCarryingAmount,
          code: item.assetCode,
          dateOfReception: item?.assetYearPutIntoUse,
          id: item.assetItemId,
          inventoryQuantity: item.inventoryQuantity,
          managementCode: item.assetManagementCode,
          originalCost: item.assetOriginalCost,
          quantity: item.quantity,
        },
      };
      return assetVouchers.push(p);
    });

    this.setState(
      {
        ...item,
        assetVouchers,
        handoverPerson: {
          personId: item?.handoverPersonId,
          personDisplayName: item?.handoverPersonName
        },
        receiverOrg: item?.receiverOrgId ? {
          id: item?.receiverOrgId,
          name: item?.receiverOrgName
        } : null,
      },
      () => {
        if (isRoleAssetManager) {
          this.getManagementDepartment();
        }
      }
    );
  }

  getManagementDepartment = () => {
    getListOrgManagementDepartment({}).then(({ data }) => {
      let handoverDepartment = data ? data[0] : null;
      this.setState({
        handoverDepartment: handoverDepartment,
        handoverDepartmentId: handoverDepartment?.id,
        managementDepartmentId: handoverDepartment?.id,
        isRoleManager: true,
      });
    });
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      handoverPersonId: null,
      listHandoverPerson: [],
      assetVouchers: [],
      managementDepartmentId: item?.id,
    });
  };

  handleChangeSelect = (item) => {
    this.setState({
      transferToAnotherUnitStatus: item?.code,
    });
  };

  openSelectPersonPopup = () => {
    this.setState({
      shouldOpenSelectPersonPopup: true,
    });
  };

  handleSelectPerson = (listPerson) => {
    let personIds = [];

    if (listPerson?.length > 0) {
      listPerson.forEach((item) => {
        personIds.push(item?.personId);
      });
      this.setState({
        persons: listPerson,
        personIds: personIds,
      });
    } else {
      this.setState({
        persons: [],
        personIds: [],
      });
    }
    this.handleSelectPersonPopupClose();
  };

  removePersonInlist = (personId) => {
    let { persons, personIds } = this.state;
    let index = persons.findIndex((x) => x.person?.id === personId);
    let indexId = personIds.findIndex((personIds) => personIds === personId);
    persons.splice(index, 1);
    personIds.splice(indexId, 1);

    this.setState({ persons, personIds }, () => {
      toast.info("Xóa thành công");
    });
  };

  handleSelectPersonPopupClose = () => {
    this.setState({
      shouldOpenSelectPersonPopup: false,
    });
  };

  openPopupSelectAsset = () => {
    let { voucherType, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState(
        {
          item: {},
          voucherType: voucherType,
        },
        () => {
          this.setState({
            shouldOpenAssetPopup: true,
          });
        }
      );
    } else {
      toast.warning("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: {
        id: item?.id,
        name: item?.text,
        text: item?.text,
      },
    });
    let handoverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: item?.id,
    };
    let result = false;
    let { handoverPerson } = this.state;
    personSearchByPage(handoverPersonSearchObject).then(({ data }) => {
      result = data?.content?.some(
        (person) => handoverPerson?.id === person?.id
      );
      if (result) {
        this.setState({ assetVouchers: [] });
      } else {
        this.setState({ handoverPerson: null });
      }
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null, handoverPerson: null });
    }
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = { id: item.id, name: item.name };
    this.setState({ handoverDepartment: department }, function () {
      this.handleHandoverDepartmentPopupClose();
    });
    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId
      ? this.state?.listAssetDocumentId
      : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents;
    let indexDocument = documents.findIndex(
      (item) => item?.id === document?.id
    );
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id);
      });
      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state.documentType;

      this.setState({
        item: document,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true,
      });
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex((document) => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(
      (documentId) => documentId === id
    );
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleSetDataHandoverDepartment = (data) => {
    this.setState({
      listHandoverDepartment: data,
    });
  };

  handleSetDataHandoverPerson = (data) => {
    this.setState({
      listHandoverPerson: data,
    });
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
    });
  };
  
  handleFilterSelected = (item, as) => {
    console.log(item);
    console.log(as);
    return ((as.assetItemId || as.asset?.id) === item.id)
      && (as?.asset?.dateOfReception === item?.dateOfReception)
      && ((as?.departmentId || as.asset?.useDepartmentId) === item?.useDepartmentId)
  }

  render() {
    let { open, t, item, i18n } = this.props;
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let { shouldOpenNotificationPopup } = this.state;

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll="paper"
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={t("Yêu cầu chọn tài sản")}
            agree={t("general.agree")}
          />
        )}
        <CustomValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          id="formTransferToAnotherUnitDialog"
          className="validator-form-scroll-dialog"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            {item?.id
              ? t("TransferToAnotherUnit.updateDialog")
              : t("TransferToAnotherUnit.addDialog")}
          </DialogTitle>

          <DialogContent style={{ minHeight: "450px" }}>
            <TransferToAnotherUnitScrollableTabs
              t={t}
              i18n={i18n}
              item={this.state}
              handleSelectHandoverDepartment={this.handleSelectHandoverDepartment}
              handleHandoverDepartmentPopupClose={this.handleHandoverDepartmentPopupClose}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              handleReceiverDepartmentPopupClose={this.handleReceiverDepartmentPopupClose}
              handleReceiverPersonPopupClose={this.handleReceiverPersonPopupClose}
              handleRowDataCellChange={this.handleRowDataCellChange}
              removeAssetInlist={this.removeAssetInlist}
              handleDateChange={this.handleDateChange}
              handoverPersonSearchObject={handoverPersonSearchObject}
              selectHandoverDepartment={this.selectHandoverDepartment}
              handleChangeSelect={this.handleChangeSelect}
              handleChange={this.handleChange}
              openSelectPersonPopup={this.openSelectPersonPopup}
              handleSelectPerson={this.handleSelectPerson}
              handleSelectPersonPopupClose={this.handleSelectPersonPopupClose}
              removePersonInlist={this.removePersonInlist}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleSetDataHandoverDepartment={this.handleSetDataHandoverDepartment}
              handleSetDataHandoverPerson={this.handleSetDataHandoverPerson}
              getAssetDocument={this.getAssetDocument}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              itemAssetDocument={this.state?.item}
              handleSetTransferToAnotherUnitUserList={this.handleSetTransferToAnotherUnitUserList}
              handleSetHoiDongId={this.handleSetHoiDongId}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleFilterSelected={this.handleFilterSelected}
            />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              {!this.props?.item?.isView && (
                <Button
                  variant="contained"
                  color="primary"
                  className="mr-16"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              )}
            </div>
          </DialogActions>
        </CustomValidatorForm>
      </Dialog>
    );
  }
}
InstrumentsToolTransferDialog.contextType = AppContext;
export default InstrumentsToolTransferDialog;
