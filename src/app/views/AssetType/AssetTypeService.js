import axios from "axios";
import ConstantList from "../../appConfig";
const API_ASSET_TYPE =
  ConstantList.API_ENPOINT + "/api/assettype" + ConstantList.URL_PREFIX;

export const getAllAssetTypes = () => {
  return axios.get(API_ASSET_TYPE + "/1/100000");
};

export const getAssetTypeByPage = (searchObject) => {
  let url = API_ASSET_TYPE + "/" + searchObject?.pageIndex + "/" + searchObject?.pageSize;
  return axios.get(url);
};
export const searchByPage = (keyword, pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  return axios.post(API_ASSET_TYPE + "/searchByText/" + params, keyword);
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = (id) => {
  return axios.delete(API_ASSET_TYPE + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_ASSET_TYPE + "/" + id);
};
export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_ASSET_TYPE + "/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateAssetType = (AssetType) => {
  return axios.post(API_ASSET_TYPE, AssetType);
};
