import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetTypeTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./AssetTypeTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(AssetTypeTable);

const AssetTypeRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/asset_type",
    exact: true,
    component: ViewComponent
  }
];

export default AssetTypeRoutes;
