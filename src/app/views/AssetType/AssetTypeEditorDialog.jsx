import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid, DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { addNewOrUpdateAssetType, checkCode } from './AssetTypeService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class AssetTypeEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    shouldOpenNotificationPopup: false
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;
    let { t } = this.props;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        this.setState({
          shouldOpenNotificationPopup: true,
          Notification: "AssetType.noti.dupli_code"
        })
        // alert("Code đã được sử dụng");
      } else {
        if (id) {
          addNewOrUpdateAssetType({
            ...this.state
          }).then(() => {
            toast.info(t('general.updateSuccess'));
            this.props.handleOKEditClose();
          });
        } else {
          addNewOrUpdateAssetType({
            ...this.state
          }).then(() => {
            toast.info(t('general.addSuccess'));
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentWillMount() {
    this.setState({
      ...this.props.item
    }, function () {

    }
    );
  }
  componentDidMount() {
  }
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false, })
  }

  render() {
    let { open, t } = this.props;
    let {
      name,
      code,
      id,
      shouldOpenNotificationPopup
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title" >
          {t('AssetType.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span className="colorRed">*</span>{t('AssetType.code')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  disabled={id && true}
                  validators={["required", "matchRegexp:^(?!.*\\.\\.).*$"]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span className="colorRed">*</span>{t('AssetType.name')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", "matchRegexp:^(?!.*\\.\\.).*$"]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                className="mr-15"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AssetTypeEditorDialog;
