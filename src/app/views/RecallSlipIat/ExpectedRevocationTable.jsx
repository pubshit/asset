import React, { useEffect } from "react";
import {
  IconButton,
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { getListManagementDepartment, getListUserByDepartmentId } from './RecallSlipIatService';
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import MaterialTable, { MTableToolbar } from 'material-table';
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { useState } from "react";
import { a11yProps, checkInvalidDate, convertNumberPriceRoundUp, filterOptions, NumberFormatCustom } from "app/appFunction";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { appConst, STATUS_DEPARTMENT } from "app/appConst";
import { searchByPageDepartmentNew } from "../Department/DepartmentService";
import SelectInstrumentToolsAllPopup from "../Component/InstrumentTools/SelectInstrumentToolsAllPopup";
import { LightTooltip, TabPanel } from "../Component/Utilities";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ExpectedRevocationTable(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });

  const searchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code
  };
  const [listReceiverPerson, setListReceiverPerson] = useState(props?.item?.listReceiverPerson || []);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.handoverDepartment?.id ?? '',
    });

    setSearchParamUserByReceiverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.receiverDepartment?.id ?? '',
    });

  }, [props?.item?.handoverDepartment, props?.item?.receiverDepartment]);

  useEffect(() => {
    setListReceiverPerson(props?.item?.listReceiverPerson)
  }, [props?.item]);

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      hidden: props?.item?.isView && !props?.item?.isStatusConfirmed,
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: rowData =>
      ((!props?.item?.isView && !props?.item?.isStatusConfirmed)
        && <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData?.asset?.id);
            } else {
              alert('Call Selected Here:' + rowData?.id);
            }
          }}
        />)
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      width: '50px',
      cellStyle: {
        textAlign: "center",
      },
      render: rowData => ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData.tableData.id + 1)
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
      maxWidth: 400,
    },
    {
      title: t("RecallSlipIat.dxQuantity"),
      field: "dxQuantity",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          onChange={(event) => props.handleQuantityChange(rowData, event)}
          name="dxQuantity"
          id="formatted-numberformat-originalCost"
          value={rowData?.dxQuantity || ""}
          validators={[
            "required",
            "minNumber:1",
            `maxNumber:${rowData.asset?.remainQty || 999999}`,
          ]}
          errorMessages={[
            t("general.required"),
            t("general.minNumberError"),
            t("general.mustBeSmallerThanRemainQty"),
          ]}
          InputProps={{
            inputComponent: NumberFormatCustom,
            inputProps: {
              style: {
                textAlign: "center",
              },
            },
            readOnly: props?.item?.isView
          }}
        />
      ),
    },
    {
      title: t("RecallSlipIat.remainQty"),
      field: "asset.remainQty",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.asset?.serialNumber || rowData?.assetSerialNumber
    },
    {
      title: t("Asset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
      render: (rowData) => rowData?.asset?.model || rowData?.assetModel
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "asset.manufacturer.name",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
      render: (rowData) => rowData?.asset?.manufacturerName || rowData?.manufacturerName
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.originalCost || rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.originalCost || rowData?.asset?.originalCost)
        : "",
    },
    {
      title: t("Asset.carryingAmount"),
      field: "asset.carryingAmount",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.asset?.carryingAmount
        ? convertNumberPriceRoundUp(rowData?.asset?.carryingAmount)
        : "",
    },
    {
      title: t("Asset.managementDepartment"),
      field: "usageState",
      align: "left",
      minWidth: 260,
      maxWidth: 260,
      render: (rowData) => rowData.asset?.managementDepartmentName || "",
    },
    {
      title: t("RecallSlip.importStatus"),
      field: "warehouseStatus",
      align: "left",
      minWidth: 180,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        <TextValidator
          className="w-100"
          onChange={warehouseStatus => props.handleRowDataCellChange(rowData, warehouseStatus.target.value, "warehouseStatus")}
          type="text"
          name="warehouseStatus"
          value={rowData.warehouseStatus || ""}
          InputProps={{
            readOnly: props?.item?.isView
          }}
        />
    },
    {
      title: t("Asset.note"),
      field: "note",
      align: "left",
      minWidth: 180,
      render: rowData =>
        <TextValidator
          multiline
          maxRows={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note.target.value, "note")}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props?.item?.isView
          }}
          validators={["maxStringLength:255"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
    }
  ];

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 150,
      minWidth: 100,
      render: rowData =>
        // ((!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
      // )
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} type="submit" />
          {/*<Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />*/}
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1} justifyContent="space-between">
          <Grid item md={4} sm={6} xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog mt-2"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("AssetTransfer.issueDate")}
                  </span>
                }
                name={'suggestDate'}
                inputVariant="standard"
                autoOk={true}
                format="dd/MM/yyyy"
                value={props?.item.suggestDate}
                onChange={date => props?.handleDateChange(date, "suggestDate")}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                InputProps={{
                  readOnly: props?.item?.isView
                }}
                open={isPickerOpen}
                onOpen={() => {
                  if (props?.item?.isView) {
                    setIsPickerOpen(false);
                  }
                  else {
                    setIsPickerOpen(true);
                  }
                }}
                onClose={() => setIsPickerOpen(false)}
                validators={['required']}
                errorMessages={t('general.required')}
                minDate={new Date("01/01/1900")}
                minDateMessage={"Ngày chứng từ không được nhỏ hơn ngày 01/01/1900"}
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                onBlur={() => handleBlurDate(props?.item.issueDate, "issueDate")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("allocation_asset.handoverDepartment")}
                </span>
              }
              searchFunction={getListManagementDepartment}
              searchObject={searchObject}
              listData={props?.item.listHandoverDepartment || []}
              setListData={(data) => props?.handleSetDataSelect(data, "listHandoverDepartment")}
              displayLable={"text"}
              readOnly={props?.item?.isView || props?.isRoleAssetUser}
              value={props?.item?.handoverDepartment ? props?.item?.handoverDepartment : ''}
              onSelect={handoverDepartment => props?.handleSelectHandoverDepartment(handoverDepartment)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("allocation_asset.handoverPerson")}
                </span>
              }
              disabled={!props?.item?.handoverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByHandoverDepartment}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.handoverPerson ?? ''}
              onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("maintainRequest.status")}
                </span>
              }
              searchFunction={() => { }}
              listData={appConst.expectedRecoveryStatus || []}
              setListData={(data) => props.handleSetDataSelect(data, "listStatus")}
              value={props?.item?.transferStatus || null}
              displayLable={'name'}
              readOnly={props?.item?.isView}
              onSelect={props.selectAssetTransferStatusStatus}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("allocation_asset.receiverDepartment")}
                </span>
              }
              searchFunction={searchByPageDepartmentNew}
              searchObject={searchObject}
              listData={props.item.listReceiverDepartment || []}
              setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
              typeReturnFunction="category"
              displayLable={"name"}
              isNoRenderChildren
              isNoRenderParent
              readOnly={props?.item?.isView}
              value={props?.item?.receiverDepartment ? props?.item?.receiverDepartment : ''}
              onSelect={receiverDepartment => props?.handleSelectReceiverDepartment(receiverDepartment)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              className="w-100 mb-20"
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("AssetTransfer.receiverPerson")}
                </span>
              }
              disabled={!props?.item?.receiverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByReceiverDepartment}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              readOnly={props?.item?.isView}
              value={props?.item?.receiverPerson ?? ''}
              onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            {(!props?.item?.isView && !props?.item?.isStatusConfirmed) && (
              <Button
                variant="contained"
                color="primary"
                size="small" className="mb-16 w-100"
                onClick={props?.openPopupSelectAsset}
              >
                {t('RecallSlipIat.select_iat')}
              </Button>
            )}
            {props.item.handoverDepartment && props.item.shouldOpenAssetPopup && (
              <SelectInstrumentToolsAllPopup
                open={props.item.shouldOpenAssetPopup}
                handleSelect={props.handleSelectAsset}
                departmentId={props?.item?.handoverDepartment?.id}
                isAssetTransfer={props.item.isAssetTransfer}
                statusIndexOrders={props.item.statusIndexOrders}
                handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                handoverDepartment={props?.item?.receiverDepartment}
                assetVouchers={props.item.assetVouchers != null ? props.item.assetVouchers : []}
                dateOfReceptionTop={props.item?.suggestDate}
                voucherType={true}
              />
            )}
          </Grid>
        </Grid>
        <Grid item md={10} sm={12} xs={12}></Grid>
        <Grid>
          <MaterialTable
            data={props?.item?.assetVouchers || []}
            columns={columns}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: 'dense',
              maxBodyHeight: '220px',
              minBodyHeight: '220px',
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            components={{
              Toolbar: props => (
                <div className="w-100">
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={12} sm={12} xs={12}>
          {(!props?.item?.isView && !props?.item?.isStatusConfirmed) &&
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={
                props.handleAddAssetDocumentItem
              }
            >
              {t('AssetFile.addAssetFile')}
            </Button>
          }
          {props?.item?.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props?.item.shouldOpenPopupAssetFile}
              handleClose={props?.handleAssetFilePopupClose}
              itemAssetDocument={props?.itemAssetDocument}
              getAssetDocument={props?.getAssetDocument}
              handleUpdateAssetDocument={props?.handleUpdateAssetDocument}
              documentType={props?.item?.documentType}
              item={props?.item}
              t={t}
              i18n={i18n}
            />)
          }
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props?.item?.documents || []}
            columns={columnsVoucherFile}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              padding: 'dense',
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
              },
              maxBodyHeight: '285px',
              minBodyHeight: '285px',
            }}
            components={{
              Toolbar: props => (
                <div className="w-100">
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div >
  );
}