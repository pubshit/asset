import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_USER =
  ConstantList.API_ENPOINT + "/api/v1/user-departments/page";
const API_PATH_STORE = ConstantList.API_ENPOINT + "/api/stores/org";
const API_PATH_REVOCATION_VOUCHER =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/revocation-vouchers";
const API_RECALL_SLIP_IAT = 
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/expected-revocations";
const API_RECALL_SLIP = 
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/expected-revocations";

//get phong ban bàn giao
export const getListManagementDepartment = (searchObject) => {
    return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      ...searchObject,
    });
  };
//get phòng ban theo id người
export const findDepartmentById = (id) => {
  let config = {
    params: { userId: id, isActive: STATUS_DEPARTMENT.HOAT_DONG.code },
  };
  let url = API_PATH_department + "/main-department";
  return axios.get(url, config);
};

// get người theo id phòng
export const getListUserByDepartmentId = (searchObject) => {
  let config = { params: searchObject };
  let url = API_PATH_USER;
  return axios.get(url, config);
};

// get kho theo id phòng
export const getListWarehouseByDepartmentId = (searchObject) => {
  const { pageIndex, pageSize, ...rest } = searchObject;
  let url =
    API_PATH_STORE + "/searchByText/" + pageIndex + "/" + searchObject.pageSize;
  return axios.post(url, { ...rest });
};
  //
 
export const searchByPageRecallSlipIat = (searchObject) => {
  let config = { params: { ...searchObject } };
  let url = API_RECALL_SLIP_IAT + "/search-by-page";
  return axios.get(url, config);
};

export const getItemById = (id) => {
  let url = API_RECALL_SLIP_IAT + `/${id}`;
  return axios.get(url);
}

export const createRecallSlip = (recall) => {
  return axios.post(API_RECALL_SLIP_IAT, recall);
};

export const updateRecallSlip = (recall, id) => {
  let url = API_RECALL_SLIP_IAT + `/${id}`
  return axios.put(url, recall)
}

export const deleteRecallSlip = (id) => {
  let url = API_RECALL_SLIP_IAT + `/${id}`
  return axios.delete(url)
}

// api thu hoi 
export const searchByPageRevocationVouchers = (searchObject) => {
  let config = { params: { ...searchObject } };
  let url = API_PATH_REVOCATION_VOUCHER + "/search-by-page";
  return axios.get(url, config);
}

export const getRevocationVouchersById = (id) => {
  let url = API_PATH_REVOCATION_VOUCHER + `/${id}`;
  return axios.get(url);
}

export const createRevocationVoucher = (revocation) => {
  return axios.post(API_PATH_REVOCATION_VOUCHER, revocation);
}

export const deleteRevotionvoucher = (id) => {
  let url = API_PATH_REVOCATION_VOUCHER + `/${id}`
  return axios.delete(url)
}

export const getAssetVorcher = (params) => {
  for (const [key, value] of Object.entries(params)) {
    if (Array.isArray(value)) {
      params[key] = value.toString();
    }
  }
  let url = API_RECALL_SLIP + "/recall/assets";
  return axios.get(url, {params});
}

export const updateRevotionVoucher = (recall, id) => {
  let url = API_PATH_REVOCATION_VOUCHER + `/${id}`
  return axios.put(url, recall)
}

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH + "/download/excel/revocation-vouchers",
    data: searchObject,
    responseType: "blob",
  });
};

export const getAssetVorcherRevocationForLiquidation = (params) => {
  let url = API_PATH_REVOCATION_VOUCHER + "/liquidation/assets"
  return axios.get(url, {params})
}
