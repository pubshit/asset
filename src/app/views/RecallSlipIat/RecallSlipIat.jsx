import {
  AppBar,
  Icon,
  IconButton,
  Tab,
  Tabs,
  Checkbox
} from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  TabPanel,
  convertFromToDate,
  convertNumberPriceRoundUp,
  formatDateTimeDto,
  formatDateTypeArray,
  formatTimestampToDate,
  getTheHighestRole,
  isValidDate, getUserInformation, isSuccessfulResponse, handleThrowResponseMessage,
} from "app/appFunction";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { saveAs } from "file-saver";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentTransferTable from "./ComponentTransferTable";
import MaterialTable, { MTableToolbar } from "material-table";
import { functionExportToExcel } from "../../appFunction";
import {
  deleteRecallSlip,
  deleteRevotionvoucher,
  getAssetVorcher,
  getItemById,
  getRevocationVouchersById,
  searchByPageRecallSlipIat,
  searchByPageRevocationVouchers,
  exportToExcel as exportAssetsToExcel, getAssetVorcherRevocationForLiquidation
} from "./RecallSlipIatService";
import { LightTooltip } from "../Component/Utilities";
import { convertExcelPayload } from "../RecallSlip/dataConversion";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const { departmentUser } = getUserInformation();
  const item = props.item;
  let {
    hasDeletePermission,
    hasEditPermission,
    hasWatchPermission,
    isRoleOrgAdmin,
    isRoleAssetManager,
  } = props;
  let isOwner = item.createByDepartmentId === departmentUser?.id

  return (
    <div className="none_wrap" onClick={(event) => event?.stopPropagation()}>
      {hasEditPermission &&
        (isOwner || isRoleOrgAdmin || isRoleAssetManager) && // nó là người tạo phiếu hoặc là admin
        item?.status === appConst.STATUS_EXPECTED_RECOVERY?.DANG_THONG_KE?.id && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission &&
        (isOwner || isRoleOrgAdmin || isRoleAssetManager) &&
        (item?.status === appConst.STATUS_EXPECTED_RECOVERY?.DANG_THONG_KE?.id) && (
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      {(hasWatchPermission) && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.watchInformation")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.view)}
          >
            <VisibilityIcon size="small" className="iconEye" />
          </IconButton>
        </LightTooltip>
      )}
      {item?.status === appConst.expectedRecoveryStatus[1]?.id && (
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
    </div>
  );
}

class RecallSlipToolTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetRecallSlip: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenAddDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    totalPages: 0,
    isView: false,
    shouldOpenAddLiquidationSlipDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    isOwner: false,
    statusIndex: null,
    tabValue: 0,
    isRoleAdmin: false,
    isRoleAssetManager: false,
    isCheckReceiverDP: true,
    isChecked: [],
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    rowDataAssetVouchersTable: [],
    openAdvanceSearch: false,
    listStatus: [],
    listHandoverDepartment: [],
    listReceiverDepartment: [],
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    toDate: null,
    fromToData: null,
    openPrintQR: false,
    products: [],
    checkAll: false,
    isTabExpected: true,
    itemData: {},
    dxIds: [],
    assetVouchers: [],
    voucherType: null
  };
  numSelected = 0;
  rowCount = 0;

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      itemList: [],
      selectedList: [],
      isChecked: [],
      tabValue: newValue,
      keyword: "",
      statusIndex: null,
      page: 0,
    });

    if (appConst.tabRecallSlipTool?.tabExpected === newValue) {
      this.setState({
        isTabExpected: true,
        listStatus: appConst.listStatusRecallSlip,
      },
        this.updatePageData
      );
    }
    if (appConst.tabRecallSlipTool?.tabRecall === newValue) {
      this.setState({
        isTabExpected: false,
        listStatus: appConst.revokedstatus,
      },
        this.updatePageData
      );
    }

  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleCheckboxChange = () => {
    this.setState({ isChecked: !isChecked })
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  handleCheck = (event, item) => {
    let { selectedList, itemList, isChecked, dxIds } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        isChecked.push(item)
        dxIds.push(item?.id)
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        this.setState({
          selectedList: filteredArray,
          isChecked: filteredArray,
          dxIds: filteredArray
        })
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);
      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      if (!event.target.checked) {
        isChecked = isChecked.filter(id => id?.id !== item?.id);
        dxIds = dxIds.filter(id => id !== item?.id);
      } else {
        isChecked.push(item);
        dxIds.push(item?.id);
      }
      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
        isChecked,
        dxIds
      });
    }

  };

  updatePageData = () => {
    let { t } = this.props;
    let {
      selectedList,
      fromDate,
      toDate,
      handoverDepartment,
      transferStatus,
      status,
      rowsPerPage,
      page,
      keyword,
      voucherType,
      receiverDepartment
    } = this.state;
    let { setPageLoading } = this.context;

    setPageLoading(true);
    let searchObject = {};
    searchObject.type = voucherType;
    searchObject.keyword = keyword?.trim() || null;
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchObject.status = transferStatus?.indexOrder || transferStatus?.id;
    searchObject.departmentId = receiverDepartment?.id;
    if (fromDate) {
      searchObject.fromDate = convertFromToDate(fromDate).fromDate
    }
    if (toDate) {
      searchObject.toDate = convertFromToDate(toDate).toDate
    }
    if (this.state.isTabExpected) {
      searchObject.receiptDepartmentId = handoverDepartment?.id;
    } else {
      searchObject.receiverDepartmentId = handoverDepartment?.id;
    }
    const searchFunction = this.state.isTabExpected
      ? searchByPageRecallSlipIat
      : searchByPageRevocationVouchers;

    searchFunction(searchObject)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data.code && data.data !== null) {
          if (data?.data?.totalElements && data?.data?.content.length <= 0) {
            this.search();
            return;
          }
          let countItemChecked = 0
          const itemList = data?.data?.content?.map(item => {
            item.createDate = formatDateTypeArray(item?.createDate)

            if (selectedList?.find(checkedItem => checkedItem === item.id)) {
              countItemChecked++
            }
            return {
              ...item,
              checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
            }
          }
          )

          this.setState({
            selectedItem: null,
            itemList,
            totalElements: data?.data?.totalElements,
            ids: [],
            rowDataAssetVouchersTable: [],
            checkAll: itemList?.length === countItemChecked && itemList?.length > 0
          })
        }
        else {
          toast.warning(data?.message);
        }
        setPageLoading(false);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenAddDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      shouldOpenAddLiquidationSlipDialog: false,
      isPrint: false,
      isView: false,
      item: null,
      assetVouchers: []
    }, () => {
      this.updatePageData();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenAddDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      shouldOpenAddLiquidationSlipDialog: false,
      isPrint: false,
      isView: false,
      item: null,
      assetVouchers: [],
      selectedList: [],
      isChecked: []
    }, () => {
      this.updatePageData();
    });
  };

  handleConfirmationResponse = async () => {
    let { id, isTabExpected } = this.state;
    let { t } = this.props;
    const deleteFunction = isTabExpected
      ? deleteRecallSlip
      : deleteRevotionvoucher;
    await deleteFunction(id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("Xóa thành công"));
          this.handleOKEditClose();
          this.setState({
            rowDataAssetVouchersTable: []
          })
        } else {
          const { errorMessage } = data?.data[0];
          toast.warning(errorMessage || data.message);
        }
      })
      .catch((error) => {
        toast.error(t("general.error"))
      })
  };

  componentDidMount() {
    this.setState({
      statusIndex: null,
      isTabExpected: true,
      listStatus: appConst.listStatusRecallSlip,
    }, () => {
      this.handleDataAfterRedirect();
      this.updatePageData()
      this.getRoleCurrentUser();
    });
  }

  handleDataAfterRedirect = () => {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleView(location?.state?.id)
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleEdit(location?.state?.id)
          break;
        default:
          break;
      }
    }

    window.addEventListener('beforeunload', () => {
      history.push(location?.state?.path, null);
    })
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasWatchPermission,
      hasCreatePermission,
      hasCheckPermission,
      hasReceivePermission,
      hasPrintPermission,
    } = this.state;
    let {
      currentUser,
      isRoleAdmin,
      isRoleOrgAdmin,
      isRoleAssetUser,
      isRoleAssetManager,
      departmentUser,
    } = getTheHighestRole();

    if (currentUser) {
      if (isRoleOrgAdmin || isRoleAdmin) {
        if (!hasDeletePermission) {
          hasDeletePermission = true;
        }
        if (!hasEditPermission) {
          hasEditPermission = true;
        }
        if (!hasWatchPermission) {
          hasWatchPermission = true;
        }
        if (!hasCheckPermission) {
          hasCheckPermission = true;
        }
        if (!hasReceivePermission) {
          hasReceivePermission = true;
        }
        if (!hasPrintPermission) {
          hasPrintPermission = true;
        }
        if (!hasCreatePermission) {
          hasCreatePermission = true;
        }
        if (!isRoleAdmin) {
          isRoleAdmin = true;
        }
      }
      // người quản lý vật tư
      if (isRoleAssetManager) {
        if (!hasDeletePermission) {
          hasDeletePermission = true;
        }
        if (!hasEditPermission) {
          hasEditPermission = true;
        }
        if (!hasWatchPermission) {
          hasWatchPermission = true;
        }
        if (!hasCheckPermission) {
          hasCheckPermission = true;
        }
        if (hasReceivePermission) {
          hasReceivePermission = false;
        }
        if (!hasPrintPermission) {
          hasPrintPermission = true;
        }
        if (!hasCreatePermission) {
          hasCreatePermission = true;
        }
        if (!isRoleAssetManager) {
          isRoleAssetManager = true;
        }
      }
      // người đại diện phòng ban
      if (isRoleAssetUser) {
        if (!hasDeletePermission) {
          hasDeletePermission = true;
        }
        if (!hasEditPermission) {
          hasEditPermission = true;
        }
        if (!hasWatchPermission) {
          hasWatchPermission = true;
        }
        if (hasCheckPermission) {
          hasWatchPermission = false;
        }
        if (hasReceivePermission) {
          hasReceivePermission = false;
        }
        if (!hasCreatePermission) {
          hasCreatePermission = true;
        }
        if (!hasPrintPermission) {
          hasPrintPermission = false;
        }
      }
      this.setState({
        currentUser,
        isRoleAdmin,
        isRoleOrgAdmin,
        isRoleAssetUser,
        departmentUser,
        isRoleAssetManager,
        hasEditPermission,
        hasCheckPermission,
        hasWatchPermission,
        hasPrintPermission,
        hasDeletePermission,
        hasCreatePermission,
        hasReceivePermission,
      });
    }
  };

  handleButtonAdd = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };


  validateFormRecallSlip = () => {
    let { isChecked } = this.state;
    let roles = getTheHighestRole();
    let { departmentUser, isRoleOrgAdmin } = roles;
    const firstElement = isChecked[0]?.receiptDepartmentId;

    let isValid = {
      status: false,
      receip: false,
      receiptDepartmentId: false
    }
    if (isChecked.length === 0) {
      toast.warning("Vui lòng chọn phiếu dự kiến trước khi thu hồi")
      return
    }
    isChecked.forEach(item => {
      if (item.status !== appConst.STATUS_EXPECTED_RECOVERY.DA_THONG_KE.id) {
        isValid.status = true
      }
      if (item?.receiptDepartmentId !== firstElement) {
        isValid.receip = true
      }
      if (item?.receiptDepartmentId !== departmentUser?.id && !isRoleOrgAdmin) {
        isValid.receiptDepartmentId = true
      }
    })
    if (isValid.status) {
      toast.warning("Chỉ được chọn phiếu ở trạng thái đã thống kê")
      return
    }
    if (isValid.receip) {
      toast.warning("Chỉ được chọn phiếu ở một phòng ban")
      return
    }
    if (isValid.receiptDepartmentId) {
      toast.warning("Bạn không phải phòng thu hồi phiếu này")
      return
    }

    return true
  };

  handleAddRecallSlip = (item) => {
    let { dxIds, isChecked, assetVouchers, isTabExpected } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;
    const obj = {
      receiptDepartmentName: isChecked[0]?.receiptDepartmentName,
      receiptDepartmentId: isChecked[0]?.receiptDepartmentId,
      dxIds: dxIds,
      assetVouchers: assetVouchers,
    }
    if (isTabExpected) {
      if (!this.validateFormRecallSlip()) return
      setPageLoading(true)
      let searchObject = {};
      searchObject.dxIds = dxIds;
      getAssetVorcher(searchObject).then(({ data }) => {
        data?.data?.map(item => {
          assetVouchers.push({
            asset: item,
            assetId: item?.assetId,
            tableData: item?.tableData,
            dxId: item.dxId,
            quantity: item.quantity,
            remainQty: item.remainQty,
            useDepartmentId: item?.useDepartmentId,
            id: item?.id
          })
        })
        this.setState({
          item: obj,
          shouldOpenAddDialog: true,
          assetVouchers,
        })
      })
        .catch((error) => {
          toast.error(t("toastr.error"));
        })
        .finally(() => {
          setPageLoading(false);
        });
    } else {
      this.setState({
        item: item,
        shouldOpenAddDialog: true,
      });
    }
  };

  validatorLiquidationSlip = () => {
    let { isChecked } = this.state;
    let { t } = this.props;
    let firstReceiverDepartmentId = isChecked?.[0]?.receiverDepartmentId;
    let hasOtherDepartment = isChecked?.some(item => item.receiverDepartmentId !== firstReceiverDepartmentId);
    let hasVoucherInProcessing = isChecked?.some(item => item?.status !== appConst.STATUS_REVOKED.PROCESSED.id);

    if (isChecked.length === 0) {
      toast.warning("Cần chọn phiếu thu hồi trước khi thanh lý")
      return false;
    }
    if (hasOtherDepartment) {
      toast.warning(t("RecallSlip.asynchronousReceptionDepartment"));
      return false;
    }
    if (hasVoucherInProcessing) {
      toast.warning("Chỉ được thanh lý những phiếu ở trạng thái đã thu hồi")
      return false;
    }

    return true;
  }

  handleAddLiquidationSlip = async () => {
    let { isChecked, selectedList } = this.state;
    let { setPageLoading } = this.context;
    let { t } = this.props;

    if (!this.validatorLiquidationSlip()) return;

    try {
      setPageLoading(true);
      let firstReceiverDepartment = isChecked?.[0]?.receiverDepartmentId
        ? {
          id: isChecked?.[0]?.receiverDepartmentId,
          name: isChecked?.[0]?.receiverDepartmentName,
          text: isChecked?.[0]?.receiverDepartmentName,
        }
        : null;
      let searchObject = {};
      searchObject.voucherIds = selectedList.toString();

      let res = await getAssetVorcherRevocationForLiquidation(searchObject);
      const { code, data } = res?.data;
      if (!isSuccessfulResponse(code)) {
        handleThrowResponseMessage(res);
      }

      this.setState({
        item: {
          isFromRecall: true,
          assetVouchers: data,
          handoverDepartment: firstReceiverDepartment,
          handoverDepartmentId: firstReceiverDepartment?.id,
        },
        shouldOpenAddLiquidationSlipDialog: true,
        assetVouchers: data
      })
    } catch (e) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const { tabValue } = this.state;

    try {
      setPageLoading(true);
      let fileName = '';
      const sendData = convertExcelPayload(this.state, appConst.assetClass.TSCD);

      switch (tabValue) {
        case appConst.tabRecallSlip.tabExpected:
          fileName = t("exportToExcel.iatExpectedRevocation");
          break;
        case appConst.tabRecallSlip.tabRecall:
          fileName = t("exportToExcel.iatRevocation");
          break;
        default:
          fileName = "Thu hồi";
          break;
      }

      await functionExportToExcel(
        exportAssetsToExcel,
        sendData,
        fileName,
      );
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  exportExampleImportExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcelAssetTransfer,
        {},
        t("exportToExcel.transferAssetImportExample")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };


  setStateTransfer = (item) => {
    this.setState(item);
  };
  handleConfirmationReceive = () => {
    let { setPageLoading } = this.context;
    let status = appConst.listStatusTransfer[3].indexOrder;
    let issueDate = formatDateTimeDto(this.state.item?.issueDate, 23, 59, 59);
    let dataTranfer = {
      dataState: this.state?.item,
      id: this.state?.item?.id,
      status: status,
      issueDate: issueDate,
    };

    setPageLoading(true);
    updateStatus(dataTranfer)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info("Cập nhật thành công");
          this.handleOKEditClose();
        } else {
          toast.warning(data?.message);
          this.handleOKEditClose();
        }
      })
      .catch(() => {
        setPageLoading(false);
        toast.error(this.props.t("toastr.error"));
        this.handleOKEditClose();
      });
  };
  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  formatDateEdit = (tool) => {
    let listAssetDocumentId = [];

    tool.createDate = formatDateTypeArray(tool?.createDate);

    let statusVoucher = this.state?.isTabExpected
      ?
      appConst.listStatusRecallSlip.find(i => i?.indexOrder === tool?.status)
      :
      appConst.revokedstatus.find(i => i?.indexOrder === tool?.status);

    let objStatus = {
      id: statusVoucher?.indexOrder,
      indexOrder: statusVoucher?.indexOrder,
      name: statusVoucher?.name
    }

    tool.transferStatus = { ...objStatus };

    tool.handoverDepartment = {
      id: tool?.handoverDepartmentId,
      code: tool?.handoverDepartmentCode,
      name: tool?.handoverDepartmentName,
      text:
        tool?.handoverDepartmentName +
        " - " +
        tool?.handoverDepartmentCode,
    };

    tool.receiverDepartment = {
      id: tool?.receiverDepartmentId,
      code: tool?.receiverDepartmentCode,
      name: tool?.receiverDepartmentName,
      text:
        tool?.receiverDepartmentName +
        " - " +
        tool?.receiverDepartmentCode,
    };

    tool.receiverPerson = {
      displayName: tool?.receiverPersonName,
    };
    tool.handoverPerson = {
      displayName: tool?.receiverPersonName,
    };

    tool?.assetVouchers?.map((item) => {
      let data = { ...item };

      item.usePerson = {
        id: item?.usePersonId,
        displayName: item?.usePersonDisplayName,
      };

      item.managementDepartment = {
        id: item?.assetManagementDepartmentId,
        code: item?.assetManagementDepartmentCode,
        name: item?.assetManagementDepartmentName,
      };

      item.receiverDepartment = {
        code: item?.receiveDepartmentCode,
        id: item?.receiveDepartmentId,
        name: item?.receiveDepartmentName,
      };

      item.asset = data;
      return item;
    });

    // eslint-disable-next-line no-unused-expressions
    tool?.documents?.map((item) => {
      listAssetDocumentId.push(item?.id);
    });
    tool.listAssetDocumentId = listAssetDocumentId;
    return tool;
  };

  handleEdit = (id) => {
    let { t } = this.props;
    let { isTabExpected } = this.state
    let { setPageLoading } = this.context;
    setPageLoading(true);
    const getItemFunction = isTabExpected
      ? getItemById
      : getRevocationVouchersById;
    getItemFunction(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetRecallSlip = data?.data ? data?.data : {};
        assetRecallSlip = this.formatDateEdit(assetRecallSlip);
        assetRecallSlip.isStatus = false;
        assetRecallSlip.isView = false;
        assetRecallSlip.isStatusConfirmed = false;
        assetRecallSlip.isCheckReceiverDP = false;
        isTabExpected ?
          this.setState({
            item: assetRecallSlip,
            shouldOpenEditorDialog: true,
          }) : this.setState({
            item: assetRecallSlip,
            shouldOpenAddDialog: true,
          })
      })
      .catch((error) => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleView = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    const getItemFunction = this.state.isTabExpected
      ? getItemById
      : getRevocationVouchersById;
    getItemFunction(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetRecallSlip = data?.data ? data?.data : {};
        assetRecallSlip = this.formatDateEdit(assetRecallSlip);
        assetRecallSlip.isView = true;
        assetRecallSlip.isStatus = false;
        this.state.isTabExpected ?
          this.setState({
            item: assetRecallSlip,
            shouldOpenEditorDialog: true,
          }) : this.setState({
            item: assetRecallSlip,
            shouldOpenAddDialog: true,
          })
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleApprove = (id) => {
    let { t } = this.props;
    let {
      departmentUser
    } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    const getItemFunction = this.state.isTabExpected
      ? getItemById
      : getRevocationVouchersById;
    getItemFunction(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetRecallSlip = data?.data ? data?.data : {};
        assetRecallSlip = this.formatDateEdit(assetRecallSlip);
        if (assetRecallSlip?.assetVouchers?.length > 0 && departmentUser) {
          let isAccept = assetRecallSlip.assetVouchers.find(i => i?.assetManagementDepartmentId === departmentUser?.id);
          if (!isAccept) {
            //Phòng ban không quản lý các tài sản trong phiếu, không thể xác nhận.
            toast.info(t("AssetTransfer.message.checkAssetByDepartment"));
            return;
          };
        }
        assetRecallSlip.isView = true;
        assetRecallSlip.isConfirm = true;
        assetRecallSlip.isStatus = false;
        this.state.isTabExpected ?
          this.setState({
            item: assetRecallSlip,
            shouldOpenEditorDialog: true,
          }) : this.setState({
            item: assetRecallSlip,
            shouldOpenAddDialog: true,
          });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handlePrint = (id) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    const getItemFunction = this.state.isTabExpected
      ? getItemById
      : getRevocationVouchersById;
    getItemFunction(id)
      .then(({ data }) => {
        setPageLoading(false);
        let assetRecallSlip = data?.data ? data?.data : {};
        assetRecallSlip = this.formatDateEdit(assetRecallSlip);
        this.state.isTabExpected ?
          this.setState({
            item: assetRecallSlip,
            isPrint: true,
          }) : this.setState({
            item: assetRecallSlip,
            isPrint: true,
          })
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  convertDataDto = async (rowData) => {
    let { t } = this.props;
    try {
      let { setPageLoading } = this.context;
      setPageLoading(true);
      const getItemFunction = this.state.isTabExpected
        ? getItemById
        : getRevocationVouchersById;
      let { data } = await getItemFunction(rowData?.id);
      setPageLoading(false);
      let transfer = data?.data ? data?.data : {};

      transfer.createDate = formatDateTypeArray(transfer?.createDate);
      transfer.transferStatus = {
        id: transfer?.transferStatusId,
        indexOrder: transfer?.transferStatusIndex,
      };

      transfer.handoverDepartment = {
        id: transfer?.handoverDepartmentId,
        code: transfer?.handoverDepartmentCode,
        name: transfer?.handoverDepartmentName,
      };

      transfer.receiverDepartment = {
        id: transfer?.receiverDepartmentId,
        code: transfer?.receiverDepartmentCode,
        name: transfer?.receiverDepartmentName,
        text:
          transfer?.receiverDepartmentName +
          " - " +
          transfer?.receiverDepartmentCode,
      };

      transfer.assetVouchers.map((item) => {
        let data = {
          id: item?.assetId,
          name: item?.name,
          code: item?.code,
          yearPutIntoUse:
            item?.yearPutIntoUse || item?.assetYearPutIntoUser,
          managementCode: item?.managementCode,
          carryingAmount: item?.carryingAmount,
          dateOfReception: item?.dateOfReception,
          madeIn: item?.madeIn,
          originalCost: item?.originalCost,
          yearOfManufacture: item?.yearOfManufacture,
          manufacturerName: item?.manufacturerName,
          receiveDepartmentName: item?.receiveDepartmentName,
          assetModel: item?.model,
          assetSerialNumber: item?.serialNumber
        };
        item.assetModel = item?.model,
          item.assetSerialNumber = item?.serialNumber,
          item.usePerson = {
            id: item?.usePersonId,
            displayName: item?.usePersonDisplayName,
          };

        item.managementDepartment = {
          id: item?.assetManagementDepartmentId,
          code: item?.assetManagementDepartmentCode,
          name: item?.assetManagementDepartmentName,
        };

        item.receiveDepartmentName = transfer.receiverDepartmentName;

        return (item.asset = data);
      });

      return transfer;
    } catch (error) {
      toast.error(t("general.error"));
    }
  };

  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    });
  };

  handleQrCode = async (newValue, isConvert) => {
    let assetRecallSlip = isConvert
      ? await this.convertDataDto(newValue)
      : newValue || [];
    let productsUpdate = assetRecallSlip?.assetVouchers?.map((item) => {
      item.qrInfo =
        `Mã TS: ${item?.asset?.code}\n` +
        `Mã QL: ${item?.asset?.managementCode}\n` +
        `Tên TS: ${item?.asset?.name}\n` +
        `Nước SX: ${item?.asset?.madeIn}\n` +
        `Năm SX: ${item?.asset?.yearOfManufacture}\n` +
        `Năm SD: ${item?.asset?.yearPutIntoUse}\n` +
        `PBSD: ${item?.asset?.receiveDepartmentName}\n`;
      return item;
    });
    this.setState({
      products: productsUpdate,
      openPrintQR: true,
    });
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.edit === method) {
      this.handleEdit(rowData?.id);
    } else if (appConst.active.delete === method) {
      this.handleDelete(rowData?.id);
    } else if (appConst.active.view === method) {
      this.handleView(rowData?.id);
    } else if (appConst.active.check === method) {
      this.handleApprove(rowData?.id);
    } else if (appConst.active.print === method) {
      this.handlePrint(rowData?.id);
    } else if (appConst.active.qrcode === method) {
      this.handleQrCode(rowData, true);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkStatus = (status) => {
    let { isTabExpected } = this.state
    const functionStatus = isTabExpected
      ? appConst.STATUS_EXPECTED_RECOVERY
      : appConst.STATUS_REVOKED

    const itemStatus = Object.values(functionStatus).find((item) => item?.id === status);
    return itemStatus?.name || '';
  };

  handleGetRowData = (rowData) => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    const getItemFunction = this.state.isTabExpected
      ? getItemById
      : getRevocationVouchersById
    getItemFunction(rowData?.id)
      .then(({ data }) => {
        setPageLoading(false);
        this.setState({
          selectedItem: data?.data,
          rowDataAssetVouchersTable: data?.data?.assetVouchers || []
        });
      })
      .catch((error) => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });

  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name, source) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }

    this.setState({ [name]: data }, () => {
      this.search();
    })
  };

  render() {
    const { t, i18n } = this.props;
    let {
      page,
      tabValue,
      rowsPerPage,
      isChecked,
      isRoleAdmin,
      hasEditPermission,
      isRoleAssetManager,
      hasWatchPermission,
      hasCheckPermission,
      hasPrintPermission,
      hasDeletePermission,
      rowDataAssetVouchersTable,
      currentUser,
      isRoleOrgAdmin,
      checkAll,
      isOwner,
      isTabExpected
    } = this.state;
    let TitlePage = t("Dashboard.management.recallIat");

    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        maxWidth: "170px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            value={tabValue}
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasWatchPermission={hasWatchPermission}
            hasCheckPermission={hasCheckPermission}
            hasPrintPermission={hasPrintPermission}
            isRoleAdmin={isRoleAdmin}
            isRoleAssetManager={isRoleAssetManager}
            currentUser={currentUser}
            isRoleOrgAdmin={isRoleOrgAdmin}
            isChecked={isChecked}
            isOwner={isOwner}
            onSelect={(rowData, method) =>
              this.handleCheckIcon(rowData, method)
            }
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("RecallSlipIat.suggestDate"),
        field: "suggestDate",
        align: "left",
        hidden: tabValue === appConst.tabRecallSlipTool.tabRecall,
        width: 120,
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.suggestDate
          ? formatTimestampToDate(rowData.suggestDate)
          : ""
      },
      {
        title: t("RecallSlipIat.issueDate"),
        field: "suggestDate",
        align: "left",
        hidden: tabValue === appConst.tabRecallSlipTool.tabExpected,
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          let [year, month, day] = rowData?.issueDate;
          month = month < 10 ? "0" + month : month;
          day = day < 10 ? "0" + day : day;
          return `${day}/${month}/${year}`
        }
      },
      {
        title: t("RecallSlipIat.status"),
        field: "status",
        width: "220px",
        minWidth: "220px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.status;
          const { DANG_THONG_KE, DA_THONG_KE, DA_THU_HOI, DANG_TONG_HOP } = appConst.STATUS_EXPECTED_RECOVERY;
          const classByStatus = {
            [DANG_THONG_KE.id]: "warning",
            [DA_THONG_KE.id]: "success",
            [DA_THU_HOI.id]: "info",
            [DANG_TONG_HOP.id]: "error",
          }
          let className = `status status-${classByStatus[statusIndex]}`;
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("RecallSlipIat.handoverDepartment"),
        field: "departmentName",
        hidden: !isTabExpected,
        width: "220px",
        minWidth: "220px",
      },
      {
        title: t("RecallSlipIat.handoverPerson"),
        field: "personName",
        hidden: !isTabExpected,
        width: "200px",
        minWidth: "200px",
      },
      {
        title: t("RecallSlipIat.receiverDepartment"),
        field: "receiptDepartmentName",
        width: 180,
        minWidth: 180,
        render: (rowData) => rowData?.receiptDepartmentName || rowData?.receiverDepartmentName
      },
      {
        title: t("RecallSlipIat.receiverPerson"),
        field: "receiptPersonName",
        minWidth: 200,
        render: (rowData) => rowData?.receiptPersonName || rowData?.receiverPersonName
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: "50px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("Asset.code"),
        field: "code",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
      },
      {
        title: t("Asset.serialNumber"),
        field: "serialNumber",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("Asset.model"),
        field: "model",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("RecallSlipIat.dxQuantity"),
        field: "dxRequestQuantity",
        align: "left",
        minWidth: 120,
        hidden: !isTabExpected,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("RecallSlipIat.recallQuantity"),
        field: "quantity",
        align: "left",
        hidden: isTabExpected,
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
      },
      {
        title: t("InstrumentToolsList.originalCost"),
        field: "originalCost",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.originalCost
            ? convertNumberPriceRoundUp(rowData?.originalCost)
            : "",
      },
      {
        title: t("Asset.carryingAmount"),
        field: "carryingAmount",
        align: "left",
        minWidth: 150,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.carryingAmount
            ? convertNumberPriceRoundUp(rowData?.carryingAmount)
            : "",
      },
      {
        title: t("Asset.managementDepartment"),
        field: "managementDepartmentName",
        align: "left",
        minWidth: 200,
        maxWidth: 250,
      },
      {
        title: t("RecallSlip.importStatus"),
        field: "assetImportStatus",
        align: "left",
        minWidth: 200,
        maxWidth: 250,
      },
      {
        title: t("Asset.note"),
        field: "note",
        align: "left",
        minWidth: 180,
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/recall-slip",
              },
              { name: t("Dashboard.management.recallIat") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("RecallSlipIat.expected_recall")}</span>
                </div>
              }
            />
            {!this.state.isRoleAssetUser && (
              <Tab
                className="tab"
                label={
                  <div className="tabLable">
                    <span>
                      {t("RecallSlipIat.recall_list")}
                    </span>
                  </div>
                }
              />
            )}
          </Tabs>
        </AppBar>
        {this.state?.shouldOpenConfirmationReceiveDialog && (
          <ConfirmationDialog
            title={t("InstrumentToolsTransfer.titleConfirmationReceive")}
            open={this.state?.shouldOpenConfirmationReceiveDialog}
            onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleConfirmationReceive}
            text={t("AssetTransfer.confirmationReceive")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}

        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabAll}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleButtonAdd}
            handleAddRecallSlip={this.handleAddRecallSlip}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
            handleAddLiquidationSlip={this.handleAddLiquidationSlip}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabTransfer.tabWaitConfirmation}
          className="mp-0"
        >
          <ComponentTransferTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleButtonAdd={this.handleAddRecallSlip}
            handleAddRecallSlip={this.handleAddRecallSlip}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleConfirmationReceive={this.handleConfirmationReceive}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateTransfer}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            isButton={true}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
            handleAddLiquidationSlip={this.handleAddLiquidationSlip}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataAssetVouchersTable ?? []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ witdth: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}
RecallSlipToolTable.contextType = AppContext;
export default RecallSlipToolTable;
