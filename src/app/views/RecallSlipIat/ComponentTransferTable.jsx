import {
    Card,
    Grid,
    Input,
    Button,
    Collapse,
    FormControl,
    InputAdornment,
    TablePagination,
} from "@material-ui/core";
import React, { useContext, useEffect } from "react";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst, variable } from "app/appConst";
import { ValidatorForm } from "react-material-ui-form-validator";
import { filterOptions } from "app/appFunction";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import AssetsQRPrint from "../Asset/ComponentPopups/AssetsQRPrint";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import viLocale from "date-fns/locale/vi";
import CardContent from "@material-ui/core/CardContent";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { getListManagementDepartment } from "../AssetTransfer/AssetTransferService";
import { getAllManagementDepartmentByOrg } from "../Department/DepartmentService";
import ExpectedRevocationDialog from "./ExpectedRevocationDialog";
import RevocationVoucherDialog from "./RevocationVoucherDialog";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import moment from "moment";
import InstrumentsToolTransferDialog from "../InstrumentsToolLiquidate/InstrumentsToolLiquidateDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import {handleConvertRecallPrintData} from "../FormCustom/Recall";

function ComponentTransferTable(props) {
    let { t, i18n, getRowData, isButton } = props;
    let {
        item,
        page,
        isPrint,
        itemList,
        fromDate,
        openAdvanceSearch,
        toDate,
        rowsPerPage,
        isRoleAdmin,
        statusIndex,
        totalElements,
        transferStatus,
        isRoleAssetUser,
        receiverDepartment,
        handoverDepartment,
        listHandoverDepartment,
        listReceiverDepartment,
        shouldOpenEditorDialog,
        shouldOpenAddDialog,
        shouldOpenImportExcelDialog,
        shouldOpenConfirmationDialog,
        shouldOpenConfirmationDeleteAllDialog,
        shouldOpenAddLiquidationSlipDialog,
        openPrintQR,
        products,
        tabValue,
        selectedItem
    } = props?.item;
    const { currentOrg } = useContext(AppContext);
    const { RECALL } = LIST_PRINT_FORM_BY_ORG.IAT_MANAGEMENT;
    let isButtonAll = !statusIndex;
    const searchObject = { pageIndex: 1, pageSize: 1000000 };
    const searchObjectDepartment = {
        ...searchObject,
        checkPermissionUserDepartment: false
    }

    useEffect(() => {
        let value = appConst.listStatusTransfer.find(item => item.indexOrder === statusIndex)
        props.handleSetDataSelect(value, "transferStatus")
    }, [statusIndex]);

    const handleKeyDown = (e) => {
        if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
            e.preventDefault();
        }
        props.handleKeyDownEnterSearch(e);
    };

    const dataView = handleConvertRecallPrintData(item);

    return (
        <>
            <Grid container spacing={2} justifyContent="space-between">
                <Grid item md={7} xs={12}>
                    {/* {(hasCreatePermission || isRoleAdmin) && isButtonAll && ( */}
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            props.handleButtonAdd({
                                startDate: new Date(),
                                endDate: new Date(),
                                isStatus: false,
                                isNew: true,
                                isView: false,
                                isCheckReceiverDP: true,
                                isCheckHandoverDP: true,
                                isTabValue: tabValue
                            });
                        }}
                    >
                        {tabValue === appConst.tabTransfer.tabAll ? t("Asset.recall_slip") : t("Thêm phiếu thu hồi")}
                    </Button>
                    {/* )} */}
                    {!isRoleAssetUser && tabValue === appConst.tabRecallSlip.tabRecall && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleAddLiquidationSlip({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("RecallSlip.liquidation_slip")}
                        </Button>
                    )}
                    {!isRoleAssetUser && tabValue === appConst.tabRecallSlip.tabRecall && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={props.exportToExcel}
                        >
                            {t("general.exportToExcel")}
                        </Button>
                    )}
                    {!isRoleAssetUser && !isButton && (
                        <Button
                            className="mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleAddRecallSlip({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("RecallSlipIat.addNewReacllSlip")}
                        </Button>
                    )}
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>


                    {shouldOpenImportExcelDialog && (
                        <ImportExcelDialog
                            t={t}
                            i18n={i18n}
                            handleClose={props.handleDialogClose}
                            open={shouldOpenImportExcelDialog}
                            handleOKEditClose={props.handleOKEditClose}
                            exportExampleImportExcel={props.exportExampleImportExcel}
                        />
                    )}

                </Grid>
                <Grid item md={5} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("RecallSlipIat.search")}
                            id="search_box"
                            startAdornment={
                                <InputAdornment position="end">
                                    <SearchIcon
                                        onClick={() => props.search()}
                                        className="searchTable"
                                    />
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
                {/* Bộ lọc xuất excel */}
                <Grid item xs={12}>
                    <Collapse in={openAdvanceSearch}>
                        <ValidatorForm onSubmit={() => { }}>
                            <Card elevation={2}>
                                <CardContent>
                                    <Grid container xs={12} spacing={2}>
                                        {/* Phòng bàn giao */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteTransfer
                                                className="w-100"
                                                label={t("allocation_asset.handoverDepartment")}
                                                searchFunction={getListManagementDepartment}
                                                searchObject={searchObjectDepartment}
                                                listData={listReceiverDepartment || []}
                                                setListData={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "listReceiverDepartment",
                                                    variable.listInputName.listData,
                                                )}
                                                defaultValue={receiverDepartment ? receiverDepartment : null}
                                                displayLable={"text"}
                                                value={receiverDepartment ? receiverDepartment : null}
                                                onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                                                filterOptions={(options, params) => {
                                                    params.inputValue = params.inputValue.trim()
                                                    return filterOptions(options, params)
                                                }}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        {/* Phòng tiếp nhận */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteSub
                                                label={t("allocation_asset.receiverDepartment")}
                                                searchFunction={getAllManagementDepartmentByOrg}
                                                searchObject={searchObject}
                                                listData={listHandoverDepartment || []}
                                                typeReturnFunction="list"
                                                displayLable={"text"}
                                                isNoRenderChildren
                                                isNoRenderParent
                                                setListData={(data) => props?.handleSetDataSelect(
                                                    data,
                                                    "listHandoverDepartment",
                                                    variable.listInputName.listData,
                                                )}
                                                defaultValue={handoverDepartment ? handoverDepartment : null}
                                                value={handoverDepartment ? handoverDepartment : null}
                                                onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        {/* Từ ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxFrom")}
                                                    format="dd/MM/yyyy"
                                                    value={fromDate ?? null}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "fromDate"
                                                    )}
                                                    maxDate={toDate || undefined}
                                                    minDateMessage={t("general.minDateMessage")}
                                                    maxDateMessage={t("general.maxDateFromDate")}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        {/* Đến ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxTo")}
                                                    format="dd/MM/yyyy"
                                                    value={toDate ?? null}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "toDate"
                                                    )}
                                                    minDate={fromDate || undefined}
                                                    minDateMessage={t("general.minDateToDate")}
                                                    maxDateMessage={t("general.maxDateMessage")}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        {/* Trạng thái */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteSub
                                                label={t("maintainRequest.status")}
                                                searchFunction={() => { }}
                                                listData={props.item.listStatus}
                                                value={transferStatus ? transferStatus : null}
                                                name="transferStatus"
                                                onSelect={props.handleSetDataSelect}
                                                filterOptions={filterOptions}
                                                displayLable="name"
                                                disabled={!isButtonAll}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </ValidatorForm>
                    </Collapse>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {isPrint && (
                            <PrintMultipleFormDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props?.handleDialogClose}
                                open={isPrint}
                                item={dataView || item}
                                title={t("Phiếu hoàn trả")}
                                urls={[
                                    ...RECALL.GENERAL,
                                    ...(RECALL[currentOrg?.printCode] || []),
                                ]}
                            />
                        )}
                        {shouldOpenEditorDialog && (
                            <ExpectedRevocationDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                handleQrCode={props.handleQrCode}
                            />
                        )}
                        {shouldOpenAddDialog && (
                            <RevocationVoucherDialog
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleDialogClose}
                                open={shouldOpenAddDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                                handleQrCode={props.handleQrCode}
                            />
                        )}
                        {shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {openPrintQR && (
                            <AssetsQRPrint
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                        {shouldOpenAddLiquidationSlipDialog && (
                            <InstrumentsToolTransferDialog
                                t={t}
                                i18n={i18n}
                                open={shouldOpenAddLiquidationSlipDialog}
                                handleClose={props.handleDialogClose}
                                handleOKEditClose={props.handleOKEditClose}
                                item={item}
                            />
                        )}
                    </div>
                    <MaterialTable
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            maxBodyHeight: "490px",
                            minBodyHeight: "260px",
                            padding: "dense",
                            toolbar: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: selectedItem?.id === rowData?.id
                                    ? "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        labelRowsPerPage={t("general.rows_per_page")}
                        labelDisplayedRows={({ from, to, count, page }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
                        }
                        component="paper"
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{ "aria-label": "Previous Page", }}
                        nextIconButtonProps={{ "aria-label": "Next Page", }}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid >
        </>
    )
}

export default ComponentTransferTable;
