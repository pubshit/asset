import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const RecallSlipTool = EgretLoadable({
  loader: () => import("./RecallSlipIat")

});
const ViewComponent = withTranslation()(RecallSlipTool);

const RecallSlipIatRoutes = [
  {
    path: ConstantList.ROOT_PATH + "instruments-and-tools/recall-slip",
    exact: true,
    component: ViewComponent
  }
];

export default RecallSlipIatRoutes;