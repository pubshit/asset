import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  addNewOrUpdateAssetGroup,
  checkCode,
  checkParent,
} from "./AssetGroupService";
import SelectAssetGroupPopup from "../Component/AssetGroup/SelectAssetGroupPopup";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import { NumberFormatCustom, PaperComponent } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
class AssetGroupEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    depreciationRate: 0,
    yearApply: "",
    shouldOpenAssetGroupPopup: false,
    shouldOpenNotificationPopup: false,
    parent: {},
    Notification: "",
    viewIndex: "",
    soNamSuDung: "",
  };
  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleChange = (event, source) => {
    const field = event.target.name;
    let inputValue = event?.target?.value;

    // if (
    //   field === variable.listInputName.code
    //   || field === variable.listInputName.viewIndex
    // ) {
    //   inputValue = this.handleValueByParent(inputValue, field);
    // }

    this.setState({
      [field]: inputValue,
    });

  };

  handleValueByParent = (inputValue, field) => {
    let { parent } = this.state;

    let newValue = "";
    if (parent?.id) {
      let prefix = parent?.[field] + ".";
      if (inputValue.includes(prefix)) {
        newValue = inputValue;
      } else {
        newValue = prefix;
      }
    } else {
      newValue = inputValue;
    }
    return newValue;
  }


  handleCodeChange = (e) => {
    const newCode = e.target.value;
    this.setState({ code: newCode });

    // Kiểm tra và cập nhật viewIndex nếu cần
    if (!this.state?.viewIndex.startsWith(newCode)) {
      this.setState({ viewIndex: `${newCode}.1` });
    }
  };

  handleViewIndexChange = (e) => {
    let { code } = this.state;
    const newViewIndex = e.target.value;

    // Kiểm tra xem viewIndex có bắt đầu với code không
    if (newViewIndex.startsWith(code)) {
      this.setState({ viewIndex: newViewIndex });
    } else {
      // Nếu viewIndex không bắt đầu với code, có thể báo lỗi hoặc tự động sửa
      this.setState({ viewIndex: `${code}.1` }); // Cập nhật lại cho đúng định dạng
    }
  };

  handleFormSubmit = () => {
    let { id, code } = this.state;
    let { t, handleSelect } = this.props;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    checkCode(id, code).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        if (data?.data) {
          toast.warning(t("AssetGroup.noti.dupli_code"));
        } else {
          if (id) {
            checkParent({ ...this.state }).then((isCheck) => {
              if (isCheck.data) {
                toast.error(t("AssetGroup.noti.updateFailParent"));
              } else {
                addNewOrUpdateAssetGroup({
                  ...this.state,
                }).then(() => {
                  this.props.handleOKEditClose();
                  toast.success(t("AssetGroup.noti.updateSuccess"));
                }).catch(() => {
                  toast.error(t("toastr.error"))
                });
              }
            }).catch(() => {
              toast.error(t("toastr.error"))
            });
          } else {
            addNewOrUpdateAssetGroup({
              ...this.state,
            }).then((item) => {
              if (item?.status === appConst.CODE.SUCCESS && item?.data?.data && item.data?.code === appConst.CODE.SUCCESS) {
                this.props.handleOKEditClose();
                //eslint-disable-next-line
                handleSelect?.(item?.data?.data)
                toast.success(t("AssetGroup.noti.createdSuccess"));
              }
              else {
                toast.warning(item?.data?.message)
              }
            }).catch(() => {
              toast.error(t("toastr.error"))
            });
          }
        }
      } else {
        toast.error(data?.message);
      }
    }).catch(() => {
      toast.error(t("toastr.error"))
    });;
  };

  componentWillMount() {
    let { item, assetClass, isIATType } = this.props;
    this.setState({
      ...item,
      assetClass: assetClass ? assetClass : appConst.assetClass.TSCD,
      isIATType,
    });
  }


  handleAssetGroupPopupClose = () => {
    this.setState({
      shouldOpenAssetGroupPopup: false,
    });
  };

  handleSelectAssetGroup = (item) => {
    this.setState({ parent: item }, function () {
      let text = '';
      if (item?.code) {
        text = item?.code + ".";
        this.setState({ code: text })
      }
      this.handleAssetGroupPopupClose();
    });
  };

  render() {
    let { open, t, i18n } = this.props;
    let {
      id,
      name,
      code,
      parent,
      isIATType,
      viewIndex,
      yearApply,
      namSuDung,
      depreciationRate,
      shouldOpenAssetGroupPopup,
      shouldOpenNotificationPopup,
    } = this.state;

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {
            isIATType
              ? t("AssetGroup.saveUpdateIATType")
              : t("AssetGroup.saveUpdate")
          }
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={10} xs={9}>
                <TextValidator
                  InputProps={{
                    readOnly: true,
                  }}
                  label={t("AssetGroup.parent")}
                  className=""
                  fullWidth
                  value={parent != null ? parent.name || "" : ""}
                />

                {shouldOpenAssetGroupPopup && (
                  <SelectAssetGroupPopup
                    open={shouldOpenAssetGroupPopup}
                    handleSelect={this.handleSelectAssetGroup}
                    selectedItem={parent != null ? parent : {}}
                    handleClose={this.handleAssetGroupPopupClose}
                    t={t}
                    i18n={i18n}
                    assetClass={this.props.assetClass}
                    editingItemCode={this.state.code}
                  />
                )}
              </Grid>
              <Grid item sm={2} xs={3}>
                <Button
                  className=" mt-10"
                  variant="contained"
                  size="small"
                  color="primary"
                  onClick={() =>
                    this.setState({ shouldOpenAssetGroupPopup: true, item: {} })
                  }
                >
                  {t("general.select")}
                </Button>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("AssetGroup.name")}
                      <span className="colorRed"> *</span>
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("AssetGroup.code")}
                      <span className="colorRed"> *</span>
                    </span>
                  }
                  onChange={this.handleCodeChange}
                  type="text"
                  name="code"
                  value={code}
                  placeholder={t("general.onlyTypeNumberEndDots")}
                  validators={["required", `matchRegexp:${variable.regex.onlyNumberAndDot}`, "matchRegexp:^[0-9.]+.?$"]}
                  errorMessages={[
                    t("general.required"),
                    t("general.invalidFormatCode"),
                    t("general.notEndWithDots"),
                  ]}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      {t("AssetGroup.viewIndex")}
                      <span className="colorRed"> *</span>
                    </span>
                  }
                  onChange={this.handleViewIndexChange}
                  type="text"
                  name="viewIndex"
                  value={viewIndex}
                  validators={["required", `matchRegexp:${variable.regex.onlyNumberAndDot}`, "minNumber:1",]}
                  errorMessages={[t("general.required"), t("general.invalidFormatCode"), t("general.minNumberError")]}
                />
              </Grid>
              {!isIATType && (
                <Grid item sm={4} xs={12}>
                  <TextValidator
                    className="w-100"
                    label={
                      <span>
                        {t("AssetGroup.NumberOfYearsUsed")}
                        <span className="colorRed"> *</span>
                      </span>
                    }
                    onChange={this.handleChange}
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                    type="text"
                    name="namSuDung"
                    value={namSuDung}
                    validators={["matchRegexp:^[0-9]*$", "required", "minNumber:0"]}
                    errorMessages={[t("AssetGroup.matchYear"), t("general.required"), t("general.nonNegativeNumber")]}
                  />
                </Grid>
              )}
              {!isIATType && (
                <Grid item sm={4} xs={12}>
                  <TextValidator
                    className="w-100"
                    label={t("AssetGroup.depreciationRate")}
                    onChange={this.handleChange}
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                    type="text"
                    name="depreciationRate"
                    value={depreciationRate ? depreciationRate : ""}
                    validators={["minNumber:0"]}
                    errorMessages={[t("general.nonNegativeNumber")]}
                  />
                </Grid>
              )}
              {!isIATType && (
                <Grid item sm={4} xs={12}>
                  <TextValidator
                    className="w-100"
                    label={<span>{t("AssetGroup.yearApply")}</span>}
                    onChange={this.handleChange}
                    type="text"
                    name="yearApply"
                    value={yearApply ? yearApply : ""}
                    validators={["matchRegexp:^[0-9]*$", "maxNumber:2100", "minNumber:1900"]}
                    errorMessages={[t("AssetGroup.matchYear"), t("general.maxDateDefault"), t("general.minYearDefault")]}
                  />
                </Grid>
              )}
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                className="mr-15"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AssetGroupEditorDialog;
