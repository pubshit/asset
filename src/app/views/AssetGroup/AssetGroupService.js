import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/assetgroup" + ConstantList.URL_PREFIX;

export const getTreeView = () => {
  var url = API_PATH + "/tree";
  return axios.get(url);
};

export const getAllAssetGroups = () => {
  return axios.get(API_PATH + "/1/100000");
};
export const getByRoot = (searchObject) => {
  return axios.get(
    API_PATH +
      "/getbyroot/1/" +
      searchObject.pageIndex +
      "/" +
      searchObject.pageSize
  );
};

export const getByRootByAssetClass = (searchObject) => {
  return axios.get(
    API_PATH +
      "/getbyroot/" +
      searchObject.assetClass +
      "/" +
      searchObject.pageIndex +
      "/" +
      searchObject.pageSize
  );
};

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/getById/" + id);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/check-code";
  return axios.get(url, config);
};

export const addNewOrUpdateAssetGroup = (AssetGroup) => {
  return axios.post(API_PATH, AssetGroup);
};

export const searchByPage = (searchObject) => {
  let url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};
export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
export const checkParent = (dto) => {
  var url = API_PATH + "/checkParent";
  return axios.post(url, dto);
};
export const exportExampleImportExcelAssetGroup = (assetgroup) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT + "/api/fileDownload/exportExampleAssetGroup",
    data: assetgroup,
    responseType: "blob",
  });
};

export const getByRootNew = (searchObject) => {
  let config = {
    params: {
      orgId: searchObject.orgId
    }
  }
  let url = API_PATH
    + `/getbyroot/${searchObject.assetClass}/${searchObject.pageIndex}/${searchObject.pageSize}`;
  return axios.get(url, config);
};
