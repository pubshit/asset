import React from 'react'
import {Button, Grid, Icon, IconButton, TablePagination} from '@material-ui/core'
import MaterialTable, {MTableToolbar} from 'material-table'
import {deleteCheckItem, exportExampleImportExcelAssetGroup, getByRoot, getItemById} from './AssetGroupService'
import AssetGroupEditorDialog from './AssetGroupEditorDialog'
import {Breadcrumb, ConfirmationDialog} from 'egret'
import FileSaver, {saveAs} from "file-saver";
import {useTranslation} from 'react-i18next'
import {Helmet} from 'react-helmet'
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup'
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ImportExcelDialog from "./ImportExcelDialog";
import {appConst} from 'app/appConst'
import {LightTooltip} from "../Component/Utilities";
import {functionExportToExcel, handleKeyDown, labelDisplayedRows} from "../../appFunction";
import AppContext from "../../appContext";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});
function MaterialButton(props) {
  const { t, i18n } = useTranslation()
  const item = props.item
  return (
    <div className="none_wrap">
      <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">edit</Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">delete</Icon>
        </IconButton>
      </LightTooltip>
    </div>
  )
}

class AssetGroupTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    AssetGroup: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    Notification: ""
  }

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { })
  }

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search)

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData()
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }

  search = () => {
    this.setPage(0);
  }

  clone = (itemListClone, itemList, count) => {

    itemListClone.forEach(el => {
      if (el.children != null && el.children.length > 0) {
        el.children.forEach(item => {
          if (itemList.length === 0) {
            itemList.push(item)
          } else {
            itemList.forEach(e => {
              if (e.id !== item.id) {
                itemList.push(item)
              }
            })
          }
        })
      }
      if (el.parent !== null) {
        if (itemList.length === 0) {
          itemList.push(el.parent)
        } else {
          itemList.forEach(e => {
            if (e.id !== el.parent) {
              itemList.push(el.parent)
            }
          })
        }

      }
      this.clone(itemList, itemList, count);
    })

  }

  getListItemChild(item) {
    let result = [];
    let root = {};
    root.name = item.name;
    root.depreciationRate = item.depreciationRate;
    root.yearApply = item.yearApply;
    root.code = item.code;
    root.id = item.id;
    root.parentId = item.parentId;
    root.namSuDung = item.namSuDung;
    result.push(root);
    if (item.children) {
      item.children.forEach(child => {
        let childs = this.getListItemChild(child);
        result.push(...childs);
      });
    }
    return result;
  }

  updatePageData = () => {
    let searchObject = {}
    searchObject.keyword = this.state.keyword
    searchObject.pageIndex = this.state.page + 1
    searchObject.pageSize = this.state.rowsPerPage

    getByRoot(searchObject).then(({ data }) => {
      let treeValues = [];
      let itemListClone = [...data.content];

      itemListClone.forEach(item => {
        let items = this.getListItemChild(item);
        treeValues.push(...items);
      })

      this.setState({
        itemList: treeValues,
        totalElements: data.totalElements,
      }, () => {
      })
    })
  }
  exportExampleImportExcel = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcelAssetGroup,
        {},
        t("exportToExcel.templateImportAssetGroup"),
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenImportExcelDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false,
      data: [],
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenImportExcelDialog: false,
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
    })
    this.updatePageData()
  }

  handleConfirmationResponse = () => {
    let { t } = this.props
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1
      this.setState({
        page: count,
      })
    } else if (this.state.itemList.length === 1 && this.state.page === 1) {
      this.setState({
        page: 1,
      })
    }
    deleteCheckItem(this.state.id)
      .then((res) => {
        if (res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t('general.deleteSuccess'));
        } else {
          toast.warning(t('AssetGroup.noti.use'));
        }
        this.handleDialogClose()
        this.updatePageData()
      })
      .catch((err) => {
        toast.warning(t('AssetGroup.noti.use'));
      })
  }

  componentDidMount() {
    this.updatePageData()
  }

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    })
  }

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    })
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  render() {
    const { t, i18n } = this.props
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenNotificationPopup
    } = this.state
    let TitlePage = t('AssetGroup.title')

    let columns = [
      {
        title: t('general.action'),
        field: 'custom',
        align: 'center',
        minWidth: '100px',
        maxWidth: '100px',
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                getItemById(rowData.id).then(({ data }) => {
                  if (data === null) {
                    data = {}
                  }
                  this.setState({
                    item: data,
                    shouldOpenEditorDialog: true,
                  })
                })
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id)
              } else {
                alert('Call Selected Here:' + rowData.id)
              }
            }}
          />
        ),
      },
      {
        title: t('AssetGroup.code'),
        field: 'code',
        minWidth: '140px',
        maxWidth: '140px',
        align: 'left',
        cellStyle: {
          textAlign: "center",
        }
      },
      {
        title: t('AssetGroup.name'),
        field: 'name',
        align: 'left',
        minWidth: 500,
      },
      {
        title: t('AssetGroup.depreciationRate'),
        field: 'depreciationRate',
        align: 'left',
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        }
      },
      {
        title: t('AssetGroup.yearApply'),
        field: 'yearApply',
        align: 'left',
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        }
      },
      {
        title: t("AssetGroup.NumberOfYearsUsed"),
        field: "namSuDung",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        }
      },
    ]

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t('Dashboard.category'), path: '/list/asset_group' },
              { name: TitlePage },
            ]}
          />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={6} sm={12} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                })
              }}
            >
              {t('general.add')}
            </Button>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.importExcel}
            >
              {t("general.importExcel")}
            </Button>
            <Button
              className="mb-16  align-bottom"
              variant="contained"
              color="primary"
              onClick={this.exportExampleImportExcel}
            >
              Mẫu Excel
            </Button>
            {this.state.shouldOpenImportExcelDialog && (
              <ImportExcelDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={this.state.shouldOpenImportExcelDialog}
                handleOKEditClose={this.handleOKEditClose}
              />
            )}
          </Grid>

          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <AssetGroupEditorDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenNotificationPopup && (
                <NotificationPopup
                  title={t('general.noti')}
                  open={shouldOpenNotificationPopup}
                  // onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleDialogClose}
                  text={t(this.state.Notification)}
                  agree={t('general.agree')}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t('general.confirm')}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  cancel={t('general.cancel')}
                  agree={t('general.agree')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                draggable: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: '450px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              onSelectionChange={(rows) => {
                this.setState({ data: rows })
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              component="div"
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={labelDisplayedRows}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{'aria-label': 'Previous Page'}}
              nextIconButtonProps={{'aria-label': 'Next Page'}}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
        <ToastContainer />
      </div>
    )
  }
}

export default AssetGroupTable;
AssetGroupTable.contextType = AppContext;
