import React, { Component } from "react";
import {
    Dialog,
    Button,
    Grid,
    FormControlLabel,
    Switch
} from "@material-ui/core";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    DateTimePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ValidatorForm, TextValidator, TextField } from "react-material-ui-form-validator";
import { deleteItem, saveItem, getItemById } from "./EQASerumBottleService";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import EQASerumBankSearchDialog from './EQASerumBankSearchDialog';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}
class EQASerumBottleEditorDialog extends Component {
    state = {
        shouldOpenConfirmationDialog: false,
    };

    handleChange = (event, source) => {
        event.persist();
        if (source === "switch") {
            this.setState({ isActive: event.target.checked });
            return;
        }
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        let { id } = this.state;
        saveItem({
            ...this.state
        }).then(() => {
            this.props.handleOKEditClose();
        });
    };

    componentWillMount() {
        let { open, handleClose, item } = this.props;
        this.setState({
            ...this.props.item
        });
    }

    handleSearchSerumBankDialogClose = () => {
        this.setState({
            shouldOpenSearchSerumBankDialog: false
        });
    };
    handleSelectSerumBank = (item) => {
        //alert('Test');
        this.setState({ eqaSerumBank: item });
        this.handleSearchSerumBankDialogClose();
    }
    handleSelectAdministrativeUnitType = (item) => {
        //alert('Test');
        this.setState({ administrativeUnit: item });
        this.handleSearchDialogClose();
    }
    handleThrombinAddedDateChange = (date) => {
        this.setState({
            thrombinAddedDate: date
        });
    };
    handleRemoveFibrinDateChange = (date) => {
        this.setState({
            removeFibrinDate: date
        });
    };
    handleCentrifugeDateChange = (date) => {
        this.setState({
            centrifugeDate: date
        });
    };

    render() {
        let {
            id,
            code,
            originalResult,
            bottleQuality,
            bottleVolume,
            localSaveBottle,
            eqaSerumBank,
            shouldOpenSearchSerumBankDialog,
            shouldOpenConfirmationDialog,
        } = this.state;

        let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
        return (
            <Dialog  open={open} PaperComponent={PaperComponent}>
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                    <span className="mb-20">{t("SaveUpdate")}</span>
                </DialogTitle>
                <DialogContent>
                    <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                        <Grid className="mb-16" container spacing={2}>
                            <Grid item sm={12} xs={12}>
                                <TextValidator
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    label={t("SampleManagement.serum-bottle.eqaSerumBank")}
                                    className="w-80 mb-16 mr-16"
                                    value={eqaSerumBank != null ? eqaSerumBank.name : ''}
                                />
                                <Button
                                    className="mb-16"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => this.setState({ shouldOpenSearchSerumBankDialog: true, item: {} })}
                                >
                                    {t('Select')}
                                </Button>
                            </Grid>
                            {shouldOpenSearchSerumBankDialog && (
                                <EQASerumBankSearchDialog
                                    open={this.state.shouldOpenSearchSerumBankDialog}
                                    handleSelect={this.handleSelectSerumBank}
                                    selectedItem={eqaSerumBank != null ? eqaSerumBank : {}}
                                    handleClose={this.handleSearchSerumBankDialogClose} t={t} i18n={i18n} />
                            )
                            }
                            <Grid item sm={6} xs={12}>
                                <FormControl className="w-100">
                                    <InputLabel htmlFor="originalResult">{t("SampleManagement.serum-bottle.OriginnalResult.title")}</InputLabel>
                                    <Select
                                        name="originalResult"
                                        value={originalResult}
                                        onChange={event => this.handleChange(event)}
                                        input={<Input id="originalResult" />}
                                    >
                                        <MenuItem value={1}>{t("SampleManagement.serum-bottle.OriginnalResult.positive")}</MenuItem>
                                        <MenuItem value={0}>{t("SampleManagement.serum-bottle.OriginnalResult.indertermine")}</MenuItem>
                                        <MenuItem value={-1}>{t("SampleManagement.serum-bottle.OriginnalResult.negative")}</MenuItem>
                                        <MenuItem value={-2}>{t("SampleManagement.serum-bottle.OriginnalResult.none")}</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextValidator
                                    className="w-100"
                                    label={t("SampleManagement.serum-bottle.bottleQuality")}
                                    onChange={this.handleChange}
                                    type="text"
                                    name="bottleQuality"
                                    value={bottleQuality}
                                    validators={["required"]}
                                    errorMessages={["this field is required"]}
                                />
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextValidator
                                    className="w-100"
                                    label={t("SampleManagement.serum-bottle.bottleVolume")}
                                    onChange={this.handleChange}
                                    type="number"
                                    name="bottleVolume"
                                    value={bottleVolume}
                                    validators={["required"]}
                                    errorMessages={["this field is required"]}
                                />
                            </Grid>
                            <Grid item sm={6} xs={12}>
                                <TextValidator
                                    className="w-100"
                                    label={t("SampleManagement.serum-bottle.localSaveBottle")}
                                    onChange={this.handleChange}
                                    type="number"
                                    name="localSaveBottle"
                                    value={localSaveBottle}
                                    validators={["required"]}
                                    errorMessages={["this field is required"]}
                                />
                            </Grid>
                        </Grid>
                        <div className="flex flex-space-between flex-middle">
                            <Button variant="contained" color="primary" className="mb-16 mr-16 align-bottom" type="submit">
                                {t('Save')}
                            </Button>
                            <Button onClick={() => handleClose()}> {t('Cancel')}</Button>
                        </div>
                    </ValidatorForm>
                </DialogContent>
            </Dialog >
        );
    }
}

export default EQASerumBottleEditorDialog;
