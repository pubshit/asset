import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  FormControl,
  Input,
  InputAdornment,
  Link,
} from '@material-ui/core'
import React from 'react'
import MaterialTable, {
  MTableToolbar,
} from 'material-table'
import {
  deleteItem,
  searchByPage
} from './CouncilService'
import CouncilDialog from './CouncilDialog'
import { Breadcrumb, ConfirmationDialog } from 'egret'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { appConst } from 'app/appConst';
import AppContext from 'app/appContext';
import { ValidatorForm } from 'react-material-ui-form-validator';
import { defaultPaginationProps, LightTooltip } from 'app/appFunction';
import SearchIcon from "@material-ui/icons/Search";
function MaterialButton(props) {
  const { t, } = useTranslation()
  const item = props.item
  return (
    <div className="none_wrap">
      <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
          <Icon fontSize="small" color="primary">edit</Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  )
}
class Council extends React.Component {
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    keyword: '',
    shouldOpenNotificationPopup: false,
    Notification: '',
    listCommonObjectType: [],
    commonObjectType: null,
    isActive: {}
  }
  constructor(props) {
    super(props)
    this.handleTextChange = this.handleTextChange.bind(this)
  }

  handleTextChange(event) {
    this.setState({ keyword: event.target.value })
  }

  search() {
    this.setPage(0);
  }

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search()
    }
  }

  componentDidMount() {
    this.updatePageData()
  }

  updatePageData = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let searchObject = {}

    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;


    try {
      setPageLoading(true);
      let res = await searchByPage(searchObject)

      const { status, data } = res;
      if (appConst.CODE.SUCCESS === status) {
        this.setState({
          itemList: data?.data?.content?.length > 0 ? [...data?.data?.content] : [],
          totalElements: data?.data?.totalElements,
        })
      }
      else {
        toast.error(t("general.error"))
      }
    }
    catch (e) {
      toast.error(t("general.error"))
    }
    finally {
      setPageLoading(false);
    }

  }

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData()
    })
  }

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage)
  }

  // setUsersPage = (usersPage) => {
  //   this.setState({ usersPage }, function () {
  //     this.updatePageData()
  //   })
  // }

  // setUsersRowsPerPage = (event) => {
  //   this.setState({ 
  //     usersRowsPerPage: event.target.value, 
  //     usersPage: 0 
  //   }, function () {
  //     this.updatePageData();
  //   })
  // }

  // handleChangeUsersPage = (event, newPage) => {
  //   this.setUsersPage(newPage)
  // }

  handleOKEditClose = () => {
    this.setState(
      {
        shouldOpenImportExcelDialog: false,
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
      },
      () => {
        this.updatePageData()
      }
    )
  }

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    })
  }

  handleDialogClose = () => {
    this.setState(
      {
        shouldOpenImportExcelDialog: false,
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        shouldOpenConfirmationDeleteAllDialog: false,
        shouldOpenNotificationPopup: false,
        data: [],
      },
      () => {
        this.updatePageData()
      }
    )
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
    })
    this.setPage(0)
  }

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    })
  }

  handleConfirmationResponse = () => {
    let { t } = this.props
    if (this.state.itemList.length === 1 && this.state.page === 1) {
      let count = this.state.page - 1
      this.setState({
        page: count,
      })
    }
    deleteItem(this.state.id).then((res) => {
      if (appConst.CODE.SUCCESS === res?.data?.code) {
        toast.success(t("general.success"));
        this.updatePageData();
      }
      else {
        toast.warning(res?.data?.message);
      }
      this.handleDialogClose()
    }).catch((err) => {
      this.setState({
        shouldOpenNotificationPopup: true,
        Notification: "Supplier.noti.use"
      })
    })
  }
  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    })
  }

  searchByStatus = (e, value) => {
    this.setState((preState) => ({ ...preState, isActive: value }), () => {
      this.search();
    });
  }

  handleKeyUp = (e) => {
    if (!e?.target?.value) {
      this.updatePageData();
    }
  }

  handleGetRowData = (rowData) => {
    this.setState({
      userList: rowData.hdChiTiets ?? []
    })
  }

  render() {
    const { t, i18n } = this.props
    let {
      page,
      userList,
      itemList,
      rowsPerPage,
      totalElements,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDialog,
      shouldOpenConfirmationDeleteAllDialog,
      keyword
    } = this.state
    let TitlePage = t('Dashboard.subcategory.council')

    let columns = [
      {
        title: t('general.action'),
        field: 'custom',
        align: 'left',
        minWidth: '100px',
        cellStyle: {
          textAlign: 'center',
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={(rowData, method) => {
              if (method === appConst.active.edit) {
                this.handleEditItem(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData?.id);
              } else {
                alert('Call Selected Here:' + rowData?.id)
              }
            }}
          />
        ),
      },
      {
        title: t('general.index'),
        field: 'stt',
        align: 'left',
        minWidth: 50,
        cellStyle: {
          textAlign: 'center',
        },
        render: (rowData) => page * rowsPerPage + rowData.tableData.id + 1
      },
      {
        title: t('Council.code'),
        field: 'code',
        align: 'left',
        minWidth: 120,
        cellStyle: {
          textAlign: 'center',
        }
      },
      {
        title: t('Council.name'),
        field: 'name',
        minWidth: 200,
        align: "left",
      },
      {
        title: t('Council.type'),
        field: 'type',
        align: 'left',
        minWidth: 150,
        render: (rowData) => {
          return appConst.listHdType.find(status => status?.code === rowData?.type)?.name
        },
      },
      {
        title: t('Council.active'),
        field: 'active',
        align: 'left',
        minWidth: 150,
        cellStyle: {
          textAlign: 'center',
        },
        render: (rowData) => appConst.listStatusHoiDong.find(status => status?.code === rowData?.isActive)?.name

      },
      {
        title: t('Council.note'),
        field: 'note',
        align: 'left',
        minWidth: 200,
      },
    ]

    let columnsUsersTable = [
      {
        title: t('Council.userName'),
        field: 'personName',
        minWidth: 150,
        align: "left",
      },
      {
        title: t('Council.userDepartment'),
        field: 'departmentName',
        align: 'left',
        minWidth: 200,
      },
      {
        title: t('Council.userPosition'),
        field: 'position',
        align: 'left',
        minWidth: 150,
      },
      {
        title: t('Council.userRole'),
        field: 'roleName',
        align: 'left',
        minWidth: 150,
        cellStyle: {
          textAlign: 'center',
        },
      },
      {
        title: t('Council.note'),
        field: 'note',
        align: 'left',
        minWidth: 250,
      },
    ]

    return (
      <div className="m-sm-30">
        <ValidatorForm onSubmit={() => { }}>
          <Helmet>
            <title>{TitlePage} | {t('web_site')}</title>
          </Helmet>
          <div className="mb-sm-30">
            <Breadcrumb
              routeSegments={[
                { name: t('Dashboard.category'), path: '/list/council' },
                { name: TitlePage },
              ]}
            />
          </div>
          <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={4} xs={12} >
              <Button
                className="mt-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={() => this.handleEditItem(null)}
              >
                {t('general.add')}
              </Button>
            </Grid>

            <Grid item container md={8} sm={12} xs={12} spacing={2}>
              <Grid item md={6} sm={12} xs={12} ></Grid>
              <Grid item md={6} sm={12} xs={12} >
                <FormControl fullWidth>
                  <Input
                    className='search_box w-100 mt-16'
                    onChange={this.handleTextChange}
                    onKeyDown={this.handleKeyDownEnterSearch}
                    onKeyUp={this.handleKeyUp}
                    placeholder={t("Council.search")}
                    id="search_box"
                    startAdornment={
                      <InputAdornment position='end'>
                        <Link to="#"> <SearchIcon
                          onClick={() => this.search(keyword)}
                          style={{
                            position: "absolute",
                            top: "0",
                            right: "0"
                          }} /></Link>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <div>
                {shouldOpenEditorDialog && (
                  <CouncilDialog
                    t={t}
                    i18n={i18n}
                    handleClose={this.handleDialogClose}
                    open={shouldOpenEditorDialog}
                    handleOKEditClose={this.handleOKEditClose}
                    item={this.state.item}
                  />
                )}

                {shouldOpenConfirmationDialog && (
                  <ConfirmationDialog
                    title={t('general.confirm')}
                    open={shouldOpenConfirmationDialog}
                    onConfirmDialogClose={this.handleDialogClose}
                    onYesClick={this.handleConfirmationResponse}
                    text={t('general.deleteConfirm')}
                    agree={t('general.agree')}
                    cancel={t('general.cancel')}
                  />
                )}
              </div>

              <MaterialTable
                title={t('general.list')}
                data={itemList}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  },
                  toolbar: {
                    nRowsSelected: `${t('general.selects')}`
                  }
                }}

                onRowClick={(e, rowData) => this.handleGetRowData(rowData)}

                options={{
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: '450px',
                  minBodyHeight: this.state.itemList?.length > 0
                    ? 0 : 250,
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                  padding: 'dense',
                  toolbar: false
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.setState({ data: rows })
                }}
              />
              <TablePagination
                { ...defaultPaginationProps() }
                rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                count={totalElements}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.setRowsPerPage}
              />

            </Grid>

            <Grid item xs={12}>
              <MaterialTable
                title={t('general.list')}
                data={userList}
                columns={columnsUsersTable}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  },
                }}

                options={{
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  maxBodyHeight: '450px',
                  // minBodyHeight: userList?.length > 0 
                  //   ? 0 : 250,
                  minBodyHeight: "250px",
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                  padding: 'dense',
                  toolbar: false
                }}
                components={{
                  Toolbar: (props) => <MTableToolbar {...props} />,
                }}
                onSelectionChange={(rows) => {
                  this.setState({ data: rows })
                }}
              />
            </Grid>
          </Grid>
        </ValidatorForm>
      </div>
    )
  }
}
Council.contextType = AppContext;
export default Council
