import {
  Grid,
  DialogActions,
  Button,
  Dialog,
} from '@material-ui/core'
import React from 'react'
import { createItem, updateItem } from './CouncilService'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import AsynchronousAutocompleteSub from '../utilities/AsynchronousAutocompleteSub'
import {appConst, variable} from 'app/appConst'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import AppContext from 'app/appContext'
import MaterialTable, { MTableToolbar } from 'material-table'
import SelectMultiplePersonPopup from '../Component/Person/SelectMultiplePersonPopup'
import { PaperComponent } from 'app/appFunction';
import {Label} from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class CouncilDialog extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    type: null,
    hdChiTiets: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    keyword: '',
    shouldOpenNotificationPopup: false,
    active: "",
  }

  componentWillMount() {
    let { item } = this.props
    let newArray = item?.hdChiTiets?.map(person => {
      person.role = appConst.listHdChiTietsRole.find(
        role => role.code === person.role
      )
      return person;
    })
    let isActive = appConst.listStatusHoiDong.find(
      status => status.code === item?.isActive
    )
    let type = appConst.listHdType.find(
      type => type.code === item?.type
    )
    
    this.setState({ 
      ...item,
      hdChiTiets: newArray,
      isActive,
      type,
    });
  }

  handleChange(event) {
    this.setState({ [event?.target?.name]: event?.target?.value })
  }

  validations() {
    let { t } = this.props;
    let { hdChiTiets } = this.state;

    if(!hdChiTiets || hdChiTiets.length <= 0) {
      toast.warning(t("Council.messages.emptyHdChiTiets"));
      return false;
    }

    return true;
  }

  convertSubmitData(state) {
    let item = {
      name: state?.name,
      code: state?.code,
      isActive: state?.isActive?.code,
      type: state?.type?.code,
      note: state?.note,
      hdChiTiets: state?.hdChiTiets?.map(ps => ({
        personId: ps.personId,
        personName: ps.personName,
        departmentId: ps.departmentId,
        departmentName: ps.departmentName,
        role: ps?.role?.code,
        position: ps?.position,
        note: ps?.note,
      }))
    }

    if (state?.id) {
      item.id = state.id
    }

    return item;
  }

  handleFormSubmit = async () => {
    let { id } = this.state;
    let { setPageLoading } = this.context;
    let { 
      t,
      handleOKEditClose = () => {},
      handleSelect = (value) => {},
      handleClose,
    } = this.props;
    
    if(!this.validations()) return;

    setPageLoading(true);
    const dataState = this.convertSubmitData(this.state)
    
    try {
      let res = id
      ? await updateItem(dataState)
      : await createItem(dataState)
      const { code, data, message } = res?.data;
      if(appConst.CODE.SUCCESS === code) {
        if (!id) {
          handleSelect(data);
          toast.success(t("general.addSuccess"))
        } else {
          toast.success(t("general.updateSuccess"))
        }
        handleOKEditClose();
      } else {
        toast.warning(message);
      }
    }
    catch (e) {
      toast.error(t("general.error"))
      handleClose();
    }
    finally {
      setPageLoading(false);
    }
  }

  handleSetData = (value, name) => {
    this.setState({ [name]: value });
  }

  handleAddPersons = () => {
    this.setState({ 
      shouldOpenAddPersonsPopup: true,
    });
  }

  handleClosePopup = () => {
    this.setState({ 
      shouldOpenAddPersonsPopup: false,
    });
  }

  handleSelectPersons = (hdChiTiets) => {
    let { t } = this.props;
    let newArray = []
    if (hdChiTiets?.length > 0) {
      newArray = hdChiTiets.map(person => {
        return {
          ...person,
          personId: person?.user?.person?.id || person?.personId,
          personName: person?.user?.person?.displayName || person?.personName,
          departmentId: person?.department?.id || person?.departmentId,
          departmentName: (person?.department?.text  
            ? person?.department?.text
            : person?.department?.name) || person?.departmentName,
        }
      })
    }
    else {
      toast.warning(t("Council.noPersonSelected"))
    }

    this.setState({
      hdChiTiets: newArray,
    }, () => {
      this.handleClosePopup();
    });
  }

  handleRowDataChange = (value, name, index) => {
    let { hdChiTiets = [] } = this.state;
    let item = hdChiTiets[index];

    if (item) {
      hdChiTiets[index] = {
        ...item,
        [name]: value,
      }
    }

    this.setState({
      hdChiTiets,
    });
  }

  render() {
    const {
      t, i18n,
      handleClose,
      open,
    } = this.props
    let { 
      id,
      name, 
      code,
      type,
      note,
      isActive,
      hdChiTiets,
      shouldOpenAddPersonsPopup,
    } = this.state
    
    let columns = [
      { 
        title: t('Council.userName'),
        field: 'personName',
        minWidth: 150,
        align: "left",
      },
      { 
        title: t('Council.userDepartment'), 
        field: 'departmentName', 
        align: 'left', 
        minWidth: 200,
      },
      { 
        title: t('Council.userPosition'), 
        field: '', 
        align: 'left', 
        minWidth: 150,
        render: rowData => (
          <>
            <TextValidator
              fullWidth
              label={t('Council.userPosition')}
              onChange={(e) => this.handleRowDataChange(
                e?.target?.value,
                e?.target?.name,
                rowData.tableData.id
              )}
              type="text"
              name="position"
              value={rowData?.position || ""}
            />
          </>
        )
      },
      {
        title: t('Council.userRole'), 
        field: '', 
        align: 'left', 
        minWidth: 150,
        render: rowData => (
          <>
            <AsynchronousAutocompleteSub
              isFocus={true}
              label={
                <span>
                  {/* <span className="colorRed">*</span> */}
                  {t("Council.userRole")}
                </span>
              }
              searchFunction={() => {}}
              searchObject={{}}
              listData={appConst.listHdChiTietsRole}
              setListData={() => {}}
              displayLable="name"
              value={rowData?.role || null}
              onSelect={(value) => this.handleRowDataChange(
                value,
                "role",
                rowData.tableData.id,
              )}
              noOptionsText={t("general.noOption")}
              // validators={['required']}
              // errorMessages={[t('general.required')]}
            />
          </>
        )
      },
      { 
        title: t('Council.note'), 
        field: 'active', 
        align: 'left', 
        minWidth: 250,
        render: rowData => (
          <TextValidator
            fullWidth
            label={t('Council.note')}
            onChange={(e) => this.handleRowDataChange(
              e?.target?.value,
              e?.target?.name,
              rowData.tableData.id
            )}
            type="text"
            name="note"
            value={rowData?.note || ""}
          />
        )
      },
    ]

    return (
      <Dialog 
        open={open} 
        PaperComponent={PaperComponent} 
        maxWidth="md" 
        fullWidth
      >
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t('Council.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} id="formm">
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item container spacing={1}>
                <Grid item sm={4} xs={12}>
                  <TextValidator
                    fullWidth
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t('Council.name')}
                      </span>
                    }
                    onChange={this.handleChange}
                    type="text"
                    name="name"
                    value={name || ""}
                    validators={["required", "matchRegexp:^.{1,255}$", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                    errorMessages={[t("general.required"), "Tối đa 255 ký tự", t("general.invalidFormat")]}
                  />
                </Grid>
                <Grid item sm={4} xs={12}>
                  <TextValidator
                    fullWidth
                    label={
                      <span>
                        {id && <span className="colorRed">*</span>}
                        {t('Council.code')}
                      </span>
                    }
                    disabled
                    onChange={this.handleChange}
                    type="text"
                    name="code"
                    value={code || ""}
                  />
                </Grid>
                <Grid item sm={4} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t("Council.type")}
                      </span>
                    }
                    searchFunction={()=>{}} 
                    searchObject={{}} 
                    listData={appConst.listHdType}
                    nameListData="listType"
                    setListData={this.handleSetData}
                    name="type"
                    displayLable="name"
                    value={type || null}
                    onSelect={this.handleSetData}
                    noOptionsText={t("general.noOption")}
                    validators={["required", ]}
                    errorMessages={[t("general.required"), ]}
                  />
                </Grid>
                <Grid item sm={4} xs={12}>
                  <AsynchronousAutocompleteSub
                    label={
                      <Label isRequired>
                        {t("Council.active")}
                      </Label>
                    }
                    searchFunction={()=>{}} 
                    searchObject={{}} 
                    listData={appConst.listStatusHoiDong}
                    setListData={() => {}}
                    name="isActive"
                    displayLable="name"
                    value={isActive || null}
                    onSelect={this.handleSetData}
                    noOptionsText={t("general.noOption")}
                    validators={["required", ]}
                    errorMessages={[t("general.required"), ]}
                  />
                </Grid>
                <Grid item sm={8} xs={12}>
                  <TextValidator
                    fullWidth
                    label={t("Council.note")}
                    onChange={this.handleChange}
                    type="text"
                    name="note"
                    value={note || ""}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Button
                  className="mb-8 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={this.handleAddPersons}
                >
                  {t('general.add')}
                </Button>
                {shouldOpenAddPersonsPopup && (
                  <SelectMultiplePersonPopup
                    t={t} i18n={i18n}
                    open={shouldOpenAddPersonsPopup}
                    handleClose={this.handleClosePopup}
                    selectedList={hdChiTiets}
                    handleSelect={this.handleSelectPersons}
                  />
                )}

                <MaterialTable
                  title={t('general.list')}
                  data={hdChiTiets}
                  columns={columns}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                    },
                    toolbar: {
                      nRowsSelected: `${t('general.selects')}`
                    }
                  }}
                  options={{
                    selection: false,
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    sorting: false,
                    toolbar: false,
                    rowStyle: rowData => ({
                      backgroundColor: rowData.tableData.id % 2 === 1 ? '#EEE' : '#FFF',
                    }), 
                    maxBodyHeight: 280,
                    minBodyHeight: this.state.itemList?.length > 0 
                      ? 0 : 280,
                    headerStyle: {
                      backgroundColor: '#358600',
                      color:'#fff',
                    },
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={handleClose}
              >
                {t('general.cancel')}
              </Button>
              <Button 
                variant="contained" 
                color="primary" 
                type="submit" 
                className="mr-15"
              >
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    )
  }
}
CouncilDialog.contextType = AppContext;
export default CouncilDialog
