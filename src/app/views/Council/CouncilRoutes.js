import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const Council = EgretLoadable({
  loader: () => import("./Council")
});
const ViewComponent = withTranslation()(Council);

const CouncilRoutes = [
  {
    path: ConstantList.ROOT_PATH + "list/council",
    exact: true,
    component: ViewComponent
  }
];

export default CouncilRoutes;
