import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const ProductType = EgretLoadable({
  loader: () => import("./ProductType")
});
const ViewComponent = withTranslation()(ProductType);

const ProductTypeRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"list/producttype",
    exact: true,
    component: ViewComponent
  }
];

export default ProductTypeRoutes;
