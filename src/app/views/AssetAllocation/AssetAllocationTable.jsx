import {
  AppBar,
  Icon,
  IconButton,
  Tab,
  Tabs,
} from "@material-ui/core";
import { appConst, LIST_ORGANIZATION, variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  convertFromToDate,
  formatDateTypeArray,
  functionExportToExcel,
  getTheHighestRole,
  handleKeyDown,
  isSuccessfulResponse,
  isValidDate,
} from "app/appFunction";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import {
  deleteItem,
  exportToExcel as assetAllocationExportToExcel,
  getCountStatus,
  getItemById,
  searchByPage,
  getListQRCode,
  exportExampleImportExcelAssetAllocation,
} from "./AssetAllocationService";
import ComponentAllocationTable from "./ComponentAllocationTable";
import ListAssetTable from "./ListAssetTable";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import { TabPanel } from "../Component/Utilities";
import { withRouter } from "react-router-dom";
import { getSignature } from "../Component/Signature/services";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const {
    departmentUser,
  } = getTheHighestRole();
  const {
    hasEditPermission,
    hasPrintPermission,
    hasSuperAccess,
    item,
    isRoleAssetUser,
  } = props;
  const isEdit = hasEditPermission &&
    [
      appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
      appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
    ].includes(item?.allocationStatusIndex)
  const isWaitForConfirmation = appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder === item?.allocationStatusIndex;
  const isReceiverDepartment = (item.receiverDepartmentId === departmentUser?.id) || (item?.handoverDepartmentId === departmentUser?.id);
  const isConfirm = isWaitForConfirmation && (hasSuperAccess || isReceiverDepartment);
  const isDelete = (hasSuperAccess || item?.createdByDepartment === departmentUser?.id) &&
    (
      [
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
      ].includes(item?.allocationStatusIndex)
    );
    
  return (
    <div className="none_wrap">
      {isEdit && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isDelete
        && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {isConfirm && (
        <LightTooltip
          title={t("InstrumentToolsTransfer.hoverCheck")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.check)}
          >
            <ArrowForwardOutlinedIcon className="iconArrow" />
          </IconButton>
        </LightTooltip>
      )}
      {hasPrintPermission && (
        <LightTooltip
          title={t("In phiếu")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
      {!isRoleAssetUser &&
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class AssetAllocationTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetAllocation: [],
    item: {},
    shouldOpenPrint: false,
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenNotificationPopup: false,
    shouldOpenImportExcelDialog: false,
    listAssetDocumentId: [],
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    hasSuperAccess: false,
    tabValue: 0,
    isCheckReceiverDP: true,
    isCheckHandoverDP: true,
    itemAsset: {},
    openAdvanceSearch: false,
    listStatus: [],
    listHandoverDepartment: [],
    listReceiverDepartment: [],
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    fromToData: null,
    voucherId: "",
    openPrintQR: false,
    products: [],
    statusIndex: null,
    isDisable: null
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Allocation; //cấp phát

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabAllocation?.tabAll === newValue) {
      this.setState({
        statusIndex: null,
        isDisable: null,
        allocationStatus: null,
      }, () => this.updatePageData());
    }
    if (appConst?.tabAllocation?.tabNew === newValue) {
      this.setState({
        statusIndex: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        isDisable: true,
        allocationStatus: appConst.STATUS_ALLOCATION.MOI_TAO,
      }, () => this.updatePageData());
    }
    if (appConst?.tabAllocation?.tabWaitConfirmation === newValue) {
      this.setState({
        statusIndex: appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
        isDisable: true,
        allocationStatus: appConst.STATUS_ALLOCATION.CHO_XAC_NHAN,
      }, () => this.updatePageData());
    }
    if (appConst?.tabAllocation?.tabAllocated === newValue) {
      this.setState({
        statusIndex: appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        isDisable: true,
        allocationStatus: appConst.STATUS_ALLOCATION.DA_CAP_PHAT,
      }, () => this.updatePageData());
    }
    if (appConst?.tabAllocation?.tabReturn === newValue) {
      this.setState({
        statusIndex: appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
        isDisable: true,
        allocationStatus: appConst.STATUS_ALLOCATION.TRA_VE,
      }, () => this.updatePageData());
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () {
    });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  setPage = (page) => {
    this.setState({
      page,
      itemAsset: {},
    }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let {
      toDate,
      fromDate,
      statusIndex,
      originalCostTo,
      allocationStatus,
      originalCostFrom,
      handoverDepartment,
      receiverDepartment,
      rowsPerPage,
      page,
    } = this.state;
    let searchObject = {};

    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchObject.statusIndex = statusIndex;
    if (handoverDepartment) {
      searchObject.handoverDepartmentId = handoverDepartment?.id;
    }
    if (receiverDepartment) {
      searchObject.receiverDepartmentId = receiverDepartment?.id;
    }
    if (allocationStatus && !statusIndex) {
      searchObject.statusIndex = allocationStatus?.indexOrder;
    }
    if (fromDate) {
      searchObject.issueDateBottom = convertFromToDate(fromDate).fromDate;
    }
    if (toDate) {
      searchObject.issueDateTop = convertFromToDate(toDate).toDate;
    }
    if (originalCostFrom) {
      searchObject.originalCostFrom = Number(originalCostFrom);
    }
    if (originalCostTo) {
      searchObject.originalCostTo = Number(originalCostTo);
    }

    setPageLoading(true);
    await searchByPage(searchObject)
      .then(({ data }) => {
        if (data?.data?.totalElements && data?.data?.content <= 0) {
          this.setPage(0);
        } else {
          this.setState(
            {
              itemList: data?.data?.content?.length ? [...data?.data?.content] : [],
              totalElements: data?.data?.totalElements,
              itemAsset: {},
            }
          );
        }
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      })
      .finally(() => {
        setPageLoading(false);
      });
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countStatusNew,
          countStatusAllocated,
          countStatusReturn,
          countStatusWaitConfirmation;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.listStatusAllocation[0].indexOrder === item?.status) {
            countStatusNew = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusAllocation[1].indexOrder === item?.status) {
            countStatusAllocated = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusAllocation[2].indexOrder === item?.status) {
            countStatusReturn = this.checkCount(item?.count);
            return;
          }
          if (appConst?.listStatusAllocation[3].indexOrder === item?.status) {
            countStatusWaitConfirmation = this.checkCount(item?.count);
            return;
          }
        });

        this.setState({
          countStatusNew,
          countStatusAllocated,
          countStatusReturn,
          countStatusWaitConfirmation,
        }, () => setPageLoading(false));
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenPrint: false,
      isVisibility: false,
      item: null,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      isVisibility: false,
      item: null,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleDeleteAssetAllocation = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetAllocation = (item) => {
    let { t } = this.props;
    getItemById(item.id)
      .then((result) => {
        this.setState({
          item: result.data,
          shouldOpenEditorDialog: true,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    deleteItem(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(data?.message);
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          } else {
            this.updatePageData();
          }
          this.handleDialogClose();
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleView({
            id: location?.state?.id
          })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleApprove({
            id: location?.state?.id
          })
          break;
        default:
          break;
      }
      history.push(location?.state?.path, null);
    }

    this.setState({
      statusIndex: null,
    }, () => {
      this.getRoleCurrentUser();
      this.updatePageData()
      this.getCountStatus();
    });
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasCreatePermission,
      hasSuperAccess,
      receiverDepartment,
    } = this.state;
    let roles = getTheHighestRole();
    let {
      isRoleAssetUser,
      isRoleAssetManager,
      isRoleAccountant,
      isRoleAdmin,
      isRoleOrgAdmin,
      departmentUser,
    } = roles;

    if (isRoleAssetUser) {
      receiverDepartment = departmentUser;
    }
    hasDeletePermission = isRoleOrgAdmin || isRoleAdmin || isRoleAssetManager || isRoleAssetUser;
    hasEditPermission = isRoleOrgAdmin || isRoleAdmin || isRoleAssetManager || isRoleAssetUser;
    hasPrintPermission = isRoleOrgAdmin || isRoleAdmin || isRoleAssetManager || isRoleAccountant || isRoleAssetUser;
    hasCreatePermission = isRoleOrgAdmin || isRoleAdmin || isRoleAssetManager;
    hasSuperAccess = isRoleOrgAdmin || isRoleAdmin;

    this.setState({
      ...roles,
      hasSuperAccess,
      hasEditPermission,
      receiverDepartment,
      hasPrintPermission,
      hasDeletePermission,
      hasCreatePermission,
      isRoleAssetUser,
      isRoleAdmin
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { AssetAllocation } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetAllocation.length; i++) {
      if (
        AssetAllocation[i].checked === null ||
        AssetAllocation[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (AssetAllocation[i].id === item.id) {
        AssetAllocation[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetAllocation: AssetAllocation,
    });
  };

  handleSelectAllClick = (event) => {
    let { AssetAllocation } = this.state;
    for (var i = 0; i < AssetAllocation.length; i++) {
      AssetAllocation[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetAllocation: AssetAllocation,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handlePrintAll = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    this.setState({
      item: assetAllocation,
      shouldOpenPrint: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props;
    this.handleDeleteList(this.data)
      .then(() => {
        this.updatePageData();
        this.handleDialogClose();
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate,
      currentUser,
      allocationStatus,
      originalCostTo,
      originalCostFrom,
      handoverDepartment,
      receiverDepartment,
      voucherId,
      keyword,
      statusIndex
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.TSCD,
        voucherId: voucherId,
        statusIndex: statusIndex
      };
      searchObject.keyword = keyword?.trim();
      if (handoverDepartment) {
        searchObject.handoverDepartmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiverDepartmentId = receiverDepartment?.id;
      }
      if (allocationStatus) {
        searchObject.statusIndex = allocationStatus?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }
      if (originalCostFrom) {
        searchObject.originalCostFrom = Number(originalCostFrom);
      }
      if (originalCostTo) {
        searchObject.originalCostTo = Number(originalCostTo);
      }

      const res = await assetAllocationExportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "AssetAllocation.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  handleNotificationPopup = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  exportExampleImportExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcelAssetAllocation,
        {},
        t("exportToExcel.allocatedAssetImportExample")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };


  setStateAllocation = (item) => {
    this.setState(item);
  };

  convertDataDto = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { data } = await getItemById(rowData?.id);
    const dataSign = await getSignature(rowData?.id);
    setPageLoading(false);
    let listAssetDocumentId = [];
    let allocation = data?.data ? data?.data : {};

    if (isSuccessfulResponse(dataSign?.data?.code)) {
      allocation = { ...dataSign?.data?.data, ...allocation }
    }

    allocation.createDate = formatDateTypeArray(allocation?.createDate);
    allocation.allocationStatus = {
      id: allocation?.allocationStatusId,
      indexOrder: allocation?.allocationStatusIndex,
    };

    allocation.handoverDepartment = {
      id: allocation?.handoverDepartmentId,
      code: allocation?.handoverDepartmentCode,
      name: allocation?.handoverDepartmentName,
    };

    allocation.receiverDepartment = {
      id: allocation?.receiverDepartmentId,
      code: allocation?.receiverDepartmentCode,
      name: allocation?.receiverDepartmentName,
      text:
        allocation?.receiverDepartmentName +
        " - " +
        allocation?.receiverDepartmentCode,
    };

    allocation.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse || item?.assetYearPutIntoUser,
        managementCode: item?.assetManagementCode,
        carryingAmount: item?.assetCarryingAmount,
        dateOfReception: item?.assetDateOfReception,
        madeIn: item?.assetMadeIn,
        originalCost: item?.assetOriginalCost,
        yearOfManufacture: item?.assetYearOfManufacture,
      };

      item.usePerson = {
        id: item?.usePersonId,
        displayName: item?.usePersonDisplayName,
      };

      item.managementDepartment = {
        id: item?.assetManagementDepartmentId,
        code: item?.assetManagementDepartmentCode,
        name: item?.assetManagementDepartmentName,
      };

      item.receiveDepartment = {
        code: item?.receiveDepartmentCode,
        id: item?.receiveDepartmentId,
        name: item?.receiveDepartmentName,
      };

      return (item.asset = data);
    });

    allocation?.documents?.map((item) => {
      return listAssetDocumentId?.push(item?.id);
    });
    allocation.listAssetDocumentId = listAssetDocumentId;
    return allocation;
  };

  handleApprove = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    assetAllocation.isCheckHandoverDP = false;
    assetAllocation.isCheckReceiverDP = false;
    assetAllocation.isConfirm = true;
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };

  handleEdit = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    assetAllocation.isCheckHandoverDP = false;
    assetAllocation.isCheckReceiverDP = false;

    let status = assetAllocation?.allocationStatusIndex
      === appConst?.STATUS_ALLOCATION.MOI_TAO.indexOrder;
    if (status) {
      this.setState({
        item: assetAllocation,
        shouldOpenEditorDialog: true,
        isVisibility: false,
        isStatus: false,
        isHideButton: false,
      });
      return;
    }
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: true,
      products: assetAllocation?.allocationStatusIndex ===
        appConst?.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder ? assetAllocation?.assetVouchers : [],
    });
  };

  handleView = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusAllocation.find(
      (item) => item.indexOrder === status
    );
    switch (status) {
      case appConst.listStatusAllocation[0].indexOrder:
        return (
          <span className="status status-success">
            {appConst.listStatusAllocation[0].name}
          </span>
        );
      case appConst.listStatusAllocation[1].indexOrder:
        return (
          <span className="status status-info">
            {appConst.listStatusAllocation[1].name}
          </span>
        );
      case appConst.listStatusAllocation[2].indexOrder:
        return (
          <span className="status status-error">
            {appConst.listStatusAllocation[2].name}
          </span>
        );
      case appConst.listStatusAllocation[3].indexOrder:
        return (
          <span className="status status-warning">
            {appConst.listStatusAllocation[3].name}
          </span>
        );
      default:
        break;
    }
    return itemStatus?.name;
  };

  handleSetItemState = async (rowData) => {
    this.setState({
      itemAsset: rowData,
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name, source) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.originalCostFrom !== name &&
        variable.listInputName.originalCostTo !== name &&
        variable.listInputName.listData !== source
        // && !this.state.statusIndex
      ) {
        this.search();
      }
    });
  };

  handleQrCode = async (newValue) => {
    let assetAllocation = await this.convertDataDto(newValue);

    let productsUpdate = assetAllocation?.assetVouchers?.map(item => {
      item.qrInfo = `Mã TS: ${item?.asset?.code}\n` +
        `Mã QL: ${item?.asset?.managementCode}\n` +
        `Tên TS: ${item?.asset?.name}\n` +
        `Nước SX: ${item?.asset?.madeIn}\n` +
        `Năm SX: ${item?.asset?.yearOfManufacture}\n` +
        `Năm SD: ${item?.asset?.yearPutIntoUse}\n` +
        `PBSD: ${item?.receiveDepartmentName}\n`
      return item;
    })

    this.setState({
      products: productsUpdate,
      openPrintQR: true,
    })
  }

  handlePrintQRCode = async (id) => {
    let { t } = this.props
    try {
      let res = await getListQRCode(id);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          products: res?.data?.data?.length > 0 ? [...res?.data?.data] : [],
          openPrintQR: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasSuperAccess,
      isRoleAssetUser,
      tabValue,
    } = this.state;
    let TitlePage = t("Asset.allocation_asset");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasPrintPermission={hasPrintPermission}
            isRoleAssetUser={isRoleAssetUser}
            hasSuperAccess={hasSuperAccess}
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrintAll(rowData);
              } else if (appConst.active.view === method) {
                this.handleView(rowData);
              } else if (appConst.active.check === method) {
                this.handleApprove(rowData);
              } else if (appConst.active.qrcode === method) {
                this.handlePrintQRCode(rowData.id)
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("AssetTransfer.issueDate"),
        field: "issueDate",
        align: "left",
        minWidth: 110,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "allocationStatusIndex",
        minWidth: 150,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.allocationStatusIndex),
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPersonName",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        minWidth: "250px",
        align: "left",
      },
    ];

    let columnsNoAction = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <LightTooltip
            title={t("general.viewIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton size="small" onClick={() => this.handleView(rowData)}>
              <Icon fontSize="small" color="primary">
                visibility
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("AssetTransfer.issueDate"),
        field: "issueDate",
        align: "left",
        minWidth: 110,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "allocationStatusIndex",
        minWidth: 150,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.allocationStatusIndex),
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPersonName",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartmentName",
        minWidth: "250px",
        align: "left",
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        minWidth: "250px",
        align: "left",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/allocation-vouchers",
              },
              { name: t("Asset.allocation_asset") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("allocation_asset.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabNew")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusNew || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabWaitConfirmation")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusWaitConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabAllocated")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusAllocated || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabReturn")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countStatusReturn || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabAll}
          className="mp-0"
        >
          <ComponentAllocationTable
            t={t}
            i18n={i18n}
            item={this.state}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            columns={columns}
            columnsNoAction={columnsNoAction}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabNew}
          className="mp-0"
        >
          <ComponentAllocationTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleCloseQrCode={this.handleCloseQrCode}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabWaitConfirmation}
          className="mp-0"
        >
          <ComponentAllocationTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabAllocated}
          className="mp-0"
        >
          <ComponentAllocationTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabReturn}
          className="mp-0"
        >
          <ComponentAllocationTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleCloseQrCode={this.handleCloseQrCode}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
      </div>
    );
  }
}

AssetAllocationTable.contextType = AppContext;
export default withRouter(AssetAllocationTable);
