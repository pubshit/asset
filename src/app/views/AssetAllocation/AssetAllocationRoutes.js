import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetAllocationTable = EgretLoadable({
  loader: () => import("./AssetAllocationTable")
});
const ViewComponent = withTranslation()(AssetAllocationTable);

const AssetAllocationRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "fixed-assets/allocation-vouchers",
    exact: true,
    component: ViewComponent
  }
];

export default AssetAllocationRoutes;
