import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_PERSON =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_ALLOCATION =
  ConstantList.API_ENPOINT + "/api/v1/fixed-assets/allocation-vouchers";
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
const API_PATH_TEMPLATE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

//api mới
export const searchByPage = (params) => {
  let url = API_PATH_ALLOCATION + `/page`;
  let config = {
    params,
  };
  return axios.get(url, config);
};

export const createAllocation = (data) => {
  return axios.post(API_PATH_ALLOCATION, data);
};
export const updateAllocation = (id, data) => {
  let url = API_PATH_ALLOCATION + "/" + id;
  return axios.put(url, data);
};
export const getItemById = (id) => {
  let url = API_PATH_ALLOCATION + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let url = API_PATH_ALLOCATION + "/" + id;
  return axios.delete(url);
};
export const updateToReturn = (id, data, status) => {
  let url = API_PATH_ALLOCATION + "/" + id + "/status/" + status;
  return axios.put(url, data);
};
export const getCountStatus = () => {
  let url = API_PATH_ALLOCATION + "/count-by-statuses";
  return axios.get(url);
};

export const addNewOrUpdate = (asset) => {
  if (asset.id) {
    return axios.put(API_PATH_voucher + "/" + asset.id, asset);
  } else {
    return axios.post(API_PATH_voucher, asset);
  }
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/cap-phat-tai-san-co-dinh",
    data: searchObject,
    responseType: "blob",
  });
};
export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};
export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartment/" + userId);
};
//get phòng ban bàn giao
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};
//get phòng ban tiếp nhận
export const searchReceiverDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};
//get người tiếp nhận
export const searchReceiverPerson = (searchObject) => {
  var url = API_PATH_PERSON + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

//api mới
const API_PATH_VOUCHERSALLOCATION =
  ConstantList.API_ENPOINT + "/api/instruments-and-tool/allocation-vouchers";

export const addAllocation = (allocation) => {
  return axios.post(API_PATH_VOUCHERSALLOCATION, allocation);
};

export const getListQRCode = (id) => {
  let url = API_PATH_NEW + `/voucher/print-asset-qr-code/${id}`;
  return axios.get(url);
};

export const getAccessoriesByVoucherId = (id) => {
  let url = API_PATH_NEW + `/voucher/${id}/fixed-asset/accessories`;
  return axios.get(url);
};

export const exportExampleImportExcelAssetAllocation = () => {
  return axios({
    method: "post",
    url: API_PATH_TEMPLATE + "/import-allocated-fixed-asset",
    responseType: "blob",
  });
};

export const importExcelAssetAllocationURL = () => {
  return (
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
    "/api/fixed-assets/allocation-vouchers/excel/import"
  );
};

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + linkFile,
    responseType: "blob",
  });
};
