import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { TextValidator } from "react-material-ui-form-validator";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { Icon, IconButton, TextField } from "@material-ui/core";
import { appConst } from "app/appConst";
import moment from "moment";
import { personSearchByPage } from "../AssetTransfer/AssetTransferService";
import { useEffect } from "react";
import { useState } from "react";
import {convertNumberPrice, formatDateDto, formatTimestampToDate} from "app/appFunction";

function MaterialButton(props) {
  const item = props?.item;
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => props?.onSelect(item, appConst.active.delete)}
      >
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

function ListAssetTable(props) {
  const t = props?.t;
  let columns = [
    {
      title: t("general.stt"),
      field: "code",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1
    },
    {
      title: t("Asset.code"),
      field: "assetCode",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "assetManagementCode",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.dateOfReception"),
      // field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.assetDateOfReception ? formatTimestampToDate(rowData?.assetDateOfReception) : "",
    },
    {
      title: t("Asset.name"),
      field: "assetName",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Asset.serialNumber"),
      field: "assetSerialNumber",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.model"),
      field: "assetModel",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "assetYearPutIntoUse",
      align: "left",
      minWidth: 110,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "assetYearOfManufacture",
      align: "left",
      minWidth: 105,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "manufacturerName",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("Asset.originalCost"),
      field: "assetOriginalCost",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.assetOriginalCost),
    },
    {
      title: t("Asset.carryingAmount"),
      field: "assetCarryingAmount",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.assetCarryingAmount),
    },
    {
      title: t("allocation_asset.receiverDepartment"),
      field: "receiveDepartmentName",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("Asset.usePerson"),
      minWidth: 150,
      render: (rowData) => 
          <TextField
            className={props?.className ? props?.className : "w-100"}
            value={
              rowData?.usePersonDisplayName
                ? rowData.usePersonDisplayName
                : null
            }
            InputProps={{
              readOnly: true,
              disableUnderline: true,
            }}
          />
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
      render: (rowData) =>
          <TextField
            multiline
            className="w-100"
            type="text"
            name="note"
            value={rowData.note}
            InputProps={{
              readOnly: true,
              disableUnderline: true,
            }}
          />
    },
  ];
  return (
    <MaterialTable
      data={props?.itemAsset?.assetVouchers ? props?.itemAsset?.assetVouchers : []}
      columns={columns}
      options={{
        draggable: false,
        toolbar: false,
        selection: false,
        actionsColumnIndex: -1,
        paging: false,
        search: false,
        sorting: false,
        padding: "dense",
        rowStyle: (rowData) => ({
          backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
        }),
        headerStyle: {
          backgroundColor: "#358600",
          color: "#fff",
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        maxBodyHeight: "225px",
        minBodyHeight: "225px",
      }}
      localization={{
        body: {
          emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
        },
      }}
      components={{
        Toolbar: (props) => (
          <div style={{ witdth: "100%" }}>
            <MTableToolbar {...props} />
          </div>
        ),
      }}
      onSelectionChange={(rows) => {
        this.data = rows;
      }}
    />
  );
}

export default ListAssetTable;
