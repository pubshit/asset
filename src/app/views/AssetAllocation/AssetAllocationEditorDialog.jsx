import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { Button, Dialog, DialogActions, } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { createAllocation, updateAllocation, updateToReturn, } from "./AssetAllocationService";

import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import AllocationScrollableTabsButtonForce from "./AllocationScrollableTabsButtonForce";
import { getAssetDocumentById, getNewCodeDocument, } from "../Asset/AssetService";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage as searchByPageStatus } from '../AllocationStatus/AllocationStatusService'
import { appConst, STATUS_ASSET_FOR_ALLOCATION, variable } from "app/appConst";
import {
  checkInvalidDate,
  checkMinDate,
  checkObject,
  formatDateDto, getTheHighestRole, getUserInformation,
  isCheckLenght,
  PaperComponent
} from "app/appFunction";
import AppContext from "app/appContext";
import localStorageService from "app/services/localStorageService";
import { ConfirmationDialog } from 'egret';
import { createSignature } from "../Component/Signature/services";
import { getItemById } from "../Department/DepartmentService";
import { isSuccessfulResponse } from "../../appFunction";

toast.configure();

class AssetAllocationEditorDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Allocation,
    rowsPerPage: 10,
    page: 0,
    assetVouchers: [],
    statusIndexOrders: STATUS_ASSET_FOR_ALLOCATION,
    handoverPerson: null,
    managementDepartment: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenManagementDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    totalElements: 0,
    asset: {},
    shouldOpenPersonPopup: false,
    shouldOpenNotificationPopup: false,
    textNotificationPopup: "",
    isAssetAllocation: true,
    shouldOpenPopupAssetFile: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_ALLOCATION,
    assetDocumentList: [],
    listAssetDocumentId: [],
    count: 1,
    allocationStatus: null,
    loading: false,
    keyword: '',
    checkPermissionUserDepartment: true,
    check: false,
    assetId: '',
    usePerson: null,
    status: 2,
    managementDepartmentId: '',
    documents: [],
    isConfirm: false,
    shouldOpenRefuseTransfer: false,
    openPrintQR: false,
    isViewAssetFile: false,
    signature: {},
    isViewSignPermission: {
      [variable.listInputName.headOfDepartment]: false,
      [variable.listInputName.deputyDepartment]: false,
      [variable.listInputName.director]: false,
      [variable.listInputName.deputyDirector]: false,
      [variable.listInputName.chiefAccountant]: false,
      [variable.listInputName.storeKeeper]: false,
      [variable.listInputName.createdPerson]: true,
      [variable.listInputName.handoverPerson]: true,
      [variable.listInputName.receiverPerson]: true,
    }
  };

  convertDto = (state) => {
    return {
      allocationStatusId: state?.allocationStatusId,
      assetVouchers: state?.assetVouchers,
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverDepartment: state?.handoverDepartment,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      handoverPersonId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      receiverDepartmentId: state?.receiverDepartmentId,
      receiverPersonName: state?.receiverPerson?.personDisplayName,
      receiverPersonId: state?.receiverPerson?.personId,
    };
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        receiverPersonId: item?.personId,
        receiverPersonName: item?.personDisplayName,
      }
    });
  };
  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        handoverPersonId: item?.personId,
        handoverPersonName: item?.personDisplayName,
      }
    });
  };
  openCircularProgress = () => {
    this.setState({ loading: true });
  };
  validateSubmit = () => {
    let {
      assetVouchers,
      handoverPerson,
      handoverDepartment,
      receiverDepartment,
      allocationStatus,
      issueDate } = this.state;
    let { isVisibility, isStatus, t } = this.props;
    let checkStatus = appConst.listStatusAllocation[1].indexOrder === allocationStatus.indexOrder

    if (checkStatus && isVisibility && isStatus) {
      toast.warning(t('allocation_asset.missingVoucherStatus'));
      return false;
    }
    if (!issueDate) {
      toast.warning(t('allocation_asset.missingIssueDate'));
      return false;
    }
    if (checkInvalidDate(issueDate)) {
      toast.warning(t('allocation_asset.IssueDateNotExist'));
      return false;
    }
    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(t('allocation_asset.errMissingMindate'));
      return false;
    }
    if (!handoverDepartment) {
      toast.warning(t('allocation_asset.missingHandoverDepartment'));
      return false;
    }
    if (!handoverPerson) {
      toast.warning(t('allocation_asset.missingHandoverPerson'));
      return false;
    }
    if (!assetVouchers || assetVouchers?.length === 0) {
      toast.warning(t('allocation_asset.missingCCDC'));
      return false;
    }
    if (this.validateAssetVouchers()) return;
    if (!receiverDepartment) {
      toast.warning(t('allocation_asset.missingReceiverDepartment'));
      return false;
    }
    return true;
  }
  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    let { t } = this.props;

    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning(t('allocation_asset.errMessageNote'));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  checkDataApi = async (id, dataState, t) => {
    if (id) {
      if (!this.props.isStatus) {
        return await updateAllocation(id, dataState, t);
      }
      return await updateToReturn(id, dataState, this.state?.status)
    } else {
      return await createAllocation(dataState, t);
    }
  }

  handleStatus = async (status) => {
    let dataState = this.convertDto(this.state)
    let { id } = this.state
    const res = await updateToReturn(id, dataState, status)
    if (res?.data?.code === appConst.CODE.SUCCESS) {
      toast.success(res?.data?.message);
      this.props.handleOKEditClose();
      this.props.handleClose()
    } else {
      toast.warning(res?.data?.message);
    }
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let {
      t,
      isAllocation,
      isUpdateAsset,
      handleCloseAllocationEditorDialog = () => { },
      handleOKEditClose = () => { },
      handleClose = () => { },
    } = this.props;
    if (e.target.id !== "parentAssetAllocation") return;
    if (!this.validateSubmit()) return;
    let dataState = this.convertDto(this.state);

    setPageLoading(true);
    try {
      const res = await this.checkDataApi(id, dataState, t);
      if (appConst.CODE.SUCCESS === res.data?.code) {
        // await this.handleSaveSign(res?.data?.data?.id);
        handleOKEditClose();
        handleClose();
        toast.success(res.data?.message);
      } else {
        toast.warning(res.data?.message);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
    if (!id) return; // TODO: Thêm mới sẽ giữ dialog để thao tác tiếp
    if (isAllocation && isUpdateAsset) {
      handleCloseAllocationEditorDialog()
    }
  };

  convertDataSign = (data = {}) => {
    return {
      directorId: data?.directorId,
      directorName: data?.directorName,
      deputyDirectorId: data?.deputyDirectorId,
      deputyDirectorName: data?.deputyDirectorName,
      chiefAccountantId: data?.chiefAccountantId,
      chiefAccountantName: data?.chiefAccountantName,
      headOfDepartmentId: data?.headOfDepartmentId,
      headOfDepartmentName: data?.headOfDepartmentName,
      deputyDepartmentId: data?.deputyDepartmentId,
      deputyDepartmentName: data?.deputyDepartmentName,
      storekeeperId: data?.storekeeperId,
      storekeeperName: data?.storekeeperName,
      handoverPersonId: data?.handoverPersonId,
      handoverPersonName: data?.handoverPersonName,
      receiverPersonId: data?.receiverPersonId,
      receiverPersonName: data?.receiverPersonName,
      voucherId: data?.voucherId,
    }
  }
  handleSaveSign = async (voucherId) => {
    try {
      const payload = this.convertDataSign({
        ...this.state.signature,
        voucherId: voucherId
      });
      const data = await createSignature(payload, voucherId);
    } catch (error) {

    }
  }
  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    })
  }

  getDetailDepartment = async (departmentId) => {
    if (!departmentId) return {};
    try {
      const data = await getItemById(departmentId);
      if (isSuccessfulResponse(data?.status)) {
        return {
          headOfDepartmentId: data?.data?.headOfDepartmentId,
          headOfDepartmentName: data?.data?.headOfDepartmentName,
          deputyDepartmentId: data?.data?.deputyDepartmentId,
          deputyDepartmentName: data?.data?.deputyDepartmentName,
        }
      } else {
        return {};
      }
    } catch (error) {
      return {};
    }
  }
  onMountSetSign = async (item) => {
    const { currentUser, organization } = getUserInformation();
    const itemDepartment = await this.getDetailDepartment(item?.receiverDepartmentId)
    this.setState({
      signature: {
        directorId: item?.directorId || organization?.directorId,
        directorName: item?.directorName || organization?.directorName,
        deputyDirectorId: item?.deputyDirectorId || organization?.deputyDirectorId,
        deputyDirectorName: item?.deputyDirectorName || organization?.deputyDirectorName,
        chiefAccountantId: item?.chiefAccountantId || organization?.chiefAccountantId,
        chiefAccountantName: item?.chiefAccountantName || organization?.chiefAccountantName,
        headOfDepartmentId: item?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: item?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: item?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: item?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
        storekeeperId: item?.storekeeperId,
        storekeeperName: item?.storekeeperName,
        handoverPersonId: item?.handoverPersonId,
        handoverPersonName: item?.handoverPersonName,
        receiverPersonId: item?.receiverPersonId,
        receiverPersonName: item?.receiverPersonName,
        createdPersonId: item?.createdPersonId || currentUser?.person?.id,
        createdPersonName: item?.createdPersonName || currentUser?.person?.displayName,
      }
    })
  }

  componentDidMount() {
    let { item, isVisibility } = this.props;
    let roles = getTheHighestRole();
    let { departmentUser } = getUserInformation();
    this.setState({
      ...item,
      ...roles,
      isVisibility,
      receiverPerson: {
        personId: item?.receiverPersonId,
        personDisplayName: item?.receiverPersonName,
      },
      handoverPerson: item?.handoverPersonId ? {
        personId: item?.handoverPersonId,
        personDisplayName: item?.handoverPersonName,
      } : null,
      handoverDepartment: this.props?.item?.handoverDepartment || (departmentUser?.id ? departmentUser : null),
      handoverDepartmentId: this.props?.item?.handoverDepartment?.id || departmentUser?.id || null
    });

    // this.onMountSetSign(item);

    if (this.props.isAllocation) {
      let assetVouchers = []
      // eslint-disable-next-line no-unused-expressions
      this.props?.assetAllocation?.forEach(item => {
        let asset = {}
        asset.asset = item
        assetVouchers.push(asset)
      })
      this.handleSelectAssetAll(assetVouchers)
      this.setState({
        isCheckReceiverDP: true,
        issueDate: new Date(this.props?.assetAllocation[0]?.dateOfReception),
        dateOfReception: new Date(this.props?.assetAllocation[0]?.dateOfReception),
        handoverDepartment: this.props?.assetAllocation[0]?.managementDepartment,
        handoverDepartmentId: this.props?.assetAllocation[0]?.managementDepartment?.id,
        managementDepartmentId: this.props?.assetAllocation[0]?.managementDepartment?.id
      })
      this.status(appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder);
      return;
    }
    if (!this.props?.isstatus && !this.props?.isVisibility) {
      this.status(appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder)
    }
  }

  status = (indexOrder) => {
    let { t } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 100 };
    searchByPageStatus(searchObjectStatus).then(({ data }) => {
      let dataStatus = data?.content;
      let itemStatus = dataStatus.find((status) => indexOrder === status?.indexOrder)
      this.selectAlocationStatus(itemStatus)
    }).catch((error) => {
      toast.error(t("general.error"));
    });
  }

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleAssetPopupOpen = () => {
    let { t } = this.props;
    let { statusIndexOrders, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState({
        shouldOpenAssetPopup: true,
        item: {},
        statusIndexOrders: statusIndexOrders,
      });
    } else {
      toast.info(t('allocation_asset.missingHandoverDepartment'));
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  handleSelectAsset = (item) => {
    let { assetVouchers } = this.state;
    if (item != null && item.id != null) {
      if (assetVouchers == null) {
        assetVouchers = [];
        assetVouchers.push({ asset: item });
      } else {
        let hasInList = false;
        assetVouchers.forEach((element) => {
          if (element.asset.id == item.id) {
            hasInList = true;
          }
        });
        if (!hasInList) {
          assetVouchers.push({ asset: item });
        }
      }
    }
    this.setState(
      { assetVouchers, totalElements: assetVouchers.length },
      function () {
        this.handleAssetPopupClose();
      }
    );
  };

  handleSelectAssetAll = (items) => {
    let { assetVouchers } = this.state;
    let { t } = this.props;

    items.forEach((element) => {
      element.asset.useDepartment = this.state?.receiverDepartment;
      element.assetId = element?.asset?.id
    });
    if (items.length > 0) {
      this.setState({ assetVouchers: items }, function () {
        this.handleAssetPopupClose();
      });
    } else {
      this.setState({ assetVouchers: items }, function () {
        // this.handleAssetPopupClose();
      });
      toast.warning(t("allocation_asset.missingCCDC"));
    }
  };

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    });
  };
  handlePersonPopupOpen = () => {
    this.setState({
      shouldOpenPersonPopup: true,
    });
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  handleManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: false,
    });
  };
  handleManagementDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = item;
    this.setState({
      handoverDepartment: department,
      handoverDepartmentId: department?.id
    }, function () {
      this.handleManagementDepartmentPopupClose();
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: { id: item?.id, name: item?.text },
      receiverDepartmentId: item?.id,
    },
    );
  };

  selectUsePerson = (rowData, item) => {
    let { assetVouchers } = this.state;
    if (assetVouchers?.length > 0) {
      assetVouchers.forEach(async (assetVoucher) => {
        if (assetVoucher?.asset?.id === rowData?.asset?.id) {
          assetVoucher.usePerson = item ? item : null;
          assetVoucher.usePersonId = item?.id;
          assetVoucher.usePersonId = item ? item?.personId : null;
          assetVoucher.usePersonDisplayName = item?.personDisplayName;
        }
      });
      this.setState({ assetVouchers: assetVouchers });
    }
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item ? item : null,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      managementDepartmentId: item?.id,
      assetVouchers: [],
      listHandoverPerson: [],
    });
  };

  selectReceiverDepartment = async (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );
    this.setState({
      receiverDepartment: item ? item : null,
      receiverDepartmentId: item?.id,
      listReceiverPerson: [],
      assetVouchers: assetVouchers,
      isCheckReceiverDP: item ? false : true,
      receiverPerson: null,
      receiverPersonId: null,
      usePerson: null,
    });
    const itemDepartment = await this.getDetailDepartment(item?.id)
    this.setState({
      signature: {
        ...this.state.signature,
        headOfDepartmentId: this.state.signature?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: this.state.signature?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: this.state.signature?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: this.state.signature?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
      }
    })
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes != null && attributes.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (
          attributes[index].attribute &&
          attributes[index].attribute.id == rowData.attribute.id
        ) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () { });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isViewAssetFile: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  selectAlocationStatus = (item) => {
    this.setState({
      status: item?.indexOrder,
      allocationStatus: item,
      allocationStatusId: item?.id
    });
  };

  handleRowDataCellChange = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData.tableData.id) {
        assetVoucher.note = valueText.target.value;
      }
    });
    this.setState(
      { assetVouchers: assetVouchers, handoverPersonClone: true },
    );
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleSetDataStatus = (data) => {
    let { id } = this.state;

    const statusMappings = {
      [appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder]: [
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder]: [
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder]: [
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder
      ]
    };

    const allocationStatusIndexOrder = this.state.allocationStatus?.indexOrder;
    const dataNew = data.filter((item) => statusMappings[allocationStatusIndexOrder].includes(item.indexOrder));

    this.setState({
      listStatus: dataNew
    });

  };

  handleChangeValueOpenQR = (newValue) => {
    this.setState({
      openPrintQR: newValue
    });
  };

  clearStateDate = () => {
    this.setState({
      handoverDepartment: null,
      handoverDepartmentId: "",
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      allocationStatus: appConst.STATUS_ALLOCATION.MOI_TAO,
      allocationStatusIndex: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
    })
  }

  handleChangeSignature = (data = []) => {
    this.setState({
      signature: data,
    })
  }

  render() {
    let { open, t, i18n, isStatus, isHideButton } = this.props;
    let {
      loading,
      isConfirm,
      isVisibility,
      textNotificationPopup,
      shouldOpenNotificationPopup,
    } = this.state;
    const { organization } = getUserInformation();
    let searchObjectStatus = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let handoverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      orgId: organization?.org?.id,
    };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}
        {this.state?.shouldOpenRefuseTransfer &&
          <ConfirmationDialog
            title={t('general.confirm')}
            open={this.state?.shouldOpenRefuseTransfer}
            onConfirmDialogClose={this.handleCloseRefuseTransferDialog}
            onYesClick={() => this.handleStatus(appConst.STATUS_ALLOCATION.TRA_VE.indexOrder)}
            text={t('general.cancel_receive_assets')}
            agree={t('general.agree')}
            cancel={t('general.cancel')}
          />
        }
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>

        <ValidatorForm
          id="parentAssetAllocation"
          ref="form"
          name={variable.listInputName.formAllocation}
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "3px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("allocation_asset.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent style={{ height: "550px" }}>
            <AllocationScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAlocationStatus={this.selectAlocationStatus}
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleManagementDepartmentPopupClose={
                this.handleManagementDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              handoverDepartmentSearchObject={handoverDepartmentSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              selectReceiverDepartment={this.selectReceiverDepartment}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              handleManagementDepartmentPopupOpen={
                this.handleManagementDepartmentPopupOpen
              }
              handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
              handleSelectReceiverDepartment={
                this.handleSelectReceiverDepartment
              }
              handleDateChange={this.handleDateChange}
              handleDepartmentPopupClose={this.handleDepartmentPopupClose}
              handlePersonPopupOpen={this.handlePersonPopupOpen}
              handlePersonPopupClose={this.handlePersonPopupClose}
              handleAssetPopupOpen={this.handleAssetPopupOpen}
              handleSelectAssetAll={this.handleSelectAssetAll}
              handleAssetPopupClose={this.handleAssetPopupClose}
              removeAssetInlist={this.removeAssetInlist}
              selectUsePerson={this.selectUsePerson}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleRowDataCellChange={this.handleRowDataCellChange}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              getAssetDocument={this.getAssetDocument}
              isVisibility={isVisibility}
              isStatus={isStatus}
              check={this.state.check}
              handleFormSubmit={this.handleFormSubmit}
              isAllocation={this.props.isAllocation}
              isHideButton={isHideButton}
              managementDepartmentId={this.state.managementDepartmentId}
              handleSetDataSelect={this.handleSetDataSelect}
              handleSetDataStatus={this.handleSetDataStatus}
              handleChange={this.handleChange}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleChangeValueOpenQR={this.handleChangeValueOpenQR}
              handleRowDataCellViewAssetFile={this.handleRowDataCellViewAssetFile}
              handleChangeSignature={this.handleChangeSignature}
            />

          </DialogContent>
          <DialogActions className="pr-24 pb-16">
            <Button
              className="btnClose ml-12"
              variant="contained"
              color="secondary"
              onClick={() =>
              (this.props?.isAllocation
                ? (this.props?.isUpdateAsset
                  ? this.props.handleCloseAllocationEditorDialog()
                  : this.props.handleOKEditClose())
                : this.props.handleClose())
              }
            >
              {t("InstrumentToolsTransfer.close")}
            </Button>
            {isConfirm ? <>
              <Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={() => this.handleStatus(appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder)}
              >
                {t("InstrumentToolsTransfer.confirm")}
              </Button>
              <Button
                className="btnRefuse ml-12"
                variant="contained"
                onClick={() => {
                  this.setState({
                    shouldOpenRefuseTransfer: true
                  });
                }}
              >
                {t("InstrumentToolsTransfer.refuse")}
              </Button>
            </> : <>
              {(!isVisibility || this.props.isStatus) &&
                <Button
                  id="save"
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
              {
                this.state?.allocationStatusIndex === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder &&
                <Button
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  onClick={() => {
                    this.handleChangeValueOpenQR(true);
                  }}
                >
                  {t("Asset.PrintQRCode")}
                </Button>
              }
            </>}
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetAllocationEditorDialog.contextType = AppContext;
export default AssetAllocationEditorDialog;
