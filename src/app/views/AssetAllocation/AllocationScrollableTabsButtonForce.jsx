import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Grid,
  Icon,
  IconButton,
} from "@material-ui/core";
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { makeStyles } from '@material-ui/core/styles';
import { createFilterOptions } from "@material-ui/lab";
import {
  DateTimePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { appConst } from "app/appConst";
import {
  LightTooltip,
  TabPanel,
  a11yProps,
  checkInvalidDate,
  convertNumberPrice,
  isCheckLenght,
  filterOptions
} from "app/appFunction";
import viLocale from "date-fns/locale/vi";
import MaterialTable, { MTableToolbar } from 'material-table';
import moment from "moment";
import React, { useEffect, useState } from "react";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { searchByPage as searchByPageStatus } from '../AllocationStatus/AllocationStatusService';
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchReceiverDepartment } from './AssetAllocationService';
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import AssetsQRPrint from "../Asset/ComponentPopups/AssetsQRPrint";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { Label } from "../Component/Utilities";
import { searchByPageDepartmentNew } from "../Department/DepartmentService";
import { SignatureTabComponentV2 } from "../Component/Signature/SignatureTabComponentV2";

function MaterialButton(props) {
  const item = props.item;
  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, appConst.active.delete)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AllocationScrollableTabsButtonForce(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isCheckStatusNew, setIsCheckStatusNew] = React.useState(false);
  const filterAutocomplete = createFilterOptions();
  const [pageSize, setPageSize] = useState(1000)
  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });
  const [listUsePerson, setListUsePerson] = useState([]);
  const [products, setProducts] = useState([]);
  let itemStatus = appConst.listStatusAllocation.find(
    status => status.indexOrder === props?.item?.allocationStatus?.indexOrder
  ) || "";

  useEffect(() => {
    let { item } = props

    let products = item?.assetVouchers?.map(item => {
      item.qrInfo = `Mã TS: ${item?.asset?.code}\n` +
        `Mã QL: ${item?.asset?.managementCode}\n` +
        `Tên TS: ${item?.asset?.name}\n` +
        `Nước SX: ${item?.asset?.madeIn}\n` +
        `Năm SX: ${item?.asset?.yearOfManufacture}\n` +
        `Ngày SD: ${item?.asset?.yearPutIntoUse}\n` +
        `PBSD: ${item?.receiveDepartmentName}\n`
      return item;
    })

    setProducts(products)
  }, [])

  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      departmentId: props?.item?.handoverDepartment?.id ?? '',
      pageSize: pageSize,
    });

    setSearchParamUserByReceiverDepartment({
      departmentId: props?.item?.receiverDepartment?.id ?? '',
      pageSize: pageSize,
    });
  }, [props?.item?.handoverDepartment?.id, props?.item?.receiverDepartment]);

  useEffect(() => {
    setListUsePerson([])
  }, [props?.item?.receiverDepartment?.id]);

  useEffect(() => {
    if (props?.item?.allocationStatus?.indexOrder === appConst.listStatusAllocation[0].indexOrder) {
      setIsCheckStatusNew(true);
    } else {
      setIsCheckStatusNew(false);
    }
  }, [props?.item?.allocationStatus])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      maxWidth: 200,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      hidden: props?.isAllocation || props.isVisibility,
      render: rowData =>
        <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (appConst.active.delete === method) {
              props.removeAssetInlist(rowData.asset.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
    },
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: 50,
      align: 'left',
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
      render: rowData => {
        return ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData?.tableData?.id + 1)
      }
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
    },
    {
      title: t("Asset.managementCode"),
      field: "asset.managementCode",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
    },
    {
      title: t("Asset.name"),
      field: "asset.name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Asset.serialNumber"),
      field: "assetSerialNumber",
      align: "left",
      minWidth: 75,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetSerialNumber || rowData?.asset?.serialNumber || ""
    },
    {
      title: t("Asset.model"),
      field: "assetModel",
      align: "left",
      minWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.assetModel || rowData.asset?.model || ""
    },
    ...(
      props?.item?.id ? [
        {
          title: t("Asset.yearIntoUseTable"),
          field: "asset.yearPutIntoUse",
          align: "left",
          minWidth: 105,
          cellStyle: {
            paddingLeft: 10,
            paddingRight: 10,
            textAlign: "center",
          },
        }
      ] : []
    ),
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 105,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "manufacturerName",
      align: "left",
      minWidth: 110,
      render: (rowData) => rowData?.manufacturerName || rowData?.asset?.manufacturer?.name || ""
    },
    {
      title: t("Asset.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 130,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.asset?.originalCost),
    },
    {
      title: t("Asset.carryingAmount"),
      field: "asset.carryingAmount",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.asset?.carryingAmount),
    },
    {
      title: t("Asset.dateOfReception"),
      // field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
      render: (rowData) => rowData?.asset?.dateOfReception ? moment(rowData?.asset?.dateOfReception).format('DD/MM/YYYY') : ""
    },
    ...(
      props?.item?.id ? [
        {
          title: t("allocation_asset.receiverDepartment"),
          field: "receiveDepartment.name",
          align: "left",
          minWidth: 200,
        },
      ] : []
    ),
    {
      title: t("Asset.usePerson"),
      minWidth: 150,
      align: "left",
      render: rowData => {
        const personObj = rowData?.usePersonId ? {
          personId: rowData?.usePersonId || null,
          personDisplayName: rowData?.usePersonDisplayName || null,
        } : null;
        return (
          (
            props.isVisibility || props?.item?.isCheckReceiverDP ?
              <TextValidator
                className={props.className ? props.className : "w-100"}
                value={rowData.usePerson?.displayName ? rowData.usePerson?.displayName : null}
                InputProps={{
                  readOnly: true,
                }}
              />
              :
              <AsynchronousAutocompleteSub
                className="w-100"
                listData={listUsePerson}
                setListData={setListUsePerson}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByReceiverDepartment}
                defaultValue={rowData ? personObj : null}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                closeIcon={true}
                value={rowData ? personObj : null}
                onSelect={(event) => props.selectUsePerson(rowData, event)}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
              />
          )
        )
      }
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
      render: rowData =>
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note)}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props.isVisibility,
            disableUnderline: props.isVisibility,
          }}
          validators={["maxStringLength:255"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
    },
  ];

  let columnsAssetFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: 50,
      align: 'left',
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 100,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData =>
        // !props.isVisibility ?
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          {!props.item?.isVisibility && (
            <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData?.id)}>
                <Icon fontSize="small" color="error">delete</Icon>
              </IconButton>
            </LightTooltip>
          )}
        </div>
      // : <LightTooltip title={t('general.viewIcon')} placement="top" enterDelay={300} leaveDelay={200}>
      //   <IconButton size="small" onClick={() => props.handleRowDataCellViewAssetFile(rowData)}>
      //     <Icon fontSize="small" color="primary">
      //       visibility
      //     </Icon>
      //   </IconButton>

      // </LightTooltip>
    },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name)
      return;
    }
  }
  return (
    <form id="firstChild">
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            // variant="fullWidth"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab label={t('ReceivingScrollableTabsButtonForce.tab.InformationVoting')} value={0} {...a11yProps(0)} />
            {/*<Tab label={t('ReceivingScrollableTabsButtonForce.tab.sign')} value={1} {...a11yProps(1)} />*/}
            <Tab label={t('ReceivingScrollableTabsButtonForce.tab.AttachedProfile')} value={2} {...a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container spacing={3}>
            <Grid container item md={4} sm={12} xs={12} >
              <Grid item md={6} sm={6} xs={6} className="pr-30">
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                  <DateTimePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span className="colorRed">*</span>
                        {t('allocation_asset.createDate')}
                      </span>}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    name={'createDate'}
                    value={props?.item?.createDate}
                    InputProps={{
                      readOnly: true,
                    }}
                    readOnly={true}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={6} sm={6} xs={6}>
                <CustomValidatePicker
                  fullWidth
                  margin="none"
                  label={
                    <span className="w-100">
                      <span className="colorRed">*</span>
                      {t('allocation_asset.issueDate')}
                    </span>}
                  inputVariant="standard"
                  type="text"
                  format="dd/MM/yyyy"
                  name={'issueDate'}
                  value={props?.item?.issueDate}
                  onChange={date => props.handleDateChange(date, "issueDate")}
                  readOnly={props.isVisibility}
                  minDate={
                    !props.isVisibility && (
                      props.isAllocation
                        ? (new Date(props?.item?.dateOfReception))
                        : new Date("1/1/1900")
                    )
                  }
                  maxDate={!props.isVisibility && new Date()}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                  InputProps={{
                    readOnly: props.isVisibility || props?.isAllocation,
                  }}
                  invalidDateMessage={t("general.invalidDateFormat")}
                  validators={['required']}
                  errorMessages={t('general.required')}
                  minDateMessage={t("allocation_asset.errMissingMindate")}
                  onBlur={() => handleBlurDate(props?.item?.issueDate, "issueDate")}
                  clearable
                />
              </Grid>
            </Grid>
            <Grid item md={4} sm={12} xs={12} className="">
              <AsynchronousAutocompleteSub
                label={
                  <Label isRequired={!props.isVisibility && !props?.isAllocation}>
                    {t("allocation_asset.handoverDepartment")}
                  </Label>
                }
                searchFunction={
                  props.item?.isRoleAssetManager
                    ? getListOrgManagementDepartment
                    : searchByPageDepartmentNew
                }
                searchObject={
                  props.item?.isRoleAssetManager
                    ? {}
                    : props.handoverDepartmentSearchObject
                }
                listData={props.item.listHandoverDepartment}
                nameListData="listHandoverDepartment"
                setListData={props.handleSetDataSelect}
                displayLable={'name'}
                isNoRenderParent
                isNoRenderChildren
                typeReturnFunction={
                  props.item?.isRoleAssetManager
                    ? "list"
                    : "category"
                }
                value={props.item?.handoverDepartment || null}
                onSelect={props.selectHandoverDepartment}
                validators={["required"]}
                errorMessages={[t('general.required')]}
                showCode={"showCode"}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                readOnly={props.isVisibility || props.item?.isRoleAssetManager}
              />
            </Grid>
            <Grid item md={4} sm={12} xs={12} className="">
              <AsynchronousAutocompleteSub
                label={
                  <Label isRequired={!props.isVisibility}>
                    {t("allocation_asset.handoverPerson")}
                  </Label>
                }
                disabled={!props?.item?.handoverDepartment?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchParamUserByHandoverDepartment}
                readOnly={props.isVisibility}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                value={props?.item?.handoverPerson ?? ''}
                onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              {((props.isVisibility && !props.isStatus) || props?.isAllocation) ?
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("allocation_asset.voucherStatus")}
                  defaultValue={props.item?.allocationStatus ? props.item?.allocationStatus?.name : ""}
                  value={itemStatus ? itemStatus?.name : ""}
                  InputProps={{
                    readOnly: true,
                  }}
                />
                :
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span style={{ color: "red" }}>* </span>
                      <span> {t("allocation_asset.voucherStatus")}</span>
                    </span>
                  }
                  searchFunction={searchByPageStatus}
                  searchObject={props.searchObjectStatus}
                  listData={props?.item?.listStatus}
                  setListData={props.handleSetDataStatus}
                  defaultValue={itemStatus ? itemStatus : null}
                  displayLable={'name'}
                  closeIcon={true}
                  value={itemStatus ? itemStatus : null}
                  onSelect={props.selectAlocationStatus}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                />
              }
            </Grid>
            <Grid item md={4} sm={12} xs={12} className="">
              {props.isVisibility ?
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("allocation_asset.receiverDepartment")}
                  value={props.item?.receiverDepartment ? props.item?.receiverDepartment?.text : null}
                  InputProps={{
                    readOnly: true,
                  }}
                />
                :
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      <span> {t("allocation_asset.receiverDepartment")}</span>
                    </span>
                  }
                  searchFunction={searchReceiverDepartment}
                  searchObject={props.receiverDepartmentSearchObject}
                  listData={props.item.listReceiverDepartment}
                  setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
                  defaultValue={props.item.receiverDepartment ? props.item.receiverDepartment : null}
                  displayLable={'text'}
                  value={props.item.receiverDepartment ? props.item.receiverDepartment : null}
                  onSelect={props.selectReceiverDepartment}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                />
              }

            </Grid>
            <Grid item md={4} sm={12} xs={12} >
              {props.isVisibility ? <TextValidator
                className={props.className ? props.className : "w-100"}
                label={t("allocation_asset.receiverPerson")}
                name="receiverPersonName"
                onChange={props?.handleChange}
                value={props.item?.receiverPersonName ? props.item?.receiverPersonName : null}
                InputProps={{
                  readOnly: props.isVisibility,
                }}
              />
                : <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("AssetTransfer.receiverPerson")}
                    </span>
                  }
                  // listData={listReceiverPerson}
                  // setListData={setListReceiverPerson}
                  disabled={!props?.item?.receiverDepartment?.id}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByReceiverDepartment}
                  defaultValue={props?.item?.receiverPerson ?? ''}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={props?.item?.receiverPerson ?? ''}
                  onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />}
            </Grid>
            <Grid className="" item md={3} sm={12} xs={12}>
              {(!props?.isAllocation && !props.isVisibility) &&
                (<Button
                  variant="contained"
                  color="primary"
                  size="small"
                  className="mb-16 w-100"
                  onClick={props.handleAssetPopupOpen}
                >
                  {t('allocation_asset.allocation_asset')}
                </Button>)
              }
              {props.item.shouldOpenAssetPopup && (
                <SelectAssetAllPopup
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAssetAll}
                  assetVouchers={props.item.assetVouchers ? props.item.assetVouchers : []}
                  handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                  voucherType={props.item.type}
                  handoverDepartment={props.item?.handoverDepartment}
                  isAssetAllocation={true}
                  statusIndexOrders={props.item.statusIndexOrders}
                  dateOfReceptionTop={props.item.issueDate}
                  getAssetDocumentList={props.getAssetDocumentList}
                  managementDepartmentId={props.managementDepartmentId}
                  issueDate={props.item?.issueDate}
                  isSearchForAllocation={true}
                />
              )}
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12}>
              <MaterialTable
                data={props.item.assetVouchers ? props.item.assetVouchers : []}
                columns={columns}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  padding: 'dense',
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                    paddingLeft: 10,
                    paddingRight: 10,
                    textAlign: "center",
                  },
                  maxBodyHeight: '225px',
                  minBodyHeight: '225px',
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  }
                }}
                components={{
                  Toolbar: props => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <SignatureTabComponentV2
            isExport={true}
            data={{ ...props.item?.signature }}
            permission={props.item?.isViewSignPermission}
            handleChange={props.handleChangeSignature}
          />
        </TabPanel>
        <TabPanel value={value} index={2} >
          <Grid item md={12} sm={12} xs={12}>
            {(!props?.isVisibility || props?.isAllocation) && <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={
                props.handleAddAssetDocumentItem
              }
            >
              {t('AssetFile.addAssetFile')}
            </Button>}
            {props.item.shouldOpenPopupAssetFile && (
              <VoucherFilePopup
                open={props.item.shouldOpenPopupAssetFile}
                handleClose={props.handleAssetFilePopupClose}
                itemAssetDocument={props.itemAssetDocument}
                getAssetDocument={props.getAssetDocument}
                handleUpdateAssetDocument={props.handleUpdateAssetDocument}
                documentType={props.item?.documentType}
                item={props.item}
                t={t}
                i18n={i18n}
              />)
            }
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item.documents || []}
              columns={columnsAssetFile}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: 'dense',
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                maxBodyHeight: '290px',
                minBodyHeight: '290px',
              }}
              components={{
                Toolbar: props => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
        {props?.item?.openPrintQR && (
          <AssetsQRPrint
            t={t}
            i18n={i18n}
            handleClose={props.handleChangeValueOpenQR}
            open={props?.item?.openPrintQR}
            items={products}
          />
        )}
      </div>
    </form>
  );
}