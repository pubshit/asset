import {
    Button,
    Card,
    Collapse,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination,
    TextField
} from '@material-ui/core';
import React, { useContext } from 'react';
import { ConfirmationDialog } from 'egret';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import SearchIcon from '@material-ui/icons/Search';
import AssetAllocationEditorDialog from './AssetAllocationEditorDialog';
import { MTableToolbar } from 'material-table';
import CustomMaterialTable from '../CustomMaterialTable';
import { LIST_ORGANIZATION, OPTIONS_EXCEL_QR_CARD, PRINT_TEMPLATE_MODEL, appConst, variable } from 'app/appConst';
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { NumberFormatCustom, convertNumberPriceRoundUp, filterOptions, getTheHighestRole } from "app/appFunction";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import {
    exportExcelFileError,
    getListManagementDepartment,
    importExcelAssetAllocationURL,
    searchReceiverDepartment,
} from './AssetAllocationService';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { Autocomplete } from '@material-ui/lab';
import AssetsQRPrintNew from '../Asset/ComponentPopups/AssetsQRPrintNew';
import CardContent from "@material-ui/core/CardContent";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from 'app/services/localStorageService';
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import CustomTablePagination from "../CustomTablePagination"

function ComponentAllocationTable(props) {
    let { t, i18n } = props;
    let {
        item,
        page,
        toDate,
        itemList,
        isStatus,
        fromDate,
        statusIndex,
        rowsPerPage,
        isVisibility,
        isHideButton,
        totalElements,
        originalCostTo,
        shouldOpenPrint,
        isRoleAssetUser,
        originalCostFrom,
        allocationStatus,
        hasEditPermission,
        openAdvanceSearch,
        hasPrintPermission,
        handoverDepartment,
        receiverDepartment,
        hasCreatePermission,
        hasDeletePermission,
        listHandoverDepartment,
        listReceiverDepartment,
        shouldOpenEditorDialog,
        shouldOpenImportExcelDialog,
        shouldOpenNotificationPopup,
        shouldOpenConfirmationDialog,
        shouldOpenConfirmationDeleteAllDialog,
        openPrintQR,
        products,
        isRoleAssetManager,
        isRoleOrgAdmin,
        isRoleAdmin,
        isDisable,
        itemAsset
    } = props?.item;
    const { currentOrg } = useContext(AppContext);
    const { ALLOCATION } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    const isShowPrintPreview = [
        LIST_ORGANIZATION.BVDK_BA_VI.code,
        LIST_ORGANIZATION.BVDK_MY_DUC.code,
    ].includes(currentOrg?.code);
    let isButtonAll = !statusIndex;
    const searchObject = { pageIndex: 1, pageSize: 1000000 };
    const searchObjectDepartment = {
        ...searchObject,
        checkPermissionUserDepartment: false
    }

    const handleKeyDown = (e) => {
        if (variable.regex.decimalNumberExceptThisSymbols.includes(e?.key)) {
            e.preventDefault();
        }
        props.handleKeyDownEnterSearch(e);
    };

    let roles = getTheHighestRole();

    let date = new Date(item?.issueDate);

    const addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");
    const day = String(date?.getDate())?.padStart(2, '0');
    const month = String(date?.getMonth() + 1)?.padStart(2, '0');
    const year = String(date?.getFullYear());

    let countItem = item?.assetVouchers?.reduce(
        (value, item) => value + Number(item?.quantity || 1), 0
    );
    let sumTotalCost = item?.assetVouchers?.reduce(
        (value, item) => value + Number(item?.assetOriginalCost || 0), 0
    );

    let dataView = {
        ...roles,
        addressOfEnterprise,
        day: item?.issueDate ? day : " ..... ",
        month: item?.issueDate ? month : " ..... ",
        year: item?.issueDate ? year : " ..... ",
        dataPrint: {
            ...item,
            assetVouchers: item?.assetVouchers?.map((i, index) => {
                i.index = index + 1;
                i.quantity = i.quantity || 1;
                return { ...i, assetOriginalCost: convertNumberPriceRoundUp(i.assetOriginalCost || 0) }
            })
        },
        countItem,
        sumTotalCost: convertNumberPriceRoundUp(sumTotalCost || 0)
    }

    return (
        <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={6} xs={12}>
                {hasCreatePermission && isButtonAll && (
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            props.handleEditItem({
                                startDate: new Date(),
                                endDate: new Date(),
                                isCheckReceiverDP: true,
                                isCheckHandoverDP: true,
                            });
                            props.setState({
                                isVisibility: false,
                                isStatus: false,
                                isHideButton: true,
                            });
                        }}
                    >
                        {t('Asset.allocation_asset')}
                    </Button>
                )}
                {(isRoleAssetManager || isRoleOrgAdmin || isRoleAdmin) && (
                    <Button
                        className="align-bottom mr-16"
                        variant="contained"
                        color="primary"
                        onClick={props.importExcel}
                    >
                        {t("general.importExcel")}
                    </Button>
                )}
                {(
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={(event) =>
                            props.exportToExcel(event, OPTIONS_EXCEL_QR_CARD[0].code)
                        }
                    >
                        {t("general.exportToExcel")}
                    </Button>
                )}
                <Button
                    className="mr-16 align-bottom"
                    variant="contained"
                    color="primary"
                    onClick={props.handleOpenAdvanceSearch}
                >
                    {t("general.advancedSearch")}
                    <ArrowDropDownIcon />
                </Button>

                {shouldOpenImportExcelDialog && (
                    <ImportExcelDialog
                        t={t}
                        i18n={i18n}
                        open={shouldOpenImportExcelDialog}
                        handleClose={props?.handleDialogClose}
                        handleOKEditClose={props?.handleOKEditClose}
                        exportExampleImportExcel={props.exportExampleImportExcel}
                        exportFileError={exportExcelFileError}
                        url={importExcelAssetAllocationURL()}
                    />
                )}

                {shouldOpenConfirmationDeleteAllDialog && (
                    <ConfirmationDialog
                        open={shouldOpenConfirmationDeleteAllDialog}
                        onConfirmDialogClose={props.handleDialogClose}
                        onYesClick={props.handleDeleteAll}
                        text={t('general.deleteAllConfirm')}
                    />
                )}

                {shouldOpenNotificationPopup && (
                    <NotificationPopup
                        title={t('general.noti')}
                        open={shouldOpenNotificationPopup}
                        onYesClick={props?.handleNotificationPopup}
                        text={t('allocation_asset.not_edit')}
                        agree={t('general.agree')}
                    />
                )}
                {openPrintQR && (
                    <AssetsQRPrintNew
                        t={t}
                        i18n={i18n}
                        handleClose={props.handleCloseQrCode}
                        open={openPrintQR}
                        items={products}
                    />
                )}
            </Grid>
            <Grid item md={6} sm={12} xs={12}>
                <FormControl fullWidth>
                    <Input
                        className="search_box w-100"
                        onChange={props.handleTextChange}
                        onKeyDown={props.handleKeyDownEnterSearch}
                        onKeyUp={props?.handleKeyUp}
                        placeholder={t('allocation_asset.enterSearch')}
                        id="search_box"
                        startAdornment={
                            <InputAdornment position='end'>
                                <SearchIcon onClick={() => props.search()} className="searchTable" />
                            </InputAdornment>
                        }
                    />
                </FormControl>
            </Grid>

            {/* Bộ lọc Tìm kiếm nâng cao */}
            <Grid item xs={12}>
                <Collapse in={openAdvanceSearch}>
                    <ValidatorForm onSubmit={() => { }}>
                        <Card elevation={2}>
                            <CardContent>
                                <Grid container spacing={2}>
                                    {/* Phòng bàn giao */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteTransfer
                                            label={t("allocation_asset.handoverDepartment")}
                                            searchFunction={getListManagementDepartment}
                                            searchObject={searchObject}
                                            listData={listHandoverDepartment || []}
                                            typeReturnFunction="list"
                                            setListData={(data) => props?.handleSetDataSelect(
                                                data,
                                                "listHandoverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            defaultValue={handoverDepartment ? handoverDepartment : null}
                                            displayLable={"text"}
                                            value={handoverDepartment ? handoverDepartment : null}
                                            onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Phòng tiếp nhận */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteTransfer
                                            className="w-100"
                                            label={t("allocation_asset.receiverDepartment")}
                                            searchFunction={searchReceiverDepartment}
                                            searchObject={searchObjectDepartment}
                                            listData={listReceiverDepartment || []}
                                            setListData={(data) => props.handleSetDataSelect(
                                                data,
                                                "listReceiverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            displayLable={"text"}
                                            value={receiverDepartment ? receiverDepartment : null}
                                            onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            disabled={isRoleAssetUser}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Từ ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxFrom")}
                                                format="dd/MM/yyyy"
                                                value={fromDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "fromDate"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                maxDate={toDate ? new Date(toDate) : undefined}
                                                minDateMessage={t("general.minDateMessage")}
                                                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearable
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Đến ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxTo")}
                                                format="dd/MM/yyyy"
                                                value={toDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "toDate"
                                                )}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                minDate={fromDate ? new Date(fromDate) : undefined}
                                                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                maxDateMessage={t("general.maxDateMessage")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearable
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Trạng thái */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <Autocomplete
                                            fullWidth
                                            options={appConst.listStatusAllocation}
                                            defaultValue={allocationStatus ? allocationStatus : null}
                                            value={allocationStatus ? allocationStatus : null}
                                            onChange={(e, value) => props.handleSetDataSelect(value, "allocationStatus")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            getOptionLabel={option => option.name}
                                            disabled={isDisable}
                                            noOptionsText={t("general.noOption")}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    value={allocationStatus?.name || ""}
                                                    label={t("maintainRequest.status")}
                                                />
                                            )}
                                        />
                                    </Grid>
                                    {/* Nguyên giá từ - đến */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <TextValidator
                                            fullWidth
                                            label={t("general.originalCostFrom")}
                                            value={originalCostFrom || ""}
                                            name="originalCostFrom"
                                            onKeyUp={props.handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => props.handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={3}>
                                        <TextValidator
                                            fullWidth
                                            label={t("general.originalCostTo")}
                                            value={originalCostTo || ""}
                                            name="originalCostTo"
                                            onKeyUp={props.handleKeyUp}
                                            onKeyDown={handleKeyDown}
                                            onChange={(e) => props.handleSetDataSelect(
                                                e?.target?.value,
                                                e?.target?.name,
                                            )}
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </ValidatorForm>
                </Collapse>
            </Grid>

            <Grid item xs={12}>
                <div>
                    {shouldOpenEditorDialog && (
                        <AssetAllocationEditorDialog
                            t={t}
                            i18n={i18n}
                            open={shouldOpenEditorDialog}
                            handleClose={props.handleDialogClose}
                            handleOKEditClose={props.handleOKEditClose}
                            item={props?.item?.item}
                            isVisibility={isVisibility}
                            isStatus={isStatus}
                            isAllocation={false}
                            isHideButton={isHideButton}
                            exportToExcel={props.exportToExcel}
                        />
                    )}

                    {shouldOpenPrint &&
                        (
                            isShowPrintPreview ? (
                                <PrintPreviewTemplateDialog
                                    t={t}
                                    handleClose={props.handleDialogClose}
                                    open={shouldOpenPrint}
                                    item={item}
                                    title={t("allocation_asset.allocation_assetPrint")}
                                    model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.ALLOCATION}
                                />
                            ) : (
                                <PrintMultipleFormDialog
                                    t={t}
                                    i18n={i18n}
                                    handleClose={props.handleDialogClose}
                                    open={shouldOpenPrint}
                                    item={dataView}
                                    title={t("allocation_asset.allocation_assetPrint")}
                                    urls={[
                                        ...ALLOCATION.GENERAL,
                                        ...(ALLOCATION[currentOrg?.printCode] || []),
                                    ]}
                                />
                            ))}

                    {shouldOpenConfirmationDialog && (
                        <ConfirmationDialog
                            title={t('general.confirm')}
                            open={shouldOpenConfirmationDialog}
                            onConfirmDialogClose={props.handleDialogClose}
                            onYesClick={props.handleConfirmationResponse}
                            text={t('general.cancel_asset_allocation')}
                            agree={t('general.agree')}
                            cancel={t('general.cancel')}
                        />
                    )}
                </div>
                <CustomMaterialTable
                    title={t('general.list')}
                    data={itemList}
                    onRowClick={(e, rowData) => { return props?.setItemState(rowData) }}
                    columns={
                        hasDeletePermission || hasEditPermission || hasPrintPermission
                            ? props?.columns
                            : props?.columnsNoAction
                    }
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`,
                        },
                    }}
                    options={{
                        draggable: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        sorting: false,
                        rowStyle: (rowData) => ({
                            backgroundColor: itemAsset?.id === rowData?.id
                                ?
                                "#ccc"
                                : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "490px",
                        minBodyHeight: "260px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                            paddingLeft: 10,
                            paddingRight: 10,
                            textAlign: "center",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                    onSelectionChange={(rows) => {
                        this.data = rows;
                    }}
                />
                <CustomTablePagination
                    rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                    labelDisplayedRows={({ from, to, count }) => {
                        return `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`
                    }}
                    count={totalElements}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={props.handleChangePage}
                    onRowsPerPageChange={props.setRowsPerPage}
                />
            </Grid>
        </Grid>
    );
}

export default ComponentAllocationTable;
