import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input, InputAdornment, TextField,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { getItemById, deleteCheckItem, getPageBySearchTextAndOrgStore, getListOfStoreAssets, } from "./StoreService";
import StoreEditorDialog from "./StoreEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from 'react-i18next';
import { saveAs } from 'file-saver';
import { Helmet } from 'react-helmet';
import { withStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchReceiverDepartment } from "../AssetTransfer/AssetTransferService";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { ValidatorForm } from "react-material-ui-form-validator";
import AppContext from "app/appContext";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { defaultPaginationProps, getRoleCategory } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { STATUS_STORE, appConst } from "../../appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});

const style = {
  relative: {
    position: "relative"
  },
  autocompletePosition: {
    marginTop: "6px",
  }
}

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: '-1.0em',
  }
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { isRoleAssetManager } = props
  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    {
      !isRoleAssetManager &&
      <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
        PopperProps={{
          popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
        }} >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
          <Icon fontSize="small" color="error">delete</Icon>
        </IconButton>
      </LightTooltip>
    }
  </div>;
}

class StoreTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    Store: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationDeleteListDialog: false,
    departmentFilter: {},
    mainStore: {},
    isActive: '',
    shouldOpenNotificationPopup: false,
    isRoleAssetManager: getRoleCategory().isRoleAssetManager,
    listDataDepartment: []
  };
  filterAutocomplete = createFilterOptions();
  department = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.DEPARTMENT_USER)

  numSelected = 0;
  rowCount = 0;

  departmentSearchObject = {
    pageIndex: 1,
    pageSize: 100,
    keyword: '',
    checkPermissionUserDepartment: true,
  }
  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
    })
  };

  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.search();
    }
  };

  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageDataSearch();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageDataSearch();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      this.updatePageDataSearch()
    });
  }
  searchByStatus = (e, value) => {
    this.setState((preState) => ({ ...preState, isActive: value }), () => {
      this.search();
    });
  }

  checkData = () => {
    if (!this.data || this.data.length === 0) {
      // alert("Chưa chọn dữ liệu");
      toast.warning("Chưa chọn dữ liệu");
    } else if (this.data.length === this.state.itemList.length) {
      // alert("Bạn có muốn xoá toàn bộ");

      this.setState({ shouldOpenConfirmationDeleteAllDialog: true })

    } else {
      this.setState({ shouldOpenConfirmationDeleteListDialog: true })

    }
  }

  updatePageDataSearch = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let objectSearch = {
      pageSize: this.state.rowsPerPage,
      pageIndex: this.state.page + 1,
      departmentId: this.state.departmentFilterId,
      keyword: this.state.keyword.trim(),
      isActive: this.state.isActive?.value
    }
    if (this.state.isRoleAssetManager) {
      objectSearch.departmentId = this.department?.id;
    }
    getPageBySearchTextAndOrgStore(objectSearch).then(({ data }) => {
      this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      setPageLoading(false)
    })
      .catch((e) => {
        setPageLoading(false)
      });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenConfirmationDeleteListDialog: false,
      shouldOpenNotificationPopup: false

    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageDataSearch();
  };

  handleDeleteStore = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditStore = item => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true)
    getItemById(item.id).then((result) => {
      setPageLoading(false)
      this.setState({
        item: result?.data || {},
        shouldOpenEditorDialog: true
      });
    }).catch((e) => {
      setPageLoading(false)
      toast.error(t("toastr.error"))
    });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true)
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count
      })
    } else if (this.state.itemList.length === 1 && this.state.page === 1) {
      this.setState({
        page: 1
      })
    }

    deleteCheckItem(this.state.id)
      .then((res) => {
        setPageLoading(false);
        toast.success(t('Store.noti.deleteSuccess'));
        this.handleDialogClose()
        this.updatePageDataSearch()
      })
      .catch((err) => {
        toast.warning(t('Store.noti.use'));
        setPageLoading(false);
        // alert('Loại sản phẩm đang sử dụng, không thể xóa')
        // this.handleDialogClose()
      })
  };

  componentDidMount() {
    this.updatePageDataSearch();
  }

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.departmentFilterId !== this.state.departmentFilterId) {
      this.updatePageDataSearch();
    }
  }

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleEditItem = item => {
    if (this.state.isRoleAssetManager) {
      item.departmentId = this.department?.id
      item.departmentName = this.department?.name
    }
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleClick = (event, item) => {
    let { Store } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < Store.length; i++) {
      if (Store[i].checked == null || Store[i].checked === false) {
        selectAllItem = false;
      }
      if (Store[i].id === item.id) {
        Store[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, Store: Store });

  };
  handleSelectAllClick = (event) => {
    let { Store } = this.state;
    for (var i = 0; i < Store.length; i++) {
      Store[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, Store: Store });
  };

  handleDelete = (id) => {
    getListOfStoreAssets(id)
      .then(res => {
        if (res?.data) {
          this.setState({ shouldOpenNotificationPopup: true });
        } else {
          this.openConfirmationDialog(id);
        }
      })
      .catch(err => {
      });
  };

  openConfirmationDialog = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };


  async handleDeleteList(list) {
    let listAlert = [];
    let { t } = this.props
    for (var i = 0; i < list.length; i++) {

      try {
        await deleteCheckItem(list[i].id);
      } catch (error) {
        listAlert.push(list[i].name);
      }
    }
    // this.handleDialogClose()
    if (listAlert.length === list.length) {
      toast.warning(t('Store.noti.use_all'));
      // alert("Loại sản phẩm đều đã sử dụng");
    } else if (listAlert.length > 0) {
      toast.warning(t('Store.noti.deleted_unused'));

      // alert("Đã xoá loại sản phẩm chưa sử dụng");
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props;
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageDataSearch();
      this.handleDialogClose();
      this.data = null;
    }).catch(() => {
      toast.error(t("toastr.error"))
    });
  };

  selectDepartmentFilter = (item) => {
    this.setState({
      departmentFilter: item,
      departmentFilterId: item?.id,
    });
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenNotificationPopup,
      isRoleAssetManager
    } = this.state;
    const filterAutocomplete = createFilterOptions();
    let TitlePage = t("Store.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: "100px",
        headerStyle: {
          padding: '0px'
        },
        cellStyle: {
          padding: '0px',
          textAlign: 'center'
        },
        render: rowData => <MaterialButton
          item={rowData}
          isRoleAssetManager={isRoleAssetManager}
          onSelect={(rowData, method) => {
            if (method === 0) {
              this.handleEditStore(rowData)
            } else if (method === 1) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      {
        title: t("Store.code"), field: "code", minWidth: "140px", cellStyle: {
          textAlign: 'center'
        }
      },
      { title: t("Store.name"), field: "name", align: "left", minWidth: "200px" },
      { title: t("Store.status"), field: "isActive", align: "center", minWidth: "140px", render: (rowData) => rowData.isActive === STATUS_STORE.HOAT_DONG.code ? t("Store.active") : t("Store.unactive") },
      { title: t("Store.type"), field: "type", align: "left", minWidth: "140px", render: (rowData) => appConst.typeStore.find(item => item.code === rowData.type)?.name },
      { title: t("Store.managementDepartment"), field: "departmentName", align: "left", minWidth: "260px" },
      { title: t("Store.address"), field: "address", align: "left", minWidth: "220px" },
      { title: t("Store.parentStore"), field: "parentName", align: "left", minWidth: "200px" },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.category"), path: "/list/stores" },
            { name: TitlePage }]} />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={isRoleAssetManager ? 4 : 2} xs={12} >
            <Button
              className="align-bottom mr-16 mb-16"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({ startDate: new Date(), endDate: new Date() });
              }
              }
            >
              {t('general.add')}
            </Button>
          </Grid>
          <Grid item md={2} xs={12} >
            <div style={{ ...style.relative }}>
              <Autocomplete
                style={{ ...style.autocompletePosition }}
                className="search_box w-100 statusFilter"
                fullWidth
                options={[
                  {
                    name: t('Store.unactive'),
                    value: 0
                  },
                  {
                    name: t('Store.active'),
                    value: 1
                  },

                ]}
                value={this.state.isActive}
                onChange={(e, value) => this.searchByStatus(e, value)}
                getOptionLabel={(option) => option?.name || ""}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder={t('Store.filterStatus')}
                    variant="standard"
                  />
                )}
                filterOptions={(options, params) => {
                  params.inputValue = params.inputValue.trim()
                  let filtered = filterAutocomplete(options, params)
                  return filtered
                }}
                noOptionsText={t("general.noOption")}
              />
            </div>
          </Grid>

          {
            !isRoleAssetManager &&
            <Grid item md={2} sm={12} xs={12}>
              <ValidatorForm
                id=""
                ref="form"
                onSubmit={() => { }}
                className="departmentFilterStore"
              >
                <AsynchronousAutocompleteSub
                  placeholder={t("Store.filterDepartment")}
                  listData={this.state?.listDataDepartment}
                  setListData={(value) => this.setState({ listDataDepartment: value })}
                  searchFunction={searchReceiverDepartment}
                  searchObject={this.departmentSearchObject}
                  defaultValue={this.state.departmentFilter ? this.state.departmentFilter : null}
                  displayLable={'text'}
                  value={this.state.departmentFilter ? this.state.departmentFilter : null}
                  onSelect={this.selectDepartmentFilter}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = this.filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                />
              </ValidatorForm>
            </Grid>
          }

          <Grid item md={6} sm={12} xs={12} >
            <FormControl fullWidth style={{ marginTop: '6px' }}>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("Store.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#"> <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>

          {shouldOpenConfirmationDeleteAllDialog && (
            <ConfirmationDialog
              open={shouldOpenConfirmationDeleteAllDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleDeleteAll}
              text={t('general.deleteAllConfirm')}
              agree={t('general.agree')}
              cancel={t('general.cancel')}
            />
          )}
          {this.state.shouldOpenConfirmationDeleteListDialog && (
            <ConfirmationDialog
              open={this.state.shouldOpenConfirmationDeleteListDialog}
              onConfirmDialogClose={this.handleDialogClose}
              onYesClick={this.handleDeleteAll}
              text={t('general.deleteConfirm')}
              agree={t('general.agree')}
              cancel={t('general.cancel')}
            />
          )}

          {shouldOpenNotificationPopup && (
            <NotificationPopup
              title={t("general.noti")}
              open={shouldOpenNotificationPopup}
              onYesClick={this.handleDialogClose}
              onNoClick={this.handleDialogClose}
              text={t('Department.messageWarning')}
              cancel={t("general.cancel")}
            />
          )}

          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <StoreEditorDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  departmentSearchObject={this.departmentSearchObject}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              parentChildData={(row, rows) => {
                return rows.find(a => a.id === row.parentId)
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  // nRowsSelected: '${t('general.selects')}',
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
            { ...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

StoreTable.contextType = AppContext;
export default StoreTable;
