import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { addNewOrUpdateStore, checkCode, getAllStores, getListOfStoreAssets, getMainStore } from "./StoreService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { STATUS_STORE, appConst, variable } from "app/appConst";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { getListUserByDepartmentId, searchReceiverDepartment } from "../AssetTransfer/AssetTransferService";
import { createFilterOptions } from "@material-ui/lab";
import AppContext from "app/appContext";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { filterOptions, getRoleCategory } from "app/appFunction";
import { Label, PaperComponent } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
class StoreEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    type: "",
    address: "",
    departmentManagement: null,
    isMain: false,
    parentId: "",
    parentName: "",
    departmentId: "",
    departmentName: "",
    isActive: STATUS_STORE.KHONG_HOAT_DONG.code,
    shouldOpenNotificationPopup: false,
    mainStore: null,
    storekeeper: null,
  };

  filterAutocomplete = createFilterOptions();

  async getMainStoreByDepartmentId(departmentId) {
    let { t } = this.props
    try {
      if (departmentId) {
        let res = await getMainStore(departmentId)
        if (res?.status === appConst.CODE.SUCCESS && res?.data) {
          let storeIsMainStore = res.data?.id === this.state?.id
          if (storeIsMainStore) {
            this.setState({
              mainStore: null
            })
          }
          else {
            this.setState({
              mainStore: res?.data,
              isMain: false
            })
          }
        }
        else {
          this.setState({
            mainStore: null
          })
        }
      }
    } catch (error) {
      toast.error(t("toastr.error"))
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.departmentId !== this.state.departmentId) {
      if (this.state.departmentId) {
        this.getMainStoreByDepartmentId(this.state.departmentId)
      }
    }
  }

  handleChange = (event, source) => {
    if (source === "isMain") {
      this.setState({
        isMain: !this.state.isMain,
      });
      return;
    }
    if (source === 'isActive') {
      let { item } = this.props;
      if (item?.isActive) {
        getListOfStoreAssets(item.id).then((res) => {
          if (res?.data) {
            this.setState({ shouldOpenNotificationPopup: true });
          } else {
            if (this.state.isActive) {
              this.setState({ isActive: STATUS_STORE.KHONG_HOAT_DONG.code });
            } else {
              this.setState({ isActive: STATUS_STORE.HOAT_DONG.code });
            }
          }
        }).catch(err => { })
      } else {
        this.setState({ isActive: event.target.checked === true ? STATUS_STORE.HOAT_DONG.code : STATUS_STORE.KHONG_HOAT_DONG.code });
      }
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = () => {
    let { setPageLoading } = this.context;
    let { id, code } = this.state;
    let { t, selectStores = () => { } } = this.props;
    setPageLoading(true)
    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        setPageLoading(false)
        toast.warning(t("Store.noti.dupli_code"));
      } else {
        if (id) {
          addNewOrUpdateStore({
            ...this.state,
            type: this.state.type?.code,
          }).then((response) => {
            setPageLoading(false)
            if (response.data && response.status === appConst?.CODE.SUCCESS && response?.data?.id) {
              selectStores(response.data);
              toast.success(t("Store.noti.updateSuccess"));
              this.props.handleOKEditClose();
            } else {
              toast.warning(response?.data?.message);
            }
          }).catch((e) => {
            setPageLoading(false)
            toast.error(t("Store.noti.addFail"));
          });
        } else {
          addNewOrUpdateStore({
            ...this.state,
            type: this.state.type?.code,
          }).then((response) => {
            setPageLoading(false)
            if (response.data && response.status === appConst?.CODE.SUCCESS && response?.data?.id) {
              selectStores(response.data);
              // this.props.handleSetDataSelect([]);
              toast.success(t("Store.noti.addSuccess"));
              this.props.handleOKEditClose();
            } else {
              toast.error(t("Store.noti.addFail"));
            }
          }).catch((e) => {
            setPageLoading(false)
            toast.error(t("Store.noti.addFail"));
          });
        }
      }
    }).catch(() => {
      setPageLoading(false)
      toast.error(t("toastr.error"))
    });
  };

  componentWillMount() {
    this.setState(
      {
        ...this.props.item,
        departmentManagement: {
          id: this.props.item?.departmentId,
          name: this?.props.item?.departmentName,
          text: this?.props.item?.departmentName
        },
        parentStore: {
          id: this.props.item?.parentId,
          name: this.props.item?.parentName
        },
        name: this.props?.item?.keySearch || this.props.item?.name,
        type: appConst.typeStore.find(x => x.code === this.props.item.type) ?? null,
        storekeeper: this.props?.item?.storekeeperId ? {
          personId: this.props?.item?.storekeeperId,
          personDisplayName: this.props?.item?.storekeeperName,
          storekeeperName: this.props?.item?.storekeeperName,
          storekeeperId: this.props?.item?.storekeeperId,
        } : null
      },
      function () {
        this.getMainStoreByDepartmentId(this.state?.departmentId)
      }
    );
  }
  componentDidMount() { }

  selectDepartmentManagement = (item) => {
    this.setState({
      departmentManagement: item,
      departmentId: item?.id,
      departmentName: item?.text,
      storekeeper: null,
      storekeeperName: null,
      storekeeperId: null
    })
  }

  handleSelectParentStore = (item) => {
    this.setState({
      parentStore: item,
      parentId: item?.id,
      parentName: item?.name
    })
  }

  handleSelectType = type => {
    this.setState({ type })
  }

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleSetData = (data, source) => {
    this.setState({ [source]: data })
    this.checkFieldChange(data, source);
  }

  checkFieldChange = (data, source) => {
    if (source === "storekeeper") {
      this.setState({ storekeeperName: data?.personDisplayName, storekeeperId: data?.personId })
    }
  }
  render() {
    let { open, t } = this.props;
    let { name, address, type, code, departmentManagement, isMain, parentStore, isActive, id, shouldOpenNotificationPopup, mainStore, departmentId, storekeeper } = this.state;
    const searchParamUserByHandoverDepartment = {
      departmentId: this.state?.departmentId ?? '',
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
    }
    return (
      <Dialog open={open} PaperComponent={PaperComponent} width="md" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            onNoClick={this.handleDialogClose}
            text={t('Department.messageWarning')}
            cancel={t("general.cancel")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <span className="">{t("Store.saveUpdate")}</span>
        </DialogTitle>
        <ValidatorForm
          ref="form"
          id="idDialogSotre"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Store.code")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  disabled={id && true}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Store.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Store.address")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="address"
                  value={address}
                  validators={["required", `matchRegexp:${variable.regex.addressValid}`]}
                  errorMessages={[t("general.required"), t("general.addressNotValid")]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  searchFunction={() => { }}
                  label={<Label isRequired>{t("Store.type")}</Label>}
                  listData={appConst.typeStore}
                  displayLable="name"
                  name="type"
                  value={type || null}
                  onSelect={this.handleSelectType}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      <span> {t("Store.managementDepartment")}</span>
                    </span>
                  }
                  listData={this.state?.listManagementDepartment ?? []}
                  setListData={(data) => this.handleSetData(data, "listManagementDepartment")}
                  searchFunction={searchReceiverDepartment}
                  searchObject={this.props?.departmentSearchObject}
                  defaultValue={departmentManagement?.id ? departmentManagement : null}
                  displayLable={'text'}
                  value={departmentManagement?.id ? departmentManagement : null}
                  onSelect={this.selectDepartmentManagement}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = this.filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                  disabled={getRoleCategory().isRoleAssetManager}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span> {t("Store.inventoryController")}</span>
                    </span>
                  }
                  nameListData="listPerson"
                  listData={this.state.listPerson}
                  setListData={this.handleSetData}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchParamUserByHandoverDepartment}
                  selectedOptionKey="personId"
                  value={storekeeper ?? null}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  onSelect={(data) => this.handleSetData(data, "storekeeper")}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  disabled={!departmentManagement?.id}
                />
              </Grid>
              <Grid className="" item md={12} sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      {t("Store.parentStore")}
                    </span>
                  }
                  searchFunction={getAllStores}
                  searchObject={{}}
                  defaultValue={parentStore?.id ? parentStore : null}
                  displayLable="name"
                  value={parentStore?.id ? parentStore : null}
                  onSelect={parentStore => this.handleSelectParentStore(parentStore)}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim()
                    let filtered = this.filterAutocomplete(options, params)
                    return filtered
                  }}
                  noOptionsText={t("general.noOption")}
                />
              </Grid>
              <Grid className="mt-12" item xs={12} sm={12} md={12}>
                <FormControlLabel
                  onChange={(event) => this.handleChange(event, "isMain")}
                  name="isCheckFinancialCode"
                  disabled={(mainStore || !departmentId) && true}
                  control={
                    <Checkbox
                      checked={isMain}
                    />
                  }
                  label={
                    <span style={{ fontSize: "14px" }}>
                      {t("Store.isMain")}
                    </span>
                  }
                />
                {
                  mainStore &&
                  <span
                    style={{ transform: "translateY(1px)", display: "inline-block" }}
                  >
                    Kho chính của phòng ban quản lý: <span style={{ color: "red", fontStyle: "italic" }}>{mainStore?.name}</span>
                  </span>
                }
              </Grid>
            </Grid>
            <Grid item sm={12} xs={12}>
              <FormControlLabel
                name="isActive"
                onChange={(isActive) =>
                  this.handleChange(isActive, "isActive")
                }
                control={<Checkbox checked={this.state.isActive === STATUS_STORE.HOAT_DONG.code ? true : false}

                />}
                label={`${t('Store.status')}: ${this.state.isActive ? t("Store.active") : t("Store.unactive")}`}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                className="mr-12"
                variant="contained"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

StoreEditorDialog.contextType = AppContext;
export default StoreEditorDialog;
