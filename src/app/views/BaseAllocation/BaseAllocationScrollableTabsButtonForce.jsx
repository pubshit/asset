import DateFnsUtils from "@date-io/date-fns";
import { Button, Grid } from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import React from "react";
import { TextValidator } from "react-material-ui-form-validator";

import { personSearchByPage } from "../InventorySuggestedSupplies/InventorySuggestedSuppliesService";
import { getListManagementDepartment } from "../Asset/AssetService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { createFilterOptions } from "@material-ui/lab";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { appConst } from "../../appConst";
import {
  convertNumberPrice,
  convertNumberPriceRoundUp,
  filterOptions,
  formatTimestampToDate,
  LightTooltip,
  TabPanel,
} from "app/appFunction";
import AssetTable from "./AssetTable";
import { searchByPage } from "../Department/DepartmentService";
import { getManagementDepartment } from "app/appServices";
import moment from "moment";

export default function BaseAllocationScrollableTabsButtonForce(props) {
  const {
    t,
    handleChange,
    dateAllocation,
    voucherDay,
    handleSelectHandoverDepartment,
    handoverDepartment,
    handleSelectHandoverPeople,
    handoverPeople,
    handleSelectStatus,
    status,
    handleSelectReceptionDepartment,
    receptionDepartment,
    handleSelectReceptionPeople,
    receptionPeople,
    handleChangeTabValue,
    tabValue,
    listTableBASE,
    listTableTSCD,
    listTableCCDC,
    listTableVT,
  } = props;

  const filterAutocomplete = createFilterOptions();

  let columnsBASE = [
    {
      title: t("general.action"),
      field: "valueText",
      cellStyle: {
        textAlign: "center",
      },
      // render: (rowData) => (
      //   <LightTooltip
      //     title={t("general.delete")}
      //     placement="top"
      //     enterDelay={300}
      //     leaveDelay={200}
      //   >
      //     <IconButton
      //       size="small"
      //       onClick={() =>
      //         props.handleRowDeleteProduct(
      //           rowData,
      //           appConst.tabBaseList.tabTSCD.nameTable
      //         )
      //       }
      //     >
      //       <Icon fontSize="small" color="error">
      //         delete
      //       </Icon>
      //     </IconButton>
      //   </LightTooltip>
      // ),
    },
    {
      title: t("Asset.stt"),
      field: "code",
      sorting: false,
      minWidth: "40px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("base_allocation.column.tableBASE.code"),
      field: "code",
      align: "left",
      sorting: false,
      minWidth: "140px",
    },
    {
      title: t("base_allocation.column.tableBASE.name"),
      field: "name",
      align: "left",
      sorting: false,
      minWidth: "110px",
    },
    {
      title: t("base_allocation.column.tableBASE.type"),
      field: "type",
      align: "left",
      sorting: false,
      minWidth: "220px",
    },
    {
      title: t("base_allocation.column.tableBASE.receive_date"),
      field: "unit.name",
      align: "left",
      sorting: false,
      minWidth: "90px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("base_allocation.column.tableBASE.originalPrice"),
      field: "name",
      align: "left",
      sorting: false,
      minWidth: "150px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("base_allocation.column.tableBASE.unitPrice"),
      field: "unitPrice",
      sorting: false,
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("base_allocation.column.tableBASE.store"),
      field: "model",
      align: "left",
      sorting: false,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("base_allocation.column.tableBASE.managementDepartment"),
      field: "serialNumber",
      align: "left",
      sorting: false,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("base_allocation.column.tableBASE.useDepartment"),
      field: "yearOfManufacture",
      align: "left",
      sorting: false,
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
  ];
  let columnsTSCD = [
    {
      title: t("general.action"),
      field: "valueText",
      cellStyle: {
        textAlign: "center",
      },
      // render: (rowData) => (
      //   <LightTooltip
      //     title={t("general.delete")}
      //     placement="top"
      //     enterDelay={300}
      //     leaveDelay={200}
      //   >
      //     <IconButton
      //       size="small"
      //       onClick={() =>
      //         props.handleRowDeleteProduct(
      //           rowData,
      //           appConst.tabBaseList.tabTSCD.nameTable
      //         )
      //       }
      //     >
      //       <Icon fontSize="small" color="error">
      //         delete
      //       </Icon>
      //     </IconButton>
      //   </LightTooltip>
      // ),
    },
    {
      title: t("Asset.stt"),
      field: "code",
      sorting: false,
      minWidth: "40px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("Asset.managementCode"),
      field: "managementCode",
      align: "left",
      sorting: false,
      minWidth: "140px",
    },
    {
      title: t("Asset.code"),
      field: "code",
      align: "left",
      sorting: false,
      minWidth: "110px",
    },
    {
      title: t("Asset.name"),
      field: "name",
      align: "left",
      sorting: false,
      minWidth: "220px",
    },
    {
      title: t("Product.stockKeepingUnit"),
      field: "unit.name",
      align: "left",
      sorting: false,
      minWidth: "90px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.receive_date"),
      field: "name",
      align: "left",
      sorting: false,
      minWidth: "150px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.dateOfReception
          ? moment(rowData?.dateOfReception).format("DD/MM/YYYY")
          : null,
    },
    {
      title: t("Asset.yearIntoUseTable"),
      field: "yearPutIntoUse",
      sorting: false,
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.model"),
      field: "model",
      align: "left",
      sorting: false,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.serialNumber"),
      field: "serialNumber",
      align: "left",
      sorting: false,
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.yearOfManufactureTable"),
      field: "yearOfManufacture",
      align: "left",
      sorting: false,
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturerTable"),
      field: "manufacturer.name",
      align: "left",
      sorting: false,
      minWidth: 200,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.originalCost"),
      field: "originalCost",
      sorting: false,
      minWidth: "150px",
      align: "left",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.originalCost
          ? convertNumberPriceRoundUp(rowData?.originalCost)
          : "",
    },
    {
      title: t("Asset.depreciationRateTable"),
      field: "depreciationRate",
      sorting: false,
      minWidth: 80,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.carryingAmountTable"),
      field: "carryingAmount",
      align: "left",
      width: "120",
      sorting: false,
      minWidth: "120px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) =>
        rowData?.carryingAmount
          ? convertNumberPriceRoundUp(rowData?.carryingAmount)
          : "",
    },
    {
      title: t("Asset.status"),
      field: "status.name",
      align: "left",
      width: "120",
      sorting: false,
      minWidth: "115px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.useDepartment"),
      field: "useDepartment.name",
      align: "left",
      sorting: false,
      minWidth: "220px",
    },
  ];
  let columnsCCDC = [
    {
      title: t("general.action"),
      field: "valueText",
      cellStyle: {
        textAlign: "center",
      },
      // render: (rowData) => (
      //   <LightTooltip
      //     title={t("general.delete")}
      //     placement="top"
      //     enterDelay={300}
      //     leaveDelay={200}
      //   >
      //     <IconButton
      //       size="small"
      //       onClick={() =>
      //         props.handleRowDeleteProduct(
      //           rowData,
      //           appConst.tabBaseList.tabCCDC.nameTable
      //         )
      //       }
      //     >
      //       <Icon fontSize="small" color="error">
      //         delete
      //       </Icon>
      //     </IconButton>
      //   </LightTooltip>
      // ),
    },
    {
      title: t("InstrumentToolsList.stt"),
      maxWidth: 50,
      sorting: false,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("InstrumentToolsList.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.dateOfReception"),
      field: "dateOfReception",
      align: "left",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) =>
        moment(rowData?.dateOfReception).format("DD/MM/YYYY"),
    },
    {
      title: t("InstrumentToolsList.managementCode"),
      field: "managementCode",
      align: "left",
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.name"),
      field: "name",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("InstrumentToolsList.amount"),
      field: "originalCost",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        return convertNumberPrice(Math.ceil(rowData?.originalCost));
      },
    },
    {
      title: t("InstrumentToolsList.percentDistributionColumn"),
      field: "depreciationRate",
      align: "left",
      minWidth: 100,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => {
        let numberOfAllocations = rowData?.numberOfAllocations
          ? Math.round((100 / rowData?.numberOfAllocations) * 100) / 100
          : 0;
        return numberOfAllocations;
      },
    },
    {
      title: t("InstrumentToolsList.quantity"),
      field: "quantity",
      align: "left",
      minWidth: 100,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.status"),
      field: "statusName",
      align: "left",
      minWidth: 150,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("InstrumentToolsList.useDepartment"),
      field: "useDepartmentName",
      align: "left",
      minWidth: 180,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
  ];
  let columnsVT = [
    {
      title: t("general.action"),
      field: "valueText",
      cellStyle: {
        textAlign: "center",
      },
      // render: (rowData) => (
      //   <LightTooltip
      //     title={t("general.delete")}
      //     placement="top"
      //     enterDelay={300}
      //     leaveDelay={200}
      //   >
      //     <IconButton
      //       size="small"
      //       onClick={() =>
      //         props.handleRowDeleteProduct(
      //           rowData,
      //           appConst.tabBaseList.tabVT.nameTable
      //         )
      //       }
      //     >
      //       <Icon fontSize="small" color="error">
      //         delete
      //       </Icon>
      //     </IconButton>
      //   </LightTooltip>
      // ),
    },
    {
      title: t("InventoryReceivingVoucher.stt"),
      field: "",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
        textAlign: "center",
      },
      cellStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
        textAlign: "center",
      },
      render: (rowData) => rowData?.tableData?.id + 1,
    },
    {
      title: t("Product.code"),
      field: "productCode",
      minWidth: "80px",
      align: "left",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
      },
      cellStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
        textAlign: "center",
      },
    },
    {
      title: t("Product.name"),
      field: "productName",
      align: "left",
      minWidth: "120px",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
      },
      cellStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
        textAlign: "left",
      },
    },
    {
      title: "ĐVT",
      align: "left",
      field: "skuName",
      minWidth: "20px",
      headerStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
      },
      cellStyle: {
        paddingLeft: "10px",
        paddingRight: "0px",
        textAlign: "center",
      },
    },
    {
      title: t("Product.purchasePlaning"),
      minWidth: "120px",
      align: "left",
      render: (rowData) => rowData?.purchasePlaning?.name,
    },
    {
      title: t("Product.quantity"),
      field: "quantity",
      minWidth: 80,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.quantity,
    },
    {
      title: t("Product.quantityOfVoucher"),
      field: "quantityOfVoucher",
      minWidth: 80,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.quantityOfVoucher,
    },
    {
      title: t("Product.price"),
      field: "price",
      minWidth: "130px",
      align: "left",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.price),
    },
    {
      title: t("InventoryReceivingVoucher.batchCode"),
      field: "batchCode",
      width: "200px",
      align: "left",
    },
    {
      title: t("InventoryReceivingVoucher.expiryDate"),
      field: "expiryDate",
      minWidth: "110px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => formatTimestampToDate(rowData?.expiryDate),
    },
    {
      title: t("Product.amount"),
      field: "amount",
      align: "left",
      cellStyle: {
        textAlign: "right",
      },
      minWidth: 100,
      render: (rowData) => {
        convertNumberPrice(rowData?.amount);
      },
    },
  ];

  return (
    <>
      <Grid item xs={12} sm={3} md={3}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            fullWidth
            margin="none"
            id="mui-pickers-date"
            label={
              <span>
                <span className="colorRed">* </span>
                {t("base_allocation.date_allocation")}
              </span>
            }
            inputVariant="standard"
            autoOk={false}
            format="dd/MM/yyyy"
            name={"date_allocation"}
            value={dateAllocation}
            minDate={new Date("01/01/1900")}
            minDateMessage={t("general.minDateDefault")}
            invalidDateMessage={t("general.invalidDateFormat")}
            onChange={(e) => handleChange(e, "date_allocation")}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            fullWidth
            margin="none"
            id="mui-pickers-date"
            label={
              <span>
                <span className="colorRed">* </span>
                {t("base_allocation.voucherDay")}
              </span>
            }
            inputVariant="standard"
            autoOk={false}
            format="dd/MM/yyyy"
            name={"voucherDay"}
            value={voucherDay}
            minDate={new Date("01/01/1900")}
            minDateMessage={t("general.minDateDefault")}
            invalidDateMessage={t("general.invalidDateFormat")}
            onChange={(e) => handleChange(e, "voucherDay")}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <AsynchronousAutocompleteSub
          className="w-100"
          label={
            <span>
              <span className="colorRed">* </span>
              {t("base_allocation.handover_dapartment")}
            </span>
          }
          searchFunction={getManagementDepartment}
          displayLable="text"
          typeReturnFunction="list"
          value={handoverDepartment ?? ""}
          onSelect={(handoverDepartment) =>
            handleSelectHandoverDepartment(handoverDepartment)
          }
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            let filtered = filterAutocomplete(options, params);
            return filtered;
          }}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <AsynchronousAutocompleteSub
          className="w-100"
          label={
            <span>
              <span className="colorRed">* </span>
              {t("base_allocation.handover_people")}
            </span>
          }
          searchFunction={personSearchByPage}
          searchObject={{
            pageIndex: 1,
            pageSize: 10000,
            departmentId: handoverDepartment?.id,
          }}
          displayLable="displayName"
          value={handoverPeople ?? ""}
          onSelect={(handoverPeople) =>
            handleSelectHandoverPeople(handoverPeople)
          }
          disabled={!handoverDepartment?.id}
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            let filtered = filterAutocomplete(options, params);
            return filtered;
          }}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <Autocomplete
          style={{ marginTop: 3 }}
          id="combo-box"
          size="small"
          fullWidth
          options={appConst.listStatusCoSo}
          onChange={(event, value) => handleSelectStatus(value)}
          value={status || null}
          getOptionLabel={(option) => option.name || ""}
          filterOptions={(options, params) =>
            filterOptions(options, params, true, "name")
          }
          renderInput={(params) => (
            <TextValidator
              {...params}
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("base_allocation.status")}
                </span>
              }
              variant="standard"
            />
          )}
          noOptionsText={t("general.noOption")}
        />
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <AsynchronousAutocompleteSub
          className="w-100"
          label={
            <span>
              <span className="colorRed">* </span>
              {t("base_allocation.reception_department")}
            </span>
          }
          searchFunction={searchByPage}
          displayLable="text"
          value={receptionDepartment ?? ""}
          onSelect={(receptionDepartment) =>
            handleSelectReceptionDepartment(receptionDepartment)
          }
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            let filtered = filterAutocomplete(options, params);
            return filtered;
          }}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>
      <Grid item xs={12} sm={3} md={3}>
        <AsynchronousAutocompleteSub
          className="w-100"
          label={
            <span>
              <span className="colorRed">* </span>
              {t("base_allocation.handover_people")}
            </span>
          }
          searchFunction={personSearchByPage}
          searchObject={{
            pageIndex: 1,
            pageSize: 10000,
            departmentId: receptionDepartment?.id,
          }}
          displayLable="displayName"
          value={receptionPeople ?? ""}
          onSelect={(receptionPeople) =>
            handleSelectReceptionPeople(receptionPeople)
          }
          disabled={!receptionDepartment?.id}
          filterOptions={(options, params) => {
            params.inputValue = params.inputValue.trim();
            let filtered = filterAutocomplete(options, params);
            return filtered;
          }}
          noOptionsText={t("general.noOption")}
          validators={["required"]}
          errorMessages={[t("general.required")]}
        />
      </Grid>

      <div className="mb-sm-30 w-100 mt-20">
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("base_allocation.tab.base")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("base_allocation.tab.TSCD")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("base_allocation.tab.CCDC")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("base_allocation.tab.VT")}</span>
                </div>
              }
            />
          </Tabs>
        </AppBar>
      </div>
      <Grid item xs={12} sm={3} md={3}>
        <Button
          className="mr-12"
          variant="contained"
          color="primary"
          // onClick={props.handleOpenDialog}
        >
          {t("general.add")}
        </Button>
      </Grid>
      <TabPanel
        value={tabValue}
        index={appConst?.tabBaseAllocationList?.tabBase.code}
        className="w-100"
      >
        <AssetTable
          t={t}
          columns={columnsBASE}
          itemList={listTableBASE || []}
        />
      </TabPanel>
      <TabPanel
        value={tabValue}
        index={appConst?.tabBaseAllocationList?.tabTSCD.code}
        className="w-100"
      >
        <AssetTable
          t={t}
          columns={columnsTSCD}
          itemList={listTableTSCD || []}
        />
      </TabPanel>
      <TabPanel
        value={tabValue}
        index={appConst?.tabBaseAllocationList?.tabCCDC.code}
        className="w-100"
      >
        <AssetTable
          t={t}
          columns={columnsCCDC}
          itemList={listTableCCDC || []}
        />
      </TabPanel>
      <TabPanel
        value={tabValue}
        index={appConst?.tabBaseAllocationList?.tabVT.code}
        className="w-100"
      >
        <AssetTable t={t} columns={columnsVT} itemList={listTableVT || []} />
      </TabPanel>
    </>
  );
}
