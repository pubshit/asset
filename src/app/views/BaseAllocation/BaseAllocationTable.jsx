import { Button, Grid } from '@material-ui/core'
import React, { useState } from 'react'
import BaseAllocationDialog from './BaseAllocationDialog';
import { Helmet } from 'react-helmet';
import { Breadcrumb } from 'egret';

export default function BaseAllocationTable({t}) {
  const [shouldOpenEditorDialog, setShouldOpenEditorDialog] = useState(false)
  const handleAddItem = () => {
    setShouldOpenEditorDialog(true)
  }
  const handleClose = () => {
    setShouldOpenEditorDialog(false)
  }
  return (
    <div className="m-sm-30">
      <Helmet>
          <title>
            {t("Dashboard.Base.base_allocation")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.base"), path: "/list/base_allocation" },
              { name: t("Dashboard.Base.base_allocation") },
            ]}
          />
        </div>
      <Grid container spacing={2} justify="space-between" className="mt-10">
        <Grid item md={6} xs={12}>
          <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                handleAddItem();
              }}
            >
              {t("general.add")}
            </Button>
        </Grid>
        <Grid item sx={12}>
          {shouldOpenEditorDialog && 
            <BaseAllocationDialog 
              open={shouldOpenEditorDialog}
              t={t}
              handleClose={handleClose}
            />
          }
        </Grid>
      </Grid>
    </div>
  )
}
