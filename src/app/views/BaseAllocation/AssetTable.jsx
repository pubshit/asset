import {
  Button,
  Grid,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import React from "react";

function AssetTable(props) {
  let {
    t,
    columns,
    itemList = [],
  } = props;

  return (
    <Grid container spacing={2} justify="space-between" >
      <Grid item xs={12}>
        <MaterialTable
          title={t("general.list")}
          data={itemList}
          columns={columns}
          // parentChildData={(row, rows) => {
          //   var list = rows.find((a) => a.id === row.parentId);
          //   return list;
          // }}
          localization={{
            body: {
              emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
            },
            toolbar: {
              nRowsSelected: `${t("general.selects")}`,
            },
          }}
          options={{
            selection: false,
            sorting: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            rowStyle: (rowData) => ({
              backgroundColor: rowData?.tableData?.id % 2 === 1 ? "#EEE" : "#FFF",
            }),
            maxBodyHeight: "450px",
            minBodyHeight: itemList?.length > 0 
              ? 0 : 250,
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
          onSelectionChange={(rows) => {
            this.data = rows;
          }}
        />
      </Grid>
    </Grid>
  );
}

export default AssetTable;
