import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const BaseAllocation = EgretLoadable({
  loader: () => import("./BaseAllocationTable")
});
const ViewComponent = withTranslation()(BaseAllocation);

const BaseAllocationRouter = [
  {
    path: ConstantList.ROOT_PATH + "list/base_allocation",
    exact: true,
    component: ViewComponent
  }
];

export default BaseAllocationRouter;
