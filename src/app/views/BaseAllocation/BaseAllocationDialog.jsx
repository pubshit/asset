import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Paper,
} from "@material-ui/core";
import React, { useState } from "react";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import BaseAllocationScrollableTabsButtonForce from "./BaseAllocationScrollableTabsButtonForce";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

export default function BaseAllocationDialog(props) {
  const { open, t, handleClose } = props;
  const [dateAllocation, setDateAllocation] = useState(new Date());
  const [voucherDay, setVoucherDay] = useState(new Date());
  const [handoverDepartment, setHandoverDepartment] = useState("");
  const [handoverPeople, setHandoverPeople] = useState("");
  const [status, setStatus] = useState("");
  const [receptionDepartment, setReceptionDepartment] = useState("");
  const [receptionPeople, setReceptionPeople] = useState("");
  const [tabValue, setTabValue] = useState(0);
  const [listTableBASE, setListTableBASE] = useState([]);
  const [listTableTSCD, setListTableTSCD] = useState([]);
  const [listTableCCDC, setListTableCCDC] = useState([]);
  const [listTableVT, setListTableVT] = useState([]);

  const handleFormSubmit = async (e) => {};
  const handleChange = (value, name) => {
    if (name === "date_allocation") {
      setDateAllocation(value);
    }
    if (name === "voucherDay") {
      setVoucherDay(value);
    }
  };
  const handleSelectHandoverDepartment = (e) => {
    setHandoverDepartment(e);
  };
  const handleSelectHandoverPeople = (e) => {
    setHandoverPeople(e);
  };
  const handleSelectStatus = (e) => {
    setStatus(e);
  };
  const handleSelectReceptionDepartment = (e) => {
    setReceptionDepartment(e);
  };
  const handleSelectReceptionPeople = (e) => {
    setReceptionPeople(e);
  };
  const handleChangeTabValue = (event, newTab) => {
    setTabValue(newTab);
  };
  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth="lg"
      fullWidth
      scroll={"paper"}
    >
      <ValidatorForm
        onSubmit={handleFormSubmit}
        class="validator-form-scroll-dialog"
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <span className="">{t("base_allocation.title")}</span>
        </DialogTitle>
        <DialogContent style={{ minHeight: "450px" }}>
          <Grid container spacing={1} className="">
            <BaseAllocationScrollableTabsButtonForce
              t={t}
              handleChange={handleChange}
              dateAllocation={dateAllocation}
              voucherDay={voucherDay}
              handleSelectHandoverDepartment={handleSelectHandoverDepartment}
              handoverDepartment={handoverDepartment}
              handleSelectHandoverPeople={handleSelectHandoverPeople}
              handoverPeople={handoverPeople}
              handleSelectStatus={handleSelectStatus}
              status={status}
              handleSelectReceptionDepartment={handleSelectReceptionDepartment}
              receptionDepartment={receptionDepartment}
              handleSelectReceptionPeople={handleSelectReceptionPeople}
              receptionPeople={receptionPeople}
              handleChangeTabValue={handleChangeTabValue}
              tabValue={tabValue}
              listTableBASE={listTableBASE}
              listTableTSCD={listTableTSCD}
              listTableCCDC={listTableCCDC}
              listTableVT={listTableVT}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-12"
              onClick={() => {
                handleClose();
              }}
            >
              {t("general.close")}
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className="mr-12"
            >
              {t("general.save")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
