import { Grid, MuiThemeProvider, IconButton, Icon, TextField, Button, TableHead, TableCell, TableRow, Checkbox, TablePagination } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import { Breadcrumb, ConfirmationDialog } from "egret";
import EQARoundSearchDialog from "./EQARoundSearchDialog";

class EQALabelPrinting extends React.Component {
    state = {
        eqaRound : {},
        shouldOpenSearchRoundDialog: false
    }
    handleSearchRoundDialogClose = () => {
        this.setState({
            shouldOpenSearchRoundDialog: false
        });
    };
    handleSelectRound = (item) => {
        this.setState({ eqaRound: item });
        let eqaRound = this.state.eqaRound;
        if (eqaRound){
        }
        this.handleSearchRoundDialogClose();
    }
    handleEditItem = ()=> {
        this.setState({
          shouldOpenSearchRoundDialog: true
        });
      };
    render() {
        const { t, i18n } = this.props;
        let {
            eqaRound,
            shouldOpenSearchRoundDialog
        } = this.state;
        return (
            <div className="m-sm-30">
                <div className="mb-sm-30">
                    <Breadcrumb routeSegments={[{ name: t("SampleManagement.sample-list.title") }]} />
                </div>
                <Grid item xs={12}>

                    <Button className="mb-16 mr-16 align-bottom" variant="contained" color="primary" onClick={() => {this.handleEditItem()}}>
                        {t('EQARound.choose')}
                    </Button>
                    {/* <TextField label={t('EnterSearch')} className="mb-16 mr-10" type="text" /> */}
                    <Button
                        className="mb-16 mr-30 align-bottom "
                        variant="contained"
                        color="primary"
                    >
                        {t('Print')}
                    </Button>

                </Grid>
                {shouldOpenSearchRoundDialog && (
                    <EQARoundSearchDialog
                        open={this.state.shouldOpenSearchRoundDialog}
                        handleSelect={this.handleSelectRound}
                        selectedItem={eqaRound != null ? eqaRound : {}}
                        handleClose={this.handleSearchRoundDialogClose} t={t} i18n={i18n}
                    />
                )
                }
                
                    
            </div>
        )
    }
}


export default EQALabelPrinting;