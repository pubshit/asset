import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const EQALabelPrinting = EgretLoadable({
  loader: () => import("./EQALabelPrinting")
});
const ViewComponent = withTranslation()(EQALabelPrinting);

const EQALabelPrintingRoutes = [
  {
    path:  ConstantList.ROOT_PATH+"sample/label-printing",
    exact: true,
    component: ViewComponent
  }
];

export default EQALabelPrintingRoutes;
