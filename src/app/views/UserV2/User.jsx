import React, { Component } from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  TextField,
  FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import { findUserByUserName, searchByPage, getUserDepartmentByUserId, getItemById, deleteById } from "./UserService";
import SelectOrganizationPopup from '../Component/Organization/SelectOrganizationPopup'
import UserEditorDialog from "./UserEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import shortid from "shortid";
import { saveAs } from 'file-saver';
import { Helmet } from 'react-helmet';
import SearchIcon from '@material-ui/icons/Search';
import Backspace from '@material-ui/icons/Backspace';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { appConst } from "app/appConst";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});
function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return <div>
    <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
      <Icon fontSize="small" color="primary">edit</Icon>
    </IconButton>

    <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

class User extends Component {
  state = {
    userDepartmentId: '',
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    eQAHealthOrgType: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenSelectOrganizationPopup: false,
    org: {}
  };
  numSelected = 0;
  rowCount = 0;

  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
    })
  };


  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.search();
    }
  };

  handleSelectOrganization = (item) => {
    this.setState({ org: item, shouldOpenSelectOrganizationPopup: false }, () => {
      this.updatePageData()
    })

  }

  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page + 1;
      searchObject.pageSize = this.state.rowsPerPage;
      if (this.state.org != null) {
        searchObject.orgId = this.state.org.id;
      }
      findUserByUserName(searchObject).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      });
    });
  }

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    if (this.state.org != null) {
      searchObject.orgId = this.state.org.id;
    }
    findUserByUserName(searchObject).then(({ data }) => {
      this.setState({ itemList: [...data.content], totalElements: data.totalElements })
    });
  }

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  } 
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenSelectOrganizationPopup: false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleDeleteUser = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditUser = item => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteById(this.state.userId).then((data) => {
      if (!data.data) {
        toast.warning("Không thể xóa.")
      } else {
        toast.info("Xoá thành công.")
      }
      this.updatePageData();
      this.handleDialogClose();
    });
  };

  componentDidMount() {
    this.updatePageData();

  }
  componentWillMount() {
    this.updatePageData();

  }


  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleDelete = row => {
    this.setState({
      userId: row.user.id,
      id: row.id,
      shouldOpenConfirmationDialog: true
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteById(list[i].user.id);
    }
  }

  handleDeleteAll = (event) => {
    //alert(this.data.length);
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    }
    );
  };

  checkData = () => {
    let { t } = this.props
    if (!this.data || this.data.length === 0) {
      toast.warn(t('general.noti_check_data'));
      toast.clearWaitingQueue();
      // this.setState({shouldOpenNotificationPopup: true,
      //   Notification:"general.noti_check_data"})
      //  ("Chưa chọn dữ liệu");
    } else if (this.data.length === this.state.itemList.length) {
      // alert("Bạn có muốn xoá toàn bộ");

      this.setState({ shouldOpenConfirmationDeleteAllDialog: true })
    } else {
      this.setState({ shouldOpenConfirmationDeleteAllDialog: true })
    }
  }

  render() {
    const { t, i18n } = this.props;
    let {
      id,
      userDepartmentId,
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      department,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog,
      shouldOpenSelectOrganizationPopup,
      org
    } = this.state;
    let TitlePage = t("user.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        headerStyle: {
          paddingLeft: '10px'
        },
        cellStyle: {
          paddingLeft: '10px'
        },
        render: rowData => <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            if (method === 0) {
              getItemById(rowData.user.id).then(({ data }) => {
                if (data != null && data.id != null) {
                  this.setState({
                    item: data
                  }, function () {
                    this.setState({ shouldOpenEditorDialog: true });
                  });
                }
              });
            } else if (method === 1) {
              this.handleDelete(rowData);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      {
        title: t("general.index"), field: "code", width: "150", align: "left",
        headerStyle: {
          paddingLeft: '7px'
        },
        cellStyle: {
          paddingLeft: '10px'
        },
        render: rowData => ((page) * rowsPerPage) + (rowData.tableData.id + 1)
      },
      {
        title: t("user.username"), field: "user.username", width: "150", align: "left",
        headerStyle: {
          paddingLeft: '10px'
        },
        cellStyle: {
          paddingLeft: '10px'
        },
      },
      { title: t("user.displayName"), field: "user.displayName", width: "150" },
      { title: t("user.email"), field: "user.email", align: "left", width: "150" },
      { title: t("user.isMainDepartment"), field: "department.name", align: "left", width: "150" },


    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('user.title') }]} /> */}
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.manage"), path: "/manage/user" },
            { name: TitlePage }]} />
        </div>

        <Grid container spacing={2} justify="space-between">
          <Grid item md={7} xs={12} >
            <Button
              className=" align-bottom mb-16 mr-12"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({ startDate: new Date(), endDate: new Date(), isAddNew: true });
              }
              }
            >
              {t('general.add')}
            </Button>

            <Button className="mb-16 mr-36 align-bottom" variant="contained" color="primary"
              onClick={() => this.checkData()}>
              {t('general.delete')}
            </Button>


            <Input
              className="mb-16 mr-36 align-bottom"
              // InputLabelProps={{ shrink: true }}
              InputProps={{
                readOnly: true,
              }}
              InputLabelProps={{ shrink: true }}
              label="Organization"
              style={{ width: "40%" }}
              value={
                org != null ? org.name : ''
              }
              validators={['required']}
              errorMessages={[t('general.required')]}
              startAdornment={
                <InputAdornment>

                  <Link> <Backspace
                    onClick={() => this.setState({ org: null }, () => {
                      this.updatePageData()
                    })}
                    style={{
                      position: "absolute",
                      top: "0",
                      right: "0"
                    }} /></Link>
                </InputAdornment>
              }
            />


            <Button
              className="mb-16 mr-36 align-bottom" variant="contained" color="primary"
              onClick={() => {
                this.setState({ shouldOpenSelectOrganizationPopup: true })
              }}
            >
              {t('general.select')}
            </Button>





            {shouldOpenSelectOrganizationPopup && (
              <SelectOrganizationPopup
                open={shouldOpenSelectOrganizationPopup}
                handleCloseDialog={this.handleDialogClose}
                handleSelect={this.handleSelectOrganization} 
                t={t}
                i18n={i18n}
              />
            )}


            {shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t('DeleteAllConfirm')}
                agree={t('general.agree')}
                cancel={t('general.cancel')}
              />
            )}
          </Grid>
          <Grid item md={5} sm={12} xs={12} >
            <FormControl fullWidth style={{ marginTop: '6px' }}>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("user.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment>

                    <Link> <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <UserEditorDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  department={department}
                  userDepartmentId={userDepartmentId}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('DeleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('user.userList')}
              data={itemList}
              columns={columns}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              parentChildData={(row, rows) => {
                var list = rows.find(a => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                selection: true,
                actionsColumnIndex: 0,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: (rowData.tableData.id % 2 === 1) ? '#EEE' : '#FFF',
                }),
                maxBodyHeight: '450px',
                minBodyHeight: '370px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
                // this.setState({selectedItems:rows});
              }}
              actions={[
                {
                  hidden: true,
                  isFreeAction: true,
                  tooltip: 'Remove All Selected Users',
                  icon: 'delete',
                  onClick: (evt, data) => {
                    this.handleDeleteAll(data);
                    alert('You want to delete ' + data.length + ' rows');
                  }
                },
              ]}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default User;
