import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/stockkeepingunit" + ConstantList.URL_PREFIX;

export const getAllStockKeepingUnits = () => {
  return axios.get(API_PATH + "/1/100000");
};

export const getStockKeepingUnitByPage = (pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  var url = API_PATH + "/" + params;
  return axios.get(url);
};
export const getStockKeepingUnit = (searchObject) => {
  let params = searchObject.pageIndex + "/" + searchObject.pageSize;
  let url = API_PATH + "/" + params;
  return axios.get(url);
};

export const searchByPage = (keyword, pageIndex, pageSize) => {
  var pageIndex = pageIndex;
  var params = pageIndex + "/" + pageSize;
  return axios.post(API_PATH + "/searchByText/" + params, keyword);
};

export const getUserById = (id) => {
  return axios.get("/api/user", { data: id });
};
export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const checkCode = (id, code) => {
  const config = { params: { id: id, code: code } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};

export const addNewOrUpdateStockKeepingUnit = (StockKeepingUnit) => {
  return axios.post(API_PATH, StockKeepingUnit);
};

export const deleteCheckItem = (id) => {
  return axios.delete(API_PATH + "/delete/" + id);
};
