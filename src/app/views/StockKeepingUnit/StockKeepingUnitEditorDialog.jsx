import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  addNewOrUpdateStockKeepingUnit,
  checkCode,
} from "./StockKeepingUnitService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {appConst, variable} from "../../appConst";
import {PaperComponent} from "../Component/Utilities";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class StockKeepingUnitEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    shouldOpenNotificationPopup: false,
  };
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = () => {
    let { id, code } = this.state;
    let { t, selectUnit = () => { }, itemIndex } = this.props;
    //Nếu trả về false là code chưa sử dụng có thể dùng
    checkCode(id, code)
      .then((result) => {
        //Nếu trả về true là code đã được sử dụng
        if (result.data) {
          toast.warning(t("StockKeepingUnit.noti.dupli_code"));
        } else {
          if (id) {
            addNewOrUpdateStockKeepingUnit({
              ...this.state,
            }).then((response) => {
              if (response.data && response.status === appConst.CODE.SUCCESS) {
                selectUnit(response.data, itemIndex);
                toast.success(t("StockKeepingUnit.noti.updateSuccess"));
                this.props.handleOKEditClose();
              } else {
                toast.error(t("StockKeepingUnit.noti.addFail"));
              }
            });
          } else {
            addNewOrUpdateStockKeepingUnit({
              ...this.state,
            }).then((response) => {
              if (response.data && response.status === appConst.CODE.SUCCESS) {
                // this.handleSetDataAsset();
                selectUnit(response.data, itemIndex);
                toast.success(t("StockKeepingUnit.noti.addSuccess"));
                this.props.handleOKEditClose();
              } else {
                toast.error(t("StockKeepingUnit.noti.addFail"));
              }
            });
          }
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  componentDidMount() {
    this.setState(
      {
        ...this.props.item,
        name: this.props.keySearch || this.props.item?.name
      },
      function () { }
    );
  }

  handleDialogClose = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  render() {
    let { open, t } = this.props;
    let { id, name, code, shouldOpenNotificationPopup } = this.state;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md">
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t("general.agree")}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t("StockKeepingUnit.saveUpdate")}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("StockKeepingUnit.code")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required", `matchRegexp:${variable.regex.codeValid}`]}
                  errorMessages={[t("general.required"), t("general.regexCode")]}
                />
              </Grid>
              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("StockKeepingUnit.name")}
                    </span>
                  }
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required", `matchRegexp:${variable.regex.regexNoDoubleDots}`]}
                  errorMessages={[t("general.required"), t("general.invalidFormat")]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default StockKeepingUnitEditorDialog;
