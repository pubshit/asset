import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import MaterialTable, { MTableToolbar } from 'material-table';
import { getStockKeepingUnitByPage, getItemById, searchByPage, deleteCheckItem } from "./StockKeepingUnitService";
import StockKeepingUnitEditorDialog from "./StockKeepingUnitEditorDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from 'react-i18next';
import { saveAs } from 'file-saver';
import { Helmet } from 'react-helmet';
import { withStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from "react-router-dom";
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup'
import { toast } from "react-toastify";
import localStorageService from "app/services/localStorageService";
import ConstantList from "../../appConfig";
import AppContext from "app/appContext";
import { appConst } from "app/appConst";
import {defaultPaginationProps, handleThrowResponseMessage, isSuccessfulResponse} from "app/appFunction";
import {LightTooltip} from "../Component/Utilities";


function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let { hasDeletePermission } = props
  return <div className="none_wrap">
    <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>
    {
      hasDeletePermission && <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}
        PopperProps={{
          popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
        }} >
        <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
          <Icon fontSize="small" color="error">delete</Icon>
        </IconButton>
      </LightTooltip>
    }
  </div>;
}

class StockKeepingUnitTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    StockKeepingUnit: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenNotificationPopup: false,
    hasDeletePermission: false,
  };

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
    } = this.state;
    let currentUser = localStorageService.getSessionItem("currentUser");
    if (currentUser) {
      currentUser.roles.forEach((role) => {
        if (role.name === ConstantList.ROLES.ROLE_ORG_ADMIN) {
          if (hasDeletePermission !== true) {
            hasDeletePermission = true;
          }
        }
      });
      this.setState({
        hasDeletePermission,
      });
    }
  }

  numSelected = 0;
  rowCount = 0;

  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
    })
  };

  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.search();
    }
  };

  handleKeyUp = e => {
    if(!e.target.value)
      this.search();
  }

  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state?.keyword?.trim();
      searchByPage(searchObject, this.state.page + 1, this.state.rowsPerPage).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      });
    });
  }

  updatePageData = () => {
      getStockKeepingUnitByPage(this.state.page + 1, this.state.rowsPerPage).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      }
      );
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenNotificationPopup: false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleDeleteStockKeepingUnit = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleEditStockKeepingUnit = item => {
    getItemById(item.id).then((result) => {
      this.setState({
        item: result.data,
        shouldOpenEditorDialog: true
      });
    });
  };

  handleConfirmationResponse = () => {
    let { t } = this.props;
    if (this.state.itemList.length === 1) {
      let count = this.state.page - 1;
      this.setState({
        page: count
      })
    }
    deleteCheckItem(this.state.id).then((res) => {
      if (isSuccessfulResponse(res?.data?.code)) {
        toast.success(t('StockKeepingUnit.noti.deleteSuccess'));
        this.updatePageData();
        this.handleDialogClose()
      } else {
        handleThrowResponseMessage(res);
      }
    }).catch((err) => {
      toast.error(t('StockKeepingUnit.noti.deleteFail'));
    })
  };

  componentDidMount() {
    this.updatePageData()
    this.getRoleCurrentUser()
  }

  handleEditItem = item => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true
    });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenNotificationPopup,
      hasDeletePermission
    } = this.state;
    let TitlePage = t("StockKeepingUnit.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        maxWidth: "100px",
        cellStyle: {
          textAlign: 'center'
        },
        render: rowData => <MaterialButton item={rowData}
          hasDeletePermission={hasDeletePermission}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              getItemById(rowData.id).then(({ data }) => {
                if (data === null) {
                  data = {};
                }
                this.setState({
                  item: data,
                  shouldOpenEditorDialog: true
                });
              })
            } else if (method === appConst.active.delete) {
              this.handleDelete(rowData.id);
            } else {
              alert('Call Selected Here:' + rowData.id);
            }
          }}
        />
      },
      { title: t("StockKeepingUnit.code"), field: "code", },
      { title: t("StockKeepingUnit.name"), field: "name", align: "left", },

    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            { name: t("Dashboard.category"), path: "/list/stock_keeping_unit" },
            { name: TitlePage }]} />
        </div>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} xs={12} >
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleEditItem({ startDate: new Date(), endDate: new Date() });
              }
              }
            >
              {t('general.add')}
            </Button>
            {shouldOpenNotificationPopup && (
              <NotificationPopup
                title={t('general.noti')}
                open={shouldOpenNotificationPopup}
                // onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDialogClose}
                text={t(this.state.Notification)}
                agree={t('general.agree')}
              />
            )}
          </Grid>
          <Grid item md={6} sm={12} xs={12} >
            <FormControl fullWidth>
              <Input
                className='search_box w-100'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("StockKeepingUnit.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#"> <SearchIcon
                      onClick={() => this.search(keyword)}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0"
                      }} /></Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <StockKeepingUnitEditorDialog t={t} i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                />
              )}

              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                },
                toolbar: {
                  nRowsSelected: `${t('general.selects')}`
                }
              }}
              options={{
                sorting: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: '450px',
                minBodyHeight: this.state.itemList?.length > 0
                  ? 0 : 250,
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
StockKeepingUnitTable.contextType = AppContext
export default StockKeepingUnitTable;
