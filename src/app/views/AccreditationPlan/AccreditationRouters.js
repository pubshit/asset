import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetAllocationTable = EgretLoadable({
  loader: () => import("./AccreditationTable")
});
const ViewComponent = withTranslation()(AssetAllocationTable);

const AccreditationRouters = [
  {
    path: ConstantList.ROOT_PATH + "asset/planing",
    exact: true,
    component: ViewComponent
  }
];

export default AccreditationRouters;
