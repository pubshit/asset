import { Icon, IconButton } from "@material-ui/core";
import { appConst } from "app/appConst";
import { convertNumberPrice } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";

function MaterialButton(props) {
  const item = props?.item;
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => props?.onSelect(item, appConst.active.delete)}
      >
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

function ListAssetTable(props) {
  const t = props?.t;
  const formatType = (rowData) => {
        switch (rowData?.asset?.type) {
            case appConst.TYPE_KD_HC.KIEM_DINH.code:
                return t("VerificationCalibration.verification")
            case appConst.TYPE_KD_HC.KIEM_XA.code:
                return t("VerificationCalibration.gunnery_inspection")
            case appConst.TYPE_KD_HC.KIEM_DINH_KIEM_XA.code:
                return t("VerificationCalibration.verification_gunnery_inspection")
            default:
                break;
        }
    }
  let columns = [
    {
      title: t("general.stt"),
      field: "stt",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("Asset.name"),
      field: "asset.tsTen",
      align: "left",
      minWidth: 250,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      maxWidth: 150,
      minWidth: 120,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
        wordBreak: "break-word",
      },
      render: (rowData) => rowData?.asset?.tsMa || rowData?.asset?.code,
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.tsSerialNo",
      align: "center",
      minWidth: 75,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    {
      title: t("VerificationCalibration.type"),
      field: "type",
      minWidth: 150,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => formatType(rowData)
    },
    {
      title: t("Calibration.status"),
      field: "asset.kdTrangThaiText",
      minWidth: 150,
      align: "center",
    },
    {
      title: t("AssetType.type"),
      field: "asset.type",
      minWidth: 180,
      align: "center",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
      render: (rowData) =>
        rowData?.asset?.kdHinhThucText ? rowData?.asset?.kdHinhThucText : null,
    },
    {
      title: t("Asset.model"),
      field: "asset.tsModel",
      align: "left",
      minWidth: 50,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },

    {
      title: t("AssetType.cost"),
      field: "asset.kdChiPhi",
      align: "left",
      minWidth: 130,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(rowData?.asset?.kdChiPhi),
    },
    {
      title: t("Calibration.implementingAgencies"),
      field: "asset.kdDvThucHienText",
      minWidth: 250,
    },
    {
      title: t("Asset.manufacturerTable"),
      field: "asset.tsHangSx",
      minWidth: 100,
    },
    {
      title: t("Asset.country_manufacturer"),
      field: "asset.tsNuocSx",
      minWidth: 100,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
  ];
  return (
    <MaterialTable
      data={
        props?.itemAsset?.assetVouchers ? props?.itemAsset?.assetVouchers : []
      }
      columns={columns}
      options={{
        draggable: false,
        toolbar: false,
        selection: false,
        actionsColumnIndex: -1,
        paging: false,
        search: false,
        sorting: false,
        padding: "dense",
        rowStyle: (rowData) => ({
          backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
        }),
        headerStyle: {
          backgroundColor: "#358600",
          color: "#fff",
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        maxBodyHeight: "225px",
        minBodyHeight: "225px",
      }}
      localization={{
        body: {
          emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
        },
      }}
      components={{
        Toolbar: (props) => (
          <div style={{ witdth: "100%" }}>
            <MTableToolbar {...props} />
          </div>
        ),
      }}
      onSelectionChange={(rows) => {
        this.data = rows;
      }}
    />
  );
}

export default ListAssetTable;
