/* eslint-disable no-unused-expressions */
import {
  AppBar,
  Box,
  Icon,
  IconButton,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import { variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  convertFromToDate,
  formatDateDto,
  getTheHighestRole,
  handleThrowResponseMessage
} from "app/appFunction";
import { Breadcrumb } from "egret";
import FileSaver, { saveAs } from "file-saver";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import { appConst } from "../../appConst";
import {
  exportToExcel as assetAllocationExportToExcel,
  deleteItem,
  getItemById,
  searchByPage
} from "./AccreditationService";
import ComponentAccreditationAssetsTable from "./ComponentAccreditationAssetsTable";
import ListAssetTable from "./ListAssetTable";
import { checkInvalidDate } from "../../appFunction";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 9,
    position: "absolute",
    top: "-10px",
    left: "-25px",
    width: "80px",
  },
}))(Tooltip);

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const hasDeletePermission = props.hasDeletePermission;
  let hasEditPermission = props.hasEditPermission;
  const hasPrintPermission = props.hasPrintPermission;
  const hasSuperAccess = props.hasSuperAccess;
  const value = props.value;

  return (
    <div className="none_wrap">
      {hasEditPermission &&
        [
          appConst.listStatusCalibrationObject.MOI_TAO.indexOrder,
          appConst.listStatusCalibrationObject.DA_DUYET_DANH_MUC.indexOrder,
          appConst.listStatusCalibrationObject.DA_DUYET_BAO_GIA.indexOrder,
        ].includes(item?.trangThai) && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {(hasDeletePermission || hasSuperAccess) &&
        (hasSuperAccess
          ? hasSuperAccess
          : [appConst.listStatusCalibrationObject.MOI_TAO.indexOrder].includes(
            item?.trangThai
          )) && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasPrintPermission && (
        <LightTooltip
          title={t("In phiếu")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

class AccreditationTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetAllocation: [],
    item: {},
    shouldOpenPrint: false,
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenNotificationPopup: false,
    shouldOpenImportExcelDialog: false,
    listAssetDocumentId: [],
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    hasSuperAccess: false,
    tabValue: 0,
    isCheckReceiverDP: true,
    isCheckHandoverDP: true,
    itemAsset: {},
    openAdvanceSearch: false,
    listStatus: [],
    listHandoverDepartment: [],
    listReceiverDepartment: [],
    transferStatus: null,
    receiverDepartment: null,
    handoverDepartment: null,
    fromDate: null,
    fromToData: null,
    clone: null,
    ngayLap: null,
    ngayDuyetBaoGiaTu: null,
    ngayDuyetBaoGiaDen: null,
    ngayDuyetTu: null,
    ngayDuyetDen: null,
    trangThai: null,
    ngayLapTu: null,
    ngayLapDen: null
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Allocation; //cấp phát

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.tabAllocation?.tabAll === newValue) {
      return this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          allocationStatus: null,
          trangThai: "",
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabAllocation?.tabNew === newValue) {
      return this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          allocationStatus: null,
          trangThai: appConst.listStatusCalibrationObject.MOI_TAO.indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabAllocation?.tabWaitConfirmation === newValue) {
      return this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          allocationStatus: null,
          trangThai:
            appConst.listStatusCalibrationObject.DA_DUYET_DANH_MUC.indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabAllocation?.tabAllocated === newValue) {
      return this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          allocationStatus: null,
          trangThai:
            appConst.listStatusCalibrationObject.DA_DUYET_BAO_GIA.indexOrder,
        },
        () => this.updatePageData()
      );
    }
    if (appConst?.tabAllocation?.tabReturn === newValue) {
      return this.setState(
        {
          itemList: [],
          tabValue: newValue,
          keyword: "",
          allocationStatus: null,
          trangThai:
            appConst.listStatusCalibrationObject.DA_DUYET_KET_THUC.indexOrder,
        },
        () => this.updatePageData()
      );
    }
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.setState({ ...this.state });
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    let { handoverDepartment, trangThai } = this.state;
    let searchObject = {};

    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.type = appConst.TYPE_KD_HC.KIEM_DINH.code;
    searchObject.ngayLapTu = this.state?.ngayLapTu ? moment(this.state?.ngayLapTu).format("YYYY-MM-DDT00:00:00") : null;
    searchObject.ngayLapDen = this.state?.ngayLapDen ? moment(this.state?.ngayLapDen).format("YYYY-MM-DDT23:59:59") : null;
    searchObject.ngayDuyetBaoGiaTu = this.state?.ngayDuyetBaoGiaTu ? moment(this.state?.ngayDuyetBaoGiaTu).format("YYYY-MM-DDT00:00:00") : null;
    searchObject.ngayDuyetBaoGiaDen = this.state?.ngayDuyetBaoGiaDen ? moment(this.state?.ngayDuyetBaoGiaDen).format("YYYY-MM-DDT23:59:59") : null;
    searchObject.ngayDuyetTu = this.state?.ngayDuyetTu ? moment(this.state?.ngayDuyetTu).format("YYYY-MM-DDT00:00:00") : null;
    searchObject.ngayDuyetDen = this.state?.ngayDuyetDen ? moment(this.state?.ngayDuyetDen).format("YYYY-MM-DDT23:59:59") : null;
    if (this.state?.keyword) {
      searchObject.keyword = this.state?.keyword?.trim();
    }
    if (this.state?.ngayLap) {
      searchObject.ngayLap = formatDateDto(this.state?.ngayLap);
    }
    if (this.state?.trangThai) {
      searchObject.trangThai = this.state?.trangThai;
    }
    if (handoverDepartment) {
      searchObject.phongBanId = handoverDepartment?.id;
    }

    setPageLoading(true);
    searchByPage(searchObject)
      .then(({ data }) => {
        this.setState(
          {
            itemList: [...data?.data?.content],
            totalElements: data?.data?.totalElements,
            itemAsset: {},
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
    this.getCountStatus();
  };
  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };
  getCountStatus = () => {
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenPrint: false,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
    });
    this.updatePageData();
    this.getCountStatus();
  };

  handleDeleteAssetAllocation = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    deleteItem(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(t("general.deleteSuccess"));
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          } else {
            this.updatePageData();
          }
          this.handleDialogClose();
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    this.setState(
      {
        statusIndex: null,
      },
      // () => this.updatePageData()
    );
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasCreatePermission,
      hasSuperAccess,
      receiverDepartment,
    } = this.state;
    let roles = getTheHighestRole();
    let {
      isRoleAssetUser,
      isRoleAssetManager,
      isRoleAccountant,
      isRoleAdmin,
      isRoleOrgAdmin,
      departmentUser,
    } = roles;

    // người quản lý vật tư
    if (isRoleAssetManager) {
      if (hasDeletePermission !== true) {
        hasDeletePermission = true;
      }
      if (hasEditPermission !== true) {
        hasEditPermission = true;
      }
      if (hasPrintPermission !== true) {
        hasPrintPermission = true;
      }
      if (hasCreatePermission !== true) {
        hasCreatePermission = true;
      }
    }
    // Kế toán
    if (isRoleAccountant) {
      if (hasDeletePermission === true) {
        hasDeletePermission = false;
      }
      if (hasEditPermission === true) {
        hasEditPermission = false;
      }
      if (hasPrintPermission !== true) {
        hasPrintPermission = true;
      }
      if (hasCreatePermission === true) {
        hasCreatePermission = false;
      }
    }
    // người đại diện phòng ban
    if (isRoleAssetUser) {
      receiverDepartment = departmentUser;
      if (hasDeletePermission !== true) {
        hasDeletePermission = true;
      }
      if (hasEditPermission !== true) {
        hasEditPermission = true;
      }
      if (hasPrintPermission !== true) {
        hasPrintPermission = false;
      }
      if (hasCreatePermission !== true) {
        hasCreatePermission = false;
      }
    }
    // admin hoặc supper_admin
    if (isRoleOrgAdmin || isRoleAdmin) {
      if (hasDeletePermission !== true) {
        hasDeletePermission = true;
      }
      if (hasEditPermission !== true) {
        hasEditPermission = true;
      }
      if (hasCreatePermission !== true) {
        hasCreatePermission = true;
      }
      if (hasPrintPermission !== true) {
        hasPrintPermission = true;
      }
      if (hasSuperAccess !== true) {
        hasSuperAccess = true;
      }
    }

    this.setState({
      ...roles,
      hasSuperAccess,
      hasEditPermission,
      receiverDepartment,
      hasPrintPermission,
      hasDeletePermission,
      hasCreatePermission,
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      clone: item,
      shouldOpenEditorDialog: true,
    });
  };

  handleClick = (event, item) => {
    let { AssetAllocation } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetAllocation.length; i++) {
      if (
        AssetAllocation[i].checked === null ||
        AssetAllocation[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (AssetAllocation[i].id === item.id) {
        AssetAllocation[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem,
      AssetAllocation: AssetAllocation,
    });
  };

  handleSelectAllClick = (event) => {
    let { AssetAllocation } = this.state;
    for (var i = 0; i < AssetAllocation.length; i++) {
      AssetAllocation[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem,
      AssetAllocation: AssetAllocation,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handlePrintAll = async (rowData) => {
    let items = await this.convertDataDto(rowData);
    this.setState({
      item: items,
      shouldOpenPrint: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props;
    this.handleDeleteList(this.data)
      .then(() => {
        this.updatePageData();
        this.handleDialogClose();
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  /* Export to excel */
  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate,
      currentUser,
      allocationStatus,
      originalCostTo,
      originalCostFrom,
      handoverDepartment,
      receiverDepartment,
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.TSCD,
      };
      if (handoverDepartment) {
        searchObject.handoverDepartmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiverDepartmentId = receiverDepartment?.id;
      }
      if (allocationStatus) {
        searchObject.statusIndex = allocationStatus?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }
      if (originalCostFrom) {
        searchObject.originalCostFrom = Number(originalCostFrom);
      }
      if (originalCostTo) {
        searchObject.originalCostTo = Number(originalCostTo);
      }

      const res = await assetAllocationExportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "AssetAllocation.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  handleNotificationPopup = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  setStateAllocation = (item) => {
    this.setState(item);
  };

  convertDataDto = async (rowData) => {
    let { setPageLoading } = this.context;
    let { t } = this.props
    setPageLoading(true);
    try {
      let res = await getItemById(rowData?.id);
      if (res?.data?.code === appConst?.CODE?.SUCCESS) {
        let { data } = res
        setPageLoading(false);
        let allocation = data?.data ? data?.data : {};

        allocation.clone = allocation?.kdTaiSans?.map((i) => {
          return { ...i };
        });
        allocation.trangThai = appConst.listStatuscalibration.find(
          (i) => i.code === allocation?.trangThai
        );
        allocation.statusIndex = allocation.trangThai;

        allocation.handoverDepartment = {
          id: allocation?.phongBanId,
          name: allocation?.phongBanText,
        };

        allocation.assetVouchers = allocation?.kdTaiSans?.map((items) => {
          let data = {
            asset: {
              ...items,
              kdHinhThucObject: appConst.OPTION_TYPE_KH_KIEM_DINH.find(
                (i) => i.code === items?.kdHinhThuc
              ),
              kdTrangThai: appConst.listStatusAssetsKD.find(
                (i) => i.code === items?.kdTrangThai
              ),

              kdDvThucHien: items?.kdDvThucHienId
                ? {
                  id: items.kdDvThucHienId,
                  name: items.kdDvThucHienText,
                }
                : null,
              id: items?.id,
            },
            assetId: items?.tsId,
          };
          return (items.asset = data);
        });
        return allocation;
      }
      else {
        handleThrowResponseMessage(res)
      }

    } catch (error) {
      toast.error(t("general.error"))
    }

  };

  handleEdit = async (rowData) => {
    let convertItemData = await this.convertDataDto(rowData);
    this.setState({
      item: convertItemData,
      clone: convertItemData,
      shouldOpenEditorDialog: true,
      isVisibility: false,
      isStatus: true,
    });
  };

  handleViews = async (rowData) => {
    let convertItemData = await this.convertDataDto(rowData);
    this.setState({
      item: convertItemData,
      clone: convertItemData,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusAllocation.find(
      (item) => item.indexOrder === status
    );
    switch (status) {
      case appConst.listStatusCalibrationObject.MOI_TAO.indexOrder:
        return (
          <div
            style={{
              maxWidth: 180,
            }}
            className="status status-warning"
          >
            {appConst.listStatusCalibrationObject.MOI_TAO.name}
          </div>
        );
      case appConst.listStatusCalibrationObject.DA_DUYET_DANH_MUC.indexOrder:
        return (
          <div
            style={{
              maxWidth: 180,
            }}
            className="status status-info"
          >
            {appConst.listStatusCalibrationObject.DA_DUYET_DANH_MUC.name}
          </div>
        );
      case appConst.listStatusCalibrationObject.DA_DUYET_BAO_GIA.indexOrder:
        return (
          <div
            style={{
              maxWidth: 180,
            }}
            className="status status-success"
          >
            {appConst.listStatusCalibrationObject.DA_DUYET_BAO_GIA.name}
          </div>
        );
      case appConst.listStatusCalibrationObject.DA_DUYET_KET_THUC.indexOrder:
        return (
          <div
            style={{
              maxWidth: 180,
            }}
            className="status status-error"
          >
            {appConst.listStatusCalibrationObject.DA_DUYET_KET_THUC.name}
          </div>
        );
      default:
        break;
    }
    return itemStatus?.name;
  };

  handleSetItemState = async (rowData) => {
    let convertItemData = await this.convertDataDto(rowData);
    this.setState({
      itemAsset: convertItemData,
    });
  };

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    openAdvanceSearch && this.setState({
      ngayDuyetTu: null, ngayDuyetDen: null,
      ngayDuyetBaoGiaTu: null,
      ngayDuyetBaoGiaDen: null,
      ngayLapTu: null,
      ngayLapDen: null,
      handoverDepartment: null
    }, () => this.updatePageData())
    this.setState({ openAdvanceSearch: !openAdvanceSearch, });
  };

  handleSetDataSelect = (data, name, source) => {
    if ([
      "ngayLapTu",
      "ngayLapDen",
      "ngayDuyetTu",
      "ngayDuyetDen",
      "ngayDuyetBaoGiaTu",
      "ngayDuyetBaoGiaDen"
    ].includes(name)) {
      if (!checkInvalidDate(data)) {
        this.setState({ [name]: data }, () => {
          this.search();
        });
      }
      return
    }
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.originalCostFrom !== name &&
        variable.listInputName.originalCostTo !== name &&
        variable.listInputName.listData !== source &&
        !this.state.statusIndex
      ) {
        this.search();
      }
    });
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      hasPrintPermission,
      hasSuperAccess,
      tabValue,
    } = this.state;
    let TitlePage = t("Verification.planing");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasPrintPermission={hasPrintPermission}
            hasSuperAccess={hasSuperAccess}
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrintAll(rowData);
              } else if (appConst.active.view === method) {
                this.handleViews(rowData);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Verification.issueDate"),
        field: "ngayLap",
        align: "left",
        minWidth: 130,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.ngayLap ? (
            <span>{moment(rowData.ngayLap).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "trangThai",
        minWidth: 180,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.trangThai),
      },
      {
        title: t("Verification.code"),
        field: "khMa",
        minWidth: 150,
        align: "center",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Verification.name"),
        field: "khTen",
        minWidth: "200px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Calibration.performingDepartment"),
        field: "phongBanText",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    let columnsNoAction = [
      {
        title: t("general.action"),
        field: "custom",
        width: "150px",
        minWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <LightTooltip
            title={t("general.viewIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton size="small" onClick={() => this.handleViews(rowData)}>
              <Icon fontSize="small" color="primary">
                visibility
              </Icon>
            </IconButton>
          </LightTooltip>
        ),
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("AssetTransfer.issueDate"),
        field: "issueDate",
        align: "left",
        minWidth: 110,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "allocationStatusIndex",
        minWidth: 150,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => this.checkStatus(rowData?.allocationStatusIndex),
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPersonName",
        minWidth: "200px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartmentName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        minWidth: "250px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/allocation-vouchers",
              },
              { name: t("Verification.planing") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("Calibration.tab.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("Calibration.tab.new")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("Calibration.tab.approved")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("Calibration.tab.approvedCost")}</span>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("Calibration.tab.approvedEnd")}</span>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst?.tabCalibration?.tabAll}
          className="mp-0"
        >
          <ComponentAccreditationAssetsTable
            t={t}
            i18n={i18n}
            item={this.state}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabCalibration?.tabNew}
          className="mp-0"
        >
          <ComponentAccreditationAssetsTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabCalibration?.tabApprovedList}
          className="mp-0"
        >
          <ComponentAccreditationAssetsTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabCalibration?.tabAllocatedCost}
          className="mp-0"
        >
          <ComponentAccreditationAssetsTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleSetDataSelect={this.handleSetDataSelect}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabCalibration?.tabEnd}
          className="mp-0"
        >
          <ComponentAccreditationAssetsTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
          <ListAssetTable itemAsset={this.state.itemAsset} {...this.props} />
        </TabPanel>
      </div>
    );
  }
}
AccreditationTable.contextType = AppContext;
export default AccreditationTable;
