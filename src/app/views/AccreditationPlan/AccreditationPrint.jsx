import React from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { convertNumberPriceRoundUp } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import Title from "../FormCustom/Component/Title";
import NationalName from "../FormCustom/Component/NationalName";
import DateToText from "../FormCustom/Component/DateToText";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontWeight: "bold",
    marginBottom: "0px",
  },
  text: {
    fontWeight: "bold",
    marginTop: "0px",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: "bold",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_3: {
    border: "1px solid",
    width: "3%",
    textAlign: "center",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
  },
  w_9: {
    border: "1px solid",
    width: "9%",
    textAlign: "center",
  },
  w_11: {
    border: "1px solid",
    width: "11%",
  },
  w_20: {
    border: "1px solid",
    width: "20%",
  },
  border: {
    border: "1px solid",
    padding: "0 5px",
    fontSize: "0.975rem",
  },
  name: {
    border: "1px solid",
    paddingLeft: "5px",
    textAlign: "left",
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "left",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 12%",
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: "flex",
    justifyContent: "space-between",
    textTransform: "uppercase",
  },
  textCenter: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    width: "33%",
  },
  marginTop25minus: {
    marginTop: -25,
  },
  pt_30: {
    paddingTop: 30,
  },
  pt_40: {
    paddingTop: 40,
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
  }
};

class AccreditationPrint extends React.Component {
  state = {
    AssetAllocation: [],
    item: {},
    asset: {},
    assetVouchers: [],
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    kdTaiSans: [],
  };

  componentWillMount() {
    this.setState({
      ...this.props.item,
      kdTaiSans: this.props?.item?.kdTaiSans,
    });
  }

  componentDidMount() {}

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenViewDialog: false,
      shouldOpenExportDialog: false,
    });
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  render() {
    const { t, item } = this.props;
    let { open } = this.props;
    let now = new Date(this.state?.issueDate);
    let currentUser = localStorageService.getSessionItem("currentUser");
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("Phiếu kế hoạch kiểm định")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
        ></iframe>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          class="validator-form-scroll-dialog"
        >
          <DialogContent id="divcontents" style={{}} className="dialog-print">
            <div style={{ ...style.a4Container }} className="form-print">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <div style={{ ...style.title }}>{currentUser?.org?.name}</div>
                </div>
                <div>
                  <NationalName />
                </div>
              </div>

              <div>
                <div style={{ textAlign: "center" }}>
                  <Title title={"PHIẾU KẾ HOẠCH KIỂM ĐỊNH"} />
                  <div style={{fontStyle: "italic"}}>Số phiếu: {item?.khMa}</div>
                </div>
                <div style={{ textAlign: "center" }}>
                  <DateToText isLocation noUppercase date={this.state?.ngayLap} />
                </div>
                <p style={style.text}></p>
              </div>

              {
                <div>
                  <div style={style.textAlignLeft}>
                    Căn cứ Quyết định số: ....... ngày ...... tháng ...... năm
                    ...... của ..................
                  </div>
                  <div style={style.textAlignLeft}>
                    <ul mr-4>
                      <li>
                        <div>
                          Phòng ban thực hiện: {item?.handoverDepartment?.name}.
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              }
              <div>
                {
                  <table style={style.table} className="table-report">
                    <tr>
                      <th style={style.w_3}>STT</th>
                      <th style={style.w_11}>Mã tài sản</th>
                      <th style={style.w_20}>Tên tài sản</th>
                      <th style={style.w_5}>Model</th>
                      <th style={style.w_5}>Serial</th>
                      <th style={style.w_5}>Hãng SX</th>
                      <th style={style.w_5}>Nước SX</th>
                      <th style={style.w_5}>Năm SX</th>
                      <th style={style.w_5}>Năm SD</th>
                      <th style={style.w_5}>SL</th>
                      <th style={style.w_20}>Người tiếp nhận</th>
                      <th style={style.w_9}>Chi phí</th>
                    </tr>

                    {this.state?.kdTaiSans &&
                      this.state?.kdTaiSans?.map((row, index) => {
                        return (
                          <tr>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {index + 1 || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.tsMa || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.tsTen || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.tsModel || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.tsSerialNo || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.manufacturerName || row?.tsHangSx || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {row?.tsNuocSx || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {row?.tsNamSx || ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {row?.asset?.yearPutIntoUse ||
                                row?.assetYearPutIntoUser ||
                                row?.tsNamSd ||
                                ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignCenter,
                              }}
                            >
                              {row?.quantity ? row?.quantity : 1}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                              }}
                            >
                              {this.state.receiverPersonName
                                ? this.state.receiverPersonName
                                : ""}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignRight,
                              }}
                            >
                              {convertNumberPriceRoundUp(
                                row?.kdChiPhi || 0
                              )}
                            </td>
                          </tr>
                        );
                      })}
                    <tr>
                      <td
                        colSpan={9}
                        style={{
                          ...style.border,
                          ...style.textAlignLeft,
                          ...style.forwarder,
                        }}
                      >
                        Tổng cộng:
                      </td>
                      <td
                        colSpan={1}
                        style={{ ...style.border, ...style.textAlignCenter }}
                      >
                        {this.state?.assetVouchers?.length}
                      </td>
                      <td
                        colSpan={1}
                        style={{ ...style.border, ...style.textAlignLeft }}
                      ></td>
                      <td
                        colSpan={1}
                        style={{ ...style.border, ...style.textAlignRight }}
                      >
                        {this.state.assetVouchers &&
                          convertNumberPriceRoundUp( item?.tongChiPhi || item?.kdTaiSans?.reduce((acc, cur)=> acc + cur?.kdChiPhi??0,0) ) || 0
                        }
                      </td>
                    </tr>
                  </table>
                }
              </div>
              <div>
                <div style={style.signContainer}>
                  <div style={{ ...style.textCenter }}>
                    <p>Lãnh đạo</p>
                  </div>
                  <div style={{ ...style.textCenter }}>
                    <p>Bên giao</p>
                  </div>
                  <div style={{ ...style.textCenter }}>
                    <p>Bên nhận</p>
                  </div>
                </div>
              </div>
              <div style={style.marginTop25minus}>
                <div style={style.signContainer}>
                  <div
                    style={{
                      ...style.textCenter,
                      ...style.pt_40,
                    }}
                  >
                    <p></p>
                  </div>
                  <div
                    style={{
                      ...style.textCenter,
                      ...style.pt_40,
                    }}
                  >
                    <p>{this.state?.handoverPersonName}</p>
                  </div>
                  <div
                    style={{
                      ...style.textCenter,
                      ...style.pt_40,
                    }}
                  >
                    <p>{this.state?.receiverPersonName}</p>
                  </div>
                </div>
              </div>
            </div>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle mr-16">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("In")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AccreditationPrint;
