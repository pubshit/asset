import moment from "moment";
import {convertNumberPriceRoundUp} from "../../appFunction";
import { TextValidator } from "react-material-ui-form-validator";
import { appConst } from "app/appConst";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {searchByPage as searchByPagePurchasePlaning} from "../SuppliesPurchasePlaning/SuppliesPurchasePlaningService";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {NumberFormatCustom} from "app/views/Component/Utilities";
import React from "react";

export const COLUMNS_RECEIVING = (t) => [
  {
    title: t("general.index"),
    field: "stt",
    minWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("receivingAsset.code"),
    field: "voucherCode",
    minWidth: 140,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.status"),
    field: "status",
    minWidth: 160,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      if (
        rowData?.status === appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder
      ) {
        return (
          <span className="status status-warning">
            {appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.name}
          </span>
        );
      } else if (
        rowData?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder
      ) {
        return (
          <span className="status status-info">
            {appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.name}
          </span>
        );
      }
      return "";
    },
  },
  {
    title: t("receivingAsset.decisionCode"),
    field: "decisionCode",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    minWidth: 120,
  },
  {
    title: t("receivingAsset.contractNumber"),
    field: "billNumber",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    minWidth: 120,
  },
  {
    title: t("receivingAsset.contractDate"),
    field: "",
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    minWidth: 120,
    render: (rowData) =>
      rowData?.billDate ? moment(rowData?.billDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("Reception.issueDate"),
    field: "issueDate",
    minWidth: 140,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) =>
      rowData.issueDate ? moment(rowData.issueDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("Contract.code"),
    field: "contractCode",
    align: "left",
    minWidth: 150,
  },
  {
    title: t("ReceivingAsset.supplier"),
    field: "supplierName",
    align: "left",
    minWidth: 400,
  },
  {
    title: t("allocation_asset.receiverDepartment"),
    field: "receiverDepartmentName",
    minWidth: 250,
  },
  {
    title: t("Asset.note"),
    field: "note",
    minWidth: 400,
  },
];

export const fixedAssetColumns = (t, props) => [
  {
    title: t("Reception.columns.index"),
    field: "",
    minWidth: "40px",
    align: "center",
    render: (rowData) =>
      props.item?.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
  },
  {
    title: t("Reception.columns.assetName"),
    field: "asset.name",
    align: "left",
    minWidth: "200px",
  },
  {
    title: t("Asset.managementCode"),
    field: "asset.managementCode",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="managementCode"
          className="w-100"
          value={rowData?.asset?.managementCode || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("Reception.columns.serialNumber"),
    field: "asset.serialNumber",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="serialNumber"
          className="w-100"
          value={rowData?.asset?.serialNumber || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("Asset.healthInsuranceCode"),
    field: "asset.bhytMaMay",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="bhytMaMay"
          className="w-100"
          value={rowData?.asset?.bhytMaMay || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
          disabled={!rowData?.asset?.shouldGenerateBhytMaMay}
        />
      );
    },
  },
  {
    title: t("Reception.columns.symbolCode"),
    field: "asset.symbolCode",
    align: "center",
    minWidth: "130px",
  },
  {
    title: t("Reception.columns.model"),
    field: "asset.model",
    align: "center",
    minWidth: "130px",
  },
  {
    title: t("Asset.stockKeepingUnitTable"),
    field: "sku.name",
    align: "center",
    minWidth: 120,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
    },render: (rowData) =>
      rowData?.asset?.unit?.name || rowData?.asset?.unitName,
  },
  {
    title: t("Asset.yearOfManufacture"),
    field: "asset.yearOfManufacture",
    align: "left",
    minWidth: 120,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: "center",
    },
  },
  {
    title: t("Asset.manufacturer"),
    field: "asset.manufacturerName",
    align: "left",
    minWidth: 150,
    maxWidth: 200,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    render: (rowData) =>
      rowData?.asset?.manufacturer?.name || rowData?.asset?.manufacturerName,
  },
  {
    title: t("Reception.columns.originalCost"),
    field: "asset.originalCost",
    align: "right",
    minWidth: "150px",
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) =>
      rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
        : "",
  },
  {
    title: t("Reception.columns.note"),
    field: "asset.note",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="note"
          className="w-100"
          value={rowData?.note || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
];

export const iatColumns = (t, props) => [
  {
    title: t("InstrumentsToolsReception.columns.index"),
    field: "",
    minWidth: "40px",
    align: "center",
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("InstrumentsToolsReception.columns.assetName"),
    field: "asset.name",
    align: "left",
    minWidth: "200px",
  },
  {
    title: t("Asset.managementCode"),
    field: "asset.managementCode",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="managementCode"
          className="w-100"
          value={rowData?.asset?.managementCode || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.serialNumber"),
    field: "asset.serialNumber",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="serialNumber"
          className="w-100"
          value={rowData?.asset?.serialNumber || ""}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
          disabled={rowData?.asset?.quantity > 1}
        />
      );
    },
  },
  {
    title: t("InstrumentsToolsReception.columns.model"),
    field: "asset.model",
    align: "center",
    minWidth: "130px",
  },
  // {
  //   title: t("InstrumentsToolsReception.columns.symbolCode"),
  //   field: "asset.symbolCode",
  //   align: "center",
  //   minWidth: "130px",
  // },
  {
    title: t("InstrumentsToolsReception.quantity"),
    field: "asset.quantity",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.quantity
        ? convertNumberPriceRoundUp(rowData?.asset?.quantity)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("Asset.unitPrice"),
    field: "asset.unitPrice",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.unitPrice
        ? convertNumberPriceRoundUp(rowData?.asset?.unitPrice)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("InstrumentsToolsReception.amount"),
    field: "asset.amount",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("InstrumentsToolsReception.columns.note"),
    field: "asset.note",
    align: "left",
    minWidth: "200px",
  },
];

export const suppliesColumns = (t, props) => [
  {
    title: t("InventoryReceivingVoucher.stt"),
    field: "",
    width: "80px",
    headerStyle: {
      textAlign: "center",
    },
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
  },
  {
    title: t("Product.code"),
    field: "product.code",
    align: "center",
    minWidth: "180px",
    render: (rowData) => rowData?.product?.code || rowData?.productCode
  },
  {
    title: t("Product.name"),
    field: "product.name",
    align: "left",
    minWidth: "180px",
    cellStyle: {
      textAlign: "left",
    },
    render: (rowData) => rowData?.product?.name || rowData?.productName
  },
  {
    title: "ĐVT",
    align: "center",
    minWidth: "30px",
    cellStyle: {
      textAlign: "center",
    },
    field: "sku.name",
    render: (rowData) =>
      rowData.skus ? (
        <Autocomplete
          options={rowData.skus}
          defaultValue={rowData.sku}
          value={rowData.sku}
          getOptionLabel={(option) => option?.name || option?.skuName}
          onChange={(event, newValue) => {
            props.selectSku(newValue, rowData);
          }}
          renderInput={(params) => <TextValidator
            {...params}
            value={rowData.sku || null}
            validators={["required"]}
            errorMessages={[t("general.required")]}
            InputProps={{
              readOnly: props?.item?.isView,
            }}
          />}
          closeIcon={<></>}
        />
      ) : (
        <label>{rowData.sku?.name}</label>
      ),
  },
  {
    title: t("Product.purchasePlaning"),
    minWidth: "140px",
    align: "left",
    render: (rowData) => (
      <AsynchronousAutocompleteSub
        searchFunction={searchByPagePurchasePlaning}
        searchObject={props.searchObjectPurchasePlaning}
        typeReturnFunction="category"
        noOptionsText={"no data"}
        displayLable={"name"}
        value={rowData.purchasePlaning}
        onSelect={(value) => props.selectPurchasePlaning(value, rowData)}
        onSelectOptions={rowData}
        readOnly={props?.item?.isView}
      />
    ),
  },
  {
    title: t("Product.quantityOfVoucher"),
    field: "quantityOfVoucher",
    minWidth: 100,
    render: (rowData) => (
      <TextValidator
        type="text"
        onChange={(event) => props.handleChangeSupplyField(rowData, event, "quantityOfVoucher")}
        id="formatted-numberformat-carryingAmount"
        InputProps={{
          inputComponent: NumberFormatCustom,
          inputProps: {
            style: { textAlign: "center", minWidth: "50px" },
          },
          readOnly: props?.item?.isView,
        }}
        value={rowData.quantityOfVoucher || ""}
        name="quantityOfVoucher"
        validators={props.item?.allowSubmit ? ["minFloat:0.000000001", "required", "matchRegexp:^\\d+(\\.\\d{1,2})?$"] : []}
        errorMessages={[t("general.minNumberError"), t("general.required"), t("general.quantityError")]}
      />
    ),
  },
  {
    title: t("Product.quantity"),
    field: "quantity",
    minWidth: 100,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => (
      <TextValidator
        type="text"
        onChange={(event) => props.handleChangeSupplyField(rowData, event, "quantity")}
        id="formatted-numberformat-carryingAmount"
        InputProps={{
          inputComponent: NumberFormatCustom,
          inputProps: {
            style: { textAlign: "center", minWidth: "50px" },
          },
          readOnly: props?.item?.isView,
        }}
        name="quantity"
        validators={props.item?.allowSubmit ? ["minFloat:0.000000001", "required", "matchRegexp:^\\d+(\\.\\d{1,2})?$"] : []}
        errorMessages={[t("general.minNumberError"), t("general.required"), t("general.quantityError")]}
        value={rowData.quantity || ""}
      />
    ),
  },
  {
    title: t("InventoryReceivingVoucher.batchCode"),
    field: "batchCode",
    minWidth: "120px",
    align: "left",
    render: (rowData) => (
      <TextValidator
        onChange={(event) => props.handleChangeSupplyField(rowData, event, "batchCode")}
        name="batchCode"
        InputProps={{
          inputProps: {
            style: { textAlign: "center", minWidth: "120px" },
          },
          readOnly: props?.item?.isView,
        }}
        value={rowData.batchCode || ""}
      />
    ),
  },
  {
    title: t("InventoryReceivingVoucher.expiryDate"),
    field: "expiryDate",
    minWidth: "110px",
    align: "left",
    render: (rowData) => (
      <CustomValidatePicker
        fullWidth
        margin="none"
        id="mui-pickers-date"
        inputVariant="standard"
        type="text"
        autoOk={false}
        format="dd/MM/yyyy"
        name={"expiryDate"}
        value={rowData.expiryDate || null}
        invalidDateMessage={t("general.invalidDateFormat")}
        onChange={(expiryDate) =>
          props.handleChangeSupplyField(rowData, expiryDate, "expiryDate")
        }
        InputProps={{
          inputProps: {
            style: { textAlign: "center", minWidth: "80px" },
          },
        }}
        readOnly={props?.item?.isView}
        disabled={props?.item?.isView}
      />
    ),
  },
  {
    title: t("Product.price"),
    field: "price",
    minWidth: "130px",
    render: (rowData) => (
      <TextValidator
        onChange={(event) => props.handleChangeSupplyField(rowData, event, "price")}
        name="price"
        id="formatted-numberformat-originalCost"
        InputProps={{
          inputComponent: NumberFormatCustom,
          inputProps: {
            className: "text-align-right",
          },
          readOnly: props?.item?.isView,
        }}
        validators={props.item?.allowSubmit ? ["minNumber:0"] : []}
        value={rowData.price || ""}
      />
    ),
  },
  {
    title: t("Product.amount"),
    field: "amount",
    align: "left",
    cellStyle: {
      textAlign: "right",
    },
    minWidth: 130,
    render: (rowData) => (
      <TextValidator
        name="amount"
        id="formatted-numberformat-originalCost"
        InputProps={{
          readOnly: true,
          inputComponent: NumberFormatCustom,
          inputProps: {
            style: { textAlign: "right", minWidth: "100px" },
            className: "text-align-right",
          },
        }}
        validators={["minNumber:0"]}
        value={rowData.amount || ""}
      />
    ),
  },
];
