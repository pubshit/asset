import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from "react-i18next";
import { ROUTES_PATH } from "app/appConst";
const ReceptionTable = EgretLoadable({
  loader: () => import("./ReceptionTable"),
});
const ViewComponent = withTranslation()(ReceptionTable);

const ReceptionRoutes = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.COMPREHENSIVE_MANAGEMENT.RECEPTION,
    exact: true,
    component: ViewComponent,
  },
];

export default ReceptionRoutes;
