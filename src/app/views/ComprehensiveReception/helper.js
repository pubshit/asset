
import {
    formatDateDto,
  } from "../../appFunction";
  
  export const convertAssetToFlattenItem = (item, state) => {     
    let { listIat = [], indexEdit } = state;
    const newArr = [...listIat];
    let assetSourcesUpdated = item?.assetSources?.map((item) => {
      item.assetSourceId = item?.assetSource?.id;
      item.assetSourceName = item?.assetSource?.name;
      return item;
    });
    
    if (indexEdit >= 0) {
      let index = newArr.findIndex((x) => x?.tableData?.id === indexEdit);
      newArr[index] = {
        ...newArr[index],
        asset: {
          ...item,
          ...newArr[index],
        },
      }
    } else {
      let data = {
        asset: {
          ...item,
          accumulatedDepreciationAmount: item?.accumulatedDepreciationAmount,
          allocationPeriod: item?.allocationPeriod,
          allocationPeriodValue: item?.allocationPeriodValue,
          assetGroupId: item?.assetGroupId
            ? item?.assetGroupId
            : item?.assetGroup?.id,
          assetSources: assetSourcesUpdated,
          attributes: item?.attributes,
          carryingAmount: Number(item?.carryingAmount),
          circulationNumber: item?.circulationNumber,
          code: item?.code,
          countryOriginId: item?.countryOriginId
            ? item?.countryOriginId
            : item?.countryOrigin?.id,
          contract: item?.contract,
          contractId: item?.contract?.id,
          contractDate: item?.contract?.contractDate
            ? formatDateDto(item?.contract?.contractDate)
            : null,
          supplyUnit: item?.contract?.supplier,
          dateOfReception: item?.dateOfReception
            ? formatDateDto(item?.dateOfReception)
            : null,
          dayStartedUsing: item?.dayStartedUsing
            ? formatDateDto(item?.dayStartedUsing)
            : null,
          depreciationDate: item?.depreciationDate
            ? formatDateDto(item?.depreciationDate)
            : null,
          depreciationPeriod: item?.depreciationPeriod,
          depreciationRate: item?.depreciationRate,
          installationLocation: item?.installationLocation,
          invoiceNumber: item?.invoiceNumber,
          isBuyLocally: item?.isBuyLocally,
          isManageAccountant: item?.isManageAccountant,
          isTemporary: item?.isTemporary,
          listAssetDocumentId: item?.listAssetDocumentId,
          lotNumber: item?.lotNumber,
          madeIn: item?.madeIn,
          managementCode: item?.managementCode,
          managementDepartmentId: item?.managementDepartmentId
            ? item?.managementDepartmentId
            : item?.managementDepartment?.id,
          manufacturerId: item?.manufacturerId
            ? item?.manufacturerId
            : item?.manufacturer?.id,
          medicalEquipmentId: item?.medicalEquipmentId
            ? item?.medicalEquipmentId
            : item?.medicalEquipment?.id,
          model: item?.model,
          name: item?.name,
          ngayHoaDon: item?.ngayHoaDon,
          note: item?.note,
          numberOfAllocations: item?.numberOfAllocations,
          numberOfAllocationsRemaining: item?.numberOfAllocationsRemaining,
          originalCost: item?.originalCost,
          productId: item?.productId ? item?.productId : item?.product?.id,
          qrcode: item?.qrcode,
          quantityAsset: item?.quantityAsset,
          quantity: item?.quantity,
          receiverPersonId: item?.receiverPersonId
            ? item?.receiverPersonId
            : item?.receiverPerson?.id,
          riskClassification: item?.riskClassification,
          serialNumber: item?.serialNumber,
          serialNumbers: item?.serialNumbers,
          shoppingFormId: item?.shoppingFormId
            ? item?.shoppingFormId
            : item?.shoppingForm?.id,
          storeId: item?.storeId ? item?.storeId : item?.store?.id,
          supplyUnitId: item?.supplyUnitId
            ? item?.supplyUnitId
            : item?.supplyUnit?.id,
          unitId: item?.unitId ? item?.unitId : item?.unit?.id,
          unitPrice: item?.unitPrice,
          useDepartmentId: item?.useDepartmentId
            ? item?.useDepartmentId
            : item?.useDepartment?.id,
          useDepartmentLateId: item?.useDepartmentLateId
            ? item?.useDepartmentLateId
            : item?.useDepartmentLate?.id,
          usePersonId: item?.usePersonId || item?.usePerson?.personId,
          usedTime: item?.usedTime,
          warrantyExpiryDate: item?.warrantyExpiryDate
            ? formatDateDto(item?.warrantyExpiryDate)
            : null,
          warrantyMonth: item?.warrantyMonth,
          yearOfManufacture: item?.yearOfManufacture,
          yearPutIntoUse: item?.yearPutIntoUse,
          acceptanceDate: item?.acceptanceDate
            ? formatDateDto(item?.acceptanceDate)
            : null,
          // decisionCode: item?.decisionCode ? item?.decisionCode : null,
          decisionCode: null,
          symbolCode: item?.symbolCode,
        },
        assetGroupId: item?.assetGroupId,
        managementDepartmentId: item?.managementDepartmentId
          ? item?.managementDepartmentId
          : item?.managementDepartment?.id,
        name: item?.name,
        productId: item?.productId ? item?.productId : item?.product?.id,
        quantity: item?.quantity,
        originalCost: item?.originalCost
      };
      newArr.push(data);
    }

    return newArr;
  };
  
  export const convertSelectedAssets = (item, state) => {
    let { listFixedAsset = [], indexEdit } = state;
    const newArr = [...listFixedAsset];
    const isBhytMaMay = Boolean(item?.shouldGenerateBhytMaMay);
    
    if (indexEdit >= 0) {
      let index = newArr.findIndex((x) => x?.tableData?.id === indexEdit);
      newArr[index] = {
        ...newArr[index],
        asset: {
          ...newArr[index]?.asset,
          ...item,
          shouldGenerateBhytMaMay: isBhytMaMay,
          bhytMaMay: isBhytMaMay ? item?.isBhytMaMay : "",
        },
      }
    } else {
      let data = {
        asset: {
          ...item,
          shouldGenerateBhytMaMay: isBhytMaMay
        },
        bhytMaMay: isBhytMaMay ? item?.isBhytMaMay : "",
        quantity: item?.quantity || 1,
        managementDepartmentId: item?.managementDepartmentId,
        name: item?.name,
        productId: item?.productId,
        assetGroupId: item?.assetGroupId,
        originalCost: item?.originalCost,

      }
      newArr.push(data);
    }
    
   return newArr;
  };

  export const convertSelectedSupplies = (arr, supplies) => {
    if (!Array.isArray(arr) || arr.length === 0) return;
    
    return arr?.map(element => {
      const existingItem = supplies?.find((supply) =>
        (supply?.product?.id === element?.id || supply?.productId === element?.id) &&
        (supply?.product?.inputDate === element?.inputDate || supply?.receiptDate === element?.inputDate) &&
        (supply?.product?.lotNumber === element?.lotNumber || supply?.batchCode === element?.lotNumber) &&
        (supply?.product?.unitPrice === element?.unitPrice
          || supply?.price === element?.unitPrice
          || supply?.amount === element?.unitPrice) &&
        (supply?.product?.code === element?.code || supply?.productCode === element?.code)
      );
  
      return {
        product: {
          name: element.name || element?.productName,
          code: element.code || element?.productName,
          id: element?.product?.id || element?.id,
        },
        sku: {
          name: element?.defaultSku?.sku?.name || element?.skuName || element?.unitName,
          id: element?.defaultSku?.sku?.id || element?.skuId || element?.unitId
        },
        skuName: element?.sku?.name || element?.unitName,
        skuId: element?.sku?.id || element?.defaultSku?.sku?.id || element?.skuId || element?.unitId,
        remainingQuantity: element?.remainingQuantity,
        productName: element.name || element?.productName,
        productCode: element.code || element?.productName,
        productId: element?.product?.id || element?.id,
        price: element?.unitPrice,
        quantityOfVoucher: existingItem?.quantityOfVoucher || 1,
        quantity: existingItem?.quantity || 1,
        amount: element?.unitPrice,
        receiptDate: element?.inputDate,
        batchCode: element?.lotNumber,
        expiryDate: element?.expiryDate,
        purchasePlaning: element?.purchasePlaning,
        supplierId: element?.supplierId,
        supplierName: element?.supplierName,
        decisionCode: element?.decisionCode,
      };
    })
  };
  
  export const checkDuplicateSerial = (assetVouchers, item) => {
    return (
      item?.serialNumber &&
      assetVouchers?.some(
        (assetVoucher) => assetVoucher.asset?.serialNumber === item?.serialNumber
      )
    );
  };

  export const handleCopyAsset = (value, state) => {    
    let { selectedRow, listFixedAsset = [] } = state;
    const newListDataCopy = [...listFixedAsset];
    const rowDataToString = { ...JSON.parse(JSON.stringify(selectedRow)) };
    
    const _selectedRow = {
      id: null,
      quantity: rowDataToString?.quantity,
      note: rowDataToString?.note,
      isBhytMaMay: Boolean(rowDataToString?.asset?.bhytMaMay),
      asset: {
        ...rowDataToString?.asset,
        serialNumber: null,
        managementCode: null,
        shouldGenerateBhytMaMay: Boolean(rowDataToString?.asset?.shouldGenerateBhytMaMay),
        code: null,
        assetId: null,
        bhytMaMay: null
      },
    };

    const largeArray = Array.from({ length: value }, () => ({ ..._selectedRow }));
    newListDataCopy.push(...largeArray);

    return newListDataCopy;
  };

  export const handleCopyIat = (value, state) => {
    let { selectedRow, listIat = [] } = state;
    const newListDataCopy = [...listIat];
    const rowDataToString = { ...JSON.parse(JSON.stringify(selectedRow)) };
    
    const _selectedRow = {
      id: null,
      quantity: rowDataToString?.quantity,
      note: rowDataToString?.note,
      asset: {
        ...rowDataToString?.asset,
        serialNumber: null,
        managementCode: null,
        isDisableBhytMaMay: Boolean(rowDataToString?.asset?.bhytMaMay),
        code: null,
        assetId: null
      },
    };

    const largeArray = Array.from({ length: value }, () => ({ ..._selectedRow }));
    newListDataCopy.push(...largeArray);
    return newListDataCopy;
  };

  export const handleCopyProduct = (value, state) => {
    let { selectedRow, listSupplies = [] } = state;
    const newListDataCopy = [...listSupplies];
    const rowDataToString = { ...JSON.parse(JSON.stringify(selectedRow)) };
    
    const _selectedRow = {
      id: null,
      quantity: rowDataToString?.quantity,
      quantityOfVoucher: rowDataToString?.quantityOfVoucher,
      note: rowDataToString?.note,
      skuName: rowDataToString?.skuName,
      price: rowDataToString?.price,
      amount: rowDataToString?.amount,
      batchCode: rowDataToString?.batchCode,
      expiryDate: rowDataToString?.expiryDate,
      purchasePlaning: rowDataToString?.purchasePlaning,
      product: {
        ...rowDataToString?.product,
        code: null,
        name: rowDataToString?.productName
      },
      sku: {
        id: rowDataToString?.skuId,
        name: rowDataToString?.skuName
      },
      productId: rowDataToString?.productId,
      skuId: rowDataToString?.skuId
    };

    const largeArray = Array.from({ length: value }, () => ({ ..._selectedRow }));
    newListDataCopy.push(...largeArray);

    return newListDataCopy;

  };

  export const handleMapDataIat = async (event, item, state, data = {}) => {
    const { contract, billNumber, billDate, supplier, issueDate, store, receiverDepartment } = state;
    const value = {...data};
    
    // Tạo một đối tượng mới với các thuộc tính được lấy từ value và contract.
    const newData = {
      ...value,
      id: null,
      assetAccessories: value?.assetAccessories?.map(phuTung => ({
        ...phuTung,
        unit: {
          name: phuTung.unitName,
          id: phuTung.unitId,
        },
        product: {
          name: phuTung.productName,
          type: phuTung.productType,
          id: phuTung.productId
        },
      })),
      serialNumber: item?.serialNumber || "",
      assetGroupId: value?.assetGroup?.id,
      depreciationRate: value?.assetGroup?.depreciationRate,
      contract: contract?.id ? contract : null,
      lotNumber: billNumber,
      billDate: billDate,
      ngayHoaDon: billDate,
      supplyUnit: supplier,
      dateOfReception: issueDate,
      isReceivingAsset: true,
      managementDepartmentId: receiverDepartment?.id,
      depreciationDate: value?.depreciationDate || new Date(),
      serialNumbers: item?.serialNumbers || [],
      valueSerialNumber: item?.valueSerialNumber || "",
      store,
      managementDepartment: receiverDepartment,
      soNamKhConLai: value?.assetGroup?.namSuDung
    };

    // Xóa các thuộc tính không cần thiết.
    for (const prop of [
      "status",
      "code",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      'useDepartment'
    ]) {
      delete newData[prop];
    }

    // Cập nhật state của component.
    // this.setState({ item: newData });

    return newData ;
  };

  export const handleMapDataAsset = async (event, item, state, data = {}) => {
    const { contract, billNumber, billDate, supplier, issueDate, store, receiverDepartment, decisionCode } = state;

    const value = {...data};

    if (item?.product?.id && decisionCode) {
      value.decisionCode = decisionCode;
      value.product = item.product;
    };
    // Tạo một đối tượng mới với các thuộc tính được lấy từ value và contract.
    const newData = {
      ...value,
      id: null,
      assetAccessories: value?.assetAccessories?.map(phuTung => ({
        ...phuTung,
        unit: {
          name: phuTung.unitName,
          id: phuTung.unitId,
        },
        product: {
          name: phuTung.productName,
          type: phuTung.productType,
          id: phuTung.productId
        },
      })),
      serialNumber: item?.serialNumber || "",
      assetGroupId: value?.assetGroup?.id,
      depreciationRate: value?.assetGroup?.depreciationRate,
      contract: contract?.id ? contract : null,
      lotNumber: billNumber,
      billDate: billDate,
      ngayHoaDon: billDate,
      supplyUnit: supplier,
      dateOfReception: issueDate,
      isReceivingAsset: true,
      managementDepartmentId: receiverDepartment?.id,
      depreciationDate: value?.depreciationDate || new Date(),
      serialNumbers: item?.serialNumbers || [],
      valueSerialNumber: item?.valueSerialNumber || "",
      store,
      managementDepartment: receiverDepartment,
      soNamKhConLai: value?.assetGroup?.namSuDung,
      decisionCode,
      unitPrice: value?.unitPrice,
    };

    // Xóa các thuộc tính không cần thiết.
    for (const prop of [
      "status",
      "code",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      'useDepartment'
    ]) {
      delete newData[prop];
    }

    // Cập nhật state của component.
    return newData;
  };