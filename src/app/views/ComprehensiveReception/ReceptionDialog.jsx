import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";
import { addNewOrUpdate, getNewCodeAsset } from "./ReceptionService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeDocument,
  getRecevingAssetDocumentById,
  updateRecevingAssetDocument,
} from "../Asset/AssetService";
import ReceptionScrollableTabsButtonForce from "./Component/ReceptionScrollableTabsButtonForce";
import SelectMultiProductPopup from "../InventoryReceivingVoucher/components/SelectMultiProductPopup";
import InstrumentToolsListEditorDialog from "../InstrumentToolsList/InstrumentToolsListEditorDialog";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AssetEditorDialogNew from "../Asset/AssetEditorDialogNew";
import { appConst, STATUS_SUPPLIER, variable } from "../../appConst";
import AppContext from "app/appContext";
import ComponentDialog from "./Component/ComponentDialog";
import { searchByTextDVBH } from "../Supplier/SupplierService";
import {
  formatDateDto,
  formatDateNoTime,
  getTheHighestRole, getUserInformation,
  handleThrowResponseMessage, isSuccessfulResponse,
  PaperComponent,
} from "../../appFunction";
import {
  convertAssetToFlattenItem,
  convertSelectedAssets,
  convertSelectedSupplies,
  handleCopyAsset,
  handleCopyIat,
  handleCopyProduct,
  handleMapDataAsset,
  handleMapDataIat,
} from "./helper";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import { getItemById } from "../InstrumentToolsList/InstrumentToolsListService";
import { getItemById as getItemByIdAsset } from "../Asset/AssetService";
import CouncilDialog from "../Council/CouncilDialog";
import {getVoucherAttributes, saveVoucherAttributes} from "../../appServices";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class ReceptionDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    supplier: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: formatDateNoTime(new Date()),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    shouldOpenSelectDepartmentPopup: false,
    shouldOpenSelectSupplyPopup: false,
    totalElements: 0,
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenAssetEditorPopup: false,
    voucherCode: "",
    handoverRepresentative: "",
    note: "",
    isReceivingAsset: true,
    shouldOpenSelectAssetFilePopup: false,
    isNew: true,
    documentType: appConst.documentType.ASSET_DOCUMENT_RECEIPT, // hồ sơ tiếp nhân tài sản
    contract: null,
    isEditLocal: false,
    shouldOpenContractAddPopup: false,
    shouldOpenSupplierAddPopup: false,
    shouldOpenProductPopup: false,
    shouldOpenIatEditorPopup: false,
    listAssetDocumentId: [],
    assetDocumentList: [],
    listFixedAsset: [],
    listIat: [],
    listSupplies: [],
    voucherId: null,
    idHoSoDK: null,
    serialNumbers: [],
    isClones: false,
    listSupplyUnit: [],
    listCodes: [],
    isTiepNhan: true,
    listToastFunction: {
      error: toast.error,
      warning: toast.warning,
      success: toast.success,
    },
    supplierSearchObject: {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      typeCodes: [appConst.TYPE_CODES.NCC_CU],
      isActive: STATUS_SUPPLIER.HOAT_DONG.code,
    },
    indexEdit: undefined,
    selectedRows: [],
    shouldOpenCopyDialog: false,
    selectedRow: null,
    tabValueTable: appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code,
    productTypeCode: appConst.productTypeCode.VTHH,
    council: null,
    listCouncil: [],
    persons: [],
    keySearch: "",
    signatures: {},
  };
  
  voucherType = ConstantList.VOUCHER_TYPE.ReceivingAsset;
  handleChange = (event, source) => {
    const { name, value } = event.target;

    if (name === variable.listInputName.handoverRepresentative) {
      this.setState({
        signatures: {
          ...this.state.signatures,
          handoverPersonName: value,
          handoverPersonId: null,
        }
      });
    }

    this.setState({
      [name]: value,
    })
  };

  convertDataSubmit = (state) => {
    let { listFixedAsset = [], listIat = [], listSupplies = [] } = state;

    return {
      bidId: state?.bidId,
      billNumber: state?.billNumber,
      contractCode: state?.contract?.contractCode,
      contractName: state?.contract?.contractName,
      supplierAddress: state?.supplierAddress,
      handoverRepresentative: state?.handoverRepresentative,
      note: state?.note,
      reason: state?.reason,
      hoiDongId: state?.council?.id,
      hoiDongName: state?.council?.name,
      receiverPersonName: state?.receiverPersonName,
      totalOriginalCost: state?.totalOriginalCost,
      documents: state?.documents,
      id: state?.id,
      type: state?.type,
      voucherName: state?.voucherName,
      voucherCode: state?.voucherCode,
      assetClass: appConst.assetClass.TSCD,
      issueDate: formatDateDto(state?.issueDate),
      billDate: formatDateDto(state?.billDate),
      storeId: state?.store?.id,
      receiverDepartmentId: state?.receiverDepartment?.id,
      receiverPersonId: state?.receiverPerson?.id,
      decisionCode: state?.decisionCode?.decisionCode,
      supplierId: state?.supplier?.id,
      contractId: state?.contract?.id,
      contractDate: state?.contract?.contractDate,
      status: state?.status?.indexOrder,
      listFixedAsset: listFixedAsset?.map((item) => ({
        ...item?.asset,
        quantity: item?.asset?.quantity || 1,
        managementDepartmentId: item?.asset?.managementDepartmentId,
        name: item?.asset?.name,
        productId: item?.asset?.productId,
        assetGroupId: item?.asset?.assetGroupId,
        originalCost: item?.asset?.originalCost,
        assetId: item?.asset?.assetId,
        isBhytMaMay: item?.shouldGenerateBhytMaMay,
        decisionCode: state?.decisionCode?.decisionCode,
      })),
      listSupplies: listSupplies?.map((supply) => ({
        ...supply,
        productId: supply?.product?.id || supply?.productId,
        skuId: supply?.sku?.id || supply?.skuId,
        skuType: 1,
        quantityOfVoucher: supply?.quantityOfVoucher,
        quantity: supply?.quantity,
        purchasePlaning: supply?.purchasePlaning,
        purchasePlaningId: supply?.purchasePlaning?.id,
      })),
      listIat: listIat?.map((iat) => ({
        ...iat?.asset,
        productId: iat?.asset?.productId,
        managementDepartmentId: iat?.asset?.managementDepartment?.id || iat?.asset?.managementDepartmentId,
        name: iat?.asset?.name,
        quantity: iat?.asset?.quantity,
        assetGroupId: iat?.asset?.assetGroupId,
        originalCost: iat?.asset?.originalCost,
        assetId: iat?.asset?.assetId,
        decisionCode: state?.decisionCode?.decisionCode,
      })),
      persons: state?.persons?.map(person => ({
        departmentId: person?.departmentId,
        departmentName: person?.departmentName,
        personId: person?.personId,
        personName: person?.personName,
        position: person?.position,
        role: person?.role,
      })),
    };
  };

  handleFormSubmit = async () => {
    const {
      id,
      handoverRepresentative,
      receiverDepartment,
      listFixedAsset,
      listIat,
      listSupplies,
    } = this.state;
    const { t } = this.props;
    let { setPageLoading } = this.context;

    // Check for required fields
    if (handoverRepresentative?.id === null) {
      this.showToast(
        "Vui lòng chọn đại diện bên bàn giao(Thông tin phiếu).",
        "warning"
      );
      return;
    }
    if (receiverDepartment?.id === null) {
      this.showToast(
        "Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).",
        "warning"
      );
      return;
    }
    if (
      listFixedAsset?.length === 0 &&
      listIat?.length === 0 &&
      listSupplies?.length === 0
    ) {
      this.showToast(
        "Vui lòng chọn tài sản tiếp nhận(Thông tin phiếu).",
        "warning"
      );
      return;
    }
    // Define success and error handlers
    const handleSuccess = (message) => {
      this.showToast(message, "success");
      this.props.handleOKEditClose();
    };
    try {
      setPageLoading(true);
      let payload = this.convertDataSubmit(this.state);
      let res = await addNewOrUpdate(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        // let isUpdateVoucherAttributesSuccess = await this.handleSaveVoucherAttributes(data?.id);
        // if (!isUpdateVoucherAttributesSuccess) {return;}
        handleSuccess(
          id ? t("general.updateSuccess") : t("general.addSuccess")
        );
        await this.updateAssetDocument(data?.id);
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  showToast = (message, type) => {
    let { listToastFunction = {} } = this.state;
    toast.clearWaitingQueue();
    const toastFunction = listToastFunction[type]
      ? listToastFunction[type]
      : () => { };

    toastFunction(message);
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  checkListDataForTabValue = () => {
    let { listFixedAsset = [], listIat = [], listSupplies = [], tabValueTable } = this.state;
    let listData = {
      [appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code]: listFixedAsset,
      [appConst.TABS_COMPREHENSIVE_ITEMS.CCDC.code]: listIat,
      [appConst.TABS_COMPREHENSIVE_ITEMS.VT.code]: listSupplies,
    };

    let nameListData = {
      [appConst.assetTypeObject.TSCD.code]: variable.listInputName.listFixedAsset,
      [appConst.assetTypeObject.CCDC.code]: variable.listInputName.listIat,
      [appConst.assetTypeObject.VT.code]: variable.listInputName.listSupplies,
    };

    return {
      value: [...listData[tabValueTable]],
      name: nameListData[tabValueTable],
    };
  };

  handleSelectAssetAll = (item) => {
    let { t } = this.props;
    let { tabValueTable, supplies, receiverDepartment } = this.state;
    let name = this.checkListDataForTabValue().name;

    if (item?.length <= 0) {
      return toast.warning(t("allocation_asset.missingCCDC"));
    }

    const newData = {
      [appConst.assetTypeObject.TSCD.code]: convertSelectedAssets(
        item,
        this.state,
        receiverDepartment
      ),
      [appConst.assetTypeObject.CCDC.code]: convertAssetToFlattenItem(
        item,
        this.state,
        receiverDepartment
      ),
      [appConst.assetTypeObject.VT.code]: convertSelectedSupplies(
        item,
        supplies
      ),
    };

    this.setState(
      {
        [name]: newData[tabValueTable] || [],
      },
      () => {
        this.handleSumTotalCost();
        this.handleDialogAssetEditorClose();
      }
    );
  };

  selectPurchasePlaning = (item, rowData) => {
    let { listSupplies } = this.state;
    if (listSupplies != null && listSupplies.length > 0) {
      listSupplies.forEach((vd) => {
        if (vd.tableData.id === rowData.tableData.id) {
          vd.purchasePlaning = item;
        }
      });
      this.setState({ listSupplies });
    }
  };

  removeAssetInlist = async (rowData) => {
    let { tabValueTable } = this.state;
    const dataRemove = this.checkListDataForTabValue(tabValueTable);
    const { name, value } = dataRemove;
    let index = value.findIndex(
      (x) => x.tableData.id === rowData.tableData?.id
    );

    value.splice(index, 1);

    this.setState({
      [name]: value,
    }, this.handleSumTotalCost);
  };

  handleCopyAssetAll = (value) => {
    let { tabValueTable } = this.state;
    let { t } = this.props;
    let name = this.checkListDataForTabValue().name;

    const newCopyNew = {
      [appConst.assetTypeObject.TSCD.code]: handleCopyAsset(value, this.state),
      [appConst.assetTypeObject.CCDC.code]: handleCopyIat(value, this.state),
      [appConst.assetTypeObject.VT.code]: handleCopyProduct(value, this.state),
    };

    this.setState(
      {
        [name]: newCopyNew[tabValueTable] || [],
      },
      () => {
        toast.info(t("Reception.noti.copySuccess"));
        this.handleSumTotalCost();
        this.handleCloseCopyDialog();
      }
    );
  };

  handleSelectionChange = (selectedRows, rowData) => {
    this.setState({ selectedRows });
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      tabValueTable: newValue,
    });
  };

  componentDidMount() {
    let { departmentUser, isRoleAssetManager } = getTheHighestRole();
    let { item, isView } = this.props;
    let {organization, currentUser} = getUserInformation();

    let updatedItem = {
      ...item,
      receiverDepartment: item?.receiverDepartmentId
        ? {
          id: item?.receiverDepartmentId,
          name: item?.receiverDepartmentName,
        }
        : departmentUser?.id ? departmentUser : null,
      isRoleAssetManager,
      signatures: {
        directorId: organization?.directorId,
        directorName: organization?.directorName,
        deputyDirectorId: organization?.deputyDirectorId,
        deputyDirectorName: organization?.deputyDirectorName,
        chiefAccountantId: organization?.chiefAccountantId,
        chiefAccountantName: organization?.chiefAccountantName,
        headOfDepartmentId: item?.headOfDepartmentId,
        headOfDepartmentName: item?.headOfDepartmentName,
        deputyDepartmentId: item?.deputyDepartmentId,
        deputyDepartmentName: item?.deputyDepartmentName,
        storeKeeperId: item?.storeKeeperId,
        storeKeeperName: item?.storeKeeperName,
        handoverPersonId: null,
        handoverPersonName: item.handoverRepresentative,
        receiverPersonId: item?.receiverPersonId,
        receiverPersonName: item?.receiverPersonName,
        createdPersonId: currentUser?.person?.id,
        createdPersonName: currentUser?.person?.displayName,
      }
    };

    if (updatedItem.id) {
      this.getAssetDocument();
      this.setState({
        ...updatedItem,
        isNew: false,
        store: item?.storeId
          ? {
            name: item?.storeName,
            id: item?.storeId,
          }
          : null,
        supplier: item?.supplierId
          ? {
            name: item?.supplierName,
            id: item?.supplierId,
          }
          : null,
        contract: item?.contractId
          ? {
            contractName: item?.contractCode && item?.contractName && `${item?.contractCode} - ${item?.contractName}`,
            contractId: item?.contractId,
            id: item?.contractId,
            contractDate: item?.contractDate,
            contractCode: item?.contractCode,
          }
          : null,
        status: appConst.LIST_STATUS_ASSET_RECEPTION.find(st => st?.code === item?.status),
        listSupplies: item?.listSupplies?.map(supply => ({
          ...supply,
          product: {
            name: supply?.productName,
            code: supply?.productCode,
            id: supply?.productId
          },
          sku: {
            name: supply?.skuName,
            id: supply?.skuId,
          }
        })),
        isView,
        listFixedAsset: item?.listFixedAsset?.map(asset => ({
          asset: {
            ...asset,
            shouldGenerateBhytMaMay: Boolean(asset?.bhytMaMay)
          }
        })),
        listIat: item?.listIat?.map(iat => ({
          asset: { ...iat }
        })),
        decisionCode: item?.bidId
          ? {
            id: item?.bidId,
            decisionCode: item?.decisionCode,
          }
          : null,
        receiverPerson: item?.receiverPersonId
          ? {
            id: item?.receiverPersonId,
            name: item?.receiverPersonName
          }
          : null,
        council: item?.hoiDongId
          ? {
            id: item?.hoiDongId,
            name: item?.hoiDongName,
          }
          : null,
      }, async () => {
        // await this.handleGetVoucherAttributes();
      });
    } else {
      this.setState({ ...updatedItem });
    }
  }

  handleOpenAssetDialog = async () => {
    let {
      issueDate,
      billDate,
      supplier,
      receiverDepartment,
      store,
      contract,
      billNumber,
      tabValueTable,
      decisionCode
    } = this.state;

    const isTSCD =
      tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code;
    const isCCDC =
      tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.CCDC.code;
    const isVT = tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.VT.code;
    try {
      const { data } = await getNewCodeAsset();
      if (data?.code) {
        this.setState({
          item: {
            ...data,
            issueDate,
            ngayHoaDon: billDate,
            supplyUnit: supplier,
            store,
            contract,
            billNumber,
            managementDepartment: receiverDepartment,
            dateOfReception: issueDate,
            decisionCode
          },
          isAddAsset: true,
          indexEdit: undefined,
        });
      }
      this.setState({
        shouldOpenProductPopup: isVT,
        shouldOpenAssetEditorPopup: isTSCD,
        shouldOpenIatEditorPopup: isCCDC,
      });
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  convertItemTableData = (item) => {
    let {
      issueDate,
      billDate,
      supplier,
      receiverDepartment,
      store,
      contract,
      billNumber,
      decisionCode,
    } = this.state;
    let _assetSources = item?.assetSources?.length
      ? [...item?.assetSources]
      : [];
    return {
      ...item,
      issueDate,
      ngayHoaDon: billDate,
      supplyUnit: supplier,
      store,
      contract,
      billNumber,
      decisionCode,
      isBuyLocally: item?.madeIn === "Việt Nam",
      madeIns: item?.madeIn
        ? item?.madeIn === "Việt Nam"
          ? appConst.MADE_IN.TRONG_NUOC
          : appConst.MADE_IN.NGOAI_NUOC
        : null,
      managementDepartment: receiverDepartment,
      isBhytMaMay: item?.shouldGenerateBhytMaMay,
      product: {
        id: item?.productId,
        name: item?.productName || item?.product?.name,
      },
      unit: {
        id: item?.unit?.id || item?.unitId,
        name: item?.unit?.name || item?.unitName,
      },
      assetGroup: {
        id: item?.assetGroup?.id || item?.assetGroupId,
        name: item?.assetGroup?.name || item?.assetGroupName,
      },
      assetSources: _assetSources?.map((x) => {
        return {
          ...x,
          assetSource: {
            ...x?.assetSource,
            id: x?.assetSourceId || x?.id,
            name: x?.assetSourceName || x?.assetSource?.name,
            code: x?.assetSourceCode || x?.assetSource?.code,
            isMainAssetSource: x?.isMainAssetSource,
          },
        };
      }),
      attributes: item?.attributes?.map((i) => {
        return {
          ...i,
          attribute: {
            ...(i?.attribute || {}),
            code: i?.attributeCode || i?.attribute?.code,
            name: i?.attributeName || i?.attribute?.name,
            id: i?.attributeId || i?.attribute?.id,
          },
        };
      }),
      manufacturer: {
        ...item?.manufacturer,
        id: item?.manufacturer?.id || item?.manufacturerId,
        name: item?.manufacturer?.name || item?.manufacturerName,
        manufacturerName: item?.manufacturer?.name || item?.manufacturerName,
      },
      donViBaoHanh: {
        id: item?.donViBaoHanh?.id || item?.donViBaoHanhId,
        name: item?.donViBaoHanh?.name || item?.donViBaoHanhName,
      },
      donViKiemDinh: item?.donViKiemDinhId
        ? {
          id: item?.donViKiemDinhId,
          name: item?.donViKiemDinhTen,
        }
        : null,
      donViKiemXa: item?.donViKiemXaId
        ? {
          id: item?.donViKiemXaId,
          name: item?.donViKiemXaTen,
        }
        : null,
      donViHieuChuan: item?.donViHieuChuanId
        ? {
          id: item?.donViHieuChuanId,
          name: item?.donViHieuChuanTen,
        }
        : null,
      medicalEquipment: item?.medicalEquipmentId
        ? {
          id: item?.medicalEquipmentId,
          name: item?.medicalEquipmentName,
        }
        : null,
      shoppingForm: item?.shoppingFormId
        ? {
          id: item?.shoppingFormId,
          name: item?.shoppingFormName,
        }
        : null,
    };
  };

  handleEditAssetInList = async (rowData) => {
    let dataConvert = this.convertItemTableData({ ...rowData?.asset });
    let { tabValueTable } = this.state;
    this.setState({
      item: JSON.parse(JSON.stringify(dataConvert)),
      shouldOpenAssetEditorPopup: Boolean(tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code),
      shouldOpenIatEditorPopup: Boolean(tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.CCDC.code),
      isAddAsset: true,
      isEditLocal: true,
      indexEdit: rowData?.tableData?.id,
    });
  };

  handleChangeAssetInList = async (e, rowData) => {
    let { t } = this.props;
    let { tabValueTable } = this.state;
    let { TSCD, CCDC } = appConst.TABS_COMPREHENSIVE_ITEMS;
    let arrayItem = this.checkListDataForTabValue(tabValueTable).value;

    let listDataName = {
      [TSCD.code]: variable.listInputName.listFixedAsset,
      [CCDC.code]: variable.listInputName.listIat,
    };

    let updatedListData = [...arrayItem];

    const index = updatedListData.findIndex(
      (x) => x.tableData.id === rowData?.tableData?.id
    );

    let { name, value } = e.target;
    if (["managementCode", "serialNumber", "bhytMaMay"].includes(name)) {
      updatedListData[index] = {
        ...updatedListData[index],
        asset: {
          ...updatedListData[index].asset,
          [name]: value,
        },
      };
    }

    this.setState({
      [listDataName[tabValueTable]]: [...updatedListData],
    });
  };

  handleChangeSupplyField = (rowData, event, fieldName) => {
    let { listSupplies } = this.state;
    let value = fieldName === "expiryDate" ? event : event.target.value; // Nếu là expiryDate, sử dụng event trực tiếp

    if (listSupplies != null && listSupplies.length > 0) {
      listSupplies.forEach((vd) => {
        if (vd?.tableData?.id === rowData?.tableData?.id) {
          switch (fieldName) {
            case "quantity":
              vd.quantity = value || null;
              if (value && vd.price) {
                vd.amount = (value / 1) * vd.price;
              }
              break;
            case "quantityOfVoucher":
              vd.quantityOfVoucher = value || null;
              break;
            case "price":
              vd.price = value;
              if (value != null && vd.quantity != null) {
                vd.amount = (value / 1) * vd.quantity;
              }
              break;
            case "batchCode":
              vd.batchCode = value;
              break;
            case "expiryDate":
              vd.expiryDate = value ? formatDateDto(value) : null;
              break;
            default:
              break;
          }
        }
      });
      this.setState({ listSupplies }, () => {
        if (["price", "quantity"].includes(fieldName)) {
          this.handleSumTotalCost();
        }
      });
    }
  };

  handleDialogAssetEditorClose = () => {
    this.setState({
      isEditLocal: false,
      shouldOpenAssetEditorPopup: false,
      shouldOpenProductPopup: false,
      shouldOpenIatEditorPopup: false,
    });
  };

  handleOKEditAssetClose = () => {
    this.setState({ shouldOpenAssetEditorPopup: false });
  };

  handleOpenAssetFilePopup = () => {
    this.setState({ shouldOpenSelectAssetFilePopup: true });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      idHoSoDK: null,
      shouldOpenSelectAssetFilePopup: false,
    });
  };

  handleAddAssetDocumentItem = async () => {
    const type = appConst.documentType.ASSET_DOCUMENT_RECEIPT;
    try {
      const result = await getNewCodeDocument(type);
      if (result != null && result.data && result.data.code) {
        const item = result.data.data;
        this.setState({
          item: item,
          shouldOpenSelectAssetFilePopup: true,
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  handleRowDataCellEditAssetFile = async (rowData) => {
    try {
      const { data } = await getAssetDocumentById(rowData.id);
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({
          item: data?.data,
          idHoSoDK: rowData.id,
          shouldOpenSelectAssetFilePopup: true,
          isEditAssetDocument: true,
        });
      } else {
        toast.warn(data?.message);
      }
    } catch (error) {
      // Xử lý lỗi nếu có
      console.error(error);
    }
  };

  handleRowDataCellDeleteAssetFile = (rowData) => {
    let { listAssetDocumentId, assetDocumentList } = this.state;
    listAssetDocumentId.splice(listAssetDocumentId?.indexOf(rowData?.id), 1);
    assetDocumentList.splice(assetDocumentList?.indexOf(rowData?.id), 1);
    this.setState({ listAssetDocumentId, assetDocumentList });
  };

  getAssetDocument = () => {
    getRecevingAssetDocumentById(this.props.item?.id).then((response) => {
      const { data } = response;
      if (data) {
        this.setState({
          assetDocumentList: data?.data,
          totalElements: data?.total,
        });
      }
    });
  };

  updateAssetDocument = async (id) => {
    try {
      const payload = {
        assetDocumentIds: this.state?.assetDocumentList?.map((i) => i?.id),
        documentType: appConst.documentType.ASSET_DOCUMENT_RECEIPT,
      };
      await updateRecevingAssetDocument(id, payload);
    } catch (error) { }
  };

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;
    if (listAssetDocumentId != null && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach((id) => deleteAssetDocumentById(id));
    }
  };

  handleChangeComboBox = (value, name) => {

    if (name === variable.listInputName.receiverDepartment) {
      this.setState({
        receiverPerson: null,
        store: null,
        signatures: {
          ...this.state.signatures,
          deputyDepartmentId: value?.deputyDepartmentId,
          deputyDepartmentName: value?.deputyDepartmentName,
          headOfDepartmentId: value?.headOfDepartmentId,
          headOfDepartmentName: value?.headOfDepartmentName,
        }
      });
    }
    if (name === variable.listInputName.store) {
      this.setState({
        signatures: {
          ...this.state.signatures,
          storekeeperId: value?.storekeeperId,
          storekeeperName: value?.storekeeperName,
        }
      });
    }

    if (name === "decisionCode") {
      this.setState({
        listFixedAsset: [],
        listIat: [],
        listSupplies: [],
      });
    }

    this.setState({
      [name]: value
    });
  };

  handleChangeSelectContract = (item) => {
    this.setState({ contract: item });
  };

  handleSelectSupplier = (item) => {
    let { supplier } = this.state;

    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenSupplierAddPopup: true,
      });
    }
    if (supplier?.id !== item?.id) {
      this.setState({
        listFixedAsset: [],
        listIat: [],
        listSupplies: [],
      })
    }
    this.setState({
      supplier: item,
    });
  };

  handleCloseSupplier = () => {
    const { supplier } = this.state;
    if (variable.listInputName.New === supplier?.code) {
      this.setState({ supplier: null });
    }
    this.setState({
      shouldOpenSupplierAddPopup: false,
    });

    this.updateListSupplier();
  };

  handleSelectContract = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenContractAddPopup: true,
      });
    }
    this.setState({
      contract: item,
      supplier: item?.supplier,
    });
  };

  handleCloseContract = () => {
    const { contract } = this.state;
    if (variable.listInputName.New === contract.code) {
      this.setState({ contract: null });
    }
    this.setState({
      shouldOpenContractAddPopup: false,
    });
  };

  updateListSupplier = async () => {
    let { t } = this.props;
    try {
      let res = await searchByTextDVBH(this.state.supplierSearchObject);
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({
          listSupplyUnit: res.data.content,
        });
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    }
  };

  setListSupplyUnit = (value) => {
    this.setState({
      listSupplyUnit: value,
    });
  };

  getItemForMapping = async (id) => {
    const { tabValueTable } = this.state;
    const isTSCD =
      tabValueTable === appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code;
    const getItem = isTSCD ? getItemByIdAsset : getItemById;
    try {
      const data = await getItem(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        return data?.data?.data || {};
      }
      return {};
    } catch (error) {
      return {};
    }
  };

  handleMapDataAssetAll = async (event, itemSelected, item) => {
    const { tabValueTable } = this.state;
    const value = await this.getItemForMapping(itemSelected?.id);

    const assetMappingFunctions = {
      [appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code]: handleMapDataAsset,
      [appConst.TABS_COMPREHENSIVE_ITEMS.CCDC.code]: handleMapDataIat,
    };

    const mappingFunction = assetMappingFunctions[tabValueTable];

    if (mappingFunction) {
      const newData = await mappingFunction(event, item, this.state, value);
      this.setState({ item: newData }); // Cập nhật state với newData
    }
  };

  handleAddFileLocal = (listFile) => {
    let newList = [];
    newList.push(listFile);
    this.setState({
      assetDocumentList: [...this.state.assetDocumentList, ...newList],
    });
  };

  handleEditFileLocal = (file) => {
    let { assetDocumentList = [] } = this.state;
    let _assetDocumentList = [...assetDocumentList];

    _assetDocumentList = assetDocumentList.map(item => {
      if (item?.id === file?.id) {
        return file;
      } else {
        return item;
      }
    })

    this.setState({
      assetDocumentList: [..._assetDocumentList],
    });
  };

  handleCloseCopyDialog = () => {
    this.setState({ shouldOpenCopyDialog: false });
  };

  handleOpenCopyDialog = () => {
    this.setState({ shouldOpenCopyDialog: true });
  };

  setDataSate = (data, source) => {
    this.setState({ [source]: data });
  };

  handleChangeKeySearch = (value, name) => {
    this.setState({ [name]: value });
  };

  handleSumTotalCost = () => {
    const { listFixedAsset = [], listIat = [], listSupplies = [] } = this.state;

    const totalFixedAsset = listFixedAsset?.reduce(
      (total, item) => (total += Number(item?.asset?.originalCost || 0)),
      0
    );
    const totalIat = listIat?.reduce(
      (total, item) => (total += Number(item?.asset?.originalCost || 0)),
      0
    );
    const totalSupplies = listSupplies?.reduce(
      (total, item) => (total += Number(item?.amount || 0)),
      0
    );

    this.setState({
      totalOriginalCost: Number(totalFixedAsset + totalIat + totalSupplies) || 0,
    });
  };
  
  handleSelectCouncil = (item) => {
    if (variable.listInputName.New === item?.code) {
      this.setState({shouldOpenCouncilDialog: true});
      return;
    }
    
    item?.hdChiTiets?.map((person) => {
      person.handoverDepartment = {
        departmentId: person.departmentId,
        departmentName: person.departmentName,
        name: person.departmentName,
      };
      return person;
    });
    
    this.handleChangeComboBox(item, "council");
    this.handleChangeComboBox(item?.hdChiTiets, "persons");
  };
  
  handleDeletePersonInList = (item) => {
    let newDataRow = this.state.persons?.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    
    this.handleChangeComboBox(newDataRow, "persons");
  };

  handleClosePopup = () => {
    this.setState({
      shouldOpenCouncilDialog: false,
    })
  }
  
  handleChangeSignature = (data = []) => {
    this.setState({
      signatures: data,
    })
  }
  
  handleGetVoucherAttributes = async () => {
    let {setPageLoading} = this.context;
    try {
      setPageLoading(true);
      let payload = {
        voucherId: this.state.id,
      };
      let res = await getVoucherAttributes(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          signatures: data,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      this.showToast(t("general.error"), "error");
    } finally {
      setPageLoading(false);
    }
  }
  
  handleSaveVoucherAttributes = async (id) => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    try {
      setPageLoading(true);
      let payload = {
        ...this.state.signatures,
        voucherId: id,
      };
      let res = await saveVoucherAttributes(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          signatures: data,
        })
        return true;
      } else {
        handleThrowResponseMessage(res);
        return false;
      }
    } catch (err) {
      toast.error(t("general.error"));
      return false;
    } finally {
      setPageLoading(false);
    }
  }
  
  render() {
    let { open, handleClose, t, i18n, item, isEditItemStatusProcessed } = this.props;
    let {
      isView,
      shouldOpenAssetEditorPopup,
      receiverDepartment,
      shouldOpenProductPopup,
      productTypeCode,
      shouldOpenIatEditorPopup,
      listSupplies,
      status,
      shouldOpenCouncilDialog,
    } = this.state;
    let isDaXuLy = status?.code === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder;
    let contractSearchObject = { pageIndex: 1, pageSize: 999 };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: receiverDepartment?.id,
    };
    let searchObjectPurchasePlaning = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      type: this.state.purchasePlaningType,
    };

    return (
      <>
        <Dialog
          open={open}
          PaperComponent={PaperComponent}
          maxWidth="lg"
          fullWidth
          scroll="paper"
        >
          <CustomValidatorForm
            ref="form"
            onSubmit={this.handleFormSubmit}
            className="validator-form-scroll-dialog"
          >
            <DialogTitle
              className="mb-0 cursor-move"
              id="draggable-dialog-title"
            >
              <span>
                {item?.id
                  ? t("ReceivingAsset.updatePhieuTiepNhan")
                  : t("ReceivingAsset.addPhieuTiepNhan")}
              </span>
              <IconButton
                onClick={handleClose}
                style={{ position: "absolute", top: 0, right: 0 }}
              >
                <Icon className="text-black">clear</Icon>
              </IconButton>
            </DialogTitle>

            <DialogContent style={{ minHeight: "450px" }}>
              <ReceptionScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                contractSearchObject={contractSearchObject}
                receiverPersonSearchObject={receiverPersonSearchObject}
                handleDateChange={this.handleDateChange}
                handleEditAssetInList={this.handleEditAssetInList}
                handleChange={this.handleChange}
                handleOKEditClose={this.props.handleOKEditClose}
                handleOKEditAssetClose={this.handleOKEditAssetClose}
                handleOpenAssetDialog={this.handleOpenAssetDialog}
                getAssetDocument={this.getAssetDocument}
                removeAssetInlist={this.removeAssetInlist}
                itemAssetDocument={this.state.item}
                handleRowDataCellEditAssetFile={
                  this.handleRowDataCellEditAssetFile
                }
                handleRowDataCellDeleteAssetFile={
                  this.handleRowDataCellDeleteAssetFile
                }
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                onChangeComboBox={this.handleChangeComboBox}
                onChageSelectContract={this.handleChangeSelectContract}
                itemEdit={this.state.item}
                handleSelectContract={this.handleSelectContract}
                handleSelectSupplier={this.handleSelectSupplier}
                handleAddFileLocal={this.handleAddFileLocal}
                listSupplyUnit={this.state.listSupplyUnit}
                setListSupplyUnit={this.setListSupplyUnit}
                handleChangeAssetInList={this.handleChangeAssetInList}
                handleSelectionChange={this.handleSelectionChange}
                handleOpenCopyDialog={this.handleOpenCopyDialog}
                setDataSate={this.setDataSate}
                handleChangeKeySearch={this.handleChangeKeySearch}
                handleChangeTabValue={this.handleChangeTabValue}
                selectPurchasePlaning={this.selectPurchasePlaning}
                handleChangeSupplyField={this.handleChangeSupplyField}
                handleSelectCouncil={this.handleSelectCouncil}
                handleDeletePersonInList={this.handleDeletePersonInList}
                searchObjectPurchasePlaning={searchObjectPurchasePlaning}
                isEditItemStatusProcessed={isEditItemStatusProcessed}
                handleChangeSignature={this.handleChangeSignature}
              />
            </DialogContent>
            <DialogActions>
              <div className="flex flex-space-between flex-middle">
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={this.props.handleClose}
                >
                  {t("general.cancel")}
                </Button>
                {(!isView || isEditItemStatusProcessed) && (
                  <Button variant="contained" color="primary" type="submit">
                    {item?.id ? t("general.update") : t("general.save")}
                  </Button>
                )}
              </div>
            </DialogActions>
          </CustomValidatorForm>
        </Dialog>

        {shouldOpenAssetEditorPopup && (
          <AssetEditorDialogNew
            open={shouldOpenAssetEditorPopup}
            t={t}
            i18n={i18n}
            isReceivingAsset={this.state.isReceivingAsset}
            handleSelect={this.handleSelectAssetAll}
            handleClose={this.handleDialogAssetEditorClose}
            handleOKEditClose={this.handleOKEditClose}
            handleOKEditAssetClose={this.handleOKEditAssetClose}
            item={this.state.item}
            isTiepNhan={this.state.isTiepNhan}
            itemAssetDocument={{
              ...this.state,
              contract: this.state.contract
                ? {
                  ...this.state.contract,
                  contractName:
                    this.state.contract?.contractCode +
                    " - " +
                    (this.state.contract?.contractText || ""),
                }
                : null,
            }}
            isAddAsset={this.state.isAddAsset}
            isEditLocal={this.state.isEditLocal}
            handleMapDataAsset={this.handleMapDataAssetAll}
          />
        )}

        {shouldOpenProductPopup && (
          <SelectMultiProductPopup
            t={t}
            i18n={i18n}
            open={shouldOpenProductPopup}
            handleSelect={this.handleSelectAssetAll}
            products={listSupplies?.length > 0 ? [...listSupplies] : []}
            handleClose={this.handleDialogAssetEditorClose}
            selectedItem={{ product: null }}
            productTypeCode={
              productTypeCode != null ? productTypeCode : this.productTypeCode
            }
            allowedAddNew={true}
            isManagementPurchaseDepartment={true}
            managementPurchaseDepartment={receiverDepartment} //todo: phòng ban quản lý
            isReception
            item={this.state.item}
          />
        )}

        {shouldOpenIatEditorPopup && (
          <InstrumentToolsListEditorDialog
            open={shouldOpenIatEditorPopup}
            t={t}
            i18n={i18n}
            handleSelect={this.handleSelectAssetAll}
            handleClose={this.handleDialogAssetEditorClose}
            handleOKEditClose={this.handleOKEditAssetClose}
            item={this.state.item}
            isIatReception
            handleMapDataAsset={this.handleMapDataAssetAll}
          />
        )}

        <ComponentDialog
          {...this.state}
          t={t}
          i18n={i18n}
          voucherId={this.state?.id}
          handleCloseContract={this.handleCloseContract}
          handleSelectContract={this.handleSelectContract}
          handleCloseSupplier={this.handleCloseSupplier}
          handleSelectSupplier={this.handleSelectSupplier}
          getAssetDocument={this.getAssetDocument}
          handleAssetFilePopupClose={this.handleAssetFilePopupClose}
          handleAddFileLocal={this.handleAddFileLocal}
          handleCloseCopyDialog={this.handleCloseCopyDialog}
          handleEditFileLocal={this.handleEditFileLocal}
          handleCopyAssetAll={this.handleCopyAssetAll}
        />
        
        
        {shouldOpenCouncilDialog && (
          <CouncilDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleClosePopup}
            open={shouldOpenCouncilDialog}
            handleOKEditClose={this.handleClosePopup}
            item={{
              name: this.state.keySearch,
            }}
          />
        )}
      </>
    );
  }
}

ReceptionDialog.contextType = AppContext;
export default ReceptionDialog;
