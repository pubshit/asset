import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MTableToolbar } from "material-table";
import { createFilterOptions } from "@material-ui/lab/Autocomplete";
import { searchByTextDVBH } from "../../Supplier/SupplierService";
import {
  getListManagementDepartment,
} from "../../Asset/AssetService";
import { personSearchByPage } from "../../AssetAllocation/AssetAllocationService";
import CustomMaterialTable from "../../CustomMaterialTable";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {searchByPage} from "../../Contract/ContractService";
import {searchByPage as searchByPageCouncil} from "../../Council/CouncilService";
import { a11yProps, filterOptions } from "app/appFunction";
import { appConst, DEFAULT_TOOLTIPS_PROPS, STATUS_STORE, variable } from "app/appConst";
import { searchByPage as searchByPageStore } from "../../Store/StoreService";
import { vi } from "date-fns/locale";
import CustomValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { convertNumberPriceRoundUp } from "../../../appFunction";
import VoucherItemsTable from "./VoucherItemsTable";
import { LightTooltip, TabPanel } from "../../Component/Utilities";
import { searchByPage as searchByPageBidding } from "../../bidding-list/BiddingListService";
import {SignatureTabComponent} from "../../Component/Signature/SignatureTabComponent";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ReceptionScrollableTabsButtonForce(props) {
  const classes = useStyles();
  const filterAutocomplete = createFilterOptions();
  const { t, onChangeComboBox, item, handleSelectSupplier } = props;
  const [value, setValue] = React.useState(0);
  const [listContract, setListContract] = useState([]);
  const [listReceiverDepartment, setListReceiverDepartment] = React.useState([]);
  const [listReceiverPerson, setListReceiverPerson] = React.useState([]);
	
  const addBtnLabel = {
    [appConst.TABS_COMPREHENSIVE_ITEMS.TSCD.code]: t("general.add_asset"),
    [appConst.TABS_COMPREHENSIVE_ITEMS.CCDC.code]: t("general.addIat"),
    [appConst.TABS_COMPREHENSIVE_ITEMS.VT.code]: t("general.select_product"),
  }
  const initiateSignatures = {...props.item?.signatures};

  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: item?.receiverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code
  };
  let searchObjectBidding = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    status: appConst.STATUS_BIDDING.OPEN.code
  };
  let councilSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    type: appConst.OBJECT_HD_TYPE.KIEM_NHAP.code,
    isActive: appConst.STATUS_HOI_DONG.HOAT_DONG.code
  };

  let isAllowEditAndAddAsset = item?.issueDate
    && item?.supplier?.id
    && item?.handoverRepresentative
    && item?.status?.indexOrder
    && item?.store?.id
    && item?.receiverDepartment?.id;

  useEffect(() => {
    setListReceiverPerson([])
  }, [item?.receiverDepartment?.id]);

  const getListContract = () => {
    try {
      const searchObject = {};
      searchObject.pageIndex = 1; // Assuming `query` is defined somewhere
      searchObject.pageSize = 100000; // Assuming `query` is defined somewhere
      searchObject.keyword = ""; // Include the keyword from the event target
      searchByPage(searchObject).then(({ data }) => {
        if (data) {
          const updatedList =
            data?.content?.map((item) => ({
              ...item,
              contractName: item?.contractCode + " - " + item?.contractName,
            })) || [];

          setListContract(updatedList);
        }
      });
    } catch (error) { }
  }

  useEffect(() => {
    getListContract();
  }, [item?.shouldOpenContractAddPopup])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let columnsVoucherFile = [
    {
      title: t("Reception.columns.index"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("Reception.columns.codeProfile"),
      field: "code",
      align: "left",
      minWidth: "150px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Reception.columns.nameProfile"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("Reception.columns.description"),
      field: "description",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "center",
      maxWidth: 120,
      render: (rowData) => (
        <div className="none_wrap">
          {!item?.isView ?
            <>
              <LightTooltip
                {...DEFAULT_TOOLTIPS_PROPS}
                title={t("general.editIcon")}
              >
                <IconButton
                  size="small"
                  onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
                >
                  <Icon fontSize="small" color="primary">
                    edit
                  </Icon>
                </IconButton>
              </LightTooltip>
              <LightTooltip
                {...DEFAULT_TOOLTIPS_PROPS}
                title={t("general.deleteIcon")}
              >
                <IconButton
                  size="small"
                  onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}
                >
                  <Icon fontSize="small" color="error">
                    delete
                  </Icon>
                </IconButton>
              </LightTooltip>
            </>
            :
            <LightTooltip
              {...DEFAULT_TOOLTIPS_PROPS}
              title={t("general.viewDocument")}
            >
              <IconButton
                size="small"
                title={t("general.viewDocument")}
                onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
              >
                <Icon fontSize="small" color="primary">
                  visibility
                </Icon>
              </IconButton>
            </LightTooltip>
          }
        </div>
      ),
    },
  ];
  
  let columnPerson = [
    {
      title: t("general.action"),
      field: "",
      align: "center",
      maxWidth: 100,
      minWidth: 80,
      hidden: item?.isView,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <div className="none_wrap">
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleDeletePersonInList(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
    {
      title: t("Asset.stt"),
      field: "",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("asset_liquidate.persons"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) =>
        appConst.listHdChiTietsRole.find((item) => item?.code === rowData?.role)
          ?.name,
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 200,
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("Reception.tab.InformationVoting")} value={0} {...a11yProps(0)}/>
          <Tab label={t("Reception.tab.persons")} value={1} {...a11yProps(1)} />
          {/*<Tab label={t("Reception.tab.signature")} value={2} {...a11yProps(2)} />*/}
          <Tab label={t("Reception.tab.AttachedProfile")} value={3} {...a11yProps(3)}/>
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={3} sm={6} xs={12} style={{ display: "flex", alignItems: "center" }}>
            <div>
              <span style={{ fontWeight: "bold" }}>Mã phiếu: </span>
              {props.item.voucherCode}
            </div>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={vi}>
              <CustomValidatePicker
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("Reception.issueDate")}
                  </span>
                }
                name={"issueDate"}
                value={props.item?.issueDate || null}
                onChange={(date) => props.handleDateChange(date, "issueDate")}
                maxDate={new Date()}
                invalidDateMessage={t("general.invalidDateFormat")}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                disabled={item?.isView}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={6} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("Asset.decisionCode")}
                </span>
              }
              listData={props?.item?.listBidding}
              setListData={(value) => props?.onChangeComboBox(value, "listBidding")}
              searchFunction={searchByPageBidding}
              searchObject={searchObjectBidding}
              displayLable="decisionCode"
              typeReturnFunction="category"
              value={item?.decisionCode || null}
              onSelect={(value) => onChangeComboBox(value, "decisionCode")}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              size="small"
              label={t("Reception.lotNumber")}
              name="billNumber"
              className="w-100 mt-3"
              value={props.item.billNumber || ""}
              onChange={props.handleChange}
              InputProps={{
                readOnly: item?.isView && !props?.isEditItemStatusProcessed
              }}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={vi}>
              <CustomValidatePicker
                label={t("Reception.dateBill")}
                name={"billDate"}
                value={props.item.billDate}
                onChange={(date) => props.handleDateChange(date, "billDate")}
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                disabled={item?.isView && !props?.isEditItemStatusProcessed}
                clearable
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
						<AsynchronousAutocompleteSub
							label={t("Reception.procurementContract")}
							searchFunction={searchByPage}
							searchObject={{...appConst.OBJECT_SEARCH_MAX_SIZE}}
							listData={listContract}
							setListData={setListContract}
							displayLable="contractName"
							readOnly={item?.isView && !props?.isEditItemStatusProcessed}
							typeReturnFunction="category"
							name="contract"
							value={item?.contract || null}
							onSelect={props?.handleSelectContract}
							filterOptions={(options, params) =>
								filterOptions(options, params, true, "contractName")
							}
							onInputChange={e => props.handleChangeKeySearch(e?.target?.value, "keySearch")}
							noOptionsText={t("general.noOption")}
						/>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t("Reception.dayContract")}
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"contractDate"}
                value={item?.contract?.contractDate || null}
                onChange={(date) =>
                  props.handleDateChange(date, "contractDate")
                }
                disabled
                invalidDateMessage={t("general.invalidDateFormat")}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={t("general.maxDateDefault")}
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Reception.supplier")}
                </span>
              }
              listData={props?.listSupplyUnit}
              setListData={props?.setListSupplyUnit}
              searchFunction={searchByTextDVBH}
              searchObject={props.item?.supplierSearchObject}
              displayLable="name"
              value={item?.supplier || null}
              onSelect={(value) => handleSelectSupplier(value, "SupplyUnit")}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "name")
              }
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              readOnly={item?.isView}
              disabled={item?.isView ? false : item?.contract?.id}
              onInputChange={(event) => props.handleChangeKeySearch(event.target.value, "keySearch")}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              size="small"
              onChange={props.handleChange}
              type="text"
              name="handoverRepresentative"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("Reception.handoverRepresentative")}
                </span>
              }
              className="w-100"
              value={props.item.handoverRepresentative || ""}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span style={{ color: "red" }}>* </span>
                  <span>
                    {t("allocation_asset.status")}</span>
                </span>
              }
              listData={appConst.LIST_STATUS_ASSET_RECEPTION}
              searchFunction={() => { }}
              searchObject={{}}
              displayLable={'name'}
              selectedOptionKey="code"
              value={item?.status ? item?.status : null}
              onSelect={(value) => onChangeComboBox(value, variable.listInputName.status)}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("Reception.receiverDepartment")}
                </span>
              }
              listData={listReceiverDepartment}
              setListData={setListReceiverDepartment}
              searchFunction={getListManagementDepartment}
              searchObject={{}}
              isNoRenderChildren
              displayLable="name"
              showCode="code"
              selectedOptionKey="id"
              typeReturnFunction="list"
              value={item?.receiverDepartment ? item?.receiverDepartment : null}
              name={variable.listInputName.receiverDepartment}
              onSelect={onChangeComboBox}
              filterOptions={(options, params) => {
                params.inputValue = params?.inputValue?.trim();
                return filterAutocomplete(options, params);
              }}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              readOnly={item?.isView || item?.isRoleAssetManager}
            />
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("Reception.receiverPerson")}
                </span>
              }
              listData={listReceiverPerson}
              setListData={setListReceiverPerson}
              searchFunction={personSearchByPage}
              searchObject={props.receiverPersonSearchObject}
              displayLable="displayName"
              selectedOptionKey="personId"
              value={item?.receiverPerson ? item?.receiverPerson : null}
              name={variable.listInputName.receiverPerson}
              onSelect={onChangeComboBox}
              filterOptions={(options, params) => {
                params.inputValue = params?.inputValue?.trim();
                return filterAutocomplete(options, params);
              }}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span style={{ color: "red" }}>* </span>
                  <span>
                    {t("Reception.receiverWarehouse")}</span>
                </span>
              }
              searchFunction={searchByPageStore}
              searchObject={searchObjectStore}
              displayLable={'name'}
              selectedOptionKey="id"
              readOnly={item?.isView}
              value={item?.store ? item?.store : null}
              onSelect={(value) => onChangeComboBox(value, variable.listInputName.store)}
              filterOptions={filterOptions}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              onChange={props.handleChange}
              type="text"
              name="reason"
              label={
                <span>
                  <span className="colorRed"></span>
                  {t("ReceivingAsset.reason")}
                </span>
              }
              className="w-100"
              value={item?.reason || ""}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <TextValidator
              onChange={props.handleChange}
              type="text"
              name="note"
              label={
                <span>
                  <span className="colorRed"></span>
                  {t("Reception.note")}
                </span>
              }
              className="w-100"
              value={item.note || ""}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid container item md={12} sm={12} xs={12} justifyContent="space-between">
            {!item?.isView &&
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={() => props.handleOpenAssetDialog()}
                  style={{ marginRight: "12px" }}
                  disabled={!Boolean(isAllowEditAndAddAsset)}
                >
                  {addBtnLabel[props.item.tabValueTable] || t("general.add_asset")}
                </Button>
                <span className="colorRed">(Khi thay đổi số quyết định hoặc đơn vị cung cấp sẽ xóa toàn bộ tài sản)</span>
              </div>
            }
            <div>
              <b>{t("Reception.amount")}: <span className="colorRed"> {convertNumberPriceRoundUp(props.item?.totalOriginalCost) || 0} VNĐ</span></b>
            </div>
          </Grid>
        </Grid>

        <VoucherItemsTable
          {...props}
          t={t}
          item={props.item}
          handleChangeTabValue={props.handleChangeTabValue}
          removeAssetInlist={props.removeAssetInlist}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item xs={3}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPageCouncil}
            searchObject={councilSearchObject}
            typeReturnFunction="category"
            label={t("Reception.council")}
            listData={props.item?.listCouncil}
            displayLable={"name"}
            onSelect={props.handleSelectCouncil}
            value={props.item?.council}
            InputProps={{
              readOnly: true,
            }}
            onInputChange={e => props.onChangeComboBox(e?.target?.value, variable.listInputName.keySearch)}
            filterOptions={(options, params) => filterOptions(options, params, true, "name")}
            noOptionsText={t("general.noOption")}
          />
        </Grid>

        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <CustomMaterialTable
            data={item?.persons || []}
            columns={columnPerson}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <SignatureTabComponent
          data={initiateSignatures}
          handleChange={props.handleChangeSignature}
          managementDepartmentId={item?.receiverDepartment?.id}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        {!item.isView &&
          <Grid item md={12} sm={12} xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          </Grid>
        }

        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <CustomMaterialTable
            data={props.item.assetDocumentList}
            columns={columnsVoucherFile}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
          />
        </Grid>
      </TabPanel>

    </div>
  );
}
