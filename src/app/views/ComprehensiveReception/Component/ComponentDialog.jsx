import React from "react";
import SupplierDialog from "app/views/Supplier/SupplierDialog";
import ContractDialog from "app/views/Contract/ContractDialog";
import VoucherFilePopup from "../VoucherFilePopup";
import ContentCopyConfirmationDialog from "./ContentCopyConfirmationDialog";
import { appConst } from "app/appConst";

function ComponentDialog(props) {
  const {
    t,
    i18n,
    shouldOpenSupplierAddPopup,
    shouldOpenContractAddPopup,
    handleCloseSupplier,
    handleSelectSupplier,
    handleCloseContract,
    handleSelectContract,
    voucherId,
    shouldOpenCopyDialog,
    handleCloseCopyDialog,
    handleCopyAsset,
    keySearch,
    handleCopyAssetAll
  } = props;

  return (
    <div>
      {/* Add hợp đồng mua sắm */}
      {shouldOpenContractAddPopup && (
        <ContractDialog
          t={t}
          i18n={i18n}
          handleClose={handleCloseContract}
          open={shouldOpenContractAddPopup}
          handleOKEditClose={handleCloseContract}
          handleSelect={handleSelectContract}
          item={{ name: keySearch }}
          contractTypeCode={appConst.TYPES_CONTRACT.CU}
          isHDCUType={true}
        />
      )}

      {shouldOpenSupplierAddPopup && (
        <SupplierDialog
          t={t}
          i18n={i18n}
          open={shouldOpenSupplierAddPopup}
          handleClose={handleCloseSupplier}
          handleOKEditClose={handleCloseSupplier}
          handleSelectDVBH={handleSelectSupplier}
          item={{ name: keySearch }}
          typeCodes={[appConst.TYPE_CODES.NCC_CU]}
          disabledType
        />
      )}
      {props.shouldOpenSelectAssetFilePopup && (
        <VoucherFilePopup
          {...props}
          open={props.shouldOpenSelectAssetFilePopup}
          getAssetDocument={props.getAssetDocument}
          handleClose={props.handleAssetFilePopupClose}
          itemAssetDocument={props.item}
          getAssetDocumentId={props.getAssetDocumentId}
          t={props.t}
          i18n={props.i18n}
          handleAddFileLocal={props.handleAddFileLocal}
          handleEditFileLocal={props.handleEditFileLocal}
          voucherId={voucherId}
          documentType={appConst.documentType.ASSET_DOCUMENT_RECEIPT}
        />
      )}
      {shouldOpenCopyDialog && (
        <ContentCopyConfirmationDialog
          t={t}
          open={shouldOpenCopyDialog}
          onClose={handleCloseCopyDialog}
          // onSubmit={handleCopyAsset}
          onSubmit={handleCopyAssetAll}
        />
      )}
    </div>
  );
}

ComponentDialog.propTypes = {};
export default ComponentDialog;
