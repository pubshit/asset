import {Grid, Icon, IconButton} from "@material-ui/core";
import CustomMaterialTable from "../../CustomMaterialTable";
import React, {useState} from "react";
import {fixedAssetColumns, iatColumns, suppliesColumns} from "../columns";
import {appConst} from "../../../appConst";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {a11yProps, TabPanel} from "../../../appFunction";
import {GroupActionIcons} from "../../Component/Utilities";


function MaterialButton(props) {
  const item = props.item;
  const isShowEditButton = props?.tabValueTable !== appConst.TABS_COMPREHENSIVE_ITEMS.VT.code
  let { selectedRow } = props;
  return (
    <div className="flex flex-center">
      <GroupActionIcons
        item={item}
        onSelect={props.onSelect}
        editIcon={isShowEditButton}
        deleteIcon
        copyIcon={selectedRow?.tableData?.id === item?.tableData?.id}
      />
    </div>
  );
}

function VoucherItemsTable (props) {
  const {t, item = {}} = props;  
  const {TSCD, CCDC, VT} =  appConst.TABS_COMPREHENSIVE_ITEMS;
  const {
    listFixedAsset = [],
    listIat = [],
    listSupplies = [],
    tabValueTable
  } = item;
  const isShowEditButton = tabValueTable !== appConst.TABS_COMPREHENSIVE_ITEMS.VT.code;

  const columns = {
    [TSCD.code]: fixedAssetColumns(t, props),
    [CCDC.code]: iatColumns(t, props),
    [VT.code]: suppliesColumns(t, props),
  };
  
  const data = {
    [TSCD.code]: listFixedAsset,
    [CCDC.code]: listIat,
    [VT.code]: listSupplies,
  };

  let columnsActions = [
    {
      title: t("Reception.columns.action"),
      hidden: item?.isView,
      field: "custom",
      align: "center",
      minWidth: 120,
      render: (rowData) => (
        <GroupActionIcons
          item={rowData}
          editIcon={isShowEditButton}
          deleteIcon
          copyIcon={item?.selectedRow?.tableData?.id === rowData?.tableData?.id}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              props.handleEditAssetInList(rowData);
            } else if (method === appConst.active.delete) {
              props.removeAssetInlist(rowData);
            } else if (method === appConst.active.copy) {
              props.handleOpenCopyDialog(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
  ];

  return (
    <Grid container>
      <AppBar position="static" color="default" className="my-10">
        <Tabs
          className="tabsStatus"
          value={tabValueTable}
          onChange={props.handleChangeTabValue}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab
            value={TSCD.code}
            className="tab"
            label={
              <div className="tabLable">
                <span className="mr-10">{t(TSCD.name)}</span>
                <div className="tabQuantity tabQuantity-success">
                  {item?.listFixedAsset?.length || 0}
                </div>
              </div>
            }
          />
          <Tab
            value={CCDC.code}
            className="tab"
            label={
              <div className="tabLable">
                <span className="mr-10">{t(CCDC.name)}</span>
                <div className="tabQuantity tabQuantity-warning">
                  {item?.listIat?.length || 0}
                </div>
              </div>
            }
          />
          <Tab
            value={VT.code}
            className="tab"
            label={
              <div className="tabLable">
                <span className="mr-10">{t(VT.name)}</span>
                <div className="tabQuantity tabQuantity-info">
                  {item?.listSupplies?.length || 0}
                </div>
              </div>
            }
          />
        </Tabs>
      </AppBar>
      <Grid item xs={12}>
        <CustomMaterialTable
          data={data[tabValueTable] || []}
          columns={columns[tabValueTable] || []}
          columnActions={columnsActions || []}
          options={{
            toolbar: false,
            selection: false,
            paging: false,
            search: false,
            sorting: false,
            showSelectAllCheckbox: false,
            rowStyle: (rowData) => ({
              backgroundColor: item?.selectedRow?.tableData?.id === rowData.tableData.id
                ? "#ccc"
                : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
            }),
            maxBodyHeight: "285px",
            minBodyHeight: "285px",
          }}
          onSelectionChange={props.handleSelectionChange}
          onRowClick={(e, rowData) => props.setDataSate(rowData, "selectedRow")}
        />
      </Grid>
    </Grid>
  )
}

export default VoucherItemsTable;
VoucherItemsTable.propTypes = {
  t: PropTypes.func.isRequired,
  item: PropTypes.object,
}
