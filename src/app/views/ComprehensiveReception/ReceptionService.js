import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_ASSET =
  ConstantList.API_ENPOINT + "/api/asset" + ConstantList.URL_PREFIX;
const API_PATH_MAINTANE =
  ConstantList.API_ENPOINT_ASSET_MAINTANE +
  "/api/receiving-vouchers";

export const deleteItem = (id) => {
  return axios.delete(API_PATH_MAINTANE + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH_MAINTANE + "/" + id);
};

export const addNewOrUpdate = (asset) => {
  if (asset.id) {
    return axios.put(API_PATH_MAINTANE + "/" + asset.id, asset);
  } else {
    return axios.post(API_PATH_MAINTANE, asset);
  }
};

export const getItemsById = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  return axios.get(API_PATH_MAINTANE + "/items", config);
};

export const searchByPage = (searchObject) => {
  let config = {
    params: { ...searchObject },
  };
  var url = API_PATH_MAINTANE + "/search-by-page";
  return axios.get(url, config);
};
export const getNewCode = () => {
  let params = {
    voucherType: ConstantList.VOUCHER_TYPE.ReceivingAsset,
  };
  return axios.get(API_PATH_voucher + "/addNew/getNewCodeByVoucherType", {
    params,
  });
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/fixed-asset/tiep-nhan",
    data: searchObject,
    responseType: "blob",
  });
};

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};

export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const getNewCodeAsset = () => {
  return axios.get(API_PATH_ASSET + "/addNew/getNewCode");
};

//check BHYT
// http://192.168.1.27:8065/asvn/api/fixed-assets/bhyt-ma-may/is-used?code=213213
export const checkBHYT = (bhytMaMay) => {
  return axios.get(
    ConstantList.API_ENPOINT +
      `/api/fixed-assets/bhyt-ma-may/is-used?code=${bhytMaMay}`
  );
};

// http://192.168.1.27:8065/asvn/api/asset/org/new-codes/1
export const getNewCodes = (number) => {
  return axios.get(
    ConstantList.API_ENPOINT + `/api/fixed-assets/new-codes/${number}`
  );
};

export const getCountByStatus = () => {
  var url = API_PATH_MAINTANE + "/count-by-status";
  return axios.get(url);
};
