import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions,
} from "@material-ui/core";
import {
  getUserDepartmentByUserId,
  addNewOrUpdate,
  personSearchByPage,
  findDepartment,
} from "./InstrumentsToolsReceptionService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeDocument,
  getRecevingAssetDocumentById,
  updateRecevingAssetDocument,
} from "../Asset/AssetService";
import ReceptionScrollableTabsButtonForce from "./Component/ReceptionScrollableTabsButtonForce";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "../../appConst";
import AppContext from "app/appContext";
import ComponentDialog from "./Component/ComponentDialog";
import { searchByTextDVBH } from "../Supplier/SupplierService";
import {
  formatDateDto,
  formatDateNoTime,
  getTheHighestRole,
  getUserInformation,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../appFunction";
import { PaperComponent } from "../Component/Utilities";
import InstrumentToolsListEditorDialog from "../InstrumentToolsList/InstrumentToolsListEditorDialog";
import {
  checkDuplicateSerial,
  convertAssetSource,
  convertAssetToFlattenItem,
  convertAttribute,
  convertVoucherItem
} from "./constants";
import { searchByPage as searchByPageAssetStatus } from "../AssetStatus/AssetStatusService";
import ConfirmationDuplicateAssetPopup from "../Component/Reception/ConfirmationDuplicateAssetPopup";
import PropTypes from "prop-types";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";
import { getItemById } from "../InstrumentToolsList/InstrumentToolsListService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});


class InstrumentsToolsReceptionDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    supplier: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: formatDateNoTime(new Date()),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    shouldOpenSelectDepartmentPopup: false,
    shouldOpenSelectSupplyPopup: false,
    totalElements: 0,
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenIatEditorPopup: false,
    voucherCode: "",
    handoverRepresentative: "",
    note: "",
    isIatReception: true,
    shouldOpenSelectAssetFilePopup: false,
    isNew: true,
    listAssetId: [],
    documentType: appConst.documentTypeNumbers.ASSET_DOCUMENT_RECEIPT,
    contract: null,
    isEditLocal: false,
    shouldOpenContractAddPopup: false,
    shouldOpenSupplierAddPopup: false,
    listAssetDocumentId: [],
    assetDocumentList: [],
    voucherId: null,
    idHoSoDK: null,
    listSupplyUnit: [],
    listCodes: [],
    listContract: [],
    listToastFunction: {
      error: toast.error,
      warning: toast.warning,
      success: toast.success,
    },
    store: null,
    selectedRow: null,
    reason: "",
    status: appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY,
    billNumber: null,
    billDate: null,
    listBidding: []
  };
  voucherType = ConstantList.VOUCHER_TYPE.ReceivingAsset;

  handleChange = (event, source) => {
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    const { name, value } = event.target;
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  handleRowDataCellChange = (rowData, valueText) => {
    this.setState({ valueText: rowData.valueText });
  };

  handleFormSubmit = async () => {
    const { id } = this.state;
    const { t } = this.props;
    const { setPageLoading } = this.context;

    if (this.handleValidateSubmit()) {
      return;
    }
    try {
      setPageLoading(true);
      let sendData = this.convertDataSubmit(this.state);
      let res = await addNewOrUpdate(sendData);
      const { code } = res?.data;
      if (isSuccessfulResponse(code)) {
        toast.success(id ? t("general.updateSuccess") : t("general.addSuccess"));
        await this.updateAssetDocument(res?.data?.data?.id);
        this.props.handleOKEditClose();
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleValidateSubmit = () => {
    const {
      assetVouchers = [],
      handoverRepresentative,
      receiverDepartment,
    } = this.state;

    if (handoverRepresentative === null) {
      toast.warning("Vui lòng chọn đại diện bên bàn giao(Thông tin phiếu).");
      return true;
    }
    if (receiverDepartment === null) {
      toast.warning("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).");
      return true;
    }
    if (assetVouchers?.length === 0) {
      toast.warning("Vui lòng chọn tài sản tiếp nhận(Thông tin phiếu).");
      return true;
    }

    return false;
  }

  convertDataSubmit = state => {
    return {
      assetClass: appConst.assetClass.CCDC,
      assetVouchers: state?.assetVouchers?.map(item => {
        return {
          assetId: item.asset?.id,
          ...convertAssetToFlattenItem(item.asset || {}),
          voucherId: item?.voucherId,
          id: item?.id,
          decisionCode: state?.decisionCode?.decisionCode,
        }
      }),
      billDate: formatDateDto(state?.billDate),
      billNumber: state?.billNumber,
      contractDate: state?.contract?.contractDate,
      contractId: state?.contract?.id,
      handoverRepresentative: state?.handoverRepresentative,
      id: state?.id,
      issueDate: formatDateDto(state?.issueDate),
      note: state?.note,
      reason: state?.reason,
      receiverDepartmentId: state?.receiverDepartment?.id,
      receiverPersonId: state?.receiverPerson?.id,
      status: state?.status?.indexOrder,
      storeId: state?.store?.id,
      supplierId: state?.supplier?.id,
      type: state?.type,
      voucherCode: state?.voucherCode,
      decisionCode: state?.decisionCode?.decisionCode,
    }
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleSelectAsset = async (item) => {
    const { assetVouchers = [], currentIndex } = this.state;
    let _assetSources = [...assetVouchers];
    const { t } = this.props;
    const existItem = _assetSources?.find(
      asset => (asset.id === item?.id || asset.asset?.assetId === item?.assetId)
        && asset.tableData?.id === currentIndex
    )

    let convertedItem = convertAssetToFlattenItem(item);
    let newItem = {
      ...convertedItem,
      asset: item,
    }

    if (checkDuplicateSerial(_assetSources, item) && !existItem) {
      return toast.warning(t("InstrumentsToolsReception.notification.duplicateSerialNumber"));
    }

    if (existItem) {
      _assetSources?.splice(_assetSources?.indexOf(existItem), 1, newItem);
    } else {
      _assetSources.push(newItem);
    }

    this.setState({ assetVouchers: _assetSources }, this.handleDialogAssetEditorClose);
  };

  removeAssetInlist = async (rowData) => {
    const { assetVouchers } = this.state;
    let index = assetVouchers?.findIndex(item => item.tableData?.id === rowData?.tableData?.id);

    assetVouchers.splice(index, 1);

    this.setState({
      assetVouchers: [...assetVouchers],
    });
  };

  componentDidMount() {
    let roles = getTheHighestRole();
    let { departmentUser } = getUserInformation();
    let { item, isView, isAllowEdit } = this.props;
    let newItem = item || {}
    let receiverDepartment = departmentUser || null;

    if (newItem?.id) {
      newItem = convertVoucherItem(item);
      receiverDepartment = newItem?.receiverDepartment;
    }

    this.setState({
      ...newItem,
      ...roles,
      isView,
      isAllowEdit,
      receiverDepartment,
    }, this.handleGetListAssetStatus);
    if (item?.id) {
      this.getAssetDocument();
    }
  }

  handleGetListAssetStatus = () => {
    searchByPageAssetStatus({ ...appConst.OBJECT_SEARCH_MAX_SIZE }).then((res) => {
      this.setState({
        listAssetStatus: res?.data?.content || [],
      })
    }).catch((e) => {
      toast.error(t("InstrumentsToolsReception.searchAssetStatusError"));
    })
  }

  handleOpenIatDialog = async () => {
    let {
      issueDate,
      receiverDepartment,
      store,
      supplier,
      listAssetStatus = [],
      contract,
      billDate,
      billNumber,
      decisionCode
    } = this.state;

    this.setState({
      item: {
        allocationFor: appConst.LOAI_CAP_PHAT.PHONG_BAN,
        dateOfReception: issueDate,
        dayStartedUsing: issueDate,
        managementDepartment: receiverDepartment,
        supplyUnit: supplier,
        contractDate: contract?.contractDate,
        store,
        listAssetStatus,
        contract,
        ngayHoaDon: billDate,
        lotNumber: billNumber,
        isErrorGeneralInfomation: false,
        isErrorOriginTools: false,
        isErrorInstrumentToolProperties: false,
        decisionCode
      },
      shouldOpenIatEditorPopup: true,
      isAddAsset: true,
    });
  };

  handleEditAssetInList = async (rowData) => {
    this.setState({
      item: {
        ...rowData?.asset,
        decisionCode: this?.state?.decisionCode,
        isErrorGeneralInfomation: false,
        isErrorOriginTools: false,
        isErrorInstrumentToolProperties: false,
      },
      currentIndex: rowData?.tableData?.id,
      shouldOpenIatEditorPopup: true,
      isAddAsset: true,
      isEditLocal: true,
    });
  };

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleSelectReceiverDepartment = (item) => {
    let { assetVouchers, receiverDepartment, receiverPerson } = this.state;
    let updatedAssetVouchers = assetVouchers.map((element) => ({
      ...element,
      asset: {
        ...element.asset,
        useDepartment: { id: item.id, name: item.text },
      },
    }));
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: receiverDepartment.id,
    };

    personSearchByPage(receiverPersonSearchObject).then((res) => {
      let found = res?.data?.content?.some((person) => receiverPerson && receiverPerson.id === person.id);

      if (found) {
        this.setState({
          receiverDepartment: {
            id: item.id,
            name: item.text
          },
          assetVouchers: updatedAssetVouchers,
        },
          this.handleReceiverDepartmentPopupClose
        );
      } else {
        this.setState({
          receiverDepartment: { id: item.id, name: item.text },
          assetVouchers: updatedAssetVouchers,
          receiverPerson: null,
        },
          this.handleReceiverDepartmentPopupClose
        );
      }
    });
  };

  handleHandoverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenHandoverDepartmentPopup: false,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: { id: item.id, name: item.text },
    },
      this.handleHandoverDepartmentPopupClose
    );
  };

  selectHandoverPerson = async (item) => {
    try {
      const departmentData = await getUserDepartmentByUserId(item.userId);
      if (departmentData.data.department) {
        const department = await findDepartment(
          departmentData.data.department.id
        );
        this.setState({ handoverDepartment: department.data });
      }
      this.setState({ handoverPerson: item });
    } catch (error) {
      console.error(error);
    }
  };

  handleSelectReceiverPerson = async (item) => {
    this.setState({
      receiverPerson: { id: item.id, displayName: item.displayName },
    });
    const userData = await getUserDepartmentByUserId(item.userId);
    if (userData.data.department) {
      const departmentData = await findDepartment(userData.data.department.id);
      const department = {
        id: departmentData.data.id,
        name: `${departmentData.data.name} - ${departmentData.data.code}`,
        text: `${departmentData.data.name} - ${departmentData.data.code}`,
      };
      this.setState({ receiverDepartment: department });
    }
    this.handleReceiverPersonPopupClose();
  };

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false,
    });
  };

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true,
      item: {},
    });
  };

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name };
    this.setState({ receiverDepartment: department }, function () {
      this.handleSelectDepartmentPopupClose();
    });
  };

  handleSelectDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectDepartmentPopup: false,
    });
  };

  setSupplier = (value) => {
    this.setState({
      supplier: value
    })
  }

  handleSelectSupply = (item) => {
    let supplier = { id: item.id, name: item.name };
    this.setState({ supplier, shouldOpenSelectSupplyPopup: false });
  };

  handleSelectSupplyPopupClose = () => {
    this.setState({
      shouldOpenSelectSupplyPopup: false,
    });
  };

  handleDialogAssetEditorClose = () => {
    this.setState({
      isEditLocal: false,
      shouldOpenIatEditorPopup: false,
    });
  };

  handleOKEditAssetClose = () => {
    this.setState({ shouldOpenIatEditorPopup: false });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      idHoSoDK: null,
      shouldOpenSelectAssetFilePopup: false,
    });
  };

  handleAddAssetDocumentItem = async () => {
    const type = appConst.documentType.ASSET_DOCUMENT_RECEIPT;
    try {
      const result = await getNewCodeDocument(type);
      if (result != null && result.data && result.data.code) {
        const item = result.data.data;
        this.setState({
          item: item,
          shouldOpenSelectAssetFilePopup: true,
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  handleRowDataCellDelete = (rowData) => {
    const { attributes } = this.state;
    if (attributes && attributes.length > 0) {
      const attributesLength = attributes.length;
      let foundIndex = null;
      for (let index = 0; index < attributesLength; index++) {
        if (
          attributes[index].attribute &&
          attributes[index].attribute.id === rowData.attribute.id
        ) {
          foundIndex = index;
          break;
        }
      }
      if (foundIndex !== null) {
        const updatedAttributes = attributes.slice();
        updatedAttributes.splice(foundIndex, 1);
        this.setState({ attributes: updatedAttributes });
      }
    }
  };

  handleRowDataCellEditAssetFile = async (rowData) => {
    try {
      const res = await getAssetDocumentById(rowData.id);
      const { data, code } = res?.data;
      if (!isSuccessfulResponse(code)) {
        handleThrowResponseMessage(res);
      }
      this.setState({
        item: data,
        idHoSoDK: rowData.id,
        shouldOpenSelectAssetFilePopup: true,
        isEditAssetDocument: true,
      })
    } catch (error) {
      console.error(error);
    }
  };

  handleRowDataCellDeleteAssetFile = (rowData) => {
    let { listAssetDocumentId, assetDocumentList } = this.state;
    listAssetDocumentId.splice(listAssetDocumentId?.indexOf(rowData?.id), 1)
    assetDocumentList.splice(assetDocumentList?.indexOf(rowData?.id), 1)
    this.setState({ listAssetDocumentId, assetDocumentList });
  };

  getAssetDocument = () => {
    getRecevingAssetDocumentById(this.props.item?.id).then((response) => {
      const { data } = response;
      if (data) {
        this.setState({
          assetDocumentList: data?.data,
          totalElements: data?.total,
        });
      }
    });
  };

  updateAssetDocument = async (id) => {
    try {
      const payload = {
        assetDocumentIds: this.state?.assetDocumentList?.map(i => i?.id),
        documentType: appConst.documentType.ASSET_DOCUMENT_RECEIPT
      }
      await updateRecevingAssetDocument(id, payload);
    } catch (error) {

    }
  };

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;
    if (listAssetDocumentId != null && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach((id) => deleteAssetDocumentById(id));
    }
  };

  handleChangeComboBox = (value, name) => {
    if (name === variable.listInputName.receiverDepartment) {
      this.setState({
        store: null,
        receiverPerson: null,
        listReceiverPerson: [],
        listStore: [],
      });
    }

    if (name === "decisionCode") {
      this.setState({
        assetVouchers: []
      });
    }

    this.setState({
      [name]: value
    }, () => {
      this.handleUpdateAssetVouchersByVoucherDetails(value, name)
    });
  };

  handleSelectSupplier = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenSupplierAddPopup: true,
      });
    }
    this.setState({
      supplier: item,
      assetVouchers: [],
      // listContract: [],
    });
  };

  handleCloseSupplier = () => {
    this.setState({
      shouldOpenSupplierAddPopup: false,
    });

    this.updateListSupplier()
  };

  handleSelectContract = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenContractAddPopup: true,
      });
      return;
    }
    this.setState({
      contract: {
        ...item,
        id: item?.contractId || item?.id
      },
      supplier: this?.state?.isAllowEdit ? this?.state?.supplier : item?.supplierId || item?.supplier?.id
        ? {
          id: item?.supplierId || item?.supplier?.id,
          name: item?.supplierName || item?.supplier?.name,
        }
        : null,
      assetVouchers: [],
    });
  };

  handleUpdateAssetVouchersByVoucherDetails = (item, fieldName) => {
    let { assetVouchers } = this.state;
    if (fieldName === variable.listInputName.receiverDepartment) {
      assetVouchers?.map(assetVoucher => ({
        ...assetVoucher,
        asset: {
          ...assetVoucher.asset,
          managementDepartment: item,
        },
      }))
    } else if (fieldName === variable.listInputName.contract) {
      assetVouchers?.map(assetVoucher => ({
        ...assetVoucher,
        asset: {
          ...assetVoucher.asset,
          contract: item,
          contractDate: item?.contractDate,
          supplyUnit: item?.supplier,
        },
      }))
    } else if (fieldName === variable.listInputName.store) {
      assetVouchers?.map(assetVoucher => ({
        ...assetVoucher,
        asset: {
          ...assetVoucher.asset,
          store: item,
        },
      }))
    }

    this.setState({ assetVouchers });
  }

  handleCloseContract = () => {
    this.setState({
      shouldOpenContractAddPopup: false,
    });
  };

  updateListSupplier = async () => {
    let { t } = this.props;
    try {
      let supplierSearchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        typeCodes: [appConst.TYPE_CODES.NCC_CU],
      };
      let res = await searchByTextDVBH(supplierSearchObject)
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({
          listSupplyUnit: res.data.content
        })
      }
    }
    catch (e) {
      toast.error(t("toastr.error"))
    }
  }

  setListSupplyUnit = (value) => {
    this.setState({
      listSupplyUnit: value
    })
  }

  getItemForMapping = async (id) => {
    try {
      const data = await getItemById(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        return data?.data?.data || {};
      }
      return {};
    } catch (error) {
      return {};
    }
  }

  handleMapDataAsset = async (event, itemSelected, item) => {
    const value = await this.getItemForMapping(itemSelected?.id);
    let { billDate, billNumber, decisionCode } = this.state;
    let assetSourcesUpdated = convertAssetSource(value?.assetSources);
    let attributesUpdated = convertAttribute(value?.attributes);

    if (item?.product?.id && decisionCode) {
      value.decisionCode = decisionCode;
      value.product = item.product;
    };

    const newData = {
      ...value,
      ngayHoaDon: billDate,
      lotNumber: billNumber,
      assetGroup: value?.assetGroup?.id
        ? {
          id: value?.assetGroup?.id,
          name: value?.assetGroup?.name
        }
        : null,
      manufacturer: value?.manufacturer?.id
        ? {
          name: value?.manufacturer?.name,
          id: value?.manufacturer?.id,
          code: value?.manufacturer?.code
        }
        : null,
      unit: value?.unit?.id
        ? {
          code: value?.unit?.code,
          id: value?.unit?.id,
          name: value?.unit?.name
        }
        : null,
      medicalEquipment: value?.medicalEquipment?.id
        ? {
          code: value?.medicalEquipment?.code,
          id: value?.medicalEquipment?.id,
          name: value?.medicalEquipment?.name
        }
        : null,
      product: value?.product?.id
        ? {
          name: value?.product?.name,
          id: value?.product?.id,
        }
        : null,
      shoppingForm: value?.shoppingForm?.id
        ? {
          name: value?.shoppingForm?.name,
          id: value?.shoppingForm?.id,
          code: value?.shoppingForm?.code,
        }
        : null,
      assetSources: assetSourcesUpdated,
      attributes: attributesUpdated,
      id: null,
      depreciationRate: value?.depreciationRate,
      useDepartment: "",
      allocationFor: "",
      decisionCode,
    };

    // Xóa các thuộc tính không cần thiết.
    for (const prop of [
      "status",
      "code",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      "accumulatedDepreciationAmount",
      "lastYearOfDepreciation",
      "useDepartment",
      "usePerson",
      "store",
      "useDepartmentId",
      "useDepartmentName",
      "useDepartmentCode",
      "serialNumber",
      "serialNumbers",
    ]) {
      delete newData[prop];
    }

    // Cập nhật state của component.
    this.setState({ item: newData });
  };

  handleAddFileLocal = (listFile) => {
    let newList = []
    newList.push(listFile)
    this.setState({
      assetDocumentList: [...this.state.assetDocumentList, ...newList],
    })
  }

  handleUpdateFile = (file) => {
    if (!file?.id) return;

    this.setState({
      assetDocumentList: this.state.assetDocumentList?.map(x => {
        if (x?.id === file?.id) {
          return { ...file }
        } else {
          return { ...x }
        }
      }),
    })
  }

  handleRowClick = (e, rowData) => {
    let isExist = rowData?.tableData?.id === this.state.selectedRow?.tableData?.id
    this.setState({ selectedRow: isExist ? null : rowData });
  }

  handleDuplicateAsset = (rowData) => {
    this.setState({
      selectedRow: rowData,
      shouldOpenDuplicateAssetPopup: true,
    })
  }

  handleConfirmDuplicateAsset = (item) => {
    let { assetVouchers = [], selectedRow = {} } = this.state;
    let numberOfClones = +item?.quantity || 0;
    let newArray = [];
    let clonedItems = [];
    let newItem = {
      ...selectedRow,
      asset: {
        ...selectedRow?.asset,
        serialNumber: null,
        managementCode: null,
        code: null,
      },
      id: null
    };

    clonedItems = Array(numberOfClones).fill(newItem);
    newArray = assetVouchers.concat(clonedItems).map((item, index) => ({
      ...item,
      tableData: {
        ...item.tableData,
        id: index + 1
      }
    }))

    this.setState({
      assetVouchers: newArray
    }, this.handleCloseConfirmPopup);
  }

  handleCloseConfirmPopup = () => {
    this.setState({
      shouldOpenDuplicateAssetPopup: false,
      selectedRow: null,
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState?.assetVouchers !== this?.state?.assetVouchers) {
      this.setState({ totalOriginalCost: this?.state?.assetVouchers?.reduce((total, item) => total += Number(item?.asset?.originalCost), 0) });
    }
  }
  handleChangeAssetInList = async (e, rowData) => {
    let { t } = this.props;
    let { assetVouchers } = this.state;
    let updatedAssetVouchers = [...assetVouchers];

    const index = updatedAssetVouchers.findIndex(
      (x) => x.tableData.id === rowData?.tableData?.id
    );

    let { name, value } = e.target;
    if (["managementCode", "serialNumber"].includes(name)) {
      updatedAssetVouchers[index] = {
        ...updatedAssetVouchers[index],
        asset: {
          ...updatedAssetVouchers[index].asset,
          [name]: value
        }
      };
    } else {
      updatedAssetVouchers[index][name] = value;
    }
    this.setState({
      assetVouchers: updatedAssetVouchers,
    });
  };
  render() {
    let { open, t, i18n, item } = this.props;
    let {
      isView,
      shouldOpenIatEditorPopup,
      shouldOpenDuplicateAssetPopup,
      receiverDepartment,
      isAllowEdit
    } = this.state;

    let contractSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
    };
    let supplierSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      typeCodes: [appConst.TYPE_CODES.NCC_CU],
    };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: receiverDepartment?.id
    };

    return (
      <>
        <Dialog
          open={open}
          PaperComponent={PaperComponent}
          maxWidth="lg"
          fullWidth
          scroll="paper"
        >
          <CustomValidatorForm
            ref="form"
            onSubmit={this.handleFormSubmit}
            className="validator-form-scroll-dialog"
          >
            <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
              <span className="">
                {!item?.id ? t("ReceivingAsset.addPhieuTiepNhan") : t("ReceivingAsset.updatePhieuTiepNhan")}
              </span>
            </DialogTitle>

            <DialogContent style={{ minHeight: "450px" }}>
              <ReceptionScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                contractSearchObject={contractSearchObject}
                supplierSearchObject={supplierSearchObject}
                receiverPersonSearchObject={receiverPersonSearchObject}
                handleDateChange={this.handleDateChange}
                handleEditAssetInList={this.handleEditAssetInList}
                handleChange={this.handleChange}
                handleSelectReceiverPerson={this.handleSelectReceiverPerson}
                handleOpenIatDialog={this.handleOpenIatDialog}
                removeAssetInlist={this.removeAssetInlist}
                handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
                handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                onChangeComboBox={this.handleChangeComboBox}
                itemEdit={this.state.item}
                handleSelectContract={this.handleSelectContract}
                handleSelectSupplier={this.handleSelectSupplier}
                listSupplyUnit={this.state.listSupplyUnit}
                setListSupplyUnit={this.setListSupplyUnit}
                handleRowClick={this.handleRowClick}
                handleDuplicateAsset={this.handleDuplicateAsset}
                handleChangeAssetInList={this.handleChangeAssetInList}
              />
            </DialogContent>
            <DialogActions>
              <div className="flex flex-space-between flex-middle">
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={this.props.handleClose}
                >
                  {t("general.cancel")}
                </Button>
                {(!isView || isAllowEdit) && (
                  <Button variant="contained" color="primary" type="submit" className="mr-12">
                    {!item?.id ? t("general.save") : t("general.update")}
                  </Button>
                )}
              </div>
            </DialogActions>
          </CustomValidatorForm>
        </Dialog>

        {shouldOpenIatEditorPopup && (
          <InstrumentToolsListEditorDialog
            open={shouldOpenIatEditorPopup}
            t={t}
            i18n={i18n}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleDialogAssetEditorClose}
            handleOKEditClose={this.handleOKEditAssetClose}
            item={this.state.item}
            isIatReception
            // itemAssetDocument={{
            //   ...this.state,
            //   contract: this.state.contract
            //     ? {
            //       ...this.state.contract,
            //       contractName:
            //         this.state.contract?.contractCode +
            //         " - " +
            //         (this.state.contract?.contractText || ""),
            //     }
            //     : null,
            // }}
            // isAddAsset={this.state.isAddAsset}
            // isEditLocal={this.state.isEditLocal}
            handleMapDataAsset={this.handleMapDataAsset}
          />
        )}
        {shouldOpenDuplicateAssetPopup && (
          <ConfirmationDuplicateAssetPopup
            open={shouldOpenDuplicateAssetPopup}
            handleSelect={this.handleConfirmDuplicateAsset}
            handleClose={this.handleCloseConfirmPopup}
            assetClass={appConst.assetClass.CCDC}
          />
        )}

        <ComponentDialog
          {...this.state}
          t={t}
          i18n={i18n}
          voucherId={this.state?.id}
          handleCloseContract={this.handleCloseContract}
          handleSelectContract={this.handleSelectContract}
          handleCloseSupplier={this.handleCloseSupplier}
          handleSelectSupplier={this.handleSelectSupplier}
          getAssetDocument={this.getAssetDocument}
          handleAssetFilePopupClose={this.handleAssetFilePopupClose}
          handleAddFileLocal={this.handleAddFileLocal}
          handleUpdateFile={this.handleUpdateFile}
          setSupplier={this.setSupplier}
        />
      </>
    );
  }
}

export default InstrumentsToolsReceptionDialog;
InstrumentsToolsReceptionDialog.contextType = AppContext;
InstrumentsToolsReceptionDialog.propTypes = {
  t: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleOKEditClose: PropTypes.func.isRequired,
  item: PropTypes.any,
  isView: PropTypes.bool,
}
