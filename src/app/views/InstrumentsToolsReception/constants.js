import moment from "moment";
import {
  convertNumberPriceRoundUp,
  formatDateDto,
  formatTimestampToDate,
} from "../../appFunction";
import { appConst } from "../../appConst";
import { TextValidator } from "react-material-ui-form-validator";

export const tableColumns = (t, props) => [
  {
    title: t("general.index"),
    field: "stt",
    maxWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("receivingAsset.code"),
    field: "voucherCode",
    minWidth: 150,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("InstrumentsToolsReception.status"),
    field: "status",
    minWidth: 150,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: props?.renderStatus,
  },
  {
    title: t("receivingAsset.issueDate"),
    field: "issueDate",
    minWidth: 120,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) =>
      rowData.issueDate ? moment(rowData.issueDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("Asset.decisionCode"),
    field: "decisionCode",
    align: "center",
    minWidth: 140,
  },
  {
    title: t("receivingAsset.contractNumber"),
    field: "billNumber",
    align: "left",
    minWidth: 150,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("InstrumentsToolsReception.contractDate"),
    field: "",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => formatTimestampToDate(rowData?.billDate) || "",
  },
  {
    title: t("InstrumentsToolsReception.supplier"),
    field: "supplierName",
    minWidth: 350,
    maxWidth: 500,
  },

  {
    title: t("InstrumentsToolsReception.receiverDepartment"),
    field: "receiverDepartmentName",
    minWidth: 200,
  },
];

export const dialogColumns = (t, props) => [
  {
    title: t("InstrumentsToolsReception.columns.index"),
    field: "",
    minWidth: "40px",
    align: "center",
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("InstrumentsToolsReception.columns.assetCode"),
    field: "asset.code",
    align: "center",
    hidden: !props?.item?.id,
    minWidth: "130px",
  },
  {
    title: t("InstrumentsToolsReception.columns.assetName"),
    field: "asset.name",
    align: "left",
    minWidth: "200px",
  },
  {
    title: t("InstrumentsToolsReception.columns.model"),
    field: "asset.model",
    align: "center",
    minWidth: "130px",
  },
  {
    title: t("Asset.managementCode"),
    field: "asset.managementCode",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="managementCode"
          className="w-100"
          value={rowData?.asset?.managementCode}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.serialNumber"),
    field: "asset.serialNumber",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="serialNumber"
          className="w-100"
          value={rowData?.asset?.serialNumber}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
          disabled={rowData?.asset?.quantity > 1}
        />
      );
    },
  },
  {
    title: t("InstrumentsToolsReception.quantity"),
    field: "asset.quantity",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.quantity
        ? convertNumberPriceRoundUp(rowData?.asset?.quantity)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("Asset.unitPrice"),
    field: "asset.unitPrice",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.unitPrice
        ? convertNumberPriceRoundUp(rowData?.asset?.unitPrice)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("InstrumentsToolsReception.amount"),
    field: "asset.amount",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("InstrumentsToolsReception.columns.note"),
    field: "asset.note",
    align: "left",
    minWidth: "200px",
  },
];

export const subTableColumns = (t, subPage, subRowsPerPage) => [
  {
    title: t("Asset.stt"),
    field: "",
    align: "left",
    width: "50px",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => subPage * subRowsPerPage + (rowData.tableData.id + 1),
  },
  {
    title: t("InstrumentToolsList.code"),
    field: "code",
    minWidth: 120,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("InstrumentToolsList.name"),
    field: "name",
    align: "left",
    minWidth: 250,
    maxWidth: 400,
  },
  {
    title: t("Asset.managementCode"),
    field: "managementCode",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.serialNumber"),
    field: "serialNumber",
    align: "center",
    minWidth: 120,
  },
  {
    title: t("Asset.model"),
    field: "model",
    align: "center",
    minWidth: 120,
  },
  {
    title: t("Asset.yearIntoUseTable"),
    field: "yearPutIntoUse",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.yearOfManufacture"),
    field: "yearOfManufacture",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.manufacturer"),
    field: "manufacturerName",
    align: "left",
    minWidth: 150,
    maxWidth: 200,
  },
  {
    title: t("InstrumentsToolsReception.quantity"),
    field: "quantity",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.quantity ? convertNumberPriceRoundUp(rowData?.quantity) : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("Asset.unitPrice"),
    field: "unitPrice",
    align: "left",
    minWidth: 150,
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) => convertNumberPriceRoundUp(rowData.unitPrice) || "",
  },
  {
    title: t("InstrumentsToolsReception.amount"),
    field: "originalCost",
    align: "right",
    minWidth: "150px",
    render: (rowData) =>
      rowData?.originalCost
        ? convertNumberPriceRoundUp(rowData?.originalCost)
        : "",
    cellStyle: {
      textAlign: "right",
    },
  },
  {
    title: t("Asset.note"),
    field: "note",
    align: "left",
    minWidth: 180,
  },
];

export const convertAssetToFlattenItem = (state) => {
  let assetSourcesUpdated = state.assetSources.map((item) => {
    item.assetSourceId = item?.assetSource?.id;
    item.assetSourceName = item?.assetSource?.name;
    return item;
  });
  let data = {
    accumulatedDepreciationAmount: state?.accumulatedDepreciationAmount,
    allocationPeriod: state?.allocationPeriod,
    allocationPeriodValue: state?.allocationPeriodValue,
    assetGroupId: state?.assetGroupId
      ? state?.assetGroupId
      : state?.assetGroup?.id,
    assetSources: assetSourcesUpdated,
    attributes: state?.attributes,
    carryingAmount: Number(state?.carryingAmount),
    circulationNumber: state?.circulationNumber,
    code: state?.code,
    countryOriginId: state?.countryOriginId
      ? state?.countryOriginId
      : state?.countryOrigin?.id,
    contract: state?.contract,
    contractId: state?.contract?.id,
    contractDate: state?.contract?.contractDate
      ? formatDateDto(state?.contract?.contractDate)
      : null,
    supplyUnit: state?.contract?.supplier,
    dateOfReception: state?.dateOfReception
      ? formatDateDto(state?.dateOfReception)
      : null,
    dayStartedUsing: state?.dayStartedUsing
      ? formatDateDto(state?.dayStartedUsing)
      : null,
    depreciationDate: state?.depreciationDate
      ? formatDateDto(state?.depreciationDate)
      : null,
    depreciationPeriod: state?.depreciationPeriod,
    depreciationRate: state?.depreciationRate,
    installationLocation: state?.installationLocation,
    invoiceNumber: state?.invoiceNumber,
    isBuyLocally: state?.isBuyLocally,
    isManageAccountant: state?.isManageAccountant,
    isTemporary: state?.isTemporary,
    listAssetDocumentId: state?.listAssetDocumentId,
    lotNumber: state?.lotNumber,
    madeIn: state?.madeIn,
    managementCode: state?.managementCode,
    managementDepartmentId: state?.managementDepartmentId
      ? state?.managementDepartmentId
      : state?.managementDepartment?.id,
    manufacturerId: state?.manufacturerId
      ? state?.manufacturerId
      : state?.manufacturer?.id,
    medicalEquipmentId: state?.medicalEquipmentId
      ? state?.medicalEquipmentId
      : state?.medicalEquipment?.id,
    model: state?.model,
    name: state?.name,
    ngayHoaDon: state?.ngayHoaDon,
    note: state?.note,
    numberOfAllocations: state?.numberOfAllocations,
    numberOfAllocationsRemaining: state?.numberOfAllocationsRemaining,
    originalCost: state?.originalCost,
    productId: state?.productId ? state?.productId : state?.product?.id,
    qrcode: state?.qrcode,
    quantityAsset: +state?.quantityAsset,
    quantity: +state?.quantity,
    receiverPersonId: state?.receiverPersonId
      ? state?.receiverPersonId
      : state?.receiverPerson?.id,
    riskClassification: state?.riskClassification,
    serialNumber: state?.serialNumber,
    serialNumbers: state?.serialNumbers,
    shoppingFormId: state?.shoppingFormId
      ? state?.shoppingFormId
      : state?.shoppingForm?.id,
    storeId: state?.storeId ? state?.storeId : state?.store?.id,
    supplyUnitId: state?.supplyUnitId
      ? state?.supplyUnitId
      : state?.supplyUnit?.id,
    unitId: state?.unitId ? state?.unitId : state?.unit?.id,
    unitPrice: state?.unitPrice,
    useDepartmentId: state?.useDepartmentId
      ? state?.useDepartmentId
      : state?.useDepartment?.id,
    useDepartmentLateId: state?.useDepartmentLateId
      ? state?.useDepartmentLateId
      : state?.useDepartmentLate?.id,
    usePersonId: state?.usePersonId || state?.usePerson?.personId,
    usedTime: state?.usedTime,
    warrantyExpiryDate: state?.warrantyExpiryDate
      ? formatDateDto(state?.warrantyExpiryDate)
      : null,
    warrantyMonth: state?.warrantyMonth,
    yearOfManufacture: state?.yearOfManufacture,
    yearPutIntoUse: state?.yearPutIntoUse,
    acceptanceDate: state?.acceptanceDate
      ? formatDateDto(state?.acceptanceDate)
      : null,
    decisionCode: state?.decisionCode,
    symbolCode: state?.symbolCode,
  };
  return data;
};

export const convertVoucherItem = (item) => {
  return {
    ...item,
    ...convertItemFromFlattenToObject(item),
    assetVouchers: item?.assetVouchers?.map((assetVoucher) => ({
      ...assetVoucher,
      asset: {
        ...assetVoucher.asset,
        ...convertItemFromFlattenToObject(assetVoucher.asset),
        note: assetVoucher.asset?.note || assetVoucher.note,
      },
    })),
    receiverDepartment: item?.receiverDepartmentId
      ? {
          id: item?.receiverDepartmentId,
          name: item?.receiverDepartmentName,
        }
      : null,
    receiverPerson: item?.receiverPersonId
      ? {
          id: item?.receiverPersonId,
          displayName: item?.receiverPersonName,
        }
      : null,
    decisionCode: {
      id: item?.bidId,
      decisionCode: item?.decisionCode,
    },
    status: appConst.LIST_STATUS_ASSET_RECEPTION.find(
      (x) => x.indexOrder === item.status
    ),
  };
};

const convertItemFromFlattenToObject = (item) => {
  return {
    ...item,
    attributes:
      item?.assetSources?.length > 0
        ? convertAttribute(item?.attributes)
        : null,
    assetGroup: item?.assetGroupId
      ? {
          id: item?.assetGroupId,
          name: item?.assetGroupName,
        }
      : null,
    assetSources:
      item?.assetSources?.length > 0
        ? convertAssetSource(item?.assetSources)
        : null,
    contract: item?.contractId
      ? {
          id: item?.contractId,
          contractId: item?.contractId,
          contractName: item.contractName,
          contractDate: item.contractDate,
          contractCode: item.contractCode,
          supplier: item?.supplierId
            ? {
                id: item?.supplierId,
                name: item?.supplierName,
              }
            : null,
        }
      : null,
    managementDepartment: item?.managementDepartmentId
      ? {
          id: item?.managementDepartmentId,
          name: item?.managementDepartmentName,
        }
      : null,
    manufacturer: item?.manufacturerId
      ? {
          name: item?.manufacturerName,
          id: item?.manufacturerId,
          code: item?.manufacturerCode,
        }
      : null,
    medicalEquipment: item?.medicalEquipmentId
      ? {
          code: item?.medicalEquipmentCode,
          id: item?.medicalEquipmentId,
          name: item?.medicalEquipmentName,
        }
      : null,
    product: item?.productId
      ? {
          name: item?.productName,
          id: item?.productId,
        }
      : null,
    shoppingForm: item?.shoppingFormId
      ? {
          name: item?.shoppingFormName,
          id: item?.shoppingFormId,
          code: item?.shoppingFormCode,
        }
      : null,
    supplier: item?.supplierId
      ? {
          id: item?.supplierId,
          name: item?.supplierName,
        }
      : null,
    supplyUnit: item?.supplierId
      ? {
          id: item?.supplierId,
          name: item?.supplierName,
        }
      : null,
    store: item?.storeId
      ? {
          id: item?.storeId,
          name: item?.storeName,
        }
      : null,
    unit: item?.unitId
      ? {
          code: item?.unitCode,
          id: item?.unitId,
          name: item?.unitName,
        }
      : null,
  };
};

export const convertAssetSource = (assetSources) =>
  assetSources?.map((item) => ({
    ...item,
    assetSource: {
      code: item?.assetSourceCode,
      id: item?.assetSourceId,
      name: item?.assetSourceName,
    },
  }));

export const convertAttribute = (attributes) =>
  attributes?.map((attribute) => ({
    ...attribute,
    attribute: {
      code: attribute?.attributeCode,
      id: attribute?.attributeId,
      name: attribute?.attributeName,
    },
  }));

export const checkDuplicateSerial = (assetVouchers, item) => {
  return (
    item?.serialNumber &&
    assetVouchers?.some(
      (assetVoucher) => assetVoucher.asset?.serialNumber === item?.serialNumber
    )
  );
};
