import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
import {ROUTES_PATH} from "../../appConst";

const InstrumentsToolsReception = EgretLoadable({
  loader: () => import("./InstrumentsToolsReception"),
});
const ViewComponent = withTranslation()(InstrumentsToolsReception);

const InstrumentsToolsReceptionRoutes = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.IAT_MANAGEMENT.RECEPTION,
    exact: true,
    component: ViewComponent
  }
];

export default InstrumentsToolsReceptionRoutes;