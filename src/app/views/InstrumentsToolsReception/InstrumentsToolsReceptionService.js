import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/receiving-vouchers";

export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const addNewOrUpdate = (asset) => {
  if (asset.id) {
    return axios.put(API_PATH + "/" + asset.id, asset);
  } else {
    return axios.post(API_PATH, asset);
  }
};

export const searchByPage = (params) => {
  let url = API_PATH + "/search-by-page";
  return axios.get(url, {params});
};

export const getNewCode = () => {
  let params = {
    voucherType: ConstantList.VOUCHER_TYPE.ReceivingAsset,
  }
  return axios.get(API_PATH + "/addNew/getNewCodeByVoucherType", { params });
};

export const personSearchByPage = (searchObject) => {
  let url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url:
      ConstantList.API_ENPOINT_ASSET_MAINTANE +
      "/api/download/excel/fixed-asset/tiep-nhan",
    data: searchObject,
    responseType: "blob",
  });
};

export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};

export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartmentById/" + userId);
};

export const getCountByStatus = () => {
  return axios.get(API_PATH + "/count-by-status");
};

export const getVoucherItems = (params) => {
  return axios.get(API_PATH + "/items", {params});
};
