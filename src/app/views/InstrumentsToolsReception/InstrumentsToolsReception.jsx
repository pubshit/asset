import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  FormControl,
  Input,
  InputAdornment, Tabs, Tab, AppBar, CircularProgress,
  TablePagination,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountByStatus, getVoucherItems,
} from "./InstrumentsToolsReceptionService";
import InstrumentsToolsReceptionDialog from "./InstrumentsToolsReceptionDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomMaterialTable from "../CustomMaterialTable";
import CustomTablePagination from "../CustomTablePagination";
import AppContext from "app/appContext";
import {appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, ROUTES_PATH} from "app/appConst";
import {subTableColumns, tableColumns} from "./constants";
import MaterialTable from "material-table";
import {
  formatDateDto,
  formatDateToTimestamp,
  handleKeyDown,
  isSuccessfulResponse,
  isValidDate,
  handleThrowResponseMessage,
  handleKeyUp, checkCount, labelDisplayedRows,
} from "app/appFunction";
import {KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import {FadeLayout, LightTooltip} from "../Component/Utilities";
import {PrintPreviewTemplateDialog} from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import {PrintMultipleFormDialog} from "../FormCustom/PrintMultipleFormDialog";
import {LIST_PRINT_FORM_BY_ORG} from "../FormCustom/constant";


function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let isAllowEdit = [
    appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder,
    appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder
  ].includes(item?.status);
  const isProcessing = item?.status === appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder;
  const isProcessed = item?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder;
  return (
    <div className="none_wrap">
      {isAllowEdit && (
        <LightTooltip
          title={t("general.editIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isProcessed && (
        <LightTooltip
          title={t("general.viewIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
            <Icon fontSize="small" color="primary">
              visibility
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isProcessing && (
        <LightTooltip
          title={t("general.deleteIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("In phiếu")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class InstrumentsToolsReception extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    subRowsPerPage: 5,
    page: 0,
    subPage: 0,
    ReceivingAsset: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    subTotalElements: 0,
    isView: false,
    isPrint: false,
    loading: false,
    listAsset: [],
    toDate: null,
    fromDate: null,
    countProcessing: null,
    countProcessed: null,
    status: null,
    tabValue: appConst.TABS_ASSET_RECEPTION.ALL.code,
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  handleKeyUpSearch = (e) => handleKeyUp(e, this.search);

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    const newRowsPerPage = event.target.value;
    this.setState({ rowsPerPage: newRowsPerPage, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = async () => {
    const {toDate, fromDate, keyword, page, rowsPerPage, status} = this.state;
    const {setPageLoading} = this.context;
    const {t} = this.props;
    const searchObject = {
      keyword: keyword?.trim(),
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      toDate: toDate ? formatDateDto(toDate) : null,
      fromDate: fromDate ? formatDateDto(fromDate) : null,
      assetClass: appConst.assetClass.CCDC,
      status: status?.indexOrder
    };

    setPageLoading(true);
    try {
      const res = await searchByPage(searchObject);
      const {code, data} = res?.data;

      if (isSuccessfulResponse(code)) {
        if(!data?.content?.length && data?.totalPages) {
          this.search();
          return;
        }
        
        this.setState({
          itemList: data?.content?.map((item, index) => ({
            ...item,
            stt: page * rowsPerPage + (index + 1),
          })) ?? [],
          totalElements: data?.totalElements ?? 0,
          listAsset: [],
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      isPrint: false,
      isView: false,
      item: null,
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      isPrint: false,
      isView: false,
      item: null,
    }, () => {
      this.updatePageData();
      this.getCountStatus()
    });
  };

  handlePrintReceivingAsset = async (rowData) => {
    let { setPageLoading } = this.context;
    const { t } = this.props;
    const {currentOrg} = this.context;
    
    if (currentOrg?.code === LIST_ORGANIZATION.BVDK_BA_VI.code) {
      return this.setState({
        item: rowData,
        isPrint: true,
      })
    }
    try {
      setPageLoading(true);
      const res = await getItemById(rowData?.id);
      const {code, data} = res?.data;

      if (isSuccessfulResponse(code)) {
        this.setState({
          item: data ? data : {},
          isPrint: true,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleGetVoucherDetail = async (id) => {
    let {setPageLoading} = this.context;
    const {t} = this.props;
    try {
      setPageLoading(true);
      const res = await getItemById(id);
      const {code, data} = res?.data;
      let item = {
        data,
        isSuccess: false,
      }

      if (isSuccessfulResponse(code)) {
        item.isSuccess = true;
        this.setState({
          item: data ? data : {},
        });
        return item;
      } else {
        handleThrowResponseMessage(res);
      }
      return item;
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleConfirmationResponse = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    try {
      setPageLoading(true);
      const response = await deleteItem(this.state.id);
      if (isSuccessfulResponse(response?.data?.code)) {
        this.handleOKEditClose();
        toast.success(t("general.deleteSuccess"));
      } else {
        handleThrowResponseMessage(response);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
    this.getCountStatus();
  }

  handleAddItem = async () => {
    const { t } = this.props;
    try {
      this.setState({
        shouldOpenEditorDialog: true,
      });
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleRowClick = async (rowData) => {
    this.setState({ selectedItem: rowData, subPage: 0, subTotalElements: 0 }, () => {
      this.updateSubPageData();
    });
  }

  updateSubPageData = async () => {
    const { t } = this.props;
    try {
      this.setState({loading: true});
      let { subRowsPerPage, selectedItem, subPage } = this.state;
      const params = {
        voucherId: selectedItem?.id,
        pageSize: subRowsPerPage,
        pageIndex: subPage + 1
      }
      const response = await getVoucherItems(params);
      if(response?.data?.code === appConst.CODE.SUCCESS) {
        let { content, totalElements } = response?.data?.data;
        this.setState({ 
          listAsset: content || [], 
          subTotalElements: totalElements,
        });
      }else {
        handleThrowResponseMessage(response);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      this.setState({loading: false});
    }
  };

  handleSetDate = (data, name) => {
    this.setState({
      [name]: data,
    }, () => {
      if (!isValidDate(data) && data) {return;}
      this.search()
    });
  };

  handleRenderVoucherStatus = (rowData) => {
    const classNameByStatus = {
      [appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder]: "status status-warning",
      [appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder]: "status status-success",
    };
    const status = appConst.LIST_STATUS_ASSET_RECEPTION.find(item => item.indexOrder === rowData?.status)

    return <div className={classNameByStatus[rowData?.status]}>{status?.name}</div>
  }

  handleEditOrViewVoucher = (rowData, method) => {
    this.handleGetVoucherDetail(rowData?.id).then((res) => {
      let statusItem = rowData?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder;
      this.setState({
        shouldOpenEditorDialog: res.isSuccess,
        isView: (method === appConst.active.view) || statusItem,
        isAllowEdit: method === appConst.active.edit && statusItem
      })
    })
  }

  handleChangeTabValue = (event, newValue) => {
    let {ALL, PROCESSED, PROCESSING} = appConst.TABS_ASSET_RECEPTION;
    let statusByTab = {
      [ALL.code]: null,
      [PROCESSING.code]: appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY,
      [PROCESSED.code]: appConst.STATUS_ASSET_RECEPTION.DA_XU_LY,
    }
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      status: statusByTab[newValue],
    }, this.updatePageData);
  };

  getCountStatus = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let countProcessing = 0, countProcessed = 0;
    try {
      let response = await getCountByStatus();
      response?.data?.data?.forEach((item) => {
        if (appConst?.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder === item?.trangThai) {
          countProcessing = checkCount(item?.soLuong);
        }
        if (appConst?.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder === item?.trangThai) {
          countProcessed = checkCount(item?.soLuong);
        }
      });

      this.setState({countProcessing, countProcessed,})
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false)
    }
  };

  handleChangeSubPage = (event, newPage) => {
    this.setState({ subPage: newPage }, () => {
      this.updateSubPageData();
    });
  };

  setSubRowsPerPage = (event) => {
    this.setState({ subRowsPerPage: event.target.value, subPage: 0 }, () => {
      this.updateSubPageData();
    })
  };

  render() {
    const { t } = this.props;
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      listAsset,
      toDate,
      fromDate,
      countProcessing,
      countProcessed,
      tabValue,
      loading,
      isView,
      subPage, 
      subRowsPerPage,
      isAllowEdit,
      isPrint,
      dataView,
    } = this.state;
    const { currentOrg } = this.context;
    const { RECEIVING } = LIST_PRINT_FORM_BY_ORG.IAT_MANAGEMENT;

    let TitlePage = t("InstrumentsToolsReception.title");
    const columns = tableColumns(
      t,
      {
        renderStatus: this.handleRenderVoucherStatus,
      }
    );
    let columnsActions = [
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        maxWidth: 100,
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={async (rowData, method) => {
              if (
                [appConst.active.edit, appConst.active.view].includes(method)
              ) {
                this.handleEditOrViewVoucher(rowData, method);
              } else if (method === appConst.active.print) {
                this.handlePrintReceivingAsset(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    let columnsSubTable = subTableColumns(t , subPage, subRowsPerPage);

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {name: t("Dashboard.toolsManagement"),},
              { name: t("InstrumentsToolsReception.title"), path: ROUTES_PATH.IAT_MANAGEMENT.RECEPTION },
            ]}
          />
        </div>

        <AppBar position="static" color="default" className="mb-20">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              className="tab"
              value={appConst.TABS_ASSET_RECEPTION.ALL.code}
              label={t("InstrumentsToolsReception.tab.all")}
            />
            <Tab
              className="tab"
              value={appConst.TABS_ASSET_RECEPTION.PROCESSING.code}
              label={
                <div className="tabLable">
                  <span>{t("InstrumentsToolsReception.tab.processing")}</span>
                  <div className="tabQuantity tabQuantity-warning">{countProcessing || 0}</div>
                </div>
              }
            />
            <Tab
              className="tab"
              value={appConst.TABS_ASSET_RECEPTION.PROCESSED.code}
              label={
                <div className="tabLable">
                  <span>{t("InstrumentsToolsReception.tab.processed")}</span>
                  <div className="tabQuantity tabQuantity-success">{countProcessed || 0}</div>
                </div>
              }
            />
          </Tabs>
        </AppBar>

        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} xs={12}>
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.handleAddItem}
            >
              {t("InstrumentsToolsReception.addNew")}
            </Button>
          </Grid>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                className="mb-16 mr-16 align-bottom"
                margin="none"
                id="mui-pickers-date"
                label={t("InstrumentsToolsReception.fromDate")}
                inputVariant="standard"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"fromDate"}
                value={fromDate}
                onChange={(date) => this.handleSetDate(date, "fromDate")}
                invalidDateMessage={t("general.invalidDateFormat")}
                minDate={new Date("01/01/1900")}
                maxDate={toDate ? toDate : new Date("01/01/2100")}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={
                  toDate
                    ? t("general.minDateTo")
                    : t("general.maxDateDefault")
                }
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t("InstrumentsToolsReception.toDate")}
                inputVariant="standard"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"toDate"}
                value={toDate}
                onChange={(date) => this.handleSetDate(date, "toDate")}
                invalidDateMessage={t("general.invalidDateFormat")}
                maxDate={new Date("01/01/2100")}
                minDate={fromDate ? fromDate : new Date("01/01/1900")}
                minDateMessage={
                  toDate
                    ? t("general.maxDateFrom")
                    : t("general.minDateDefault")
                }
                maxDateMessage={t("general.maxDateDefault")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={5} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100 mt-16"
                onKeyUp={this.handleKeyUpSearch}
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                placeholder={t("InstrumentsToolsReception.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon onClick={this.search} className="searchTable"/>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>

          <Grid item xs={12}>
            <div>
              {isPrint && currentOrg?.code === LIST_ORGANIZATION.BVDK_BA_VI.code ? (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={isPrint}
                  item={this.state.item}
                  title={t("Phiếu in biên bản kiểm nhập")}
                  model={PRINT_TEMPLATE_MODEL.COMPREHENSIVE_MANAGEMENT.RECEIVING}
                />
              ) : (
                <PrintMultipleFormDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={isPrint}
                  item={dataView}
                  title={t("Phiếu in biên bản kiểm nhập")}
                  urls={[
                    ...RECEIVING.GENERAL,
                    ...(RECEIVING[currentOrg?.printCode] || []),
                  ]}
                />
              )}
              {shouldOpenEditorDialog && (
                <InstrumentsToolsReceptionDialog
                  t={t}
                  item={item}
                  open={shouldOpenEditorDialog}
                  handleClose={this.handleDialogClose}
                  handleOKEditClose={this.handleOKEditClose}
                  isView={isView}
                  isAllowEdit={isAllowEdit}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
            </div>
            <CustomMaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              columnActions={columnsActions}
              onRowClick={(e, rowData) =>
                this.handleRowClick(rowData)
              }
            />

            <CustomTablePagination
              totalElements={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              handleChangePage={this.handleChangePage}
              setRowsPerPage={this.setRowsPerPage}
            />
          </Grid>
        </Grid>

        <div className="position-relative">
          <FadeLayout show={loading} fullSize blur justifyContent="center" alignItems="center" color="primary">
            <CircularProgress color="inherit" />
          </FadeLayout>
          <MaterialTable
            columns={columnsSubTable}
            data={listAsset || []}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              pageSize: appConst.rowsPerPage.category,
              pageSizeOptions: appConst.rowsPerPageOptions.table,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
              },
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
              ...appConst.localizationVi,
            }}
          />
          <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            labelRowsPerPage={t("general.rows_per_page")}
            labelDisplayedRows={labelDisplayedRows}
            component="div"
            count={this.state?.subTotalElements}
            rowsPerPage={this.state?.subRowsPerPage}
            page={this.state?.subPage}
            onPageChange={this.handleChangeSubPage}
            onRowsPerPageChange={this.setSubRowsPerPage}
          />
        </div>
      </div>
    );
  }
}

InstrumentsToolsReception.contextType = AppContext;
export default InstrumentsToolsReception;
