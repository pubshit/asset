import React from "react";
import SupplierDialog from "app/views/Supplier/SupplierDialog";
import ContractDialog from "app/views/Contract/ContractDialog";
import { appConst } from "../../../appConst";
import VoucherFilePopup from "app/views/ReceivingAsset/VoucherFilePopup";

function ComponentDialog(props) {
  const {
    t,
    i18n,
    shouldOpenSupplierAddPopup,
    shouldOpenContractAddPopup,
    handleCloseSupplier,
    handleSelectSupplier,
    handleCloseContract,
    handleSelectContract,
    voucherId,
    keySearch,
    keySearchSupplier
  } = props;
  return (
    <div>
      {/* Add hợp đồng mua sắm */}
      {shouldOpenContractAddPopup && (
        <ContractDialog
          t={t}
          i18n={i18n}
          isVisibleType
          item={{
            name: keySearch,
          }}
          contractTypeCode={appConst.TYPES_CONTRACT.CU}
          handleClose={handleCloseContract}
          open={shouldOpenContractAddPopup}
          handleOKEditClose={handleCloseContract}
          handleSelect={handleSelectContract}
          isHDCUType={true}
        />
      )}

      {shouldOpenSupplierAddPopup && (
        <SupplierDialog
          t={t}
          i18n={i18n}
          item={{
            name: keySearchSupplier
          }}
          open={shouldOpenSupplierAddPopup}
          handleClose={handleCloseSupplier}
          handleOKEditClose={handleCloseSupplier}
          handleSelectDVBH={handleSelectSupplier}
          typeCodes={[appConst.TYPE_CODES.NCC_CU]}
          disabledType
        />
      )}
      {props.shouldOpenSelectAssetFilePopup && (
        <VoucherFilePopup
          {...props}
          open={props.shouldOpenSelectAssetFilePopup}
          getAssetDocument={props.getAssetDocument}
          handleClose={props.handleAssetFilePopupClose}
          itemAssetDocument={props.item}

          t={props.t}
          i18n={props.i18n}
          handleAddFileLocal={props.handleAddFileLocal}
          handleUpdateFile={props.handleUpdateFile}
          voucherId={voucherId}
          documentType={appConst.documentType.ASSET_DOCUMENT_RECEIPT}
        />
      )}
    </div>
  );
}

ComponentDialog.propTypes = {};
export default ComponentDialog;
