import React from "react";
import { IconButton, Button, Icon, Grid } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { MTableToolbar } from "material-table";
import { searchByTextDVBH } from "../../Supplier/SupplierService";
import {
  getListManagementDepartment,
} from "../../Asset/AssetService";
import { personSearchByPage } from "../../AssetAllocation/AssetAllocationService";
import CustomMaterialTable from "../../CustomMaterialTable";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { searchByPageList } from "../../Contract/ContractService";
import { dialogColumns } from "../constants";
import { a11yProps, convertNumberPriceRoundUp, filterOptions } from "app/appFunction";
import { appConst, STATUS_STORE, variable } from "app/appConst";
import { Label, LightTooltip, TabPanel } from "../../Component/Utilities";
import { searchByPage as searchByPageStore } from "../../Store/StoreService";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { useTranslation } from "react-i18next";
import ValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import { searchByPage as searchByPageBidding } from "../../bidding-list/BiddingListService";

function MaterialButton(props) {
  let { t } = useTranslation();
  const item = props.item;
  return (
    <div className="flex">
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">
          edit
        </Icon>
      </IconButton>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
      <LightTooltip
        title={t("InstrumentsToolsReception.multiple")}
        placement="right"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.copy)}
        >
          <Icon
            fontSize="small"
            color="primary"
          >
            content_copy
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function InstrumentsToolsReception(props) {
  const classes = useStyles();
  const { t, onChangeComboBox, item, handleSelectSupplier } = props;
  const { isView, isAllowEdit } = props?.item;

  const [value, setValue] = React.useState(0);
  const [listReceiverDepartment, setListReceiverDepartment] = React.useState([]);

  const searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: item?.receiverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code,
  };
  const searchObjectContract = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    contractTypeCode: appConst.TYPES_CONTRACT.CU,
    // supplierId: item?.supplier?.id
  }
  const isDisabledAddAssetBtn = (
    !item?.receiverDepartment?.id
    || !item?.store?.id
    || !item?.supplier?.id
  );
  let searchObjectBidding = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    status: appConst.STATUS_BIDDING.OPEN.code
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const columns = dialogColumns(t, props);
  let columnsActions = [
    {
      title: t("InstrumentsToolsReception.columns.action"),
      field: "custom",
      align: "center",
      hidden: isView,
      minWidth: 60,
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              props.handleEditAssetInList(rowData);
            } else if (method === appConst.active.delete) {
              props.removeAssetInlist(rowData);
            } else if (method === appConst.active.copy) {
              props.handleDuplicateAsset(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
  ];

  let columnsVoucherFile = [
    {
      title: t("InstrumentsToolsReception.columns.index"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("InstrumentsToolsReception.columns.codeProfile"),
      field: "code",
      align: "left",
      minWidth: "150px",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentsToolsReception.columns.nameProfile"),
      field: "name",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("InstrumentsToolsReception.columns.description"),
      field: "description",
      align: "left",
      minWidth: 250,
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 120,
      align: "center",
      hidden: isView && !isAllowEdit,
      render: (rowData) => (
        <div className="none_wrap">
          <LightTooltip
            title={t("general.editIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab
            label={t("InstrumentsToolsReception.tab.InformationVoting")}
            {...a11yProps(0)}
          />
          <Tab
            label={t("InstrumentsToolsReception.tab.AttachedProfile")}
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={3} sm={12} xs={12}>
            {item?.id && (
              <div className="mt-20">
                <span style={{ fontWeight: "bold" }}>Mã phiếu: </span>
                {props.item.voucherCode}
              </div>
            )}
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <ValidatedDatePicker
              fullWidth
              margin="none"
              id="mui-pickers-date"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("InstrumentsToolsReception.issueDate")}
                </span>
              }
              readOnly={isView}
              type="text"
              autoOk={false}
              format="dd/MM/yyyy"
              name={"issueDate"}
              value={props.item?.issueDate}
              onChange={(date) => props.handleDateChange(date, "issueDate")}
              maxDate={new Date()}
              invalidDateMessage={t("general.invalidDateFormat")}
              maxDateMessage={t("general.maxDateNow")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={6} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("Asset.decisionCode")}
                </span>
              }
              listData={props?.item?.listBidding}
              setListData={(value) => props?.onChangeComboBox(value, "listBidding")}
              searchFunction={searchByPageBidding}
              searchObject={searchObjectBidding}
              displayLable="decisionCode"
              typeReturnFunction="listData"
              value={props.item?.decisionCode || null}
              onSelect={(value) => onChangeComboBox(value, "decisionCode")}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              readOnly={isView}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("InstrumentsToolsReception.lotNumber")}
              name="billNumber"
              InputProps={{
                readOnly: isView,
              }}
              value={props.item.billNumber}
              onChange={props.handleChange}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <ValidatePicker
              fullWidth
              margin="none"
              id="mui-pickers-date"
              label={t("InstrumentsToolsReception.dateBill")}
              inputVariant="standard"
              type="text"
              readOnly={isView}
              format="dd/MM/yyyy"
              name={"billDate"}
              value={props.item.billDate}
              onChange={props.handleDateChange}
              maxDate={new Date()}
              maxDateMessage={t("general.maxDateNow")}
            />
          </Grid>

          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={t("InstrumentsToolsReception.procurementContract")}
              searchFunction={searchByPageList}
              searchObject={searchObjectContract}
              nameListData="listContract"
              listData={item?.listContract}
              setListData={onChangeComboBox}
              displayLable="contractName"
              readOnly={isView && !isAllowEdit}
              typeReturnFunction="category"
              value={item?.contract || null}
              onSelect={props?.handleSelectContract}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "contractName")
              }
              onInputChange={e => onChangeComboBox(e?.target?.value, "keySearch")}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <ValidatePicker
              fullWidth
              label={t("InstrumentsToolsReception.dayContract")}
              type="text"
              readOnly={isView}
              name={"contractDate"}
              value={item?.contract?.contractDate || null}
              onChange={(date) =>
                props.handleDateChange(date, "contractDate")
              }
              disabled
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("InstrumentsToolsReception.supplier")}
                </span>
              }
              listData={props?.listSupplyUnit}
              setListData={props?.setListSupplyUnit}
              searchFunction={searchByTextDVBH}
              searchObject={props.supplierSearchObject}
              displayLable="name"
              readOnly={isView}
              value={item?.supplier || null}
              onSelect={(value) => handleSelectSupplier(value, "SupplyUnit")}
              onInputChange={e => onChangeComboBox(e?.target?.value, "keySearchSupplier")}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "name")
              }
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              disabled={isView ? false : !!item?.contract?.id}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <TextValidator
              fullWidth
              type="text"
              name="handoverRepresentative"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("InstrumentsToolsReception.handoverRepresentative")}
                </span>
              }
              InputProps={{
                readOnly: isView,
              }}
              value={props.item.handoverRepresentative}
              onChange={props.handleChange}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>

          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired>
                  {t("InstrumentsToolsReception.status")}
                </Label>
              }
              listData={appConst.LIST_STATUS_ASSET_RECEPTION}
              searchFunction={() => { }}
              searchObject={{}}
              name={variable.listInputName.status}
              displayLable={'name'}
              readOnly={isView}
              value={item?.status || null}
              onSelect={onChangeComboBox}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired>
                  {t("InstrumentsToolsReception.receiverDepartment")}
                </Label>
              }
              listData={listReceiverDepartment}
              setListData={setListReceiverDepartment}
              searchFunction={getListManagementDepartment}
              searchObject={{}}
              isNoRenderChildren
              displayLable="name"
              typeReturnFunction="list"
              readOnly={isView || item?.isRoleAssetManager}
              name={variable.listInputName.receiverDepartment}
              value={item?.receiverDepartment || null}
              onSelect={onChangeComboBox}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("InstrumentsToolsReception.receiverPerson")}
                </span>
              }
              nameListData="listReceiverPerson"
              listData={item?.listReceiverPerson}
              setListData={onChangeComboBox}
              searchFunction={personSearchByPage}
              searchObject={props.receiverPersonSearchObject}
              displayLable="displayName"
              readOnly={isView}
              name={variable.listInputName.receiverPerson}
              value={item?.receiverPerson || null}
              onSelect={onChangeComboBox}
              filterOptions={filterOptions}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={3} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <Label isRequired>
                  {t("ReceivingScrollableTabsButtonForce.receiverWarehouse")}
                </Label>
              }
              searchFunction={searchByPageStore}
              searchObject={searchObjectStore}
              nameListData="listStore"
              listData={item?.listStore}
              setListData={onChangeComboBox}
              displayLable={'name'}
              name={variable.listInputName.store}
              value={item?.store || null}
              readOnly={isView}
              onSelect={onChangeComboBox}
              filterOptions={filterOptions}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              onChange={props.handleChange}
              type="text"
              name="note"
              label={
                <span>
                  <span className="colorRed"></span>
                  {t("InstrumentsToolsReception.note")}
                </span>
              }
              InputProps={{
                readOnly: isView,
              }}
              className="w-100"
              value={props.item.note}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <TextValidator
              fullWidth
              label={t("InstrumentsToolsReception.reason")}
              type="text"
              name="reason"
              multiline
              InputProps={{
                readOnly: isView,
              }}
              onChange={props.handleChange}
              value={props.item.reason || ""}
            />
          </Grid>

          <Grid item container md={12} sm={12} xs={12} justifyContent="space-between" className="mt-3 mb-3">
            {!isView && (
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  disabled={isDisabledAddAssetBtn}
                  onClick={props.handleOpenIatDialog}
                  style={{ marginRight: "12px" }}
                >
                  {t("general.addIat")}
                </Button>
                <span className="colorRed">(Khi thay đổi số quyết định hoặc đơn vị cung cấp sẽ xóa toàn bộ công cụ dụng cụ)</span>
              </div>
            )}
            <div style={{ marginTop: 5 }}>
              <b>{t("InstrumentsToolsReception.amount")}: <span className="colorRed"> {convertNumberPriceRoundUp(props.item?.totalOriginalCost) || 0} VNĐ</span></b>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <CustomMaterialTable
              data={props.item.assetVouchers}
              columns={columns}
              columnActions={columnsActions}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: (rowData) => ({
                  backgroundColor: rowData.tableData.id === props?.item?.selectedRow?.tableData.id
                    ? "#ccc"
                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "230px",
                padding: "dense",
                minBodyHeight: "230px",
                sorting: false,
              }}
              onRowClick={props.handleRowClick}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        {(!isView || isAllowEdit) &&
          <Grid item md={12} sm={12} xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          </Grid>
        }
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <CustomMaterialTable
            data={props.item.assetDocumentList}
            columns={columnsVoucherFile}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>

    </div>
  );
}
