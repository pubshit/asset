import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
  IconButton,
  Menu,
  MenuItem,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import { BO_PHAN_CAN_SUA, STATUS_REPORT, appConst, formatDate } from "app/appConst";
import AppContext from "app/appContext";
import { deleteArAttachmentById, getArAttachmentById } from "app/views/Asset/AssetService";
import { ConfirmationDialog } from "egret";
import MaterialTable from "material-table";
import React, { useContext, useEffect, useRef, useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AssetAcceptanceForm from "../Component/AssetAcceptanceForm";
import AttachmentPopup from "../Component/AttachmentPopup";
import RateIncidentForm from "../Component/RateIncidentForm";
import ReportIncidentForm from "../Component/ReportIncidentForm";
import {
  checkTotalCost,
  createArSuCo,
  getDetailArSuCo,
  updateArSuCo,
  updateArSuCoNghiemThu,
  updateArSuCoTiepNhan
} from "../MaintainRequestService";
import moment from "moment";
import {
  PaperComponent,
  TabPanel,
  a11yProps,
  getTheHighestRole,
  LightTooltip,
  isSuccessfulResponse, handleThrowResponseMessage,
  convertNumberPrice,
  convertFromToDate
} from "app/appFunction";
import { PrintMultipleFormDialog } from "../../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import { LIST_PRINT_FORM_BY_ORG } from "../../FormCustom/constant";
import CustomValidatorForm from "app/views/Component/ValidatorForm/CustomValidatorForm";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");

let titleName = localStorageService.getSessionItem(
  appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
);
function DeviceTroubleshootingDialog(props) {
  let { setPageLoading, currentOrg } = useContext(AppContext);
  let { departmentUser, currentUser } = getTheHighestRole();
  let organization = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
  const { REPAIRING } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;

  let {
    open,
    t,
    i18n,
    handleClose = () => { },
    itemEdit = {},
    setItemEdit,
    isRoleAssetManager,
    isRoleAssetUser,
    isRoleAdmin,
    isAdd,
    isView
  } = props;
  const form = useRef();
  const [isSubmitNghiemThuSuCo, setIsSubmitNghiemThuSuCo] = useState(false);
  const [isAllowedSubmit, setIsAllowedSubmit] = useState(true);
  const [tabDeviceTroubleshooting, setTabDeviceTroubleshooting] = React.useState(0);
  const [arTrangThai, setArTrangThai] = useState(null);
  const [disable, setDisable] = useState(false)
  const [state, setState] = useState({
    reportDate: moment().format(formatDate),
    status: "",
    arTnPhongBanId: "",
    arTnPhongBanText: "",
    arTnPhongBan: null,
    arTgBaoCao: moment().format(formatDate),
  });
  const [itemTiepNhan, setItemTiepNhan] = useState({ arLinhKienVatTus: [], ...itemEdit })
  const [openBM01, setOpenBM01] = useState(false);
  const [openBM0601, setOpenBM0601] = useState(false);
  const [openBM0607, setOpenBM0607] = useState(false);
  const [openBM0606, setOpenBM0606] = useState(false);
  const [openBMKTTS01, setOpenBMKTTS01] = useState(false);
  const [openMAU_DNMTS, setOpenMAU_DNMTS] = useState(false);
  const [dataNghiemThuSuCo, setDataNghiemThuSuCo] = useState({});
  const [totalCost, setTotalCost] = useState(0);
  const [shouldOpenConfirmDialog, setShouldOpenConfirmDialog] = useState(false);
  const isEdit = (itemEdit?.arTrangThai ?
    (departmentUser?.id === itemTiepNhan?.tsPhongBanId || departmentUser?.id === itemTiepNhan?.arTnPhongBanId) &&
    itemEdit?.arTrangThai === STATUS_REPORT.WAITING :
    true);
  const [idArSuCo, setIdArSuCo] = useState("");
  const [isSavedInfoTab, setIsSavedInfoTab] = useState(false);
  const [isSavedEvaluateTab, setIsSavedEvaluateTab] = useState(false);
  const [isSavedAcceptanceTab, setIsSavedAcceptanceTab] = useState(false);
  const [documents, setDocuments] = useState({})
  const [listAssetDocumentIdState, setListAssetDocumentId] = useState([]);
  const [item, setItem] = useState({});
  const [listDocument, setListDocument] = useState([]);
  const [shouldOpenPopupAssetFile, setShouldOpenPopupAssetFile] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [listPrintArSucoOptions, setListPrintArSucoOptions] = useState(appConst.listPrintArSucoOptions);
  const [dataArSuCo, setDataArSuCo] = useState({ arPhongBanText: "..".repeat(20), addressOfEnterprise, currentUser, organization, titleName })
  const [heightSuCo, setHeightSuCo] = useState(26);
  const [heightArMoTa, setHeightArMoTa] = useState(26);
  const [heightArPhanTichSoBo, setHeightArPhanTichSoBo] = useState(26);
  const [heightArDeXuatBienPhap, setHeightArDeXuatBienPhap] = useState(26);
  const [heightArNghiemThus, setHeightArNghiemThus] = useState(26);// chieu cao cua dongf
  const [arLinhKienVatTus, setArLinhKienVatTus] = useState([])
  const [permission, setPermission] = useState({
    arTnPhongBan: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arPhongBanId: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arNoiDung: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arTgXacNhan: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arTgCanCoVt: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arHanSuaChuaXong: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arMoTa: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arPhanTichSoBo: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arDeXuatBienPhap: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arType: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arScope: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arHinhThuc: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arLinhKienVatTus: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    submit: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arChiPhi: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arNtThoiGian: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arNghiemThus: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arNtTinhTrang: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arYeuCauXuLy: { ...appConst.OBJECT_PERMISSION_DEFAULT },
    arNtTsTinhTrangSd: { ...appConst.OBJECT_PERMISSION_DEFAULT },
  });

  const isTabAttachment = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab
  const isTabInfo = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab
  const isTabAcceptance = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab
  const isTabEvaluate = tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab

  useEffect(() => {
    if (isRoleAssetUser) {
      setDisable(isTabEvaluate || isTabAcceptance);
    } else {
      setDisable(
        (Number(itemEdit?.arTrangThai) === STATUS_REPORT.PROCESSED && !isTabAttachment)
        || (arTrangThai > tabDeviceTroubleshooting + 1 && !isTabEvaluate)
      )
    }

    if (itemEdit?.arTrangThai !== STATUS_REPORT.PROCESSED) {
      setListPrintArSucoOptions(listPrintArSucoOptions.filter(({ code }) =>
        ![STATUS_REPORT.PROCESSED, appConst?.PRINT_AR_SUCO_OPTIONS?.MAU_DNMTS?.code].includes(code)
      ));
    } else if (itemEdit?.arHinhThuc === appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code) {
      setListPrintArSucoOptions(appConst.listPrintArSucoOptions)
    } else {
      setListPrintArSucoOptions(
        listPrintArSucoOptions.filter(i => i.code !== appConst?.PRINT_AR_SUCO_OPTIONS?.MAU_DNMTS?.code)
      )
    }
  }, [itemEdit?.arTrangThai, arTrangThai, tabDeviceTroubleshooting]);

  useEffect(() => {
    if (itemEdit?.id && isTabInfo) {
      let newItem = {
        ...itemEdit,
        arTnPhongBan: itemEdit?.arTnPhongBanId
          ? {
            id: itemEdit?.arTnPhongBanId,
            name: itemEdit?.arTnPhongBanText,
          }
          : null
      }
      setIdArSuCo(itemEdit.id);
      setItemTiepNhan(newItem)
      setState({ ...newItem });
      setListDocument(itemEdit?.arAttachments);
      setArTrangThai(itemEdit?.arTrangThai);
    }
  }, [itemEdit?.id]);

  const getAssetDocument = async (document) => {
    let listAssetDocumentId = listAssetDocumentIdState ? listAssetDocumentIdState : [];
    let documents = listDocument ? listDocument : [];
    if (document?.id) {
      listAssetDocumentId.push(document?.id);
      documents.push(document);
    }
    setListAssetDocumentId(listAssetDocumentId)
    setListDocument(documents)
    try {
      const attachmentIds = documents?.map((item) => {
        return item?.id
      })
      setPageLoading(true);
      if (arTrangThai === STATUS_REPORT.WAITING) {
        const result = await updateArSuCo({ ...state, arAttachmentIds: attachmentIds });
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"));
          setState((pre) => ({ ...pre, ...result?.data?.data }))
          setIsSavedAcceptanceTab(true);
        } else {
          toast.error(result?.data?.message);
        }
      }
      else if (arTrangThai === STATUS_REPORT.PROCESSING) {
        const result = await updateArSuCoTiepNhan({ ...itemTiepNhan, arAttachmentIds: attachmentIds });
        if (result?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t("general.updateSuccess"));
          setState((pre) => ({ ...pre, ...result?.data?.data }))
          setIsSavedAcceptanceTab(true);
        } else {
          toast.error(result?.data?.message);
        }
      }

    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };
  const handleUpdateAssetDocument = (document) => {
    let documents = listDocument
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    setListDocument(documents);
  };

  const handleAddAssetDocumentItem = () => {
    setDocuments({})
    setShouldOpenPopupAssetFile(true)
    setItem({ isView: false, isEditAssetDocument: false });
  };

  const handleRowDataCellEditAssetFile = (rowData) => {
    getArAttachmentById(rowData.id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        setItem({ isView: false, isEditAssetDocument: true });
        setDocuments(data?.data);
        setShouldOpenPopupAssetFile(true);
      } else {
        toast.error(t("general.error"))
      }
    }).catch(() => {
      toast.error(t("general.error"))
    });
  };

  const handleRowDataCellViewAssetFile = (rowData) => {
    getArAttachmentById(rowData.id).then(({ data }) => {
      if (data?.code === appConst.CODE.SUCCESS) {
        setItem({ isView: true, isEditAssetDocument: false });
        setDocuments(data?.data);
        setShouldOpenPopupAssetFile(true);
      } else {
        toast.error(t("general.error"))
      }
    }).catch(() => {
      toast.error(t("general.error"))
    });
  };

  const handleRowDataCellDeleteAssetFile = async (id) => {

    try {
      const result = await deleteArAttachmentById(id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        const newListDocument = [...listDocument];
        const newListAssetDocumentId = [...listAssetDocumentIdState]
        let index = listDocument?.findIndex(document => document?.id === id);
        let indexId = listAssetDocumentIdState?.findIndex(documentId => documentId === id);
        newListDocument.splice(index, 1);
        newListAssetDocumentId.splice(indexId, 1);
        setListDocument(newListDocument)
        setListAssetDocumentId(newListAssetDocumentId)
        toast.success(t("general.success"))
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  const handleAssetFilePopupClose = () => {
    setShouldOpenPopupAssetFile(false)
  };

  const isActivePrintBtn = () => {
    if (isTabInfo) {
      if (isSavedInfoTab || itemEdit?.arTrangThai === STATUS_REPORT.WAITING ||
        itemEdit?.arTrangThai === STATUS_REPORT.PROCESSING ||
        itemEdit?.arTrangThai === STATUS_REPORT.PROCESSED) {
        return true;
      }
    } else if (isTabEvaluate) {
      return true;
    } else if (isTabAcceptance) {
      if (isSavedAcceptanceTab ||
        itemEdit?.arTrangThai === STATUS_REPORT.PROCESSED) {
        return true;
      }
    }
    return false;
  };

  const isActiveNextBtn = () => {
    let isProcessing = itemEdit?.arTrangThai === STATUS_REPORT.PROCESSING || isSavedEvaluateTab;
    let isWaiting = itemEdit?.arTrangThai === STATUS_REPORT.WAITING;
    return (!isTabAcceptance && (isProcessing))
      || (isTabInfo && (isSavedInfoTab || isWaiting))
      || isTabAttachment;
  };

  const isShowSaveBtn = () => {
    if (isRoleAssetUser) {
      return isTabInfo;
    }
    if (
      itemEdit?.arTrangThai === STATUS_REPORT.PROCESSING
      && isTabInfo
    ) {
      return false;
    }
    else if (
      (itemEdit?.arTrangThai === STATUS_REPORT.PROCESSED || disable) && (
        isTabInfo
        || isTabEvaluate
        || isTabAcceptance
        || isTabAttachment
      )
    ) {
      return false;
    } else {
      return !isTabAttachment;
    }
  };

  const handleChangeTabsDeviceTroubleshooting = (event, newValue) => {
    const {INFO, ATTACHMENTS} = appConst.tabDeviceTroubleshootingDialog;
    if (isRoleAssetUser) {
      if (newValue === INFO.valueTab) {
        setDisable(false);
      }
    }
    if (
      (
        !arTrangThai
        && INFO.valueTab !== newValue
        && ATTACHMENTS.valueTab !== newValue
      )
      || (
        arTrangThai
        && newValue > arTrangThai
        && newValue !== ATTACHMENTS.valueTab
      )
    ) {
      toast.warning(t("general.preventNextTab"))
      return;
    }
    if (newValue !== undefined) {
      handleCheckPermissionOnChagneTab();
      setTabDeviceTroubleshooting(newValue)
    } else if (isTabAttachment) {
      setTabDeviceTroubleshooting(appConst.tabDeviceTroubleshootingDialog.INFO.valueTab)
    } else {
      setTabDeviceTroubleshooting(tabDeviceTroubleshooting + 1)
    }
  };

  const handleCheckPermissionOnChagneTab = () => {
    let newPermission = { ...permission };
    if (isRoleAssetManager) {
      newPermission.arTgXacNhan.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arTgCanCoVt.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arHanSuaChuaXong.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arType.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arHinhThuc.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arScope.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arLinhKienVatTus.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.submit.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arPhanTichSoBo.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arDeXuatBienPhap.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arMoTa.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arChiPhi.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arNtThoiGian.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arNghiemThus.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arNtTinhTrang.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arYeuCauXuLy.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
      newPermission.arNtTsTinhTrangSd.isDisabled = state.arTnPhongBan?.id !== departmentUser?.id;
    }

    setPermission(newPermission);
  }

  const handleUpdateArSuCo = async () => {
    try {
      setPageLoading(true);
      const sendData = {
        ...state,
        arTnPhongBanId: state?.arTnPhongBan?.id,
        arTnPhongBanText: state?.arTnPhongBanText,
        assetClass: appConst.assetClass.TSCD,
      };
      let res = state?.id ? await updateArSuCo(sendData) : await createArSuCo(sendData);
      const { code, data } = res?.data;

      if (!isSuccessfulResponse(code)) {
        handleThrowResponseMessage(res);
        return;
      }
      if (!state.id) {
        toast.success(t("general.addSuccess"));
        setItemEdit(data);
      } else {
        toast.success(t("general.updateSuccess"));
      }
      setIsSavedInfoTab(true);
      setItemTiepNhan(data);
      setState({
        ...data,
        arTnPhongBan: data?.arTnPhongBanId
          ? {
            id: data?.arTnPhongBanId,
            name: data?.arTnPhongBanText
          }
          : null,
      });
      setArTrangThai(data?.arTrangThai)
    } catch (e) {
      console.error(e);
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  const handleUpdateTiepNhan = async () => {
    const sendData = {
      ...itemTiepNhan,
      donViThucHienId: itemTiepNhan?.unit?.id,
      donViThucHienText: itemTiepNhan?.unit?.name,
      donViThucHienCode: itemTiepNhan?.unit?.code,
      chiPhiThucHien: itemTiepNhan?.chiPhiThucHien,
      assetClass: appConst.assetClass.TSCD,
      arTgXacNhan: convertFromToDate(itemTiepNhan?.arTgXacNhan).toDate,
      arTgCanCoVt: convertFromToDate(itemTiepNhan?.arTgCanCoVt).toDate,
      arHanSuaChuaXong: convertFromToDate(itemTiepNhan?.arHanSuaChuaXong).toDate,
      arTgBaoCao: convertFromToDate(itemTiepNhan?.arTgBaoCao).toDate,
      arAttachmentIds: itemTiepNhan?.arAttachments?.map(x => x?.id)
    }
    try {
      if (validatorTiepNhan(itemTiepNhan)) {
        let res = await updateArSuCoTiepNhan(sendData)
        if (res?.data?.data && res?.data?.code === appConst.CODE.SUCCESS) {
          toast.success(t(
            sendData?.id ? "general.updateSuccess" : "general.addSuccess"
          ))
          setIdArSuCo(res?.data?.data?.id);
          setArTrangThai(res?.data?.data?.arTrangThai);
          setIsSavedEvaluateTab(true);
          setState(res?.data?.data);
          setItemTiepNhan(res?.data?.data);
          setArLinhKienVatTus(res?.data?.data?.arLinhKienVatTus || []);
        }
        else {
          toast.warning(res?.data?.data?.[0]?.errorMessage || res?.data?.message)
        }
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }

  const handlePrint = (e, valueTab) => {
    const itemValue = e?.target?.value || e;

    (isShowSaveBtn() && !isView && !permission.submit.isDisabled)
      ? handleSave(valueTab).then(() => {
        handleConfirmPrint(itemValue, valueTab);
      })
      : handleConfirmPrint(itemValue, valueTab);
  };

  const handleSave = async (valueTab) => {
    const buttonSubmit = document.querySelector('#button-submit')
    const buttonPrintPVT = document.querySelector('#button-print-1')
    const buttonPrintHCQT = document.querySelector('#button-print-2')
    const buttonPrintKTTS01 = document.querySelector('#button-print-3')
    if (
      buttonSubmit !== document.activeElement
      && buttonPrintPVT !== document.activeElement
      && buttonPrintHCQT !== document.activeElement
      && buttonPrintKTTS01 !== document.activeElement
    ) return;

    if (isAllowedSubmit) {
      if (valueTab === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab) {
        await handleUpdateArSuCo(valueTab);
      } else if (valueTab === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab) {
        await handleUpdateTiepNhan(valueTab);
      } else if (valueTab === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab) {
        itemEdit?.arTrangThai !== STATUS_REPORT.PROCESSED
          ? await handleCheckTotalCost(valueTab)
          : setTabDeviceTroubleshooting(valueTab + 1);
      } else if (valueTab === appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab) {
        setPageLoading(false);
      }
    }
  };

  const handleGetDetailArSuCo = async (id) => {
    try {
      const result = await getDetailArSuCo(id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        let data = result?.data?.data;

        let newDate = new Date(data?.arTgBaoCao);
        const day = data?.arTgBaoCao ? String(newDate?.getDate())?.padStart(2, '0') : "....";
        const month = data?.arTgBaoCao ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : "....";
        const year = data?.arTgBaoCao ? String(newDate?.getFullYear()) : "......";
        let arNghiemThusTxt = data?.arNghiemThus?.map(item => item?.arNtHangMuc)?.join(", ");

        setDataArSuCo((pre) => ({
          ...pre,
          ...data,
          dataPrint: {
            phongBanLapText: data?.jText,
            day,
            month,
            year,
            assets: [{
              index: 1,
              tsSerialNoTxt: data?.tsSerialNo ? data?.tsSerialNo : "....",
              tsModelTxt: data?.tsModel ? data?.tsModel : "....",
              tsHangsxTxt: data?.tsHangsx ? data?.tsHangsx : "....",
              tsNamsxTxt: data?.tsNamsx ? data?.tsNamsx : "....",
              tsNamsdTxt: data?.tsNamsd ? data?.tsNamsd : "....",
              ckSoluong: 1,
              arNghiemThusTxt
            }],
            totalQuantity: 1
          },
          arMa: data?.arMa || "..".repeat(10),
          jText: data?.jText || "..".repeat(20),
          textQuyCach: data?.tsModel && data?.tsSerialNo
            ? `${data.tsModel}/${data.tsSerialNo}`
            : data?.tsModel
              ? `${data.tsModel}`
              : data?.tsSerialNo
                ? `${data.tsSerialNo}`
                : "",
          arNtTsTinhTrangSdTxt: appConst.typeOfWarehouseAssets.find(x => x.id === data?.arNtTsTinhTrangSd)?.name,
          day,
          month,
          year,
        }))
      }
    } catch (error) {
      toast.error(t("general.error"));
    }
  }

  const handleCheckTotalCost = async (valueTab) => {
    try {
      setPageLoading(true);
      const result = await checkTotalCost({
        idAsset: state?.tsId,
        totalCost: totalCost,
      });
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        if (result?.data?.data === false) {
          setShouldOpenConfirmDialog(true);
        } else {
          await handleUpdateNghiemThu(valueTab);
        }
      }
    } catch (error) {
    } finally {
      setPageLoading(false);
    }
  };

  const handleUpdateNghiemThu = async () => {
    if (validatorAcepptance(dataNghiemThuSuCo)) {
      try {
        setPageLoading(true);
        let sendData = {
          ...dataNghiemThuSuCo,
          assetClass: appConst.assetClass.TSCD,
          arNtThoiGian: convertFromToDate(dataNghiemThuSuCo?.arNtThoiGian).toDate,
        }
        const result = await updateArSuCoNghiemThu(sendData);
        const { code, data } = result?.data;
        if (isSuccessfulResponse(code)) {
          toast.success(t("general.updateSuccess"));
          setDataNghiemThuSuCo(data)
          setIsSavedAcceptanceTab(true);
          setDisable(true);
          setItemEdit(data);
          setItemTiepNhan(data);
        } else {
          handleThrowResponseMessage(result);
        }
      } catch (error) {
        toast.error(t("general.error"));
      } finally {
        setPageLoading(false);
      }
    }
  };

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: '50px',
      align: 'left',
      cellStyle: {
        textAlign: "center"
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("AssetFile.description"),
      field: "note",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("general.action"),
      field: "valueText",
      maxWidth: 100,
      minWidth: 70,
      cellStyle: {
        textAlign: "center"
      },
      render: rowData =>
        (!props?.item?.isView && !props?.item?.isStatusConfirmed && !disable) ? (
          <div className="none_wrap">
            <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellEditAssetFile(rowData)}>
                <Icon fontSize="small" color="primary">edit</Icon>
              </IconButton>
            </LightTooltip>
            <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellDeleteAssetFile(rowData?.id)}>
                <Icon fontSize="small" color="error">delete</Icon>
              </IconButton>
            </LightTooltip>
          </div>
        ) : (
          <div className="none_wrap">
            <LightTooltip title={t('general.viewIcon')} placement="top" enterDelay={300} leaveDelay={200}>
              <IconButton size="small" onClick={() => handleRowDataCellViewAssetFile(rowData)}>
                <Icon fontSize="small" color="primary">visibility</Icon>
              </IconButton>
            </LightTooltip>
          </div>
        )
    },
  ];

  const validatorTiepNhan = (sendData) => {
    if (!sendData?.arType) {
      toast.warning(t("maintainRequest.required.arType"))
      return false;
    }
    else if (
      (
        sendData?.arType === BO_PHAN_CAN_SUA.SUA_CA_HAI.code
        || sendData?.arType === BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code
      )
      && sendData?.arPhuTungs?.length <= 0
    ) {
      toast.warning(t("maintainRequest.required.arPhuTungs"))
      return false;
    }
    else if (!sendData?.arHinhThuc) {
      toast.warning(t("maintainRequest.required.arHinhThuc"))
      return false;
    }
    else if (
      sendData?.arHinhThuc === appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.code
      && sendData?.arUsers?.length <= 0
    ) {
      toast.warning(t("maintainRequest.required.arUsers"))
      return false
    }
    else if (!sendData?.arScope) {
      toast.warning(t("maintainRequest.required.arScope"));
      return false;
    }
    else if (
      sendData?.arType !== BO_PHAN_CAN_SUA.SUA_TAI_SAN.code
      && sendData?.arPhuTungs?.length <= 0
    ) {
      toast.warning(t("maintainRequest.required.arPhuTungId"));
      return sendData?.arPhuTungs?.some(pt => !pt.product);
    }
    return true;
  }

  const validatorAcepptance = (sendData) => {
    if (!sendData?.arNtTinhTrang) {
      toast.warning(t("maintainRequest.required.arNtTinhTrang"))
      return false;
    }
    else if (!sendData?.arYeuCauXuLy) {
      toast.warning(t("maintainRequest.required.arYeuCauXuLy"));
      return false;
    }
    return true;
  }

  const handleOpenMenuPrint = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenuPrint = () => {
    setAnchorEl(null);
  };

  const handleConfirmPrint = async (itemValue, valueTab) => {
    await handleGetDetailArSuCo(itemEdit?.id);
    if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_DNMTS.code === itemValue) {
      setOpenMAU_DNMTS(true)
    } else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_HCQT.code === itemValue) {
      setOpenBM01(true);
    } else if (appConst.PRINT_AR_SUCO_OPTIONS.MAU_KTTS01.code === itemValue) {
      setOpenBMKTTS01(true);
    } else if (valueTab === appConst.tabDeviceTroubleshootingDialog.INFO.valueTab) {
      setOpenBM0601(true);
    } else if (
      valueTab === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab
    ) {
      setOpenBM0606(true);
    } else if (
      valueTab === appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab
    ) {
      setOpenBM0607(true);
    }
  };

  const getTemplateDetail = (template) => {
    // Tạo một div tạm thời để chứa HTML string
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = template;

    // Thêm div tạm thời vào body (hoặc bất kỳ container nào khác)
    document.body.appendChild(tempDiv);

    // Lấy element có id là 'dottedEl' từ div tạm thời
    const dottedEl = tempDiv.querySelector('#dottedEl');
    const dottedElArMoTa = tempDiv.querySelector('#dottedElArMoTa');
    const dottedElArPhanTichSoBo = tempDiv.querySelector('#dottedElArPhanTichSoBo');
    const dottedElArDeXuatBienPhap = tempDiv.querySelector('#dottedElArDeXuatBienPhap');
    const dottedElArNghiemThus = tempDiv.querySelector('#dottedElArNghiemThus');

    // Lấy chiều cao của element này
    const heightReason = dottedEl?.scrollHeight;
    const heightArMoTa = dottedElArMoTa?.scrollHeight;
    const heightArPhanTichSoBo = dottedElArPhanTichSoBo?.scrollHeight;
    const heightArDeXuatBienPhap = dottedElArDeXuatBienPhap?.scrollHeight;
    const heightArNghiemThus = dottedElArNghiemThus?.scrollHeight;

    // Loại bỏ div tạm thời khỏi DOM
    document.body.removeChild(tempDiv);

    // Trả về hoặc log chiều cao của element
    setHeightSuCo(heightReason);
    setHeightArMoTa(heightArMoTa);
    setHeightArPhanTichSoBo(heightArPhanTichSoBo);
    setHeightArDeXuatBienPhap(heightArDeXuatBienPhap);
    setHeightArNghiemThus(heightArNghiemThus);
  };

  const renderDotted = () => {
    let heightReason = 0;
    let heightMoTa = 0;
    let heightPhanTichSoBo = 0;
    let heightDeXuatBienPhap = 0;
    let heightNghiemThus = 0;

    heightReason = heightSuCo
    heightMoTa = heightArMoTa
    heightPhanTichSoBo = heightArPhanTichSoBo
    heightDeXuatBienPhap = heightArDeXuatBienPhap
    heightNghiemThus = heightArNghiemThus

    const newArr = [];
    const extraLine = 5;
    let rowNumber = 1;

    rowNumber = Math.floor(heightReason / 25) + extraLine;
    rowNumber = Math.floor(heightMoTa / 25) + extraLine;
    rowNumber = Math.floor(heightPhanTichSoBo / 25) + extraLine;
    rowNumber = Math.floor(heightDeXuatBienPhap / 25) + extraLine;
    rowNumber = Math.floor(heightNghiemThus / 25) + extraLine;

    for (let i = 1; i <= (rowNumber || 1); i++) {
      newArr.push(`<div style="border-bottom: 1px dotted black; line-height: 24px;">&nbsp;</div>`);
    }

    return newArr
  };
  let dataViewBM01 = {
    ...dataArSuCo,
    currentUser,
    isOk: dataArSuCo?.arNtTinhTrang
      === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code,
    isNotOk: dataArSuCo?.arNtTinhTrang
      === appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code,
    arNghiemThus: dataArSuCo?.arNghiemThus?.map((item) => {
      return {
        ...item,
        arNtHangMuc: item?.arNtHangMuc?.replaceAll("\n", `<br>`),
        arNtKetLuan: item?.arNtKetLuan?.replaceAll("\n", `<br>`)
      };
    }),
    arNoiDung: dataArSuCo?.arNoiDung
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
          ${dataArSuCo.arNoiDung?.replace(/\n/g, '<br>')}
        </div>`
      : "",
    arMoTa: dataArSuCo?.arMoTa
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
            - Mô tả sự cố: <br>${dataArSuCo.arMoTa?.replace(/\n/g, '&nbsp;<br>')}
        </div>`
      : "",
    arPhanTichSoBo: dataArSuCo?.arPhanTichSoBo
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
          - Phân tích sơ bộ:<br>${dataArSuCo.arPhanTichSoBo?.replace(/\n/g, '&nbsp;<br>')}
        </div>`
      : "",
    arDeXuatBienPhap: dataArSuCo?.arDeXuatBienPhap
      ? `
      <div style="
        position: relative;
        line-height: 25px;
        word-break: break-word;
        overflow: hidden;
      ">
        - Đề xuất biện pháp:<br>${dataArSuCo.arDeXuatBienPhap?.replace(/\n/g, '&nbsp;<br>')}
      </div>`
      : "",
  }

  const renderDate = (dateData) => {
    let date = new Date(dateData);
    const dateEmty = new Date();

    let day = moment(date).format("D").toString().padStart(2, "0");
    let month = (date.getMonth() + 1).toString().padStart(2, "0");
    let year = date.getFullYear();
    const dateHasText = dateData ? `ngày ${day} tháng ${month} năm ${year}` : "ngày .... tháng .... năm ......";
    const dateNoText = dateData ? moment(date).format("DD/MM/YYYY") : ".... / .... / ...... ";
    const dateHasTextV2 = dateData ? `Ngày: ${day} tháng: ${month} năm: ${year}` : `Ngày:..........tháng:..........năm:${moment(dateEmty).format("YYYY")}`;

    return {
      dateHasText,
      dateNoText,
      dateHasTextV2,
    };
  }

  let dataViewKTTS01 = {
    ...dataArSuCo,
    dateHasText: renderDate(dataArSuCo?.arNtThoiGian)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arNtThoiGian)?.dateNoText,
    arUsers: dataArSuCo?.arUsers?.map((i) => {
      return {
        ...i,
        userName: i?.userName || "..".repeat(10)
      }
    })
  };

  let dataViewBM0601 = {
    ...dataArSuCo,
    organizationName: organization?.org?.name || "",
    arMoTa: dataArSuCo?.arMoTa?.replace(/\n/g, '<br>'),
    arPhanTichSoBo: dataArSuCo?.arPhanTichSoBo?.replace(/\n/g, '<br>'),
    arDeXuatBienPhap: dataArSuCo?.arDeXuatBienPhap?.replace(/\n/g, '<br>'),
    arNoiDung: dataArSuCo?.arNoiDung?.replace(/\n/g, '<br>'),
  };

  let dataViewBM0606 = {
    ...dataArSuCo,
    currentUser,
    dateHasText: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    dateHasTextV2: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasTextV2,
    arTgCanCoVtTo: renderDate(dataArSuCo?.arTgCanCoVt)?.dateNoText,
    arLinhKienVatTus: dataArSuCo?.arLinhKienVatTus?.map((i, x) => {
      return {
        ...i,
        index: x + 1
      }
    }),
    arMoTa: dataArSuCo?.arMoTa?.replace(/\n/g, '<br>'),
    arPhanTichSoBo: dataArSuCo?.arPhanTichSoBo?.replace(/\n/g, '<br>'),
    arDeXuatBienPhap: dataArSuCo?.arDeXuatBienPhap?.replace(/\n/g, '<br>'),
    arNoiDung: dataArSuCo?.arNoiDung?.replace(/\n/g, '<br>'),
  };

  let dataViewBM0607 = {
    ...dataArSuCo,
    tsTen: dataArSuCo?.tsTen || "..".repeat(10),
    tsModel: dataArSuCo?.tsModel || "..".repeat(10),
    tsSerialNo: dataArSuCo?.tsSerialNo || "..".repeat(10),
    currentUser,
    dateHasText: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    dateHasTextV2: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasTextV2,
    arTgCanCoVtTo: renderDate(dataArSuCo?.arTgCanCoVt)?.dateNoText,
    arHanSuaChuaXong: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    userName: dataArSuCo?.arUsers?.[0]?.userName,
    arChiPhi: convertNumberPrice(dataArSuCo?.arChiPhi) || 0,
    arTongChiPhi: convertNumberPrice(dataArSuCo?.arTongChiPhi) || 0,
    isSuaChuaNgoai: dataArSuCo?.arHinhThuc === Number(appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code),
    checked: dataArSuCo?.arNtTinhTrang === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code,
    arLinhKienVatTus: dataArSuCo?.arLinhKienVatTus?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        productCost: convertNumberPrice(i?.productCost) || 0,
        productTotal: convertNumberPrice(i?.productTotal) || 0,
      }
    }),
    arNghiemThus: dataArSuCo?.arNghiemThus?.map(x => {
      return {
        ...x,
        arNtHangMuc: x?.arNtHangMuc?.replace(/\n/g, '<br>'),
        arNtKetLuan: x?.arNtKetLuan?.replace(/\n/g, '<br>'),
      }
    }),
    arMoTa: dataArSuCo?.arMoTa?.replace(/\n/g, '<br>'),
    arPhanTichSoBo: dataArSuCo?.arPhanTichSoBo?.replace(/\n/g, '<br>'),
    arDeXuatBienPhap: dataArSuCo?.arDeXuatBienPhap?.replace(/\n/g, '<br>'),
  };

  let isHasNewEquipment = itemTiepNhan?.arLinhKienVatTus?.some(i => i?.storeId && !i?.id)
    && tabDeviceTroubleshooting === appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab;

  let txtBtnSubmit = isHasNewEquipment ? t("general.saveAndExport") : t("general.save");
  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth>
      <DialogTitle
        style={{ cursor: "move", paddingBottom: "0px" }}
        id="draggable-dialog-title"
      >
        {t("ReportIncident.titleDialog")}
      </DialogTitle>
      <CustomValidatorForm
        ref={form}
        id="device-form"
        onSubmit={() => handleSave(tabDeviceTroubleshooting)}
        className="validator-form-scroll-dialog m-0"
      >
        <DialogContent>
          <AppBar position="static" color="default">
            <Tabs
              value={tabDeviceTroubleshooting}
              onChange={handleChangeTabsDeviceTroubleshooting}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="simple tabs example"
            >
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.INFO.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.INFO.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.INFO.valueTab)}
              />
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.EVALUATE.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab)}
              />
              <Tab
                value={appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab)}
              />
              {itemEdit?.id && <Tab
                value={appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab}
                label={t(appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.nameTab)}
                {...a11yProps(appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab)}
              />}
            </Tabs>
          </AppBar>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.INFO.valueTab}
          >
            <ReportIncidentForm
              t={t}
              i18n={i18n}
              isEdit={isEdit}
              itemEdit={itemEdit}
              state={state}
              setState={setState}
              isRoleAssetManager={isRoleAssetManager}
              isRoleAdmin={isRoleAdmin}
              disable={disable}
              isAdd={isAdd}
            />
          </TabPanel>

          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.EVALUATE.valueTab}
          >
            <RateIncidentForm
              t={t}
              i18n={i18n}
              setItemTiepNhan={setItemTiepNhan}
              itemTiepNhan={itemTiepNhan}
              isAllowedSubmit={isAllowedSubmit}
              setIsAllowedSubmit={setIsAllowedSubmit}
              disable={disable}
              arLinhKienVatTus={arLinhKienVatTus}
              permission={permission}
            />
          </TabPanel>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.ACCEPTANCE.valueTab}
          >
            <AssetAcceptanceForm
              t={t}
              i18n={i18n}
              itemEdit={state}
              isSubmitNghiemThuSuCo={isSubmitNghiemThuSuCo}
              setIsSubmitNghiemThuSuCo={setIsSubmitNghiemThuSuCo}
              dataNghiemThuSuCo={dataNghiemThuSuCo}
              setDataNghiemThuSuCo={setDataNghiemThuSuCo}
              totalCost={totalCost}
              setTotalCost={setTotalCost}
              disable={disable}
              setIsSavedAcceptanceTab={setIsSavedAcceptanceTab}
              permission={permission}
            />
          </TabPanel>
          <TabPanel
            value={tabDeviceTroubleshooting}
            index={appConst.tabDeviceTroubleshootingDialog.ATTACHMENTS.valueTab}
          >
            <Grid item md={12} sm={12} xs={12}>
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={handleAddAssetDocumentItem}
                disabled={disable}
              >
                {t('AssetFile.addFileAttachment')}
              </Button>
              {shouldOpenPopupAssetFile && (
                <AttachmentPopup
                  open={shouldOpenPopupAssetFile}
                  handleClose={handleAssetFilePopupClose}
                  itemAssetDocument={documents}
                  getAssetDocument={getAssetDocument}
                  handleUpdateAssetDocument={handleUpdateAssetDocument}
                  item={item}
                  t={t}
                  i18n={i18n}
                  idArSuCo={idArSuCo}
                  type={appConst.FILE_TYPE.ATTACHMENTS}
                />)
              }
            </Grid>
            <Grid item md={12} sm={12} xs={12} className="mt-16">
              <MaterialTable
                data={listDocument}
                columns={columnsVoucherFile}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                  }
                }}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  padding: 'dense',
                  rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: '#358600',
                    color: '#fff',
                  },
                  maxBodyHeight: '285px',
                  minBodyHeight: '285px',
                }}

                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </TabPanel>
        </DialogContent>
        <DialogActions>
          <Button
            className="mb-16 mr-12 align-bottom"
            variant="contained"
            color="secondary"
            onClick={() => handleClose()}
          >
            {t("general.cancel")}
          </Button>
          <Button
            disabled={!isActivePrintBtn()}
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={handleOpenMenuPrint}
          >
            {!isView && !permission.submit.isDisabled
              ? t("general.saveAndPrint")
              : t("general.print")
            }
          </Button>
          {/* Menu Print */}
          <Menu
            id="simple-menu"
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            keepMounted
            elevation={0}
            onClose={handleCloseMenuPrint}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            {listPrintArSucoOptions.map((item, index) => (
              <MenuItem
                key={index}
                id={`button-print-${item.code}`}
                onClick={(e) => handlePrint(e, tabDeviceTroubleshooting)}
                value={item.code}
              >
                {item.name}
              </MenuItem>
            ))}
          </Menu>

          {(!isView && isShowSaveBtn()) && (
            <Button
              id="button-submit"
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              type="submit"
              disabled={disable || permission.submit.isDisabled}
            >
              {txtBtnSubmit}
            </Button>
          )}
          {!isView && !isRoleAssetUser && (
            (isRoleAdmin ||
              (itemTiepNhan?.arTnPhongBanId === departmentUser?.id && state?.arTnPhongBanId === departmentUser?.id)
            ) && (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={handleChangeTabsDeviceTroubleshooting}
                disabled={!isActiveNextBtn()}
              >
                {t("general.Next")}
              </Button>
            )
          )}
        </DialogActions>
      </CustomValidatorForm>

      {openBM0601 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0601(false)
          }}
          open={openBM0601}
          item={dataViewBM0601}
          title={t("Phiếu báo cáo sự cố")}
          urls={[
            ...REPAIRING.INCIDENT_REPORT_PVT.GENERAL,
            ...REPAIRING.INCIDENT_REPORT_PVT[currentOrg?.printCode],
          ]}
        />
      )}
      {openMAU_DNMTS && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenMAU_DNMTS(false)
          }}
          open={openMAU_DNMTS}
          item={dataArSuCo}
          title={t("Phiếu đề nghị mang tài sản ra ngoài viện")}
          urls={[
            ...REPAIRING.TAKE_ASSET_OUT_OF_ORG.GENERAL,
            ...REPAIRING.TAKE_ASSET_OUT_OF_ORG[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM01 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM01(false)
          }}
          open={openBM01}
          item={dataViewBM01}
          getTemplate={getTemplateDetail}
          title={t("Phiếu yêu cầu sửa chữa")}
          urls={[
            ...REPAIRING.INCIDENT_REPORT_HCQT.GENERAL,
            ...REPAIRING.INCIDENT_REPORT_HCQT[currentOrg?.printCode],
          ]}
        />
      )}

      {openBMKTTS01 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBMKTTS01(false)
          }}
          open={openBMKTTS01}
          item={dataViewKTTS01}
          title={t("Biên bản xử lý vật tư thay thế, sửa chữa")}
          urls={[
            ...REPAIRING.MINUTES_OF_HANDLING_SUPPLIES.GENERAL,
            ...REPAIRING.MINUTES_OF_HANDLING_SUPPLIES[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM0606 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0606(false)
          }}
          open={openBM0606}
          item={dataViewBM0606}
          title={t("Phiếu đánh giá sự cố")}
          urls={[
            ...REPAIRING.INCIDENT_RATING.GENERAL,
            ...REPAIRING.INCIDENT_RATING[currentOrg?.printCode],
          ]}
        />
      )}

      {openBM0607 && (
        <PrintMultipleFormDialog
          t={t}
          i18n={props.item?.i18n}
          handleClose={() => {
            setOpenBM0607(false)
          }}
          open={openBM0607}
          item={dataViewBM0607}
          title={t("Phiếu nghiệm thu")}
          urls={[
            ...REPAIRING.ACCEPTANCE.GENERAL,
            ...REPAIRING.ACCEPTANCE[currentOrg?.printCode],
          ]}
        />
      )}

      {shouldOpenConfirmDialog && (
        <ConfirmationDialog
          title={t("general.confirm")}
          open={shouldOpenConfirmDialog}
          onConfirmDialogClose={() => setShouldOpenConfirmDialog(false)}
          onYesClick={() => {
            setShouldOpenConfirmDialog(false);
            handleUpdateNghiemThu(tabDeviceTroubleshooting);
          }}
          text={t("ReportIncident.AssetAcceptanceForm.confirmTotalCost")}
          agree={t("general.saveAndNext")}
          cancel={t("general.edit")}
        />
      )}
    </Dialog>
  );
}

export default DeviceTroubleshootingDialog;
