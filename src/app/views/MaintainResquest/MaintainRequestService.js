import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/maintainRequest" + ConstantList.URL_PREFIX;
const API_PATH_REQUEST_COMMENT =
  ConstantList.API_ENPOINT + "/api/maintain_request_comment";
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT + "/api/fileDownload" + ConstantList.URL_PREFIX;
const API_GET_USER_DEPARTMENT_V1 =
  ConstantList.API_ENPOINT + "/api/v1/user-departments";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_AR = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const deleteMaintainRequest = (id) => {
  return axios.delete(API_PATH + "/" + id);
};

export const checkEditPermission = (id) => {
  return axios.get(API_PATH + "/checkEditPermission/" + id);
};

export const getMaintainRequestById = (id) => {
  return axios.get(API_PATH + "/" + id);
};

export const updateMaintainRequest = (asset) => {
  return axios.put(API_PATH + "/" + asset.id, asset);
};
export const saveMaintainRequest = (asset) => {
  return axios.post(API_PATH, asset);
};
export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_AR + "/download/excel/ar-su-co",
    data: searchObject,
    responseType: "blob",
  });
};

export const updateMaintainRequestComment = (comment, id) => {
  return axios.put(API_PATH_REQUEST_COMMENT + "/update/" + id, comment);
};
export const createMaintainRequestComment = (comment) => {
  return axios.post(API_PATH_REQUEST_COMMENT + "/create", comment);
};
export const searchByPageRequestComment = (searchObject) => {
  var url = API_PATH_REQUEST_COMMENT + "/searchByPage";
  return axios.post(url, searchObject);
};

export const deleteRequestComment = (id) => {
  return axios.delete(API_PATH_REQUEST_COMMENT + "/delete/" + id);
};
export const deleteCkeckRequestComment = (id) => {
  return axios.delete(API_PATH_REQUEST_COMMENT + "/deleteCheck/" + id);
};

export const getOneRequestComment = (id) => {
  return axios.get(API_PATH_REQUEST_COMMENT + "/getOne/" + id);
};

//http://asset-management-dev.oceantech.com.vn:80/api/ar-suco/search-by-page

export const getCountStatus = (payload) => {
  let config = {
    params: { ...payload },
  };
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/count-by-status",
    config
  );
};

export const searchByPageArSuco = (searchObject) => {
  var url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/search-by-page";
  return axios.post(url, searchObject);
};

//http://asset-management-dev.oceantech.com.vn:80/api/ar-suco "put update"
export const updateArSuCo = (searchObject) => {
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco";
  return axios.put(url, searchObject);
};

//http://asset-management-dev.oceantech.com.vn:80/api/ar-suco "post update"
export const createArSuCo = (searchObject) => {
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco";
  return axios.post(url, searchObject);
};

// api/ar-suco/{id} "get detail"
export const getDetailArSuCo = (id) => {
  return axios.get(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/" + id
  );
};
// api/ar-suco/{id} "detele"
export const deleteArSuCo = (id) => {
  return axios.delete(
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/" + id
  );
};

//http://asset-management-dev.oceantech.com.vn:80/api/ar-suco/assets "post search asset"

export const searchByPageAssetSuco = (searchObject) => {
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/assets";
  return axios.post(url, searchObject);
};

//http://asset-management-dev.oceantech.com.vn:80/api/ar-suco/nghiem-thu "put Nghiệm thu"

// http://asset-management-dev.oceantech.com.vn:80/api/ar-suco/tiep-nhan "put Tiếp nhận"
export const updateArSuCoTiepNhan = (searchObject) => {
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/tiep-nhan";
  return axios.put(url, searchObject);
};

//http://192.168.1.27:8065/asvn/api/product/org/searchByPage
export const searchByPageProductOrg = (searchObject) => {
  var url = ConstantList.API_ENPOINT + "/api/product/org/searchByPage";
  return axios.post(url, searchObject);
};
export const searchByPageProductOrgNew = (searchObject) => {
  let config = {
    params: searchObject
  }
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/products/search-by-page";
  return axios.get(url, config);
};

export const updateArSuCoNghiemThu = (searchObject) => {
  var url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ar-suco/nghiem-thu";
  return axios.put(url, searchObject);
};
export const checkTotalCost = (data) => {
  var url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
    "/api/ar-suco/kiem-tra-tong-chi-phi";
  return axios.post(url, null, { params: data });
};
export const searchByPageUser = (searchObject) => {
  var url =
    ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/users/search-by-page";
  return axios.post(url, searchObject);
};

export const getAllDepartmentByPage = (searchObject) => {
  var url =
    ConstantList.API_ENPOINT + "/api/assetDepartment/org/searchByPageDeparment";
  return axios.post(url, {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  var url =
    ConstantList.API_ENPOINT +
    "/api/assetDepartment/org/management-departments";
  return axios.get(url, config);
};
export const getAllUserDepartment = ({
  departmentId,
  keyword,
  pageIndex,
  pageSize,
}) => {
  let config = {
    params: {
      departmentId,
      keyword,
      pageIndex,
      pageSize,
      isNotAdminUser: true,
      isMainDepartment: true,
    },
  };
  return axios.get(API_GET_USER_DEPARTMENT_V1 + "/page", config);
};
export const searchByPageListUserDepartment = (SeachObjectct) => {
  let url = API_PATH_ASSET_DEPARTMENT + "/searchByPage";
  return axios.post(url, SeachObjectct);
};
export const downLoadArAttachment = (id) => {
  const url =
    ConstantList.API_ENPOINT + "/api/fileDownload/assetDocument/" + id;
  return axios.get(url, { responseType: "arraybuffer" });
};
export const downLoadAttachment = (id) => {
  const url = API_PATH_AR + "/download/document/" + id;
  return axios.get(url, { responseType: "arraybuffer" });
};

export const getPhuTungByAsset = (searchObject) => {
  // list phụ tùng
  let url = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/asset/phu-tung";
  return axios.post(url, searchObject);
};

export const checkIsValidTimes = (params) => {
  //Check cảnh báo nếu ts đã sửa >= 3 lần/ năm
  return axios.get(API_PATH_AR + `/ar-suco/is-valid-times`, {
    params,
  });
};

export const uploadIncidentImages = (formData) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(
    API_PATH_AR + "/ar-attachment/upload",
    formData,
    config
  );
};

export const deleteIncidentImage = (id) => {
  return axios.delete(`${API_PATH_AR}/ar-attachment/${id}`);
};

export const getDetailLinkKien = (payload) => {
  return axios.post(
    API_PATH_AR + "/ar-suco/linh-kien-vat-tu-xuat-kho",
    payload
  );
};

export const deleteSupplies = (id) => {
  return axios.delete(`${API_PATH_AR}/ar-suco/${id}/supplies`);
};