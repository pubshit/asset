import React, { Component } from "react";
import {
  IconButton,
  Dialog,
  Button,
  Icon,
  Grid,
  FormControlLabel,
  TablePagination,
  Switch,
  DialogActions,
  Checkbox,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  InputAdornment,
  Card ,TextField
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import NumberFormat from 'react-number-format';
import { useTranslation, withTranslation, Trans, composeInitialProps } from 'react-i18next';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import VoucherFilePopup from './VoucherFilePopup'
import { searchByPage as maintainStatusSearchByPage } from "../MaintainRequestStatus/MaintainRequestStatusService"
import SelectAssetPopup from "../Component/Asset/SelectAssetPopup";
import SelectManagementDepartmentPopup from '../Component/Department/SelectManagementDepartmentPopup'
import SelectPersonPopup from '../Component/Person/SelectPersonPopup';
import RequestCommentDialog from './RequestCommentDialog'; 

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;

  return <div>
    <IconButton size='small' onClick={() => props.onSelect(item, 1)}>
      <Icon fontSize="small" color="error">delete</Icon>
    </IconButton>
  </div>;
}

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
    position: "absolute",
    top: '-10px',
    left: '-25px',
    width: '80px'
  }
}))(Tooltip);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,

          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default function MaintainScrollableTabsButtonForce(props) {
  const t = props.t
  const i18n = props.i18n
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const comments = props.item.comments
  const currentUser = props.item.currentUser
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let columnsVoucherFile = [
    {
      title: t('general.stt'),
      field: 'code',
      width: '50px',
      align: 'center',
      headerStyle: {
        paddingLeft: '10px',
        paddingRight: '10px',
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    { title: t("AssetFile.code"), field: "code", align: "left", width: "200px" },
    { title: t("AssetFile.name"), field: "name", align: "left", width: "250px" },
    { title: t("AssetFile.description"), field: "description", align: "left", width: "250px" },
    {
      title: t("general.action"),
      field: "valueText",
      width: "150px",
      render: rowData =>
        <div className="none_wrap">
          <LightTooltip title={t('general.editIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellEditAssetFile(rowData)}>
              <Icon fontSize="small" color="primary">edit</Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip title={t('general.deleteIcon')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton size="small" onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}>
              <Icon fontSize="small" color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
        </div>
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          // variant="fullWidth"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t('Thông tin phiếu')} {...a11yProps(0)} />
          <Tab label={t('Hồ sơ đính kèm')} {...a11yProps(1)} />
          <Tab label={t('Ghi chú')} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={2}>
          <Grid className="" item md={6} sm={12} xs={12}>
            <TextValidator
              className="w-100 "
              label={<span><span className="colorRed">* </span> <span> {t("maintainRequest.name")}</span></span>}
              onChange={props.handleChange}
              type="text"
              name="name"
              value={props.item.name}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid className="" item md={3} sm={12} xs={12}>
            {!props.isNew ? <>
              <AsynchronousAutocomplete label={<span><span className="colorRed">* </span> <span> {t("maintainRequest.status")}</span></span>}
                searchFunction={maintainStatusSearchByPage}
                searchObject={props.searchObject}
                defaultValue={props.item.status}
                displayLable={'name'}
                value={props.item.status}
                onSelect={props.selectMaintainStatus}
                validators={["required"]}
                errorMessages={[t('general.required')]}
              />
            </> :
              <Autocomplete
                id="combo-box"
                fullWidth
                size="small"
                options={[]}
                disabled
                value={props.item.status}
                getOptionLabel={(option) => option.name.toString()}
                renderInput={(params) => <TextField {...params} label={t("maintainRequest.status")} variant="standard" />}
              />}
          </Grid>
          <Grid className="" item md={3} sm={12} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DateTimePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t('maintainRequest.dateRequest')}
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={'dateRequest'}
                value={props.item.dateRequest}
                onChange={date => props.handleDateChange(date, "dateRequest")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid className="" item md={6} sm={12} xs={12}>
            <Button
              size="small"
              style={{ float: 'right' }}
              className=" mt-12"
              variant="contained"
              color="primary"
              onClick={props.handleDepartmentPopupOpen}
            >
              {t('general.select')}
            </Button>
            <TextValidator
              InputProps={{
                readOnly: true,
              }}
              label={<span><span className="colorRed">* </span> <span> {t("maintainRequest.depRequest")}</span></span>}
              className="w-80  mr-16"
              value={props.item.depRequest != null ? props.item.depRequest.name : ''}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />

            {props.item.shouldOpenDepartmentPopup && (
              <SelectDepartmentPopup
                open={props.item.shouldOpenDepartmentPopup}
                handleSelect={props.handleSelectDepRequest}
                selectedItem={props.item.depRequest != null ? props.item.depRequest : {}}
                handleClose={props.handleDepartmentPopupClose} t={t} i18n={i18n} />
            )}
          </Grid>
          <Grid className="" item md={6} sm={12} xs={12}>
            <Button
              size='small'
              style={{ float: 'right' }}
              className=" mt-12"
              variant="contained"
              color="primary"
              onClick={props.openPopupSelectAsset}
            >
              {t('general.select')}
            </Button>
            <TextValidator
              InputProps={{
                readOnly: true,
              }}
              label={<span><span className="colorRed">* </span> <span> {t("maintainRequest.asset")}</span></span>}
              className="w-80  mr-16"
              value={props.item.asset != null ? props.item.asset.name : ''}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />

            {props.item.depRequest && props.item.shouldOpenAssetPopup && (
              <SelectAssetPopup
                open={props.item.shouldOpenAssetPopup}
                handleSelect={props.handleSelectAsset}
                selectedItem={props.item.asset != null ? props.item.asset : {}}
                departmentId={props.item.departmentId}
                handleClose={props.handleAssetPopupClose} t={t} i18n={i18n} />
            )}
          </Grid>

          <Grid className="" item md={6} sm={12} xs={12} >
            <Button
              style={{ float: 'right' }}
              size="small"
              className=" mt-12"
              variant="contained"
              color="primary"
              onClick={props.openSelectManagementDepartmentPopup}
            >
              {t('general.select')}
            </Button>
            <TextValidator
              size="small"
              // disabled={true}
              InputProps={{
                readOnly: true,
              }}
              label={<span><span className="colorRed">*</span>{t('allocation_asset.receiverDepartment')}</span>}
              className="mr-16"
              style={{ width: "80%", fontWeight: "bold" }}
              value={props.item.managementDepartment != null ? props.item.managementDepartment.name : ''}
              validators={['required']}
              errorMessages={[t('general.required')]}
            />

            {props.item.shouldOpenSelectManagementDepartmentPopup && (
              <SelectManagementDepartmentPopup
                open={props.item.shouldOpenSelectManagementDepartmentPopup}
                handleSelect={props.handleSelectManagementDepartment}
                selectedItem={
                  props.item.managementDepartment != null
                    ? props.item.managementDepartment
                    : {}
                }
                handleClose={props.handleSelectManagementDepartmentPopupClose}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>

          <Grid item md={6} sm={12} xs={12} className="">
            <Button
              size='small'
              style={{ float: 'right' }}
              className=" mt-12"
              variant="contained"
              color="primary"
              onClick={() =>
                props.handleReceiverPersonPopupOpen()
              }
            >
              {t('general.select')}
            </Button>
            <TextValidator
              InputProps={{
                readOnly: true,
              }}
              label={<span><span className="colorRed">*</span>{t('maintainRequest.receiverPerson')}</span>}
              className="w-80"
              value={props.item.receiverPerson != null ? props.item.receiverPerson.displayName : ''}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />

            {props.item.shouldOpenReceiverPersonPopup && (
              <SelectPersonPopup
                open={props.item.shouldOpenReceiverPersonPopup}
                handleSelect={props.handleSelectReceiverPerson}
                selectedItem={props.item.receiverPerson ? props.item.receiverPerson : {}}
                handleClose={props.handleReceiverPersonPopupClose}
                receiverDepartmentId={props.item.managementDepartment != null ? props.item.managementDepartment.id : ''}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>

          <Grid className="" item md={3} sm={12} xs={12} >
            <TextValidator
              className="w-100"
              label={t('maintainRequest.maintain_request_costs')}
              onChange={props.handleChangeMaintainCost}
              name="originalCost"
              value={props.item.maintainCost}
              id="formatted-numberformat-originalCost"
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
            />
          </Grid>

          <Grid className="" item md={9} sm={12} xs={12}>
            <TextValidator
              className="w-100 "
              label={t("maintainRequest.note")}
              onChange={props.handleChange}
              type="text"
              name="note"
              value={props.item.note}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={12} sm={12} xs={12}>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={
              props.handleAddAssetDocumentItem
            }
          >
            {t('AssetFile.addAssetFile')}
          </Button>
          {props.item.shouldOpenPopupAssetFile && (
            <VoucherFilePopup
              open={props.item.shouldOpenPopupAssetFile}
              updatePageAssetDocument={props.updatePageAssetDocument}
              handleClose={props.handleAssetFilePopupClose}
              itemAssetDocument={props.itemAssetDocument}
              getAssetDocumentId={props.getAssetDocumentId}
              item={props.item}
              t={t}
              i18n={i18n}
            />)
          }
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={props.item.assetDocumentList}
            columns={columnsVoucherFile}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              padding: 'dense',
              rowStyle: rowData => ({
                backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: '#358600',
                color: '#fff',
              },
              maxBodyHeight: '285px',
              minBodyHeight: '285px',
            }}
            components={{
              Toolbar: props => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
              }
            }}
          />
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Grid container spacing={2}>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={
                props.openRequestCommentDialog
              }
            >
              {t('Thêm ghi chú')}
            </Button>
            {props.item.shouldOpenCommentDialog && (
              <RequestCommentDialog
                open={props.item.shouldOpenCommentDialog}
                handleSelectRequestComment={props.handleSelectRequestComment}
                handleCloseComment={props.handleRequestCommentClose}
                SearchByPageRequestComment={props.SearchByPageRequestComment}
                getRequestCommentId={props.getRequestCommentId}
                editComment={props.item.comment}
                item={props.item}
                t={t}
                i18n={i18n}
              />)
            }
          </Grid>
          {
            comments?.map((item, index) => {
              return (
                <Card className="mb-6 w-100 p-8" elevation={2} style={{ marginBottom: '3px' }}>
                  <Grid container spacing={2} className="mb-3">
                    <Grid item md={3} sm={12} xs={12}>{item.user.displayName}</Grid>
                    <Grid item md={8} sm={12} xs={12}>{item.comment}</Grid>
                    <Grid item md={1} sm={12} xs={12}>
                      {(item.user.id === currentUser.id) &&
                        <div>
                          <IconButton size="small" title={t('general.removeDocument')} onClick={() => props.deleteCkeckRequestComment(item.id)}>
                            <Icon fontSize="small" color="error">delete</Icon>
                          </IconButton>

                          <IconButton size="small" title={t('general.removeDocument')} onClick={() => props.handleEditRequestComment(item.id)}>
                            <Icon fontSize="small" color="primary">edit</Icon>
                          </IconButton>
                        </div>
                      }
                    </Grid>
                  </Grid>
                </Card>
              )
            })
          }
        </Grid>
      </TabPanel>
    </div >
  );
}