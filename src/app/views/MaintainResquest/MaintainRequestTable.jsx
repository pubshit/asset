import React from "react";
import {
  Grid,
  Icon,
  TablePagination,
  Button,
  FormControl,
  Input,
  InputAdornment,
  AppBar,
  Collapse,
  Card,
  TextField, Checkbox, FormControlLabel,
} from "@material-ui/core";
import {
  exportToExcel,
  searchByPageArSuco,
  deleteArSuCo,
  getCountStatus,
  getAllDepartmentByPage, getDetailArSuCo,
} from "./MaintainRequestService";
import { Breadcrumb, ConfirmationDialog } from "egret";
import moment from "moment";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomMaterialTable from "../CustomMaterialTable";
import { STATUS_REPORT, appConst, variable } from "app/appConst";
import ReportIncidentDialog from "./Dialog/ReportIncidentDialog";
import DeviceTroubleshootingDialog from "./Dialog/DeviceTroubleshootingDialog";
import MaterialTable, { MTableToolbar } from "material-table";
import AppContext from "app/appContext";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { Autocomplete } from "@material-ui/lab";
import {
  convertFromToDate,
  filterOptions,
  handleKeyUp,
  handleKeyDown,
  getUserInformation,
  getTheHighestRole,
  isSuccessfulResponse,
  handleThrowResponseMessage,
  isInValidDateForSubmit,
  isValidDate, functionExportToExcel, formatTimestampToDate
} from "app/appFunction";
import { GeneralTab, LightTooltip } from "../Component/Utilities";
import CardContent from "@material-ui/core/CardContent";
import { withRouter } from "react-router-dom";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import ValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "../../services/localStorageService";
import AssetExpectedRevocationDialog from "../RecallSlip/AssetExpectedRevocationDialog";
import AssetRevocationVoucherDialog from "../RecallSlip/AssetRevocationVoucherDialog";
import { ActionsComponent } from "../Component/Repair";


toast.configure({
  autoClose: 3000,
  draggable: false,
  limit: 3,
});

class MaintainRequestTable extends React.Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    qualificationList: [],
    item: {},
    shouldOpenReportIncidentDialog: false,
    shouldOpenDeviceTroubleshootingDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    keyword: "",
    isPrint: false,
    hasCreatePermission: false,
    hasDeletePermission: false,
    tabValue: 0,
    itemEdit: null,
    isAdd: false,
    reportDateFrom: null,
    reportDateTo: null,
    listDepartment: [],
    departmentFilter: null,
    query: {
      keyword: '',
      pageIndex: 0,
      pageSize: 999
    },
    openAdvanceSearch: true,
    isFullyDepreciatedAsset: false,
    isExpiredAssetHasVTNotRepaired: false,
    arTgCanCoVtTo: null,
    tsSoNamKhConLai: null,
    checkAll: false,
    listChecked: [],
  };

  handleGetListManagementDepartment = async () => {
    const searchObject = {};
    let { query } = this.state
    searchObject.pageIndex = query.pageIndex + 1
    searchObject.pageSize = query.pageSize
    searchObject.keyword = query.keyword
    try {
      const result = await getAllDepartmentByPage(searchObject)
      if (result?.status === appConst.CODE.SUCCESS) {
        this.setState({
          listDepartment: result?.data?.content
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
    }
  }

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageDataArSuco();
    });
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => {
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageDataArSuco(this.state.tabValue);
    });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search)

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(appConst.PAGE_INDEX.FIRST_PAGE);
  }

  updatePageDataArSuco = () => {
    const { t } = this.props
    const {
      page,
      keyword = "",
      rowsPerPage,
      reportDateTo,
      reportStatus,
      reportDateFrom,
      departmentFilter,
      arTgCanCoVtTo,
      tsSoNamKhConLai,
      tabValue,
      arHinhThuc,
      arNtThoiGianFrom,
      arNtThoiGianTo,
      arNghiemThuStatus,
      selectedList,
    } = this.state;

    let { setPageLoading } = this.context
    let searchObject = {
      arTgCanCoVtTo,
      tsSoNamKhConLai,
      arPhongBanId: departmentFilter?.id,
      arTrangThai: tabValue === appConst.tabStatus.tabAll
        ? reportStatus?.id
        : tabValue,
      assetClass: appConst.assetClass.TSCD
    };
    searchObject.keyword = keyword?.trim();
    searchObject.pageIndex = page + 1;
    searchObject.pageSize = rowsPerPage;
    searchObject.arHinhThuc = arHinhThuc?.code;
    searchObject.arNghiemThuStatus = arNghiemThuStatus?.code;
    if (isValidDate(arNtThoiGianFrom)) {
      searchObject.arNtThoiGianFrom = convertFromToDate(arNtThoiGianFrom).fromDate;
    }
    if (isValidDate(arNtThoiGianTo)) {
      searchObject.arNtThoiGianTo = convertFromToDate(arNtThoiGianTo).toDate;
    }
    if (reportDateTo) {
      searchObject.reportDateTo = convertFromToDate(reportDateTo).fromDate;
    }
    if (reportDateFrom) {
      searchObject.reportDateFrom = convertFromToDate(reportDateFrom).toDate;
    }
    setPageLoading(true)

    searchByPageArSuco(searchObject).then(({ data }) => {
      if (appConst.CODE.SUCCESS === data.code && data.data !== null) {
        let countItemChecked = 0;
        const itemList = data?.data?.content?.map(item => {
          if (selectedList?.find(checkedItem => checkedItem === item.id)) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
          }
        })
        this.setState({
          itemList: itemList || [],
          totalElements: data?.data?.totalElements,
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0,
          page: itemList?.length ? 0 : this.state.page,
        });
      } else {
        toast.warning(data?.message);
      }
      setPageLoading(false)
    }).catch((error) => {
      toast.error(t("general.error"))
    });
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  handleGetCountStatus = async () => {
    let { t } = this.props;
    let countStatusWaiting, countStatusProcessing, countStatusProcessed;
    try {
      let searchObject = {
        assetClass: appConst.assetClass.TSCD,
      }
      const result = await getCountStatus(searchObject)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        // eslint-disable-next-line no-unused-expressions
        result?.data?.data?.forEach((item) => {
          if (item?.trangThai === STATUS_REPORT.WAITING) {
            countStatusWaiting = this.checkCount(item?.soLuong);
          } else if (item?.trangThai === STATUS_REPORT.PROCESSING) {
            countStatusProcessing = this.checkCount(item?.soLuong);
          } else if (item?.trangThai === STATUS_REPORT.PROCESSED) {
            countStatusProcessed = this.checkCount(item?.soLuong);
          }
        });
        this.setState({ countStatusWaiting, countStatusProcessing, countStatusProcessed })
      }
    } catch (error) {
      toast.error(t("general.error"));
    }

  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenReportIncidentDialog: false,
      shouldOpenDeviceTroubleshootingDialog: false,
      shouldOpenPrintSummaryForm: false,
      shouldOpenEditorReCallAsset: false,
      shouldOpenEditorReceiptForm: false,
      shouldOpenConfirmationExcel: false,
      isPrint: false,
      item: {}
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenConfirmationDialog: false,
      shouldOpenEditorReCallAsset: false,
      shouldOpenEditorReceiptForm: false,
      item: {}
    });
    this.updatePageDataArSuco();
    this.handleGetCountStatus();
  };

  handleDeleteItem = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleConfirmationResponse = async () => {
    try {
      const result = await deleteArSuCo(this.state.id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        toast.info(this.props.t("general.deleteSuccess"));
        this.updatePageDataArSuco();
        this.handleDialogClose();
        this.handleGetCountStatus();
      } else {
        toast.error(this.props.t("general.error"));
      }
    } catch (error) {
      toast.error(this.props.t("general.error"));
    }
  };

  async componentDidMount() {
    await this.handleDataAfterRedirect();
    this.updatePageDataArSuco();
    this.getRoleCurrentUser();
    this.handleGetCountStatus();
    this.handleGetListManagementDepartment();

    const { isRoleAssetUser, departmentUser } = getTheHighestRole();
    if (isRoleAssetUser) {
      this.setState({ departmentFilter: departmentUser?.id ? departmentUser : null })
    }
  }

  handleDataAfterRedirect = () => {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleView({ id: location?.state?.id })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleEdit({ id: location?.state?.id })
          break;
        default:
          break;
      }
    } else {
      this.setState({
        ...location?.state,
      })
    }

    window.addEventListener('beforeunload', () => {
      history.push(location?.state?.path, null);
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.query !== prevState.query) {
      this.handleGetListManagementDepartment()
    }
  }

  getRoleCurrentUser = () => {
    let { hasCreatePermission, hasDeletePermission } = this.state;
    const user = getUserInformation();
    const roles = getTheHighestRole();
    const {
      isRoleOrgAdmin,
      isRoleAssetManager,
      isRoleAssetUser,
      isRoleUser,
    } = roles;

    if (isRoleOrgAdmin) {
      hasDeletePermission = true;
      hasCreatePermission = true;
    } else if (isRoleAssetManager) {
      hasDeletePermission = true;
      hasCreatePermission = true;
    } else if (isRoleAssetUser) {
      hasDeletePermission = true;
      hasCreatePermission = false;
    } else if (isRoleUser) {
      hasDeletePermission = false;
      hasCreatePermission = false;
    }

    this.setState({
      ...roles,
      ...user,
      hasCreatePermission,
      hasDeletePermission,
    })
  };

  handleOpenReportIncidentDialog = () => {
    this.setState({
      shouldOpenReportIncidentDialog: true,
    });
  };

  handleOpenDeviceTroubleshootingDialog = (value) => {
    this.setState({
      // itemEdit: value,
      shouldOpenDeviceTroubleshootingDialog: true
    })
  }

  handleDeviceTroubleshootingDialogClose = (tabValue) => {
    this.setState({
      shouldOpenDeviceTroubleshootingDialog: false,
      isView: false,
      itemEdit: {}
    });
    this.updatePageDataArSuco(tabValue);
  };

  handleReportIncidentDialogClose = () => {
    this.setState({
      shouldOpenReportIncidentDialog: false,
      itemEdit: {}
    });
    this.updatePageDataArSuco();
  };

  handleDelete = (rowData) => {
    this.setState({
      id: rowData?.id,
      shouldOpenConfirmationDialog: true,
    });
  };

  /* Export to excel */
  exportToExcel = async () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    let {
      reportStatus,
      reportDateTo,
      reportDateFrom,
      tabValue,
      departmentFilter,
      keyword = "",
      arTgCanCoVtTo,
      tsSoNamKhConLai,
      arHinhThuc,
      arNghiemThuStatus,
      arNtThoiGianFrom,
      arNtThoiGianTo
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {};
      searchObject.assetClass = appConst.assetClass.TSCD;
      searchObject.keyword = keyword?.trim();
      searchObject.arTgCanCoVtTo = arTgCanCoVtTo;
      searchObject.tsSoNamKhConLai = tsSoNamKhConLai;
      searchObject.arHinhThuc = arHinhThuc?.code;
      searchObject.arNghiemThuStatus = arNghiemThuStatus?.code;
      searchObject.arTrangThai = tabValue === appConst.tabStatus.tabAll
        ? reportStatus?.id
        : tabValue;
      searchObject.arPhongBanId = departmentFilter?.id
      if (reportDateTo) {
        searchObject.reportDateTo = convertFromToDate(reportDateTo).fromDate;
      }
      if (reportDateFrom) {
        searchObject.reportDateFrom = convertFromToDate(reportDateFrom).toDate;
      }

      if (isValidDate(arNtThoiGianFrom)) {
        searchObject.arNtThoiGianFrom = convertFromToDate(arNtThoiGianFrom).fromDate;
      }
      if (isValidDate(arNtThoiGianTo)) {
        searchObject.arNtThoiGianTo = convertFromToDate(arNtThoiGianTo).toDate;
      }
      await functionExportToExcel(
        exportToExcel,
        searchObject,
        t("exportToExcel.maintainRequest")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      this.setState({ shouldOpenConfirmationExcel: false });
      setPageLoading(false);
    }
  };

  handleChangeTabValue = (event, value) => {
    this.setState({ tabValue: value }, () => {
      this.setPage(appConst.PAGE_INDEX.FIRST_PAGE)
      this.handleGetCountStatus();
    });
  };

  handleChangeItemEdit = (e) => {
    this.setState({
      itemEdit: {
        ...this.state.itemEdit,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleCheckStatus = (rowData) => {
    let status = rowData?.arTrangThai;
    let className = {
      [STATUS_REPORT.WAITING]: 'info',
      [STATUS_REPORT.PROCESSING]: 'warning',
      [STATUS_REPORT.PROCESSED]: 'success',
    }
    let itemStatus = appConst.listStatusReportIncidentForm.find(
      (item) => item.id === status
    );

    return (
      <div className="align-center">
        <span className={`status status-${className[status]}`}>
          {itemStatus?.name}
        </span>
      </div>
    );
  };

  handleSetData = (value, name) => {
    this.setState({
      [name]: value,
    }, () => {
      let { reportDateFrom, reportDateTo } = this.state;
      const isInValidDate = (
        reportDateFrom && !isValidDate(reportDateFrom)
      ) || (
          reportDateTo && !isValidDate(reportDateTo)
        )

      if (!isInValidDateForSubmit() && !isInValidDate) {
        this.search();
      }
    });
  }

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleChangeSelect = (name, value) => {
    this.setState({
      [name]: value
    },
      () => {
        this.search()
      }
    )
  }

  handleSearchDepartment = (event, source) => {
    this.setState({
      query: {
        ...this.state.query,
        keyword: event.target.value
      }
    })
  }

  handleKeyUp = (e) => handleKeyUp(e, this.search)

  handleEdit = (rowData) => {
    this.handleGetDetailArSuCo(rowData?.id, () => {
      this.setState({ isAdd: false })
      this.handleOpenDeviceTroubleshootingDialog();
    })
  }
  handlePrint = (rowData) => {
    this.handleGetDetailArSuCo(rowData?.id, () => {
      this.setState({ isPrint: true })
    })
  }

  handleView = (rowData) => {
    this.handleGetDetailArSuCo(rowData?.id, () => {
      this.setState({ isAdd: false, isView: true }, () => {
        this.handleOpenDeviceTroubleshootingDialog(rowData);
      })
    })
  }

  handleGetDetailArSuCo = async (id, callBack = () => { }) => {
    let { setPageLoading } = this.context;
    let { t } = this.props;

    try {
      setPageLoading(true);
      const result = await getDetailArSuCo(id);
      const { code, data } = result?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemEdit: data,
        }, () => {
          callBack?.();
        })
      } else {
        handleThrowResponseMessage(result);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  }

  handleCheck = e => {
    let { arTgCanCoVtTo, tsSoNamKhConLai } = this.state;

    if (variable.listInputName.isExpiredAssetHasVTNotRepaired === e.target.name) {
      arTgCanCoVtTo = e.target.checked ? convertFromToDate(new Date()).fromDate : null;
    } else if (variable.listInputName.isFullyDepreciatedAsset === e.target.name) {
      tsSoNamKhConLai = e.target.checked ? 0 : null;
    }

    this.setState({
      [e.target.name]: e.target.checked,
      arTgCanCoVtTo,
      tsSoNamKhConLai,
    }, this.search);
  }

  handleChangeSelectTable = (event, item) => {
    let { selectedList, itemList, listChecked } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        listChecked.push(item)
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        this.setState({
          selectedList: filteredArray,
          listChecked: filteredArray,
        })
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);
      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      if (!event.target.checked) {
        listChecked = listChecked.filter(id => id?.id !== item?.id);
      } else {
        listChecked.push(item);
      }
      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
        listChecked,
      });
    }
  };
  validDataToReCallAsset = (data = [], isReceiptForm = false) => {
    const { departmentUser } = getUserInformation();
    const { isRoleAssetManager, isRoleAdmin, isRoleOrgAdmin } = getTheHighestRole();

    let check = false;
    const isGreaterThanRoleAM = isRoleAdmin || isRoleOrgAdmin
    const isOkStatus = data?.every(i => i?.tsStatusIndexOrder !== appConst.tabStatus.tabApproved)
    const isOkDepartment = data?.every(i => i?.tsPhongBanId === data[0]?.tsPhongBanId);
    const isOkUseDepartment = data?.every(i => i?.tsUseDepartmentId === departmentUser?.id);
    const isOkDepartmentLogged = data?.every(i => i?.tsPhongBanId === departmentUser?.id);
    const isNotOkUseProductDepartment = data?.every(i => i?.tsUseDepartmentId);
    if (!data.length) {
      check = true;
      toast.warning("Chưa có tài sản nào được chọn")
      return true;
    }
    if (!isOkStatus) {
      check = true;
      toast.warning("Tài sản được chọn có trạng thái không hợp lệ")
      return true;
    }
    if (!isOkDepartment) {
      check = true;
      toast.warning("Các tài sản được chọn không cùng phòng ban quản lý")
      return true;
    }
    if (!isOkUseDepartment && !isReceiptForm) {
      check = true;
      toast.warning("Tài sản phải thuộc phòng ban sử dụng hiện tại")
      return true;
    }
    if (isRoleAssetManager && !isOkDepartmentLogged && !isGreaterThanRoleAM && isReceiptForm) {
      check = true;
      toast.warning("Phòng ban quản lý không phù hợp");
      return true;
    }
    if (!isNotOkUseProductDepartment && isReceiptForm) {
      check = true;
      toast.warning("Tài sản không có phòng ban sử dụng");
      return true;
    }
    return false;
  }
  handleOpenEditorReCallAsset = () => {
    let { listChecked = [] } = this.state;
    const { departmentUser } = getUserInformation();
    if (this?.validDataToReCallAsset(listChecked)) {
      return;
    } else {
      let convertItemEdit = {
        handoverDepartment: departmentUser || null,
        receiverDepartment: {
          id: listChecked[0]?.tsPhongBanId,
          text: listChecked[0]?.tsPhongBanText,
        },
        assetVouchers: listChecked?.map(i => {
          return {
            asset: {
              assetId: i?.tsId,
              code: i?.tsMa,
              managementCode: i?.tsMaquanly,
              name: i?.tsTen,
              serialNumber: i?.tsSerialNo,
              model: i?.tsModel,
              yearPutIntoUse: i?.tsNamSd,
              yearOfManufacture: i?.tsNamSx,
              manufacturerName: i?.tsHangSx,
              originalCost: i?.tsNguyenGia,
              carryingAmount: i?.tsGiaTriConLai,
            },
            assetManagementDepartmentName: i?.arPhongBanText,
            note: i?.note,
            assetId: i?.tsId,
          }
        })
      }
      this.setState({ shouldOpenEditorReCallAsset: true, itemEdit: convertItemEdit })
    }
  }
  handleOpenEditorReceiptForm = () => {
    let { listChecked = [] } = this.state;
    const { departmentUser } = getUserInformation();
    if (this?.validDataToReCallAsset(listChecked, true)) {
      return;
    } else {
      let convertItemEdit = {
        handoverDepartment: departmentUser || null,
        receiverDepartment: {
          id: listChecked[0]?.tsPhongBanId,
          text: listChecked[0]?.tsPhongBanText,
        },
        assetVouchers: listChecked?.map(i => {
          return {
            asset: {
              assetId: i?.tsId,
              code: i?.tsMa,
              managementCode: i?.tsMaquanly,
              name: i?.tsTen,
              serialNumber: i?.tsSerialNo,
              model: i?.tsModel,
              yearPutIntoUse: i?.tsNamSd,
              yearOfManufacture: i?.tsNamSx,
              manufacturerName: i?.tsHangSx,
              originalCost: i?.tsNguyenGia,
              carryingAmount: i?.tsGiaTriConLai,
            },
            useDepartmentId: i?.tsUseDepartmentId,
            assetManagementDepartmentName: i?.arPhongBanText,
            note: i?.note,
            assetId: i?.tsId,
          }
        })
      }
      this.setState({ shouldOpenEditorReceiptForm: true, itemEdit: convertItemEdit })
    }
  }

  getListTab = () => {
    let { t } = this.props;
    let { tabAll, tablPending, tabProcessing, tabApproved } = appConst.tabStatus;
    return [
      {
        key: tabAll,
        label: t("maintainRequest.tab.all")
      },
      {
        key: tablPending,
        label: t("maintainRequest.tab.pending"),
        count: this.state.countStatusWaiting || 0,
        type: "info",
      },
      {
        key: tabProcessing,
        label: t("maintainRequest.tab.processing"),
        count: this.state.countStatusProcessing || 0,
        type: "warning",
      },
      {
        key: tabApproved,
        label: t("maintainRequest.tab.approved"),
        count: this.state.countStatusProcessed || 0,
        type: "success",
      },
    ];
  }

  render() {
    const { t, i18n } = this.props;
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenConfirmationExcel,
      shouldOpenEditorReCallAsset,
      shouldOpenEditorReceiptForm,
      shouldOpenReportIncidentDialog,
      shouldOpenDeviceTroubleshootingDialog,
      isPrint,
      hasDeletePermission,
      hasCreatePermission,
      isRoleOrgAdmin,
      isRoleAssetManager,
      isRoleAssetUser,
      isRoleAccountant,
      isRoleAdmin,
      tabValue,
      itemEdit,
      isAdd,
      reportDateFrom,
      reportDateTo,
      openAdvanceSearch,
      reportStatus,
      departmentFilter,
      arHinhThuc,
      arNtThoiGianFrom,
      arNtThoiGianTo,
      listDepartment,
      isView,
      isExpiredAssetHasVTNotRepaired,
      isFullyDepreciatedAsset,
      arNghiemThuStatus,
      checkAll,
      selectedList,
    } = this.state;

    let isShowBtnRecall = (isRoleAssetManager || isRoleAssetUser) && arNghiemThuStatus?.code === appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code
    let isShowBtnreceiptForm = (isRoleAssetManager || isRoleOrgAdmin || isRoleAdmin) && arNghiemThuStatus?.code === appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code

    let TitlePage = t("maintainRequest.title");
    const formatDate = (date) => date ? moment(date).format("DD/MM/YYYY") : ""
    const isTabAll = appConst.tabStatus.tabAll === tabValue;
    const tabs = this.getListTab();
    const isTabChoXuLy = appConst.tabStatus.tablPending === tabValue;
    const isTabDangXuLy = appConst.tabStatus.tabProcessing === tabValue;
    let titleName = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    );
    const newDate = new Date(itemEdit?.arTgBaoCao);
    const day = String(newDate?.getDate())?.padStart(2, "0");
    const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
    const year = String(newDate?.getFullYear());

    let dataView = {
      ...itemEdit,
      titleName,
      arPhongBanText: itemEdit?.arPhongBanText || "..".repeat(30),
      arTnPhongBanText: itemEdit?.arTnPhongBanText || "Khoa/Phòng ..................",
      day: itemEdit?.arTgBaoCao ? day : " ..... ",
      month: itemEdit?.arTgBaoCao ? month : " ..... ",
      year: itemEdit?.arTgBaoCao ? year : " ..... ",
      amChukys: [{
        index: 1,
        eqName: `${itemEdit?.tsTen ? itemEdit?.tsTen : ""} ${itemEdit?.tsMa ? `; ${itemEdit?.tsMa}` : ""}`,
        ckTrangthaiName: itemEdit?.arTrangThaiMoTa
      }]
    };

    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleChangeSelectTable(e)}
            />
          </>
        ),
        hidden: !(isShowBtnreceiptForm || isShowBtnRecall),
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData.checked ? rowData.checked : false}
            onChange={(e) => {
              this.handleChangeSelectTable(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 80,
        cellStyle: {
          borderRight: "1px white solid !important",
          textAlign: "center"
        },
        render: (rowData) => (
          <ActionsComponent
            rowData={rowData}
            item={this.state}
            hasDeletePermission={hasDeletePermission}
            hasCreatePermission={hasCreatePermission}
            onSelect={(rowData, method) => {
              let actionByMethod = {
                [appConst.active.edit]: this.handleEdit,
                [appConst.active.delete]: this.handleDelete,
                [appConst.active.view]: this.handleView,
                [appConst.active.print]: this.handlePrint,
              }
              if (Object.keys(actionByMethod)?.includes(method.toString())) {
                actionByMethod[method]?.(rowData)
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        width: "50px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("maintainRequest.columns.reportDay"),
        field: "arTgBaoCao",
        minWidth: "140px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatDate(rowData?.arTgBaoCao),
      },
      {
        title: t("maintainRequest.columns.confirmDay"),
        field: "arTgXacNhan",
        hidden: STATUS_REPORT.WAITING === tabValue,
        minWidth: "160px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatTimestampToDate(rowData?.arTgXacNhan),
      },
      {
        title: t("maintainRequest.columns.finishDay"),
        field: "arNtThoiGian",
        hidden: STATUS_REPORT.PROCESSED !== tabValue,
        minWidth: "140px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => formatTimestampToDate(rowData?.arNtThoiGian),
      },
      {
        title: t("maintainRequest.columns.status"),
        field: "arTrangThaiMoTa",
        minWidth: "150px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: this.handleCheckStatus
      },
      {
        title: t("maintainRequest.columns.codeAsset"),
        field: "tsMa",
        minWidth: "140px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("maintainRequest.columns.managementCode"),
        field: "tsMaquanly",
        minWidth: "140px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("maintainRequest.columns.nameAsset"),
        field: "tsTen",
        minWidth: "240px",
        align: "left",
      },
      {
        title: t("maintainRequest.columns.model"),
        field: "tsModel",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("maintainRequest.columns.seriNo"),
        field: "tsSerialNo",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("maintainRequest.columns.useDepartment"),
        field: "arPhongBanText",
        minWidth: "200px",
        align: "left",
      },
      {
        title: t("maintainRequest.columns.IncidentContent"),
        field: "arNoiDung",
        minWidth: "200px",
        align: "left",
        render: (rowData) => <div style={{
          display: '-webkit-box',
          WebkitBoxOrient: 'vertical',
          WebkitLineClamp: 2,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        }}>{rowData?.arNoiDung}</div>
      },
      {
        title: t("maintainRequest.columns.receiverDepartment"),
        field: "arTnPhongBanText",
        minWidth: "200px",
        align: "left",
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "asset/maintain_request",
              },
              { name: TitlePage },
            ]}
          />
        </div>
        {(isRoleAssetManager || isRoleOrgAdmin) && (
          <AppBar position="static" color="default">
            <GeneralTab tabs={tabs} value={tabValue} onChange={this.handleChangeTabValue} />
          </AppBar>
        )}

        <Grid container spacing={2} justifyContent="space-between" className="mt-12">
          <div className="flex pt-10">
            {(isRoleAssetUser) && (
              <Grid item>
                <Button
                  className="ml-10 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleOpenReportIncidentDialog();
                    this.setState({ itemEdit: {}, isAdd: true })
                  }}
                >
                  {t("general.ReportIncident")}
                </Button>
              </Grid>
            )}
            {(isRoleAssetManager || isRoleOrgAdmin || isRoleAccountant) && (
              <Grid item>
                <Button
                  className="ml-10 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleOpenDeviceTroubleshootingDialog({});
                    this.setState({ isAdd: true })
                  }}
                >
                  {t("general.ReportIncident")}
                </Button>
              </Grid>
            )}
            {(isShowBtnRecall) && (
              <Grid item>
                <Button
                  className="ml-10 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleOpenEditorReCallAsset();
                  }}
                  disabled={Boolean(!selectedList.length)}
                >
                  {t("ReportIncident.btnRecall")}
                </Button>
              </Grid>
            )}
            {(isShowBtnreceiptForm) && (
              <Grid item>
                <Button
                  className="ml-10 align-bottom"
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleOpenEditorReceiptForm();
                  }}
                  disabled={Boolean(!selectedList.length)}
                >
                  {t("Asset.receipt_form")}
                </Button>
              </Grid>
            )}
            <Grid item>
              <Button
                className="align-bottom ml-16"
                variant="contained"
                color="primary"
                onClick={() => this.setState({ shouldOpenConfirmationExcel: true })}
              >
                {t("general.exportToExcel")}
              </Button>
            </Grid>
            <Grid item>
              <Button
                className="mb-16 ml-16 align-bottom"
                variant="contained"
                color="primary"
                onClick={this.handleOpenAdvanceSearch}
              >
                {t("general.advancedSearch")}
                <ArrowDropDownIcon />
              </Button>
            </Grid>

          </div>

          <div className="w-35 ml-8 mt-16">
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("maintainRequest.search")}
                id="search_box"
                endAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      className="searchTable"
                      onClick={() => this.search(keyword)}
                    />
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>

          {/* Bộ lọc tìm kiếm nâng cao */}
          <Grid item xs={12} className="mb-20">
            <Collapse in={openAdvanceSearch}>
              <ValidatorForm onSubmit={() => { }}>
                <Card elevation={2}>
                  <CardContent>
                    <Grid container spacing={2}>
                      {/* Từ ngày */}
                      <Grid item xs={12} sm={12} md={3}>
                        <ValidatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          clearable
                          label={t("maintainRequest.reportDateFrom")}
                          format="dd/MM/yyyy"
                          value={reportDateFrom ?? null}
                          onChange={this.handleSetData}
                          name="reportDateFrom"
                          maxDate={reportDateTo}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={reportDateTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          invalidDateMessage={t("general.invalidDateFormat")}
                        />
                      </Grid>
                      {/* Đến ngày */}
                      <Grid item xs={12} sm={12} md={3}>
                        <ValidatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          clearable
                          id="date-picker-dialog"
                          label={t("maintainRequest.reportDateTo")}
                          format="dd/MM/yyyy"
                          value={reportDateTo ?? null}
                          onChange={this.handleSetData}
                          name="reportDateTo"
                          minDate={reportDateFrom}
                          minDateMessage={reportDateFrom ? t("general.minDateToDate") : t("general.minYearDefault")}
                          maxDateMessage={t("general.maxDateMessage")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          invalidDateMessage={t("general.invalidDateFormat")}
                        />
                      </Grid>
                      {/* Hoàn thành nghiệm thu từ ngày */}
                      <Grid item xs={12} sm={12} md={3}>
                        <ValidatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          clearable
                          label={t("maintainRequest.arNtThoiGianFrom")}
                          format="dd/MM/yyyy"
                          value={arNtThoiGianFrom ?? null}
                          onChange={this.handleSetData}
                          name="arNtThoiGianFrom"
                          maxDate={arNtThoiGianTo || undefined}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={arNtThoiGianTo ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          invalidDateMessage={t("general.invalidDateFormat")}
                        />
                      </Grid>
                      {/* Đến ngày */}
                      <Grid item xs={12} sm={12} md={3}>
                        <ValidatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          clearable
                          id="date-picker-dialog"
                          label={t("maintainRequest.arNtThoiGianTo")}
                          format="dd/MM/yyyy"
                          value={arNtThoiGianTo ?? null}
                          onChange={this.handleSetData}
                          name="arNtThoiGianTo"
                          minDate={arNtThoiGianFrom || undefined}
                          minDateMessage={arNtThoiGianFrom ? t("general.minDateToDate") : t("general.minYearDefault")}
                          maxDateMessage={t("general.maxDateMessage")}
                          KeyboardButtonProps={{ "aria-label": "change date", }}
                          invalidDateMessage={t("general.invalidDateFormat")}
                        />
                      </Grid>
                      {/* Trạng thái */}
                      <Grid item xs={12} sm={12} md={3}>
                        <Autocomplete
                          fullWidth
                          options={appConst.listStatusReportIncidentForm}
                          defaultValue={reportStatus ? reportStatus : null}
                          value={reportStatus ? reportStatus : null}
                          onChange={(e, value) => this.handleSetData(value, "reportStatus")}
                          filterOptions={(options, params) => {
                            params.inputValue = params.inputValue.trim()
                            return filterOptions(options, params)
                          }}
                          getOptionLabel={option => option.name}
                          disabled={!isTabAll}
                          noOptionsText={t("general.noOption")}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              value={reportStatus?.name || ""}
                              label={t("maintainRequest.status")}
                            />
                          )}
                        />
                      </Grid>
                      {/* Phòng ban báo sự cố */}
                      <Grid item xs={12} sm={12} md={3} className="mt-3">
                        <Autocomplete
                          id="combo-box"
                          fullWidth
                          size="small"
                          name='room'
                          options={listDepartment}
                          onChange={(event, value) => this.handleChangeSelect("departmentFilter", value)}
                          getOptionLabel={(option) => option.name}
                          value={departmentFilter || null}
                          filterOptions={filterOptions}
                          renderInput={(params) => (
                            <TextValidator
                              {...params}
                              label={t("maintainRequest.departmentFilter")} variant="standard"
                              value={departmentFilter ? departmentFilter?.name : null}
                              inputProps={{
                                ...params.inputProps,
                              }}
                              placeholder="Chọn phòng ban"
                              onChange={this.handleSearchDepartment}
                            />
                          )}
                          disabled={isRoleAssetUser}
                          noOptionsText={t("general.noOption")}
                        />
                      </Grid>
                      {/* Hình thức sửa chữa */}
                      <Grid item xs={12} sm={12} md={3} className="mt-3">
                        <Autocomplete
                          id="combo-box"
                          fullWidth
                          size="small"
                          name='room'
                          options={appConst.LIST_HINH_THUC_SUA_CHUA}
                          onChange={(event, value) => this.handleChangeSelect("arHinhThuc", value)}
                          getOptionLabel={(option) => option.name}
                          value={arHinhThuc || null}
                          filterOptions={filterOptions}
                          renderInput={(params) => (
                            <TextValidator
                              {...params}
                              label={t("ReportIncident.RateIncidentForm.repairForm")} variant="standard"
                              value={arHinhThuc ? arHinhThuc?.name : null}
                              inputProps={{
                                ...params.inputProps,
                              }}
                            />
                          )}
                          noOptionsText={t("general.noOption")}
                        />
                      </Grid>
                      {/* Kết quả nghiệm thu */}
                      <Grid item xs={12} sm={12} md={3} className="mt-3">
                        <Autocomplete
                          id="combo-box"
                          fullWidth
                          size="small"
                          name='room'
                          options={appConst.LIST_STATUS_NGHIEM_THU}
                          onChange={(event, value) => this.handleChangeSelect("arNghiemThuStatus", value)}
                          getOptionLabel={(option) => option.name}
                          value={arNghiemThuStatus || null}
                          filterOptions={filterOptions}
                          renderInput={(params) => (
                            <TextValidator
                              {...params}
                              label={t("ReportIncident.RateIncidentForm.resultAcceptance")} variant="standard"
                              value={arNghiemThuStatus ? arNghiemThuStatus?.name : null}
                              inputProps={{
                                ...params.inputProps,
                              }}
                            />
                          )}
                          noOptionsText={t("general.noOption")}
                        />
                      </Grid>
                      {/* TS quá có VT chưa sửa chữa */}
                      {isTabDangXuLy && (
                        <Grid item xs={12} sm={12} md={3}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                                checkedIcon={<CheckBoxIcon fontSize="small" />}
                                size="small"
                                name={variable.listInputName.isExpiredAssetHasVTNotRepaired}
                                checked={isExpiredAssetHasVTNotRepaired}
                                onChange={this.handleCheck}
                              />
                            }
                            label={
                              <LightTooltip title="TS quá hạn có VT chưa sửa chữa" placement="right-end" enterDelay={300} leaveDelay={200}>
                                <div className="text-ellipsis">
                                  TS quá hạn có VT chưa sửa chữa
                                </div>
                              </LightTooltip>
                            }
                          />
                        </Grid>
                      )}
                      {/* TS hỏng đã hết khấu hao */}
                      {(isTabChoXuLy || isTabDangXuLy) && (
                        <Grid item xs={12} sm={12} md={3}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                                checkedIcon={<CheckBoxIcon fontSize="small" />}
                                size="small"
                                name={variable.listInputName.isFullyDepreciatedAsset}
                                checked={isFullyDepreciatedAsset}
                                onChange={this.handleCheck}
                              />
                            }
                            label={
                              <LightTooltip title="TS hỏng đã hết khấu hao" placement="right-end" enterDelay={300} leaveDelay={200}>
                                <div className="text-ellipsis">
                                  TS hỏng đã hết khấu hao
                                </div>
                              </LightTooltip>
                            }
                          />
                        </Grid>
                      )}
                    </Grid>
                  </CardContent>
                </Card>
              </ValidatorForm>
            </Collapse>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <div>
              {shouldOpenReportIncidentDialog && (
                <ReportIncidentDialog //PHiếu báo sự cố
                  t={t}
                  i18n={i18n}
                  open={shouldOpenReportIncidentDialog}
                  handleClose={this.handleReportIncidentDialogClose}
                  isRoleAdmin={isRoleOrgAdmin}
                  itemEdit={itemEdit}
                  isRoleAssetManager={isRoleAssetManager}
                  isAdd={isAdd}
                />
              )}
              {shouldOpenDeviceTroubleshootingDialog && (
                <DeviceTroubleshootingDialog //Xử lý phiếu báo cáo sự cố sửa chữa thay thế
                  t={t}
                  i18n={i18n}
                  open={shouldOpenDeviceTroubleshootingDialog}
                  handleClose={() => this.handleDeviceTroubleshootingDialogClose(tabValue)}
                  itemData={null}
                  itemEdit={itemEdit}
                  setItemEdit={(data) => this.setState({
                    itemEdit: data
                  })}
                  handleChangeItemEdit={this.handleChangeItemEdit}
                  isRoleAssetManager={isRoleAssetManager}
                  isRoleAssetUser={isRoleAssetUser}
                  isRoleAdmin={isRoleOrgAdmin}
                  isAdd={isAdd}
                  isView={isView}
                />
              )}
              {/* Đễ xuất thu hồi */}
              {shouldOpenEditorReCallAsset && (
                <AssetExpectedRevocationDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorReCallAsset}
                  handleOKEditClose={this.handleOKEditClose}
                  item={itemEdit}
                  isFormMaintainRqquest={true}
                  handleQrCode={() => { }}
                />
              )}
              {/* Phiếu thu hồi */}
              {shouldOpenEditorReceiptForm && (
                <AssetRevocationVoucherDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorReceiptForm}
                  handleOKEditClose={this.handleOKEditClose}
                  item={itemEdit}
                  isFormMaintainQuest={true}
                  handleQrCode={() => { }}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
              {shouldOpenConfirmationExcel && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationExcel}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.exportToExcel}
                  text={t("general.exportToExcelConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}

              {isPrint &&
                <PrintMultipleFormDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={isPrint}
                  item={dataView}
                  title={t("MaintenanceProposal.title")}
                  urls={[
                    {
                      // tabLabel: "Mẫu BB BGNT BV Hòe Nhai",
                      url: '/assets/form-print/acceptanceMaintainaceTemplate_hoeNhai.txt', //hoenhai on;y
                      config: {
                        layout: "portrait",
                        margin: "horizontal",
                      },
                    },
                  ]}
                />}
            </div>

            <MaterialTable
              title={t("general.list")}
              data={itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                maxBodyHeight: "450px",
                minBodyHeight: "450px",
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  borderRight: '1px solid #000 !important'
                },
                padding: "dense",
                toolbar: false,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              component="div"
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}
MaintainRequestTable.contextType = AppContext;
export default withRouter(MaintainRequestTable);
