import {
  Backdrop,
  Box,
  Button,
  Grid,
  Icon,
  IconButton,
  InputLabel,
  makeStyles,
  MenuItem,
  TextField
} from "@material-ui/core";
import ConstantList from "app/appConfig";
import { appConst, formatDate, LIST_IMAGE_TYPE, MEGABYTE } from "app/appConst";
import {
  LightTooltip,
  filterOptions,
  getTheHighestRole,
  handleThrowResponseMessage,
  getUserInformation
} from "app/appFunction";
import NotificationPopup from "app/views/Component/NotificationPopup/NotificationPopup";
import moment from "moment";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { TextValidator } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import SelectAssetDialog from "../Dialog/SelectAssetDialog";
import {
  checkIsValidTimes,
  deleteIncidentImage,
  getAllDepartmentByPage,
  getListManagementDepartment,
  uploadIncidentImages
} from "../MaintainRequestService";
import LoadingIndicator from "app/EgretLayout/SharedCompoents/LoadingIndicator";
import { useRef } from "react";
import { FadeLayout } from "app/views/Component/Utilities";
import {AssetOrToolSelectionPopup, ActionsComponent} from "../../Component/Repair";
import { searchByPageDepartmentNew } from "app/views/Department/DepartmentService";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const useStyles = makeStyles((theme) => ({

  backdrop: {
    zIndex: () => {
      return theme.zIndex.drawer + 1
    },
    '& img': {
      height: "80%"
    }
  },
  box_icon: {
    display: "flex",
    justifyContent: "center",
    gap: "10px",
    width: "100%",
  }
}));

function ReportIncidentForm(props) {
  const { i18n } = useTranslation();
  const { currentUser, organization } = getUserInformation();
  const { departmentUser, isRoleOrgAdmin, isRoleAssetUser, } = getTheHighestRole();
  let {
    t,
    itemEdit = {},
    setState = () => { },
    state,
    disable = false,
    isAdd = false
  } = props;

  const classes = useStyles();
  const [item, setItem] = useState(state)
  const [openSelectAssetDialog, setOpenSelectAssetDialog] = useState(false);
  const [listDepartment, setListDepartment] = useState([])
  const [listReceptionDepartment, setReceptionListDepartment] = useState([])
  const [department, setDepartment] = useState({ ...departmentUser })
  const [receptionDepartment, setReceptionDepartment] = useState({})
  const [images, setImages] = useState([]);
  const [shouldOpenConfirmDialog, setShouldOpenConfirmDialog] = useState(false);
  const [loading, setLoading] = useState(false);
  const [confirmText, setConfirmText] = useState();
  const [deleteImageId, setDeleteImageId] = useState();
  const filesInput = useRef();
  const [shouldOpenViewImageDialog, setShouldOpenViewImageDialog] = useState(false);
  const [urlImgView, setUrlImgView] = useState("");
  const [shouldShowImgLayout, setShouldShowImgLayout] = useState(false);
  const [currentViewImage, setCurrentViewImage] = useState(null);
  const [query, setQuery] = useState({
    keyword: '',
    pageIndex: 0,
    pageSize: 999
  })
  const [objectWarning, setObjectWarning] = useState({
    isQua3Lan1Nam: false,
    tuNgay: null,
  })
  let departmentSearchObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: organization?.org?.id,
  };
  useEffect(() => {
    if (!state.images) return;
    setImages(state.images)
  }, [state.images])

  const searchObjectDepartment = {
    pageIndex: 1,
    pageSize: 10000,
  }

  useEffect(() => {
    if (state?.id) {
      setItem({ ...state });
      setDepartment({ id: state?.arPhongBanId, name: state?.arPhongBanText });
      setReceptionDepartment({ id: state?.arTnPhongBanId, name: state?.arTnPhongBanText });
    }
  }, [state?.id]);

  const handleOpenDialog = () => {
    setOpenSelectAssetDialog(true)
  }

  const handleAddAsset = (value) => {
    handleCheckWarningAsset(value?.id);
    convertData(value)
    setOpenSelectAssetDialog(false)
  }

  const convertData = (data) => {
    const valueConvert = {
      ...item,
      tsId: data?.id,
      tsMa: data?.code,
      tsMaquanly: data?.managementCode,
      tsTen: data?.name,
      tsModel: data?.model,
      tsSerialNo: data?.serialNumber,
      tsPhongBanText: data?.managementDepartmentName,
      arNoiDung: data?.arNoiDung || item?.arNoiDung,
      arTgBaoCao: data?.arTgBaoCao || item?.arTgBaoCao || moment().format(formatDate),
      arTrangThai: data?.arTrangThai || item?.arTrangThai,
      arPhongBanId: department?.id,
      arPhongBanText: department?.name,
      arTnPhongBan: receptionDepartment,
      arTnPhongBanId: receptionDepartment?.id,
      arTnPhongBanText: receptionDepartment?.name,
    }
    setState(valueConvert)
    setItem(valueConvert)
  }

  const convertDataSelect = (data) => {
    return {
      id: data?.tsId,
      code: data?.tsMa,
      managementCode: data?.tsMaquanly,
      name: data?.tsTen,
      model: data?.tsModel,
      serialNumber: data?.tsSerialNo,
      managementDepartmentName: data?.tsPhongBanText,
      arNoiDung: data?.arNoiDung || item?.arNoiDung,
      arTgBaoCao: data?.arTgBaoCao || item?.arTgBaoCao || moment().format(formatDate),
      arTrangThai: data?.arTrangThai || item?.arTrangThai,
      arPhongBanId: department?.id,
      arPhongBanText: department?.name,
      arTnPhongBan: receptionDepartment,
      arTnPhongBanId: receptionDepartment?.id,
      arTnPhongBanText: receptionDepartment?.name,
    }
  }

  const handleChange = (event, source) => {
    const { value } = event.target;
    event.persist();
    setItem({
      ...item,
      arNoiDung: value,
    });
    setState({ ...state, arNoiDung: value })
  }

  const handleChangeSelect = (value) => {
    setItem({
      ...item,
      tsId: null,
      tsMa: null,
      tsMaquanly: null,
      tsTen: null,
      tsModel: null,
      tsSerialNo: null,
      tsPhongBanText: null
    })
    setDepartment(value)
    setState({
      ...state, arPhongBanId: value?.id,
      arPhongBanText: value?.name,
    })
  }

  const handleChangeSelectReceptionDepartment = (value) => {
    setReceptionDepartment(value);
    setState({
      ...state,
      arTnPhongBan: value,
      arTnPhongBanId: value?.id,
      arTnPhongBanText: value?.name
    })
  }

  const handleCheckWarningAsset = async (id) => {
    let searchObject = {
      tsId: id,
      toDate: moment(item?.arTgBaoCao).format(formatDate),
    };
    //Tính 1 năm đổ về trước từ ngày bc bao gồm cả năm thường lẫn năm nhuận
    const arWarningLeapYearHandled = moment(item?.arTgBaoCao)
      .subtract(1, 'years')
      .subtract(
        moment(item?.arTgBaoCao).isLeapYear() ? 1 : 0,
        'days'
      )
      .format("DD/MM/YYYY");
    try {
      let res = await checkIsValidTimes(searchObject);
      if (res?.data) {
        setObjectWarning({
          ...objectWarning,
          isQua3Lan1Nam: !res?.data?.data,
          tuNgay: arWarningLeapYearHandled,
        })
      }
    }
    catch (e) {

    }
  }

  const handleChangeDate = (date, name) => {
    setItem({
      ...item,
      arTgBaoCao: moment(date).format(formatDate),
    });
    setState({ ...state, arTgBaoCao: moment(date).format(formatDate) });
  };

  const handleFilesSelect = async (event) => {
    let fileList = event.target.files;
    let arrayFromFiles = Array.from(fileList);

    if (handleCheckFilesSize(arrayFromFiles)) {
      return filesInput.current.value = null;
    }

    let formData = new FormData();
    arrayFromFiles?.forEach(file => formData.append("files", file))

    try {
      setLoading(true);
      const result = await uploadIncidentImages(formData)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        let imgResponse = result?.data?.data || [];
        let newImages = [...images, ...imgResponse];
        let resImgIds = imgResponse?.map(item => item?.id);
        let imageIds = (state?.imageIds || []).concat(resImgIds);

        setImages([...newImages]);
        setState({ ...state, imageIds })
      }
      handleThrowResponseMessage(result);
    } catch (error) {
      toast.error(t("MaintainResquest.noti.updateImageError"));
    } finally {
      setLoading(false);
      filesInput.current.value = null;
    }
  }

  const handleConfirmSubmit = async () => {
    try {
      setLoading(true);
      const result = await deleteIncidentImage(deleteImageId)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        let _images = images.filter(image => image?.id !== deleteImageId);
        setImages(_images);
        let imageIds = state.imageIds.filter(id => id !== deleteImageId);
        setState({ ...state, imageIds, images: _images });
        toast.success(t("MaintainResquest.noti.deleteImageSuccess"));
      } else {
        toast.error(result?.data?.message);
      }
    } catch (error) {
      toast.error(t("MaintainResquest.noti.deleteImageError"));
    } finally {
      setShouldOpenConfirmDialog(false);
      setLoading(false);
    }
  }

  const handleConfirmDialogClose = () => {
    setShouldOpenConfirmDialog(false);
  }

  const handleDeleteImageClick = (id) => {
    setDeleteImageId(id);
    setShouldOpenConfirmDialog(true);
    setConfirmText("Bạn có muốn xóa ảnh này?")
  }

  const handleCheckFilesSize = (files = []) => {

    if (!files.length) {
      return true;
    }

    let hasOverSizeFile = files.some(file => file.size > 10 * MEGABYTE);
    let hasNotMatchTypeFile = files.some(file => !LIST_IMAGE_TYPE.includes(file.type));
    let hasOverTotalSizeFile = files.reduce((total, file) => total + file.size, 0) > 10 * MEGABYTE;

    if (hasOverSizeFile) {
      toast.warning("Kích thước ảnh không được quá 10MB");
      return true;
    }

    if (hasNotMatchTypeFile) {
      toast.warning("Định dạng file không hợp lệ");
      return true;
    }

    if (hasOverTotalSizeFile) {
      toast.warning("Tổng kích thước ảnh không được quá 10MB");
      return true;
    }
    return false;
  }

  const handleOpenViewImageDialog = image => {
    setShouldOpenViewImageDialog(true);
  }
  const handleShowImgLayout = image => {
    setUrlImgView(`${ConstantList.API_ENPOINT_ASSET_MAINTANE}/${image?.file?.filePath || image?.filePath}`)
    setCurrentViewImage(image)
    setShouldShowImgLayout(true);
  }

  const handleHiddenImgLayout = e => {
    setCurrentViewImage(null)
    setShouldShowImgLayout(false);
  }

  return (
    <Grid container spacing={2} justifyContent="space-between">
      <LoadingIndicator show={loading} />
      <Grid item container spacing={1}>
        <Grid item xs={12} sm={6} md={3}>
          <ValidatedDatePicker
            className="w-100"
            margin="none"
            label={
              <span>
                <span className="colorRed">* </span>
                {t("ReportIncident.dateReport")}
              </span>
            }
            autoOk
            format="dd/MM/yyyy"
            onChange={(value) => handleChangeDate(value)}
            value={item?.arTgBaoCao || state?.arTgBaoCao}
            name="arTgBaoCao"
            maxDate={new Date()}
            maxDateMessage={t("ReportIncident.maxDateFinishError")}
            disabled={
              !isAdd || currentUser.username !== item?.createdBy || disable
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} className="mt-3 px-12">
          <TextField
            fullWidth
            select
            variant="standard"
            size="small"
            label={t("ReportIncident.status")}
            value={
              itemEdit?.arTrangThai ||
              appConst.listStatusReportIncidentForm[0]?.id
            } //trạng thái mặc định: chờ xử lý
            name="arTrangThai"
            onChange={handleChange}
            disabled
          >
            {appConst.listStatusReportIncidentForm.map((item) => {
              return (
                <MenuItem key={item.id} value={item.id}>
                  {item.name}
                </MenuItem>
              );
            })}
          </TextField>
        </Grid>
        <Grid item xs={12} sm={6} md={3} className="">
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span className="colorRed">* </span>
                {t("ReportIncident.room")}
              </span>
            }
            displayLable="name"
            searchFunction={getAllDepartmentByPage}
            searchObject={searchObjectDepartment}
            onSelect={handleChangeSelect}
            listData={listDepartment}
            setListData={setListDepartment}
            value={department || null}
            isNoRenderChildren
            isNoRenderParent
            filterOptions={filterOptions}
            validators={["required"]}
            errorMessages={[t("general.required")]}
            noOptionsText={t("general.noOption")}
            disabled={
              isRoleAssetUser ||
              (currentUser.username !== item?.createdBy && !isAdd) ||
              disable
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} className="">
          <AsynchronousAutocompleteSub
            label={
              <span>
                <span className="colorRed">* </span>
                {t("MaintenanceProposal.receptionRoom")}
              </span>
            }
            displayLable="name"
            searchFunction={searchByPageDepartmentNew}
            searchObject={departmentSearchObject}
            onSelect={handleChangeSelectReceptionDepartment}
            listData={listReceptionDepartment}
            setListData={setReceptionListDepartment}
            typeReturnFunction="category"
            value={receptionDepartment || null}
            isNoRenderChildren
            isNoRenderParent
            filterOptions={filterOptions}
            validators={["required"]}
            errorMessages={[t("general.required")]}
            noOptionsText={t("general.noOption")}
            disabled={
              (
                (currentUser.username !== item?.createdBy || departmentUser?.id !== state.arTnPhongBanId)
                && !isRoleOrgAdmin
                && !isAdd
                && state?.arTrangThai !== appConst.statusReportIncidentForm.CHO_XU_LY.code
              )
              || disable
            }
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12} className="px-12 mt-12">
          <TextValidator
            id="standard-multiline-static"
            className="w-100"
            label={<span>{t("ReportIncident.note")}</span>}
            multiline
            minRows={4}
            value={item?.arNoiDung || ""}
            variant="outlined"
            name="arNoiDung"
            onChange={(event) => handleChange(event)}
            disabled={
              isAdd
                ? false
                : currentUser.username !== item?.createdBy || disable
            }
          />
        </Grid>
      </Grid>

      <p className="mt-20">
        &nbsp;
        {objectWarning?.isQua3Lan1Nam && (
          <span className="text-italic colorRed">
            Tài sản sửa chữa quá 3 lần trong 1 năm! kể từ&nbsp;{" "}
            {objectWarning?.tuNgay}
            &nbsp;đến&nbsp;{moment(item?.arTgBaoCao).format("DD/MM/YYYY")}
          </span>
        )}
      </p>

      <Grid
        container
        spacing={1}
        justifyContent="space-between"
        className="pb-40"
      >
        <Grid item xs={12} sm={12} md={12}>
          <div className="flex flex-space-between flex-middle">
            <div>
              <InputLabel htmlFor="isManageAccountant">
                <span className="font-weight-600 font-size-16 text-black">
                  {t("Asset.infoAsset")}:
                </span>
              </InputLabel>
            </div>
            <div>
              {(isAdd ||
                (currentUser.username === item?.createdBy && !disable)) && (
                  <Button
                    // className="mb-16 mr-16 align-bottom"
                    variant="contained"
                    color="primary"
                    required
                    disabled={!department?.id}
                    onClick={() => {
                      handleOpenDialog();
                    }}
                  >
                    <>
                      <span>{t("general.select_asset")}</span>
                    </>
                  </Button>
                )}
            </div>
          </div>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.code")}</span>}
            name="code"
            value={item?.tsMa || ""}
            disabled
            validators={["required"]}
            errorMessages={[t("Asset.please_select_asset")]}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.managementCode")}</span>}
            name="managementCode"
            value={item?.tsMaquanly || ""}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.name")}</span>}
            name="name"
            value={item?.tsTen || ""}
            disabled
            validators={["required"]}
            errorMessages={[t("Asset.please_select_asset")]}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.model")}</span>}
            name="model"
            value={item?.tsModel || ""}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.serialNumber")}</span>}
            name="serialNumber"
            value={item?.tsSerialNo || ""}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TextValidator
            className="w-100"
            label={<span>{t("Asset.managementDepartment")}</span>}
            name="managementDepartment"
            value={item?.tsPhongBanText || ""}
            disabled
            validators={["required"]}
            errorMessages={[t("Asset.please_select_asset")]}
          />
        </Grid>
      </Grid>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={12} md={12}>
          <InputLabel htmlFor="isManageAccountant">
            <span className="font-weight-600 font-size-16 text-black">
              {t("MaintainResquest.ReportIncident.images")}:
            </span>
          </InputLabel>
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <div className="flex flex-wrap mb-20">
            <label htmlFor="upload-images">
              <Button
                size="small"
                className="capitalize"
                component="span"
                variant="contained"
                color="primary"
              >
                <div className="flex flex-middle">
                  <Icon className="pr-8">cloud_upload</Icon>
                  <span>{t("general.select_file")}</span>
                </div>
              </Button>
            </label>
            <input
              className="display-none"
              ref={filesInput}
              onChange={handleFilesSelect}
              multiple
              id="upload-images"
              type="file"
              accept="image/*"
            />
          </div>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Backdrop className={classes.backdrop} open={shouldOpenViewImageDialog} onClick={() => setShouldOpenViewImageDialog(false)}>
          <img
            src={urlImgView}
            alt="Image"
          />
        </Backdrop>
        {images?.map((image, index) => (
          <Grid item xs={4} key={index}>
            <div className="square"
              onMouseEnter={() => handleShowImgLayout(image)}
              onMouseLeave={handleHiddenImgLayout}>
              {currentViewImage?.fileId === image?.fileId
                &&
                <FadeLayout
                  show={shouldShowImgLayout}
                  blur
                  fullSize
                  justifyContent="space-evenly"
                  alignItems="center"
                  backgroundColor="fade"
                >
                  <div className={classes.box_icon}>
                    <LightTooltip title={t('general.viewDocument')} >
                      <IconButton onClick={() => handleOpenViewImageDialog(image)}>
                        <Icon fontSize="small" color="primary">
                          visibility
                        </Icon>
                      </IconButton>
                    </LightTooltip>
                    {!disable && (
                      <LightTooltip title={t('general.removeDocument')}>
                        <IconButton onClick={() => handleDeleteImageClick(image?.id)}>
                          <Icon fontSize="small" color="error">
                            delete
                          </Icon>
                        </IconButton>
                      </LightTooltip>
                    )}
                  </div>
                </FadeLayout>}
              <img
                src={`${ConstantList.API_ENPOINT_ASSET_MAINTANE}/${image?.file?.filePath || image?.filePath}`}
                alt="Incident image"
              />
            </div>
          </Grid>
        ))}
      </Grid>
      {openSelectAssetDialog && (
        <AssetOrToolSelectionPopup
          t={t}
          i18n={i18n}
          assetClass={appConst.assetClass.TSCD}
          open={openSelectAssetDialog}
          handleClose={() => {
            setOpenSelectAssetDialog(false);
          }}
          item={convertDataSelect(item)}
          handleSelect={handleAddAsset}
          useDepartmentId={department?.id}
        />
      )}
      {shouldOpenConfirmDialog && (
        <NotificationPopup
          title={t("general.noti")}
          open={shouldOpenConfirmDialog}
          onYesClick={handleConfirmSubmit}
          onNoClick={handleConfirmDialogClose}
          text={confirmText}
          agree={t("general.agree")}
          cancel={t("general.cancel")}
        />
      )}
    </Grid>
  );
}

export default ReportIncidentForm;
