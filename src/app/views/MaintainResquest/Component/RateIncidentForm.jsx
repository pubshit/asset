import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Icon,
  IconButton,
  InputLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { BO_PHAN_CAN_SUA, appConst, formatDate, variable } from "app/appConst";
import {
  convertNumberPrice,
  filterOptions,
  isSuccessfulResponse,
  getOptionSelected,
  isValidDate,
  handleKeyDownIntegerOnly
} from "app/appFunction";
import ProductDialog from "app/views/Product/ProductDialog";
import { getNewCode } from "app/views/Product/ProductService";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import SelectUserTodoDialog from "../Dialog/SelectUserToDoDialog";
import { checkTotalCost, deleteSupplies, getPhuTungByAsset, searchByPageProductOrgNew } from "../MaintainRequestService";

import Autocomplete from "@material-ui/lab/Autocomplete";
import { debounce } from "lodash";
import SelectSupplyPopup from '../../Component/SupplyUnit/SelectSupplyPopup'
import AccessaryPopup from "../../Component/Accessary/AccessaryPopup";
import AsynchronousAutoCompleteSub from "app/views/utilities/AsynchronousAutocompleteSub";
import { LightTooltip } from "../../Component/Utilities/LightToolTip";
import ValidatedDatePicker from "../../Component/ValidatePicker/ValidatePicker";
import NumberFormatCustom from "../../Component/Utilities/NumberFormatCustom";
import SelectProductsPopup from "../../Component/Product/SelectProductsPopup";
import { formatDateDto, formatTimestampToDate } from "../../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function RateIncidentForm(props) {

  let { t, i18n,
    setItemTiepNhan = () => { },
    itemTiepNhan,
    disable,
    arLinhKienVatTus,
    permission,
  } = props;

  const [valueTable, setValueTable] = useState([])
  const [itemProduct, setItemProduct] = useState({})
  const [openSelectUserDialog, setOpenSelectUserDialog] = useState(false)
  const [openSelectSupply, setOpenSelectSupply] = useState(false)
  const [keywordSearch, setKeyword] = useState({
    index: null,
    keywordSearch: ""
  })
  const [indexLinhKien, setIndexLinhKien] = useState(null);
  const [shouldOpenDialogProduct, setShouldOpenDialogProduct] = useState(false)
  const [listProductOrg, setListProductOrg] = useState([])
  const [dataLinhKienVatTu, setDataLinhKienVatTu] = useState([...(itemTiepNhan?.arLinhKienVatTus || [])])
  const [isSelectAccessary, setIsSelectAccessary] = useState(false)
  const [openAccessaryPopup, setopenAccessaryPopup] = useState(false)
  const [openInventoryDeliveryPopup, setopenInventoryDeliveryPopup] = useState(false)
  const [listAccessary, setListAccessary] = useState([])
  const [isExceed, setIsExceed] = useState(false);
  const [phuTungSearchObject, setPhuTungSearchObject] = useState({
    pageIndex: 0,
    pageSize: 1000,
    keyword: '',
    assetId: itemTiepNhan?.tsId,
  })

  useEffect(() => {
    if (itemTiepNhan?.tsId && itemTiepNhan.chiPhiThucHien) {
      isCheckTotalCost();
    }
  }, [itemTiepNhan.chiPhiThucHien]);

  useEffect(() => {
    setDataLinhKienVatTu(arLinhKienVatTus?.map(i => convertDataVatTu(i)))
  }, [arLinhKienVatTus]);

  useEffect(() => {
    getDanhSachVatTu()
  }, [listProductOrg.length])

  useEffect(() => {
    if (!shouldOpenDialogProduct) {
      if (props?.setIsAllowedSubmit) {
        props.setIsAllowedSubmit(true);
      }
      if (props?.setIsAllowedSubmit && shouldOpenDialogProduct === false) {
        getDanhSachVatTu();
      }
    }
  }, [shouldOpenDialogProduct])

  useEffect(() => {
    if (dataLinhKienVatTu) {
      let updateNameLinhKien = dataLinhKienVatTu.map(item => {
        let nameLinhKien = listProductOrg.find(itemProduct => itemProduct.id === item.productId)?.name
        if (item.name) {
          return item
        }
        return {
          ...item,
          name: nameLinhKien
        }
      })
      setDataLinhKienVatTu(updateNameLinhKien)
    }
  }, [listProductOrg.length])

  useEffect(() => {
    let newArPhuTungs = itemTiepNhan?.arPhuTungs ? itemTiepNhan?.arPhuTungs : []
    const isSelectAccessary = itemTiepNhan?.arType === BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code
      || itemTiepNhan?.arType === BO_PHAN_CAN_SUA.SUA_CA_HAI.code
    const newItemTiepNhan = {
      ...itemTiepNhan,
      arTgXacNhan: itemTiepNhan?.arTgXacNhan || moment().format(formatDate),
      arHanSuaChuaXong: itemTiepNhan?.arHanSuaChuaXong || moment().format(formatDate),
      arTgCanCoVtTo: itemTiepNhan?.arTgCanCoVtTo || moment().format(formatDate),
      arHinhThuc: itemTiepNhan?.arHinhThuc,
      arUsers: itemTiepNhan?.arUsers || [],
      arLinhKienVatTus: dataLinhKienVatTu,
      arPhuTungs: newArPhuTungs?.map(phuTung => {
        if (phuTung.tsPhutungName) {
          phuTung.tsPhutung = {
            productName: phuTung.tsPhutungName
          }
        }
        return phuTung
      }),
      unit: itemTiepNhan?.unit?.id
        ? itemTiepNhan?.unit
        : {
          id: itemTiepNhan?.donViThucHienId,
          name: itemTiepNhan?.donViThucHienText,
          code: itemTiepNhan?.donViThucHienCode,
        },
    };

    setIsSelectAccessary(isSelectAccessary)
    setItemTiepNhan(newItemTiepNhan);
  }, [
    itemTiepNhan?.arMoTa,
    itemTiepNhan?.arPhanTichSoBo,
    itemTiepNhan?.arDeXuatBienPhap,
    itemTiepNhan?.arTgXacNhan,
    itemTiepNhan?.arTgCanCoVtTo,
    itemTiepNhan?.arHanSuaChuaXong,
    itemTiepNhan?.arUsers,
    dataLinhKienVatTu,
  ]);

  useEffect(() => {
    let newArLinhKienVatTus = itemTiepNhan?.arLinhKienVatTus ? itemTiepNhan?.arLinhKienVatTus : []
    const newItemTiepNhan = {
      ...itemTiepNhan,
      unit: {
        id: itemTiepNhan?.donViThucHienId,
        name: itemTiepNhan?.donViThucHienText,
        code: itemTiepNhan?.donViThucHienCode,
      }
    };
    let query = {
      pageIndex: 0,
      pageSize: 10000,
      keyword: '',
      productTypeCode: appConst.productTypeCode.VTHH
    }

    newArLinhKienVatTus?.map(linhKien => {
      if (!linhKien.product?.id) {
        linhKien.product = {
          productName: linhKien?.name || linhKien.productName,
          id: linhKien.productId,
          code: linhKien?.code || linhKien?.productCode || linhKien?.product?.code
        }
      }
      return linhKien;
    })

    setDataLinhKienVatTu([...newArLinhKienVatTus]);
    handleGetArPhuTungsUsingQuantity().then(() => {
      setItemTiepNhan(newItemTiepNhan);
      getProductOrg(query);
    });
  }, []);

  useEffect(() => {
    getDanhSachVatTu();
  }, [itemTiepNhan?.arLinhKienVatTus]);

  const convertDataVatTu = (data) => {
    return {
      id: data?.id,
      expiryDate: data?.expiryDate || data?.warrantyExpiryDate,
      lotNumber: data?.lotNumber,
      exportedDate: formatDateDto(data?.exportedDate || new Date()),
      product: {
        code: data?.code || data?.productCode || data?.product?.code,
        id: data?.productId,
        productName: data?.name || data?.productName
      },
      productId: data?.productId,
      productName: data?.name || data?.productName,
      skuName: data?.defaultSku?.sku?.name || data?.unitName || data?.skuName,
      skuId: data?.defaultSku?.sku?.id || data?.unitId || data?.skuId,
      productCost: data?.price || data?.unitPrice || data?.productCost,
      productQty: data?.productQty || data?.remainQuantity || 1,
      arScId: itemTiepNhan?.id,
      inputDate: data?.inputDate,
      receiptDate: data?.receiptDate || data?.inputDate,
      storeId: data?.storeId,
      remainQuantity: data?.remainQuantity,
      voucherId: data?.voucherId,
      status: data?.status,
    }
  };

  const selectProduct = (productSelected, index) => {
    if (productSelected?.code === variable.listInputName.New) {
      getNewCode()
        .then((result) => {
          if (result != null && result?.data && result?.data?.code) {
            let item = result.data;
            setItemProduct((pre) => ({ ...pre, ...item }))
          }
        })
        .catch(() => {
        });
      if (props?.setIsAllowedSubmit) {
        props.setIsAllowedSubmit(false);
      }
      setIndexLinhKien(index)
      setShouldOpenDialogProduct(true);
    }
    else {
      const checkExistProduct = dataLinhKienVatTu?.some(x => (x?.product?.id === productSelected?.productId) && !x?.storeId);
      
      if (checkExistProduct) {
        toast.warning("Vật tư đã có trong danh sách")
      } else {
        let dataEquiptCompUpdate = dataLinhKienVatTu?.map((item, indexData) => {
          if (indexData === index) {
            item = { ...convertDataVatTu({ ...productSelected, productId: (productSelected?.id || productSelected?.productId), id: null }) }
          }
          return item;
        });
        setDataLinhKienVatTu(dataEquiptCompUpdate)
      }
    }
  }

  const getProductOrg = async (query) => {
    try {
      let res = await searchByPageProductOrgNew(query)

      if (res?.data?.data?.content && res?.data?.code === appConst.CODE.SUCCESS) {
        setListProductOrg(res?.data?.data?.content)
      }
    } catch (error) {
      toast.error("Xảy ra lỗi")
    }
  }

  const handleChangeDataEdit = (e) => {
    if (e.target?.name === variable.listInputName.chiPhiThucHien) {
      setItemTiepNhan({
        ...itemTiepNhan,
        [e.target.name]: Number(e.target.value)
      })
      return;
    }
    setItemTiepNhan({
      ...itemTiepNhan,
      [e.target.name]: e.target.value
    })
  }

  const handleDateChange = (date, name) => {
    setItemTiepNhan({
      ...itemTiepNhan,
      [name]: isValidDate(date) ? moment(date).format(formatDate) : date
    })
  }

  const handleDialogClose = () => {
    setShouldOpenDialogProduct(false)
    if (!itemProduct?.id) {
      setKeyword({
        index: null,
        keywordSearch: ""
      });
      getDanhSachVatTu()
    }
    if (props?.setIsAllowedSubmit) {
      props.setIsAllowedSubmit(true);
    }
  }

  const handleOKEditClose = () => {
    setShouldOpenDialogProduct(false)
  };

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      hidden: disable,
      minWidth: 100,
      maxWidth: 100,
      cellStyle: {
        textAlign: 'center'
      },
      render: (rowData) => {
        return (
          <>
            {itemTiepNhan?.hasExpectedSupplies === false && <LightTooltip
              title={t("general.delete")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
              PopperProps={{
                popperOptions: {
                  modifiers: {
                    offset: { enabled: true, offset: "5px, 20px" },
                  },
                },
              }}
            >
              <IconButton
                style={{ textAlign: "center" }}
                size="small"
                onClick={() => handleRowDataDeleteInList(rowData, variable.listInputName.viewIndex)}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>}
          </>
        )
      },
    },
    {
      title: t("Asset.stt"),
      field: "stt",
      minWidth: 60,
      maxWidth: 60,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.product"),
      field: "productId",
      minWidth: 400,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Product.batchCode"),
      field: "lotNumber",
      minWidth: 200,
      align: "left",
    },
    {
      title: t("Product.expiryDate"),
      field: "expiryDate",
      align: "center",
      minWidth: 200,
    },
    {
      title: t("Asset.stockKeepingUnitTable"),
      field: "skuName",
      minWidth: 200,
      align: "center",
    },
    {
      title: t("Product.approvedQuanlity"),
      field: "productQty",
      minWidth: 200,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Product.status"),
      field: "status",
      minWidth: 200,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Product.price"),
      field: "unitPrice",
      minWidth: 200,
      align: "left",
      cellStyle: {
        textAlign: "right",
      },
    },
  ];

  const handleDeleteRowDataUser = (rowData) => {
    const newData = itemTiepNhan?.arUsers.filter((item) => item.userId !== rowData.userId)
    setItemTiepNhan({
      ...itemTiepNhan,
      arUsers: newData
    })
  }

  let columnsUser = [
    ...(!disable ? [{
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 60,
      maxWidth: 60,
      cellStyle: {
        textAlign: 'center'
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: {
                offset: { enabled: true, offset: "10px, 0px" },
              },
            },
          }}
        >
          <IconButton
            style={{ textAlign: "center" }}
            size="small"
            onClick={() => handleDeleteRowDataUser(rowData)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    }] : []),
    {
      title: t("Asset.stt"),
      field: "stt",
      minWidth: 40,
      maxWidth: 40,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1
    },
    {
      title: t("RateIncidentForm.hoTen"),
      field: "userName",
      minWidth: "150px",
      align: "left",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: t("RateIncidentForm.phongBan"),
      field: "userPhongBanText",
      minWidth: 200,
      align: "left",
      cellStyle: {
      },
    },
    {
      title: t("RateIncidentForm.chucVu"),
      field: "productQty",
      maxWidth: 80,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
  ];

  const handleRowDataDeleteInList = (rowData, source) => {
    if (source) {
      if (itemTiepNhan?.arPhuTungs && itemTiepNhan?.arPhuTungs?.length > 0 && source === variable.listInputName.arPhuTungs) {
        // let item = itemTiepNhan?.arPhuTungs[rowData?.tableData?.id]
        //eslint-disable-next-line
        itemTiepNhan?.arPhuTungs?.splice(rowData?.tableData?.id, 1)
        setItemTiepNhan({
          ...itemTiepNhan,
          arPhuTungs: itemTiepNhan?.arPhuTungs,
        })
      } else {
        let listAfterDelete = dataLinhKienVatTu?.filter((i, x) => x !== rowData?.tableData?.id);
        setDataLinhKienVatTu(listAfterDelete)
      }
    }
    else {
      let listAfterDelete = dataLinhKienVatTu?.filter(item => item.index !== rowData.id)
      setDataLinhKienVatTu(listAfterDelete)
    }
  };

  const onChange = (event, source) => {
    const { name, value } = event.target;
    event.persist();
    if (variable.listInputName.arHinhThuc === source) {
      setItemTiepNhan({
        ...itemTiepNhan,
        arUsers: [],
        unit: null,
        donViThucHienText: "",
        [name]: +value,
      })
    }
    if (variable.listInputName.arType === source) {
      if (value) {
        setIsSelectAccessary(
          +value === BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code
          || +value === BO_PHAN_CAN_SUA.SUA_CA_HAI.code
        )
      }
      setItemTiepNhan({
        ...itemTiepNhan,
        arPhuTungs: [],
        [name]: +value,
      })
    }
    if (variable.listInputName.arScope === source) {
      setItemTiepNhan({
        ...itemTiepNhan,
        [name]: +value,
      })
    }
  };

  const handleAddRow = () => {
    setDataLinhKienVatTu([...dataLinhKienVatTu, {
      productId: "",
      productName: "",
      productQty: "",
      index: dataLinhKienVatTu.length,
      name: "",
    }])
  }

  const debouncedSetSearchTerm = debounce((newTerm, index) => {
    setKeyword({
      index: index,
      keywordSearch: newTerm
    });
  }, 1000);

  const handleSearchListDvbh = (e, index) => {
    debouncedSetSearchTerm(e.target.value, index)
  }

  const handleCheckStatus = (status) => {
    switch (status) {
      case appConst.EXPORT_SUPPLY_STATUS.DU_KIEN_XUAT_KHO.indexOrder:
        return (
          <div className="align-center">
            <span className="status status-success">
              {appConst.EXPORT_SUPPLY_STATUS.DU_KIEN_XUAT_KHO.name}
            </span>
          </div>
        );
      case appConst.EXPORT_SUPPLY_STATUS.DA_XUAT_KHO.indexOrder:
        return (
          <div className="align-center">
            <span className="status status-warning">
              {appConst.EXPORT_SUPPLY_STATUS.DA_XUAT_KHO.name}
            </span>
          </div>
        );
      case appConst.EXPORT_SUPPLY_STATUS.MUA_NGOAI.indexOrder:
        return (
          <div className="align-center">
            <span className="status status-info">
              {appConst.EXPORT_SUPPLY_STATUS.MUA_NGOAI.name}
            </span>
          </div>
        );
      case appConst.EXPORT_SUPPLY_STATUS.DA_XOA.indexOrder:
        return (
          <div className="align-center">
            <span className="status status-error">
              {appConst.EXPORT_SUPPLY_STATUS.DA_XOA.name}
            </span>
          </div>
        );

      default:
        break;
    }
    return;
  };
  const getDanhSachVatTu = () => {
    let dataEquiptComp = [];
    dataEquiptComp = dataLinhKienVatTu?.map(
      (item, index) => {
        let isSetDisable = item?.id && item?.storeId;
        return {
          id: item?.id,
          productId:
            <div>
              <Autocomplete
                id="combo-box"
                fullWidth
                size="small"
                name='productOrg'
                options={listProductOrg}
                value={item?.product || null}
                onChange={(e, value) => selectProduct(value, index)}
                getOptionLabel={(option) => option.productName}
                getOptionSelected={getOptionSelected}
                filterOptions={(options, params) => filterOptions(
                  options,
                  params,
                  true,
                  "productName"
                )}
                noOptionsText={t("general.noOption")}
                renderInput={(params) => (
                  <TextValidator
                    {...params}
                    variant="standard"
                    value={item?.productName || item?.product?.name || keywordSearch.keywordSearch}
                    onChange={(event) => setItemProduct((pre) => ({ ...pre, name: event.target.value }))}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />
                )}
                disabled={disable || item?.storeId}
              />
            </div>
          ,
          productName: <TextValidator
            className="w-100"
            variant="standard"
            type="text"
            name="productName"
            value={item?.productName || ""}
            size="small"
            placeholder=""
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(e) => handleChangeEquiptComp(e, index)}
            validators={["required"]}
            errorMessages={[t("general.required")]}
            disabled={disable || isSetDisable}
          />,
          skuName: item?.skuName,
          productCost: convertNumberPrice(item?.productCost) || 0,
          productQty: <TextValidator
            className="w-100"
            variant="standard"
            type="number"
            name="productQty"
            value={
              typeof item?.productQty === "undefined"
                ? ""
                : item?.productQty
            }
            size="small"
            placeholder=""
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              style: { textAlign: 'center' },
              // readOnly: true
            }}
            onChange={(e) => handleChangeEquiptComp(e, index)}
            validators={[
              "required",
              "minFloat:0.00000001",
              `minFloat:${item?.exportedQty || 0}`,
              `maxFloat:${item?.remainQuantity ? item?.remainQuantity : 99999999}`,
            ]}
            errorMessages={[
              t("general.required"),
              t("general.minNumberError"),
              t("maintainRequest.minNumberProductQty"),
              item?.remainQuantity ? t("Không được lớn hơn số lượng tồn") : t("general.maxNumberError"),
            ]}
            disabled={disable || isSetDisable}

          />,
          exportedQty: <TextValidator
            className="w-100"
            variant="standard"
            type="number"
            name="exportedQty"
            value={
              typeof item?.exportedQty === "undefined"
                ? ""
                : item?.exportedQty
            }
            size="small"
            placeholder=""
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              style: { textAlign: 'center' },
            }}
            // onChange={(e) => handleChangeEquiptComp(e, index)}
            // validators={[
            //   "required",
            //   "minNumber:1",
            //   "maxNumber:999999999999999",
            // ]}
            // errorMessages={[
            //   t("general.required"),
            //   t("general.minNumberError"),
            //   t("general.maxNumberError"),
            // ]}
            disabled

          />,
          status: handleCheckStatus(item?.status),
          unitPrice: convertNumberPrice(item?.productCost) || 0,
          lotNumber: item?.lotNumber,
          expiryDate: formatTimestampToDate(item?.expiryDate),
          storeId: item?.storeId,
        }
      }
    )
    setValueTable(dataEquiptComp);
  };

  const handleChangeEquiptComp = (e, index) => {
    let listEquiptCompUpdate =
      itemTiepNhan?.arLinhKienVatTus?.map((item, indexItem) => {
        if (indexItem === index) {
          if (["productQty", "exportedQty"].includes(e.target.name) && e.target.value.trim() !== "") {
            item[e.target.name] = +e.target.value;
          }
          else {
            item[e.target.name] = e.target.value;
          }
        }
        return item;
      });
    setDataLinhKienVatTu(listEquiptCompUpdate)
  }

  const handleAddUser = (value = []) => {
    setItemTiepNhan({
      ...itemTiepNhan,
      arUsers: value.map((item) => ({
        userName: item?.displayName || item?.userName,
        userId: item?.id || item?.userId,
        userChucVuText: t("Dashboard.materialManagement"),
        userPhongBanText: item?.departmentName || item?.userPhongBanText,
        userPhongBanId: item?.departmentId || item?.userPhongBanId,
      }))
    })
    setOpenSelectUserDialog(false)
  }

  const handleSelectSupply = (value) => {
    try {
      setItemTiepNhan({
        ...itemTiepNhan,
        unit: value,
      })
    }
    catch { }
    finally {
      handleClose();
    }

  }

  const handleSelectOpenDialog = (value) => {
    setOpenSelectUserDialog(+appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.code === value)
    setOpenSelectSupply(+appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code === value)
  }

  const handleChangeSelect = (value, source) => {
    setIsSelectAccessary(false)
    // if (value === BO_PHAN_CAN_SUA[0].code || value === BO_PHAN_CAN_SUA[2].code) {
    //   setIsSelectAccessary(true)
    // }
    if (
      value === BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code
      || value === BO_PHAN_CAN_SUA.SUA_CA_HAI.code
    ) {
      setIsSelectAccessary(true)
    }
  }

  const handleDeleteSupplies = async () => {
    try {
      const data = await deleteSupplies(itemTiepNhan?.id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        let updateDataResponse = data?.data?.data?.map(x => ({ ...convertDataVatTu(x) }));

        setItemTiepNhan({ ...itemTiepNhan, hasExpectedSupplies: !itemTiepNhan?.hasExpectedSupplies });
        setDataLinhKienVatTu(updateDataResponse);
      }

    } catch (error) {

    }
  }

  let columnsAccessary = [
    ...(!disable ? [{
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      minWidth: 80,
      maxWidth: 80,
      cellStyle: {
        textAlign: 'center'
      },
      render: (rowData) => (
        <LightTooltip
          title={t("general.delete")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: {
                offset: { enabled: true, offset: "10px, 0px" },
              },
            },
          }}
        >
          <IconButton
            style={{ textAlign: "center" }}
            size="small"
            onClick={() => handleRowDataDeleteInList(
              rowData,
              variable.listInputName.arPhuTungs
            )}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      ),
    }] : []),
    {
      title: t("Asset.stt"),
      field: "stt",
      minWidth: 60,
      maxWidth: 60,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (rowData.tableData.id + 1),
    },
    {
      title: <span>
        {props?.item?.assetAccessories?.length > 0 && (
          <span className="colorRed">*</span>
        )}
        {t("SparePart.sparePart")}
      </span>,
      field: "product",
      align: "left",
      minWidth: "200px",
      render: (rowData) => (
        <AsynchronousAutoCompleteSub
          size="small"
          listData={listAccessary}
          setListData={setListAccessary}
          searchFunction={getPhuTungByAsset}
          searchObject={phuTungSearchObject}
          value={rowData?.tsPhutung ? rowData.tsPhutung : null}
          onSelect={(value) => handleSelectLinhKien(
            value,
            rowData?.tableData?.id,
            "tsPhutung"
          )}
          typeReturnFunction="category"
          displayLable='productName'
          noOptionsText={t("general.noOption")}
          disabled={disable}
          validators={["required",]}
          errorMessages={[t("general.required"),]}
        />
      )
    }, {
      title: t("MaintainResquest.RateIncident.quantity"),
      field: "originQuantity",
      align: "left",
      minWidth: 80,
      render: rowData => (
        <TextValidator
          className="w-100"
          variant="standard"
          type="text"
          name="arPhutungSoluong"
          InputProps={{
            inputProps: {
              className: "text-center",
            },
            inputComponent: NumberFormatCustom
          }}
          value={rowData?.arPhutungSoluong || ""}
          onChange={(e) => handleChange(
            e,
            rowData?.tableData?.id
          )}
          onKeyDown={handleKeyDownIntegerOnly}
          validators={["required", `maxNumber:${rowData.usingQuantity || 9999999999}`]}
          errorMessages={[t("general.required"), t("AccessaryPopup.inValidSoLuong"),]}
        />
      ),
    },
    {
      title: t("AccessaryPopup.usingQuantity"),
      field: "usingQuantity",
      minWidth: 280,
      align: "left",
      cellStyle: {
        textAlign: "center",
      }
    },
    {
      title: t("AccessaryPopup.ghiChu"),
      field: "arGhichu",
      minWidth: 280,
      align: "left",
      render: rowData => (
        <TextValidator
          className="w-100"
          variant="standard"
          type="text"
          name="arGhichu"
          InputProps={{
            // readOnly: isView,
          }}
          value={rowData?.arGhichu || ""}
          onChange={(e) => handleChange(
            e,
            rowData?.tableData?.id
          )}
        />
      ),
    },
  ];

  const handleAddAccessary = () => {
    let arPhuTungs = itemTiepNhan?.arPhuTungs
      ? itemTiepNhan.arPhuTungs
      : []
    //eslint-disable-next-line
    arPhuTungs?.push({
      tsPhutung: null,
      quantity: "",
    })
    setItemTiepNhan({
      ...itemTiepNhan,
      arPhuTungs,
    })
  }

  const handleClose = () => {
    setOpenSelectSupply(false);
    setopenAccessaryPopup(false);
    setOpenSelectUserDialog(false);
    setopenInventoryDeliveryPopup(false);
  }

  const handleSelectLinhKien = (value, index, name) => {
    let { arPhuTungs = [], } = itemTiepNhan;
    let item = arPhuTungs ? arPhuTungs[index] : {};
    if (item) {
      item.tsPhutungId = value?.id
      item.usingQuantity = value?.usingQuantity
      item[name] = value;
    }
    setItemTiepNhan({
      ...itemTiepNhan,
      arPhuTungs,
    });
  };

  const handleChange = (e, index) => {
    let { arPhuTungs = [], } = itemTiepNhan;
    let item = arPhuTungs ? arPhuTungs[index] : {};
    if (item) {
      item[e?.target?.name] = e?.target?.value
    }
    setItemTiepNhan({
      ...itemTiepNhan,
      arPhuTungs,
    })
  }

  const isCheckTotalCost = async () => {
    try {
      const result = await checkTotalCost({
        idAsset: itemTiepNhan?.tsId,
        totalCost: itemTiepNhan.chiPhiThucHien,
      });
      if (result?.data?.code === 200) {
        setIsExceed(!result?.data?.data);
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  const handleGetArPhuTungsUsingQuantity = async () => {
    try {
      let res = await getPhuTungByAsset(phuTungSearchObject)
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        let newData = itemTiepNhan?.arPhuTungs?.map(item => {
          let existItem = data?.content?.find(x => x.id === item.tsPhutungId)
          if (existItem) {
            item.usingQuantity = existItem.usingQuantity;
          }
          return item;
        })
        setItemTiepNhan({
          ...newData,
        })
      }
    }
    catch (error) {

    }
    finally {

    }
  }

  const validateExportStore = (data = []) => {
    let check = false;
    if (!data.length) {
      check = true;
      toast.warning("Chưa có vật tư nào có trong danh sách");
      return check;
    }

    let isEqual = data?.every(i => Number(i?.productQty) === Number(i?.exportedQty))
    if (isEqual) {
      check = true;
      toast.warning("Không có vật tư nào cần xuất kho");
      return check;
    }

    for (let i of data) {
      if (Number(i?.productQty) < Number(i?.exportedQty)) {
        check = true;
        toast.warning("Số lượng đề xuất của vật tư phải lớn hơn số lượng đã xuất");
        break;
      }
      if ((i?.productQty === 0) || (!i?.productQty)) {
        check = true;
        toast.warning("Số lượng đề xuất phải lớn hơn không");
        break;
      }
    }

    return check;
  }

  const handleGetDataEquiment = async (data) => {
    try {
      let updateList = data?.map(i => {
        let newData = convertDataVatTu(i);
        return newData;
      });
      setDataLinhKienVatTu(updateList)
    } catch (error) {

    } finally {
      handleClose();
    }
  }


  return (
    <Grid container spacing={2} justifyContent="space-between">
      <Grid container spacing={1} className="m-0">
        <Grid item md={4} sm={12} xs={12}>
          <ValidatedDatePicker
            className="w-100"
            margin="none"
            id="mui-pickers-date"
            label={t("ReportIncident.RateIncidentForm.confirmationDate")}
            autoOk
            format="dd/MM/yyyy"
            name={"arTgXacNhan"}
            value={itemTiepNhan?.arTgXacNhan ? new Date(itemTiepNhan?.arTgXacNhan) : new Date()}
            minDate={itemTiepNhan?.arTgBaoCao}
            maxDate={new Date()}
            minDateMessage={
              itemTiepNhan?.arTgBaoCao
                ? t("ReportIncident.RateIncidentForm.minDateFinishError")
                : t("general.minDateDefault")
            }
            maxDateMessage={t("ReportIncident.RateIncidentForm.maxDateFinishError")}
            onChange={(date) => handleDateChange(date, "arTgXacNhan")}
            invalidDateMessage={t("general.invalidDateFormat")}
            disabled={disable || permission.arTgXacNhan.isDisabled}
            clearable
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <ValidatedDatePicker
            className="w-100"
            margin="none"
            id="mui-pickers-date"
            label={t("ReportIncident.RateIncidentForm.timeRequiredForSupplies")}
            autoOk
            format="dd/MM/yyyy"
            name={"arTgCanCoVt"}
            value={itemTiepNhan?.arTgCanCoVt}
            minDate={itemTiepNhan?.arTgXacNhan}
            maxDate={itemTiepNhan?.arHanSuaChuaXong}
            minDateMessage={
              itemTiepNhan?.arTgXacNhan
                ? `Thời gian cần có vật tư không được bé hơn ngày hơn ngày ${moment(itemTiepNhan?.arTgXacNhan).format("DD/MM/YYYY")}`
                : t("general.minDateDefault")
            }
            maxDateMessage={
              itemTiepNhan?.arHanSuaChuaXong
                ? `Thời gian cần có vật tư không được lớn hơn ngày hơn ngày ${moment(itemTiepNhan?.arHanSuaChuaXong).format("DD/MM/YYYY")}`
                : t("general.maxDateDefault")
            }
            invalidDateMessage={t("general.invalidDateFormat")}
            onChange={(date) => handleDateChange(date, "arTgCanCoVt")}
            disabled={disable || permission.arTgCanCoVt.isDisabled}
            clearable
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <ValidatedDatePicker
            className="w-100"
            margin="none"
            id="mui-pickers-date"
            label={t("ReportIncident.RateIncidentForm.durationOfRepair")}
            autoOk
            format="dd/MM/yyyy"
            name={"arHanSuaChuaXong"}
            value={itemTiepNhan?.arHanSuaChuaXong}
            minDate={itemTiepNhan?.arTgXacNhan}
            minDateMessage={
              itemTiepNhan?.arTgXacNhan
                ? `Thời hạn sửa chữa xong không được nhỏ hơn ngày ${moment(itemTiepNhan?.arTgXacNhan).format("DD/MM/YYYY")}`
                : t("general.minDateDefault")
            }
            onChange={(date) => handleDateChange(date, "arHanSuaChuaXong")}
            disabled={disable || permission.arTgXacNhan.isDisabled}
            clearable
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <TextValidator
            className="mt-12"
            fullWidth
            multiline
            label={t("ReportIncident.RateIncidentForm.describe")}
            InputProps={{
              rows: 2,
            }}
            value={itemTiepNhan?.arMoTa ? itemTiepNhan?.arMoTa : ""}
            onChange={handleChangeDataEdit}
            variant="outlined"
            name="arMoTa"
            disabled={disable || permission.arMoTa.isDisabled}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <TextValidator
            className="mt-12"
            fullWidth
            multiline
            label={t("ReportIncident.RateIncidentForm.analysisOfCauses")}
            InputProps={{
              rows: 2,
            }}
            value={itemTiepNhan?.arPhanTichSoBo || ""}
            onChange={handleChangeDataEdit}
            variant="outlined"
            name="arPhanTichSoBo"
            disabled={disable || permission.arPhanTichSoBo.isDisabled}
          />
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <TextValidator
            className="mt-12"
            fullWidth
            multiline
            label={t(
              "ReportIncident.RateIncidentForm.suggestionOfCountermeasures"
            )}
            InputProps={{
              rows: 2,
            }}
            value={itemTiepNhan?.arDeXuatBienPhap || ""}
            onChange={handleChangeDataEdit}
            variant="outlined"
            name="arDeXuatBienPhap"
            disabled={disable || permission.arDeXuatBienPhap.isDisabled}
          />
        </Grid>

        <Grid item md={4} sm={12} xs={12}>
          <FormControl component="fieldset" className="mt-10">
            <FormLabel id="demo-radio-buttons-group-label">
              <span className="colorRed">*</span>
              {t("MaintainResquest.RateIncident.repairPart")}
            </FormLabel>
            <RadioGroup
              aria-label="repairForm"
              name="arType"
              value={itemTiepNhan?.arType || ""}
              onChange={(event) => onChange(event, "arType")}
              className="mt-10"
            >
              <Grid container>
                <Grid item>
                  <FormControlLabel
                    label={t(BO_PHAN_CAN_SUA.SUA_TAI_SAN.text)}
                    value={BO_PHAN_CAN_SUA.SUA_TAI_SAN.code}
                    control={<Radio />}
                    disabled={disable || permission.arType.isDisabled}
                  />
                </Grid>
                <Grid item>
                  <FormControlLabel
                    label={t(BO_PHAN_CAN_SUA.SUA_PHU_TUNG.text)}
                    value={BO_PHAN_CAN_SUA.SUA_PHU_TUNG.code}
                    control={<Radio />}
                    id="radio-domestic"
                    disabled={disable || permission.arType.isDisabled}
                  />
                </Grid>
                <Grid item>
                  <FormControlLabel
                    label={t(BO_PHAN_CAN_SUA.SUA_CA_HAI.text)}
                    value={BO_PHAN_CAN_SUA.SUA_CA_HAI.code}
                    control={<Radio />}
                    id="radio-domestic"
                    disabled={disable || permission.arType.isDisabled}
                  />
                </Grid>
              </Grid>
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <FormControl component="fieldset" className="mt-10">
            <FormLabel id="demo-radio-buttons-group-label">
              <span className="colorRed">*</span>
              {t("ReportIncident.RateIncidentForm.repairForm")}
            </FormLabel>
            <RadioGroup
              aria-label="repairForm"
              name="arHinhThuc"
              value={itemTiepNhan?.arHinhThuc || ""}
              onChange={(event) => onChange(event, "arHinhThuc")}
              className="mt-10"
              required
            >
              <Grid container>
                <Grid item>
                  <FormControlLabel
                    label={appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.name}
                    value={+appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.code}
                    control={<Radio />}
                    id="radio-domestic"
                    disabled={disable || permission.arHinhThuc.isDisabled}
                  />
                </Grid>
                <Grid item>
                  <FormControlLabel
                    label={appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.name}
                    value={+appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code}
                    control={<Radio />}
                    disabled={disable || permission.arHinhThuc.isDisabled}
                  />
                </Grid>
              </Grid>
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <FormControl component="fieldset" className="mt-10">
            <FormLabel id="demo-radio-buttons-group-label">
              <span className="colorRed">*</span>
              {t("ReportIncident.RateIncidentForm.scaleOfRepair")}
            </FormLabel>
            <RadioGroup
              aria-label="repairForm"
              name="arScope"
              value={itemTiepNhan?.arScope || ""}
              onChange={(event) => onChange(event, "arScope")}
              className="mt-10"
              required
            >
              <Grid container>
                <Grid item>
                  <FormControlLabel
                    label={appConst.QUY_MO_SUA_CHUA.SC_LON.name}
                    value={+appConst.QUY_MO_SUA_CHUA.SC_LON.code}
                    control={<Radio />}
                    id="radio-domestic"
                    disabled={disable || permission.arScope.isDisabled}
                  />
                </Grid>
                <Grid item>
                  <FormControlLabel
                    label={appConst.QUY_MO_SUA_CHUA.SC_NHO_LE.name}
                    value={+appConst.QUY_MO_SUA_CHUA.SC_NHO_LE.code}
                    control={<Radio />}
                    disabled={disable || permission.arScope.isDisabled}
                  />
                </Grid>
              </Grid>
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>

      {itemTiepNhan.arHinhThuc && (
        <Grid
          container
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          className="pt-30"
        >
          <InputLabel htmlFor="isManageAccountant">
            <span className="pl-5 pt-10 font-weight-600 font-size-16 text-black">
              {
                itemTiepNhan.arHinhThuc !== +appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code
                  ? <span>
                    {t("ReportIncident.RateIncidentForm.executor")}
                    <span className="colorRed">*</span>
                  </span>
                  : <span>
                    {t("ReportIncident.RateIncidentForm.unit")}
                  </span>
              }:
            </span>
          </InputLabel>
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            disabled={!itemTiepNhan.arHinhThuc || disable || permission.arHinhThuc.isDisabled}
            onClick={() => handleSelectOpenDialog(itemTiepNhan.arHinhThuc)}
          >
            {t("general.select")}
          </Button>
        </Grid>
      )}

      <Grid container spacing={1} className="m-0">
        {appConst.HINH_THUC_SUA_CHUA.DON_VI_SUA_CHUA.code === itemTiepNhan.arHinhThuc && (
          <Grid item md={12} sm={12} xs={12} className="mt-12">
            <MaterialTable
              data={itemTiepNhan?.arUsers || []}
              columns={columnsUser}
              title={""}
              options={{
                maxBodyHeight: 500,
                toolbar: false,
                search: false,
                paging: false,
                selection: false,
                sorting: false,
                draggable: false,
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                  padding: "6px 24px 6px 16px"
                },
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
            />
          </Grid>
        )}
        {appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code === itemTiepNhan.arHinhThuc &&
          <>
            <Grid item md={3} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span>
                    {/* <span className="colorRed">*</span> */}
                    {t("SelectSupplyPopup.maDonVi")}
                  </span>
                }
                value={itemTiepNhan.unit?.code || ''}
                InputProps={{
                  readOnly: true,
                }}
                disabled={disable || permission.arHinhThuc.isDisabled}
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                label={t("SelectSupplyPopup.tenDonVi")}
                InputProps={{
                  readOnly: true,
                }}
                disabled={disable || permission.arHinhThuc.isDisabled}
                value={itemTiepNhan.unit?.name || ''}
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                label={t("maintainRequest.donViThucHienCost")}
                name="chiPhiThucHien"
                onChange={handleChangeDataEdit}
                InputProps={{
                  inputComponent: NumberFormatCustom,
                  inputProps: {
                    className: "text-align-right",
                  }
                }}
                disabled={disable || permission.arHinhThuc.isDisabled}
                value={
                  +itemTiepNhan.chiPhiThucHien > 0
                    ? convertNumberPrice(itemTiepNhan.chiPhiThucHien)
                    : ''
                }
              />
            </Grid>
            <Grid item md={3} sm={12} xs={12}>
            </Grid>
            <Grid item md={12} sm={12} xs={12}>
              {isExceed && (
                <div className="warning">
                  {t("ReportIncident.AssetAcceptanceForm.confirmTotalCost")}{" "}
                </div>
              )}
            </Grid>
          </>
        }

      </Grid>


      {isSelectAccessary &&
        <>
          <Grid
            container spacing={1}
            justifyContent="space-between"
            alignItems="center"
            className="pb-10 pt-30"
          >
            <InputLabel htmlFor="isManageAccountant">
              <span className="pl-5 pt-10 font-weight-600 font-size-16 text-black">
                {t("MaintainResquest.RateIncident.listAccesory")}:
              </span>
            </InputLabel>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                handleAddAccessary();
              }}
              disabled={disable}
            >
              {t("general.addButton")}
            </Button>
          </Grid>

          <Grid container spacing={2} className="m-0">
            <Grid md={12} sm={12} xs={12}>
              <MaterialTable
                data={
                  itemTiepNhan?.arPhuTungs
                    ? itemTiepNhan?.arPhuTungs
                    : []
                }
                columns={columnsAccessary}
                title={""}
                options={{
                  maxBodyHeight: 500,
                  toolbar: false,
                  search: false,
                  paging: false,
                  selection: false,
                  sorting: false,
                  draggable: false,
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    paddingLeft: 10,
                    paddingRight: 10,
                    textAlign: "center",
                    padding: "6px 24px 6px 16px"
                  },
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                  },
                }}
                components={{
                  Toolbar: (props) => (
                    <div className="w-100">
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
              />
            </Grid>
          </Grid>
        </>
      }

      <Grid
        container spacing={1}
        justifyContent="space-between"
        alignItems="center"
        className="pb-10 pt-30"
      >
        <InputLabel htmlFor="isManageAccountant">
          <span className="pl-10 pt-10 font-weight-600 font-size-16 text-black">
            {t("ReportIncident.RateIncidentForm.listOfSuppliesToBeUsed")}:
          </span>
        </InputLabel>
        <Grid item>
          {!disable && itemTiepNhan?.hasExpectedSupplies && <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={handleDeleteSupplies}
          >
            {t("MaintainResquest.RateIncident.deleteStore")}
          </Button>}
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => {
              setopenInventoryDeliveryPopup(true);
            }}
            disabled={disable || permission.arLinhKienVatTus.isDisabled}
          >
            {t("MaintainResquest.RateIncident.exportStore")}
          </Button>
          <Button
            className="mb-16 mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => {
              handleAddRow();
            }}
            disabled={disable || permission.arLinhKienVatTus.isDisabled}
          >
            {t("general.addButton")}
          </Button>
        </Grid>
      </Grid>

      <Grid container spacing={2} className="m-0">
        <Grid item md={12} sm={12} xs={12}>
          <MaterialTable
            data={valueTable}
            columns={columns}
            title={""}
            options={{
              maxBodyHeight: 500,
              toolbar: false,
              search: false,
              paging: false,
              selection: false,
              sorting: false,
              draggable: false,
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
                padding: "6px 24px 6px 16px"
              },
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            components={{
              Toolbar: (props) => (
                <div className="w-100">
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
          />
        </Grid>
      </Grid>

      {shouldOpenDialogProduct && (
        <ProductDialog
          t={t}
          i18n={props.i18n}
          handleClose={handleDialogClose}
          open={shouldOpenDialogProduct}
          handleOKEditClose={handleOKEditClose}
          item={itemProduct}
          type="addFromRateIncident"
          selectProduct={selectProduct}
          indexLinhKien={indexLinhKien}
          setIsAllowedSubmit={props?.setIsAllowedSubmit}
        />
      )}

      {openSelectUserDialog &&
        <SelectUserTodoDialog
          t={t}
          open={openSelectUserDialog}
          handleClose={handleClose}
          onAddUser={handleAddUser}
          arUsers={itemTiepNhan?.arUsers}
        />
      }

      {openSelectSupply &&
        <SelectSupplyPopup
          t={t}
          i18n={i18n}
          open={openSelectSupply}
          onClose={handleClose}
          handleSelect={handleSelectSupply}
          typeCodes={[appConst.TYPE_CODES.NCC_BDSC]}
          isMaintainRequest={true}
          unit={itemTiepNhan?.unit}
        />}

      {openAccessaryPopup &&
        <AccessaryPopup
          t={t}
          open={openAccessaryPopup}
          onClose={handleClose}
          handleSelect={() => { }}
          unit={""}
        />}
      {openInventoryDeliveryPopup &&
        <SelectProductsPopup
          t={t}
          i18n={i18n}
          handleClose={handleClose}
          open={openInventoryDeliveryPopup}
          handleSelect={(data) => handleGetDataEquiment(data)}
          selectedList={[...dataLinhKienVatTu]}
        />}
    </Grid>
  );
}

export default RateIncidentForm;
