import React, { Component } from "react";
import {
  Dialog,
  Button,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { saveMaintainRequest, updateMaintainRequest, searchByPageRequestComment, deleteRequestComment, deleteCkeckRequestComment, getOneRequestComment } from "./MaintainRequestService";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NumberFormat from 'react-number-format';
import MaintainScrollableTabsButtonForce from './MaintainScrollableTabsButtonForce'
import { getCurrentUser } from '../page-layouts/UserProfileService'
import {
  searchByPageAssetDocument,
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeAssetDocument,
} from '../Asset/AssetService';
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { searchByPage as maintainStatusSearchByPage } from "../MaintainRequestStatus/MaintainRequestStatusService"
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,

          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
}
class MaintainRequestEditorDialog extends Component {
  state = {
    asset: null,
    userRequest: null,
    maintainOwner: null,
    status: null,
    name: "",
    dateRequest: new Date(),
    note: "",
    issueDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenDepartmentOwnerPopup: false,
    shouldOpenAssetPopup: false,
    depRequest: null,
    maintainDepOwner: null,
    isActive: false,
    depId: "",
    maintainDepOwner2: "",
    maintainCost: null,
    managementDepartment: null,
    shouldOpenSelectManagementDepartmentPopup: false,
    shouldOpenReceiverPersonPopup: false,
    receiverPerson: null,
    assetDocumentList: [],
    shouldOpenPopupAssetFile: false,
    page: 0,
    currentUser: null,
    listAssetDocumentId: [],
    listRequestCommentId: [],
    comments: [],
    shouldOpenCommentDialog: false,
    documentType: 5, // hồ sơ bảo dưỡng sửa chữa
    loading: false,
    isNew: true
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  openCircularProgress = () => {
    this.setState({ loading: true });
  };

  handleFormSubmit = async () => {
    await this.openCircularProgress();
    let { t } = this.props;
    let { name, status, depRequest, asset, managementDepartment, receiverPerson } = this.state;
    if (name === null || name === "") {
      toast.warning('Tên phiếu không được để trống(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    if (!status || status?.length === 0) {
      toast.warning('Vui lòng chọn trạng thái sửa chữa(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    if (depRequest === null) {
      toast.warning('Vui lòng chọn phòng yêu cầu(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    if (asset === null) {
      toast.warning('Vui lòng chọn tài sản(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    if (managementDepartment === null) {
      toast.warning('Vui lòng chọn phòng tiếp nhận(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    if (receiverPerson === null) {
      toast.warning('Vui lòng chọn người tiếp nhận(Thông tin phiếu).')
      toast.clearWaitingQueue();
      this.setState({ loading: false });
      return
    }
    let { id } = this.state;
    let { code } = this.state;
    let MaintainRequestObject = {}
    MaintainRequestObject.asset = this.state.asset;
    MaintainRequestObject.dateRequest = this.state.dateRequest;
    MaintainRequestObject.depRequest = this.state.depRequest;
    MaintainRequestObject.maintainDepOwner = this.state.maintainDepOwner;
    MaintainRequestObject.maintainOwner = this.state.maintainOwner;
    MaintainRequestObject.status = this.state.status;
    MaintainRequestObject.userRequest = this.state.userRequest;
    MaintainRequestObject.name = this.state.name;
    MaintainRequestObject.note = this.state.note;
    MaintainRequestObject.managementDepartment = this.state.managementDepartment;
    MaintainRequestObject.maintainCost = this.state.maintainCost;
    MaintainRequestObject.listAssetDocumentId = this.state.listAssetDocumentId;
    MaintainRequestObject.receiverPerson = this.state.receiverPerson;
    MaintainRequestObject.listRequestCommentId = this.state.listRequestCommentId;

    if (id) {
      updateMaintainRequest({
        ...this.state
      }).then(() => {
        toast.info(t('general.updateSuccess'));
        this.props.handleOKEditClose();
      }).catch(err => {
        this.setState({ loading: false });
      });
    } else {
      saveMaintainRequest(MaintainRequestObject).then(() => {
        toast.info(t('general.addSuccess'));
        this.props.handleOKEditClose();
      }).catch(err => {
        this.setState({ loading: false });
      });
    }

  };

  handleSelectMaintainDepOwner = (item) => {

    this.setState({ maintainDepOwner: { id: item.id, name: item.text } }, function () {
      this.handleMaintainDepOwnerPopupClose();
    });
    this.setState({ maintainDepOwner2: { id: item.id, name: item.text } }, function () {
      this.handleMaintainDepOwnerPopupClose();
    });
  }

  handleMaintainDepOwnerPopupClose = () => {
    this.setState({
      shouldOpenDepartmentOwnerPopup: false
    });
  };


  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
      item: {}
    });
  };

  handleSelectDepRequest = (item) => {
    this.setState({ depRequest: { id: item.id, name: item.text } }, function () {
      this.handleDepartmentPopupClose();
    });
    if (Object.keys(item).length === 0) {
      this.setState({ depRequest: null })
    }
  }

  selectProduct = (product) => {
    this.setState({ product: product }, function () {
    });
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  openPopupSelectAsset = () => {
    let { depRequest } = this.state;
    if (depRequest) {
      this.setState({ item: {}, departmentId: depRequest.id }, function () {
        this.setState({
          shouldOpenAssetPopup: true
        });
      });
    }
    else {
      toast.warning("Vui lòng chọn phòng ban yêu cầu.");
      // alert("Vui lòng chọn phòng ban yêu cầu.");
    }
  }

  componentWillMount() {
    let currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    let { item } = this.props;
    this.setState({ ...item, currentUser });
    if (item.id && item.id !== null) {
      this.updatePageAssetDocument();
      this.setState({ isNew: false })
      this.SearchByPageRequestComment();
    } else {
      let searchObject = {}
      searchObject.pageIndex = 1;
      searchObject.pageSize = 999;
      let item;
      maintainStatusSearchByPage(searchObject).then(({ data }) => {
        let itemList = [];
        itemList = [...data.content]
        itemList.forEach(e => {
          if (e.code === "MOI") {
            item = e;
          }
        })
        this.selectMaintainStatus(item)
      });

    }

  }

  handleSelectAsset = (item) => {
    this.setState({
      asset: { id: item.id, name: item.name },
      managementDepartment: item.managementDepartment
    }, function () {
      this.handleAssetPopupClose();
    });
    if (Object.keys(item).length === 0) {
      this.setState({ asset: null, managementDepartment: null })
    }
  }

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false
    });
  };

  selectMaintainStatus = (item) => {
    this.setState({ status: item })
  }

  selectMaintainOwner = (item) => {
    this.setState({ maintainOwner: item }, function () {
    });
  }

  handleChangeMaintainCost = (event) => {

    this.setState({
      maintainCost: event.target.value,
    });
  };

  openSelectManagementDepartmentPopup = () => {
    this.setState({
      shouldOpenSelectManagementDepartmentPopup: true,
    })
  }

  handleSelectManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenSelectManagementDepartmentPopup: false,
    })
  }

  handleSelectManagementDepartment = (item) => {
    let department = { id: item.id, name: item.name, }
    this.setState({ managementDepartment: department }, function () {
      this.handleSelectManagementDepartmentPopupClose()
    })
    if (Object.keys(item).length === 0) {
      this.setState({ managementDepartment: null })
    }
  }

  handleReceiverPersonPopupOpen = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: true
    })
  }

  handleReceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenReceiverPersonPopup: false
    })
  }

  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: { id: item.id, displayName: item.displayName },
    }, function () {
      this.handleReceiverPersonPopupClose();
    })
    if (Object.keys(item).length === 0) {
      this.setState({ receiverPerson: null })
    }
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true
    });
  }

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false
    });
  }

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes != null && attributes.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (attributes[index].attribute && attributes[index].attribute.id == rowData.attribute.id) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () {
    });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      this.setState({
        item: data ? data.data : {},
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true
      },
        () => {
          this.updatePageAssetDocument()
        });
    })
  }

  handleRowDataCellDeleteAssetFile = (rowData) => {
    deleteAssetDocumentById(rowData.id).then(data => {
      this.updatePageAssetDocument()
    })
  }

  handleAddAssetDocumentItem = () => {
    getNewCodeAssetDocument().then((result) => {
      if (result != null && result.data && result.data.code) {
        let item = result.data;
        this.setState({
          item: item,
          shouldOpenPopupAssetFile: true,
        });
      }
    }).catch(() => {
      // alert(this.props.t("general.error_reload_page"));
      toast.warning(this.props.t("general.error_reload_page"));
    });
  }

  updatePageAssetDocument = () => {
    let searchObject = {}
    searchObject.maintainRequestId = this.props.item.id;
    searchObject.keyword = this.state.keyword
    searchObject.pageIndex = this.state.page + 1
    searchObject.documentType = this.state.documentType
    searchObject.pageSize = 100000;
    searchByPageAssetDocument(searchObject).then(({ data }) => {
      this.setState({
        assetDocumentList: [...data.content],
        totalElements: data.totalElements,
      }, () => {
      })
    })
  }

  getAssetDocumentId = (documentId) => {
    let { listAssetDocumentId } = this.state;
    if (!listAssetDocumentId) {
      listAssetDocumentId = [];
    };
    if (documentId) {
      listAssetDocumentId.push(documentId);
    };
    this.setState({
      listAssetDocumentId: listAssetDocumentId
    }, () => {
    })
  }

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;
    if (listAssetDocumentId != null && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach(id => deleteAssetDocumentById(id));
    }
  }

  openRequestCommentDialog = () => {
    this.setState({ shouldOpenCommentDialog: true, comment: null })
  }

  handleRequestCommentClose = () => {
    this.setState({ shouldOpenCommentDialog: false })
  }

  handleSelectRequestComment = () => {
  }

  SearchByPageRequestComment = () => {
    let searchObject = {};
    if (this.props.item.id) {
      searchObject.maintainRequestId = this.props.item.id;
    }
    searchObject.keyword = this.state.keyword
    searchObject.pageIndex = this.state.page + 1
    searchObject.pageSize = 1000000;
    searchByPageRequestComment(searchObject).then(({ data }) => {
      this.setState({
        comments: [...data.content]
      }, () => {
      })
    })
  }

  getRequestCommentId = (commentId) => {
    // debugger
    let { listRequestCommentId } = this.state;
    if (!listRequestCommentId) {
      listRequestCommentId = [];
    };
    if (commentId) {
      listRequestCommentId.push(commentId);
    };
    this.setState({
      listRequestCommentId: listRequestCommentId
    }, () => {
    })
  }

  deleteRequestCommentById = () => {
    let { listRequestCommentId } = this.state;
    if (listRequestCommentId != null && listRequestCommentId.length > 0) {
      listRequestCommentId.forEach(id => deleteRequestComment(id));
    }
  }

  deleteCkeckRequestComment = (id) => {
    deleteCkeckRequestComment(id).then(({ data }) => {
      if (data) {
        toast.info('Xoá thành công.')
        this.SearchByPageRequestComment();
      } else {
        toast.warning('Bạn không thể xoá bình luận này.')
      }
    })
  }

  handleEditRequestComment = (id) => {
    if (id) {
      getOneRequestComment(id).then(({ data }) => {
        if (data != null) {
          this.setState({ comment: data, shouldOpenCommentDialog: true })
        }
      })
    }
  }

  render() {
    let {
      loading,
    } = this.state;
    let searchObject = { pageIndex: 0, pageSize: 1000 };
    let { open, t, i18n } = this.props;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>

        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
            <span className="">{t('maintainRequest.dialog')}</span>
          </DialogTitle>

          <DialogContent style={{ height: '450px' }}>
            <MaintainScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObject={searchObject}
              selectMaintainStatus={this.selectMaintainStatus}
              handleDateChange={this.handleDateChange}
              handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
              handleSelectDepRequest={this.handleSelectDepRequest}
              handleDepartmentPopupClose={this.handleDepartmentPopupClose}
              openPopupSelectAsset={this.openPopupSelectAsset}
              handleSelectAsset={this.handleSelectAsset}
              handleAssetPopupClose={this.handleAssetPopupClose}
              openSelectManagementDepartmentPopup={this.openSelectManagementDepartmentPopup}
              handleSelectManagementDepartment={this.handleSelectManagementDepartment}
              handleSelectManagementDepartmentPopupClose={this.handleSelectManagementDepartmentPopupClose}
              handleReceiverPersonPopupOpen={this.handleReceiverPersonPopupOpen}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleReceiverPersonPopupClose={this.handleReceiverPersonPopupClose}
              handleChangeMaintainCost={this.handleChangeMaintainCost}
              handleChange={this.handleChange}
              isNew={this.state.isNew}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              updatePageAssetDocument={this.updatePageAssetDocument}
              getAssetDocumentId={this.getAssetDocumentId}

              handleRequestCommentClose={this.handleRequestCommentClose}
              openRequestCommentDialog={this.openRequestCommentDialog}
              SearchByPageRequestComment={this.SearchByPageRequestComment}
              getRequestCommentId={this.getRequestCommentId}
              deleteCkeckRequestComment={this.deleteCkeckRequestComment}
              handleEditRequestComment={this.handleEditRequestComment}
            />
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="secondary" className="mr-12" onClick={() => { this.props.handleClose(); this.deleteAssetDocumentById(); this.deleteRequestCommentById() }}>{t('general.cancel')}</Button>
              <Button variant="contained" color="primary" type="submit">
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default MaintainRequestEditorDialog;
