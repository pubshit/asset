import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';

const MaintainRequestTable = EgretLoadable({
  loader: () => import("./MaintainRequestTable")
});
const ViewComponent = withTranslation()(MaintainRequestTable);

const MaintainRequestRoutes = [
  {
    path: ConstantList.ROOT_PATH + "asset/maintain_request",
    exact: true,
    component: ViewComponent
  }
];

export default MaintainRequestRoutes;