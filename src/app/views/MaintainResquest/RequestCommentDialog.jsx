import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  Paper,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  TextField,
  FormControlLabel,
  DialogContent
} from "@material-ui/core";
// import Paper from '@material-ui/core/Paper'
import {updateMaintainRequestComment, createMaintainRequestComment} from './MaintainRequestService';
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from 'react-draggable';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});

function PaperComponent(props) {
    return (
      <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
      </Draggable>
    );
  }

class RequestCommentDialog extends Component {
  state = {
    id:"",
    name: "",
    code: "",
    shouldOpenNotificationPopup:false,
    Notification:"",
    comment: ""
  };

  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let maintainRequest = this.props.item;   
    let editComment = this.state.editComment;
    let id;
    if(editComment != null) {
      id = editComment.id;
    }    
    let { code, comment } = this.state;
    if (id) {      
      updateMaintainRequestComment({comment, "request": maintainRequest}, id).then((data) => {
        this.props.SearchByPageRequestComment();
        this.props.handleCloseComment();
      });
    } else {
      createMaintainRequestComment({comment , "request": maintainRequest}).then((data) => {
        
        this.props.getRequestCommentId(data.data.id)
        this.props.SearchByPageRequestComment();
        this.props.handleCloseComment();
      });
    }
  };

  componentWillMount() {
    let { open, handleClose,editComment } = this.props;
    // console.log(this.props.editComment)
    if(editComment != null) {
      this.setState({editComment, comment:editComment.comment }, ()=> console.log(this.state.editComment));
    }
  }

  render() {
    let {
      id,
      name,
      code,
      comment,
      shouldOpenNotificationPopup
      
    } = this.state;
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    console.log(this.props)
    return (
      <Dialog   open={open}  PaperComponent={PaperComponent} maxWidth="sm" fullWidth>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <h4 className="">{t("Thêm mới/ cập nhật ghi chú")}</h4>
        </DialogTitle>
          
        <ValidatorForm onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={2}>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  className="w-100 "
                  label={<span><span className="colorRed">*</span>{t('Ghi chú')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="comment"
                  value={comment}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>              
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleCloseComment()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                // type="submit"
                onClick={() => this.handleFormSubmit()}
              >
                {t('general.save')}
              </Button>              
            </div>
        </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default RequestCommentDialog;
