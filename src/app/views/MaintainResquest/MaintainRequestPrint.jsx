import React, { Component, useRef } from "react";
import {
  Dialog,
  Button,
  Grid, Checkbox,
  IconButton,
  Icon,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';


function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}



function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;
  return <div>
    <IconButton onClick={() => props.onSelect(item, 1)}>
      <Icon color="error">delete</Icon>
    </IconButton>
  </div>;
}
export default class MaintainRequestPrint extends Component {
  state = {
    type: 2,
    rowsPerPage: 1,
    page: 0,
    totalElements: 0,
    departmentId: '',
    asset: {},
    isView: false,
    managementDepartmentCode: 'A.1.9.VPB8'
  };

  handleFormSubmit = () => {

    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    let { open, handleClose, item } = this.props;

    this.setState({
      ...this.props.item
    }, function () {
    }
    );
  }
  componentDidMount() {
  }

  render() {
    let { open, handleClose, handleOKEditClose, t, i18n, item } = this.props;
    let now = new Date();
    let {
      rowsPerPage,
      page,
      assetVouchers,
    } = this.state;

    return (


      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <span className="mb-20">{t('Phiếu bảo dưỡng')}</span>
          {/* <IconButton onClick={handleClose} style={{ position: "absolute", top: 0, right:0 }}>
            <Icon className="text-black">clear</Icon>
          </IconButton> */}
        </DialogTitle>
        <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute', print: { size: 'auto', margin: '0mm' } }}></iframe>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} class="validator-form-scroll-dialog">
          <DialogContent id='divcontents'>
            <Grid>
              <div style={{ textAlign: 'center' }}>

                <div style={{ display: 'flex' }}>
                  <div style={{ flexGrow: 1 }}>
                    <p>BYT.{now.getDate()}.{now.getMonth() + 1}</p>
                    <p>Số:   /</p>
                  </div>
                  <div style={{ flexGrow: 3 }}>
                    <p>Phiếu sửa chữa, bảo dưỡng cơ sở vật chất, máy móc, thiết bị</p>
                    <p>Kính gửi: {item?.managementDepartment?.name || ''}</p>
                  </div>
                </div>

                <div style={{ textAlign: 'left' }}>
                  <p>I. THÔNG BÁO HỎNG:</p>
                  <div style={{ display: 'flex' }}>
                    <p style={{ width: '47%', marginLeft: '10px' }} >Tên người sử dụng: </p>
                    <p>Phòng ban: </p>
                  </div>
                  <div style={{ display: 'flex' }}>
                    <p style={{ width: '47%', marginLeft: '10px' }} >Điện thoại: </p>
                    <p>Mã quản lý thiết bị: </p>
                  </div>

                  <p style={{ marginLeft: '10px' }} >Đề nghị sửa chữa: </p>
                  <div style={{ width: '100%' }}>
                    {item?.managementDepartment?.code !== this.state.managementDepartmentCode ?
                      <div style={{ display: 'flex', marginLeft: '10%' }}>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Điện</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Nước</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Khác:............</span>
                      </div>
                      :
                      <div style={{ display: 'flex', marginLeft: '10%' }}>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Máy tính</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Máy in</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Điện Thoại</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Máy fax</span>
                        <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0' }}></div>
                        <span style={{ marginRight: '7%' }}> Khác:............</span>
                      </div>
                    }
                  </div>

                  <p style={{ marginLeft: '10px' }} >Tình trạng: </p>

                  <div style={{ display: 'flex', margin: '0 7%', textAlign: 'center', marginBottom: '20%' }}>
                    <div style={{ flexGrow: 1 }}>
                      <p style={{ width: '100%', display: 'inline-block' }}>     </p>
                      <p>Người sử dụng</p>
                    </div>
                    <div style={{ flexGrow: 1 }}>
                      <p>Ngày....tháng....năm</p>
                      <p>Lãnh đạo đơn vị</p>
                    </div>
                  </div>
                </div>

                <div style={{ textAlign: 'left' }}>
                  <p>II. TÌNH TRẠNG - XỬ LÍ (Phần dành cho Văn Phòng Bộ)</p>
                  <p style={{ marginLeft: '60%' }}>Ngày....tháng....năm {now.getFullYear()}</p>
                  <p>Cán bộ kĩ thuật phòng {item?.managementDepartment?.code !== this.state.managementDepartmentCode ? 'CNTT' : 'Điện nước'} giải quyết:</p>

                  <table style={{ width: '100%', border: '1px solid', borderCollapse: 'collapse' }} className="table-report">
                    <tr>
                      <th style={{ border: '1px solid', textAlign: 'center', width: '12%' }} rowSpan="2">Tài sản</th>
                      <th style={{ border: '1px solid', width: '25%', textAlign: 'center' }} rowSpan="2">Nội dung</th>
                      <th style={{ border: '1px solid', width: '8%' }} rowSpan="2">Tình trạng</th>
                      <th style={{ border: '1px solid', width: '8%', textAlign: 'center' }} colSpan="6">Đề xuất</th>
                      <th style={{ border: '1px solid', width: '8%', textAlign: 'center' }} rowSpan="2">Giá tiền (VNĐ)</th>
                      <th style={{ border: '1px solid', width: '12%', textAlign: 'center' }} rowSpan="2">Ghi chú</th>
                    </tr>
                    <tr>
                      <th style={{ border: '1px solid', textAlign: 'center' }}>Khắc phục</th>
                      <th style={{ border: '1px solid', textAlign: 'center' }}>Bảo hành</th>
                      <th style={{ border: '1px solid', textAlign: 'center' }}>Sửa chữa</th>
                      <th style={{ border: '1px solid', textAlign: 'center' }}>Thay thế</th>
                      <th style={{ border: '1px solid', textAlign: 'center' }}>Nâng cấp</th>
                      <th style={{ border: '1px solid', width: '8%', textAlign: 'center' }}>Tạm thời cho mượn</th>
                    </tr>
                    <tbody>
                      <tr>
                        <td style={{ border: '1px solid' }}>{item?.asset !== null ? item?.asset?.name : ''}</td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                        <td style={{ border: '1px solid', textAlign: 'left' }}></td>
                        <td style={{ border: '1px solid' }}></td>
                      </tr>
                    </tbody>
                  </table>

                  <div style={{ display: 'flex', justifyContent: 'space-between', margin: '0 7%', marginBottom: '20%' }}>
                    <div><p>Cán bộ kĩ thuật</p></div>
                    <div><p>Lãnh đạo văn phòng</p></div>
                    <div><p>Lãnh đạo văn phòng duyệt</p></div>
                  </div>

                  <div>
                    <p>III. KẾT QUẢ XỬ LÝ</p>
                    <div style={{ display: 'flex', marginLeft: '10%' }}>
                      <span style={{ marginRight: '3px' }}> Thiết bị hoạt động tốt</span>
                      <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0', marginRight: '15%' }}></div>
                      <span style={{ marginRight: '3px' }}> Đề nghị thu hồi</span>
                      <div style={{ width: '15px', height: '15px', border: '1px solid', margin: '2px 2px 0 0', marginRight: '7%' }}></div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between', margin: '0 7%', marginBottom: '20%' }}>
                      <div>
                        <p style={{ width: '100%', display: 'inline-block' }}> </p>
                        <p>NGƯỜI BÀN GIAO</p>
                      </div>
                      <div style={{ textAlign: 'center' }}>
                        <p>Ngày....tháng....năm {now.getFullYear()}</p>
                        <p>XÁC NHẬN CỦA NGƯỜI SỬ DỤNG</p>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>

        </ValidatorForm>

      </Dialog>
    );
  }
}
