import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid } from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchReceiverDepartment } from "./InstrumentsToolsAllocationService";
import SelectAssetAllPopup from "./SelectAssetAllPopup";
import MaterialTable, { MTableToolbar } from "material-table";
import VoucherFilePopup from "../Component/AssetFile/VoucherFilePopup";
import { searchByPage as searchByPageStatus } from "../AllocationStatus/AllocationStatusService";
import {
  LightTooltip,
  TabPanel,
  a11yProps,
  checkInvalidDate,
  convertNumberPrice,
  isCheckLenght, filterOptions, handleKeyDownIntegerOnly,
} from "app/appFunction";
import { createFilterOptions } from "@material-ui/lab";
import { appConst } from "app/appConst";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import DateFnsUtils from "@date-io/date-fns";
import { getListOrgManagementDepartment } from "../Asset/AssetService";
import moment from "moment";
import { getListUserByDepartmentId } from "../AssetTransfer/AssetTransferService";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { Label } from "../Component/Utilities";
import { searchByPageDepartmentNew } from "../Department/DepartmentService";
import { SignatureTabComponentV2 } from "../Component/Signature/SignatureTabComponentV2";

function MaterialButton(props) {
  const item = props.item;
  return (
    <div>
      <IconButton size="small" onClick={() => props.onSelect(item, 1)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AllocationScrollableTabsButtonForce(props) {
  const t = props.t;
  const i18n = props.i18n;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [isCheckStatusNew, setIsCheckStatusNew] = useState(false);
  const filterAutocomplete = createFilterOptions();

  let itemStatus =
    appConst.listStatusAllocation.find(
      (status) =>
        status.indexOrder === props?.item?.allocationStatus?.indexOrder
    ) || "";

  const isAllocated = props?.item?.allocationStatus?.indexOrder
    === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder
    || props?.item?.allocationStatus?.indexOrder
    === appConst.STATUS_ALLOCATION.TRA_VE.indexOrder;

  let searchObjectHandoverPerson = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    departmentId: props?.item?.handoverDepartment?.id,
  }
  let searchObjectReceiverPerson = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    departmentId: props?.item?.receiverDepartment?.id,
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (
      props?.item?.allocationStatus?.indexOrder ===
      appConst.listStatusAllocation[0]?.indexOrder
    ) {
      setIsCheckStatusNew(true);
    } else {
      setIsCheckStatusNew(false);
    }
  }, [props?.item?.allocationStatus]);
  let columns = [
    {
      title: t("InstrumentToolsType.action"),
      field: "custom",
      align: "center",
      hidden: props.isVisibility,
      maxWidth: 150,
      minWidth: 80,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
            } else if (method === appConst.active.delete) {
              props.removeAssetInlist(rowData?.asset?.id);
            } else {
              alert("Call Selected Here:" + rowData?.id);
            }
          }}
        />
      ),
    },
    {
      title: t("InstrumentToolsType.stt"),
      field: "asset.code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("InstrumentToolsAllocation.code"),
      field: "asset.code",
      align: "left",
      minWidth: 120,
      maxWidth: 150,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsType.managementCode"),
      field: "asset.managementCode",
      align: "left",
      maxWidth: 150,
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InstrumentToolsList.name"),
      field: "asset.name",
      align: "left",
      minWidth: 200,
    },
    {
      title: t("Asset.serialNumber"),
      field: "asset.serialNumber",
      align: "left",
      minWidth: 120,
    },
    {
      title: t("Asset.model"),
      field: "asset.model",
      align: "left",
      minWidth: 120,
    },
    {
      title: t("InstrumentToolsList.dateOfReception"),
      field: "asset.dateOfReception",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => {
        return moment(rowData?.asset?.dateOfReception).format("DD-MM-YYYY");
      },
    },
    {
      title: t("Asset.yearOfManufacture"),
      field: "asset.yearOfManufacture",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("Asset.manufacturer"),
      field: "asset.manufacturerName",
      align: "left",
      minWidth: 150,
      maxWidth: 200,
    },
    {
      title: t("InstrumentToolsType.storeName"),
      field: "asset.storeName",
      align: "left",
      maxWidth: 140,
      minWidth: 140,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.storeName || rowData?.storeName,
    },
    {
      title: t("InstrumentToolsType.availableQuantity"),
      field: "availableQuantity",
      align: "left",
      maxWidth: 100,
      minWidth: 80,
      hidden: props.isVisibility,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.availableQuantity || rowData?.assetItemQuantity,
    },
    {
      title: t("InstrumentToolsType.quantityAllocation"),
      field: "quantity",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          onChange={(quantity) =>
            props.handleRowDataCellChangeQuantity(rowData, quantity)
          }
          onKeyDown={handleKeyDownIntegerOnly}
          type="number"
          name="quantity"
          value={rowData.quantity || ""}
          inputProps={{
            style: { textAlign: "center" },
            readOnly: props.isVisibility,
            disableUnderline: props.isVisibility,
          }}
          validators={
            isAllocated
              ? ["required"]
              : [
                "required",
                `maxNumber:${rowData?.availableQuantity || rowData?.assetItemQuantity
                }`,
                `minNumber:1`,
              ]
          }
          errorMessages={[
            t("general.required"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
            t("general.minNumberError"),
          ]}
        />
      ),
    },
    {
      title: t("Asset.usePerson"),
      minWidth: 150,
      render: (rowData) => {
        const personObj = rowData?.usePersonId ? {
          personId: rowData?.usePersonId || null,
          personDisplayName: rowData?.usePersonDisplayName || null,
        } : null;
        return (
          <AsynchronousAutocompleteSub
            className="w-100"
            searchFunction={getListUserByDepartmentId}
            searchObject={props?.item?.searchParamUserByReceiverDepartment}
            displayLable="personDisplayName"
            typeReturnFunction="category"
            closeIcon={true}
            readOnly={props.isVisibility || props?.item?.isCheckReceiverDP}
            value={personObj || rowData.usePerson || null}
            onSelect={(event) => props.selectUsePerson(rowData, event)}
            noOptionsText={t("general.noOption")}
          />
        );
      },
    },
    {
      title: t("InstrumentToolsList.originalCost"),
      field: "asset.originalCost",
      align: "left",
      minWidth: 140,
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData?.asset?.originalCost
        ? convertNumberPrice(rowData?.asset?.originalCost)
        : 0
    },
    {
      title: t("InstrumentToolsType.note"),
      field: "note",
      align: "left",
      minWidth: 250,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          multiline
          rowsMax={2}
          className="w-100"
          onChange={(e) => props.handleRowDataCellChangeNote(rowData, e)}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: props.isVisibility,
            disableUnderline: props.isVisibility,
          }}
          validators={["maxStringLength:255"]}
          errorMessages={["Không nhập quá 255 ký tự"]}
        />
      ),
    },
  ];

  let columnsAssetFile = [
    {
      title: t("general.stt"),
      field: "code",
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("AssetFile.code"),
      field: "code",
      align: "left",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("AssetFile.name"),
      field: "name",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("AssetFile.description"),
      field: "description",
      align: "left",
      minWidth: 350,
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "left",
      maxWidth: 120,
      minWidth: 100,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        // !props.isVisibility ?
        <div className="none_wrap">
          <LightTooltip
            title={t("general.editIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="top"
            enterDelay={300}
            leaveDelay={200}
          >
            <IconButton
              size="small"
              onClick={() =>
                props.handleRowDataCellDeleteAssetFile(rowData?.id)
              }
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        </div>
      ),
    },
  ];

  // Trường hợp đặc biệt duy nhất để xử lý lỗi Invalid Date khi chuyển Tabpanel
  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name);
      return;
    }
  };
  const convertDataAssets = (data) => {
    let newData = [...data]?.map((item) => ({
      ...item,
      asset: {
        ...item?.asset,
        serialNumber:
          item?.assetSerialNumber || item?.asset.serialNumber || null,
        model: item?.assetModel || item?.asset.model || null,
        yearOfManufacture:
          item?.assetYearOfManufacture || item?.asset.yearOfManufacture || null,
        manufacturerName:
          item?.manufacturerName || item?.asset.manufacturerName || null,
        manufacturerId:
          item?.manufacturerId || item?.asset.manufacturerId || null,
        manufacturerCode:
          item?.manufacturerCode || item?.asset.manufacturerCode || null,
        note: item?.note || item?.asset?.note || null,
      },
    }));
    return newData;
  };

  return (
    <form id="firstChild">
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab
              label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.NAME)}
              {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE)}
              value={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE}
            />
            {/*<Tab*/}
            {/*  label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.NAME)}*/}
            {/*  {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE)}*/}
            {/*  value={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE}*/}
            {/*/>*/}
            <Tab
              label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.FILES.NAME)}
              {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.FILES.CODE)}
              value={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.FILES.CODE}
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={value}
          index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE}
        >
          <Grid container spacing={3}>
            <Grid item md={2} sm={6} xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                <DateTimePicker
                  fullWidth
                  margin="none"
                  id="mui-pickers-date"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t("Ngày tạo phiếu")}
                    </span>
                  }
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  format="dd/MM/yyyy"
                  name={"createAt"}
                  value={moment(props?.item?.createDate) || new Date()}
                  InputProps={{
                    readOnly: true,
                  }}
                  readOnly={true}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={2} sm={6} xs={6}>
              <CustomValidatePicker
                margin="none"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("allocation_asset.issueDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                format="dd/MM/yyyy"
                name={"issueDate"}
                value={props?.item?.issueDate}
                onChange={(date) => props.handleDateChange(date, "issueDate")}
                readOnly={props.isVisibility}
                minDate={
                  props.isAllocation
                    ? new Date(props?.item?.dateOfReception)
                    : new Date("1/1/1900")
                }
                maxDate={new Date()}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                InputProps={{
                  readOnly: props.isVisibility || props?.isAllocation,
                }}
                validators={["required"]}
                errorMessages={t("general.required")}
                minDateMessage={t("allocation_asset.errMissingMindate")}
                onBlur={() =>
                  handleBlurDate(props?.item?.issueDate, "issueDate")
                }
                clearable
              />
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                label={
                  <Label isRequired={!props.isVisibility && !props?.isAllocation}>
                    {t("allocation_asset.handoverDepartment")}
                  </Label>
                }
                searchFunction={
                  props.item?.isRoleAssetManager
                    ? getListOrgManagementDepartment
                    : searchByPageDepartmentNew
                }
                searchObject={
                  props.item?.isRoleAssetManager
                    ? {}
                    : props.handoverDepartmentSearchObject
                }
                listData={props.item.listHandoverDepartment}
                nameListData="listHandoverDepartment"
                setListData={props.handleSetDataSelect}
                displayLable={'name'}
                isNoRenderParent
                isNoRenderChildren
                typeReturnFunction={
                  props.item?.isRoleAssetManager
                    ? "list"
                    : "category"
                }
                value={props.item?.handoverDepartment || null}
                onSelect={props.selectHandoverDepartment}
                validators={["required"]}
                errorMessages={[t('general.required')]}
                showCode={"showCode"}
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                readOnly={props.isVisibility || props.item?.isRoleAssetManager}
              />
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              <AsynchronousAutocompleteSub
                label={
                  <Label isRequired={!props.isVisibility}>
                    {t("allocation_asset.handoverPerson")}
                  </Label>
                }
                disabled={!props?.item?.handoverDepartment?.id}
                searchFunction={getListUserByDepartmentId}
                searchObject={searchObjectHandoverPerson}
                displayLable="personDisplayName"
                typeReturnFunction="category"
                readOnly={props.isVisibility}
                value={props?.item?.handoverPerson ?? ""}
                onSelect={(handoverPerson) =>
                  props?.handleSelectHandoverPerson(handoverPerson)
                }
                filterOptions={filterOptions}
                noOptionsText={t("general.noOption")}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </Grid>
          </Grid>

          <Grid container spacing={3} className="mb-12">
            <Grid className="" item md={4} sm={12} xs={12}>
              {(props.isVisibility && !props.isStatus) ||
                props?.isAllocation ? (
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("InstrumentToolsType.voucherStatus")}
                  defaultValue={
                    props.item?.allocationStatus
                      ? props.item?.allocationStatus?.name
                      : ""
                  }
                  value={itemStatus ? itemStatus?.name : ""}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              ) : (
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>{" "}
                      <span> {t("InstrumentToolsType.voucherStatus")}</span>
                    </span>
                  }
                  searchFunction={searchByPageStatus}
                  searchObject={props.searchObjectStatus}
                  listData={props?.item?.listStatus}
                  setListData={props.handleSetDataStatus}
                  displayLable={"name"}
                  closeIcon={true}
                  value={itemStatus ? itemStatus : null}
                  defaultValue={itemStatus ? itemStatus : null}
                  onSelect={props.selectAlocationStatus}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                />
              )}
              <Grid item md={7} sm={12} xs={12} className="mt-12 mb-12">
                {!props?.isAllocation && !props.isVisibility && (
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className="mt-12 w-100"
                    onClick={props.handleAssetPopupOpen}
                  >
                    {t("InstrumentToolsAllocation.choose")}
                  </Button>
                )}
                {props.item.shouldOpenAssetPopup && (
                  <SelectAssetAllPopup
                    open={props.item.shouldOpenAssetPopup}
                    handleSelect={props.handleSelectAssetAll}
                    assetVouchers={
                      props.item.assetVouchers != null
                        ? props.item.assetVouchers
                        : []
                    }
                    handleClose={props.handleAssetPopupClose}
                    t={t}
                    i18n={i18n}
                    voucherType={props.item.type}
                    handoverDepartment={props.item?.handoverDepartment}
                    isAssetAllocation={props.item.isAssetAllocation}
                    statusIndexOrders={props.item.statusIndexOrders}
                    dateOfReceptionTop={props.item.issueDate}
                    getAssetDocumentList={props.getAssetDocumentList}
                    managementDepartmentId={props.item?.handoverDepartment?.id}
                    isSearchForAllocation={true}
                  />
                )}
              </Grid>
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              {props.isVisibility ? (
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("allocation_asset.receiverDepartment")}
                  value={
                    props.item?.receiverDepartment
                      ? props.item?.receiverDepartment?.text
                      : null
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                />
              ) : (
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      <span> {t("allocation_asset.receiverDepartment")}</span>
                    </span>
                  }
                  searchFunction={searchReceiverDepartment} //linh todo : searchReceiverDepartment
                  searchObject={props.receiverDepartmentSearchObject}
                  listData={props.item.listReceiverDepartment}
                  setListData={props.handleSetDataReceiverDepartment}
                  defaultValue={
                    props.item.receiverDepartment
                      ? props.item.receiverDepartment
                      : null
                  }
                  displayLable={"text"}
                  value={
                    props.item.receiverDepartment
                      ? props.item.receiverDepartment
                      : null
                  }
                  onSelect={props.selectReceiverDepartment}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                />
              )}
            </Grid>
            <Grid className="" item md={4} sm={12} xs={12}>
              {props.isVisibility ? (
                <TextValidator
                  className={props.className ? props.className : "w-100"}
                  label={t("allocation_asset.receiverPerson")}
                  name="receiverPersonName"
                  onChange={props.handleChange}
                  value={
                    props.item?.receiverPersonName
                      ? props.item?.receiverPersonName
                      : null
                  }
                  InputProps={{
                    readOnly: props.isVisibility,
                  }}
                />
              ) : (
                <AsynchronousAutocompleteSub
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t("AssetTransfer.receiverPerson")}
                    </span>
                  }
                  disabled={!props?.item?.receiverDepartment?.id}
                  searchFunction={getListUserByDepartmentId}
                  searchObject={searchObjectReceiverPerson}
                  defaultValue={props?.item?.receiverPerson ?? ""}
                  displayLable="personDisplayName"
                  typeReturnFunction="category"
                  value={props?.item?.receiverPerson ?? ""}
                  onSelect={(receiverPerson) =>
                    props?.handleSelectReceiverPerson(receiverPerson)
                  }
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    let filtered = filterAutocomplete(options, params);
                    return filtered;
                  }}
                  noOptionsText={t("general.noOption")}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              )}
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <MaterialTable
                data={
                  props.item.assetVouchers
                    ? convertDataAssets(props.item.assetVouchers)
                    : []
                }
                columns={columns}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  paging: false,
                  search: false,
                  sorting: false,
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    paddingLeft: 10,
                    paddingRight: 10,
                    textAlign: "center",
                  },
                  maxBodyHeight: '225px',
                  minBodyHeight: '225px',
                }}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                components={{
                  Toolbar: (props) => (
                    <div style={{ witdth: "100%" }}>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value}
          index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE}>
          <SignatureTabComponentV2
            isExport={true}
            data={{ ...props.item?.signature }}
            permission={props.item?.isViewSignPermission}
            handleChange={props.handleChangeSignature}
          />
        </TabPanel>
        <TabPanel
          value={value}
          index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.FILES.CODE}
        >
          <Grid item md={12} sm={12} xs={12}>
            {!props.isVisibility && (
              <Button
                size="small"
                variant="contained"
                color="primary"
                onClick={props.handleAddAssetDocumentItem}
              >
                {t("InstrumentToolsList.addProfile")}
              </Button>
            )}
            {props.item.shouldOpenPopupAssetFile && (
              <VoucherFilePopup
                open={props.item.shouldOpenPopupAssetFile}
                handleClose={props.handleAssetFilePopupClose}
                itemAssetDocument={props.itemAssetDocument}
                getAssetDocument={props.getAssetDocument}
                handleUpdateAssetDocument={props.handleUpdateAssetDocument}
                documentType={props?.item?.documentType}
                item={props.item}
                t={t}
                i18n={i18n}
              />
            )}
          </Grid>
          <Grid item md={12} sm={12} xs={12} className="mt-16">
            <MaterialTable
              data={props.item.documents || []}
              columns={columnsAssetFile}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                maxBodyHeight: "290px",
                minBodyHeight: "290px",
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ width: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </TabPanel>
      </div>
    </form>
  );
}
