import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";
const InstrumentsToolsAllocationTable = EgretLoadable({
  loader: () => import("./InstrumentsToolsAllocationTable"),
});
const ViewComponent = withTranslation()(InstrumentsToolsAllocationTable);

const IntrumentsToolsAllocationRoutes = [
  {
    path: ConstantList.ROOT_PATH + "instruments-and-tools/allocation-vouchers",
    exact: true,
    component: ViewComponent,
  },
];

export default IntrumentsToolsAllocationRoutes;
