import ConstantList from "../../appConfig";
import React, { Component } from "react";
import { appConst, STATUS_ASSET_FOR_ALLOCATION, variable } from '../../appConst';
import { Button, Dialog, DialogActions, Grid, } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { createAllocation, updateAllocation, updateToReturn } from "./InstrumentsToolsAllocationService";
import AllocationScrollableTabsButtonForce from "./AllocationScrollableTabsButtonForce";
import { getAssetDocumentById, getNewCodeDocument, } from "../Asset/AssetService";
import { searchByPage as searchByPageStatus } from '../AllocationStatus/AllocationStatusService'
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  checkInvalidDate,
  checkMinDate,
  checkObject,
  formatDateDto,
  getTheHighestRole, getUserInformation,
  isCheckLenght
} from "app/appFunction";
import AppContext from "app/appContext";
import { ConfirmationDialog } from "egret";
import { PaperComponent } from "../Component/Utilities";
import { getItemById } from "../Department/DepartmentService";
import { createSignature } from "../Component/Signature/services";
import { isSuccessfulResponse } from "../../appFunction";


class AllocationEditorDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Allocation,
    rowsPerPage: 10,
    page: 0,
    assetVouchers: [],
    statusIndexOrders: STATUS_ASSET_FOR_ALLOCATION, // chỉ cấp phát tài sản lưu kho
    handoverPerson: null,
    managementDepartment: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: (this.props?.InstrumentsToolsAllocation)
      ? new Date(this.props?.InstrumentsToolsAllocation?.dateOfReception)
      : new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenManagementDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    totalElements: 0,
    asset: {},
    shouldOpenPersonPopup: false,
    textNotificationPopup: "",
    isAssetAllocation: true,
    shouldOpenPopupAssetFile: false,
    documentType: appConst.documentType.IAT_DOCUMENT,
    assetDocumentList: [],
    listAssetDocumentId: [],
    count: 1,
    allocationStatus: null,
    loading: false,
    keyword: '',
    checkPermissionUserDepartment: true,
    check: false,
    assetId: '',
    usePerson: null,
    status: appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
    managementDepartmentId: '',
    documents: [],
    isViewAssetFile: false,
    searchParamUserByReceiverDepartment: {},
    signature: {},
    isViewSignPermission: {
      [variable.listInputName.headOfDepartment]: false,
      [variable.listInputName.deputyDepartment]: false,
      [variable.listInputName.director]: false,
      [variable.listInputName.deputyDirector]: false,
      [variable.listInputName.chiefAccountant]: false,
      [variable.listInputName.storeKeeper]: false,
      [variable.listInputName.createdPerson]: true,
      [variable.listInputName.handoverPerson]: true,
      [variable.listInputName.receiverPerson]: true,
    }
  };

  convertDto = (state) => {
    return {
      allocationStatusId: state?.allocationStatusId,
      assetVouchers: state?.assetVouchers || [],
      handoverDepartmentId: state?.handoverDepartmentId,
      handoverPersonName: state?.handoverPerson?.personDisplayName,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      listAssetDocumentId: state?.listAssetDocumentId,
      receiverDepartmentId: state?.receiverDepartmentId,
      receiverPersonName: state?.receiverPerson?.personDisplayName,
      receiverPersonId: state?.receiverPerson?.personId,
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleDateChange = (date, name) => {
    if (name === "issueDate" && date) {
      this.setState({
        [name]: date,
        assetVouchers: []
      });
    }
    else {
      this.setState({
        [name]: date,
      });
    }
  };

  validateSubmit = () => {
    let {
      assetVouchers,
      handoverPerson,
      handoverDepartment,
      receiverDepartment,
      allocationStatus,
      receiverPerson,
      issueDate
    } = this.state;
    let { isVisibility, isStatus } = this.props;
    let checkStatus = appConst.listStatusAllocation[1].indexOrder === allocationStatus.indexOrder

    if (checkStatus && isVisibility && isStatus) {
      toast.warning('Chưa thay đổi trạng thái phiếu');
      return false;
    }
    if (!issueDate) {
      toast.warning('Chưa chọn ngày chứng từ');
      return false;
    }
    if (checkInvalidDate(issueDate)) {
      toast.warning('Ngày chứng từ không tồn tại');
      return false;
    }
    if (checkMinDate(issueDate, "01/01/1900")) {
      toast.warning(`Ngày chứng từ không được nhỏ hơn ngày 01/01/1900`);
      return false;
    }
    if (!handoverDepartment) {
      toast.warning('Chưa chọn phòng bàn giao');
      return false;
    }
    if (!handoverPerson?.personDisplayName) {
      toast.warning('Nhập chọn người bàn giao');
      return false;
    }
    if (!assetVouchers || assetVouchers?.length === 0) {
      toast.warning('Chưa chọn CCDC');
      return false;
    }
    if (!receiverDepartment) {
      toast.warning('Chưa chọn phong ban tiếp nhận');
      return false;
    }
    if (!receiverPerson) {
      toast.warning('Chưa chọn người tiếp nhận');
      return false;
    }
    if (this.validateAssetVouchers()) return;
    return true;
  }

  validateAssetVouchers = () => {
    let { assetVouchers } = this.state;
    let check = false;
    // eslint-disable-next-line no-unused-expressions
    assetVouchers?.some(asset => {
      if (isCheckLenght(asset?.note)) {
        toast.warning("Ghi chú không nhập quá 255 ký tự.");
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  checkDataApi = async (id, dataState) => {
    if (id) {
      if (!this.props.isStatus) {
        return updateAllocation(id, dataState);
      }
      return updateToReturn(id, dataState, this.state?.status)
    } else {
      return createAllocation(dataState);
    }
  }

  handleStatus = async (status) => {
    let dataState = this.convertDto(this.state)
    let { id } = this.state
    const res = await updateToReturn(id, dataState, status)
    if (res?.data?.code === appConst.CODE.SUCCESS) {
      toast.success(res?.data?.message);
      this.props.handleOKEditClose();
      this.props.handleClose()
    } else {
      toast.warning(res?.data?.message);
    }
  }

  handleCloseRefuseTransferDialog = () => {
    this.setState({
      shouldOpenRefuseTransfer: false
    })
  }

  handleFormSubmit = async (e) => {
    let { setPageLoading } = this.context;
    let { id } = this.state;
    let {
      t,
      handleOKEditClose = () => { },
    } = this.props;

    if (e.target.id !== "parent") return;
    if (!this.validateSubmit()) return;

    let dataState = this.convertDto(this.state);
    setPageLoading(true);
    try {
      const res = await this.checkDataApi(id, dataState);
      if (appConst.CODE.SUCCESS === res.data?.code) {
        // await this.handleSaveSign(res?.data?.data?.id);
        handleOKEditClose();
        toast.success(res.data?.message);
      } else {
        toast.warning(res.data?.message);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };
  convertDataSign = (data = {}) => {
    return {
      directorId: data?.directorId,
      directorName: data?.directorName,
      deputyDirectorId: data?.deputyDirectorId,
      deputyDirectorName: data?.deputyDirectorName,
      chiefAccountantId: data?.chiefAccountantId,
      chiefAccountantName: data?.chiefAccountantName,
      headOfDepartmentId: data?.headOfDepartmentId,
      headOfDepartmentName: data?.headOfDepartmentName,
      deputyDepartmentId: data?.deputyDepartmentId,
      deputyDepartmentName: data?.deputyDepartmentName,
      storekeeperId: data?.storekeeperId,
      storekeeperName: data?.storekeeperName,
      handoverPersonId: data?.handoverPersonId,
      handoverPersonName: data?.handoverPersonName,
      receiverPersonId: data?.receiverPersonId,
      receiverPersonName: data?.receiverPersonName,
      voucherId: data?.voucherId,
    }
  }
  handleSaveSign = async (voucherId) => {
    try {
      const payload = this.convertDataSign({
        ...this.state.signature,
        voucherId: voucherId
      });
      const data = await createSignature(payload, voucherId);
    } catch (error) {

    }
  }

  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        receiverPersonId: item?.personId,
        receiverPersonName: item?.personDisplayName,
      }
    });
  };

  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        handoverPersonId: item?.personId,
        handoverPersonName: item?.personDisplayName,
      }
    });
  };

  getDetailDepartment = async (departmentId) => {
    if (!departmentId) return {};
    try {
      const data = await getItemById(departmentId);
      
      if (isSuccessfulResponse(data?.status)) {
        return {
          headOfDepartmentId: data?.data?.headOfDepartmentId,
          headOfDepartmentName: data?.data?.headOfDepartmentName,
          deputyDepartmentId: data?.data?.deputyDepartmentId,
          deputyDepartmentName: data?.data?.deputyDepartmentName,
        }
      } else {
        return {}
      }
    } catch (error) {
      return {};
    }
  }

  onMountSetSign = async (item) => {
    const { currentUser, organization } = getUserInformation();
    const itemDepartment = await this.getDetailDepartment(item?.receiverDepartmentId)
    this.setState({
      signature: {
        directorId: item?.directorId || organization?.directorId,
        directorName: item?.directorName || organization?.directorName,
        deputyDirectorId: item?.deputyDirectorId || organization?.deputyDirectorId,
        deputyDirectorName: item?.deputyDirectorName || organization?.deputyDirectorName,
        chiefAccountantId: item?.chiefAccountantId || organization?.chiefAccountantId,
        chiefAccountantName: item?.chiefAccountantName || organization?.chiefAccountantName,
        headOfDepartmentId: item?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: item?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: item?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: item?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
        storekeeperId: item?.storekeeperId,
        storekeeperName: item?.storekeeperName,
        handoverPersonId: item?.handoverPersonId,
        handoverPersonName: item?.handoverPersonName,
        receiverPersonId: item?.receiverPersonId,
        receiverPersonName: item?.receiverPersonName,
        createdPersonId: item?.createdPersonId || currentUser?.person?.id,
        createdPersonName: item?.createdPersonName || currentUser?.person?.displayName,
      }
    })
  }

  componentDidMount() {
    let { item, isVisibility } = this.props;
    let roles = getTheHighestRole();
    let { departmentUser } = getUserInformation();
    this.setState({
      ...roles,
      ...item,
      isVisibility,
      receiverPerson: {
        personId: item?.receiverPersonId,
        personDisplayName: item?.receiverPersonName,
      },
      handoverPerson: {
        personId: item?.handoverPersonId,
        personDisplayName: item?.handoverPersonName,
      },
      handoverDepartment: item?.handoverDepartment || (departmentUser?.id ? departmentUser : null),
      handoverDepartmentId: item?.handoverDepartment?.id || (departmentUser?.id || null)
    });

    // this.onMountSetSign(item)

    if (this.props.isAllocation) {
      let assetVouchers = []
      if (Array.isArray(this.props?.InstrumentsToolsAllocation?.assetVouchers)) {
        assetVouchers = this.props?.InstrumentsToolsAllocation?.assetVouchers ?? []
      }
      else {
        let asset = {}
        asset.asset = this.props?.InstrumentsToolsAllocation
        assetVouchers.push(asset)
      }
      this.handleSelectAssetAll(assetVouchers)
      this.status(appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder)
      this.setState({
        ...item,
        createDate: new Date(),
        isCheckReceiverDP: true,
      })
      return;
    }

    if (!this.props?.isstatus && !this.props?.isVisibility) {
      this.status(appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder)
    }
  }

  status = (indexOrder) => {
    let { t } = this.props;
    let searchObjectStatus = { pageIndex: 0, pageSize: 100 };
    searchByPageStatus(searchObjectStatus).then(({ data }) => {
      let dataStatus = data?.content;
      let itemStatus = dataStatus.find((status) => indexOrder === status?.indexOrder)
      this.selectAlocationStatus(itemStatus)
    }).catch((error) => {
      toast.error(t("general.error"));
    });
  }

  handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
    });
  };

  handleAssetPopupOpen = () => {
    let { statusIndexOrders, handoverDepartment } = this.state;
    if (handoverDepartment) {
      this.setState({
        shouldOpenAssetPopup: true,
        item: {},
        statusIndexOrders: statusIndexOrders,
      });
    } else {
      toast.info("Vui lòng chọn phòng ban bàn giao.");
    }
  };

  removeAssetInlist = (id) => {
    let { assetVouchers } = this.state;
    let index = assetVouchers.findIndex((x) => x.asset.id === id);
    assetVouchers.splice(index, 1);
    this.setState({
      assetVouchers,
    });
  };

  handleSelectAssetAll = (items) => {
    let { t } = this.props;
    let assetVouchers = []
    // eslint-disable-next-line no-unused-expressions
    items?.forEach((element) => {
      let exitsItem = this.state.assetVouchers.find(i => i.assetId === (element.assetId || element.asset.assetId));
      element.assetId = element?.assetId ? element?.assetId : element.asset?.assetId;
      element.quantity = element?.quantity || element?.asset?.quantity;
      element.availableQuantity = element?.asset?.quantity || element?.assetItemQuantity
      element.storeId = element?.asset?.storeId || element?.storeId;
      if (exitsItem) {
        element.usePerson = exitsItem?.usePerson?.personId ? { ...exitsItem?.usePerson } : null;
        element.usePersonDisplayName = exitsItem?.usePersonDisplayName;
        element.usePersonId = exitsItem?.usePersonId;
      }
      assetVouchers.push(element)
    });
    this.setState({ assetVouchers: assetVouchers }, () => {
      this.handleAssetPopupClose();
    });
    if (items?.length === 0) {
      toast.warning(t("general.noCCDCSelected"));
      return;
    }
  };

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    });
  };
  handlePersonPopupOpen = () => {
    this.setState({
      shouldOpenPersonPopup: true,
    });
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  handleManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: false,
    });
  };
  handleManagementDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = item;
    this.setState({
      handoverDepartment: department,
      handoverDepartmentId: department?.id
    }, function () {
      this.handleManagementDepartmentPopupClose();
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  selectUsePerson = (rowData, event) => {
    let { assetVouchers } = this.state;
    if (assetVouchers != null && assetVouchers.length > 0) {
      assetVouchers.forEach((av) => {
        if (av != null && rowData != null && av.asset?.id == rowData.asset?.id) {
          av.usePerson = event;
          av.usePersonId = event?.id
          av.usePersonId = event ? event?.personId : null;
          av.usePersonDisplayName = event?.personDisplayName;
        }
      });
      this.setState({ assetVouchers: assetVouchers });
    }
  };

  // selectReceiverPerson = (item) => {
  //   this.setState({ receiverPerson: item }, function () { });
  // }

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item ? item : null,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      handoverPersonId: null,
      managementDepartmentId: item?.id,
      assetVouchers: [],
      listHandoverPerson: []
    });
  };

  selectReceiverDepartment = async (item) => {
    let { assetVouchers } = this.state;
    (assetVouchers?.length > 0) && assetVouchers.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );
    this.setState({
      assetVouchers: assetVouchers,
      receiverDepartment: item,
      receiverDepartmentId: item?.id,
      receiverPerson: null,
      receiverPersonId: null,
      isCheckReceiverDP: item ? false : true,
      usePerson: null,
      listReceiverPerson: [],
      searchParamUserByReceiverDepartment: { departmentId: item?.id || "" }
    });
    const itemDepartment = await this.getDetailDepartment(item?.id)

    this.setState({
      signature: {
        ...this.state.signature,
        headOfDepartmentId: this.state.signature?.headOfDepartmentId || itemDepartment?.headOfDepartmentId,
        headOfDepartmentName: this.state.signature?.headOfDepartmentName || itemDepartment?.headOfDepartmentName,
        deputyDepartmentId: this.state.signature?.deputyDepartmentId || itemDepartment?.deputyDepartmentId,
        deputyDepartmentName: this.state.signature?.deputyDepartmentName || itemDepartment?.deputyDepartmentName,
      }
    })
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      // eslint-disable-next-line no-unused-expressions
      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isViewAssetFile: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  selectAlocationStatus = (item) => {
    this.setState({
      status: item?.indexOrder,
      allocationStatus: item,
      allocationStatusId: item?.id
    });
  };

  handleSetDataStatus = (data) => {
    const statusMappings = {
      [appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder]: [
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
      ],
      [appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder]: [
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder]: [
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder
      ]
    };

    const allocationStatusIndexOrder = this.state.allocationStatus?.indexOrder;
    const dataNew = data.filter((item) => statusMappings[allocationStatusIndexOrder].includes(item.indexOrder));

    this.setState({
      listStatus: dataNew
    });
  };

  handleRowDataCellChangeNote = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.asset?.id === rowData.asset?.id) {
        assetVoucher.note = valueText.target.value;
      }
    });
    this.setState({ assetVouchers, handoverPersonClone: true });
  };

  handleRowDataCellChangeQuantity = (rowData, valueText) => {
    let { assetVouchers } = this.state;
    assetVouchers.map((assetVoucher) => {
      if (assetVoucher.asset?.id === rowData.asset?.id) {
        let inputValue = +valueText.target.value;
        assetVoucher.quantity = inputValue;
        assetVoucher.asset = {
          ...(rowData?.asset || {}),
          originalCost: (rowData?.asset?.unitPrice * inputValue) || 0,
        }
      }
    });

    this.setState({ assetVouchers: assetVouchers, handoverPersonClone: true });
  };

  handleSetDataHandoverDepartment = (data) => {
    this.setState({
      listHandoverDepartment: data
    })
  }

  handleSetDataHandoverPerson = (data) => {
    this.setState({
      listHandoverPerson: data
    })
  }

  handleSetDataReceiverDepartment = (data) => {
    this.setState({
      listReceiverDepartment: data
    })
  }

  handleSetDataReceiverPerson = (data) => {
    this.setState({
      listReceiverPerson: data
    })
  }

  clearStateDate = () => {
    this.setState({
      listHandoverPerson: [],
      handoverPerson: null,
      handoverPersonId: "",
      receiverDepartment: null,
      receiverDepartmentId: "",
      listReceiverPerson: [],
      receiverPerson: null,
      receiverPersonId: "",
      assetVouchers: [],
      allocationStatus: appConst.STATUS_ALLOCATION.MOI_TAO,
      allocationStatusIndex: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
    })
  }

  handleChangeSignature = (data = []) => {
    this.setState({
      signature: data,
    })
  }

  render() {
    let { open, t, i18n, isStatus } = this.props;
    let {
      loading,
      isConfirm,
      isVisibility,
    } = this.state;
    const { organization } = getUserInformation();
    let searchObjectStatus = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let handoverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      orgId: organization?.org?.id,
    };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      keyword: '',
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >

        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        {this.state?.shouldOpenRefuseTransfer &&
          <ConfirmationDialog
            title={t('general.confirm')}
            open={this.state?.shouldOpenRefuseTransfer}
            onConfirmDialogClose={this.handleCloseRefuseTransferDialog}
            onYesClick={() => this.handleStatus(appConst.STATUS_ALLOCATION.TRA_VE.indexOrder)}
            text={t('general.cancel_receive_tools')}
            agree={t('general.agree')}
            cancel={t('general.cancel')}
          />
        }
        <ValidatorForm
          id="parent"
          onSubmit={(event) => this.handleFormSubmit(event)}
          class="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "3px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("allocation_asset.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent style={{ height: "550px" }}>
            <AllocationScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAlocationStatus={this.selectAlocationStatus}
              handleSelectHandoverDepartment={
                this.handleSelectHandoverDepartment
              }
              handleManagementDepartmentPopupClose={
                this.handleManagementDepartmentPopupClose
              }
              handoverPersonSearchObject={handoverPersonSearchObject}
              handoverDepartmentSearchObject={handoverDepartmentSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              selectHandoverDepartment={this.selectHandoverDepartment}
              selectReceiverDepartment={this.selectReceiverDepartment}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              // selectReceiverPerson={this.selectReceiverPerson}
              handleManagementDepartmentPopupOpen={
                this.handleManagementDepartmentPopupOpen
              }
              handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
              handleDateChange={this.handleDateChange}
              handleDepartmentPopupClose={this.handleDepartmentPopupClose}
              handlePersonPopupOpen={this.handlePersonPopupOpen}
              handlePersonPopupClose={this.handlePersonPopupClose}
              handleAssetPopupOpen={this.handleAssetPopupOpen}
              handleSelectAssetAll={this.handleSelectAssetAll}
              handleAssetPopupClose={this.handleAssetPopupClose}
              removeAssetInlist={this.removeAssetInlist}
              selectUsePerson={this.selectUsePerson}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={
                this.handleRowDataCellEditAssetFile
              }
              handleRowDataCellDeleteAssetFile={
                this.handleRowDataCellDeleteAssetFile
              }
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              handleRowDataCellChangeNote={this.handleRowDataCellChangeNote}
              handleRowDataCellChangeQuantity={this.handleRowDataCellChangeQuantity}
              getAssetDocument={this.getAssetDocument}
              isVisibility={isVisibility}
              isStatus={isStatus}
              check={this.state.check}
              handleSetDataHandoverDepartment={this.handleSetDataHandoverDepartment}
              handleSetDataHandoverPerson={this.handleSetDataHandoverPerson}
              handleSetDataReceiverDepartment={this.handleSetDataReceiverDepartment}
              handleSetDataReceiverPerson={this.handleSetDataReceiverPerson}
              handleSetDataStatus={this.handleSetDataStatus}
              handleChange={this.handleChange}
              isAllocation={this.props.isAllocation}
              isHideButton={this.props.isHideButton}
              managementDepartmentId={this.state.managementDepartmentId}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleRowDataCellViewAssetFile={this.handleRowDataCellViewAssetFile}
              handleChangeSignature={this.handleChangeSignature}
            />
          </DialogContent>
          <DialogActions>
            <Grid className="mr-16">
              <Button
                className="ml-12"
                variant="contained"
                color="secondary"
                onClick={() =>
                (this.props?.isAllocation
                  ? (this.props?.isUpdateAsset
                    ? this.props.handleCloseAllocationEditorDialog()
                    : this.props.handleOKEditClose())
                  : this.props.handleClose())
                }
              >
                {t("InstrumentToolsTransfer.close")}
              </Button>
              {
                isConfirm ? <>
                  <Button
                    className="ml-12"
                    variant="contained"
                    color="primary"
                    onClick={() => this.handleStatus(appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder)}
                  >
                    {t("InstrumentToolsTransfer.confirm")}
                  </Button>
                  <Button
                    className="btnRefuse ml-12"
                    variant="contained"
                    onClick={() => {
                      this.setState({
                        shouldOpenRefuseTransfer: true
                      });
                    }}
                  >
                    {t("InstrumentToolsTransfer.refuse")}
                  </Button>
                </> : <>
                  {(!isVisibility || this.props.isStatus) &&
                    <Button
                      id="save"
                      variant="contained"
                      color="primary"
                      type="submit"
                      className="ml-12"
                    >
                      {t("general.save")}
                    </Button>
                  }
                  {
                    this.state.allocationStatusIndex === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder &&
                    <Button
                      variant="contained"
                      color="primary"
                      className="ml-12"
                      onClick={() => {
                        this.props.handleQrCode(this.state);
                      }}
                    >
                      {t("Asset.PrintQRCode")}
                    </Button>
                  }
                </>
              }
            </Grid>
          </DialogActions>
        </ValidatorForm>

      </Dialog>
    );
  }
}

AllocationEditorDialog.contextType = AppContext;
export default AllocationEditorDialog;
