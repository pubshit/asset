import {
  Grid,
  InputAdornment,
  Input,
  Button,
  Checkbox,
  TablePagination,
  Dialog,
  DialogActions,
  FormControl,
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import MaterialTable, {
  MTableToolbar,
} from "material-table";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { searchByPageCCDC, searchByPageCCDCV2 } from "../InstrumentToolsList/InstrumentToolsListService";
import { convertNumberPrice, formatDateDto } from "app/appFunction";
import moment from "moment";
import { appConst } from "app/appConst";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
class SelectAssetAllPopop extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    rowsPerPage: 5,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectedItem: {},
    keyword: "",
    shouldOpenProductDialog: false,
    assetList: [],
  };

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  updatePageData = () => {
    let { t } = this.props
    let status = 0;
    var searchObject = {};
    searchObject.voucherType = this.props.voucherType;
    searchObject.useDepartmentId = this.props.departmentId;
    if (this.props.isAssetTransfer) {
      searchObject.statusIndexOrders = this.props.statusIndexOrders;
    } else if (this.props.isAssetAllocation) {
      searchObject.statusIndexOrders = this.props.statusIndexOrders;
      searchObject.isSearchForAllocation = this.props.isSearchForAllocation;
    } else {
      searchObject.indexOrder = status;
    }
    searchObject.keyword = this.state.keyword.trim();
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.CCDC;
    searchObject.dateOfReceptionTop = this.props?.dateOfReceptionTop
      ? formatDateDto(this.props?.dateOfReceptionTop)
      : null;
    searchObject.managementDepartmentId = this.props.managementDepartmentId;
    searchByPageCCDCV2(searchObject).then(({ data }) => {
      let itemListClone = [...data?.data?.content];
      itemListClone.map((item) => {
        item.isCheck = false;
        if (!this.props.isSearchForAllocation) {
          item.id = item.assetId;
        }
        if (this.state.assetVouchers && this.state.assetVouchers.length > 0) {
          this.state.assetVouchers.forEach((assetVoucher) => {
            if (
              (assetVoucher?.assetItemId === item.id || assetVoucher.asset.id === item.id)
              && ((assetVoucher.asset.storeId === item.storeId) || (assetVoucher.storeId === item.storeId))
            ) {
              item.isCheck = true;
            }
            return assetVoucher
          });
        }
      });
      this.setState(
        { itemList: [...itemListClone], totalElements: data?.data?.totalElements },
        () => { }
      );
    }).catch((e) => {
      toast.error(t("toastr.error"))
    });
  };

  componentDidMount() {
    if (this.props.assetVouchers) {
      this.setState({ assetVouchers: this.props.assetVouchers });
    }
    this.updatePageData();
  }

  handleClick = (event, item) => {
    document.querySelector(`#radio${item.id}`).click();
    item.isCheck = !event.target.checked;
    let { assetVouchers = [] } = this.state;
    if (item.isCheck === true) {
      let check = assetVouchers.filter(
        assetVoucher => (assetVoucher.assetId || assetVoucher.asset.assetId) === item.assetId
          && assetVoucher.asset.storeId === item.storeId
      );
      if (check.length === 0) {
        let p = {};
        p = { asset: item };
        assetVouchers = assetVouchers.concat(p);
      }
    }
    if (item.isCheck === false) {
      assetVouchers = assetVouchers.filter(assetVoucher => (assetVoucher.assetId || assetVoucher.asset.assetId) !== item.assetId)
    }
    this.setState({ assetVouchers: assetVouchers }, () => {
      console.log(assetVouchers);
    });
  };

  UNSAFE_componentWillMount() {
    let {
      handoverDepartment,
      managementDepartmentId,
      assetVouchers
    } = this.props;

    this.setState({
      handoverDepartment,
      managementDepartmentId,
      assetVouchers
    });
  }

  search() {
    this.setPage(0);
  }

  handleChange = (event, source) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
    // this.setState({ keyword: event.target.value }, function () { });
  };

  // onClickRow = (selectedRow) => {
  //   document.querySelector(`#radio${selectedRow.id}`).click();
  // };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  render() {
    const {
      t,
      handleClose,
      handleSelect,
      open,
    } = this.props;
    let {
      keyword,
      assetVouchers,
      rowsPerPage,
      page
    } = this.state;
    let stt = null;
    let columns = [
      {
        title: t("general.select"),
        field: "custom",
        align: "left",
        width: "60px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            id={`radio${rowData.id}`}
            name="radSelected"
            value={rowData.id}
            checked={rowData.isCheck}
            onClick={(event) => this.handleClick(event, rowData)}
          />
        ),
      },
      {
        title: t("InstrumentToolsType.stt"),
        field: "code",
        minWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (rowsPerPage * page + (rowData?.tableData?.id + 1))
      },
      {
        title: t("InstrumentToolsType.code"),
        field: "code",
        align: "left",
        width: "150px",
      },
      {
        title: t("InstrumentToolsType.managementCode"),
        field: "managementCode",
        align: "left",
        width: "150",
      },
      {
        title: t("InstrumentToolsType.name"),
        field: "name",
        align: "left",
        minWidth: "150px",
        maxWidth: "200px",
        cellStyle: {
          overflow: "hidden",
          textOverflow: "ellipsis"
        }
      },
      {
        title: t("InstrumentToolsList.dateOfReception"),
        field: "dateOfReception",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => moment(rowData.dateOfReception).format('DD-MM-YYYY')
      },
      {
        title: t("InstrumentToolsType.storeName"),
        field: "storeName",
        align: "left",
        width: "150",
      },
      {
        title: t("InstrumentToolsType.quantity"),
        field: "quantity",
        align: "left",
        width: "150",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("TransferToAnotherUnit.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.originalCost
          ? convertNumberPrice(rowData?.originalCost)
          : 0
      },
    ];
    return (
      <Dialog
        onClose={handleClose}
        open={open}
        PaperComponent={PaperComponent}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{"Chọn CCDC"}</span>
        </DialogTitle>
        <DialogContent style={{ height: "370px" }}>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl fullWidth>
              <Input
                label={t("general.enterSearch")}
                type="text"
                name="keyword"
                value={keyword}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                className="w-100 mb-16"
                id="search_box"
                placeholder={t("general.enterSearch")}
                startAdornment={
                  <InputAdornment>
                    <Link to="#">
                      {" "}
                      <SearchIcon
                        onClick={() => this.search(keyword)}
                        style={{
                          position: "absolute",
                          top: "0",
                          right: "0",
                        }}
                      />
                    </Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={this.state.itemList}
              columns={columns}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
              }}
              options={{
                sorting: false,
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                padding: "dense",
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                },
                maxBodyHeight: "253px",
                minBodyHeight: "253px",
              }}
              components={{
                Toolbar: (props) => (
                  <div style={{ witdth: "100%" }}>
                    <MTableToolbar {...props} />
                  </div>
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[5, 10, 20, 30]}
              component="div"
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              labelRowsPerPage={t("general.rows_per_page")}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              backIconButtonProps={{
                "aria-label": "Previous Page",
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page",
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              className="mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={(event) => handleSelect(assetVouchers)}
            >
              {t("general.select")}
            </Button>
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}
export default SelectAssetAllPopop;
