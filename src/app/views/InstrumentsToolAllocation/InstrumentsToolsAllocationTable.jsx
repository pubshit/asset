import ConstantList from "../../appConfig";
import React from "react";
import { appConst, DEFAULT_TOOLTIPS_PROPS, LIST_ORGANIZATION, variable } from "app/appConst";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import { exportToExcel } from "./InstrumentsToolsAllocationService";
import {
  searchByPage,
  deleteInstrumentTool,
  getItemById,
  deleteItem,
  getCountStatus,
  exportExampleImportExcel,
} from "./InstrumentsToolsAllocationService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import FileSaver, { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import ComponentAllocationTable from "./ComponentAllocationTable";
import AppContext from "app/appContext";
import ArrowForwardOutlinedIcon from "@material-ui/icons/ArrowForwardOutlined";
import {
  convertFromToDate,
  convertNumberPriceRoundUp,
  formatDateTypeArray,
  getTheHighestRole,
  isValidDate,
  functionExportToExcel,
  encodeWithVariableCharacters,
  isSuccessfulResponse,
  handleThrowResponseMessage,
} from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import { LightTooltip, TabPanel } from "../Component/Utilities";
import { getSignature } from "../Component/Signature/services";


toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const {
    departmentUser,
  } = getTheHighestRole();
  const {
    hasDeletePermission,
    hasEditPermission,
    hasPrintPermission,
    hasSuperAccess,
    value,
    item,
  } = props;
  const isEdit = hasEditPermission &&
    [
      appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
    ].includes(item?.allocationStatusIndex)
  const isWaitForConfirmation = appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder === item?.allocationStatusIndex;
  const isReceiverDepartment = hasSuperAccess || (item.receiverDepartmentId === departmentUser?.id) || (item?.handoverDepartmentId === departmentUser?.id);
  const isDelete = (hasDeletePermission || item?.createdByDepartment === departmentUser?.id) &&
    (
      [
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
      ].includes(item?.allocationStatusIndex)
    );
  return (
    <div className="none_wrap">
      {isEdit && (
        <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.edit)}
          >
            <Icon fontSize="small" color="primary">
              edit
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {isWaitForConfirmation && isReceiverDepartment && (
        <LightTooltip title={t("InstrumentToolsTransfer.hoverCheck")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.check)}
          >
            <ArrowForwardOutlinedIcon className="iconArrow" />
          </IconButton>
        </LightTooltip>
      )}
      {isDelete && (
        <LightTooltip
          title={t("general.deleteIcon")}
          {...DEFAULT_TOOLTIPS_PROPS}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.delete)}
          >
            <Icon fontSize="small" color="error">
              delete
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip title={t("general.viewIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
      {hasPrintPermission && (
        <LightTooltip title={t("In phiếu")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      {appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder === item?.allocationStatusIndex &&
        <LightTooltip title={t("general.qrIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class AssetAllocationTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: appConst.rowsPerPageOptions.popup[0],
    page: 0,
    AssetAllocation: [],
    item: {},
    shouldOpenPrint: false,
    shouldOpenEditorDialog: false,
    shouldOpenViewDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    listAssetDocumentId: [],
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasVisibilityPermission: false,
    hasCreatePermission: false,
    hasSuperAccess: false,
    isHideButton: false,
    obj: {},
    tabValue: 0,
    rowDataAssetVouchersTable: [],
    isCheckReceiverDP: true,
    statusIndex: null,
    openAdvanceSearch: false,
    listHandoverDepartment: [],
    handoverDepartment: null,
    listReceiverDepartment: [],
    receiverDepartment: null,
    fromDate: null,
    toDate: null,
    allocationStatus: null,
    products: [],
    openPrintQR: false,
    selectedRow: null,
    isDisable: null,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Allocation; //cấp phát

  handleChangeTabValue = (event, newValue) => {
    let status = {
      [appConst?.tabAllocation?.tabAll]: null,
      [appConst?.tabAllocation?.tabNew]: appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
      [appConst?.tabAllocation?.tabWaitConfirmation]: appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
      [appConst?.tabAllocation?.tabAllocated]: appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
      [appConst?.tabAllocation?.tabReturn]: appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
    }
    let allocationStatus = appConst.listStatusAllocation.find(item => item.indexOrder === status[newValue]);
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      allocationStatus,
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
      statusIndex: status[newValue],
      isDisable: allocationStatus ?? null,
    }, this.updatePageData);
  };

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page: page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = () => {
    this.setPage(0);
  };

  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  updatePageData = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let {
      toDate,
      fromDate,
      statusIndex,
      allocationStatus,
      handoverDepartment,
      receiverDepartment,
    } = this.state;
    let searchObject = {};
    searchObject.keyword = encodeWithVariableCharacters(this.state.keyword?.trim(), appConst.INVALID_END_PARAMS);
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.statusIndex = statusIndex;
    if (handoverDepartment) {
      searchObject.handoverDepartmentId = handoverDepartment?.id;
    }
    if (receiverDepartment) {
      searchObject.receiverDepartmentId = receiverDepartment?.id;
    }
    if (allocationStatus && !statusIndex) {
      searchObject.statusIndex = allocationStatus?.indexOrder;
    }
    if (fromDate) {
      searchObject.issueDateBottom = convertFromToDate(fromDate).fromDate;
    }
    if (toDate) {
      searchObject.issueDateTop = convertFromToDate(toDate).toDate;
    }
    try {
      setPageLoading(true);
      const res = await searchByPage(searchObject);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          itemList: data?.content?.length ? [...data?.content] : [],
          totalElements: data?.totalElements,
          rowDataAssetVouchersTable: [],
          selectedRow: null
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };
  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountStatus()
      .then(({ data }) => {
        let countStatusNew,
          countStatusAllocated,
          countStatusReturn,
          countStatusWaitConfirmation;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (appConst?.STATUS_ALLOCATION.MOI_TAO.indexOrder === item?.status) {
            countStatusNew = this.checkCount(item?.count);
            return;
          }
          if (appConst?.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder === item?.status) {
            countStatusAllocated = this.checkCount(item?.count);
            return;
          }
          if (appConst?.STATUS_ALLOCATION.TRA_VE.indexOrder === item?.status) {
            countStatusReturn = this.checkCount(item?.count);
            return;
          }
          if (appConst?.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder === item?.status) {
            countStatusWaitConfirmation = this.checkCount(item?.count);
            return;
          }
        });
        this.setState(
          {
            countStatusNew,
            countStatusAllocated,
            countStatusReturn,
            countStatusWaitConfirmation,
          },
          () => setPageLoading(false)
        );
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenPrint: false,
      item: false,
      isVisibility: false,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      item: false,
      isVisibility: false,
    }, () => {
      this.updatePageData();
      this.getCountStatus();
    });
  };

  handleConfirmationResponse = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    deleteInstrumentTool(this.state.id)
      .then(({ data }) => {
        setPageLoading(false);
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.info(data?.message);
          if (this.state?.itemList.length - 1 === 0 && this.state.page !== 0) {
            this.setState(
              {
                page: this.state.page - 1,
              },
              () => this.updatePageData()
            );
          } else {
            this.updatePageData();
          }
          this.handleDialogClose();
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.error(t("toastr.error"));
        setPageLoading(false);
      });
  };

  componentDidMount() {
    let { location, history } = this.props;

    if (location?.state?.id) {
      switch (location?.state?.status) {
        case appConst.STATUS_REQUIREMENTS.PROCESSED:
          this.handleView({
            id: location?.state?.id
          })
          break;
        case appConst.STATUS_REQUIREMENTS.NOT_PROCESSED_YET:
          this.handleApprove({
            id: location?.state?.id
          })
          break;
        default:
          break;
      }
      history.push(location?.state?.path, null);
    }

    this.setState({
      statusIndex: null,
    }, this.updatePageData
    );
    this.getRoleCurrentUser();
    this.getCountStatus();
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasVisibilityPermission,
      hasCreatePermission,
      hasPrintPermission,
      hasSuperAccess,
    } = this.state;
    let roles = getTheHighestRole();
    let {
      isRoleAssetUser,
      isRoleAssetManager,
      isRoleAccountant,
      isRoleOrgAdmin,
    } = roles;

    hasDeletePermission = isRoleAssetManager || isRoleOrgAdmin;
    hasEditPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAssetUser;
    hasCreatePermission = isRoleOrgAdmin || isRoleAssetManager;
    hasVisibilityPermission = isRoleOrgAdmin || isRoleAssetManager || isRoleAccountant || isRoleAssetUser;
    hasSuperAccess = isRoleOrgAdmin;
    hasPrintPermission = isRoleOrgAdmin || isRoleAccountant || isRoleAssetManager;

    this.setState({
      ...roles,
      hasDeletePermission,
      hasEditPermission,
      hasVisibilityPermission,
      hasCreatePermission,
      hasPrintPermission,
      hasSuperAccess,
    });
  };

  handleEditItem = (item) => {
    this.setState({
      item: item,
      shouldOpenEditorDialog: true,
      isVisibility: false,
      isStatus: false,
      isHideButton: true,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    for (var i = 0; i < list.length; i++) {
      await deleteItem(list[i].id);
    }
  }

  handleDeleteAll = (event) => {
    let { t } = this.props;
    this.handleDeleteList(this.data)
      .then(() => {
        this.updatePageData();
        this.handleDialogClose();
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      });
  };

  /* Export to excel */

  exportToExcel = async () => {
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const {
      fromDate,
      toDate,
      currentUser,
      allocationStatus,
      handoverDepartment,
      receiverDepartment,
    } = this.state;

    try {
      setPageLoading(true);
      let searchObject = {
        orgId: currentUser?.org?.id,
        assetClass: appConst.assetClass.CCDC,
      };

      searchObject.keyword = this.state?.keyword?.trim();

      if (handoverDepartment) {
        searchObject.handoverDepartmentId = handoverDepartment?.id;
      }
      if (receiverDepartment) {
        searchObject.receiverDepartmentId = receiverDepartment?.id;
      }
      if (allocationStatus) {
        searchObject.statusIndex = allocationStatus?.indexOrder;
      }
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }

      const res = await exportToExcel(searchObject);

      if (appConst.CODE.SUCCESS === res?.status) {
        const blob = new Blob([res?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });

        FileSaver.saveAs(blob, "AssetAllocation.xlsx");
        toast.success(t("general.successExport"));
      }
    } catch (error) {
      toast.error(t("general.failExport"));
    } finally {
      setPageLoading(false);
    }
  };

  setStateAllocation = (item) => {
    this.setState(item);
  };

  convertDataDto = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let { data } = await getItemById(rowData.id);
    const dataSign = await getSignature(rowData?.id);
    setPageLoading(false);
    let listAssetDocumentId = [];
    let allocation = data?.data ? data?.data : {};

    if (isSuccessfulResponse(dataSign?.data?.code)) {
      allocation = { ...dataSign?.data?.data, ...allocation }
    }

    allocation.createDate = formatDateTypeArray(allocation?.createDate);
    allocation.allocationStatus = {
      id: allocation?.allocationStatusId,
      indexOrder: allocation?.allocationStatusIndex,
    };

    allocation.handoverDepartment = {
      id: allocation?.handoverDepartmentId,
      code: allocation?.handoverDepartmentCode,
      name: allocation?.handoverDepartmentName,
    };

    allocation.receiverDepartment = {
      id: allocation?.receiverDepartmentId,
      code: allocation?.receiverDepartmentCode,
      name: allocation?.receiverDepartmentName,
      text:
        allocation?.receiverDepartmentName +
        " - " +
        allocation?.receiverDepartmentCode,
    };

    allocation.assetVouchers.map((item) => {
      let data = {
        id: item?.assetId,
        name: item?.assetName,
        code: item?.assetCode,
        yearPutIntoUse: item?.assetYearPutIntoUse || item?.assetYearPutIntoUser,
        managementCode: item?.assetManagementCode,
        carryingAmount: item?.assetCarryingAmount,
        dateOfReception: item?.assetDateOfReception,
        madeIn: item?.assetMadeIn,
        originalCost: Math.ceil(item?.assetOriginalCost),
        yearOfManufacture: item?.assetYearOfManufacture,
        unitPrice: Math.ceil(item?.assetOriginalCost / item?.quantity),
      };

      item.usePerson = {
        id: item?.usePersonId,
        displayName: item?.usePersonDisplayName,
      };

      item.receiveDepartmentName = allocation?.receiverDepartmentName

      item.receiverDepartment = {
        id: item?.assetUseDepartmentId,
        code: item?.assetUseDepartmentCode,
        name: item?.assetUseDepartmentName,
      };
      item.asset = data;
      return item;
    });

    allocation?.documents?.map((item) => listAssetDocumentId?.push(item?.id));
    allocation.listAssetDocumentId = listAssetDocumentId;
    return allocation;
  };
  handleApprove = async (rowData) => {
    let assetAllocation = await this.convertDataDto(rowData);
    assetAllocation.isCheckHandoverDP = false;
    assetAllocation.isCheckReceiverDP = false;
    assetAllocation.isConfirm = true;
    this.setState({
      item: assetAllocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };
  handleEdit = async (rowData) => {
    let allocation = await this.convertDataDto(rowData);
    allocation.isCheckReceiverDP = false;

    let status =
      allocation?.allocationStatusIndex ===
      appConst?.STATUS_ALLOCATION.MOI_TAO.indexOrder;
    if (status) {
      this.setState({
        item: allocation,
        shouldOpenEditorDialog: true,
        isVisibility: false,
        isStatus: false,
        isHideButton: false,
      });
      return;
    }
    this.setState({
      item: allocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: true,
      products: allocation?.allocationStatusIndex ===
        appConst?.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder ? allocation?.assetVouchers : [],
    });
  };

  handleView = async (rowData) => {
    let allocation = await this.convertDataDto(rowData);
    this.setState({
      item: allocation,
      shouldOpenEditorDialog: true,
      isVisibility: true,
      isStatus: false,
    });
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusAllocation.find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  handleGetRowData = (rowData) => {
    this.setState({
      selectedRow: rowData?.id,
      rowDataAssetVouchersTable: rowData?.assetVouchers ?? [],
    });
  };

  handlePrintAll = async (rowData) => {
    let { currentOrg } = this.context;
    let isShowPrintPreview = [
      LIST_ORGANIZATION.BVDK_BA_VI.code,
      LIST_ORGANIZATION.BVDK_MY_DUC.code,
    ].includes(currentOrg?.code);
    let assetAllocation = null;

    if (isShowPrintPreview) {
      assetAllocation = rowData;
    } else {
      assetAllocation = await this.convertDataDto(rowData);
    }
    this.setState({
      item: assetAllocation,
      shouldOpenPrint: true,
    });
  };
  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };
  handleSetDataSelect = (data, name, source) => {
    if (name === "fromDate" || name === "toDate") {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      if (
        variable.listInputName.listData !== source
        // && !this.state.statusIndex
      ) {
        this.search();
      }
    });
  };
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }
  handleQrCode = async (newValue) => {
    let assetAllocation = await this.convertDataDto(newValue) || []

    let productsUpdate = assetAllocation?.assetVouchers?.map(item => {
      item.qrInfo = `Mã TS: ${item?.asset?.code || ""}\n` +
        `Mã QL: ${item?.asset?.managementCode || ""}\n` +
        `Tên TS: ${item?.asset?.name || ""}\n` +
        `Nước SX: ${item?.asset?.madeIn || ""}\n` +
        `Năm SX: ${item?.asset?.yearOfManufacture || ""}\n` +
        `Năm SD: ${item?.asset?.yearPutIntoUse || ""}\n` +
        `PBSD: ${item?.receiveDepartmentName || ""}\n`
      return item;
    })

    this.setState({
      products: productsUpdate,
      openPrintQR: true,
    })
  }

  importExcel = () => {
    this.setState({
      shouldOpenImportExcelDialog: true,
    });
  };

  exportExampleImportExcel = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;

    try {
      setPageLoading(true);
      await functionExportToExcel(
        exportExampleImportExcel,
        {},
        t("exportToExcel.allocatedAssetImportExample")
      )
    } catch (e) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    }
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      hasVisibilityPermission,
      hasSuperAccess,
      hasPrintPermission,
      tabValue,
      rowDataAssetVouchersTable,
    } = this.state;
    let TitlePage = t("Asset.instruments_tools_allocation");
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        maxWidth: 120,
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            hasDeletePermission={hasDeletePermission}
            hasEditPermission={hasEditPermission}
            hasVisibilityPermission={hasVisibilityPermission}
            hasSuperAccess={hasSuperAccess}
            hasPrintPermission={hasPrintPermission}
            item={rowData}
            value={tabValue}
            onSelect={(rowData, method) => {
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.print === method) {
                this.handlePrintAll(rowData);
              } else if (appConst.active.view === method) {
                this.handleView(rowData);
              } else if (appConst.active.check === method) {
                this.handleApprove(rowData);
              } else if (appConst.active.qrcode === method) {
                this.handleQrCode(rowData);
              }
              else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        align: "left",
        maxWidth: 50,
        minWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("AssetTransfer.issueDate"),
        field: "issueDate",
        align: "left",
        maxWidth: 120,
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.issueDate ? (
            <span>{moment(rowData.issueDate).format("DD/MM/YYYY")}</span>
          ) : (
            ""
          ),
      },
      {
        title: t("allocation_asset.status"),
        field: "allocationStatusIndexOrder",
        align: "left",
        maxWidth: 140,
        minWidth: 140,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          const statusIndex = rowData?.allocationStatusIndex;
          let className = "";
          if (statusIndex === appConst.STATUS_ALLOCATION.TRA_VE.indexOrder) {
            className = "status status-error";
          } else if (
            statusIndex === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder
          ) {
            className = "status status-info";
          } else if (
            statusIndex === appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder
          ) {
            className = "status status-success";
          } else if (
            statusIndex === appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder
          ) {
            className = "status status-warning";
          } else {
            className = "";
          }
          return (
            <span className={className}>{this.checkStatus(statusIndex)}</span>
          );
        },
      },
      {
        title: t("allocation_asset.handoverDepartment"),
        field: "handoverDepartmentName",
        align: "left",
        minWidth: 180,
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPersonName",
        align: "left",
        minWidth: 180,
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartmentName",
        align: "left",
        minWidth: 180,
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        align: "left",
        minWidth: 180,
      },
    ];

    let columnsNoAction = [
      {
        title: t("general.index"),
        field: "",
        width: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("allocation_asset.status"),
        field: "allocationStatus.name",
        width: "150",
      },
      {
        title: t("allocation_asset.handoverPerson"),
        field: "handoverPerson.displayName",
        width: "150",
      },
      {
        title: t("allocation_asset.receiverDepartment"),
        field: "receiverDepartment.name",
        width: "150",
      },
      {
        title: t("allocation_asset.receiverPerson"),
        field: "receiverPersonName",
        width: "150",
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: "50px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("Asset.code"),
        field: "assetCode",
        minWidth: 120,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "assetManagementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "assetName",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
      },
      {
        title: t("Asset.serialNumber"),
        field: "assetSerialNumber",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("Asset.model"),
        field: "assetModel",
        align: "left",
        minWidth: 120,
      },
      {
        title: t("InstrumentToolsList.dateOfReception"),
        field: "asset.dateOfReception",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          return moment(rowData?.assetDateOfReception).format('DD-MM-YYYY')
        }
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "assetYearOfManufacture",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
      },
      {
        title: t("InstrumentToolsType.storeName"),
        field: "storeName",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
      },
      {
        title: t("InstrumentToolsType.quantity"),
        field: "quantity",
        align: "left",
        minWidth: 100,
        maxWidth: 200,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.receiverPerson"),
        field: "usePersonDisplayName",
        minWidth: 230,
        align: "left",
      },
      {
        title: t("InstrumentToolsList.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => rowData?.assetOriginalCost
          ? convertNumberPriceRoundUp(rowData?.assetOriginalCost)
          : 0
      },
      {
        title: t("Asset.note"),
        field: "note",
        align: "left",
        minWidth: 180,
      },
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.toolsManagement"),
                path: "instruments-and-tools/allocation-vouchers",
              },
              { name: t("InstrumentToolsAllocation.allocation") },
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
            className="tabsStatus"
          >
            <Tab className="tab" label={t("allocation_asset.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabNew")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusNew || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabWaitConfirmation")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusWaitConfirmation || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabAllocated")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusAllocated || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("allocation_asset.tabReturn")}</span>
                  <div className="tabQuantity tabQuantity-error">
                    {this.state?.countStatusReturn || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel
          value={tabValue}
          index={appConst.tabAllocation.tabAll}
          className="mp-0"
        >
          <ComponentAllocationTable
            openAdvanceSearch={this.state.openAdvanceSearch}
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            getRowData={this.handleGetRowData}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabAllocation.tabNew}
          className="mp-0"
        >
          <ComponentAllocationTable
            openAdvanceSearch={this.state.openAdvanceSearch}
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            getRowData={this.handleGetRowData}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst?.tabAllocation?.tabWaitConfirmation}
          className="mp-0"
        >
          <ComponentAllocationTable
            openAdvanceSearch={this.state.openAdvanceSearch}
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleExportToExcel={this.handleExportToExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            getRowData={this.handleGetRowData}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabAllocation.tabAllocated}
          className="mp-0"
        >
          <ComponentAllocationTable
            openAdvanceSearch={this.state.openAdvanceSearch}
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            getRowData={this.handleGetRowData}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handleQrCode={this.handleQrCode}
            handleCloseQrCode={this.handleCloseQrCode}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={appConst.tabAllocation.tabReturn}
          className="mp-0"
        >
          <ComponentAllocationTable
            openAdvanceSearch={this.state.openAdvanceSearch}
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            handleDeleteAll={this.handleDeleteAll}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateAllocation}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            columnsNoAction={columnsNoAction}
            getRowData={this.handleGetRowData}
            importExcel={this.importExcel}
            exportExampleImportExcel={this.exportExampleImportExcel}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
          />
        </TabPanel>
        <div>
          <MaterialTable
            data={rowDataAssetVouchersTable ?? []}
            // data={props.item.assetVouchers ? props.item.assetVouchers : []}
            columns={columnsSubTable}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </div>
      </div>
    );
  }
}
AssetAllocationTable.contextType = AppContext;
export default AssetAllocationTable;
