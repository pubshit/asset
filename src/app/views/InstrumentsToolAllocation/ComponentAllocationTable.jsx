import React, { useContext } from "react";
import {
  Button,
  Card,
  Collapse,
  FormControl,
  Grid,
  Input,
  InputAdornment,
  TablePagination,
  TextField,
} from "@material-ui/core";
import { ConfirmationDialog } from "egret";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL, variable } from "app/appConst";
import AllocationEditerDialog from "./AllocationEditerDialog";
import { ValidatorForm } from "react-material-ui-form-validator";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {
  exportExcelFileError,
  getListManagementDepartment,
  importExcelAllocatedIatURL,
  searchReceiverDepartment
} from "./InstrumentsToolsAllocationService";
import { convertNumberPriceRoundUp, filterOptions, getTheHighestRole, getUserInformation } from "app/appFunction";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import viLocale from "date-fns/locale/vi";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import { Autocomplete } from "@material-ui/lab";
import AssetsQRPrint from "../Asset/ComponentPopups/AssetsQRPrint";
import ImportExcelDialog from "../Component/ImportExcel/ImportExcelDialog";
import CardContent from "@material-ui/core/CardContent";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import CustomTablePagination from "../CustomTablePagination";

function ComponentAllocationTable(props) {
  const { t, getRowData, i18n } = props;
  const {
    item,
    shouldOpenPrint,
    listHandoverDepartment,
    handoverDepartment,
    listReceiverDepartment,
    receiverDepartment,
    fromDate,
    toDate,
    allocationStatus,
    statusIndex,
    openPrintQR,
    products,
    shouldOpenImportExcelDialog,
    isRoleAssetManager,
    isRoleOrgAdmin,
    selectedRow,
    isDisable
  } = props?.item;
  const { currentOrg } = useContext(AppContext);
  const { ALLOCATION } = LIST_PRINT_FORM_BY_ORG.IAT_MANAGEMENT;
  const searchObject = { ...appConst.OBJECT_SEARCH_MAX_SIZE, };
  const importUrl = importExcelAllocatedIatURL();
  const isShowPrintPreview = [
    LIST_ORGANIZATION.BVDK_BA_VI.code,
    LIST_ORGANIZATION.BVDK_MY_DUC.code,
  ].includes(currentOrg?.code);

  let roles = getTheHighestRole();

  let date = new Date(item?.issueDate);

  const { addressOfEnterprise } = getUserInformation();
  const day = String(date?.getDate())?.padStart(2, '0');
  const month = String(date?.getMonth() + 1)?.padStart(2, '0');
  const year = String(date?.getFullYear());

  let countItem = item?.assetVouchers?.reduce(
    (value, item) => value + Number(item?.quantity || 1), 0
  );
  let sumTotalCost = item?.assetVouchers?.reduce(
    (value, item) => value + Number(item?.assetOriginalCost || 0), 0
  );

  let dataView = {
    ...roles,
    addressOfEnterprise,
    day: item?.issueDate ? day : " ..... ",
    month: item?.issueDate ? month : " ..... ",
    year: item?.issueDate ? year : " ..... ",
    dataPrint: {
      ...item,
      assetVouchers: item?.assetVouchers?.map((i, index) => {
        i.index = index + 1;
        i.quantity = i.quantity || 1;
        return { ...i, assetOriginalCost: convertNumberPriceRoundUp(i.assetOriginalCost || 0) }
      })
    },
    countItem,
    sumTotalCost: convertNumberPriceRoundUp(sumTotalCost || 0)
  }

  return (
    <>
      <Grid container spacing={2} justifyContent="space-between" className="mt-10">
        <Grid item md={6} xs={12}>
          {props.item?.hasCreatePermission && (
            <Button
              className="mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                props.handleEditItem({
                  startDate: new Date(),
                  endDate: new Date(),
                  isCheckReceiverDP: true,
                });
              }}
            >
              {t("InstrumentToolsAllocation.allocation")}
            </Button>
          )}
          {(isRoleAssetManager || isRoleOrgAdmin) && (
            <Button
              className="align-bottom mr-16"
              variant="contained"
              color="primary"
              onClick={props.importExcel}
            >
              {t("general.importExcel")}
            </Button>
          )}
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={() => {
              props.exportToExcel();
            }}
          >
            {t("general.exportToExcel")}
          </Button>
          <Button
            className="mr-16 align-bottom"
            variant="contained"
            color="primary"
            onClick={props.handleOpenAdvanceSearch}
          >
            {t("general.advancedSearch")}
            <ArrowDropDownIcon />
          </Button>
          {props.item?.shouldOpenConfirmationDeleteAllDialog && (
            <ConfirmationDialog
              open={props.item?.shouldOpenConfirmationDeleteAllDialog}
              onConfirmDialogClose={props?.handleDialogClose}
              onYesClick={props?.handleDeleteAll}
              text={t("general.deleteAllConfirm")}
            />
          )}
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <FormControl fullWidth>
            <Input
              className="search_box w-100"
              onChange={props.handleTextChange}
              onKeyDown={props.handleKeyDownEnterSearch}
              onKeyUp={props.handleKeyUp}
              placeholder={t("InstrumentToolsType.enterSearch")}
              id="search_box"
              endAdornment={
                <InputAdornment position="end">
                  <SearchIcon onClick={props.search} />
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <Collapse in={props?.openAdvanceSearch}>
            <ValidatorForm onSubmit={() => { }}>
              <Card elevation={2}>
                <CardContent>
                  <Grid container spacing={2}>
                    {/* Phòng bàn giao */}
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteSub
                        label={t("allocation_asset.handoverDepartment")}
                        searchFunction={getListManagementDepartment}
                        searchObject={searchObject}
                        listData={listHandoverDepartment || []}
                        typeReturnFunction="list"
                        setListData={(data) => props?.handleSetDataSelect(
                          data,
                          "listHandoverDepartment",
                          variable.listInputName.listData
                        )}
                        isNoRenderChildren
                        isNoRenderParent
                        displayLable={"text"}
                        value={handoverDepartment || null}
                        name="handoverDepartment"
                        onSelect={props?.handleSetDataSelect}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    {/* Phòng tiếp nhận */}
                    <Grid item xs={12} sm={12} md={3}>
                      <AsynchronousAutocompleteSub
                        className="w-100"
                        label={t("allocation_asset.receiverDepartment")}
                        searchFunction={searchReceiverDepartment}
                        searchObject={searchObject}
                        listData={listReceiverDepartment || []}
                        setListData={(data) => props?.handleSetDataSelect(
                          data,
                          "listReceiverDepartment",
                          variable.listInputName.listData
                        )}
                        displayLable={"text"}
                        value={receiverDepartment || null}
                        name="receiverDepartment"
                        onSelect={props?.handleSetDataSelect}
                        filterOptions={filterOptions}
                        noOptionsText={t("general.noOption")}
                      />
                    </Grid>
                    {/* Từ ngày */}
                    <Grid item xs={12} sm={12} md={3}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("MaintainPlaning.dxFrom")}
                          format="dd/MM/yyyy"
                          maxDate={toDate ? new Date(toDate) : undefined}
                          value={fromDate ?? null}
                          onChange={(data) =>
                            props.handleSetDataSelect(data, "fromDate")
                          }
                          KeyboardButtonProps={{ "aria-label": "change date" }}
                          minDateMessage={t("general.minDateMessage")}
                          maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    {/* Đến ngày */}
                    <Grid item xs={12} sm={12} md={3}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                        <KeyboardDatePicker
                          margin="none"
                          fullWidth
                          autoOk
                          id="date-picker-dialog"
                          label={t("MaintainPlaning.dxTo")}
                          format="dd/MM/yyyy"
                          minDate={fromDate ? new Date(fromDate) : undefined}
                          value={toDate ?? null}
                          onChange={(data) =>
                            props.handleSetDataSelect(data, "toDate")
                          }
                          KeyboardButtonProps={{ "aria-label": "change date" }}
                          minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                          maxDateMessage={t("general.maxDateMessage")}
                          invalidDateMessage={t("general.invalidDateFormat")}
                          clearable
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    {/* Trạng thái */}
                    <Grid item xs={12} sm={12} md={3}>
                      <Autocomplete
                        fullWidth
                        options={appConst.listStatusAllocation}
                        defaultValue={allocationStatus ? allocationStatus : null}
                        value={allocationStatus ? allocationStatus : null}
                        onChange={(e, value) =>
                          props.handleSetDataSelect(value, "allocationStatus")
                        }
                        filterOptions={(options, params) => {
                          params.inputValue = params.inputValue.trim();
                          return filterOptions(options, params);
                        }}
                        disabled={isDisable}
                        getOptionLabel={(option) => option.name}
                        noOptionsText={t("general.noOption")}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            value={allocationStatus?.name || ""}
                            label={t("maintainRequest.status")}
                          />
                        )}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </ValidatorForm>
          </Collapse>
        </Grid>


        <Grid item xs={12}>
          <div>
            {props?.item?.shouldOpenEditorDialog && (
              <AllocationEditerDialog
                t={t}
                i18n={props.item?.i18n}
                handleClose={props?.handleDialogClose}
                open={props?.item?.shouldOpenEditorDialog}
                handleOKEditClose={props?.handleOKEditClose}
                item={props?.item?.item}
                isVisibility={props.item?.isVisibility}
                isStatus={props.item?.isStatus}
                isAllocation={false}
                isHideButton={props.item?.isHideButton}
                handleQrCode={props.handleQrCode}
              />
            )}

            {shouldOpenPrint && (
              isShowPrintPreview ? (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={props.handleDialogClose}
                  open={shouldOpenPrint}
                  item={item}
                  title={t("allocation_asset.allocation_assetPrint")}
                  model={PRINT_TEMPLATE_MODEL.IAT_MANAGEMENT.ALLOCATION}
                />
              ) : (
                <PrintMultipleFormDialog
                  t={t}
                  i18n={i18n}
                  handleClose={props.handleDialogClose}
                  open={shouldOpenPrint}
                  item={dataView}
                  title={t("allocation_asset.allocation_assetPrint")}
                  urls={[
                    ...ALLOCATION.GENERAL,
                    ...(ALLOCATION[currentOrg?.printCode] || []),
                  ]}
                />
              ))}

            {props?.item?.shouldOpenConfirmationDialog && (
              <ConfirmationDialog
                title={t("general.confirm")}
                open={props.item?.shouldOpenConfirmationDialog}
                onConfirmDialogClose={props?.handleDialogClose}
                onYesClick={props?.handleConfirmationResponse}
                text={t("general.cancel_asset_allocation")}
                agree={t("general.agree")}
                cancel={t("general.cancel")}
              />
            )}

            {openPrintQR && (
              <AssetsQRPrint
                t={t}
                i18n={i18n}
                handleClose={props.handleCloseQrCode}
                open={openPrintQR}
                items={products}
              />
            )}

            {shouldOpenImportExcelDialog && (
              <ImportExcelDialog
                t={t}
                i18n={i18n}
                handleClose={props.handleDialogClose}
                open={shouldOpenImportExcelDialog}
                handleOKEditClose={props.handleOKEditClose}
                exportExampleImportExcel={props.exportExampleImportExcel}
                exportFileError={exportExcelFileError}
                url={importUrl}
              />
            )}
          </div>
          <MaterialTable
            title={t("general.list")}
            data={props.item?.itemList}
            onRowClick={(e, rowData) => {
              getRowData(rowData);
            }}
            columns={
              props.item?.hasDeletePermission ||
                props.item?.hasEditPermission ||
                props.item?.hasVisibilityPermission ||
                props.item?.hasSuperAccess
                ? props?.columns
                : props?.columnsNoAction
            }
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              draggable: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  selectedRow === rowData.id
                    ? "#ccc"
                    : rowData.tableData.id % 2 === 1
                      ? "#EEE"
                      : "#FFF",
              }),
              maxBodyHeight: "450px",
              minBodyHeight: "250px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
          <CustomTablePagination
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            labelDisplayedRows={({ from, to, count }) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`}`
            }
            count={props.item?.totalElements}
            rowsPerPage={props.item?.rowsPerPage}
            page={props.item?.page}
            onPageChange={props.handleChangePage}
            onRowsPerPageChange={props.setRowsPerPage}
          />
        </Grid>
      </Grid>
    </>
  );
}

export default ComponentAllocationTable;
