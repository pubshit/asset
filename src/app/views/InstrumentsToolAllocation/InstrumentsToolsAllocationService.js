import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH =
  ConstantList.API_ENPOINT + "/api/commonobject" + ConstantList.URL_PREFIX;
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_PERSON = ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_ASSET = ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_ALLOCATION = ConstantList.API_ENPOINT + "/api/v1/instruments-and-tools/allocation-vouchers";
const API_PATH_GETLIST = ConstantList.API_ENPOINT + "/api/instruments-and-tools/page/";
const API_PATH_NEWCODE = ConstantList.API_ENPOINT + "/api/asset_document/instruments-and-tools/new-code";
const API_PATH_DOCUMENT = ConstantList.API_ENPOINT + "/api/asset_document/instruments-and-tools/searchByPage/";
const API_PATH_EXPORT_TO_EXCEL = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_TEMPLATE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

export const searchByPage = (searchObject) => {
  let url = API_PATH_ALLOCATION + `/page`;
  let config = {
    params: searchObject
  };
  return axios.get(url, config);
};

export const saveItem = (item) => {
  return axios.post(API_PATH + "/htms", item);
};
export const updateItem = (item) => {
  return axios.put(API_PATH + "/htms/" + item.id, item);
};
export const deleteItem = (id) => {
  let url = API_PATH + "/htms/" + id;
  return axios.delete(url);
};

export const listInstrumentTool = (searchObject) => {
  let url = API_PATH_GETLIST + searchObject.pageIndex + "/" + searchObject.pageSize;
  return axios.post(url, searchObject);
};

export const getListManagementDepartment = () => {
  return axios.get(API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",{});
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_PERSON + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const getListReceiveDepartment = (searchObject) => {
  var url = API_PATH_ASSET + "/searchParentByPage";
  return axios.post(url, searchObject);
};

export const createAllocation = (data) =>{
  return axios.post(API_PATH_ALLOCATION, data);
}

export const updateAllocation = (id, data) => {
  let url = API_PATH_ALLOCATION + "/" + id;
  return axios.put(url, data);
}

export const addAllocation = allocation => {
    return axios.post(API_PATH_ALLOCATION, allocation );
};

export const updateToReturn = (id, data, status) => {
  return axios.put(API_PATH_ALLOCATION + "/" + id + "/status/" + status, data)
}

export const getItemById = (asset) => {
  return axios.get(API_PATH_ALLOCATION + "/" + asset, {});
}

export const deleteInstrumentTool = (id) => {
  let url = API_PATH_ALLOCATION + "/" + id;
  return axios.delete(url);
};

export const searchReceiverPerson = (searchObject) => {
  var url = API_PATH_PERSON + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const searchReceiverDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage",{isActive:STATUS_DEPARTMENT.HOAT_DONG.code,...searchObject});
};

export const getNewCode = () => {
  var url = API_PATH_NEWCODE;
  return axios.get(url, {});
}

export const searchByPageAssetDocument = (searchObject) => {
  var url = API_PATH_DOCUMENT + searchObject.pageIndex + "/" + searchObject.pageSize;
  return axios.post(url, searchObject)
}

export const getCountStatus = () => {
  let url = API_PATH_ALLOCATION + "/count-by-statuses" ;
  return axios.get(url)
}

export const exportToExcel = (searchObject) => {
  return axios({
    method: 'post',
    url: API_PATH_EXPORT_TO_EXCEL + "/cap-phat-cong-cu-dung-cu",
    data: searchObject,
    responseType: 'blob',
  })
};

export const exportExampleImportExcel = () => {
  return axios({
    method: 'get',
    url: API_PATH_TEMPLATE + "/import-allocated-iat",
    // data: searchObject,
    responseType: 'blob',
  })
};

export const importExcelAllocatedIatURL = () => {
  return ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/instruments-and-tools/allocation-vouchers/import";
};

export const exportExcelFileError = (linkFile) => {
  return axios({
    method: "get",
    url: ConstantList.API_ENPOINT_ASSET_MAINTANE + linkFile,
    responseType: "blob",
  });
};
