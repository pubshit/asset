import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/staff" + ConstantList.URL_PREFIX;

export const searchByPage = (searchObject) => {
  var url = API_PATH + "/searchByPage";
  return axios.post(url, searchObject);
};

export const getItemById = (id) => {
  return axios.get(API_PATH + "/" + id);
};
export const deleteItem = (id) => {
  return axios.delete(API_PATH + "/" + id);
};
export const addItem = (item) => {
  return axios.post(API_PATH, item);
};

export const updateItem = (id, item) => {
  return axios.put(API_PATH + "/" + id, item);
};
export const checkCode = (id, staffCode) => {
  const config = { params: { id: id, staffCode: staffCode } };
  var url = API_PATH + "/checkCode";
  return axios.get(url, config);
};
export const getNewCode = () => {
  return axios.get(API_PATH + "/addNew/getNewCode");
};
export const checkEmail = (id, Email) => {
  const config = { params: { id: id, Email: Email } };
  var url = API_PATH + "/checkEmail";
  return axios.get(url, config);
};
// export const deleteCheckItem = id => {
//   return axios.delete(API_PATH  + "/delete/" + id);
// };
