import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid, DialogActions
} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { getAllProductCategorys, addItem, searchByPage,checkCode,updateItem,checkEmail} from "./StaffService";
import { searchByPage as productSearchByPage } from "../Product/ProductService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DateTimePicker, KeyboardDatePicker } from "@material-ui/pickers";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchReceiverDepartment } from "../AssetTransfer/AssetTransferService";
import { createFilterOptions } from "@material-ui/lab";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit:3
  //etc you get the idea
});

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class StaffEditorDialog extends Component {
  state = {
    name: "",
    staffCode: "",
    firstName:"",
    lastName:"",
    birthDate:new Date(),
    birthPlaceame:"",
    displayName:"",
    phoneNumber:"",
    userName:null,
    email:"",
    gender:"",
    parent: {},
    parentSelect: {},
    confirmPassword:"",
    password:"",
    department:null,
    shouldOpenDepartmentPopup:false,
    ischeck:false,
  };
  departmentSearchObject = {
    pageIndex: 1,
    pageSize: 10000,
    keyword: '',
    checkPermissionUserDepartment: true,
  }
  listGender = [
    { id: 'M', name: 'Nam' },
    { id: 'F', name: 'Nữ' },
    { id: 'U', name: 'Không rõ' },
  ]
  filterAutocomplete = createFilterOptions();
  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { email } = this.state;
    let { staffCode } = this.state;

    checkEmail(id,email).then((result) =>{
      if (result.data) {
        toast.warning('Gmail đã được sử dụng')
        return
      }
      checkCode(id,staffCode).then((result) => {
        if (result.data) {
          toast.warning(this.props.t('CommonObjectType.noti.dupli_code'))
          return
        }

        if (id) {
          updateItem(id,{
            ...this.state
          }).then(() => {
            toast.info('Sửa thông tin nhân viên thành công.')
            this.props.handleOKEditClose();
          });
        } else {
          addItem({
            ...this.state
          }).then(() => {
            toast.info('Thêm nhân viên thành công.')
            this.props.handleOKEditClose();
          });
        }
      });
    })
  };

  selectProduct = (product) => {
    this.setState({ product: product }, function () {
    });
  }
  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };
  componentWillMount() {
    //getUserById(this.props.uid).then(data => this.setState({ ...data.data }));
    let { open, handleClose, item } = this.props;
    if(item?.department) {
      item.department = {
        ...item.department,
        text: item.department.name
      }
    }
    this.setState(item);
  }
componentDidMount (){
  ValidatorForm.addValidationRule('isPhoneNumber', (value) => {
    let phoneRegex = /^(0|\+84)+([0-9]{7,12})$/;
    if ( phoneRegex.test(value) === false) {
      return false
    }
    return true
  })

  // getAllRoles().then(({ data }) => {
  //   this.setState({
  //     listRole: data,
  //   })
  // })
}
handleSelectDepartment = (item) => {
  if(item) {
    this.setState({ department: { id: item?.id, name: item?.text, text: item?.text} }, function () {
      this.handleDepartmentPopupClose();
    });
  }
  else {
    this.setState({department: null})
  }
}
handleDepartmentPopupClose = () => {
  this.setState({
    shouldOpenDepartmentPopup: false
  });
};
  handleDialogClose =()=>{
    this.setState({shouldOpenNotificationPopup:false,})
  }
  render() {
    let {
      id,
      name,
      code,
      product,
      description,
      shouldOpenNotificationPopup,
      staffCode,userName,displayName,
      gender,birthDate,phoneNumber,email,
      shouldOpenDepartmentPopup,department,confirmPassword,
      password
    } = this.state;
    let searchObject = { pageIndex: 0, pageSize: 1000 };
    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm"
      fullWidth={true}>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            // onConfirmDialogClose={this.handleDialogClose}
            onYesClick={this.handleDialogClose}
            text={t(this.state.Notification)}
            agree={t('general.agree')}
          />
        )}
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t('staff.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className=" w-100" container spacing={1}>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={<span><span className="colorRed">*</span>{t('staff.displayName')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="displayName"
                  value={displayName}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                    className=" w-100"
                    label={<span><span className="colorRed">*</span>{t('staff.code')}</span>}

                    onChange={this.handleChange}
                    type="text"
                    name="staffCode"
                    value={staffCode}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                    className=" w-100"
                    label={<span><span className="colorRed">*</span>{t('staff.email')}</span>}
                    onChange={this.handleChange}
                    type="text"
                    name="email"
                    value={email}
                    validators={["required", 'isEmail']}
                    errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={4} xs={12}>
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="gender-simple">
                    {t('staff.gender')}
                  </InputLabel>
                  <Select
                    value={gender}
                    onChange={(gender) => this.handleChange(gender, 'gender')}
                    inputProps={{
                      name: 'gender',
                      id: 'gender-simple',
                    }}
                  >
                    {this.listGender.map((item) => {
                      return (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={4} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog mt-2"
                    // style={{marginTop:'2px'}}
                    label={t('staff.birthDate')}
                    format="dd/MM/yyyy"
                    value={birthDate}
                    onChange={(date) =>
                      this.handleDateChange(date, "birthDate")
                    }
                    KeyboardButtonProps={{
                      'aria-label': 'change date',
                    }}
                    invalidDateMessage={t('general.invalidDateFormat')}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={4} xs={12}>
                <TextValidator
                  className=" w-100"
                  label={<span><span className="colorRed">*</span>{t('staff.phoneNumber')}</span>}
                  onChange={this.handleChange}
                  type="text"
                  name="phoneNumber"
                  value={phoneNumber}
                  validators={['required','minNumber:0', 'isPhoneNumber']}
                  errorMessages={[t('general.required'),t('general.invalidFormat'),t('general.invalidFormat')]}
                />
              </Grid>
              <Grid item sm={12} xs={12}>
                <AsynchronousAutocompleteSub
                    label={
                      <span>
                        <span className="colorRed">* </span>
                        <span> {t("Store.managementDepartment")}</span>
                      </span>
                    }
                    searchFunction={searchReceiverDepartment}
                    searchObject={this.departmentSearchObject}
                    defaultValue={department || null}
                    displayLable={'text'}
                    value={department || null}
                    onSelect={this.handleSelectDepartment}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = this.filterAutocomplete(options, params)
                      return filtered
                    }}
                    noOptionsText={t("general.noOption")}
                  />
              </Grid>
            </Grid>
          </DialogContent>

          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-10">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                className="mr-16"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>              
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default StaffEditorDialog;
