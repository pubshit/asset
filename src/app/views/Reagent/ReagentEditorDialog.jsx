import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkCode, addNewReagent, updateReagent } from "./ReagentService";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

class ReagentEditorDialog extends Component {
  state = {
    name: "",
    code: "",
    description:"",
    registrationNumber:"",//Số đăng ký
    dateOfIssue:new Date(), //Ngày cấp
    expirationDate:new Date(), //Ngày hết hạn
    activeIngredients:"", //Hoạt chất
    dosageForms:"", //Dạng bào chế
    packing:"", //Quy cách đóng gói
    registeredFacilityName:"", //Tên cơ sở đăng ký
    productionFacilityName:"", //Tên cơ sở sản xuất
    isActive: false
  };

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleFormSubmit = () => {
    let { id } = this.state;
    let { code } = this.state;

    checkCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        alert("Code đã được sử dụng");
      } else {
        //Nếu trả về false là code chưa sử dụng có thể dùng
        if (id) {
            updateReagent({
            ...this.state
          }).then(() => {
            this.props.handleOKEditClose();
          });
        } else {
            addNewReagent({
            ...this.state
          }).then(() => {
            this.props.handleOKEditClose();
          });
        }
      }
    });
  };

  componentWillMount() {
    //getUserById(this.props.uid).then(data => this.setState({ ...data.data }));
    let { open, handleClose,item } = this.props;
    this.setState(item);
  }

  render() {
    let {
      id,
      name,
      code,
      registrationNumber,//Số đăng ký
      dateOfIssue, //Ngày cấp
      expirationDate, //Ngày hết hạn
      activeIngredients, //Hoạt chất
      dosageForms, //Dạng bào chế
      packing, //Quy cách đóng gói
      registeredFacilityName, //Tên cơ sở đăng ký
      productionFacilityName, //Tên cơ sở sản xuất
      description
    } = this.state;

    let { open, handleClose, handleOKEditClose, t, i18n } = this.props;
    return (
      <Dialog  open={open}>
        <div className="p-24">
          <h4 className="mb-20">{t("SaveUpdate")}</h4>
          <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
            <Grid className="mb-16" container spacing={4}>
              <Grid item sm={6} xs={6}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Name")}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Code")}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("Description")}
                  onChange={this.handleChange}
                  type="text"
                  name="description"
                  value={description}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.activeIngredients")}
                  onChange={this.handleChange}
                  type="text"
                  name="activeIngredients"
                  value={activeIngredients}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.dosageForms")}
                  onChange={this.handleChange}
                  type="text"
                  name="dosageForms"
                  value={dosageForms}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.packing")}
                  onChange={this.handleChange}
                  type="text"
                  name="packing"
                  value={packing}
                />
              </Grid>
              <Grid item sm={6} xs={6}>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.registrationNumber")}
                  onChange={this.handleChange}
                  type="text"
                  name="registrationNumber"
                  value={registrationNumber}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    className="w-100 mb-16"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("reagent.dateOfIssue")}
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    value={dateOfIssue}
                    onChange={date => this.handleDateChange(date, "dateOfIssue")}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    className="w-100 mb-16"
                    margin="none"
                    id="mui-pickers-date"
                    label={t("reagent.expirationDate")}
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    value={expirationDate}
                    onChange={date => this.handleDateChange(date, "expirationDate")}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                </MuiPickersUtilsProvider>
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.registeredFacilityName")}
                  onChange={this.handleChange}
                  type="text"
                  name="registeredFacilityName"
                  value={registeredFacilityName}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  className="w-100 mb-16"
                  label={t("reagent.productionFacilityName")}
                  onChange={this.handleChange}
                  type="text"
                  name="productionFacilityName"
                  value={productionFacilityName}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
            </Grid>

            <div className="flex flex-space-between flex-middle">
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
              <Button variant="contained" color="secondary" onClick={() => this.props.handleClose()}>Cancel</Button>
            </div>
          </ValidatorForm>
        </div>
      </Dialog>
    );
  }
}

export default ReagentEditorDialog;
