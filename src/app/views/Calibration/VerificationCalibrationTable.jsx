import React from "react";
import AppContext from "app/appContext";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import {AppBar, Grid, Tab, Tabs} from "@material-ui/core";
import {appConst} from "../../appConst";
import {TabPanel} from "../Component/Utilities";
import ComponentTable from "./ComponentTable";
import AcceptantCalibrationTab from "./AcceptantCalibrationTab";

class VerificationCalibrationTable extends React.Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    item: {},
    shouldOpenMainProposal: false,
    tabValue: 0,
    itemList: [],
  };

  handleEditItem = () => {
    this.setState({
      shouldOpenMainProposal: true,
    });
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      tabValue: newValue
    });
  };


  render() {
    const {t} = this.props;
    const {tabValue} = this.state;
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {t("Calibration.list")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/page",
              },
              { name: t("Calibration.list") },
            ]}
          />
        </div>
        <Grid className="mt-12">
          <AppBar position="static" color="default">
            <Tabs
              value={tabValue}
              onChange={this.handleChangeTabValue}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              <Tab key={appConst.tabVerification.pending} label={t("Calibration.pending")} />
              <Tab key={appConst.tabVerification.verificating} label={t("Calibration.calibrating")} />
              <Tab key={appConst.tabVerification.verificated} label={t("Calibration.calibrated")} />
              <Tab key={appConst.tabVerification.acceptance} label={t("Calibration.acceptance")} />
            </Tabs>
          </AppBar>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.pending}
            className="mp-0"
          >
            <ComponentTable tabValue={tabValue} status={appConst.tabVerificationStatus.pending} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.verificating}
            className="mp-0"
          >
            <ComponentTable tabValue={tabValue} status={appConst.tabVerificationStatus.verificating} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.verificated}
            className="mp-0"
          >
            <ComponentTable tabValue={tabValue} status={appConst.tabVerificationStatus.verificated} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.acceptance}
            className="mp-0"
          >
            <AcceptantCalibrationTab tabValue={tabValue} />
          </TabPanel>
        </Grid>
      </div>
    );
  }
}

VerificationCalibrationTable.contextType = AppContext;
export default VerificationCalibrationTable;
