import { Box, Button, Card, CardContent, FormControl, Grid, Icon, IconButton, Input, InputAdornment, Link, TablePagination, TextField, Typography } from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SearchIcon from "@material-ui/icons/Search";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { appConst, formatDate } from "app/appConst";
import AppContext from "app/appContext";
import { LightTooltip } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage as searchByPageDepartment } from "../Department/DepartmentService";
import { searchByTextNew } from "../Supplier/SupplierService";
import { searchProductByPage } from "../VerificationCalibration/VerificationCalibrationService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import CalibrationDialogV2 from "./CalibrationDialogV2";
;
toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
    //etc you get the idea
});
function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <React.Fragment>
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`scrollable-force-tabpanel-${index}`}
                aria-labelledby={`scrollable-force-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        </React.Fragment>
    );
}
function MaterialButton(props) {
    const { t } = useTranslation();
    const item = props.item;
    const hasDeletePermission = props.hasDeletePermission;
    const hasEditPermission = props.hasEditPermission;
    const hasPrintPermission = props.hasPrintPermission;
    const hasSuperAccess = props.hasSuperAccess;
    const value = props.value;
    return (
        <div className="none_wrap">
            {
                // hasEditPermission &&
                // appConst.tabAllocation.tabReturn !== value &&
                // item?.allocationStatusIndex !==
                // appConst.listStatusAllocation[2].indexOrder && 
                (
                    <LightTooltip
                        title={t("general.editIcon")}
                        placement="right-end"
                        enterDelay={300}
                        leaveDelay={200}
                        PopperProps={{
                            popperOptions: {
                                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                            },
                        }}
                    >
                        <IconButton
                            size="small"
                            onClick={() => props.onSelect(item, appConst.active.edit)}
                        >
                            <Icon fontSize="small" color="primary">
                                edit
                            </Icon>
                        </IconButton>
                    </LightTooltip>
                )}
            {(hasDeletePermission || hasSuperAccess) &&
                (hasSuperAccess
                    ? hasSuperAccess
                    : appConst.listStatusAllocation[0].indexOrder ===
                    item?.allocationStatusIndex) && (
                    <LightTooltip
                        title={t("general.deleteIcon")}
                        placement="right-end"
                        enterDelay={300}
                        leaveDelay={200}
                        PopperProps={{
                            popperOptions: {
                                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                            },
                        }}
                    >
                        <IconButton
                            size="small"
                            onClick={() => props.onSelect(item, appConst.active.delete)}
                        >
                            <Icon fontSize="small" color="error">
                                delete
                            </Icon>
                        </IconButton>
                    </LightTooltip>
                )}
            {hasPrintPermission && (
                <LightTooltip
                    title={t("In phiếu")}
                    placement="right-end"
                    enterDelay={300}
                    leaveDelay={200}
                    PopperProps={{
                        popperOptions: {
                            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                        },
                    }}
                >
                    <IconButton
                        size="small"
                        onClick={() => props.onSelect(item, appConst.active.print)}
                    >
                        <Icon fontSize="small" color="inherit">
                            print
                        </Icon>
                    </IconButton>
                </LightTooltip>
            )}
            <LightTooltip
                title={t("general.viewIcon")}
                placement="right-end"
                enterDelay={300}
                leaveDelay={200}
            >
                <IconButton
                    size="small"
                    onClick={() => props.onSelect(item, appConst.active.view)}
                >
                    <Icon fontSize="small" color="primary">
                        visibility
                    </Icon>
                </IconButton>
            </LightTooltip>
        </div>
    );
}
function ComponentTableTabV2(props) {
    const { tabValue, status = null } = props
    const { t } = useTranslation();
    const { setPageLoading } = useContext(AppContext);
    const [selectedValue, setSelectedValue] = useState([])
    const [searchObject, setSearchObject] = useState({
        page: 0,
        rowsPerPage: 10,
        totalElements: 0,
        keyword: ""
    })
    const [itemList, setItemList] = useState([])
    const [item, setItem] = useState({})
    const [isView, setIsView] = useState(false)
    const [isSearch, setIsSearch] = useState(false)
    const [state, setState] = useState(null)
    const [shouldOpenAccreditationDialog, setShouldOpenAccreditationDialog] = useState(false)
    let supplierUnitSearchObject = {
        pageIndex: 1,
        pageSize: 1000000,
        typeCodes: [appConst.TYPE_CODES.NCC_KD]
    };
    let filterAutocomplete = createFilterOptions()

    const handleChangePage = (event, newPage) => {
        setSearchObject({ ...searchObject, page: newPage });
    };

    useEffect(() => {
        updatePageData()
    }, [searchObject.page, searchObject.rowsPerPage, state, status])


    const handleOKEditAccreditationClose = () => {
        updatePageData()
        setShouldOpenAccreditationDialog(false)
    };

    const handleAccreditationDialogClose = () => {
        setItem({})
        // setSelectedValue([])
        setShouldOpenAccreditationDialog(false)
        setIsView(false)
        // updatePageData()
    };

    const handleCreateAccreditation = () => {
        setItem({kdTaiSans: selectedValue})
        setShouldOpenAccreditationDialog(true)
    };
    const handleTextChange = (event) => {
        setSearchObject({
            ...searchObject,
            keyword: event.target.value,
            type : appConst.TYPE_KD_HC.KIEM_DINH.code
        })
    };
    const search = async () => {
        updatePageData()
    }

    const handleKeyDownEnterSearch = (e) => {
        if (e.key === "Enter") {
            search();
        }
    };
    const handleKeyUp = (e) => {
        !e.target.value && search();
    };
    const updatePageData = async () => {
        setPageLoading(true);
        let dataSearch = {};
        dataSearch.keyword = searchObject.keyword.trim() || "";
        dataSearch.pageIndex = searchObject.page + 1;
        dataSearch.pageSize = searchObject.rowsPerPage;
        dataSearch.kdTrangThai = status;
        dataSearch.type = appConst.TYPE_KD_HC.HIEU_CHUAN.code;
        dataSearch.kdDvThucHienId = state?.kdDvThucHien?.id;
        dataSearch.tsPhongSdId = state?.useDepartment?.id;
        dataSearch.kdHinhThuc = state?.kdHinhThuc?.code;
        dataSearch.toDate = state?.toDate ? moment(state?.toDate).format(formatDate) : null;
        dataSearch.fromDate = state?.fromDate ? moment(state?.fromDate).format(formatDate) : null;

        try {
            const result = await searchProductByPage(dataSearch)
            if (result?.status === appConst.CODE.SUCCESS) {
                setItemList([...result?.data?.data?.content])
                setSearchObject({ ...searchObject, totalElements: result?.data?.data?.totalElements })
            }

        } catch (error) {
            toast.error(t("toastr.error"));
        }
        finally {
            setPageLoading(false);
        };

    };

    let columns = [
        {
            title: t("general.stt"),
            field: "code",
            maxWidth: 50,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => ((searchObject?.page) * searchObject?.rowsPerPage) + (rowData.tableData.id + 1),
        },
        {
            title: t("Asset.code"),
            field: "tsMa",
            minWidth: 120,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("Asset.name"),
            field: "tsTen",
            minWidth: 250,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Calibration.type_calibration"),
            field: "kdHinhThucText",
            maxWidth: 150,
            minWidth: 150,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.performingUnit"),
            field: "kdDvThucHienText",
            align: "left",
            minWidth: 250,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.model"),
            field: "tsModel",
            align: "center",
            minWidth: 50,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("MaintainPlaning.seri"),
            field: "tsSerialNo",
            align: "center",
            minWidth: 50,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.manufacturerTable"),
            field: "tsHangSx",
            align: "left",
            minWidth: 150,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.status"),
            field: "kdTrangThaiText",
            align: "center",
            minWidth: 120,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("MaintainPlaning.useDepartment"),
            field: "tsPhongSdText",
            align: "left",
            minWidth: 125,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("purchasePlaning.code"),
            field: "tsMaQl",
            align: "left",
            minWidth: 110,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
    ];

    return (<div>
        <Grid container spacing={2} className="mt-12">
            <Grid item md={6} sm={12} xs={12}>
                {tabValue === appConst.tabVerification.pending && <Button
                    disabled={selectedValue?.length === 0}
                    className="mb-16 mr-16 align-bottom"
                    variant="contained"
                    color="primary"
                    onClick={() => handleCreateAccreditation()}
                >
                    {t("Calibration.acceptance_calibration")}
                </Button>}
                <Button
                    className="align-bottom mr-16 mb-16"
                    variant="contained"
                    color="primary"
                    style={{ paddingRight: "3px" }}
                    onClick={() => setIsSearch(!isSearch)}
                >
                    {t("Tìm kiếm nâng cao")}
                    <ArrowDropDownIcon />
                </Button>
            </Grid>
            <Grid item md={6} sm={12} xs={12}>
                <FormControl fullWidth>
                    <Input
                        className="search_box w-100"
                        onChange={handleTextChange}
                        onKeyDown={handleKeyDownEnterSearch}
                        onKeyUp={handleKeyUp}
                        placeholder={t("AssetTransfer.filter")}
                        id="search_box"
                        startAdornment={
                            <InputAdornment>
                                <Link>
                                    <SearchIcon
                                        onClick={() => search()}
                                        className="searchTable"
                                    />
                                </Link>
                            </InputAdornment>
                        }
                    />
                </FormControl>
            </Grid>
            {isSearch && (
                <Card className="mt-10 mb-20 w-100 mx-10">
                    <ValidatorForm>
                        <CardContent>
                            <Grid
                                container
                                xs={12}
                                spacing={2}
                                className="mx-0"
                            >
                                <Grid
                                    item
                                    md={12}
                                    xs={12}
                                    style={{
                                        fontWeight: "400",
                                        textTransform: "uppercase",
                                        textAlign: "center",
                                    }}
                                >
                                    <b>Tìm kiếm nâng cao</b>
                                </Grid>
                                {/* <Grid item md={3} xs={12}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            margin="none"
                                            fullWidth
                                            autoOk
                                            id="date-picker-dialog"
                                            label={t("MaintainPlaning.dxFrom")}
                                            format="dd/MM/yyyy"
                                            value={state?.fromDate ?? null}
                                            onChange={(data) => handleSetDataSelect(
                                                data,
                                                "fromDate"
                                            )}
                                            KeyboardButtonProps={{ "aria-label": "change date", }}
                                            invalidDateMessage={t("general.invalidDateFormat")}
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            margin="none"
                                            fullWidth
                                            autoOk
                                            id="date-picker-dialog"
                                            label={t("MaintainPlaning.dxTo")}
                                            format="dd/MM/yyyy"
                                            value={state?.toDate ?? null}
                                            onChange={(data) => handleSetDataSelect(
                                                data,
                                                "toDate"
                                            )}
                                            KeyboardButtonProps={{ "aria-label": "change date", }}
                                            invalidDateMessage={t("general.invalidDateFormat")}
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid> */}

                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        label={t("Asset.CalibrationUnit")}
                                        searchFunction={searchByTextNew}
                                        searchObject={supplierUnitSearchObject}
                                        displayLable={"name"}
                                        showCode={"showCode"}
                                        value={state?.kdDvThucHien}
                                        onSelect={(value) => setState(
                                            { ...state, kdDvThucHien: value }
                                        )}
                                        filterOptions={(options, params) => {
                                            params.inputValue = params.inputValue.trim()
                                            let filtered = filterAutocomplete(options, params)
                                            return filtered
                                        }}
                                        noOptionsText={t("general.noOption")}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <Autocomplete
                                        options={appConst.hinhThucKiemDinhOptions}
                                        fullWidth
                                        value={state?.kdHinhThuc || null}
                                        getOptionLabel={option => option.name}
                                        onChange={(e, value) => setState(
                                            { ...state, kdHinhThuc: value })}
                                        filterOptions={(options, params) => {
                                            params.inputValue = params.inputValue.trim()
                                            let filtered = filterAutocomplete(options, params)
                                            return filtered
                                        }}
                                        renderInput={params => <TextField
                                            {...params}
                                            label={t("Calibration.type_calibration")}
                                            value={state?.kdHinhThuc || null}
                                        />}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        searchFunction={searchByPageDepartment}
                                        searchObject={{
                                            pageIndex: 1,
                                            pageSize: 1000000,
                                        }}
                                        label={t("MaintainPlaning.useDepartment")}
                                        displayLable="text"
                                        value={state?.useDepartment}
                                        onSelect={(value) => setState(
                                            { ...state, useDepartment: value }
                                        )}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>

                                </Grid>
                            </Grid>
                        </CardContent>
                    </ValidatorForm>
                </Card>
            )}

            <Grid item xs={12}>
                <MaterialTable
                    // onRowClick={(e, rowData) => {
                    //     getRowData(rowData)
                    // }}
                    parentChildData={(row, rows) => {
                        return rows.find((a) => a.id === row.parentId);
                    }}
                    title={t("general.list")}
                    data={itemList}
                    columns={columns}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t(
                                "general.emptyDataMessageTable"
                            )}`,
                        },
                    }}
                    options={{
                        actionsColumnIndex: -1,
                        selection: tabValue === appConst.tabVerification.pending,
                        paging: false,
                        search: false,
                        sorting: false,
                        rowStyle: (rowData) => ({
                            backgroundColor:
                                rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
                        }),
                        maxBodyHeight: "490px",
                        minBodyHeight: "260px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                            paddingLeft: 10,
                            paddingRight: 10,
                            textAlign: "center",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                    onSelectionChange={(rows) => {
                        setSelectedValue(rows)
                    }}
                />
                <TablePagination
                    align="left"
                    className="px-16"
                    rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                    labelRowsPerPage={t("general.rows_per_page")}
                    labelDisplayedRows={({ from, to, count }) =>
                        `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                        }`
                    }
                    component="div"
                    count={searchObject.totalElements}
                    rowsPerPage={searchObject.rowsPerPage}
                    page={searchObject.page}
                    backIconButtonProps={{
                        "aria-label": "Previous Page",
                    }}
                    nextIconButtonProps={{
                        "aria-label": "Next Page",
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={(e) => setSearchObject({ ...searchObject, rowsPerPage: e.target.value })}
                />
            </Grid>
            {shouldOpenAccreditationDialog && (
                <CalibrationDialogV2
                    t={t}
                    handleClose={() => handleAccreditationDialogClose()}
                    open={shouldOpenAccreditationDialog}
                    handleOKEditClose={handleOKEditAccreditationClose}
                    item={item}
                    isView={isView}
                />
            )}
        </Grid>
    </div>);
}

export default ComponentTableTabV2;