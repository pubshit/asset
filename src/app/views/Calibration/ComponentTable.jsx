import {
    Button,
    Card,
    CardContent,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination,
    TextField,
} from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SearchIcon from "@material-ui/icons/Search";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { appConst, formatDate } from "app/appConst";
import AppContext from "app/appContext";
import {defaultPaginationProps, handleKeyDown} from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage as searchByPageDepartment } from "../Department/DepartmentService";
import { searchByTextNew } from "../Supplier/SupplierService";
import { searchProductByPage } from "../VerificationCalibration/VerificationCalibrationService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import CalibrationDialogV2 from "./CalibrationDialogV2";
import { searchByPage } from "../Contract/ContractService";

toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
});


function ComponentTable(props) {
    const { tabValue, status = null } = props
    const { t } = useTranslation();
    const { setPageLoading } = useContext(AppContext);
    const [selectedValue, setSelectedValue] = useState([])
    const [searchObject, setSearchObject] = useState({
        page: 0,
        rowsPerPage: 10,
        totalElements: 0,
        keyword: ""
    })
    let pageSearchDefault = 1
    const [itemList, setItemList] = useState([])
    const [item, setItem] = useState({})
    const [isView, setIsView] = useState(false)
    const [isSearch, setIsSearch] = useState(false)
    const [stateAdvance, setStateAdvance] = useState(null)
    const [shouldOpenAccreditationDialog, setShouldOpenAccreditationDialog] = useState(false)
    let supplierUnitSearchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        typeCodes: [appConst.TYPE_CODES.NCC_HC]
    };
    let filterAutocomplete = createFilterOptions()

    const handleChangePage = (event, newPage) => {
        setSearchObject({ ...searchObject, page: newPage });
    };

    useEffect(() => {
        !isSearch && setStateAdvance({})
    }, [isSearch])

    useEffect(() => {
        updatePageData()
    }, [searchObject.page, searchObject.rowsPerPage, stateAdvance, status])


    const handleOKEditAccreditationClose = () => {
        updatePageData()
        setShouldOpenAccreditationDialog(false)
    };

    const handleAccreditationDialogClose = () => {
        setItem({})
        setShouldOpenAccreditationDialog(false)
        setIsView(false)
    };

    const handleCreateAccreditation = () => {
        setItem({ kdTaiSans: selectedValue })
        setShouldOpenAccreditationDialog(true)
    };
    const handleTextChange = (event) => {
        setSearchObject({
            ...searchObject,
            keyword: event.target.value,
            types: appConst.TYPE_KD_HC.HIEU_CHUAN.code
        })
    };
    const search = async () => {
        updatePageData(pageSearchDefault)
    }

    const handleKeyDownEnterSearch = (e) => handleKeyDown(e, search);

    const handleKeyUp = (e) => {
        !e.target.value && search();
    };

    const updatePageData = async (pageSearchDefault) => {
        setPageLoading(true);
        let dataSearch = {};
        dataSearch.keyword = searchObject.keyword.trim() || "";
        dataSearch.pageIndex = (pageSearchDefault ? pageSearchDefault : null) || searchObject.page + 1;
        dataSearch.pageSize = searchObject.rowsPerPage;
        dataSearch.kdTrangThai = status;
        dataSearch.types = appConst.TYPE_KD_HC.HIEU_CHUAN.code;
        dataSearch.kdDvThucHienId = stateAdvance?.kdDvThucHien?.id;
        dataSearch.tsPhongSdId = stateAdvance?.useDepartment?.id;
        dataSearch.kdHinhThuc = stateAdvance?.kdHinhThuc?.code;
        dataSearch.toDate = stateAdvance?.toDate ? moment(stateAdvance?.toDate).format(formatDate) : null;
        dataSearch.fromDate = stateAdvance?.fromDate ? moment(stateAdvance?.fromDate).format(formatDate) : null;
        dataSearch.contractId = stateAdvance?.contract?.id;

        try {
            const result = await searchProductByPage(dataSearch)
            if (result?.status === appConst.CODE.SUCCESS) {
                setItemList([...result?.data?.data?.content])
                setSearchObject({ ...searchObject, totalElements: result?.data?.data?.totalElements })
            }

        } catch (error) {
            toast.error(t("toastr.error"));
        }
        finally {
            setPageLoading(false);
        };

    };

    let columns = [
        {
            title: t("general.stt"),
            field: "code",
            maxWidth: 50,
            align: "left",
            cellStyle: {
                textAlign: "center",
            },
            render: (rowData) => ((searchObject?.page) * searchObject?.rowsPerPage) + (rowData.tableData.id + 1),
        },
        {
            title: t("Asset.code"),
            field: "tsMa",
            minWidth: 120,
            align: "left",
            cellStyle: {
                textAlign: "center",
            },
        },
        {
            title: t("purchasePlaning.code"),
            field: "khMa",
            align: "left",
            minWidth: 110,
            cellStyle: {
                textAlign: "center",
            },
        },
        {
            title: t("Asset.name"),
            field: "tsTen",
            minWidth: 250,
            align: "left",
        },
        {
            title: t("Calibration.type_calibration"),
            field: "kdHinhThucText",
            maxWidth: 150,
            minWidth: 150,
            align: "center",
            cellStyle: {
                textAlign: "center",
            },
        },
        {
            title: t("Asset.performingUnit"),
            field: "kdDvThucHienText",
            align: "left",
            minWidth: 250,
        },
        {
            title: t("Asset.model"),
            field: "tsModel",
            align: "center",
            minWidth: 50,
        },
        {
            title: t("MaintainPlaning.seri"),
            field: "tsSerialNo",
            align: "center",
            minWidth: 50,
        },
        {
            title: t("Asset.manufacturerTable"),
            field: "tsHangSx",
            align: "left",
            minWidth: 150,
        },
        {
            title: t("MaintainPlaning.useDepartment"),
            field: "tsPhongSdText",
            align: "left",
            minWidth: 125,
        },

    ];

    return (<div>
        <Grid container spacing={2} className="mt-12">
            <Grid item md={6} sm={12} xs={12}>
                {tabValue === appConst.tabVerification.pending && <Button
                    disabled={selectedValue?.length === 0}
                    className="mb-16 mr-16 align-bottom"
                    variant="contained"
                    color="primary"
                    onClick={() => handleCreateAccreditation()}
                >
                    {t("Calibration.acceptance_calibration")}
                </Button>}
                <Button
                    className="align-bottom mr-16 mb-16"
                    variant="contained"
                    color="primary"
                    style={{ paddingRight: "3px" }}
                    onClick={() => setIsSearch(!isSearch)}
                >
                    {t("Tìm kiếm nâng cao")}
                    <ArrowDropDownIcon />
                </Button>
            </Grid>
            <Grid item md={6} sm={12} xs={12}>
                <FormControl fullWidth>
                    <Input
                        className="search_box w-100"
                        onChange={handleTextChange}
                        onKeyDown={handleKeyDownEnterSearch}
                        onKeyUp={handleKeyUp}
                        placeholder={t("VerificationCalibration.search_asset")}
                        id="search_box"
                        startAdornment={
                            <InputAdornment position="end">
                                <SearchIcon
                                    onClick={() => search()}
                                    className="searchTable"
                                />
                            </InputAdornment>
                        }
                    />
                </FormControl>
            </Grid>
            {isSearch && (
                <Card className="mt-10 mb-20 w-100 mx-10">
                    <ValidatorForm>
                        <CardContent>
                            <Grid container spacing={2} className="mx-0">
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        label={t("Asset.CalibrationUnit")}
                                        searchFunction={searchByTextNew}
                                        searchObject={supplierUnitSearchObject}
                                        displayLable={"name"}
                                        showCode={"showCode"}
                                        value={stateAdvance?.kdDvThucHien}
                                        onSelect={(value) => setStateAdvance(
                                            { ...stateAdvance, kdDvThucHien: value }
                                        )}
                                        noOptionsText={t("general.noOption")}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <Autocomplete
                                        options={appConst.hinhThucKiemDinhOptions}
                                        fullWidth
                                        value={stateAdvance?.kdHinhThuc || null}
                                        getOptionLabel={option => option.name}
                                        onChange={(e, value) => setStateAdvance(
                                            { ...stateAdvance, kdHinhThuc: value })}
                                        filterOptions={(options, params) => {
                                            params.inputValue = params.inputValue.trim()
                                            let filtered = filterAutocomplete(options, params)
                                            return filtered
                                        }}
                                        renderInput={params => <TextField
                                            {...params}
                                            label={t("Calibration.type_calibration")}
                                            value={stateAdvance?.kdHinhThuc || null}
                                        />}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        searchFunction={searchByPageDepartment}
                                        searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                                        label={t("MaintainPlaning.useDepartment")}
                                        displayLable="text"
                                        value={stateAdvance?.useDepartment}
                                        onSelect={(value) => setStateAdvance(
                                            { ...stateAdvance, useDepartment: value }
                                        )}
                                    />
                                </Grid>
                                <Grid item md={3} xs={12}>
                                    <AsynchronousAutocomplete
                                        searchFunction={searchByPage}
                                        searchObject={appConst.OBJECT_SEARCH_MAX_SIZE}
                                        label={t("Contract.title")}
                                        displayLable="contractName"
                                        typeReturnFunction="categorylist"
                                        value={stateAdvance?.contract}
                                        onSelect={(value) => setStateAdvance(
                                            { ...stateAdvance, contract: value }
                                        )}
                                    />
                                </Grid>
                            </Grid>
                        </CardContent>
                    </ValidatorForm>
                </Card>
            )}

            <Grid item xs={12}>
                <MaterialTable
                    title={t("general.list")}
                    data={itemList}
                    columns={columns}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
                        },
                    }}
                    options={{
                        actionsColumnIndex: -1,
                        selection: tabValue === appConst.tabVerification.pending,
                        paging: false,
                        search: false,
                        sorting: false,
                        draggable: false,
                        rowStyle: (rowData) => ({
                            backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "490px",
                        minBodyHeight: "260px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                    onSelectionChange={(rows) => {
                        setSelectedValue(rows)
                    }}
                />
                <TablePagination
                  {...defaultPaginationProps()}
                  rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                  count={searchObject.totalElements}
                  rowsPerPage={searchObject.rowsPerPage}
                  page={searchObject.page}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={(e) => setSearchObject({ ...searchObject, rowsPerPage: e.target.value })}
                />
            </Grid>
            {shouldOpenAccreditationDialog && (
                <CalibrationDialogV2
                    t={t}
                    handleClose={() => handleAccreditationDialogClose()}
                    open={shouldOpenAccreditationDialog}
                    handleOKEditClose={handleOKEditAccreditationClose}
                    item={item}
                    isView={isView}
                />
            )}
        </Grid>
    </div>);
}

export default ComponentTable;