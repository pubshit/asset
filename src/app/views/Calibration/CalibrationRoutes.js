import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const CalibrationTable = EgretLoadable({
  loader: () => import("./VerificationCalibrationTable")
});
const ViewComponent = withTranslation()(CalibrationTable);

const CalibrationListRoutes = [
  {
    path: ConstantList.ROOT_PATH + "calibration/list",
    exact: true,
    component: ViewComponent
  }
];

export default CalibrationListRoutes;