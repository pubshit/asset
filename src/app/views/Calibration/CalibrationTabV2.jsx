import { AppBar, Box, Grid, Tab, Tabs, Typography } from "@material-ui/core";
import { appConst } from "app/appConst";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AcceptantCalibrationTab from "./AcceptantCalibrationTab";
import ComponentTableTabV2 from "./ComponentTableTabV2";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}
function CalibrationTabV2(props) {
  const { t } = useTranslation();
  const [tabValue, setTabValue] = useState(0);

  const handleChangeTabValue = (event, newValue) => {
    if (appConst?.tabVerification?.pending === newValue) {
      setTabValue(newValue)
    }
    if (appConst?.tabVerification?.verificated === newValue) {
      setTabValue(newValue)
    }
    if (appConst?.tabVerification?.verificating === newValue) {
      setTabValue(newValue)
    }
    if (appConst?.tabVerification?.acceptance === newValue) {
      setTabValue(newValue)
    }
  };

  return (
    <div>
      <Grid container spacing={2} className="mt-12">
        <Grid item xs={12}>
          <AppBar position="static" color="default">
            <Tabs
              value={tabValue}
              onChange={(e, value) => handleChangeTabValue(e, value)}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              [
              <Tab key={appConst.tabVerification.pending} label={t("Calibration.pending")} />,
              <Tab key={appConst.tabVerification.verificating} label={t("Calibration.calibrating")} />,
              <Tab key={appConst.tabVerification.verificated} label={t("Calibration.calibrated")} />,
              <Tab key={appConst.tabVerification.acceptance} label={t("Calibration.acceptance")} />,
              ]

            </Tabs>
          </AppBar>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.pending}
            className="mp-0"
          >
            <ComponentTableTabV2 tabValue={tabValue} status={appConst.tabVerificationStatus.pending} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.verificating}
            className="mp-0"
          >
            <ComponentTableTabV2 tabValue={tabValue} status={appConst.tabVerificationStatus.verificating} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.verificated}
            className="mp-0"
          >
            <ComponentTableTabV2 tabValue={tabValue} status={appConst.tabVerificationStatus.verificated} />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerification.acceptance}
            className="mp-0"
          >
            <AcceptantCalibrationTab tabValue={tabValue} />
          </TabPanel>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
    </div>
  );
}

export default CalibrationTabV2;
