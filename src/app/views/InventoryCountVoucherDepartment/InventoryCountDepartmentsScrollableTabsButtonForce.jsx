import DateFnsUtils from "@date-io/date-fns";
import {Grid, Icon, IconButton, Radio, TableCell, TableHead, TableRow,} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import {makeStyles} from "@material-ui/core/styles";
import {createFilterOptions} from "@material-ui/lab/useAutocomplete";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import {appConst, DEFAULT_TOOLTIPS_PROPS, STATUS_STORE, variable} from "app/appConst";
import {a11yProps, convertNumberPrice, correctPrecision, NumberFormatCustom} from "app/appFunction";
import clsx from "clsx";
import MaterialTable, {MTableToolbar} from "material-table";
import moment from "moment";
import React, {useState} from "react";
import {TextValidator} from "react-material-ui-form-validator";
import {getAllAssetStatuss as searchAssetStatus} from "../AssetStatus/AssetStatusService";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import {getAllInventoryCountStatus} from "../InventoryCountStatus/InventoryCountStatusService";
import {searchByPage as storesSearchByPage} from "../Store/StoreService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {searchByPage} from "../Council/CouncilService";
import {LightTooltip, TabPanel} from "../Component/Utilities"

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    zIndex: 1,
    background: "#358600",
    position: "sticky",
    top: 0,
    left: 0,
    "& .MuiTableCell-head": {
      padding: "4px"
    }
  },
  borderRight: {
    borderRight: "1px solid white !important",
    color: "#ffffff",
    textAlign: "center"
  },
  colorWhite: {
    color: "#ffffff",
    textAlign: "center"
  },
  mw_100: {
    minWidth: "90px"
  },
  mw_70: {
    minWidth: "70px"
  },
  tabPanel: {
    "& .MuiBox-root": {
      paddingLeft: "0",
      paddingRight: "0"
    }
  },
  textRight: {
    "& input": {
      textAlign: "right"
    }
  }
}));

export default function InventoryCountDepartmentsScrollableTabsButtonForce(props) {
  const t = props.t;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  const [listAssetStatus, setListAssetStatus] = useState([]);
  const [listStatus, setListStatus] = useState([]);
  const [listInventoryCountBoard, setListInventoryCountBoard] = useState(null);
  const [listInventoryPerson, setListInventoryPerson] = useState(props?.item?.inventoryCountPersons || []);
  const [inventoryBoard, setInventoryBoard] = useState([])

  const searchObjectStore = {
    pageIndex: 0,
    pageSize: 1000000,
    isActive: STATUS_STORE.HOAT_DONG.code
  }; // kho tài sản
  const filterAutocomplete = createFilterOptions();

  const handleClick = (event, item) => {
    if (props?.setSelectedValue) {
      if (item?.id) {
        props.setSelectedValue(item?.id);
        props.handleSetAssetReevaluate(item)
      }
    } else {
      props.setSelectedValue(null);
      props.handleSetAssetReevaluate({})
    }
    return;
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  
  let searchObject = {pageIndex: 0, pageSize: 1000000, type: appConst.OBJECT_HD_TYPE.KIEM_KE.code,};
  let columns = (props?.item?.id && props?.isEnabledBtn) ? [
    {
      title: t("general.reevaluateAcronyms"),
      field: "custom",
      align: "left",
      minWidth: "45px",
      render: (rowData) => {
        return rowData.isAllowedReevaluate && <Radio
          id={`radio${rowData.id}`}
          className="p-0"
          name="radSelected"
          value={rowData.asset.id}
          checked={props?.selectedValue === rowData.asset.id}
          onClick={(event) => handleClick(event, rowData?.asset)}
        />
      }
    },
    {
      title: t("Asset.stt"),
      field: "",
      maxWidth: "50px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("Asset.oldCode"),
      field: "asset.managementCode",
      minWidth: "100px",
      align: "left",
    },
    {
      title: t("Asset.code"),
      field: "asset.code",
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.asset?.code || rowData?.assetCode,
    },
    {
      title: t("Asset.name"),
      field: "asset",
      align: "left",
      minWidth: 200,
      render: (rowData) => rowData?.asset?.name || rowData?.assetName,
    },
    {
      title: t("InventoryCountStatus.CountAsset.currentStatus"),
      field: "currentStatus",
      minWidth: "160px",
      render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
    },
    {
      title: t("InventoryCountStatus.CountAsset.status"),
      field: "status",
      minWidth: 180,
      render: (rowData) => ((props?.item?.isView || props?.item?.isLiquidate)
        ? (
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData.status ? rowData.status?.name : ''}
          />
        ) : (
          <AsynchronousAutocompleteSub
            listData={listAssetStatus}
            setListData={setListAssetStatus}
            searchFunction={searchAssetStatus}
            searchObject={searchObject}
            defaultValue={rowData.status}
            displayLable={"name"}
            value={rowData.status}
            onSelect={(value) => props.selectStatus(value, rowData)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim()
              return filterAutocomplete(options, params)
            }}
            noOptionsText={t("general.noOption")}
          />
        )
      ),
    },
    {
      title: t("Asset.statusDgl"),
      field: "asset.dglTrangthai",
      minWidth: "120px",
      render: (rowData) => {
        return appConst.statusAssetReevaluate.find(item => item.id === rowData.dglTrangthai)?.name
      }
    },
    {
      title: t("Asset.receive_date"),
      field: "dateOfReception",
      minWidth: 120,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.asset?.dateOfReception ? (
          <span>
            {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
          </span>
        ) : (
          ""
        ),
    },
    {
      title: t("InventoryCountVoucher.store"),
      field: "asset.store",
      minWidth: "200px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) =>
        rowData?.isStore && (props?.item?.isView ?
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData?.store ? rowData?.store?.name : ''}
          /> : <AsynchronousAutocomplete
            searchFunction={storesSearchByPage}
            searchObject={searchObjectStore}
            defaultValue={rowData?.store}
            displayLable={"name"}
            value={rowData?.store}
            onSelect={props.selectStores}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim()
              let filtered = filterAutocomplete(options, params)
              return filtered
            }}
            noOptionsText={t("general.noOption")}
            validators={["required"]}
            errorMessages={[t('general.required')]}
          />)
    },
    {
      title: t("InstrumentToolsList.useDepartment"),
      field: "departmentName",
      align: "left",
      minWidth: 160,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.departmentName
    },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.accountantQuantity",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.accountantQuantity || 0,
      },
    {
      title: t("InventoryCountVoucher.originalCost"),
      field: "asset.accountantOriginalCost",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
    },
    {
      title: t("InventoryCountVoucher.carryingAmount"),
      field: "asset.accountantCarryingAmount",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
    },
    {
      title: t("InventoryCountVoucher.quanlity"),
      field: "asset.inventoryQuantity",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          name="inventoryQuantity"
          value={rowData?.inventoryQuantity || 0}
          onKeyDown={e => variable.regex.numberExceptThisSymbols.includes(e.key) && e.preventDefault()}
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          InputProps={{
            readOnly: props?.item?.isView,
            inputProps: {
              className: "text-center"
            }
          }}
          validators={["required", "minNumber:0", "maxNumber:1"]}
          errorMessages={[t("general.required"), "Số lượng không được âm", "Phải nhỏ hơn hoặc bằng 1"]}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.originalCost"),
      field: "asset.inventoryOriginalCost",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          name="inventoryOriginalCost"
          defaultValue={rowData?.inventoryOriginalCost}
          value={rowData?.inventoryOriginalCost}
          id="formatted-numberformat-originalCost"
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: props?.item?.isView
          }}
          validators={["required", "minNumber:0"]}
          errorMessages={[t("general.required"), "Số không được âm"]}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.carryingAmount"),
      field: "asset.inventoryCarryingAmount",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          name="inventoryCarryingAmount"
          defaultValue={rowData?.inventoryCarryingAmount}
          value={rowData?.inventoryCarryingAmount}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: props?.item?.isView
          }}
          validators={["required", "minNumber:0"]}
          errorMessages={[t("general.required"), "Số không được âm"]}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.quanlity"),
      field: "asset.differenceQuantity",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData?.differenceQuantity
    },
    {
      title: t("InventoryCountVoucher.originalCost"),
      field: "asset.differenceOriginalCost",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          name="differenceOriginalCost"
          defaultValue={correctPrecision(rowData?.differenceOriginalCost)}
          value={correctPrecision(rowData?.differenceOriginalCost) || 0}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: true,
            disableUnderline: true,
          }}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.carryingAmount"),
      field: "asset.differenceCarryingAmount",
      minWidth: "100px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => (
        <TextValidator
          className={clsx("w-40", classes.textRight)}
          name="differenceCarryingAmount"
          defaultValue={correctPrecision(rowData?.differenceCarryingAmount)}
          value={correctPrecision(rowData?.differenceCarryingAmount) || 0}
          InputProps={{
            inputComponent: NumberFormatCustom,
            readOnly: true,
            disableUnderline: true,
          }}
        />
      ),
    },
    {
      title: t("InventoryCountVoucher.note"),
      field: "custom",
      minWidth: "150px",
      render: (rowData) => (
        <TextValidator
          className="w-40"
          onChange={(event) => props.handleRowDataCellChange(rowData, event)}
          type="text"
          name="note"
          value={rowData.note}
          InputProps={{
            readOnly: props?.item?.isView
          }}
        />
      ),
    },
  ] :
    [
      {
        title: t("Asset.stt"),
        field: "",
        maxWidth: "50px",
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        minWidth: "100px",
        align: "left",
        cellStyle: {
          textAlign: "left",
        },
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        align: "left",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.asset?.code || rowData?.assetCode,
      },
      {
        title: t("Asset.name"),
        field: "asset",
        align: "left",
        minWidth: 200,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => rowData?.asset?.name || rowData?.assetName,
      },
      {
        title: t("InventoryCountStatus.CountAsset.currentStatus"),
        field: "currentStatus",
        minWidth: "160px",
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) => rowData?.currentStatus?.name || rowData?.currentStatus
      },
      {
        title: t("InventoryCountStatus.CountAsset.status"),
        minWidth: 180,
        cellStyle: {
          textAlign: "left",
        },
        render: (rowData) =>
        ((props?.item?.isView || props?.item?.isLiquidate) ?
          <TextValidator
            className="w-100"
            InputProps={{
              readOnly: true,
            }}
            value={rowData.status ? rowData.status?.name : ''}
          /> : <AsynchronousAutocompleteSub
            listData={listAssetStatus}
            setListData={setListAssetStatus}
            searchFunction={searchAssetStatus}
            searchObject={searchObject}
            defaultValue={rowData.status}
            displayLable={"name"}
            value={rowData.status}
            onSelect={(value) => props.selectStatus(value, rowData)}
            onSelectOptions={rowData}
            disableClearable={true}
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim()
              let filtered = filterAutocomplete(options, params)
              return filtered
            }}
            noOptionsText={t("general.noOption")}
          />
        ),
      },
      {
        title: t("Asset.receive_date"),
        field: "dateOfReception",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.asset?.dateOfReception ? (
            <span>
              {moment(rowData?.asset?.dateOfReception).format("DD/MM/YYYY")}
            </span>
          ) : (
            ""
          ),
      },
      {
        title: t("InventoryCountVoucher.store"),
        field: "asset.store",
        minWidth: "200px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.isStore && (props?.item?.isView ?
            <TextValidator
              className="w-100"
              InputProps={{
                readOnly: true,
              }}
              value={rowData?.store ? rowData?.store?.name : ''}
            /> : <AsynchronousAutocomplete
              searchFunction={storesSearchByPage}
              searchObject={searchObjectStore}
              defaultValue={rowData?.store}
              displayLable={"name"}
              value={rowData?.store}
              onSelect={props.selectStores}
              onSelectOptions={rowData}
              disableClearable={true}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim()
                return filterAutocomplete(options, params)
              }}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />)
      },
      {
        title: t("InstrumentToolsList.useDepartment"),
        field: "departmentName",
        align: "left",
        minWidth: 160,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.departmentName
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.accountantQuantity",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.accountantQuantity || 0,
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.accountantOriginalCost",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.accountantCarryingAmount",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.inventoryQuantity",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <TextValidator
            name="inventoryQuantity"
            value={rowData?.inventoryQuantity || 0}
            onKeyDown={e => variable.regex.numberExceptThisSymbols.includes(e.key) && e.preventDefault()}
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            InputProps={{
              readOnly: props?.item?.isView,
              inputProps: {
                className: "text-center"
              }
            }}
            validators={["required", "minNumber:0", "maxNumber:1"]}
            errorMessages={[t("general.required"), "Số lượng không được âm", "Phải nhỏ hơn hoặc bằng 1"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.inventoryOriginalCost",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            name="inventoryOriginalCost"
            defaultValue={rowData?.inventoryOriginalCost}
            value={rowData?.inventoryOriginalCost}
            id="formatted-numberformat-originalCost"
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.inventoryCarryingAmount",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            name="inventoryCarryingAmount"
            defaultValue={rowData?.inventoryCarryingAmount}
            value={rowData?.inventoryCarryingAmount}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: props?.item?.isView
            }}
            validators={["required", "minNumber:0"]}
            errorMessages={[t("general.required"), "Số không được âm"]}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.quanlity"),
        field: "asset.differenceQuantity",
        minWidth: "100px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData?.differenceQuantity
      },
      {
        title: t("InventoryCountVoucher.originalCost"),
        field: "asset.differenceOriginalCost",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceOriginalCost"
            defaultValue={correctPrecision(rowData?.differenceOriginalCost)}
            value={correctPrecision(rowData?.differenceOriginalCost) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.carryingAmount"),
        field: "asset.differenceCarryingAmount",
        minWidth: "100px",
        cellStyle: {
          textAlign: "right",
        },
        render: (rowData) => (
          <TextValidator
            className={clsx("w-40", classes.textRight)}
            name="differenceCarryingAmount"
            defaultValue={correctPrecision(rowData?.differenceCarryingAmount)}
            value={correctPrecision(rowData?.differenceCarryingAmount) || 0}
            InputProps={{
              inputComponent: NumberFormatCustom,
              readOnly: true,
              disableUnderline: true,
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.note"),
        field: "custom",
        minWidth: "150px",
        render: (rowData) => (
          <TextValidator
            className="w-40"
            onChange={(event) => props.handleRowDataCellChange(rowData, event)}
            type="text"
            name="note"
            value={rowData.note}
            InputProps={{
              readOnly: props?.item?.isView
            }}
          />
        ),
      },
    ];

  const columnsInventoryCount = [
    {
      title: t("InventoryCountVoucher.stt"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    ...(!props?.item?.isView && props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder ? [
      {
        title: t("general.action"),
        field: "",
        align: "center",
        maxWidth: 80,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          <div className="none_wrap">
            <LightTooltip title={t("general.deleteIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
              <IconButton size="small" onClick={() => handleRowDataCellDelete(rowData)}>
                <Icon fontSize="small" color="error">delete</Icon>
              </IconButton>
            </LightTooltip>
          </div>
      }
    ] : []),
    {
      title: t("InventoryCountVoucher.assessor"),
      field: "personName",
      align: "left",
      minWidth: 300,
    },
    {
      title: t("asset_liquidate.position"),
      field: "position",
      align: "left",
      minWidth: 150,
    },
    {
      title: t("InventoryCountVoucher.role"),
      field: "role",
      align: "left",
      minWidth: 150,
      render: (rowData) => appConst.listHdChiTietsRole.find(item => item?.code === rowData?.role)?.name
    },
    {
      title: t("asset_liquidate.department"),
      field: "departmentName",
      align: "left",
      minWidth: 150,
    }
  ]
  
  const handleChangeSelectInventoryBoard = (item) => {
    setInventoryBoard(item);
    props.handleSetHoiDongId(item?.id)
    props.handleSetListInventoryPerson(item?.hdChiTiets)
    // eslint-disable-next-line no-unused-expressions
    item?.hdChiTiets?.map(chitiet => {
      chitiet.handoverDepartment = {
        departmentId: chitiet.departmentId,
        departmentName: chitiet.departmentName,
        name: chitiet.departmentName
      }
      return chitiet
    })
    setListInventoryPerson(item?.hdChiTiets || []);
  };
  const handleRowDataCellDelete = (item) => {
    let newDataRow = listInventoryPerson.filter(
      (i) => i.tableData.id !== item.tableData.id
    );
    props.handleSetListInventoryPerson(newDataRow)
    setListInventoryPerson(newDataRow || []);
  };
  const handleSelectHandoverDepartment = (item, rowData) => {
    let newDataRow = listInventoryPerson.map((i) => {
      if (i.tableData.id === rowData.tableData.id) {
        i.handoverDepartment = item;
        i.departmentId = item?.id;
        i.departmentName = item?.name
      }
      return i;
    });
    
    setListInventoryPerson(newDataRow || []);
  };
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label={t("InventoryCountVoucher.tabInfo")} {...a11yProps(0)} />
          <Tab label={t("InventoryCountVoucher.inventoryCouncil")} {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel className={classes.tabPanel} value={value} index={0}>
        <Grid className="" container spacing={1}>
          <Grid item md={2} sm={12} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog mt-2"
                label={
                  <span>
                    <span className="colorRed">* </span>
                    {t("InventoryCountVoucher.inventoryCountDate")}
                  </span>
                }
                name={'inventoryCountDate'}
                inputVariant="standard"
                autoOk={true}
                format="dd/MM/yyyy"
                value={props?.item?.inventoryCountDate}
                onChange={(date) => props.handleDateChange(date, "inventoryCountDate")}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                open={isPickerOpen}
                onOpen={() => setIsPickerOpen(!props?.item?.isView)}
                onClose={() => setIsPickerOpen(false)}
                validators={props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventory[0].indexOrder ? ['required'] : []}
                errorMessages={props?.item?.inventoryCountStatus?.indexOrder !== appConst.listStatusInventory[0].indexOrder ? t('general.required') : []}
                maxDate={new Date()}
                maxDateMessage={t("InventoryCountVoucher.noti_maxDate")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={2} sm={12} xs={12}>
            {props?.item?.isView ?
              <TextValidator
                className="w-100"
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InventoryCountVoucher.status")}
                  </span>
                }
                InputProps={{
                  readOnly: true,
                }}
                value={props.item.inventoryCountStatus ? props.item.inventoryCountStatus?.name : ''}
              /> : <AsynchronousAutocompleteSub
                label={
                  <span>
                    <span className="colorRed"> * </span>
                    {t("InventoryCountVoucher.status")}
                  </span>
                }
                listData={listStatus}
                setListData={setListStatus}
                searchFunction={getAllInventoryCountStatus}
                searchObject={searchObject}
                defaultValue={props.item.inventoryCountStatus}
                displayLable={"name"}
                value={props.item.inventoryCountStatus}
                onSelect={props.selectInventoryCount}
                validators={["required"]}
                errorMessages={[t("general.required")]}
                noOptionsText={t("general.noOption")}
              />
            }
          </Grid>
          <Grid item md={8} sm={12} xs={12}>
            <TextValidator
              label={
                <span>
                  <span className="colorRed"> * </span>
                  {t("InventoryCountVoucher.voucherName")}
                </span>
              }
              className="w-100"
              name="title"
              value={props.item.title}
              onChange={props.handleChange}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: props?.item?.isView
              }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} className="mt-12">
            <MaterialTable
              data={props?.dataAssetTable}
              columns={columns}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: true,
                search: false,
                maxBodyHeight: "500px",
                minBodyHeight: "273px",
                padding: "dense",
                pageSizeOptions: appConst.rowsPerPageOptions.table,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: "#358600",
                  color: "#fff",
                  position: "relative"
                },
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: appConst.localizationVi.pagination,
              }}
              components={{
                Toolbar: (props) => (
                  <div className="w-100">
                    <MTableToolbar {...props} />
                  </div>
                ),
                
                Header: () => {
                  return (
                    <TableHead className={clsx(classes.header)}>
                      <TableRow>
                        {props?.item?.id && props?.isEnabledBtn && <TableCell className={clsx(classes.borderRight)}
                                                                              rowSpan={2}>{t("general.reevaluateAcronyms")}</TableCell>}
                        <TableCell className={clsx(classes.borderRight)} rowSpan={2}>{t("Asset.stt")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)}
                                   rowSpan={2}>{t("Asset.managementCode")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("Asset.code")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("Asset.name")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_100)}
                                   rowSpan={2}>{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                        {props?.item?.id && props?.isEnabledBtn &&
                          <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                     rowSpan={2}>{t("Asset.statusDgl")}</TableCell>}
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("Asset.receive_date")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("InventoryCountVoucher.store")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   rowSpan={2}>{t("InstrumentToolsList.useDepartment")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   colSpan={3}>{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   colSpan={3}>{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                        <TableCell className={clsx(classes.borderRight, classes.mw_70)}
                                   colSpan={3}>{t("InventoryCountVoucher.deviant")}</TableCell>
                        <TableCell className={clsx(classes.colorWhite, classes.mw_70)}
                                   rowSpan={2}>{t("InventoryCountVoucher.note")}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.quanlity")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.originalCost")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.quanlity")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.originalCost")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.quanlity")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.originalCost")}</TableCell>
                        <TableCell
                          className={clsx(classes.borderRight, classes.mw_100)}>{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                      </TableRow>
                    </TableHead>
                  );
                },
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid item md={3} sm={12} xs={12}>
          <AsynchronousAutocompleteSub
            searchFunction={searchByPage}
            typeReturnFunction='category'
            searchObject={searchObject}
            label={t('InventoryCountVoucher.InventoryCountBoard')}
            listData={listInventoryCountBoard}
            setListData={setListInventoryCountBoard}
            displayLable={"name"}
            onSelect={handleChangeSelectInventoryBoard}
            value={inventoryBoard}
            InputProps={{
              readOnly: true,
            }}
            disabled={
              props?.item?.isView
              || (
                props?.item?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder
                && props?.item?.id
              )
            }
            filterOptions={(options, params) => {
              params.inputValue = params.inputValue.trim();
              let filtered = filterAutocomplete(options, params);
              return filtered;
            }}
            noOptionsText={t("general.noOption")}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <MaterialTable
            data={listInventoryPerson}
            columns={columnsInventoryCount}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },
              
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{width: "100%"}}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>
    </div>
  );
}
