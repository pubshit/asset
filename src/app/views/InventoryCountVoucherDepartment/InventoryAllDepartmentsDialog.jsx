import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { addAsset, getAsset, updateAsset } from './InventoryCountVoucherDepartmentService';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import {
  searchByPageAssetDocument,
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeAssetDocument,
} from '../Asset/AssetService'
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../../../styles/views/_loadding.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InventoryCountDepartmentsScrollableTabsButtonForce from './InventoryCountDepartmentsScrollableTabsButtonForce'
import { appConst, variable } from "../../appConst";
import AppContext from "app/appContext";
import AssetTransferDialog from "../AssetLiquidate/AssetLiquidateDialog";
import AssetReevaluateDialog from "../AssetReevaluate/AssetReevaluateDialog";
import {PaperComponent} from "../Component/Utilities"

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class InventoryAllDepartmentsDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    assets: [],
    inventoryLiquidateAssets: [],
    title: null,
    inventoryCountDate: new Date(),
    department: null,
    inventoryCountStatus: null,
    shouldOpenDepartmentPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    totalElements: 0,
    departmentId: '',
    asset: {},
    shouldOpenSelectPersonPopup: false,
    assetDocumentList: [],
    shouldOpenPopupAssetFile: false,
    listAssetDocumentId: [],
    loading: false,
    documentType: 6, // hồ sơ kiểm kê,
    assetReevaluate: {},
    shouldOpenReevaluateDialog: false,
    selectedValue: null,
    isLiquidate: false,
    dataAssetTable: [],
    isEnabledBtn: false,
    inventoryCountPersons: [],
    timer: null,
    listItemEdit: []
  };

  setSelectedValue = (id) => {
    this.setState({
      selectedValue: id,
    })
  }

  handleSetAssetReevaluate = (item) => {
    this.setState({
      assetReevaluate: item
    })
  }

  handleOpenAssetReevaluateDialog = () => {
    if (this.state.assetReevaluate?.id) {
      this.setState({
        shouldOpenReevaluateDialog: true,
      })
    }
    else {
      toast.warning("Chưa có tài sản nào được chọn")
    }
  }

  handleCloseAssetReevaluateDialog = () => {
    this.setState({
      shouldOpenReevaluateDialog: false,
    })
  }

  handleChange = (event, source) => {
    this.setState({ [event?.target?.name]: event?.target?.value });
  };

  validateAsset = () => {
    let { t } = this.props;
    let { assets } = this.state;
    let check = false;
    assets?.some(asset => {
      let isStore = (
        appConst.listStatusAssets.NHAP_KHO.indexOrder === asset?.status?.indexOrder
        || appConst.listStatusAssets.LUU_MOI.indexOrder === asset?.status?.indexOrder
      );
      
      if (isStore && !asset?.store?.id) {
        console.log(asset);
        
        toast.warning((`Tài sản mã: ${asset?.code} có trạng thái của kho không hợp lệ`));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryOriginalCost && asset?.inventoryOriginalCost !== 0) {
        toast.warning(t("InventoryCountVoucher.messages.emptyOriginalCost"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryOriginalCost < 0) {
        toast.warning(t("InventoryCountVoucher.messages.originalCostError"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (!asset?.inventoryCarryingAmount && asset?.inventoryCarryingAmount !== 0) {
        toast.warning(t("InventoryCountVoucher.messages.emptyCaryingAmount"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
      if (asset?.inventoryCarryingAmount < 0) {
        toast.warning(t("InventoryCountVoucher.messages.caryingAmountError"));
        toast.clearWaitingQueue();
        check = true;
        return true;
      }
    })
    return check;
  }

  validateSubmit = () => {
    let {
      id,
      assets,
      hoiDongId,
      inventoryCountStatus,
      inventoryCountPersons,
      title,
    } = this.state;
    let { t } = this.props;
    if (!title) {
      toast.warning(t("InventoryCountVoucher.messages.emptyTitle"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!inventoryCountStatus) {
      toast.warning(t("InventoryCountVoucher.messages.emptyStatus"));
      toast.clearWaitingQueue();
      return false;
    }
    if (assets?.length === 0) {
      toast.warning(t("InventoryCountVoucher.messages.emptyAsset"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!id && hoiDongId && inventoryCountPersons?.length === 0) {
      toast.warning(t("InventoryCountVoucher.messages.emptyCouncil"));
      toast.clearWaitingQueue();
      return false;
    }
    if (!hoiDongId && !id) {
      toast.warning(t("InventoryCountVoucher.messages.noCouncilSelected"));
      toast.clearWaitingQueue();
      return false;
    }
    if (this.validateAsset()) return;

    return true;
  }

  convertDataSubmit = () => {
    return {
      id: this.state?.id,
      assets: this.state?.assets?.map(i => {
        return {
          assetStatusId: i?.status?.id,
          assetId: i?.assetId,
          accountantQuantity: i?.accountantQuantity,
          accountantOriginalCost: i?.accountantOriginalCost,
          accountantCarryingAmount: i?.accountantCarryingAmount,
          inventoryQuantity: i?.inventoryQuantity,
          inventoryOriginalCost: i?.inventoryOriginalCost,
          inventoryCarryingAmount: i?.inventoryCarryingAmount,
          differenceQuantity: i?.differenceQuantity,
          differenceOriginalCost: i?.differenceOriginalCost,
          differenceCarryingAmount: i?.differenceCarryingAmount,
          departmentId: i?.asset?.useDepartmentId || i?.asset?.departmentId,
          note: i?.note,
          storeId: i?.storeId,
          usedCondition: i?.usedCondition,
        }
      }),
      hoiDongId: this.state?.hoiDongId,
      inventoryCountStatusId: this.state?.inventoryCountStatus?.id || this.state?.inventoryCountStatusId,
      title: this.state?.title,
      inventoryCountDate: this.state?.inventoryCountDate,
      inventoryCountPersons: this.state?.inventoryCountPersons
        ? this.state?.inventoryCountPersons?.map(item => ({
          departmentId: item?.departmentId,
          departmentName: item?.departmentName,
          personId: item?.personId,
          personName: item?.personName,
          position: item?.position,
          role: item?.role,
        }))
        : []
    }
  }

  handleFormSubmit = () => {
    let { setPageLoading } = this.context;
    let { id, shouldOpenLiquidateDialog } = this.state;
    let { t, handleClose } = this.props;
    let dataSubmit = this.convertDataSubmit();
    if (!this.validateSubmit() && !shouldOpenLiquidateDialog) return;

    setPageLoading(true)
    if (id) {
      updateAsset(dataSubmit)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            toast.success(t("InventoryCountVoucher.updateSuccess"));
            this.setState({
              id: data?.data?.id,
              isEnabledBtn: data.data?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
              isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
                === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
            })
            if (this.props?.updatePageData) {
              this.props.updatePageData()
            }
            handleClose();
          } else {
            toast.warning(data.message)
          }
          setPageLoading(false)
        })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
          setPageLoading(false)
        })
    } else {
      addAsset(dataSubmit).then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          this.setState({
            id: data?.data?.id,
            isEnabledBtn: data.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
            isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder,
          })
          toast.success(t("InventoryCountVoucher.addSuccess"));
          this.setState({
            id: data?.data?.id,
            isLiquidate: data?.data?.inventoryCountStatus?.indexOrder
              === appConst.listStatusInventory[2].indexOrder
          });
          if (this.props?.updatePageData) {
            this.props.updatePageData()
          }
          handleClose();
        } else {
          toast.warning(data.message)
        }
        setPageLoading(false)
      })
        .catch((error) => {
          toast.error(this.props.t("general.error"))
          setPageLoading(false)
        })
    }
  };

  getAssetsNotInventoried = async () => {
    try {

    } catch (error) {

    }
  }

  //nếu đã kiểm kê thì không gọi list chưa lưu detail
  componentWillMount() {
    let { item } = this.props;
    if (item?.isAll) {
      this.getAssetAllDepartment();
    } else {
      if (item?.assets) {
        this.props.item?.assets.sort((a, b) => (a.id - b.id));
      }
      if (item?.inventoryCountStatus) {
        if (item?.inventoryCountStatus.indexOrder === appConst.listStatusInventory[2].indexOrder) {
          this.setState({
            isEnabledBtn: true,
            isLiquidate: true
          })
        }
      }

      this.setState({
        ...this.props.item,
        dataAssetTable: this.props?.item?.assets
      });
    }
  }

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true
    });
  };

  getAssetAllDepartment = () => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    let inventoryCountAssets = [];

    let searchObject = {}
    searchObject.assetClass = appConst.assetClass.TSCD
    getAsset(searchObject)
      .then(({ data }) => {
        setPageLoading(false)
        if (appConst.CODE.SUCCESS !== data?.code) {
          toast.warning(data?.message)
          return;
        }
        if (data?.data?.length > 0) {
          data.data.forEach(asset => {
            let inventoryCountAsset = { ...asset };
            let status = {
              id: asset?.statusId,
              indexOrder: asset?.statusIndexOrder,
              name: asset?.statusName,
            }
            let store = {
              id: asset?.storeId,
              name: asset?.storeName
            }
            inventoryCountAsset.asset = asset;
            inventoryCountAsset.assetId = asset?.assetId;
            inventoryCountAsset.currentStatus = asset.statusName || "";
            inventoryCountAsset.status = asset?.statusId ? status : null;
            inventoryCountAsset.assetStatusId = asset.statusId;
            inventoryCountAsset.note = '';
            inventoryCountAsset.inventoryQuantity = 1;
            inventoryCountAsset.inventoryOriginalCost = (asset?.originalCost || 0);
            inventoryCountAsset.inventoryCarryingAmount = (asset?.carryingAmount || 0);
            inventoryCountAsset.accountantQuantity = 1;
            inventoryCountAsset.accountantOriginalCost = (asset?.originalCost || 0);
            inventoryCountAsset.accountantCarryingAmount = (asset?.carryingAmount || 0);
            inventoryCountAsset.differenceQuantity = inventoryCountAsset.accountantQuantity
              - inventoryCountAsset.inventoryQuantity;
            inventoryCountAsset.differenceOriginalCost = inventoryCountAsset.accountantOriginalCost
              - inventoryCountAsset.inventoryOriginalCost;
            inventoryCountAsset.differenceCarryingAmount = inventoryCountAsset.accountantCarryingAmount
              - inventoryCountAsset.inventoryCarryingAmount;
            inventoryCountAsset.store = asset?.storeId ? store : null;
            inventoryCountAsset.storeId = asset?.storeId;
            inventoryCountAsset.isStore = [
              appConst.listStatusAssets.NHAP_KHO.indexOrder,
              appConst.listStatusAssets.LUU_MOI.indexOrder,
              appConst.listStatusAssets.DA_HONG.indexOrder,
            ].includes(asset?.statusIndexOrder);

            inventoryCountAssets.push(inventoryCountAsset);
          });
          this.setState({
            dataAssetTable: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
            assets: inventoryCountAssets.sort((a, b) => (b.asset?.code).localeCompare(a.asset?.code)),
          },);
        }
        else {
          this.setState({
            dataAssetTable: [],
          },);
        }
      })
      .catch((error) => {
        console.log(error)
        toast.error(this.props.t("general.error"))
        setPageLoading(false)
      })
  }

  selectInventoryCount = (item) => {
    if (item.indexOrder === appConst.listStatusInventory[0].indexOrder && !this.state.inventoryCountDate) {
      this.setState({
        inventoryCountDate: new Date()
      });
    }
    else {
      this.setState({
        inventoryCountDate: this.state.inventoryCountDate
      });
    }
    this.setState({
      inventoryCountStatus: item,
      inventoryCountStatusId: item?.id
    });
  }

  selectStatus = (assetStatus, rowData) => {
    let { assets } = this.state;
    assets[rowData?.tableData?.id].status = assetStatus
    assets[rowData?.tableData?.id].assetStatusId = assetStatus?.id
    assets[rowData?.tableData?.id].isStore = [
      appConst.listStatusAssets.NHAP_KHO.indexOrder,
      appConst.listStatusAssets.LUU_MOI.indexOrder,
      appConst.listStatusAssets.DA_HONG.indexOrder,
    ].includes(assetStatus?.indexOrder);
    this.setState({ assets });

    this.handleCheckListAsset(assets[rowData?.tableData?.id]);
    this.debounce();
  }

  handleRowDataCellChange = (rowData, event) => {
    let { assets } = this.state;
    let item = assets[rowData?.tableData?.id];
    let name = event?.target?.name;
    if (name) {
      if (variable.listInputName.note === name) {
        item.note = event.target.value;
        this.setState({ assets })
      }
      if (variable.listInputName.inventoryOriginalCost === name) {
        let value = event.target.value;
        item.differenceOriginalCost = (value - item?.accountantOriginalCost);
        item.inventoryOriginalCost = (value);
        this.setState({ assets });
      }
      if (variable.listInputName.inventoryCarryingAmount === name) {
        let value = event.target.value;
        item.differenceCarryingAmount = (value - item?.accountantCarryingAmount);
        item.inventoryCarryingAmount = (value);
        this.setState({ assets });
      }
      if (variable.listInputName.inventoryQuantity === name) {
        let value = event.target.value;
        item.differenceQuantity = Number(value || 0) - Number(item?.accountantQuantity || 0);
        item.inventoryQuantity = value;
        if (+value === 0) {
          item.inventoryOriginalCost = 0;
          item.inventoryCarryingAmount = 0;
          item.differenceOriginalCost = (0 - item?.accountantOriginalCost);
          item.differenceCarryingAmount = (0 - item?.accountantCarryingAmount);
        }
        this.setState({ assets });
      }
    }
    this.handleCheckListAsset(item);
    this.debounce();
  };

  handleCheckListAsset = (assets) => {
    try {
      if (!assets) {
        return;
      }

      const { listItemEdit } = this.state;
      const existingIndex = listItemEdit.findIndex((item) => item?.assetId === assets?.assetId);

      if (existingIndex !== -1) {
        const updatedList = [...listItemEdit];
        updatedList[existingIndex] = assets;
        this.setState({ listItemEdit: updatedList });
      } else {
        this.setState((prevState) => ({
          listItemEdit: [...prevState.listItemEdit, assets],
        }));
      }
    } catch (error) {
    }
  };

  //sau 3h thì thực hiện lưu những tài sản đã được chỉnh sửa thông tin
  debounce = () => {
    clearTimeout(this.state.timer);
    const newTimer = setTimeout(this.handleSubmitAssetChange, 3000);
    this.setState({ timer: newTimer });
  }

  handleSubmitAssetChange = () => {
    //list tài sản
    console.log(this.state.listItemEdit)
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true
    });
  }

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false
    });
  }

  handleRowDataCellDelete = (rowData) => {
    let { attributes } = this.state;
    if (attributes?.length > 0) {
      for (let index = 0; index < attributes.length; index++) {
        if (attributes[index]?.attribute?.id === rowData.attribute?.id) {
          attributes.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ attributes }, function () {
    });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData?.id).then(({ data }) => {
      if (!data) {
        data = {};
      }
      this.setState({
        item: data,
        shouldOpenPopupAssetFile: true,
        isEditAssetDocument: true
      },
        () => {
          this.updatePageAssetDocument()
        });
    })
  }

  handleRowDataCellDeleteAssetFile = (rowData) => {
    deleteAssetDocumentById(rowData?.id).then(data => {
      this.updatePageAssetDocument()
    })
  }

  handleSetListInventoryPerson = (item) => {
    this.setState({ inventoryCountPersons: item || [] })
  }

  handleSetHoiDongId = (id) => {
    this.setState({ hoiDongId: id })
  }

  handleAddAssetDocumentItem = () => {
    getNewCodeAssetDocument().then((result) => {
      if (result?.data?.code) {
        let item = result.data;
        this.setState({
          item: item,
          shouldOpenPopupAssetFile: true,
        });
      }
    }).catch(() => {
      toast.warning(this.props.t("general.error_reload_page"));
    });
  }

  updatePageAssetDocument = () => {
    let searchObject = {}
    searchObject.inventoryCountVoucherId = this.props.item.id;
    searchObject.keyword = this.state.keyword
    searchObject.pageIndex = this.state.page + 1
    searchObject.documentType = this.state.documentType
    searchObject.pageSize = 100000;
    searchByPageAssetDocument(searchObject).then(({ data }) => {
      this.setState({
        assetDocumentList: [...data?.content],
        totalElements: data?.totalElements,
      }, () => {
      })
    })
  }

  getAssetDocumentId = (documentId) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    if (documentId) {
      listAssetDocumentId.push(documentId);
    };
    this.setState({
      listAssetDocumentId: listAssetDocumentId
    }, () => {
    })
  }

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;

    (listAssetDocumentId?.length > 0) && listAssetDocumentId.forEach(id => deleteAssetDocumentById(id));
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  selectStores = (store, rowData) => {
    let { assets } = this.state;
    assets[rowData?.tableData?.id].store = store
    assets[rowData?.tableData?.id].storeId = store?.id
    this.setState({ assets });

    this.handleCheckListAsset(assets[rowData?.tableData?.id]);
    this.debounce();
  };

  handleOpenLiquidateDialog = () => {
    let { assets, inventoryLiquidateAssets, isLiquidate } = this.state;
    let { t } = this.props;

    inventoryLiquidateAssets = assets?.filter((asset) =>
      asset?.status?.indexOrder === appConst.listStatusAssets.DE_XUAT_THANH_LY.indexOrder
      && !inventoryLiquidateAssets?.find(
        item => item?.asset?.id === asset?.asset?.id
      )
    )
    if (isLiquidate) {
      this.setState({
        shouldOpenLiquidateDialog: true,
        isInventoryLiquidateAsset: true,
        inventoryLiquidateAssets,
      });
    }
    else {
      toast.warning(t("InventoryCountVoucher.saveBeforeLiquidate"))
    }
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenLiquidateDialog: false,
      inventoryLiquidateAssets: []
    });
  }

  render() {
    let { open, t, i18n, item, handleOKEditClose } = this.props;
    let { loading, shouldOpenLiquidateDialog, } = this.state;
    return (
      <Dialog className="w-90 m-auto" open={open} PaperComponent={PaperComponent} maxWidth="xl" fullWidth scroll="paper" >
        <div className={clsx("wrapperButton", !loading && 'hidden')} >
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <ValidatorForm ref="form" onSubmit={shouldOpenLiquidateDialog ? () => { } : this.handleFormSubmit}
          class="validator-form-scroll-dialog">
          <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
            <h4 className="">
              {item?.id ? t('InventoryCountVoucherDepartment.updateDialog') : t('InventoryCountVoucherDepartment.addDialog')}
            </h4>
          </DialogTitle>
          <DialogContent >
            <Grid>
              <InventoryCountDepartmentsScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
                handleChange={this.handleChange}
                selectInventoryCount={this.selectInventoryCount}
                handleDepartmentPopupClose={this.handleDepartmentPopupClose}
                selectStatus={this.selectStatus}
                selectStores={this.selectStores}
                handleSelectPerson={this.handleSelectPerson}
                removePersonInlist={this.removePersonInlist}
                handleRowDataCellChange={this.handleRowDataCellChange}
                handleDateChange={this.handleDateChange}
                itemAssetDocument={this.state.item}
                shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
                handleAssetFilePopupClose={this.handleAssetFilePopupClose}
                handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
                handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                updatePageAssetDocument={this.updatePageAssetDocument}
                getAssetDocumentId={this.getAssetDocumentId}
                assetReevaluate={this.state.assetReevaluate}
                handleSetAssetReevaluate={this.handleSetAssetReevaluate}
                selectedValue={this.state.selectedValue}
                setSelectedValue={this.setSelectedValue}
                dataAssetTable={this.state.dataAssetTable}
                isEnabledBtn={this.state.isEnabledBtn}
                handleSetListInventoryPerson={this.handleSetListInventoryPerson}
                handleSetHoiDongId={this.handleSetHoiDongId}
              />
            </Grid>

            {shouldOpenLiquidateDialog && <>
              <AssetTransferDialog
                t={t}
                i18n={i18n}
                item={this.state}
                open={shouldOpenLiquidateDialog}
                handleClose={this.handleDialogClose}
                handleOKEditClose={this.props.handleOKEditClose}
                disabledDgl={true}
                isInventoryLiquidateAsset={true}
              />
            </>}

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={handleOKEditClose}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                disabled={!this.state.isEnabledBtn}
                className="mr-12"
                onClick={() => this.handleOpenAssetReevaluateDialog()}
              >
                {t('Dashboard.subcategory.reEvaluate')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-12"
                disabled={!this.state.isEnabledBtn}
                onClick={this.handleOpenLiquidateDialog}
              >
                {t('Asset.asset_liquidate')}
              </Button>
              {!item?.isView && <>
                {!this.state.isLiquidate && <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  className="mr-12"
                >
                  {t('general.save')}
                </Button>}
              </>}
            </div>
          </DialogActions>
        </ValidatorForm>
        {
          this.state.shouldOpenReevaluateDialog &&
          <AssetReevaluateDialog
            t={t}
            handleClose={() => this.handleCloseAssetReevaluateDialog()}
            open={this.state.shouldOpenReevaluateDialog}
            handleOKEditClose={this.handleOKEditClose}
            item={this.state.assetReevaluate}
            type="hasSelectedAsset"
            setSelectedValue={this.setSelectedValue}
            getDsPhieuDanhGiaOfVoucher={this.props?.getDsPhieuDanhGiaOfVoucher}
            assetVoucherIds={this.props?.assetVoucherIds}
            updateData={this.props?.updateData}
          />
        }
      </Dialog>
    );
  }
}
InventoryAllDepartmentsDialog.contextType = AppContext;
export default InventoryAllDepartmentsDialog;
