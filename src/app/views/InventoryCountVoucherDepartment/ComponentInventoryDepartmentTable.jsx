import React, { useContext } from "react";
import { Button, FormControl, Grid, Input, InputAdornment, TablePagination } from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ConfirmationDialog } from "egret";
import viLocale from "date-fns/locale/vi";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst, PRINT_TEMPLATE_MODEL } from "app/appConst";
import InventoryAllDepartmentsDialog from "./InventoryAllDepartmentsDialog";
import AssetsQRPrintNew from "../Asset/ComponentPopups/AssetsQRPrintNew";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import AppContext from "../../appContext";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { convertNumberPriceRoundUp, getTheHighestRole } from "app/appFunction";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";
import { LIST_ORGANIZATION } from "../../appConst";

function ComponentInventoryDepartmentTable(props) {
    const { currentOrg } = useContext(AppContext);
    const { INVENTORY_COUNT_ORG } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    const t = props.t;
    const i18n = props.i18n;
    let { openPrintQR, products, openExcel, item } = props?.item;

    const { organization } = getTheHighestRole();

    const newDate = new Date(item?.inventoryCountDate)
    const day = String(newDate?.getDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());

    const renderRole = (role) => {
        switch (role) {
            case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
                return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name;
            case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
                return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name;
            case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
                return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name;
            default:
                break;
        }
    };

    let dataView = {
        organization,
        isTSCD: true,
        isCCDC: false,
        department: item?.department?.text,
        inventoryCountDate: item?.inventoryCountDate ? `ngày ${day} tháng ${month} năm ${year}` : "",
        inventoryCountPersons: item?.inventoryCountPersons
            ?
            [...item?.inventoryCountPersons?.sort((a, b) => a?.role - b?.role)]?.map((i, x) => {
                return {
                    ...i,
                    index: x + 1,
                    role: renderRole(i?.role),
                    position: i?.position || "..".repeat(10)
                }
            })
            : [],
        assets: item?.assets?.map((row, index) => {
            let {
                accountantQuantity,
                accountantOriginalCost,
                inventoryQuantity,
                inventoryOriginalCost,
                accountantCarryingAmount,
                inventoryCarryingAmount,
                usedCondition
            } = row;

            const isGood = (appConst.OBJECT_USED_CONDITION.TOT.code === usedCondition) || "";
            const isMedium = (appConst.OBJECT_USED_CONDITION.TRUNG_BINH.code === usedCondition) || "";
            const isBroken = (appConst.OBJECT_USED_CONDITION.HONG.code === usedCondition) || "";
            let isExcess = (accountantQuantity - inventoryQuantity) > 0 ? (accountantQuantity - inventoryQuantity) || 0 : 0
            let isDeficiency = (accountantQuantity - inventoryQuantity) <= 0 ? Math.abs((accountantQuantity - inventoryQuantity) || 0) : 0
            return {
                ...row,
                index: index + 1,
                isGood,
                isMedium,
                isBroken,
                isExcess,
                isDeficiency,
                accountantOriginalCost: convertNumberPriceRoundUp(accountantOriginalCost) || 0,
                inventoryOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
                accountantCarryingAmount: convertNumberPriceRoundUp(accountantCarryingAmount) || 0,
                inventoryCarryingAmount: convertNumberPriceRoundUp(inventoryCarryingAmount) || 0,
                accountantQuantity: accountantQuantity || 1,
                inventoryQuantity: inventoryQuantity || 1,
                differenceQuantity: (inventoryQuantity - accountantQuantity) || 0,
                differenceOriginalCost: convertNumberPriceRoundUp((inventoryOriginalCost - accountantOriginalCost)) || 0,
                differenceCarryingAmount: convertNumberPriceRoundUp((inventoryCarryingAmount - accountantCarryingAmount)) || 0,
                carryingAmountRate: (((inventoryCarryingAmount) / (inventoryOriginalCost) || 0) * 100).toFixed(2)
            }
        }),
        sumAccountantQuantity: item?.assets?.reduce((total, item) => total + (item?.accountantQuantity || 1), 0),
        sumInventoryQuantityQuantity: item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity || 1), 0),
        sumAccountantOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantOriginalCost || 0), 0)) || 0,
        sumAccountantCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantCarryingAmount || 0), 0)) || 0,
        sumInventoryOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost || 0), 0)) || 0,
        sumInventoryCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount || 0), 0)) || 0,
        sumDifferenceQuantity: (item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity - item?.accountantQuantity || 0), 0)),
        sumDifferenceOriginalCost: convertNumberPriceRoundUp((item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost - item?.accountantOriginalCost || 0), 0))) || 0,
        sumDifferenceCarryingAmount: convertNumberPriceRoundUp((item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount - item?.accountantCarryingAmount || 0), 0))) || 0,
        arrUnitPrice: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.asset?.originalCost / 1), 0)) || 0,
        day,
        month,
        year,
    }

    const isMongCai = currentOrg?.code === LIST_ORGANIZATION.TTYTTP_MONG_CAI.code;
    return (
        <div>
            <Grid container spacing={3} className="alignItemsFlexEnd spaceBetween">
                <Grid item md={4} sm={12} xs={12} className="">
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            props.handleEditItem({
                                startDate: new Date(),
                                endDate: new Date(),
                                isView: false,
                                isAll: true
                            });
                        }}
                    >
                        {t("InventoryCountVoucher.add")}
                    </Button>
                    {/* <Button
                        className="align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleExcel}
                    >
                        {t("general.exportToExcel")}
                    </Button> */}
                    {props.item?.shouldOpenConfirmationExcel && (
                        <ConfirmationDialog
                            title={t("general.confirm")}
                            open={props.item?.shouldOpenConfirmationExcel}
                            onConfirmDialogClose={props.handleDialogClose}
                            onYesClick={props.exportToExcel}
                            text={t("general.exportToExcelConfirm")}
                            agree={t("general.agree")}
                            cancel={t("general.cancel")}
                        />
                    )}

                </Grid>
                <Grid item md={8} sm={12} xs={12} className="flex alignItemsFlexEnd" >
                    <Grid item md={3} sm={3} xs={3} className="pr-16" >
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                            <KeyboardDatePicker
                                className="w-100"
                                id="mui-pickers-date"
                                label={t("InventoryCountVoucher.inventoryFromDate")}
                                type="text"
                                autoOk={true}
                                format="dd/MM/yyyy"
                                name={"inventoryCountDateBottom"}
                                value={props.item?.inventoryCountDateBottom}
                                invalidDateMessage={t("general.invalidDateFormat")}
                                minDateMessage={t("general.minDateDefault")}
                                maxDateMessage={props.item?.inventoryCountDateTop ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                maxDate={props.item?.inventoryCountDateTop ? (new Date(props.item?.inventoryCountDateTop)) : undefined}
                                clearable
                                onChange={(date) => props.handleDateChange(date, "inventoryCountDateBottom")}
                                clearLabel={t("general.remove")}
                                cancelLabel={t("general.cancel")}
                                okLabel={t("general.select")}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={3} sm={3} xs={3} className="pr-16">
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                            <KeyboardDatePicker
                                className="w-100"
                                id="mui-pickers-date"
                                label={t("InventoryCountVoucher.inventoryToDate")}
                                type="text"
                                autoOk={true}
                                format="dd/MM/yyyy"
                                name={"inventoryCountDateTop"}
                                value={props.item?.inventoryCountDateTop}
                                invalidDateMessage={t("general.invalidDateFormat")}
                                minDateMessage={props.item?.inventoryCountDateBottom ? t("general.minDateToDate") : t("general.minYearDefault")}
                                maxDateMessage={t("general.maxDateDefault")}
                                minDate={props.item?.inventoryCountDateBottom ? (new Date(props.item?.inventoryCountDateBottom)) : undefined}
                                clearable
                                onChange={(date) => props.handleDateChange(date, "inventoryCountDateTop")}
                                clearLabel={t("general.remove")}
                                cancelLabel={t("general.cancel")}
                                okLabel={t("general.select")}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item md={6} sm={6} xs={6}>
                        <FormControl fullWidth>
                            <Input
                                className="search_box"
                                onChange={props.handleTextChange}
                                onKeyDown={props.handleKeyDownEnterSearch}
                                onKeyUp={props.handleKeyUp}
                                placeholder={t("InventoryCountVoucher.searchByName")}
                                id="search_box"
                                value={props.item?.keyword}
                                startAdornment={
                                    <InputAdornment position="end">
                                        <SearchIcon
                                            onClick={() => props?.search(props.item?.keyword)}
                                            className="searchTable"
                                        />
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <div>
                        {props.item?.shouldOpenPrintDialog && (
                            isMongCai ? (
                                <PrintPreviewTemplateDialog
                                    t={t}
                                    handleClose={props.handleDialogClose}
                                    open={props.item?.shouldOpenPrintDialog}
                                    item={props.item?.item}
                                    title={t("InventoryCountVoucher.printTitle")}
                                    model={PRINT_TEMPLATE_MODEL.FIXED_ASSET_MANAGEMENT.INVENTORY_COUNT_VOUCHER}
                                />
                            ) : (
                                <PrintMultipleFormDialog
                                    t={t}
                                    i18n={props.item?.i18n}
                                    handleClose={props.handleDialogClose}
                                    open={props.item?.shouldOpenPrintDialog}
                                    item={dataView || props.item?.item}
                                    title={t("InventoryCountVoucher.printTitle")}
                                    urls={[
                                        ...INVENTORY_COUNT_ORG.GENERAL,
                                        ...(INVENTORY_COUNT_ORG[currentOrg?.printCode] || []),
                                    ]}
                                />
                            )
                        )}
                        {props.item?.shouldOpenEditorDialog && (
                            <InventoryAllDepartmentsDialog
                                t={t}
                                i18n={props.item?.i18n}
                                handleClose={props.handleDialogClose}
                                open={props.item?.shouldOpenEditorDialog}
                                handleOKEditClose={props.handleOKEditClose}
                                item={props.item?.item}
                                getDsPhieuDanhGiaOfVoucher={props?.getDsPhieuDanhGiaOfVoucher}
                                assetVoucherIds={props?.assetVoucherIds}
                                updateData={props?.updateData}
                                updatePageData={props?.updatePageData}
                            />
                        )}

                        {props.item?.shouldOpenConfirmationDialog && (
                            <ConfirmationDialog
                                title={t("general.confirm")}
                                open={props.item?.shouldOpenConfirmationDialog}
                                onConfirmDialogClose={props.handleDialogClose}
                                onYesClick={props.handleConfirmationResponse}
                                text={t("general.deleteConfirm")}
                                agree={t("general.agree")}
                                cancel={t("general.cancel")}
                            />
                        )}
                        {openPrintQR && (
                            <AssetsQRPrintNew
                                t={t}
                                i18n={i18n}
                                handleClose={props.handleCloseQrCode}
                                open={openPrintQR}
                                items={products}
                            />
                        )}
                    </div>
                    <MaterialTable
                        title={t("general.list")}
                        data={props.item?.itemList}
                        columns={props.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            draggable: false,
                            // selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor:
                                    props?.item?.idVoucher === rowData.id
                                        ? "#ccc" :
                                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "270px",
                            minBodyHeight: "270px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                paddingLeft: 10,
                                paddingRight: 10,
                                textAlign: "center",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            let listIds = []
                            // eslint-disable-next-line no-unused-expressions
                            rows?.forEach(item => listIds.push(item.id))
                            props.setState({ ids: [...listIds] })
                        }}
                        onRowClick={(e, rowData) => {
                            return props?.setItemState(rowData);
                        }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        component="div"
                        labelRowsPerPage={t("general.rows_per_page")}
                        count={props.item?.totalElements}
                        rowsPerPage={props.item?.rowsPerPage}
                        labelDisplayedRows={({ from, to, count }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                            }`
                        }
                        page={props.item?.page}
                        backIconButtonProps={{
                            "aria-label": "Previous Page",
                        }}
                        nextIconButtonProps={{
                            "aria-label": "Next Page",
                        }}
                        onpPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

export default ComponentInventoryDepartmentTable;
