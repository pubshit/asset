import React from "react";
import { TableCell, TableHead, TablePagination, TableRow, makeStyles } from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import clsx from "clsx";
import { convertNumberPrice } from "app/appFunction";
import moment from "moment";
import { appConst } from "app/appConst";

function ListAssetTable(props) {
    const { t } = props
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            width: "100%",
            backgroundColor: theme.palette.background.paper,
        },
        header: {
            zIndex: 1,
            background: "#358600",
            position: "sticky",
            top: 0,
            left: 0,
            "& .MuiTableCell-head": {
                padding: "4px" // <-- arbitrary value
            }
        },
        borderRight: {
            borderRight: "1px solid white !important",
            color: "#ffffff",
            textAlign: "center"
        },
        colorWhite: {
            color: "#ffffff",
            textAlign: "center"
        },
        mw_200: {
            minWidth: "200px"
        },
        mw_160: {
            minWidth: "160px"
        },
        mw_120: {
            minWidth: "120px"
        },
        mw_100: {
            minWidth: "90px"
        },
        mw_70: {
            minWidth: "70px"
        },
        tabPanel: {
            "& .MuiBox-root": {
                paddingLeft: "0",
                paddingRight: "0"
            }
        },
        textRight: {
            "& input": {
                textAlign: "right"
            }
        }
    }));
    const classes = useStyles();
    let columns = [
        {
            title: t("Asset.stt"),
            field: "",
            maxWidth: "50px",
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) =>
                (props.item?.pageAsset * props.item?.rowsPerPageAsset + (rowData.tableData.id + 1)),
        },
        {
            title: t("Asset.managementCode"),
            field: "managementCode",
            minWidth: "100px",
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.code"),
            field: "assetCode",
            align: "left",
            minWidth: "100px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("Asset.name"),
            field: "assetName",
            align: "left",
            minWidth: 200,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.currentStatus"),
            field: "currentStatusName",
            minWidth: "160px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("InventoryCountStatus.CountAsset.status"),
            field: "updateStatusName",
            minWidth: "160px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
        },
        {
            title: t("Asset.statusDgl"),
            field: "dglTrangthai",
            minWidth: "160px",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "left",
            },
            render: (rowData) => {
                return appConst.statusAssetReevaluate.find(item => item?.id === rowData?.dglTrangthai)?.name
            }
        },
        {
            title: t("Asset.receive_date"),
            field: "dateOfReception",
            minWidth: 120,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) =>
                rowData?.dateOfReception ? (
                    <span>
                        {moment(rowData?.dateOfReception).format("DD/MM/YYYY")}
                    </span>
                ) : (
                    ""
                ),
        },
        {
            title: t("InventoryCountVoucher.store"),
            field: "storeName",
            minWidth: "200px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("InstrumentToolsList.useDepartment"),
            field: "departmentName",
            align: "left",
            minWidth: 160,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => rowData?.departmentName
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => 1
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantOriginalCost ? rowData.accountantOriginalCost : 0)),
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "accountantCarryingAmount",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(Math.ceil(rowData?.accountantCarryingAmount ? rowData.accountantCarryingAmount : 0)),
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "inventoryOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => 1
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "inventoryOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryOriginalCost ? rowData?.inventoryOriginalCost : 0)
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "inventoryCarryingAmount",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryCarryingAmount ? rowData?.inventoryCarryingAmount : 0)
        },
        {
            title: t("InventoryCountVoucher.quanlity"),
            field: "accountantOriginalCost",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "center",
            },
            render: (rowData) => 0
        },
        {
            title: t("InventoryCountVoucher.originalCost"),
            field: "nguyenGiaChenhLech",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryOriginalCost - rowData?.accountantOriginalCost),
        },
        {
            title: t("InventoryCountVoucher.carryingAmount"),
            field: "giaTriConLaiLech",
            minWidth: "100px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: "5px",
                paddingRight: "5px",
                textAlign: "right",
            },
            render: (rowData) => convertNumberPrice(rowData?.inventoryCarryingAmount - rowData?.accountantCarryingAmount),
        },
        {
            title: t("InventoryCountVoucher.note"),
            field: "note",
            minWidth: "150px",
            headerStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            cellStyle: {
                paddingLeft: 10,
                paddingRight: "10px",
                textAlign: "left",
            },
        },
    ];
    return (<div>
        <MaterialTable
            data={props?.itemAsset?.reverse()}
            columns={columns}
            options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                maxBodyHeight: "500px",
                minBodyHeight: "273px",
                padding: "dense",
                rowStyle: (rowData) => ({
                    backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    position: "relative"
                },
            }}
            localization={{
                body: {
                    emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                    )}`,
                },
                pagination: {
                    labelDisplayedRows: "{from}-{to} trong {count}",
                    labelRowsPerPage: "Số bản ghi mỗi trang:",
                    firstTooltip: "Trang đầu",
                    previousTooltip: "Trang trước",
                    nextTooltip: "Trang tiếp",
                    lastTooltip: "Trang cuối",
                    labelRowsSelect: "hàng mỗi trang",
                },
            }}
            components={{
                Toolbar: (props) => (
                    <div className="w-100">
                        <MTableToolbar {...props} />
                    </div>
                ),

                Header: () => {
                    return (
                        <TableHead className={clsx(classes.header)}>
                            <TableRow>
                                {props?.item?.id && props?.isEnabledBtn && <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("general.reevaluateAcronyms")}</TableCell>}
                                <TableCell className={clsx(classes.borderRight)} rowSpan={2} >{t("Asset.stt")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} rowSpan={2} >{t("Asset.managementCode")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("Asset.code")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_200)} rowSpan={2} >{t("Asset.name")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_160)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.currentStatus")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_160)} rowSpan={2} >{t("InventoryCountStatus.CountAsset.status")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_160)} rowSpan={2} >{t("Asset.statusDgl")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_120)} rowSpan={2} >{t("Asset.receive_date")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.store")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} rowSpan={2} >{t("InstrumentToolsList.useDepartment")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingAccountingBooks")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.accordingInventory")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_70)} colSpan={3} >{t("InventoryCountVoucher.deviant")}</TableCell>
                                <TableCell className={clsx(classes.colorWhite, classes.mw_70)} rowSpan={2} >{t("InventoryCountVoucher.note")}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.quanlity")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.originalCost")}</TableCell>
                                <TableCell className={clsx(classes.borderRight, classes.mw_100)} >{t("InventoryCountVoucher.carryingAmount")}</TableCell>
                            </TableRow>
                        </TableHead>
                    );
                },
            }}
            onSelectionChange={(rows) => {
                this.data = rows;
            }}
        />
        <TablePagination
            align="left"
            className="px-16"
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            component="div"
            labelRowsPerPage={t("general.rows_per_page")}
            count={props.item?.totalElementsAsset}
            rowsPerPage={props.item?.rowsPerPageAsset}
            labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
            }
            page={props.item?.pageAsset}
            backIconButtonProps={{
                "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
                "aria-label": "Next Page",
            }}
            onPageChange={props.handleChangePageAsset}
            onRowsPerPageChange={props.setRowsPerPageAsset}
        />
    </div>);
}

export default ListAssetTable;