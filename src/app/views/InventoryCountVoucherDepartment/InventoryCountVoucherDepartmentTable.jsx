/* eslint-disable array-callback-return */
import React from "react";
import {
  IconButton,
  Icon,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
  Checkbox,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getCountByStatus,
  getListQRCode,
  getAssetByVouchers,
  getAssetByVoucher,
  getInfoSumary,
} from "./InventoryCountVoucherDepartmentService";
import { Breadcrumb } from "egret";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import ComponentInventoryDepartmentTable from "./ComponentInventoryDepartmentTable";
import { getByPageDsPhieuDanhGiaOfVoucher } from "../AssetReevaluate/AssetReevaluateService";
import ListAssetTable from "./ListAssetTable";
import { formatDateDtoMore, getTheHighestRole, isValidDate } from "app/appFunction";
import { TabPanel } from "../Component/Utilities";
import { LightTooltip } from "../Component/Utilities/LightToolTip";
import { handleThrowResponseMessage, isSuccessfulResponse } from "../../appFunction";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});
function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  const hasDeletePermission = props.hasDeletePermission;
  const hasEditPermission = props.hasEditPermission;

  return (
    <div className="none_wrap">
      {hasEditPermission
        &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder,
        ].includes(item?.statusIndexOrder)
        && (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {hasDeletePermission
        &&
        [
          appConst.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder,
        ].includes(item?.statusIndexOrder)
        && (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      <LightTooltip
        title={t("general.print")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.print)}>
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
      {
        <LightTooltip
          title={t("general.qrIcon")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.qrcode)}
          >
            <Icon fontSize="small" color="primary">
              filter_center_focus
            </Icon>
          </IconButton>
        </LightTooltip>
      }
    </div>
  );
}

class InventoryCountVoucherTable extends React.Component {
  constructor(props) {
    super(props)
    this.anchorRef = React.createRef();
  }
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    checkAll: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenConfirmationExcel: false,
    shouldOpenPrintDialog: false,
    hasEditPermission: false,
    hasDeletePermission: false,
    hasCreatePermission: false,
    inventoryCountDateBottom: null,
    inventoryCountDateTop: null,
    ids: [],
    tabValue: 0,
    assetVoucherIds: [],
    idItem: null,
    itemAsset: [],
    openPrintQR: false,
    products: [],
    openExcel: false,
    rowsPerPageAsset: appConst.rowsPerPageOptions.popup[0],
    pageAsset: 0,
    totalElementsAsset: 0,
  };
  numSelected = 0;
  rowCount = 0;

  handleToggleOpenExcel = (e) => {
    const { openExcel } = this.state;
    this.setState((prevState) => ({
      openExcel: !openExcel,
    }));
  };

  handleChangeTabValue = (event, newValue) => {
    let { setPageLoading } = this.context;
    setPageLoading(true)
    this.setState({
      page: 0,
      rowsPerPage: this.state.rowsPerPage
    })
    if (appConst?.tabInventory?.tabAll === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: null,
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabPlan === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[0].indexOrder
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabTakingInventory === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[1].indexOrder
      },
        () => this.updatePageData()
      )
    }
    if (appConst?.tabInventory?.tabInventory === newValue) {
      this.setState({
        itemList: [],
        tabValue: newValue,
        inventoryCountStatusIndexOrder: appConst.listStatusInventory[2].indexOrder
      },
        () => this.updatePageData()
      )
    }
  }

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search()
  }

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleChangePageAsset = (event, newPage) => {
    this.setState({ pageAsset: newPage }, () => {
      this.updatePageDataAsset();
    })
  };

  setRowsPerPageAsset = (event) => {
    this.setState({ rowsPerPageAsset: event.target.value, pageAsset: 0 }, () => {
      this.updatePageDataAsset();
    });
  };

  search = () => {
    this.setPage(0)
  }

  updatePageDataAsset = async () => {
    let { setPageLoading } = this.context
    try {
      setPageLoading(true)
      let {
        rowsPerPageAsset,
        pageAsset,
        idVoucher
      } = this.state;

      let searchObject = {};

      searchObject.pageIndex = pageAsset + 1;
      searchObject.pageSize = rowsPerPageAsset;
      searchObject.id = idVoucher;

      const data = await getAssetByVoucher(searchObject);
      let { content, totalElements } = data?.data?.data;
      this.setState({
        itemAsset: content || [],
        totalElementsAsset: totalElements || 0,
      });
    } catch (error) {
      toast.error("Xảy ra lỗi")
    } finally {
      setPageLoading(false)
    }
  }

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { selectedList } = this.state;
    let searchObject = {};
    searchObject.type = appConst.assetTypeInventoryCountObject.ALL.code;
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = formatDateDtoMore(this.state.inventoryCountDateBottom, "start");
    searchObject.inventoryCountDateTop = formatDateDtoMore(this.state.inventoryCountDateTop);
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.assetClass = appConst.assetClass.TSCD;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;
    setPageLoading(true);
    searchByPage(searchObject).then(({ data }) => {
      if (appConst.CODE.SUCCESS === data?.code) {
        let countItemChecked = 0
        const itemList = data?.data?.content?.map(item => {
          if (selectedList?.find(checkedItem => checkedItem === item.id)) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
          }
        }
        )

        this.setState({
          itemList,
          totalElements: data?.data?.totalElements,
          ids: [],
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0
        })
      } else {
        toast.warning(data?.message)
      }
    })
      .catch(() => {
        toast.error(this.props.t("general.error"))
      })
      .finally(() => {
        setPageLoading(false)
      })
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenPrintDialog: false,
      shouldOpenConfirmationExcel: false,
    }, () => {
      this.getCountStatus();
      this.updatePageData();
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationExcel: false,
      inventoryCountDateBottom: null,
      inventoryCountDateTop: null,
      keyword: ""
    }, () => {
      this.getCountStatus();
      this.updatePageData();
    });
  };

  handleDeleteAssetTransfer = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleEditAssetTransfer = (item) => {
    getItemById(item.id).then(({ data }) => {
      this.setState({
        item: data?.data,
        shouldOpenEditorDialog: true,
      });
    });
  };

  handleConfirmationResponse = () => {
    deleteItem(this.state.id)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          toast.success(this.props.t("general.deleteSuccess"))
          if ((this.state?.itemList.length - 1 === 0) && this.state.page !== 0) {
            this.setState({
              page: this.state.page - 1
            },
              () => this.updatePageData()
            )
          }
          this.updatePageData();
          this.handleDialogClose();
        } else {
          toast.warning(data.message)
        }
      }).
      catch((error) => {
        toast.error(this.props.t("general.error"))
      })
  };

  componentDidMount() {
    this.getRoleCurrentUser();
    this.getCountStatus();
    this.setState({
      inventoryCountStatusIndexOrder: null
    },
      () => this.updatePageData()
    );
  }

  getDsPhieuDanhGiaOfVoucher = async (assetVoucherIds) => {
    try {
      let searchObject = {
        assetVoucherIds,
        pageIndex: 1,
        pageSize: 10000,
        voucherType: appConst.assetClass.TSCD,
        voucherId: this.state.item.id
      }
      let res = await getByPageDsPhieuDanhGiaOfVoucher(searchObject)
      if (res?.data?.data?.content && res?.status === 200 && res?.data?.code === 200) {
        let listDataDgl = res?.data?.data?.content
        let assetsUpdate = this.state.item.assets.map(item => {
          let itemDgl = listDataDgl.find(itemDgl => itemDgl?.assetVoucherId === item?.id)
          if (!itemDgl?.isEvaluated) {
            item.isAllowedReevaluate = true
          }
          else if (itemDgl?.dglInfo) {
            if (itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.CHO_DANH_GIA.indexOrder || itemDgl?.dglInfo?.dglTrangthai === appConst.listStatusAssetReevaluate.DANG_DANH_GIA.indexOrder) {
              item.isAllowedReevaluate = false
            }
            else {
              item.isAllowedReevaluate = true
            }
          }
          item.asset.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.dglTrangthai = itemDgl?.dglInfo?.dglTrangthai
          item.asset.assetVoucherId = itemDgl?.assetVoucherId
          item.asset.voucherId = this.state.item.id
          item.asset.voucherType = appConst.assetClass.TSCD
          return item
        })

        this.setState({
          item: {
            ...this.state.item,
            assets: assetsUpdate
          }
        })
      }
    } catch (error) {
      toast.error("Xảy ra lỗi")
    }
  }

  getRoleCurrentUser = () => {
    let {
      hasDeletePermission,
      hasEditPermission,
      hasCreatePermission,
    } = this.state;
    const roles = getTheHighestRole();
    let {
      currentUser,
      isRoleAccountant,
      isRoleAdmin,
      isRoleAssetManager,
      isRoleOrgAdmin,
    } = roles;
    if (currentUser) {
      const isAllManagers = isRoleAssetManager
        || isRoleAdmin
        || isRoleOrgAdmin
        || isRoleAccountant;
      hasDeletePermission = isAllManagers;
      hasCreatePermission = isAllManagers;
      hasEditPermission = isAllManagers;

      this.setState({
        ...roles,
        hasDeletePermission,
        hasEditPermission,
        hasCreatePermission,
        orgName: currentUser?.org?.name
      });
    }
  };

  handleEditItem = async (item) => {
    try {
      this.setState({
        item: item,
        shouldOpenEditorDialog: true,
      });
    } catch (error) {

    }
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handleExcel = () => {
    this.setState({
      shouldOpenConfirmationExcel: true,
    });
  };

  /* Export to excel */
  exportToExcel = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true)
    let searchObject = {};
    searchObject.keyword = this.state.keyword.trim();
    searchObject.inventoryCountDateBottom = this.state.inventoryCountDateBottom;
    searchObject.inventoryCountDateTop = this.state.inventoryCountDateTop;
    searchObject.ids = this.state?.selectedList;
    searchObject.assetClass = appConst.assetClass.TSCD;
    if (variable.listInputName.all !== this.state.departmentId) {
      searchObject.departmentId = this.state.departmentId;
    }
    searchObject.status = this.state.status ? this.state.status : null;
    searchObject.assetGroup = this.state.assetGroup
      ? this.state.assetGroup
      : null;
    searchObject.inventoryCountStatusIndexOrder = this.state?.inventoryCountStatusIndexOrder;
    searchObject.type = appConst.assetTypeInventoryCountObject.ALL.code;

    exportToExcel(searchObject)
      .then((res) => {
        setPageLoading(false)
        this.setState({ ids: [] })
        toast.success(t('general.successExport'));
        this.handleOKEditClose();
        let blob = new Blob([res.data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, t("InventoryCountVoucher.exportExcelFileName"));
      })
      .catch((err) => {
        setPageLoading(false)
        toast.error(t('general.failExport'));
      });
  };

  handleDateChange = (date, name) => {
    if (date === null) {
      this.setState({ [name]: date }, () => { this.search() });
    } else {
      this.setState({ [name]: date }, () => { isValidDate(date) && this.search() });
    }
  }

  handleEditData = (inventoryCountVoucher, openEdit = false, openPrint = false) => {
    let inventoryCountPersonIds = [];
    let userIds = [];
    inventoryCountVoucher?.assets?.map((asset, index) => {
      let differenceQuantity = (
        asset?.inventoryQuantity - asset?.accountantQuantity
      );
      let differenceOriginalCost = (
        asset?.inventoryOriginalCost - asset?.accountantOriginalCost
      );
      let differenceCarryingAmount = (
        asset?.inventoryCarryingAmount - asset?.accountantCarryingAmount
      );
      asset.asset = {
        ...asset,
        name: asset?.assetName,
        code: asset?.assetCode,
        status: {
          name: asset?.updateStatusName,
          id: asset?.updateStatusId,
          indexOrder: asset?.updateStatusIndexOrder
        },
        store: {
          name: asset?.storeName,
          id: asset?.storeId
        },
      };
      asset.status = {
        name: asset?.updateStatusName,
        id: asset?.updateStatusId,
        indexOrder: asset?.updateStatusIndexOrder
      };
      asset.store = {
        name: asset?.storeName,
        id: asset?.storeId
      };
      asset.currentStatus = {
        name: asset?.currentStatusName,
        id: asset?.currentStatusId,
        indexOrder: asset?.currentStatusIndexOrder
      }
      asset.differenceOriginalCost = differenceOriginalCost;
      asset.assetId = asset?.assetId;
      asset.differenceCarryingAmount = differenceCarryingAmount;
      asset.storeId = asset?.storeId;
      asset.departmentId = asset?.departmentId;
      asset.differenceQuantity = differenceQuantity;
      asset.price = (
        asset?.accountantOriginalCost / asset?.accountantQuantity
      );
      asset.isStore = [
        appConst.listStatusAssets.NHAP_KHO.indexOrder,
        appConst.listStatusAssets.LUU_MOI.indexOrder,
        appConst.listStatusAssets.DA_HONG.indexOrder].includes(asset?.updateStatusIndexOrder);
    });

    inventoryCountVoucher?.inventoryCountPersons?.map((item) => {
      inventoryCountPersonIds.push(item?.personId);
      userIds.push(item?.personId);
    });

    inventoryCountVoucher = {
      ...inventoryCountVoucher,
      inventoryCountPersonIds: inventoryCountPersonIds,
      userIds: userIds,
      inventoryCountStatus:
        inventoryCountVoucher?.statusId
          ?
          {
            name: inventoryCountVoucher?.statusName,
            id: inventoryCountVoucher?.statusId,
            statusIndexOrder: inventoryCountVoucher?.statusIndexOrder,
          }
          : null,
      inventoryCountStatusId: inventoryCountVoucher?.statusId,
    };

    this.setState({
      item: inventoryCountVoucher,
      shouldOpenEditorDialog: openEdit,
      shouldOpenPrintDialog: openPrint,
    });
  };

  setStateInventory = (item) => {
    if (item) {
      this.setState(item)
    }
  }

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));
        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  updateData = () => {
    getItemById(this.state.item?.id).then(({ data }) => {
      let inventoryCountVoucher = data?.data ? data?.data : {}
      inventoryCountVoucher.isView = false;
      this.handleEditData(inventoryCountVoucher, true, false)
    });
  }

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusInventory.find(
      (item) => item.indexOrder === status
    );
    return itemStatus?.name;
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    getCountByStatus({ type: appConst.assetTypeInventoryCountObject.ALL.code })
      .then(({ data }) => {
        let countStatusPlan,
          countStatusTakingInventory,
          countStatusInventory;
        // eslint-disable-next-line no-unused-expressions
        data?.data?.forEach((item) => {
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.MOI_TAO.indexOrder
            === item?.status
          ) {
            countStatusPlan = this.checkCount(item?.count);
            return;
          }
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.DANG_KIEM_KE.indexOrder
            === item?.status
          ) {
            countStatusTakingInventory = this.checkCount(item?.count);
            return;
          }
          if (
            appConst?.STATUS_INVENTORY_COUNT_VOUCHER.DA_KIEM_KE.indexOrder
            === item?.status
          ) {
            countStatusInventory = this.checkCount(item?.count);
            return;
          }
        });
        this.setState({
          countStatusPlan,
          countStatusTakingInventory,
          countStatusInventory,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      })
      .finally(() => {
        setPageLoading(false);
      });
  };

  handleSetItemState = async (rowData) => {
    this.setState({ idVoucher: rowData?.id, pageAsset: 0 }, () => {
      this.updatePageDataAsset();
    })
  };

  handleStatus = (rowData) => {
    const statusIndex = rowData?.statusIndexOrder;
    let className = "";
    switch (statusIndex) {
      case appConst.listStatusInventoryObject.DA_KIEM_KE.indexOrder:
        className = "status status-info";
        break;
      case appConst.listStatusInventoryObject.DANG_KIEM_KE.indexOrder:
        className = "status status-warning";
        break;
      case appConst.listStatusInventoryObject.NEW.indexOrder:
        className = "status status-success";
        break;
      default: break;
    }

    return (
      <span className={className}>{this.checkStatus(statusIndex)}</span>
    );

  };

  handlePrintQRCode = async (id) => {
    let { t } = this.props
    try {
      let res = await getListQRCode(id);
      if (res?.status === appConst.CODE.SUCCESS && res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          products: res?.data?.data?.length > 0 ? [...res?.data?.data] : [],
          openPrintQR: true,
        })
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  }
  handleCloseQrCode = () => {
    this.setState({
      products: [],
      openPrintQR: false,
    })
  }

  getFullInfoAsset = async (rowData) => {
    let mergeObj = {};
    let { t } = this.props
    try {
      const searchObject = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        id: rowData?.id,
      }
      const info = await getInfoSumary(rowData?.id);
      const resInfo = info?.data?.data || {};
      const assets = await getAssetByVoucher(searchObject);
      const resAssets = assets?.data?.data?.content?.length ? assets?.data?.data?.content : [];

      if (isSuccessfulResponse(info?.data?.code)) {
        mergeObj = { ...mergeObj, ...resInfo };
      } else {
        handleThrowResponseMessage(info)
      }
      if (isSuccessfulResponse(assets?.data?.code)) {
        mergeObj = { ...mergeObj, assets: [...resAssets] };
      } else {
        handleThrowResponseMessage(assets)
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      return mergeObj;
    }
  };

  handleEdit = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.isView = false;
      this.handleEditData(data, true, false)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handleView = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.isView = true;
      this.handleEditData(data, true, false)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  handlePrint = async (rowData) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);

    try {
      let data = await this.getFullInfoAsset(rowData);
      data.orgName = this.state?.orgName
      this.handleEditData(data, false, true)
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      hasDeletePermission,
      hasEditPermission,
      tabValue,
      checkAll,
    } = this.state;

    let TitlePage = t("InventoryCountVoucherDepartment.title");

    let columns = [
      {
        title: t("InventoryCountVoucher.stt"),
        field: "",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 100,
        maxWidth: 150,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            hasEditPermission={hasEditPermission}
            hasDeletePermission={hasDeletePermission}
            onSelect={(rowData, method) => {
              let { setPageLoading } = this.context;
              if (appConst.active.edit === method) {
                this.handleEdit(rowData);
              } else if (appConst.active.delete === method) {
                this.handleDelete(rowData.id);
              } else if (appConst.active.view === method) {
                this.handleView(rowData);
              } else if (appConst.active.print === method) {
                this.handlePrint(rowData);
              } else if (appConst.active.qrcode === method) {
                this.handlePrintQRCode(rowData.id)
              }
              else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
      {
        title: t("InventoryCountVoucher.inventoryCountDate"),
        maxWidth: 100,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData?.inventoryCountDate
            ? moment(rowData?.inventoryCountDate).format("DD/MM/YYYY")
            : "",
      },
      {
        title: t("InventoryCountVoucher.status"),
        field: "inventoryCountStatus.name",
        maxWidth: 120,
        align: "left",
        sorting: false,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.handleStatus(rowData),
      },
      {
        title: t("InventoryCountVoucher.voucherName"),
        field: "title",
        minWidth: 200,
        align: "left",
      }
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {" "}
            {t("Asset.inventory_count_voucher_department")} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: t("Dashboard.assetManagement"), path: 'fixed-assets/inventory-count-vouchers-department' },
              { name: TitlePage }
            ]}
          />
        </div>
        <AppBar position="static" color="default">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("InventoryCountVoucher.tabAll")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabNew")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {this.state?.countStatusPlan || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabTakingInventory")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {this.state?.countStatusTakingInventory || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("InventoryCountVoucher.tabInventory")}</span>
                  <div className="tabQuantity tabQuantity-success">
                    {this.state?.countStatusInventory || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabAll} className="mp-0">
          <ComponentInventoryDepartmentTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            assetVoucherIds={this.state.assetVoucherIds}
            updateData={this.updateData}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabPlan} className="mp-0" >
          <ComponentInventoryDepartmentTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabTakingInventory} className="mp-0" >
          <ComponentInventoryDepartmentTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={appConst.tabInventory.tabInventory} className="mp-0" >
          <ComponentInventoryDepartmentTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleEditItem={this.handleEditItem}
            exportToExcel={this.exportToExcel}
            importExcel={this.importExcel}
            handleDialogClose={this.handleDialogClose}
            handleOKEditClose={this.handleOKEditClose}
            search={this.search}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            handleConfirmationResponse={this.handleConfirmationResponse}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            setState={this.setStateInventory}
            handleDelete={this.handleDelete}
            handleChangeTabValue={this.handleChangeTabValue}
            columns={columns}
            handleDateChange={this.handleDateChange}
            handleExcel={this.handleExcel}
            getDsPhieuDanhGiaOfVoucher={this.getDsPhieuDanhGiaOfVoucher}
            updatePageData={this.updatePageData}
            setItemState={(rowData) => this.handleSetItemState(rowData)}
            handleCloseQrCode={this.handleCloseQrCode}
            anchorRef={this.anchorRef}
            handleToggleOpenExcel={this.handleToggleOpenExcel}
          />
        </TabPanel>
        <ListAssetTable {...this.props} item={this.state} itemAsset={this.state.itemAsset} handleChangePageAsset={this.handleChangePageAsset} setRowsPerPageAsset={this.setRowsPerPageAsset} />
      </div>
    );
  }
}
InventoryCountVoucherTable.contextType = AppContext;
export default InventoryCountVoucherTable;
