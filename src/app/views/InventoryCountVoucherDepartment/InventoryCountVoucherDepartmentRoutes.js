import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";
const AssetTransferTable = EgretLoadable({
  loader: () => import("./InventoryCountVoucherDepartmentTable"),
});
const ViewComponent = withTranslation()(AssetTransferTable);

const InventoryCountVoucherDepartmentRoutes = [
  {
    path:
      ConstantList.ROOT_PATH +
      "fixed-assets/inventory-count-vouchers-department",
    exact: true,
    component: ViewComponent,
  },
];

export default InventoryCountVoucherDepartmentRoutes;
