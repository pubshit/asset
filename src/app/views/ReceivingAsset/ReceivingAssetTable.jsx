import ConstantList from "../../appConfig";
import React from "react";
import {
  Grid,
  IconButton,
  Icon,
  Button,
  FormControl,
  Input,
  InputAdornment,
  AppBar,
  Tabs,
  Tab,
  TablePagination,
} from "@material-ui/core";
import {
  deleteItem,
  getItemById,
  searchByPage,
  exportToExcel,
  getNewCode,
  getCountByStatus,
  getItemsById,
} from "./ReceivingAssetService";
import ReceivingAssetDialog from "./ReceivingAssetDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation } from "react-i18next";
import { saveAs } from "file-saver";
import { Helmet } from "react-helmet";
import SearchIcon from "@material-ui/icons/Search";
import VoucherFilePopup from "./VoucherFilePopup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomMaterialTable from "../CustomMaterialTable";
import CustomTablePagination from "../CustomTablePagination";
import clsx from "clsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import AppContext from "app/appContext";
import { appConst, LIST_ORGANIZATION, PRINT_TEMPLATE_MODEL } from "app/appConst";
import { COLUMNS_RECEIVING } from "./constants";
import MaterialTable from "material-table";
import { LightTooltip, convertNumberPriceRoundUp, convertNumberToWords, convertFromToDate, formatDateTimeDto, formatDateToTimestamp, getTheHighestRole, handleKeyDown, handleThrowResponseMessage, isSuccessfulResponse, isValidDate } from "app/appFunction";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import localStorageService from "app/services/localStorageService";
import FilterOptions from "./Component/FilterOptions";
import { defaultPaginationProps } from "../../appFunction";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;
  let isAllowEdit = [
    appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder,
    appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder
  ].includes(item?.status);
  let isAllowDelete = item?.status === appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder;
  return (
    <div className="none_wrap">
      {isAllowEdit && <LightTooltip
        title={t("general.editIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.edit)}
        >
          <Icon fontSize="small" color="primary">
            edit
          </Icon>
        </IconButton>
      </LightTooltip>}
      {isAllowDelete && <LightTooltip
        title={t("general.deleteIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.delete)}
        >
          <Icon fontSize="small" color="error">
            delete
          </Icon>
        </IconButton>
      </LightTooltip>}
      <LightTooltip
        title={t("In phiếu")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
        PopperProps={{
          popperOptions: {
            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
          },
        }}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.print)}
        >
          <Icon fontSize="small" color="inherit">
            print
          </Icon>
        </IconButton>
      </LightTooltip>
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="default">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}

class ReceivingAssetTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    subRowsPerPage: 5,
    page: 0,
    subPage: 0,
    ReceivingAsset: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    subTotalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    isPrint: false,
    shouldOpenSelectAssetFilePopup: false,
    loading: false,
    listAsset: [],
    toDate: null,
    fromDate: null,
    fromBillDate: null,
    toBillDate: null,
    selectedItem: {},
    tabValue: 0,
    status: null,
    openAdvanceSearch: false,
  };
  voucherType = ConstantList.VOUCHER_TYPE.ReceivingAsset; //Tiếp nhận tài sản

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };

  handleKeyDownEnterSearch = (e) => handleKeyDown(e, this.search);

  handleKeyUp = (e) => {
    if (!e.target.value || e.target.value === null) {
      this.search();
    }
  }

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    const newRowsPerPage = event.target.value;
    this.setState({ rowsPerPage: newRowsPerPage, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search = async () => {
    this.setPage(0);
  };

  updatePageData = async () => {
    const { toDate, fromDate, keyword, page, rowsPerPage, status, toBillDate, fromBillDate, store, receiverDepartment } = this.state;
    const { setPageLoading } = this.context;
    const { t } = this.props;
    const searchObject = {
      // type: this.voucherType,

      assetClass: appConst.assetClass.TSCD,
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      toDate: toDate ? convertFromToDate(toDate).toDate : null,
      fromDate: fromDate ? convertFromToDate(fromDate).fromDate : null,
      toBillDate: toBillDate ? convertFromToDate(toBillDate).toDate : null,
      fromBillDate: fromBillDate ? convertFromToDate(fromBillDate).fromDate : null,
      status: status?.indexOrder,
      storeId: store?.id,
      receiverDepartmentId: receiverDepartment?.id
    };

    if (keyword) {
      searchObject.keyword = keyword?.trim();
    }

    setPageLoading(true);
    try {
      const res = await searchByPage(searchObject);
      const { code, data, message } = res?.data;

      if (appConst.CODE.SUCCESS === code) {
        if (!data?.content.length && data?.totalPages) {
          this.setPage(0);
          return;
        }
        this.setState({
          itemList: data?.content?.map((item, index) => ({
            ...item,
            stt: page * rowsPerPage + (index + 1),
          })) ?? [],
          totalElements: data?.totalElements ?? 0,
          listAsset: [],
          selectedItem: null
        });
      }
      else {
        toast.warning(message);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleDownload = () => {
    const blob = new Blob(["Hello, world!"], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "hello world.txt");
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      isPrint: false,
    });
  };

  handleOKEditClose = () => {
    this.setState(
      {
        shouldOpenEditorDialog: false,
        shouldOpenConfirmationDialog: false,
        isPrint: false,
      },
      () => {
        this.updatePageData();
        this.getCountStatus();
      }
    );
  };

  handleDeleteReceivingAsset = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  handlePrintReceivingAsset = async (rowData) => {
    let { setPageLoading } = this.context;
    const { t } = this.props;
    const { currentOrg } = this.context;

    if ([LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV_VAN_DINH.code].includes(currentOrg?.code)) {
      return this.setState({
        item: rowData,
        isPrint: true,
      })
    }
    try {
      setPageLoading(true);
      const res = await getItemById(rowData?.id);
      const { code, data, message } = res?.data;

      if (isSuccessfulResponse(code)) {
        this.setState({
          item: data ? data : {},
          isPrint: true,
        });
      }
      else {
        toast.warning(message);
      }
    }
    catch (e) {
      toast.error(t("general.error"));
    }
    finally {
      setPageLoading(false);
    }
  };

  handleConfirmationResponse = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      const response = await deleteItem(this.state.id);
      if (response.data && appConst.CODE.SUCCESS === response?.status) {
        await this.updatePageData();
        this.handleDialogClose();
        handleThrowResponseMessage(response)
      } else {
        toast.warning("Tài sản trong phiếu đã được sử dụng, không thể xoá.");
      }
    } catch (error) {
      // Xử lý lỗi nếu cần
    } finally {
      setPageLoading(false);
    }
  };

  componentDidMount() {
    this.updatePageData();
    this.getCountStatus()
  }

  handleAddItem = async () => {
    const { t } = this.props;
    try {
      const result = await getNewCode();
      const { code, message, data } = result?.data;
      if (code === appConst.CODE.SUCCESS) {
        const newItem = data?.voucherCode ? data : null;
        if (newItem) {
          newItem.assetVouchers = newItem.assetVouchers || [];
          newItem.issueDate = newItem.issueDate || new Date();
          this.setState({
            item: newItem,
            shouldOpenEditorDialog: true,
          });
        }
      } else {
        toast.error(message);
      }
    } catch (error) {
      toast.error(t("general.error"))
    }
  };

  handleClick = (event, item) => {
    const { ReceivingAsset } = this.state;
    if (item.checked === null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < ReceivingAsset.length; i++) {
      if (
        ReceivingAsset[i].checked === null ||
        ReceivingAsset[i].checked === false
      ) {
        selectAllItem = false;
      }
      if (ReceivingAsset[i].id === item.id) {
        ReceivingAsset[i] = item;
      }
    }
    this.setState({
      selectAllItem,
      ReceivingAsset,
    });
  };

  handleSelectAllClick = (event) => {
    const { ReceivingAsset, selectAllItem } = this.state;
    const updatedReceivingAsset = ReceivingAsset.map((item) => ({
      ...item,
      checked: !selectAllItem,
    }));
    this.setState({
      selectAllItem: !selectAllItem,
      ReceivingAsset: updatedReceivingAsset,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true,
    });
  };

  async handleDeleteList(list) {
    if (!Array.isArray(list) || list.length === 0) {
      return;
    }
    let { setPageLoading } = this.context;
    setPageLoading(true);
    for (const item of list) {
      try {
        await deleteItem(item.id);
      } catch (error) {
        console.error(`Error deleting item with id ${item.id}:`, error);
      } finally {
        setPageLoading(false);
      }
    }
  }

  handleDeleteAll = async (event) => {
    await this.handleDeleteList(this.data);
    this.updatePageData();
    this.handleDialogClose();
  };
  /* Export to excel */
  exportToExcel = async () => {
    let { setPageLoading } = this.context;
    const { toDate, fromDate } = this.state;
    const { t } = this.props;
    const searchObject = {
      type: this.voucherType,
      keyword: this.state.keyword,
      toDate: formatDateToTimestamp(toDate),
      fromDate: formatDateToTimestamp(fromDate),
    };
    try {
      setPageLoading(true);
      const result = await exportToExcel(searchObject);
      if (result?.status === appConst.CODE.SUCCESS) {
        toast.info("Xuất file thành công.");
        const blob = new Blob([result?.data], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "ReceivingAsset.xlsx");
      } else {
        toast.error(t("toastr.error"));
      }
    } catch (err) {
      toast.error(t("toastr.error"));
      toast.clearWaitingQueue();
    } finally {
      setPageLoading(false);
    }
  };

  handleRowClick = (rowData) => {
    this.setState({ selectedItem: rowData, subPage: 0, subTotalElements: 0 }, () => {
      this.updateSubPageData();
    });
  };

  updateSubPageData = async () => {
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let { subRowsPerPage, selectedItem, subPage } = this.state;
      const params = {
        voucherId: selectedItem?.id,
        pageSize: subRowsPerPage,
        pageIndex: subPage + 1
      }
      const response = await getItemsById(params);
      if (response?.data?.code === appConst.CODE.SUCCESS) {
        let { content, totalElements } = response?.data?.data;
        this.setState({
          listAsset: content || [],
          subTotalElements: totalElements,
        });
      } else {
        handleThrowResponseMessage(response);
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleSetDate = (data, name) => {
    this.setState({
      [name]: data,
    }, function () {
      if (data === null || isValidDate(data)) {
        this.search();
      }
    });
  };

  formatDataEdit = (data) => {

    return {
      ...data,
      receiverDepartment: data?.receiverDepartmentId ? {
        id: data?.receiverDepartmentId,
        name: data?.receiverDepartmentName,
      } : null,
      store: {
        id: data?.storeId,
        name: data?.storeName,
      },
      supplier: {
        id: data?.supplierId,
        name: data?.supplierName,
      },
      contract: data?.contractId ? {
        contractDate: data?.contractDate,
        contractCode: data?.contractCode,
        id: data?.contractId,
        contractName: data?.contractId ? data?.contractName + " - " + data?.contractCode : null,
      } : null,
      receiverPerson: {
        id: data?.receiverPersonId,
        displayName: data?.receiverPersonName
      },
      decisionCode: {
        id: data?.bidId,
        decisionCode: data?.decisionCode
      },
      statusIndex: data?.status,
      status: appConst.LIST_STATUS_ASSET_RECEPTION.find(i => i.indexOrder === data?.status),
      assetVouchers: data?.assetVouchers?.map(x => {
        return {
          ...x,
          asset: {
            ...x?.asset,
            isDisableBhytMaMay: Boolean(x?.asset.bhytMaMay),
          }
        }
      })
    }
  };

  handleChangeTabValue = (event, newValue) => {
    this.setState({
      itemList: [],
      tabValue: newValue,
      keyword: "",
      page: 0,
      rowsPerPage: this.state.rowsPerPage,
    });
    if (appConst?.TAB_STATUS_ASSET_RECEPTION?.ALL === newValue) {
      this.setState({
        status: null,
      }, () => this.updatePageData());
    }
    if (appConst?.TAB_STATUS_ASSET_RECEPTION?.CHO_XU_LY === newValue) {
      this.setState({
        status: appConst?.STATUS_ASSET_RECEPTION?.CHO_XU_LY,
      }, () => this.updatePageData());
    }
    if (appConst?.TAB_STATUS_ASSET_RECEPTION?.DA_XU_LY === newValue) {
      this.setState({
        status: appConst?.STATUS_ASSET_RECEPTION?.DA_XU_LY,
      }, () => this.updatePageData());
    }
  };

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  getCountStatus = async () => {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    let countStatusNew, countWatting, countApproved, countRefuse;
    try {
      let response = await getCountByStatus();
      response?.data?.data?.forEach((item) => {
        if (appConst?.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder === item?.trangThai) {
          countWatting = this.checkCount(item?.soLuong);
        }
        if (appConst?.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder === item?.trangThai) {
          countApproved = this.checkCount(item?.soLuong);
        }
      });

      this.setState({
        countStatusNew,
        countWatting,
        countApproved,
        countRefuse,
      })
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false)
    }
  };

  handleSetDataSelect = (value, source) => {
    if (source === "receiverDepartment") {
      this.setState({
        store: null,
        listStores: []
      })
    }
    this.setState({
      [source]: value,
    }, () => {
      if ([
        "toDate",
        "fromDate",
        "toBillDate",
        "fromBillDate",
      ].includes(source)) {
        if (value === null || isValidDate(value)) {
          this.search();
        }
      } else {
        this.search();
      }
    });
  }

  handleSetListData = (value, name) => {
    this.setState({ [name]: value })
  }

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleChangeSubPage = (event, newPage) => {
    this.setState({ subPage: newPage }, () => {
      this.updateSubPageData();
    });
  };

  setSubRowsPerPage = (event) => {
    this.setState({ subRowsPerPage: event.target.value, subPage: 0 }, () => {
      this.updateSubPageData();
    })
  };

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog,
      isPrint,
      shouldOpenSelectAssetFilePopup,
      loading,
      listAsset,
      toDate,
      fromDate,
      tabValue,
      countWatting,
      countApproved,
      subPage,
      subRowsPerPage
    } = this.state;

    const { currentOrg } = this.context;
    const { RECEIVING } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;

    let TitlePage = t("ReceivingAsset.title");
    const COLUMNS = COLUMNS_RECEIVING(t);
    let columnsActions = [
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        minWidth: 100,
        render: (rowData) => (
          <MaterialButton
            item={rowData}
            onSelect={async (rowData, method) => {
              let data = null;
              if (
                [
                  appConst.active.edit,
                  appConst.active.view,
                  appConst.active.check,
                  appConst.active.receive,
                ].includes(method)
              ) {
                const response = await getItemById(rowData.id);
                data = response?.data?.data || {};
                if (method === appConst.active.view || data?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder) {
                  data.isView = true;
                }
                if (method === appConst.active.edit && data?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder) {
                  data.isAllowEdit = true;
                }
                this.setState({
                  item: this.formatDataEdit(data),
                  shouldOpenEditorDialog: [
                    appConst.active.edit,
                    appConst.active.view,
                  ].includes(method),
                  isPrint: method === appConst.active.check,
                  shouldOpenSelectAssetFilePopup:
                    method === appConst.active.receive,
                  voucherId: rowData.id
                });
              } else if (method === appConst.active.print) {
                this.handlePrintReceivingAsset(rowData);
              } else if (method === appConst.active.delete) {
                this.handleDelete(rowData.id);
              } else {
                alert("Call Selected Here:" + rowData.id);
              }
            }}
          />
        ),
      },
    ];

    let columnsSubTable = [
      {
        title: t("Asset.stt"),
        field: "",
        align: "left",
        width: "50px",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => subPage * subRowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("Asset.code"),
        field: "code",
        minWidth: 120,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "name",
        align: "left",
        minWidth: 250,
        maxWidth: 400,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Asset.managementCode"),
        field: "managementCode",
        minWidth: "200px",
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("ReceivingScrollableTabsButtonForce.columns.serialNumber"),
        field: "serialNumber",
        align: "left",
        minWidth: "200px",
      },
      // {
      //   title: t("Asset.healthInsuranceCode"),
      //   field: "bhytMaMay",
      //   align: "center",
      //   minWidth: "200px",
      // },
      {
        title: t("Asset.model"),
        field: "model",
        align: "left",
        minWidth: "200px",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Asset.stockKeepingUnitTable"),
        field: "skuName",
        align: "center",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Asset.yearIntoUseTable"),
        field: "yearPutIntoUse",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("Asset.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: 150,
        maxWidth: 200,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
      {
        title: t("Asset.originalCost"),
        field: "originalCost",
        align: "left",
        minWidth: 150,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "right",
        },
        render: (rowData) =>
          rowData?.originalCost
            ? convertNumberPriceRoundUp(rowData?.originalCost)
            : "",
      },
      {
        title: t("Asset.note"),
        field: "note",
        align: "left",
        minWidth: 180,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
      },
    ];

    let titleName = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    );

    const formatDate2String = (date, isShowText) => {
      const newDate = new Date(date)
      const day = String(newDate?.getDate())?.padStart(2, '0');
      const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
      const year = String(newDate?.getFullYear());
      if (isShowText) {
        return date ? `Ngày ${day} tháng ${month} năm ${year}` : "Ngày .... tháng .... năm ......"
      }
      return date ? `${day}/${month}/${year}` : ".... / .... / ......";
    }

    let { addressOfEnterprise } = getTheHighestRole();

    let dataView = {
      orgName: titleName?.org?.name || ".........",
      ...item,
      ...titleName,
      day: String(new Date(item?.issueDate).getDate())?.padStart(2, '0'),
      month: String(new Date(item?.issueDate).getMonth() + 1)?.padStart(2, '0'),
      year: String(new Date(item?.issueDate).getFullYear()),
      daybillDate: String(new Date(item?.billDate).getDate())?.padStart(2, '0'),
      monthbillDate: String(new Date(item?.billDate).getMonth() + 1)?.padStart(2, '0'),
      yearbillDate: String(new Date(item?.billDate).getFullYear()),
      billDateFormat: formatDate2String(item?.billDate),
      issueDateFormat: formatDate2String(item?.issueDate),
      dateSign: formatDate2String(item?.issueDate, true),
      addressOfEnterprise,
      time: item?.issueDate ? `${String(new Date().getHours())}:${String(new Date().getMinutes())}` : " ........ ",
      billNumber: item?.billNumber || "........",
      voucherCode: item?.voucherCode || "........",
      departmentName: item?.receiverDepartment?.name,
      handoverPersonName: item?.handoverRepresentative || "..".repeat(100),
      stockReceiptDeliveryStore: {
        name: item?.storeName
      },
      inventoryCountPersons: (item?.inventoryCountPersons || [{}, {}])?.map((i, x) => {
        return {
          ...i,
          index: x + 1,
          name: i?.name || "..".repeat(30),
          position: i?.position || "..".repeat(30),
        }
      }),
      voucherDetails: item?.assetVouchers?.map((i, x) => {
        return {
          ...i,
          product: {
            name: i?.asset?.name,
          },
          sku: {
            name: i?.asset?.unit?.name || i?.asset?.unitName
          },
          quantityOfVoucher: i?.asset?.quantity,
          quantity: i?.asset?.quantity,
          price: convertNumberPriceRoundUp(i?.asset?.unitPrice),
          amount: convertNumberPriceRoundUp(i?.asset?.originalCost),
          asset: {
            ...i?.asset,
            unitPrice: convertNumberPriceRoundUp(i?.asset?.unitPrice),
            originalCost: convertNumberPriceRoundUp(i?.asset?.originalCost),
          },
          index: x + 1,
        }
      }),
      totalAmount: convertNumberPriceRoundUp(item?.assetVouchers?.reduce((total, item) => total + (item?.asset?.originalCost), 0) || 0),
      totalAmountToWords: convertNumberToWords(item?.assetVouchers?.reduce((total, item) => total + (item?.asset?.originalCost), 0)) || "Không",
    };

    return (
      <div className="m-sm-30">
        <div className={clsx("wrapperButton", !loading && "hidden")}>
          <CircularProgress className="buttonProgress" size={24} />
        </div>
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "/purchasing/receiving_asset",
              },
              { name: t("ReceivingAsset.title") },
            ]}
          />
        </div>
        <AppBar position="static" color="default" className="mb-20">
          <Tabs
            className="tabsStatus"
            value={tabValue}
            onChange={this.handleChangeTabValue}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab className="tab" label={t("ReceivingScrollableTabsButtonForce.tab.all")} />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("ReceivingScrollableTabsButtonForce.tab.watting")}</span>
                  <div className="tabQuantity tabQuantity-warning">
                    {countWatting || 0}
                  </div>
                </div>
              }
            />
            <Tab
              className="tab"
              label={
                <div className="tabLable">
                  <span>{t("ReceivingScrollableTabsButtonForce.tab.approved")}</span>
                  <div className="tabQuantity tabQuantity-info">
                    {countApproved || 0}
                  </div>
                </div>
              }
            />
          </Tabs>
        </AppBar>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={7} sm={12} xs={12}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.handleAddItem}
            >
              {t("ReceivingAsset.addNew")}
            </Button>
            <Button
              className="mb-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={this.handleOpenAdvanceSearch}
            >
              {t("general.advancedSearch")}
            </Button>
            {shouldOpenConfirmationDeleteAllDialog && (
              <ConfirmationDialog
                open={shouldOpenConfirmationDeleteAllDialog}
                onConfirmDialogClose={this.handleDialogClose}
                onYesClick={this.handleDeleteAll}
                text={t("general.deleteAllConfirm")}
              />
            )}
          </Grid>
          <Grid item md={5} sm={12} xs={12} style={{ transform: "translateY(7px)" }}>
            <FormControl fullWidth>
              <Input
                className="search_box w-100"
                onBlur={this.handleTextChange}
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("ReceivingAsset.filter")}
                id="search_box"
                startAdornment={
                  <InputAdornment position="end">
                    <SearchIcon
                      onClick={() => this.updatePageData()}
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                        cursor: "pointer", // Add cursor style
                      }}
                    />
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <FilterOptions state={this.state} handleSetDataSelect={this.handleSetDataSelect} t={t} handleSetListData={this.handleSetListData} />
          <Grid item xs={12}>
            <div>
              {isPrint &&
                [LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV_VAN_DINH.code].includes(currentOrg?.code) ? (
                <PrintPreviewTemplateDialog
                  t={t}
                  handleClose={this.handleDialogClose}
                  open={isPrint}
                  item={this.state.item}
                  title={t("Phiếu in biên bản kiểm nhập")}
                  model={PRINT_TEMPLATE_MODEL.COMPREHENSIVE_MANAGEMENT.RECEIVING}
                />
              ) : (
                <PrintMultipleFormDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={isPrint}
                  item={dataView}
                  title={t("Phiếu in biên bản kiểm nhập")}
                  urls={[
                    ...RECEIVING.GENERAL,
                    ...(RECEIVING[currentOrg?.printCode] || []),
                  ]}
                />
              )}
              {shouldOpenEditorDialog && (
                <ReceivingAssetDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  handleOKEditClose={this.handleOKEditClose}
                  item={item}
                  voucherId={this.state?.voucherId}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t("general.deleteConfirm")}
                  agree={t("general.agree")}
                  cancel={t("general.cancel")}
                />
              )}
              {shouldOpenSelectAssetFilePopup && (
                <VoucherFilePopup
                  open={shouldOpenSelectAssetFilePopup}
                  updatePageAssetDocument={this.updatePageAssetDocument}
                  handleClose={this.handleAssetFilePopupClose}
                  itemAssetDocument={this.state.item}
                  item={this.state}
                  t={t}
                  i18n={i18n}
                  voucherId={this.state?.voucherId}
                />
              )}
            </div>
            <CustomMaterialTable
              title={t("general.list")}
              data={itemList}
              columns={COLUMNS}
              columnActions={columnsActions}
              options={{
                sorting: false,
                maxBodyHeight: "350px",
                minBodyHeight: "260px",
              }}
              selectedItem={this.state?.selectedItem}
              onRowClick={(e, rowData) =>
                this.handleRowClick(rowData)
              }
            />

            <CustomTablePagination
              totalElements={totalElements}
              rowsPerPage={rowsPerPage}
              page={page}
              handleChangePage={this.handleChangePage}
              setRowsPerPage={this.setRowsPerPage}
            />
          </Grid>
        </Grid>

        <div>
          <MaterialTable
            columns={columnsSubTable}
            data={listAsset?.length ? listAsset : []}
            options={{
              draggable: false,
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              maxBodyHeight: "350px",
              minBodyHeight: "260px",
            }}
            localization={{
              body: {
                emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
              },
            }}
          />
          <TablePagination
            {...defaultPaginationProps()}
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            count={this.state?.subTotalElements}
            rowsPerPage={this.state?.subRowsPerPage}
            page={this.state?.subPage}
            onPageChange={this.handleChangeSubPage}
            onRowsPerPageChange={this.setSubRowsPerPage}
          />
        </div>
      </div>
    );
  }
}

ReceivingAssetTable.contextType = AppContext;
export default ReceivingAssetTable;
