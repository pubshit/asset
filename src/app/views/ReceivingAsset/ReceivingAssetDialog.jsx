import ConstantList from "../../appConfig";
import React, { Component } from "react";
import {
  Dialog,
  Button,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";

import {
  addNewOrUpdate,
  getNewCodeAsset,
} from "./ReceivingAssetService";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";

import {
  getAssetDocumentById,
  deleteAssetDocumentById,
  getNewCodeDocument,
  getRecevingAssetDocumentById,
  updateRecevingAssetDocument,
  getItemById,
} from "../Asset/AssetService";
import ReceivingScrollableTabsButtonForce from "./ReceivingScrollableTabsButtonForce";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AssetEditorDialogNew from "../Asset/AssetEditorDialogNew";
import { appConst, STATUS_SUPPLIER, variable } from "../../appConst";
import AppContext from "app/appContext";
import ComponentDialog from "./Component/ComponentDialog";
import { searchByTextDVBH } from "../Supplier/SupplierService";
import { formatDateDto, formatDateNoTime, getTheHighestRole, handleThrowResponseMessage, PaperComponent } from "../../appFunction";
import CustomValidatorForm from "../Component/ValidatorForm/CustomValidatorForm";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

class ReceivingAssetDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    rowsPerPage: 1,
    page: 0,
    assetVouchers: [],
    voucherType: ConstantList.VOUCHER_TYPE.ReceivingAsset,
    supplier: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: formatDateNoTime(new Date()),
    shouldOpenDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenHandoverDepartmentPopup: false,
    shouldOpenSelectDepartmentPopup: false,
    shouldOpenSelectSupplyPopup: false,
    totalElements: 0,
    isView: false,
    shouldOpenReceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenAssetEditorPopup: false,
    voucherCode: "",
    handoverRepresentative: "",
    note: "",
    isReceivingAsset: true,
    shouldOpenSelectAssetFilePopup: false,
    isNew: true,
    documentType: appConst.documentType.ASSET_DOCUMENT_RECEIPT, // hồ sơ tiếp nhân tài sản
    contract: null,
    isEditLocal: false,
    shouldOpenContractAddPopup: false,
    shouldOpenSupplierAddPopup: false,
    listAssetDocumentId: [],
    assetDocumentList: [],
    voucherId: null,
    idHoSoDK: null,
    serialNumbers: [],
    isClones: false,
    listSupplyUnit: [],
    listCodes: [],
    isTiepNhan: true,
    listToastFunction: {
      error: toast.error,
      warning: toast.warning,
      success: toast.success,
    },
    supplierSearchObject: {
      pageIndex: 0,
      pageSize: 10000,
      typeCodes: [appConst.TYPE_CODES.NCC_CU],
      isActive: STATUS_SUPPLIER.HOAT_DONG.code
    },
    indexEdit: undefined,
    selectedRows: [],
    shouldOpenCopyDialog: false,
    selectedRow: null,
    listBidding: [],
  };
  voucherType = ConstantList.VOUCHER_TYPE.ReceivingAsset;

  handleChange = (event, source) => {
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    const { name, value } = event.target;
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  selectReceiverPerson = (item) => {
    this.setState({ useDepartment: item });
  };

  removeKeyInObj = (data = {}) => {
    let keysToDelete = [
      'rowsPerPage',
      'page',
      "shouldOpenAssetEditorPopup",
      "shouldOpenAssetPopup",
      "shouldOpenContractAddPopup",
      "shouldOpenDepartmentPopup",
      "shouldOpenHandoverDepartmentPopup",
      "shouldOpenNotificationPopup",
      "shouldOpenReceiverPersonPopup",
      "shouldOpenSelectAssetFilePopup",
      "shouldOpenSelectDepartmentPopup",
      "shouldOpenSelectSupplyPopup",
      "shouldOpenSupplierAddPopup",
      "isClones",
      "isEditLocal",
      "isNew",
      "isReceivingAsset",
      "isTiepNhan",
      "isView",
      "listToastFunction",
      "totalElements",
      "listCodes",
      "listSupplyUnit",
      "serialNumbers",
      "allocationStatus",
      "assetVouchers",
      "item",
      "supplierSearchObject",
      "statusIndex",
      "indexEdit",
      "selectedRows",
      "shouldOpenCopyDialog",
      "selectedRow",
      "assetDocumentList",
      "documentType"
    ];
    keysToDelete.forEach(key => {
      delete data[key];
    });
    return data;
  }

  convertDataSubmit = () => {
    let _assetVoucher = [...this.state?.assetVouchers]
    let _state = this.removeKeyInObj({ ...this.state });
    return {
      id: _state?.id,
      assetClass: appConst.assetClass.TSCD,
      voucherCode: _state?.voucherCode,
      issueDate: formatDateDto(_state?.issueDate),
      billNumber: _state?.billNumber,
      decisionCode: _state?.decisionCode?.decisionCode,
      note: _state?.note,
      reason: _state?.reason,
      handoverRepresentative: _state?.handoverRepresentative,
      billDate: formatDateDto(_state?.billDate),
      storeId: _state?.store?.id,
      receiverDepartmentId: _state?.receiverDepartment?.id,
      receiverPersonId: _state?.receiverPerson?.id,
      supplierId: _state?.supplier?.id,
      contractId: _state?.contract?.id,
      contractDate: _state?.contract?.contractDate,
      status: _state?.status?.indexOrder,
      assetVouchers: _assetVoucher?.map((item) => ({
        ...item?.asset,
        decisionCode: _state?.decisionCode?.decisionCode,
        assetId: item?.asset?.id,
        createDate: item?.asset?.createDate,
        createdBy: item?.asset?.createdBy,
        modifiedBy: item?.asset?.modifiedBy,
        modifyDate: item?.asset?.modifyDate,
        note: item?.note,
        quantity: item?.asset?.quantity || 1,
        managementDepartmentId: _state?.receiverDepartment?.id,
        usePerson: item?.asset?.usePerson,
        voided: item?.asset?.voided,
        voucherId: item?.voucherId,
        depreciationDate: formatDateDto(item?.asset?.depreciationDate || new Date()),
        id: item?.id,
        // các trường tài sản phải đúng với phiếu cha
        issueDate: formatDateDto(_state?.issueDate),
        billDate: formatDateDto(_state?.billDate),
        dateOfReception: formatDateDto(_state?.issueDate),
        productId: item?.asset?.productId,
        contractId: _state?.contract?.id,
        contractDate: _state?.contract?.contractDate,
        storeId: _state?.store?.id,
        supplierId: _state?.supplier?.id,
      }))
    }
  }

  handleFormSubmit = async () => {
    const {
      id,
      assetVouchers,
      handoverRepresentative,
      receiverDepartment,
    } = this.state;
    const { t } = this.props;

    // Check for required fields
    if (handoverRepresentative?.id === null) {
      this.showToast("Vui lòng chọn đại diện bên bàn giao(Thông tin phiếu).", "warning");
      return;
    }
    if (receiverDepartment?.id === null) {
      this.showToast("Vui lòng chọn phòng ban tiếp nhận(Thông tin phiếu).", "warning");
      return;
    }
    if (assetVouchers?.length === 0) {
      this.showToast("Vui lòng chọn tài sản tiếp nhận(Thông tin phiếu).", "warning");
      return;
    }
    // Define success and error handlers
    const handleSuccess = (message) => {
      this.showToast(message, "success");
      this.props.handleOKEditClose();
    };
    // Submit the data
    let { setPageLoading } = this.context;
    try {
      setPageLoading(true);
      let payload = this.convertDataSubmit();
      let res = await addNewOrUpdate(payload);
      if (appConst.CODE.SUCCESS === res?.data?.code) {
        handleSuccess(id ? t("general.updateSuccess") : t("general.addSuccess"));
        await this.updateAssetDocument(res?.data?.data?.id);
      } else {
        handleThrowResponseMessage(res)
      }
    } catch (err) {
      this.showToast(t("general.error"), "error");
    } finally {
      setPageLoading(false);
    }
  };

  showToast = (message, type) => {
    let { listToastFunction = {} } = this.state;
    toast.clearWaitingQueue();
    const toastFunction = listToastFunction[type]
      ? listToastFunction[type]
      : () => { };

    toastFunction(message);
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  // Main function to handle selecting an asset
  handleSelectAsset = async (items) => {
    const { assetVouchers, indexEdit } = this.state;
    if (!assetVouchers?.length) {
      this.setState({ assetVouchers: [] });
    };
    let _assetVouchers = [...assetVouchers];
    if (indexEdit >= 0) {
      let index = _assetVouchers.findIndex((x) => x?.tableData?.id === indexEdit);
      _assetVouchers[index] = { ..._assetVouchers[index], asset: { ...items, id: _assetVouchers[index]?.asset?.id, isDisableBhytMaMay: Boolean(items?.bhytMaMay) } }
    } else {
      const clone = {
        ...items,
        unitId: items?.unit?.id,
        dateOfReception: this.state.issueDate,
        supplyUnitId: items?.supplyUnit?.id,
        managementDepartmentId: items?.managementDepartment?.id,
      };
      delete clone.useDepartment;
      delete clone.code;
      delete clone.qrcode;

      _assetVouchers.push({ asset: { ...clone, isDisableBhytMaMay: Boolean(clone?.bhytMaMay), } });
    }
    this.setState({ assetVouchers: _assetVouchers }, () => {
      this.handleDialogAssetEditorClose();
    });
  };

  removeAssetInlist = async (rowData) => {
    const { assetVouchers } = this.state;

    const updatedAssetVouchers = [...assetVouchers].filter(
      (x) => x?.tableData?.id !== rowData?.tableData?.id
    );

    this.setState({
      assetVouchers: updatedAssetVouchers,
    });
  };

  handleCopyAsset = (value) => {
    let { t } = this.props;
    let { selectedRow, assetVouchers } = this.state;

    const rowDataToString = { ...JSON.parse(JSON.stringify(selectedRow)) };

    const _selectedRow = {
      id: null,
      quantity: rowDataToString?.quantity,
      note: rowDataToString?.note,
      asset: {
        ...rowDataToString?.asset,
        serialNumber: null,
        managementCode: null,
        isDisableBhytMaMay: Boolean(rowDataToString?.asset.bhytMaMay),
        code: null,
      },
    };

    const largeArray = Array.from({ length: value }, () => ({ ..._selectedRow }));
    Array.prototype.push.apply(assetVouchers, largeArray);

    this.setState({ assetVouchers }, () => {
      toast.info(t("ReceivingScrollableTabsButtonForce.noti.copySuccess"));
      this.handleCloseCopyDialog();
    })

  };

  handleSelectionChange = (selectedRows, rowData) => {
    this.setState({ selectedRows })
  };

  componentDidMount() {
    let { departmentUser, isRoleAssetManager } = getTheHighestRole();

    let { item } = this.props;
    let updatedItem = {
      ...item,
      receiverDepartment: item?.receiverDepartment?.id ? item?.receiverDepartment : departmentUser,
      isRoleAssetManager
    };
    if (updatedItem.id) {
      this.setState({ isNew: false });
    }
    if (updatedItem.assetVouchers?.length <= 0) {
      updatedItem.assetVouchers = [];
    } else {
      this.setState({ selectedRow: updatedItem?.assetVouchers?.[0] })
    }
    this.setState(updatedItem);
    if (updatedItem.id) {
      this.getAssetDocument();
    }
  }

  handleOpenAssetDialog = async () => {
    let {
      issueDate,
      billDate,
      supplier,
      receiverDepartment,
      store,
      contract,
      billNumber,
      decisionCode
    } = this.state;
    try {
      const { data } = await getNewCodeAsset();
      if (data?.code) {
        this.setState({
          item: {
            ...data,
            issueDate,
            ngayHoaDon: billDate,
            supplyUnit: supplier,
            store,
            contract,
            billNumber,
            managementDepartment: receiverDepartment,
            decisionCode,
          },
          shouldOpenAssetEditorPopup: true,
          isAddAsset: true,
          indexEdit: undefined,
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  convertItemTableData = (item) => {
    let {
      issueDate,
      billDate,
      supplier,
      receiverDepartment,
      store,
      contract,
      billNumber,
      decisionCode
    } = this.state;
    let _assetSources = item?.assetSources?.length ? [...item?.assetSources] : [];
    return {
      ...item,
      issueDate,
      ngayHoaDon: billDate,
      supplyUnit: supplier,
      store,
      contract,
      billNumber,
      decisionCode,
      isBuyLocally: item?.madeIn === "Việt Nam",
      madeIns: item?.madeIn ? item?.madeIn === "Việt Nam" ? appConst.MADE_IN.TRONG_NUOC : appConst.MADE_IN.NGOAI_NUOC : null,
      managementDepartment: receiverDepartment,
      isBhytMaMay: item?.bhytMaMay,
      product: {
        id: item?.productId,
        name: item?.productName || item?.product?.name
      },
      unit: {
        id: item?.unit?.id || item?.unitId,
        name: item?.unit?.name || item?.unitName
      },
      assetGroup: {
        id: item?.assetGroup?.id || item?.assetGroupId,
        name: item?.assetGroup?.name || item?.assetGroupName
      },
      assetSources: _assetSources?.map(x => {
        return {
          ...x,
          assetSource: {
            ...x?.assetSource,
            id: x?.assetSourceId || x?.id,
            name: x?.assetSourceName || x?.assetSource?.name,
            code: x?.assetSourceCode || x?.assetSource?.code,
            isMainAssetSource: x?.isMainAssetSource,
          }
        }
      }),
      attributes: item?.attributes?.map(i => {
        return {
          ...i,
          attribute: {
            ...(i?.attribute || {}),
            code: i?.attributeCode || i?.attribute?.code,
            name: i?.attributeName || i?.attribute?.name,
            id: i?.attributeId || i?.attribute?.id,
          }
        }
      }),
      manufacturer: {
        ...item?.manufacturer,
        id: item?.manufacturer?.id || item?.manufacturerId,
        name: item?.manufacturer?.name || item?.manufacturerName,
        manufacturerName: item?.manufacturer?.name || item?.manufacturerName,
      },
      donViBaoHanh: {
        id: item?.donViBaoHanh?.id || item?.donViBaoHanhId,
        name: item?.donViBaoHanh?.name || item?.donViBaoHanhName,
      },
      donViKiemDinh: item?.donViKiemDinhId ? {
        id: item?.donViKiemDinhId,
        name: item?.donViKiemDinhTen
      } : null,
      donViKiemXa: item?.donViKiemXaId ? {
        id: item?.donViKiemXaId,
        name: item?.donViKiemXaTen
      } : null,
      donViHieuChuan: item?.donViHieuChuanId ? {
        id: item?.donViHieuChuanId,
        name: item?.donViHieuChuanTen
      } : null,
      medicalEquipment: item?.medicalEquipmentId ? {
        id: item?.medicalEquipmentId,
        name: item?.medicalEquipmentName
      } : null,
      shoppingForm: item?.shoppingFormId ? {
        id: item?.shoppingFormId,
        name: item?.shoppingFormName
      } : null
    }
  };

  handleEditAssetInList = async (rowData) => {
    let dataConvert = this.convertItemTableData({ ...rowData?.asset });
    this.setState({
      item: JSON.parse(JSON.stringify(dataConvert)),
      shouldOpenAssetEditorPopup: true,
      isAddAsset: true,
      isEditLocal: true,
      indexEdit: rowData?.tableData?.id
    });
  };

  handleChangeAssetInList = async (e, rowData) => {
    let { t } = this.props;
    let { assetVouchers } = this.state;
    let updatedAssetVouchers = [...assetVouchers];

    const index = updatedAssetVouchers.findIndex(
      (x) => x.tableData.id === rowData?.tableData?.id
    );

    let { name, value } = e.target;
    if (["managementCode", "serialNumber", "bhytMaMay"].includes(name)) {
      updatedAssetVouchers[index] = {
        ...updatedAssetVouchers[index],
        asset: {
          ...updatedAssetVouchers[index].asset,
          [name]: value
        }
      };
    } else {
      updatedAssetVouchers[index][name] = value;
    }
    this.setState({
      assetVouchers: updatedAssetVouchers,
    });
  };

  handleDialogAssetEditorClose = () => {
    this.setState({
      isEditLocal: false,
      shouldOpenAssetEditorPopup: false,
    });
  };

  handleOKEditAssetClose = () => {
    this.setState({ shouldOpenAssetEditorPopup: false });
  };

  handleOpenAssetFilePopup = () => {
    this.setState({ shouldOpenSelectAssetFilePopup: true });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      idHoSoDK: null,
      shouldOpenSelectAssetFilePopup: false,
    });
  };

  handleAddAssetDocumentItem = async () => {
    const type = appConst.documentType.ASSET_DOCUMENT_RECEIPT;
    try {
      const result = await getNewCodeDocument(type);
      if (result != null && result.data && result.data.code) {
        const item = result.data.data;
        this.setState({
          item: item,
          shouldOpenSelectAssetFilePopup: true,
        });
      }
    } catch (error) {
      alert(this.props.t("general.error_reload_page"));
    }
  };

  handleRowDataCellEditAssetFile = async (rowData) => {
    try {
      const { data } = await getAssetDocumentById(rowData.id);
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({
          item: data?.data,
          idHoSoDK: rowData.id,
          shouldOpenSelectAssetFilePopup: true,
          isEditAssetDocument: true,
        })
      } else {
        toast.warn(data?.message)
      }
    } catch (error) {
      // Xử lý lỗi nếu có
      console.error(error);
    }
  };

  handleRowDataCellDeleteAssetFile = (rowData) => {
    let { listAssetDocumentId, assetDocumentList } = this.state;
    listAssetDocumentId.splice(listAssetDocumentId?.indexOf(rowData?.id), 1)
    assetDocumentList.splice(assetDocumentList?.indexOf(rowData?.id), 1)
    this.setState({ listAssetDocumentId, assetDocumentList });
  };

  getAssetDocument = () => {
    getRecevingAssetDocumentById(this.props.item?.id).then((response) => {
      const { data } = response;
      if (data) {
        this.setState({
          assetDocumentList: data?.data,
          totalElements: data?.total,
        });
      }
    });
  };

  updateAssetDocument = async (id) => {
    try {
      if (this.state?.assetDocumentList?.length) {
        const payload = {
          assetDocumentIds: this.state?.assetDocumentList?.map(i => i?.id),
          documentType: appConst.documentType.ASSET_DOCUMENT_RECEIPT
        }
        await updateRecevingAssetDocument(id, payload);
      }
    } catch (error) {

    }
  };

  deleteAssetDocumentById = () => {
    let { listAssetDocumentId } = this.state;
    if (listAssetDocumentId != null && listAssetDocumentId.length > 0) {
      listAssetDocumentId.forEach((id) => deleteAssetDocumentById(id));
    }
  };

  handleChangeComboBox = (value, name) => {
    if (name === variable.listInputName.receiverDepartment) {
      this.setState({
        receiverPerson: null,
        store: null
      });
    }

    if (name === "decisionCode") {
      this.setState({
        assetVouchers: []
      });
    }

    this.setState({
      [name]: value
    });
  };

  handleChangeSelectContract = (item) => {
    this.setState({ contract: item });
  };

  handleSelectSupplier = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenSupplierAddPopup: true,
      });
    }
    this.setState({
      supplier: item,
      assetVouchers: [],
    });
  };

  handleCloseSupplier = () => {
    const { supplier } = this.state;
    if (variable.listInputName.New === supplier?.code) {
      this.setState({ supplier: null });
    }
    this.setState({
      shouldOpenSupplierAddPopup: false,
    });

    this.updateListSupplier()
  };

  handleSelectContract = (item) => {
    if (item?.code === variable.listInputName.New) {
      this.setState({
        shouldOpenContractAddPopup: true,
      });
    }
    this.setState({
      contract: item,
      supplier: this?.props?.item?.isAllowEdit ? this?.state?.supplier : item?.supplier,
      assetVouchers: [],
    });
  };

  handleCloseContract = () => {
    const { contract } = this.state;
    if (variable.listInputName.New === contract.code) {
      this.setState({ contract: null });
    }
    this.setState({
      shouldOpenContractAddPopup: false,
    });
  };

  updateListSupplier = async () => {
    let { t } = this.props;
    try {
      let res = await searchByTextDVBH(this.state.supplierSearchObject)
      if (res?.data?.content && res.status === appConst.CODE.SUCCESS) {
        this.setState({
          listSupplyUnit: res.data.content
        })
      }
    }
    catch (e) {
      toast.error(t("toastr.error"))
    }
  }

  setListDataState = (value, source) => {
    this.setState({
      [source]: value
    })
  }

  getItemForMapping = async (id) => {
    try {
      const data = await getItemById(id);
      if (data?.data?.code === appConst.CODE.SUCCESS) {
        return data?.data?.data || {};
      }
      return {};
    } catch (error) {
      return {};
    }
  }

  handleMapDataAsset = async (event, itemSelected, item) => {
    const { contract, billNumber, billDate, supplier, issueDate, store, receiverDepartment, decisionCode } = this.state;

    const value = await this.getItemForMapping(itemSelected?.id);

    if (item?.product?.id && decisionCode) {
      value.decisionCode = decisionCode;
      value.product = item.product;
    };
    // Tạo một đối tượng mới với các thuộc tính được lấy từ value và contract.
    const newData = {
      ...value,
      id: null,
      assetAccessories: value?.assetAccessories?.map(phuTung => ({
        ...phuTung,
        unit: {
          name: phuTung.unitName,
          id: phuTung.unitId,
        },
        product: {
          name: phuTung.productName,
          type: phuTung.productType,
          id: phuTung.productId
        },
      })),
      serialNumber: item?.serialNumber || "",
      assetGroupId: value?.assetGroup?.id,
      depreciationRate: value?.assetGroup?.depreciationRate,
      contract: contract?.id ? contract : null,
      lotNumber: billNumber,
      billDate: billDate,
      ngayHoaDon: billDate,
      supplyUnit: supplier,
      dateOfReception: issueDate,
      isReceivingAsset: true,
      managementDepartmentId: receiverDepartment?.id,
      depreciationDate: value?.depreciationDate || new Date(),
      serialNumbers: item?.serialNumbers || [],
      valueSerialNumber: item?.valueSerialNumber || "",
      store,
      managementDepartment: receiverDepartment,
      soNamKhConLai: value?.assetGroup?.namSuDung,
      decisionCode,
    };

    // Xóa các thuộc tính không cần thiết.
    for (const prop of [
      "status",
      "code",
      "yearPutIntoUse",
      "managementCode",
      "bhytMaMay",
      "depreciationDate",
      "dateOfReception",
      'useDepartment'
    ]) {
      delete newData[prop];
    }

    // Cập nhật state của component.
    this.setState({ item: newData });
  };

  handleAddFileLocal = (listFile) => {
    let newList = []
    newList.push(listFile)
    this.setState({
      assetDocumentList: [...this.state.assetDocumentList, ...newList],
    })
  }

  handleUpdateFile = (file) => {
    if (!file?.id) return;

    this.setState({
      assetDocumentList: this.state.assetDocumentList?.map(x => {
        if (x?.id === file?.id) {
          return { ...file }
        } else {
          return { ...x }
        }
      }),
    })
  }

  handleCloseCopyDialog = () => {
    this.setState({ shouldOpenCopyDialog: false });
  }

  handleOpenCopyDialog = () => {
    this.setState({ shouldOpenCopyDialog: true });
  }

  setDataSate = (data, source) => {
    this.setState({ [source]: data })
  };

  handleChangeKeySearch = (value, name) => {
    this.setState({ [name]: value })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState?.assetVouchers !== this?.state?.assetVouchers) {
      this.setState({ totalOriginalCost: this?.state?.assetVouchers?.reduce((total, item) => total += Number(item?.asset?.originalCost), 0) });
    }
  }

  render() {
    let { open, handleClose, t, i18n, item } = this.props;
    let {
      isView,
      shouldOpenAssetEditorPopup,
      receiverDepartment,
    } = this.state;

    let contractSearchObject = { pageIndex: 1, pageSize: 999 };
    let receiverPersonSearchObject = {
      pageIndex: 0,
      pageSize: 1000000,
      departmentId: receiverDepartment?.id
    };

    return (
      <>
        <Dialog
          open={open}
          PaperComponent={PaperComponent}
          maxWidth="lg"
          fullWidth
          scroll="paper"
        >
          <CustomValidatorForm
            ref="form"
            onSubmit={this.handleFormSubmit}
            className="validator-form-scroll-dialog"
          >
            <DialogTitle
              style={{ cursor: "move", paddingBottom: "0px" }}
              id="draggable-dialog-title"
            >
              <span className="">{item?.id ? t("ReceivingAsset.updatePhieuTiepNhan") : t("ReceivingAsset.addPhieuTiepNhan")}</span>
              <IconButton
                onClick={handleClose}
                style={{ position: "absolute", top: 0, right: 0 }}
              >
                <Icon className="text-black">clear</Icon>
              </IconButton>
            </DialogTitle>

            <DialogContent style={{ minHeight: "450px" }}>
              <ReceivingScrollableTabsButtonForce
                t={t}
                i18n={i18n}
                item={this.state}
                contractSearchObject={contractSearchObject}
                receiverPersonSearchObject={receiverPersonSearchObject}
                handleDateChange={this.handleDateChange}
                handleEditAssetInList={this.handleEditAssetInList}
                handleChange={this.handleChange}
                handleOKEditClose={this.props.handleOKEditClose}
                handleOKEditAssetClose={this.handleOKEditAssetClose}
                handleOpenAssetDialog={this.handleOpenAssetDialog}
                getAssetDocument={this.getAssetDocument}
                removeAssetInlist={this.removeAssetInlist}
                itemAssetDocument={this.state.item}
                handleRowDataCellEditAssetFile={
                  this.handleRowDataCellEditAssetFile
                }
                handleRowDataCellDeleteAssetFile={
                  this.handleRowDataCellDeleteAssetFile
                }
                handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
                onChangeComboBox={this.handleChangeComboBox}
                onChageSelectContract={this.handleChangeSelectContract}
                itemEdit={this.state.item}
                handleSelectContract={this.handleSelectContract}
                handleSelectSupplier={this.handleSelectSupplier}
                handleAddFileLocal={this.handleAddFileLocal}
                listSupplyUnit={this.state.listSupplyUnit}
                setListDataState={this.setListDataState}
                handleChangeAssetInList={this.handleChangeAssetInList}
                handleCopyAsset={this.handleCopyAsset}
                handleSelectionChange={this.handleSelectionChange}
                handleOpenCopyDialog={this.handleOpenCopyDialog}
                setDataSate={this.setDataSate}
                handleChangeKeySearch={this.handleChangeKeySearch}
              />
            </DialogContent>
            <DialogActions>
              <div className="flex flex-space-between flex-middle">
                <Button
                  variant="contained"
                  color="secondary"
                  className="mr-12"
                  onClick={this.props.handleClose}
                >
                  {t("general.cancel")}
                </Button>
                {(!isView || item?.isAllowEdit) && (
                  <Button variant="contained" color="primary" type="submit">
                    {item?.id ? t("general.update") : t("general.save")}
                  </Button>
                )}
              </div>
            </DialogActions>
          </CustomValidatorForm>
        </Dialog>

        {shouldOpenAssetEditorPopup && (
          <AssetEditorDialogNew
            open={shouldOpenAssetEditorPopup}
            t={t}
            i18n={i18n}
            isReceivingAsset={this.state.isReceivingAsset}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleDialogAssetEditorClose}
            handleOKEditClose={this.handleOKEditClose}
            handleOKEditAssetClose={this.handleOKEditAssetClose}
            item={this.state.item}
            isTiepNhan={this.state.isTiepNhan}
            itemAssetDocument={{
              ...this.state,
              contract: this.state.contract
                ? {
                  ...this.state.contract,
                  contractName:
                    this.state.contract?.contractCode +
                    " - " +
                    (this.state.contract?.contractText || ""),
                }
                : null,
            }}
            isAddAsset={this.state.isAddAsset}
            isEditLocal={this.state.isEditLocal}
            handleMapDataAsset={this.handleMapDataAsset}
          />
        )}

        <ComponentDialog
          {...this.state}
          t={t}
          i18n={i18n}
          voucherId={this.state?.id}
          handleCloseContract={this.handleCloseContract}
          handleSelectContract={this.handleSelectContract}
          handleCloseSupplier={this.handleCloseSupplier}
          handleSelectSupplier={this.handleSelectSupplier}
          getAssetDocument={this.getAssetDocument}
          handleAssetFilePopupClose={this.handleAssetFilePopupClose}
          handleAddFileLocal={this.handleAddFileLocal}
          handleUpdateFile={this.handleUpdateFile}
          handleCloseCopyDialog={this.handleCloseCopyDialog}
          handleCopyAsset={this.handleCopyAsset}
        />
      </>
    );
  }
}

ReceivingAssetDialog.contextType = AppContext;
export default ReceivingAssetDialog;
