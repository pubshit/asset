import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from "react-i18next";
import { ROUTES_PATH } from "app/appConst";
const ReceivingAssetTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./ReceivingAssetTable"),
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(ReceivingAssetTable);

const ReceivingAssetRoutes = [
  {
    path: ConstantList.ROOT_PATH + ROUTES_PATH.ASSET_RECEPTION,
    exact: true,
    component: ViewComponent,
  },
];

export default ReceivingAssetRoutes;
