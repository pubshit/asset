import {
  Card,
  CardContent,
  Collapse,
  Grid,
  TextField,
} from "@material-ui/core";
import React from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { Autocomplete } from "@material-ui/lab";
import { getListManagementDepartment } from "../../Asset/AssetService";
import { filterOptions } from "../../../appFunction";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { appConst, STATUS_STORE } from "app/appConst";
import { searchByPage as searchByPageStore } from "../../Store/StoreService";

function FilterOptions(props) {
  let {
    openAdvanceSearch,
    receiverDepartment,
    fromDate,
    toDate,
    status,
    listReceiverDepartment = [],
    fromBillDate,
    toBillDate,
    store,
    listStores,
    tabValue,
  } = props.state;
  let { t } = props;
  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: receiverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code,
  };
  return (
    <Grid item xs={12} style={{ paddingTop: 0, paddingBottom: 0 }}>
      <Collapse in={openAdvanceSearch}>
        <ValidatorForm onSubmit={() => {}}>
          <Card elevation={2}>
            <CardContent>
              <Grid container xs={12} spacing={2}>
                {/* Từ ngày */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <MuiPickersUtilsProvider
                    utils={DateFnsUtils}
                    locale={viLocale}
                  >
                    <KeyboardDatePicker
                      fullWidth
                      className="mb-16 mr-16 align-bottom"
                      margin="none"
                      id="mui-pickers-date"
                      label={t("ReceivingAsset.receivingFromDate")}
                      inputVariant="standard"
                      autoOk
                      format="dd/MM/yyyy"
                      name={"fromDate"}
                      value={fromDate}
                      minDate={new Date("01/01/1900")}
                      minDateMessage={t("general.minDateDefault")}
                      maxDate={toDate ? toDate : new Date("01/01/2100")}
                      maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "fromDate")
                      }
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* Đến ngày */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <MuiPickersUtilsProvider
                    utils={DateFnsUtils}
                    locale={viLocale}
                  >
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("MaintainPlaning.dxTo")}
                      format="dd/MM/yyyy"
                      value={toDate ?? null}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "toDate")
                      }
                      minDate={fromDate}
                      KeyboardButtonProps={{ "aria-label": "change date" }}
                      minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                      maxDateMessage={t("general.maxDateMessage")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* Từ ngày */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <MuiPickersUtilsProvider
                    utils={DateFnsUtils}
                    locale={viLocale}
                  >
                    <KeyboardDatePicker
                      fullWidth
                      className="mb-16 mr-16 align-bottom"
                      margin="none"
                      id="mui-pickers-date"
                      label={t("ReceivingAsset.fromBillDate")}
                      inputVariant="standard"
                      autoOk
                      format="dd/MM/yyyy"
                      name={"fromBillDate"}
                      value={fromBillDate}
                      minDate={new Date("01/01/1900")}
                      minDateMessage={t("general.minDateDefault")}
                      maxDate={toBillDate ? toBillDate : new Date("01/01/2100")}
                      maxDateMessage={toBillDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "fromBillDate")
                      }
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* Đến ngày */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <MuiPickersUtilsProvider
                    utils={DateFnsUtils}
                    locale={viLocale}
                  >
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("MaintainPlaning.dxTo")}
                      format="dd/MM/yyyy"
                      value={toBillDate ?? null}
                      onChange={(data) =>
                        props.handleSetDataSelect(data, "toBillDate")
                      }
                      KeyboardButtonProps={{ "aria-label": "change date" }}
                      minDate={fromBillDate}
                      minDateMessage={fromBillDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                      maxDateMessage={t("general.maxDateMessage")}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* Trạng thái */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <Autocomplete
                    fullWidth
                    options={appConst.LIST_STATUS_ASSET_RECEPTION}
                    defaultValue={status ? status : null}
                    value={status ? status : null}
                    onChange={(e, value) =>
                      props.handleSetDataSelect(value, "status")
                    }
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterOptions(options, params);
                    }}
                    getOptionLabel={(option) => option.name}
                    noOptionsText={t("general.noOption")}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        value={status?.name || ""}
                        label={t("maintainRequest.status")}
                      />
                    )}
                    disabled={tabValue > 0}
                  />
                </Grid>
                {/* Phòng tiếp nhận */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={t("allocation_asset.receiverDepartment")}
                    searchFunction={getListManagementDepartment}
                    searchObject={{}}
                    isNoRenderChildren
                    displayLable={"text"}
                    typeReturnFunction="list"
                    listData={listReceiverDepartment.filter(i => i?.isActive)}
                    setListData={(value) =>
                      props.handleSetListData(value, "listReceiverDepartment")
                    }
                    value={receiverDepartment ? receiverDepartment : null}
                    onSelect={(data) =>
                      props?.handleSetDataSelect(data, "receiverDepartment")
                    }
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterOptions(options, params);
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                {/* Kho */}
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <AsynchronousAutocompleteSub
                    className="w-100"
                    label={t(
                      "ReceivingScrollableTabsButtonForce.receiverWarehouse"
                    )}
                    searchFunction={searchByPageStore}
                    searchObject={searchObjectStore}
                    displayLable={"name"}
                    listData={listStores}
                    setListData={(value) =>
                      props.handleSetListData(value, "listStores")
                    }
                    value={store ? store : null}
                    onSelect={(data) =>
                      props?.handleSetDataSelect(data, "store")
                    }
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim();
                      return filterOptions(options, params);
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                {/* Nguyên giá từ - đến
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <TextValidator
                    fullWidth
                    label={t("general.originalCostFrom")}
                    value={originalCostFrom}
                    name="originalCostFrom"
                    onKeyUp={props.handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) =>
                      props.handleSetDataSelect(
                        e?.target?.value,
                        e?.target?.name
                      )
                    }
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={3}>
                  <TextValidator
                    fullWidth
                    label={t("general.originalCostTo")}
                    value={originalCostTo}
                    name="originalCostTo"
                    onKeyUp={props.handleKeyUp}
                    onKeyDown={handleKeyDown}
                    onChange={(e) =>
                      props.handleSetDataSelect(
                        e?.target?.value,
                        e?.target?.name
                      )
                    }
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                    }}
                  />
                </Grid> */}
              </Grid>
            </CardContent>
          </Card>
        </ValidatorForm>
      </Collapse>
    </Grid>
  );
}

export default FilterOptions;
