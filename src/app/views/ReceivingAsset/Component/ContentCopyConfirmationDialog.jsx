import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";
import React, { useState } from "react";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { handleKeyDownIntegerOnly } from "../../../appFunction";
function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

function ContentCopyConfirmationDialog(props) {
    const { open, onClose = () => { }, t, onSubmit } = props;
    const [number, setNumber] = useState(null);

    const handleChange = (e) => {
        let { value } = e.target;
        setNumber(value)
    }
    return (
        <Dialog onClose={onClose} open={open} PaperComponent={PaperComponent} maxWidth={"xs"} fullWidth>
            <ValidatorForm onSubmit={() => onSubmit(Number(number))}>
                <DialogTitle>
                    <span className="mb-20">{t("general.copyAsset")}</span>
                </DialogTitle>
                <DialogContent>
                    <TextValidator
                        onChange={handleChange}
                        type="number"
                        onKeyDown={handleKeyDownIntegerOnly}
                        name="number"
                        label={
                            <span>
                                <span className="colorRed">*</span>
                                {t("Asset.quantityAsset")}
                            </span>
                        }
                        validators={["required"]}
                        errorMessages={[t('general.required')]}
                        className="w-100"
                        value={number}
                    />
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle mt-10">
                        <Button variant="contained" color="secondary" className="mr-12" onClick={onClose}>
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit" style={{ marginRight: "15px" }}>
                            {t("general.confirm")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
}

export default ContentCopyConfirmationDialog;
