import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Radio } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import CustomMaterialTable from "../../CustomMaterialTable";
import { searchByPage } from "../../Contract/ContractService";
import moment from "moment";

function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

function ContractDialog(props) {
    const { open, onClose = () => {}, t, onSelect, selectedItem } = props;

    const [listContract, setListContract] = useState([]);
    const [selectedValue, setSelectedValue] = useState(selectedItem);
    const [query, setQuery] = useState({
        keyword: "",
        pageIndex: 0,
        rowsPerPage: 5,
    });

    useEffect(() => {
        getListContract();
    }, [query]);

    const getListContract = async () => {
        try {
            const { data, status } = await searchByPage(query);
            setListContract([]);
            if (status === 200) {
                setListContract(data?.content);
            }
        } catch (error) {
            console.error("Error getListContract", error);
        }
    };

    const handleSelect = (event, value) => {
        setSelectedValue(value);
    };
    const cellBorderStyle = {
        borderRight: "1px solid #E4E7EC",
    };
    const columns = [
        {
            title: t("general.select"),
            field: "custom",
            minWidth: "60px",
            cellStyle: {
                cellBorderStyle,
            },
            render: (rowData) => (
                <Radio
                    id={`radio${rowData.id}`}
                    style={{ padding: "0px" }}
                    name="radSelected"
                    value={rowData.id}
                    checked={selectedValue.id === rowData.id}
                    onClick={(event) => handleSelect(event, rowData)}
                />
            ),
        },
        {
            title: t("Số hợp đồng"),
            field: "contractCode",
            minWidth: "200px",
            cellStyle: {
                cellBorderStyle,
            },
        },
        {
            title: t("Tên hợp đồng"),
            field: "contractName",
            minWidth: "200px",
            cellStyle: {
                cellBorderStyle,
            },
        },
        {
            title: t("Ngày lập hợp đồng"),
            field: "contractDate",
            minWidth: "200px",
            cellStyle: {
                cellBorderStyle,
            },
            render: (rowData) => moment(rowData?.contractDate).format("DD/MM/YYYY"),
        },
    ];

    return (
        <Dialog onClose={onClose} open={open} PaperComponent={PaperComponent} maxWidth={"md"} fullWidth>
            <DialogTitle>
                <span className="mb-20">{t("Contract.title")}</span>
            </DialogTitle>
            <DialogContent>
                <Grid className="" container spacing={2}>
                    <Grid item xs={12}>
                        <CustomMaterialTable title={t("Contract.listContract")} data={listContract} columns={columns} />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <div className="flex flex-space-between flex-middle mt-10">
                    <Button variant="contained" color="secondary" className="mr-12" onClick={onClose}>
                        {t("general.cancel")}
                    </Button>
                    <Button variant="contained" color="primary" type="submit" style={{ marginRight: "15px" }} onClick={() => onSelect(selectedValue)}>
                        {t("general.select")}
                    </Button>
                </div>
            </DialogActions>
        </Dialog>
    );
}

export default ContractDialog;
