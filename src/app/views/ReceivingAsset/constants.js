import moment from "moment";
import { convertNumberPriceRoundUp } from "../../appFunction";
import { TextValidator } from "react-material-ui-form-validator";
import { appConst } from "app/appConst";

export const COLUMNS_RECEIVING = (t) => [
  {
    title: t("general.index"),
    field: "stt",
    minWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("receivingAsset.code"),
    field: "voucherCode",
    minWidth: 140,
    align: "center",
  },
  {
    title: t("Asset.decisionCode"),
    field: "decisionCode",
    align: "center",
    minWidth: 140,
  },
  {
    title: t("receivingAsset.contractNumber"),
    field: "billNumber",
    align: "center",
    minWidth: 120,
  },
  {
    title: t("receivingAsset.contractDate"),
    field: "",
    align: "center",
    minWidth: 120,
    render: (rowData) =>
      rowData?.billDate ? moment(rowData?.billDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.issueDate"),
    field: "issueDate",
    minWidth: 140,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) =>
      rowData.issueDate ? moment(rowData.issueDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("Asset.status"),
    field: "status",
    minWidth: 160,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      if (
        rowData?.status === appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.indexOrder
      ) {
        return (
          <span className="status status-warning">
            {appConst.STATUS_ASSET_RECEPTION.CHO_XU_LY.name}
          </span>
        );
      } else if (
        rowData?.status === appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.indexOrder
      ) {
        return (
          <span className="status status-info">
            {appConst.STATUS_ASSET_RECEPTION.DA_XU_LY.name}
          </span>
        );
      }
      return "";
    },
  },
  {
    title: t("ReceivingAsset.supplier"),
    field: "supplierName",
    minWidth: 400,
  },
  {
    title: t("allocation_asset.receiverDepartment"),
    field: "receiverDepartmentName",
    minWidth: 400,
  },
  {
    title: t("Asset.note"),
    field: "note",
    minWidth: 400,
  },
];

export const columns_ScrollableTabs = (t, props) => [
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.index"),
    field: "",
    minWidth: "40px",
    align: "center",
    render: (rowData) =>
      props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.assetCode"),
    field: "asset.code",
    minWidth: "140px",
    align: "center",
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.assetName"),
    field: "asset.name",
    align: "left",
    minWidth: "200px",
  },
  {
    title: t("Asset.managementCode"),
    field: "asset.managementCode",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="managementCode"
          className="w-100"
          value={rowData?.asset?.managementCode}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.serialNumber"),
    field: "asset.serialNumber",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="serialNumber"
          className="w-100"
          value={rowData?.asset?.serialNumber}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
  {
    title: t("Asset.healthInsuranceCode"),
    field: "asset.bhytMaMay",
    align: "center",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="bhytMaMay"
          className="w-100"
          value={rowData?.asset?.bhytMaMay}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
          disabled={!rowData?.asset?.isDisableBhytMaMay}
        />
      );
    },
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.model"),
    field: "asset.model",
    align: "center",
    minWidth: "130px",
  },
  {
    title: t("Asset.stockKeepingUnitTable"),
    field: "sku.name",
    align: "center",
    minWidth: 120,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
    },render: (rowData) =>
      rowData?.asset?.unit?.name || rowData?.asset?.unitName,
  },
  {
    title: t("Asset.yearIntoUseTable"),
    field: "asset.yearPutIntoUse",
    align: "left",
    minWidth: 120,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: "center",
    },
  },
  {
    title: t("Asset.yearOfManufacture"),
    field: "asset.yearOfManufacture",
    align: "left",
    minWidth: 120,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: "center",
    },
  },
  {
    title: t("Asset.manufacturer"),
    field: "asset.manufacturerName",
    align: "left",
    minWidth: 150,
    maxWidth: 200,
    cellStyle: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    render: (rowData) =>
      rowData?.asset?.manufacturer?.name || rowData?.asset?.manufacturerName,
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.originalCost"),
    field: "asset.originalCost",
    align: "right",
    minWidth: "150px",
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) =>
      rowData?.asset?.originalCost
        ? convertNumberPriceRoundUp(rowData?.asset?.originalCost)
        : "",
  },
  {
    title: t("ReceivingScrollableTabsButtonForce.columns.note"),
    field: "asset.note",
    align: "left",
    minWidth: "200px",
    render: (rowData) => {
      return (
        <TextValidator
          onChange={(e) => props.handleChangeAssetInList(e, rowData)}
          name="note"
          className="w-100"
          value={rowData?.note}
          InputProps={{
            readOnly: props?.item?.isView,
          }}
        />
      );
    },
  },
];
