import React, { useEffect, useState } from "react";
import { IconButton, Button, Icon, Grid } from "@material-ui/core";
import { TextValidator } from "react-material-ui-form-validator";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import NumberFormat from "react-number-format";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MTableToolbar } from "material-table";
import Autocomplete, { createFilterOptions } from "@material-ui/lab/Autocomplete";
import { searchByTextDVBH } from "../Supplier/SupplierService";
import {
  getListManagementDepartment,
} from "../Asset/AssetService";
import { personSearchByPage } from "../AssetAllocation/AssetAllocationService";
import CustomMaterialTable from "../CustomMaterialTable";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import { searchByPage } from "../Contract/ContractService";
import { columns_ScrollableTabs } from "./constants";
import { a11yProps, filterOptions, LightTooltip, TabPanel } from "app/appFunction";
import { appConst, STATUS_STORE, variable } from "app/appConst";
import { searchByPage as searchByPageStore } from "../Store/StoreService";
import { vi } from "date-fns/locale";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { convertNumberPriceRoundUp } from "../../appFunction";
import { searchByPage as searchByPageBidding } from "../bidding-list/BiddingListService";

function MaterialButton(props) {
  const item = props.item;
  let { selectedRow } = props;
  return (
    <div className="flex flex-center">
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">
          edit
        </Icon>
      </IconButton>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">
          delete
        </Icon>
      </IconButton>
      {selectedRow?.tableData?.id === item?.tableData?.id && <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.copy)}>
        <Icon fontSize="small" color="primary">
          content_copy
        </Icon>
      </IconButton>}
    </div>
  );
}


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

const NumberFormatCustom = React.memo((props) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
});

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default function ReceivingScrollableTabsButtonForce(props) {
  const classes = useStyles();
  const filterAutocomplete = createFilterOptions();
  const { t, i18n, onChangeComboBox, item, handleSelectSupplier } = props;
  const [value, setValue] = React.useState(0);
  const [listContract, setListContract] = useState([]);
  const [listReceiverDepartment, setListReceiverDepartment] = React.useState(
    []
  );
  const [listReceiverPerson, setListReceiverPerson] = React.useState([]);

  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: item?.receiverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code
  };

  let searchObjectBidding = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    status: appConst.STATUS_BIDDING.OPEN.code
  };

  let isAllowEditAndAddAsset =
    item?.issueDate
    && item?.supplier?.id
    && item?.handoverRepresentative
    && item?.status?.indexOrder
    && item?.store?.id
    && item?.receiverDepartment?.id;

  useEffect(() => {
    setListReceiverPerson([])
  }, [item?.receiverDepartment?.id]);

  const debounce = (func, delay) => {
    let timeoutId;
    return function (...args) {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

  const getListContract = () => {
    try {
      const searchObject = {};
      searchObject.pageIndex = 1; // Assuming `query` is defined somewhere
      searchObject.pageSize = 100000; // Assuming `query` is defined somewhere
      searchObject.keyword = ""; // Include the keyword from the event target
      searchByPage(searchObject).then(({ data }) => {
        if (data) {
          const updatedList =
            data?.content?.map((item) => ({
              ...item,
              contractName: item?.contractCode + " - " + item?.contractName,
            })) || [];

          setListContract(updatedList);
        }
      });
    } catch (error) { }
  }

  useEffect(() => {
    getListContract();
  }, [item?.shouldOpenContractAddPopup])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const columns = columns_ScrollableTabs(t, props);
  let columnsActions = [
    {
      title: t("ReceivingScrollableTabsButtonForce.columns.action"),
      hidden: item?.isView,
      field: "custom",
      align: "center",
      minWidth: 120,
      render: (rowData) => (
        <MaterialButton
          item={rowData}
          isHidden={true}
          selectedRow={item?.selectedRow}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              props.handleEditAssetInList(rowData);
            } else if (method === appConst.active.delete) {
              props.removeAssetInlist(rowData);
            } else if (method === appConst.active.copy) {
              props.handleOpenCopyDialog(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
  ];

  let columnsVoucherFile = [
    {
      title: t("ReceivingScrollableTabsButtonForce.columns.index"),
      field: "",
      maxWidth: 50,
      align: "left",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData.tableData.id + 1,
    },
    {
      title: t("ReceivingScrollableTabsButtonForce.columns.codeProfile"),
      field: "code",
      align: "left",
      minWidth: "150px",
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        textAlign: "center",
      },
    },
    {
      title: t("ReceivingScrollableTabsButtonForce.columns.nameProfile"),
      field: "name",
      align: "left",
      minWidth: 250,
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
      },
    },
    {
      title: t("ReceivingScrollableTabsButtonForce.columns.description"),
      field: "description",
      align: "left",
      minWidth: 250,
      cellStyle: {
        paddingRight: 10,
        paddingLeft: 10,
      },
    },
    {
      title: t("general.action"),
      field: "valueText",
      align: "center",
      maxWidth: 120,
      render: (rowData) => (
        <div className="none_wrap">
          {(!item?.isView || item?.isAllowEdit) ?
            <>
              <LightTooltip
                title={t("general.editIcon")}
                placement="top"
                enterDelay={300}
                leaveDelay={200}
              >
                <IconButton
                  size="small"
                  onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
                >
                  <Icon fontSize="small" color="primary">
                    edit
                  </Icon>
                </IconButton>
              </LightTooltip>
              <LightTooltip
                title={t("general.deleteIcon")}
                placement="top"
                enterDelay={300}
                leaveDelay={200}
              >
                <IconButton
                  size="small"
                  onClick={() => props.handleRowDataCellDeleteAssetFile(rowData)}
                >
                  <Icon fontSize="small" color="error">
                    delete
                  </Icon>
                </IconButton>
              </LightTooltip>
            </>
            :
            <LightTooltip
              title={t("general.viewDocument")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                size="small"
                title={t("general.viewDocument")}
                onClick={() => props.handleRowDataCellEditAssetFile(rowData)}
              >
                <Icon fontSize="small" color="primary">
                  visibility
                </Icon>
              </IconButton>
            </LightTooltip>
          }
        </div>
      ),
    },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          // variant="fullWidth"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab
            label={t(
              "ReceivingScrollableTabsButtonForce.tab.InformationVoting"
            )}
            {...a11yProps(0)}
          />
          <Tab
            label={t("ReceivingScrollableTabsButtonForce.tab.AttachedProfile")}
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Grid container spacing={1}>
          <Grid item md={3} sm={6} xs={12} style={{ display: "flex", alignItems: "center" }}>
            <div>
              <span style={{ fontWeight: "bold" }}>Mã phiếu: </span>
              {props.item.voucherCode}
            </div>
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={vi}>
              <CustomValidatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={
                  <span>
                    <span className="colorRed">*</span>
                    {t("ReceivingScrollableTabsButtonForce.issueDate")}
                  </span>
                }
                inputVariant="standard"
                type="text"
                autoOk
                format="dd/MM/yyyy"
                name={"issueDate"}
                value={props.item?.issueDate}
                onChange={(date) => props.handleDateChange(date, "issueDate")}
                maxDate={new Date()}
                invalidDateMessage={t("general.invalidDateFormat")}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                disabled={item?.isView}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                validators={["required"]}
                errorMessages={[t("general.required")]}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={6} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("Asset.decisionCode")}
                </span>
              }
              listData={props?.item?.listBidding}
              setListData={(value) => props?.onChangeComboBox(value, "listBidding")}
              searchFunction={searchByPageBidding}
              searchObject={searchObjectBidding}
              displayLable="decisionCode"
              typeReturnFunction="listData"
              value={item?.decisionCode || null}
              onSelect={(value) => onChangeComboBox(value, "decisionCode")}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <TextValidator
              size="small"
              label={t("ReceivingScrollableTabsButtonForce.lotNumber")}
              name="billNumber"
              className="w-100 mt-3"
              // value={props.item.receiverPerson != null ? props.item.receiverPerson.displayName : ''}
              value={props.item.billNumber}
              onChange={props.handleChange}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={vi}>
              <CustomValidatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t("ReceivingScrollableTabsButtonForce.dateBill")}
                inputVariant="standard"
                type="text"
                autoOk
                format="dd/MM/yyyy"
                name={"billDate"}
                value={props.item.billDate}
                onChange={(date) => props.handleDateChange(date, "billDate")}
                maxDate={new Date()}
                invalidDateMessage={t("general.invalidDateFormat")}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={t("allocation_asset.maxDateMessage")}
                disabled={item?.isView}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            {
              item?.isView && !item?.isAllowEdit ?
                <TextValidator
                  size="small"
                  type="text"
                  label={<span>
                    {t("ReceivingScrollableTabsButtonForce.procurementContract")}
                  </span>}
                  variant="standard"
                  className="w-100"
                  value={props.item?.contract?.contractName}
                  InputProps={{
                    readOnly: true
                  }}
                /> :
                <Autocomplete
                  id="combo-box"
                  className="mt-3"
                  fullWidth
                  size="small"
                  name="contract"
                  options={listContract}
                  onChange={(e, value) =>
                    props?.handleSelectContract(value)
                  }
                  value={item?.contract || null}
                  getOptionLabel={(option) => option.contractName || ""}
                  filterOptions={(options, params) =>
                    filterOptions(options, params, true, "contractName")
                  }
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      label={<span>
                        {t("ReceivingScrollableTabsButtonForce.procurementContract")}
                      </span>}
                      variant="standard"
                      onChange={(event) => props.handleChangeKeySearch(event.target.value, "keySearch")}
                    />
                  )}
                />}
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                fullWidth
                margin="none"
                id="mui-pickers-date"
                label={t("ReceivingScrollableTabsButtonForce.dayContract")}
                inputVariant="standard"
                type="text"
                autoOk={false}
                format="dd/MM/yyyy"
                name={"contractDate"}
                value={item?.contract?.contractDate || null}
                onChange={(date) =>
                  props.handleDateChange(date, "contractDate")
                }
                disabled
                invalidDateMessage={t("general.invalidDateFormat")}
                minDateMessage={t("general.minDateDefault")}
                maxDateMessage={t("general.maxDateDefault")}
              />
            </MuiPickersUtilsProvider>
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t("ReceivingScrollableTabsButtonForce.supplier")}
                </span>
              }
              listData={props?.listSupplyUnit}
              setListData={(value) => props?.setListDataState(value, "listSupplyUnit")}
              searchFunction={searchByTextDVBH}
              searchObject={props.item?.supplierSearchObject}
              displayLable="name"
              value={item?.supplier || null}
              onSelect={(value) => handleSelectSupplier(value, "SupplyUnit")}
              filterOptions={(options, params) =>
                filterOptions(options, params, true, "name")
              }
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              readOnly={item?.isView}
              disabled={item?.isView ? false : item?.contract?.id}
              onInputChange={(event) => props.handleChangeKeySearch(event.target.value, "keySearch")}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}  >
            <TextValidator
              size="small"
              onChange={props.handleChange}
              type="text"
              name="handoverRepresentative"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t(
                    "ReceivingScrollableTabsButtonForce.handoverRepresentative"
                  )}
                </span>
              }
              className="w-100 mt-3"
              value={props.item.handoverRepresentative}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span style={{ color: "red" }}>* </span>
                  <span>
                    {t("allocation_asset.status")}</span>
                </span>
              }
              listData={appConst.LIST_STATUS_ASSET_RECEPTION}
              searchFunction={() => { }}
              searchObject={{}}
              displayLable={'name'}
              value={item?.status ? item?.status : null}
              onSelect={(value) => onChangeComboBox(value, variable.listInputName.status)}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("ReceivingScrollableTabsButtonForce.receiverDepartment")}
                </span>
              }
              listData={listReceiverDepartment}
              setListData={setListReceiverDepartment}
              searchFunction={getListManagementDepartment}
              searchObject={{}}
              isNoRenderChildren
              defaultValue={
                item?.receiverDepartment ? item?.receiverDepartment : null
              }
              displayLable="name"
              showCode="code"
              typeReturnFunction="list"
              value={item?.receiverDepartment ? item?.receiverDepartment : null}
              onSelect={(value) => onChangeComboBox(
                value,
                variable.listInputName.receiverDepartment
              )}
              filterOptions={(options, params) => {
                params.inputValue = params?.inputValue?.trim();
                return filterAutocomplete(options, params);
              }}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t("general.required")]}
              readOnly={item?.isView || item?.isRoleAssetManager}
            />
          </Grid>

          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  {t("ReceivingScrollableTabsButtonForce.receiverPerson")}
                </span>
              }
              listData={listReceiverPerson}
              setListData={setListReceiverPerson}
              searchFunction={personSearchByPage}
              searchObject={props.receiverPersonSearchObject}
              defaultValue={item?.receiverPerson ? item?.receiverPerson : null}
              displayLable="displayName"
              value={item?.receiverPerson ? item?.receiverPerson : null}
              onSelect={(value) => onChangeComboBox(
                value,
                variable.listInputName.receiverPerson
              )}
              filterOptions={(options, params) => {
                params.inputValue = params?.inputValue?.trim();
                return filterAutocomplete(options, params);
              }}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={3} sm={6} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span style={{ color: "red" }}>* </span>
                  <span>
                    {t("ReceivingScrollableTabsButtonForce.receiverWarehouse")}</span>
                </span>
              }
              searchFunction={searchByPageStore}
              searchObject={searchObjectStore}
              displayLable={'name'}
              value={item?.store ? item?.store : null}
              onSelect={(value) => onChangeComboBox(value, variable.listInputName.store)}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              filterOptions={filterOptions}
              disabled={!item?.receiverDepartment?.id}
              noOptionsText={t("general.noOption")}
              readOnly={item?.isView}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextValidator
              onChange={props.handleChange}
              type="text"
              name="note"
              label={
                <span>
                  <span className="colorRed"></span>
                  {t("ReceivingScrollableTabsButtonForce.note")}
                </span>
              }
              className="w-100"
              value={props.item.note}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <TextValidator
              onChange={props.handleChange}
              type="text"
              name="reason"
              label={
                <span>
                  <span className="colorRed"></span>
                  {t("ReceivingAsset.reason")}
                </span>
              }
              className="w-100"
              value={props.item?.reason}
              InputProps={{
                readOnly: item?.isView
              }}
            />
          </Grid>
          <Grid container item md={12} sm={12} xs={12} justifyContent="space-between" className="mt-3 mb-3">
            {!item?.isView &&
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={() => props.handleOpenAssetDialog()}
                  style={{ marginRight: "12px" }}
                  disabled={!Boolean(isAllowEditAndAddAsset)}
                >
                  {t("general.add_asset")}
                </Button>
                <span className="colorRed">(Khi thay đổi số quyết định hoặc đơn vị cung cấp sẽ xóa toàn bộ tài sản)</span>
              </div>
            }
            <div style={{ marginTop: 5 }}>
              <b>{t("ReceivingScrollableTabsButtonForce.amount")}: <span className="colorRed"> {convertNumberPriceRoundUp(props.item?.totalOriginalCost) || 0} VNĐ</span></b>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <CustomMaterialTable
              data={props.item.assetVouchers}
              columns={columns}
              columnActions={columnsActions}
              options={{
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                padding: "dense",
                showSelectAllCheckbox: false,
                rowStyle: (rowData) => ({
                  backgroundColor: item?.selectedRow?.tableData?.id === rowData.tableData.id
                    ? "#ccc"
                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                maxBodyHeight: "285px",
                minBodyHeight: "285px",
              }}
              onSelectionChange={props.handleSelectionChange}
              onRowClick={(e, rowData) => props.setDataSate(rowData, "selectedRow")}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        {(!item.isView || item?.isAllowEdit) &&
          <Grid item md={12} sm={12} xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={props.handleAddAssetDocumentItem}
            >
              {t("AssetFile.addAssetFile")}
            </Button>
          </Grid>
        }

        <Grid item md={12} sm={12} xs={12} className="mt-16">
          <CustomMaterialTable
            data={props.item.assetDocumentList}
            columns={columnsVoucherFile}
            options={{
              toolbar: false,
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              padding: "dense",
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingRight: 10,
                paddingLeft: 10,
                textAlign: "center",
              },
              maxBodyHeight: "285px",
              minBodyHeight: "285px",
            }}
            components={{
              Toolbar: (props) => (
                <div style={{ width: "100%" }}>
                  <MTableToolbar {...props} />
                </div>
              ),
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
        </Grid>
      </TabPanel>

    </div>
  );
}
