import ConstantList from "../../appConfig";
import React, {Component} from "react";
import {Button, Dialog, DialogActions,} from "@material-ui/core";
import {ValidatorForm} from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {
	cancelApproval,
	createReceipt, getAssetsExpectedDepartment,
	getAssetsExpectedQuantity, getSuppliesExpectedDepartment,
	getSuppliesExpectedQuantity,
	updateReceipt,
} from "./ReceiptService";
import NotificationPopup from "../Component/NotificationPopup/NotificationPopup";
import ReceiptScrollableTabsButtonForce from "./Components/ReceiptScrollableTabsButtonForce";
import {getAssetDocumentById, getNewCodeDocument, searchByPage as SearchByPageAsset} from "../Asset/AssetService";
import "../../../styles/views/_loadding.scss";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {appConst, DEFAULT_MAX_STRING_LENGTH, STATUS_DEPARTMENT, variable} from "app/appConst";
import {
  checkObject, convertFromToDate,
  formatDateDto,
  getTheHighestRole,
  getUserInformation,
  handleThrowResponseMessage,
  isCheckLenght,
  PaperComponent, removeEmptyFields
} from "app/appFunction";
import {getListRemainingQuantity} from "../Product/ProductService";
import AppContext from "app/appContext";
import {ConfirmationDialog} from 'egret';
import moment from "moment";
import {isSuccessfulResponse} from "../../appFunction";
import {
	convertAssetItem,
	convertSelectedAssets,
	convertSelectedIats,
	convertSelectedSupplies,
	findAssetItem, findProductItem, getField, getMessage, handleGetRowError
} from "./helper";
import ViewDepartmentByAssetOrProductPopup from "./Components/ViewDepartmentByAssetOrProductPopup";
import {departmentColumns} from "./columns";
import {getDxVoucherAttributes} from "../../appServices";

class SummaryReceiptEditorDialog extends Component {
  state = {
    type: ConstantList.VOUCHER_TYPE.Allocation,
    rowsPerPage: 10,
    page: 0,
    fixedAssets: [],
    iats: [],
    supplies: [],
		expectedFixedAssets: [],
		expectedIats: [],
    expectedSupplies: [],
    statusIndexOrders: [
      appConst.listStatusAssets.LUU_MOI.indexOrder,
      appConst.listStatusAssets.NHAP_KHO.indexOrder,
    ],
    handoverPerson: null,
    managementDepartment: null,
    handoverDepartment: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    suggestDate: new Date(),
    createDate: new Date(),
    shouldOpenDepartmentPopup: false,
    shouldOpenManagementDepartmentPopup: false,
    shouldOpenAssetPopup: false,
    shouldOpenProductPopup: false,
    totalElements: 0,
    asset: {},
    shouldOpenNotificationPopup: false,
    textNotificationPopup: "",
    isAssetAllocation: true,
    shouldOpenPopupAssetFile: false,
    documentType: appConst.documentType.ASSET_DOCUMENT_ALLOCATION,
    assetDocumentList: [],
    listAssetDocumentId: [],
    count: 1,
    allocationStatus: null,
    keyword: '',
    checkPermissionUserDepartment: true,
    check: false,
    assetId: '',
    usePerson: null,
    managementDepartmentId: '',
    documents: [],
    isConfirm: false,
    isCancel: false,
    shouldOpenRefuseTransfer: false,
    shouldOpenConfirmQuantityPopup: false,
    openPrintQR: false,
    isViewAssetFile: false,
    tabValue: appConst.assetTypeObject.TSCD.code,
    error: {
        fixedAssets: false,
        iats: false,
        supplies: false,
    },
    warningMsg: "",
    shouldOpenListExpectedDepartmentPopup: false,
    expectedDepartments: [],
    isUpdateStatus: false,
    currentSubmitStatus: null,
    tabHeaderValue: 0,
    signature: {},
    isViewSignPermission: {
      [variable.listInputName.headOfDepartment]: false,
      [variable.listInputName.deputyDepartment]: false,
      [variable.listInputName.director]: false,
      [variable.listInputName.deputyDirector]: false,
      [variable.listInputName.chiefAccountant]: false,
      [variable.listInputName.storeKeeper]: false,
      [variable.listInputName.creator]: true,
      [variable.listInputName.handoverPerson]: true,
      [variable.listInputName.receiverPerson]: true,
    }
  };

  convertDto = (state, status) => {
    return {
      id: state?.id,
      status: status || state?.status?.indexOrder,
      name: state?.name,
      approvedDate: state?.approvedDate ? formatDateDto(state?.approvedDate) : null,
      storeId: state?.store?.id,
      departmentId: state?.handoverDepartment?.id,
      personName: state?.handoverPerson?.personDisplayName,
      personId: state?.handoverPerson?.personId,
      issueDate: state?.issueDate ? formatDateDto(state?.issueDate) : null,
      suggestDate: state?.suggestDate ? formatDateDto(state?.suggestDate) : null,
      receiptDepartmentId: state?.receiverDepartment?.id,
      receiptPersonName: state?.receiverPerson?.personDisplayName,
      receiptPersonId: state?.receiverPerson?.personId,
      fixedAssets: state?.fixedAssets?.map(i => ({
        assetId: i?.assetId,
        dxRequestQuantity: i?.dxRequestQuantity,
        id: i?.id,
        note: i?.note,
        quantity: i?.approvedQuantity
      })),
      iats: state?.iats?.map(i => ({
        assetId: i?.assetId,
        dxRequestQuantity: i?.dxRequestQuantity,
        id: i?.id,
        note: i?.note,
        quantity: i?.approvedQuantity
      })),
      supplies: state?.supplies
    };
  }

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date,
    });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSelectReceiverPerson = (item) => {
    this.setState({
      receiverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        receiverPersonId: item?.personId,
        receiverPersonName: item?.personDisplayName,
      }
    });
  };
  handleSelectHandoverPerson = (item) => {
    this.setState({
      handoverPerson: item ? item : null,
      signature: {
        ...this.state.signature,
        handoverPersonId: item?.personId,
        handoverPersonName: item?.personDisplayName,
      }
    });
  };

  validateSubmit = () => {
    let {t} = this.props;
    let listData = this.handleGetListDataByTab().value;

    let {
      fixedAssets,
      handoverDepartment,
      receiverDepartment,
      suggestDate,
      iats,
      supplies
    } = this.state;
    const isInvalidSuggestDate = listData.some(
      asset => moment(asset?.dateOfReception || asset?.receiptDate).isAfter(suggestDate, 'date')
    );

    if (isInvalidSuggestDate) { // ngày đề xuất nhỏ hơn ngày tiếp nhận
      toast.warning(t('allocation_asset.minSuggestDate'))
      return false;
    }
    if (!handoverDepartment) {
      toast.warning(t('allocation_asset.missingHandoverDepartment'));
      return false;
    }
    if (fixedAssets?.length === 0 && iats?.length === 0 && supplies?.length === 0) {
      toast.warning(t('receipt.emptyAssetsOrProducts'));
      return false;
    }
    if (this.validateAssetVouchers()) return;
    if (!receiverDepartment) {
      toast.warning(t('allocation_asset.missingReceiverDepartment'));
      return false;
    }
    return true;
  }

  validateAssetVouchers = (status) => {
    let {fixedAssets, iats, supplies} = this.state;
    let check = false;
    let {t} = this.props;
		let invalidNoteAssets = fixedAssets?.filter(x => {
			if(isCheckLenght(x?.note, DEFAULT_MAX_STRING_LENGTH)) {
				return x.code;
			}
		});
		let invalidNoteIats = iats?.filter(x => {
			if(isCheckLenght(x?.note, DEFAULT_MAX_STRING_LENGTH)) {
				return x.code;
			}
		});
		let invalidNoteSupplies = supplies?.filter(x => {
			if(isCheckLenght(x?.note, DEFAULT_MAX_STRING_LENGTH)) {
				return x.code;
			}
		});

    if (invalidNoteAssets?.length > 0) {
      toast.warning(t('allocation_asset.errMessageNote') + " (TSCĐ): " + invalidNoteAssets.join(", "));
      return true;
    }
    if (invalidNoteIats?.length > 0) {
      toast.warning(t('allocation_asset.errMessageNote') + " (CCDC): " + invalidNoteIats.join(", "));
      return true;
    }
    if (invalidNoteSupplies?.length > 0) {
      toast.warning(t('allocation_asset.errMessageNote') + " (CCDC): " + invalidNoteSupplies.join(", "));
      return true;
    }

    return check;
  }
	handleWarningQuantity = (status) => {
		let {fixedAssets = [], iats = [], supplies = [], shouldOpenConfirmQuantityPopup} = this.state;
		let {t} = this.props;
		let invalidIatsRemainQty = [];
		let invalidSuppliesRemainQty = [];
		let invalidIatsRestQty = [];
		let invalidSuppliesRestQty = [];
		let messageByStatus = getMessage(this.state, status);
		let fieldByStatus = getField(this.state, status);
		let assetMessage = "";
		let iatMessages = "";
		let suppliesMessage = "";
		let warningAssets = [];
		let textIndent = "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ";
        const isRefuse = status === appConst.STATUS_RECEIPT.refuse.indexOrder;

		if (shouldOpenConfirmQuantityPopup || isRefuse) {return false;}

		fixedAssets?.forEach(asset => {
			if (asset.requiredQty > 0) {
				return warningAssets.push(asset.code);
			}
		});
		iats?.forEach(iat => {
			let restQty = iat.remainQty - iat.requiredQty > 0
				? iat.remainQty - iat.requiredQty
				: 0;

			if (fieldByStatus.assetQtyField && iat[fieldByStatus.assetQtyField] > iat.remainQty) {
				invalidIatsRemainQty.push(iat.code);
			} else if (fieldByStatus.assetQtyField && iat[fieldByStatus.assetQtyField] > restQty) {
				invalidIatsRestQty.push(iat.code);
			}
		})
		supplies?.forEach(product => {
			let restQty = product.remainingQuantity - product.requiredQty > 0
				? product.remainingQuantity - product.requiredQty
				: 0;

			if (fieldByStatus.productQtyField && product[fieldByStatus.productQtyField] > product.remainingQuantity) {
				invalidSuppliesRemainQty.push(product.code);
			} else if (fieldByStatus.productQtyField && product[fieldByStatus.productQtyField] > restQty) {
				invalidSuppliesRestQty.push(product.productCode);
			}
		})

		//TSCĐ
		if (warningAssets?.length > 0) {
			assetMessage += ("<b>TSCĐ:</b>" + textIndent + "Số lượng dự kiến lớn hơn 1: " + warningAssets.join(", "))
		}

		//CCDC
		if(invalidIatsRemainQty?.length > 0 || invalidIatsRestQty?.length > 0) {
			iatMessages =  "<br><b>CCDC:</b>";
		}
		if (invalidIatsRemainQty?.length > 0) {
			iatMessages += (textIndent + t(messageByStatus.invalidRemainQtyMsg) + ": " + invalidIatsRemainQty.join(", "));
		}
		if (invalidIatsRestQty?.length > 0) {
			iatMessages += (textIndent + t(messageByStatus.invalidRestQtyMsg) + ": " + invalidIatsRestQty.join(", "));
		}

		// VT
		if(invalidSuppliesRemainQty?.length > 0 || invalidSuppliesRestQty?.length > 0) {
			suppliesMessage =  "<br><b>VT:</b>";
		}
		if (invalidSuppliesRemainQty?.length > 0) {
			suppliesMessage += (textIndent + t(messageByStatus.invalidRemainQtyMsg) + ": " + invalidSuppliesRemainQty.join(", "));
			return true;
		} else if (invalidSuppliesRestQty?.length > 0) {
			suppliesMessage += (textIndent + t(messageByStatus.invalidRestQtyMsg) + ": " + invalidSuppliesRestQty.join(", "));
		}

		if (
			warningAssets?.length > 0
			|| invalidIatsRemainQty?.length > 0
			|| invalidIatsRestQty?.length > 0
			|| invalidSuppliesRemainQty?.length > 0
			|| invalidSuppliesRestQty?.length > 0
		) {
			this.setState({
				warningMsg: assetMessage + iatMessages + suppliesMessage,
				shouldOpenConfirmQuantityPopup: true,
			});
			return true;
		}

		return false;
	}

  handleUpdateStatus = async (status) => {
    const { t, handleOKEditClose = () => { }, handleClose = () => { }, currentSubmitStatus } = this.props;
    const isInvalidAssetQuantity = this?.state?.fixedAssets?.find(x => x?.approvedQuantity > 1);

    if (!this.state.approvedDate && this.state?.isConfirm) {
        toast.warning(t('allocation_asset.isEmtyApprovedDate'));
        return;
    } else if (isInvalidAssetQuantity) {
        toast.warning(t('Asset.invalidQuantityAsset'));
        return;
    } else if (this.validateAssetVouchers(status)) {
        return
    // } else if (this.handleWarningQuantity(status) && !currentSubmitStatus) {
    //     return this.setState({currentSubmitStatus: status, isUpdateStatus: true});
    }

    let dataState = this.convertDto(this.state, status)
    let { id } = this.state;

    let res = await updateReceipt(id, dataState);
    if (appConst.CODE.SUCCESS === res.data?.code) {
      handleOKEditClose();
      handleClose();
      toast.success(t("general.updateSuccess"));
    } else {
      handleThrowResponseMessage(res);
    }
  }

  handleCancelApproval = async (status) => {
    const { t, handleOKEditClose = () => { }, handleClose = () => { } } = this.props;
    let { id } = this.state;
    let { setPageLoading } = this.context;
		try {
			setPageLoading(true);
			let dataState = this.convertDto(this.state, status);
			let res = await cancelApproval(id, dataState);
			let {code} = res?.data;
			if (isSuccessfulResponse(code)) {
				handleOKEditClose();
				handleClose();
				toast.success(t("general.updateSuccess"));
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (error) {
			console.error(error);
			toast.error(t("toastr.error"));
		} finally {
			setPageLoading(false);
		}
  }

  handleFormSubmit = async () => {
    const { setPageLoading } = this.context;
    const { id } = this.state;
    const { t, handleOKEditClose = () => { }, handleClose = () => { } } = this.props;

    if (!this.validateSubmit()) return;
		// if (this.handleWarningQuantity()) {return;}

    const dataState = this.convertDto(this.state);
    setPageLoading(true);

    try {
      let res;
      if (id) {
        res = await updateReceipt(id, dataState);
        if (appConst.CODE.SUCCESS === res.data?.code) {
          this.handleSaveSign(res?.data?.data?.id);
          handleOKEditClose();
          handleClose();
          toast.success(t("general.updateSuccess"));
        } else {
          handleThrowResponseMessage(res);
        }
      } else {
        res = await createReceipt(dataState);
        if (appConst.CODE.SUCCESS === res.data?.code) {
          this.handleSaveSign(res?.data?.data?.id);
          handleOKEditClose();
          handleClose();
          toast.success(t("general.addSuccess"));
        } else {
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      console.error(error)
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };

  handleSaveSign = async (voucherId) => {
    try {
      const payload = {
        voucherId,
        ...this.state.signature
      };

      console.log(payload);

    } catch (error) {

    }
  }

  handleChangeSignature = (data = []) => {
    this.setState({
      signature: data,
    })
  }

  handleChangeTabHeader = (event, newValue) => {
    this.setState({ tabHeaderValue: newValue })
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false });
  };

  handleConfirmWarningQuantity = () => {
    let submitFunc = this.state.isUpdateStatus ? this.handleUpdateStatus : this.handleFormSubmit
      submitFunc(this.state.currentSubmitStatus).then(() => {
        this.setState({
            shouldOpenConfirmQuantityPopup: false,
            warningMsg: "",
        });
    });
  };

  handleClosePopup = () => {
    this.setState({
      shouldOpenRefuseTransfer: false,
      shouldOpenConfirmQuantityPopup: false,
      shouldOpenListExpectedDepartmentPopup: false,
      warningMsg: "",
      currentSubmitStatus: null,
      isUpdateStatus: false,
    })
  }

  onMountSetSign = (item) => {
    const { currentUser, organization } = getUserInformation();
    this.setState({
      signature: {
        directorId: organization?.directorId,
        directorName: organization?.directorName,
        deputyDirectorId: organization?.deputyDirectorId,
        deputyDirectorName: organization?.deputyDirectorName,
        chiefAccountantId: organization?.chiefAccountantId,
        chiefAccountantName: organization?.chiefAccountantName,
        headOfDepartmentId: item?.headOfDepartmentId,
        headOfDepartmentName: item?.headOfDepartmentName,
        deputyDepartmentId: item?.deputyDepartmentId,
        deputyDepartmentName: item?.deputyDepartmentName,
        storeKeeperId: item?.storeKeeperId,
        storeKeeperName: item?.storeKeeperName,
        handoverPersonId: item?.personId,
        handoverPersonName: item?.personName,
        receiverPersonId: item?.receiptPersonId,
        receiverPersonName: item?.receiptPersonName,
        creatorId: item?.creatorId || currentUser?.person?.id,
        creatorName: item?.creatorName || currentUser?.person?.displayName,
      }
    })
  }

  componentDidMount() {
    let { item, isVisibility } = this.props;
    const { currentUser, departmentUser } = getUserInformation();
    const { isRoleAssetUser, isRoleAssetManager } = getTheHighestRole();

    if (item?.id) {
      this.setState({
        ...item,
        isVisibility,
        receiverDepartment: {
          text: item?.receiptDepartmentName,
          id: item?.receiptDepartmentId,
        },
        receiverPerson: {
          personId: item?.receiptPersonId,
          personDisplayName: item?.receiptPersonName,
        },
        handoverPerson: {
          personId: item?.personId,
          personDisplayName: item?.personName,
        },
        handoverDepartment: {
          text: item?.departmentName,
          name: item?.departmentName,
          id: item?.departmentId,
        }, store: {
          id: item?.storeId,
          name: item?.storeName,
        },
        fixedAssets: [...item?.fixedAssets?.map(convertAssetItem)],
        iats: [...item?.iats?.map(convertAssetItem)],
        supplies: [...item?.supplies?.map((item) => {
          return {
            ...item,
            asset: {
              name: item?.assetName,
              assetId: item?.assetId
            }
          };
        })]
      }, async () => {
        this.handleGetVoucherAttributes();
        this.handleGetSuppliesRemainQuantity();
				await this.handleGetFixedAssetsExpectedQuantity();
				await this.handleGetIatsExpectedQuantity();
				await this.handleGetSuppliesExpectedQuantity();
				await this.handleFillRequiredQuantityByAll()
				this.handleCheckErrorQuantity();
      });
    } else {
      this.setState({
        ...item,
        isVisibility,
        receiverDepartment: isRoleAssetUser ? departmentUser : null,
        receiverPerson: isRoleAssetUser
          ? {
            personId: currentUser?.person?.id,
            personDisplayName: currentUser?.person?.displayName,
          }
          : null,
        handoverPerson: isRoleAssetManager
          ? {
            personId: currentUser?.person?.id,
            personDisplayName: currentUser?.person?.displayName,
          }
          : null,
        handoverDepartment: isRoleAssetManager ? departmentUser : null,
        status: appConst.STATUS_RECEIPT.waitApprove,
      }, () => {
        this.onMountSetSign(item);
      });
    }
  }

	componentDidUpdate(prevProps, prevState, snapshot) {
		let {fixedAssets, iats, supplies} = this.state;
		const isDifferentFA = prevState.fixedAssets !== fixedAssets;
		const isDifferentIAT = prevState.iats !== iats;
		const isDifferentSupplies = prevState.supplies !== supplies;

		if (isDifferentFA || isDifferentIAT || isDifferentSupplies) {
			this.handleCheckErrorQuantity();
		}
	}

	handleAssetPopupClose = () => {
    this.setState({
      shouldOpenAssetPopup: false,
      shouldOpenProductPopup: false,
    });
  };

  handleAssetPopupOpen = () => {
    let { t } = this.props;
    let { handoverDepartment, store, tabValue } = this.state;
		let getExpectedQuantityFunc = {
			[appConst.assetClass.TSCD]: this.handleGetFixedAssetsExpectedQuantity,
			[appConst.assetClass.CCDC]: this.handleGetIatsExpectedQuantity,
			[appConst.assetClass.VT]: this.handleGetSuppliesExpectedQuantity,
		}
		let popupDialogName = {
			[appConst.assetClass.TSCD]: "shouldOpenAssetPopup",
			[appConst.assetClass.CCDC]: "shouldOpenAssetPopup",
			[appConst.assetClass.VT]: "shouldOpenProductPopup",
		}
		const apiFunc = getExpectedQuantityFunc[tabValue] || (async () => {});

    if (handoverDepartment) {
			apiFunc().then(() => {
				this.setState({
					[popupDialogName[tabValue]]: true,
					item: {},
				}, () => {
				});
			});
    } else if (!handoverDepartment) {
      toast.info(t('allocation_asset.missingHandoverDepartment'));
    } else if (!store) {
      toast.info(t('receipt.missingStore'));
    }
  };

  handleGetListDataByTab = (tab) => {
		let {
			fixedAssets = [],
			iats = [],
			supplies = [],
			tabValue,
			expectedFixedAssets = [],
			expectedIats = [],
			expectedSupplies = [],
		} = this.state;
		let listData = {
      [appConst.assetTypeObject.TSCD.code]: fixedAssets,
      [appConst.assetTypeObject.CCDC.code]: iats,
      [appConst.assetTypeObject.VT.code]: supplies,
    }
    let nameListData = {
      [appConst.assetTypeObject.TSCD.code]: variable.listInputName.fixedAssets,
      [appConst.assetTypeObject.CCDC.code]: variable.listInputName.iats,
      [appConst.assetTypeObject.VT.code]: variable.listInputName.supplies,
    }
    let listExpectedData = {
      [appConst.assetTypeObject.TSCD.code]: expectedFixedAssets,
      [appConst.assetTypeObject.CCDC.code]: expectedIats,
      [appConst.assetTypeObject.VT.code]: expectedSupplies,
    }
    let nameExpectedData = {
      [appConst.assetTypeObject.TSCD.code]: variable.listInputName.expectedFixedAssets,
      [appConst.assetTypeObject.CCDC.code]: variable.listInputName.expectedIats,
      [appConst.assetTypeObject.VT.code]: variable.listInputName.expectedSupplies,
    }

    return {
      value: listData[tab || tabValue] || [],
      name: nameListData[tab || tabValue] || "",
			expectedValue: listExpectedData[tabValue] || [],
			expectedName: nameExpectedData[tabValue] || "",
    };
  }

  removeAssetInlist = (rowData) => {
    const newData = [...this.handleGetListDataByTab().value];
    const name = this.handleGetListDataByTab().name;
    const index = newData.findIndex((x) => x.tableData?.id === rowData.tableData?.id);

    newData.splice(index, 1);
    this.setState({[name]: newData});
  };

  handleSelectAssetAll = (items = []) => {
    let { t } = this.props;
    let { tabValue, supplies, receiverDepartment } = this.state;
    let name = this.handleGetListDataByTab().name;
		console.log(name)
    if (items?.length <= 0) {
      return toast.warning(t("allocation_asset.missingCCDC"));
    }

    const newData = {
      [appConst.assetTypeObject.TSCD.code]: convertSelectedAssets(items, receiverDepartment),
      [appConst.assetTypeObject.CCDC.code]: convertSelectedIats(items, receiverDepartment),
      [appConst.assetTypeObject.VT.code]: convertSelectedSupplies(items, supplies),
    }

    this.setState({
      [name]: [...(newData[tabValue] || [])],
    }, () => {
			this.handleAssetPopupClose();
			this.handleFillRequiredQuantityByTab();
		});
  };

  handleSearchAsset = (e) => {
    let searchObject = {
      keyword: e?.target?.value,
      pageIndex: 1,
      pageSize: 10,
    }
    try {
      SearchByPageAsset(searchObject).then((res) => {
        if (isSuccessfulResponse(res?.data?.code)) {
          this.setState({
            listAsset: res?.data?.data?.content
          })
        }
      });
    } catch (error) {
      console.error(error);
    }
  };

  handleGetSuppliesRemainQuantity = async () => {
    let { supplies = [], storeId } = this.state;

    let { t } = this.props;
    let searchObject = {};
    searchObject.pageIndex = 1;
    searchObject.pageSize = 100000;
    searchObject.storeId = storeId;
    searchObject.productIds = supplies?.map(item => item?.productId).join(',');

    if (supplies?.length === 0) return; // suppliesItems rỗng thì k cần gọi api

    try {
      let res = await getListRemainingQuantity(searchObject)
      const { code, data } = res?.data;

      if (isSuccessfulResponse(code)) {
        supplies?.map(x => {
          let existItem = data?.find(
            item => item?.productId === x?.productId
              && item?.inputDate === x?.receiptDate
              && item?.lotNumber === x?.batchCode
              && item?.unitPrice === x?.price
              && item?.expiryDate === x?.expiryDate
          )
          if (existItem) {
            x.remainingQuantity = existItem.remainQuantity || 0;
          }
          return x;
        });
        this.setState({
          supplies,
        });
      }
      else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("InventoryDeliveryVoucher.getRemainQuantityError"));
    }
  }

  handleSelectRowDataChange = (value, rowData) => {
    let { supplies } = this.state;

    if (supplies.length > 0) {
      supplies.forEach((element) => {
        if (element.productId === rowData.productId) {
          element.asset = value;
          element.assetName = value?.name;
          element.assetId = value?.id;
        }
      });
      this.setState({ supplies });
    }
  };

  handleDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false,
    });
  };

  handleDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenDepartmentPopup: true,
    });
  };

  handleManagementDepartmentPopupClose = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: false,
    });
  };
  handleManagementDepartmentPopupOpen = () => {
    this.setState({
      shouldOpenManagementDepartmentPopup: true,
    });
  };

  handleSelectHandoverDepartment = (item) => {
    let department = item;
    this.setState({
      handoverDepartment: department,
      handoverDepartmentId: department?.id
    }, function () {
      this.handleManagementDepartmentPopupClose();
    });

    if (checkObject(item)) {
      this.setState({ handoverDepartment: null });
    }
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState({
      receiverDepartment: { id: item?.id, name: item?.text },
      receiptDepartmentId: item?.id,
    },
    );
  };

  selectHandoverDepartment = (item) => {
    this.setState({
      handoverDepartment: item ? item : null,
      handoverDepartmentId: item?.id,
      handoverPerson: null,
      store: null,
      managementDepartmentId: item?.id,
      fixedAssets: [],
      iats: [],
      supplies: [],
      listHandoverPerson: [],
    });
  };

  selectReceiverDepartment = (item) => {
    let { fixedAssets } = this.state;
    (fixedAssets?.length > 0) && fixedAssets.forEach((assetVoucher) => {
      assetVoucher.usePerson = null;
      assetVoucher.usePersonId = null;
    }
    );
    this.setState({
      receiverDepartment: item ? item : null,
      receiptDepartmentId: item?.id,
      listReceiverPerson: [],
      fixedAssets: fixedAssets,
      isCheckReceiverDP: !item,
      receiverPerson: null,
      receiverPersonId: null,
      usePerson: null,
    });
  }

  shouldOpenPopupAssetFile = () => {
    this.setState({
      item: null,
      shouldOpenPopupAssetFile: true,
    });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId ? this.state?.listAssetDocumentId : [];
    let documents = this.state?.documents ? this.state?.documents : [];
    document && listAssetDocumentId.push(document?.id);
    document && documents.push(document);
    this.setState({ listAssetDocumentId, documents });
  };

  handleRowDataCellEditAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        }
      );
    });
  };

  handleRowDataCellViewAssetFile = (rowData) => {
    getAssetDocumentById(rowData.id).then(({ data }) => {
      let fileDescriptionIds = [];
      let document = data?.data ? data?.data : null;

      document?.attachments?.map((item) => {
        fileDescriptionIds.push(item?.file?.id)
      })

      document.fileDescriptionIds = fileDescriptionIds;
      document.documentType = this.state?.documentType;

      this.setState(
        {
          item: document,
          shouldOpenPopupAssetFile: true,
          isViewAssetFile: true,
        }
      );
    });
  };

  handleRowDataCellDeleteAssetFile = (id) => {
    let { documents, listAssetDocumentId } = this.state;
    let index = documents?.findIndex(document => document?.id === id);
    let indexId = listAssetDocumentId?.findIndex(documentId => documentId === id);
    documents.splice(index, 1);
    listAssetDocumentId.splice(indexId, 1);
    this.setState({ listAssetDocumentId, documents });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {}
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return
        }
        toast.warning(data?.message)
      }
      )
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  handleUpdateAssetDocument = (document) => {
    let documents = this.state?.documents
    let indexDocument = documents.findIndex(item => item?.id === document?.id)
    documents[indexDocument] = document;
    this.setState({ documents });
  };

  selectAlocationStatus = (item) => {
    this.setState({
      status: item
    });
  };

  handleRowDataCellChange = (rowData, event) => {
    let { fixedAssets, iats, tabValue } = this.state;
    const data = this.handleGetListDataByTab(tabValue);
    const quantityArray = [
      variable.listInputName.quantity,
      variable.listInputName.approvedQuantity,
      variable.listInputName.dxRequestQuantity,
    ];
    data?.value?.map((assetVoucher) => {
      if (assetVoucher.tableData.id === rowData?.tableData?.id) {
        let { name, value } = event.target;
        if (quantityArray.includes(name)) {
          assetVoucher[name] = +value;
        } else {
          assetVoucher[name] = value;
        }
      }
    });
    this.setState({
      fixedAssets,
      iats,
    });
  };

  handleQuantityChange = (rowData, event) => {
    const { name, value } = event.target;

    const { supplies } = this.state;
    supplies?.forEach((element) => {
      if (element?.productId === rowData?.productId
        && element?.receiptDate === rowData?.receiptDate
        && element?.price === rowData?.price
      ) {
        element[name] = value;
        if (name === "quantity") {
          element.amount = Number(value) * element.price;
        }
      }
    });
    this.setState({ supplies });
  };

  handleSetDataSelect = (data, source) => {
    this.setState({
      [source]: data
    })
  }

  handleSetDataStatus = (data) => {
    let { id } = this.state;

    const statusMappings = {
      [appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder]: id ? [
        // Sửa phiếu ở TT mới tạo
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
      ] : [
        // Tạo mới phiếu
        appConst.STATUS_ALLOCATION.MOI_TAO.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder]: [
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder,
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.CHO_XAC_NHAN.indexOrder
      ],
      [appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder]: [
        appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder,
        appConst.STATUS_ALLOCATION.TRA_VE.indexOrder
      ]
    };

    const allocationStatusIndexOrder = this.state.allocationStatus?.indexOrder;
    const dataNew = data.filter((item) => statusMappings[allocationStatusIndexOrder].includes(item.indexOrder));

    this.setState({
      listStatus: dataNew
    });

  };

  handleChangeValueOpenQR = (newValue) => {
    this.setState({
      openPrintQR: newValue
    });
  };

  handleSelect = (value, name) => {
    this.setState({ [name]: value })
    if (name === variable.listInputName.store) {
      this.setState({
        fixedAssets: [],
        iats: [],
        supplies: [],
      })
    }
  }

  handleChangeTabValue = (event, newTab) => {
    this.setState({
      tabValue: newTab,
		}, async () => {
			this.handleGetFixedAssetsExpectedQuantity();
			this.handleGetIatsExpectedQuantity();
			this.handleGetSuppliesExpectedQuantity();
		});
  };

	handleGetFixedAssetsExpectedQuantity = async () => {
		let {expectedFixedAssets = [], handoverDepartment, store, fixedAssets = []} = this.state;
		let {t} = this.props;
		let {setPageLoading} = this.context;

		if (expectedFixedAssets?.length > 0 && handoverDepartment?.id && store?.id) {return;}

		try {
			setPageLoading(true);
			let params = {
				assetClass: appConst.assetClass.TSCD,
				managementDepartmentId: handoverDepartment?.id,
				storeId: store?.id,
			};
			let res = await getAssetsExpectedQuantity(params);
			let {code, data} = res?.data;
			if (isSuccessfulResponse(code)) {
				this.setState({
					expectedFixedAssets: data || [],
				})
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (error) {
			console.error(error);
		} finally {
			setPageLoading(false);
		}
	}

	handleGetIatsExpectedQuantity = async () => {
		let {expectedIats = [], handoverDepartment, store, iats = [],} = this.state;
		let {t} = this.props;
		let {setPageLoading} = this.context;

		if (expectedIats?.length > 0 && handoverDepartment?.id && store?.id) {return;}

		try {
			setPageLoading(true);
			let params = {
				assetClass: appConst.assetClass.CCDC,
				managementDepartmentId: handoverDepartment?.id,
				storeId: store?.id,
			};
			let res = await getAssetsExpectedQuantity(params);
			let {code, data} = res?.data;
			if (isSuccessfulResponse(code)) {
				this.setState({
					expectedIats: data || [],
				})
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (error) {
			console.error(error);
		} finally {
			setPageLoading(false);
		}
	}

	handleGetSuppliesExpectedQuantity = async () => {
		let {expectedSupplies = [], handoverDepartment, store, suggestDate} = this.state;
		let {t} = this.props;
		let {setPageLoading} = this.context;

		if (expectedSupplies?.length > 0 && handoverDepartment?.id && store?.id) {return;}

		try {
			setPageLoading(true);
			let params = {
				date: formatDateDto(suggestDate),
			};
			let res = await getSuppliesExpectedQuantity(store?.id, params);
			let {code, data} = res?.data;
			if (isSuccessfulResponse(code)) {
				this.setState({
					expectedSupplies: data || [],
				})
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (error) {
			console.error(error);
		} finally {
			setPageLoading(false);
		}
	}

	handleFillRequiredQuantityByTab = (tab) => {
		let {
			tabValue,
			expectedFixedAssets = [],
			expectedIats = [],
			expectedSupplies = [],
		} = this.state;
		const nameByTab = this.handleGetListDataByTab(tab).name;
		const listDataByTab = this.handleGetListDataByTab(tab).value;
		const listExpectedByTab = {
			[appConst.assetClass.TSCD]: expectedFixedAssets,
			[appConst.assetClass.CCDC]: expectedIats,
			[appConst.assetClass.VT]: expectedSupplies,
		}
		const findingFunc = {
			[appConst.assetClass.TSCD]: findAssetItem,
			[appConst.assetClass.CCDC]: findAssetItem,
			[appConst.assetClass.VT]: findProductItem,
		}
		const newExpectedArray = listExpectedByTab[tab || tabValue] || [];

		listDataByTab?.map(item => {
			let existItem = findingFunc[tab || tabValue]?.(newExpectedArray, item);
			item.requiredQty = existItem?.requiredQty || 0;
			return item;
		})

		this.setState({
			[nameByTab[tab || tabValue]]: [...listDataByTab],
		})
	}

	handleFillRequiredQuantityByAll = async () => {
		await this.handleFillRequiredQuantityByTab(appConst.assetClass.TSCD);
		await this.handleFillRequiredQuantityByTab(appConst.assetClass.CCDC);
		await this.handleFillRequiredQuantityByTab(appConst.assetClass.VT);
	}

	handleGetRowStyleByTab = (rowData = {}) => {
		let {tabValue} = this.state;
		let error = handleGetRowError(this.state, rowData);

		switch (tabValue) {
			case appConst.assetClass.TSCD:
				return {
					backgroundColor: error.TSCD ? "var(--signal-light-error)" : "white",
				};
			case appConst.assetClass.CCDC:
				return {
					backgroundColor: error.CCDC ? "var(--signal-light-error)" : "white",
				};
			case appConst.assetClass.VT:
				return {
					backgroundColor: error.VT ? "var(--signal-light-error)" : "white",
				};
			default:
				return {};
		}
	}

	handleViewExpectedDepartments = async (rowData) => {
		const {tabValue, store} = this.state;
		const {t} = this.props;
		const {setPageLoading} = this.context;
		const listExpected = this.handleGetListDataByTab().expectedValue;
		const findingFunc = {
			[appConst.assetClass.TSCD]: findAssetItem,
			[appConst.assetClass.CCDC]: findAssetItem,
			[appConst.assetClass.VT]: findProductItem,
		}
		const apiFunc = {
			[appConst.assetClass.TSCD]: getAssetsExpectedDepartment,
			[appConst.assetClass.CCDC]: getAssetsExpectedDepartment,
			[appConst.assetClass.VT]: getSuppliesExpectedDepartment,
		}
		const item = findingFunc[tabValue]?.(listExpected, rowData);
		const convertedItem = removeEmptyFields(item);
		const assetsParams = {
			storeId: store?.id,
			assetId: convertedItem.assetId,
		}
		const suppliesParams = {
			storeId: store?.id,
			...convertedItem,
		}
		const paramsByTab = {
			[appConst.assetClass.TSCD]: assetsParams,
			[appConst.assetClass.CCDC]: assetsParams,
			[appConst.assetClass.VT]: suppliesParams,
		}

		try {
			let params = paramsByTab[tabValue];
			let res = await apiFunc[tabValue]?.(params);
			let {code, data} = res?.data;
			if (isSuccessfulResponse(code)) {
				this.setState({
					expectedDepartments: data || [],
					shouldOpenListExpectedDepartmentPopup: true,
				})
			} else {
				handleThrowResponseMessage(res);
			}
		} catch (error) {
			toast.error(t("general.error"));
		} finally {
			setPageLoading(false);
		}
	}

	handleCheckErrorQuantity = () => {
		let {
			fixedAssets = [],
			iats = [],
			supplies = [],
			error = {},
		} = this.state;
		let fixedAssetsError = fixedAssets?.some(x => x.requiredQty > 0);
		let iatsError = iats?.some(x => x?.requiredQty > x?.remainQty);
		let suppliesError = supplies?.some(x => x?.requiredQty > x?.remainingQuantity);

		error = {
			fixedAssets: fixedAssetsError,
			iats: iatsError,
			supplies: suppliesError,
		}

		this.setState({
			error,
		})
	}
  
  handleGetVoucherAttributes = async () => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    
    try {
      setPageLoading(true);
      let payload = {
        dxId: this.state.id,
      };
      let res = await getDxVoucherAttributes(payload);
      let {code, data} = res?.data;
      if (isSuccessfulResponse(code)) {
        this.setState({
          signature: data,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (err) {
      toast.error(t("general.error"), "error");
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    let { open, t, i18n, isStatus, isHideButton } = this.props;
    let {
      isConfirm,
      isVisibility,
      textNotificationPopup,
      shouldOpenNotificationPopup,
      status,
			isCancel,
    } = this.state;
    let searchObjectStatus = { ...appConst.OBJECT_SEARCH_MAX_SIZE };
    let handoverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isAssetManagement: true,
      departmentId: this.state.handoverDepartment
        ? this.state.handoverDepartment.id
        : null,
    };
    let receiverPersonSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      checkPermissionUserDepartment: true,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };
    let receiverDepartmentSearchObject = {
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
      departmentId: this.state.receiverDepartment
        ? this.state.receiverDepartment.id
        : null,
    };

    const isDangDuyet = status?.indexOrder === appConst.listStatusSuggestSuppliesObject.DANG_DUYET.indexOrder;
    const { isRoleOrgAdmin, departmentUser } = getTheHighestRole();
		const popupDepartmentColumns = departmentColumns({
			t,
			tabValue: this.state.tabValue,
		});

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        scroll={"paper"}
      >
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t("general.noti")}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t("general.agree")}
          />
        )}
        {this.state?.shouldOpenRefuseTransfer &&
          <ConfirmationDialog
            title={t('general.confirm')}
            open={this.state?.shouldOpenRefuseTransfer}
            onConfirmDialogClose={this.handleClosePopup}
            onYesClick={() => this.handleUpdateStatus(appConst.STATUS_RECEIPT.refuse.indexOrder)}
            text={t('general.cancel_receive_assets')}
            agree={t('general.agree')}
            cancel={t('general.cancel')}
          />
        }

        {this.state?.shouldOpenConfirmQuantityPopup &&
          <ConfirmationDialog
            title={t('general.noti')}
						size="sm"
            open={this.state?.shouldOpenConfirmQuantityPopup}
            onConfirmDialogClose={this.handleClosePopup}
            onYesClick={this.handleConfirmWarningQuantity}
            text={this.state.warningMsg}
            agree={t('general.agree')}
            cancel={t('general.cancel')}
          />
        }

				{this.state.shouldOpenListExpectedDepartmentPopup && (
					<ViewDepartmentByAssetOrProductPopup
						t={t}
						open={this.state.shouldOpenListExpectedDepartmentPopup}
						handleClose={this.handleClosePopup}
						data={this.state.expectedDepartments || []}
						columns={popupDepartmentColumns}
					/>
				)}

        <ValidatorForm
          id="parentAssetAllocation"
          ref="form"
          name={variable.listInputName.formAllocation}
          onSubmit={this.handleFormSubmit}
          className="validator-form-scroll-dialog"
        >
          <DialogTitle
            style={{ cursor: "move", paddingBottom: "3px" }}
            id="draggable-dialog-title"
          >
            <span className="">{t("receipt.saveUpdate")}</span>
          </DialogTitle>
          <DialogContent>
            <ReceiptScrollableTabsButtonForce
              t={t}
              i18n={i18n}
              item={this.state}
              searchObjectStatus={searchObjectStatus}
              selectAlocationStatus={this.selectAlocationStatus}
              handleSelectHandoverDepartment={this.handleSelectHandoverDepartment}
              handleManagementDepartmentPopupClose={this.handleManagementDepartmentPopupClose}
              handoverPersonSearchObject={handoverPersonSearchObject}
              receiverPersonSearchObject={receiverPersonSearchObject}
              selectHandoverPerson={this.selectHandoverPerson}
              selectHandoverDepartment={this.selectHandoverDepartment}
              selectReceiverDepartment={this.selectReceiverDepartment}
              receiverDepartmentSearchObject={receiverDepartmentSearchObject}
              handleManagementDepartmentPopupOpen={this.handleManagementDepartmentPopupOpen}
              handleDepartmentPopupOpen={this.handleDepartmentPopupOpen}
              handleSelectReceiverDepartment={this.handleSelectReceiverDepartment}
              handleDateChange={this.handleDateChange}
              handleDepartmentPopupClose={this.handleDepartmentPopupClose}
              handlePersonPopupOpen={this.handlePersonPopupOpen}
              handlePersonPopupClose={this.handlePersonPopupClose}
              handleAssetPopupOpen={this.handleAssetPopupOpen}
              handleSelectAssetAll={this.handleSelectAssetAll}
              handleAssetPopupClose={this.handleAssetPopupClose}
              removeAssetInlist={this.removeAssetInlist}
              selectUsePerson={this.selectUsePerson}
              itemAssetDocument={this.state.item}
              shouldOpenPopupAssetFile={this.shouldOpenPopupAssetFile}
              handleAssetFilePopupClose={this.handleAssetFilePopupClose}
              handleRowDataCellEditAssetFile={this.handleRowDataCellEditAssetFile}
              handleRowDataCellDeleteAssetFile={this.handleRowDataCellDeleteAssetFile}
              handleAddAssetDocumentItem={this.handleAddAssetDocumentItem}
              handleRowDataCellChange={this.handleRowDataCellChange}
              handleUpdateAssetDocument={this.handleUpdateAssetDocument}
              getAssetDocument={this.getAssetDocument}
              isVisibility={isVisibility}
              isStatus={isStatus}
              check={this.state.check}
              handleFormSubmit={this.handleFormSubmit}
              isAllocation={this.props.isAllocation}
              isHideButton={isHideButton}
              managementDepartmentId={this.state.managementDepartmentId}
              handleSetDataSelect={this.handleSetDataSelect}
              handleSetDataStatus={this.handleSetDataStatus}
              handleChange={this.handleChange}
              handleSelectReceiverPerson={this.handleSelectReceiverPerson}
              handleSelectHandoverPerson={this.handleSelectHandoverPerson}
              handleChangeValueOpenQR={this.handleChangeValueOpenQR}
              handleRowDataCellViewAssetFile={this.handleRowDataCellViewAssetFile}
              handleSelect={this.handleSelect}
              handleChangeTabValue={this.handleChangeTabValue}
              tabValue={this.state.tabValue}
              handleQuantityChange={this.handleQuantityChange}
              handleSelectRowDataChange={this.handleSelectRowDataChange}
              handleSearchAsset={this.handleSearchAsset}
							handleGetRowStyleByTab={this.handleGetRowStyleByTab}
							handleViewExpectedDepartments={this.handleViewExpectedDepartments}
              handleChangeSignature={this.handleChangeSignature}
              handleChangeTabHeader={this.handleChangeTabHeader}
            />

          </DialogContent>
          <DialogActions className="pr-24 pb-16">
            <Button
              className="btnClose ml-12"
              variant="contained"
              color="secondary"
              onClick={() =>
              (this.props?.isAllocation
                ? (this.props?.isUpdateAsset
                  ? this.props.handleCloseAllocationEditorDialog()
                  : this.props.handleOKEditClose())
                : this.props.handleClose())
              }
            >
              {t("InstrumentToolsTransfer.close")}
            </Button>
            {isConfirm ? <>
              {!isDangDuyet && (
                isRoleOrgAdmin
                || (this.state?.departmentId === departmentUser?.id)
              ) && <Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={() => this.handleUpdateStatus(appConst.STATUS_RECEIPT.review.indexOrder)}
              >
                  {t("SuggestedSupplies.review")}
                </Button>}
              <Button
                className="ml-12"
                variant="contained"
                color="primary"
                onClick={() => this.handleUpdateStatus(appConst.STATUS_RECEIPT.allocated.indexOrder)}
              >
                {t("InstrumentToolsTransfer.confirm")}
              </Button>
              <Button
                className="btnRefuse ml-12"
                variant="contained"
                onClick={() => {
                  this.setState({
                    shouldOpenRefuseTransfer: true
                  });
                }}
              >
                {t("InstrumentToolsTransfer.refuse")}
              </Button>
            </> : <>
              {(!isVisibility || this.props.isStatus) &&
                <Button
                  id="save"
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  type="submit"
                >
                  {t("general.save")}
                </Button>
              }
              {
                this.state?.allocationStatusIndex === appConst.STATUS_ALLOCATION.DA_CAP_PHAT.indexOrder &&
                <Button
                  variant="contained"
                  className="ml-12"
                  color="primary"
                  onClick={() => {
                    this.handleChangeValueOpenQR(true);
                  }}
                >
                  {t("Asset.PrintQRCode")}
                </Button>
              }
            </>}
						{isCancel && (
							<Button
								className="ml-12"
								variant="contained"
								color="primary"
								onClick={() => this.handleCancelApproval(appConst.STATUS_RECEIPT.cancelApproval.indexOrder)}
							>
								{t("general.cancelApproval")}
							</Button>
						)}
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

SummaryReceiptEditorDialog.contextType = AppContext;
export default SummaryReceiptEditorDialog;
