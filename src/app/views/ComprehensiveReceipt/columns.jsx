import {convertNumberPrice, filterOptions, formatTimestampToDate, handleKeyDownIntegerOnly} from "../../appFunction";
import React from "react";
import { TextValidator, ValidatorForm  } from "react-material-ui-form-validator";
import moment from "moment";
import AutoComplete from "@material-ui/lab/Autocomplete";
import {appConst} from "../../appConst";


export const columnsTSCD = (props) => {
	const {
		t,
		isVisibility,
		isRoleAssetUser,
	} = props;
	let columns = [
		{
			title: t('general.stt'),
			field: 'code',
			maxWidth: 50,
			align: 'left',
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => {
				return ((props.item.page) * props.item.rowsPerPage) + (rowData?.tableData?.id + 1)
			}
		},
		{
			title: t("Asset.code"),
			field: "code",
			align: "left",
			maxWidth: 120,
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("Asset.name"),
			field: "name",
			align: "left",
			minWidth: 250,
		},
		{
			title: t("Asset.serialNumber"),
			field: "serialNumber",
			align: "left",
			minWidth: 150,
		},
		{
			title: t("Asset.model"),
			field: "model",
			align: "left",
			minWidth: 150,
		},
		{
			title: t("receipt.expectedExportQuantity"),
			field: "requiredQty",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("receipt.dxRequestQuantity"),
			field: "quantity",
			minWidth: 100,
			align: "left",
			render: rowData => (
				<TextValidator
					className="w-100"
					type="number"
					name="quantity"
					value={rowData.dxRequestQuantity}
					InputProps={{
						readOnly: isVisibility,
						disableUnderline: isVisibility,
						inputProps: {
							className: "text-center"
						},
					}}
					disabled={!isVisibility}
				/>
			)
		},
		{
			title: t("receipt.approvedQuantity"),
			field: "quantity",
			minWidth: 100,
			align: "left",
			render: rowData => (
				<TextValidator
					className="w-100"
					onChange={e => props.handleRowDataCellChange(rowData, e)}
					onKeyDown={handleKeyDownIntegerOnly}
					type="number"
					name="approvedQuantity"
					value={rowData.approvedQuantity}
					InputProps={{
						readOnly: isVisibility,
						disableUnderline: isVisibility,
						inputProps: {
							className: "text-center"
						},
					}}
					disabled={isRoleAssetUser || !props?.item?.isConfirm}
				/>
			)
		},
		{
			title: t("Asset.yearOfManufactureTable"),
			field: "yearOfManufacture",
			align: "left",
			minWidth: 80,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("Asset.dateOfReception"),
			field: "custom",
			align: "left",
			minWidth: 120,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => rowData.dateOfReception
				? formatTimestampToDate(rowData.dateOfReception)
				: ""
		},
		{
			title: t("Asset.originalCost"),
			field: "originalCost",
			align: "left",
			minWidth: 140,
			cellStyle: {
				textAlign: "right",
			},
			render: rowData => rowData.originalCost
				? convertNumberPrice(rowData.originalCost)
				: ""
		},
		{
			title: t("Product.amount"),
			field: "amount",
			minWidth: "130px",
			cellStyle: {
				textAlign: "right",
			},
			render: (rowData) => {
				let unitPrice = Number(rowData?.originalCost) > 0 ? rowData?.originalCost : rowData?.originalCostPerOne;
				return convertNumberPrice(unitPrice * rowData?.approvedQuantity) || "";
			},
		},
		{
			title: t("Asset.note"),
			field: "note",
			minWidth: 180,
			align: "left",
      render: rowData => (
        <TextValidator
          multiline
          maxRows={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note)}
          type="text"
          name="note"
          value={rowData?.note || ""}
          InputProps={{
            readOnly: isVisibility && props.item?.isEditRefuseStatus,
            disableUnderline: isVisibility && props.item?.isEditRefuseStatus,
          }}
          validators={["maxStringLength:255"]}
          errorMessages={[t("general.errorInput255")]}
        />
      )
		},
	];
	return columns?.map(column => ({
		...column,
		align: "left",
	}))
}

export const columnsCCDC = (props) => { 
  const {
    t, 
    isVisibility,
    isAllowEditQuantityReq, 
    isDisableRecommendedQuantity,
    isRoleAssetUser,
    isAllocated
  } =  props;
  let columns = [
    {
      title: t('general.stt'),
      field: 'code',
      maxWidth: "60px",
      align: 'left',
      cellStyle: {
        textAlign: "center",
        wordBreak: "break-word"
      },
      render: rowData => {
        return ((props?.item?.page) * props?.item?.rowsPerPage) + (rowData?.tableData?.id + 1)
      }
    },
    {
      title: t("InstrumentToolsType.code"),
      field: "code",
      align: "left",
      minWidth: "140px",
    },
    {
      title: t("InstrumentsToolsInventory.name"),
      field: "asset.name",
      align: "left",
      minWidth: "250px",
      maxWidth: "250px",
    },
		{
			title: t("receipt.expectedExportQuantity"),
			field: "requiredQty",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
		},
    {
      title: t("instrumentsToolsReceipt.dxRequestQuantity"),
      field: "asset.dxRequestQuantity",
      align: "center",
      minWidth: "100px",
      render: rowData =>
        <TextValidator
          className="w-100"
          onChange={e => props.handleRowDataCellChange(rowData, e)}
          type="number"
          name="dxRequestQuantity"
          value={rowData.dxRequestQuantity}
          InputProps={{
            readOnly: !isAllowEditQuantityReq ? false : isVisibility,
            inputProps: {
              className: "text-center"
            },
          }}
          disabled={isDisableRecommendedQuantity || isVisibility}
          validators={[
            'required',
            "minNumber:1",
            `maxNumber:${rowData.remainQty}`,
          ]}
          errorMessages={[
            t('general.required'),
            t("general.minNumberError"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
          ]}
        />
    },
    {
      title: t("instrumentsToolsReceipt.approvedQuantity"),
      field: "asset.approvedQuantity",
      align: "center",
      minWidth: "80px",
      render: rowData =>
        <TextValidator
          className="w-100"
          onChange={e => props.handleRowDataCellChange(rowData, e)}
          type="number"
          name="approvedQuantity"
          value={rowData.approvedQuantity}
          InputProps={{
            readOnly: isVisibility && !props.item?.isConfirm,
            inputProps: {
              className: "text-center"
            },
          }}
          disabled={isRoleAssetUser || !props.item?.isConfirm}
          validators={[
            'required',
            "minNumber:0",
            `maxNumber:${rowData.remainQty}`,
          ]}
          errorMessages={[
            t('general.required'),
            t("general.errorNumber0"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
          ]}
        />
    },
		{
			title: t("receipt.remainQty"),
			field: "remainQty",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
		},
		{
			title: t("receipt.restReceiptQuantity"),
			field: "",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => rowData.remainQty - rowData.requiredQty > 0
				? rowData.remainQty - rowData.requiredQty
				: 0
		},
    {
      title: t("Asset.stockKeepingUnitTable"),
      field: "skuName",
      minWidth: "100px",
    },
    {
      title: t("Product.price"),
      field: "unitPrice",
      minWidth: "120px",
      align: "right",
      render: (rowData) => convertNumberPrice(rowData?.unitPrice || 0)
    },
    {
      title: t("Product.amount"),
      field: "amount",
      minWidth: "130px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        let quantityAsset = props.item?.isConfirm || isAllocated ? rowData?.approvedQuantity : rowData?.dxRequestQuantity
        return rowData?.unitPrice ? convertNumberPrice(rowData?.unitPrice * quantityAsset) : convertNumberPrice(rowData?.originalCostPerOne * quantityAsset)
      },
    },
    {
      title: t("Model"),
      field: "model",
      minWidth: "120px",
    },
    {
      title: t("InstrumentToolsList.serialNumber"),
      field: "serialNumber",
      minWidth: "120px",
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: "180px",
      align: "left",
      render: rowData =>(
        <TextValidator
          multiline
          maxRows={2}
          className="w-100"
          onChange={note => props.handleRowDataCellChange(rowData, note)}
          type="text"
          name="note"
          value={rowData?.note || ""}
          InputProps={{
            readOnly: isVisibility && isAllowEditQuantityReq,
            disableUnderline: isVisibility && isAllowEditQuantityReq,
          }}
          validators={["maxStringLength:255"]}
          errorMessages={[t("general.errorInput255")]}
        />
      )
    },
  ];
  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
}

export const columnsVT = (props) => {
  const {
    t, 
    permission, 
    handleKeyDownIntegerOnly, 
    isSatusRowData, 
    isView, 
    handleSelectRowDataChange,
    handleSearchAsset,
    isConfirm,
    listAsset,
    handleRowDataCellChange,
    isQuantity,
    isVisibility,
    handleQuantityChange
  } =  props;  
  
  let columns = [
    {
      title: t("InventoryDeliveryVoucher.stt"),
      field: "",
      minWidth: "50px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => 
        props.item.page * props.item.rowsPerPage + (rowData.tableData.id + 1),
    },
    {
      title: t("SuggestedSupplies.codeSupplies"),
      field: "productCode",
      minWidth: "165px",
      align: "left",
    },
    {
      title: t("SuggestedSupplies.nameSupplies"),
      field: "productName",
      align: "left",
      minWidth: "250px",
    },
    {
      title: t("Product.stockKeepingUnit"),
      field: "skuName",
      minWidth: "120px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
    {
      title: t("InventoryReceivingVoucher.batchCode"),
      field: "batchCode",
      minWidth: "120px",
      align: "left",
      cellStyle: {
        textAlign: "center",
      },
    },
		{
			title: t("receipt.expectedExportQuantity"),
			field: "requiredQty",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
		},
    {
      title: t("Product.recommendedQuantity"),
      field: "quantityOfVoucher",
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          onChange={(event) => handleQuantityChange(rowData, event)}
          name="quantityOfVoucher"
          id="formatted-numberformat-originalCost"
          value={rowData.quantityOfVoucher || ""}
          validators={[
            "minFloat:0.000000001",
            "required",
            `maxNumber:${rowData.remainingQuantity}`,
            "matchRegexp:^\\d+(\\.\\d{1,2})?$"
          ]}
          errorMessages={[
            t("general.minNumberError"),
            t("general.required"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
            t("general.quantityError")
          ]}
          disabled={permission?.rcmQuantity?.disable || isVisibility}
          InputProps={{
            inputProps: {
              style: {
                textAlign: "center",
              },
            },
            readOnly: permission?.rcmQuantity?.readOnly
          }}
        />
      ),
    },
    {
      title: t("Product.browsersQuantity"),
      field: "quantity",
      align: "left",
      minWidth: "100px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <TextValidator
          onChange={(event) => handleQuantityChange(rowData, event)}
          onKeyDown={handleKeyDownIntegerOnly}
          type="number"
          name="quantity"
          id="formatted-numberformat-originalCost"
          value={rowData.quantity}
          validators={[
            "minNumber:0",
            "required",
            `maxNumber:${rowData.remainingQuantity}`,
            "matchRegexp:^\\d+(\\.\\d{1,2})?$"
          ]}
          errorMessages={[
            t("general.minNumberError"),
            t("general.required"),
            t("InventoryDeliveryVoucher.quantity_cannot_be_greater_than_remaining_quantity"),
            t("general.quantityError")
          ]}
          disabled={permission?.approvedQuantity?.disable || isVisibility}
          InputProps={{
            inputProps: {
              style: {
                textAlign: "center",
              },
            },
            readOnly: permission?.approvedQuantity?.readOnly
          }}
        />
      ),
    },
    {
      title: t("InventoryDeliveryVoucher.remainingQuantity"),
      align: "center",
      hidden: isQuantity || isVisibility,
      minWidth: "90px",
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => <span>{rowData?.remainingQuantity || 0}</span>,
    },
		{
			title: t("receipt.restReceiptQuantity"),
			field: "",
			align: "left",
			minWidth: 100,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => rowData.remainingQuantity - rowData.requiredQty > 0
				? rowData.remainingQuantity - rowData.requiredQty
				: 0
		},
		{
			title: t("InventoryReceivingVoucher.dateAdded"),
			field: "receiptDate",
			minWidth: "120px",
			align: "left",
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData?.receiptDate ? moment(rowData?.receiptDate).format("DD/MM/YYYY") : ""
		},
		{
			title: t("InventoryReceivingVoucher.expiryDate"),
			field: "expiryDate",
			minWidth: "120px",
			align: "left",
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => rowData?.expiryDate ? moment(rowData?.expiryDate).format("DD/MM/YYYY") : ""
		},
    {
      title: t("InventoryDeliveryVoucher.selectAsset"),
      field: "valueText",
      minWidth: 200,
      render: (rowData) => isView || isSatusRowData ? (
        <TextValidator
          fullWidth
          InputProps={{
            readOnly: true,
          }}
          name="note"
          label={t("Asset.searchAsset")}
          value={rowData?.asset?.name || ""}
        />
      ) : (
        <AutoComplete
          id="combo-box"
          size="small"
          options={listAsset || []}
          onChange={(event, value) => handleSelectRowDataChange(
            value,
            rowData,
          )}
          value={rowData?.asset || null}
          getOptionLabel={(option) => option.name || ""}
          filterOptions={(options, params) => filterOptions(options, params)}
          renderInput={(params) => (
            <TextValidator
              {...params}
              label={t("Asset.searchAsset")}
              placeholder={t("Asset.SimilarProperty")}
              variant="standard"
              onChange={handleSearchAsset}
              onFocus={handleSearchAsset}
              value={""}
            />
          )}
          noOptionsText={t("general.noOption")}
        />
      ),
    },
    {
      title: t("Product.price"),
      field: "price",
      align: "left",
      minWidth: "130px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData.price
        ? convertNumberPrice(rowData.price)
        : "",
    },
    {
      title: t("Product.amount"),
      field: "amount",
      minWidth: "130px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => rowData.amount
        ? convertNumberPrice(rowData.amount)
        : "",
    },
    {
      title: t("Asset.note"),
      field: "note",
      minWidth: 180,
      align: "left",
      render: rowData =>
        <TextValidator
          multiline
          maxRows={2}
          className="w-100"
          onChange={(event) => handleRowDataCellChange(rowData, event)}
          type="text"
          name="note"
          value={rowData.note || ""}
          InputProps={{
            readOnly: isView && !isConfirm,
          }}
        />
    },
  ];
  return columns?.map(column => ({
    ...column,
    align: "left",
  }))
}

export const departmentColumns = (props) => {
	const {t, tabValue} = props;
	const isVT = tabValue === appConst.assetClass.VT
	return [
		{
			title: t("receipt.suggestDate"),
			field: "suggestDate",
			align: "left",
			minWidth: 120,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => formatTimestampToDate(rowData.suggestDate),
		},
		{
			title: t("Asset.dateOfReception"),
			field: "suggestDate",
			align: "left",
			minWidth: 120,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => formatTimestampToDate(rowData.dateOfReception || rowData.receiptDate),
		},
		{
			title: t("receipt.expectedExportQuantity"),
			field: "quantity",
			align: "left",
			minWidth: 80,
			cellStyle: {
				textAlign: "center",
			},
			render: rowData => rowData.quantity || rowData.requiredQty,
		},
		{
			title: t("allocation_asset.receiverDepartment"),
			field: "receiveDepartmentName",
			minWidth: 350,
			align: "left",
			render: rowData => rowData.receiveDepartmentName || rowData.receiptDepartmentName,
		},
	];
}