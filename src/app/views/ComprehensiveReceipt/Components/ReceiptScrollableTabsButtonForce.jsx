import {
  Button,
  Grid,
  Icon,
  IconButton,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { appConst, STATUS_STORE } from "app/appConst";
import {
  checkInvalidDate,
  getTheHighestRole,
  filterOptions,
  getUserInformation,
  a11yProps,
} from "app/appFunction";
import ConstantList from "app/appConfig";
import React, { useCallback, useEffect, useState } from "react";
import { TextValidator } from "react-material-ui-form-validator";
import SelectAssetAllPopup from "../../Component/Asset/SelectAssetAllPopup";
import SelectAssetAllPopupFromTools from "../../InstrumentsToolsReceipt/SelectAssetAllPopup";
import CustomValidatePicker from "../../Component/ValidatePicker/ValidatePicker";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import { getListUserByDepartmentId } from "../../AssetTransfer/AssetTransferService";
import { searchByPage } from "../../Store/StoreService";
import { getUserDepartmentAll, searchByPageDepartmentNew } from "../../Department/DepartmentService";
import SelectMultiProductPopup from "../../Component/Product/SelectMultiProductPopup";
import CustomMaterialTable from "../../CustomMaterialTable";
import { columnsTSCD, columnsCCDC, columnsVT } from "../columns";
import { TabPanel } from "../../Component/Utilities";
import { handleGetRowError } from "../helper";
import { SignatureTabComponentV2 } from "app/views/Component/Signature/SignatureTabComponentV2";


function MaterialButton(props) {
  const { item = {}, state = {} } = props;
  const { status } = state;
  const { DANG_THONG_KE, DA_THONG_KE } = appConst.listStatusSuggestSuppliesObject;
  const isDangThongKe = status?.indexOrder === DANG_THONG_KE.indexOrder;
  const isDaThongKe = status?.indexOrder === DA_THONG_KE.indexOrder;
  let error = handleGetRowError(state, item);

  return <div>
    {(error.TSCD || error.CCDC || error.VT) && item.requiredQty > 0 && (
      <IconButton size='small' onClick={() => props.onSelect(item, appConst.active.view)}>
        <Icon fontSize="small" color="primary">visibility</Icon>
      </IconButton>
    )}
    {(isDangThongKe || isDaThongKe) && (
      <IconButton size='small' onClick={() => props.onSelect(item, appConst.active.delete)}>
        <Icon fontSize="small" color="error">delete</Icon>
      </IconButton>
    )}
  </div>;
}

export default function ReceiptScrollableTabsButtonForce(props) {
  const {
    t,
    i18n,
    tabValue,
    handleChangeTabValue,
    handleRowDataCellChange,
    handleSelectRowDataChange,
    handleSearchAsset,
    handleQuantityChange
  } = props;
  let {
    statusEdit,
    listAsset,
    error = {},
    value,
  } = props?.item;

  let roles = getTheHighestRole();
  let { isRoleAssetUser, isRoleAssetManager, departmentUser } = roles;
  const { refuse, allocated, approved, waitApprove, review } = appConst.STATUS_RECEIPT;
  const { DANG_THONG_KE, DA_THONG_KE, XAC_NHAN } = appConst.listStatusSuggestSuppliesObject;

  const [searchParamUserByHandoverDepartment, setSearchParamUserByHandoverDepartment] = useState({ departmentId: '' });
  const [searchParamUserByReceiverDepartment, setSearchParamUserByReceiverDepartment] = useState({ departmentId: '' });

  const data = {
    [appConst.assetClass.TSCD]: props.item.fixedAssets,
    [appConst.assetClass.CCDC]: props.item.iats,
    [appConst.assetClass.VT]: props.item.supplies,
  }
  const objPermission = {
    [appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: {
      rcmQuantity: { disable: false, readOnly: false, },
      approvedQuantity: { disable: true, readOnly: true, }
    },
    [appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: {
      rcmQuantity: { disable: true, readOnly: true, },
      approvedQuantity: { disable: false, readOnly: false, }
    },
    [appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: {
      rcmQuantity: { disable: false, readOnly: false, },
      approvedQuantity: { disable: false, readOnly: false, }
    },
    [appConst.listStatusSuggestSuppliesObject.TU_CHOI.code]: {
      rcmQuantity: { disable: false, readOnly: false, },
      approvedQuantity: { disable: true, readOnly: true, },
      receiptDepartment: { disable: true, readOnly: true, }
    },
    [null]: {
      rcmQuantity: { disable: false, readOnly: false, },
      approvedQuantity: { disable: true, readOnly: true, }
    },
    [undefined]: {
      rcmQuantity: { disable: false, readOnly: false, },
      approvedQuantity: { disable: true, readOnly: true, }
    },
  }
  const permission = objPermission[props.item?.status?.indexOrder];
  const isQuantity = props.item?.status?.indexOrder === XAC_NHAN.code;
  const isDangThongKe = props.item?.status?.indexOrder === DANG_THONG_KE.indexOrder;
  const isDaThongKe = props.item?.status?.indexOrder === DA_THONG_KE.indexOrder;
  let isShowCode = props.item?.id;
  let isNotChangeStatus = props.item?.id && (
    statusEdit === approved.indexOrder
  );
  let searchObjectStore = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    managementDepartmentId: props?.item?.handoverDepartment?.id,
    isActive: STATUS_STORE.HOAT_DONG.code
  };
  let isDisableRecommendedQuantity = (
    isRoleAssetManager && props?.item?.id
    && props?.item?.createByDepartmentId !== departmentUser?.id
  )
    || statusEdit === approved.indexOrder;
  let optionsStatus = statusEdit === refuse.indexOrder
    ? appConst.LIST_RECEIPT.filter(
      i => [refuse.indexOrder, approved.indexOrder].includes(i?.indexOrder)
    )
    : appConst.LIST_RECEIPT.filter(
      i => [waitApprove.indexOrder, approved.indexOrder].includes(i?.indexOrder)
    )
  let isShowApproveDate = [refuse.indexOrder, allocated.indexOrder].includes(statusEdit)
    || props?.item?.isConfirm;


  useEffect(() => {
    setSearchParamUserByHandoverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.handoverDepartment?.id ?? '',
    });

    setSearchParamUserByReceiverDepartment({
      ...appConst.OBJECT_SEARCH_MAX_SIZE,
      departmentId: props?.item?.receiverDepartment?.id ?? '',
    });
  }, [props?.item?.handoverDepartment?.id, props?.item?.receiverDepartment]);

  const renderMaterialTable = (
    title,
    data,
    columns,
    columnActions = [],
  ) => {
    return (
      <CustomMaterialTable
        title={title}
        data={data}
        columns={columns}
        columnActions={columnActions}
        options={{
          draggable: false,
          sorting: false,
          minBodyHeight: 250,
          maxBodyHeight: 250,
          rowStyle: props.handleGetRowStyleByTab
        }}
      />
    );
  };

  let columnActions = [{
    title: t("general.action"),
    field: "custom",
    align: "center",
    hidden: props.isVisibility,
    minWidth: 80,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => (
      <MaterialButton
        item={rowData}
        state={{
          isConfirm: props.item.isConfirm,
          status: props.item.status,
          tabValue: props.item.tabValue,
        }}
        onSelect={(rowData, method) => {
          if (appConst.active.delete === method) {
            props.removeAssetInlist(rowData);
          } else if (appConst.active.view === method) {
            props.handleViewExpectedDepartments(rowData);
          } else {
            alert('Call Selected Here:' + rowData.id);
          }
        }}
      />
    ),
  },
  ];

  const handleBlurDate = (date, name) => {
    if (checkInvalidDate(date)) {
      props.handleDateChange(null, name)
    }
  }
  const searchDepartmentObject = {
    ...appConst.OBJECT_SEARCH_MAX_SIZE,
    isAssetManagement: true,
    orgId: getUserInformation().organization?.org?.id,
  };

  const handleCheckTabValue = (tabValue) => {
    let title = "";
    if (tabValue === appConst.assetTypeObject.TSCD.code) title = "general.select_asset";
    if (tabValue === appConst.assetTypeObject.CCDC.code) title = "general.select_iat";
    if (tabValue === appConst.assetTypeObject.VT.code) title = "general.select_product";
    return title;
  }

  const columnsByTab = {
    [appConst.assetTypeObject.TSCD.code]: columnsTSCD,
    [appConst.assetTypeObject.CCDC.code]: columnsCCDC,
    [appConst.assetTypeObject.VT.code]: columnsVT,
  }
  const columns = columnsByTab[tabValue]?.({
    t,
    item: props.item,
    listAsset: listAsset,
    handleSearchAsset,
    handleSelectRowDataChange,
    handleRowDataCellChange,
    permission,
    isQuantity,
    isVisibility: props.isVisibility,
    handleQuantityChange,
    isRoleAssetUser,
    isDisableRecommendedQuantity,
  }) || [];

  return (
    <>
      <AppBar position="static" color="default">
        <Tabs
          value={props.item?.tabHeaderValue}
          onChange={props.handleChangeTabHeader}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab
            label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.NAME)}
            {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE)}
          />
           <Tab
            label={t(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.NAME)}
            {...a11yProps(appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE)}
          />
        </Tabs>
      </AppBar>
      <TabPanel
        value={props.item?.tabHeaderValue}
        index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.INFO.CODE}
      >
        <Grid container spacing={1} >
          {isShowCode && <Grid item md={2} sm={6} xs={6}>
            <TextValidator
              className={props.className ? props.className : "w-100"}
              label={t("receipt.code")}
              value={props.item?.code || ""}
              InputProps={{
                readOnly: true,
              }}
            />
          </Grid>}
          <Grid item md={isShowCode ? 2 : 4} sm={isShowCode ? 6 : 12} xs={isShowCode ? 6 : 12}>
            <CustomValidatePicker
              fullWidth
              margin="none"
              id="mui-pickers-date"
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t('receipt.suggestDate')}
                </span>}
              inputVariant="standard"
              type="text"
              autoOk={true}
              format="dd/MM/yyyy"
              name={'suggestDate'}
              value={props?.item?.suggestDate || ""}
              onChange={date => props.handleDateChange(date, "suggestDate")}
              readOnly={props.isVisibility || isNotChangeStatus}
              minDate={
                !props.isVisibility && (
                  props.isAllocation
                    ? (new Date(props?.item?.dateOfReception))
                    : new Date("1/1/1900")
                )
              }
              maxDate={!props.isVisibility && new Date()}
              maxDateMessage={t("general.maxDateNow")}
              InputProps={{
                readOnly: props.isVisibility || props?.isAllocation,
              }}
              invalidDateMessage={t("general.invalidDateFormat")}
              validators={['required']}
              errorMessages={[t('general.required')]}
              onBlur={() => handleBlurDate(props?.item?.suggestDate, "suggestDate")}
            />
          </Grid>
          {isShowApproveDate && <Grid item md={4} sm={12} xs={12}>
            <CustomValidatePicker
              className="w-100"
              label={
                <span>
                  <span className="colorRed">*</span>
                  {t('Asset.approvedDate')}
                </span>
              }
              autoOk
              name={'approvedDate'}
              value={props?.item?.approvedDate || null}
              onChange={date => props.handleDateChange(date, "approvedDate")}
              readOnly={props.isVisibility && ![refuse.indexOrder].includes(statusEdit)}
              minDate={new Date(props?.item?.suggestDate)}
              minDateMessage={t("allocation_asset.minApprovedDateMessage")}
              maxDate={new Date()}
              maxDateMessage={t("general.maxDateNow")}
              invalidDateMessage={t("general.invalidDateFormat")}
              validators={['required']}
              errorMessages={[t('general.required')]}
              onBlur={() => handleBlurDate(props?.item?.approvedDate, "approvedDate")}
            />
          </Grid>}
          <Grid item md={isShowApproveDate ? 4 : 8} sm={12} xs={12}>
            <TextValidator
              className={props.className ? props.className : "w-100"}
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("receipt.name")}
                </span>
              }
              InputProps={{
                readOnly: props.isVisibility || isNotChangeStatus,
              }}
              value={props.item?.name || ""}
              name="name"
              onChange={props?.handleChange}
              validators={["required"]}
              errorMessages={[t("general.required")]}
            />
          </Grid>

          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  {t("maintainRequest.status")}
                </span>
              }
              searchFunction={() => { }}
              listData={optionsStatus || []}
              setListData={props.handleSetDataStatus}
              value={props.item?.status ? props.item?.status : null}
              displayLable={'name'}
              selectedOptionKey="indexOrder"
              readOnly={(props.isVisibility && !props.isStatus) || isNotChangeStatus}
              onSelect={props.selectAlocationStatus}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              validators={["required"]}
              errorMessages={[t('general.required')]}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12} >
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  <span> {t("allocation_asset.handoverDepartment")}</span>
                </span>
              }
              searchFunction={searchByPageDepartmentNew}
              searchObject={searchDepartmentObject}
              listData={props.item.listHandoverDepartment}
              setListData={(data) => props.handleSetDataSelect(data, "listHandoverDepartment")}
              displayLable={'name'}
              isNoRenderParent
              isNoRenderChildren
              showCode="code"
              readOnly={props.isVisibility || props?.isAllocation || isNotChangeStatus || isRoleAssetManager}
              value={props.item?.handoverDepartment || null}
              onSelect={props.selectHandoverDepartment}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              typeReturnFunction="category"
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>

          <Grid item md={4} sm={12} xs={12} >
            <AsynchronousAutocompleteSub
              className="w-100"
              label={t("allocation_asset.handoverPerson")}
              disabled={!props?.item?.handoverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByHandoverDepartment}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              selectedOptionKey="personId"
              readOnly={props.isVisibility || isNotChangeStatus}
              value={props?.item?.handoverPerson ?? null}
              onSelect={handoverPerson => props?.handleSelectHandoverPerson(handoverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span style={{ color: "red" }}>* </span>
                  <span> {t("receipt.store")}</span>
                </span>
              }
              searchFunction={searchByPage}
              searchObject={searchObjectStore}
              displayLable={'name'}
              readOnly={props.isVisibility || isNotChangeStatus}
              value={props.item?.store ? props.item?.store : null}
              onSelect={(value) => props.handleSelect(value, "store", tabValue)}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              filterOptions={filterOptions}
              disabled={!props?.item?.handoverDepartment?.id}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12} >
            <AsynchronousAutocompleteSub
              label={
                <span>
                  <span className="colorRed">* </span>
                  <span> {t("allocation_asset.receiverDepartment")}</span>
                </span>
              }
              searchFunction={getUserDepartmentAll}
              searchObject={props.receiverDepartmentSearchObject}
              listData={props.item.listReceiverDepartment}
              setListData={(data) => props.handleSetDataSelect(data, "listReceiverDepartment")}
              displayLable={'text'}
              isNoRenderChildren
              isNoRenderParent
              readOnly={props.isVisibility || isNotChangeStatus || isRoleAssetUser}
              value={props.item.receiverDepartment ? props.item.receiverDepartment : null}
              onSelect={props.selectReceiverDepartment}
              validators={["required"]}
              errorMessages={[t('general.required')]}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
          <Grid item md={4} sm={12} xs={12} >
            <AsynchronousAutocompleteSub
              className="w-100"
              label={
                <span>
                  {t("AssetTransfer.receiverPerson")}
                </span>
              }
              disabled={!props?.item?.receiverDepartment?.id}
              searchFunction={getListUserByDepartmentId}
              searchObject={searchParamUserByReceiverDepartment}
              displayLable="personDisplayName"
              typeReturnFunction="category"
              selectedOptionKey="personId"
              readOnly={props.isVisibility || isNotChangeStatus}
              value={props?.item?.receiverPerson ?? null}
              onSelect={receiverPerson => props?.handleSelectReceiverPerson(receiverPerson)}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
            />
          </Grid>
        </Grid>
        <Grid container>
          <div className="w-100 mt-20 mb-20">
            <AppBar position="static" color="default">
              <Tabs
                className="tabsStatus"
                value={tabValue}
                onChange={handleChangeTabValue}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab
                  value={appConst.assetTypeObject.TSCD.code}
                  className="tab"
                  label={
                    <div className="tabLable">
                      {error?.fixedAssets && (
                        <Icon fontSize="small" color="error" className="animation-ripple">
                          priority_high rounded
                        </Icon>
                      )}
                      <span>{t("summary_receipt.tabTSCD")}</span>
                      <div className="tabQuantity tabQuantity-info">
                        {props.item?.fixedAssets.length || 0}
                      </div>
                    </div>
                  }
                />
                <Tab
                  value={appConst.assetTypeObject.CCDC.code}
                  className="tab"
                  label={
                    <div className="tabLable">
                      {error?.iats && (
                        <Icon fontSize="small" color="error" className="animation-ripple">
                          priority_high rounded
                        </Icon>
                      )}
                      <span>{t("summary_receipt.tabCCDC")}</span>
                      <div className="tabQuantity tabQuantity-info">
                        {props.item?.iats.length || 0}
                      </div>
                    </div>
                  }
                />
                <Tab
                  value={appConst.assetTypeObject.VT.code}
                  className="tab"
                  label={
                    <div className="tabLable">
                      {error?.supplies && (
                        <Icon fontSize="small" color="error" className="animation-ripple">
                          priority_high rounded
                        </Icon>
                      )}
                      <span>{t("summary_receipt.tabVTYT")}</span>
                      <div className="tabQuantity tabQuantity-info">
                        {props.item?.supplies.length || 0}
                      </div>
                    </div>
                  }
                />
              </Tabs>
            </AppBar>
          </div>
          <Grid item md={2} sm={12} xs={12}>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className="w-100"
              disabled={
                (!isDangThongKe && !isDaThongKe)
                || !props.item.store?.id
                || isNotChangeStatus
                || props.isVisibility
              }
              onClick={props.handleAssetPopupOpen}
            >
              {t(handleCheckTabValue(tabValue))}
            </Button>
            {props.item.shouldOpenAssetPopup && (
              tabValue === appConst.assetTypeObject.TSCD.code ? (
                <SelectAssetAllPopup
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAssetAll}
                  assetVouchers={data[tabValue] || []}
                  handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                  voucherType={props.item.type}
                  handoverDepartment={props.item?.handoverDepartment}
                  isAssetAllocation
                  statusIndexOrders={props.item.statusIndexOrders}
                  dateOfReceptionTop={props.item.issueDate}
                  getAssetDocumentList={props.getAssetDocumentList}
                  managementDepartmentId={props.managementDepartmentId}
                  issueDate={props.item?.issueDate}
                  storeId={props.item?.store?.id}
                  isSearchForAllocation
                />
              ) :
                <SelectAssetAllPopupFromTools
                  open={props.item.shouldOpenAssetPopup}
                  handleSelect={props.handleSelectAssetAll}
                  assetVouchers={data[tabValue] || []}
                  handleClose={props.handleAssetPopupClose} t={t} i18n={i18n}
                  handoverDepartment={props.item?.handoverDepartment}
                  isAssetAllocation={props.item.isAssetAllocation}
                  statusIndexOrders={props.item.statusIndexOrders}
                  dateOfReceptionTop={props.item.issueDate}
                  getAssetDocumentList={props.getAssetDocumentList}
                  managementDepartmentId={props.item?.handoverDepartment?.id}
                  storeId={props.item?.store?.id}
                  isSearchForAllocation
                />
            )}
            {props.item.shouldOpenProductPopup && (
              <SelectMultiProductPopup
                open={props.item.shouldOpenProductPopup}
                handleSelect={props.handleSelectAssetAll}
                product={props.item?.product ? props.item?.product : []}
                handleClose={props.handleAssetPopupClose}
                t={t}
                i18n={i18n}
                voucherType={ConstantList.VOUCHER_TYPE.StockIn}
                storeId={props.item?.store?.id}
                assetVouchers={props.item.supplies ? props.item.supplies : []}
                date={props.item?.suggestDate}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <TabPanel
              value={tabValue}
              index={tabValue}
              boxProps={{ className: "px-0" }}
            >
              {renderMaterialTable(
                "general.list",
                data[tabValue],
                columns || [],
                columnActions,
              )}
            </TabPanel>
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel
        value={props.item?.tabHeaderValue}
        index={appConst.TABS_INSTRUMENTS_TOOL_ALLOCATION_DIALOG.SIGN.CODE}>
        <SignatureTabComponentV2
          data={{ ...props.item?.signature }}
          permission={props.item?.isViewSignPermission}
          handleChange={props.handleChangeSignature}
        />
      </TabPanel>
    </>

  );
}