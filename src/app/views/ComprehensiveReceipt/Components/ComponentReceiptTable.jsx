import {
    Button,
    Card,
    Collapse,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    TablePagination,
    TextField
} from '@material-ui/core';
import React, { useContext } from 'react';
import { ConfirmationDialog } from 'egret';
import NotificationPopup from '../../Component/NotificationPopup/NotificationPopup';
import SearchIcon from '@material-ui/icons/Search';
import SummaryReceiptEditorDialog from '../ReceiptEditorDialog';
import { MTableToolbar } from 'material-table';
import CustomMaterialTable from '../../CustomMaterialTable';
import { appConst, variable } from 'app/appConst';
import { ValidatorForm } from "react-material-ui-form-validator";
import { filterOptions } from "app/appFunction";
import AsynchronousAutocompleteSub from "../../utilities/AsynchronousAutocompleteSub";
import {getListManagementDepartment, importExcelAssetAllocationURL, searchReceiverDepartment,} from '../ReceiptService';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import { Autocomplete } from '@material-ui/lab';
import AssetsQRPrintNew from '../../Asset/ComponentPopups/AssetsQRPrintNew';
import CardContent from "@material-ui/core/CardContent";
import ImportExcelDialog from "../../Component/ImportExcel/ImportExcelDialog";
import { PrintMultipleFormDialog } from "../../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../../FormCustom/constant";
import AppContext from "../../../appContext";
import {receiptPrintData} from "../../FormCustom/Receipt";

function ComponentReceiptTable(props) {
    let { t, i18n } = props;
    let {
        item,
        page,
        toDate,
        itemList,
        isStatus,
        fromDate,
        status,
        rowsPerPage,
        isVisibility,
        isHideButton,
        totalElements,
        shouldOpenPrint,
        isRoleAssetUser,
        openAdvanceSearch,
        handoverDepartment,
        receiverDepartment,
        hasCreatePermission,
        listHandoverDepartment,
        listReceiverDepartment,
        shouldOpenEditorDialog,
        shouldOpenImportExcelDialog,
        shouldOpenNotificationPopup,
        shouldOpenConfirmationDialog,
        shouldOpenConfirmationDeleteAllDialog,
        openPrintQR,
        products,
        tabValue,
        itemAsset
    } = props?.item;
    const { currentOrg } = useContext(AppContext);
    const { RECEIPT } = LIST_PRINT_FORM_BY_ORG.FIXED_ASSET_MANAGEMENT;
    let isButtonAll = tabValue === appConst.OBJECT_TAB_RECEIPT.all;
    const searchObject = { pageIndex: 1, pageSize: 1000000 };
    const searchObjectDepartment = {
        ...searchObject,
        checkPermissionUserDepartment: false
    }

    let dataView = receiptPrintData(props.item);
    
    return (
        <Grid container spacing={2} justifyContent="space-between">
            <Grid item md={7} xs={12} >
                {hasCreatePermission && isButtonAll && (
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            props.handleEditItem({
                                startDate: new Date(),
                                endDate: new Date(),
                                isCheckReceiverDP: true,
                                isCheckHandoverDP: true,
                            });
                            props.setState({
                                isVisibility: false,
                                isStatus: false,
                                isHideButton: true,
                            });
                        }}
                    >
                        {t('general.add')}
                    </Button>
                )}
                <Button
                    className="mr-16 align-bottom"
                    variant="contained"
                    color="primary"
                    onClick={props.handleOpenAdvanceSearch}
                >
                    {t("general.advancedSearch")}
                    <ArrowDropDownIcon />
                </Button>

                {shouldOpenImportExcelDialog && (
                    <ImportExcelDialog
                        t={t}
                        i18n={i18n}
                        open={shouldOpenImportExcelDialog}
                        handleClose={props?.handleDialogClose}
                        handleOKEditClose={props?.handleOKEditClose}
                        exportExampleImportExcel={props.exportExampleImportExcel}
                        url={importExcelAssetAllocationURL()}
                    />
                )}

                {shouldOpenConfirmationDeleteAllDialog && (
                    <ConfirmationDialog
                        open={shouldOpenConfirmationDeleteAllDialog}
                        onConfirmDialogClose={props.handleDialogClose}
                        onYesClick={props.handleDeleteAll}
                        text={t('general.deleteAllConfirm')}
                    />
                )}

                {shouldOpenNotificationPopup && (
                    <NotificationPopup
                        title={t('general.noti')}
                        open={shouldOpenNotificationPopup}
                        onYesClick={props?.handleNotificationPopup}
                        text={t('allocation_asset.not_edit')}
                        agree={t('general.agree')}
                    />
                )}
                {openPrintQR && (
                    <AssetsQRPrintNew
                        t={t}
                        i18n={i18n}
                        handleClose={props.handleCloseQrCode}
                        open={openPrintQR}
                        items={products}
                    />
                )}
            </Grid>
            <Grid item md={5} sm={12} xs={12}>
                <FormControl fullWidth>
                    <Input
                        className="search_box w-100"
                        onChange={props.handleTextChange}
                        onKeyDown={props.handleKeyDownEnterSearch}
                        onKeyUp={props?.handleKeyUp}
                        placeholder={t('receipt.enterSearch')}
                        id="search_box"
                        value={props.item?.keyword || ""}
                        startAdornment={
                          <InputAdornment position='end'>
                            <SearchIcon onClick={() => props.search()} className="searchTable"/>
                          </InputAdornment>
                        }
                    />
                </FormControl>
            </Grid>

            {/* Bộ lọc Tìm kiếm nâng cao */}
            <Grid item xs={12} style={{ padding: "0 8px" }}>
                <Collapse in={openAdvanceSearch} >
                    <ValidatorForm onSubmit={() => { }}>
                        <Card elevation={2}>
                            <CardContent>
                                <Grid container spacing={2}>
                                    {/* Phòng bàn giao */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteSub
                                            label={t("allocation_asset.handoverDepartment")}
                                            searchFunction={getListManagementDepartment}
                                            searchObject={searchObject}
                                            listData={listHandoverDepartment || []}
                                            typeReturnFunction="list"
                                            setListData={(data) => props?.handleSetDataSelect(
                                                data,
                                                "listHandoverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            defaultValue={handoverDepartment ? handoverDepartment : null}
                                            displayLable={"text"}
                                            value={handoverDepartment ? handoverDepartment : null}
                                            onSelect={(data) => props?.handleSetDataSelect(data, "handoverDepartment")}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Phòng tiếp nhận */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <AsynchronousAutocompleteSub
                                            className="w-100"
                                            label={t("allocation_asset.receiverDepartment")}
                                            searchFunction={searchReceiverDepartment}
                                            searchObject={searchObjectDepartment}
                                            listData={listReceiverDepartment || []}
                                            setListData={(data) => props.handleSetDataSelect(
                                                data,
                                                "listReceiverDepartment",
                                                variable.listInputName.listData,
                                            )}
                                            defaultValue={receiverDepartment ? receiverDepartment : null}
                                            displayLable={"text"}
                                            value={receiverDepartment ? receiverDepartment : null}
                                            onSelect={data => props?.handleSetDataSelect(data, "receiverDepartment")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            disabled={isRoleAssetUser}
                                            noOptionsText={t("general.noOption")}
                                        />
                                    </Grid>
                                    {/* Từ ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxFrom")}
                                                format="dd/MM/yyyy"
                                                value={fromDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "fromDate"
                                                )}
                                                maxDate={toDate || undefined}
                                                KeyboardButtonProps={{ "aria-label": "change date", }}
                                                minDateMessage={t("general.minYearDefault")}
                                                maxDateMessage={toDate ? t("general.maxDateFromDate") : t("general.maxDateDefault")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearable
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Đến ngày */}
                                    <Grid item xs={12} sm={12} md={3}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                            <KeyboardDatePicker
                                                margin="none"
                                                fullWidth
                                                autoOk
                                                id="date-picker-dialog"
                                                label={t("MaintainPlaning.dxTo")}
                                                format="dd/MM/yyyy"
                                                value={toDate ?? null}
                                                onChange={(data) => props.handleSetDataSelect(
                                                    data,
                                                    "toDate"
                                                )}
                                                minDate={fromDate || undefined}
                                                KeyboardButtonProps={{ "aria-label": "change date", }} 
                                                minDateMessage={fromDate ? t("general.minDateToDate") : t("general.minYearDefault")}
                                                maxDateMessage={t("general.maxDateMessage")}
                                                invalidDateMessage={t("general.invalidDateFormat")}
                                                clearable
                                                clearLabel={t("general.remove")}
                                                cancelLabel={t("general.cancel")}
                                                okLabel={t("general.select")}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* Trạng thái */}
                                    {isButtonAll && <Grid item xs={12} sm={12} md={3}>
                                        <Autocomplete
                                            fullWidth
                                            options={appConst.LIST_RECEIPT}
                                            defaultValue={status ? status : null}
                                            value={status ? status : null}
                                            onChange={(e, value) => props.handleSetDataSelect(value, "status")}
                                            filterOptions={(options, params) => {
                                                params.inputValue = params.inputValue.trim()
                                                return filterOptions(options, params)
                                            }}
                                            getOptionLabel={option => option.name}
                                            noOptionsText={t("general.noOption")}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    value={status?.name || ""}
                                                    label={t("maintainRequest.status")}
                                                />
                                            )}
                                        />
                                    </Grid>}
                                </Grid>
                            </CardContent>
                        </Card>
                    </ValidatorForm>
                </Collapse>
            </Grid>

            <Grid item xs={12}>
                <div>
                    {shouldOpenEditorDialog && (
                        <SummaryReceiptEditorDialog
                            t={t}
                            i18n={i18n}
                            open={shouldOpenEditorDialog}
                            handleClose={props.handleDialogClose}
                            handleOKEditClose={props.handleOKEditClose}
                            item={props?.item?.item}
                            isVisibility={isVisibility}
                            isStatus={isStatus}
                            isHideButton={isHideButton}
                            exportToExcel={props.exportToExcel}
                        />
                    )}


                    {shouldOpenPrint && (
                        <PrintMultipleFormDialog
                            t={t}
                            i18n={i18n}
                            handleClose={props?.handleDialogClose}
                            open={shouldOpenPrint}
                            item={dataView || item}
                            title={t("Phiếu lĩnh TSCĐ")}
                            urls={[
                                ...RECEIPT.GENERAL,
                                ...(RECEIPT[currentOrg?.printCode] || []),
                            ]}
                        />
                    )}

                    {shouldOpenConfirmationDialog && (
                        <ConfirmationDialog
                            title={t('general.confirm')}
                            open={shouldOpenConfirmationDialog}
                            onConfirmDialogClose={props.handleDialogClose}
                            onYesClick={props.handleConfirmationResponse}
                            text={t('receipt.cancel_asset_receipt')}
                            agree={t('general.agree')}
                            cancel={t('general.cancel')}
                        />
                    )}
                </div>
                <CustomMaterialTable
                    title={t('general.list')}
                    data={itemList}
                    onRowClick={(e, rowData) => { return props?.setItemState(rowData) }}
                    columns={props?.columns}
                    localization={{
                        body: {
                            emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`,
                        },
                    }}
                    options={{
                        draggable: false,
                        selection: false,
                        actionsColumnIndex: -1,
                        paging: false,
                        search: false,
                        sorting: false,
                        rowStyle: (rowData) => ({
                            backgroundColor: itemAsset?.id === rowData?.id
                                ? "#ccc"
                                : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                        }),
                        maxBodyHeight: "490px",
                        minBodyHeight: "260px",
                        headerStyle: {
                            backgroundColor: "#358600",
                            color: "#fff",
                            paddingLeft: 10,
                            paddingRight: 10,
                            textAlign: "center",
                        },
                        padding: "dense",
                        toolbar: false,
                    }}
                    components={{
                        Toolbar: (props) => <MTableToolbar {...props} />,
                    }}
                    onSelectionChange={(rows) => {
                        this.data = rows;
                    }}
                />
                <TablePagination
                    align="left"
                    className="px-16"
                    rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
                    component="div"
                    labelRowsPerPage={t('general.rows_per_page')}
                    labelDisplayedRows={({ from, to, count }) =>
                        `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`
                    }
                    count={totalElements}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{ 'aria-label': 'Previous Page', }}
                    nextIconButtonProps={{ 'aria-label': 'Next Page', }}
                    onPageChange={props.handleChangePage}
                    onRowsPerPageChange={props.setRowsPerPage}
                />
            </Grid>
        </Grid>
    );
}

export default ComponentReceiptTable;
