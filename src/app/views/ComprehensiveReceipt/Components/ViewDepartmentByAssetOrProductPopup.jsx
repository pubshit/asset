import {Button, Dialog, DialogActions, Grid, TablePagination} from "@material-ui/core";
import {PaperComponent} from "../../Component/Utilities";
import DialogTitle from "@material-ui/core/DialogTitle";
import {ValidatorForm} from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import {appConst} from "../../../appConst";
import MaterialTable from "material-table";
import React, {useState} from "react";
import PropTypes from "prop-types";

function ViewDepartmentByAssetOrProductPopup (props) {
	const {
		t,
		open,
		handleClose = () => {},
		data = [],
		columns = [],
	} = props;
	const [state, setState] = useState({
		itemList: [...data],
	});
	const mergedColumns = [
		{
			title: t("general.index"),
			field: "",
			maxWidth: 50,
			align: "left",
			cellStyle: {
				textAlign: "center",
			},
			render: (rowData) => (rowData.tableData.id + 1),
		},
		...columns,
	]
	
	return (
		<Dialog
			onClose={handleClose}
			open={open}
			PaperComponent={PaperComponent}
			maxWidth={"lg"}
			fullWidth
		>
			<DialogTitle className="cursor-move">
				<span>{t("receipt.listExpectedReceiptDepartment")}</span>
			</DialogTitle>
			<ValidatorForm onSubmit={() => { }}>
				<DialogContent>
					<Grid >
						<MaterialTable
							data={state.itemList}
							columns={mergedColumns}
							localization={{
								body: {
									emptyDataSourceMessage: `${t("general.emptyDataMessageTable")}`,
								},
							}}
							options={{
								draggable: false,
								toolbar: false,
								selection: false,
								actionsColumnIndex: -1,
								pageSize: appConst.rowsPerPage.category,
								sorting: false,
								paging: true,
								search: false,
								padding: "dense",
								rowStyle: (rowData) => ({
									backgroundColor:
										rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
								}),
								maxBodyHeight: 400,
								minBodyHeight: 280,
							}}
						/>
					</Grid>
				</DialogContent>
			</ValidatorForm>
			<DialogActions>
				<div className="flex flex-space-between flex-middle">
					<Button
						className="mr-12 align-bottom"
						variant="contained"
						color="secondary"
						onClick={() => handleClose()}
					>
						{t("general.cancel")}
					</Button>
				</div>
			</DialogActions>
		</Dialog>
	)
}

export default ViewDepartmentByAssetOrProductPopup;
ViewDepartmentByAssetOrProductPopup.propTypes = {
	t: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired,
	handleClose: PropTypes.func.isRequired,
	data: PropTypes.array,
	columns: PropTypes.array,
}
