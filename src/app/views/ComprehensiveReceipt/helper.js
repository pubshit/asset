import {appConst, variable} from "../../appConst";


export const convertAssetItem = (item) => ({
  ...item,
  unitName: item?.skuName,
  approvedQuantity: item?.quantity,
  asset: {
    ...item,
    code: item.code || item.assetCode || null,
    id: item.assetId,
  },
});

export const convertSelectedAssets = (arr = [], useDepartment) => {
	return arr?.map(asset => ({
		...asset,
		requiredQty: asset.requiredQty || 0,
		code: asset?.code || asset?.asset?.code,
		serialNumber: asset?.serialNumber || asset?.asset?.serialNumber,
		model: asset?.model || asset?.asset?.model,
		yearOfManufacture: asset?.yearOfManufacture || asset?.asset?.yearOfManufacture,
		originalCost: asset?.originalCost || asset?.asset?.originalCost,
		dateOfReception: asset?.dateOfReception || asset?.asset?.dateOfReception,
		assetId: asset?.assetId || asset?.asset?.id,
		name: asset?.name || asset?.asset?.name,
		dxRequestQuantity: 1,
		approvedQuantity: asset?.approvedQuantity || 1,
		asset: {
			...asset?.asset,
			useDepartment,
		},
	}));
}

export const convertSelectedIats = (arr = [], useDepartment) => arr?.map(iat => ({
  ...iat,
	requiredQty: iat.requiredQty || 0,
  code: iat.code || iat?.asset?.code,
  serialNumber: iat.serialNumber || iat?.asset?.serialNumber,
  model: iat.model || iat?.asset?.model,
  yearOfManufacture: iat.yearOfManufacture || iat?.asset?.yearOfManufacture,
  originalCost: iat.originalCost || iat?.asset?.originalCost,
  dateOfReception: iat.dateOfReception || iat?.asset?.dateOfReception,
  assetId: iat?.asset?.assetId,
  dxRequestQuantity: iat?.dxRequestQuantity || iat?.asset?.quantity,
  remainQty: iat?.remainQty || iat?.asset?.quantity,
  approvedQuantity: iat.approvedQuantity || iat.dxRequestQuantity || iat?.asset?.quantity,
  skuName: iat?.asset?.unitName,
  unitPrice: iat?.asset?.unitPrice,
  asset: {
    ...iat.asset,
    useDepartment,
  },
}));

export const convertSelectedSupplies = (arr, supplies) => {
  return arr?.map(element => {
    const existingItem = supplies?.find((supply) =>
      (supply?.product?.id === element?.id || supply?.productId === element?.id) &&
      (supply?.product?.inputDate === element?.inputDate || supply?.receiptDate === element?.inputDate) &&
      (supply?.product?.lotNumber === element?.lotNumber || supply?.batchCode === element?.lotNumber) &&
      (supply?.product?.unitPrice === element?.unitPrice
        || supply?.price === element?.unitPrice
        || supply?.amount === element?.unitPrice) &&
      (supply?.product?.code === element?.code || supply?.productCode === element?.code)
    );

    return {
      product: { ...element },
      skuName: element?.sku?.name,
      skuId: element?.sku?.id,
      remainingQuantity: element?.remainingQuantity,
      productName: element.name,
      productCode: element.code,
      productId: element.id,
      price: element?.unitPrice,
      quantityOfVoucher: existingItem?.quantityOfVoucher || 1,
      quantity: existingItem?.quantity || 1,
      amount: element?.amount || element?.unitPrice,
      receiptDate: element?.receiptDate || element?.inputDate,
      batchCode: element?.batchCode || element?.lotNumber,
      expiryDate: element?.expiryDate,
    };
  })
};

export const findAssetItem = (array = [], item = {}) => {
	return array?.find(x => x.assetId === item.assetId);
}

export const findProductItem = (array = [], item = {}) => {
	return array?.find(
		x => x.productId === item.productId
		&& x.price === item.price
		&& x.expiryDate === item.expiryDate
		&& x.receiptDate === item.receiptDate
		&& x.batchCode === item.batchCode
		&& x.skuId === item.skuId
	);
}

export const getMessage = (state = {}, status) => {
	let invalidRemainQtyMsg = {
		[appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: "receipt.invalidDxRemainQty",
		[appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: "receipt.invalidDxRemainQty",
		[appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: "receipt.invalidRemainQty",
		[appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code]: "receipt.invalidRemainQty",
	}
	let invalidRestQtyMsg = {
		[appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: "receipt.invalidDxRestQty",
		[appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: "receipt.invalidDxRestQty",
		[appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: "receipt.invalidRestQty",
		[appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code]: "receipt.invalidRestQty",
	}
	
	return {
		invalidRemainQtyMsg: invalidRemainQtyMsg[status || state?.status?.indexOrder],
		invalidRestQtyMsg: invalidRestQtyMsg[status || state?.status?.indexOrder],
	}
}

export const getField = (state = {}, status) => {
	let assetQtyField = {
		[appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: "dxRequestQuantity",
		[appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: "dxRequestQuantity",
		[appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: "quantity",
		[appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code]: "quantity",
	}
	let productQtyField = {
		[appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.code]: "quantityOfVoucher",
		[appConst.listStatusSuggestSuppliesObject.DA_THONG_KE.code]: "quantityOfVoucher",
		[appConst.listStatusSuggestSuppliesObject.DANG_DUYET.code]: "quantity",
		[appConst.listStatusSuggestSuppliesObject.XAC_NHAN.code]: "quantity",
	}
	
	return {
		assetQtyField: state?.isConfirm
			? variable.listInputName.quantity
			: assetQtyField[status || state?.status?.indexOrder],
		productQtyField: state?.isConfirm
			? variable.listInputName.quantity
			: productQtyField[status || state?.status?.indexOrder],
	}
}

export const handleGetRowError = (state = {}, rowData = {}) => {
	let {tabValue, isConfirm} = state;
	let quantityField = isConfirm
		? variable.listInputName.quantity
		: variable.listInputName.dxRequestQuantity;
	let prodQuantityField = isConfirm
		? variable.listInputName.quantity
		: variable.listInputName.quantityOfVoucher;
	let error = {
		TSCD: false,
		CCDC: false,
		VT: false,
	}
	
	switch (tabValue) {
		case appConst.assetClass.TSCD:
			error.TSCD = rowData?.requiredQty > 0;
			break;
		case appConst.assetClass.CCDC:
			error.CCDC = (rowData?.requiredQty + (rowData[quantityField] || 0)) > rowData?.remainQty;
			break;
		case appConst.assetClass.VT:
			error.VT = (rowData?.requiredQty + (rowData[prodQuantityField] || 0)) > rowData?.remainingQuantity;
			break;
		default:
			break;
	}
	
	return {...error};
}
