import axios from "axios";
import ConstantList from "../../appConfig";
import { STATUS_DEPARTMENT } from "app/appConst";
const API_PATH_voucher =
  ConstantList.API_ENPOINT + "/api/voucher" + ConstantList.URL_PREFIX;
const API_PATH_person =
  ConstantList.API_ENPOINT + "/api/user_department" + ConstantList.URL_PREFIX;
const API_PATH_user_department =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_EXPORTTOEXCEL =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/excel";
const API_PATH_ASSET_DEPARTMENT =
  ConstantList.API_ENPOINT + "/api/assetDepartment" + ConstantList.URL_PREFIX;
const API_PATH_NEW = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api";
const API_PATH_RECEIPT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/phieu-linh";
const API_PATH_TEMPLATE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/template";

//api mới
export const searchByPage = (searchObject) => {
  let url = API_PATH_RECEIPT + "/search-by-page";
  return axios.post(url, searchObject);
};
export const createReceipt = (data) => {
  return axios.post(API_PATH_RECEIPT, data);
};
export const updateReceipt = (id, data) => {
  let url = API_PATH_RECEIPT + "/" + id;
  return axios.put(url, data);
};
export const getItemById = (id) => {
  let url = API_PATH_RECEIPT + "/" + id;
  return axios.get(url);
};
export const deleteItem = (id) => {
  let url = API_PATH_RECEIPT + "/" + id;
  return axios.delete(url);
};
export const cancelApproval = (id, data) => {
  let url = API_PATH_NEW + `/phieu-linh/${id}/cancel-approval`;
  return axios.put(url);
};
export const getCountStatus = () => {
  let url = API_PATH_RECEIPT + "/count-by-status";
  return axios.get(url);
};

export const addNewOrUpdate = (asset) => {
  if (asset.id) {
    return axios.put(API_PATH_voucher + "/" + asset.id, asset);
  } else {
    return axios.post(API_PATH_voucher, asset);
  }
};

export const personSearchByPage = (searchObject) => {
  var url = API_PATH_person + "/searchByPageWithUserDepartment";
  return axios.post(url, searchObject);
};

export const exportToExcel = (searchObject) => {
  return axios({
    method: "post",
    url: API_PATH_EXPORTTOEXCEL + "/cap-phat-tai-san-co-dinh",
    data: searchObject,
    responseType: "blob",
  });
};
export const getUserDepartmentByUserId = (userId) => {
  return axios.get(API_PATH_person + "/getUserDepartmentByUserId/" + userId);
};
export const findDepartment = (userId) => {
  return axios.get(API_PATH_user_department + "/findDepartment/" + userId);
};
export const getListManagementDepartment = () => {
  let config = { params: { isActive: STATUS_DEPARTMENT.HOAT_DONG.code } };
  return axios.get(
    API_PATH_ASSET_DEPARTMENT + "/getListManagementDepartment",
    config
  );
};
export const searchReceiverDepartment = (searchObject) => {
  return axios.post(API_PATH_ASSET_DEPARTMENT + "/searchByPage", {
    isActive: STATUS_DEPARTMENT.HOAT_DONG.code,
    ...searchObject,
  });
};

export const getListQRCode = (id) => {
  let url = API_PATH_NEW + `/voucher/print-asset-qr-code/${id}`;
  return axios.get(url);
};

export const exportExampleImportExcelAssetAllocation = () => {
  return axios({
    method: "post",
    url: API_PATH_TEMPLATE + "/import-allocated-fixed-asset",
    responseType: "blob",
  });
};

export const importExcelAssetAllocationURL = () => {
  return (
    ConstantList.API_ENPOINT_ASSET_MAINTANE +
    "/api/fixed-assets/transfer-vouchers/excel/import"
  );
};

export const getAssetsExpectedQuantity = (params) => {
	let url = API_PATH_RECEIPT + `/asset/expected-export/quantity`;
	return axios.get(url, {params});
};

export const getSuppliesExpectedQuantity = (storeId, params) => {
	let url = API_PATH_RECEIPT + `/supplies/expected-export/${storeId}/quantity`;
	return axios.get(url, {params});
};

export const getAssetsExpectedDepartment = (params) => {
	let {assetId, ...rest} = params;
	let url = API_PATH_RECEIPT + `/asset/${assetId}/expected-export`;
	return axios.get(url, {params: rest});
};

export const getSuppliesExpectedDepartment = (params) => {
	let {storeId, ...rest} = params;
	let url = API_PATH_RECEIPT + `/supplies/expected-export/${storeId}`;
	return axios.post(url, {...rest});
};
