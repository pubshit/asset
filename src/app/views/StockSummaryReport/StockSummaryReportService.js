import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_StockSummaryReport = ConstantList.API_ENPOINT + "/api/summary_report/stock_summary_report/searchByPage";

export const searchByPage = (searchObject) => {
  return axios.post(API_PATH_StockSummaryReport, searchObject);
};
