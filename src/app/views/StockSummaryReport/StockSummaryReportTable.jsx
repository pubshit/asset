import React, { Component } from "react";
import {
  Grid,
  IconButton,
  Icon,
  TablePagination,
  Button,
  TextField,
  FormControl,
  Input, InputAdornment,
} from "@material-ui/core";
import AsynchronousAutocomplete from '../utilities/AsynchronousAutocomplete'
import MaterialTable, { MTableToolbar, Chip, MTableBody, MTableHeader } from 'material-table';
import {searchByPage } from "./StockSummaryReportService";
import StockSummaryReportDialog from "./StockSummaryReportDialog";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
import moment from "moment";
import { saveAs } from 'file-saver';
import DateFnsUtils from "@date-io/date-fns";
import { Helmet } from 'react-helmet';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
import ConstantList from "../../appConfig";
import { searchByPage as storeSearchByPage } from '../Store/StoreService'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
    marginLeft: '-1.5em',
  }
}))(Tooltip);

function MaterialButton(props) {
  const { t, i18n } = useTranslation();
  const item = props.item;

  return <div className="none_wrap">
    <LightTooltip title={t('general.viewDetail')} placement="right-end" enterDelay={300} leaveDelay={200}
      PopperProps={{
        popperOptions: { modifiers: { offset: { enabled: true, offset: '10px, 0px', }, }, },
      }} >
      <IconButton size="small" onClick={() => props.onSelect(item, 0)}>
        <Icon fontSize="small" color="primary">visibility</Icon>
      </IconButton>
    </LightTooltip>
  </div>;
}

class StockSummaryReportTable extends React.Component {
  state = {
    keyword: '',
    rowsPerPage: 10,
    page: 0,
    store: {},
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    shouldOpenConfirmationDeleteAllDialog: false
  };
  numSelected = 0;
  rowCount = 0;

  selectStore = (storeSelected) => {
    this.setState({ store: storeSelected }, function () {
      this.search()
    })
  }

  handleTextChange = event => {
    this.setState({ keyword: event.target.value }, function () {
    })
  };

  handleKeyDownEnterSearch = e => {
    if (e.key === 'Enter') {
      this.search();
    }
  };

  setPage = page => {
    this.setState({ page }, function () {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  search() {
    this.setState({ page: 0 }, function () {
      var searchObject = {};
      searchObject.keyword = this.state.keyword.trim();
      searchObject.pageIndex = this.state.page;
      searchObject.pageSize = this.state.rowsPerPage;
      searchByPage(searchObject).then(({ data }) => {
        this.setState({ itemList: [...data.content], totalElements: data.totalElements })
      });
    });
  }

  updatePageData = () => {
    var searchObject = {};
    searchObject.keyword = this.state.keyword;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchByPage(searchObject).then(({ data }) => {
      this.setState({ itemList: [...data.content], totalElements: data.totalElements })
    });
  };

  handleDownload = () => {
    var blob = new Blob(["Hello, world!"], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "hello world.txt");
  }
  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false
    });
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false
    });
    this.updatePageData();
  };

  handleDeleteAssetTransfer = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };
  componentDidMount() {
    this.updatePageData()
  }

  componentWillMount() { }

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    if (item.checked == null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    var selectAllItem = true;
    for (var i = 0; i < AssetTransfer.length; i++) {
      if (AssetTransfer[i].checked == null || AssetTransfer[i].checked == false) {
        selectAllItem = false;
      }
      if (AssetTransfer[i].id == item.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({ selectAllItem: selectAllItem, AssetTransfer: AssetTransfer });

  };
  handleSelectAllClick = (event) => {
    let { AssetTransfer } = this.state;
    for (var i = 0; i < AssetTransfer.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({ selectAllItem: !this.state.selectAllItem, AssetTransfer: AssetTransfer });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  render() {
    const { t, i18n } = this.props;
    let searchObject = { pageIndex: 0, pageSize: 1000000 }
    let {
      keyword,
      rowsPerPage,
      page,
      totalElements,
      itemList,
      item,
      store,
      shouldOpenConfirmationDialog,
      shouldOpenEditorDialog,
      shouldOpenConfirmationDeleteAllDialog
    } = this.state;
    let TitlePage = t("StockSummaryReport.title");
    
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        width: "120px",
        headerStyle:{
          paddingLeft:'5px'
        },
        render: rowData => <MaterialButton item={rowData}
          onSelect={(rowData, method) => {
            
          }}
        />
      },
      {
        title: t("StockSummaryReport.stt"), field: "", width: "50px", align: "left",
        headerStyle:{
          paddingLeft:'0px',
          paddingRight:'10px'
        },
        render: rowData => ((page) * rowsPerPage) + (rowData.tableData.id + 1)
      },
      {
        title: t("StockSummaryReport.issueDate"), width: "150",
        render: rowData =>
          (rowData.issueDate) ? <span>{moment(rowData.issueDate).format('DD/MM/YYYY')}</span> : ''
      },
      { title: t("StockSummaryReport.code"), field: "voucherCode", width: "150" },
      { title: t("StockSummaryReport.store"), field: "stockReceiptDeliveryStore.name", width: "150" },
      { title: t("StockSummaryReport.totalAmountReceipt"), field: "totalAmountReceipt", width: "150" },
      { title: t("StockSummaryReport.totalAmountDelivery"), field: "totalAmountDelivery", width: "150" },
      { title: t("StockSummaryReport.stockInTrack"), field: "totalAmountDelivery", width: "150" },
      
    ];

    return (
      <div className="m-sm-30">
        <Helmet>
          <title> {t('StockSummaryReport.title')} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          {/* <Breadcrumb routeSegments={[{ name: t('StockSummaryReport.title') }]} /> */}
          <Breadcrumb routeSegments={[
            { name: t("StockSummaryReport.title") },
            { name: TitlePage }]} />
        </div>
        <Grid container spacing={3} justifyContent="space-between">
          <Grid item md={3} xs={12}>
            {/* <AsynchronousAutocomplete
              label={t('general.filterStore')}
              searchFunction={storeSearchByPage}
              multiple={true}
              searchObject={searchObject}
              defaultValue={store}
              displayLable={'name'}
              value={store}
              onSelect={this.selectStore}
            /> */}
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              title={t('general.list')}
              data={itemList}
              columns={columns}
              //parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
              parentChildData={(row, rows) => {
                var list = rows.find(a => a.id === row.parentId);
                return list;
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              options={{
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }), 
                maxBodyHeight: '350px',
                minBodyHeight: '350px',
                headerStyle: {
                  backgroundColor: '#358600',
                  color:'#fff',
                },
                padding: 'dense',
                toolbar: false
              }}
              components={{
                Toolbar: props => (
                  <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
                // this.setState({selectedItems:rows});
              }}
            />
            <TablePagination
              align="left"
              className="px-16"
              rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
              component="div"
              count={totalElements}
              rowsPerPage={rowsPerPage}
              labelRowsPerPage={t('general.rows_per_page')}
              labelDisplayedRows={ ({ from, to, count }) => `${from}-${to} ${t('general.of')} ${count !== -1 ? count : `more than ${to}`}`}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default StockSummaryReportTable;
