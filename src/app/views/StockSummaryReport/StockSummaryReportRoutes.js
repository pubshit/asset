import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const StockSummaryReportTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./StockSummaryReportTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(StockSummaryReportTable);

const StockSummaryReportRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "report/stock_summary_report",
    exact: true,
    component: ViewComponent
  }
];

export default StockSummaryReportRoutes;