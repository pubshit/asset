import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  IconButton,
  Icon,
  DialogActions,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import MaterialTable, { MTableToolbar} from 'material-table';
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useTranslation} from 'react-i18next';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import SelectStorePopup from "../Component/Store/SelectStorePopup";
import SelectMultiProductPopup from "../Component/Product/SelectMultiProductPopup"
import DateFnsUtils from "@date-io/date-fns";
import SelectDepartmentPopup from "../Component/Department/SelectDepartmentPopup";
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import SelectPersonPopup from '../Component/Person/SelectPersonPopup'
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import ConstantList from "../../appConfig";
import NotificationPopup from '../Component/NotificationPopup/NotificationPopup';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11
  }
}))(Tooltip);
function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        props.onChange({
          target: {
            name: props.name,
            value: values.value,

          },
        });
      }}
      name={props.name}
      value={props.value}
      thousandSeparator
      isNumericString
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

class StockSummaryReportDialog extends Component {
  state = {
    rowsPerPage: 1,
    page: 0,
    handoverPerson: null,
    title: null,
    receiverDepartment: null,
    receiverPerson: null,
    issueDate: new Date(),
    shouldOpenStorePopup: false,
    shouldOpenPersonPopup: false,
    shouldOpenProductPopup: false,
    shouldOpenHandoverStorePopup: false,
    shouldOpenreceiverPersonPopup: false,
    shouldOpenNotificationPopup: false,
    shouldOpenDepartmentPopup: false,
    textNotificationPopup: '',
    receiverPerson: null,
    stockReceiptDeliveryStore: null,
    voucherCode: "",
    voucherName: "",
    voucherDetails: [],
    totalElements: 0,
    product: [],
    amount: 0,
    type: ConstantList.VOUCHER_TYPE.StockOut,
  };
  voucherType = ConstantList.VOUCHER_TYPE.StockOut; //Xuất kho

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = () => {
    let { id, voucherDetails } = this.state;
    let checkQuantityIsEqual0 = false;  //kiểm tra số lượng sản phẩm xuất ra đều phải lớn hơn 0
    //kiểm tra trong phiếu phải xuất ít nhất 1 sản phẩm
    if (voucherDetails && voucherDetails.length > 0) {
      voucherDetails.forEach(item => {
        if (!item.quantity || item.quantity === 0) {
          checkQuantityIsEqual0 = true;
          let text = this.props.t('StockSummaryReport.check_quantity_submit_1') + " '" + item.product.name + "' (" + item.product.code + ") " + this.props.t('StockSummaryReport.check_quantity_submit_2');
          this.setState({
            shouldOpenNotificationPopup: true,
            textNotificationPopup: text
          }, function () {
            return;
          });
        }
      });
      if (!checkQuantityIsEqual0) {
      }
    } else {
      let text = this.props.t('StockSummaryReport.check_product_in_voucher');
      this.setState({
        shouldOpenNotificationPopup: true,
        textNotificationPopup: text
      }, function () {
        return;
      });
    }
  };

  handleDateChange = (date, name) => {
    this.setState({
      [name]: date
    });
  };

  handleProductPopupClose = () => {
    this.setState({
      shouldOpenProductPopup: false
    });
  };

  handleSelectMultiProduct = (items) => {
    let { voucherDetails } = this.state;
    if (items != null && items.length > 0) {
      let listProductId = voucherDetails.map(el => el.product.id);

      items.forEach(item => {
        if (listProductId.indexOf(item.id) < 0) {
          let vc = {};
          vc.sku = item.sku;
          if (item.skuType === 1) {
            vc.remainingQuantity = item.remainingQuantity;
          } else if (item.skuType === 2) {
            vc.remainingVolume = item.remainingVolume;
          }
          vc.product = item;
          vc.price = item.price;
          vc.maxQuantity = item.remainingQuantity;
          vc.quantity = 0;
          vc.amount = 0;
          voucherDetails.push(vc);
        }
      });

      if (!voucherDetails) {
        voucherDetails = [];
      }

      this.setState({ voucherDetails }, function () {
        this.handleProductPopupClose();
      });
    }

  }
  handleSelectPerson = (item) => {
    this.setState({ receiverPerson: { id: item.id, displayName: item.displayName } }, function () {
      this.handlePersonPopupClose();
    })
  }

  handlePersonPopupClose = () => {
    this.setState({
      shouldOpenPersonPopup: false,
    })
  }

  handleSelectreceiverPerson = (item) => {
    this.setState({ receiverPerson: { id: item.id, displayName: item.displayName } }, function () {
      this.handlereceiverPersonPopupClose();
    });
  }

  handlereceiverPersonPopupClose = () => {
    this.setState({
      shouldOpenreceiverPersonPopup: false,
    })
  }
  componentWillMount() {
    let { item } = this.props;
    this.setState(item, function () {
    });
  }
  componentDidMount() {
  }
  openPopupSelectProduct = () => {
  }

  handleStorePopupClose = () => {
    this.setState({
      shouldOpenStorePopup: false
    });
  };

  handleSelectStore = (item) => {
    this.setState({ stockReceiptDeliveryStore: { id: item.id, name: item.name } }, function () {
      this.handleStorePopupClose();
    });
  }

  handleQuantityChange = (rowData, event) => {
    let quantity = 0;
    if (event.target.value) {
      quantity = event.target.value;
    }
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach(element => {
        if (element != null && rowData != null && element.product.id == rowData.product.id) {
          if (!element.remainingQuantity || element.remainingQuantity <= 0) {
            quantity = 0;
            this.setState({ shouldOpenNotificationPopup: true, textNotificationPopup: this.props.t('StockSummaryReport.remaining_quantity_cannot_be_less_than_0') }, function () {
            });
          } else if (quantity < 0) {
            quantity = 0;
            this.setState({ shouldOpenNotificationPopup: true, textNotificationPopup: this.props.t('StockSummaryReport.quantity_cannot_be_less_than_0') }, function () {
            });
          } else if (quantity > element.remainingQuantity) {
            quantity = element.remainingQuantity;
            this.setState({ shouldOpenNotificationPopup: true, textNotificationPopup: this.props.t('StockSummaryReport.quantity_cannot_be_greater_than_remaining_quantity') }, function () {
            });
          }
          element.quantity = quantity;
          if (element.price != null && element.price > 0) {
            element.amount = quantity / 1 * element.price;
          }
        }
      });
      this.setState({ voucherDetails });
    }
  };

  handleConfirmationResponse = () => {
    this.setState({ shouldOpenNotificationPopup: false, textNotificationPopup: '' }, function () {
    });
  }

  selectSku = (item, rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      voucherDetails.forEach(vd => {
        if (vd.product.id == rowData.product.id) {
          vd.sku = item;
          vd.skuType = 1;
        }
      });
      this.setState({ voucherDetails });
    }
  }

  handleRowDeleteProduct = (rowData) => {
    let { voucherDetails } = this.state;
    if (voucherDetails != null && voucherDetails.length > 0) {
      for (let index = 0; index < voucherDetails.length; index++) {
        if (voucherDetails && voucherDetails[index].product.id == rowData.product.id) {
          voucherDetails.splice(index, 1);
          break;
        }
      }
    }
    this.setState({ voucherDetails }, function () {
    });
  };

  selectHandoverPerson = (item) => {
    this.setState({ handoverPerson: item }, function () {
    });
  }

  handleReceiverDepartmentPopupClose = () => {
    this.setState({
      shouldOpenDepartmentPopup: false
    });
  };

  handleSelectReceiverDepartment = (item) => {
    this.setState({ receiverDepartment: { id: item.id, name: item.text }, receiverPerson: null }, function () {
      this.handleReceiverDepartmentPopupClose();
    });
  }

  render() {
    let { open, t, i18n } = this.props;
    let {
      type,
      id,
      receiverDepartment,
      shouldOpenDepartmentPopup,
      issueDate,
      rowsPerPage,
      page,
      shouldOpenStorePopup,
      shouldOpenProductPopup,
      shouldOpenPersonPopup,
      voucherCode,
      product,
      receiverPerson,
      stockReceiptDeliveryStore,
      voucherDetails,
      shouldOpenNotificationPopup,
      textNotificationPopup
    } = this.state;

    let columns = [
      {
        title: t("StockSummaryReport.stt"), field: "", width: "50px", align: "left",
        render: rowData => ((page) * rowsPerPage) + (rowData.tableData.id + 1)
      },
      { title: t("Product.code"), field: "product.code", width: "150px" },
      { title: t("Product.name"), field: "product.name", align: "left", width: "150" },
      {
        title: t("Product.stockKeepingUnit"),
        width: "150",
        field: "sku.name"
      },
      {
        title: t("StockSummaryReport.remainingQuantity"), field: "remainingQuantity", align: "left", width: "150",
      },
      {
        title: t("Product.price"), field: "price", align: "left", width: "150",
        render: (rowData) => {
          let number = new Number(rowData.price);
          if (number != null) {
            let plainNumber = number.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            return plainNumber;
          }
        }
      },
      {
        title: t("Product.quantity"), field: "product.quantity", align: "left", width: "150",
        render: rowData =>
          <TextValidator
            className="w-10"
            onChange={event => this.handleQuantityChange(rowData, event)}
            name="quantity"
            id="formatted-numberformat-originalCost"
            value={rowData.quantity}
            id="formatted-numberformat-carryingAmount"
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
      },
      {
        title: t("Product.amount"),
        field: "amount",
        align: "left",
        width: "250",
        render: (rowData) => {
          let number = new Number(rowData.amount);
          if (number != null) {
            let plainNumber = number.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            return plainNumber;
          }
        },
      },
      {
        title: t("general.action"),
        field: "valueText",
        width: "150px",
        render: rowData =>
          <LightTooltip title={t('general.delete')} placement="top" enterDelay={300} leaveDelay={200}>
            <IconButton onClick={() => this.handleRowDeleteProduct(rowData)}>
              <Icon color="error">delete</Icon>
            </IconButton>
          </LightTooltip>
      },
    ];
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          <h4 className="mb-20">{t('general.saveUpdate')}</h4>
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="mb-16" container spacing={2}>
              <Grid item md={4} sm={12} xs={12} className="mt-36">
                <label>
                  <span style={{ fontWeight: 'bold', textAlign: 'center' }}>
                    {t("StockSummaryReport.code")}:
                    </span> {voucherCode}
                </label>
              </Grid>
              <Grid item md={2} sm={12} xs={12} className="mt-16">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    fullWidth
                    margin="none"
                    id="mui-pickers-date"
                    label={
                      <span>
                        <span style={{ color: 'red' }}> * </span>
                        {t("StockSummaryReport.issueDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={false}
                    format="dd/MM/yyyy"
                    name={'issueDate'}
                    value={issueDate}
                    onChange={date => this.handleDateChange(date, "issueDate")}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={6} sm={6} xs={12} className="mt-16">
                <TextValidator
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span>
                      <span style={{ color: 'red' }}> * </span>
                      {t("StockSummaryReport.store")}
                    </span>
                  }
                  className="w-80  mr-16"
                  value={stockReceiptDeliveryStore != null ? stockReceiptDeliveryStore.name : ''}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
                <Button
                  className=" mt-10"
                  variant="contained"
                  color="primary"
                  onClick={() => this.setState({ shouldOpenStorePopup: true, item: {} })}
                >
                  {t('general.select')}
                </Button>
                {shouldOpenStorePopup && (
                  <SelectStorePopup
                    open={shouldOpenStorePopup}
                    handleSelect={this.handleSelectStore}
                    selectedItem={stockReceiptDeliveryStore != null ? stockReceiptDeliveryStore : {}}
                    handleClose={this.handleStorePopupClose} t={t} i18n={i18n} />
                )}
              </Grid>
              <Grid className="mb-16" container spacing={2}>
                <Grid item md={6} sm={12} xs={12} className="mt-16">
                  <TextValidator
                    style={{ width: '85%' }}
                    InputProps={{
                      readOnly: true,
                    }}
                    label={<span><span className="colorRed">*</span>{t('StockSummaryReport.receiverDepartment')}</span>}
                    // label={t("allocation_asset.receiverDepartment")}
                    className="w-80  mr-16"
                    value={receiverDepartment != null ? receiverDepartment.name : ''}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />
                  <Button
                    style={{ float: 'right' }}
                    className=" mt-10"
                    variant="contained"
                    color="primary"
                    onClick={() => this.setState({ shouldOpenDepartmentPopup: true, item: {} })}
                  >
                    {t('general.select')}
                  </Button>
                  {shouldOpenDepartmentPopup && (
                    <SelectDepartmentPopup
                      open={shouldOpenDepartmentPopup}
                      handleSelect={this.handleSelectReceiverDepartment}
                      selectedItem={receiverDepartment != null ? receiverDepartment : {}}
                      handleClose={this.handleReceiverDepartmentPopupClose} t={t} i18n={i18n}
                      checkPermissionUserDepartment={false} />
                  )}
                </Grid>
                <Grid className="mt-16" item md={6} sm={12} xs={12}>
                  <TextValidator
                    InputProps={{
                      readOnly: true,
                    }}
                    label={<span><span className="colorRed">*</span>{t('StockSummaryReport.receiverPerson')}</span>}
                    className="w-80  mr-16"
                    value={receiverPerson != null ? receiverPerson.displayName : ''}
                    validators={["required"]}
                    errorMessages={[t('general.required')]}
                  />
                  <Button
                    className=" mt-10"
                    variant="contained"
                    color="primary"
                    onClick={() => this.setState({ shouldOpenPersonPopup: true, item: {} })}
                  >
                    {t('general.select')}
                  </Button>
                  {shouldOpenPersonPopup && (
                    <SelectPersonPopup
                      open={shouldOpenPersonPopup}
                      handleSelect={this.handleSelectPerson}
                      selectedItem={receiverPerson != null ? receiverPerson : {}}
                      receiverDepartmentId={receiverDepartment ? receiverDepartment.id : null}
                      handleClose={this.handlePersonPopupClose} t={t} i18n={i18n} />
                  )}
                </Grid>

              </Grid>
              <Grid className="mb-16" container spacing={2}>
                <Grid item md={6} sm={6} xs={12} className="mt-16">
                  <Button
                    className=" mt-10"
                    variant="contained"
                    color="primary"
                    onClick={() => this.setState({ shouldOpenProductPopup: true, item: {} })}
                  >
                    {t('component.product.title')}
                  </Button>
                  {shouldOpenProductPopup && (
                    <SelectMultiProductPopup
                      open={shouldOpenProductPopup}
                      handleSelect={this.handleSelectMultiProduct}
                      product={product != null ? product : []}
                      handleClose={this.handleProductPopupClose} t={t} i18n={i18n}
                      voucherType={ConstantList.VOUCHER_TYPE.StockIn}
                      voucherId={id ? id : null}
                      selectedItem={{ product: null }}
                    />
                  )}
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <MaterialTable
                    data={voucherDetails}
                    columns={columns}
                    options={{
                      toolbar: false,
                      selection: false,
                      actionsColumnIndex: -1,
                      paging: true,
                      search: false
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                      }
                    }}
                    components={{
                      Toolbar: props => (
                        <div style={{ witdth: "100%" }}>
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                    onSelectionChange={(rows) => {
                      this.data = rows;
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions style={{ position: 'sticky', bottom: 0, right: 0, zIndex: 9999 }}>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
        {shouldOpenNotificationPopup && (
          <NotificationPopup
            title={t('general.noti')}
            open={shouldOpenNotificationPopup}
            onYesClick={this.handleConfirmationResponse}
            text={textNotificationPopup}
            agree={t('general.agree')}
          />
        )}
      </Dialog>
    );
  }
}

export default StockSummaryReportDialog;
