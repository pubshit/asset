import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Paper,
  Radio,
  RadioGroup,
  TablePagination,
} from "@material-ui/core";

import React from "react";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";

import { convertNumberPrice, removeCommas } from "app/appFunction";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import moment from "moment";
import { appConst } from "app/appConst";
import { searchByTextAndOrg } from "../Supplier/SupplierService";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { debounce } from "lodash";
import {
  createVerificationCalibration,
  updateVerificationCalibration,
} from "./VerificationCalibrationService";
import AppContext from "app/appContext";
import MaterialTable from "material-table";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class AccreditationDialog extends React.Component {
  state = {
    dataAccreditation: {
      kdThoihan: moment().format("YYYY-MM-DDTHH:mm:ss"),
      kdDate: moment().format("YYYY-MM-DDTHH:mm:ss"),
    },
    setShowSelectAssetPopup: false,
    keywordSearch: "",
    listDonVi: [],
    itemDonVi: null,
    listVouchers: []
  };

  query = {
    pageSize: 20,
    pageIndex: 0,
    keyword: this.state.keywordSearch,
    typeCode: "NCC-KD",
  };

  filterAutocomplete = createFilterOptions();

  componentWillMount() {
    let { item } = this.props;
    if (item.id) {
      let itemDonVi = {
        id: item.kdDonvithuchienId,
        name: item?.kdDonvithuchienText,
        text: item?.kdDonvithuchienText,
      };
      this.setState({
        dataAccreditation: item,
        itemDonVi: itemDonVi,
      });
    }
  }

  componentDidMount() {
    this.getListDonVi();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.keywordSearch !== this.state.keywordSearch) {
      this.getListDonVi();
    }
  }

  getListDonVi = async () => {
    let query = {
      pageSize: 20,
      pageIndex: 0,
      keyword: this.state.keywordSearch,
      typeCodes: ["NCC-KD"],
    };
    try {
      let res = await searchByTextAndOrg(query);
      if (res?.data?.content && res?.status === 200) {
        this.setState({
          listDonVi: res?.data?.content,
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  handleFormSubmit = async () => {
    let { t } = this.props;
    let { dataAccreditation } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      if (dataAccreditation?.id) {
        let res = await updateVerificationCalibration(
          this.state.dataAccreditation
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          toast.success(t("VerificationCalibration.noti_update_success"));
          this.props.handleClose();
        } else {
          toast.warning(res?.data?.message);
        }
      } else {
        let res = await createVerificationCalibration(
          this.state.dataAccreditation
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          toast.success(t("VerificationCalibration.noti_create_success"));
          this.props.handleClose();
        } else {
          toast.warning(res?.data?.message);
        }
      }
    } catch (error) {
      setPageLoading(false);
      toast.error(t("toastr.error"));
    }
  };

  handleSelectAsset = async (item = []) => {
    this.setState({
      listVouchers: item,
      setShowSelectAssetPopup: false,
    });
  };
  handleCloseSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: false,
    });
  };
  handleOpenSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: true,
    });
  };
  onChangeInput = (event) => {
    let { name, value } = event.target;
    event.persist();
    if (!value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: "",
        },
      });
      return;
    }
    if (
      (name === "kdChiphi" ||
        name === "kdHinhthuc" ||
        name === "kdKetqua" ||
        name === "kdTrangthai") &&
      !isNaN(+removeCommas(value))
    ) {
      value = removeCommas(value);
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: +value,
        },
      });
    } else if (name !== "kdChiphi") {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: value,
        },
      });
    }
  };

  handleChangeDate = (date, name) => {
    let dataAccreditationUpdate = {
      ...this.state.dataAccreditation,
    };
    if (name === "kdThoihan") {
      if (moment(date).isBefore(moment())) {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.HET_HIEU_LUC.code;
      } else {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.CON_HIEU_LUC.code;
      }
    }
    this.setState({
      dataAccreditation: {
        ...dataAccreditationUpdate,
        [name]: moment(date).format("YYYY-MM-DDTHH:mm:ss"),
      },
    });
  };

  handleSelectDonVi = (value, name) => {
    if (value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: {
          //   id: value?.id,
          //   name: value?.name,
          //   code: value?.code,
          // },
          kdDonvithuchienId: value?.id,
          kdDonvithuchienText: value?.name,
        },
        itemDonVi: value,
      });
    } else {
      this.setState({
        keywordSearch: "",
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: null,
          kdDonvithuchienId: null,
          kdDonvithuchienText: null,
        },
        itemDonVi: null,
      });
    }
  };

  debouncedSetSearchTerm = debounce((newTerm) => {
    this.setState({
      keywordSearch: newTerm,
    });
  }, 300);

  handleSearchDonVi = (e) => {
    this.debouncedSetSearchTerm(e.target.value);
  };

  render() {
    let { open, handleClose, t, isView } = this.props;
    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 100,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        sorting: false,
      },
      {
        title: t("Asset.code"),
        field: "asset.code",
        minWidth: 120,
        align: "right",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        sorting: false,
      },
      {
        title: t("Asset.name"),
        field: "asset.name",
        align: "left",
        minWidth: 200,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        sorting: false,
      },
      {
        title: t("Asset.managementCode"),
        field: "asset.managementCode",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        sorting: false,
      },
      {
        title: t("Asset.serialNumber"),
        field: "asset.serialNumber",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        sorting: false,
      },
      {
        title: t("Asset.model"),
        field: "asset.model",
        align: "left",
        minWidth: 120,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        sorting: false,
      },
      {
        title: t("Asset.originalCost"),
        field: "asset.originalCost",
        align: "left",
        minWidth: 130,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData?.asset?.originalCost),
        sorting: false,
      },
      {
        title: t("Asset.carryingAmount"),
        field: "asset.carryingAmount",
        align: "left",
        minWidth: 170,
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "right",
        },
        render: (rowData) => convertNumberPrice(rowData?.asset?.carryingAmount),
        sorting: false,
      },
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
        className="accreditationDialog"
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4>{t("Verification.saveUpdate")}</h4>
        </DialogTitle>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          style={{
            paddingBottom: "5px",
            paddingTop: "5px",
            overflow: "auto",
            marginBottom: "50px",
          }}
        >
          <DialogContent style={{ overflow: "unset" }}>
            <Grid
              item
              container
              spacing={2}
              lg={12}
              md={12}
              sm={12}
              xs={12}
              className="mt-10"
            >
              <Grid item lg={2} md={2} sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <CustomValidatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog"
                    style={{ marginTop: "2px" }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Verification.createDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={this.state.dataAccreditation?.kdDate || new Date()}
                    onChange={(date) => this.handleChangeDate(date, "kdDate")}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    clearable
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                    disabled={isView}
                    maxDate={new Date()}
                    maxDateMessage={t("allocation_asset.maxDateMessage")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item lg={2} md={2} sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <CustomValidatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog"
                    style={{ marginTop: "2px" }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Verification.approvalDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={this.state.dataAccreditation?.kdDate || new Date()}
                    onChange={(date) => this.handleChangeDate(date, "kdDate")}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    clearable
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                    disabled={isView}
                    maxDate={new Date()}
                    maxDateMessage={t("allocation_asset.maxDateMessage")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsTen || ""}
                  name="tsTen"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">
                      <span style={{ color: "red" }}> * </span>
                      {t("Verification.name")}
                    </span>
                  }
                  validators={["required"]}
                  errorMessages={t("general.required")}
                  onChange={() => { }}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsMa || ""}
                  name="tsMa"
                  size="small"
                  label={
                    <span className="font">{t("Verification.year")}</span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  onChange={() => { }}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <Autocomplete
                  id="combo-box"
                  fullWidth
                  size="small"
                  name="donVi"
                  value={this.state.itemDonVi}
                  disabled={isView}
                  options={[...this.state.listDonVi]}
                  onChange={(e, value) =>
                    this.handleSelectDonVi(value, "donVi")
                  }
                  getOptionLabel={(option) => {
                    return option?.name
                      ? option?.name
                      : this.state.keywordSearch || "";
                  }}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    return this.filterAutocomplete(options, params);
                  }}
                  noOptionsText={t("general.noOption")}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      variant="standard"
                      onChange={this.handleSearchDonVi}
                      onBlur={() => {
                        this.debouncedSetSearchTerm("");
                      }}
                      disabled={isView}
                      label={
                        <span className="font">
                          <span style={{ color: "red" }}> * </span>{t("Verification.performingDepartment")}
                        </span>
                      }
                      value={
                        this.state.keywordSearch !== "" && this.state.itemDonVi
                          ? this.state.keywordSearch
                          : this.state.itemDonVi
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  )}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsModel || ""}
                  name="tsModel"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font"><span style={{ color: "red" }}> * </span>{t("Verification.status")}</span>
                  }
                  managementCode
                  onChange={() => { }}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsSerialNo || ""}
                  name="tsSerialNo"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font"><span style={{ color: "red" }}> * </span>{t("Verification.note")}</span>
                  }
                  onChange={() => { }}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <Autocomplete
                  id="combo-box"
                  fullWidth
                  size="small"
                  name="donVi"
                  value={this.state.itemDonVi}
                  disabled={isView}
                  options={[...this.state.listDonVi]}
                  onChange={(e, value) =>
                    this.handleSelectDonVi(value, "donVi")
                  }
                  getOptionLabel={(option) => {
                    return option?.name
                      ? option?.name
                      : this.state.keywordSearch || "";
                  }}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    return this.filterAutocomplete(options, params);
                  }}
                  noOptionsText={t("general.noOption")}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      variant="standard"
                      onChange={this.handleSearchDonVi}
                      onBlur={() => {
                        this.debouncedSetSearchTerm("");
                      }}
                      disabled={isView}
                      label={
                        <span className="font">
                          <span style={{ color: "red" }}> * </span>{t("Verification.supplier")}
                        </span>
                      }
                      value={
                        this.state.keywordSearch !== "" && this.state.itemDonVi
                          ? this.state.keywordSearch
                          : this.state.itemDonVi
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  )}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsSerialNo || ""}
                  name="tsSerialNo"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font"><span style={{ color: "red" }}> * </span>{t("Verification.implementationCosts")}</span>
                  }
                  onChange={() => { }}
                />
              </Grid> <Grid item lg={4} md={4} sm={6} xs={12}>
                <Autocomplete
                  id="combo-box"
                  fullWidth
                  size="small"
                  name="donVi"
                  value={this.state.itemDonVi}
                  disabled={isView}
                  options={[...this.state.listDonVi]}
                  onChange={(e, value) =>
                    this.handleSelectDonVi(value, "donVi")
                  }
                  getOptionLabel={(option) => {
                    return option?.name
                      ? option?.name
                      : this.state.keywordSearch || "";
                  }}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    return this.filterAutocomplete(options, params);
                  }}
                  noOptionsText={t("general.noOption")}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      variant="standard"
                      onChange={this.handleSearchDonVi}
                      onBlur={() => {
                        this.debouncedSetSearchTerm("");
                      }}
                      disabled={isView}
                      label={
                        <span className="font">
                          <span style={{ color: "red" }}> * </span>{t("Verification.performingUnit")}
                        </span>
                      }
                      value={
                        this.state.keywordSearch !== "" && this.state.itemDonVi
                          ? this.state.keywordSearch
                          : this.state.itemDonVi
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  )}
                />
              </Grid>
              {!isView && (
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.handleOpenSelectAssetPopup()}
                    className="mb-10"
                  >
                    {t("general.select_asset")}
                  </Button>
                </Grid>
              )}
            </Grid>
            <Grid item xs={12}>
              <MaterialTable
                data={this.state.listVouchers}
                columns={columns}
                localization={{
                  body: {
                    emptyDataSourceMessage: `${t(
                      "general.emptyDataMessageTable"
                    )}`,
                  },
                }}
                onRowClick={(event, rowData) => console.log(rowData)}
                options={{
                  draggable: false,
                  toolbar: false,
                  selection: false,
                  actionsColumnIndex: -1,
                  sorting: false,
                  paging: false,
                  search: false,
                  padding: "dense",
                  rowStyle: (rowData) => ({
                    backgroundColor:
                      rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                  }),
                  headerStyle: {
                    backgroundColor: "#358600",
                    color: "#fff",
                    paddingRight: 10,
                    paddingLeft: 10,
                    textAlign: "center",
                  },
                  maxBodyHeight: 280,
                  minBodyHeight: 280,
                }}
                components={{
                  Toolbar: (props) => (
                    <div className="w-100">
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
                onSelectionChange={(rows) => {
                  this.data = rows;
                }}
              />
            </Grid>
          </DialogContent>
          <DialogActions
            style={{
              position: "absolute",
              bottom: "-8px",
              right: 0,
              width: "100%",
              backgroundColor: "#fff",
              zIndex: 100,
            }}
          >
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            {!isView && (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            )}
          </DialogActions>
        </ValidatorForm>
        {this.state.setShowSelectAssetPopup && (
          <SelectAssetAllPopup
            t={t}
            open={this.state.setShowSelectAssetPopup}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleCloseSelectAssetPopup}
            isGetAll={true}
          />
        )}
      </Dialog>
    );
  }
}

AccreditationDialog.contextType = AppContext;
export default AccreditationDialog;
