import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/ai-kiem-dinh";

export const createVerificationCalibration = (data) => {
  return axios.post(API_PATH, data);
};
export const updateVerificationCalibration = (data) => {
  return axios.put(API_PATH, data);
};

export const searchByPage = (searchObject) => {
  let url = API_PATH + `/search-by-page`;
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
    },
  };
  return axios.get(url, config);
};

export const getItemById = (id) => {
  let url = API_PATH + "/" + id;
  return axios.get(url);
};
