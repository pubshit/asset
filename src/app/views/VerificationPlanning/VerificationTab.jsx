import { AppBar, Box, Grid, Tab, Tabs, Typography } from "@material-ui/core";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage } from "../Asset/AssetService";
import ComponentTableTab from "./ComponentTableTab";
import { TabPanel } from "app/appFunction";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});

function VerificationTab(props) {
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const [tabValue, setTabValue] = useState(0);
  const [searchObject, setSearchObject] = useState({
    page: 1,
    rowsPerPage: 10,
    totalElements: 0,
    keyword: "",
  });
  const handleChangePage = (event, newPage) => {
    setSearchObject({ ...searchObject, page: newPage });
  };
  const [itemList, setItemList] = useState([]);
  const updatePageData = async () => {
    setPageLoading(true);
    let dataSearch = {};
    dataSearch.keyword = searchObject.keyword.trim() || null;
    dataSearch.pageIndex = searchObject.page + 1;
    dataSearch.pageSize = searchObject.rowsPerPage;
    try {
      const result = searchByPage(dataSearch);
      if (result?.status === appConst.CODE.SUCCESS) {
        setItemList([...result?.data?.data?.content]);
        setSearchObject({
          ...searchObject,
          totalElements: result?.data?.data?.totalElements,
        });
      }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };
  const handleChangeTabValue = (event, newValue) => {
    setSearchObject({
      ...searchObject,
      page: 0,
    });

    if (appConst.tabVerificationPlanning.tabAll === newValue) {
      setSearchObject({
        ...searchObject,
        keyword: "",
      });
      setItemList([]);
      setTabValue(newValue);
      updatePageData();
    }
    if (appConst.tabVerificationPlanning.tabNew === newValue) {
      setSearchObject({
        ...searchObject,
        keyword: "",
      });
      setItemList([]);
      setTabValue(newValue);
      updatePageData();
    }
    if (appConst.tabVerificationPlanning.tabApprovedList === newValue) {
      setSearchObject({
        ...searchObject,
        keyword: "",
      });
      setItemList([]);
      setTabValue(newValue);
      updatePageData();
    }
    if (appConst.tabVerificationPlanning.tabApprovedCost === newValue) {
      setSearchObject({
        ...searchObject,
        keyword: "",
      });
      setItemList([]);
      setTabValue(newValue);
      updatePageData();
    }
    if (appConst.tabVerificationPlanning.tabApprovedEnd === newValue) {
      setSearchObject({
        ...searchObject,
        keyword: "",
      });
      setItemList([]);
      setTabValue(newValue);
      updatePageData();
    }
  };

  return (
    <div>
      <Grid container spacing={2} className="mt-12">
        <Grid item xs={12}>
          <AppBar position="static" color="default">
            <Tabs
              value={tabValue}
              onChange={(e, value) => handleChangeTabValue(e, value)}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              [
              <Tab
                key={appConst.tabVerificationPlanning.tabAll}
                label={t("Verification.tab.all")}
              />
              ,
              <Tab
                key={appConst.tabVerificationPlanning.tabNew}
                label={t("Verification.tab.new")}
              />
              ,
              <Tab
                key={appConst.tabVerificationPlanning.tabApprovedList}
                label={t("Verification.tab.approved")}
              />
              <Tab
                key={appConst.tabVerificationPlanning.tabApprovedCost}
                label={t("Verification.tab.quoteApproved")}
              />
              <Tab
                key={appConst.tabVerificationPlanning.tabApprovedEnd}
                label={t("Verification.tab.finished")}
              />
              , ]
            </Tabs>
          </AppBar>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerificationPlanning.tabAll}
            className="mp-0"
          >
            <ComponentTableTab />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerificationPlanning.tabNew}
            className="mp-0"
          >
            <ComponentTableTab />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerificationPlanning.tabApprovedList}
            className="mp-0"
          >
            <ComponentTableTab />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerificationPlanning.tabApprovedCost}
            className="mp-0"
          >
            <ComponentTableTab />
          </TabPanel>
          <TabPanel
            value={tabValue}
            index={appConst.tabVerificationPlanning.tabApprovedEnd}
            className="mp-0"
          >
            <ComponentTableTab />
          </TabPanel>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
    </div>
  );
}

export default VerificationTab;
