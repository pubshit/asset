import React from "react";
import AppContext from "app/appContext";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import {
  Box,
  Typography,
} from "@material-ui/core";
import VerificationTab from "./VerificationTab";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

class VerificationCalibrationPlanningTable extends React.Component {
  state = {
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    item: {},
    shouldOpenMainProposal: false,
    tabValue: 0,
    itemList: [],
  };

  handleEditItem = () => {
    this.setState({
      shouldOpenMainProposal: true,
    });
  };

  render() {
    const { t, i18n, breadcrumb } = this.props;
    let {
      rowsPerPage,
      page,
      totalElements,
      itemList,
      shouldOpenMainProposal,
      tabValue,
    } = this.state;
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {t(breadcrumb)} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/page",
              },
              { name: t(breadcrumb) },
            ]}
          />
        </div>
        <VerificationTab />
      </div>
    );
  }
}

VerificationCalibrationPlanningTable.contextType = AppContext;
export default VerificationCalibrationPlanningTable;
