import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const CalibrationTable = EgretLoadable({
  loader: () => import("./VerificationPlanningTable")
});
const ViewComponent = withTranslation()(CalibrationTable);

const VerificationPlanningRouters = [
  {
    path: ConstantList.ROOT_PATH + "asset/planing",
    exact: true,
    component: ViewComponent
  }
];

export default VerificationPlanningRouters;