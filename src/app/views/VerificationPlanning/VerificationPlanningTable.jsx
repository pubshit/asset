import React from "react";
import VerificationCalibrationPlanningTable from "./VerificationCalibrationPlanningTable";

function VerificationPlanningTable(props) {
  return (
    <VerificationCalibrationPlanningTable
      {...props}
      breadcrumb={"Verification.planing"}
    />
  );
}

export default VerificationPlanningTable;
