import React from "react";
import VerificationCalibrationTable from "./VerificationCalibrationTable";

function VerificationTable(props) {
  return ( 
    <VerificationCalibrationTable {...props} breadcrumb={"VerificationCalibration.verification"} />
   );
}

export default VerificationTable;