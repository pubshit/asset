import React, { useContext, useState } from "react";
import { AppBar, Box, Button, FormControl, Grid, Icon, IconButton, Input, InputAdornment, Link, Tab, TablePagination, Tabs, TextField, Typography } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst } from "app/appConst";
import { useTranslation } from "react-i18next";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { LightTooltip, convertNumberPrice } from "app/appFunction";
import AccreditationDialog from "./AccreditationDialog";
import { getItemById, searchByPage } from "./VerificationCalibrationService";
import { useEffect } from "react";
import moment from "moment";
toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
    //etc you get the idea
});
function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <React.Fragment>
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`scrollable-force-tabpanel-${index}`}
                aria-labelledby={`scrollable-force-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        </React.Fragment>
    );
}
function MaterialButton(props) {
    const { t } = useTranslation();
    const item = props.item;
    const hasDeletePermission = props.hasDeletePermission;
    const hasEditPermission = props.hasEditPermission;
    const hasPrintPermission = props.hasPrintPermission;
    const hasSuperAccess = props.hasSuperAccess;
    const value = props.value;
    return (
        <div className="none_wrap">
            {
                // hasEditPermission &&
                // appConst.tabAllocation.tabReturn !== value &&
                // item?.allocationStatusIndex !==
                // appConst.listStatusAllocation[2].indexOrder && 
                (
                    <LightTooltip
                        title={t("general.editIcon")}
                        placement="right-end"
                        enterDelay={300}
                        leaveDelay={200}
                        PopperProps={{
                            popperOptions: {
                                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                            },
                        }}
                    >
                        <IconButton
                            size="small"
                            onClick={() => props.onSelect(item, appConst.active.edit)}
                        >
                            <Icon fontSize="small" color="primary">
                                edit
                            </Icon>
                        </IconButton>
                    </LightTooltip>
                )}
            {(hasDeletePermission || hasSuperAccess) &&
                (hasSuperAccess
                    ? hasSuperAccess
                    : appConst.listStatusAllocation[0].indexOrder ===
                    item?.allocationStatusIndex) && (
                    <LightTooltip
                        title={t("general.deleteIcon")}
                        placement="right-end"
                        enterDelay={300}
                        leaveDelay={200}
                        PopperProps={{
                            popperOptions: {
                                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                            },
                        }}
                    >
                        <IconButton
                            size="small"
                            onClick={() => props.onSelect(item, appConst.active.delete)}
                        >
                            <Icon fontSize="small" color="error">
                                delete
                            </Icon>
                        </IconButton>
                    </LightTooltip>
                )}
            {hasPrintPermission && (
                <LightTooltip
                    title={t("In phiếu")}
                    placement="right-end"
                    enterDelay={300}
                    leaveDelay={200}
                    PopperProps={{
                        popperOptions: {
                            modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
                        },
                    }}
                >
                    <IconButton
                        size="small"
                        onClick={() => props.onSelect(item, appConst.active.print)}
                    >
                        <Icon fontSize="small" color="inherit">
                            print
                        </Icon>
                    </IconButton>
                </LightTooltip>
            )}
            <LightTooltip
                title={t("general.viewIcon")}
                placement="right-end"
                enterDelay={300}
                leaveDelay={200}
            >
                <IconButton
                    size="small"
                    onClick={() => props.onSelect(item, appConst.active.view)}
                >
                    <Icon fontSize="small" color="primary">
                        visibility
                    </Icon>
                </IconButton>
            </LightTooltip>
        </div>
    );
}
function ComponentTableTab(props) {
    const { t } = useTranslation();
    const { setPageLoading } = useContext(AppContext);
    // const [page, setPage] = useState(1)
    // const [totalElements, setTotalElements] = useState(0)
    // const [rowsPerPage, setRowsPerPage] = useState(10)
    // const [keyword, setKeyword] = useState("")
    const [tabValue, setTabValue] = useState(0)
    const [searchObject, setSearchObject] = useState({
        page: 0,
        rowsPerPage: 10,
        totalElements: 0,
        keyword: ""
    })

    const handleChangePage = (event, newPage) => {
        setSearchObject({ ...searchObject, page: newPage });
    };

    useEffect(() => {
        updatePageData()
    }, [searchObject.page, searchObject.rowsPerPage])

    const [itemList, setItemList] = useState([])
    const [item, setItem] = useState({})
    const [isView, setIsView] = useState(false)
    const [shouldOpenAccreditationDialog, setShouldOpenAccreditationDialog] = useState(false)

    const handleOKEditAccreditationClose = () => {
        setShouldOpenAccreditationDialog(false)
    };

    const handleAccreditationDialogClose = () => {
        setShouldOpenAccreditationDialog(false)
        setIsView(false)
        updatePageData()
    };

    const handleCreateAccreditation = () => {
        setShouldOpenAccreditationDialog(true)
    };
    const handleTextChange = (event) => {
        setSearchObject({
            ...searchObject,
            keyword: event.target.value
        })
    };
    const search = async () => {
        try {

            const { data } = await searchByPage(searchObject);
            const listData = data?.content || [];
            const totalElements = data?.totalElements || 0;
            setItemList(listData)
            setSearchObject({
                ...searchObject,
                totalElements: totalElements,
            });

        } catch (error) {
            // Handle the error gracefully (e.g., display an error message)
            console.error("Error occurred while calling the API:", error);
        }
    }
    const handleKeyDownEnterSearch = (e) => {
        if (e.key === "Enter") {
            search();
        }
    };
    const handleKeyUp = (e) => {
        !e.target.value && search();
    };
    const updatePageData = async () => {
        setPageLoading(true);
        let dataSearch = {};
        dataSearch.keyword = searchObject.keyword.trim() || "";
        dataSearch.pageIndex = searchObject.page + 1;
        dataSearch.pageSize = searchObject.rowsPerPage;

        try {
            const result = await searchByPage(dataSearch)
            if (result?.status === appConst.CODE.SUCCESS) {
                setItemList([...result?.data?.data?.content])
                setSearchObject({ ...searchObject, totalElements: result?.data?.data?.totalElements })
            }

        } catch (error) {
            toast.error(t("toastr.error"));
        }
        finally {
            setPageLoading(false);
        };

    };  

    const handleEdit = (item) => {
        getItemById(item.id).then(({ data }) => {
            setItem(data?.data)
            setShouldOpenAccreditationDialog(true)
        }).catch(() => {
            toast.error(t("toastr.error"))
        });
    };

    const handleViews = (item) => {
        getItemById(item.id).then(({ data }) => {
            setItem(data?.data)
            setIsView(true)
            setShouldOpenAccreditationDialog(true)
        }).catch(() => {
            toast.error(t("toastr.error"))
        });
    }

    let columns = [
        {
            title: t("general.action"),
            field: "custom",
            width: "150px",
            minWidth: 150,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => (
                <MaterialButton
                    hasDeletePermission={props?.hasDeletePermission}
                    hasEditPermission={props?.hasEditPermission}
                    hasPrintPermission={props?.hasPrintPermission}
                    hasSuperAccess={props?.hasSuperAccess}
                    item={rowData}
                    value={tabValue}
                    onSelect={(rowData, method) => {
                        if (appConst.active.edit === method) {
                            handleEdit(rowData);
                        } else if (appConst.active.delete === method) {
                            // handleDelete(rowData.id);
                        } else if (appConst.active.print === method) {
                            // handlePrintAll(rowData);
                        } else if (appConst.active.view === method) {
                            handleViews(rowData);
                        } else {
                            alert("Call Selected Here:" + rowData.id);
                        }
                    }}
                />
            ),
        },
        {
            title: t("general.stt"),
            field: "code",
            maxWidth: 50,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => ((searchObject?.page) * searchObject?.rowsPerPage) + (rowData.tableData.id + 1),
        },
        {
            title: t("VerificationCalibration.verificationDate"),
            field: "kdDate",
            minWidth: 120,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => rowData?.kdDate ? moment(rowData.kdDate).format("DD/MM/YYYY") : "",
        },
        {
            title: t("VerificationCalibration.timeLimit"),
            field: "kdThoihan",
            minWidth: 110,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => rowData?.kdThoihan ? moment(rowData.kdThoihan).format("DD/MM/YYYY") : "",
        },
        {
            title: t("Asset.code"),
            field: "tsMa",
            maxWidth: 150,
            minWidth: 120,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("Asset.managementCode"),
            field: "tsMaquanly",
            maxWidth: 150,
            minWidth: 120,
            align: "left",
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
        },
        {
            title: t("Asset.name"),
            field: "tsTen",
            align: "left",
            minWidth: 250,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.formName"),
            field: "kdHinhthucMota",
            align: "left",
            minWidth: 250,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("maintainRequest.columns.costs"),
            field: "kdChiphi",
            align: "left",
            minWidth: 75,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "right",
            },
            render: (rowData) => rowData?.kdChiphi ? convertNumberPrice(rowData?.kdChiphi) : ""
        },
        {
            title: t("Asset.TestingUnit"),
            field: "kdDonvithuchienText",
            align: "left",
            minWidth: 140,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.status"),
            field: "kdTrangthaiMota",
            align: "left",
            minWidth: 90,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.model"),
            field: "tsModel",
            align: "left",
            minWidth: 50,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.yearOfManufacture"),
            field: "tsNamsx",
            align: "left",
            minWidth: 105,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.manufacturer"),
            field: "tsHangsx",
            align: "left",
            minWidth: 110,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
            },
        },
        {
            title: t("Asset.originalCost"),
            field: "originalCost",
            align: "left",
            minWidth: 130,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => convertNumberPrice(rowData?.originalCost)
        },
        {
            title: t("Asset.carryingAmount"),
            field: "carryingAmount",
            align: "left",
            minWidth: 140,
            cellStyle: {
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
            },
            render: (rowData) => convertNumberPrice(rowData?.carryingAmount),
        },
    ];

    return (
        <div>
            <Grid container spacing={2} className="mt-12">
                <Grid item md={6} sm={12} xs={12}>
                    <Button
                        className="mb-16 mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={() => handleCreateAccreditation()}
                    >
                        {t("general.add")}
                    </Button>
                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={handleTextChange}
                            onKeyDown={handleKeyDownEnterSearch}
                            onKeyUp={handleKeyUp}
                            placeholder={t("Verification.filter")}
                            id="search_box"
                            startAdornment={
                                <InputAdornment>
                                    <Link>
                                        <SearchIcon
                                            onClick={() => search()}
                                            className="searchTable"
                                        />
                                    </Link>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <MaterialTable
                        // onRowClick={(e, rowData) => {
                        //     getRowData(rowData)
                        // }}
                        title={t("general.list")}
                        data={itemList}
                        columns={columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor:
                                    rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
                            }),
                            maxBodyHeight: "490px",
                            minBodyHeight: "260px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                paddingLeft: 10,
                                paddingRight: 10,
                                textAlign: "center",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            this.data = rows;
                        }}
                    />
                    <TablePagination
                        align="left"
                        className="px-16"
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        labelRowsPerPage={t("general.rows_per_page")}
                        labelDisplayedRows={({ from, to, count }) =>
                            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                            }`
                        }
                        component="div"
                        count={searchObject.totalElements}
                        rowsPerPage={searchObject.rowsPerPage}
                        page={searchObject.page}
                        backIconButtonProps={{
                            "aria-label": "Previous Page",
                        }}
                        nextIconButtonProps={{
                            "aria-label": "Next Page",
                        }}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={(e) =>
                            setSearchObject({
                                ...searchObject,
                                rowsPerPage: e.target.value,
                            })
                        }
                    />
                </Grid>
                {shouldOpenAccreditationDialog && (
                    <AccreditationDialog
                        t={t}
                        handleClose={() => handleAccreditationDialogClose()}
                        open={shouldOpenAccreditationDialog}
                        handleOKEditClose={handleOKEditAccreditationClose}
                        item={item}
                        isView={isView}
                    />
                )}
            </Grid>
        </div>
    );
}

export default ComponentTableTab;