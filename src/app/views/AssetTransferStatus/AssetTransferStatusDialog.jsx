import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions,
  FormControl,
  DialogTitle,
  MenuItem,
  Select,
  InputLabel,
  DialogContent
} from "@material-ui/core";

import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { checkDuplicateCode, createAssetTransferStatus, updateAssetTransferStatus } from "./AssetTransferStatusService";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Label, PaperComponent } from "../Component/Utilities";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3
});

class AllocationStatusDialog extends Component {
  state = {
    id: "",
    name: "",
    code: "",
    indexOrder: "",
    isActive: false,
  };

  listIndexOder = [
    { id: 0, name: 'Tiến một bước' },
    { id: 1, name: 'Lùi một bước' }
  ]

  handleChange = (event, source) => {
    event.persist();
    if (source === "switch") {
      this.setState({ isActive: event.target.checked });
      return;
    }
    if (source === "indexOrder") {
      const value = Number(event.target.value);

      let newIndexOrder;
      if (value === this.listIndexOder[0].id) {
        newIndexOrder = this.props.item?.indexOrder
          ? this.props.item.indexOrder + 1
          : this.state.indexOrder + 1;
      } else if (value === this.listIndexOder[1].id) {
        newIndexOrder = this.props.item?.indexOrder
          ? this.props.item.indexOrder - 1
          : this.state.indexOrder - 1;
      }

      this.setState({ [event.target.name]: newIndexOrder });
      return;
    }
    this.setState({
      [event.target.name]: event.target.value
    }
    )
  };

  handleFormSubmit = () => {
    let { id, code } = this.state;
    let { t } = this.props;
    
    checkDuplicateCode(id, code).then((result) => {
      //Nếu trả về true là code đã được sử dụng
      if (result.data) {
        toast.warning(this.props.t('AllocationStatus.noti.dupli_code'))
      } else {
        if (id) {
          updateAssetTransferStatus({
            ...this.state
          }).then(() => {
            toast.success('Sửa trạng thái thành công.')
            this.props.handleOKEditClose();
          }).catch((e) => {
            console.error(e);
            toast.error(t("general.error"));
          });
        } else {
          createAssetTransferStatus({
            ...this.state
          }).then(() => {
            toast.success('Thêm mới trạng thái thành công.')
            this.props.handleOKEditClose();
          }).catch((e) => {
            console.error(e);
            toast.error(t("general.error"));
          });
        }
      }
    });
  };

  componentDidMount = () => {
    let { item } = this.props;
    this.setState(item);
  }

  render() {
    let {
      name,
      code,
      indexOrder,
    } = this.state;
    let { open, t } = this.props;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="sm" fullWidth>
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          {t("TransferStatus.dialog")}
        </DialogTitle>

        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid className="" container spacing={1}>
              <Grid item sm={7} xs={12}>
                <TextValidator
                  fullWidth
                  label={<Label isRequired>{t('AllocationStatus.code')}</Label>}
                  onChange={this.handleChange}
                  type="text"
                  name="code"
                  value={code}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item sm={5} xs={12} >
                <FormControl fullWidth={true} >
                  <InputLabel htmlFor="indexOrderId" >{t("AllocationStatus.order")}</InputLabel>
                  <Select
                    onChange={indexOrder => this.handleChange(indexOrder, "indexOrder")}
                    inputProps={{
                      name: "indexOrder",
                      id: "indexOrderId"
                    }}
                  >
                    {this.listIndexOder.map(item => {
                      return <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} xs={12}>
                <TextValidator
                  fullWidth
                  label={<Label isRequired>{t('AllocationStatus.name')}</Label>}
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  value={name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle mt-12">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                className="mr-15"
                color="primary"
                type="submit"
              >
                {t('general.save')}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

export default AllocationStatusDialog;
