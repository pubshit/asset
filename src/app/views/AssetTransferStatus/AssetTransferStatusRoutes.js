import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetTransferStatusTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./AssetTransferStatusTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(AssetTransferStatusTable);

const AssetTransferStatusRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "list/asset_transfer_status",
    exact: true,
    component: ViewComponent
  }
];

export default AssetTransferStatusRoutes;