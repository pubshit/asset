import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/asset_transfer_status" + ConstantList.URL_PREFIX;

export const updateAssetTransferStatus = assetTransferStatus => {
  return axios.put(API_PATH +"/update/" + assetTransferStatus.id , assetTransferStatus);
};

export const createAssetTransferStatus = assetTransferStatus => {
  return axios.post(API_PATH + "/create", assetTransferStatus);
};

export const getOneById = id => {
  return axios.get(API_PATH + "/getOne/" + id);
};

export const deleteById = id => {
  return axios.delete(API_PATH + "/delete/" + id);
};

export const deleteCheck = id => {
  return axios.delete(API_PATH + "/deleteCheck/" + id);
};

export const searchByPage = (searchObject) => {
  return axios.post(API_PATH + "/searchByPage", searchObject);
};

export const checkDuplicateCode = (id, code) => {
  const config = { params: {id: id, code: code } };
  var url = API_PATH + "/checkDuplicateCode";
  return axios.get(url, config);
};










