import {
  appConst,
  subVoucherType,
  typeWarehouseVoucher,
  TYPE_STORE,
  LIST_ORGANIZATION,
  PRINT_TEMPLATE_MODEL
} from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  convertMoney,
  convertNumberPriceRoundUp,
  convertNumberToWords,
  handleThrowResponseMessage,
  isSuccessfulResponse,
} from "app/appFunction";
import { Breadcrumb } from "egret";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React from "react";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ComponentTransferTable from "./ComponentTransferTable";
import {
  getVouchersPrint,
  searchByPage,
  searchByPageAsset
} from "./WarehouseReceivingService";
import { COLUMNS_SubTable, convertListSubTable } from "../WarehouseDelivery/constants";
import { ValidatorForm } from "react-material-ui-form-validator";
import { Icon, IconButton, Checkbox, } from "@material-ui/core";
import { convertFromToDate, getUserInformation, isValidDate } from "../../appFunction";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import CustomTablePagination from "../CustomTablePagination";
import { inventoryReceivingPrintData } from "../FormCustom/InventoryReceiving";
import { searchByPageAssetPrint } from "../WarehouseDelivery/WarehouseDeliveryService";
import { variable } from "../../appConst";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

class WarehouseTransferTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    totalElements: 0,
    isView: false,
    status: null,
    tabValue: 0,
    isRoleAdmin: false,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    listItemAsset: [],
    selectedList: [],
    pageAssets: 0,
    rowsPerPageAssets: 5,
    totalElementAssets: 0,
    selectedData: {}
  };
  voucherType = typeWarehouseVoucher.warehouseReceivingVoucher; //Điều chuyển 

  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, () => { });
  };

  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };

  setPage = (page) => {
    this.setState({ page }, () => {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, () => {
      this.updatePageData();
    });
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  setPageAsset = (page) => {
    this.setState({ pageAssets: page }, () => {
      this.handleGetDataAsset();
    });
  };

  setRowsPerPageAsset = (event) => {
    this.setState({ rowsPerPageAssets: event.target.value, page: 0 }, () => {
      this.handleGetDataAsset();
    });
  };

  handleChangePageAsset = (event, newPage) => {
    this.setPageAsset(newPage);
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  search = () => {
    this.setPage(0);
  };

  updatePageData = () => {
    let {setPageLoading} = this.context;
    let {selectedList} = this.state;
    let {t} = this.props;
    setPageLoading(true);
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword.trim() || null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.status = appConst.listStatusWarehouseTransfer[2].indexOrder;
    searchObject.assetClass = this.props?.assetClass;
    searchObject.type = TYPE_STORE.NHAP_KHO.code;
    searchObject.fromDate = this.state?.fromDate ? convertFromToDate(this.state?.fromDate).fromDate : null;
    searchObject.toDate = this.state?.toDate ? convertFromToDate(this.state?.toDate).toDate : null;
    searchObject.storeReceiptId = this.state?.store?.id;
    searchByPage(searchObject).then((res) => {
      const {data, code} = res?.data;
      if (isSuccessfulResponse(code)) {
        let countItemChecked = 0;
        let itemList = data.content.map(item => {
          if (selectedList.find(checkedItem => checkedItem === item?.id)) {
            countItemChecked++
          }
          return {
            ...item,
            checked: !!selectedList?.find(checkedItem => checkedItem === item.id),
          }
        })
        this.setState({
          selectedData: null,
          listItemAsset: [],
          itemList,
          totalElements: data?.totalElements,
          checkAll: itemList?.length === countItemChecked && itemList?.length > 0
        });
      } else {
        handleThrowResponseMessage(res);
      }
      setPageLoading(false);
    }).catch(() => {
      toast.error(t("toastr.error"));
      setPageLoading(false);
    });
  };

  componentDidMount() {
    this.setState(
      {
        status: null,
      },
      () => this.updatePageData()
    )
  }

  setStateTransfer = (item) => {
    this.setState(item);
  };

  checkStatus = (status) => {
    let itemStatus = appConst.listStatusWarehouseTransfer.find(
      (item) => item.transferStatusIndex === status
    );
    return itemStatus?.name;
  };

  handleGetRowData = (rowData) => {
    this.setState({ selectedData: rowData, pageAssets: 0 }, () => {
      this.handleGetDataAsset(rowData.id)
    })
  }
  handleGetDataAsset = async (id) => {
    let { setPageLoading } = this.context;
    setPageLoading(true);
    const { pageAssets, rowsPerPageAssets } = this.state
    const searchObject = {}
    searchObject.pageIndex = pageAssets + 1;
    searchObject.pageSize = rowsPerPageAssets;
    try {
      const { data } = await searchByPageAsset(id || this.state.selectedData?.id, searchObject)
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({ listItemAsset: convertListSubTable(data?.data?.content) || [], totalElementAssets: data?.data?.totalElements, })
      } else {
        this.setState({ listItemAsset: [] })
      }
    } catch (e) {
      console.error(e)
    } finally {
      setPageLoading(false);
    }
  }

  convertDataPrint = (value = [], rowData = {}) => {
    return {
      ...rowData,
      handoverPerson: {
        displayName: rowData?.personReceiptName || rowData?.receiverPersonName
      },
      stockReceiptDeliveryStore: {
        name: rowData?.storeReceiptName
      },
      receiverDepartment: {
        name: rowData?.departmentName
      },
      handoverRepresentative: rowData?.handoverPersonName || rowData?.handoverRepresentative,
      issueDate: rowData?.inputDate,
      voucherCode: rowData?.code,
      voucherDetails: value?.map(i => {
        return {
          ...i,
          product: {
            name: i?.assetName || i?.productName,
            manufacturer: {
              name: i?.assetManufacturerName
            }
          },
          sku: {
            name: i?.unitName
          },
          quantity: i?.totalQuantityReceipt,
          quantityOfVoucher: i?.quantityOfVoucher || i?.totalQuantityReceipt || 0,
          price: i?.price || i?.unitPrice || i?.assetOriginalCost,
          amount: i?.amount || i?.assetOriginalCost || (i?.totalQuantityReceipt * i?.unitPrice)
        }
      })
    }
  }

  handlePrint = async (value) => {
    const { currentOrg } = this.context;

    if ([LIST_ORGANIZATION.BVDK_BA_VI.code, LIST_ORGANIZATION.BV_VAN_DINH.code].includes(currentOrg?.code)) {
      return this.setState({
        item: value,
        shouldOpenPrintDialog: true,
      })
    }

    try {
      const searchObject = {}
      searchObject.pageIndex = 1;
      searchObject.pageSize = 10000;
      const { data } = await searchByPageAsset(value?.id, searchObject);
      const dataHeaderPrint = await searchByPageAssetPrint(value?.id);
      let responseDateHeaderPrint = dataHeaderPrint?.data?.data || {};
      delete responseDateHeaderPrint.id;

      const mergeObj = { ...value, ...responseDateHeaderPrint };

      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({ item: this.convertDataPrint(data?.data?.content, mergeObj) }, () => {
          this.setState({ shouldOpenPrintDialog: true })
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  handleCheck = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item.id);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(event.target.name) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length,
      });
    }
  };

  convertDataPrintVoucher = (dataConvent, currentOrg) => {
    const { organization } = getUserInformation();
    const moneyType = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
      ? appConst.TYPES_MONEY.SPACE.code
      : appConst.TYPES_MONEY.COMMA.code;
    const voucherDetails = dataConvent?.map(i => {
      const date = new Date(i?.inputDate);
      const calculateTotal = (key) => i?.items?.map(x => ({ ...x, amount: (x?.assetCarryingAmount || (x?.totalQuantityReceipt * (x?.unitPrice || x?.assetOriginalCost))) }))?.reduce(
        (total, item) => Number(total) + Number(item[key]),
        0
      );
      return {
        ...i,
        day: String(date?.getDate())?.padStart(2, '0'),
        month: String(date?.getMonth() + 1)?.padStart(2, '0'),
        year: String(date?.getFullYear()),
        voucherCode: i?.code,
        handoverPersonName: i?.departmentReceiptName,
        titleName: organization?.org?.name,
        budgetCode: organization?.budgetCode,
        items: i?.items?.map((item, index) => ({
          ...item,
          index: index + 1,
          productName: item?.productName,
          manufacturer: {
            name: item?.assetMadeIn
          },
          sku: {
            name: item?.unitName
          },
          quantity: item?.totalQuantityReceipt || 0,
          quantityOfVoucher: item?.totalQuantityReceipt || 0,
          price: convertMoney((item?.unitPrice || item?.assetOriginalCost), moneyType) || 0,
          amount: convertMoney((item?.assetCarryingAmount || (item?.totalQuantityReceipt * item?.unitPrice)), moneyType) || 0,
        })),
        totalQuantityOfVoucher: calculateTotal("totalQuantityReceipt"),
        totalQuantity: calculateTotal("totalQuantityReceipt"),
        totalAmount: convertMoney(calculateTotal("amount"), moneyType) || 0,
        totalPrice: convertMoney(calculateTotal("unitPrice"), moneyType) || 0,
        totalAmountToWords: convertNumberToWords(calculateTotal("unitPrice"), moneyType) || "Không",
      }
    })
    return voucherDetails
  }

  handlePrintVoucher = async () => {
    const { t } = this.props;
    let { setPageLoading, currentOrg } = this.context;
    this.setState({
      isPrintForUtilizationReport: true,
    })
    setPageLoading(true)
    try {
      let {
        selectedList,
        store,
        fromDate,
        toDate,
      } = this.state;

      let searchObject = {
        ids: selectedList.join(",") || null,
        status: appConst.listStatusWarehouseTransferObject.KET_THUC.code,
        type: TYPE_STORE.NHAP_KHO.code
      }
      searchObject.storeReceiptId = store?.id;
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }

      const response = await getVouchersPrint(searchObject);
      if (response?.data?.code === appConst.CODE.SUCCESS) {

        this.setState({
          item: {
            voucherDetails: this.convertDataPrintVoucher(response?.data?.data, currentOrg)
          }
        }, () => {
          this.setState({ shouldOpenPrintDialog: true })
        })
      } else {
        toast.error(t("general.error"));
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }

  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenPrintDialog: false,
      isPrintForUtilizationReport: false
    })
  }

  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name, source) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data }, () => {
      this.search();
    });
  };
  calculateTotal = (key) => this?.state?.item?.voucherDetails?.reduce((total, item) => Number(total) + Number(item[key]), 0);
  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      listItemAsset,
      shouldOpenPrintDialog,
      item,
      totalElementAssets,
      rowsPerPageAssets,
      pageAssets,
      checkAll,
      isPrintForUtilizationReport
    } = this.state;
    let TitlePage = t("Store.inventory_receiving_voucher");
    const { currentOrg } = this.context;
    const { RECEIVING, RECEIVING_SYNTHETIC } = LIST_PRINT_FORM_BY_ORG.WAREHOUSE_MANAGEMENT;
    let columns = [
      {
        title: (
          <>
            <Checkbox
              name="all"
              className="p-0"
              checked={checkAll}
              onChange={(e) => this.handleCheck(e)}
            />
          </>
        ),
        align: "center",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <Checkbox
            name={rowData.id}
            className="p-0"
            checked={rowData?.checked ? rowData?.checked : false}
            onChange={(e) => {
              this.handleCheck(e, rowData);
            }}
          />
        ),
      },
      {
        title: t("general.index"),
        field: "",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
      },
      {
        title: t("general.action"),
        field: "",
        maxWidth: 60,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          return (
            <LightTooltip
              title={t("general.print")}
              placement="right-end"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton size="small" onClick={() => this.handlePrint(rowData)}>
                <Icon fontSize="small">print</Icon>
              </IconButton>
            </LightTooltip>
          )
        },
      },
      {
        title: t("InstrumentToolsList.issueDate"),
        field: "inputDate",
        align: "left",
        width: 120,
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) =>
          rowData.inputDate ? moment(rowData.inputDate).format("DD/MM/YYYY") : "",
      },
      {
        title: t("allocation_asset.status"),
        field: "status",
        width: "180px",
        maxWidth: "180px",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => {
          let { KET_THUC, DANG_THUC_HIEN, MOI_TAO } = appConst.STATUS_WAREHOUSE_TRANSFER;
          let className = {
            [MOI_TAO.indexOrder]: "status status-info",
            [DANG_THUC_HIEN.indexOrder]: "status status-warning",
            [KET_THUC.indexOrder]: "status status-success",
          }
          let status = appConst.listStatusWarehouseTransfer.find(
            (item) => item.transferStatusIndex === rowData?.status
          );
          return <div className={className[rowData?.status]}>{status?.name}</div>
        }
      },
      {
        title: t("general.code"),
        field: "code",
        width: "180px",
        maxWidth: "180px",
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("AssetWarehouseTransfer.typeDocument"),
        field: "",
        width: "150px",
        minWidth: "150px",
        align: 'left',
        render: (rowData) => {
          const foundItem = subVoucherType.find((item) => item.code === rowData?.subtype);
          return foundItem ? <span>{foundItem.name}</span> : '';
        }
      },
      {
        title: t("AssetWarehouseTransfer.receiverWarehouse"),
        field: "storeReceiptName",
        width: "150px",
        minWidth: "150px",
      },
      {
        title: t("Product.supplier"),
        field: "supplierName",
        width: "220px",
        minWidth: "220px",
      },
    ];
    let VTSubColumnsProps = {
      isWarehouseReceiving: true,
    }
    let columnsSubTable = COLUMNS_SubTable(t, convertNumberPriceRoundUp, VTSubColumnsProps);

    let dataView = inventoryReceivingPrintData(item, currentOrg);
    let printUrls = isPrintForUtilizationReport
      ? [
        ...RECEIVING_SYNTHETIC.GENERAL,
        ...(RECEIVING_SYNTHETIC[currentOrg?.printCode] || []),
      ] : [
        ...RECEIVING.GENERAL,
        ...(RECEIVING[currentOrg?.printCode] || [])
      ];
    let isNormalPrint = [
      LIST_ORGANIZATION.BV_VAN_DINH.code,
      LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code,
    ].includes(currentOrg?.code) && !isPrintForUtilizationReport;
    let isShowPrintPreview = LIST_ORGANIZATION.BVDK_BA_VI.code === currentOrg?.code
      || isNormalPrint;
    
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
            {TitlePage} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.warehouseManagement"),
              },
              {
                name: t("Store.inventory_receiving_voucher"),
                path: "warehouse_receiving",
              },
            ]}
          />
        </div>
        <ComponentTransferTable
          t={t}
          i18n={i18n}
          item={this.state}
          search={this.search}
          handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
          handleKeyUp={this.handleKeyUp}
          handleTextChange={this.handleTextChange}
          handleChangePage={this.handleChangePage}
          setRowsPerPage={this.setRowsPerPage}
          setState={this.setStateTransfer}
          columns={columns}
          getRowData={(rowData) => this.handleGetRowData(rowData)}
          handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
          handleSetDataSelect={this.handleSetDataSelect}
          handlePrintVoucher={this.handlePrintVoucher}
        />
        {shouldOpenPrintDialog && (
          isShowPrintPreview ? (
            <PrintPreviewTemplateDialog
              t={t}
              handleClose={this.handleDialogClose}
              open={shouldOpenPrintDialog}
              item={this.state.item}
              title={t("Phiếu nhập kho")}
              model={PRINT_TEMPLATE_MODEL.WAREHOUSE_MANAGEMENT.RECEIVING}
            />
          ) : (
            <PrintMultipleFormDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenPrintDialog}
              item={dataView}
              title={t("Phiếu nhập kho")}
              urls={printUrls}
            />
          )
        )}

        <div>
          <ValidatorForm onSubmit={() => { }}>
            <MaterialTable
              data={listItemAsset ?? []}
              columns={columnsSubTable}
              options={{
                draggable: false,
                toolbar: false,
                selection: false,
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                sorting: false,
                rowStyle: rowData => ({
                  backgroundColor: rowData?.tableData?.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                headerStyle: {
                  backgroundColor: '#358600',
                  color: '#fff',
                  paddingLeft: 10,
                  paddingRight: 10,
                  textAlign: "center",
                },
                padding: 'dense',
                maxBodyHeight: '350px',
                minBodyHeight: '260px',
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t('general.emptyDataMessageTable')}`
                }
              }}
              components={{
                Toolbar: props => (
                    <MTableToolbar {...props} />
                ),
              }}
              onSelectionChange={(rows) => {
                this.data = rows;
              }}
            />
            <CustomTablePagination
              rowsPerPageOptions={appConst.rowsPerPageOptions.popup}
              labelDisplayedRows={({ from, to, count }) =>
                `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
                }`
              }
              count={totalElementAssets}
              rowsPerPage={rowsPerPageAssets}
              page={pageAssets}
              onPageChange={this.handleChangePageAsset}
              onRowsPerPageChange={this.setRowsPerPageAsset}
            />
          </ValidatorForm>
        </div>
      </div>
    );
  }
}
WarehouseTransferTable.contextType = AppContext;
export default WarehouseTransferTable;
