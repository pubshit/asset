import {
    Button,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    Link,
    TablePagination,
    Card,
    Collapse,
    CardContent
} from "@material-ui/core";
import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst } from "app/appConst";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi";
import AsynchronousAutocompleteTransfer from "../utilities/AsynchronousAutocompleteTransfer";
import { ValidatorForm } from "react-material-ui-form-validator";
import { searchByPage } from "../Store/StoreService";
import { STATUS_STORE } from "../../appConst";
import {getTheHighestRole, getUserInformation, defaultPaginationProps} from "../../appFunction";

function ComponentTransferTable(props) {
    let isButtonAll = props?.item?.statusIndex === null;
    let { t, getRowData } = props;
    let {
        rowsPerPage,
        page,
        totalElements,
        itemList,
        hasCreatePermission,
        isRoleAdmin,
        fromDate,
        toDate,
        openAdvanceSearch,
        listStore,
        store,
        selectedData
    } = props?.item;
    const {isRoleAccountant} = getTheHighestRole();
    const {departmentUser} = getUserInformation();
    let searchObjectStore = {
        ...appConst.OBJECT_SEARCH_MAX_SIZE,
        isActive: STATUS_STORE.HOAT_DONG.code,
        managementDepartmentId: isRoleAccountant ? null : departmentUser?.id
    }
    return (
        <>
            <Grid container spacing={2} justifyContent="space-between" className="mt-10">
                <Grid item md={6} xs={12}>
                    {(hasCreatePermission || isRoleAdmin) && isButtonAll && (
                        <Button
                            className="mb-16 mr-16 align-bottom"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                props.handleButtonAdd({
                                    startDate: new Date(),
                                    endDate: new Date(),
                                    isStatus: false,
                                    isNew: true,
                                    isView: false,
                                    isCheckReceiverDP: true,
                                    isCheckHandoverDP: true,
                                });
                            }}
                        >
                            {t("general.add")}
                        </Button>
                    )}
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handleOpenAdvanceSearch}
                    >
                        {t("general.advancedSearch")}
                        <ArrowDropDownIcon />
                    </Button>
                    <Button
                        className="mr-16 align-bottom"
                        variant="contained"
                        color="primary"
                        onClick={props.handlePrintVoucher}
                    >
                        {t("general.summaryPrinting")}
                    </Button>
                </Grid>
                <Grid item md={6} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <Input
                            className="search_box w-100"
                            onChange={props.handleTextChange}
                            onKeyDown={props.handleKeyDownEnterSearch}
                            onKeyUp={props.handleKeyUp}
                            placeholder={t("AssetWarehouseTransfer.search")}
                            id="search_box"
                            startAdornment={
                              <InputAdornment position="end">
                                <SearchIcon onClick={() => props.search()} className="searchTable"/>
                              </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>

                {/* Bộ lọc Tìm kiếm nâng cao */}
                <Grid item xs={12}>
                    <Collapse in={openAdvanceSearch} className="pt-0 pb-0">
                        <ValidatorForm onSubmit={() => {}}>
                            <Card elevation={2} className="pt-0 pb-0">
                                <CardContent>
                                    <Grid container spacing={2}>
                                        {/* Phòng bàn giao */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <AsynchronousAutocompleteTransfer
                                                label={t("AssetWarehouseTransfer.receiverWarehouse")}
                                                searchFunction={searchByPage}
                                                searchObject={searchObjectStore}
                                                listData={listStore || []}
                                                setListData={(data) => props?.handleSetDataSelect(
                                                    data,
                                                    "listStore"
                                                )}
                                                defaultValue={store ? store : null}
                                                displayLable={"name"}
                                                value={store ? store : null}
                                                onSelect={(data) => props?.handleSetDataSelect(data, "store")}
                                                noOptionsText={t("general.noOption")}
                                            />
                                        </Grid>
                                        {/* Từ ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxFrom")}
                                                    format="dd/MM/yyyy"
                                                    value={fromDate ?? null}
                                                    maxDate={toDate || undefined}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "fromDate"
                                                    )}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    minDateMessage={t("general.minDateDefault")}
                                                    maxDateMessage={t("general.maxDateDefault")}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearable
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        {/* Đến ngày */}
                                        <Grid item xs={12} sm={12} md={3}>
                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
                                                <KeyboardDatePicker
                                                    margin="none"
                                                    fullWidth
                                                    autoOk
                                                    id="date-picker-dialog"
                                                    label={t("MaintainPlaning.dxTo")}
                                                    format="dd/MM/yyyy"
                                                    value={toDate ?? null}
                                                    minDate={fromDate || undefined}
                                                    onChange={(data) => props.handleSetDataSelect(
                                                        data,
                                                        "toDate"
                                                    )}
                                                    KeyboardButtonProps={{ "aria-label": "change date", }}
                                                    minDateMessage={t("general.minDateDefault")}
                                                    maxDateMessage={t("general.maxDateDefault")}
                                                    invalidDateMessage={t("general.invalidDateFormat")}
                                                    clearable
                                                    clearLabel={t("general.remove")}
                                                    cancelLabel={t("general.cancel")}
                                                    okLabel={t("general.select")}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </ValidatorForm>
                    </Collapse>
                </Grid>
                <Grid item xs={12}>
                    <MaterialTable
                        onRowClick={(e, rowData) => {
                            getRowData(rowData)
                        }}
                        title={t("general.list")}
                        data={itemList}
                        columns={props?.columns}
                        localization={{
                            body: {
                                emptyDataSourceMessage: `${t(
                                    "general.emptyDataMessageTable"
                                )}`,
                            },
                        }}
                        options={{
                            selection: false,
                            actionsColumnIndex: -1,
                            paging: false,
                            search: false,
                            sorting: false,
                            rowStyle: (rowData) => ({
                                backgroundColor: selectedData?.id === rowData?.id
                                    ? "#ccc"
                                    : rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                            }),
                            maxBodyHeight: "490px",
                            minBodyHeight: "260px",
                            headerStyle: {
                                backgroundColor: "#358600",
                                color: "#fff",
                                paddingLeft: 10,
                                paddingRight: 10,
                                textAlign: "center",
                            },
                            padding: "dense",
                            toolbar: false,
                        }}
                        components={{
                            Toolbar: (props) => <MTableToolbar {...props} />,
                        }}
                        onSelectionChange={(rows) => {
                            this.data = rows;
                        }}
                    />
                    <TablePagination
                        {...defaultPaginationProps()}
                        rowsPerPageOptions={appConst.rowsPerPageOptions.table}
                        count={totalElements}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={props.handleChangePage}
                        onRowsPerPageChange={props.setRowsPerPage}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default ComponentTransferTable;
