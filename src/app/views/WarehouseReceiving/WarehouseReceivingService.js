import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/api/asset_transfer_status";
const API_PATH_TRANSFER =
  ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/inventory";

//api mới
export const searchByPage = (searchObject) => {
  let url = API_PATH_TRANSFER + `/search-by-page`;
  return axios.post(url, searchObject);
};

export const searchByPageAsset = (id, searchObject) => {
  const url =
    API_PATH_TRANSFER +
    `/${id}/items?pageIndex=${searchObject?.pageIndex}&pageSize=${searchObject?.pageSize}`;
  return axios.get(url);
};

export const getVouchersPrint = (params) => {
  let config = { params };
  return axios.get(API_PATH_TRANSFER + "/print", config);
};
