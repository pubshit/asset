import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const WarehouseReceivingTable = EgretLoadable({
  loader: () => import("./WarehouseReceivingTable")
});
const ViewComponent = withTranslation()(WarehouseReceivingTable);

const WarehouseReceivingRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "warehouse_receiving",
    exact: true,
    component: ViewComponent
  }
];

export default WarehouseReceivingRoutes;