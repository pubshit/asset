import React from 'react';
import {Grid,} from "@material-ui/core";
import {MTableToolbar} from "material-table";
import {appConst} from "../../../appConst";
import CustomMaterialTable from "../../CustomMaterialTable";
import CustomTablePagination from "../../CustomTablePagination";


function ComponentAssetTable(props) {
  let {t} = props;
  let {
    listItemAsset,
    pageAssets,
    rowsPerPageAssets,
    totalElementAssets,
  } = props?.item;
  

  return (
    <>
      <Grid container spacing={2} justifyContent="space-between" className="mt-10">
        <Grid item xs={12}>
          <CustomMaterialTable
            title={t("general.list")}
            data={listItemAsset || []}
            columns={props.columns}
            localization={{
              body: {
                emptyDataSourceMessage: `${t(
                  "general.emptyDataMessageTable"
                )}`,
              },
            }}
            options={{
              selection: false,
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              sorting: false,
              rowStyle: (rowData) => ({
                backgroundColor:
                  rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
              }),
              maxBodyHeight: "490px",
              minBodyHeight: "260px",
              headerStyle: {
                backgroundColor: "#358600",
                color: "#fff",
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: "center",
              },
              padding: "dense",
              toolbar: false,
            }}
            components={{
              Toolbar: (props) => <MTableToolbar {...props} />,
            }}
            onSelectionChange={(rows) => {
              this.data = rows;
            }}
          />
          <CustomTablePagination
            rowsPerPageOptions={appConst.rowsPerPageOptions.table}
            labelDisplayedRows={({from, to, count}) =>
              `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
              }`
            }
            count={totalElementAssets}
            rowsPerPage={rowsPerPageAssets}
            page={pageAssets}
            onPageChange={props.handleChangePage}
            onRowsPerPageChange={props.setRowsPerPage}
          />
        </Grid>
      </Grid>
    </>
  );
}

ComponentAssetTable.propTypes = {};
export default ComponentAssetTable;