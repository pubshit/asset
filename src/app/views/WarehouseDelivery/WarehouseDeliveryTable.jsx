import { appConst, PRINT_TEMPLATE_MODEL, variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  convertFromToDate,
  convertNumberPriceRoundUp,
  isValidDate
} from "app/appFunction";
import { Breadcrumb } from "egret";
import React from "react";
import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConstantList from "../../appConfig";
import {
  searchByPageAsset,
  searchByPageAssetPrint,
  searchByPageInventory,
} from "./WarehouseDeliveryService";
import ComponentTransferTable from "./ComponentTransferTable";
import { COLUMNS, COLUMNS_SubTable, convertListSubTable } from "./constants";
import ComponentAssetTable from "./Component/ComponentAssetTable";
import { ValidatorForm } from "react-material-ui-form-validator";
import { PrintMultipleFormDialog } from "../FormCustom/PrintMultipleFormDialog";
import { TabPanel } from "../Component/Utilities";
import { LIST_PRINT_FORM_BY_ORG } from "../FormCustom/constant";
import { Checkbox } from "@material-ui/core";
import { getVouchersPrint } from "../WarehouseReceiving/WarehouseReceivingService"
import { LIST_ORGANIZATION, TYPE_STORE } from "../../appConst";
import { utilizationReportPrintData } from "../FormCustom/Utilization";
import { convertMoney, convertNumberPrice, convertNumberToWords, getUserInformation } from "../../appFunction";
import { PrintPreviewTemplateDialog } from "../Component/PrintPopup/PrintPreviewTemplateDialog";

toast.configure({
  autoClose: 2000, draggable: false, limit: 3,
});

class WarehouseDeliveryTable extends React.Component {
  state = {
    keyword: "",
    rowsPerPage: 5,
    page: 0,
    AssetTransfer: [],
    item: {},
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    selectAllItem: false,
    selectedList: [],
    totalElements: 0,
    isView: false,
    shouldOpenConfirmationDeleteAllDialog: false,
    shouldOpenImportExcelDialog: false,
    isPrint: false,
    hasDeletePermission: false,
    hasEditPermission: false,
    hasPrintPermission: false,
    hasCreatePermission: false,
    statusIndex: null,
    tabValue: 0,
    isRoleAdmin: false,
    isCheckReceiverDP: true,
    tableHeightMax: "500px",
    tableHeightMin: "450px",
    selectedRow: null,
    selectedData: null,
    listItemAsset: [],
    pageAssets: 0,
    rowsPerPageAssets: 5,
    totalElementAssets: 0,
    openPrint: false,
    checkAll: false,
    isPrintForUtilizationReport: false,
  };
  numSelected = 0;
  rowCount = 0;
  voucherType = ConstantList.VOUCHER_TYPE.Outbound; //Điều chuyển

  setPage = (page) => {
    this.setState({ page }, function () {
      this.updatePageData();
    });
  };

  setRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value, page: 0 }, function () {
      this.updatePageData();
    });
  };

  setPageAsset = (page) => {
    this.setState({ pageAssets: page }, function () {
      this.handleGetDataAsset();
    });
  };

  setRowsPerPageAsset = (event) => {
    this.setState({ rowsPerPageAssets: event.target.value, page: 0 }, function () {
      this.handleGetDataAsset();
    });
  };

  handleChangePageAsset = (event, newPage) => {
    this.setPageAsset(newPage);
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  updatePageData = () => {
    let { setPageLoading } = this.context;
    let { t } = this.props;
    setPageLoading(true);
    let searchObject = {};
    searchObject.type = this.voucherType;
    searchObject.keyword = this.state.keyword.trim() || null;
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;
    searchObject.status = this.state?.status;
    searchObject.fromDate = this.state?.fromDate ? convertFromToDate(this.state?.fromDate).fromDate : null;
    searchObject.toDate = this.state?.toDate ? convertFromToDate(this.state?.toDate).toDate : null;
    searchObject.storeExportId = this.state?.store?.id;
    searchByPageInventory(searchObject)
      .then(({ data }) => {
        let countItemChecked = 0;
        let newArray = data?.data?.content?.map((voucher) => {
          if (this.state?.selectedList?.find(checkedItem => checkedItem?.id === voucher.id)) {
            countItemChecked++
          }
          return {
            ...voucher,
            checked: !!this.state?.selectedList?.find(checkedItem => checkedItem?.id === voucher.id),
          };
        });
        this.setState({
          selectedRow: null,
          listItemAsset: [],
          itemList: [...newArray],
          checkAll: newArray?.length === countItemChecked && newArray?.length > 0,
          totalElements: data?.data?.totalElements,
        });
      })
      .catch(() => {
        toast.error(t("toastr.error"));
      })
      .finally(() => {
        setPageLoading(false);
      })
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenConfirmationDeleteAllDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isPrintForUtilizationReport: false,
      openPrint: false,
      isPrint: false,
    });
    this.updatePageData();
  };

  handleOKEditClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      shouldOpenImportExcelDialog: false,
      shouldOpenConfirmationReceiveDialog: false,
      isPrintForUtilizationReport: false,
    });
    this.updatePageData();
  };

  componentDidMount() {
    this.setState({
      statusIndex: null,
    }, () => this.updatePageData());
  }

  handleClick = (event, item) => {
    let { AssetTransfer } = this.state;
    if (item.checked === null) {
      item.checked = true;
    } else {
      item.checked = !item.checked;
    }
    let selectAllItem = true;
    for (let i = 0; i < AssetTransfer.length; i++) {
      if (AssetTransfer[i].checked === null || AssetTransfer[i].checked === false) {
        selectAllItem = false;
      }
      if (AssetTransfer[i].id === item.id) {
        AssetTransfer[i] = item;
      }
    }
    this.setState({
      selectAllItem: selectAllItem, AssetTransfer: AssetTransfer,
    });
  };
  handleSelectAllClick = () => {
    let { AssetTransfer } = this.state;
    for (let i = 0; i < AssetTransfer.length; i++) {
      AssetTransfer[i].checked = !this.state.selectAllItem;
    }
    this.setState({
      selectAllItem: !this.state.selectAllItem, AssetTransfer: AssetTransfer,
    });
  };

  handleDelete = (id) => {
    this.setState({
      id, shouldOpenConfirmationDialog: true,
    });
  };

  handleDeleteAll = () => {
    this.handleDeleteList(this.data).then(() => {
      this.updatePageData();
      this.handleDialogClose();
    });
  };
  /* Export to excel */

  checkCount = (count) => {
    return count ? (count > 99 ? "99+" : count) : 0;
  };

  handleRenderStatus = (rowData) => {
    let { KET_THUC, DANG_THUC_HIEN, MOI_TAO } = appConst.STATUS_WAREHOUSE_TRANSFER;
    let className = {
      [MOI_TAO.indexOrder]: "status status-info",
      [DANG_THUC_HIEN.indexOrder]: "status status-warning",
      [KET_THUC.indexOrder]: "status status-success",
    }
    let status = appConst.listStatusWarehouseTransfer.find(
      (item) => item.transferStatusIndex === rowData?.status
    );
    return <div className={className[rowData?.status]}>{status?.name}</div>
  };

  handleGetRowData = (rowData) => {
    this.setState({ selectedRow: rowData.id, selectedData: rowData, pageAssets: 0 }, () => {
      this.handleGetDataAsset(rowData?.id)
    });
  }

  handleGetDataAsset = async (id) => {
    const { pageAssets, rowsPerPageAssets } = this.state
    const searchObject = {}
    searchObject.pageIndex = pageAssets + 1;
    searchObject.pageSize = rowsPerPageAssets;
    try {
      const { data } = await searchByPageAsset(id || this.state.selectedData?.id, searchObject)
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({ listItemAsset: convertListSubTable(data?.data?.content) || [], totalElementAssets: data?.data?.totalElements, })
      } else {
        this.setState({ listItemAsset: [] })
      }
    } catch (e) {
      console.error(e)
    }
  }

  convertDataPrint = (value = {}, vouchers = []) => {
    return {
      ...value,
      stockReceiptDeliveryStore: {
        name: value?.storeExportName,
        address: value?.address || value?.storeAddress
      },
      receiverPerson: {
        ...value?.receiverPerson,
        displayName: value?.receiverPersonName
      },
      voucherDetails: vouchers?.map(i => {
        return {
          ...i,
          product: {
            name: i?.assetName || i?.productName
          },
          sku: {
            name: i?.unitName
          },
          quantity: i?.totalQuantityExport,
          price: i?.unitPrice || (i?.assetOriginalCost / i?.totalQuantityExport),
          amount: i?.assetOriginalCost || (i?.unitPrice * i.totalQuantityExport),
          batchCode: i?.lotNumber,
        }
      })
    }
  }

  handlePrint = async (value) => {
    const { currentOrg } = this.context;
    const shouldShowPrintPreviewDialog = [
      LIST_ORGANIZATION.BVDK_BA_VI.code,
      LIST_ORGANIZATION.BV_VAN_DINH.code
    ].includes(currentOrg?.code)

    if (shouldShowPrintPreviewDialog) {
      return this.setState({
        item: value,
        openPrint: true,
      })
    }

    try {
      const searchObject = {};
      searchObject.pageIndex = 1;
      searchObject.pageSize = 10000;
      const { data } = await searchByPageAsset(value?.id, searchObject);
      const dataHeaderPrint = await searchByPageAssetPrint(value?.id);
      let responseDateHeaderPrint = dataHeaderPrint?.data?.data || {};

      delete responseDateHeaderPrint.id;
      const mergeObj = { ...value, ...responseDateHeaderPrint }
      if (appConst.CODE.SUCCESS === data?.code) {
        this.setState({
          dataPrint: this.convertDataPrint(mergeObj, data?.data?.content),
          isPrintForUtilizationReport: false,
          openPrint: true
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  search = () => {
    this.setPage(0);
  };


  handleOpenAdvanceSearch = () => {
    let { openAdvanceSearch } = this.state;
    this.setState({ openAdvanceSearch: !openAdvanceSearch });
  };

  handleSetDataSelect = (data, name) => {
    if (["fromDate", "toDate"].includes(name)) {
      this.setState({ [name]: data, selectedList: [] }, () => {
        if (data === null || isValidDate(data)) {
          this.search();
        }
      })
      return;
    }
    this.setState({ [name]: data, selectedList: [] }, () => {
      this.search();
    });
  };
  handleTextChange = (event) => {
    this.setState({ keyword: event.target.value }, function () { });
  };
  handleKeyDownEnterSearch = (e) => {
    if (appConst.KEY.ENTER === e.key) {
      this.search();
    }
  };
  handleKeyUp = (e) => {
    !e.target.value && this.search();
  };

  handleChangeSelectTable = (event, item) => {
    let { selectedList, itemList } = this.state;

    if (variable.listInputName.all === event?.target?.name) {
      itemList.map((item) => {
        item.checked = event.target.checked;
        selectedList.push(item);
        return item;
      });
      if (!event.target.checked) {
        const itemListIds = itemList.map((item) => item?.id)
        const filteredArray = selectedList.filter(item => !itemListIds.includes(item?.id));

        selectedList = filteredArray;
      }
      this.setState({ checkAll: event.target.checked, selectedList });
    }
    else {
      let existItem = selectedList.find(item => item?.id === event.target.name);

      item.checked = event.target.checked;
      !existItem ?
        selectedList.push(item) :
        selectedList.map((item, index) =>
          item === existItem ?
            selectedList.splice(index, 1) :
            null
        );
      let countItemChecked = 0
      itemList.map((item) => {
        if (item?.checked) {
          countItemChecked++
        }
      })

      this.setState({
        selectAllItem: selectedList,
        selectedList,
        checkAll: countItemChecked === itemList.length && itemList?.length > 0,
      });
    }
  };

  convertDataPrintVoucher = (dataConvent, currentOrg) => {
    const { organization } = getUserInformation();
    const moneyType = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
      ? appConst.TYPES_MONEY.SPACE.code
      : appConst.TYPES_MONEY.COMMA.code;
    const convertMoneyFunc = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
      ? convertMoney
      : convertNumberPrice;
    const voucherDetails = dataConvent?.map(i => {
      const date = new Date(i?.inputDate);
      const calculateTotal = (key) => i?.items?.map(x => ({ ...x, amount: (x?.assetCarryingAmount || (x?.totalQuantityExport * (x?.unitPrice || x?.assetOriginalCost))) }))?.reduce(
        (total, item) => Number(total) + Number(item[key]),
        0
      );
      return {
        ...i,
        titleName: organization?.org?.name,
        budgetCode: organization?.budgetCode,
        day: String(date?.getDate()).padStart(2, '0'),
        month: String(date?.getMonth()).padStart(2, '0'),
        year: String(date?.getFullYear()),
        receiverPerson: {
          displayName: i?.personExportName
        },
        receiverDepartment: {
          name: i?.departmentReceiptName
        },
        stockReceiptDeliveryStore: {
          name: i?.storeExportName || "..".repeat(10),
          address: i?.storeExportAddress || "..".repeat(10)
        },
        reason: i?.voucherReason,
        voucherDetails: i?.items?.map((item, index) => {
          return {
            ...item,
            index: index + 1,
            price: convertNumberPrice((item?.unitPrice || item?.assetOriginalCost), moneyType) || 0,
            amount: convertMoneyFunc((item?.assetCarryingAmount || (item?.totalQuantityExport * item?.unitPrice)), moneyType) || 0,
          }
        }),
        totalQuantityOfVoucher: calculateTotal("totalQuantityExport"),
        totalQuantity: calculateTotal("totalQuantityExport"),
        totalAmount: convertNumberPrice(calculateTotal("amount"), moneyType) || 0,
        totalPrice: convertNumberPrice(calculateTotal("unitPrice"), moneyType) || 0,
        totalAmountToWords: convertNumberToWords(calculateTotal("unitPrice"), moneyType) || "Không",
      }
    })
    return voucherDetails;
  }

  handlePrintVoucher = async () => {
    const { t } = this.props;
    let { setPageLoading, currentOrg } = this.context;
    this.setState({
      isPrintForUtilizationReport: true,
    })
    setPageLoading(true)
    try {
      let { selectedList,
        receiverDepartment,
        store,
        fromDate,
        toDate,
      } = this.state;

      let searchObject = {
        ids: selectedList?.map(i => i?.id).join(", "),
        type: TYPE_STORE.XUAT_KHO.code
      }
      searchObject.receiverDepartmentId = receiverDepartment?.id;
      searchObject.storeExportId = store?.id;
      if (fromDate) {
        searchObject.fromDate = convertFromToDate(fromDate).fromDate;
      }
      if (toDate) {
        searchObject.toDate = convertFromToDate(toDate).toDate;
      }
      const response = await getVouchersPrint(searchObject);
      if (response?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          dataPrint: {
            voucherDetails: this.convertDataPrintVoucher(response?.data?.data, currentOrg),
            stores: response?.data?.data?.storeNames,
          },
          openPrint: true,
        })
      } else {
        toast.error(t("general.error"));
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false)
    }

  }

  render() {
    const { t, i18n } = this.props;
    let {
      rowsPerPage,
      page,
      tabValue,
      openPrint,
      dataPrint,
      checkAll,
      isPrintForUtilizationReport
    } = this.state;
    const { currentOrg } = this.context;
    let is_BVC_org = currentOrg.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code;
    const { DELIVERY, DELIVERY_SYNTHETIC } = LIST_PRINT_FORM_BY_ORG.WAREHOUSE_MANAGEMENT;
    let TitlePage = t("Store.inventory_delivery_voucher");
    let VTSubColumnsProps = {
      isWarehouseDelivery: true,
    }
    let columnSelect = [{
      title: (
        <>
          <Checkbox
            name="all"
            className="p-0"
            checked={checkAll}
            onChange={(e) => this.handleChangeSelectTable(e)}
          />
        </>
      ),
      align: "left",
      maxWidth: 50,
      cellStyle: {
        textAlign: "center",
      },
      render: (rowData) => (
        <Checkbox
          name={rowData.id}
          className="p-0"
          checked={rowData.checked ? rowData.checked : false}
          onChange={(e) => {
            this.handleChangeSelectTable(e, rowData);
          }}
        />
      ),
    }];
    let columns = [
      ...columnSelect,
      ...COLUMNS(t, page, rowsPerPage, this.handleRenderStatus, this.handlePrint)
    ];
    let columnsSubTable = COLUMNS_SubTable(t, convertNumberPriceRoundUp)
    let dataView = utilizationReportPrintData(dataPrint, currentOrg);
    let printUrls = isPrintForUtilizationReport
      ? [
        ...DELIVERY_SYNTHETIC.GENERAL,
        ...(DELIVERY_SYNTHETIC[currentOrg?.printCode] || []),
      ] : [
        ...DELIVERY.GENERAL,
        ...(DELIVERY[currentOrg?.printCode] || []),
      ];
    let isShowPrintPreview = LIST_ORGANIZATION.BVDK_BA_VI.code === currentOrg?.code
      || (LIST_ORGANIZATION.BV_VAN_DINH.code === currentOrg?.code && !isPrintForUtilizationReport);
    return (<div className="m-sm-30">
      <Helmet>
        <title>
          {TitlePage} | {t("web_site")}
        </title>
      </Helmet>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            {
              name: t("Dashboard.warehouseManagement"),
            },
            {
              name: t("Store.inventory_delivery_voucher"),
              path: "warehouse_delivery",
            },
          ]}
        />
      </div>

      <TabPanel
        value={tabValue}
        index={appConst.tabTransfer.tabAll}
        className="mp-0"
      >
        <ValidatorForm onSubmit={() => { }}>
          <ComponentTransferTable
            t={t}
            is_BVC_org={is_BVC_org}
            i18n={i18n}
            item={this.state}
            handleChangePage={this.handleChangePage}
            setRowsPerPage={this.setRowsPerPage}
            columns={columns}
            search={this.search}
            handlePrint={this.handlePrint}
            handleKeyDownEnterSearch={this.handleKeyDownEnterSearch}
            handleKeyUp={this.handleKeyUp}
            handleTextChange={this.handleTextChange}
            getRowData={(rowData) => this.handleGetRowData(rowData)}
            handleOpenAdvanceSearch={this.handleOpenAdvanceSearch}
            handleSetDataSelect={this.handleSetDataSelect}
            handlePrintVoucher={this.handlePrintVoucher}
          />
          {openPrint && (
            isShowPrintPreview ? (
              <PrintPreviewTemplateDialog
                t={t}
                handleClose={this.handleDialogClose}
                open={openPrint}
                item={this.state.item}
                title={t("Phiếu xuất kho")}
                model={PRINT_TEMPLATE_MODEL.WAREHOUSE_MANAGEMENT.DELIVERY}
              />
            ) : (
              <PrintMultipleFormDialog
                t={t}
                i18n={i18n}
                handleClose={this.handleDialogClose}
                open={openPrint}
                item={dataView}
                title={t('Phiếu xuất kho')}
                urls={printUrls}
              />
            )
          )}

          <ComponentAssetTable
            t={t}
            i18n={i18n}
            item={this.state}
            handleChangePage={this.handleChangePageAsset}
            setRowsPerPage={this.setRowsPerPageAsset}
            columns={columnsSubTable}
          />
        </ValidatorForm>
      </TabPanel>
    </div>);
  }
}

WarehouseDeliveryTable.contextType = AppContext;
export default WarehouseDeliveryTable;
