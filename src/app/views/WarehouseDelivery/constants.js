import React from "react";
import moment from "moment/moment";
import { subVoucherType } from "../../appConst";
import { LightTooltip, formatTimestampToDate } from "../../appFunction";
import { TextValidator } from "react-material-ui-form-validator";
import { Icon, IconButton } from "@material-ui/core";

export const COLUMNS = (
  t,
  page,
  rowsPerPage,
  handleRenderStatus,
  handlePrint
) => [
  {
    title: t("general.action"),
    field: "",
    minWidth: 80,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      return (
        <LightTooltip
          title={t("general.print")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
        >
          <IconButton size="small" onClick={() => handlePrint(rowData)}>
            <Icon fontSize="small">print</Icon>
          </IconButton>
        </LightTooltip>
      );
    },
  },
  {
    title: t("general.index"),
    field: "",
    maxWidth: 50,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => page * rowsPerPage + (rowData.tableData.id + 1),
  },
  {
    title: t("AssetTransfer.issueDate"),
    field: "inputDate",
    align: "left",
    width: 120,
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) =>
      rowData.inputDate ? moment(rowData.inputDate).format("DD/MM/YYYY") : "",
  },
  {
    title: t("allocation_asset.status"),
    field: "status",
    width: "150px",
    minWidth: "150px",
    cellStyle: {
      textAlign: "center",
    },
    render: handleRenderStatus,
  },
  {
    title: t("general.code"),
    field: "code",
    width: "130px",
    minWidth: "120px",
    align: "center",
  },
  {
    title: t("Loại chứng từ"),
    field: "",
    width: "150px",
    minWidth: "150px",
    align: "center",
    render: (rowData) => {
      const foundItem = subVoucherType.find(
        (item) => item.code === rowData?.subtype
      );
      return foundItem ? <span>{foundItem.name}</span> : "";
    },
  },
  {
    title: t("Kho xuất"),
    field: "storeExportName",
    width: 180,
    minWidth: 180,
  },
];

export const COLUMNS_SubTable = (t, convertNumberPriceRoundUp, props) => [
  {
    title: t("Asset.stt"),
    field: "",
    align: "left",
    width: "50px",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("Product.code"),
    field: "assetCode",
    minWidth: 120,
    align: "left",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Asset.managementCode"),
    field: "assetManagementCode",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Product.name"),
    field: "assetName",
    align: "left",
    minWidth: 250,
    maxWidth: 400,
  },
  {
    title: t("Asset.serialNumber"),
    field: "assetSerialNumber",
    align: "left",
    minWidth: 120,
  },
  {
    title: t("Asset.model"),
    field: "assetModel",
    align: "left",
    minWidth: 120,
  },
  {
    title: t("Asset.manufacturer"),
    field: "assetManufacturerName",
    align: "left",
    minWidth: 150,
    maxWidth: 200,
  },
  {
    title: t("Asset.quantityReceipt"),
    field: "totalQuantityReceipt",
    align: "center",
    minWidth: 150,
    maxWidth: 200,
    render: (rowData) =>
      props?.isWarehouseReceiving
        ? rowData?.totalQuantityReceipt
        : rowData?.totalQuantityExport,
  },
  {
    title: t("Asset.originalCost"),
    field: "assetOriginalCost",
    align: "left",
    minWidth: 150,
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) => convertNumberPriceRoundUp(rowData?.assetOriginalCost),
  },
  {
    title: t("Asset.carryingAmount"),
    field: "assetCarryingAmount",
    align: "left",
    minWidth: 150,
    cellStyle: {
      textAlign: "right",
    },
    render: (rowData) => convertNumberPriceRoundUp(rowData?.assetCarryingAmount),
  },
];

export const COLUMNS_SubTableVT = (t, props, isFromWarehouseTransfer = false) => [
  {
    title: t("Asset.stt"),
    field: "",
    align: "center",
    maxWidth: "50px",
    render: (rowData) => rowData.tableData.id + 1,
  },
  {
    title: t("SuggestedMaterials.code"),
    field: "productCode",
    align: "center",
    minWidth: 120,
    maxWidth: 120,
  },
  {
    title: t("SuggestedMaterials.nameMaterial"),
    field: "productName",
    align: "left",
    minWidth: 400,
    maxWidth: 400,
  },
  {
    title: t("Asset.stockKeepingUnit"),
    field: "unitName",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("general.Quantity"),
    field: "totalQuantityExport",
    align: "left",
    minWidth: 120,
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      return props?.isWarehouseReceiving || props?.isWarehouseDelivery ? (
        props?.isWarehouseReceiving ? (
          rowData?.totalQuantityReceipt
        ) : (
          rowData?.totalQuantityExport
        )
      ) : (
        <TextValidator
          className="w-100"
          type="number"
          onChange={(totalQuantityExport) =>
            props.handleRowDataCellChange(rowData, totalQuantityExport)
          }
          name="totalQuantityExport"
          value={rowData?.totalQuantityExport || rowData?.asset?.quantity}
          inputProps={{
            readOnly: props?.item?.isView || props?.item?.isStatus,
            className: "text-center",
          }}
          validators={[
            "required",
            "matchRegexp:^\\d+(\\.\\d{1,2})?$",
            "minFloat:0.000000001",
            `maxFloat:${rowData.remainQuantity || 9999999999999}`,
          ]}
          errorMessages={[
            t("general.required"),
            t("general.quantityError"),
            t("general.minNumberError"),
            t("general.mustBeSmallerThanRemainQty"),
          ]}
        />
      );
    },
  },
  {
    title: t("Product.remainQuantity"),
    field: "remainQuantity",
    align: "left",
    hidden: props?.isWarehouseReceiving || props?.isWarehouseDelivery,
    minWidth: "100px",
    cellStyle: {
      textAlign: "center",
    },
  },
  {
    title: t("Product.expiryDate"),
    field: "expiryDate",
    align: "left",
    minWidth: "100px",
    cellStyle: {
      textAlign: "center",
    },
    render: (rowData) => {
      if(isFromWarehouseTransfer) {
        let date = new Date(rowData?.expiryDate) || new Date();
        date.setMonth(date.getMonth() + 1)
        return rowData?.expiryDate ? formatTimestampToDate(date) : null;
      } else {
        return rowData?.expiryDate ? formatTimestampToDate(rowData.expiryDate) : "";
      }
    }
  },
];

export const convertListSubTable = (data = []) => {
  return data?.map((x) => {
    return {
      assetCode: x?.assetCode || x?.productCode,
      assetManagementCode: x?.assetManagementCode,
      assetName: x?.assetName || x?.productName,
      assetSerialNumber: x?.assetSerialNumber,
      assetModel: x?.assetModel,
      assetManufacturerName: x?.assetManufacturerName,
      totalQuantityReceipt: x?.totalQuantityReceipt,
      totalQuantityExport: x?.totalQuantityExport,
      assetOriginalCost: x?.assetOriginalCost || x?.unitPrice,
      assetCarryingAmount: x?.assetCarryingAmount,
    };
  });
};
