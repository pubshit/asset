import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';
const WarehouseDeliveryTable = EgretLoadable({
  loader: () => import("./WarehouseDeliveryTable")
});
const ViewComponent = withTranslation()(WarehouseDeliveryTable);

const WarehouseDeliveryRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "warehouse_delivery",
    exact: true,
    component: ViewComponent
  }
];

export default WarehouseDeliveryRoutes;