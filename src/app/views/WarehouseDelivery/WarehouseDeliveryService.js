import axios from "axios";
import ConstantList from "../../appConfig";

const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/";

export const searchByPageInventory = (searchObject) => {
  const url = API_PATH + "inventory/search-by-page";
  return axios.post(url, searchObject);
};

export const searchByPageAsset = (id, searchObject) => {
  const url =
    API_PATH +
    `inventory/${id}/items?pageIndex=${searchObject?.pageIndex}&pageSize=${searchObject?.pageSize}`;
  return axios.get(url);
};

export const searchByPageAssetPrint = (voucherId) => {
  const url = API_PATH + `inventory/${voucherId}/voucher/related`;
  return axios.get(url);
};

export const getVouchersPrint = (params) => {
  let config = { params };
  return axios.get(API_PATH + "report/utilization/assets", config);
};
