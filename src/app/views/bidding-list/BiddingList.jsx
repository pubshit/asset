import {
  Grid,
  IconButton,
  Icon,
  Button,
  TablePagination,
  FormControl,
  Input,
  InputAdornment,
} from "@material-ui/core";
import React from "react";
import MaterialTable, {MTableToolbar} from 'material-table';
import {useTranslation} from 'react-i18next';
import {deleteItem, getItemById, searchByPage} from "./BiddingListService";
import BiddingListDialog from "./BiddingListDialog";
import {Breadcrumb, ConfirmationDialog} from "egret";
import {Helmet} from 'react-helmet';
import SearchIcon from '@material-ui/icons/Search';
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import {appConst} from "app/appConst";
import {LightTooltip} from "../Component/Utilities";
import {
  defaultPaginationProps,
  formatTimestampToDate,
  getTheHighestRole,
  handleKeyDown,
  handleKeyUp,
  handleThrowResponseMessage,
  isSuccessfulResponse
} from "../../appFunction";
import AppContext from "../../appContext";
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator';
import CustomMaterialTable from "../CustomMaterialTable";

function MaterialButton(props) {
  const {t} = useTranslation();
  const {isRoleOrgAdmin, isRoleAssetManager} = getTheHighestRole();
  const item = props.item;
  const isEdit = item?.status === appConst.STATUS_BIDDING.OPEN.code;
  const isDelete = isRoleOrgAdmin || isRoleAssetManager;
  return <div className="none_wrap" onClick={props?.onClick}>
    {isEdit && <LightTooltip title={t('general.editIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.edit)}>
        <Icon fontSize="small" color="primary">edit</Icon>
      </IconButton>
    </LightTooltip>}
    {
      isDelete && (
        <LightTooltip title={t('general.deleteIcon')} placement="right-end" enterDelay={300} leaveDelay={200}>
          <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.delete)}>
            <Icon fontSize="small" color="error">delete</Icon>
          </IconButton>
        </LightTooltip>
      )}
    <LightTooltip title={t('general.viewDetail')} placement="right-end" enterDelay={300} leaveDelay={200}>
      <IconButton size="small" onClick={() => props.onSelect(item, appConst.active.view)}>
        <Icon fontSize="small" color="primary">
          visibility
        </Icon>
      </IconButton>
    </LightTooltip>
  </div>;
}

class BiddingList extends React.Component {
  state = {
    rowsPerPage: appConst.rowsPerPage.category,
    page: 0,
    data: [],
    totalElements: 0,
    itemList: [],
    itemListSub: [],
    shouldOpenEditorDialog: false,
    shouldOpenConfirmationDialog: false,
    keyword: '',
    selectedRow: {}
  }

  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
  }

  handleTextChange(event) {
    this.setState({keyword: event.target.value});
  }

  search = async () => {
    this.setPage(0);
  }

  handleKeyDownEnterSearch = e => handleKeyDown(e, this.search);

  handleKeyUp = e => handleKeyUp(e, this.search);

  componentDidMount() {
    this.updatePageData();
  }

  updatePageData = async () => {
    let {t} = this.props;
    let {setPageLoading} = this.context;
    let searchObject = {};
    searchObject.keyword = this.state.keyword?.trim() || "";
    searchObject.pageIndex = this.state.page + 1;
    searchObject.pageSize = this.state.rowsPerPage;

    try {
      setPageLoading(true);
      let res = await searchByPage(searchObject)
      if (res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          itemList: res?.data?.data?.content || [],
          totalElements: res?.data?.data?.totalElements || 0
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  };

  setPage = page => {
    this.setState({page}, () => {
      this.updatePageData();
    })
  };

  setRowsPerPage = event => {
    this.setState({rowsPerPage: event.target.value, page: 0}, () => {
      this.updatePageData();
    })
  };

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
  };

  handleDialogClose = () => {
    this.setState({
      shouldOpenEditorDialog: false,
      shouldOpenConfirmationDialog: false,
      isView: false,
      itemListSub: [],
      selectedRow: {},
    }, () => {
      this.updatePageData();
    });
  };

  handleDelete = id => {
    this.setState({
      id,
      shouldOpenConfirmationDialog: true
    });
  };

  handleConfirmationResponse = () => {
    let {t} = this.props;
    deleteItem(this.state.id).then((res) => {
      if (isSuccessfulResponse(res?.data?.code)) {
        toast.success(t("general.success"))
      } else {
        handleThrowResponseMessage(res);
      }
      this.setPage(0);
      this.handleDialogClose()
    });
  };

  convertItem = (value) => {
    return {
      biddingPackage: value?.biddingPackage,
      biddingYear: value?.biddingYear ? new Date(value?.biddingYear, 0, 1) : null,
      decisionCode: value?.decisionCode,
      decisionDate: value?.decisionDate,
      endDate: value?.endDate,
      id: value?.id,
      note: value?.note,
      name: value?.name,
      status: this.checkStatus(value?.status)?.code,
      startDate: value?.startDate,
      products: value?.products?.map(x => {
        return {
          product: {
            id: x?.productId,
            code: x?.productCode,
            name: x?.productName,
            manufacturerName: x?.manufacturerName,
            model: x?.model,
          },
          sku: {
            id: x?.skuId,
            name: x?.skuName,
          },
          supplier: {
            id: x?.supplierId,
            name: x?.supplierName,
          },
          quantity: x?.quantity,
          manufacturerName: x?.manufacturerName,
          model: x?.model,
          productTypeName: x?.productTypeName,
          yearOfManufacture: x?.yearOfManufacture,
          productTypeCode: x?.productTypeCode,
          unitPrice: x?.unitPrice,
        }
      })
    }
  };

  handleEdit = async item => {
    if (!item?.id) {
      this.setState({
        item: {},
        shouldOpenEditorDialog: true
      })
      return;
    }
    let {setPageLoading} = this.context;
    try {
      setPageLoading(true)
      const data = await getItemById(item?.id);
      if (isSuccessfulResponse(data?.data?.code)) {
        this.setState({
          item: this.convertItem(data?.data?.data || {}),
          shouldOpenEditorDialog: true
        })
      } else {
        handleThrowResponseMessage(data);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setPageLoading(false)
    }
  };

  handleViews = async (item) => {
    if (!item?.id) {
      this.setState({
        item: {},
        shouldOpenEditorDialog: true
      })
      return;
    }
    let {setPageLoading} = this.context;
    try {
      setPageLoading(true)
      const data = await getItemById(item?.id);
      if (data?.status === appConst.CODE.SUCCESS) {
        this.setState({
          item: this.convertItem(data?.data?.data || {}),
          isView: true,
          shouldOpenEditorDialog: true
        })
      }
    } catch (error) {
      console.error(error);
    } finally {
      setPageLoading(false)
    }
  };

  handleCheckIcon = (rowData, method) => {
    if (appConst.active.edit === method) {
      this.handleEdit(rowData);
    } else if (appConst.active.delete === method) {
      this.handleDelete(rowData?.id);
    } else if (appConst.active.view === method) {
      this.handleViews(rowData);
    } else {
      alert("Call Selected Here:" + rowData.id);
    }
  };

  checkStatus = (status) => {
    switch (status) {
      case appConst.STATUS_BIDDING.OPEN.code:
        return {
          code: appConst.STATUS_BIDDING.OPEN,
          jsx: (
            <span className="status status-success">
              {appConst.STATUS_BIDDING.OPEN.name}
            </span>
          )
        };
      case appConst.STATUS_BIDDING.CLOSE.code:
        return {
          code: appConst.STATUS_BIDDING.CLOSE,
          jsx: (
            <span className="status status-error">
              {appConst.STATUS_BIDDING.CLOSE.name}
            </span>
          )
        };
      default:
        return {
          code: null,
          jsx: null
        };
    }
  };

  handleGetRowData = async (rowData) => {
    let {setPageLoading} = this.context;
    try {
      setPageLoading(true)
      const data = await getItemById(rowData?.id);
      if (data?.status === appConst.CODE.SUCCESS) {
        this.setState({
          itemListSub: this.convertItem(data?.data?.data || {})?.products,
          selectedRow: rowData,
        })
      }
    } catch (error) {
      console.error(error);
    } finally {
      setPageLoading(false)
    }
  };

  render() {
    const {t, i18n} = this.props;
    let {
      shouldOpenEditorDialog,
      shouldOpenConfirmationDialog,
      itemList,
      item,
      isView,
      itemListSub,
      selectedRow,
    } = this.state;
    let TitlePage = t("BiddingList.title");

    let columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "left",
        minWidth: 80,
        maxWidth: 100,
        cellStyle: {
          textAlign: "center"
        },
        render: rowData => <MaterialButton item={rowData}
                                           onSelect={(rowData, method) => {
                                             this.handleCheckIcon(rowData, method);
                                           }}
                                           onClick={(event) => {
                                             event.stopPropagation();
                                           }}
        />
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => this.state.page * this.state.rowsPerPage + (rowData.tableData.id + 1)
      },
      {
        title: t("BiddingList.name"),
        field: "name",
        minWidth: 200,
      },
      {
        title: t("BiddingList.code"),
        field: "decisionCode",
        minWidth: "150px",
      },
      {
        title: t("BiddingList.date"),
        field: "decisionDate",
        align: "center",
        minWidth: 120,
        render: rowData => formatTimestampToDate(rowData?.decisionDate)
      },
      {
        title: t("BiddingList.status"),
        field: "status",
        align: "center",
        minWidth: "150px",
        render: rowData => this.checkStatus(rowData?.status)?.jsx
      },
      {
        title: t("BiddingList.startDate"),
        field: "startDate",
        align: "center",
        minWidth: 120,
        render: rowData => formatTimestampToDate(rowData?.startDate)
      },
      {
        title: t("BiddingList.endDate"),
        field: "endDate",
        align: "center",
        minWidth: 120,
        render: rowData => formatTimestampToDate(rowData?.endDate)
      },
      {
        title: t("Product.note"),
        field: "note",
        align: "left",
        minWidth: 250,
      },
    ];
    let columnsSubTable = [
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1
      },
      {
        title: t("Product.code"),
        field: "product.code",
        align: "center",
        maxWidth: "100px",
      },
      {
        title: t("Product.name"),
        field: "product",
        align: "left",
        render: rowData => rowData?.product?.name
      },
      {
        title: t("general.Quantity"),
        field: "quantity",
        align: "right",
        maxWidth: "100px",
        render: rowData => {
          return (
            <ValidatorForm onSubmit={() => {
            }}>
              <TextValidator
                className='w-100'
                type='number'
                name='quantity'
                value={rowData?.quantity || ""}
                InputProps={{
                  readOnly: true,
                  inputProps: {
                    className: "text-align-right"
                  },
                }}
              />
            </ValidatorForm>
          )
        }
      },
      {
        title: t("Asset.stockKeepingUnit"),
        field: "sku",
        align: "center",
        maxWidth: "100px",
        render: rowData => rowData?.sku?.name
      },
      {
        title: t("Product.supplier"),
        field: "supplier",
        align: "left",
        render: rowData => rowData?.supplier?.name
      },
      {
        title: t("Product.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: "120px",
      },
      {
        title: t("Product.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: "120px",
      },
      {
        title: t("Product.model"),
        field: "model",
        align: "left",
        minWidth: "120px",
      },
      {
        title: t("Product.type"),
        field: "type",
        align: "left",
        minWidth: "120px",
      },
    ];
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>{TitlePage} | {t('web_site')}</title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb routeSegments={[
            {name: t("Dashboard.category"), path: "/list/bidding"},
            {name: TitlePage}]}/>
        </div>
        <Grid container spacing={2} justifyContent="space-between">
          <Grid item md={3} sm={3} xs={12}>
            <Button
              className="mr-16 mt-12"
              variant="contained"
              color="primary"
              onClick={() => this.handleEdit({})}
            >
              {t('general.add')}
            </Button>
          </Grid>
          <Grid item md={6} sm={6} xs={12}>
            <FormControl fullWidth>
              <Input
                className='search_box mt-16'
                onChange={this.handleTextChange}
                onKeyDown={this.handleKeyDownEnterSearch}
                onKeyUp={this.handleKeyUp}
                placeholder={t("BiddingList.search")}
                startAdornment={
                  <InputAdornment position="end">
                    <Link to="#">
                      <SearchIcon
                        onClick={this.search}
                        style={{
                          position: "absolute",
                          top: "0",
                          right: "0"
                        }}/>
                    </Link>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <div>
              {shouldOpenEditorDialog && (
                <BiddingListDialog
                  t={t}
                  i18n={i18n}
                  handleClose={this.handleDialogClose}
                  open={shouldOpenEditorDialog}
                  item={item}
                  isView={isView}
                />
              )}
              {shouldOpenConfirmationDialog && (
                <ConfirmationDialog
                  title={t("general.confirm")}
                  open={shouldOpenConfirmationDialog}
                  onConfirmDialogClose={this.handleDialogClose}
                  onYesClick={this.handleConfirmationResponse}
                  text={t('general.deleteConfirm')}
                  agree={t('general.agree')}
                  cancel={t('general.cancel')}
                />
              )}
            </div>
            <CustomMaterialTable
              data={itemList}
              columns={columns}
              selectedItem={selectedRow}
              onRowClick={(rows, rowData) => {
                this.handleGetRowData(rowData);
              }}
              options={{
                maxBodyHeight: "450px",
                minBodyHeight: "450px",
              }}
            />
            <TablePagination
              {...defaultPaginationProps()}
              rowsPerPageOptions={appConst.rowsPerPageOptions.table}
              count={this.state.totalElements}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.setRowsPerPage}
            />
          </Grid>
          <Grid item xs={12}>
            <MaterialTable
              data={itemListSub}
              columns={columnsSubTable}
              options={{
                selection: false,
                paging: true,
                search: false,
                sorting: false,
                toolbar: false,
                rowStyle: (rowData) => ({
                  backgroundColor:
                    rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                }),
                padding: "dense",
                maxBodyHeight: "350px",
                minBodyHeight: "253px",
                pageSizeOptions: appConst.rowsPerPageOptions.popup,
              }}
              components={{
                Toolbar: (props) => <MTableToolbar {...props} />,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: `${t(
                    "general.emptyDataMessageTable"
                  )}`,
                },
                pagination: appConst.localizationVi.pagination
              }}
            />
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default BiddingList;
BiddingList.contextType = AppContext;
