import {
  Grid,
  DialogActions,
  Button,
  Dialog,
  Icon,
  IconButton,
} from '@material-ui/core';
import React from 'react';
import { createBilding, getReceivedProducts, updateBilding } from './BiddingListService';
import { TextValidator } from 'react-material-ui-form-validator';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {
  convertFromToDate, convertMoney, convertObjectToStringByFields,
  filterOptions,
  getOptionSelected,
  getTheHighestRole,
  handleKeyDownFloatOnly,
  handleThrowResponseMessage,
  isSuccessfulResponse,
} from 'app/appFunction';
import {appConst, DEFAULT_TOOLTIPS_PROPS, STATUS_SUPPLIER, variable} from 'app/appConst';
import { toast } from 'react-toastify';
import ValidatedDatePicker from '../Component/ValidatePicker/ValidatePicker';
import AsynchronousAutocompleteSub from '../utilities/AsynchronousAutocompleteSub';
import {LightTooltip, NumberFormatCustom, PaperComponent} from "../Component/Utilities";
import PropTypes from "prop-types";
import AppContext from "../../appContext";
import CustomValidatorForm from '../Component/ValidatorForm/CustomValidatorForm';
import { Autocomplete } from '@material-ui/lab';
import MaterialTable, { MTableToolbar } from 'material-table';
import {getItemById, getListSkuByProduct, getListsProduct, getNewCode} from '../Product/ProductService';
import { searchByText } from "../Supplier/SupplierService";
import ProductDialog from "../Product/ProductDialog";
import SupplierDialog from "../Supplier/SupplierDialog";


class BiddingListDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    decisionCode: "",
    decisionDate: "",
    status: "",
    biddingPackage: "",
    biddingYear: "",
    startDate: "",
    endDate: "",
    note: "",
    name: "",
    orgId: "",
    products: [],
    listProducts: [],
    listSkus: [],
    listSupply: [],
    shouldOpenDialogProduct: false,
    shouldOpenDialogSupplier: false,
    itemProductNew: {},
    indexRowAddNew: null,
    selectedSupplier: null,
    query: {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
    }
  };

  handleChange = (value, name) => {
    if (name === "selectedSupplier") {
      this.setState({ [name]: value }, () => {
        value && this.handleMapQuickSupplier(value);
      });
      return;
    }
    this.setState({ [name]: value });
  };

  convertDataSubmit = () => ({
    biddingPackage: this.state?.biddingPackage,
    biddingYear: this.state?.biddingYear ? new Date(this.state?.biddingYear)?.getFullYear() : null,
    decisionCode: this.state?.decisionCode,
    decisionDate: convertFromToDate(this.state?.decisionDate)?.fromDate,
    endDate: convertFromToDate(this.state?.endDate)?.toDate,
    id: this.state.id,
    note: this.state?.note,
    name: this.state?.name,
    status: this.state?.status?.code,
    startDate: convertFromToDate(this.state?.startDate)?.fromDate,
    products: this.state?.products?.map(x => {
      return {
        productId: x?.product?.id,
        quantity: x?.quantity,
        skuId: x?.sku?.id,
        supplierId: x?.supplier?.id,
        productTypeCode: x?.productTypeCode,
        productTypeName: x?.productTypeName,
        unitPrice: x?.unitPrice,
      }
    })
  })

  validateSubmit = () => {
    let { t } = this.props;
    let {
      products,
    } = this.state;

    if (products.length <= 0) {
      toast.warn(t("BiddingList.emtyProduct"));
      return true;
    }
    return false;
  };

  handleFormSubmit = async () => {
    let { setPageLoading } = this.context;
    if (this.validateSubmit()) return;
    setPageLoading(true);
    let { id } = this.state;
    let { t } = this.props;

    let payload = this.convertDataSubmit();
    try {
      let funApi = id ? updateBilding : createBilding;
      let data = await funApi(payload);
      if (isSuccessfulResponse(data?.data?.code)) {
        toast.success(id ? t("general.updateSuccess") : t("general.addSuccess"));
        this.props.handleClose();
      } else {
        handleThrowResponseMessage(data);
      }
    } catch (error) {
      toast.error(t("general.error"));
    } finally {
      setPageLoading(false);
    };
  };

  componentDidMount() {
    let { item } = this.props;
    let { query } = this.state;
    const { organization } = getTheHighestRole();
    this.getProductOrg(query);
    this.getListSupply();
    this.setState({ ...item, orgId: organization?.org?.id }, () => {
      if (item?.id) {
        this.getRemainingQuantity();
      }
    });
  };

  getRemainingQuantity = async () => {
    let { products } = this.state;
    products = [...this.state.products]
    try {
      let res = await getReceivedProducts(this.state.id);
      if (isSuccessfulResponse(res?.data?.code)) {
        let data = res?.data?.data || [];
        products?.forEach(item => {
          let exitsItem = data.find(x => (x?.productId === item?.product?.id) && (x?.supplierId === item?.supplier?.id));
          if (exitsItem) {
            item.usedQty = exitsItem?.quantity;
          } else {
            item.usedQty = 0;
          }
        });

        this.setState({ products })
      }

    } catch (error) {

    }
  }
  getProductOrg = async (query) => {
    try {
      let res = await getListsProduct(query)

      if (res?.data?.data?.content && isSuccessfulResponse(res?.data?.code)) {
        this.setState({ listProducts: res?.data?.data?.content || [] })
      }
    } catch (error) {
      console.error(error);
    }
  }

  getListSkus = async (id) => {
    if (!id) return;

    try {
      const data = await getListSkuByProduct(id);
      if (isSuccessfulResponse(data?.data?.code)) {
        this.setState({ listSkus: data?.data?.data || [] })
      } else {
        this.setState({ listSkus: [] })
      }
    } catch (error) {
      console.error(error);
    }
  };

  getListSupply = async (keyword = "") => {
    try {
      let searchObject = {
        keyword,
        pageIndex: 0,
        pageSize: 10,
        isActive: STATUS_SUPPLIER.HOAT_DONG.code,
        typeCodes: [appConst.TYPE_CODES.NCC_CU]
      }
      const data = await searchByText(searchObject.keyword, searchObject.pageIndex, searchObject.pageSize, searchObject);
      if (isSuccessfulResponse(data?.status)) {
        this.setState({ listSupply: data?.data?.content })
      } else {
        this.setState({ listSupply: [] })
      }
    } catch (error) {
      console.error(error);
    }
  };

  handleRowDataCellDelete = (rowData) => {
    let { products } = this.state;
    let index = products.findIndex(x => x?.tableData?.id === rowData?.tableData?.id);

    if (index >= 0) {
      products.splice(index, 1);
      this.setState({ products })
    }
  };

  handleAddProductItem = () => {
    getNewCode()
      .then((result) => {
        if (result != null && result.data && result.data.code) {
          let item = result.data;
          this.setState({
            itemProductNew: { ...item, name: this.state?.query?.keyword || "" },
            shouldOpenDialogProduct: true,
          });
        }
      })
      .catch(() => {
        toast.error(this.props.t("general.error"))
      });
  };

  selectCellProduct = (value, index, source) => {
    let { products } = this.state;

    if (value?.code === variable.listInputName.New && source === "product") {
      this.setState({ indexRowAddNew: index });
      this.handleAddProductItem();
      return;
    }

    if (value?.code === variable.listInputName.New && source === "supplier") {
      this.setState({ indexRowAddNew: index, shouldOpenDialogSupplier: true, itemProductNew: { name: this.state?.query?.keyword || "" } });
      return;
    }

    let isExistItem = products?.find(x => x?.product?.id === value?.id);
    products.map(pro => {
      if (pro?.tableData?.id === index) {
        if (source === "product") {
          if (!isExistItem || this.state.indexRowAddNew >= 0) {
            pro.product = value;
            pro.sku = {
              id: value?.unitId,
              name: value?.unitName,
            }
            pro.manufacturerName = value?.manufacturerName;
            pro.model = value?.model;
            pro.productTypeName = value?.productTypeName;
            pro.yearOfManufacture = value?.yearOfManufacture;
            pro.productTypeCode = value?.productTypeCode;
            pro.productTypeName = value?.productTypeName;
            pro.usedQty = 0;
            pro.madeIn = value?.madeIn;
          } else {
						toast.warning("Sản phẩm đã có trong danh sách");
					}
        } else {
          pro[source] = value;
        }
      }
    })

    this.setState({ products })
  }
  handleAddRow = () => {
    this.setState({ products: [{}, ...this.state?.products] })
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenDialogProduct: false,
      shouldOpenDialogSupplier: false,
      indexRowAddNew: null,
    })
  }

  selectProduct = (item) => {
    item.unitId = item?.defaultSku?.sku?.id;
    item.unitName = item?.defaultSku?.sku?.name;
    item.manufacturerName = item?.manufacturer?.name;
    item.productTypeName = item?.productType?.name;
    item.productTypeCode = item?.productType?.code;
    this.selectCellProduct(item, this.state?.indexRowAddNew, "product");

    this.handleDialogClose();
  };

  selectSupplier = (item) => {
    this.selectCellProduct(item, this.state?.indexRowAddNew, "supplier");
    this.handleDialogClose();
  };

  handleMapQuickSupplier = (value) => {
    let { products } = this.state;
    if (!products?.length) return;

    products.forEach(x => {
      x.supplier = { ...value }
    })

    this.setState({ products })
  }

  handleChangeInputSearch = async (event) => {
    let { name, value } = event.target;
    this.setState({ query: { ...this.state?.query, keyword: value } }, () => {
      if (name === "product") {
        this.getProductOrg(this.state.query);
        return;
      }
      if (name === "supplier") {
        this.getListSupply(this.state.query?.keyword);
        return;
      }
    })
  }

  handleChangeInputFocus = async (event) => {
    let { name } = event.target;
    this.setState({ query: { ...this.state?.query, keyword: "" }, listProducts: [], listSupply: [] }, () => {
      if (name === "product") {
        this.getProductOrg(this.state.query);
        return;
      }
      if (name === "supplier") {
        this.getListSupply(this.state.query?.keyword);
        return;
      }
    })
  }
  
  handleEditProductRowData = async (rowData) => {
    let {setPageLoading} = this.context;
    let {t} = this.props;
    
    try {
      setPageLoading(true);
      let res = await getItemById(rowData?.product?.id);
     
      if (isSuccessfulResponse(res?.status) && res?.data) {
        this.setState({
          itemProductNew: res?.data,
          shouldOpenDialogProduct: true,
          indexRowAddNew: rowData?.tableData?.id,
        })
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (e) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false);
    }
  }

  render() {
    const {
      t,
      i18n,
      open,
      isView,
      handleClose,
    } = this.props;

    let {
      note,
      decisionDate,
      status,
      decisionCode,
      biddingPackage,
      biddingYear,
      startDate,
      endDate,
      name,
      products,
      listProducts,
      listSkus,
      listSupply,
      shouldOpenDialogProduct,
      shouldOpenDialogSupplier,
      itemProductNew,
      selectedSupplier
    } = this.state;

    const columns = [
      {
        title: t("general.action"),
        field: "custom",
        align: "center",
        hidden: isView,
        minWidth: "80px",
        render: (rowData) =>
          <div className="none_wrap">
            {rowData?.product?.id && (
              <LightTooltip title={t("general.editIcon")} {...DEFAULT_TOOLTIPS_PROPS}>
                <IconButton size="small" onClick={() => this.handleEditProductRowData(rowData)}>
                  <Icon fontSize="small" color="primary">
                    edit
                  </Icon>
                </IconButton>
              </LightTooltip>
            )}
            <LightTooltip title={t("general.deleteIcon")}{...DEFAULT_TOOLTIPS_PROPS}>
              <IconButton size="small" onClick={() => this.handleRowDataCellDelete(rowData)}>
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
      },
      {
        title: t("general.stt"),
        field: "code",
        maxWidth: 50,
        align: "left",
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1
      },
      {
        title: t("Product.name"),
        field: "product",
        align: "left",
        minWidth: "400px",
        render: rowData => {
          return (
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              name='product'
              options={listProducts || []}
              value={rowData?.product || null}
              onChange={(e, value) => this.selectCellProduct(value, rowData?.tableData?.id, "product")}
              getOptionLabel={(option) => convertObjectToStringByFields(option, ["code", "name", "model", "manufacturerName"])}
              getOptionSelected={getOptionSelected}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                true,
                "name"
              )}
              disabled={isView}
              noOptionsText={t("general.noOption")}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  name='product'
                  value={rowData?.product?.name}
                  onFocus={this.handleChangeInputFocus}
                  onChange={this.handleChangeInputSearch}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              )}
            />
          )
        }
      },
      {
        title: t("BiddingList.quantity"),
        field: "quantity",
        align: "right",
        minWidth: 90,
        render: rowData => {
          return (
            <TextValidator
              className='w-100'
              onChange={(e) => this.selectCellProduct(e?.target?.value, rowData?.tableData?.id, "quantity")}
              type='text'
              name='quantity'
              value={rowData?.quantity || ""}
              onKeyPress={handleKeyDownFloatOnly}
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
                inputProps: {
                  className: "text-align-right"
                },
              }}
              validators={["minFloat:0.000000001", "required", `matchRegexp:${variable.regex.numberQuantityFloatValid}`]}
              errorMessages={[t("general.minNumberError"), t("general.required"), t("general.quantityError")]}
            />
          )
        }
      },
      {
        title: t("BiddingList.usedQuantity"),
        field: "usedQty",
        align: "right",
        minWidth: 90,
        render: rowData => {
          let usedQty = Number(rowData?.usedQty || 0);
          return (
            <TextValidator
              className='w-100'
              type='text'
              name='usedQty'
              value={usedQty?.toString() || ""}
              InputProps={{
                disableUnderline: true,
                inputComponent: NumberFormatCustom,
                readOnly: true,
                inputProps: {
                  className: "text-align-right"
                },
              }}
            />
          )
        }
      },
      {
        title: t("BiddingList.remainingQty"),
        field: "remainingQty",
        align: "right",
        minWidth: 90,
        render: rowData => {
          let remainingQty = Number((rowData?.quantity || 0) - (rowData?.usedQty || 0)) || 0;
          return (
            <TextValidator
              className='w-100'
              type='text'
              name='remainingQty'
              value={remainingQty?.toString() || ""}
              InputProps={{
                disableUnderline: true,
                inputComponent: NumberFormatCustom,
                readOnly: true,
                inputProps: {
                  className: "text-align-right"
                },
              }}
            />
          )
        }
      },
      {
        title: t("Asset.stockKeepingUnit"),
        field: "sku",
        align: "center",
        minWidth: "120px",
        render: rowData => {
          return (
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              name='sku'
              options={listSkus || []}
              value={rowData?.sku || null}
              onChange={(e, value) => this.selectCellProduct(value, rowData?.tableData?.id, "sku")}
              getOptionLabel={(option) => option.name}
              getOptionSelected={getOptionSelected}
              filterOptions={filterOptions}
              noOptionsText={t("general.noOption")}
              onFocus={() => this.getListSkus(rowData?.product?.id)}
              disabled={!rowData?.product?.id || isView}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  value={rowData?.sku || this.state?.query?.keyword}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              )}
            />
          )
        }
      },
      {
        title: t("Product.supplier"),
        field: "supplier",
        align: "left",
        minWidth: "200px",
        render: rowData => {
          return (
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              name='supplier'
              options={listSupply || []}
              value={rowData?.supplier || null}
              onChange={(e, value) => this.selectCellProduct(value, rowData?.tableData?.id, "supplier")}
              getOptionLabel={(option) => option.name}
              getOptionSelected={getOptionSelected}
              filterOptions={(options, params) => filterOptions(
                options,
                params,
                true,
                "name"
              )}
              noOptionsText={t("general.noOption")}
              disabled={isView}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  name='supplier'
                  onFocus={this.handleChangeInputFocus}
                  onChange={this.handleChangeInputSearch}
                  value={rowData?.supplier?.name || this.state?.query?.keyword}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              )}
            />
          )
        }
      },
      {
        title: t("BiddingList.unitPrice"),
        field: "quantity",
        align: "right",
        minWidth: 130,
        render: rowData => {
          return (
            <TextValidator
              fullWidth
              onChange={(e) => this.selectCellProduct(
                e?.target?.value,
                rowData?.tableData?.id,
                e?.target?.name,
              )}
              type='text'
              name='unitPrice'
              value={rowData?.unitPrice || ""}
              InputProps={{
                inputComponent: NumberFormatCustom,
                readOnly: isView,
                inputProps: {
                  className: "text-align-right"
                },
              }}
              validators={["minFloat:0.000000001"]}
              errorMessages={[t("general.minNumberError")]}
            />
          )
        }
      },
      {
        title: t("BiddingList.amount"),
        field: "quantity",
        align: "left",
        minWidth: 140,
        cellStyle: {
          textAlign: "right",
        },
        render: rowData => convertMoney((rowData.unitPrice * rowData.quantity) || 0),
      },
      {
        title: t("Product.manufacturer"),
        field: "manufacturerName",
        align: "left",
        minWidth: "200px",
      },
      {
        title: t("Asset.country_manufacturer"),
        field: "madeIn",
        align: "left",
        minWidth: "200px",
      },
      {
        title: t("Product.yearOfManufacture"),
        field: "yearOfManufacture",
        align: "left",
        minWidth: 80,
      },
      {
        title: t("Product.model"),
        field: "model",
        align: "left",
        minWidth: "200px",
      },
      {
        title: t("Product.type"),
        field: "productTypeName",
        align: "left",
        minWidth: "320px",
      },
    ]

    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'lg'} fullWidth>
        <DialogTitle className="pb-0 cursor-move" id='draggable-dialog-title'>
          {t('BiddingList.dialog')}
        </DialogTitle>
        <CustomValidatorForm ref='form' onSubmit={this.handleFormSubmit}>
          <DialogContent>
            <Grid container spacing={1}>
              <Grid item md={6} sm={6} xs={12}>
                <TextValidator
                  className='w-100'
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('BiddingList.name')}
                    </span>
                  }
                  onChange={(e) => this.handleChange(e.target?.value, "name")}
                  type='text'
                  name='name'
                  value={name || ""}
                  InputProps={{
                    readOnly: isView,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <TextValidator
                  className='w-100'
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('BiddingList.code')}
                    </span>
                  }
                  onChange={(e) => this.handleChange(e.target?.value, "decisionCode")}
                  type='text'
                  name='decisionCode'
                  value={decisionCode || ""}
                  InputProps={{
                    readOnly: isView,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatedDatePicker
                  margin='none'
                  fullWidth
                  id='date-picker-dialog'
                  label={t('BiddingList.date')}
                  format='dd/MM/yyyy'
                  value={decisionDate || null}
                  readOnly={isView}
                  InputProps={{
                    readOnly: isView
                  }}
                  onChange={decisionDate => {
                    this.handleChange(decisionDate, 'decisionDate');
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  maxDateMessage={t("allocation_asset.maxDateMessage")}
                  invalidDateMessage={t('general.invalidDateFormat')}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <AsynchronousAutocompleteSub
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('BiddingList.status')}
                    </span>
                  }
                  searchFunction={() => { }}
                  listData={appConst.LIST_STATUS_BIDDING}
                  onSelect={(value) => this.handleChange(value, "status")}
                  readOnly={isView}
                  displayLable='name'
                  value={status || null}
                  filterOptions={filterOptions}
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
								<TextValidator
									className='w-100'
									label={
										<span>
                      <span className="colorRed">* </span>
											{t('BiddingList.packageItem')}
                    </span>
									}
									onChange={(e) => this.handleChange(e.target?.value, "biddingPackage")}
									type='text'
									name='biddingPackage'
									value={biddingPackage || ""}
									InputProps={{
										readOnly: isView,
									}}
									validators={['required']}
									errorMessages={[t('general.required')]}
								/>
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatedDatePicker
                  margin='none'
                  fullWidth
                  id='date-picker-dialog'
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('BiddingList.year')}
                    </span>
                  }
                  views={["year"]}
                  format='yyyy'
                  value={biddingYear || null}
                  readOnly={isView}
                  InputProps={{
                    readOnly: isView
                  }}
                  onChange={biddingYear => {
                    this.handleChange(biddingYear, 'biddingYear');
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  minDateMessage={t("general.minDateDefault")}
                  maxDateMessage={t("general.maxDateDefault")}
                  invalidDateMessage={t('general.invalidDateFormat')}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatedDatePicker
                  margin='none'
                  fullWidth
                  id='date-picker-dialog'
                  label={
                    <span>
                      <span className="colorRed">* </span>
                      {t('BiddingList.startDate')}
                    </span>
                  }
                  format='dd/MM/yyyy'
                  value={startDate || null}
                  readOnly={isView}
                  InputProps={{
                    readOnly: isView
                  }}
                  onChange={startDate => {
                    this.handleChange(startDate, 'startDate');
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  maxDate={endDate || undefined}
                  minDateMessage={t("general.minDateDefault")}
                  maxDateMessage={endDate ? t("BiddingList.maxDateStart") : t("general.maxDateDefault")}
                  invalidDateMessage={t('general.invalidDateFormat')}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                  validators={['required']}
                  errorMessages={[t('general.required')]}
                />
              </Grid>
              <Grid item md={3} sm={6} xs={12}>
                <ValidatedDatePicker
                  margin='none'
                  fullWidth
                  id='date-picker-dialog'
                  label={
                    <span>
                      {t('BiddingList.endDate')}
                    </span>
                  }
                  format='dd/MM/yyyy'
                  value={endDate || null}
                  readOnly={isView}
                  InputProps={{
                    readOnly: isView
                  }}
                  onChange={endDate => {
                    this.handleChange(endDate, 'endDate');
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  minDate={startDate || undefined}
                  minDateMessage={startDate ? t("BiddingList.minDateEnd") : t("general.minDateDefault")}
                  maxDateMessage={t("general.maxDateDefault")}
                  invalidDateMessage={t('general.invalidDateFormat')}
                  clearable
                  clearLabel={t("general.remove")}
                  cancelLabel={t("general.cancel")}
                  okLabel={t("general.select")}
                />
              </Grid>
              <Grid item md={9} sm={6} xs={12}>
                <TextValidator
                  className='w-100'
                  label={
                    <span>
                      {t('BiddingList.note')}
                    </span>
                  }
                  onChange={(e) => this.handleChange(e.target?.value, "note")}
                  type='text'
                  name='note'
                  value={note || ""}
                  InputProps={{
                    readOnly: isView,
                  }}
                />
              </Grid>
              <Grid item md={8} sm={6} xs={12}>
                {!isView && <Button
                  variant='contained'
                  color='primary'
                  onClick={this.handleAddRow}
                > <b>+</b> </Button>}
              </Grid>
              <Grid item md={4} sm={6} xs={12}>
                {!isView && <Autocomplete
                  fullWidth
                  size="small"
                  name='supplier'
                  options={listSupply || []}
                  value={selectedSupplier || null}
                  onChange={(e, value) => this.handleChange(value, "selectedSupplier")}
                  getOptionLabel={(option) => option.name}
                  getOptionSelected={getOptionSelected}
                  filterOptions={filterOptions}
                  noOptionsText={t("general.noOption")}
                  disabled={isView}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      style={{ marginTop: 10 }}
                      variant="standard"
                      value={selectedSupplier?.name}
                      placeholder={t('Product.quicklySelectSupplier')}
                      name='supplier'
                      onFocus={this.handleChangeInputFocus}
                      onChange={this.handleChangeInputSearch}
                    />
                  )}
                />}
              </Grid>
              <Grid item xs={12}>
                <MaterialTable
                  data={products || []}
                  columns={columns}
                  options={{
                    selection: false,
                    paging: true,
                    search: false,
                    sorting: false,
                    toolbar: false,
                    rowStyle: (rowData) => ({
                      backgroundColor:
                        rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                    }),
                    padding: "dense",
                    maxBodyHeight: "350px",
                    pageSizeOptions: appConst.rowsPerPageOptions.popup,
                  }}
                  components={{
                    Toolbar: (props) => <MTableToolbar {...props} />,
                  }}
                  onSelectionChange={(rows) => {
                    this.data = rows;
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: `${t(
                        "general.emptyDataMessageTable"
                      )}`,
                    },
                    pagination: appConst.localizationVi.pagination
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className='flex flex-space-between flex-middle mt-12'>
              <Button
                className='mr-12'
                variant='contained'
                color='secondary'
                onClick={handleClose}
              >
                {t('general.close')}
              </Button>
              {!isView && (
                <Button
                  variant='contained'
                  className="mr-15"
                  color='primary'
                  type='submit'
                >
                  {
                    t('general.save')
                  }
                </Button>
              )}
            </div>
          </DialogActions>
        </CustomValidatorForm>
        {shouldOpenDialogProduct && (
          <ProductDialog
            t={t}
            i18n={i18n}
            handleClose={this.handleDialogClose}
            handleOKEditClose={this.handleDialogClose}
            open={shouldOpenDialogProduct}
            selectProduct={this.selectProduct}
            item={itemProductNew}
          />
        )}
        {shouldOpenDialogSupplier && (
          <SupplierDialog
            t={t}
            i18n={i18n}
            item={itemProductNew}
            typeCodes={[appConst.TYPE_CODES.NCC_CU]}
            open={shouldOpenDialogSupplier}
            handleClose={this.handleDialogClose}
            handleSelect={this.selectSupplier}
          />
        )}
      </Dialog>
    );
  }
}

BiddingListDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  isVisibleType: PropTypes.bool,
  handleClose: PropTypes.func,
}
BiddingListDialog.contextType = AppContext;
export default BiddingListDialog;
