import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";
const Contract = EgretLoadable({
  loader: () => import("./BiddingList"),
});
const ViewComponent = withTranslation()(Contract);

const BiddingListRoutes = [
  {
    path: ConstantList.ROOT_PATH + "list/bidding",
    exact: true,
    component: ViewComponent,
  },
];

export default BiddingListRoutes;
