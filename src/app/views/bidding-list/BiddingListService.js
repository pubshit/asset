import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH_MAINTANE = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/bids";

export const searchByPage = (searchObject) => {
  let url = API_PATH_MAINTANE + "/search-by-page";
  let config = {
    params: { ...searchObject },
  };
  return axios(url, config);
};

export const getItemById = (id) => {
  var url = API_PATH_MAINTANE + "/" + id;
  return axios(url);
};

export const deleteItem = (id) => {
  var url = API_PATH_MAINTANE + "/" + id;
  return axios.delete(url);
};

export const createBilding = (payload) => {
  var url = API_PATH_MAINTANE;
  return axios.post(url, payload);
};

export const updateBilding = (payload) => {
  var url = API_PATH_MAINTANE + "/" + payload?.id;
  return axios.put(url, payload);
};

export const getReceivedProducts = (id) => {
  var url = API_PATH_MAINTANE + "/" + id + "/received-products";
  return axios(url);
};
