import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const AssetAllocationTable = EgretLoadable({
  loader: () => import("./CalibrationAssetsTable")
});
const ViewComponent = withTranslation()(AssetAllocationTable);

const CalibrationAssetsRouters = [
  {
    path: ConstantList.ROOT_PATH + "calibration/planning",
    exact: true,
    component: ViewComponent
  }
];

export default CalibrationAssetsRouters;
