import { Button, Card, Dialog, DialogActions, Divider, Grid, Icon, IconButton } from "@material-ui/core";
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import GetAppSharpIcon from '@material-ui/icons/GetAppSharp';
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import { EgretProgressBar } from 'egret';
import FileSaver from 'file-saver';
import { Component } from "react";
import Draggable from 'react-draggable';
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { uploadFile } from "../Asset/AssetService";
import { downLoadAttachment } from "../MaintainResquest/MaintainRequestService";
import { createAttachment, updateAttachment } from "./CalibrationAssetsService";
import ImportExcelDialog from "./ImportExcelDialog";
toast.configure();

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
class AssetFilePopup extends Component {
  state = {
    name: "",
    code: this.props.item?.code || "",
    note: "",
    shouldOpenImportExcelDialog: false,
    shouldOpenNotificationPopup: false,
    dragClass: "",
    attachments: [],
    files: [],
    statusList: [],
    queProgress: 0,
    progress: 0,
    assetId: null,
    documentType: this.props.documentType,
    fileDescriptionIds: [],
    maintainRequestId: null,
    voucherId: null,
    idVoucherFile: null,
    isModifyFile: false,
    dataSubmit: {},
    file: {},
    fileId: {}
  };

  convertDto = (state) => {
    const { item, assetId } = this.props
    return {
      // name: state?.name || item?.name,
      // code: state?.code || item?.code,
      // note: state?.note || item?.note,
      // assetId: state?.assetId || assetId,
      // documentType: state?.documentType,
      // fileDescriptionIds: state?.fileDescriptionIds,
      // maintainRequestId: state?.maintainRequestId,
      // voucherId: state?.voucherId

    };
  }

  handleChange = (event, field) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleFormSubmit = (e) => {
    e.preventDefault()
    let { setPageLoading } = this.context;
    const { item, isTSCD = false, t } = this.props
    const { idVoucherFile, dataSubmit } = this.state
    if (this.state?.files.length <= 0) {
      toast.warning("Chưa có hoặc chưa tải tập tin");
      return;
    }
    if (this.state?.files.length <= 0) {
      toast.warning("Chưa có hoặc chưa tải tập tin");
      return;
    }
    setPageLoading(true);
    let dataState = this.convertDto(this.state)
    if (idVoucherFile) {
      updateAttachment({ fileId: this.state?.fileId, file: this.state?.file, name: this.state?.name, note: this.state?.note, id: this.state?.id, khId: this.props?.item?.id })
        .then(({ data }) => {
          toast.success(t("general.updateSuccess"))
          this.props.handleUpdateAssetDocument({ ...data?.data, fileId: data?.data?.file?.id })
          setPageLoading(false);
          this.props.handleClose();
        })
        .catch(() => {
          setPageLoading(false);
        })
    } else {
      createAttachment(dataSubmit)
        .then(({ data }) => {
          toast.success(t("general.addSuccess"))
          this.props.getAssetDocument({ ...data?.data, fileId: data?.data?.file?.id })
          setPageLoading(false);
          this.props.handleClose();
        })
        .catch(() => {
          setPageLoading(false);
        })
    }
  };

  componentWillMount() {
    let { item, itemAssetDocument } = this.props;
    let { isEditAssetDocument, attachments = [] } = item?.item;
    let files = [];
    // eslint-disable-next-line no-unused-expressions
    // attachments?.forEach(element => {
    //   let rest = Object.assign(element, { 'isEditAssetDocument': isEditAssetDocument });
    //   files.push(rest);
    // });

    const fileDescriptionIds = attachments.map(attachment => attachment?.file?.id);
    this.setState({
      code: item?.item?.code,
      ...attachments,
      ...itemAssetDocument,
      files: itemAssetDocument?.file ? [itemAssetDocument?.file] : [],
      fileDescriptionIds,
      idVoucherFile: item?.item?.id,
      name: item?.item?.name,
      note: item?.item?.note,
      isEditAssetDocument: item?.item?.id
    });
  }

  componentDidMount() {
  }

  handleDialogClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
  }

  handleOKEditClose = () => {
    this.setState({
      shouldOpenNotificationPopup: false,
      shouldOpenImportExcelDialog: false
    })
    this.updatePageData()
  }

  handleFileUploadOnSelect = event => {
    let files = event.target.files;
    this.fileUpload(files[0]).then(() => {
      toast.success("Tải tập tin thành công");
    });
  }

  handleFileSelect = event => {
    let files = event.target.files;
    let list = [];

    for (const iterator of files) {
      let MaxSizeFile = 2097152;

      if (iterator?.size >= MaxSizeFile) {
        toast.warn("Kích thước của file không được vượt quá 2MB")
        return;
      }

      list.push({
        file: iterator,
        uploading: false,
        error: false,
        progress: 0,
        isModifyFile: true
      });
    }

    this.setState({
      files: [...list],
    }, () => this.uploadSingleFile(this.state.files?.length - 1));
  };

  handleSingleRemove = index => {
    let files = [...this.state.files];
    let attachments = [...this.state.attachments];
    let fileDescriptionIds = [...this.state.fileDescriptionIds];

    if (attachments?.length === 1) {
      this.setState({
        files: [],
        attachments: [],
        fileDescriptionIds: []
      });
    } else {
      files.splice(index, 1);
      attachments.splice(index, 1);
      fileDescriptionIds.splice(index, 1);
      this.setState({
        files: [...files],
        attachments: [...attachments],
        fileDescriptionIds: [...fileDescriptionIds]
      });
    }


  };

  fileUpload = (file) => {
    let { setPageLoading } = this.context;
    let formData = new FormData();
    formData.append('uploadfile', file);//Lưu ý tên 'uploadfile' phải trùng với tham số bên Server side
    setPageLoading(true);

    return uploadFile(formData)
      .then(({ data }) => {
        setPageLoading(false)
        let attachment = this.props.isIAT ? data?.data : data?.data;
        let { attachments, fileDescriptionIds, dataSubmit } = this.state;
        attachments.push(attachment);
        this.setState({ dataSubmit: { file: attachment, name: this.state?.name, note: this.state?.note, khId: this.props?.item?.id, fileId: attachment?.id } })
        fileDescriptionIds.push(
          this.props.isIAT
            ? attachment?.id
            : attachment?.id
        );
        this.setState({ attachments, fileDescriptionIds });
        toast.success("Tải tập tin thành công");

      })
      .catch(() => {
        toast.error("Lỗi tải file");
        setPageLoading(false)
      })
  }

  uploadSingleFile = async (index) => {
    let allFiles = [...this.state.files];
    let file = this.state.files[index];

    await this.fileUpload(file.file);

    allFiles[index] = { ...file, uploading: true, success: true, error: false };

    this.setState({
      files: [...allFiles]
    });
  };

  handleViewDocument = async (item) => {
    const { setPageLoading } = this.context
    const { t } = this.props;
    const {TYPES_FILE} = appConst;
    let fileName = item?.name;

    try {
      const result = await downLoadAttachment(item?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = new Blob([document], { type: item?.contentType });
        if (
          file.type === TYPES_FILE.PDF
          || file.type === TYPES_FILE.TEXT
          ||  TYPES_FILE.IMAGE_ARRAY.includes(file.type)
        ) {
          let fileURL = URL.createObjectURL(file, fileName);
          window.open(fileURL,  '_blank');
          URL.revokeObjectURL(fileURL);
        } else {
          toast.warning(t("general.viewAttachmentError"))
        }
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    };
  }

  handleDownloadDocument = async item => {
    const { setPageLoading } = this.context
    const { t } = this.props
    let contentType = item?.contentType;
    let fileName = item?.name;
    try {
      setPageLoading(true)
      const result = await downLoadAttachment(item?.id)
      if (result?.status === appConst.CODE.SUCCESS) {
        let document = result?.data;
        let file = new Blob([document], { type: contentType });
        return FileSaver.saveAs(file, fileName);
      }
    } catch (error) {
      toast.error(t("general.error"))
    } finally {
      setPageLoading(false)
    };
  }

  render() {
    let { open, t, i18n, item, } = this.props;
    let { files } = this.state;
    let isEmpty = files.length === 0;
    let {
      name,
      shouldOpenImportExcelDialog,
      isEditAssetDocument,
      note
    } = this.state;
    return (
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth={'md'} fullWidth>
        <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
          {t('general.saveUpdate')}
        </DialogTitle>
        <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
          {shouldOpenImportExcelDialog && (
            <ImportExcelDialog
              t={t}
              i18n={i18n}
              handleClose={this.handleDialogClose}
              open={shouldOpenImportExcelDialog}
              handleOKEditClose={this.handleOKEditClose}
            />
          )}
          <DialogContent style={{ minHeight: '420px', maxHeight: '420px' }}>
            <Grid className="" container spacing={1}>
              <Grid item md={3} sm={12} xs={12} hidden={true}>
                {/* Mã hồ sơ tài sản */}
                <div className="mt-24"><label
                  style={{ fontWeight: 'bold' }}>{t('AssetFile.code')} : </label> {this.state.code || item?.code}</div>
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={
                    <span>
                      <span className="colorRed">*</span>
                      {t('AssetFile.name')}
                    </span>
                  }
                  InputProps={{
                    readOnly: item?.isViewAssetFile,
                  }}
                  onChange={(e) => this.handleChange(e, "name")}
                  type="text"
                  name="name"
                  value={name || item?.item?.name}
                  validators={["required"]}
                  errorMessages={[t('general.required')]}
                />
              </Grid>

              <Grid item md={12} sm={12} xs={12}>
                <TextValidator
                  className="w-100"
                  label={t('AssetFile.description')}
                  onChange={(e) => this.handleChange(e, "note")}
                  type="text"
                  name="note"
                  value={note || item?.item?.note}
                  InputProps={{
                    readOnly: item?.isViewAssetFile,
                  }}
                />
              </Grid>

            </Grid>
            <div className="mt-12">
              {(!item?.isViewAssetFile) &&
                <div className="flex flex-wrap mb-12">
                  <label htmlFor="upload-single-file">
                    <Button
                      size="small"
                      className="capitalize"
                      component="span"
                      variant="contained"
                      color="primary"
                      disabled={this.props?.item?.item?.id || files?.length}
                    >
                      <div className="flex flex-middle">
                        <span>{t('general.select_file')}</span>
                      </div>
                    </Button>
                  </label>
                  {!this.props?.item?.item?.id && <input
                    className="display-none"
                    onChange={this.handleFileSelect}
                    id="upload-single-file"
                    type="file"
                  />}
                  <div className="px-16"></div>
                </div>
              }
              <Card className="mb-24" elevation={2}>
                <div className="p-16">
                  <Grid
                    container
                    spacing={2}
                    justifyContent="center"
                    alignItems="center"
                    direction="row"
                  >
                    <Grid item lg={4} md={4}>
                      {t('general.file_name')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.size')}
                    </Grid>
                    <Grid item lg={4} md={4}>
                      {t('general.action')}
                    </Grid>
                  </Grid>
                </div>
                <Divider></Divider>

                {isEmpty && <p className="px-16 center">{t('general.empty_file')}</p>}

                {(files.length !== 0 ? files : [])?.map((item, index) => {
                  // let { file, success, error, progress, isModifyFile } = item;
                  return (
                    <div className="px-16 py-8" key={item?.file?.name}>
                      <Grid
                        container
                        spacing={2}
                        justifyContent="center"
                        alignItems="center"
                        direction="row"
                      >
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          {item?.name || item?.file?.name}
                        </Grid>
                        {(item?.id) ? (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {((item?.contentSize || item?.file?.contentSize || this.state?.contentSize) / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        ) : (
                          <Grid item lg={1} md={1} sm={12} xs={12}>
                            {((item?.size || item?.file?.size || this.state?.size) / 1024 / 1024).toFixed(1)} MB
                          </Grid>
                        )}
                        {(isEditAssetDocument || item?.success) ? (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={100}></EgretProgressBar>
                          </Grid>
                        ) : (
                          <Grid item lg={2} md={2} sm={12} xs={12}>
                            <EgretProgressBar value={item?.progress}></EgretProgressBar>
                          </Grid>
                        )}
                        <Grid item lg={1} md={1} sm={12} xs={12}>
                          {item?.error && <Icon fontSize="small" color="error">error</Icon>}
                          {/* {uploading && <Icon className="text-green">done</Icon>} */}
                        </Grid>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                          <div className="flex">
                            {(!isEditAssetDocument || item?.isModifyFile) && (
                              <IconButton disabled={item?.success} size="small" title={t('general.upload')}
                                onClick={() => this.uploadSingleFile(index)}>
                                <Icon color={item?.success ? "disabled" : "primary"} fontSize="small">cloud_upload</Icon>
                              </IconButton>
                            )}
                            {(isEditAssetDocument && !item?.isModifyFile) && (<IconButton size="small" title={t('general.viewDocument')}
                              onClick={() => this.handleViewDocument(item)}>
                              <Icon fontSize="small" color="primary">visibility</Icon>
                            </IconButton>
                            )}

                            {(isEditAssetDocument && !item?.isModifyFile) && (<IconButton size="small" title={t('general.downloadDocument')}
                              onClick={() => this.handleDownloadDocument(item)}>
                              <Icon fontSize="small" color="default"><GetAppSharpIcon /></Icon>
                            </IconButton>
                            )}

                            {
                              !this.props.item?.isViewAssetFile &&
                              <IconButton size="small" title={t('general.removeDocument')}
                                onClick={() => this.handleSingleRemove(index)}>
                                <Icon fontSize="small" color="error">delete</Icon>
                              </IconButton>
                            }

                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  );
                })}
              </Card>
            </div>

          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                className="mr-12"
                color="secondary"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              {
                !item?.isViewAssetFile &&
                <Button
                  className="mr-16"
                  variant="contained"
                  color="primary"
                  // type="submit"
                  onClick={this.handleFormSubmit}
                >
                  {t('general.save')}
                </Button>
              }
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

AssetFilePopup.contextType = AppContext;
export default AssetFilePopup;
