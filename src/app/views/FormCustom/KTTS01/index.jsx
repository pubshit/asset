import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import localStorageService from "app/services/localStorageService";
import Title from "../Component/Title";
import { appConst } from "../../../appConst";
import NationalName from "../Component/NationalName";
import { componentDidMount } from "react-google-maps/lib/utils/MapChildHelper";
import { getDetailArSuCo } from "app/views/MaintainResquest/MaintainRequestService";
import { toast } from "react-toastify";
import moment from "moment";
import Signature from "../Component/Signature";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textIndent: {
    textAlign: "left",
    textIndent: "2em",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
    fontSize: "14px",
  },
  w_2: {
    border: "1px solid",
    width: "2%",
    textAlign: "center",
  },
  w_20: {
    border: "1px solid",
    width: "20%",
    textAlign: "center",
  },
  w_30: {
    border: "1px solid",
    width: "30%",
    textAlign: "center",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_15: {
    border: "1px solid",
    width: "15%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
    textAlign: "center",
  },
  mt_10: {
    marginTop: 10,
  },
  mb_10: {
    marginBottom: 10,
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "left",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 10%",
  },
  border: {
    border: "1px solid",
    textAlign: "center",
    padding: 8,
  },
  flex_w_20: {
    display: "flex",
    justifyContent: "space-around",
    width: "20%",
  },
  flexColumn: {
    flexDirection: "column",
  },
  alignItemsFlexStart: {
    alignItems: "flex-start",
  },
  flex: {
    display: "flex",
  },
  fontItalic: {
    fontStyle: "italic",
  },
};
export default class KTTS01 extends Component {
  state = {
    detailArSuCo: null,
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    let { itemEdit } = this.props;
    this.setState({
      detailArSuCo: itemEdit,
    });
  }
  componentDidMount() { }

  renderDate(dateData) {
    let date = new Date(dateData);
    let day = moment(date).format("D").toString().padStart(2, "0");
    let month = (date.getMonth() + 1).toString().padStart(2, "0");
    let year = date.getFullYear();
    const dateResultVer1 = `ngày ${day} tháng ${month} năm ${year}`;
    const dateResultVer2 = moment(date).format("DD/MM/YYYY");
    return {
      dateResultVer1,
      dateResultVer2,
    };
  }
  render() {
    let { open, t } = this.props;
    let { detailArSuCo } = this.state;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">
            {t("Biên bản xử lý vật tư thay thế, sửa chữa")}
          </span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
          title="ifmcontentstoprint"
        ></iframe>

        <ValidatorForm
          ref="form"
          class="validator-form-scroll-dialog"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className="dialog-print">
            <Grid>
              <div
                style={style.textAlignCenter}
                className="form-print form-print-landscape"
              >
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <NationalName />
                </div>
                <Title title={"biên bản xử lý vật tư thay thế, sửa chữa"} />
                <div style={style.textIndent}>
                  <p>
                    Hôm nay,{" "}
                    {this.renderDate(detailArSuCo?.arNtThoiGian).dateResultVer1},
                    chúng tôi gồm:
                  </p>
                </div>
                <div>
                  {detailArSuCo?.arUsers.map((i) => {
                    return (
                      <div
                        style={{
                          ...style.textAlignLeft,
                          display: "flex",
                        }}
                      >
                        <div style={{ ...style, width: "50%" }}>
                          - Ông/Bà:&nbsp;
                          {i?.userName ? i?.userName : "..........."}
                        </div>
                        <div style={{ ...style, width: "50%" }}>
                          &nbsp;- Cán bộ {i?.userPhongBanText}
                        </div>
                      </div>
                    );
                  })}
                </div>
                <div style={style.textIndent}>
                  <p>
                    Cùng tiến hành đánh giá, xử lý vật tư thay thế, sửa chữa
                    theo biên bản nghiệm thu số {detailArSuCo?.arMa} ngày{" "}
                    {this.renderDate(detailArSuCo?.arNtThoiGian).dateResultVer2}{" "}
                    như sau:
                  </p>
                </div>
                <div style={{ textAlign: "center" }}>
                  <div>
                    {
                      <table style={style.table} className="table-report">
                        <tr>
                          <th style={style.w_5} rowSpan="2">
                            STT
                          </th>
                          <th style={style.w_15} rowSpan="2">
                            Tên thiết bị
                          </th>
                          <th style={style.w_2} colSpan="2">
                            Loại thiết bị
                          </th>
                          <th style={style.w_15} rowSpan="2">
                            Vật tư thay thế sửa chữa
                          </th>
                          <th style={style.w_5} rowSpan="2">
                            Số lượng
                          </th>
                          <th style={style.w_10} rowSpan="2">
                            Mô tả hiện trạng hư hỏng
                          </th>
                          <th style={style.w_20} rowSpan="2">
                            Yêu cầu xử lý
                          </th>
                        </tr>
                        <tr>
                          <th style={style.border}>TSCD</th>
                          <th style={style.border}>CCDC</th>
                        </tr>
                        <tbody>
                          <tr>
                            <td style={style.border}>
                              1
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft
                              }}
                            >
                              {detailArSuCo.tsTen}
                            </td>
                            {/* is TSCD */}
                            <td style={style.border}>x</td>
                            {/* is CCCD */}
                            <td style={style.border}></td>
                            {/* List linh kiện */}
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft
                              }}
                            >
                              {detailArSuCo?.arLinhKienVatTus?.map(item => (
                                <div>- {item.productName}. SL: {item.productQty}</div>
                              ))}
                            </td>
                            <td style={style.border}>1</td>
                            <td style={style.border}>
                              {detailArSuCo?.arHinhThucMoTa}
                            </td>
                            <td
                              style={{
                                ...style.border,
                                ...style.textAlignLeft,
                                ...style.fontItalic,
                              }}
                            >
                              {detailArSuCo?.arDeXuatBienPhap}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    }
                  </div>
                  <div
                    style={{
                      ...style.mt_10,
                      ...style.mb_10,
                      ...style.flex,
                      ...style.flexColumn,
                      ...style.alignItemsFlexStart,
                    }}
                  >
                    <div style={style.flex_w_20}>
                      <div>- Phòng KT - TH</div> <div>:</div>
                    </div>
                    <div style={style.flex_w_20}>
                      <div>- Phòng TC - KT </div>
                      <div>:</div>
                    </div>
                    <div style={style.flex_w_20}>
                      <div>- Phòng .............. </div>
                      <div>:</div>
                    </div>
                  </div>
                  <div style={{ display: "flex", fontWeight: "bold" }}>
                    <div
                      style={{
                        width: "50%",
                        display: "flex",
                        justifyContent: "center",
                        textTransform: "uppercase",
                      }}
                    >
                      Đại diện các khoa, phòng
                    </div>
                    <div
                      style={{
                        width: "50%",
                        display: "flex",
                        justifyContent: "center",
                        textTransform: "uppercase",
                      }}
                    >
                      Đại diện phòng quản lý
                    </div>
                  </div>
                  <div style={{
                    display: "grid",
                    gridTemplateColumns: "1fr 1fr"
                  }}>
                    <Signature title={detailArSuCo?.arPhongBanText} />
                    <Signature title={detailArSuCo?.tsPhongBanText} />
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
