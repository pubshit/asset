import React from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import HeaderLeft from "../Component/HeaderLeft";
import { PaperComponent, getAssetStatus, getRoleCategory } from "app/appFunction";
import PropTypes from "prop-types"
import { appConst } from "app/appConst";
import localStorageService from "app/services/localStorageService";

const style = {
  formPrint: {
    minWidth: "900px",
  },
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlignLeft: {
    textAlign: "left",
    padding: "4px",
  },
  fontBold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  textAlignCenter: {
    textAlign: "center",
    padding: "4px",
  },
  textAlignRight: {
    textAlign: "right",
    padding: "4px",
  },
  signature: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr 1fr"
  },
  mt_20: {
    marginTop: "20px",
  },
  w_5: {
    width: "5%",
  },
  w_8: {
    width: "8%",
  },
  w_15: {
    width: "15%",
  },
  w_25: {
    width: "25%",
  },
  subTitleProps: {
    fontSize: "14pt",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  a4Container: {
    margin: "auto",
    fontSize: "18px",
    // padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
  },
};

function InventoryCountVoucherAll (props) {
  let {
    open,
    t,
    item,
    assetClass = 1,
    handleClose = () => {}
  } = props;
  const isTSCĐ = assetClass === appConst.assetClass.TSCD;

  let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  const checkAssetStatus = (row = {}) => {
    const statusIndexOrder = row?.usedCondition;
    const isGood = appConst.OBJECT_USED_CONDITION.TOT.code === statusIndexOrder;
    const isMedium = appConst.OBJECT_USED_CONDITION.TRUNG_BINH.code === statusIndexOrder;
    const isBroken = appConst.OBJECT_USED_CONDITION.HONG.code === statusIndexOrder;

    return {
      isGood: isGood ? "x" : "",
      isMedium: isMedium ? "x" : "",
      isBroken: isBroken ? "x" : "",
    }
  }
  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth="md"
      fullWidth
    >
      <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
        <span className="">{t("Phiếu kiểm kê")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        className="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className="dialog-print">
          <div className="form-print form-print-landscape" style={style.a4Container}>
            <div item xs={12}>
              <HeaderLeft unit={titleName?.org?.name || ""} />
            </div>
            <Title
              title={
                isTSCĐ
                  ? "Danh mục tài sản"
                  : "Danh mục công cụ dụng cụ"
              }
              subTitle={item?.department?.name}
              subTitleProps={style.subTitleProps}
            />
            <Title
              date={new Date(item?.inventoryCountDate)}
              timeOfInventory={"Kiểm kê"}
              isNoSpaceTop
            />

            <div style={style.mt_20}>
              <table style={style.tableCollapse}>
                <thead>
                  <tr>
                    <th
                      style={{
                        ...style.border,
                        ...style.w_5,
                      }}
                      rowSpan={2}
                    >
                      STT
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_25,
                      }}
                      rowSpan={2}
                    >
                      {
                        isTSCĐ
                          ? "Tên tài sản cố định"
                          : "Tên công cụ dụng cụ"
                      }
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_8,
                      }}
                      rowSpan={2}
                    >
                      ĐVT
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_5,
                      }}
                      rowSpan={2}
                    >
                      SL
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_8,
                      }}
                      rowSpan={2}
                    >
                      Năm đưa vào SD
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_25,
                      }}
                      colSpan={3}
                    >
                      Đánh giá tình trạng
                    </th>
                    <th 
                      style={{
                        ...style.border,
                        ...style.w_15,
                      }}
                      rowSpan={2}
                    >
                      Ghi chú
                    </th>
                  </tr>
                  <tr>
                    <th style={style.border}>Tốt</th>
                    <th style={style.border}>TB</th>
                    <th style={style.border}>Hỏng</th>
                  </tr>
                </thead>
                <tbody>
                  {item?.map((r, i) => (
                    <React.Fragment key={i}>
                    <tr><td colSpan={7} style={{ paddingLeft: 10 }}><b>{r?.department?.name}</b></td></tr>
                    {r?.assets?.map((row, index) => (
                      <tr key={index}>
                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {index + 1}
                        </td>

                        <td style={{
                          ...style.border,
                        }}>
                          {row?.asset?.name}
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {row?.asset?.unit?.name}
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {
                            isTSCĐ
                              ? 1
                              : row?.inventoryQuantity
                          }
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {
                            row?.asset?.dateOfReception
                            ? new Date(row?.asset?.dateOfReception).getFullYear()
                            : ""
                          }
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {checkAssetStatus(row).isGood}
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {checkAssetStatus(row).isMedium}
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>
                          {checkAssetStatus(row).isBroken}
                        </td>

                        <td style={{
                          ...style.textAlignCenter,
                          ...style.border,
                        }}>

                        </td>
                      </tr>
                      ))}
                    </React.Fragment>
                  ))}
                </tbody>
              </table>
            </div>

            <div 
              style={style.signature}
              className="mt-20"
            >
              <Signature
                title="Phòng TCHC"
                sign="Ký, họ tên"
              />
              <Signature
                title="kế toán"
                sign="Ký, họ tên"
              />

              <Signature
                title={"Giám đốc"}
                sign="Ký, họ tên"
              />              
              <Signature
                title="Người lập"
                sign="Ký, họ tên"
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-12"
              onClick={handleClose}
            >
              {t("general.cancel")}
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="mr-16"
              type="submit"
            >
              {t("In")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

InventoryCountVoucherAll.propTypes = {
  open: PropTypes.bool.isRequired,
  assetClass: PropTypes.number.isRequired,
  handleClose: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  t: PropTypes.node.isRequired,
}
export default InventoryCountVoucherAll;
