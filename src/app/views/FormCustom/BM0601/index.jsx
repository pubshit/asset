import React, { useEffect, useState } from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import { appConst } from "../../../appConst";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import HeaderLeft from "../Component/HeaderLeft";
import { getDetailArSuCo } from "app/views/MaintainResquest/MaintainRequestService";
import { toast } from "react-toastify";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  flexSpaceArround: {
    display: "flex",
    justifyContent: "space-around",
  },
  contentContainer: {
  },
  rowContainer: {
    paddingTop: 10,
  },
  noMarginTop: {
    marginTop: "0",
  },
  headLine: {
    marginLeft: "4%",
  },
  py_2: {
    padding: "0 2%",
  },
};

export default function BM0601(props) {
  const { itemEdit, t, open, handleClose } = props;
  const [dataArSuCo, setDataArSuCo] = useState({})
  useEffect(() => {
    handleGetDetailArSuCo(itemEdit?.id)
  }, [])
  const handleGetDetailArSuCo = async (id) => {
    try {
      const result = await getDetailArSuCo(id)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        setDataArSuCo(result?.data?.data)
      }
    } catch (error) {
      toast.error(t("general.error"));
    }
  }
  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("MaintainResquest.title")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
        title="ifmcontentstoprint"
      ></iframe>

      <ValidatorForm
        class="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className=" dialog-print">
          <div className="form-print">
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <HeaderLeft textAlign={{ textAlign: "center" }} code={dataArSuCo?.arPhongBanText} />
              <NationalName />
            </div>

            <div style={style.textAlignCenter}>
              <Title title={t("print.equipmentFailureReport")} />
            </div>

            <div style={style.contentContainer}>
              <div style={style.rowContainer}>
                <div style={{ paddingLeft: "30px" }}>
                  Khoa/Phòng:  {dataArSuCo?.arPhongBanText ? dataArSuCo?.arPhongBanText : ""}
                </div>
                <div></div>
              </div>

              <div style={{ ...style.rowContainer, paddingLeft: 30, }}>
                <b>Báo cáo thiết bị</b>
                <div>Tên tài sản: {dataArSuCo?.tsTen ? dataArSuCo?.tsTen : ""}</div>
                <div>{dataArSuCo?.tsModel ? "Model: " + dataArSuCo?.tsModel : ""}</div>
                <div>{dataArSuCo?.tsSerialNo ? "Số Seri: " + dataArSuCo?.tsSerialNo : ""}</div>
              </div>

              <div style={{ ...style.rowContainer, paddingLeft: 30, }}>
                <div>Có sự cố: </div>
                <div>{dataArSuCo?.arNoiDung}</div>
              </div>

              <div style={{ marginTop: "20px" }}>
                <span style={style.headLine}>Khoa</span>
                {dataArSuCo?.arPhongBanText
                  ? ` ${dataArSuCo?.arPhongBanText} `
                  : ".........................."}
                kính báo cáo và đề xuất Lãnh đạo bệnh viện cho kiểm tra và sửa
                chữa
              </div>
            </div>

            <div style={style.flexSpaceArround}>
              <div className="mt-30 pt-4">
                <Signature
                  title={t("print.headOfDepartmentManagement")}
                />
              </div>
              <div style={{ marginTop: 5 }}>
                <Signature
                  title={t("print.headOfDepartment")}
                  date={dataArSuCo?.arTgBaoCao}
                  isLocation={true} />
              </div>
            </div>

            <div style={style.textAlignCenter}>
              <Signature
                sign={dataArSuCo?.userRequest?.displayName || ""}
                title={t("print.hospitalDirector")}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={handleClose}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("general.print")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
