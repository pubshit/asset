import React, { Component } from "react";
import {
    Dialog,
    Button,
    Grid,
    DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { PaperComponent, convertNumberPrice, convertNumberToWords, spliceString } from "app/appFunction";
import Title from "../../FormCustom/Component/Title";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import Signature from "../../FormCustom/Component/Signature";
import moment from "moment";

const style = {
    textItalic: {
        fontStyle: "italic",
    },
    justifyContentSpaceBetween: {
        display: "flex",
        justifyContent: "space-between"
    },
    textAlignCenter: {
        textAlign: "center",
    },
    fontWeightBold: {
        fontWeight: "bold",
    },
    title: {
        fontSize: "0.975rem",
        fontWeight: "bold",
        marginBottom: "1em",
        marginTop: "1.5em",
    },
    text: {
        fontStyle: "italic",
        fontSize: "0.8rem",
        marginTop: "0px",
    },
    mb0: {
        marginBottom: 0,
    },
    textAlignLeft: {
        textAlign: "left",
    },
    textAlignRight: {
        textAlign: "right",
    },
    table: {
        width: "100%",
        border: "1px solid",
        borderCollapse: "collapse",
    },
    w_5: {
        border: "1px solid",
        width: "5%",
        textAlign: "center",
    },
    w_8: {
        border: "1px solid",
        width: "8%",
        textAlign: "center",
    },
    w_10: {
        border: "1px solid",
        width: "10%",
    },
    w_15: {
        border: "1px solid",
        width: "15%",
    },
    w_20: {
        border: "1px solid",
        width: "20%",
    },
    w_25: {
        border: "1px solid",
        width: "25%",
    },
    w_30: {
        border: "1px solid",
        width: "30%",
    },
    w_40: {
        border: "1px solid",
        width: "40%",
    },
    p_10: {
        padding: 10,
    },
    mt_20: {
        marginTop: 20,
    },
    fz_16: {
        fontSize: 16,
    },
    represent: {
        display: "flex",
        justifyContent: "space-between",
        margin: "0 10%",
    },
    border: {
        border: "1px solid",
        textAlign: "center",
        fontSize: "0.975rem",
        paddingLeft: 3,
        paddingRight: 3,
    },
    nameColumn: {
        border: "1px solid",
        textAlign: "left",
        paddingLeft: "10px",
        fontSize: "0.975rem",
        wordBreak: "break-all",
    },
    signatureContainer: {
        display: "grid",
        gridTemplateColumns: "1fr 1fr 2fr",
        // marginTop: "20px",
    },
    signatureItemHeader: {
        width: "100%",
        height: 1,
    },
    styleGrid(gridTemplateColumns) {
        return {
            display: "grid",
            gridTemplateColumns: gridTemplateColumns,
        }
    },

    a4Container: {
        margin: "auto",
        fontSize: "18px",
    },
};

export default class AssetCard extends Component {
    state = {
    };

    handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();

        pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
        pri.document.write(content.innerHTML);

        pri.document.close();
        pri.focus();
        pri.print();
    };

    componentWillMount() {
        let { item } = this.props;

        this.setState({
            ...item,
            itemList: item?.voucherDetails,
        });
    }
    componentDidMount() {
    }

    render() {
        let {
            t,
            open,
            item,
        } = this.props;
        let { itemList = [] } = this.state;
        let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
        const org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
        const orgAddress = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.ADDRESS_OF_ENTERPRISE);
        const totalAmount = itemList?.reduce((total, item) => Number(total) + Number(item.amount), 0)
        return (
            <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
                <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
                    <span>{t('Thẻ tài sản')}</span>
                </DialogTitle>
                <iframe
                    id="ifmcontentstoprint"
                    style={{
                        height: '0px',
                        width: '0px',
                        position: 'absolute',
                        print: { size: 'auto', margin: '0mm' }
                    }}
                ></iframe>

                <ValidatorForm
                    className="validator-form-scroll-dialog"
                    ref="form"
                    onSubmit={this.handleFormSubmit}
                >
                    <DialogContent id="divcontents" className=" dialog-print">
                        <Grid>
                            <div
                                style={style.a4Container}
                                className="form-print form-print-landscape"
                            >
                                <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}>
                                    <div
                                        style={{
                                            ...style.textAlignLeft,
                                            fontWeight: "bold",
                                        }}
                                    >
                                        <div>Đơn vị:&nbsp;{org?.org?.name || ".............................."}</div>
                                        <div>Địa chỉ:&nbsp;{orgAddress || "............................."}</div>
                                    </div>
                                    <div
                                        style={{
                                            ...style.textAlignCenter,
                                        }}
                                    >
                                        <div><b>Mẫu số S25-H</b></div>
                                        <div><i>(Ban hành kèm theo thông tư số 107/2017/TT/BTC ngày 10/10/2017 của Bộ Tài Chính)</i></div>
                                    </div>
                                </div>
                                <Title
                                    title={"THẺ TÀI SẢN CỐ ĐỊNH"}
                                />
                                <div style={{ ...style.textItalic, ...style.textAlignCenter }}>
                                    <div>Số:....................</div>
                                    <div>Ngày ...... tháng ....... năm ....... Lập thẻ ............</div>
                                </div>
                                <div
                                    style={{
                                        ...style.textAlignLeft,
                                        ...style.mt_20,
                                        display: "flex",
                                        flexDirection: "column",
                                        gap: 10
                                    }}
                                >
                                    <div style={{ whiteSpace: "nowrap", overflow: "hidden" }}>
                                        <div>Căn cứ vào Biên bản giao nhận TSCĐ số ............. ngày ...... tháng ...... năm {"..".repeat(100)}</div>
                                    </div>
                                    <div style={style.styleGrid("3fr 2fr")}>
                                        <span>
                                            Tên, ký mã hiệu, quy cách (cấp hạng) TSCD: {item?.name}
                                        </span>
                                        <span>
                                            Số hiệu TSCĐ: {item?.code}
                                        </span>
                                    </div>
                                    <div style={style.styleGrid("3fr 2fr")}>
                                        <span>
                                            Nước sản xuất (xây dựng): {item?.madeIn}
                                        </span>
                                        <span>Năm sản xuất: {item?.yearOfManufacture}</span>
                                    </div>
                                    <div style={style.styleGrid("3fr 2fr")}>
                                        <span>
                                            Bộ phận quản lý, sử dụng: {item?.managementDepartmentName}
                                        </span>
                                        <span>Năm đưa vào sử dụng: {item?.yearPutIntoUse}</span>
                                    </div>
                                    <div>
                                        Công suất (diện tích thiết kế): {item?.powerCapacityOrArea}
                                    </div>
                                    <div style={{ whiteSpace: "nowrap", overflow: "hidden" }}>
                                        Đình chỉ sử dụng TSCĐ ngày ........... tháng .......... năm  {"..".repeat(100)}
                                    </div>
                                    <div style={{ display: "flex", overflow: "hidden" }}>
                                        <div style={{ minWidth: "110px" }}>Lý do đình chỉ</div> <div>{"..".repeat(200)}</div>
                                    </div>
                                </div>
                                <div style={{ ...style.textAlignCenter, ...style.mt_20 }}>
                                    <div>
                                      <table style={style.table} className="table-report">
                                        <thead>
                                        <tr>
                                          <th style={style.w_15} rowSpan="2">
                                            Số hiệu chứng từ
                                          </th>
                                          <th style={style.w_40} colSpan="3">
                                            Nguyên giá tài sản cố định
                                          </th>
                                          <th style={style.w_40} colSpan="3">
                                            Giá trị hao mòn tài sản cố định
                                          </th>
                                        </tr>
                                        <tr>
                                          <th style={style.border}>Ngày, tháng, năm</th>
                                          <th style={style.border}>Diễn giải</th>
                                          <th style={style.border}>Nguyên giá</th>
                                          <th style={style.border}>Năm</th>
                                          <th style={style.border}>Giá trị hao mòn</th>
                                          <th style={style.border}>Lũy kế số đã tính</th>
                                        </tr>
                                        </thead>
                                        {item?.details?.map((row, index) => {
                                          return (
                                            <tbody key={index}>
                                            <tr>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignCenter
                                                }}
                                              >
                                                {row?.voucherCode}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignCenter
                                                }}
                                              >
                                                {row?.voucherDate ? moment(row?.voucherDate).format("DD/MM/YYYY") : ""}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignLeft,
                                                }}
                                              >
                                                {row?.explain}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignRight,
                                                }}
                                              >
                                                {convertNumberPrice(row?.originalCost)}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignCenter,
                                                }}
                                              >
                                                {row?.depreciationYear}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignRight,
                                                }}
                                              >
                                                {row?.depreciatedAmount}
                                              </td>
                                              <td
                                                style={{
                                                  ...style.border,
                                                  ...style.textAlignRight,
                                                }}
                                              >
                                                {row?.annualDepreciationAmount}
                                              </td>
                                            </tr>
                                            </tbody>
                                          );
                                        })
                                        }
                                      </table>
                                    </div>
                                    <div style={{ ...style.textAlignCenter, ...style.fontWeightBold, marginTop: 10, marginBottom: 10 }}>
                                        Dụng cụ phụ tùng kèm theo
                                        <div>
                                            <table style={style.table} className="table-report">
                                                <thead>
                                                    <tr>
                                                        <th style={style.w_5} rowSpan="2">
                                                            Số TT
                                                        </th>
                                                        <th style={style.w_30} rowSpan="2">
                                                            Tên, quy cách, dụng cụ, phụ tùng
                                                        </th>
                                                        <th style={style.w_20} rowSpan="2">
                                                            Đơn vị tính
                                                        </th>
                                                        <th style={style.w_15} rowSpan="2">
                                                            Số lượng
                                                        </th>
                                                        <th style={style.w_30} rowSpan="2">
                                                            Giá trị
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div
                                        style={{
                                            ...style.textAlignLeft,
                                            ...style.mt_20,
                                            display: "flex",
                                            flexDirection: "column",
                                            gap: 10
                                        }}
                                    >
                                        <div style={{ whiteSpace: "nowrap", overflow: "hidden" }}>
                                            Ghi giảm TSCĐ chứng từ số:........................ ngày ........... tháng .......... năm  {"..".repeat(100)}
                                        </div>
                                        <div style={{ display: "flex", overflow: "hidden" }}>
                                            <div style={{ minWidth: "90px" }}> Lý do giảm:</div> <div>{"..".repeat(200)}</div>
                                        </div>
                                    </div>
                                    <div
                                        style={{
                                            ...style.textAlignRight,
                                            ...style.mt_20,
                                            ...style.textItalic,
                                        }}
                                    >
                                        <div>Ngày......tháng.......năm.......</div>
                                    </div>
                                    <div style={style.signatureContainer}>
                                        <Signature
                                            titleProps={{ fontSize: "18px", marginTop: 0 }}
                                            signProps={{ fontSize: "18px" }}
                                            title="Người lập phiếu" subTitle="Ký, họ tên" />
                                        <Signature
                                            titleProps={{ fontSize: "18px", marginTop: 0 }}
                                            signProps={{ fontSize: "18px" }}
                                            title="Kế toán trưởng" subTitle="Ký, họ tên" />
                                        <Signature
                                            titleProps={{ fontSize: "18px", marginTop: 0 }}
                                            signProps={{ fontSize: "18px" }}
                                            title="Người đại diện theo pháp luật" subTitle="Ký, họ tên, đóng dấu" />
                                    </div>
                                </div>
                            </div>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <div className="flex flex-space-between flex-middle">
                            <Button
                                variant="contained"
                                color="secondary"
                                className="mr-12"
                                onClick={() => this.props.handleClose()}
                            >
                                {t('general.cancel')}
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className="mr-12"
                                type="submit"
                            >
                                {t('In')}
                            </Button>
                            <Button
                                className="align-bottom mr-12"
                                variant="contained"
                                color="primary"
                                onClick={() => this.props.exportExcelAssetCard()}
                            >
                                {t("Asset.exportExcel")}
                            </Button>
                        </div>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

