import {DEFAULT_PRINT_EXCEL, LIST_ORGANIZATION} from "../../../appConst";


export const SUPPLIES_MANAGEMENT_URLS = {
  INVENTORY_RECEIVING: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/C30HDTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/C30HDTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: ["/assets/form-print/C30HDTemplate.txt",],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      {
        url: "/assets/form-print/nhapKho_BVC.txt",
        config: {excelProps: {show: true, type: 1}},
      },
      {
        url: "/assets/form-print/nhapKhoVT_HCQT_1_bvcThaiNguyen.txt",
        config: {excelProps: {show: true, type: 2}},
      },
      {
        url: "/assets/form-print/nhapKhoVT_HCQT_2_bvcThaiNguyen.txt",
        config: {excelProps: {show: true, type: 3}},
      },
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/nhapKho_bvVanDinh.txt",
      "/assets/form-print/nhapKhoLoaiATemplate.txt",
    ],
  },
  INVENTORY_DELIVERY: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      {
        url: "/assets/form-print/xuatKhoVT_HCQT_bvcThaiNguyen.txt",
        config: {...DEFAULT_PRINT_EXCEL}
      },
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
  },
  RECEIPT: {
    GENERAL: ["/assets/form-print/phieuLinhVatTuThietBiYTe.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/receiptTemplate.txt",
    ],
  },
  INVENTORY_REPORT: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/baoCaoTonKhoVT_HCQT_bvcThaiNguyen.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/baoCaoTonKhoVT_HCQT_bvcThaiNguyen.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: ["/assets/form-print/baoCaoTonKhoVT_HCQT_bvcThaiNguyen.txt"],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      {
        url: "/assets/form-print/baoCaoTonKhoVT_HCQT_bvcThaiNguyen.txt",
        config: {...DEFAULT_PRINT_EXCEL},
      }
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: ["/assets/form-print/baoCaoTonKhoVT_HCQT_bvcThaiNguyen.txt"],
  },
  UTILIZATION_REPORT: {
    GENERAL: [
      {
        url: "/assets/form-print/xuatKhoVT_HCQT_1_bvcThaiNguyen.txt",
        config: {...DEFAULT_PRINT_EXCEL},
      }
    ],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
};