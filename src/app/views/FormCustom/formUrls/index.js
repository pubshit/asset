export {SHOPPING_MANAGEMENT_URLS as SHOPPING_MANAGEMENT} from "./shoppingManagement";
export {FIXED_ASSET_MANAGEMENT_URLS as FIXED_ASSET_MANAGEMENT} from "./fixedAssetManagment";
export {IAT_MANAGEMENT_URLS as IAT_MANAGEMENT} from "./iatManagement";
export {SUPPLIES_MANAGEMENT_URLS as SUPPLIES_MANAGEMENT} from "./suppliesManagement";
export {WAREHOUSE_MANAGEMENT_URLS as WAREHOUSE_MANAGEMENT} from "./warehouseManagement";