import {LIST_ORGANIZATION} from "../../../appConst";
import ConstantList from "../../../appConfig";

export const SHOPPING_MANAGEMENT_URLS = {
    ASSET_PURCHASING: {
      PURCHASE_REQUEST: {
        GENERAL: [],
        [LIST_ORGANIZATION.ASVN.code]: [
          "/assets/form-print/equipmentNeedsTemplate.html",
          "/assets/form-print/danhGiaYeuCauMuaSam_general.html",
        ],
        [LIST_ORGANIZATION.BV199.code]: [
          "/assets/form-print/equipmentNeedsTemplate.html",
          "/assets/form-print/danhGiaYeuCauMuaSam_general.html",
        ],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
          "/assets/form-print/deXuatDauTu_bvHoeNhai.txt",
        ],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
          "/assets/form-print/equipmentNeedsTemplate_bvcThaiNguyen.txt",
        ],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
          "/assets/form-print/duTruTS_CC_DC_bvVanDinh.txt",
          "/assets/form-print/deXuatMuaSamTBYT_bvVanDinh.txt",
        ],
        urlPrintWord:
          ConstantList.API_ENPOINT_ASSET_MAINTANE +
        "/api/download/word/purchase-request",
      },
      PURCHASE_REQUEST_DIALOG: {
        GENERAL: ["/assets/form-print/danhGiaYeuCauMuaSam_general.html"],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
      PURCHASE_REQUEST_COUNT: {
        GENERAL: ["/assets/form-print/tongHopYeuCauMuaSam_bvHoeNhai.txt"],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
      PURCHASE_PLANING: {
        GENERAL: [],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
    },
    SUPPLIES_PURCHASING: {
      PURCHASE_REQUEST: {
        GENERAL: [],
        [LIST_ORGANIZATION.ASVN.code]: [
          "/assets/form-print/equipmentNeedsTemplate.html",
          "/assets/form-print/danhGiaYeuCauMuaSam_general.html",
        ],
        [LIST_ORGANIZATION.BV199.code]: [
          "/assets/form-print/equipmentNeedsTemplate.html",
          "/assets/form-print/danhGiaYeuCauMuaSam_general.html",
        ],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
          "/assets/form-print/deXuatDauTu_bvHoeNhai.txt",
        ],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
          "/assets/form-print/duTruVatTuYCMS_bvcThaiNguyen.txt",
        ],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
          "/assets/form-print/duTruTS_CC_DC_bvVanDinh.txt",
          "/assets/form-print/deXuatMuaSamTBYT_bvVanDinh.txt",
        ],
        urlPrintWord: ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/download/word/purchase-request",
      },
      PURCHASE_REQUEST_DIALOG: {
        GENERAL: ["/assets/form-print/danhGiaYeuCauMuaSam_general.html"],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
      PURCHASE_REQUEST_COUNT: {
        GENERAL: ["/assets/form-print/tongHopYeuCauMuaSam_bvHoeNhai.txt"],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
      PURCHASE_PLANING: {
        GENERAL: [],
        [LIST_ORGANIZATION.ASVN.code]: [],
        [LIST_ORGANIZATION.BV199.code]: [],
        [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
        [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
        [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
      },
    },
  };