import { LIST_ORGANIZATION } from "../../../appConst";

export const FIXED_ASSET_MANAGEMENT_URLS = {
  LIST: {},
  ALLOCATION: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/assetAllocationTemplate_hoeNhai.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/assetAllocationTemplate_bvThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
  },
  TRANSFER: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/DCTSTBTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/DCTSTBTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/DCTSTB_hoeNhaiTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/DCTSTB_bvBVCTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/DCTSTB_bvVanDinhTemplate.html",
    ],
  },
  TRANSFER_TO_ANOTHER_UNIT: {
    GENERAL: ["/assets/form-print/C50HDTemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  LIQUIDATE: {
    GENERAL: ["/assets/form-print/assetLiquidateTemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  INVENTORY_COUNT: {
    GENERAL: [
      {
        url: "/assets/form-print/C53HDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.ASVN.code]: [
      {
        url: "/assets/form-print/KKTS_bv_199.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkTSCDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      {
        url: "/assets/form-print/KKTS_bv_199.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkTSCDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      {
        url: "/assets/form-print/bbkkTSCDTemplate_hoeNhai.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      {
        url: "/assets/form-print/bbkkTSCDTemplate_BVC_PVT.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      {
        url: "/assets/form-print/assetInventorySheetTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
  },
  INVENTORY_COUNT_ORG: {
    GENERAL: [
      {
        url: "/assets/form-print/C53HDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkTSCDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  MAINTAIN_PLANING: {
    PROPOSAL: {
      GENERAL: [],
      [LIST_ORGANIZATION.ASVN.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate.html",
      ],
      [LIST_ORGANIZATION.BV199.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate.html",
      ],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate_hoeNhai.txt",
      ],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate_bvcThaiNguyen.txt",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate.html",
      ],
    },
    ACCEPTANCE: {
      GENERAL: [],
      [LIST_ORGANIZATION.ASVN.code]: [
        "/assets/form-print/acceptanceMaintainaceTemplate.txt",
      ],
      [LIST_ORGANIZATION.BV199.code]: [
        "/assets/form-print/acceptanceMaintainaceTemplate.txt",
      ],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
        "/assets/form-print/acceptanceMaintainaceTemplate_hoeNhai.txt",
      ],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/acceptanceMaintainaceTemplate.txt",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
        "/assets/form-print/acceptanceMaintainaceTemplate.txt",
      ],
    },
  },
  REPAIRING: {
    INCIDENT_REPORT_PVT: {
      GENERAL: [],
      [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/BM0601.txt"],
      [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/BM0601.txt"],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
        "/assets/form-print/suggestedMaintenanceTemplate_hoeNhai.txt",
      ],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/BM0601_bvcThaiNguyen.txt",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
        "/assets/form-print/duTruSuaChua_bvVanDinh.txt",
      ],
    },
    TAKE_ASSET_OUT_OF_ORG: {
      GENERAL: ["/assets/form-print/MAU_DNMTS_hoeNhai.txt"],
      [LIST_ORGANIZATION.ASVN.code]: [],
      [LIST_ORGANIZATION.BV199.code]: [],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
    },
    INCIDENT_REPORT_HCQT: {
      GENERAL: [
        {
          url: "/assets/form-print/BM01.html",
          config: {
            margin: "0px 40px",
          },
        },
      ],
      [LIST_ORGANIZATION.ASVN.code]: [],
      [LIST_ORGANIZATION.BV199.code]: [],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/BM01_bvcThaiNguyen.txt",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
    },
    MINUTES_OF_HANDLING_SUPPLIES: {
      GENERAL: ["/assets/form-print/KTTS01.txt"],
      [LIST_ORGANIZATION.ASVN.code]: [],
      [LIST_ORGANIZATION.BV199.code]: [],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
    },
    INCIDENT_RATING: {
      GENERAL: [],
      [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/BM0606.txt"],
      [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/BM0606.txt"],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: ["/assets/form-print/BM0606.txt"],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/BM0606_bvcThaiNguyen.txt",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
        "/assets/form-print/bienBanXacNhanTinhTrangMay_bvVanDinh.txt",
      ],
    },
    ACCEPTANCE: {
      GENERAL: [],
      [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/BM0607.html"],
      [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/BM0607.html"],
      [LIST_ORGANIZATION.BV_HOE_NHAI.code]: ["/assets/form-print/BM0607.html"],
      [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
        "/assets/form-print/BM0607.html",
      ],
      [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
        "/assets/form-print/banGiaoSauSuaChua_bvVanDinh.txt",
        "/assets/form-print/deXuatSuaChua_bvVanDinh.txt",
      ],
    },
  },
  RECEIPT: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/receiptTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/receiptTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/receiptTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/receiptTemplate_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/receiptTemplate.txt",
    ],
  },
  RECALL: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      {
        url: "/assets/form-print/phieuHoanTra_bvVanDinh.txt",
        config: { margin: "0 40px" },
      },
    ],
  },
  RECEIVING: {
    GENERAL: ["/assets/form-print/nhapKhoLoaiATemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/bienBanKiemNhap_BVC.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
};
