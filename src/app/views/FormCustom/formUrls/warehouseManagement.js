import {LIST_ORGANIZATION} from "../../../appConst";


export const WAREHOUSE_MANAGEMENT_URLS = {
  RECEIVING: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/C30HDTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/C30HDTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/C30HDTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/nhapKho_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/nhapKho_bvVanDinh.txt",
      "/assets/form-print/nhapKhoLoaiATemplate.txt",
    ],
  },
  DELIVERY: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/xuatKho_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/xuatKho_bvVanDinh.txt",
    ],
  },
  UTILIZATION_REPORT: {
    GENERAL: ["/assets/form-print/xuatKhoVT_HCQT_1_bvcThaiNguyen.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  RECEIVING_SYNTHETIC: {
    GENERAL: ["/assets/form-print/nhapKhoTongHop.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  DELIVERY_SYNTHETIC: {
    GENERAL: ["/assets/form-print/xuatKhoTongHop.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  }
};