import { LIST_ORGANIZATION } from "../../../appConst";

export const IAT_MANAGEMENT_URLS = {
  LIST: {},
  RECEIVING: {
    GENERAL: ["/assets/form-print/nhapKhoLoaiATemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/bienBanKiemNhap_BVC.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  ALLOCATION: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/assetAllocationTemplate_hoeNhai.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/assetAllocationTemplate_bvThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/assetAllocationTemplate.txt",
    ],
  },
  TRANSFER: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/DCTSTBTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/DCTSTBTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/DCTSTB_hoeNhaiTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/DCTSTB_bvBVCTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/DCTSTB_bvVanDinhTemplate.html",
    ],
  },
  TRANSFER_TO_ANOTHER_UNIT: {
    GENERAL: ["/assets/form-print/C50HDTemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  LIQUIDATE: {
    GENERAL: ["/assets/form-print/assetLiquidateTemplate.txt"],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  INVENTORY_COUNT: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      {
        url: "/assets/form-print/C53HDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/KKTS_bv_199.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkCCDCTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      {
        url: "/assets/form-print/C53HDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/KKTS_bv_199.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkCCDCTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/C53HDTemplate.txt",
      "/assets/form-print/bbkkTSCDTemplate_hoeNhai.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/bbkkTSCDTemplate_BVC_PVT.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/C53HDTemplate.txt",
      "/assets/form-print/assetInventorySheetTemplate.txt",
    ],
  },
  INVENTORY_COUNT_ORG: {
    GENERAL: [
      {
        url: "/assets/form-print/C53HDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkTSCDTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
      {
        url: "/assets/form-print/bbkkCCDCTemplate.txt",
        config: {
          layout: "landscape",
          margin: "horizontal",
        },
      },
    ],
    [LIST_ORGANIZATION.ASVN.code]: [],
    [LIST_ORGANIZATION.BV199.code]: [],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [],
  },
  RECEIPT: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: ["/assets/form-print/receiptTemplate.txt"],
    [LIST_ORGANIZATION.BV199.code]: ["/assets/form-print/receiptTemplate.txt"],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/receiptTemplate.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/receiptTemplate_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      "/assets/form-print/receiptTemplate.txt",
    ],
  },
  RECALL: {
    GENERAL: [],
    [LIST_ORGANIZATION.ASVN.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV199.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_HOE_NHAI.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN.code]: [
      "/assets/form-print/phieuHoanTra_bvcThaiNguyen.txt",
    ],
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: [
      {
        url: "/assets/form-print/phieuHoanTra_bvVanDinh.txt",
        config: { margin: "0 40px" },
      },
    ],
  },
};
