import React, { Component } from "react";
import {
  Dialog,
  Button,
  Grid,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { PaperComponent, convertNumberPrice, convertNumberToWords, spliceString } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import Title from "../Component/Title";
import Signature from "../Component/Signature";

const style = {
  textItalic: {
    fontStyle: "italic",
  },
  justifyContentSpaceBetween: {
    display: "flex",
    justifyContent: "space-between"
  },
  textAlignCenter: {
    textAlign: "center",
  },
  fontWeightBold: {
    fontWeight: "bold",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
  },
  w_15: {
    border: "1px solid",
    width: "15%",
  },
  w_25: {
    border: "1px solid",
    width: "25%",
  },
  w_30: {
    border: "1px solid",
    width: "30%",
  },
  p_10: {
    padding: 10,
  },
  mt_20: {
    marginTop: 20,
  },
  fz_16: {
    fontSize: 16,
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 10%",
  },
  border: {
    border: "1px solid",
    textAlign: "center",
    fontSize: "0.975rem",
  },
  nameColumn: {
    border: "1px solid",
    textAlign: "left",
    paddingLeft: "10px",
    fontSize: "0.975rem",
    wordBreak: "break-all",
  },
  signatureContainer: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr 1fr",
    marginTop: "20px",
  },
  signatureItemHeader: {
    width: "100%",
    height: 1,
  },
};

class C30HD extends Component {
  state = {
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write(content.innerHTML);
    
    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    let { item } = this.props;

    this.setState({
      ...item,
      itemList: item?.voucherDetails,
    });
  }
  componentDidMount() {
  }

  render() {
    let {
      t,
      open, 
      item,
    } = this.props;
    let { itemList = [] } = this.state;
    let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
    const totalAmount = itemList?.reduce((total, item) => Number(total) + Number(item.amount), 0)
    
    return (     
      <Dialog open={open} PaperComponent={PaperComponent} maxWidth="lg" fullWidth  >
        <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
          <span className="">{t('Phiếu nhập kho')}</span>
        </DialogTitle>
        <iframe 
          id="ifmcontentstoprint" 
          style={{
            height: '0px',
            width: '0px', 
            position: 'absolute', 
            print :{ size: 'auto',  margin: '0mm' }
          }}
        ></iframe>
        
        <ValidatorForm 
          className="validator-form-scroll-dialog" 
          ref="form" 
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className=" dialog-print">
            <Grid>
              <div
                style={style.textAlignCenter}
                className="form-print form-print-landscape"
              >
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div
                    style={{
                      ...style.textAlignCenter,
                      fontWeight: "bold",
                      marginTop: "20px",
                    }}
                  >
                    <div>{titleName && titleName?.org?.name}</div>
                    <div
                      style={{
                        textDecoration: "underline solid 1px",
                        textTransform: "uppercase",
                        textUnderlineOffset: "5px",
                      }}
                    >
                      {/* {item?.dxPhongBanText} */}
                      Phòng TC - KT
                    </div>
                  </div>
                  <div style={{ ...style.fz_16, }}>
                    <div>Mẫu số: C30-HD</div>
                    <div>Ban hành theo TT 107/2017/TT - BTC</div>
                    <div>Ngày 10/10/2017 của Bộ trưởng BTC</div>
                    <div style={style.fontWeightBold}>Số phiếu:&nbsp;{item?.voucherCode}</div>
                  </div>
                </div>
                <Title
                  title={"PHIẾU NHẬP KHO"} 
                  date={item.issueDate}
                  subTitle={"Số HĐ: " + (item?.billNumber || "")}
                  subTitleProps={style.fontWeightBold}
                />
                
                <div 
                  style={{
                    display: "grid",
                    gridTemplateColumns: "2fr 1fr",
                    ...style.textAlignLeft,
                  }}
                >
                  <div>
                    Họ tên người nhận hàng:&nbsp;
                    {spliceString(item?.handoverPerson?.displayName)}
                  </div>
                  <div>
                    Địa chỉ:&nbsp;
                    <span style={style.textItalic}>{spliceString(item?.stockReceiptDeliveryStore?.departmentName)}</span>
                  </div>
                </div>

                <div style={style.textAlignLeft}>Lý do nhập kho:&nbsp;
                  <span style={style.textAlignLeft}>{item?.reason}</span>
                </div>

                <div style={style.textAlignLeft}>
                  Nhập tại kho:&nbsp;
                  <span style={style.fontWeightBold}>{item?.stockReceiptDeliveryStore?.name}</span>
                </div>

                <div style={{ ...style.textAlignCenter, ...style.mt_20 }}>
                  <div>
                    {
                      <table style={style.table} className="table-report">
                        <thead>
                          <tr>
                            <th style={style.w_5} rowSpan="2">
                              STT
                            </th>
                            <th style={style.w_15} rowSpan="2">
                              Tên thuốc/ hàm lượng
                            </th>
                            <th style={style.w_10} rowSpan="2">
                              Nước SX
                            </th>
                            <th style={style.w_5} rowSpan="2">
                              ĐVT
                            </th>
                            <th style={style.w_10} colSpan="2">
                              Số lượng
                            </th>
                            <th style={style.w_15} rowSpan="2">
                              Đơn giá (Có VAT)
                            </th>
                            <th style={style.w_15} rowSpan="2">
                              Thành tiền
                            </th>
                          </tr>
                            
                          <tr>
                            <th >CT</th>
                            <th >Thực</th>
                          </tr>
                        </thead>
                        {itemList !== null
                          ? itemList?.map((row, index) => {
                              return (
                                <tbody>
                                  <tr>
                                    <td style={style.border}>
                                      {index !== null ? index + 1 : ""}
                                    </td>
                                    <td style={{ ...style.nameColumn, ...style.w_25}}>
                                      {row?.product?.name || ""}
                                    </td>
                                    <td
                                      style={{
                                        ...style.border,
                                      }}
                                    >
                                      {row?.product?.manufacturer || ""}
                                    </td>
                                    <td 
                                      style={{
                                        ...style.border,
                                        wordBreak: "break-all",
                                      }}
                                    >
                                      {row?.sku?.name || ""}
                                    </td>
                                    <td 
                                      style={{
                                        ...style.border,
                                        wordBreak: "break-all",
                                      }}
                                    >
                                      {row?.quantityOfVoucher || ""}
                                    </td>
                                    <td 
                                      style={{
                                        ...style.border,
                                        wordBreak: "break-all",
                                      }}
                                    >
                                      {row?.quantity || ""}
                                    </td>
                                    <td 
                                      style={{
                                        ...style.border,
                                        wordBreak: "break-all",
                                      }}
                                    >
                                      {convertNumberPrice(row?.price) || ""}
                                    </td>
                                    <td 
                                      style={{
                                        ...style.border,
                                        wordBreak: "break-all",
                                      }}
                                    >
                                      {convertNumberPrice(row?.amount) || ""}
                                    </td>
                                  </tr>
                                </tbody>
                              );
                            })
                          : ""}
                      </table>
                    }
                  </div>

                  <p style={style.textAlignLeft}>
                    Tổng số tiền (viết bằng chữ):&nbsp;
                    <b>
                      {
                      totalAmount 
                        ? convertNumberToWords(totalAmount) + " đồng."
                        : ""
                      }
                    </b>
                  </p>
                  <div style={style.signatureContainer}>
                    <Signature title="Kế toán" subTitle="Ký, ghi rõ họ tên"/>
                    <Signature title="Thủ kho" subTitle="Ký, ghi rõ họ tên"/>
                    <Signature title="Người giao hàng" subTitle="Ký, ghi rõ họ tên"/>
                    <Signature title="Trưởng phòng TC - KT" subTitle="Ký, ghi rõ họ tên"/>
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">              
              <Button
                variant="contained"
                color="secondary"
                className="mr-12"
                onClick={() => this.props.handleClose()}
              >
                {t('general.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="mr-16"
                type="submit"
              >
                {t('In')}
              </Button>
            </div>
          </DialogActions>         
        </ValidatorForm>        
      </Dialog>
    );
  }
}

export default C30HD;
