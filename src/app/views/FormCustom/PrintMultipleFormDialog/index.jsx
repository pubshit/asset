import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogContent from "@material-ui/core/DialogContent";
import { Button, CircularProgress, Dialog, DialogActions, Grid, Tab, Tabs } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { FadeLayout, PaperComponent, TabPanel } from "../../Component/Utilities";
import { a11yProps, functionExportToWord } from "../../../appFunction";
import Divider from "@material-ui/core/Divider";
import { TYPE_OF, variable } from "../../../appConst";
import Mustache from "mustache";
import Scrollbar from "react-perfect-scrollbar";
import {exportToWord} from "../../../appServices";

export const PrintMultipleFormDialog = (props) => {
  let {
    t,
    open,
    item,
    handleClose,
    title,
    urls,
    getTemplate = () => { },
    urlWord,
  } = props;
  const form = useRef(null);
  const [tabValue, setTabValue] = useState(0);
  const [formConfig, setFormConfig] = useState({ ...(urls)[0]?.config });
  const [template, setTemplate] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    handleGetTemplateByTabValue()
  }, [tabValue]);

  const handleGetTemplateByTabValue = async () => {
    try {
      setLoading(true);
      let res = await fetch(
        typeof urls[tabValue] === TYPE_OF.OBJECT
          ? urls[tabValue].url
          : urls[tabValue]
      );
      if (!res.ok) {
        throw new Error(t("general.error"));
      }
      let data = await res.text();
      let newTemplate = Mustache.render(data, item)
      getTemplate(newTemplate)
      setTemplate(newTemplate);
    } catch (e) {
      console.error(e)
    } finally {
      setLoading(false);
    }
  }

  const handleChangeTabValue = (event, newValue) => {
    setTabValue(newValue);
    if (typeof (urls)[newValue] === TYPE_OF.OBJECT) {
      setFormConfig((urls)[newValue].config)
    }
  };

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let iframe = document.getElementById("ifmcontentstoprint");
    let pri = iframe.contentWindow;

    pri.document.open();

    pri.document.write(`<title>&nbsp;</title>`);
    pri.document.write(getFormStyleByConfig());
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  const getFormStyleByConfig = () => {
    let margin = "";
    let marginArray = [
      formConfig?.margin === variable.formPrint.margin.horizontal,
      formConfig?.margin === variable.formPrint.margin.vertical,
    ];

    if (formConfig?.margin && !marginArray.includes(formConfig?.margin)) {
      margin = formConfig?.margin;
    } else if (formConfig?.margin === variable.formPrint.margin.horizontal) {
      margin = "40px 60px"
    } else {
      margin = "2cm 1.5cm 2cm 3cm"
    }

    return `
      <style>
        @page {
          margin: ${margin};
          size: ${formConfig?.layout};
        }
      </style>
    `
  }

  const handlePrintWord = async () => {
    try {
      let titleFile = `${title || "file"}.docx`;

      await functionExportToWord(props?.funcExportWord || exportToWord, urlWord, props?.payloadExport, titleFile, setLoading);

    } catch (error) {
      console.error("Error exporting Word file:", error);
    }
  };

  const handleExportToExcel = () => {
    props.exportExcelFunction(formConfig?.excelProps);
  }

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={props?.maxWidth || "xl"}
      fullWidth
    >
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        ref={form}
        className="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogTitle className="cursor-move">
          <span className="mb-20">{title || "In phiếu"}</span>
        </DialogTitle>
        <Divider />
        <DialogContent className="p-0">
          <Grid container>
            <Grid item xs={2} className="bg-white">
              <Tabs
                className="tabs-container"
                orientation="vertical"
                variant="scrollable"
                value={tabValue}
                onChange={handleChangeTabValue}
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                {urls?.map((children, index) => {
                  let label = "Mẫu ";
                  if (typeof children === TYPE_OF.OBJECT && children.tabLabel) {
                    label = children.tabLabel;
                  } else {
                    label = "Mẫu số " + (index + 1);
                  }
                  return (
                    <Tab label={label} {...a11yProps(index)} value={index} />
                  )
                })}
              </Tabs>
            </Grid>
            <Grid
              item container xs={10}
              className="dialog-print"
              justifyContent="center"
            >
              <Scrollbar
                className="m-10 position-relative"
                style={{ minHeight: "65vh", maxHeight: "68vh", }}
              >
                <Grid
                  item xs={12}
                  id="divcontents"
                >
                  <FadeLayout
                    show={loading}
                    fullSize
                    blur
                    justifyContent="center"
                    alignItems="center"
                    color="primary"
                  >
                    <CircularProgress color="inherit" />
                  </FadeLayout>
                  {urls?.map((children, index) => (
                    <TabPanel index={index} value={tabValue} boxProps={{ className: "p-0" }}>
                      <div dangerouslySetInnerHTML={{ __html: template }} />
                    </TabPanel>
                  ))}
                </Grid>
              </Scrollbar>
            </Grid>
          </Grid>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Grid container spacing={2} justifyContent="center">
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                // className="mr-36"
                onClick={handleClose}
              >
                {t("general.cancel")}
              </Button>
            </Grid>
            <Grid item>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </Grid>
            {urlWord && <Grid item>
              <Button variant="contained" color="primary" onClick={() => handlePrintWord(tabValue)}>
                {t("general.word")}
              </Button>
            </Grid>}
            {formConfig?.excelProps?.show && (
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleExportToExcel}
                >
                  {t("general.exportToExcel")}
                </Button>
              </Grid>
            )}
          </Grid>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

PrintMultipleFormDialog.propTypes = {
  t: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  urls: PropTypes.arrayOf(PropTypes.shape({
    tabLabel: PropTypes.string,
    component: PropTypes.node,
    config: PropTypes.shape({
      margin: PropTypes.oneOf(['vertical', 'horizontal']),
      layout: PropTypes.oneOf(['portrait', 'landscape', 'A3', 'A4', 'A5', 'letter']),
      excelProps: PropTypes.shape({
        type: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        show: PropTypes.bool,
      })
    }),
  })),
  exportExcelFunction: PropTypes.func,
  urlWord: PropTypes.string,
  funcExportWord: PropTypes.func,
  payloadExport: PropTypes.object,
};
