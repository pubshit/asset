import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    Grid,
    withStyles
} from "@material-ui/core";

import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ValidatorForm } from "react-material-ui-form-validator";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";

const style = {
    textAlignCenter: {
        textAlign: "center",
    },
    textAlignLeft: {
        textAlign: "left",
    },
    table: {
        width: "100%",
        borderCollapse: "collapse",
        marginTop:"20px"
    },
    pl_20: {
        paddingLeft: "20px"
    },
    px:{
        padding:"0 2px"
    },
    italic: {
        fontStyle: "italic",
    },
    sign: { width: "100%", display: "flex", justifyContent: "space-around", marginBottom: "30px" }
};
const styles = (theme) => ({
    content_container: {
        "& .table-report th.border-none": {
            border: "none !important",
        }
    },
    unHover: {
        pointerEvents: "none !important",
    },
});

const DXTH = (props) => {
    const { t, handleClose, classes } = props;
    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
    };
    return (
        <Dialog open={props.open} maxWidth="md" fullWidth>
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                <span>{"Phiếu đề xuất tổng hợp"}</span>
            </DialogTitle>
            <iframe
                id="ifmcontentstoprint"
                style={{
                    height: "0px",
                    width: "0px",
                    position: "absolute",
                    print: { size: "auto", margin: "0mm" },
                }}
            ></iframe>
            <ValidatorForm
                class="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <Grid className={classes.content_container}>
                        <div className="form-print">
                            <div
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between"
                                }}
                            >
                                <div
                                    style={{ textTransform: "uppercase", textAlign: "center", marginTop: "20px" }}
                                >
                                    BỆNH VIỆN RĂNG HÀM MẶT TRUNG ƯƠNG <br />
                                    Thành phố Hồ Chí Minh <br />
                                    <span style={{ position: "relative", fontWeight: "bold" }}>
                                        PHÒNG VẬT TƯ - TRANG THIẾT BỊ Y TẾ
                                        <div style={{
                                            position: "absolute", width: "50%", height: "1px",borderBottom: "1px solid",
                                            left: "50%", top: "104%",
                                            transform: "translateX(-50%)"
                                        }}></div>
                                    </span>
                                </div>
                                <div style={{ textAlign: "center" }}>
                                    <NationalName></NationalName>
                                    <Title date={new Date()} isLocation={true}></Title>
                                </div>
                            </div>
                            <div style={{ ...style.textAlignCenter, marginBottom: "1rem" }}>
                                <Title title={"Phiếu đề xuất tổng hợp"}></Title>
                                <span>Về việc sửa chữa monitor theo dõi bệnh nhân 5 thông số</span>
                            </div>
                            <p style={{ textAlign: "center" }}>Kính gửi: Tổ thẩm định</p>
                            <p style={style.rowContainer}>
                                <div style={style.rowContainer}>
                                    Căn cứ phiếu đề xuất của khoa Nha chu về việc sửa chữa monitor theo dõi bệnh nhân 5 thông số
                                    đã được phê duyệt. Phòng Vật tư - Trang Thiết bị y tế kính trình Tổ thẩm định xem xét,
                                    phê duyệt giá. Chi tiết như sau:
                                </div>
                                <div style={style.pl_20}>
                                    - Thời gian thực hiện: Quý 3/2023. <br />
                                    - Địa điểm thực hiện: Khoa Nha chu. <br />
                                    - Kinh phí thực hiện
                                </div>
                                <table className=" table-report" style={{ ...style.table }}>
                                    <tr style={{ border: "1px solid",...style.px }} className={classes.unHover}>
                                        <th style={{ textAlign: "center", width: "5%" }}>STT</th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "35%",...style.px }}>Nội dung</th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}>ĐVT</th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}>Số lượng</th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%",...style.px }}>Đơn giá</th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%",...style.px }}>Thành tiền</th>
                                    </tr>
                                    {/* <tr style={{ border: "1px solid" }} className={classes.unHover}>
                                        <th style={{ textAlign: "center", width: "5%" }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "35%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%" ,...style.px}}></th>
                                    </tr> */}
                                    <tr style={{ border: "1px solid" }} className={classes.unHover}>
                                            <th
                                                align="center"
                                                colSpan={2}
                                                
                                            >
                                                <span style={{ fontWeight: "bold" }}>Cộng</span>
                                            </th>
                                            <th
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="right"
                                                style={{borderLeft: "1px solid",...style.px }}
                                            >
                                                <span style={{ fontWeight: "bold" }}>5.600.000</span>
                                            </th>
                                        </tr>
                                        <tr style={{ border: "1px solid" }} className={classes.unHover}>
                                            <th
                                                align="center"
                                                colSpan={2}
                                                
                                            >
                                                <span style={{ fontWeight: "bold" }}>VAT 8%</span>
                                            </th>
                                            <th
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="right"
                                                style={{borderLeft: "1px solid",...style.px }}
                                                
                                            >
                                                <span style={{ fontWeight: "bold" }}>400.000</span>
                                            </th>
                                        </tr>
                                        <tr style={{ border: "1px solid" }} className={classes.unHover}>
                                            <th
                                                align="center"
                                                colSpan={2}
                                            >
                                            </th>
                                            <th
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="center"
                                                style={{ borderLeft: "1px solid"}}
                                            >
                                            </th>
                                            <th
                                                
                                                align="right"
                                                style={{borderLeft: "1px solid",...style.px }}
                                            >
                                                <span style={{ fontWeight: "bold" }}>6.600.000</span>
                                            </th>
                                        </tr>
                                </table>
                                <p style={style.textAlignCenter}>(Bằng chữ: Sáu triệu không trăm bốn mưới tắm ngàn đồng)</p>
                                <p style={style.pl_20}>Trân trọng./</p>
                            </p>
                            <div style={{...style.sign}}>
                                <div></div>
                                <Signature title={"TRƯỞNG PHÒNG"} name={"Bùi Công Toản"} mNone={true}></Signature>
                            </div>
                            <div style={{
                                width: "80%", height: "1px",borderTop: "1px solid",margin:"auto"
                            }}></div>
                            <Signature title={"Ý KIẾN CỦA TỔ TRƯỞNG THẨM ĐỊNH"} name={"Nguyễn Văn Khoa"} ></Signature>
                        </div>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-36"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            {t("general.print")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
};

export default withStyles(styles)(DXTH);
