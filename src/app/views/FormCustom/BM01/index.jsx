import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    Grid,
    withStyles,
} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import HeaderLeft from "../Component/HeaderLeft";
import moment from "moment";
import { ValidatorForm } from "react-material-ui-form-validator";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import { appConst, variable } from "app/appConst";
import { getTheHighestRole } from "app/appFunction";

const style = {
    textAlignCenter: {
        textAlign: "center",
    },
    textAlignLeft: {
        textAlign: "left",
    },
    table: {
        width: "100%",
        borderCollapse: "collapse",
    },
    tableSecond: {
        display: "flex",
        width: "100%",
        borderCollapse: "collapse",
    },
    tableThree: {
        width: "97%",
        margin: "20px auto",
        border: "1px solid",
        borderCollapse: "collapse",
    },
    w_20: {
        width: "20%",
        wordBreak: "break-all",
    },
    w_40: {
        width: "40%",
        wordBreak: "break-all",
    },
    w_50: {
        width: "50%",
        wordBreak: "break-all",
    },
    w_100: {
        width: "100%",
    },
    m_10: {
        margin: 10,
    },
    mt_20: {
        marginTop: 20,
    },
    mb_16: {
        marginBottom: 16,
    },
    pt_6: {
        paddingTop: 6,
    },
    h_80px: {
        height: 80,
    },
    min_h_100px: {
        minHeight: 100,
    },
    border: {
        borderTop: "1px solid",
        borderBottom: "1px solid",
        textAlign: "left",
    },
    italic: {
        fontStyle: "italic",
    },
    inputDotted: (child) => {
        return {
            display: child ? "" : "flex",
            flex: 1,
            border: "none",
            borderBottom: "2px dotted #000",
            lineHeight: "1.6rem",
            fontSize: "1rem",
            marginTop: "0",
            marginBottom: "10px",
            padding: "1px 0",
            position: "relative",
            bottom: "-3px",
            outline: "none",
            height: "12px",
            marginLeft: "3px",
        };
    },
    bg: {
        background: "#e6e6e6",
    },
    colTable: {
        width: "20%",
        fontWeight: 600,
        textAlign: "center",
        wordBreak: "break",
    },
    wordWrap: {
        wordWrap: "break-word",
        display: "block",
    },
    flex: {
        display: "flex",
    },
    flexMiddle: {
        display: "flex",
        alignItems: "center",
    },
    spaceBetween: {
        justifyContent: "space-between"
    },
    gridMiddle: {
        display: "grid",
        alignItems: "center",
    },
    gridTemplateColumns_2_7: {
        gridTemplateColumns: "2fr 7fr",
    },
    border1Solid: {
        border: "1px solid",
    },
    borderLeft: {
        borderLeft: "1px solid",
    },
    borderBottomNone: {
        borderBottom: 0,
    },
    borderTopNone: {
        borderTop: 0,
    },
    borderBottomDotted: {
        borderBottom: "2px dotted",
        lineHeight: "13px",
    },
    positionRelative: {
        position: "relative",
    },
    subDottedRow: {
        firstRow: {
            height: 19,
            borderBottom: "2px dotted",
        },
        row: {
            height: 23,
            borderBottom: "2px dotted",
        },
    },
    rowContainer: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 10,
        right: 10,
        overflow: "hidden",
    },
    lineHeight25px: {
        lineHeight: "25px",
    },
    printFrame: {
        height: "0px",
        width: "0px",
        position: "absolute",
        print: { size: "auto", margin: "0mm" },
    },
};
const styles = (theme) => ({
    content_container: {
        "& .table-report th.border-none": {
            border: "none !important",
        },
        "& .table-report th.border-b-none": {
            borderBottom: "none !important",
        },
        "& .table-report th.border-t-none": {
            borderTop: "none !important",
        },
    },
    borderNone: {
        border: "none !important",
    },
    unHover: {
        pointerEvents: "none !important",
    },
});

const BM01 = (props) => {
    const { t, handleClose, classes, item } = props;
    const { currentUser } = getTheHighestRole();
    const date = new Date();
    const dateNull = `Ngày ..... tháng ..... năm ${moment(date).format("YYYY")}`;
    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
    };
    return (
        <Dialog open={props.open} maxWidth="md" fullWidth>
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                <span>{t("print.repairRequest")}</span>
            </DialogTitle>
            <iframe
                title="a"
                id="ifmcontentstoprint"
                style={style.printFrame}
            ></iframe>
            <ValidatorForm
                class="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <Grid className={classes.content_container}>
                        <div className="form-print">
                            <div
                                style={{
                                    ...style.spaceBetween,
                                    ...style.flexMiddle,
                                }}
                            >
                                <div style={style.mt_20}>
                                    <HeaderLeft
                                        textAlign={style.textAlignCenter}
                                        code={currentUser?.org?.name ?? ""}
                                    />
                                </div>
                                <span style={{ ...style.italic, ...style.mt_20 }}>
                                    BM01/QT.QT.03
                                </span>
                            </div>
                            <div style={{ ...style.textAlignCenter, ...style.mb_16 }}>
                                <Title title={t("print.repairRequest")}></Title>
                            </div>
                            <div style={style.border1Solid}>
                                <div style={{ ...style.table, ...style.borderBottomNone }}>
                                    <span
                                        style={{
                                            ...style.m_10,
                                            ...style.gridMiddle,
                                            ...style.gridTemplateColumns_2_7,
                                        }}
                                    >
                                        <span>Khoa,phòng:</span>
                                        <span style={{ 
                                            ...style.wordWrap,
                                            ...style.borderBottomDotted,
                                        }}>
                                            {item?.arPhongBanText ?? ""}
                                        </span>
                                    </span>
                                    <span
                                        style={{
                                            ...style.m_10,
                                            ...style.gridMiddle,
                                            ...style.gridTemplateColumns_2_7,
                                        }}
                                    >
                                        <span>Kính báo cáo thiết bị:</span>
                                        <span style={{ 
                                            ...style.wordWrap,
                                            ...style.borderBottomDotted,
                                        }}>
                                            {item?.tsTen ?? ""}
                                        </span>
                                    </span>
                                </div>
                                <div style={{ ...style.border, ...style.bg }}>
                                    <strong style={style.m_10}>1. Sự cố:</strong>
                                </div>
                                <div
                                    style={{ 
                                        ...style.min_h_100px, 
                                        ...style.table,
                                        ...style.positionRelative,
                                        ...style.borderBottomNone,
                                        ...style.borderTopNone,
                                    }}
                                >
                                    <span style={{ 
                                        ...style.wordWrap, 
                                        ...style.lineHeight25px, 
                                        ...style.m_10, 
                                    }}>
                                        {item?.arNoiDung ?? ""}
                                    </span>
                                    <div className="row-dotted-form-container" style={style.rowContainer}>
                                        <div style={style.subDottedRow.firstRow}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                    </div>
                                </div>
                                <div style={style.tableSecond}>
                                    <div
                                        style={{ ...style.w_50, ...style.textAlignCenter }}
                                    ></div>
                                    <div
                                        style={{ ...style.w_50, ...style.textAlignCenter }}
                                    >
                                        <Signature 
                                            title={t("maintainRequest.formText.assetUser")}
                                            date={" "} 
                                        />
                                    </div>
                                </div>

                                <div style={{ ...style.border, ...style.bg }}>
                                    <strong style={style.m_10}>
                                        2. Kiểm tra, nêu biện pháp khắc phục&nbsp;
                                    </strong>
                                </div>
                                <div
                                    style={{ 
                                        ...style.min_h_100px, 
                                        ...style.table,
                                        ...style.positionRelative,
                                        ...style.borderBottomNone, 
                                        ...style.borderTopNone,
                                    }}
                                    className="table-report"
                                >
                                    <span style={{ 
                                        ...style.wordWrap, 
                                        ...style.lineHeight25px, 
                                        ...style.m_10, 
                                    }}>
                                        {item?.arMoTa && (
                                            <div style={style.w_100}>
                                                - Mô tả sự cố:&nbsp;{item?.arMoTa ?? ""}
                                            </div>
                                        )}
                                        {item?.arPhanTichSoBo && (
                                            <div style={style.w_100}>
                                                - Phân tích sơ bộ:&nbsp;{item?.arPhanTichSoBo ?? ""}
                                            </div>
                                        )}
                                        {item?.arDeXuatBienPhap && (
                                            <div style={style.w_100}>
                                                - Đề xuất biện pháp:&nbsp;{item?.arDeXuatBienPhap ?? ""}
                                            </div>
                                        )}
                                    </span>
                                    <div className="row-dotted-form-container" style={style.rowContainer}>
                                        <div style={style.subDottedRow.firstRow}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                    </div>
                                </div>
                                <div style={style.tableSecond} className="table-report">
                                    <div
                                        className="border-none"
                                        style={{
                                            ...style.w_50
                                        }}
                                    >
                                        <Signature
                                            sign={""}
                                            titleProps={style.mt_20}
                                            title={t("maintainRequest.formText.assessor")}
                                        />

                                    </div>
                                    <div
                                        className="border-none"
                                        style={{
                                            ...style.w_50
                                        }}
                                    >
                                        <Signature
                                            sign={""}
                                            titleProps={style.mt_20}
                                            title={t("maintainRequest.formText.headOfAdministrativeDepartment")}
                                        />
                                    </div>
                                </div>

                                <div style={{ ...style.border, ...style.bg }}>
                                    <strong style={style.m_10}>
                                        3. Kết quả sửa chữa, khắc phục
                                    </strong>
                                </div>
                                <div
                                    style={{ 
                                        ...style.min_h_100px, 
                                        ...style.table,
                                        ...style.positionRelative,
                                        ...style.borderBottomNone, 
                                        ...style.borderTopNone,
                                    }}
                                    className="table-report"
                                >
                                    <span style={{ 
                                        ...style.wordWrap, 
                                        ...style.lineHeight25px, 
                                        ...style.m_10, 
                                    }}>
                                        {item?.arNghiemThus?.map(itemNT => (
                                            <div style={style.w_100}>
                                                - Hạng mục&nbsp;
                                                {itemNT.arNtHangMuc ?? ""}:&nbsp;
                                                {itemNT?.arNtKetLuan ?? ""}
                                            </div>
                                        ))}
                                        
                                    </span>
                                    <div className="row-dotted-form-container" style={style.rowContainer}>
                                        <div style={style.subDottedRow.firstRow}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                        <div style={style.subDottedRow.row}></div>
                                    </div>
                                </div>
                                    
                                <div
                                    style={{
                                        ...style.flex,
                                        ...style.m_10,
                                    }}
                                >
                                    <div
                                        className={classes.borderNone}
                                        style={{
                                            ...style.w_20,
                                            ...style.textAlignLeft,
                                        }}
                                    >
                                        Tình trạng:
                                    </div>
                                    <div
                                        className={classes.borderNone}
                                        style={{
                                            ...style.w_40,
                                            ...style.textAlignLeft,
                                        }}
                                    >
                                        &nbsp;
                                        <input
                                            type="checkbox"
                                            className={classes.unHover}
                                            checked={
                                                item?.arNtTinhTrang 
                                                === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code
                                            }
                                        />&nbsp;
                                        Đạt yêu cầu
                                    </div>
                                    <div
                                        className={classes.borderNone}
                                        style={{
                                            ...style.w_40,
                                            ...style.textAlignLeft,
                                        }}
                                    >
                                        &nbsp;
                                        <input
                                            type="checkbox"
                                            className={classes.unHover}
                                            checked={
                                                item?.arNtTinhTrang 
                                                === appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code
                                            }
                                        />&nbsp;
                                        Chưa đạt yêu cầu
                                    </div>
                                </div>
                                <div style={style.tableThree}>
                                    <div style={style.flex}>
                                        <div style={{ ...style.colTable }}>Người sửa chữa</div>
                                        <div
                                            style={{ ...style.colTable, ...style.borderLeft }}
                                        >
                                            Người sử dụng thiết bị
                                        </div>
                                        <div
                                            style={{ ...style.colTable, ...style.borderLeft }}
                                        >
                                            Trưởng khoa/Phòng sử dụng
                                        </div>
                                        <div
                                            style={{ ...style.colTable, ...style.borderLeft }}
                                        >
                                            Cán bộ phòng HCQT
                                        </div>
                                        <div
                                            style={{ ...style.colTable, ...style.borderLeft }}
                                        >
                                            Trưởng phòng HCQT
                                        </div>
                                    </div>
                                    <div style={style.flex}>
                                        {/* Người sửa chữa */}
                                        <div
                                            style={{ 
                                                ...style.h_80px, 
                                                ...style.colTable 
                                            }}
                                        ></div>

                                        {/* Người sử dụng thiết bị */}
                                        <div
                                            style={{
                                                ...style.h_80px,
                                                ...style.colTable,
                                                ...style.borderLeft,
                                            }}
                                        ></div>

                                        {/* Trưởng khoa/phòng sử dụng */}
                                        <div
                                            style={{
                                                ...style.h_80px,
                                                ...style.colTable,
                                                ...style.borderLeft,
                                            }}
                                        ></div>

                                        {/* Cán bộ phòng HCQT */}
                                        <div
                                            style={{
                                                ...style.h_80px,
                                                ...style.colTable,
                                                ...style.borderLeft,
                                            }}
                                        ></div>

                                        {/* P.Trưởng phòng HCQT */}
                                        <div
                                            style={{
                                                ...style.h_80px,
                                                ...style.colTable,
                                                ...style.borderLeft,
                                            }}
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-36"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            {t("general.print")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
};

export default withStyles(styles)(BM01);
