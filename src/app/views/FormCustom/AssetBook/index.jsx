import React from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Title from "../Component/Title";
import { PaperComponent, convertNumberPrice } from "app/appFunction";
import PropTypes from "prop-types"
import { appConst } from "app/appConst";
import localStorageService from "app/services/localStorageService";
import Signature from "../Component/Signature";
import moment from "moment";

const style = {
    formPrint: {
        minWidth: "900px",
    },
    header: {
        display: "grid",
        gridTemplateColumns: "1fr 2fr 1fr"
    },
    tableCollapse: {
        width: "100%",
        border: "1px solid",
        borderCollapse: "collapse",
    },
    textAlignLeft: {
        textAlign: "left",
        padding: "4px",
    },
    fontBold: {
        fontWeight: "bold",
    },
    border: {
        border: "1px solid",
        padding: "4px",
    },
    borderHeader: {
        border: "1px solid",
        padding: "4px",
        textAlign: "center",
    },
    textAlignCenter: {
        textAlign: "center",
        padding: "4px",
    },
    textAlignRight: {
        textAlign: "right",
        padding: "4px",
    },
    mt_20: {
        marginTop: "20px",
    },
    w_5: {
        width: "5%",
    },
    w_10: {
        width: "10%",
    },
    w_15: {
        width: "15%",
    },
    w_20: {
        width: "20%",
    },
    w_30: {
        width: "30%",
    },
    w_41: {
        width: "41%",
    },
    w_27: {
        width: "27%",
    },
    subTitleProps: {
        fontSize: "11pt",
        textTransform: "uppercase",
        fontWeight: "bold",
    },
    fontItalic: {
        fontStyle: "italic",
    },
    signatureContainer: {
        display: "grid",
        gridTemplateColumns: "1fr 1fr 1fr",
    },
};


function AssetBookFormPrint(props) {
    let {
        open,
        t,
        item,
        assetClass = 1,
        handleClose = () => { }
    } = props;

    let dataAsset = []
    item?.map(itemAsset => {
        dataAsset = dataAsset.concat(itemAsset?.details || [])
    })
    const org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
    const orgAddress = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.ADDRESS_OF_ENTERPRISE);

    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();

        pri.document.write(content.innerHTML);

        pri.document.close();
        pri.focus();
        pri.print();
    };

    return (
        <Dialog
            open={open}
            PaperComponent={PaperComponent}
            maxWidth="lg"
            fullWidth
        >
            <DialogTitle className="pb-0 cursor-move" id="draggable-dialog-title">
                <span className="">Sổ tài sản</span>
            </DialogTitle>
            <iframe
                id="ifmcontentstoprint"
                style={{
                    height: "0px",
                    width: "0px",
                    position: "absolute",
                    print: { size: "auto", margin: "0mm" },
                }}
            ></iframe>

            <ValidatorForm
                className="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <div className="form-print form-print-landscape">
                        <div item xs={12} style={style.header}>
                            <div >
                                <h5 style={style.fontBold}>
                                    Đơn vị:&nbsp;{org?.name || "........."}
                                </h5>
                                <h5 style={style.fontBold}>
                                    {/* Địa chỉ:&nbsp;{orgAddress || "........."} */}
                                    Mã QHNS:..............................
                                </h5>
                            </div>
                            <div></div>
                            <div>
                                <div style={{
                                    ...style.fontBold,
                                    ...style.textAlignCenter,
                                    fontSize: "1rem"
                                }}>
                                    Mẫu số: S26-H
                                </div>
                                <div style={{
                                    ...style.textAlignCenter,
                                    fontSize: "1rem"
                                }}>
                                    (Ban hành theo Thông tư số 107/2017/TT-BTC
                                    Ngày 10/10/2017 của Bộ Tài chính)
                                </div>
                            </div>
                        </div>
                        <Title
                            title="Sổ theo dõi TSCĐ và công cụ, dụng cụ tại nơi sử dụng"
                        />
                        <div style={{ ...style?.textAlignCenter, ...style.fontItalic }}>
                            <div>Năm...........</div>
                            <div>Tên đơn vị, Phòng ban (hoặc người sử dụng): {props?.useDepartmentFilter?.text}</div>
                            <div>Loại công cụ, dụng cụ (hoặc nhóm công cụ, dụng cụ): {props?.assetGroupFilter ? props?.assetGroupFilter?.name : "............................"}</div>
                        </div>

                        <div style={style.mt_20}>
                            <table style={style.tableCollapse}>
                                <thead>
                                    <tr>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_5,
                                            }}
                                            rowSpan={3}
                                        >
                                            Ngày, tháng ghi sổ
                                        </th>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_10,
                                            }}
                                            colSpan={2}
                                        >
                                            Chứng từ
                                        </th>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_15,
                                            }}
                                            rowSpan={2}
                                        >
                                            Tên TSCDD và công cụ, dụng cụ
                                        </th>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_5,
                                            }}
                                            rowSpan={2}
                                        >
                                            ĐVT
                                        </th>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_30,
                                            }}
                                            colSpan={3}
                                        >
                                            Ghi tăng TSCĐ và công cụ, dụng cụ
                                        </th>
                                        <th
                                            style={{
                                                ...style.border,
                                                ...style.w_30,
                                            }}
                                            colSpan={4}
                                        >
                                            Ghi giảm TSCĐ và công cụ, dụng cụ
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style={style.border}>Số hiệu</th>
                                        <th style={style.border}>Ngày, tháng</th>
                                        <th style={style.border}>Số lượng</th>
                                        <th style={style.border}>Đơn giá</th>
                                        <th style={style.border}>Thành tiền</th>
                                        <th style={style.border}>Lý do</th>
                                        <th style={style.border}>Số lượng</th>
                                        <th style={style.border}>Đơn giá</th>
                                        <th style={style.border}>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {dataAsset?.map((row, index) => (
                                        <tr>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}></td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}>{row?.assetCode}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}>{moment(row?.voucherDate).format("DD/MM/YYYY")}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignLeft }}>{row?.assetName}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}>{row?.skuName}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}>{row?.increaseQuantity}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignRight }}>{convertNumberPrice(row?.increaseUnitPrice)}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignRight }}>{convertNumberPrice(row?.increaseOriginalCost)}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignLeft }}>{row?.reason}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignCenter }}>{row?.decreaseQuantity}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignRight }}>{convertNumberPrice(row?.decreaseUnitPrice)}</td>
                                            <td style={{ ...style.borderHeader, ...style.textAlignRight }}>{convertNumberPrice(row?.decreaseOriginalCost)}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <p>
                            - Sổ này có .... trang, đánh số từ trang 01 đến trang .... <br></br>
                            - Ngày mở sổ:...............................................................
                        </p>
                        <div style={{ ...style.fontItalic, textAlign: "right" }}>Ngày.....tháng.....năm</div>
                        <div style={style.signatureContainer}>
                            <Signature title="Người lập sổ" subTitle="Ký, họ tên" />
                            <Signature title="Kế toán trưởng" subTitle="Ký, họ tên" />
                            <Signature title="Thủ trưởng đơn vị" subTitle="Ký, họ tên, đóng dấu" />
                        </div>


                    </div>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-12"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            className="mr-16"
                            type="submit"
                        >
                            {t("In")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
}

AssetBookFormPrint.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired,
    t: PropTypes.node.isRequired,
}
export default AssetBookFormPrint;
