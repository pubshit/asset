import React from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import HeaderRight from "../Component/HeaderRight";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import HeaderLeft from "../Component/HeaderLeft";
import NationalName from "../Component/NationalName";
import DateToText from "../Component/DateToText";
import "../FormCustom.scss"

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  containFontSize: {
    fontSize: "18px"
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0 5em"
  },
  rowContainer: {
    paddingBottom: 10,
    fontSize: "18px"
  },
  py_2: {
    padding: "0 2%",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  tableRow: {
    border: "1px solid",
  },
  tableCell: {
    borderLeft: "1px solid",
    padding: "4px"
  },
  subTitle: {
    fontFamily:"'Times New Roman', Times, serif !important",
    fontSize: "0.975rem !important",
    fontWeight: "bold",
    textUnderlineOffset: "5px"
  }
};

export default function EquipmentNeeds(props) {
  const { t, open, handleClose, itemEdit } = props;
  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    
    pri.document.write("<style>" + `
    @media print {
      @page {
        margin: 7mm;
      }
    }` + "</style>");
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };
  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("Phiếu xác định nhu cầu cần mua")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        class="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className=" dialog-print">
          <div className="form-print" style={style.containFontSize}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div style={{ marginTop: "20px" }}>
                    <HeaderLeft unit={itemEdit?.currentUser?.org?.name} unitProps={{ textDecoration: "none", fontSize: "18px"}}/>
                    <div style={{...style.subTitle}}>{`Đơn vị: ${itemEdit?.requestDepartmentName}`}</div>
                </div>
                <div>
                    <NationalName titleProps={{ fontSize: "18px !important" }}/>
                    <div style={{ margin: "16px 0 0 0", textAlign: "center"}}>
                        <DateToText noUppercase isLocation date={new Date()}/>
                    </div>
                </div>
            </div>

            <div style={{display: "flex", flexDirection: "column", justifyContent:"center"}}>
              <Title title={"Phiếu xác Định Nhu Cầu Thiết Bị"} titleProps={{fontSize: "23px"}}/>
              <div style={{display: "flex", margin: "10px 0 20px 9em"}}>
                <div><b>Kính gửi: </b></div>
                <div>
                  <div> - Ban Giám đốc Bệnh viện</div> 
                  <div> - {itemEdit?.requestDepartmentName}</div> 
                </div>
              </div>
            </div>

       
              <div style={{marginLeft: "2em"}}>
                <div style={style.rowContainer}>
                    {"- Căn cứ vào nhu cầu khám chữa bệnh công tác."}
                </div>
                <div style={style.rowContainer}>
                    {"- Căn cứ vào định hướng phát triển của bệnh viện, của khoa /phòng."}
                </div>
                <div style={style.rowContainer}>
                    &nbsp;&nbsp;Khoa, phòng: &nbsp;&nbsp;  
                    {itemEdit?.requestDepartmentName}
                </div>
                <div style={style.rowContainer}>
                    {"Có nhu cầu sử dụng TTBYT:"}
                </div>
              </div>

              <div>
                <table style={style.table}>
                  <thead>
                    <tr style={style.tableRow}>
                      <th style={{...style.tableCell, width: "5%"}}>{"TT"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Tên thiết bị dụng cụ"}</th>
                      <th style={{...style.tableCell, width: "10%"}}>{"ĐVT"}</th>
                      <th style={{...style.tableCell, width: "10%"}}>{"Số lượng hiện có"}</th>
                      <th style={{...style.tableCell, width: "10%"}}>{"Số lượng đề xuất"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Thuyết minh trang bị (kỹ thuật áp dụng)"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Ghi chú"}</th>
                    </tr>
                  </thead>

                  <tbody>
                    {
                      itemEdit?.purchaseProducts?.map((item, index) => {
                        return (
                          <tr style={style.tableRow} key={index}>
                            <td style={{ ...style.tableCell, textAlign: "center"}}>{index + 1}</td>
                            <td style={style.tableCell}>{item?.productName}</td>
                            <td style={{...style.tableCell, textAlign: "center"}}>{item?.skuName}</td>
                            <td style={{ ...style.tableCell, textAlign: "center"}}>{item?.approvedQuantity}</td>
                            <td style={{ ...style.tableCell, textAlign: "center"}}>{item?.quantity}</td>
                            <td style={style.tableCell}>{item?.productSpecs}</td>
                            <td style={style.tableCell}>{item?.note}</td>
                          </tr>
                        )
                      })
                    }
                    <tr style={style.tableRow}>
                      <td style={{ ...style.tableCell, textAlign: "center"}}> </td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}><b>Tổng cộng</b></td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}> </td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}>
                       { itemEdit?.purchaseProducts?.reduce((total, obj) => total + obj.approvedQuantity, 0) || ""}
                      </td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}>
                       { itemEdit?.purchaseProducts?.reduce((total, obj) => total + obj.quantity, 0) || "" }
                      </td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}> </td>
                      <td style={{ ...style.tableCell, textAlign: "center"}}> </td>
                    </tr>
                  </tbody>
                </table>
                <div style={{ fontSize: "18px", margin: "10px 0 0 2em" }} className="mb-40">
                  Vậy khoa/phòng kính đề nghị Ban Giám đốc Bệnh viện xét duyệt.
                </div>
              </div>

            <div style={style.flexSpaceBetween}>
              <Signature
                sign={"Ký, ghi rõ họ tên"}
                title="Trưởng phòng VTTBYT"
                name={itemEdit?.receptionDepartmentName}
                titleProps={{fontSize: "18px"}}        
                signProps={{fontSize: "16px"}}       
                nameProps={{fontSize: "18px"}}       
              />
              <Signature 
                sign={"Ký, ghi rõ họ tên"} 
                title="Lãnh đạo Khoa, Phòng"
                name={itemEdit?.requestDepartmentName}  
                titleProps={{fontSize: "18px"}}        
                signProps={{fontSize: "16px"}}       
                nameProps={{fontSize: "18px"}}               
              />
            </div>

            <div style={{textAlignCenter: "center", marginTop: "20px"}}>
              <Signature
                sign={""}
                titleProps={{fontSize: "18px"}}    
                title={"Giám đốc"}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("general.print")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
