import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import localStorageService from "app/services/localStorageService";
import Signature from "../Component/Signature";
import HeaderLeft from "../Component/HeaderLeft";
import HeaderRight from "../Component/HeaderRight";
import Title from "../Component/Title";
import { appConst } from "../../../appConst";
import { convertNumberPrice } from "app/appFunction";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  textAlignRight: {
    textAlign: "right",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_2: {
    border: "1px solid",
    width: "2%",
    textAlign: "center",
  },
  w_3: {
    border: "1px solid",
    width: "3%",
    textAlign: "center",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
    textAlign: "center",
  },
  w_25: {
    border: "1px solid",
    width: "25%",
    textAlign: "center",
  },
  w_40: {
    border: "1px solid",
    width: "40%",
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: "0.975rem",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 10%",
  },
  border: {
    border: "1px solid",
    fontSize: "0.975rem",
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
  },
  alignItems: (textAlign) => {
    return textAlign ? textAlign : { alignItems: "flex-start" }
  },
};
export default class C50HD extends Component {
  state = {
    type: 2,
    rowsPerPage: 1,
    page: 0,
    totalElements: 0,
    departmentId: "",
    asset: {},
    isView: false,
  };
  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    this.setState({
      ...this.props.item,
    });
  }
  componentDidMount() { }

  render() {
    let { open, t, item } = this.props;
    let currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
    console.log(item)
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("AssetTransfer.assetTransferPrint")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
        ></iframe>

        <ValidatorForm
          ref="form"
          class="validator-form-scroll-dialog"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className=" dialog-print">
            <Grid>
              <div style={style.textAlignCenter} className="form-print form-print-landscape">
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  {/* <HeaderLeft unit={currentUser?.org?.name} code={item?.code} /> */}
                  <div>
                    <div
                      style={{
                        ...style.flexColumn,
                        ...style.alignItems(),// center/flex-start
                      }}
                    >
                      <div style={{ fontWeight: "bold" }}>Đơn vị:..........................</div>
                      <div style={{ fontWeight: "bold" }}>Mã QHNS:....................</div>
                    </div>
                  </div>
                  <HeaderRight model={"C50-HD"} />
                </div>
                <Title
                  title={"BIÊN BẢN GIAO NHẬN TSCĐ"}
                  date={new Date()}
                  serialNumber={"dotted"}
                />
                {/* date,seriaNumbe = "dotted" để hiện ..., truyền date dạng YYYY-MM-DD để hiện ngày */}
                <div style={style.textAlignLeft}>
                  <div>
                    Căn cứ Quyết định số: ..... ngày ..... tháng ..... năm......
                    của ..... về việc bàn giao TSCĐ
                  </div>
                </div>
                <div style={style.textAlignLeft}>
                  <div>Ban giao nhận TSCĐ gồm: </div>
                </div>
                <div style={style.textAlignLeft}>
                  <ul mr-4 className="form-print-list" style={{ margin: 0 }}>
                    <li>
                      <span>Đại diện bên giao:&nbsp; </span>
                      <span>
                        Ông/Bà:&nbsp;
                        {item?.handoverRepresentative
                          ? item?.handoverRepresentative
                          : "..........."}
                      </span>
                      <span>&nbsp;Chức vụ:............... </span>

                      <span>Đơn vị cung cấp:&nbsp; {item?.supplier?.name}</span>
                    </li>
                    <li>
                      <span>Đại diện bên nhận:&nbsp; </span>
                      <span>
                        Ông/Bà:&nbsp;
                        {item?.receiverPerson?.displayName
                          ? item?.receiverPerson?.displayName
                          : "..........."}
                        &nbsp;
                      </span>
                      <span>Chức vụ:..............</span>

                      <span>
                        Phòng ban tiếp nhận:&nbsp;
                        {item?.receiverDepartment?.name}
                      </span>
                    </li>
                  </ul>
                </div>
                <div style={style.textAlignLeft}>
                  <div>Địa điểm giao nhận TSCĐ:........................</div>
                </div>
                <div style={style.textAlignLeft}>
                  <div>Xác nhận việc giao nhận TSCĐ như sau:</div>
                </div>
                <div style={{ textAlign: "center" }}>
                  <div>
                    {
                      <table style={style.table}>
                        <tr>
                          <th rowSpan="2">STT</th>
                          <th style={style.w_25} rowSpan="2">
                            Tên, ký hiệu, quy cách, cấp hạng TSCĐ
                          </th>
                          <th style={style.w_8} rowSpan="2">
                            Số hiệu TSCĐ
                          </th>
                          <th style={style.w_8} rowSpan="2">
                            Nước sản xuất
                          </th>
                          <th style={style.w_8} rowSpan="2">
                            Năm sản xuất
                          </th>
                          <th style={style.w_7} rowSpan="2">
                            Năm đưa vào SD
                          </th>
                          <th style={style.w_7} rowSpan="2">
                            Công suất (diện tích thiết kế)
                          </th>
                          <th style={style.w_40} colSpan="5">
                            Tính nguyên giá tài sản cố định
                          </th>
                          <th rowSpan="2">Tài liệu kỹ thuật kèm theo</th>
                        </tr>
                        <tr>
                          <th style={style.w_5}>Giá mua</th>
                          <th style={style.w_7}>Chi phí vận chuyển</th>
                          <th style={style.w_7}>Chi phí chạy thử</th>
                          <th style={style.w_3}>...</th>
                          <th style={style.w_10}>Nguyên giá TSCĐ</th>
                        </tr>
                        {item.assetVouchers !== null
                          ? item.assetVouchers?.map((row, index) => {
                            return (
                              <tbody>
                                <tr>
                                  <td style={{ ...style.border, ...style.textAlignCenter }}>{index !== null ? index + 1 : ""}</td>
                                  <td style={{ ...style.border, ...style.textAlignLeft }}>
                                    {row.asset !== null ? row.asset?.name : ""}
                                  </td>
                                  <td style={{ ...style.border, ...style.textAlignCenter }}>
                                    {row.asset !== null
                                      ? row.asset?.model
                                      : ""}
                                  </td>
                                  <td style={{ ...style.border, ...style.textAlignCenter }}>
                                    {row.asset !== null
                                      ? row.asset?.madeIn
                                      : ""}
                                  </td>
                                  <td style={{ ...style.border, ...style.textAlignCenter }}>
                                    {row.asset !== null
                                      ? row.asset?.yearOfManufacture
                                      : ""}
                                  </td>
                                  <td style={{ ...style.border, ...style.textAlignCenter }}>
                                    {row.asset !== null
                                      ? row.asset?.yearPutIntoUse
                                      : ""}
                                  </td>

                                  <td style={style.border}></td>
                                  <td style={{ ...style.border, ...style.textAlignRight }}>
                                    {row.asset !== null
                                      ? row.asset?.unitPrice
                                      : ""}
                                  </td>
                                  <td style={style.border}></td>
                                  <td style={style.border}></td>
                                  <td style={style.border}></td>
                                  <td style={{ ...style.border, ...style.textAlignRight }}>
                                    {convertNumberPrice(row.asset?.originalCost) || 0}
                                  </td>
                                  <td style={style.border}></td>
                                </tr>
                              </tbody>
                            );
                          })
                          : ""}

                        <tr>
                          <td style={style.sum} colSpan={7}>Tổng cộng:</td>

                          <td style={{ ...style.border, ...style.textAlignRight }}>
                            {
                              item.assetVouchers !== null
                                ? convertNumberPrice(item.assetVouchers?.reduce((acc, itemAsset) => acc + itemAsset?.asset?.unitPrice, 0)) : ""
                            }
                          </td>
                          <td style={style.border}></td>
                          <td style={style.border}></td>
                          <td style={style.border}></td>
                          <td style={{ ...style.border, ...style.textAlignRight }}>
                            {
                              item.assetVouchers !== null
                                ? convertNumberPrice(item.assetVouchers?.reduce((acc, itemAsset) => acc + itemAsset?.asset?.originalCost, 0)) : ""
                            }
                          </td>
                          <td style={style.border}></td>
                        </tr>
                      </table>
                    }
                  </div>
                  <p style={style.title} className="form-print-title">
                    DỤNG CỤ, PHỤ TÙNG KÈM THEO
                  </p>
                  <div>
                    {
                      <table style={style.table} className="table-report">
                        <tr>
                          <th style={style.w_2}>STT</th>
                          <th style={style.w_25}>
                            Tên, quy cách dụng cụ, phụ tùng
                          </th>
                          <th style={style.w_8}>Đơn vị tính</th>
                          <th style={style.w_8}>Số lượng</th>
                          <th style={style.w_8}>Giá trị</th>
                        </tr>

                        <tbody>
                          <tr style={{ height: "2em" }}>
                            <td style={style.border}></td>
                            <td style={style.border}></td>
                            <td style={style.border}></td>
                            <td style={style.border}></td>
                            <td style={style.border}></td>
                          </tr>
                          {/*{item?.assetVouchers?.map((row, index) => {*/}
                          {/*  return (*/}
                          {/*    <tbody>*/}
                          {/*      <tr style={{ height: "2em" }}>*/}
                          {/*        <td style={style.border}></td>*/}
                          {/*        <td style={style.border}></td>*/}
                          {/*        <td style={style.border}></td>*/}
                          {/*        <td style={style.border}></td>*/}
                          {/*        <td style={style.border}></td>*/}
                          {/*      </tr>*/}
                          {/*    </tbody>*/}
                          {/*  );*/}
                          {/*})}*/}
                        </tbody>
                      </table>
                    }
                  </div>

                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-around",
                      marginTop: "20px",
                    }}
                  >
                    <Signature
                      titleProps={{
                        marginTop: 5,
                      }}
                      title="Thủ trưởng đơn vị"
                      note="Bên nhận"
                      sign="Ký, họ tên, đóng dấu"
                    />
                    <Signature
                      titleProps={{
                        marginTop: 5,
                      }}
                      title="Kế toán trưởng"
                      note="Bên nhận"
                      sign="Ký, họ tên"
                    />
                    <Signature
                      titleProps={{
                        marginTop: 5,
                      }}
                      title="người nhận"
                      sign="Ký, họ tên"
                    />
                    <Signature
                      titleProps={{
                        marginTop: 5,
                      }}
                      title="người giao"
                      sign="Ký, họ tên"
                    />
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
