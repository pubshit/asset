import React from "react";
import { Button, Dialog, DialogActions, makeStyles } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import DateToText from "../Component/DateToText";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import HeaderLeft from "../Component/HeaderLeft";
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import { convertNumberPrice } from "app/appFunction";
import { PaperComponent } from "../../Component/Utilities";
import Mustache from 'mustache';

const currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
const organizationName = currentUser?.org?.name;

const style = {
    title: {
        fontSize: "0.975rem",
        fontWeight: "bold",
        marginBottom: "1em",
        marginTop: "1.5em",
    },
    flexSpaceBetween: {
        display: "flex",
        justifyContent: "space-between",
    },
    rowContainer: {
        fontSize: "1.07rem",
    },
    py_2: {
        padding: "0 2%",
    },
    table: {
        width: "100%",
        borderCollapse: "collapse",
        border: "1px solid",
        fontSize: "1rem"
    },
    tableRow: {
        border: "1px solid",
    },
    tableCell: {
        borderLeft: "1px solid",
        padding: "4px",
        fontSize: "0.85rem"
    },
    textDataContainer: {
        display: "inline-block",
        position: "relative",
    },
    textDataUnderline: {
        position: "absolute",
        // top: 0, left: 0, right: 0, bottom: "3px",
        // borderBottom: "1px dotted"
    },
    a4Container: {
        width: "21cm",
        margin: "auto",
        fontSize: "18px",
        padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
    },
};

const useStyles = makeStyles({
    textDataUnderline: {
        bottom: "5px !important",
    }
})

export default function DCTSTB_bvVanDinh(props) {
    const { t, dataPhieu, handleClose } = props;
    const classes = useStyles();
    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();

        pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
        pri.document.write(content.innerHTML);

        pri.document.close();
        pri.focus();
        pri.print();
    };

    const newDate = new Date(dataPhieu?.issueDate)
    const day = String(newDate?.getUTCDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());

    const addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");

    let dataView = {
        dataPhieu: {
            ...dataPhieu,
            assetVouchers: dataPhieu?.assetVouchers?.map((item, index) => ({
                ...item,
                index,
            }))
        },
        organizationName,
        day,
        month,
        year,
        addressOfEnterprise
    }


    return (
        <Dialog open={true} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                <span className="mb-20">{t("Phiếu điều chuyển tài sản thiết bị")}</span>
            </DialogTitle>
            <iframe
                id="ifmcontentstoprint"
                style={{
                    height: "0px",
                    width: "0px",
                    position: "absolute",
                    print: { size: "auto", margin: "0mm" },
                }}
            ></iframe>

            <ValidatorForm
                class="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <div className="form-print">
                        <div>
                            <div style={{ display: "flex", justifyContent: "space-between" }}>
                                <div style={{
                                    display: "flex",
                                    alignItems: "center",
                                    flexDirection: "column",
                                    width: "50%"
                                }}>
                                    <div style={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center"
                                    }}>
                                        <div
                                            style={{
                                                display: "flex",
                                                flexDirection: "column",
                                                textAlign: "center"
                                            }}
                                        >
                                            <div style={{
                                                fontWeight: "bold",
                                                margin: "0px",
                                                textTransform: "uppercase",
                                                textUnderlineOffset: "5px"
                                            }}>
                                                {organizationName}
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{textTransform: "uppercase",}}>
                                        {dataPhieu?.assetVouchers[0]?.assetManagementDepartmentName || ""}
                                        <div style={{
                                            width: "70%",
                                            margin: "0 auto",
                                            borderBottom: "1px solid #000",
                                        }}></div>
                                    </div>
                                </div>
                                <div style={{
                                    width: "60%",
                                    display: "flex",
                                    flexDirection: "column",
                                    alignItems: "end",
                                }}>
                                    <div>
                                        <div
                                            style={{
                                                display: "flex",
                                                flexDirection: "column",
                                                alignItems: "center",
                                            }}
                                        >
                                            <div
                                                style={{
                                                    margin: "0px",
                                                    fontWeight: "bold",
                                                    textUnderlineOffset: "5px",
                                                }}
                                            >
                                                CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
                                            </div>
                                            <div style={{
                                                margin: "0px",
                                                fontWeight: "bold",
                                            }}>
                                                <div>
                                                    Độc lập - Tự do - Hạnh phúc

                                                    <div style={{
                                                        width: "70%",
                                                        margin: "0 auto",
                                                        borderBottom: "1px solid #000",
                                                    }}></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ margin: "16px 0 0 0", textAlign: "right" }}>
                                        <div style={{
                                            fontStyle: "italic",
                                        }}>{addressOfEnterprise}, ngày {day}  tháng {month}  năm {year}</div>
                                    </div>
                                </div>
                            </div>

                            <div
                                style={{
                                    textAlign: "center",
                                    marginBottom: 30,
                                    marginTop: 30,
                                    fontWeight: "bold",
                                    textTransform: "uppercase",
                                    lineHeight: 1.3
                                }}
                                className="form-print-title"
                            >
                                Biên bản điều chuyển<br></br>tài sản - công cụ dụng cụ
                            </div>

                            <div style={{ fontSize: "1.07rem", marginBottom: 8 }}>
                                <div style={{ fontSize: "1.07rem", }}>
                                    &nbsp;&nbsp; Căn cứ chức năng, nhiệm vụ và quyền hạn của Giám đốc {organizationName}.
                                </div>
                                <div style={{ fontSize: "1.07rem", }}>
                                    &nbsp;&nbsp; - Căn cứ vào tính hình sử dụng tài sản trang thiết bị y tế, công cụ dụng cụ và nhu cầu sử dụng của:
                                    <br></br>
                                    <div style={{ display: "flex" }}> &nbsp;&nbsp; - Khoa/Phòng: <div style={{ minWidth: 140 }}>&nbsp;{dataPhieu?.handoverDepartmentName || "..".repeat(20)}</div>&nbsp;<i>  (bên giao)</i></div>
                                    <div style={{ display: "flex" }}>&nbsp;&nbsp; - Khoa/Phòng: <div style={{ minWidth: 140 }}>&nbsp;{dataPhieu?.receiverDepartmentName || "..".repeat(20)}</div>&nbsp;<i>  (bên nhận)</i></div>
                                    &nbsp;&nbsp; - Giám đốc bệnh viện đa khoa Vân Đình quyết định điều chuyển một số tài sản, công cụ dụng cụ sau đây từ Khoa, Phòng: {dataPhieu?.handoverDepartmentName || "..".repeat(20)}.
                                    <br></br>
                                    &nbsp;&nbsp; - Về Khoa/Phòng: {dataPhieu?.receiverDepartmentName || "..".repeat(20)}.
                                </div>
                            </div>
                        </div>
                        <div style={{ fontSize: "1.07rem", }}>
                            <table style={{
                                width: "100%",
                                borderCollapse: "collapse",
                                border: "1px solid",
                                fontSize: "1rem"
                            }}>
                                <thead style={{ border: "1px solid" }}>
                                    <tr style={{ border: "1px solid", }}>
                                        <th style={{
                                            borderLeft: "1px solid",
                                            padding: "4px",
                                            fontSize: "0.85rem",
                                            width: "2%"
                                        }}>{"TT"}</th>
                                        <th style={{
                                            borderLeft: "1px solid",
                                            padding: "4px",
                                            fontSize: "0.85rem",
                                            width: "15%"
                                        }}>{"Tên tài sản, nhãn hiệu, qui cách mẫu mã"}</th>
                                        <th style={{
                                            borderLeft: "1px solid",
                                            padding: "4px",
                                            fontSize: "0.85rem",
                                            width: "6%"
                                        }}>{"Đơn vị tính"}</th>
                                        <th style={{
                                            borderLeft: "1px solid",
                                            padding: "4px",
                                            fontSize: "0.85rem",
                                            width: "5%"
                                        }}>{"Số lượng"}</th>
                                        <th style={{
                                            borderLeft: "1px solid",
                                            padding: "4px",
                                            fontSize: "0.85rem",
                                            width: "10%"
                                        }}>{"Tình trạng tài sản"}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        dataPhieu?.assetVouchers?.map((item, index) => {
                                            return (
                                                <tr style={{ border: "1px solid", }} key={index}>
                                                    <td style={{
                                                        borderLeft: "1px solid",
                                                        padding: "4px",
                                                        fontSize: "0.85rem",
                                                        textAlign: "center"
                                                    }}>{index + 1}</td>
                                                    <td style={style.tableCell}>{item?.assetName ?? ""}</td>
                                                    <td style={{
                                                        borderLeft: "1px solid",
                                                        padding: "4px",
                                                        fontSize: "0.85rem",
                                                        textAlign: "center"
                                                    }}>{item?.assetUnitName ?? ""}</td>
                                                    <td style={{
                                                        borderLeft: "1px solid",
                                                        padding: "4px",
                                                        fontSize: "0.85rem",
                                                        textAlign: "center"
                                                    }}>{item?.quantity ?? "1"}</td>
                                                    <td style={{
                                                        borderLeft: "1px solid",
                                                        padding: "4px",
                                                        fontSize: "0.85rem"

                                                    }}>{item?.assetStatusName ?? ""}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                            &nbsp;&nbsp; - Đã tổ chức bàn giao cho khoa/phòng và đủ điều kiện vận hành, hoạt động (sử dụng) phục vụ công tác chuyên môn: {"..".repeat(20)}
                            <br></br>
                            &nbsp;&nbsp; Từ hồi  &nbsp; ngày &nbsp;&nbsp;&nbsp; tháng &nbsp;&nbsp;&nbsp; năm 20
                        </div>

                        <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr" }} className="mt-20">
                            <div>
                                <div className="signature-container" style={{
                                    textAlign: "center",
                                    fontSize: "14px",
                                    paddingBottom: "80px",
                                }}>
                                    <div className="signature-title" style={{
                                        textTransform: "uppercase",
                                        fontWeight: "bold",
                                        fontSize: 12,
                                        marginTop: 20
                                    }}>
                                        Thủ trưởng đơn vị
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="signature-container" style={{
                                    textAlign: "center",
                                    fontSize: "14px",
                                    paddingBottom: "80px",
                                }}>
                                    <div className="signature-title" style={{
                                        textTransform: "uppercase",
                                        fontWeight: "bold",
                                        fontSize: 12,
                                        marginTop: 20
                                    }}>
                                        Kế toán tài sản
                                    </div>
                                    <div style={{
                                        fontStyle: "italic"
                                    }}>
                                        (Ký, ghi rõ họ tên)
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="signature-container" style={{
                                    textAlign: "center",
                                    fontSize: "14px",
                                    paddingBottom: "80px",
                                }}>
                                    <div className="signature-title" style={{
                                        textTransform: "uppercase",
                                        fontWeight: "bold",
                                        fontSize: 12,
                                        marginTop: 20
                                    }}>
                                        Đại diện phòng VT-TBYT
                                    </div>
                                    <div style={{
                                        fontStyle: "italic"
                                    }}>
                                        (Ký, ghi rõ họ tên)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr" }} className="mt-20">
                            <div>
                                <div className="signature-container" style={{
                                    textAlign: "center",
                                    fontSize: "14px",
                                    paddingBottom: "80px",
                                }}>
                                    <div className="signature-title" style={{
                                        textTransform: "uppercase",
                                        fontWeight: "bold",
                                        fontSize: 12,
                                        marginTop: 20
                                    }}>
                                        Đại diện khoa, phòng giao tài sản
                                    </div>
                                    <div style={{
                                        fontStyle: "italic"
                                    }}>
                                        (Ký, ghi rõ họ tên)
                                    </div>
                                    <div
                                        style={{
                                            fontWeight: "bold",
                                            marginTop: 100,
                                            textTransform: "capitalize",
                                        }}
                                    >
                                        {dataPhieu?.handoverPersonName}
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="signature-container" style={{
                                    textAlign: "center",
                                    fontSize: "14px",
                                    paddingBottom: "80px",
                                }}>
                                    <div className="signature-title" style={{
                                        textTransform: "uppercase",
                                        fontWeight: "bold",
                                        fontSize: 12,
                                        marginTop: 20
                                    }}>
                                        Đại diện khoa, phòng nhận tài sản
                                    </div>
                                    <div style={{
                                        fontStyle: "italic"
                                    }}>
                                        (Ký, ghi rõ họ tên)
                                    </div>
                                    <div
                                        style={{
                                            fontWeight: "bold",
                                            marginTop: 100,
                                            textTransform: "capitalize",
                                        }}
                                    >
                                        {dataPhieu?.receiverPersonName}
                                    </div>
                                </div>
                            </div>
                            <div></div>
                        </div>
                    </div>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-36"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            {t("general.print")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog >
    );
}
