import { appConst } from "../../../appConst";
import {
  convertDepartmentNameForSignature,
  convertNumberPriceRoundUp,
  convertToAcronym,
  formatTimestampToDate,
  getUserInformation,
} from "../../../appFunction";

export const receiptPrintData = (item) => {
  const { currentUser, addressOfEnterprise, organization } =
    getUserInformation();
  let date = item?.item?.suggestDate
    ? new Date(item?.item?.suggestDate)
    : new Date();
  const day = String(date?.getDate())?.padStart(2, "0");
  const month = String(date?.getMonth() + 1)?.padStart(2, "0");
  const year = String(date?.getFullYear());

  const signReceiptDepartmentName = convertDepartmentNameForSignature(
    item?.item?.receiptDepartmentName,
    ["phòng", "p."]
  );
  const acronymReceiptDepartmentName = convertToAcronym(
    signReceiptDepartmentName
  );

  const signDepartmentName = convertDepartmentNameForSignature(
    item?.item?.departmentName,
    ["phòng", "p."]
  );
  const acronymDepartmentName = convertToAcronym(signDepartmentName);

  return {
    ...item?.item,
    currentUser,
    day,
    month,
    year,
    addressOfEnterprise,
    suggestDate: formatTimestampToDate(item?.item?.suggestDate),
    affiliatedEstablishment: organization?.affiliatedEstablishment
      ? organization?.affiliatedEstablishment + "/"
      : "",
    receiptDepartmentName: item?.item?.receiptDepartmentName || "..".repeat(6),
    isShowCode:
      item?.item?.status ===
      appConst.listStatusSuggestSuppliesObject.DANG_THONG_KE.indexOrder,
    signReceiptDepartmentName: acronymReceiptDepartmentName,
    signDepartmentName: acronymDepartmentName,
    assetVouchers: (
      item?.item?.voucherDetails || item?.item?.assetVouchers
    )?.map((asset, i) => {
      return {
        ...asset,
        code: asset.code || asset.productCode,
        name: asset.name || asset.productName,
        index: i + 1,
        dxRequestQuantity: asset.dxRequestQuantity || asset.quantityOfVoucher,
        amount: convertNumberPriceRoundUp(asset?.amount) || 0,
        unitPrice: convertNumberPriceRoundUp(asset?.unitPrice) || 0,
        thanhTien:
          convertNumberPriceRoundUp(
            asset?.unitPrice *
              (asset.dxRequestQuantity || asset.quantityOfVoucher)
          ) || 0,
      };
    }),
    totalQuantity: (
      item?.item?.voucherDetails || item?.item?.assetVouchers
    )?.reduce((total, item) => total + item?.quantity, 0),
    totalQuantityOfVoucher: (
      item?.item?.voucherDetails || item?.item?.assetVouchers
    )?.reduce((total, item) => total + item?.quantityOfVoucher, 0),
    totalRequestQuantity: (
      item?.item?.voucherDetails || item?.item?.assetVouchers
    )?.reduce(
      (total, item) =>
        total + (item?.dxRequestQuantity || item?.quantityOfVoucher),
      0
    ),
    totalPrice: convertNumberPriceRoundUp(
      (item?.item?.voucherDetails || item?.item?.assetVouchers)?.reduce(
        (total, item) =>
          total +
          item?.unitPrice *
            (item?.dxRequestQuantity || item?.quantityOfVoucher),
        0
      )
    ),
  };
};
