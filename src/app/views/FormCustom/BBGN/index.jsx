import React, { useEffect, useState } from "react";
import {
  Dialog,
  Button,
  DialogActions
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import { convertNumberPrice, convertNumberPriceRoundUp } from "app/appFunction";
import localStorageService from "app/services/localStorageService";
import Signature from "../Component/Signature";
import { appConst } from "app/appConst";
import DateToText from "../Component/DateToText";
import Mustache from 'mustache';

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  textAlignCenter: {
    textAlign: "center"
  },
  title: {
    fontWeight: 'bold',
    marginBottom: '0px'
  },
  text: {
    fontWeight: 'bold',
    marginTop: '0px'
  },
  textAlignLeft: {
    textAlign: "left",
  },
  textAlignRight: {
    textAlign: "right",
  },
  forwarder: {
    textAlign: "left",
    fontWeight: 'bold'
  },
  table: {
    width: '100%',
    border: '1px solid',
    borderCollapse: 'collapse'
  },
  w_3: {
    border: '1px solid',
    width: '3%',
    textAlign: 'center'
  },
  w_5: {
    border: '1px solid',
    width: '5%',
    textAlign: 'center'
  },
  w_7: {
    border: '1px solid',
    width: '7%',
    textAlign: 'center'
  },
  w_9: {
    border: '1px solid',
    width: '9%',
    textAlign: 'center'
  },
  w_11: {
    border: '1px solid',
    width: '11%',
  },
  w_20: {
    border: '1px solid',
    width: '20%',
  },
  mt_25minus: {
    marginTop: -25
  },
  border: {
    border: '1px solid',
    padding: "0 5px"
  },
  name: {
    border: '1px solid',
    paddingLeft: "5px",
    textAlign: 'left'
  },
  sum: {
    border: '1px solid',
    paddingLeft: "5px",
    fontWeight: 'bold',
    textAlign: 'left'
  },
  represent: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 12%'
  },
  pos_absolute: {
    position: "absolute",
    top: "100%",
  },
  signContainer: {
    display: "flex",
    justifyContent: "space-around",
  },
  textCenter: {
    display: "flex",
    justifyContent: "center",
    fontWeight: 'bold',
    width: "33%"
  },
  pt_40: {
    paddingTop: 40
  },
  bold: {
    fontWeight: "bold",
  },
  alignCenter: {
    alignItems: "center"
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
    padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
  }
}

const currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
const organizationName = currentUser?.org?.name;
let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);

export default function ReceiptOfDeliveryForm(props) {
  const [html, setHtml] = useState("")
  let { open, handleClose, t, item, assetClass } = props;
  let now = new Date(item?.issueDate);
  const day = now?.getDate() < 10 ? `0${now.getDate()}` : now.getDate();
  const month = now?.getMonth() + 1 < 10 ? `0${now.getMonth() + 1}` : now.getMonth() + 1;
  const year = now?.getFullYear();

  let currentUser = localStorageService.getSessionItem("currentUser");
  let addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");

  let isClassType = {
    isCCDC: assetClass === appConst.assetClass.CCDC,
    isTSCD: assetClass === appConst.assetClass.TSCD,
    isVT: assetClass === appConst.assetClass.VT,
  }

  let arrOriginalCost = [];

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };
  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return convertNumberPrice(sum);
  }

  const getTypeNameByCode = () => {
    if (assetClass === appConst.assetClass.CCDC) {
      return {
        name: "CCDC",
        code: "Mã CCDC",
        nameInTable: "Tên CCDC"
      }
    }
    if (assetClass === appConst.assetClass.TSCD) {
      return {
        name: "tài sản",
        code: "Mã tài sản",
        nameInTable: "Tên tài sản"
      }
    }
    if (assetClass === appConst.assetClass.VT) {
      return {
        name: "vật tư",
        code: "Mã vật tư",
        nameInTable: "Tên vật tư"
      }
    }
    return {
      name: "",
      code: "",
      name: ""
    }
  }

  const renderTemplate = async () => {
    try {
      const res = await fetch("/assets/form-print/receiptOfDeliveryFormTemplate.txt")
      const data = await res.text();

      let dataView = {
        organizationName,
        titleName,
        addressOfEnterprise,
        day,
        month,
        year,
        typeName: getTypeNameByCode()?.name,
        tableAssetCode: getTypeNameByCode()?.code,
        tableAssetName: getTypeNameByCode()?.nameInTable,
        dataPrint: {
          ...item,
          arrOriginalCost: convertNumberPrice(item?.assetVouchers?.reduce((total, item) => total + item?.asset?.originalCost, 0)) || 0,
          assetVouchers: item?.assetVouchers?.map((i, x) => {
            return {
              ...i,
              index: x + 1,
              madeIn: i?.asset?.madeIn || i?.asset?.assetManufacturerName,
              yearOfManufacture: i?.asset?.yearOfManufacture || i?.asset?.assetYearOfManufacture,
              yearPutIntoUse: i?.asset?.yearPutIntoUse || i?.assetYearPutIntoUser,
              quantity: i?.quantity || 1,
              originalCost: convertNumberPriceRoundUp(i?.asset?.originalCost || 0)
            }
          })
        }
      }

      setHtml(Mustache.render(data, dataView))
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    renderTemplate();
  }, [])

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth={true}>
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        <span className="mb-20">{t('TransferToAnotherUnit.transferSlip')}</span>
      </DialogTitle>
      <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute' }} title="ifmcontentstoprint"></iframe>
      <ValidatorForm onSubmit={handleFormSubmit} className="validator-form-scroll-dialog">
        <DialogContent id='divcontents' className="dialog-print">
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-16"
              onClick={() => handleClose()}
            >
              {t('general.cancel')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
            >
              {t('In')}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}

