import Title from "../Component/Title";
import { convertNumberPrice, convertNumberPriceRoundUp } from "../../../appFunction";
import Signature from "../Component/Signature";
import React, { useEffect, useState } from "react";
import { appConst } from "../../../appConst";
import DateToText from "../Component/DateToText";
import localStorageService from "app/services/localStorageService";
import Mustache from 'mustache';

const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  textWordWrap: {
    // wordWrap: "break-word", 
    whiteSpace: "normal",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    minWidth: "76px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60px",
  },
  minWidth: {
    minWidth: "76px",
    maxWidth: "76px"
  },
  code: {
    border: "1px solid",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
  },
  textUppercase: {
    textTransform: "uppercase"
  },
  textCapitalize: {
    textTransform: "capitalize"
  },
  justifyContent: {
    display: "flex",
    justifyContent: "space-between",
    gap: "100px"
  },
  borderBottom: {
    borderBottom: "1px dotted #000",
    flexGrow: "1",
    transform: "translateY(-0.5rem)"
  }

};

export const AssetInventorySheet = props => {
  const [html, setHtml] = useState("")
  const { item, t } = props;
  let arrAccountantQuantity = [];
  let arrAccountantOriginalCost = [];
  let arrInventoryQuantity = [];
  let arrInventoryOriginalCost = [];
  let arrDifferenceQuantity = [];
  let arrDifferenceOriginalCost = [];
  let arrNegativeDifferenceOriginalCost = [];
  let arrPositiveDifferenceOriginalCost = [];
  let arrNegativeDifferenceQuantity = [];
  let arrPositiveDifferenceQuantity = [];
  let arrUnitPrice = [];

  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b, 0);
    return convertNumberPrice(sum);
  };

  let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);

  const newDate = new Date(item?.inventoryCountDate)
  const day = String(newDate?.getUTCDate())?.padStart(2, '0');
  const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
  const year = String(newDate?.getFullYear());

  let dataView = {
    item: {
      ...item,
      inventoryCountPersonsCol1: item?.inventoryCountPersons?.map((i, index) => {
        if (index < Math.ceil(item?.inventoryCountPersons?.length / 2)) {
          i.displayText = `${index + 1}: ${i?.personName} - ${i?.departmentName} ${appConst.listHdChiTietsRole.find(iRole => iRole.code === i.role)?.name ? " - " + appConst.listHdChiTietsRole.find(iRole => iRole.code === i.role)?.name : ""}`
          return { ...i }
        } else {
          return {}
        }
      }),
      inventoryCountPersonsCol2: item?.inventoryCountPersons?.map((i, index) => {
        if (index >= Math.ceil(item?.inventoryCountPersons?.length / 2)) {
          i.displayText = `${index + 1}: ${i?.personName} - ${i?.departmentName} ${appConst.listHdChiTietsRole.find(iRole => iRole.code === i.role)?.name ? " - " + appConst.listHdChiTietsRole.find(iRole => iRole.code === i.role)?.name : ""}`
          return { ...i }
        } else {
          return {}
        }
      }),
      assets: item?.assets?.map((row, index) => {
        let {
          accountantQuantity,
          accountantOriginalCost,
          inventoryQuantity,
          inventoryOriginalCost,
          asset,
        } = row;
        let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
        let totalQuantity = inventoryQuantity - accountantQuantity;
        let unitPrice = asset.originalCost / 1
        arrAccountantOriginalCost.push(accountantOriginalCost);
        arrAccountantQuantity.push(accountantQuantity);
        arrInventoryOriginalCost.push(inventoryOriginalCost);
        arrInventoryQuantity.push(inventoryQuantity);
        arrDifferenceQuantity.push(totalQuantity);
        arrDifferenceOriginalCost.push(totalOriginalCost);
        arrUnitPrice.push(unitPrice)
        if (totalOriginalCost > 0) {
          arrPositiveDifferenceOriginalCost.push(totalOriginalCost)
        }
        else {
          arrNegativeDifferenceOriginalCost.push(totalOriginalCost)
        }
        if (totalQuantity > 0) {
          arrPositiveDifferenceQuantity.push(totalQuantity)
        }
        else {
          arrNegativeDifferenceQuantity.push(totalQuantity)
        }
        return {
          ...row,
          index: index + 1,
          unitPrice: convertNumberPriceRoundUp(unitPrice) || 0,
          accountantOriginalCost: convertNumberPriceRoundUp(accountantOriginalCost) || 0,
          inventoryOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
          totalOriginalCost: convertNumberPriceRoundUp(totalOriginalCost) || 0,
          totalQuantity: convertNumberPriceRoundUp(totalQuantity) || 0,
        }
      })

    },
    day,
    month,
    year,
    titleName,
    arrUnitPrice: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.asset?.originalCost / 1), 0)) || 0,
    arrAccountantOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.accountantOriginalCost), 0)) || 0,
    arrInventoryOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.inventoryOriginalCost), 0)) || 0,
    arrPositiveDifferenceQuantity: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.inventoryQuantity - item?.asset?.accountantQuantity), 0)) || 0,
    arrPositiveDifferenceOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((sum, item) => sum + (item?.inventoryOriginalCost - item?.accountantOriginalCost), 0)) || 0,
  }
  const renderTemplate = async () => {
    try {
      const res = await fetch("/assets/form-print/assetInventorySheetTemplate.txt")
      const data = await res.text();
      setHtml(Mustache.render(data, dataView))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    renderTemplate()
  }, [])

  return (
    <>
      <div className="form-print form-print-landscape">
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </div>
    </>
  );
};