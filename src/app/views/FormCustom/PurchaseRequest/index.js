import {printStyleText} from "../constant";
import {convertDepartmentNameForSignature, getUserInformation, removeAccents} from "../../../appFunction";
import {useContext} from "react";
import AppContext from "../../../appContext";
import {LIST_ORGANIZATION} from "../../../appConst";


export const purchaseRequestPrintData = (item) => {
  const {addressOfEnterprise} = getUserInformation();
  const {currentOrg} = useContext(AppContext);
  const newDate = new Date(item?.requestDate)
  const day = String(newDate?.getDate())?.padStart(2, '0');
  const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
  const year = String(newDate?.getFullYear());
  let data = {...item};
	const receptionDepartmentNameConverted = convertDepartmentNameForSignature(
		item?.receptionDepartment?.name,
		["Phòng ", "P. "],
		"Trưởng phòng "
	)
	
  return {
    itemEdit: {
      ...data,
      evaluationContent: item?.evaluationContent?.replaceAll("\n", "<br/>"),
      confirmationContent: item?.confirmationContent?.replaceAll("\n", "<br/>"),
			bv199Signature: receptionDepartmentNameConverted,
      requestDepartmentName: item?.requestDepartmentName || "..".repeat(10),
      purchaseProducts: item?.purchaseProducts?.map((i, x) => {
        return {
          ...i,
          index: x + 1,
          productName: i?.alterProductName || i?.productName,
        }
      })
    },
    addressOfEnterprise,
      day: item?.requestDate ? day : ".......",
    month: item?.requestDate ? month : ".......",
    year: item?.requestDate ? year : ".......",
    totalavailableQuantity: item?.purchaseProducts?.reduce((total, obj) => total + obj?.availableQuantity, 0) || "",
    totalquantity: item?.purchaseProducts?.reduce((total, obj) => total + obj.quantity, 0) || "",
    style: printStyleText,
  }
}

const convertDepartmentNameForBVC = (name, prefix) => {
  let string = "";
  const department = "phòng";
  // Chuẩn hóa chuỗi gốc và từ khóa
  let normalizedName = removeAccents(name).toLowerCase();
  let normalizedDepartment = removeAccents(department).toLowerCase();

  if (!name) {return null;}
  if (normalizedName?.includes(normalizedDepartment)) {
    // Tìm vị trí của từ khóa trong chuỗi đã chuẩn hóa
    let startIndex = normalizedName.indexOf(normalizedDepartment);

    // Nếu tìm thấy từ khóa, loại bỏ nó khỏi chuỗi gốc
    if (startIndex !== -1) {
      // Tìm vị trí bắt đầu của từ khóa trong chuỗi gốc
      let originalStartIndex = normalizedName.toLowerCase().indexOf(normalizedDepartment.toLowerCase());
      let originalEndIndex = originalStartIndex + normalizedDepartment.length + 1;

      // Loại bỏ từ khóa khỏi chuỗi gốc
      string = name.substring(0, originalStartIndex) + name.substring(originalEndIndex);
    }
    // Loại bỏ khoảng trắng thừa ở đầu và cuối chuỗi kết quả
    string = string.trim();
  } else {
    string = name;
  }

  return `${prefix}${string}`
}
