import { appConst } from "../../../appConst";
import {
  convertMoney,
  convertNumberPrice,
  convertNumberPriceRoundUp,
  getUserInformation,
} from "../../../appFunction";

export const getInventoryCountVoucherPrintData = (data, assetClass) => {
  let { listDetailItems, item } = data;
  const { organization } = getUserInformation();
  const newDate = new Date(item?.inventoryCountDate);
  const day = String(newDate?.getDate())?.padStart(2, "0");
  const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
  const year = String(newDate?.getFullYear());
  const currentMonth = new Date().getMonth();
  const currentYear = new Date().getFullYear();
  const titleName = organization;
  const isTSCD = assetClass === appConst.assetClass.TSCD;
  const isCCDC = assetClass === appConst.assetClass.CCDC;
  const assets = handleConvertAsset(item.assets, isTSCD);
  const bvcTAssetType = handleGetBVCAssetType(isTSCD);

  const renderRole = (role) => {
    switch (role) {
      case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
        return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name;
      case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
        return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name;
      case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
        return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name;
      default:
        break;
    }
  };

  return {
    day,
    month,
    currentMonth,
    year,
    currentYear,
    titleName,
    organization,
    isTSCD,
    isCCDC,
    assets,
    bvcTAssetType,
    bcvAssets: handleRenderAssetsByAssetGroupForBVC(item?.items),
    department: item?.department?.text,
    inventoryCountDate: item?.inventoryCountDate
      ? `ngày ${day} tháng ${month} năm ${year}`
      : "",
    inventoryCountPersons: item?.inventoryCountPersons
      ? [
          ...item?.inventoryCountPersons?.sort((a, b) => a?.role - b?.role),
        ]?.map((i, x) => {
          return {
            ...i,
            index: x + 1,
            role: renderRole(i?.role),
            position: i?.position || "..".repeat(10),
          };
        })
      : [],
    sumAccountantQuantity: item?.assets?.reduce(
      (total, item) => total + (item?.accountantQuantity || 1),
      0
    ),
    sumInventoryQuantityQuantity: item?.assets?.reduce(
      (total, item) => total + (item?.inventoryQuantity || 1),
      0
    ),
    sumAccountantOriginalCost:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) => total + (item?.accountantOriginalCost || 0),
          0
        )
      ) || 0,
    sumAccountantCarryingAmount:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) => total + (item?.accountantCarryingAmount || 0),
          0
        )
      ) || 0,
    sumInventoryOriginalCost:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) => total + (item?.inventoryOriginalCost || 0),
          0
        )
      ) || 0,
    sumInventoryCarryingAmount:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) => total + (item?.inventoryCarryingAmount || 0),
          0
        )
      ) || 0,
    sumDifferenceQuantity: item?.assets?.reduce(
      (total, item) =>
        total + (item?.inventoryQuantity - item?.accountantQuantity || 0),
      0
    ),
    sumDifferenceOriginalCost:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) =>
            total +
            (item?.inventoryOriginalCost - item?.accountantOriginalCost || 0),
          0
        )
      ) || 0,
    sumDifferenceCarryingAmount:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (total, item) =>
            total +
            (item?.inventoryCarryingAmount - item?.accountantCarryingAmount ||
              0),
          0
        )
      ) || 0,
    arrUnitPrice:
      convertNumberPriceRoundUp(
        item?.assets?.reduce(
          (sum, item) => sum + item?.asset?.originalCost / 1,
          0
        )
      ) || 0,
    sumIsBiggerThanZeroQty: Math.abs(item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity - item?.accountantQuantity > 0 ? item?.inventoryQuantity - item?.accountantQuantity : 0), 0)),
    sumIsLessThanZeroQty: Math.abs(item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity - item?.accountantQuantity < 0 ? item?.inventoryQuantity - item?.accountantQuantity : 0), 0)),
    sumIsBiggerThanZeroPrice: convertNumberPriceRoundUp(Math.abs(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost - item?.accountantOriginalCost > 0 ? item?.inventoryOriginalCost - item?.accountantOriginalCost : 0), 0))) || 0,
    sumIsLessThanZeroQtyPrice: convertNumberPriceRoundUp(Math.abs(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost - item?.accountantOriginalCost < 0 ? item?.inventoryOriginalCost - item?.accountantOriginalCost : 0), 0))),
    item: {
      ...item,
      inventoryCountPersonsCol1: item?.inventoryCountPersons?.map(
        (i, index) => {
          if (index < Math.ceil(item?.inventoryCountPersons?.length / 2)) {
            i.displayText = `${index + 1}: ${i?.personName} - ${
              i?.departmentName
            } ${
              appConst.listHdChiTietsRole.find((iRole) => iRole.code === i.role)
                ?.name
                ? " - " +
                  appConst.listHdChiTietsRole.find(
                    (iRole) => iRole.code === i.role
                  )?.name
                : ""
            }`;
            return { ...i };
          } else {
            return {};
          }
        }
      ),
      inventoryCountPersonsCol2: item?.inventoryCountPersons?.map(
        (i, index) => {
          if (index >= Math.ceil(item?.inventoryCountPersons?.length / 2)) {
            i.displayText = `${index + 1}: ${i?.personName} - ${
              i?.departmentName
            } ${
              appConst.listHdChiTietsRole.find((iRole) => iRole.code === i.role)
                ?.name
                ? " - " +
                  appConst.listHdChiTietsRole.find(
                    (iRole) => iRole.code === i.role
                  )?.name
                : ""
            }`;
            return { ...i };
          } else {
            return {};
          }
        }
      ),
      assets: item?.assets?.map((row, index) => {
        let {
          accountantQuantity,
          accountantOriginalCost,
          inventoryQuantity,
          inventoryOriginalCost,
          asset,
        } = row;
        let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
        let totalQuantity = inventoryQuantity - accountantQuantity;
        let unitPrice = asset?.originalCost / 1;

        return {
          ...row,
          index: index + 1,
          unitPrice: convertNumberPriceRoundUp(unitPrice) || 0,
          accountantOriginalCost:
            convertNumberPriceRoundUp(accountantOriginalCost) || 0,
          inventoryOriginalCost:
            convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
          totalOriginalCost: convertNumberPriceRoundUp(totalOriginalCost) || 0,
          totalQuantity: convertNumberPriceRoundUp(totalQuantity) || 0,
        };
      }),
    },
    //du lieu phieu in bv hoenhai nhieu phong ban
    listDetailItems: listDetailItems?.map((i) => {
      return {
        ...i,
        assets: i?.assets?.map((item, x) => {
          let usedCondition = item?.usedCondition;
          const isGood =
            appConst.OBJECT_USED_CONDITION.TOT.code === usedCondition || "";
          const isMedium =
            appConst.OBJECT_USED_CONDITION.TRUNG_BINH.code === usedCondition ||
            "";
          const isBroken =
            appConst.OBJECT_USED_CONDITION.HONG.code === usedCondition || "";
          return {
            ...item,
            index: x + 1,
            isGood,
            isMedium,
            isBroken,
            asset: {
              ...item?.asset,
              quantity: item?.asset?.quantity || 1,
            },
          };
        }),
      };
    }),
  };
};

const handleConvertAsset = (assets, isTSCD) => {
  let newArray = assets?.map((row, index) => {
    let {
      accountantQuantity,
      accountantOriginalCost,
      inventoryQuantity,
      inventoryOriginalCost,
      accountantCarryingAmount,
      inventoryCarryingAmount,
      usedCondition,
    } = row;
    const isGood =
      appConst.OBJECT_USED_CONDITION.TOT.code === usedCondition || "";
    const isMedium =
      appConst.OBJECT_USED_CONDITION.TRUNG_BINH.code === usedCondition || "";
    const isBroken =
      appConst.OBJECT_USED_CONDITION.HONG.code === usedCondition || "";
    let isExcess =
      accountantQuantity - inventoryQuantity > 0
        ? accountantQuantity - inventoryQuantity || 0
        : 0;
    let isDeficiency =
      accountantQuantity - inventoryQuantity <= 0
        ? Math.abs(accountantQuantity - inventoryQuantity || 0)
        : 0;
    let unitPrice = isTSCD
      ? inventoryOriginalCost
      : inventoryOriginalCost / inventoryQuantity;
    let qty = inventoryQuantity - accountantQuantity;
    let price = inventoryOriginalCost - accountantOriginalCost;
    let mathAbsQty = Math.abs(qty);
    let mathAbsPrice = convertNumberPrice(Math.abs(price));
    let isBiggerThanZero = qty > 0
    let isLessThanZero = qty < 0
    let isBiggerPrice = inventoryOriginalCost > accountantOriginalCost
    let isLessPrice = inventoryOriginalCost < accountantOriginalCost
    return {
      ...row,
      index: index + 1,
      isGood,
      isMedium,
      isBroken,
      isExcess,
      isDeficiency,
      unitPrice: convertNumberPriceRoundUp(unitPrice) || 0,
      accountantOriginalCost:
        convertNumberPriceRoundUp(accountantOriginalCost) || 0,
      inventoryOriginalCost:
        convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
      accountantCarryingAmount:
        convertNumberPriceRoundUp(accountantCarryingAmount) || 0,
      inventoryCarryingAmount:
        convertNumberPriceRoundUp(inventoryCarryingAmount) || 0,
      accountantQuantity: accountantQuantity || 1,
      inventoryQuantity: inventoryQuantity || 1,
      differenceQuantity: inventoryQuantity - accountantQuantity || 0,
      differenceOriginalCost:
        convertNumberPriceRoundUp(
          inventoryOriginalCost - accountantOriginalCost
        ) || 0,
      differenceCarryingAmount:
        convertNumberPriceRoundUp(
          inventoryCarryingAmount - accountantCarryingAmount
        ) || 0,
      carryingAmountRate: (
        (inventoryCarryingAmount / inventoryOriginalCost || 0) * 100
      ).toFixed(2),
      isBiggerThanZeroQty: isBiggerThanZero ? mathAbsQty : 0,
      isBiggerThanZeroPrice: isBiggerPrice ? mathAbsPrice : 0,
      isLessThanZeroQty: isLessThanZero ? mathAbsQty : 0,
      isLessThanZeroQtyPrice: isLessPrice ? mathAbsPrice : 0,
    };
  });

  newArray?.sort((a, b) => {
    if (a.asset?.code < b.asset?.code) return -1;
    if (a.asset?.code > b.asset?.code) return 1;
    return 0;
  })

  return newArray?.map((x, index) => ({ ...x, index: index + 1 }));
};

export const handleConvertBVCPrintData = (data, assetClass) => {
  let {item} = data;
  const {organization} = getUserInformation();
  const newDate = new Date(item?.inventoryCountDate);
  const day = String(newDate?.getDate())?.padStart(2, "0");
  const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
  const year = String(newDate?.getFullYear());
  const currentMonth = new Date().getMonth();
  const currentYear = new Date().getFullYear();
  const titleName = organization;
  const isTSCD = assetClass === appConst.assetClass.TSCD;
  const isCCDC = assetClass === appConst.assetClass.CCDC;
  const assets = handleConvertAsset(item.assets, isTSCD);
  const bvcTAssetType = handleGetBVCAssetType(isTSCD);
  const sumInventoryOriginalCost = item?.items?.reduce(
    (total, asset) => Number(total) + Number(asset.totalOriginalCost || 0),
    0
  )

  return {
    ...item,
    day,
    month,
    currentMonth,
    year,
    currentYear,
    titleName,
    organization,
    isTSCD,
    isCCDC,
    assets,
    bvcTAssetType,
    bcvAssets: handleRenderAssetsByAssetGroupForBVC(item?.items),
    department: item?.department?.text,
    inventoryCountDate: item?.inventoryCountDate
      ? `ngày ${day} tháng ${month} năm ${year}`
      : "",
    sumInventoryOriginalCost: convertMoney(sumInventoryOriginalCost) || 0
  }
}

const handleGetBVCAssetType = (isTSCD) => {
  return isTSCD ? "Trang thiết bị" : "Y dụng cụ";
};

const handleRenderAssetsByAssetGroupForBVC = (items, parentIndex) => {
  let assets = items?.map((item, index) => {
    if (!item?.quantity || item?.quantity === 0) {
      return '';
    } else {
      return `
        <tr style="border: 1px solid;">
          <td colspan="9" style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; font-weight: bold;">
            ${parentIndex ? `${parentIndex}.` : ""}${index + 1}. ${item.name}
          </td>
        </tr>
        
        ${renderAsset(item.assets)}
        ${item?.children ? handleRenderAssetsByAssetGroupForBVC(item?.children, index + 1) : ""}
        
        <tr style="border: 1px solid;">
          <td colspan="7" style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; font-weight: bold;">
            Tổng ${item.name}
          </td>
          <td colspan="2" style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: right; font-weight: bold;">
            ${convertMoney(item.totalOriginalCost) || 0}
          </td>
        </tr>
      `
    }
  })?.filter(Boolean)?.join('');
  return  (`
      ${assets || ""}
  `)
}

const renderAsset = (assets) => {
  if (assets?.length <= 0) {return "";}

  return assets?.map((item, index) => {
    return (`
      <tr style="border: 1px solid;">
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">
          ${index + 1}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">
          ${item.name || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">
          ${item.unitName || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">
          ${item.code || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">
          ${item.model || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">
          ${item.manufacturerName || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">
          ${item.quantity || ""}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: right;">
          ${convertNumberPrice(item.unitPrice) || 0}
        </td>
        <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: right;">
          ${convertMoney(item.originalCost) || 0}
        </td>
      </tr>
    `)
  })?.filter(Boolean)?.join('');
}
