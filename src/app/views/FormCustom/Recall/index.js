import moment from "moment/moment";
import {
  convertDepartmentNameForSignature,
  convertNumberPriceRoundUp,
  convertToAcronym,
  getTheHighestRole,
} from "../../../appFunction";
import {appConst, LIST_ORGANIZATION} from "../../../appConst";
import {useContext} from "react";
import AppContext from "../../../appContext";


export const handleConvertRecallPrintData = (item) => {
  let {currentOrg} = useContext(AppContext);
  let roles = getTheHighestRole();

  const newDate = new Date(item?.suggestDate)
  const day = item?.suggestDate ? String(newDate?.getUTCDate())?.padStart(2, '0') : ".... ";
  const month = item?.suggestDate ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : ".... ";
  const year = item?.suggestDate ? String(newDate?.getFullYear()) : "...... ";
  const currentPrefixArray = ["phòng", "p."]
  const prefixConverted = {
    [LIST_ORGANIZATION.BV_VAN_DINH.code]: "P.",
  }
  let convertedReceiptDepartment = convertDepartmentNameForSignature(
    item?.receiptDepartmentName || item?.receiverDepartmentName,
    currentPrefixArray,
    prefixConverted[currentOrg?.printCode]
  );
  const acronymReceiptDepartment = convertToAcronym(convertedReceiptDepartment);

  return {
    ...item,
    ...roles,
    day,
    month,
    year,
    phoPhongQuanLy: convertedReceiptDepartment,
    signDepartmentName: acronymReceiptDepartment,
    code: item?.code || item?.voucherCode || "..".repeat(10),
    suggestDate: item?.suggestDate ? moment(item?.suggestDate).format('DD/MM/YYYY') : ".... / .... / ......",
    issueDate: item?.issueDate ? moment(item?.issueDate).format('DD/MM/YYYY') : ".... / .... / ......",
    receiptDepartmentName: item?.receiptDepartmentName || item?.receiverDepartmentName || "..".repeat(20),
    departmentName: item?.departmentName || "..".repeat(20),
    assetVouchers: item?.assetVouchers?.map((ts, index) => {
      return {
        ...ts,
        index: index + 1,
        originalCost: convertNumberPriceRoundUp(ts?.originalCost || (ts?.price || ts?.unitPrice) * (ts?.quantity || 1)),
        price: convertNumberPriceRoundUp(ts?.price || ts?.unitPrice),
        soHieu: getSerialAndModel(ts),
        tinhTrangSuDung: appConst.listUsageState.find(x => x.code === ts.assetUsageState)?.name,
        madeInAndManufacturer: getManuFactureAndMadeIn(ts),
      }
    }),

    totalRequestQuantity: item?.assetVouchers?.reduce((total, item) => total + (item?.dxRequestQuantity || 0), 0) || 0,
    totalQuantity: item?.assetVouchers?.reduce((total, item) => total + (item?.quantity || 0), 0) || 0,
    totalPrice: convertNumberPriceRoundUp(
			item?.assetVouchers?.reduce(
				(total, item) => total + Number(
					item?.originalCost || (item?.price || item?.unitPrice) * (item?.quantity || 1)
				)
				, 0
			)
		),
  }
}

const getSerialAndModel = (item) => {
  if (!item?.model && !item?.serialNumber) {
    return "";
  } else if (!item?.model) {
    return item?.serialNumber
  } else if (!item?.serialNumber) {
    return item?.model
  } else {
    return item?.model + ", " + item?.serialNumber
  }
}

const getManuFactureAndMadeIn = (item) => {
  if (!item?.madeIn && !item?.manufacturerName) {
    return "";
  } else if (!item?.madeIn) {
    return item?.manufacturerName
  } else if (!item?.manufacturerName) {
    return item?.madeIn
  } else {
    return item?.madeIn + ", " + item?.manufacturerName
  }
}
