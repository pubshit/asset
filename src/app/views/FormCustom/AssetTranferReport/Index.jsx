const { default: NationalName } = require("../Component/NationalName");
const { default: Signature } = require("../Component/Signature");
const { default: Title } = require("../Component/Title");
import { getTheHighestRole } from "app/appFunction";
import { printStyles } from "../constant";

const AssetTranferReport = () => {
  const { departmentUser, organization, addressOfEnterprise } =
    getTheHighestRole();

  return (
    <>
      <div
        style={{
          ...printStyles.d_flex,
          ...printStyles.justify_content.space_between,
        }}
      >
        <div>
          <div>{organization?.org?.name}</div>
          <div>{departmentUser?.name}</div>
        </div>
        <div>
          <NationalName />
        </div>
      </div>
      <div style={printStyles.text_align.right}>
        {addressOfEnterprise}, ngày ... tháng ... năm 20 ...
      </div>
      <Title
        title="Biên bản điều chuyển"
        subTitle="Tài sản - Công cụ dụng cụ"
        titleProps={{ ...printStyles.font_size._24px }}
        subTitleProps={{
          ...printStyles.text_align.center,
          ...printStyles.font_size._23px,
          ...printStyles.font_weight._600,
          ...printStyles.margin.bottom._30px,
          ...printStyles.line_height._16px,
          ...printStyles.text_transform.uppercase
        }}
      />
      <div style={printStyles.text_indent._40px}>
        Căn cứ chức năng, nhiệm vụ và quyền hạn của Giám đốc Bệnh viện da khoa
        Vân Đình.
      </div>
      <div style={printStyles.text_indent._40px}>
        - Căn cú vào tình hình sử dụng tài sản trang thiết bị y tế, công cụ dụng
        cụ và nhu cầu sử dụng của
      </div>
      <div style={printStyles.text_indent._40px}>
        - Khoa/ Phòng: ......................................<i>(bên giao)</i>
      </div>
      <div style={printStyles.text_indent._40px}>
        - Khoa/ Phòng: ......................................<i>(bên nhận)</i>
      </div>
      <div style={printStyles.text_indent._40px}>
        - Giám đốc bệnh viện đa khoa Vân Đình quyết định điều chuyển một số tài
        sản, công cụ dụng cụ sau đây từ Khoa, Phòng: ...........................
      </div>
      <div style={printStyles.text_indent._40px}>- Về Khoa/ Phòng: ................................</div>
      <table style={{ ...printStyles.tableCollapse }}>
        <thead>
          <tr>
            <th style={printStyles.border}>TT</th>
            <th style={printStyles.border}>Tên tài sản, nhãn hiệu, quy cách mẫu mã</th>
            <th style={printStyles.border}>Đơn vị tính</th>
            <th style={printStyles.border}>Số lượng</th>
            <th style={printStyles.border}>Tình trạng tài sản</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style={printStyles.border}>1</td>
            <td style={printStyles.border}></td>
            <td style={printStyles.border}>cái</td>
            <td style={printStyles.border}>2</td>
            <td style={printStyles.border}></td>
          </tr>
        </tbody>
      </table>
      <div style={{...printStyles.margin.top._10px, ...printStyles.text_indent._40px}}>
        - Đã tổ chức bàn giao cho khoa/phòng và đủ điều kiện vận hành, hoạt động
        (sử dụng) phục vụ công tác chuyên môn: ..........................
      </div>
      <div style={printStyles.text_indent._40px}>Từ hồi ..... ngày ... tháng ... năm 20 ...</div>
      <div style={{...printStyles.d_flex, ...printStyles.justify_content.space_around}}>
        <Signature title="THỦ TRƯỞNG ĐƠN VỊ" />
        <Signature
          title="ĐẠI DIỆN PHÒNG VT-TBYT"
          subTitle="Ký, ghi rõ họ tên"
        />
      </div>
      <div style={{...printStyles.d_flex, ...printStyles.justify_content.space_around}}>
        <div style={printStyles.width._25persent}>
          <Signature
            title="ĐẠI DIỆN KHOA, PHÒNG GIAO TÀI SẢN"
            subTitle="Ký, ghi rõ họ tên"
          />
        </div>
        <div style={printStyles.width._25persent}>
          <Signature
            title="ĐẠI DIỆN KHOA, PHÒNG NHẬN TÀI SẢN"
            subTitle="Ký, ghi rõ họ tên"
          />
        </div>
        <div style={printStyles.width._25persent}>
          <Signature title="KẾ TOÁN TÀI SẢN" subTitle="Ký, ghi rõ họ tên" />
        </div>
      </div>
    </>
  );
};

export default AssetTranferReport;
