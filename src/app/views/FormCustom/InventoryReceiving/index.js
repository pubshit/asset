import {
    convertMoney,
    convertNumberPrice,
    convertNumberToWords,
    getUserInformation,
    spliceString
} from "app/appFunction";
import moment from "moment/moment";
import {appConst, LIST_ORGANIZATION} from "../../../appConst";


export const inventoryReceivingPrintData = (item, currentOrg) => {
    const {organization, addressOfEnterprise} = getUserInformation();
    const date = new Date(item?.issueDate);
    const billDate = new Date(item?.billDate);
    const day = String(date?.getDate())?.padStart(2, '0');
    const dayOfBillDate = billDate ? String(billDate?.getDate())?.padStart(2, '0') : "......";
    const month = String(date?.getMonth() + 1)?.padStart(2, '0');
    const monthOfBillDate = billDate ? String(billDate?.getMonth() + 1)?.padStart(2, '0') : "......";
    const year = String(date?.getFullYear());
    const yearOfBillDate = billDate ? String(billDate?.getFullYear()) : "........";
    const time = `${String(new Date().getHours())}:${String(new Date().getMinutes())}`;
    const moneyType = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
      ? appConst.TYPES_MONEY.SPACE.code
      : appConst.TYPES_MONEY.COMMA.code;
    const convertMoneyFunc = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
      ? convertMoney
      : convertNumberPrice

    const getManuFactureAndMadeIn = (item) => {
        if (!item?.madeIn && !item?.product?.manufacturer?.name && !item?.assetManufacturerName && !item?.assetMadeIn) {
            return "";
        } else if (!item?.product?.madeIn && !item?.assetMadeIn) {
            return (item?.product?.manufacturer?.name || item?.assetManufacturerName || "")
        } else if (!item?.product?.manufacturer?.name && !item?.assetManufacturerName) {
            return (item?.product?.madeIn || item?.assetMadeIn || "")
        } else {
            return (item?.product?.madeIn || item?.assetMadeIn || "")
              + ", "
              + (item?.product?.manufacturer?.name || item?.assetManufacturerName || "")
        }
    }

    const getSerialAndModel = (item) => {
        if (!item?.assetModel && !item?.assetSerialNumber && !item?.product?.serialNumber && !item?.product?.model) {
            return "";
        } else if (!item?.assetModel && !item?.product?.model) {
            return (item?.product?.serialNumber || item?.assetSerialNumber || "")
        } else if (!item?.assetSerialNumber && !item?.product?.serialNumber) {
            return (item?.product?.model || item?.assetModel || "")
        } else {
            return (item?.assetModel || item?.product?.model || "")
              + ", "
              + (item?.product?.serialNumber || item?.assetSerialNumber || "")
        }
    }

    const calculateTotal = (key) => item?.voucherDetails?.reduce(
      (total, item) => Number(total) + Number(item[key]),
      0
    );

    return {
        orgName: organization?.org?.name || ".........",
        ...organization,
        ...item,
        day,
        month,
        year,
        time,
        dayOfBillDate,
        monthOfBillDate,
        yearOfBillDate,
        billNumber: item?.billNumber || "..............",    
        handoverPersonName: spliceString(
          item?.handoverRepresentative
          || item?.handoverPerson?.displayName
          || item?.receiverPerson?.displayName
        ),
        departmentName: item?.receiverDepartment?.name,
        supplierName: item?.supplierName || item?.supplier?.name,
        supplierAddress: item?.supplierAddress || item?.supplier?.address,
        voucherDetails: item?.voucherDetails?.map((i, x) => {
            return {
                ...i,
                index: x + 1,
                price: convertMoney(i?.price, moneyType) || 0,
                amount: convertMoney(i?.amount, moneyType) || 0,
                expiryDate: i?.expiryDate ? moment(i?.expiryDate).format("DD/MM/YYYY") : "",
            }
        }),
        totalAmount: convertMoney(calculateTotal("amount"), moneyType) || 0,
        totalAmountToWords: convertNumberToWords(calculateTotal("amount"), moneyType) || "Không",
        totalPrice: convertMoney(calculateTotal("price"), moneyType),
        totalQuantityOfVoucher: calculateTotal("quantityOfVoucher"),
        totalQuantity: calculateTotal("quantity"),
        item: {
            ...item,
            voucherDetails: item?.voucherDetails?.map((i, index) => {
                return {
                    ...i,
                    index: index + 1,
                    status: i?.status || "",
                    productName: i?.productName || i?.product?.name,
                    getSerialAndModel: getSerialAndModel(i),
                    getManuFactureAndMadeIn: getManuFactureAndMadeIn(i),
                    amount: convertMoney(i?.amount, moneyType) || 0,
                    expiryDate: i?.expiryDate ? moment(i?.expiryDate).format("DD/MM/YYYY") : "..../..../.....",
                }
            })
        },
        billDate: item?.billDate ? moment(item?.billDate).format("DD/MM/YYYY") : "..../..../.....",
        organization,
        addressOfEnterprise,
    }
}
