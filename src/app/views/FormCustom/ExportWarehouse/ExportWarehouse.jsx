import React, { Component } from "react";
import {
    Dialog,
    Button,
    DialogActions,
    DialogContent
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';
import localStorageService from "app/services/localStorageService";
import { appConst } from "app/appConst";
import Mustache from 'mustache';
import { convertNumberPrice, convertNumberToWords } from "app/appFunction";

function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

export default class ExportWarehouse extends Component {
    state = {
        html: ""
    };

    handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();
        pri.document.write("<style>" + `
        @page {
            margin: 2cm 1.5cm 2cm 3cm;
        }
        ` + "</style>");
        let htmlContent = Mustache.render(content.innerHTML, this.state);
        pri.document.write(htmlContent);

        pri.document.close();
        pri.focus();
        pri.print();
    };

    componentDidMount() {
        this.setState({
            ...this.props.item,
            voucherDetails: this.props.item?.voucherDetails?.map((i, index) => {
                return {
                    ...i,
                    index: index + 1,
                    price: convertNumberPrice(i?.price),
                    amount: convertNumberPrice(i?.amount),
                }
            }),
            totalAmount: convertNumberPrice(this.props.item?.voucherDetails?.reduce((sum, i) => (sum + i?.amount), 0) || 0),
            totalPrice: convertNumberPrice(this.props.item?.voucherDetails?.reduce((sum, i) => (sum + i?.price), 0) || 0),
            totalQuantity: this.props.item?.voucherDetails?.reduce((sum, i) => (sum + i?.quantity), 0) || 0,
            numberToWordPrice: convertNumberToWords(this.props.item?.voucherDetails?.reduce((sum, i) => (sum + i?.amount), 0) || 0) || "Không"
        }, () => {
            this.renderTemplate();
        });
    }

    renderTemplate = async () => {
        try {

            let { item } = this.props;

            const res = await fetch("/assets/form-print/xuatKho_bvVanDinh.txt");
            const data = await res.text();

            let date = new Date(item?.inputDate || item?.issueDate) || new Date();

            const day = String(date?.getUTCDate())?.padStart(2, '0');
            const month = String(date?.getMonth() + 1)?.padStart(2, '0');
            const year = String(date?.getFullYear());

            let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);

            let dataView = {
                titleName: titleName?.org?.name || "Đơn vị",
                budgetCode: titleName?.budgetCode,
                day: day || ".....",
                month: month || ".....",
                year: year || ".....",
                item: this.state,
                code: item?.code || item?.voucherCode || "........................................."
            }

            const renderedHtml = Mustache.render(data, dataView);
            this.setState({ html: renderedHtml });
        } catch (error) {
            console.log(error);
        }
    };

    render() {
        let { open, t } = this.props;

        return (
            <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth  >
                <DialogTitle style={{ cursor: 'move', paddingBottom: '0px' }} id="draggable-dialog-title">
                    <span className="">{t('Phiếu xuất kho')}</span>
                </DialogTitle>
                <iframe id="ifmcontentstoprint" style={{ height: '0px', width: '0px', position: 'absolute', print: { size: 'auto', margin: '0mm' } }}></iframe>

                <ValidatorForm className="validator-form-scroll-dialog" onSubmit={this.handleFormSubmit}>
                    <DialogContent id='divcontents' className="dialog-print">
                        <div dangerouslySetInnerHTML={{ __html: this.state.html }} />
                    </DialogContent>
                    <DialogActions>
                        <div className="flex flex-space-between flex-middle">
                            <Button
                                variant="contained"
                                color="secondary"
                                className="mr-12"
                                onClick={() => this.props.handleClose()}
                            >
                                {t('general.cancel')}
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className="mr-16"
                                type="submit"
                            >
                                {t('In')}
                            </Button>
                        </div>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

