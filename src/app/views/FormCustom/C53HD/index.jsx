/* eslint-disable no-new-wrappers */
import React, { Component } from "react";
import Signature from "../Component/Signature";
import { convertNumberPrice, convertNumberPriceRoundUp, getTheHighestRole } from "app/appFunction";
import { appConst } from "app/appConst";
import Mustache from "mustache"

const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    // minWidth: "76px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60x",
  },
  code: {
    border: "1px solid",
    padding: "2px",
  },
  date: {
    marginRight: "7%",
    textAlign: "right",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
  },
  nameText: {
    textAlign: "center",
    border: "1px solid",
    fontWeight: "bold",
    padding: "4px",
    minWidth: "130px",
  },
  w_33: {
    width: "33%",
  },
  d_flex: {
    display: "flex",
    paddingLeft: "20px",
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
  }
};

export default class C53HD extends Component {
  state = {
    sumPrice: 0,
    sumCarryingAmount: 0,
    html: ""
  };

  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write('<style>@page { padding: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  componentWillMount() {
    this.setState({
      ...this.props.item,
    }, () => {
      this.renderTemplate();
    });
  }
  componentDidMount() { }

  sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return convertNumberPrice(sum);
  };

  sumArrayNumber = (arr) => {
    return arr.reduce((a, b) => a + b);
  };

  renderRole = (role) => {
    switch (role) {
      case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
        return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name;
      case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
        return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name;
      case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
        return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name;
      default:
        break;
    }
  };

  formatDate2String = (value) => {
    const newDate = new Date(value)
    const day = String(newDate?.getDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());

    return value ? `ngày ${day} tháng ${month} năm ${year}` : "";
  }

  renderTemplate = async () => {
    try {
      let { t, item } = this.props;
      const res = await fetch("/assets/form-print/C53HDTemplate.txt")
      const data = await res.text();

      const { organization } = getTheHighestRole();
      let dataView = {
        organization,
        inventoryCountDate: this.formatDate2String(new Date(item?.inventoryCountDate)),
        inventoryCountPersons: item?.inventoryCountPersons
          ?
          [...item?.inventoryCountPersons?.sort((a, b) => a?.role - b?.role)]?.map(i => {
            return {
              ...i,
              role: this.renderRole(i?.role),
              position: i?.position || "..".repeat(10)
            }
          })
          : [],
        assets: item?.assets?.map((row, index) => {
          let {
            accountantQuantity,
            accountantOriginalCost,
            inventoryQuantity,
            inventoryOriginalCost,
            accountantCarryingAmount,
            inventoryCarryingAmount,
            asset,
          } = row;

          return {
            ...row,
            index: index + 1,
            accountantOriginalCost: convertNumberPriceRoundUp(accountantOriginalCost) || 0,
            inventoryOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
            accountantCarryingAmount: convertNumberPriceRoundUp(accountantCarryingAmount) || 0,
            inventoryCarryingAmount: convertNumberPriceRoundUp(inventoryCarryingAmount) || 0,
            accountantQuantity: accountantQuantity || 1,
            inventoryQuantity: inventoryQuantity || 1,
            differenceQuantity: (inventoryQuantity - accountantQuantity) || 0,
            differenceOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost - accountantOriginalCost) || 0,
            differenceCarryingAmount: convertNumberPriceRoundUp(inventoryCarryingAmount - accountantCarryingAmount) || 0,
          }
        }),
        sumAccountantQuantity: item?.assets?.reduce((total, item) => total + (item?.accountantQuantity || 1), 0),
        sumInventoryQuantityQuantity: item?.assets?.reduce((total, item) => total + (item?.accountantQuantity || 1), 0),
        sumAccountantOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantOriginalCost || 0), 0)) || 0,
        sumAccountantCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantCarryingAmount || 0), 0)) || 0,
        sumInventoryOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost || 0), 0)) || 0,
        sumInventoryCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount || 0), 0)) || 0,
        sumDifferenceQuantity: item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity - item?.accountantQuantity || 0), 0),
        sumDifferenceOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost - item?.accountantOriginalCost || 0), 0)) || 0,
        sumDifferenceCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount - item?.accountantCarryingAmount || 0), 0)) || 0,
      }
      this.setState({ html: Mustache.render(data, dataView) })
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    let { t, item } = this.props;
    const { organization } = getTheHighestRole();
    let arrAccountantQuantity = [];
    let arrAccountantOriginalCost = [];
    let arrInventoryQuantity = [];
    let arrInventoryOriginalCost = [];
    let arrDifferenceQuantity = [];
    let arrDifferenceOriginalCost = [];
    let arrCarryingAmountAccount = [];
    let arrCarryingAmountCheck = [];
    let arrDifferenceCarryingAmount = [];

    return (
      <>
        <div dangerouslySetInnerHTML={{ __html: this.state?.html }} />
      </>
    );
  }
}
