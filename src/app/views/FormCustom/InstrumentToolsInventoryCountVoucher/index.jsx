import Title from "../Component/Title";
import {convertNumberPrice, convertNumberPriceRoundUp} from "../../../appFunction";
import Signature from "../Component/Signature";
import React from "react";
import {appConst} from "../../../appConst";
import DateToText from "../Component/DateToText";


const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    minWidth: "76px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60x",
  },
  code: {
    border: "1px solid",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px",
  },
  textUppercase: {
    textTransform: "uppercase"
  },
};

export const InstrumentToolsInventoryCountVoucherPrint = props => {
  const { item, t } = props;
  let arrAccountantQuantity = [];
  let arrAccountantOriginalCost = [];
  let arrInventoryQuantity = [];
  let arrInventoryOriginalCost = [];
  let arrDifferenceQuantity = [];
  let arrDifferenceOriginalCost = [];
  let arrNegativeDifferenceOriginalCost = [];
  let arrPositiveDifferenceOriginalCost = [];
  let arrNegativeDifferenceQuantity = [];
  let arrPositiveDifferenceQuantity = [];
  let arrUnitPrice = [];

  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b, 0);
    return convertNumberPrice(sum);
  };

  const renderRole = (role) => {
    switch (role) {
      case appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.code:
        return appConst.OBJECT_HD_CHI_TIETS.TRUONG_BAN.name
      case appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.code:
        return appConst.OBJECT_HD_CHI_TIETS.UY_VIEN.name
      case appConst.OBJECT_HD_CHI_TIETS.THU_KY.code:
        return appConst.OBJECT_HD_CHI_TIETS.THU_KY.name
      default:
        break;
    }
  }

  return (
    <div className="form-print form-print-landscape">
      <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr", ...style.bold }}>
        <div>
          <div style={{...style.textAlign, ...style.textUppercase}}>Tổng cục IV</div>
          <div style={{...style.textAlign,}}>Bệnh viện 199</div>
        </div>
        <div></div>
      </div>

      <Title title="BIÊN BẢN KIỂM KÊ công cụ, dụng cụ"/>
      <div style={{ textAlign: "center", marginTop: 30}}>
        <div style={style.textAlign}>
          Tài khoản: ........................................................
        </div>
        <div style={style.textAlign}>
          Đơn vị:&nbsp;{item?.department?.text}
        </div>
        <div>
          <DateToText unItalic date={new Date(item?.inventoryCountDate)} textPreDate={"Thời điểm kiểm kê"}/>
        </div>
      </div>

      <div style={{marginBottom: "20px", display: "grid", gridTemplateColumns: "1fr 2fr 1fr", }}>
        <div></div>
        <div>
          <div style={{ textIndent: "30px"}}>Ban kiểm kê bao gồm: </div>
          <table style={{marginLeft: 60}}>
            <tbody>
            {item?.inventoryCountPersons &&
              item?.inventoryCountPersons?.map((person) => {
                return (
                  <tr style={{ marginBottom: 5 }}>
                    <td style={{ minWidth: "260px" }}>
                      Ông/ Bà: {person?.personName || "......................................"}.
                    </td>
                    <td style={{ paddingLeft: "10px"}}>
                      {person?.position || ".................................................."}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <div></div>
      </div>

      <div>
        <div className="overflow-auto">
          <table style={{ ...style.tableCollapse }} className="table-report">
            <thead>
              <tr>
                <th style={{ ...style.border, minWidth: "30px" }} rowSpan="3">
                  STT
                </th>
                <th style={{...style.border}} rowSpan="3">
                  {t("InventoryCountVoucher.iatCode")}
                </th>
                <th style={{...style.border, maxWidth: "20px"}} rowSpan="3">
                  {t("InventoryCountVoucher.iatName")}
                </th>
                <th style={{...style.border, minWidth: "20px"}} rowSpan="3">
                  {t("InventoryCountVoucher.sku")}
                </th>
                <th style={{...style.border, minWidth: "20px"}} rowSpan="3">
                  {t("InventoryCountVoucher.unitPrice")}
                </th>
                <th style={{...style.alignCenter, ...style.border}} colSpan="2">
                  {t("InventoryCountVoucher.accordingBooks")}
                </th>
                <th style={{...style.alignCenter, ...style.border}} colSpan="2">
                  {t("InventoryCountVoucher.accordingToInventory")}
                </th>
                <th style={{...style.alignCenter, ...style.border}} colSpan="4">
                  {t("InventoryCountVoucher.quantityDifferent")}
                </th>
              </tr>
              <tr>
                {/* Theo sổ sách*/}
                <th style={{...style.alignCenter, ...style.border}} rowSpan="2">Số lượng</th>
                <th style={{...style.alignCenter, ...style.border}} rowSpan="2">Thành tiền</th>

                {/* Thực kiểm kê*/}
                <th style={{...style.alignCenter, ...style.border}} rowSpan="2">Số lượng</th>
                <th style={{...style.alignCenter, ...style.border}} rowSpan="2">Thành tiền</th>

                {/* NG chênh lệch*/}
                <th style={{...style.alignCenter, ...style.border}} colSpan="2">Thừa</th>
                <th style={{...style.alignCenter, ...style.border}} colSpan="2">Thiếu</th>
              </tr>
              <tr>
                <th style={{...style.alignCenter, ...style.border}}>Số lượng</th>
                <th style={{...style.alignCenter, ...style.border}}>Thành tiền</th>
                <th style={{...style.alignCenter, ...style.border}}>Số lượng</th>
                <th style={{...style.alignCenter, ...style.border}}>Thành tiền</th>
              </tr>
            </thead>

            <tbody>
              {item?.assets?.map((row, index) => {
                let {
                  accountantQuantity,
                  accountantOriginalCost,
                  inventoryQuantity,
                  inventoryOriginalCost,
                  asset,
                } = row;
                let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
                let totalQuantity = inventoryQuantity - accountantQuantity;
                let unitPrice = asset.originalCost / asset.quantity
                arrAccountantOriginalCost.push(accountantOriginalCost);
                arrAccountantQuantity.push(accountantQuantity);
                arrInventoryOriginalCost.push(inventoryOriginalCost);
                arrInventoryQuantity.push(inventoryQuantity);
                arrDifferenceQuantity.push(totalQuantity);
                arrDifferenceOriginalCost.push(totalOriginalCost);
                arrUnitPrice.push(unitPrice)
                if (totalOriginalCost > 0) {
                  arrPositiveDifferenceOriginalCost.push(totalOriginalCost)
                }
                else {
                  arrNegativeDifferenceOriginalCost.push(totalOriginalCost)
                }
                if (totalQuantity > 0) {
                  arrPositiveDifferenceQuantity.push(totalQuantity)
                }
                else {
                  arrNegativeDifferenceQuantity.push(totalQuantity)
                }

                return (
                  <tr>
                    <td style={{...style.alignCenter}}>{index + 1}</td>
                    <td style={{ ...style.code, textAlign: "center"}}>{row.asset?.code}</td>
                    <td style={{ ...style.code, padding: 5}}>{row?.asset?.name}</td>
                    <td style={{ ...style.code, padding: 5}}>{row.asset?.unit?.name}</td>
                    <td style={style.alignRightOriginalCost}>{convertNumberPriceRoundUp(unitPrice)}</td>
                    <td style={style.alignCenterNumber}>{accountantQuantity}</td>
                    <td style={style.alignRightOriginalCost}>
                      {convertNumberPriceRoundUp(accountantOriginalCost)}
                    </td>
                    <td style={style.alignCenterNumber}>{inventoryQuantity}</td>
                    <td style={style.alignRightOriginalCost}>
                      {convertNumberPriceRoundUp(inventoryOriginalCost)}
                    </td>
                    <td style={style.alignCenterNumber}>
                      {totalQuantity > 0 ? convertNumberPriceRoundUp(totalQuantity) : ""}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost > 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                    <td style={style.alignCenterNumber}>
                      {totalQuantity < 0 ? convertNumberPriceRoundUp(totalQuantity) :""}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost < 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                  </tr>
                );
              })}
              <tr>
                <th colSpan="4">Tổng cộng</th>
                <th style={{ ...style.alignRight }}>
                  {sumArray(arrUnitPrice)}
                </th>
                <th style={style.alignCenter}>{sumArray(arrAccountantQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrAccountantOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrInventoryQuantity)}</th>
                <th style={style.alignCenter}>{sumArray(arrInventoryOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrPositiveDifferenceQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrPositiveDifferenceOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrNegativeDifferenceQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrNegativeDifferenceOriginalCost)}</th>
              </tr>
            </tbody>
          </table>
        </div>

        <div style={{marginTop: "10px"}}>
          Ý kiến giải quyết số chênh lệch ..................................................................................................................................................................................
        </div>
      </div>
      <div style={style.signature}>
        <Signature
          titleProps={{fontSize: 18}}
          signProps={{fontSize: 18}}
          title="Thủ trưởng đơn vị"
          sign="Ký, họ tên, đóng dấu"
        />
        <Signature
          titleProps={{fontSize: 18}}
          signProps={{fontSize: 18}}
          title="Kế toán trưởng" sign="Ký, họ tên" />
        <Signature
          titleProps={{fontSize: 18}}
          signProps={{fontSize: 18}}
          title="Trưởng ban kiểm kê"
          sign="Ký, họ tên"
          // date={new Date(item?.inventoryCountDate)}
        />
      </div>
    </div>
  );
};