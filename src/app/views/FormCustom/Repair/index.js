import {convertMoney, convertNumberPrice, getUserInformation} from "../../../appFunction";
import {appConst, LIST_ORGANIZATION} from "../../../appConst";
import moment from "moment/moment";
import ConstantList from "app/appConfig";

export const convertRepairingPrintData = item => {
  const {organization} = getUserInformation();
  const newDate = new Date(item?.arTgBaoCao);
  const day = String(newDate?.getDate())?.padStart(2, "0");
  const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
  const year = String(newDate?.getFullYear());

  return {
    ...item,
    titleName: organization,
    arPhongBanText: item?.arPhongBanText || "..".repeat(30),
    arTnPhongBanText: item?.arTnPhongBanText || "Khoa/Phòng ..................",
    day: item?.arTgBaoCao ? day : " ..... ",
    month: item?.arTgBaoCao ? month : " ..... ",
    year: item?.arTgBaoCao ? year : " ..... ",
    amChukys: [{
      index: 1,
      eqName: `${item?.tsTen ? item?.tsTen : ""} ${item?.tsMa ? `; ${item?.tsMa}` : ""}`,
      ckTrangthaiName: item?.arTrangThaiMoTa
    }]
  }
}

const renderDate = (dateData) => {
  let date = new Date(dateData);
  const dateEmty = new Date();

  let day = moment(date).format("D").toString().padStart(2, "0");
  let month = (date.getMonth() + 1).toString().padStart(2, "0");
  let year = date.getFullYear();
  const dateHasText = dateData ? `ngày ${day} tháng ${month} năm ${year}` : "ngày .... tháng .... năm ......";
  const dateNoText = dateData ? moment(date).format("DD/MM/YYYY") : ".... / .... / ...... ";
  const dateHasTextV2 = dateData ? `Ngày: ${day} tháng: ${month} năm: ${year}` : `Ngày:..........tháng:..........năm:${moment(dateEmty).format("YYYY")}`;

  return {
    dateHasText,
    dateNoText,
    dateHasTextV2,
  };
}

export const convertBM01PrintData = (dataArSuCo) => {
  const {currentUser} = getUserInformation();

  return {
    ...dataArSuCo,
    currentUser,
    isOk: dataArSuCo?.arNtTinhTrang
      === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code,
    isNotOk: dataArSuCo?.arNtTinhTrang
      === appConst.STATUS_NGHIEM_THU.THIET_BI_KHONG_SU_DUNG_DUOC.code,
    arNghiemThus: dataArSuCo?.arNghiemThus?.map((item) => {
      return {
        ...item,
        arNtHangMuc: item?.arNtHangMuc?.replaceAll("\n", `<br>`),
        arNtKetLuan: item?.arNtKetLuan?.replaceAll("\n", `<br>`)
      };
    }),
    arNoiDung: dataArSuCo?.arNoiDung
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
          ${dataArSuCo.arNoiDung}
        </div>`
      : "",
    arMoTa: dataArSuCo?.arMoTa
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
            - Mô tả sự cố:&nbsp;${dataArSuCo.arMoTa}
        </div>`
      : "",
    arPhanTichSoBo: dataArSuCo?.arPhanTichSoBo
      ? `
        <div style="
          position: relative;
          line-height: 25px;
          word-break: break-word;
          overflow: hidden;
        ">
          - Phân tích sơ bộ:&nbsp;${dataArSuCo.arPhanTichSoBo}
        </div>`
      : "",
    arDeXuatBienPhap: dataArSuCo?.arDeXuatBienPhap
      ? `
      <div style="
        position: relative;
        line-height: 25px;
        word-break: break-word;
        overflow: hidden;
      ">
        - Đề xuất biện pháp:&nbsp;${dataArSuCo.arDeXuatBienPhap}
      </div>`
      : "",
  }
}

export const convertBM0606PrintData = (dataArSuCo) => {
  const {currentUser} = getUserInformation();

  return {
    ...dataArSuCo,
    currentUser,
    dateHasText: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    dateHasTextV2: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasTextV2,
    arTgCanCoVtTo: renderDate(dataArSuCo?.arTgCanCoVt)?.dateNoText,
    arLinhKienVatTus: dataArSuCo?.arLinhKienVatTus?.map((i, x) => {
      return {
        ...i,
        index: x + 1
      }
    })
  }
}

export const convertKTTS01PrintData = (dataArSuCo) => {

  return {
    ...dataArSuCo,
    dateHasText: renderDate(dataArSuCo?.arNtThoiGian)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arNtThoiGian)?.dateNoText,
    arUsers: dataArSuCo?.arUsers?.map((i) => {
      return {
        ...i,
        userName: i?.userName || "..".repeat(10)
      }
    })
  };
}

export const convertBM0607PrintData = (dataArSuCo) => {
  const {currentUser} = getUserInformation();
  const currentOrg = ConstantList.CURRENT_ORG;
  const typeByOrg = {
    [LIST_ORGANIZATION.BV199.code]: appConst.TYPES_MONEY.DOT.code,
    [LIST_ORGANIZATION.ASVN.code]: appConst.TYPES_MONEY.DOT.code,
  }
  const moneyType = typeByOrg[currentOrg?.code] || appConst.TYPES_MONEY.COMMA.code
  return {
    ...dataArSuCo,
    tsTen: dataArSuCo?.tsTen || "..".repeat(10),
    tsModel: dataArSuCo?.tsModel || "..".repeat(10),
    tsSerialNo: dataArSuCo?.tsSerialNo || "..".repeat(10),
    currentUser,
    dateHasText: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasText,
    dateNoText: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    dateHasTextV2: renderDate(dataArSuCo?.arTgXacNhan)?.dateHasTextV2,
    arTgCanCoVtTo: renderDate(dataArSuCo?.arTgCanCoVt)?.dateNoText,
    arHanSuaChuaXong: renderDate(dataArSuCo?.arHanSuaChuaXong)?.dateNoText,
    arNtThoiGian: renderDate(dataArSuCo?.arNtThoiGian)?.dateNoText,
    arNtThoiGianHasText: renderDate(dataArSuCo?.arNtThoiGian)?.dateHasText,
    userName: dataArSuCo?.arUsers?.[0]?.userName,
    arChiPhi: convertMoney(dataArSuCo?.arChiPhi, moneyType) || 0,
    arTongChiPhi: convertMoney(dataArSuCo?.arTongChiPhi, moneyType) || 0,
    isSuaChuaNgoai: dataArSuCo?.arHinhThuc === Number(appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code),
    checked: dataArSuCo?.arNtTinhTrang === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code,
    arLinhKienVatTus: dataArSuCo?.arLinhKienVatTus?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        productCost: convertMoney(i?.productCost, moneyType) || 0,
        productTotal: convertMoney(i?.productTotal, moneyType) || 0,
      }
    })
  };
}