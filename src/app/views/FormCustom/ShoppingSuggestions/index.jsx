import React, { useEffect, useState } from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import HeaderLeft from "../Component/HeaderLeft";
import NationalName from "../Component/NationalName";
import "../FormCustom.scss"
import localStorageService from "app/services/localStorageService";
import Mustache from 'mustache';
import { getUserInformation } from "../../../appFunction";
import { printStyles } from "../constant";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  wrapper: {
    fontSize: "18px",
  },
  containFontSize: {
    fontSize: "18px"
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0"
  },
  flexEnd: {
    display: "flex",
    justifyContent: "end",
    marginTop: 20
  },
  rowContainer: {
    paddingBottom: 10,
    fontSize: "18px"
  },
  py_2: {
    padding: "0 2%",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  tableRow: {
    border: "1px solid",
  },
  tableCell: {
    borderLeft: "1px solid",
    padding: "4px"
  },
  subTitle: {
    fontFamily: "'Times New Roman', Times, serif !important",
    fontSize: "0.975rem !important",
    fontWeight: "bold",
    textUnderlineOffset: "5px"
  }
};

export default function ShoppingSuggestions(props) {
  const [html, setHtml] = useState("")
  const { t, open, handleClose, itemEdit } = props;
  const { addressOfEnterprise } = getUserInformation();

  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  const newDate = new Date(itemEdit?.requestDate)
  const day = String(newDate?.getDate())?.padStart(2, '0');
  const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
  const year = String(newDate?.getFullYear());



  const renderTemplate = async () => {
    try {
      const res = await fetch("/assets/form-print/duTruTS_CC_DC_bvVanDinh.txt")
      const data = await res.text();

      let dataView = {
        itemEdit,
        addressOfEnterprise,
        day,
        month,
        year
      }

      setHtml(Mustache.render(data, dataView))
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    renderTemplate();
  }, [])

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("Phiếu dự trù tài sản, công cụ, dụng cụ")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        class="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className=" dialog-print">
          <div className="form-print py-cm-2 pl-cm-3 pr-cm-h3">
            <div dangerouslySetInnerHTML={{ __html: html }} />
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("general.print")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
