import Title from "../Component/Title";
import { convertNumberPrice, convertNumberPriceRoundUp, getTheHighestRole } from "../../../appFunction";
import Signature from "../Component/Signature";
import React, { useEffect, useState } from "react";
import Mustache from "mustache"


const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    minWidth: "76px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60x",
  },
  code: {
    border: "1px solid",
    padding: "2px",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px",
  },
};

export const AssetInventoryCountVoucher = props => {
  const [html, setHtml] = useState("")
  const { item, t } = props;
  let arrAccountantQuantity = [];
  let arrAccountantOriginalCost = [];
  let arrInventoryQuantity = [];
  let arrInventoryOriginalCost = [];
  let arrDifferenceQuantity = [];
  let arrDifferenceOriginalCost = [];
  let arrCarryingAmountAccount = [];
  let arrCarryingAmountCheck = [];
  let arrDifferenceCarryingAmount = [];

  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b);
    return convertNumberPrice(sum);
  };

  const sumArrayNumber = (arr) => {
    return arr.reduce((a, b) => a + b);
  };

  const newDate = new Date(item?.inventoryCountDate)
  const day = String(newDate?.getDate())?.padStart(2, '0');
  const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
  const year = String(newDate?.getFullYear());

  const renderTemplate = async () => {
    try {
      const res = await fetch("/assets/form-print/bbkkTSCDTemplate.txt")
      const data = await res.text();

      const { organization } = getTheHighestRole();
      let dataView = {
        organization,
        inventoryCountDate: year,
        department: item?.department?.text,
        assets: item?.assets?.map((row, index) => {
          let {
            accountantQuantity,
            accountantOriginalCost,
            inventoryQuantity,
            inventoryOriginalCost,
            accountantCarryingAmount,
            inventoryCarryingAmount,
            asset,
          } = row;

          return {
            ...row,
            index: index + 1,
            accountantOriginalCost: convertNumberPriceRoundUp(accountantOriginalCost) || 0,
            inventoryOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost) || 0,
            accountantCarryingAmount: convertNumberPriceRoundUp(accountantCarryingAmount) || 0,
            inventoryCarryingAmount: convertNumberPriceRoundUp(inventoryCarryingAmount) || 0,
            accountantQuantity: accountantQuantity || 1,
            inventoryQuantity: inventoryQuantity || 1,
            differenceQuantity: (inventoryQuantity - accountantQuantity) || 0,
            differenceOriginalCost: convertNumberPriceRoundUp(inventoryOriginalCost - accountantOriginalCost) || 0,
            differenceCarryingAmount: convertNumberPriceRoundUp(inventoryCarryingAmount - accountantCarryingAmount) || 0,
            carryingAmountRate: (((inventoryCarryingAmount) / (inventoryOriginalCost) || 0) * 100).toFixed(2)
          }
        }),
        sumAccountantQuantity: item?.assets?.reduce((total, item) => total + (item?.accountantQuantity || 1), 0),
        sumInventoryQuantityQuantity: item?.assets?.reduce((total, item) => total + (item?.accountantQuantity || 1), 0),
        sumAccountantOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantOriginalCost || 0), 0)) || 0,
        sumAccountantCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.accountantCarryingAmount || 0), 0)) || 0,
        sumInventoryOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost || 0), 0)) || 0,
        sumInventoryCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount || 0), 0)) || 0,
        sumDifferenceQuantity: item?.assets?.reduce((total, item) => total + (item?.inventoryQuantity - item?.accountantQuantity || 0), 0),
        sumDifferenceOriginalCost: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryOriginalCost - item?.accountantOriginalCost || 0), 0)) || 0,
        sumDifferenceCarryingAmount: convertNumberPriceRoundUp(item?.assets?.reduce((total, item) => total + (item?.inventoryCarryingAmount - item?.accountantCarryingAmount || 0), 0)) || 0,
      }
      setHtml(Mustache.render(data, dataView))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    renderTemplate()
  }, [])
  return (
    <>
      <div dangerouslySetInnerHTML={{ __html: html }} />
      {/* <div className="form-print form-print-landscape">
        <div style={{ display: "flex", justifyContent: "space-between", ...style.bold }}>
          <div>
            <div style={style.textAlign}>Bộ Công an</div>
            <div>Bệnh viện 199 - Bộ Công an</div>
          </div>
          <div></div>
        </div>
        <div style={{ textAlign: "center", marginBottom: 20 }}>
          <Title title="BIÊN BẢN KIỂM KÊ TÀI SẢN CỐ ĐỊNH" />
          <p style={{ marginBottom: 40 }}>
            <div style={style.textAlign}>
              Năm báo cáo:&nbsp;{
                item?.inventoryCountDate ? new Date(item?.inventoryCountDate).getFullYear() : "........"
              }
            </div>
            <div style={style.textAlign}>
              Phòng, ban:&nbsp;{item?.department?.text}
            </div>
          </p>
        </div>
  
        <div>
          <div className="overflow-auto">
            <table style={{ ...style.tableCollapse, }} className="table-report">
              <thead>
                <tr>
                  <th style={{ ...style.border, minWidth: "30px" }} rowSpan="2">
                    STT
                  </th>
                  <th style={{ ...style.border, maxWidth: "20px" }} rowSpan="2">
                    {t("InventoryCountVoucher.nameAsset")}
                  </th>
                  <th style={{ ...style.border }} rowSpan="2">
                    {t("InventoryCountVoucher.codeAsset")}
                  </th>
                  <th style={{ ...style.border, minWidth: "20px" }} rowSpan="2">
                    {t("InventoryCountVoucher.yearPutIntoUse")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                    {t("InventoryCountVoucher.accordingBooks")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border }} colSpan="4">
                    {t("InventoryCountVoucher.accordingToInventory")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                    NG chênh lệch (Giảm ghi âm(-))
                  </th>
                  <th style={style.border} rowSpan="2">
                    Ghi chú
                  </th>
                </tr>
                <tr>
                  
                  <th style={{ ...style.alignCenter, ...style.border }}>
                    {t("InventoryCountVoucher.quantityOrMass")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                  <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
             
                  <th style={{ ...style.alignCenter, ...style.border }}>
                    {t("InstrumentsToolsInventory.quantity")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border, maxWidth: "70px" }}>CL (%)</th>
                  <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                  <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
                  
                  <th style={{ ...style.alignCenter, ...style.border }}>
                    {t("InstrumentsToolsInventory.quantity")}
                  </th>
                  <th style={{ ...style.alignCenter, ...style.border }}>Nguyên giá</th>
                  <th style={{ ...style.alignCenter, ...style.border }}>GTCL</th>
                </tr>
              </thead>
  
              <tbody>
                {item?.assets?.map((row, index) => {
                  let {
                    accountantQuantity,
                    accountantOriginalCost,
                    accountantCarryingAmount,
                    inventoryQuantity,
                    inventoryOriginalCost,
                    inventoryCarryingAmount,
                  } = row;
                  let carryAmount = inventoryCarryingAmount - accountantCarryingAmount;
                  let originalCost = inventoryOriginalCost - accountantOriginalCost;
                  inventoryCarryingAmount - accountantCarryingAmount;
                  let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
                  let totalQuantity = inventoryQuantity - accountantQuantity;
                  arrAccountantOriginalCost.push(accountantOriginalCost);
                  arrAccountantQuantity.push(accountantQuantity);
                  arrInventoryOriginalCost.push(inventoryOriginalCost);
                  arrInventoryQuantity.push(inventoryQuantity);
                  arrDifferenceQuantity.push(totalQuantity);
                  arrDifferenceOriginalCost.push(totalOriginalCost);
                  arrCarryingAmountAccount.push(accountantCarryingAmount);
                  arrCarryingAmountCheck.push(inventoryCarryingAmount);
                  arrDifferenceCarryingAmount.push(carryAmount);
                  let carryingAmountRate = (
                    (inventoryCarryingAmount / inventoryOriginalCost) * 100
                  ).toFixed(2);
  
                  return (
                    <tr>
                      <td style={{ ...style.alignCenter }}>{index + 1}</td>
                      <td style={{ ...style.code, padding: 5 }}>
                        {row?.asset?.name}
                      </td>
                      <td style={{ ...style.code, textAlign: "center" }}>{row.asset?.code}</td>
                      <td style={{ ...style.code, padding: 5 }}>
                        {row.asset?.yearPutIntoUse}
                      </td>
                      <td style={style.alignCenterNumber}>1</td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(accountantOriginalCost)}
                      </td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(accountantCarryingAmount)}
                      </td>
                      <td style={style.alignCenterNumber}>1</td>
                      <td style={{ ...style.alignCenterNumber, maxWidth: "70px" }}>{carryingAmountRate}</td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(inventoryOriginalCost)}
                      </td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(inventoryCarryingAmount)}
                      </td>
                      <td style={style.alignCenterNumber}>
                        {convertNumberPriceRoundUp(totalOriginalCost) || 0}
                      </td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(originalCost) || 0}
                      </td>
                      <td style={style.alignRightOriginalCost}>
                        {convertNumberPriceRoundUp(carryAmount) || 0}
                      </td>
                      <td style={{ ...style.border }}>{row?.note}</td>
                    </tr>
                  );
                })}
                <tr>
                  <th colSpan="4">Tổng cộng</th>
                  <th style={{ ...style.alignCenter, border: "1px solid", }}>
                    {item?.assets?.length}
                  </th>
                  <th style={style.alignRight}>
                    {sumArray(arrAccountantOriginalCost)}
                  </th>
                  <th style={style.alignRight}>
                    {sumArray(arrCarryingAmountAccount)}
                  </th>
                  <th style={style.alignCenter}>
                    {item?.assets?.length}
                  </th>
                  <th style={style.alignCenter}>
  
                  </th>
                  <th style={style.alignRight}>
                    {sumArray(arrInventoryOriginalCost)}
                  </th>
                  <th style={style.alignRight}>
                    {sumArray(arrCarryingAmountCheck)}
                  </th>
                  <th style={style.alignCenter}>0</th>
                  <th style={style.alignRight}>
                    {convertNumberPrice(
                      sumArrayNumber(arrInventoryOriginalCost) - sumArrayNumber(arrAccountantOriginalCost)
                    ) || 0}
                  </th>
                  <th style={style.alignRight}>
                    {convertNumberPrice(
                      sumArrayNumber(arrCarryingAmountCheck) - sumArrayNumber(arrCarryingAmountAccount)
                    ) || 0}
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
  
          <div style={{ marginTop: "10px" }}>
            Ý kiến giải quyết số chênh lệch ..................................................................................................................................................................................
          </div>
        </div>
        <div style={style.signature}>
          <Signature
            titleProps={{ fontSize: 18 }}
            signProps={{ fontSize: 18 }}
            title="Thủ trưởng đơn vị"
            sign="Ký, họ tên, đóng dấu"
          />
          <Signature
            titleProps={{ fontSize: 18 }}
            signProps={{ fontSize: 18 }}
            title="Kế toán trưởng" sign="Ký, họ tên" />
          <Signature
            titleProps={{ fontSize: 18 }}
            signProps={{ fontSize: 18 }}
            title="Trưởng ban kiểm kê"
            sign="Ký, họ tên"
          />
        </div>
      </div> */}
    </>
  );
};