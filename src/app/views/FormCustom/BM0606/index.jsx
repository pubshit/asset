import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions, withStyles } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import HeaderRight from "../Component/HeaderRight";
import { appConst } from "../../../appConst";
import { getDetailArSuCo } from "app/views/MaintainResquest/MaintainRequestService";
import moment from "moment";
import AppContext from "app/appContext";
import { toast } from "react-toastify";
import localStorageService from "app/services/localStorageService";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const styles = (theme) => ({
  content_container: {
    '& .table-report th.border-none': {
      border: "none !important",
    }
  },
  borderNone: {
    border: "none !important",
  },
  unHover: {
    pointerEvents: "none !important"
  },
})

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_2: {
    border: "1px solid",
    width: "2%",
    textAlign: "center",
  },
  w_3: {
    border: "1px solid",
    width: "3%",
    textAlign: "center",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
    textAlign: "center",
  },
  w_25: {
    border: "1px solid",
    width: "25%",
    textAlign: "center",
  },
  w_30: {
    border: "1px solid",
    width: "30%",
    textAlign: "center",
  },
  w_40: {
    border: "1px solid",
    width: "40%",
    wordBreak: "break-all"
  },
  w_60: {
    border: "1px solid",
    width: "60%",
    wordBreak: "break-all"
  },
  w_100: {
    border: "1px solid",
    width: "100%",
    textAlign: "center",
  },
  sum: {
    border: "1px solid",
    paddingLeft: "5px",
    fontWeight: "bold",
    textAlign: "left",
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 10%",
  },
  border: {
    border: "1px solid",
    textAlign: "center"
  },
  styleFlexJustify: {
    display: "flex",
    justifyContent: 'space-between',
  },
  input: {
    border: "none",
    borderBottom: "1px dotted #000",
    lineHeight: "1.6rem",
    fontSize: "1rem",
    marginTop: "0",
    marginBottom: "10px",
    padding: "1px 0",
    position: "relative",
    bottom: "-5px",
    outline: "none",
    height: "12px",
    marginLeft: "3px"
  },
  thChildren: {
    fontSize: "1rem",
    textAlign: "start",
    padding: "0 6px"

  },
  textInTh: { fontSize: "1rem", fontStyle: "italic", position: "absolute", bottom: "0", left: "2", fontWeight: "normal" },
  paddingWithTextInTh: {
    paddingBottom: "30px"
  },
  textBox: {
    display: "block",
    padding: "0 6px",
    minHeight: "120px",
    textAlign: "left",
    fontSize: "1rem",
    fontWeight: "normal"
  },
  dateSpan: {
    marginLeft: "4px"
  }
};
class BM0606 extends Component {
  state = {
    type: 2,
    rowsPerPage: 1,
    page: 0,
    totalElements: 0,
    departmentId: "",
    asset: {},
    isView: false,
    dataPhieu: {},
  };
  handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };


  componentWillMount() {
    this.setState(
      {
        ...this.props.item,
      },
      function () { }
    );
  }

  componentDidMount() {
    let { t } = this.props;
    let { setPageLoading } = this.context;
    if (this.props.idArSuCo) {
      setPageLoading(true);
      getDetailArSuCo(this.props.idArSuCo)
        .then(({ data }) => {
          if (appConst.CODE.SUCCESS === data?.code) {
            const value = data?.data;
            this.setState({
              dataPhieu: value
            })
          } else {
            toast.error(t("toastr.error"));
          }
        })
        .catch(() => {
          toast.error(t("toastr.error"));
        })
        .finally(() => {
          setPageLoading(false);
        });
    }

  }

  renderDate(dateData) {
    let date = new Date(dateData);
    let day = moment(date).format("D").toString().padStart(2, '0');
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let year = date.getFullYear();
    const dateResultVer1 = `Ngày: ${day} tháng: ${month} năm: ${year}`;
    const dateResultVer2 = moment(date).format("DD/MM/YYYY");
    return {
      dateResultVer1,
      dateResultVer2
    }
  }

  render() {
    let { open, t, classes } = this.props;
    let { dataPhieu } = this.state;
    const date = new Date()
    const dateNull = `Ngày:..........tháng:..........năm:${moment(date).format("YYYY")}`;
    const currentUser = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CURRENT_USER
    );
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("ReportIncident.RateIncidentForm.print")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
        ></iframe>

        <ValidatorForm
          ref="form"
          className="validator-form-scroll-dialog"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className=" dialog-print">
            <Grid className={classes.content_container}>
              <div style={style.textAlignCenter} className="form-print">
                <div style={style.styleFlexJustify}>
                  {/* <DateToText isIssueDate date={Date.now()} /> */}
                  <div></div>
                  <HeaderRight model="BM.06.06" />
                </div>
                <div style={{ textAlign: "center" }}>
                  <div>
                    {
                      <>
                        <table style={style.table} className="table-report table-0606">
                          <tr>
                            <th style={{ fontSize: "1.3rem", padding: "20px", ...style.w_25 }}>{currentUser?.org?.name}</th>
                            <th style={{ fontSize: "1.5rem", padding: "20px", ...style.w_100 }}>
                              BIÊN BẢN SỰ CỐ MÁY, THIẾT BỊ
                            </th>
                          </tr>
                        </table>
                        <table style={style.table} className="table-report table-0606">
                          <tr>
                            <th style={{ fontSize: "1.3rem", padding: "20px 6px", textAlign: "start", fontWeight: "normal" }}>
                              <div>
                                <div style={{ fontStyle: "italic", fontSize: "1rem" }}>{dataPhieu?.arTgXacNhan ? this.renderDate(dataPhieu?.arTgXacNhan)?.dateResultVer1 : dateNull}</div>
                                <div style={{ display: "flex", marginTop: "20px", justifyContent: "space-between" }}>
                                  <div style={{ display: "flex", width: "50%" }}>
                                    <div style={{ minWidth: "fit-content", fontSize: "1rem" }}>Tên máy:</div>
                                    <div style={{ margin: "0 6px", fontSize: "1rem" }}>
                                      {dataPhieu?.tsTen ?? ""}
                                    </div>
                                  </div>
                                  <div style={{ display: "flex", width: "40%" }}>
                                    <div style={{ minWidth: "fit-content", fontSize: "1rem" }}>Đơn vị sử dụng:</div>
                                    <div style={{ margin: "0 6px", fontSize: "1rem" }}>
                                      {dataPhieu?.arPhongBanText ?? ""}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </th>
                          </tr>
                        </table>
                        <table style={style.table} className="table-report table-0606">
                          <tr>
                            <th style={{ ...style.thChildren, ...style.w_60 }}>Mô tả sự cố máy, thiết bị</th>
                            <th style={{ ...style.thChildren, ...style.w_40 }}>
                              Xác nhận của bộ phận sử dụng
                            </th>
                          </tr>
                          <th style={style.textBox} className="border-none">
                            {dataPhieu?.arMoTa ?? ""}
                          </th>
                          <th style={{ borderLeft: "1px solid" }}></th>

                          <tr>
                            <th style={{ ...style.thChildren, ...style.w_60 }}>Phân tích sơ bộ nguyên nhân</th>
                            <th style={{ ...style.thChildren, ...style.w_40 }}>
                              Xác nhận của CBKT quản lý/Ch.gia
                            </th>
                          </tr>
                          <th style={style.textBox} className="border-none">
                            {dataPhieu?.arPhanTichSoBo ?? ""}
                          </th>
                          <th style={{ borderLeft: "1px solid" }}></th>

                          <tr>
                            <th style={{ ...style.thChildren, ...style.w_60 }}>Đề xuất biện pháp khắc phục</th>
                            <th style={{ ...style.thChildren, ...style.w_40 }}>
                              Xác nhận của CBKT quản lý/Ch.gia
                            </th>
                          </tr>
                          <th style={{ position: "relative", ...style.textBox, ...style.paddingWithTextInTh }} className="border-none">
                            {dataPhieu?.arDeXuatBienPhap ?? ""}
                            <div style={style.textInTh} className={classes.unHover}>Thời hạn cần sửa chữa xong:
                              <span style={style.dateSpan}>{dataPhieu?.arHanSuaChuaXong?this.renderDate(dataPhieu?.arHanSuaChuaXong)?.dateResultVer2:"...../...../....."}</span>
                            </div>
                          </th>
                          <th style={{ borderLeft: "1px solid" }}></th>

                          <tr>
                            <th style={{ ...style.thChildren, ...style.w_60 }}>Các loại vật tư cần thiết cho sửa chữa (nếu có)</th>
                            <th style={{ ...style.thChildren, ...style.w_40 }}>
                              Xác nhận của CBKT quản lý/Ch.gia
                            </th>
                          </tr>
                          <th style={{ position: "relative", ...style.textBox, ...style.paddingWithTextInTh }} className="border-none">
                            {
                              dataPhieu?.arLinhKienVatTus?.map((item, index) => <div
                                key={index}
                                style={{ margin: "0", fontSize: "1rem !important" }}
                              >
                                {`${index + 1}. ${item?.productName} (số lượng: ${item?.productQty})`}
                              </div>)
                            }
                            <div style={style.textInTh}>Thời hạn có vật tư:
                              <span style={style.dateSpan} className={classes.unHover}>{dataPhieu?.arTgCanCoVtTo?this.renderDate(dataPhieu?.arTgCanCoVtTo)?.dateResultVer2:"...../...../....."}</span>
                            </div>
                          </th>
                          <th style={{ borderLeft: "1px solid" }}></th>
                        </table>
                        <table style={style.table} className="table-report table-0606">
                          <tr>
                            <th style={{ fontSize: "1rem", ...style.w_30 }}>Trưởng khoa/phòng sử dụng TB</th>
                            <th style={{ fontSize: "1rem", ...style.w_30 }}>
                              Tr.khoa/Phòng quản lý
                            </th>
                            <th style={{ textAlign: "center", fontSize: "1rem", ...style.w_40 }}>
                              Giám đốc
                            </th>
                          </tr>
                          <th style={{ padding: "50px 0", ...style.w_30 }}></th>
                          <th style={{ padding: "50px 0", ...style.w_30 }}></th>
                        </table>
                      </>
                    }
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}

BM0606.contextType = AppContext;
export default withStyles(styles)(BM0606);