import {
  convertToAcronym,
  getUserInformation,
} from "../../../appFunction";


export const purchaseRequestCountPrintData = (item) => {
  const {organization} = getUserInformation();
  const newDate = new Date(item?.date);
  const day = item?.date ? String(newDate?.getDate())?.padStart(2, '0') : ".....";
  const month = item?.date ? String(newDate?.getMonth() + 1)?.padStart(2, '0') : ".....";
  const year = item?.date ? String(newDate?.getFullYear()) : "......";
  const acronymDepartment = convertToAcronym(item?.departmentName);

  return {
    ...item,
    departmentSignature: acronymDepartment,
    org: organization,
    day,
    month,
    year,
    purchaseProducts: item?.purchaseProducts?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        productName: i?.alterProductName ? i?.alterProductName : i?.productName,
      }
    })
  }
}
