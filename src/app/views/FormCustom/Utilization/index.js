import {
  convertDepartmentNameForSignature,
  convertMoney,
  convertNumberPrice,
  convertNumberToWords, convertToAcronym,
  getUserInformation
} from "../../../appFunction";
import {printStyleText} from "../constant";
import {appConst, LIST_ORGANIZATION} from "../../../appConst";

export const utilizationReportPrintData = (dataPrint, currentOrg) => {
  const {organization} = getUserInformation();
  let date = new Date(dataPrint?.issueDate || dataPrint?.inputDate);
  const day = String(date?.getUTCDate())?.padStart(2, '0');
  const month = String(date?.getMonth() + 1)?.padStart(2, '0');
  const year = String(date?.getFullYear());
  const arrayForSplice = ["phòng ", "p. "];
  const moneyType = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
    ? appConst.TYPES_MONEY.SPACE.code
    : appConst.TYPES_MONEY.COMMA.code;
  const convertMoneyFunc = currentOrg?.code === LIST_ORGANIZATION.BV_CONG_THAI_NGUYEN?.code
    ? convertMoney
    : convertNumberPrice;
  const handoverDepartmentNameSpliced = convertDepartmentNameForSignature(
    dataPrint?.handoverDepartment?.name,
    arrayForSplice,
  )
  const bvc1stSignatureName = convertToAcronym(handoverDepartmentNameSpliced);

  return {
    organization,
    bvc1stSignatureName,
    titleName: organization?.org?.name || "Đơn vị",
    budgetCode: organization?.budgetCode,
    addressOfEnterprise: organization?.province,
    day: (dataPrint?.inputDate || dataPrint?.issueDate) ? day : ".....",
    month: (dataPrint?.inputDate || dataPrint?.issueDate) ? month : ".....",
    year: (dataPrint?.inputDate || dataPrint?.issueDate) ? year : ".....",
    code: dataPrint?.code || dataPrint?.voucherCode || "..".repeat(20),
    stores: dataPrint?.stores,
    item: {
      ...dataPrint,
      receiverPerson: {
        ...dataPrint?.receiverPerson,
        displayName: dataPrint?.receiverPerson?.displayName || dataPrint?.personExportName || "..".repeat(10)
      },
      receiverDepartment: {
        ...dataPrint?.receiverDepartment,
        name: dataPrint?.receiverDepartment?.name || dataPrint?.departmentReceiptName || "..".repeat(10)
      },
      stockReceiptDeliveryStore: {
        ...dataPrint?.stockReceiptDeliveryStore,
        name: dataPrint?.stockReceiptDeliveryStore?.name || "..".repeat(10),
        address: dataPrint?.stockReceiptDeliveryStore?.address || "..".repeat(10)
      },
      voucherDetails: dataPrint?.voucherDetails?.sort((a, b) => {
        if (a.product?.code < b.product?.code) return -1;
        if (a.product?.code > b.product?.code) return 1;
        return 0;
      })?.map((i, index) => {
        let quantity =
          i?.quantity % 1 === 0  
            ? i?.quantity?.toString()
            : parseFloat(i?.quantity?.toFixed(2));
        return {
          ...i,
          index: index + 1,
          price: convertNumberPrice(i?.price, moneyType) || 0,
          amount: convertMoneyFunc(i?.amount, moneyType) || 0,
          quantity,
        }
      }),
      totalAmount: convertMoneyFunc(dataPrint?.voucherDetails?.reduce((sum, i) => (sum + Number(i?.amount || 0)), 0) || 0, moneyType),
      totalPrice: convertNumberPrice(dataPrint?.voucherDetails?.reduce((sum, i) => (sum + i?.price), 0) || 0, moneyType),
      totalQuantity: dataPrint?.voucherDetails?.reduce((sum, i) => (sum + i?.quantity), 0) || 0,
      numberToWordPrice: convertNumberToWords(dataPrint?.voucherDetails?.reduce((sum, i) => (sum + i?.amount), 0) || 0) || "Không"
    },
    styles: printStyleText,
  }
}