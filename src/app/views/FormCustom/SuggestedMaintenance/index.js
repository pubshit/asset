import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import localStorageService from "app/services/localStorageService";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import { searchByPageTaiSan } from "app/views/MaintainPlaning/MaintainPlaningService";
import { spliceString } from "app/appFunction";
import { appConst } from "app/appConst";
import Mustache from "mustache";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}
const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  text: {
    fontStyle: "italic",
    fontSize: "0.8rem",
    marginTop: "0px",
  },
  mb0: {
    marginBottom: 0,
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
  },
  w_7: {
    border: "1px solid",
    width: "7%",
    textAlign: "center",
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
  },
  min_w_8: {
    border: "1px solid",
    minWidth: "19%",
    textAlign: "center",
  },
  w_10: {
    border: "1px solid",
    width: "10%",
  },
  w_15: {
    border: "1px solid",
    width: "15%",
  },
  w_13: {
    border: "1px solid",
    width: "13%",
  },
  w_25: {
    border: "1px solid",
    width: "25%",
  },
  w_30: {
    border: "1px solid",
    width: "30%",
  },
  p_10: {
    padding: 10,
  },
  represent: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 10%",
  },
  border: {
    border: "1px solid",
    textAlign: "center",
    fontSize: "0.975rem",
  },
  nameColumn: {
    border: "1px solid",
    textAlign: "left",
    paddingLeft: "10px",
    fontSize: "0.975rem",
    wordBreak: "break-all",
  },
  signatureContainer: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "20px",
    alignItems: "flex-start",
    paddingLeft: 30,
  },
  signatureItemHeader: {
    width: "100%",
    height: 1,
  },
  a4Container: {
    width: "21cm",
    margin: "auto",
    fontSize: "18px",
  },
};
export default class SuggestedMaintenance extends Component {
  state = {
    type: 2,
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    departmentId: "",
    asset: {},
    isView: false,
    keyword: "",
    itemList: [],
    html: "",
  };

  handleFormSubmit = () => {
    let { handleOKEditClose, handleClose } = this.props;
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write("<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>");
    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
    //eslint-disable-next-line
    handleOKEditClose?.();
    //eslint-disable-next-line
    handleClose?.();
  };
  updatePageDataAsset = async (item) => {
    const keyword = this.state.keyword;
    const page = this.state.page;
    const rowsPerPage = this.state.rowsPerPage;
    const dxId = item?.id;
    const searchObject = {
      keyword: keyword,
      pageIndex: page + 1,
      pageSize: rowsPerPage,
      dxId: dxId,
    };
    this.setState({
      itemList: [],
      totalElements: 10,
    });
    try {
      const { data } = await searchByPageTaiSan(searchObject);
      this.setState({ itemList: data?.data?.content || [] }, () => {
        this.renderTemplate();
      });
    } catch (error) {
      console.warn("Error", error);
    }
  };

  renderTemplate = async () => {
    try {
      const res = await fetch(
        "/assets/form-print/suggestedMaintenanceTemplate.txt"
      );
      const data = await res.text();

      let titleName = localStorageService.getSessionItem(
        appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
      );

      const newDate = new Date(this.state?.dxThoiGian);
      const day = String(newDate?.getUTCDate())?.padStart(2, "0");
      const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
      const year = String(newDate?.getFullYear());

      let dataView = {
        dataPrint: {
          ...this.state,
          phongBanLapText: spliceString(this.state?.phongBanLapText),
          tnPhongBanText: spliceString(this.state?.tnPhongBanText),
          dxNoiDung: this.state?.dxNoiDung,
          day,
          month,
          year,
          itemList: this.state?.itemList?.map((i, x) => {
            return {
              ...i,
              index: x + 1,
              ckSoluong: i?.ckSoluong || 1,
            };
          }),
        },
        titleName,
      };
      this.setState({ html: Mustache.render(data, dataView) });
    } catch (error) {
      console.log(error);
    }
  };

  componentWillMount() {
    const { item } = this.props;
    this.setState(
      {
        ...item,
      },
      function () {}
    );
    this.updatePageDataAsset(item);
  }
  componentDidMount() {}

  render() {
    let { open, t, item } = this.props;
    let { itemList } = this.state;
    let titleName = localStorageService.getSessionItem(
      appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION
    );

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("MaintenanceProposal.title")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
          title="ifmcontentstoprint"
        ></iframe>

        <ValidatorForm
          ref="form"
          class="validator-form-scroll-dialog"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className="dialog-print">
            <div dangerouslySetInnerHTML={{ __html: this.state.html }} />
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-36"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
