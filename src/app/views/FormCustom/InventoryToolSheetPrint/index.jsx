import Title from "../Component/Title";
import { convertNumberPrice, convertNumberPriceRoundUp } from "../../../appFunction";
import Signature from "../Component/Signature";
import React from "react";
import { appConst } from "../../../appConst";
import DateToText from "../Component/DateToText";
import localStorageService from "app/services/localStorageService";


const style = {
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  textAlign: {
    textAlign: "center",
    fontWeight: "bold",
    padding: "4px",
  },
  bold: {
    fontWeight: "bold",
  },
  border: {
    border: "1px solid",
    padding: "4px",
  },
  alignCenter: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
  },
  textWordWrap: {
    // wordWrap: "break-word", 
    whiteSpace: "normal",
  },
  alignRight: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
  },
  alignRightOriginalCost: {
    border: "1px solid",
    textAlign: "right",
    padding: "4px",
    minWidth: "76px",
  },
  alignCenterNumber: {
    border: "1px solid",
    textAlign: "center",
    padding: "4px",
    minWidth: "60x",
  },
  minWidth: {
    minWidth: "76px",
    maxWidth: "76px"
  },
  code: {
    border: "1px solid",
  },
  signature: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "20px",
  },
  textUppercase: {
    textTransform: "uppercase"
  },
  textCapitalize: {
    textTransform: "capitalize"
  },
  justifyContent: {
    display: "flex",
    justifyContent: "space-between",
    gap: "100px"
  },
  borderBottom: {
    borderBottom: "1px dashed #000",
    flexGrow: "1",
    transform: "translateY(-0.5rem)"
  }

};

export const InventoryToolSheetPrint = props => {
  const { item, t } = props;
  let arrAccountantQuantity = [];
  let arrAccountantOriginalCost = [];
  let arrInventoryQuantity = [];
  let arrInventoryOriginalCost = [];
  let arrDifferenceQuantity = [];
  let arrDifferenceOriginalCost = [];
  let arrNegativeDifferenceOriginalCost = [];
  let arrPositiveDifferenceOriginalCost = [];
  let arrNegativeDifferenceQuantity = [];
  let arrPositiveDifferenceQuantity = [];
  let arrUnitPrice = [];

  const sumArray = (arr) => {
    let sum = arr.reduce((a, b) => a + b, 0);
    return convertNumberPrice(sum);
  };

  let titleName = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION);
  return (
    <div className="form-print form-print-landscape">
      <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr", ...style.bold }}>
        <div>
          <div >Đơn vị: {item?.orgName || ""}</div>
          <div>Bộ phận: {item?.department?.name || ""}</div>
          <div>Mã QHNS: {titleName?.budgetCode}</div>
        </div>
        <div></div>
      </div>

      <Title title="BIÊN BẢN KIỂM KÊ CÔNG CỤ DỤNG CỤ"/>
      <div style={{ textAlign: "center", fontStyle: "italic" }}>
        <DateToText unItalic date={new Date(item?.inventoryCountDate)} textPreDate={"Thời điểm kiểm kê"}  prefixSeperate=":" />
      </div>
      <div>
        <div>
          Căn cứ vào quyết định số: 4064 ngày 20 tháng 11 năm 2023 của Phụ trách Bệnh viện Đa khoa Vân Đình v/v:
          Thành lập ban kiểm kê tài sản BVĐK Vân Đình năm 2023
        </div>
        <div>Thành phần ban kiểm kê tài sản bao gồm: </div>
        <div style={style.justifyContent}>
          {item?.inventoryCountPersons.map((item, index) => (
            <div key={index}>
              {index + 1}: {item?.personName} - {item?.departmentName} {appConst.listHdChiTietsRole.find(i => i?.code === item?.role)?.name ? " - " + appConst.listHdChiTietsRole.find(i => i?.code === item?.role)?.name : ""}
            </div>
          ))}
        </div>
        <div style={{ marginBottom: "10px" }}>Đã tiến hành kiểm kê CCDC, kết quả như sau: </div>
      </div>
      <div>
        <div className="overflow-auto">
          <table style={{ ...style.tableCollapse }} className="table-report">
            <thead>
              <tr>
                <th style={{ ...style.border, minWidth: "30px" }} rowSpan="3">
                  STT
                </th>
                <th style={{ ...style.border, maxWidth: "20px" }} rowSpan="3">
                  {t("InventoryCountVoucher.categoryCCDC")}
                </th>
                <th style={{ ...style.border, minWidth: "20px" }} rowSpan="3">
                  {t("InventoryCountVoucher.sku")}
                </th>
                <th style={{ ...style.border, minWidth: "20px" }} rowSpan="3">
                  {t("InventoryCountVoucher.unitPrice")}
                </th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="2">
                  {t("InventoryCountVoucher.accordingBooks")}
                </th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="2">
                  {t("InventoryCountVoucher.accordingToInventory")}
                </th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="2">
                  {t("InventoryCountVoucher.quantityDifferent")}
                </th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="3">
                  {t("InventoryCountVoucher.classify")}
                </th>
                <th style={{ ...style.alignCenter, ...style.border }} rowspan="3">
                  {t("InventoryCountVoucher.note")}
                </th>
              </tr>
              <tr>
                {/* Theo sổ sách*/}
                <th style={{ ...style.alignCenter, ...style.border }} rowSpan="2">Số lượng</th>
                <th style={{ ...style.alignCenter, ...style.border }} rowSpan="2">Thành tiền</th>

                {/* Thực kiểm kê*/}
                <th style={{ ...style.alignCenter, ...style.border }} rowSpan="2">Số lượng</th>
                <th style={{ ...style.alignCenter, ...style.border }} rowSpan="2">Thành tiền</th>

                {/* NG chênh lệch*/}
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="1">Thừa</th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="1">Thiếu</th>

                {/* Phân loại */}
                <th style={{ ...style.alignCenter, ...style.border, width: "10px" }} colSpan="1">A</th>
                <th style={{ ...style.alignCenter, ...style.border, maxWidth: "10px" }} colSpan="1">B</th>
                <th style={{ ...style.alignCenter, ...style.border }} colSpan="1">C</th>
              </tr>
              <tr>
                <th style={{ ...style.alignCenter, ...style.border }}>Số lượng</th>
                <th style={{ ...style.alignCenter, ...style.border }}>Số lượng</th>
                <th style={{ ...style.alignCenter, ...style.border }}>Đang SD tốt</th>
                <th style={{ ...style.alignCenter, ...style.border, ...style.textWordWrap, maxWidth: "10px" }}>Hỏng không SD đề nghị sửa chữa</th>
                <th style={{ ...style.alignCenter, ...style.border, ...style.textWordWrap, maxWidth: "10px" }}>Hỏng không SD đề nghị thanh lý</th>
              </tr>
            </thead>

            <tbody>
              {item?.assets?.map((row, index) => {
                let {
                  accountantQuantity,
                  accountantOriginalCost,
                  inventoryQuantity,
                  inventoryOriginalCost,
                  asset,
                } = row;
                let totalOriginalCost = inventoryOriginalCost - accountantOriginalCost;
                let totalQuantity = inventoryQuantity - accountantQuantity;
                let unitPrice = asset.originalCost / asset.quantity
                arrAccountantOriginalCost.push(accountantOriginalCost);
                arrAccountantQuantity.push(accountantQuantity);
                arrInventoryOriginalCost.push(inventoryOriginalCost);
                arrInventoryQuantity.push(inventoryQuantity);
                arrDifferenceQuantity.push(totalQuantity);
                arrDifferenceOriginalCost.push(totalOriginalCost);
                arrUnitPrice.push(unitPrice)
                if (totalOriginalCost > 0) {
                  arrPositiveDifferenceOriginalCost.push(totalOriginalCost)
                }
                else {
                  arrNegativeDifferenceOriginalCost.push(totalOriginalCost)
                }
                if (totalQuantity > 0) {
                  arrPositiveDifferenceQuantity.push(totalQuantity)
                }
                else {
                  arrNegativeDifferenceQuantity.push(totalQuantity)
                }

                return (
                  <tr>
                    <td style={{ ...style.alignCenter }}>{index + 1}</td>
                    {/* <td style={{ ...style.code, textAlign: "center"}}>{row.asset?.code}</td> */}
                    <td style={{ ...style.code, padding: 5 }}>{row?.asset?.name}</td>
                    <td style={{ ...style.code, padding: 5 }}>{row.asset?.unit?.name}</td>
                    <td style={style.alignRightOriginalCost}>{convertNumberPriceRoundUp(unitPrice)}</td>
                    <td style={style.alignCenterNumber}>{accountantQuantity}</td>
                    <td style={style.alignRightOriginalCost}>
                      {convertNumberPriceRoundUp(accountantOriginalCost)}
                    </td>
                    <td style={style.alignCenterNumber}>{inventoryQuantity}</td>
                    <td style={style.alignRightOriginalCost}>
                      {convertNumberPriceRoundUp(inventoryOriginalCost)}
                    </td>
                    <td style={style.alignCenterNumber}>
                      {totalQuantity > 0 ? convertNumberPriceRoundUp(totalQuantity) : ""}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost > 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                    <td style={style.alignCenterNumber}>
                      {accountantQuantity}
                      {/* {totalQuantity < 0 ? convertNumberPriceRoundUp(accountantQuantity) :""} */}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost < 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost < 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                    <td style={style.alignRightOriginalCost}>
                      {totalOriginalCost < 0 ? convertNumberPriceRoundUp(totalOriginalCost) : ""}
                    </td>
                  </tr>
                );
              })}
              <tr>
                <th colSpan="3">Tổng cộng</th>
                <th style={{ ...style.alignRight }}>
                  {sumArray(arrUnitPrice)}
                </th>
                <th style={style.alignCenter}>{sumArray(arrAccountantQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrAccountantOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrInventoryQuantity)}</th>
                <th style={style.alignCenter}>{sumArray(arrInventoryOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrPositiveDifferenceQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrPositiveDifferenceOriginalCost)}</th>
                <th style={style.alignCenter}>{sumArray(arrInventoryQuantity)}</th>
                <th style={style.alignRight}>{sumArray(arrNegativeDifferenceOriginalCost)}</th>
                <th style={style.alignRight}>{sumArray(arrNegativeDifferenceOriginalCost)}</th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div style={{ ...style.signature, ...style.bold, fontStyle: "italic", }}>
        <div></div>
        <div></div>
        <div></div>
        <DateToText unItalic date={new Date(item?.inventoryCountDate)} />
      </div>
      <div style={style.signature}>
        <Signature
          titleProps={{ fontSize: 18 }}
          signProps={{ fontSize: 18 }}
          title="Đ/D Khoa, phòng sd tài sản"
        />
        <div>
          <Signature
            titleProps={{ fontSize: 18 }}
            signProps={{ fontSize: 18 }}
            title="Thành viên"
          />
          <div>
            {item?.inventoryCountPersons.map((item, index) => (
              <div style={{ display: "flex" }}>
                <div key={index} >
                  {item?.personName}
                </div>
                <div style={style.borderBottom}></div>
              </div>
            ))}
          </div>
        </div>
        <Signature
          titleProps={{ fontSize: 18 }}
          signProps={{ fontSize: 18 }}
          title="Thư ký"
        />
        <Signature
          titleProps={{ fontSize: 18 }}
          signProps={{ fontSize: 18 }}
          title="Trưởng ban kiểm kê"
        // date={new Date(item?.inventoryCountDate)}
        />
      </div>
    </div>
  );
};