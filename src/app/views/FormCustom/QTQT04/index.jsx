import React from "react";
import { Button, Dialog, DialogActions } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import HeaderRight from "../Component/HeaderRight";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import HeaderLeft from "../Component/HeaderLeft";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const style = {
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  flexSpaceBetween: {
    display: "flex",
    justifyContent: "space-between",
  },
  rowContainer: {
    paddingTop: 10,
  },
  py_2: {
    padding: "0 2%",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  tableRow: {
    border: "1px solid",
  },
  tableCell: {
    borderLeft: "1px solid",
    padding: "4px"
  }
};

export default function QTQT04(props) {
  const { t, open, handleClose, itemEdit } = props;
  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };

  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("Phiếu xác định nhu cầu cần mua")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        class="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className=" dialog-print">
          <div className="form-print">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div style={{ marginTop: "20px" }}>
                <HeaderLeft textAlign={{ textAlign: "center" }} code={itemEdit?.currentUser?.org?.name} />
              </div>
              <div style={{ marginTop: "20px" }}>
                <HeaderRight textAlign={{ textAlign: "center" }} model={"01/QT.QT.04"}/>
              </div>
            </div>

            <div style={{textAlignCenter: "center"}}>
              <Title title={"Phiếu xác Định Nhu Cầu Cần Mua"} />
            </div>

            <p style={style.rowContainer}>
              <div style={style.rowContainer}>
                  {"Căn cứ vào nhu cầu công tác."}
              </div>

              <div style={style.rowContainer}>
                  {"Khoa, phòng: "} 
                  {itemEdit?.receptionDepartmentName}
                  {/* <span style={{ textDecoration: "underline"}}>{""}</span> */}
              </div>

              <div style={style.rowContainer}>
                  {"Cần trang bị thiết bị sau:"}
              </div>

              <div style={style.rowContainer}>
                <table style={style.table}>
                  <thead>
                    <tr style={style.tableRow}>
                      <th style={{...style.tableCell, width: "5%"}}>{"TT"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Danh mục"}</th>
                      <th style={{...style.tableCell, width: "10%"}}>{"ĐVT"}</th>
                      <th style={{...style.tableCell, width: "10%"}}>{"Số lượng"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Thông số"}</th>
                      <th style={{...style.tableCell, width: "25%"}}>{"Ghi chú"}</th>
                    </tr>
                  </thead>

                  <tbody>
                    {
                      itemEdit?.purchaseProducts?.map((item, index) => {
                        return (
                          <tr style={style.tableRow} key={index}>
                            <td style={{ ...style.tableCell, textAlign: "center"}}>{index + 1}</td>
                            <td style={style.tableCell}>{item?.productName}</td>
                            <td style={{...style.tableCell, textAlign: "center"}}>{item?.skuName}</td>
                            <td style={{ ...style.tableCell, textAlign: "center"}}>{item?.quantity}</td>
                            <td style={style.tableCell}>{item?.productSpecs}</td>
                            <td style={style.tableCell}>{item?.note}</td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>

              <div style={{ margin: "10px 0 0 20px" }}>
                {"Vậy kính đề xuất lãnh đạo Bệnh viện xét duyệt."}
              </div>
            </p>

            <div style={style.flexSpaceBetween}>
              <div style={style.py_2}>
                <Signature
                  sign={""}
                  title={itemEdit?.receptionDepartmentName}
                />
              </div>
              <div>
                <Signature sign={""} title={itemEdit?.requestDepartmentName} date={new Date()} isLocation={true} />
              </div>
            </div>

            <div style={{textAlignCenter: "center", marginTop: "20px"}}>
              <Signature
                sign={""}
                title={"Lãnh đạo bệnh viện"}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("general.print")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
