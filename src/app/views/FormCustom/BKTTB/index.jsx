import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    Grid,
    withStyles
} from "@material-ui/core";

import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ValidatorForm } from "react-material-ui-form-validator";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import DateToText from "../Component/DateToText";

const style = {
    textAlignCenter: {
        textAlign: "center",
    },
    textAlignLeft: {
        textAlign: "left",
    },
    table: {
        width: "100%",
        borderCollapse: "collapse"
    },
    pl_26: {
        paddingLeft: "26px"
    },
    px: {
        padding: "0 2px"
    },
    italic: {
        fontStyle: "italic",
    },
    margin:{
        margin: "1em 0",
        display: "flex",
    },
    inputDotted: (child) => {
        return {
            display: child ? "" : "flex",
            flex: 1,
            border: "none",
            borderBottom: "2px dotted #000",
            lineHeight: "1.6rem",
            fontSize: "1rem",
            marginTop: "0",
            position: "relative",
            bottom: "-3px",
            outline: "none",
            height: "26px"
        }
    }
};
const styles = (theme) => ({
    content_container: {
        "& .table-report th.border-none": {
            border: "none !important",
        }
    },
    unHover: {
        pointerEvents: "none !important",
    },
});
const test = ""
const BKTTH = (props) => {
    const { t, handleClose, classes } = props;
    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
    };
    return (
        <Dialog open={props.open} maxWidth="md" fullWidth>
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                <span>{"Phiếu đề xuất tổng hợp"}</span>
            </DialogTitle>
            <iframe
                id="ifmcontentstoprint"
                style={{
                    height: "0px",
                    width: "0px",
                    position: "absolute",
                    print: { size: "auto", margin: "0mm" },
                }}
            ></iframe>
            <ValidatorForm
                class="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <Grid className={classes.content_container}>
                        <div className="form-print">
                            <NationalName></NationalName>
                            <Title title={"Biên bản kiểm tra tổng hợp"}></Title>
                            <div style={style.pl_26}>
                                <div style={{ ...style.margin }}>
                                    <DateToText date={new Date()} noUppercase={true} unItalic={true} isLocation={true} location={"Hôm nay"}></DateToText>
                                    <span>&nbsp;,&nbsp;chúng tôi gồm</span></div>
                                <p style={{ fontWeight: "bold", textTransform: "uppercase" }}>BÊN A&nbsp;:&nbsp;BỆNH VIỆN RĂNG HÀM MẶT TRUNG ƯƠNG TP.HCM</p>
                                <div style={{ ...style.margin }}>
                                    <div style={{ minWidth: "14%" }}>Địa chỉ</div>
                                    <div>:&nbsp;210 Nguyễn chí thành,hành,thành phố hà nội</div>
                                </div>
                                <div style={{ ...style.margin }}>
                                    <div style={{ minWidth: "14%" }}>Điện thoại</div>
                                    <div>:&nbsp;0999999999</div>
                                </div>
                                <div style={{ gap: "10%", ...style.margin }}>
                                    <div style={{ display: "flex" }}>
                                        <div style={{ minWidth: "14%" }}>Người đại diện&nbsp;:&nbsp;</div>
                                        <div>
                                            <span>Nguyễn Minh Hiếu</span>
                                            <div style={style.inputDotted()}></div>
                                        </div>
                                    </div>
                                    <div style={{ display: "flex" }}>
                                        <div style={{ minWidth: "14%" }}>Chức vụ&nbsp;:&nbsp;</div>
                                        <div>
                                            <span>Kỹ thuật viên</span>
                                            <div style={style.inputDotted()}></div>
                                        </div>
                                    </div>
                                </div>
                                <p style={{ fontWeight: "bold", textTransform: "uppercase" }}>BÊN A&nbsp;:&nbsp;BỆNH VIỆN RĂNG HÀM MẶT TRUNG ƯƠNG TP.HCM</p>
                                <div style={{ ...style.margin }}>
                                    <div style={{ minWidth: "14%" }}>Địa chỉ</div>
                                    <div>&nbsp;:&nbsp;210 Nguyễn chí thành,hành,thành phố hà nội</div>
                                </div>
                                <p>Nam</p>
                                <div style={{ ...style.margin }}>
                                    <div style={{ minWidth: "14%" }}>Điện thoại</div>
                                    <div>&nbsp;:&nbsp;0999999999</div>
                                </div>
                                <div style={{ gap: "10%", ...style.margin, marginBottom: "40px" }}>
                                    <div>
                                        Đại diện là&nbsp;:&nbsp;{test ? test : ".................."}
                                    </div>
                                    <div>
                                        Đại diện là&nbsp;:&nbsp;{test ? test : ".................."}
                                    </div>
                                </div>
                                <p style={{ fontWeight: "bold" }}>Thông tin thiết bị</p>
                            </div>
                            <table className=" table-report" style={{ ...style.table }}>
                                <tr style={{ border: "1px solid", ...style.px }} className={classes.unHover}>
                                    <th style={{ textAlign: "center", width: "5%" }}>STT</th>
                                    <th style={{ borderLeft: "1px solid", textAlign: "center", width: "25%", ...style.px }}>Nội dung</th>
                                    <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%", ...style.px }}>Seri</th>
                                    <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%", ...style.px }}>ĐVT</th>
                                    <th style={{ borderLeft: "1px solid", textAlign: "center", width: "15%", ...style.px }}>Số lượng</th>
                                    <th style={{ borderLeft: "1px solid", textAlign: "center", width: "25%", ...style.px }}>Tình hình hư khi kiểm tra</th>
                                </tr>
                                {/* <tr style={{ border: "1px solid" }} className={classes.unHover}>
                                        <th style={{ textAlign: "center", width: "5%" }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "35%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "10%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%",...style.px }}></th>
                                        <th style={{ borderLeft: "1px solid", textAlign: "center", width: "20%" ,...style.px}}></th>
                                    </tr> */}
                            </table>
                            <div style={style.pl_26}>
                                <p style={{ fontWeight: "bold" }}>1.&nbsp;Ý kiến của nhân viên sau khi kiểm tra:</p>
                                <p style={{ paddingLeft: "15px" }}>- Sửa chữa Module đo huyết áp: 2 cái</p>
                                <p>Hai bên thống nhất lập biên bản kiểm tra thiết bị theo những nội dung trên .</p>
                                <p>Biên bản được thành lập 02 bảng , mỗi bên giữ 01 bảng có giá trị pháp lý như nhau.</p>
                            </div>
                            <div style={{ display: "flex", justifyContent: "space-between" }}>
                                    <Signature title={"Nhân viên kỹ thuật bên A"}></Signature>
                                    <Signature title={"Khoa /phòng xác nhận"}></Signature>
                                    <Signature title={"Nhân viên kỹ thuật bên B"}></Signature>
                                </div>
                        </div>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-36"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            {t("general.print")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
};

export default withStyles(styles)(BKTTH);
