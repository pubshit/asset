import {formatTimestampToDate, getUserInformation, spliceString} from "../../../appFunction";
import {appConst} from "../../../appConst";

export const handleConvertAcceptanceData = (item) => {
  const {organization} = getUserInformation();
  const newDatAcceptance = new Date(item?.ngayKyBienBan);
  const dayAcceptance = String(newDatAcceptance?.getDate())?.padStart(2, "0");
  const monthAcceptance = String(newDatAcceptance?.getMonth() + 1)?.padStart(2, "0");
  const yearAcceptance = String(newDatAcceptance?.getFullYear());
  let itemListStatus = [],
    listHong = [],
    listBaoTri = [],
    listKhongBT = [],
    listHdBinhThuong = [];

  item?.amChukys?.map((item) => {
    if (item?.ckTrangthai === appConst.STATUS_CHU_KY.VUA_SUA_KHONG_BT.code) {
      listKhongBT.push(item);
    } else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.HONG_KHONG_BT.code) {
      listHong.push(item);
    } else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY_DXTT_LINH_KIEN.code) {
      listBaoTri.push(item);
      itemListStatus.push(item);
    } else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY.code) {
      listHdBinhThuong.push(item);
      itemListStatus.push(item);
    }
  });

  return {
    ...item,
    titleName: organization,
    org: organization,
    arPhongBanText: item?.arPhongBanText || "..".repeat(30),
    arTnPhongBanText: item?.arTnPhongBanText || "Khoa/Phòng .............................................",
    day: item?.ngayKyBienBan ? dayAcceptance : "........",
    month: item?.ngayKyBienBan ? monthAcceptance : "........",
    year: item?.ngayKyBienBan ? yearAcceptance : "........",
    itemList: itemListStatus?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        ckDate: i?.ckDate ? formatTimestampToDate(i?.ckDate) : "",
        eqName: `${i?.tsTen ? i?.tsTen : ""} ${i.tsModel ? `, ${i.tsModel}` : ""
        }`,
      };
    }),
    listHdBinhThuong: listHdBinhThuong?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        ckDate: i?.ckDate ? formatTimestampToDate(i?.ckDate) : "",
        eqName: `${i?.tsTen ? i?.tsTen : ""} ${i.tsModel ? `, ${i.tsModel}` : ""
        }`,
      };
    }),
    listKhongBT: listKhongBT?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        ckDate: i?.ckDate ? formatTimestampToDate(i?.ckDate) : "",
        eqName: `${i?.tsTen ? i?.tsTen : ""} ${i.tsModel ? `, ${i.tsModel}` : ""
        }`,
      };
    }),
    listHong: listHong?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        ckDate: i?.ckDate ? formatTimestampToDate(i?.ckDate) : "",
        eqName: `${i?.tsTen ? i?.tsTen : ""} ${i.tsModel ? `, ${i.tsModel}` : ""
        }`,
      };
    }),
    amChukys: item?.amChukys?.map((i, x) => {
      return {
        ...i,
        index: x + 1,
        eqName: `${i?.tsTen ? i?.tsTen : ""} ${i.tsMa ? `; ${i.tsMa}` : ""} ${i.tsHangsx ? `; ${i.tsHangsx}` : ""} ${i.tsXuatXu ? `, ${i.tsXuatXu}` : ""} ${i.tsNamsd ? `, ${i.tsNamsd}` : ""}`,
        ckTrangthaiName: appConst.listStatusChuKy.find(x => x.code === i?.ckTrangthai)?.name || ""
      }
    }),
    listHdBinhThuongThanOne: listHdBinhThuong?.length > 1,
    listHdBinhThuongEqualOne: listHdBinhThuong?.length === 1,
    isShowListHdBinhThuong: listHdBinhThuong?.length > 0,
    textHdBinhThuongEqualOne:
      listHdBinhThuong?.length === 1
        ? `- 01 ${listHdBinhThuong[0]?.tsTen || ""} ${listHdBinhThuong[0]?.tsSeriNo
          ? "- " + listHdBinhThuong[0]?.tsSeriNo
          : ""
        }${listHdBinhThuong[0]?.tsModel
          ? "- " + listHdBinhThuong[0]?.tsModel
          : ""
        } ngày ${listHdBinhThuong[0]?.ckDate
          ? formatTimestampToDate(listHdBinhThuong[0]?.ckDate)
          : ""
        }: hoạt động bình thường.`
        : "",
    listKhongBTThanOne: listKhongBT?.length > 1,
    listKhongBTEqualOne: listKhongBT?.length === 1,
    textKhongBTEqualOne:
      listKhongBT?.length === 1
        ? `- 01 ${listKhongBT[0]?.tsTen || ""}
              ${listKhongBT[0]?.tsSeriNo ? " - " + listKhongBT[0]?.tsSeriNo : ""
        }
              ${listKhongBT[0]?.tsModel ? " - " + listKhongBT[0]?.tsModel : ""}
               ngày 
              ${listKhongBT[0]?.ckDate
          ? formatTimestampToDate(listKhongBT[0]?.ckDate)
          : ""
        }
              : mới sửa chữa đang còn bảo hành do vậy không làm bảo trì.`
        : "",
    listHongThanZero: listHong?.length > 0,
  };
}

export const handleConvertProposalPrintData = (item) => {
  const {organization} = getUserInformation();
  const newDate = new Date(item?.dxThoiGian);
  const day = String(newDate?.getUTCDate())?.padStart(2, "0");
  const month = String(newDate?.getMonth() + 1)?.padStart(2, "0");
  const year = String(newDate?.getFullYear());

  return {
    dataPrint: {
      ...item,
      phongBanLapText: spliceString(item?.phongBanLapText),
      tnPhongBanText: spliceString(item?.tnPhongBanText),
      dxNoiDung: item?.dxNoiDung,
      day: item?.dxThoiGian ? day : " ..... ",
      month: item?.dxThoiGian ? month : " ..... ",
      year: item?.dxThoiGian ? year : " ..... ",
      totalQuantity: item?.assets?.reduce((total, item) => total + item?.ckSoluong || 1, 0),
      assets: item?.assets?.map((i, x) => {
        return {
          ...i,
          index: x + 1,
          ckSoluong: i?.ckSoluong || 1,
          tsSerialNoTxt: i?.tsSerialNo ? i?.tsSerialNo : "....",
          tsModelTxt: i?.tsModel ? i?.tsModel : "....",
          tsHangsxTxt: i?.tsHangsx ? i?.tsHangsx : "....",
          tsNamsxTxt: i?.tsNamsx ? i?.tsNamsx : "....",
          tsNamsdTxt: i?.tsNamsd ? i?.tsNamsd : "....",
        };
      }),
    },
    titleName: organization,
  }
}