import {convertDepartmentNameForSignature, getUserInformation} from "app/appFunction";
import { printStyleText } from "../constant";

export const convertPurchaseRequestPrintData = (item) => {
  const { addressOfEnterprise } = getUserInformation();
  const newDate = new Date(item?.requestDate);
  const day = item?.requestDate
    ? String(newDate?.getDate())?.padStart(2, "0")
    : ".....";
  const month = item?.requestDate
    ? String(newDate?.getMonth() + 1)?.padStart(2, "0")
    : ".....";
  const year = item?.requestDate ? String(newDate?.getFullYear()) : "......";
	const receptionDepartmentNameConverted = convertDepartmentNameForSignature(
		item?.receptionDepartment?.name,
		["Phòng ", "P. "],
		"Trưởng phòng "
	)
	
  return {
    itemEdit: {
      ...item,
			evaluationContent: item?.evaluationContent?.replaceAll("\n", "<br/>"),
			confirmationContent: item?.confirmationContent?.replaceAll("\n", "<br/>"),
			bv199Signature: receptionDepartmentNameConverted,
      requestDepartmentName: item?.requestDepartment?.name || "..".repeat(10),
      purchaseProducts: item?.purchaseProducts?.map((i, x) => {
        return {
          ...i,
          index: x + 1,
        };
      }),
    },
    addressOfEnterprise,
    day: item?.requestDate ? day : ".......",
    month: item?.requestDate ? month : ".......",
    year: item?.requestDate ? year : ".......",
    totalavailableQuantity:
      item?.purchaseProducts?.reduce(
        (total, obj) => total + obj?.availableQuantity,
        0
      ) || "",
    totalquantity:
      item?.purchaseProducts?.reduce((total, obj) => total + obj.quantity, 0) ||
      "",
    style: printStyleText,
  };
};
