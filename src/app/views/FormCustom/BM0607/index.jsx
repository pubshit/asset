import { Button, Dialog, DialogActions } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import AppContext from "app/appContext";
import {convertNumberPrice, isSuccessfulResponse} from "app/appFunction";
import { getDetailArSuCo, searchByPageProductOrg } from "app/views/MaintainResquest/MaintainRequestService";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { appConst } from "../../../appConst";
import localStorageService from "../../../services/localStorageService";
import HeaderLeft from "../Component/HeaderLeft";
import Title from "../Component/Title";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const styles = {
  border: {
    border: "1px solid",
    textAlign: "center",
  },
  textJustify: {
    border: "1px solid",
    textAlign: "justify",
    padding: "10px",
    wordBreak:"break-all"
  },
  borderRight: {
    borderRight: "1px solid",
  },
  borderLeft: {
    borderLeft: "1px solid",
  },
  textAlignCenter: {
    textAlign: "center",
    border: "1px solid",
  },
  textAlignRight: {
    textAlign: "right",
    border: "1px solid",
  },
  textAlignLeft: {
    textAlign: "left",
    border: "1px solid",
  },
  flexMidlle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  flexSpaceBetween: {
    display: "flex",
    alignItems: "base-line",
    justifyContent: "space-between",
  },
  justifyContentSpaceAround: {
    display: "flex",
    justifyContent: "space-around",
  },
  headerLeft: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginRight: "50px",
  },
  headerCenter: {
    borderLeft: "1px solid",
    borderRight: "1px solid"
  },
  headerCenterInformation: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  noMargin: {
    margin: 0,
  },
  mt_4px: {
    marginTop: 4,
  },
  mt_10px: {
    marginTop: "10px",
  },
  py_4px: {
    padding: "0 4px",
  },
  rowTitle: {
    textDecoration: "underline",
    fontStyle: "italic",
    margin: "20px 0 10px 0",
  },
  rowGroupBox: {
    display: "grid",
    gridTemplateColumns: "2fr 2fr 2fr 2fr 2fr",
    marginTop: "20px",
  },
  rowResultTitle: {
    display: "grid",
  },
  colGroup: {
    display: "grid",
    gridTemplateColumns: "3fr 2fr 3fr 2fr",
  },
  resultFooter: {
    padding: "4px 8px 0",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
    marginTop: 15,
    marginBottom: 15,
  },
  table_nghiem_thu: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  w_6: {
    border: "1px solid",
    width: "6%",
    textAlign: "center",
    backgroundColor: "white !important",
  },
  w_12: {
    border: "1px solid",
    width: "12%",
    textAlign: "center",
    backgroundColor: "white !important",
  },
  w_18: {
    border: "1px solid",
    width: "18%",
    textAlign: "center",
    backgroundColor: "white !important",
  },
  w_23: {
    border: "1px solid",
    width: "23%",
    textAlign: "center",
    backgroundColor: "white !important",
  },
  w_50: {
    border: "1px solid",
    width: "50%",
    textAlign: "center",
    backgroundColor: "white !important",
  },
  infor: {
    display: "flex",
    flexDirection: "row",
    width: "60%",
  },
  ml_20: {
    marginLeft: "20px",
  },
  mt_50: {
    marginTop: "50px",
  },
  nghiemthu: {
    display: "flex",
    flexWrap: "wrap",
  },
  minHeight_56: {
    minHeight: "56px",
    padding: 5,
  }
};

export default function BM0607(props) {
  const { t, open, handleClose, item, dataNghiemThuSuCo, arTinhTrang } = props;
  let { setPageLoading } = useContext(AppContext);
  const currentUser = localStorageService.getSessionItem(
    appConst.SESSION_STORAGE_KEY.CURRENT_USER
  );
  const [dataSuCo, setDataSuCo] = useState({});
  const [dataUser, setDataUser] = useState([]);
  const [dataLinhKienVatTu, setDataLinhKienVatTu] = useState([]);
  const [checked, setChecked] = useState(true);
  useEffect(() => {
    if (Number(arTinhTrang) === appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code || appConst.STATUS_NGHIEM_THU.THIET_BI_SU_DUNG_DUOC.code ===
      Number(item?.arNtTinhTrang)) {
      setChecked(true)
    }
    else {
      setChecked(false)
    }
  }, [dataSuCo?.arNtTinhTrang, item?.arNtTinhTrang])
  const getListLinhKienVatTu = async () => {
    try {
      // setPageLoading(true);
      // const dataProduct = await searchByPageProductOrg({
      //   keyword: "",
      //   pageIndex: 0,
      //   pageSize: 1000,
      //   productTypeCode: "VTHH",
      // });
      // if (dataProduct?.status === appConst.CODE.SUCCESS) {
      setPageLoading(true);
      const res = await getDetailArSuCo(item.id);
      const { code, data } = res?.data;
      if (isSuccessfulResponse(code)) {
        setDataUser(data?.arUsers);
        setDataSuCo(data);
        setDataLinhKienVatTu(data?.arLinhKienVatTus)
      }
      // }
    } catch (error) {
      toast.error(t("toastr.error"));
    } finally {
      setPageLoading(false);
    }
  };
  useEffect(() => {
    getListLinhKienVatTu();
  }, []);
  const handleFormSubmit = () => {
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
  };
  return (
    <Dialog open={open} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        <span className="mb-20">{t("print.acceptanceVoucher")}</span>
      </DialogTitle>
      <iframe
        id="ifmcontentstoprint"
        style={{
          height: "0px",
          width: "0px",
          position: "absolute",
          print: { size: "auto", margin: "0mm" },
        }}
      ></iframe>

      <ValidatorForm
        class="validator-form-scroll-dialog"
        onSubmit={handleFormSubmit}
      >
        <DialogContent id="divcontents" className=" dialog-print">
          <div className="form-print">
            <div>
              <div>
                <div style={styles.flexSpaceBetween}>
                  <HeaderLeft unit={currentUser?.org?.name} />
                  <div style={styles.headerLeft}>
                    <span style={{ ...styles.textAlignCenter, border: "none" }}>Mã số TB</span>
                    <span>Số phiếu quản lý:</span>
                  </div>
                </div>
              </div>
              <div>
                <Title title={t("print.bm0607")} />
              </div>
            </div>
            <div>
              <div style={styles.rowTitle}>
                <b style={styles.py_4px}>Thông tin thiết bị</b>
              </div>
              <div style={{ ...styles.ml_20, textAlign: "justify" }}>
                Thiết bị:&nbsp;
                {dataSuCo?.tsTen ? dataSuCo?.tsTen?.trim() : ".............."}
              </div>
              <div style={{ ...styles.ml_20, textAlign: "justify" }}>
                Số lượng: ........
              </div>
              <div style={{ ...styles.ml_20, textAlign: "justify" }}>
                Model:&nbsp;{dataSuCo?.tsModel ? dataSuCo?.tsModel?.trim() : ".........."}
              </div>
              <div style={{ ...styles.ml_20, textAlign: "justify" }}>
                Seri:&nbsp;{dataSuCo?.tsSerialNo ? dataSuCo?.tsSerialNo?.trim() : ".........."}
              </div>
            </div>
            <div>
              <div style={styles.rowTitle}>
                <b style={styles.py_4px}>Hiện trạng/Công việc cần thực hiện</b>
              </div>
              <div style={{ ...styles.ml_20, textAlign: "justify" }}>
                <div>Mô tả sự cố:&nbsp;{dataSuCo?.arMoTa?.trim()}</div>
                <div style={styles.mt_10px}>Phân tích sơ bộ nguyên nhân:&nbsp;{dataSuCo?.arPhanTichSoBo?.trim()}</div>
                <div style={styles.mt_10px}>Đề xuất biện pháp:&nbsp;{dataSuCo?.arDeXuatBienPhap?.trim()}</div>
              </div>
            </div>

            <div style={styles.rowTitle}>
              <b style={styles.py_4px}>Vật tư cần sử dụng</b>
            </div>
            <table style={styles.table}>
              <tr>
                <th style={styles.w_6}>{t("Asset.stt")}</th>
                <th style={styles.w_23}>
                  {t("ReportIncident.AssetAcceptanceForm.deviceComponents")}
                </th>
                <th style={styles.w_23}>
                  {t("ReportIncident.AssetAcceptanceForm.nameOfMaterialsUsed")}
                </th>
                <th style={styles.w_12}>
                  {t("ReportIncident.AssetAcceptanceForm.quantity")}
                </th>
                <th style={styles.w_18}>
                  {t("ReportIncident.AssetAcceptanceForm.unitPrice")}
                </th>
                <th style={styles.w_18}>
                  {t("ReportIncident.AssetAcceptanceForm.totalAmount")}
                </th>
              </tr>
              {dataLinhKienVatTu?.map((data, index) => (
                <tr>
                  <td style={styles.border}>{index + 1}</td>
                  <td style={styles.textAlignLeft}>{data.nameOfRequiredMaterial}</td>
                  <td style={styles.textAlignLeft}>{data.productName}</td>
                  <td style={styles.border}>{data.productQty}</td>
                  <td style={styles.textAlignRight}>
                    {convertNumberPrice(data.productCost)}
                  </td>
                  <td style={styles.textAlignRight}>
                    {convertNumberPrice(data.productTotal)}
                  </td>
                </tr>
              ))}
              {dataSuCo?.arHinhThuc === Number(appConst.HINH_THUC_SUA_CHUA.SUA_CHUA_NGOAI.code) && <tr>
                <td style={styles.textAlignLeft} colSpan={5}>
                  {t("ReportIncident.AssetAcceptanceForm.costOfRepairIncurred")}
                </td>
                <td style={styles.textAlignRight}>
                  {convertNumberPrice(dataSuCo?.arChiPhi ? dataSuCo?.arChiPhi : 0)}
                </td>
              </tr>}
              <tr>
                <td style={styles.textAlignLeft} colSpan={5}>
                  {t("ReportIncident.AssetAcceptanceForm.totalCost")}
                </td>
                <td style={styles.textAlignRight}>
                  {convertNumberPrice(
                    dataSuCo?.arTongChiPhi ? dataSuCo?.arTongChiPhi : 0
                  )}
                </td>
              </tr>
            </table>

            <table style={styles.table}>
              <thead>
                <tr>
                  <th style={{ ...styles.border, ...styles.textAlignCenter }}>Người sử dụng TB</th>
                  <th style={{ ...styles.border, ...styles.textAlignCenter }}>Khoa/Phòng quản lý</th>
                  <th style={{ ...styles.border, ...styles.textAlignCenter }}>Khoa/Phòng sử dụng</th>
                  <th style={{ ...styles.border, ...styles.textAlignCenter }}>Người sửa chữa, bảo dưỡng</th>
                  <th style={{ ...styles.border, ...styles.textAlignCenter }}>Ngày hoàn thành</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style={{ ...styles.border, padding: 5 }}>

                  </td>
                  <td style={{ ...styles.border, padding: 5 }}>
                    {dataSuCo?.tsPhongBanText}
                  </td>
                  <td style={{ ...styles.border, padding: 5 }}>
                    {dataSuCo?.arPhongBanText}
                  </td>
                  <td style={{ ...styles.border, padding: 5 }}>
                    {dataUser[0]?.userName}
                  </td>
                  <td style={{ ...styles.border, padding: 5 }}>
                    {moment(dataSuCo?.arHanSuaChuaXong).format("DD/MM/YYYY")}
                  </td>
                </tr>
              </tbody>
            </table>

            {/* Kết quả nghiệm thu */}
            <div style={styles.rowResultTitle}>
              <div>
                <div style={{ ...styles.rowTitle, ...styles.noMargin }}>
                  <b style={styles.py_4px}>
                    Nghiệm thu kết quả bảo dưỡng/ sửa chữa
                  </b>
                </div>

                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                    minHeight: "88%",
                    flexWrap: "wrap",
                  }}
                >
                  <div style={styles.nghiemthu}>
                    {dataSuCo?.arNghiemThus?.map((item) => (
                      <table style={styles.table_nghiem_thu}>
                        <tr>
                          <th style={styles.w_50}>Hạng mục</th>
                          <th style={styles.w_50}>Kết luận</th>
                        </tr>
                        <tr>
                          <td style={styles.textJustify}>
                            {item?.arNtHangMuc?.trim()}
                          </td>
                          <td style={styles.textJustify}>
                            {item?.arNtKetLuan?.trim()}
                          </td>
                        </tr>
                      </table>
                    ))}
                  </div>
                  <div style={{ ...styles.headerLeft, ...styles.resultFooter }}>
                    <span>Kết luận sau khi sửa chữa, bảo dưỡng</span>
                    <div style={styles.justifyContentSpaceAround}>
                      <div>
                        Thiết bị sử dụng được &nbsp;
                        <input type="checkbox" checked={checked ?? true} />
                      </div>
                      <div style={styles.ml_20}>
                        Thiết bị không sử dụng được &nbsp;
                        <input type="checkbox" checked={!checked} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div className="flex flex-space-between flex-middle">
            <Button
              variant="contained"
              color="secondary"
              className="mr-36"
              onClick={handleClose}
            >
              {t("general.cancel")}
            </Button>
            <Button variant="contained" color="primary" type="submit">
              {t("general.print")}
            </Button>
          </div>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
}
