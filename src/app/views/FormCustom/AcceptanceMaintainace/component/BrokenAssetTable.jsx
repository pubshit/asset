import { formatTimestampToDate } from "app/appFunction";


const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
    fontSize: "14px",
  },
  border: {
    border: "1px solid",
    textAlign: "center",
    padding: 8,
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
    padding: 8,
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
    padding: 8,
  },
  w_12: {
    border: "1px solid",
    minWidth: "12%",
    textAlign: "center",
    padding: 8,
  },
  w_15: {
    border: "1px solid",
    width: "15%",
    textAlign: "center",
    padding: 8,
  },
  w_25: {
    border: "1px solid",
    width: "25%",
    textAlign: "center",
    padding: 8,
  },
  p_8: {
    padding: 8,
  },
  indent_35: {
    textIndent: 35,
  },
}

export const BrokenAssetTable = (props) => {
  const { 
    itemList
  } = props;

  return (
    <table style={style.table} className="table-report">
      <thead style={style.header}>
        <tr>
          <th style={style.w_5} rowSpan="2">
            STT
          </th>
          <th style={style.w_12} rowSpan="2">
            Ngày thực hiện
          </th>
          <th style={style.w_25} rowSpan="2">
            Tên thiết bị, Moldel
          </th>
          <th style={style.w_15} rowSpan="2">
            Khi BT phát hiện sự cố
          </th>
          <th style={style.w_15} rowSpan="2">
            Nguyên nhân
          </th>
          <th style={style.w_15} rowSpan="2">
            Đề xuất BP khắc phục
          </th>
          <th style={style.w_12} rowSpan="2">
            Xác nhận của kỹ sư BT
          </th>
          <th style={style.w_12} rowSpan="2">
            Xác nhận của Khoa, Phòng sử dụng
          </th>
        </tr>
      </thead>
      {itemList?.map((row, index) => {
        return (
          <tbody>
            <tr>
              <td style={style.border}>
                {index !== null ? index + 1 : ""}
              </td>
              <td
                style={{
                  ...style.border,
                  ...style.textAlignCenter,
                  ...style.p_8,
                }}
              >
                {row?.ckDate ? formatTimestampToDate(row?.ckDate) : ""}
              </td>

              <td
                style={{
                  ...style.border,
                  ...style.textAlignLeft,
                  ...style.p_8,
                }}
              >
                {row?.tsTen ? row?.tsTen : ""}
              </td>

              <td 
                style={{
                  ...style.border,
                  ...style.textAlignLeft,
                  ...style.p_8,
                }}
              >
                {row?.noiDungSuCo ? row?.noiDungSuCo : ""}
              </td>

              <td
                style={{
                  ...style.border,
                  ...style.textAlignLeft,
                  ...style.p_8,
                }}
              >
                {row?.nguyenNhanSuCo ? row?.nguyenNhanSuCo : ""}
              </td>

              <td style={{
                ...style.border,
                ...style.textAlignLeft,
                ...style.p_8,
              }}>
              {row?.deXuatKhacPhuc ? row?.deXuatKhacPhuc : ""}
              </td>

              <td style={style.border}></td>

              <td style={{
                ...style.border,
                ...style.textAlignLeft,
                ...style.p_8,
              }}>
                {row?.khoaPhongSuDungText ? row?.khoaPhongSuDungText : ""}
              </td>
            </tr>
          </tbody>
        )
      })}
    </table>
  )
}

