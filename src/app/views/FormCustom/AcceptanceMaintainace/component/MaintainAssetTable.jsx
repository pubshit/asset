import { formatTimestampToDate } from "app/appFunction";


const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  table: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
    fontSize: "14px",
  },
  border: {
    border: "1px solid",
    textAlign: "center",
    padding: 8,
  },
  w_5: {
    border: "1px solid",
    width: "5%",
    textAlign: "center",
    padding: 8,
  },
  w_8: {
    border: "1px solid",
    width: "8%",
    textAlign: "center",
    padding: 8,
  },
  w_12: {
    border: "1px solid",
    width: "12%",
    textAlign: "center",
    padding: 8,
  },
  w_15: {
    border: "1px solid",
    width: "15%",
    textAlign: "center",
    padding: 8,
  },
  w_25: {
    border: "1px solid",
    width: "25%",
    textAlign: "center",
    padding: 8,
  },
  p_8: {
    padding: 8,
  },
  indent_35: {
    textIndent: 35,
  },
}

export const MaintainAssetTable = (props) => {
  const { 
    itemList
  } = props;

  return (
    <table style={style.table} className="table-report">
      <thead style={style.header}>
        <tr>
          <th style={style.w_5} rowSpan="2">
            STT
          </th>
          <th style={style.w_12} rowSpan="2">
            Ngày thực hiện
          </th>
          <th style={style.w_25} rowSpan="2">
            Tên thiết bị, Moldel
          </th>
          <th style={style.w_8} rowSpan="2">
            Nước SX
          </th>
          <th style={style.w_5} rowSpan="2">
            ĐVT
          </th>
          <th style={style.w_5} rowSpan="2">
            SL
          </th>
          <th style={style.w_25} rowSpan="2">
            Nội dung cần bảo trì
          </th>
          <th style={style.w_15} rowSpan="2">
            Xác nhận của Khoa, Phòng sử dụng
          </th>
        </tr>
      </thead>
      {itemList?.map((row, index) => {
        return (
          <tbody>
            <tr>
              <td style={style.border}>
                {index !== null ? index + 1 : ""}
              </td>
              <td
                style={{
                  ...style.border,
                  ...style.textAlignCenter,
                  ...style.p_8,
                }}
              >
                {row?.ckDate ? formatTimestampToDate(row?.ckDate) : ""}
              </td>
              <td
                style={{
                  ...style.border,
                  ...style.textAlignLeft,
                  ...style.p_8,
                }}
              >
                {row?.tsTen ? row?.tsTen : ""}
              </td>
              <td 
                style={{
                  ...style.border,
                  ...style.textAlignLeft,
                  ...style.p_8,
                }}
              >
                {row?.tsHangsx ? row?.tsHangsx : ""}
              </td>
              <td
                style={{
                  ...style.border,
                  ...style.textAlignCenter,
                  ...style.p_8,
                }}
              >
                {row?.tsDvtTen ? row?.tsDvtTen : ""}
              </td>
              <td style={{
                ...style.border,
                ...style.textAlignCenter,
                ...style.p_8,
              }}>
                1
              </td>
              <td style={{
                ...style.border,
                ...style.textAlignLeft,
              }}>
                {row?.congViec ? row?.congViec : ""}
              </td>
              <td style={{
                ...style.border,
                ...style.textAlignLeft,
                ...style.p_8,
              }}>
                {row?.khoaPhongSuDungText ? row?.khoaPhongSuDungText : ""}
              </td>
            </tr>
          </tbody>
        )
      })}
    </table>
  )
}

