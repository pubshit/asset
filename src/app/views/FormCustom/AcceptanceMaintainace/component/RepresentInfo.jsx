

const style = {
  pl_35: {
    paddingLeft: 35,
  },
  grid: {
    display: "grid",
  },
  gridCol_2_8: {
    gridTemplateColumns: "2fr 8fr",
  },
  gridCol_1_1: {
    gridTemplateColumns: "1fr 1fr",
  },
}

export const RepresentInfo = (props) => {
  const { 
    org,
    unitSupply
  } = props;

  return (
    <>
      <b>Bên A: {org?.name} - {org?.code}</b>
      <div style={style.pl_35}>
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Địa chỉ</div>
          <div>:&nbsp;{org?.address}</div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Điện thoại</div>
          <div>:&nbsp;{org?.numberPhone}</div>
        </div>

        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Fax</div>
          <div>: </div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Mã số thuế</div>
          <div>:&nbsp;{org?.taxCode}</div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Tài khoản số</div>
          <div>: </div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Do</div>
          <div>
            <div style={{ 
              ...style.grid,
              ...style.gridCol_1_1,
            }}> 
              <div>: Ông</div>
              <div>Chức vụ: </div>
            </div>

            <div style={{ 
              ...style.grid,
              ...style.gridCol_1_1,
            }}> 
              <div>: Bà</div>
              <div>Chức vụ: </div>
            </div>

            <div style={{ 
              ...style.grid,
              ...style.gridCol_1_1,
            }}> 
              <div>: Ông</div>
              <div>Chức vụ: </div>
            </div>
          </div>
        </div>
      </div>
      <b>Bên B: {unitSupply}</b>
      <div style={style.pl_35}>
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Địa chỉ</div>
          <div>:&nbsp;</div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Điện thoại</div>
          <div>:&nbsp;</div>
        </div>

        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Fax</div>
          <div>: </div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Mã số thuế</div>
          <div>:&nbsp;</div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Tài khoản số</div>
          <div>: </div>
        </div>
        
        <div style={{ 
          ...style.grid,
          ...style.gridCol_2_8,
        }}> 
          <div>Do</div>
          <div>
            <div style={{ 
              ...style.grid,
              ...style.gridCol_1_1,
            }}> 
              <div>: Ông</div>
              <div>Chức vụ: </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

