import React, { Component } from "react";
import { Dialog, Button, Grid, DialogActions } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import localStorageService from "app/services/localStorageService";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import { PaperComponent, formatTimestampToDate, spliceString } from "app/appFunction";
import { appConst } from "app/appConst";
import { RepresentInfo } from "./component/RepresentInfo";
import { MaintainAssetTable } from "./component/MaintainAssetTable";
import { NoMaintainAssetTable } from "./component/NoMaintainAssetTable";
import { NormallyAssetTable } from "./component/NormallyAssetTable";
import { BrokenAssetTable } from "./component/BrokenAssetTable";

const style = {
  textAlignCenter: {
    textAlign: "center",
  },
  title: {
    fontSize: "0.975rem",
    fontWeight: "bold",
    marginBottom: "1em",
    marginTop: "1.5em",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  fontBold: {
    fontWeight: "bold",
  },
  fontItalic: {
    fontStyle: "italic",
  },
  pl_35: {
    paddingLeft: 35,
  },
  nameColumn: {
    border: "1px solid",
    textAlign: "left",
    paddingLeft: "10px",
    wordBreak: "break-all",
  },
  signatureContainer: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    marginTop: "20px",
  },
  formContent: {
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: "left",
  },
  grid: {
    display: "grid",
  },
  gridCol_2_8: {
    gridTemplateColumns: "2fr 8fr",
  },
  indent_35: {
    textIndent: 35,
  },
};

export default class AcceptanceMaintainace extends Component {
  state = {
    type: 2,
    rowsPerPage: 10,
    page: 0,
    totalElements: 0,
    departmentId: "",
    asset: {},
    isView: false,
    keyword: "",
    itemList: [],
  };

  handleFormSubmit = () => {
    let { handleOKEditClose, handleClose } = this.props;
    let content = document.getElementById("divcontents");
    let pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();

    pri.document.write(content.innerHTML);

    pri.document.close();
    pri.focus();
    pri.print();
    //eslint-disable-next-line
    // handleClose?.();
  };
  
  componentWillMount() {
    const { item } = this.props;
    let itemList = [], 
      listHong = [], 
      listBaoTri = [],
      listKhongBT = [],
      listHdBinhThuong = [];
    item?.amChukys?.map(item => {
      if (item?.ckTrangthai === appConst.STATUS_CHU_KY.VUA_SUA_KHONG_BT.code) {
        listKhongBT.push(item);
      }
      else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.HONG_KHONG_BT.code) {
        listHong.push(item);
      }
      else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY_DXTT_LINH_KIEN.code) {
        listBaoTri.push(item);
        itemList.push(item);
      }
      else if (item?.ckTrangthai === appConst.STATUS_CHU_KY.DA_XU_LY.code) {
        listHdBinhThuong.push(item);
        itemList.push(item);
      }
    })
    this.setState({ 
      ...item,
      itemList,
      listHong,
      listBaoTri,
      listKhongBT,
      listHdBinhThuong,
    });
  }

  render() {
    let { open, t, item } = this.props;
    let {
      itemList,
      listHong,
      listBaoTri,
      listKhongBT,
      listHdBinhThuong,
    } = this.state;
    let org = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CONFIG_ORGANIZATION)
    let ngayKyBienBan = item?.ngayKyBienBan 
      ? new Date(item?.ngayKyBienBan).getDate()
      : "";
    let thangKyBienBan = item?.ngayKyBienBan 
      ? new Date(item?.ngayKyBienBan).getMonth()
      : "";
    let namKyBienBan = item?.ngayKyBienBan 
      ? new Date(item?.ngayKyBienBan).getFullYear()
      : "";

    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          <span className="mb-20">{t("MaintenanceProposal.title")}</span>
        </DialogTitle>
        <iframe
          id="ifmcontentstoprint"
          style={{
            height: "0px",
            width: "0px",
            position: "absolute",
            print: { size: "auto", margin: "0mm" },
          }}
          title="ifmcontentstoprint"
        ></iframe>

        <ValidatorForm
          ref="form"
          class="validator-form-scroll-dialog"
          onSubmit={this.handleFormSubmit}
        >
          <DialogContent id="divcontents" className=" dialog-print">
            <Grid>
              <div
                style={style.textAlignCenter}
                className="form-print form-print-landscape"
              >
                <NationalName />
                <Title 
                  title="Biên bản bàn giao nghiệm thu thực tế bảo trì"
                />

                <div style={style.formContent}>
                  <p style={style.pl_35}>
                    - Căn cứ vào hợp đồng kinh tế: 
                    số................... ký ngày
                    &nbsp;&nbsp;&nbsp;tháng
                    &nbsp;&nbsp;&nbsp;năm
                  </p>

                  <p style={{ ...style.pl_35, ...style.fontItalic}}>
                    Hôm nay, ngày &nbsp;{ngayKyBienBan}&nbsp;
                    tháng &nbsp;{thangKyBienBan}&nbsp;
                    năm &nbsp;{namKyBienBan}&nbsp;,&nbsp;
                    đại diện hai bên chúng tôi gồm có:
                  </p>

                  <p>
                    <RepresentInfo org={org} unitSupply={item?.donViThucHienText}/>
                  </p>

                  <p style={style.indent_35}>
                    Hai bên đồng ý ký biên bản bàn giao nghiệm thu bảo trì 
                    theo hợp đồng kinh tế số: .........
                    Ngày&nbsp;&nbsp;&nbsp;tháng&nbsp;&nbsp;&nbsp;năm 20&nbsp;&nbsp;
                    như sau:
                  </p>

                  <b>1. Nội dung chính:</b>

                  <p style={style.indent_35}>
                    Bên B đã thực hiện xong công việc bảo trì 
                    cho các trang thiết bị cụ thể như sau:
                  </p>
                  <MaintainAssetTable itemList={itemList} />


                  <p>
                    <b>2. Chất lượng thực hiện:</b>
                    {/* DS TS hoạt động bình thường */}
                    <div style={style.indent_35}>
                      {listHdBinhThuong?.length > 1
                        ? (<>
                            - Sau khi bảo trì các tài sản sau hoạt động bình thường: 
                          <NormallyAssetTable itemList={listHdBinhThuong} />
                        </>)
                        : listHdBinhThuong?.length === 1 
                          ? `
                            - 01 ${listHdBinhThuong[0]?.tsTen || ""}
                            ${
                              listHdBinhThuong[0]?.tsSeriNo
                              ? "- " + listHdBinhThuong[0]?.tsSeriNo 
                              : ""
                            }
                            ${
                              listHdBinhThuong[0]?.tsModel
                              ? "- " + listHdBinhThuong[0]?.tsModel 
                              : ""
                            }
                            ngày
                            ${
                              listHdBinhThuong[0]
                              ?.ckDate ? formatTimestampToDate(listHdBinhThuong[0]?.ckDate) 
                              : ""
                            }
                            : hoạt động bình thường.
                          `
                          : ""
                      }
                    </div>

                    {/* DS TS mới sửa chữa k BT */}
                    <div style={style.indent_35}>
                      {listKhongBT?.length > 1
                        ? (<>
                            - Các tài sản sau do mới sửa chữa nên không bảo trì: 
                          <NoMaintainAssetTable itemList={listKhongBT} />
                        </>)
                        : listKhongBT?.map(item => (<div>
                          - 01&nbsp;{item?.tsTen || ""}
                          {
                            item?.tsSeriNo
                            ? " - " + item?.tsSeriNo 
                            : ""
                          }
                          {
                            item?.tsModel
                            ? " - " + item?.tsModel 
                            : ""
                          }
                          &nbsp;ngày&nbsp;
                          {
                            item?.ckDate 
                            ? formatTimestampToDate(item?.ckDate) 
                            : ""
                          }
                          : mới sửa chữa đang còn bảo hành
                          do vậy không làm bảo trì.
                          </div>))
                      }
                    </div>
                    {listHong?.length > 0 && (
                      <>
                        <div style={style.indent_35}>
                          - Một số máy hỏng không tiến hành bảo trì cụ thể như sau:
                        </div>
                        <BrokenAssetTable itemList={listHong} />
                      </>
                    )}
                  </p>
                  
                  <p>
                    <b>3. Kết luận:</b>
                    <div style={style.indent_35}>
                      - Chấp nhận nghiệm thu hoàn thành công việc bảo dưỡng
                      các trang thiết bị y tế nêu trên,
                      đồng ý triển khai đưa vào hoạt động.
                    </div>
                    <div style={style.indent_35}>
                      - Biên bản được lập thành 04 bản,
                      bên A giữ 02 bản,
                      bên B giữ 02 bản
                      có giá trị pháp lý ngang nhau.
                    </div>
                  </p>

                  <div style={style.signatureContainer}>
                    <div>
                      {/* <div style={style.signatureItemHeader} className="mt-25">
                        &nbsp;
                      </div> */}
                      <Signature title="Người lập" name={item?.dxTen} />
                    </div>
                    <Signature
                      title="Trưởng khoa"
                      isLocation={true}
                    />
                  </div>
                </div>
              </div>
            </Grid>
          </DialogContent>
          <DialogActions>
            <div className="flex flex-space-between flex-middle">
              <Button
                variant="contained"
                color="secondary"
                className="mr-16"
                onClick={() => this.props.handleClose()}
              >
                {t("general.cancel")}
              </Button>
              <Button variant="contained" color="primary" type="submit">
                {t("general.print")}
              </Button>
            </div>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    );
  }
}
