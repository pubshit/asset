import {LIST_ORGANIZATION} from "../../appConst";
import {
  FIXED_ASSET_MANAGEMENT,
  IAT_MANAGEMENT,
  SHOPPING_MANAGEMENT,
  SUPPLIES_MANAGEMENT,
  WAREHOUSE_MANAGEMENT
} from "./formUrls";

export const printStyles = {
  d_flex: {
    display: "flex",
  },
  justify_content: {
    space_between: {
      justifyContent: "space-between",
    },
    space_around: {
      justifyContent: "space-around",
    },
  },
  font_size: {
    _23px: {
      fontSize: "23px",
    },
    _24px: {
      fontSize: "24px",
    },
  },
  font_weight: {
    _600: {
      fontWeight: "600",
    },
  },
  line_height: {
    _16px: {
      lineHeight: "16px",
    },
  },
  width: {
    _25persent: {
      width: "25%",
    },
  },
  margin: {
    top: {
      _10px: {
        marginTop: "10px",
      },
    },
    bottom: {
      _30px: {
        marginBottom: "30px",
      },
    },
  },
  text_indent: {
    _40px: {
      textIndent: "40px",
    },
  },
  text_align: {
    right: {
      textAlign: "right",
    },
    center: {
      textAlign: "center",
    },
  },
  text_transform: {
    uppercase: {
      textTransform: "uppercase",
    },
  },
  tableCollapse: {
    width: "100%",
    border: "1px solid",
    borderCollapse: "collapse",
  },
  border: {
    border: "1px solid",
    padding: "5px",
  },
};

export const printStyleText = {
  fs17: "font-size: 17px;",

  //display
  flex: "display: flex;",
  flexCenter: "justify-content: center;",
  flexMiddle: "align-items: center;",

  //position
  posRelative: "position: relative;",
  posAbsolute: "position: absolute;",

  //custom
  underline:
    "position: absolute; bottom: -4px; left: 15%; right: 15%; border-bottom: .5px solid #000;",
};

export const LIST_PRINT_FORM_BY_ORG = {
  // Với mỗi phiếu có thể để 1 list string hoặc object hoặc cả 2 (string | object)[]
  //  object[]: {
  //    tabLabel:...,
  //    url:...,
  //    config: {
  //      layout: portrait, landscape, A4,...
  //      margin: vertical | horizontal,
  //    },
  //  }
  SHOPPING_MANAGEMENT,
  FIXED_ASSET_MANAGEMENT,
  IAT_MANAGEMENT,
  SUPPLIES_MANAGEMENT,
  WAREHOUSE_MANAGEMENT,
};
