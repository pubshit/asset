import React, {useContext, useEffect, useState} from "react";
import { Button, Dialog, DialogActions, makeStyles } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { ValidatorForm } from "react-material-ui-form-validator";
import DateToText from "../Component/DateToText";
import Signature from "../Component/Signature";
import Title from "../Component/Title";
import NationalName from "../Component/NationalName";
import HeaderLeft from "../Component/HeaderLeft";
import localStorageService from "app/services/localStorageService";
import {appConst, LIST_ORGANIZATION} from "app/appConst";
import {
    convertDepartmentNameForSignature,
    convertNumberPrice,
    convertToAcronym,
    getUserInformation
} from "app/appFunction";
import Mustache from 'mustache';
import AppContext from "../../../appContext";

const currentUser = localStorageService.getSessionItem(appConst.SESSION_STORAGE_KEY.CURRENT_USER);
const organizationName = currentUser?.org?.name;

function PaperComponent(props) {
    return (
        <Draggable
            handle="#draggable-dialog-title"
            cancel={'[class*="MuiDialogContent-root"]'}
        >
            <Paper {...props} />
        </Draggable>
    );
}

const style = {
    title: {
        fontSize: "0.975rem",
        fontWeight: "bold",
        marginBottom: "1em",
        marginTop: "1.5em",
    },
    flexSpaceBetween: {
        display: "flex",
        justifyContent: "space-between",
    },
    rowContainer: {
        fontSize: "1.07rem",
    },
    py_2: {
        padding: "0 2%",
    },
    table: {
        width: "100%",
        borderCollapse: "collapse",
        border: "1px solid",
        fontSize: "1rem"
    },
    tableRow: {
        border: "1px solid",
    },
    tableCell: {
        borderLeft: "1px solid",
        padding: "4px",
        fontSize: "0.85rem"
    },
    textDataContainer: {
        display: "inline-block",
        position: "relative",
    },
    textDataUnderline: {
        position: "absolute",
        // top: 0, left: 0, right: 0, bottom: "3px",
        // borderBottom: "1px dotted"
    },
    a4Container: {
        width: "21cm",
        margin: "auto",
        fontSize: "18px",
        padding: "75.590551181px 56.692913386px 75.590551181px 113.38582677px",
    },
};

const useStyles = makeStyles({
    textDataUnderline: {
        bottom: "5px !important",
    }
})

export default function DCTSTB(props) {
    const { t, dataPhieu, handleClose } = props;
    const classes = useStyles();
    const [html, setHtml] = useState("")
    const handleFormSubmit = () => {
        let content = document.getElementById("divcontents");
        let pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();

        pri.document.write('<style>@page { margin: 2cm 1.5cm 2cm 3cm; }</style>');
        pri.document.write(content.innerHTML);

        pri.document.close();
        pri.focus();
        pri.print();
    };


    const addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");

    const newDate = new Date(dataPhieu?.issueDate)
    const day = String(newDate?.getUTCDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());

    let dataView = {
        dataPhieu: {
            ...dataPhieu,
            assetVouchers: dataPhieu?.assetVouchers?.map((i, x) => {
                i.index = x + 1;
                return {
                    ...i,
                    assetOriginalCost: convertNumberPrice(i?.assetOriginalCost) ?? "",
                    assetCarryingAmount: convertNumberPrice(i?.assetCarryingAmount) ?? "",
                }
            })
        },
        addressOfEnterprise,
        organizationName,
        day,
        month,
        year
    }

    const renderTemplate = async () => {
        try {
            const res = await fetch("/assets/form-print/DCTSTBTemplate.txt")
            const data = await res.text();
            setHtml(Mustache.render(data, dataView))
        } catch (error) {

        }
    }
    useEffect(() => {
        renderTemplate();
    }, [])

    return (
        <Dialog open={true} PaperComponent={PaperComponent} maxWidth="md" fullWidth>
            <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                <span className="mb-20">{t("Phiếu điều chuyển tài sản thiết bị")}</span>
            </DialogTitle>
            <iframe
                id="ifmcontentstoprint"
                style={{
                    height: "0px",
                    width: "0px",
                    position: "absolute",
                    print: { size: "auto", margin: "0mm" },
                }}
            ></iframe>

            <ValidatorForm
                class="validator-form-scroll-dialog"
                onSubmit={handleFormSubmit}
            >
                <DialogContent id="divcontents" className="dialog-print">
                    <div dangerouslySetInnerHTML={{ __html: html }} />
                    {/* <div className="form-print">
                        <div>
                            <div style={{ display: "flex", justifyContent: "space-between" }}>
                                <div>
                                    <HeaderLeft textAlign={{ textAlign: "center" }} unit={organizationName} />
                                    <div style={{ marginTop: "16px" }}>{`Số:........../`}{`........`}{`-ĐC`}</div>
                                </div>
                                <div>
                                    <NationalName />
                                    <div style={{ margin: "16px 0 0 0", textAlign: "center" }}>
                                        <DateToText isLocation noUppercase date={dataPhieu?.issueDate} />
                                    </div>
                                </div>
                            </div>

                            <div style={{ textAlign: "center", marginBottom: 30 }}>
                                <Title title={"Phiếu điều chuyển tài sản thiết bị"} />
                            </div>

                            <div style={{ ...style.rowContainer, marginBottom: 8 }}>
                                <div style={style.rowContainer}>
                                    {"- Căn cứ vào....................... số............ ngày....... tháng....... năm........ của..........................."}
                                </div>
                                <div style={style.rowContainer}>
                                    {"- Căn cứ vào nhu cầu thực tế tại các đơn vị, nay điều chuyển tài sản thiết bị:"}
                                </div>
                                <div style={style.rowContainer}>
                                    &nbsp;&nbsp;Từ: <div style={style.textDataContainer}>
                                        <span style={{ marginRight: "8px" }}>{dataPhieu?.handoverDepartmentName ?? ""}</span>
                                        <span style={style.textDataUnderline} className={classes.textDataUnderline}></span>
                                    </div>
                                    &nbsp;đến: <div style={style.textDataContainer}>
                                        <span style={{ marginRight: "8px" }}>{dataPhieu?.receiverDepartmentName ?? ""}</span>
                                        <span style={style.textDataUnderline} className={classes.textDataUnderline}></span>
                                    </div>
                                    , danh mục kèm theo dưới đây:
                                </div>
                                <table style={{
                                    width: "100%",
                                    borderCollapse: "collapse",
                                    border: "none",
                                    fontSize: "1rem",
                                    fontWeight: 600,
                                    marginLeft: "20px",
                                }}>
                                    <thead>
                                    </thead>
                                    <tbody style={{ border: "none" }}>
                                        <tr style={{ border: "none" }}>
                                            <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>- Đại diện bên giao: Ông (Bà): {dataPhieu?.handoverPersonName ?? ""}.</th>
                                            <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>Đơn vị: {dataPhieu?.handoverDepartmentName ?? ""}.</th>
                                        </tr>
                                        <tr style={{ border: "none" }}>
                                            <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>- Đại diện bên nhận: Ông (Bà): {dataPhieu?.receiverPersonName ?? ""}.</th>
                                            <th style={{ border: "none", fontWeight: 500, textAlign: "left" }}>Đơn vị: {dataPhieu?.receiverDepartmentName ?? ""}.</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div style={style.rowContainer}>
                            <table style={style.table}>
                                <thead style={{ border: "1px solid" }}>
                                    <tr style={style.tableRow}>
                                        <th style={{ ...style.tableCell, width: "5%" }}>{"STT"}</th>
                                        <th style={{ ...style.tableCell, width: "12%" }}>{"Mã quản lý"}</th>
                                        <th style={{ ...style.tableCell, width: "15%" }}>{"Tên tài sản"}</th>
                                        <th style={{ ...style.tableCell, width: "11%" }}>{"Model"}</th>
                                        <th style={{ ...style.tableCell, width: "11%" }}>{"Số serial"}</th>
                                        <th style={{ ...style.tableCell, width: "8%" }}>{"Năm SX"}</th>
                                        <th style={{ ...style.tableCell, width: "5%" }}>{"SL"}</th>
                                        <th style={{ ...style.tableCell, width: "6%" }}>{"ĐVT"}</th>
                                        <th style={{ ...style.tableCell, width: "10%" }}>{"Nguyên giá"}</th>
                                        <th style={{ ...style.tableCell, width: "10%" }}>{"GTCL"}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        dataPhieu?.assetVouchers?.map((item, index) => {
                                            return (
                                                <tr style={style.tableRow} key={index}>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{index + 1}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.assetCode ?? ""}</td>
                                                    <td style={style.tableCell}>{item?.assetName ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.assetModel ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.assetSerialNumber ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.assetYearOfManufacture ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.quantity ?? "1"}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "center" }}>{item?.assetUnitName ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "right" }}>{convertNumberPrice(item?.assetOriginalCost) ?? ""}</td>
                                                    <td style={{ ...style.tableCell, textAlign: "right" }}>{convertNumberPrice(item?.assetCarryingAmount) ?? ""}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>

                        <div style={style.flexSpaceBetween} className="mt-20">
                            <div>
                                <Signature sign={""} title={"Phòng quản trị"} titleProps={{ fontSize: 12, marginTop: 20 }} />
                            </div>
                            <div>
                                <Signature sign={""} title={"Lãnh đạo đơn vị giao"} titleProps={{ fontSize: 12, marginTop: 20 }} />
                            </div>
                            <div>
                                <Signature sign={""} title={"Bên giao"} titleProps={{ fontSize: 12, marginTop: 20 }} />
                            </div>
                            <div>
                                <Signature sign={""} title={"Lãnh đạo đơn vị nhận"} titleProps={{ fontSize: 12, marginTop: 20 }} />
                            </div>
                            <div>
                                <Signature sign={""} title={"Bên nhận"} titleProps={{ fontSize: 12, marginTop: 20 }} />
                            </div>
                        </div>

                    </div> */}
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-space-between flex-middle">
                        <Button
                            variant="contained"
                            color="secondary"
                            className="mr-36"
                            onClick={handleClose}
                        >
                            {t("general.cancel")}
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            {t("general.print")}
                        </Button>
                    </div>
                </DialogActions>
            </ValidatorForm>
        </Dialog >
    );
}

export const dataViewDCTSTB = dataPhieu => {
    let {currentOrg} = useContext(AppContext);
    const {addressOfEnterprise} = getUserInformation();

    const newDate = new Date(dataPhieu?.issueDate)
    const day = String(newDate?.getDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear());
    const managementDepartment = dataPhieu?.assetVouchers?.[0]?.assetManagementDepartmentName;
    const currentPrefixArray = ["phòng", "p.", "tổ"]
    let convertedManagementDepartment = convertDepartmentNameForSignature(
      managementDepartment,
      currentPrefixArray,
    );
    const acronymManagementDepartment = convertToAcronym(convertedManagementDepartment);

    return {
        dataPhieu: {
            ...
            dataPhieu,
            users: dataPhieu?.users || Array.from({ length: 5 }, (i, x) => ({ ...i, index: x + 1 })),
            assetManagementDepartmentName: dataPhieu?.assetVouchers[0]?.assetManagementDepartmentName || "",
            assetVouchers
                : dataPhieu?.assetVouchers?.map((i, x) => {
                    i.index = x + 1;
                    return {
                        ...i,
                        assetOriginalCost: convertNumberPrice(i?.assetOriginalCost) ?? "",
                        assetCarryingAmount: convertNumberPrice(i?.assetCarryingAmount) ?? "",
                    }
                })
        },
        acronymManagementDepartment,
        addressOfEnterprise,
        organizationName,
        day,
        month,
        year
    }
}