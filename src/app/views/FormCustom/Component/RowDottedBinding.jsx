import React, { useEffect, useRef, useState } from 'react';
import PropTypes from "prop-types";

const styles = {
  container: {
    position: 'relative',
    lineHeight: '25px',
    wordBreak: 'break-word',
    overflow: 'hidden',
  },
  contentDotted: {
    borderBottom: "1px dotted black",
    lineHeight: "24px",
  },
  rowDottedContainer: { 
    position: "absolute",
    top: -4,
    left: 0,
    right: 0,
  }
}

function RowDottedBinding(props) {
  const { content } = props;
  const ref = useRef(null);
  const [arr, setArr] = useState([])

  useEffect(() => {
    renderDotted();
  }, [ref?.current?.scrollHeight]);

  const renderDotted = () => {
    const newArr = [];
    const extraLine = 5;
    const rowNumber = Math.floor(ref.current?.scrollHeight / 25) + extraLine;

    for (let i = 1; i <= rowNumber; i++) {
      newArr.push(<div style={{ ...styles.contentDotted }}>&nbsp;</div>)
    }
    
    setArr([...newArr]);
  }

  return (
    <div style={styles.container}>
      <div ref={ref}>
        {content}
      </div>
      <div style={styles.rowDottedContainer}>
        {arr?.length > 0 ? arr : ""}
      </div>
    </div>
  )
}

RowDottedBinding.propTypes = {
  content: PropTypes.string.isRequired
}

export default RowDottedBinding;