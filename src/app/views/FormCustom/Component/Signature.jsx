import React from "react";
import DateToText from "./DateToText";
import "../FormCustom.scss"

const styles = {
  container: (date, mNone) => {
    return {
      textAlign: "center",
      fontSize: "14px",
      paddingBottom: mNone ? "" : "80px",
      marginTop: date ? "11px" : ""
    }
  },
  fontWeightBold: {
    fontWeight: "bold",
  },
  textUppercase: {
    textTransform: "uppercase",
  },
  textCapitalize: {
    textTransform: "capitalize",
  },
  textItalic: {
    fontStyle: "italic",
  },
  sign: {
    fontStyle: "italic",
    fontSize: "18px",
  },
  title: {
    fontSize: "18px",
  },
  mt_50px: {
    marginTop: "50px",
  },
  titlePropsStyle: (titleProps, date) => {
    return {
      marginTop: titleProps?.marginTop
        ? titleProps?.marginTop
        : date
          ? "10px"
          : "45px",

      ...titleProps,
    }
  },
  signPropsStyle: (signProps) => {
    return {
      ...signProps,
      fontStyle: "italic",
    }
  },
  signTitlePropsStyle: (subTitleProps) => {
    return {
      ...subTitleProps,
    }
  },
  namePropsStyle: (nameProps) => {
    return {
      ...nameProps,
    }
  },
};

function Signature(props) {
  const {
    date,
    title,
    note,
    sign,
    name,
    mNone,
    subTitle,
    location,
    isLocation,
    timeOfInventory,
    dateUpperCase,
    titleProps = {}, // css style cho title
    signProps = {}, // css style cho sign
    nameProps = {}, // css style cho name
    subTitleProps = {}, // css style cho name
  } = props;

  return (
    <div className="signature-container" style={styles.container(date, mNone)}>
      {date && (
        <div className="date-signature">
          <DateToText date={date} location={location} isLocation={isLocation} timeOfInventory={timeOfInventory} noUppercase={dateUpperCase} />
        </div>
      )}
      <div
        className={`signature-title ${date ? "" : "mt-20"}`}
        style={{
          ...styles.textUppercase,
          ...styles.fontWeightBold,
          ...styles.title,
          ...styles.titlePropsStyle(titleProps, date),
        }}
      >
        {title}
      </div>

      {subTitle && <div style={{ ...styles.textItalic, ...styles.signTitlePropsStyle(subTitleProps) }}>({subTitle})</div>}

      {note && <div style={styles.fontWeightBold}>({note})</div>}

      {sign && <div style={{ ...styles.sign, ...styles.signPropsStyle(signProps) }}>({sign})</div>}

      {name && (
        <div
          style={{
            ...styles.fontWeightBold,
            ...styles.mt_50px,
            ...styles.textCapitalize,
            ...styles.namePropsStyle(nameProps)
          }}
        >
          {name}
        </div>
      )}
    </div>
  );
}

export default Signature;
