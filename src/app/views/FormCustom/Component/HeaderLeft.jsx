import React from "react";

const style = {
  title: {
    fontFamily:"'Times New Roman', Times, serif !important",
    fontSize: "0.975rem !important",
    fontWeight: "bold",
    margin: "0px",
    textDecoration: "underline solid 1px",
    textTransform: "uppercase",
    textUnderlineOffset: "5px",
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
  },
  alignItems:(textAlign) => {
    return textAlign ? textAlign : {alignItems: "flex-start"}
  },
  unitPropsStyle: (unitProps) => {
    return {
      ...unitProps,
    }
  },
};
function HeaderLeft({ unit, code, textAlign, unitProps = {}, }) {
  return (
      <div
        style={{
          ...style.flexColumn,
          ...style.alignItems(textAlign),// center/flex-start
        }}
      >
        <div style={{...style.title, ...style.unitPropsStyle(unitProps)}}>{unit}</div>
        {code&&<p style={style.title}>{code ? code : "...................."}</p>}
        {/* <p style={style.title}>Đơn vị: {unit}</p>
        <p style={style.title}>Mã QHNS:{code ? code : "...................."}</p> */}
      </div>
  );
}

export default HeaderLeft;
