import moment from "moment";
import React from "react";
import localStorageService from "app/services/localStorageService";
import PropTypes from "prop-types";

// date,seriaNumbe = "dotted" để hiện ..., truyền date dạng YYYY-MM-DD để hiện ngày
function DateToText(props) {
  const addressOfEnterprise = localStorageService.getSessionItem("addressOfEnterprise");
  const {
    location = addressOfEnterprise,
    date,
    isIssueDate,
    fontSize,
    timeOfInventory,
    isLocation,
    unItalic,
    noUppercase,
    inlineContainer,
    prefixSeperate,
  } = props;
  const dateFormat = moment(date);

  const timeOfInventoryDisplay = timeOfInventory ? timeOfInventory + ":" : ""
  const locationDisplay = location ? location + "," : ""

  const textPreDate = props.textPreDate ? props.textPreDate + (prefixSeperate || ":") : ""

  const formatDate2String = () => {
    const newDate = new Date(dateFormat)
    const day = String(newDate?.getUTCDate())?.padStart(2, '0');
    const month = String(newDate?.getMonth() + 1)?.padStart(2, '0');
    const year = String(newDate?.getFullYear())?.slice(-2);

    return date ? `${day}/${month}/${year}` : "";
  }

  return (
    <div
      style={{
        fontSize: fontSize ? fontSize : "inherit",
        fontStyle: unItalic ? "" : "italic",
        display: !inlineContainer ? "block" : "inline"
      }}
    >
      <span>{location === "dotted" ? "..........," :
        `${timeOfInventoryDisplay} ${isLocation ? locationDisplay : ""}`}{" "}
      </span>
      {(moment(date).isValid() && date)
        ? <span>
          {
            isIssueDate
              ? <b>{textPreDate + formatDate2String(dateFormat)}</b>
              : <>
                {textPreDate}
                {noUppercase ? "ngày" : "Ngày"}&nbsp;{String(new Date(dateFormat)?.getUTCDate())?.padStart(2, '0')}&nbsp;
                tháng&nbsp;{String(new Date(dateFormat)?.getMonth() + 1)?.padStart(2, '0')}&nbsp;
                năm&nbsp;{dateFormat.year()}
              </>
          }
        </span>
        : <span>
          {
            isIssueDate ? <b>{textPreDate}.../&nbsp;.../&nbsp;...</b>
              : <>{textPreDate}{noUppercase ? "ngày" : "Ngày"}........&nbsp;tháng........ &nbsp;năm........</>
          }
        </span>
      }
    </div>
  );
}

DateToText.propTypes = {
  date: PropTypes.node,
  isIssueDate: PropTypes.node,
  textPreDate: PropTypes.string,
  fontSize: PropTypes.node,
  timeOfInventory: PropTypes.node,
  isLocation: PropTypes.node,
  unItalic: PropTypes.node,
  noUppercase: PropTypes.node,
  prefixSeperate: PropTypes.oneOf([":", ",", ";", "-", "."])
}
export default DateToText;
