import React from "react";
const style = {
  title: {
    fontFamily:"'Times New Roman', Times, serif !important",
    fontSize: "0.975rem !important",
    margin: "0px",
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
  },
  alignItems:(textAlign) => {
    return textAlign ? textAlign : {alignItems: "flex-start"}
  },
};
function HeaderRight({ model, infor, textAlign }) {
  return (
    <div>
      <div
        style={{
          ...style.flexColumn,
          ...style.alignItems(textAlign),// center/flex-start
        }}
      >
        <p
          style={{...style.title,fontWeight:"bold"}}
        >
          Mẫu số: {model}
        </p>
        {infor&&<p style={style.title}>{infor}</p>}
      </div>
    </div>
  );
}

export default HeaderRight;
