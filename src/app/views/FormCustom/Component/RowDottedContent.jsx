import React from 'react'
import clsx from "clsx";
import Content from './Content';

const styles = {
  firstLineDotted: {
	height: 20,
  },
  lineDotted: {
	height: 29,
  },
  wrapLineDotted: {
	position: "absolute",
	top: 0,
	left: 0,
	right: 0,
  },
  content: {
	lineHeight: "30px",
	zIndex: 10,
	position: "relative",
  },
  contentRow: {
	position: "relative",
	overflow: "hidden",
  }
}

export const RowDottedContent = (props) => {
  const {title, content, titleProps, contentProps, className} = props;

  return (
	  <div className={clsx(className, "content__box-dotted")}>
		  <div style={{ ...titleProps }}>{title}:</div>
		  <div style={styles.contentRow}>
			  {content?.map((item, index) => <Content item={item} />)}
		  </div>
	  </div>
  )
}
