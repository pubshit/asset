import { position, width } from "dom-helpers";
import React from "react";
const style = {
  title: {
    fontFamily: "'Times New Roman', Times, serif !important",
    fontSize: "0.975rem !important",
    margin: "0px",
    fontWeight: "bold",
    textUnderlineOffset: "5px"
  },
  flex: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",// center/flex-start
  },
  titlePropsStyle: (titleProps) => {
    return {
      ...titleProps,
    }
  },
  textUnderline: {
    width: "100px",
    height: 1,
    background: "#000"
  },
  subTitleStyle: (subTitleStyle) => {
    return {
      textDecoration: subTitleStyle ? "none" : "underline solid 1px",
    }
  },
};
function NationalName({ titleProps = {}, isUnderLine = false }) {
  return (
    <div>
      <div
        style={{ ...style.flex }}
      >
        <div
          style={{ ...style.title, ...style.titlePropsStyle(titleProps) }}
        >
          CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
        </div>
        <div style={{ ...style.title, ...style.subTitleStyle(isUnderLine) }}>Độc lập - Tự do - Hạnh phúc</div>
        {isUnderLine && <div style={{ ...style.textUnderline }}></div>}
      </div>
    </div>
  );
}

export default NationalName;
