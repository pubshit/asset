import React from 'react';
import PropTypes from "prop-types";

const styles = {
  container: {
    display: "grid",
  },
  contentGrid: (titleGrid, contentGrid) => {
    let title = titleGrid || 1;
    let content = contentGrid || 3;
    
    return {
      gridTemplateColumns: `${title}fr ${content}fr`,
    };
  },
}

function PersonPosition(props) {
  const { title, content, titleGrid, contentGrid } = props;

  return (
    <div style={{
      ...styles.container,
      ...styles.contentGrid(titleGrid, contentGrid)
    }}>
      <span>{title}:&nbsp;</span>
      <span>{content}</span>
    </div>
  )
}

PersonPosition.propTypes = {
  content: PropTypes.string.isRequired,
  contentGrid: PropTypes.node,
  title: PropTypes.string.isRequired,
  titleGrid: PropTypes.node,
}

export default PersonPosition