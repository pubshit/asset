import React from "react";
import DateToText from "./DateToText";
import PropTypes from "prop-types";
const style = {
  title: {
   fontWeight: "bold", 
   textTransform: "uppercase", 
   textAlign:"center"
  },
  titlePropsStyle: (titleProps) => {
    return {
      ...titleProps,
    }
  },
};
function Title(props) {
  let { 
    date, 
    title,
    subTitle,
    location, 
    isLocation,
    serialNumber,
    subTitleProps,
    timeOfInventory, 
    titleProps={}  // css style cho title
  } = props;
  return (
    <div style={{ marginTop: "30px" }}>
      <div
        className="form-print-title"
        style={{ ...style.title, ...style.titlePropsStyle(titleProps)}}
      >
        {title}
      </div>
      {date && (
        <div style={{ fontStyle: "italic" }}>
          <DateToText date={date} location={location} isLocation ={isLocation} timeOfInventory={timeOfInventory} />
        </div>
      )}
      {serialNumber && (
        <div style={{ fontStyle: "italic" }}>
          Số:&nbsp;{serialNumber !== "dotted" ? serialNumber : ".........."}
        </div>
      )}
      {subTitle && (
        <div style={{ ...subTitleProps }}>
          {subTitle}
        </div>
      )}
    </div>
  );
}

Title.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  serialNumber: PropTypes.string,
  date: PropTypes.any,
  titleProps: PropTypes.object,
}
export default Title;
