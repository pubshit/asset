import React, { useEffect, useRef, useState } from 'react'

const styles = {
  contentContainer: {
    position: "relative",
  },

  contentDotted: {
    width: '100%',
    position: "absolute",
    top: "-13px",
  },

  renderDotted: (firstLine) => {
    return {
      height: firstLine ? "28px" : "17px",
      width: "100%",
      borderBottom: "1px dotted"
    }
  }
}

export default function Content({ item }) {
  const [heightOfElement, setHeightOfElement] = useState(0)
  const firstLine = 1
  const lineHeight = 28
  const rowNumber = Math.floor(heightOfElement / lineHeight)
  const ref = useRef()

  useEffect(() => {
    setHeightOfElement(ref?.current?.scrollHeight);
  }, [ref?.current?.scrollHeight])

  const renderDotted = () => {
    let arr = []
    for (let i = firstLine; i <= rowNumber; i++) {
      arr.push(<div className='dotted-item' style={styles.renderDotted(i === firstLine)}></div>)
    }
    return arr
  }

  return (
    <div ref={ref} className='content-container' style={styles.contentContainer}>{item}
      <div className='content-dotted' style={styles.contentDotted}>{renderDotted()}</div>
    </div>
  )
}
