import React from "react";
import VerificationCalibrationTable from "./VerificationCalibrationTable";

function VerificationTable(props) {
  return ( 
    <VerificationCalibrationTable {...props} breadcrumb={"Verification.list"} />
   );
}

export default VerificationTable;