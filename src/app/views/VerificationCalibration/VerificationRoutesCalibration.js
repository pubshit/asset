import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const VerificationTable = EgretLoadable({
  loader: () => import("./VerificationTable")
});
const ViewComponent = withTranslation()(VerificationTable);

const VerificationRoutesCalibration = [
  {
    path:  ConstantList.ROOT_PATH + "asset/verification",
    exact: true,
    component: ViewComponent
  }
];

export default VerificationRoutesCalibration;