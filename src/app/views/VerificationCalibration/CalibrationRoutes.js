import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const CalibrationTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./CalibrationTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(CalibrationTable);

const CalibrationRoutes = [
  {
    path: ConstantList.ROOT_PATH + "calibration/list",
    exact: true,
    component: ViewComponent
  }
];

export default CalibrationRoutes;