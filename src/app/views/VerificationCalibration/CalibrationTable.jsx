import React from "react";
import VerificationCalibrationTable from "./VerificationCalibrationTable";

function CalibrationTable(props) {
  return ( 
    <VerificationCalibrationTable {...props} breadcrumb={"VerificationCalibration.calibration"} />
   );
}

export default CalibrationTable;