import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Paper,
  Radio,
  RadioGroup,
} from "@material-ui/core";

import React from "react";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";

import { convertNumberPrice, removeCommas } from "app/appFunction";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import moment from "moment";
import { appConst } from "app/appConst";
import { searchByTextAndOrg } from "../Supplier/SupplierService";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { debounce } from "lodash";
import {
  createVerificationCalibration,
  updateVerificationCalibration,
} from "./VerificationCalibrationService";
import AppContext from "app/appContext";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class AccreditationDialog extends React.Component {
  state = {
    dataAccreditation: {
      kdThoihan: moment().format("YYYY-MM-DDTHH:mm:ss"),
      kdDate: moment().format("YYYY-MM-DDTHH:mm:ss"),
    },
    setShowSelectAssetPopup: false,
    keywordSearch: "",
    listDonVi: [],
    itemDonVi: null,
  };

  query = {
    pageSize: 20,
    pageIndex: 0,
    keyword: this.state.keywordSearch,
    typeCode: "NCC-KD",
  };

  filterAutocomplete = createFilterOptions();

  componentWillMount() {
    let { item } = this.props;
    if (item.id) {
      let itemDonVi = {
        id: item.kdDonvithuchienId,
        name: item?.kdDonvithuchienText,
        text: item?.kdDonvithuchienText,
      };
      this.setState({
        dataAccreditation: item,
        itemDonVi: itemDonVi,
      });
    }
  }

  componentDidMount() {
    this.getListDonVi();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.keywordSearch !== this.state.keywordSearch) {
      this.getListDonVi();
    }
  }

  getListDonVi = async () => {
    let query = {
      pageSize: 20,
      pageIndex: 0,
      keyword: this.state.keywordSearch,
      typeCodes: ["NCC-KD"],
    };
    try {
      let res = await searchByTextAndOrg(query);
      if (res?.data?.content && res?.status === 200) {
        this.setState({
          listDonVi: res?.data?.content,
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  handleFormSubmit = async () => {
    let { t } = this.props;
    let { dataAccreditation } = this.state;
    let { setPageLoading } = this.context;
    setPageLoading(true);
    try {
      if (dataAccreditation?.id) {
        let res = await updateVerificationCalibration(
          this.state.dataAccreditation
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          toast.success(t("VerificationCalibration.noti_update_success"));
          this.props.handleClose();
        } else {
          toast.warning(res?.data?.message);
        }
      } else {
        let res = await createVerificationCalibration(
          this.state.dataAccreditation
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          toast.success(t("VerificationCalibration.noti_create_success"));
          this.props.handleClose();
        } else {
          toast.warning(res?.data?.message);
        }
      }
    } catch (error) {
      setPageLoading(false);
      toast.error(t("toastr.error"));
    }
  };

  handleSelectAsset = async (item = []) => {
    let asset = item[0]?.asset;
    this.setState({
      dataAccreditation: {
        ...this.state.dataAccreditation,
        tsId: asset?.id,
        tsMa: asset?.code,
        tsTen: asset?.name,
        tsMaquanly: asset?.managementCode,
        tsModel: asset?.model,
        tsSerialNo: asset?.serialNumber,
        hangSx: {
          id: asset?.manufacturer?.id,
          name: asset?.manufacturer?.name,
          code: asset?.manufacturer?.code,
        },
        tsHangsx: asset?.manufacturer?.name,
        tsNamsx: asset?.yearOfManufacture,
      },
      setShowSelectAssetPopup: false,
    });
  };
  handleCloseSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: false,
    });
  };
  handleOpenSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: true,
    });
  };
  onChangeInput = (event) => {
    let { name, value } = event.target;
    event.persist();
    if (!value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: "",
        },
      });
      return;
    }
    if (
      (name === "kdChiphi" ||
        name === "kdHinhthuc" ||
        name === "kdKetqua" ||
        name === "kdTrangthai") &&
      !isNaN(+removeCommas(value))
    ) {
      value = removeCommas(value);
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: +value,
        },
      });
    } else if (name !== "kdChiphi") {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: value,
        },
      });
    }
  };

  handleChangeDate = (date, name) => {
    let dataAccreditationUpdate = {
      ...this.state.dataAccreditation,
    };
    if (name === "kdThoihan") {
      if (moment(date).isBefore(moment())) {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.HET_HIEU_LUC.code;
      } else {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.CON_HIEU_LUC.code;
      }
    }
    this.setState({
      dataAccreditation: {
        ...dataAccreditationUpdate,
        [name]: moment(date).format("YYYY-MM-DDTHH:mm:ss"),
      },
    });
  };

  handleSelectDonVi = (value, name) => {
    if (value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: {
          //   id: value?.id,
          //   name: value?.name,
          //   code: value?.code,
          // },
          kdDonvithuchienId: value?.id,
          kdDonvithuchienText: value?.name,
        },
        itemDonVi: value,
      });
    } else {
      this.setState({
        keywordSearch: "",
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: null,
          kdDonvithuchienId: null,
          kdDonvithuchienText: null,
        },
        itemDonVi: null,
      });
    }
  };

  debouncedSetSearchTerm = debounce((newTerm) => {
    this.setState({
      keywordSearch: newTerm,
    });
  }, 300);

  handleSearchDonVi = (e) => {
    this.debouncedSetSearchTerm(e.target.value);
  };

  render() {
    let { open, handleClose, t, isView } = this.props;
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="md"
        fullWidth
        className="accreditationDialog"
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4>{t("Accreditation.title")}</h4>
        </DialogTitle>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          style={{
            paddingBottom: "5px",
            paddingTop: "5px",
            overflow: "auto",
            marginBottom: "50px",
          }}
        >
          <DialogContent style={{ overflow: "unset" }}>
            <Grid
              item
              container
              spacing={3}
              lg={12}
              md={12}
              sm={12}
              xs={12}
              className="mt-10"
            >
              {!isView && (
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  style={{ padding: "0 0 0 12px" }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.handleOpenSelectAssetPopup()}
                    style={{ height: "37px" }}
                  >
                    {t("Accreditation.select")}
                  </Button>
                </Grid>
              )}
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <CustomValidatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog"
                    style={{ marginTop: "2px" }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Accreditation.createdDate")}
                      </span>
                    }
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={this.state.dataAccreditation?.kdDate || new Date()}
                    onChange={(date) => this.handleChangeDate(date, "kdDate")}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    clearable
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                    disabled={isView}
                    maxDate={new Date()}
                    maxDateMessage={t("allocation_asset.maxDateMessage")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsTen || ""}
                  name="tsTen"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">
                      <span style={{ color: "red" }}> * </span>
                      {t("Accreditation.assetName")}
                    </span>
                  }
                  validators={["required"]}
                  errorMessages={t("general.required")}
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsMa || ""}
                  name="tsMa"
                  size="small"
                  label={
                    <span className="font">{t("Accreditation.code")}</span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsMaquanly || ""}
                  name="tsMaquanly"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">
                      {t("Accreditation.managementCode")}
                    </span>
                  }
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsModel || ""}
                  name="tsModel"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">{t("Accreditation.model")}</span>
                  }
                  managementCode
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsSerialNo || ""}
                  name="tsSerialNo"
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  label={
                    <span className="font">{t("Accreditation.seri")}</span>
                  }
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.hangSx?.name || ""}
                  name="hangSx"
                  size="small"
                  label={
                    <span className="font">
                      {t("Accreditation.manufacturer")}
                    </span>
                  }
                  InputProps={{
                    readOnly: true,
                  }}
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  type="text"
                  value={this.state.dataAccreditation?.tsNamsx || ""}
                  name="tsNamsx"
                  label={
                    <span className="font">
                      {t("Accreditation.yearOfManufacture")}
                    </span>
                  }
                  size="small"
                  InputProps={{
                    readOnly: true,
                  }}
                  onChange={() => {}}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <TextValidator
                  className="w-100"
                  variant="standard"
                  type="text"
                  name="kdChiphi"
                  disabled={isView}
                  label={
                    <span className="font">
                      <span style={{ color: "red" }}> * </span>
                      {t("Accreditation.cost")}
                    </span>
                  }
                  value={
                    typeof this.state.dataAccreditation?.kdChiphi !== "number"
                      ? ""
                      : convertNumberPrice(
                          +this.state.dataAccreditation?.kdChiphi || ""
                        )
                  }
                  size="small"
                  onChange={(e) => this.onChangeInput(e)}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              </Grid>
              <Grid item lg={8} md={8} sm={6} xs={12}>
                <FormControl component="fieldset">
                  <FormLabel id="demo-radio-buttons-group-label">
                    {t("Accreditation.typeAccreditation")}
                  </FormLabel>
                  <RadioGroup
                    aria-label="repairForm"
                    name="kdHinhthuc"
                    value={this.state.dataAccreditation?.kdHinhthuc || ""}
                    onChange={(event) => this.onChangeInput(event)}
                  >
                    <Grid container>
                      <Grid item>
                        <FormControlLabel
                          label={appConst.HINH_THUC_KIEM_DINH.LAN_DAU.name}
                          value={+appConst.HINH_THUC_KIEM_DINH.LAN_DAU.code}
                          control={<Radio required disabled={isView} />}
                          id="radio-domestic"
                        />
                      </Grid>
                      <Grid item>
                        <FormControlLabel
                          label={appConst.HINH_THUC_KIEM_DINH.DINH_KY.name}
                          value={+appConst.HINH_THUC_KIEM_DINH.DINH_KY.code}
                          control={<Radio required disabled={isView} />}
                        />
                      </Grid>
                      <Grid item>
                        <FormControlLabel
                          label={appConst.HINH_THUC_KIEM_DINH.DOT_XUAT.name}
                          value={+appConst.HINH_THUC_KIEM_DINH.DOT_XUAT.code}
                          control={<Radio required disabled={isView} />}
                        />
                      </Grid>
                    </Grid>
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <Autocomplete
                  id="combo-box"
                  fullWidth
                  size="small"
                  name="donVi"
                  value={this.state.itemDonVi}
                  disabled={isView}
                  options={[...this.state.listDonVi]}
                  onChange={(e, value) =>
                    this.handleSelectDonVi(value, "donVi")
                  }
                  getOptionLabel={(option) => {
                    return option?.name
                      ? option?.name
                      : this.state.keywordSearch || "";
                  }}
                  filterOptions={(options, params) => {
                    params.inputValue = params.inputValue.trim();
                    return this.filterAutocomplete(options, params);
                  }}
                  noOptionsText={t("general.noOption")}
                  renderInput={(params) => (
                    <TextValidator
                      {...params}
                      variant="standard"
                      onChange={this.handleSearchDonVi}
                      onBlur={() => {
                        this.debouncedSetSearchTerm("");
                      }}
                      disabled={isView}
                      label={
                        <span>
                          <span style={{ color: "red" }}>*</span>
                          {t("Accreditation.testingUnit")}
                        </span>
                      }
                      value={
                        this.state.keywordSearch !== "" && this.state.itemDonVi
                          ? this.state.keywordSearch
                          : this.state.itemDonVi
                      }
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                    />
                  )}
                />
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <FormControl component="fieldset">
                  <FormLabel id="demo-radio-buttons-group-label">
                    {t("Accreditation.result")}
                  </FormLabel>
                  <RadioGroup
                    aria-label="repairForm"
                    name="kdKetqua"
                    value={this.state.dataAccreditation?.kdKetqua || ""}
                    onChange={(event) => this.onChangeInput(event)}
                  >
                    <Grid container>
                      <Grid item>
                        <FormControlLabel
                          label={appConst.KET_QUA_KIEM_DINH.DAT.name}
                          value={+appConst.KET_QUA_KIEM_DINH.DAT.code}
                          control={<Radio required disabled={isView} />}
                          id="radio-domestic"
                        />
                      </Grid>
                      <Grid item>
                        <FormControlLabel
                          label={appConst.KET_QUA_KIEM_DINH.KHONG_DAT.name}
                          value={+appConst.KET_QUA_KIEM_DINH.KHONG_DAT.code}
                          control={<Radio required disabled={isView} />}
                        />
                      </Grid>
                    </Grid>
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <CustomValidatePicker
                    margin="none"
                    fullWidth
                    id="date-picker-dialog"
                    style={{ marginTop: "2px" }}
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("Accreditation.duration")}
                      </span>
                    }
                    disabled={isView}
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    format="dd/MM/yyyy"
                    value={
                      this.state.dataAccreditation?.kdThoihan || new Date()
                    }
                    onChange={(date) =>
                      this.handleChangeDate(date, "kdThoihan")
                    }
                    validators={["required"]}
                    errorMessages={t("general.required")}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage={t("general.invalidDateFormat")}
                    clearable
                    clearLabel={t("general.remove")}
                    cancelLabel={t("general.cancel")}
                    okLabel={t("general.select")}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <FormControl component="fieldset">
                  <FormLabel id="demo-radio-buttons-group-label">
                    {t("Accreditation.status")}
                  </FormLabel>
                  <RadioGroup
                    aria-label="repairForm"
                    name="kdTrangthai"
                    value={this.state.dataAccreditation?.kdTrangthai || ""}
                    onChange={(event) => this.onChangeInput(event)}
                  >
                    <Grid container>
                      <Grid item>
                        <FormControlLabel
                          label={
                            appConst.TRANG_THAI_KIEM_DINH.CON_HIEU_LUC.name
                          }
                          value={
                            +appConst.TRANG_THAI_KIEM_DINH.CON_HIEU_LUC.code
                          }
                          control={<Radio required disabled={isView} />}
                          id="radio-domestic"
                        />
                      </Grid>
                      <Grid item>
                        <FormControlLabel
                          label={
                            appConst.TRANG_THAI_KIEM_DINH.HET_HIEU_LUC.name
                          }
                          value={
                            +appConst.TRANG_THAI_KIEM_DINH.HET_HIEU_LUC.code
                          }
                          control={<Radio required disabled={isView} />}
                        />
                      </Grid>
                    </Grid>
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item lg={4} md={4} sm={6} xs={12}>
                {t("Accreditation.attachments")}
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions
            style={{
              position: "absolute",
              bottom: "-8px",
              right: 0,
              width: "100%",
              backgroundColor: "#fff",
              zIndex: 100,
            }}
          >
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            {!isView && (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            )}
          </DialogActions>
        </ValidatorForm>
        {this.state.setShowSelectAssetPopup && (
          <SelectAssetAllPopup
            t={t}
            open={this.state.setShowSelectAssetPopup}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleCloseSelectAssetPopup}
            isOneSelect={true}
            dataAccreditation={this.state.dataAccreditation}
          />
        )}
      </Dialog>
    );
  }
}

AccreditationDialog.contextType = AppContext;
export default AccreditationDialog;
