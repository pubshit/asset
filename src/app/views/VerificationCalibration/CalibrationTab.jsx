import { AppBar, Box, Grid, Tab, Tabs, Typography } from "@material-ui/core";
import { appConst } from "app/appConst";
import AppContext from "app/appContext";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage } from "../Asset/AssetService";
import ComponentTableTab from "./ComponentTableTab";
toast.configure({
    autoClose: 2000,
    draggable: false,
    limit: 3,
    //etc you get the idea
});
function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <React.Fragment>
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`scrollable-force-tabpanel-${index}`}
                aria-labelledby={`scrollable-force-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        </React.Fragment>
    );
}
function CalibrationTab(props) {
    const { t } = useTranslation();
    const { setPageLoading } = useContext(AppContext);
    // const [page, setPage] = useState(1)
    // const [totalElements, setTotalElements] = useState(0)
    // const [rowsPerPage, setRowsPerPage] = useState(10)
    // const [keyword, setKeyword] = useState("")
    const [tabValue, setTabValue] = useState(0)
    const [searchObject, setSearchObject] = useState({
        page: 1,
        rowsPerPage: 10,
        totalElements: 0,
        keyword: ""
    })
    const handleChangePage = (event, newPage) => {
        setSearchObject({ ...searchObject, page: newPage });
    };
    const [itemList, setItemList] = useState([])
    const updatePageData = async () => {

        setPageLoading(true);
        let dataSearch = {};
        dataSearch.keyword = searchObject.keyword.trim() || null;
        dataSearch.pageIndex = searchObject.page + 1;
        dataSearch.pageSize = searchObject.rowsPerPage;
        try {
            const result = searchByPage(dataSearch)
            if (result?.status === appConst.CODE.SUCCESS) {
                setItemList([...result?.data?.data?.content])
                setSearchObject({ ...searchObject, totalElements: result?.data?.data?.totalElements })
            }

        } catch (error) {
            toast.error(t("toastr.error"));
        }
        finally {
            setPageLoading(false);
        };

    };
    const handleChangeTabValue = (event, newValue) => {
        setSearchObject({
            ...searchObject,
            page: 0,
        });

        if (appConst?.tabVerificationCalibration?.effective === newValue) {
            setSearchObject(
                {
                    ...searchObject,
                    keyword: "",
                }
            );
            setItemList([])
            setTabValue(newValue)
            updatePageData()
        }
        if (appConst?.tabVerificationCalibration?.ineffective === newValue) {
            setSearchObject(
                {
                    ...searchObject,
                    keyword: "",
                }
            );
            setItemList([])
            setTabValue(newValue)
            updatePageData()
        }
        if (appConst?.tabVerificationCalibration?.all === newValue) {
            setSearchObject(
                {
                    ...searchObject,
                    keyword: "",
                }
            );
            setItemList([])
            setTabValue(newValue)
            updatePageData()
        }
    };

    return (<div>
        <Grid container spacing={2} className="mt-12">
            <Grid item xs={12}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={tabValue}
                        onChange={(e, value) => handleChangeTabValue(e, value)}
                        variant="scrollable"
                        scrollButtons="on"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="scrollable force tabs example"
                    >
                        [
                        <Tab key={appConst.tabVerificationCalibration.effective} label={t("VerificationCalibration.effective")} />,
                        <Tab key={appConst.tabVerificationCalibration.ineffective} label={t("VerificationCalibration.ineffective")} />,
                        <Tab key={appConst.tabVerificationCalibration.all} label={t("VerificationCalibration.all")} />, ]
                    </Tabs>
                </AppBar>
                <TabPanel
                    value={tabValue}
                    index={appConst.tabVerificationCalibration.effective}
                    className="mp-0"
                >
                    <ComponentTableTab />
                </TabPanel>
                <TabPanel
                    value={tabValue}
                    index={appConst.tabVerificationCalibration.ineffective}
                    className="mp-0"
                >
                    <ComponentTableTab />
                </TabPanel>
                <TabPanel
                    value={tabValue}
                    index={appConst.tabVerificationCalibration.all}
                    className="mp-0"
                >
                    <ComponentTableTab />
                </TabPanel>
            </Grid>
            <Grid item xs={12}>
            </Grid>
        </Grid>
    </div>);
}

export default CalibrationTab;