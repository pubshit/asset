import React from "react";
import AppContext from "app/appContext";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";
import { AppBar, Box, Button, FormControl, Grid, Input, InputAdornment, Link, Tab, TablePagination, Tabs, Typography } from "@material-ui/core";
import MaterialTable, { MTableToolbar } from "material-table";
import { appConst } from "app/appConst";
import VerificationTab from "./VerificationTab";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}

class VerificationCalibrationTable extends React.Component {
  
  handleEditItem = () => {
    this.setState({
      shouldOpenMainProposal: true,
    });
  };

 
  render() {
    const { t, i18n, breadcrumb } = this.props;
    return (
      <div className="m-sm-30">
        <Helmet>
          <title>
          {t(breadcrumb)} | {t("web_site")}
          </title>
        </Helmet>
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              {
                name: t("Dashboard.assetManagement"),
                path: "fixed-assets/page",
              },
              { name: t(breadcrumb) },
            ]}
          />
        </div>
          <VerificationTab />
      </div >
    );
  }
}

VerificationCalibrationTable.contextType = AppContext;
export default VerificationCalibrationTable;
