import DateFnsUtils from "@date-io/date-fns";
import { Box, Button, Card, CardContent, FormControl, Grid, Icon, IconButton, Input, InputAdornment, Link, TablePagination, TextField, Typography } from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SearchIcon from "@material-ui/icons/Search";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { appConst, formatDate } from "app/appConst";
import AppContext from "app/appContext";
import { LightTooltip } from "app/appFunction";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { searchByPage as searchByPageDepartment } from "../Department/DepartmentService";
import { searchByTextNew } from "../Supplier/SupplierService";
import AsynchronousAutocomplete from "../utilities/AsynchronousAutocomplete";
import VerificationCalibrationDialog from "./VerificationCalibrationDialog";
import { deleteItemNghiemThuKiemDinhById, searchByPage as searchByPageNghiemThu } from "./VerificationCalibrationService";
import { ConfirmationDialog } from "egret";
toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
  //etc you get the idea
});
function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </React.Fragment>
  );
}
function MaterialButton(props) {
  const { t } = useTranslation();
  const item = props.item;

  return (
    <div className="none_wrap">
      {
        item?.trangThai !== appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.code &&
        (
          <LightTooltip
            title={t("general.editIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.edit)}
            >
              <Icon fontSize="small" color="primary">
                edit
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {item?.trangThai !== appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.code &&
        (
          <LightTooltip
            title={t("general.deleteIcon")}
            placement="right-end"
            enterDelay={300}
            leaveDelay={200}
            PopperProps={{
              popperOptions: {
                modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
              },
            }}
          >
            <IconButton
              size="small"
              onClick={() => props.onSelect(item, appConst.active.delete)}
            >
              <Icon fontSize="small" color="error">
                delete
              </Icon>
            </IconButton>
          </LightTooltip>
        )}
      {false && (
        <LightTooltip
          title={t("In phiếu")}
          placement="right-end"
          enterDelay={300}
          leaveDelay={200}
          PopperProps={{
            popperOptions: {
              modifiers: { offset: { enabled: true, offset: "10px, 0px" } },
            },
          }}
        >
          <IconButton
            size="small"
            onClick={() => props.onSelect(item, appConst.active.print)}
          >
            <Icon fontSize="small" color="inherit">
              print
            </Icon>
          </IconButton>
        </LightTooltip>
      )}
      <LightTooltip
        title={t("general.viewIcon")}
        placement="right-end"
        enterDelay={300}
        leaveDelay={200}
      >
        <IconButton
          size="small"
          onClick={() => props.onSelect(item, appConst.active.view)}
        >
          <Icon fontSize="small" color="primary">
            visibility
          </Icon>
        </IconButton>
      </LightTooltip>
    </div>
  );
}
function AcceptantVerificationTab(props) {
  const { tabValue, status = null } = props
  const { t } = useTranslation();
  const { setPageLoading } = useContext(AppContext);
  const [searchObject, setSearchObject] = useState({
    page: 0,
    rowsPerPage: 10,
    totalElements: 0,
    keyword: ""
  })
  const [itemList, setItemList] = useState([])
  const [idDelete, setIdDelete] = useState(false)
  const [isView, setIsView] = useState(false)
  const [isSearch, setIsSearch] = useState(false)
  const [stateAdvance, setStateAdvance] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [shouldOpenAccreditationDialog, setShouldOpenAccreditationDialog] = useState(false)
  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] = useState(false)
  let pageSearchDefault = 1
  let supplierUnitSearchObject = {
    pageIndex: 1,
    pageSize: 1000000,
    typeCodes: [appConst.TYPE_CODES.NCC_KD]
  };
  let filterAutocomplete = createFilterOptions()

  const handleChangePage = (event, newPage) => {
    setSearchObject({ ...searchObject, page: newPage });
  };

  useEffect(() => {
    updatePageData()
  }, [searchObject.page, searchObject.rowsPerPage, stateAdvance, status])

  useEffect(() => {
    !isSearch && setStateAdvance({})
  }, [isSearch])

  const handleOKEditAccreditationClose = () => {
    setShouldOpenAccreditationDialog(false)
    updatePageData()
  };
  const handleEdit = (rowData) => {
    setDataEdit(rowData)
    setShouldOpenAccreditationDialog(true)
  }
  const handleView = (rowData) => {
    setDataEdit(rowData)
    setIsView(true)
    setShouldOpenAccreditationDialog(true)
  }
  const handleAccreditationDialogClose = () => {
    setShouldOpenAccreditationDialog(false)
    setIsView(false)
  };

  const handleTextChange = (event) => {
    setSearchObject({
      ...searchObject,
      keyword: event.target.value,
    })
  };
  const search = async () => {
    updatePageData(pageSearchDefault)
  }

  const handleDelete = async () => {
    try {
      const result = await deleteItemNghiemThuKiemDinhById(idDelete)
      if (result?.data?.code === appConst.CODE.SUCCESS) {
        updatePageData()
        setShouldOpenConfirmationDialog(false)
        toast.success(t("general.deleteSuccess"));
      }
      else {
        toast.warning(result?.data?.data[0]?.errorMessage);
        setShouldOpenConfirmationDialog(false)
      }
    } catch (error) {
      toast.error(t("toastr.error"));
      setShouldOpenConfirmationDialog(false)
    }
    finally {
      setPageLoading(false);
    };
  }
  const handleSetDataSelect = (data, source) => {
    setStateAdvance({
      ...stateAdvance, [source]: data,
    });
  };
  const handleKeyDownEnterSearch = (e) => {
    if (e.key === "Enter") {
      search();
    }
  };
  const handleKeyUp = (e) => {
    !e.target.value && search();
  };
  const updatePageData = async (pageSearchDefault) => {
    setPageLoading(true);
    let dataSearch = {};
    dataSearch.keyword = searchObject.keyword.trim() || "";
    dataSearch.pageIndex = (pageSearchDefault ? pageSearchDefault : null) || searchObject.page + 1;
    dataSearch.pageSize = searchObject.rowsPerPage;
    dataSearch.dvThucHienId = stateAdvance?.kdDvThucHien?.id;
    dataSearch.tsPhongSdId = stateAdvance?.useDepartment?.id;
    dataSearch.kdHinhThuc = stateAdvance?.kdHinhThuc?.code;
    dataSearch.ngayKyBienBanDen = stateAdvance?.ngayKyBienBanDen ? (moment(stateAdvance?.ngayKyBienBanDen).format("YYYY-MM-DDT23:59:59")) : null;
    dataSearch.ngayKyBienBanTu = stateAdvance?.ngayKyBienBanTu ? (moment(stateAdvance?.ngayKyBienBanTu).format("YYYY-MM-DDT00:00:00")) : null;
    dataSearch.type = appConst.TYPE_KD_HC.KIEM_DINH.code;

    try {
      const result = await searchByPageNghiemThu(dataSearch)
      if (result?.status === appConst.CODE.SUCCESS) {
        setItemList([...result?.data?.data?.content])
        setSearchObject({ ...searchObject, totalElements: result?.data?.data?.totalElements })
      }

    } catch (error) {
      toast.error(t("toastr.error"));
    }
    finally {
      setPageLoading(false);
    };

  };

  let columns = [
    {
      title: t("Asset.action"),
      field: "custom",
      align: "left",
      maxWidth: 120,
      minWidth: 120,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) =>
      (
        <MaterialButton
          item={rowData}
          onSelect={(rowData, method) => {
            if (method === appConst.active.edit) {
              handleEdit(rowData);
            } else if (method === appConst.active.delete) {
              setIdDelete(rowData?.id)
              setShouldOpenConfirmationDialog(true);
            } else if (method === appConst.active.view) {
              //view
              //view
              handleView(rowData);
            } else if (method === appConst.active.print) {
              handlePrint(rowData);
            } else {
              alert("Call Selected Here:" + rowData.id);
            }
          }}
        />
      ),
    },
    {
      title: t("maintainRequest.columns.codeForm"),
      field: "ma",
      minWidth: 100,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
    },
    {
      title: t("MaintainPlaning.issueDate"),
      field: "ngayKyBienBan",
      minWidth: 110,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "center",
      },
      render: (rowData) => rowData?.ngayKyBienBan ? moment(rowData?.ngayKyBienBan).format("DD/MM/YYYY") : ""
    },
    {
      title: t("Asset.status"),
      field: "trangThai",
      minWidth: 110,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "left",
      },
      render: (rowData) => rowData?.trangThai === appConst.STATUS_NGHIEM_THU_KD_HC.DANG_XU_LY.code ? appConst.STATUS_NGHIEM_THU_KD_HC.DANG_XU_LY.name : appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.name
    },
    {
      title: t("maintainRequest.name"),
      field: "ten",
      maxWidth: 250,
      maxWidth: 250,
      align: "left",
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: "left",
      },
    },
    {
      title: t("Asset.performingUnit"),
      field: "dvThucHienText",
      align: "left",
      minWidth: 200,
      cellStyle: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },

  ];

  return (<div>
    <Grid container spacing={2} className="mt-12">
      <Grid item md={6} sm={12} xs={12}>
        <Button
          className="align-bottom mr-16 mb-16"
          variant="contained"
          color="primary"
          style={{ paddingRight: "3px" }}
          onClick={() => setIsSearch(!isSearch)}
        >
          {t("Tìm kiếm nâng cao")}
          <ArrowDropDownIcon />
        </Button>
      </Grid>
      <Grid item md={6} sm={12} xs={12}>
        <FormControl fullWidth>
          <Input
            className="search_box w-100"
            onChange={handleTextChange}
            onKeyDown={handleKeyDownEnterSearch}
            onKeyUp={handleKeyUp}
            placeholder={t("VerificationCalibration.search_acceptant")}
            id="search_box"
            startAdornment={
              <InputAdornment>
                <Link>
                  <SearchIcon
                    onClick={() => search()}
                    className="searchTable"
                  />
                </Link>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      {isSearch && (
        <Card className="mt-10 mb-20 w-100 mx-10">
          <ValidatorForm>
            <CardContent>
              <Grid
                container
                xs={12}
                spacing={2}
                className="mx-0"
              >
                <Grid item md={3} xs={12}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("MaintainPlaning.dxFrom")}
                      format="dd/MM/yyyy"
                      value={stateAdvance?.ngayKyBienBanTu ?? null}
                      onChange={(data) => handleSetDataSelect(
                        data,
                        "ngayKyBienBanTu"
                      )}
                      KeyboardButtonProps={{ "aria-label": "change date", }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item md={3} xs={12}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="none"
                      fullWidth
                      autoOk
                      id="date-picker-dialog"
                      label={t("MaintainPlaning.dxTo")}
                      format="dd/MM/yyyy"
                      value={stateAdvance?.ngayKyBienBanDen ?? null}
                      onChange={(data) => handleSetDataSelect(
                        data,
                        "ngayKyBienBanDen"
                      )}
                      KeyboardButtonProps={{ "aria-label": "change date", }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>

                <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    label={t("Asset.CalibrationTestingUnit")}
                    searchFunction={searchByTextNew}
                    searchObject={supplierUnitSearchObject}
                    displayLable={"name"}
                    showCode={"showCode"}
                    value={stateAdvance?.kdDvThucHien}
                    onSelect={(value) => setStateAdvance(
                      { ...stateAdvance, kdDvThucHien: value }
                    )}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    noOptionsText={t("general.noOption")}
                  />
                </Grid>
                <Grid item md={3} xs={12}>
                  <Autocomplete
                    options={appConst.hinhThucKiemDinhOptions}
                    fullWidth
                    value={stateAdvance?.kdHinhThuc || null}
                    getOptionLabel={option => option.name}
                    onChange={(e, value) => setStateAdvance(
                      { ...stateAdvance, kdHinhThuc: value })}
                    filterOptions={(options, params) => {
                      params.inputValue = params.inputValue.trim()
                      let filtered = filterAutocomplete(options, params)
                      return filtered
                    }}
                    renderInput={params => <TextField
                      {...params}
                      label={t("AssetType.type")}
                      value={stateAdvance?.kdHinhThuc || null}
                    />}
                  />
                </Grid>
                {/* <Grid item md={3} xs={12}>
                  <AsynchronousAutocomplete
                    searchFunction={searchByPageDepartment}
                    searchObject={{
                      pageIndex: 1,
                      pageSize: 1000000,
                    }}
                    label={t("MaintainPlaning.useDepartment")}
                    displayLable="text"
                    value={stateAdvance?.useDepartment}
                    onSelect={(value) => setStateAdvance(
                      { ...stateAdvance, useDepartment: value }
                    )}
                  />
                </Grid> */}
                {/* <Grid item md={3} xs={12}>

                </Grid> */}
              </Grid>
            </CardContent>
          </ValidatorForm>
        </Card>
      )}

      <Grid item xs={12}>
        <MaterialTable
          // onRowClick={(e, rowData) => {
          //     getRowData(rowData)
          // }}
          // parentChildData={(row, rows) => {
          //     return rows.find((a) => a.id === row.parentId);
          // }}
          title={t("general.list")}
          data={itemList}
          columns={columns}
          localization={{
            body: {
              emptyDataSourceMessage: `${t(
                "general.emptyDataMessageTable"
              )}`,
            },
          }}
          options={{
            selection: false,
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            sorting: false,
            rowStyle: (rowData) => ({
              backgroundColor:
                rowData.tableData.id % 2 == 1 ? "#EEE" : "#FFF",
            }),
            maxBodyHeight: "490px",
            minBodyHeight: "260px",
            headerStyle: {
              backgroundColor: "#358600",
              color: "#fff",
              paddingLeft: 10,
              paddingRight: 10,
              textAlign: "center",
            },
            padding: "dense",
            toolbar: false,
          }}
          components={{
            Toolbar: (props) => <MTableToolbar {...props} />,
          }}
        />
        <TablePagination
          align="left"
          className="px-16"
          rowsPerPageOptions={appConst.rowsPerPageOptions.table}
          labelRowsPerPage={t("general.rows_per_page")}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} ${t("general.of")} ${count !== -1 ? count : `more than ${to}`
            }`
          }
          component="div"
          count={searchObject.totalElements}
          rowsPerPage={searchObject.rowsPerPage}
          page={searchObject.page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={(e) => setSearchObject({ ...searchObject, rowsPerPage: e.target.value })}
        />
      </Grid>
      {shouldOpenAccreditationDialog && (
        <VerificationCalibrationDialog
          t={t}
          handleClose={() => handleAccreditationDialogClose()}
          open={shouldOpenAccreditationDialog}
          handleOKEditClose={handleOKEditAccreditationClose}
          item={dataEdit}
          isView={isView}
        />
      )}
      {shouldOpenConfirmationDialog && (
        <ConfirmationDialog
          title={t("general.confirm")}
          open={shouldOpenConfirmationDialog}
          onConfirmDialogClose={() => setShouldOpenConfirmationDialog(false)}
          onYesClick={handleDelete}
          text={t("general.deleteConfirm")}
          agree={t("general.agree")}
          cancel={t("general.cancel")}
        />
      )}
    </Grid>
  </div>);
}

export default AcceptantVerificationTab;