import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from 'react-i18next';
const VerificationTable = EgretLoadable({
  //loader: () => import("./BsTableExample")
  loader: () => import("./VerificationTable")
  //loader: () => import("./AdazzleTable")
  //loader: () => import("./React15TabulatorSample")
});
const ViewComponent = withTranslation()(VerificationTable);

const VerificationRoutes = [
  {
    path:  ConstantList.ROOT_PATH + "asset/verification",
    exact: true,
    component: ViewComponent
  }
];

export default VerificationRoutes;