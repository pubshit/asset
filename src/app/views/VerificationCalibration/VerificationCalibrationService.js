import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/kd-nghiem-thu";
const API_PATH_LIST_PRODUCT = ConstantList.API_ENPOINT_ASSET_MAINTANE + "/api/kd-tai-san";

export const createVerificationCalibration = (data) => {
  return axios.post(API_PATH, data);
};
export const updateVerificationCalibration = (data) => {
  let url = API_PATH + `/${data?.id}`;
  return axios.put(url, data);
};

export const searchByPage = (searchObject) => {
  let url = API_PATH + `/page`;
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      ...searchObject
    },
  };
  return axios.get(url, config);
};

export const searchProductByPage = (searchObject) => {
  let url = API_PATH_LIST_PRODUCT + `/page`;
  let config = {
    params: {
      pageIndex: searchObject?.pageIndex,
      pageSize: searchObject?.pageSize,
      keyword: searchObject?.keyword,
      ...searchObject
    },
  };
  return axios.get(url, config);
};

export const getItemById = (id) => {
  let url = API_PATH + "/" + id;
  return axios.get(url);
};

export const deleteItemNghiemThuKiemDinhById = (id) => {
  let url = API_PATH + "/" + id;
  return axios.delete(url);
};

