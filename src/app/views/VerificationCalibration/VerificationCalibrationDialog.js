import {
  AppBar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Icon,
  IconButton,
  Paper,
  Tab,
  Tabs,
} from "@material-ui/core";

import React from "react";
import Draggable from "react-draggable";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SelectAssetAllPopup from "../Component/Asset/SelectAssetAllPopup";

import DateFnsUtils from "@date-io/date-fns";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { appConst, variable } from "app/appConst";
import AppContext from "app/appContext";
import {
  LightTooltip,
  NumberFormatCustom,
  TabPanel,
  convertNumberPrice,
  handleThrowResponseMessage,
  isValidDate,
  removeCommas,
} from "app/appFunction";
import i18next from "i18next";
import { debounce } from "lodash";
import MaterialTable, { MTableToolbar } from "material-table";
import moment from "moment";
import { getNewCodeDocument } from "../Asset/AssetService";
import CustomValidatePicker from "../Component/ValidatePicker/ValidatePicker";
import { searchByTextAndOrg } from "../Supplier/SupplierService";
import AsynchronousAutocompleteSub from "../utilities/AsynchronousAutocompleteSub";
import {
  createVerificationCalibration,
  getItemById,
  updateVerificationCalibration,
} from "./VerificationCalibrationService";
import {
  deleteAttachmentById,
  getAttachmentById,
  getListAttacthmentAcceptant,
} from "../CalibrationAssets/CalibrationAssetsService";
import AssetFilePopup from "../Calibration/AssetFilePopup";
import { ConfirmationDialog } from "egret";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class VerificationCalibrationDialog extends React.Component {
  state = {
    dataAccreditation: {
      ngayKyBienBan: moment().format("YYYY-MM-DDTHH:mm:ss"),
      tongTien: 0,
    },
    setShowSelectAssetPopup: false,
    keywordSearch: "",
    listDonVi: [],
    itemDonVi: null,
    tabValue: 0,
    shouldOpenPopupAssetFile: false,
    itemAssetDocument: null,
    documentType: appConst.documentType.ASSET_DOCUMENT,
    assetDocumentList: null,
    listAssetDocumentId: [],
    listData: [],
  };

  query = {
    pageSize: 20,
    pageIndex: 0,
    keyword: this.state.keywordSearch,
    typeCode: appConst.TYPE_CODES.NCC_KD,
  };

  filterAutocomplete = createFilterOptions();

  componentWillMount() {
    let { item } = this.props;
    if (item?.id) {
      this.getDataEdit();
    } else {
      let itemDonVi = {
        id: item?.dvThucHienId,
        name: item?.dvThucHienText,
        text: item?.dvThucHienText,
      };
      this.setState({
        dataAccreditation: {
          ...item,
          trangThai: appConst.STATUS_NGHIEM_THU_KD_HC.DANG_XU_LY,
          kdTaiSans: item?.kdTaiSans?.map((i) => {
            return {
              ...i,
              kdTrangThai: {
                code:
                  i?.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.CHO_XU_LY?.code
                    ? appConst.listStatusAssetsKDObject.DANG_KIEM_DINH?.code
                    : i?.kdTrangThai,
                indexOrder:
                  i?.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.CHO_XU_LY?.code
                    ? appConst.listStatusAssetsKDObject.DANG_KIEM_DINH?.code
                    : i?.kdTrangThai,
                name:
                  i?.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.CHO_XU_LY?.code
                    ? appConst.listStatusAssetsKDObject.DANG_KIEM_DINH?.name
                    : i?.kdTrangThaiText,
              },
              kdKetQua: {
                code: i?.kdKetQua,
                indexOrder: i?.kdKetQua,
                name: i?.kdKetQuaText,
              },
            };
          }),
          tongTien: item?.kdTaiSans?.reduce(
            (acc, cur) => acc + Number(cur.kdChiPhi),
            0
          ),
        },
        itemDonVi: itemDonVi,
      });
    }
  }

  componentDidMount() {
    this.getListDonVi();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.keywordSearch !== this.state.keywordSearch) {
      this.getListDonVi();
    }
  }

  getDataEdit = async () => {
    let { id } = this.props.item;

    try {
      let res = await getItemById(id);
      if (res?.data?.data && res?.status === appConst.CODE.SUCCESS) {
        let item = res?.data?.data;
        let itemDonVi = {
          id: item?.dvThucHienId,
          name: item?.dvThucHienText,
          text: item?.dvThucHienText,
        };
        this.setState({
          dataAccreditation: {
            ...item,
            trangThai: appConst.listStatusverification.find(
              (i) => i.indexOrder === item?.trangThai
            ),
            kdTaiSans: res?.data?.data?.kdTaiSans?.map((i) => {
              return {
                ...i,
                kdTrangThai: {
                  code: i?.kdTrangThai,
                  indexOrder: i?.kdTrangThai,
                  name: i?.kdTrangThaiText,
                },
                kdKetQua: {
                  code: i?.kdKetQua,
                  indexOrder: i?.kdKetQua,
                  name: i?.kdKetQuaText,
                },
                kdLyDoKhongDat: i?.kdLyDoKhongDat,
                kdSoTem: i?.kdSoTem,
              };
            }),
          },
          itemDonVi: itemDonVi,
        });
      } else {
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  getListDonVi = async () => {
    let query = {
      pageSize: 20,
      pageIndex: 0,
      keyword: this.state.keywordSearch,
      typeCodes: ["NCC-KD"],
    };
    try {
      let res = await searchByTextAndOrg(query);
      if (res?.data?.content && res?.status === 200) {
        this.setState({
          listDonVi: res?.data?.content,
        });
      }
    } catch (error) {
      toast.error("Xảy ra lỗi");
    }
  };

  removeEmptyProperties(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] === null || obj[key] === undefined || obj[key] === "") {
          delete obj[key];
        }
      }
    }
  }

  convertDto = (state) => {
    const cleanItem = (i) => {
      const cleanedItem = {
        dxId: i?.dxId,
        id: i?.id,
        kdChiPhi: i?.kdChiPhi,
        kdDvThucHienId: i?.kdDvThucHien?.id || i?.kdDvThucHienId,
        kdDvThucHienText: i?.kdDvThucHien?.name || i?.kdDvThucHienText,
        kdKetQua: i?.kdKetQua?.indexOrder,
        kdKetQuaText: i?.kdKetQuaText,
        kdNgayBatDau: i?.kdNgayBatDau,
        kdNgayKyHopDong: i?.kdNgayKyHopDong,
        kdNgayNghiemThu: i?.kdNgayNghiemThu,
        kdNtId: i?.kdNtId,
        kdSoTem: i?.kdSoTem,
        kdThoiHan: i?.kdThoiHan,
        kdTrangThai: i?.kdTrangThai?.indexOrder,
        kdTrangThaiText: i?.kdTrangThaiText,
        khId: i?.khId,
        tsDvtId: i?.tsDvtId,
        tsDvtTen: i?.tsDvtTen,
        tsHangSx: i?.tsHangSx,
        tsId: i?.tsId,
        tsTen: i?.tsTen,
        kdHinhThucText: i?.kdHinhThuc?.name,
        kdHinhThuc: i?.kdHinhThuc?.code?.toString(),
        kdLyDoKhongDat: i?.kdLyDoKhongDat,
        type: i?.type,
      };
      this.removeEmptyProperties(cleanedItem);
      return cleanedItem;
    };

    return {
      ...state,
      kdTaiSans: state?.kdTaiSans?.map(cleanItem),
      nam: state?.nam && Number(state?.nam),
      trangThai: state?.trangThai?.code,
      type: appConst.TYPE_KD_HC.KIEM_DINH.code,
      ngayKyBienBan:
        this.state?.ngayKyBienBan || moment().format("YYYY-MM-DDTHH:mm:ss"),
    };
  };

  checkValidAsset = (assets = []) => {
    let { t } = this.props;
    return assets.some((item) => {
      let { kdNgayKyHopDong, kdNgayBatDau, kdNgayNghiemThu, kdThoiHan, tsMa } = item;

      const fields = [
        { text: "Ngày ký hợp đồng", name: "kdNgayKyHopDong", value: kdNgayKyHopDong },
        { text: "Ngày bắt đầu", name: "kdNgayBatDau", value: kdNgayBatDau },
        { text: "Ngày nghiệm thu", name: "kdNgayNghiemThu", value: kdNgayNghiemThu },
        { text: "Ngày hết hạn", name: "kdThoiHan", value: kdThoiHan },
      ];


      return fields.some(field => {
        if (field?.value && !isValidDate(field?.value)) {
          toast.warning(t(`${field.text} của ${tsMa} không hợp lệ`));
          return true;
        }
        return false;
      });
    });
  };

  validateSubmit = () => {
    let { t } = this.props;
    let { dataAccreditation } = this.state;
    let nam = Number(dataAccreditation?.nam);
    let thang = Number(dataAccreditation?.thang);
    if (!dataAccreditation?.ten) {
      toast.warning(t("Không được để trống tên phiếu"));
      return true;
    }
    if (!dataAccreditation?.trangThai?.code) {
      toast.warning(t("Không được để trống trạng thái phiếu"));
      return true;
    }
    if (dataAccreditation?.kdTaiSans?.length <= 0) {
      toast.warning(t("VerificationCalibration.noAsset"));
      return true;
    }
    
    if (this.checkValidAsset(dataAccreditation.kdTaiSans)) {
      return true;
    }

    if (nam && (nam > new Date().getFullYear() || nam < 1900)) {
      toast.warning(t("Năm không hợp lệ"));
      return true;
    }
    if (thang && nam) {
      if (
        nam === new Date().getFullYear() &&
        thang > new Date().getMonth() + 1
      ) {
        toast.warning(t("Tháng không hợp lệ"));
        return true;
      }
    }
    if (thang && (thang > 12 || thang < 1)) {
      toast.warning(t("Tháng không hợp lệ"));
      return true;
    }
    return false;
  };


  handleFormSubmit = async () => {
    let { t } = this.props;
    let { dataAccreditation } = this.state;
    let { setPageLoading } = this.context;
    if(this.validateSubmit()) return;
    setPageLoading(true);
    try {
      if (dataAccreditation?.id) {
        let res = await updateVerificationCalibration(
          this.convertDto(dataAccreditation)
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          this.props.handleOKEditClose();
          toast.success(t("VerificationCalibration.noti_update_success"));
        } else {
          // res?.data?.data[0]?.errorMessage
          //   ? toast.warning(res?.data?.data[0]?.errorMessage)
          //   : toast.error(t("toastr.error"));
          handleThrowResponseMessage(res);
        }
      } else {
        let res = await createVerificationCalibration(
          this.convertDto(dataAccreditation)
        );
        setPageLoading(false);
        if (
          res?.data &&
          res?.status === appConst.CODE.SUCCESS &&
          res?.data?.code === appConst.CODE.SUCCESS
        ) {
          this.props.handleOKEditClose();
          toast.success(t("VerificationCalibration.noti_create_success"));
        } else {
          // res?.data?.data[0]?.errorMessage
          //   ? toast.warning(res?.data?.data[0]?.errorMessage)
          //   : toast.error(t("toastr.error"));
          handleThrowResponseMessage(res);
        }
      }
    } catch (error) {
      setPageLoading(false);
      toast.error(t("toastr.error"));
    }
  };

  handleSelectAsset = async (item = []) => {
    this.setState({
      dataAccreditation: {
        ...this.state.dataAccreditation,
        kdTaiSans: item?.map((i) => ({
          ...i.asset,
          kdAttachments: [],
          asset: i?.asset,
        })),
      },
      setShowSelectAssetPopup: false,
    });
  };
  handleCloseSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: false,
    });
  };
  handleOpenSelectAssetPopup = () => {
    this.setState({
      setShowSelectAssetPopup: true,
    });
  };
  onChangeInput = (event) => {
    let { name, value } = event.target;
    event.persist();
    if (!value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: "",
        },
      });
      return;
    }
    if (
      (name === "tongTien" || name === "kdKetQua") &&
      !isNaN(+removeCommas(value))
    ) {
      value = removeCommas(value);
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: +value,
        },
      });
    } else if (name !== "tongTien") {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: value,
        },
      });
    } else {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          [name]: value,
        },
      });
    }
  };

  handleStatusChange = (trangThai) => {
    this.setState({
      dataAccreditation: {
        ...this.state.dataAccreditation,
        trangThai: trangThai,
        kdTaiSans: this.state?.dataAccreditation?.kdTaiSans?.map((i) => {
          return trangThai?.code ===
            appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.code
            ? {
                ...i,
                kdTrangThai: appConst.listStatusAssetsKDObject.DA_KIEM_DINH,
              }
            : i;
        }),
      },
    });
  };

  handleChangeDate = (date, name) => {
    let dataAccreditationUpdate = {
      ...this.state.dataAccreditation,
    };
    if (name === "kdThoiHan") {
      if (moment(date).isBefore(moment())) {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.HET_HIEU_LUC.code;
      } else {
        dataAccreditationUpdate.kdTrangthai =
          +appConst.TRANG_THAI_KIEM_DINH.CON_HIEU_LUC.code;
      }
    }
    this.setState({
      dataAccreditation: {
        ...dataAccreditationUpdate,
        [name]: moment(date).format("YYYY-MM-DDTHH:mm:ss"),
      },
    });
  };

  handleSelectDonVi = (value, name) => {
    if (value) {
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: {
          //   id: value?.id,
          //   name: value?.name,
          //   code: value?.code,
          // },
          dvThucHienId: value?.id,
          dvThucHienText: value?.name,
        },
        itemDonVi: value,
      });
    } else {
      this.setState({
        keywordSearch: "",
        dataAccreditation: {
          ...this.state.dataAccreditation,
          // [name]: null,
          dvThucHienId: null,
          dvThucHienText: null,
        },
        itemDonVi: null,
      });
    }
  };

  debouncedSetSearchTerm = debounce((newTerm) => {
    this.setState({
      keywordSearch: newTerm,
    });
  }, 300);

  handleSearchDonVi = (e) => {
    this.debouncedSetSearchTerm(e.target.value);
  };

  handleChangeTabValue = (event, value) => {
    this.setState({ tabValue: value });
  };

  handleAssetFilePopupClose = () => {
    this.setState({
      shouldOpenPopupAssetFile: false,
    });
  };

  handleGetListAttachmemt = async () => {
    let searchObject = {};
    searchObject.ntId = this.props?.item?.id;
    if (this.props?.item?.id) {
      try {
        let res = await getListAttacthmentAcceptant(searchObject);
        if (res?.data?.code === appConst.CODE.SUCCESS) {
          this.setState({ assetDocumentList: res?.data?.data?.content });
        }
      } catch (error) {}
    }
  };

  getAssetDocument = (document) => {
    let listAssetDocumentId = this.state?.listAssetDocumentId
      ? this.state?.listAssetDocumentId
      : [];
    let assetDocumentList = this.state?.assetDocumentList
      ? this.state?.assetDocumentList
      : [];
    document && listAssetDocumentId.push(document?.id);
    document && assetDocumentList.push(document);
    this.setState({
      listAssetDocumentId,
      assetDocumentList,
    });
  };
  handleUpdateAssetDocument = (document) => {
    let assetDocumentList = this.state?.assetDocumentList;
    let indexDocument = assetDocumentList.findIndex(
      (item) => item?.id === document?.id
    );

    assetDocumentList[indexDocument] = document;
    this.setState({
      assetDocumentList,
    });
  };

  handleAddAssetDocumentItem = () => {
    getNewCodeDocument(this.state?.documentType)
      .then(({ data }) => {
        if (appConst.CODE.SUCCESS === data?.code) {
          let item = {};
          item.code = data?.data;
          this.setState({
            item: item,
            shouldOpenPopupAssetFile: true,
          });
          return;
        }
        toast.warning(data?.message);
      })
      .catch(() => {
        toast.warning(this.props.t("general.error_reload_page"));
      });
  };

  selectType = (rowData, item) => {
    let { kdTaiSans } = this.state.dataAccreditation;
    if (kdTaiSans?.length > 0) {
      for (const assetVoucher of kdTaiSans) {
        if (assetVoucher?.id === rowData?.id) {
          assetVoucher.kdKetQua = item;
          if (
            item?.indexOrder ===
            appConst.OBJECT_RESULT_KIEM_DINH.DAT?.indexOrder
          ) {
            assetVoucher.kdLyDoKhongDat = "";
          } else if (
            item?.indexOrder ===
            appConst.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.indexOrder
          ) {
            assetVoucher.kdThoiHan = null;
            assetVoucher.kdSoTem = "";
          }
        }
      }
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          kdTaiSans: kdTaiSans,
        },
      });
    }
  };

  selectAssetKd = (rowData, item) => {
    let { kdTaiSans } = this.state.dataAccreditation;
    if (kdTaiSans?.length > 0) {
      for (const assetVoucher of kdTaiSans) {
        if (assetVoucher?.id === rowData?.id) {
          assetVoucher.kdTrangThai = item;
        }
      }
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          kdTaiSans: kdTaiSans,
        },
      });
    }
  };

  removeAssetInlist = (id) => {
    let { kdTaiSans } = this.state.dataAccreditation;
    let index = kdTaiSans.findIndex((x) => x.id === id);
    kdTaiSans.splice(index, 1);
    this.setState({
      dataAccreditation: {
        ...this.state.dataAccreditation,
        kdTaiSans: kdTaiSans,
      },
    });
  };

  selectkdDvThucHien = (rowData, item) => {
    let { kdTaiSans } = this.state.dataAccreditation;
    if (kdTaiSans?.length > 0) {
      for (const assetVoucher of kdTaiSans) {
        if (assetVoucher?.id === rowData?.id) {
          assetVoucher.kdDvThucHien = item;
        }
      }
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          kdTaiSans: kdTaiSans,
        },
      });
    }
  };

  handleChange = (rowData, event) => {
    let { name, value } = event.target;
    let { kdTaiSans } = this.state.dataAccreditation;
    if (kdTaiSans?.length > 0) {
      for (const assetVoucher of kdTaiSans) {
        if (assetVoucher?.id === rowData?.id) {
          if (name === "kdChiPhi") {
            assetVoucher.kdChiPhi = value;
          } else {
            assetVoucher[name] = value;
          }
        }
      }
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          kdTaiSans: kdTaiSans,
          tongTien: this.state.dataAccreditation?.kdTaiSans?.reduce(
            (acc, cur) => acc + Number(cur.kdChiPhi),
            0
          ),
        },
      });
    }
  };

  handleChangeDateRowData = (date, name, rowData) => {
    let { kdTaiSans } = this.state.dataAccreditation;
    if (kdTaiSans?.length > 0) {
      for (const assetVoucher of kdTaiSans) {
        if (assetVoucher?.id === rowData?.id) {
          assetVoucher[name] = moment(date).format("YYYY-MM-DDTHH:mm:ss");
        }
      }
      this.setState({
        dataAccreditation: {
          ...this.state.dataAccreditation,
          kdTaiSans: kdTaiSans,
        },
      });
    }
  };

  handleSetStateData = (value, name) => {
    this.setState(
      {
        dataAccreditation: {
          ...this.state?.dataAccreditation,
          [name]: value,
        },
      },
      () => {
        if (name === "nam") {
          this.setState({
            dataAccreditation: {
              ...this.state?.dataAccreditation,
              thang: "",
            },
          });
        }
      }
    );
  };
  handleRowDataCellEditAssetFile = async (rowData) => {
    let { setPageLoading } = this.context;

    setPageLoading(true);
    try {
      let res = await getAttachmentById(rowData.id);
      if (res?.data?.code === appConst.CODE.SUCCESS) {
        let { data } = res;
        let fileDescriptionIds = [];
        let document = data?.data ? data?.data : null;

        // eslint-disable-next-line no-unused-expressions
        document?.attachments?.map((item) => {
          fileDescriptionIds.push(item?.file?.id);
        });

        document.fileDescriptionIds = fileDescriptionIds;
        document.documentType = this.state?.documentType;

        return this.setState({
          ...document,
          item: document,
          shouldOpenPopupAssetFile: true,
          isEditAssetDocument: true,
        });
      } else {
        // toast.warning(res?.data?.data[0]?.errorMessage);
        handleThrowResponseMessage(res);
      }
    } catch (error) {
      toast.error(this.props.t("toastr.error"));
      setPageLoading(false);
    } finally {
      setPageLoading(false);
    }
    // getAttachmentById(rowData.id).then(({ data }) => {

    // });
  };
  handleRowDataCellDeleteAssetFile = (id) => {
    this.setState({ idAttachmentDelete: id });
  };
  handelDeleteAttachment = async () => {
    let { t } = this.props;
    let { documents, assetDocumentList, idAttachmentDelete } = this.state;
    let index = documents?.findIndex(
      (document) => document?.id === idAttachmentDelete
    );
    let indexId = assetDocumentList?.findIndex(
      (documentId) => documentId === idAttachmentDelete
    );
    documents?.splice(index, 1);
    assetDocumentList?.splice(indexId, 1);
    const res = await deleteAttachmentById(this.state?.idAttachmentDelete);
    try {
      if (res?.data?.code === appConst.CODE.SUCCESS) {
        this.setState({
          assetDocumentList,
          documents,
          idAttachmentDelete: null,
        });
        toast.success(t("general.deleteSuccess"));
      } else {
        toast.error(t("general.deleteSuccess"));
        this.setState({ idAttachmentDelete: null });
      }
    } catch (error) {
      this.setState({ idAttachmentDelete: null });
    }
  };
  formatType = (rowData) => {
    let { t } = this.props;
    switch (rowData?.type) {
      case appConst.TYPE_KD_HC.KIEM_DINH.code:
        return t("VerificationCalibration.verification");
      case appConst.TYPE_KD_HC.KIEM_XA.code:
        return t("VerificationCalibration.gunnery_inspection");
      case appConst.TYPE_KD_HC.KIEM_DINH_KIEM_XA.code:
        return t("VerificationCalibration.verification_gunnery_inspection");
      default:
        break;
    }
  };

  render() {
    let { open, handleClose, t, isView, item } = this.props;

    let { shouldOpenPopupAssetFile, assetDocumentList, dataAccreditation } =
      this.state;
    let columns = [
      {
        title: t("general.stt"),
        field: "code",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      ...(isView
        ? []
        : [
            {
              title: t("general.action"),
              field: "valueText",
              align: "left",
              maxWidth: 120,
              minWidth: 100,
              cellStyle: {
                textAlign: "center",
              },
              render: (rowData) => (
                <div className="none_wrap">
                  <LightTooltip
                    title={t("general.deleteIcon")}
                    placement="top"
                    enterDelay={300}
                    leaveDelay={200}
                  >
                    <IconButton
                      onClick={() => this.removeAssetInlist(rowData?.id)}
                      disabled={this.props?.item?.kdTaiSans?.some(
                        (i) =>
                          i.id === rowData?.id &&
                          i.kdTrangThai ===
                            appConst.listStatusAssetsKDObject.DA_KIEM_DINH
                              .code &&
                          i.kdTrangThai === rowData?.kdTrangThai?.code
                      )}
                    >
                      <Icon
                        fontSize="small"
                        color={
                          this.props?.item?.kdTaiSans?.some(
                            (i) =>
                              i.id === rowData?.id &&
                              i.kdTrangThai ===
                                appConst.listStatusAssetsKDObject.DA_KIEM_DINH
                                  .code &&
                              i.kdTrangThai === rowData?.kdTrangThai?.code
                          )
                            ? "secondery"
                            : "error"
                        }
                      >
                        delete
                      </Icon>
                    </IconButton>
                  </LightTooltip>
                </div>
              ),
            },
          ]),
      {
        title: t("Asset.code"),
        field: "tsMa",
        align: "left",
        minWidth: 120,
        cellStyle: {
          textAlign: "center",
        },
      },
      {
        title: t("Asset.name"),
        field: "tsTen",
        align: "left",
        minWidth: 300,
      },
      {
        title: t("VerificationCalibration.type"),
        field: "type",
        minWidth: 150,
        align: "center",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
        render: (rowData) => this.formatType(rowData),
      },
      {
        title: t("Calibration.status"),
        field: "asset.kdTrangThai",
        minWidth: 180,
        align: "center",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>
              {rowData?.kdTrangThai?.name ? rowData?.kdTrangThai?.name : null}
            </div>
          ) : (
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              value={
                this.state?.dataAccreditation?.trangThai?.code ===
                appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.code
                  ? appConst.listStatusAssetsKDObject.DA_KIEM_DINH
                  : rowData?.kdTrangThai
              }
              options={
                this.state?.dataAccreditation?.trangThai?.code ===
                appConst.STATUS_NGHIEM_THU_KD_HC.DA_XU_LY.code
                  ? [{ code: 3, indexOrder: 3, name: "Đã kiểm định" }]
                  : appConst.listStatusAssetsKD.slice(1, 3)
              }
              onChange={(event, value) => this.selectAssetKd(rowData, value)}
              getOptionLabel={(option) => option.name}
              // renderInput={(params) => <TextField {...params} />}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim();
                return this.filterAutocomplete(options, params);
              }}
              disabled={this.props?.item?.kdTaiSans?.some(
                (i) =>
                  i.id === rowData?.id &&
                  i.kdTrangThai ===
                    appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                  i.kdTrangThai === rowData?.kdTrangThai?.code
              )}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  value={
                    rowData?.kdTrangThai ? rowData?.kdTrangThai?.name : null
                  }
                  inputProps={{
                    ...params.inputProps,
                  }}
                  validators={["required"]}
                  errorMessages={[t("general.required")]}
                />
              )}
              noOptionsText={t("general.noOption")}
            />
          );
        },
      },
      {
        title: t("AssetType.type"),
        field: "kdHinhThucText",
        maxWidth: 150,
        minWidth: 150,
        align: "center",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
          textAlign: "center",
        },
      },
      {
        title: t("VerificationCalibration.cost"),
        field: "kdChiPhi",
        minWidth: 180,
        align: "right",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        // render: (rowData) => {
        //   return isView ? (
        //     <TextValidator
        //       className={"w-100"}
        //       name="kdChiPhi"
        //       value={rowData?.kdChiPhi ? rowData?.kdChiPhi : null}
        //       InputProps={{
        //         readOnly: true,
        //         inputProps: {
        //           style: { textAlign: "right" },
        //         },
        //       }}
        //     />
        //   ) : (
        //     <TextValidator
        //       className={"w-100"}
        //       name="kdChiPhi"
        //       type="text"
        //       value={rowData?.kdChiPhi ? rowData?.kdChiPhi : null}
        //       InputProps={{
        //         inputComponent: NumberFormatCustom,
        //         readOnly: true,
        //         inputProps: {
        //           style: { textAlign: "right" },
        //         },
        //       }}
        //       validators={["minNumber:0"]}
        //       errorMessages={[t("general.minNumberError")]}
        //       onChange={(event) => this.handleChange(rowData, event)}
        //     />
        //   );
        // },
      },
      {
        title: t("Calibration.implementingAgencies"),
        field: "asset.kdDvThucHien",
        minWidth: 250,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return rowData?.kdDvThucHienText ? rowData?.kdDvThucHienText : null;
        },
      },
      // ...(!dataAccreditation?.id
      //   ? []
      //   : [
      {
        title: t("AssetType.result"),
        field: "asset.type",
        minWidth: 180,
        align: "center",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div className="text-center">
              {rowData?.kdKetQuaText ? rowData?.kdKetQuaText : null}
            </div>
          ) : (
            <Autocomplete
              id="combo-box"
              fullWidth
              size="small"
              value={rowData?.kdKetQua}
              options={appConst.LIST_RESULT_KIEM_DINH}
              onChange={(event, value) => this.selectType(rowData, value)}
              getOptionLabel={(option) => option.name}
              // renderInput={(params) => <TextField {...params} />}
              filterOptions={(options, params) => {
                params.inputValue = params.inputValue.trim();
                return this.filterAutocomplete(options, params);
              }}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  variant="standard"
                  value={rowData?.kdKetQua ? rowData?.kdKetQua?.name : null}
                  inputProps={{
                    ...params.inputProps,
                  }}
                  validators={
                    rowData?.kdTrangThai?.code !==
                    appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
                      ? []
                      : ["required"]
                  }
                  errorMessages={[t("general.required")]}
                />
              )}
              disabled={
                rowData?.kdTrangThai?.code !==
                appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
              }
              noOptionsText={t("general.noOption")}
            />
          );
        },
      },
      // ]),
      {
        title: t("VerificationCalibration.regDateContact"),
        field: "kdNgayKyHopDong",
        minWidth: 180,
        align: "left",
        cellStyle: {
          textAlign: "center",
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>
              {rowData?.kdNgayKyHopDong
                ? moment(rowData?.kdNgayKyHopDong).format("DD/MM/YYYY")
                : null}
            </div>
          ) : (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog"
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                value={rowData?.kdNgayKyHopDong}
                onChange={(date) =>
                  this.handleChangeDateRowData(date, "kdNgayKyHopDong", rowData)
                }
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                disabled={isView}
                disableFuture
                maxDate={rowData?.kdNgayBatDau || new Date()}
                maxDateMessage={t("VerificationCalibration.maxDateMessage")}
              />
            </MuiPickersUtilsProvider>
          );
        },
      },
      {
        title: t("VerificationCalibration.startDate"),
        field: "kdNgayBatDau",
        minWidth: 180,
        align: "center",
        cellStyle: {
          textAlign: "center",
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>
              {rowData?.kdNgayBatDau
                ? moment(rowData?.kdNgayBatDau).format("DD/MM/YYYY")
                : null}
            </div>
          ) : (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog"
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                value={rowData?.kdNgayBatDau}
                onChange={(date) =>
                  this.handleChangeDateRowData(date, "kdNgayBatDau", rowData)
                }
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                disabled={isView}
                disableFuture
                minDate={rowData?.kdNgayKyHopDong}
                // minDateMessage={t("VerificationCalibration.minDateMessage")}
                maxDate={new Date()}
                maxDateMessage={t("VerificationCalibration.maxDateMessage")}
              />
            </MuiPickersUtilsProvider>
          );
        },
      },

      // ...(!dataAccreditation?.id
      //   ? []
      //   : [
      {
        title: t("VerificationCalibration.acceptanceDate"),
        field: "kdNgayNghiemThu",
        minWidth: 180,
        align: "left",
        cellStyle: {
          textAlign: "center",
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>
              {rowData?.kdNgayNghiemThu
                ? moment(rowData?.kdNgayNghiemThu).format("DD/MM/YYYY")
                : null}
            </div>
          ) : (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog"
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                value={rowData?.kdNgayNghiemThu}
                onChange={(date) =>
                  this.handleChangeDateRowData(date, "kdNgayNghiemThu", rowData)
                }
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                validators={
                  rowData?.kdTrangThai?.code !==
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
                    ? []
                    : ["required"]
                }
                errorMessages={[t("general.required")]}
                disabled={
                  isView ||
                  rowData?.kdTrangThai?.code !==
                    appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
                }
                disableFuture
                maxDate={new Date()}
                maxDateMessage={t("VerificationCalibration.maxDateMessage")}
              />
            </MuiPickersUtilsProvider>
          );
        },
      },
      //     ]),
      // ...(!dataAccreditation?.id
      //   ? []
      //   : [
      {
        title: t("VerificationCalibration.expirationDate"),
        field: "kdThoiHan",
        minWidth: 180,
        align: "left",
        cellStyle: {
          textAlign: "center",
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>
              {rowData?.kdThoiHan
                ? moment(rowData?.kdThoiHan).format("DD/MM/YYYY")
                : null}
            </div>
          ) : (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <CustomValidatePicker
                margin="none"
                fullWidth
                id="date-picker-dialog"
                inputVariant="standard"
                type="text"
                autoOk={true}
                format="dd/MM/yyyy"
                value={rowData?.kdThoiHan}
                onChange={(date) =>
                  this.handleChangeDateRowData(date, "kdThoiHan", rowData)
                }
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
                invalidDateMessage={t("general.invalidDateFormat")}
                clearable
                clearLabel={t("general.remove")}
                cancelLabel={t("general.cancel")}
                okLabel={t("general.select")}
                minDate={new Date()}
                minDateMessage={t(
                  "VerificationCalibration.minDateExpirationDateMessage"
                )}
                disabled={
                  isView ||
                  rowData?.kdKetQua?.code ===
                    appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code ||
                  rowData?.kdTrangThai?.code !==
                    appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
                }
                validators={
                  rowData?.kdTrangThai?.code !==
                    appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code ||
                  rowData?.kdKetQua?.code ===
                    appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code
                    ? []
                    : ["required"]
                }
                errorMessages={[t("general.required")]}
              />
            </MuiPickersUtilsProvider>
          );
        },
      },
      //     ]),
      // ...(!dataAccreditation?.id
      //   ? []
      //   : [
      {
        title: t("VerificationCalibration.temNum"),
        field: "kdSoTem",
        minWidth: 180,
        align: "left",
        cellStyle: {
          textAlign: "center",
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>{rowData?.kdSoTem}</div>
          ) : (
            <TextValidator
              className={"w-100"}
              name="kdSoTem"
              value={rowData?.kdSoTem ? rowData?.kdSoTem : null}
              InputProps={{
                readOnly:
                  rowData?.kdKetQua?.code ===
                  appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code,
                inputProps: {
                  className: "text-center",
                },
              }}
              disabled={
                rowData?.kdKetQua?.code ===
                  appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code ||
                rowData?.kdTrangThai?.code !==
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
              }
              validators={
                rowData?.kdTrangThai?.code !==
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code ||
                rowData?.kdKetQua?.code ===
                  appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code
                  ? []
                  : ["required"]
              }
              errorMessages={[t("general.required")]}
              onChange={(event) => this.handleChange(rowData, event)}
            />
          );
        },
      },
      //     ]),
      // ...(!dataAccreditation?.id
      //   ? []
      //   : [
      {
        title: t("VerificationCalibration.reason"),
        field: "kdLyDoKhongDat",
        minWidth: 180,
        align: "left",
        cellStyle: {
          paddingLeft: 10,
          paddingRight: 10,
        },
        render: (rowData) => {
          return isView ||
            this.props?.item?.kdTaiSans?.some(
              (i) =>
                i.id === rowData?.id &&
                i.kdTrangThai ===
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code &&
                i.kdTrangThai === rowData?.kdTrangThai?.code
            ) ? (
            <div>{rowData?.kdLyDoKhongDat}</div>
          ) : (
            <TextValidator
              className={"w-100"}
              name="kdLyDoKhongDat"
              value={rowData?.kdLyDoKhongDat}
              InputProps={{
                readOnly:
                  rowData?.kdKetQua?.code ===
                  appConst?.OBJECT_RESULT_KIEM_DINH.DAT?.code,
              }}
              disabled={
                rowData?.kdKetQua?.code ===
                  appConst?.OBJECT_RESULT_KIEM_DINH.DAT?.code ||
                rowData?.kdTrangThai?.code !==
                  appConst.listStatusAssetsKDObject.DA_KIEM_DINH.code
              }
              validators={
                rowData?.kdKetQua?.code ===
                appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code
                  ? ["required"]
                  : []
              }
              errorMessages={
                rowData?.kdKetQua?.code ===
                appConst?.OBJECT_RESULT_KIEM_DINH.KHONG_DAT?.code
                  ? [t("general.required")]
                  : []
              }
              onChange={(event) => this.handleChange(rowData, event)}
            />
          );
        },
      },
      // ]),
    ];

    let columnsAssetFile = [
      {
        title: t("general.stt"),
        field: "code",
        align: "left",
        maxWidth: 50,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => rowData.tableData.id + 1,
      },
      {
        title: t("AssetFile.name"),
        field: "name",
        align: "left",
        minWidth: 300,
      },
      {
        title: t("AssetFile.description"),
        field: "note",
        align: "left",
        minWidth: 350,
      },
      {
        title: t("general.action"),
        field: "valueText",
        align: "left",
        maxWidth: 120,
        minWidth: 100,
        cellStyle: {
          textAlign: "center",
        },
        render: (rowData) => (
          <div className="none_wrap">
            <LightTooltip
              title={t("general.editIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                onClick={() => {
                  this.handleRowDataCellEditAssetFile(rowData);
                }}
              >
                <Icon fontSize="small" color="primary">
                  edit
                </Icon>
              </IconButton>
            </LightTooltip>
            <LightTooltip
              title={t("general.deleteIcon")}
              placement="top"
              enterDelay={300}
              leaveDelay={200}
            >
              <IconButton
                onClick={() => {
                  this.handleRowDataCellDeleteAssetFile(rowData?.id);
                }}
              >
                <Icon fontSize="small" color="error">
                  delete
                </Icon>
              </IconButton>
            </LightTooltip>
          </div>
        ),
      },
    ];

    let validateMonth = [
      "isNumber",
      "minNumber:0",
      this.state.dataAccreditation?.nam < new Date().getFullYear()
      ? `maxNumber:12`
      : `maxNumber:${new Date().getMonth() + 1}`
    ];
    let validateMonthMessage = [
      t("purchasePlaning.is_number"),
      t("general.minNumberError"),
      this.state.dataAccreditation?.nam < new Date().getFullYear()
        ? t("general.maxMonth")
        : t("general.maxMonthNow"),
    ];
    return (
      <Dialog
        open={open}
        PaperComponent={PaperComponent}
        maxWidth="lg"
        fullWidth
        className="accreditationDialog"
      >
        <DialogTitle
          style={{ cursor: "move", paddingBottom: "0px" }}
          id="draggable-dialog-title"
        >
          <h4>{t("Accreditation.title")}</h4>
        </DialogTitle>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleFormSubmit}
          style={{
            paddingBottom: "5px",
            paddingTop: "5px",
            overflow: "auto",
            marginBottom: "50px",
          }}
        >
          <DialogContent style={{ overflow: "unset" }}>
            <AppBar position="static" color="default">
              <Tabs
                value={this.state.tabValue}
                onChange={(e, value) => this.handleChangeTabValue(e, value)}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab key={0} label={t("general.details")} />,
                {item?.id && (
                  <Tab
                    key={1}
                    label={t("Accreditation.attachments")}
                    onClick={this.handleGetListAttachmemt}
                  />
                )}
              </Tabs>
            </AppBar>
            <TabPanel value={this.state.tabValue} index={0}>
              <Grid
                item
                container
                spacing={3}
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="mt-10"
              >
                {this.state.dataAccreditation?.ma && (
                  <Grid item lg={4} md={4} sm={6} xs={12}>
                    <TextValidator
                      fullWidth
                      label={<span>{t("StockSummaryReport.code")}</span>}
                      value={this.state.dataAccreditation?.ma || ""}
                      InputProps={{
                        readOnly: true,
                      }}
                      name="ma"
                    />
                    {/* {t("StockSummaryReport.code")} {this.state.dataAccreditation?.ma} */}
                  </Grid>
                )}
                <Grid item md={4} sm={3} xs={12}>
                  <TextValidator
                    fullWidth
                    label={
                      <span>
                        <span style={{ color: "red" }}>*</span>
                        {t("maintainRequest.name")}
                      </span>
                    }
                    value={this.state.dataAccreditation?.ten || ""}
                    InputProps={{
                      readOnly: isView,
                    }}
                    name="ten"
                    onChange={(e) => this.onChangeInput(e)}
                    validators={["required"]}
                    errorMessages={t("general.required")}
                  />
                </Grid>
                <Grid
                  item
                  md={this.state.dataAccreditation?.ma ? 2 : 4}
                  sm={3}
                  xs={12}
                >
                  <TextValidator
                    fullWidth
                    label={t("MaintainPlaning.year")}
                    value={this.state.dataAccreditation?.nam || ""}
                    InputProps={{
                      readOnly: isView,
                    }}
                    type="number"
                    name="nam"
                    onChange={(e) =>
                      this.handleSetStateData(e?.target?.value, e?.target?.name)
                    }
                    validators={[
                      "isNumber",
                      "minNumber:1900",
                      `maxNumber:${new Date().getFullYear()}`,
                    ]}
                    errorMessages={[
                      t("purchasePlaning.is_number"),
                      t("general.minYearDefault"),
                      t("general.maxYearNow"),
                    ]}
                  />
                </Grid>
                <Grid
                  item
                  md={this.state.dataAccreditation?.ma ? 2 : 4}
                  sm={3}
                  xs={12}
                >
                  <TextValidator
                    fullWidth
                    label={t("MaintainPlaning.month")}
                    value={this.state.dataAccreditation?.thang || ""}
                    InputProps={{
                      readOnly: isView,
                    }}
                    type="number"
                    name="thang"
                    onChange={(e) => this.handleSetStateData(e?.target?.value, e?.target?.name)}
                    validators={validateMonth}
                    errorMessages={validateMonthMessage}
                  />
                </Grid>
                <Grid item lg={4} md={4} sm={6} xs={12}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <CustomValidatePicker
                      margin="none"
                      fullWidth
                      id="date-picker-dialog"
                      style={{ marginTop: "2px" }}
                      label={
                        <span>
                          <span style={{ color: "red" }}>*</span>
                          {t("Accreditation.createdDate")}
                        </span>
                      }
                      inputVariant="standard"
                      type="text"
                      autoOk={true}
                      format="dd/MM/yyyy"
                      value={
                        this.state.dataAccreditation?.ngayKyBienBan ||
                        new Date()
                      }
                      onChange={(date) =>
                        this.handleChangeDate(date, "ngayKyBienBan")
                      }
                      validators={["required"]}
                      errorMessages={t("general.required")}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      invalidDateMessage={t("general.invalidDateFormat")}
                      clearable
                      clearLabel={t("general.remove")}
                      cancelLabel={t("general.cancel")}
                      okLabel={t("general.select")}
                      disabled={isView}
                      maxDate={new Date()}
                      maxDateMessage={t("allocation_asset.maxDateMessage")}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                {/* <Grid item lg={4} md={4} sm={6} xs={12}>
                  <TextValidator
                    className="w-100"
                    variant="standard"
                    type="text"
                    name="ten"
                    disabled={isView}
                    label={
                      <span className="font">
                        {t("Tên phiếu")}
                      </span>
                    }
                    value={this.state.dataAccreditation?.ten}
                    onChange={(e) => this.onChangeInput(e)}
                  />
                </Grid> */}

                <Grid item lg={4} md={4} sm={6} xs={12}>
                  {isView ? (
                    <TextValidator
                      className={"w-100"}
                      label={<span>{t("allocation_asset.voucherStatus")}</span>}
                      value={
                        this.state.dataAccreditation?.trangThai
                          ? this.state.dataAccreditation?.trangThai?.name
                          : ""
                      }
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  ) : (
                    <AsynchronousAutocompleteSub
                      label={
                        <span>
                          <span className="colorRed">* </span>
                          <span> {t("TransferToAnotherUnit.status")}</span>
                        </span>
                      }
                      listData={appConst.listStatusverification}
                      displayLable={"name"}
                      name="trangThai"
                      value={
                        this.state.dataAccreditation?.trangThai
                          ? this.state.dataAccreditation?.trangThai
                          : null
                      }
                      onSelect={this.handleStatusChange}
                      validators={["required"]}
                      errorMessages={[t("general.required")]}
                      // disabled={!this.state.dataAccreditation?.id}
                      filterOptions={(options, params) => {
                        params.inputValue = params.inputValue.trim();
                        let filtered = this.filterAutocomplete(options, params);
                        return filtered;
                      }}
                      noOptionsText={t("general.noOption")}
                    />
                  )}
                </Grid>

                <Grid item lg={4} md={4} sm={6} xs={12}>
                  <TextValidator
                    className="w-100"
                    variant="standard"
                    type="text"
                    name="tongTien"
                    textAlign="right"
                    InputProps={{
                      readOnly: true,
                      inputProps: {
                        style: { textAlign: "right" },
                      },
                    }}
                    label={
                      <span className="font">{t("Accreditation.cost")}</span>
                    }
                    value={
                      typeof this.state.dataAccreditation?.tongTien !== "number"
                        ? ""
                        : convertNumberPrice(
                            +this.state.dataAccreditation?.tongTien || 0
                          )
                    }
                    // onChange={(e) => this.onChangeInput(e)}
                  />
                </Grid>
                <Grid item className="flex-auto">
                  <TextValidator
                    fullWidth
                    className="w-100"
                    variant="standard"
                    type="text"
                    name="ghiChu"
                    textAlign="left"
                    InputProps={{
                      readOnly: isView,
                    }}
                    label={<span className="font">{t("Asset.note")}</span>}
                    defaultValue={this.state.dataAccreditation?.ghiChu}
                    value={this.state.dataAccreditation?.ghiChu || ""}
                    onChange={(e) => this.onChangeInput(e)}
                  />
                </Grid>
                {/* {!isView && (
                  <Grid
                    item
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                    style={{ padding: "0 0 0 12px" }}
                  >
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => this.handleOpenSelectAssetPopup()}
                      style={{ height: "37px" }}
                    >
                      {t("Verification.select")}
                    </Button>
                  </Grid>
                )} */}
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <MaterialTable
                    data={this.state?.dataAccreditation?.kdTaiSans || []}
                    columns={columns}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    options={{
                      draggable: false,
                      toolbar: false,
                      selection: false,
                      actionsColumnIndex: -1,
                      paging: false,
                      search: false,
                      sorting: false,
                      padding: "dense",
                      rowStyle: (rowData) => ({
                        backgroundColor:
                          rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                      }),
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                        paddingLeft: 10,
                        paddingRight: 10,
                        textAlign: "center",
                      },
                      maxBodyHeight: "290px",
                      minBodyHeight: "290px",
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div style={{ width: "100%" }}>
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel value={this.state.tabValue} index={1}>
              <Grid container>
                <Grid item md={12} sm={12} xs={12}>
                  {!isView && (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleAddAssetDocumentItem}
                    >
                      {t("AssetFile.addAssetFile")}
                    </Button>
                  )}
                </Grid>
                <Grid item md={12} sm={12} xs={12} className="mt-16">
                  <MaterialTable
                    data={assetDocumentList || []}
                    columns={columnsAssetFile}
                    localization={{
                      body: {
                        emptyDataSourceMessage: `${t(
                          "general.emptyDataMessageTable"
                        )}`,
                      },
                    }}
                    options={{
                      draggable: false,
                      toolbar: false,
                      selection: false,
                      actionsColumnIndex: -1,
                      paging: false,
                      search: false,
                      sorting: false,
                      padding: "dense",
                      rowStyle: (rowData) => ({
                        backgroundColor:
                          rowData.tableData.id % 2 === 1 ? "var(--primary-light-hover)" : "#FFF",
                      }),
                      headerStyle: {
                        backgroundColor: "#358600",
                        color: "#fff",
                        paddingLeft: 10,
                        paddingRight: 10,
                        textAlign: "center",
                      },
                      maxBodyHeight: "290px",
                      minBodyHeight: "290px",
                    }}
                    components={{
                      Toolbar: (props) => (
                        <div style={{ width: "100%" }}>
                          <MTableToolbar {...props} />
                        </div>
                      ),
                    }}
                    // onSelectionChange={(rows) => {
                    //   this.data = rows;
                    // }}
                  />
                </Grid>
              </Grid>
            </TabPanel>
          </DialogContent>
          <DialogActions
            style={{
              position: "absolute",
              bottom: "-8px",
              right: 0,
              width: "100%",
              backgroundColor: "#fff",
              zIndex: 100,
            }}
          >
            <Button
              className="mb-16 mr-12 align-bottom"
              variant="contained"
              color="secondary"
              onClick={() => handleClose()}
            >
              {t("general.cancel")}
            </Button>
            {!isView && (
              <Button
                className="mb-16 mr-16 align-bottom"
                variant="contained"
                color="primary"
                type="submit"
              >
                {t("general.save")}
              </Button>
            )}
          </DialogActions>
        </ValidatorForm>
        {shouldOpenPopupAssetFile && (
          <AssetFilePopup
            open={shouldOpenPopupAssetFile}
            handleClose={this.handleAssetFilePopupClose}
            getAssetDocument={this.getAssetDocument}
            handleUpdateAssetDocument={this.handleUpdateAssetDocument}
            item={{ ...this.state, id: this.state.dataAccreditation?.id }}
            t={t}
            i18n={i18next}
          />
        )}
        {this.state.setShowSelectAssetPopup && (
          <SelectAssetAllPopup
            t={t}
            open={this.state.setShowSelectAssetPopup}
            handleSelect={this.handleSelectAsset}
            handleClose={this.handleCloseSelectAssetPopup}
            assetVouchers={this.state?.dataAccreditation?.kdTaiSans}
            dataAccreditation={this.state.dataAccreditation}
            type={variable.listInputName.isVerification}
          />
        )}
        {this.state.idAttachmentDelete && (
          <ConfirmationDialog
            title={t("general.confirm")}
            open={this.state.idAttachmentDelete}
            onConfirmDialogClose={() =>
              this.setState({ idAttachmentDelete: null })
            }
            onYesClick={this.handelDeleteAttachment}
            text={t("general.deleteConfirm")}
            agree={t("general.agree")}
            cancel={t("general.cancel")}
          />
        )}
      </Dialog>
    );
  }
}

VerificationCalibrationDialog.contextType = AppContext;
export default VerificationCalibrationDialog;
