import React from "react";
import { Card } from "@material-ui/core";
import clsx from "clsx";

const SimpleCard = ({ children, title, subtitle, icon, toolbar, toolbarComponent }) => {
  return (
    <Card
      elevation={6}
      className="px-24 py-20 h-100 simple-card position-relative"
    >
      <div
        className={clsx(
          toolbar ? "flex flex-space-between" : ""
        )}
      >
        <div className="card-title">{title}</div>
        {toolbar && (toolbarComponent)}
      </div>
      {subtitle && (
        <div className="card-subtitle mb-24">{subtitle}</div>
      )}

      {children}
    </Card>
  );
};

export default SimpleCard;
