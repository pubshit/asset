import React, { Fragment } from "react";
import { Icon } from "@material-ui/core";
import { NavLink } from "react-router-dom";
import ConstantList from "../../app/appConfig";

const Breadcrumb = ({ routeSegments }) => {
  return (
    <div className="flex flex-middle position-relative breadcrumb-responsive">
      <NavLink to={ConstantList.ROOT_PATH}>
        <Icon className="text-middle mb-1" color="primary">
          home
        </Icon>
      </NavLink>
      {routeSegments
        ? routeSegments.map((route, index) => (
          <Fragment key={index}>
            <Icon className="text-hint">navigate_next</Icon>
            {route.path ? (
              <NavLink to={ConstantList.ROOT_PATH + route.path}>
                <span className="capitalize text-primary">{route.name}</span>
              </NavLink>
            ) : (
              <span className="capitalize text-dark-primary font-weight-bold">{route.name}</span>
            )}
          </Fragment>
        ))
        : null}
    </div>
  );
};

export default Breadcrumb;
