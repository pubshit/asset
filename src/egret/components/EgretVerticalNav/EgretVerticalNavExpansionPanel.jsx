import React, { Component, Fragment } from "react";
import { withStyles, Icon, Card, } from "@material-ui/core";
import TouchRipple from "@material-ui/core/ButtonBase";
import { withRouter } from "react-router-dom";
import ScrollBar from "react-perfect-scrollbar";
import clsx from "clsx";
import { LightTooltip } from "app/appFunction";

const styles = theme => {
  return {
    expandIcon: {
      transition: "transform 0.3s cubic-bezier(0, 0, 0.2, 1) 0ms",
      transform: "rotate(90deg)"
      // marginRight: "16px"
    },
    collapseIcon: {
      transition: "transform 0.3s cubic-bezier(0, 0, 0.2, 1) 0ms",
      transform: "rotate(0deg)"
      // marginRight: "16px"
    },
    "expansion-panel": {
      overflow: "hidden",
      transition: "max-height 0.3s cubic-bezier(0, 0, 0.2, 1)"
    },
    highlight: {
      background: theme.palette.primary.main
    }
  };
};

class EgretVerticalNavExpansionPanel extends Component {state = {
  collapsed: false,
  anchorEl: null,
  elementRef: null,
};
  elementRef = React.createRef();
  componentHeight = 20;
  componentTop = 0;

  handleClick = (e) => {
    this.handlePopoverOpen(e)
  };

  calcaulateHeight(node) {
    this.componentHeight += node?.clientHeight;
    return;
  }

  componentDidMount() {
    let { location, isFirstLevel, } = this.props;

    this.setState({
      isFirstLevel,
      // elCoordinates: this.elementRef?.getBoundingClientRect?.(),
    })
  }

  componentDidUpdate = (preProps, preState) => {
    if (preProps.closePopoverMenu !== this.props.closePopoverMenu) {
      this.setState({
        anchorEl: null,
      })
    }
    if(
      this.props.isNavClose
      && this.state.anchorEl
    ) {
      if (this.state.anchorEl || this.state.collapsed) {
        this.handlePopoverClose();
      }
      this.props.handlePopoverClose(); // close parent popover
    }
  }

  handlePopoverOpen = (event) => {
    this.setState({
      collapsed: true,
      anchorEl: event.currentTarget,
    }, () => {
      this.props.getChildrenCollapsed(this.state.collapsed)
    });
  };

  handlePopoverClose = () => {
    this.setState({
      anchorEl: null,
      collapsed: false,
      elementRef: null,
    })
  };

  calculationAnchorPosition = () => {
    let { anchorEl, elCoordinates } = this.state;
    let { isFirstLevel, children } = this.props;
    const viewportHeight = window.innerHeight - 37; // screen height - footer height
    const footerElement = document.querySelector('.footer');
    const footerCoordinates = footerElement?.getBoundingClientRect();
    const footerTop = footerCoordinates?.top;
    const footerHeight = footerCoordinates?.height;
    const initalElHeight = children?.length * 48 + 30
    const initalElBottom = initalElHeight + anchorEl?.getBoundingClientRect()?.top
    const top = initalElBottom < footerTop
      ? anchorEl?.getBoundingClientRect()?.top
      : (anchorEl?.getBoundingClientRect()?.top < (viewportHeight / 2))
        ? anchorEl?.getBoundingClientRect()?.top - (
        isFirstLevel ? 0 : 15 // subtract the padding of children for the right top position
      )
        : initalElHeight > (viewportHeight / 2)
          ? (viewportHeight / 2) // half of view height
          : footerTop - initalElHeight - (
          isFirstLevel ? 0 : 15 // subtract the padding of children for the right top position
        )
    let left = anchorEl?.getBoundingClientRect()?.right + 10; // sideNav width + padding

    return {
      top,
      bottom: initalElBottom > footerTop
        ? footerHeight
        : elCoordinates?.bottom,
      left,
      footerTop,
      height: initalElBottom > footerTop
        ? footerTop - top
        : elCoordinates?.height,
    }
  }

  getChildrenCollapsed = (collapsed) => {
    this.setState({
      isPostionRelative: collapsed
    })
  }

  handleGetElement = (el) => {
    let { anchorEl, collapsed } = this.state;
    if (anchorEl && collapsed && !this.state.elementRef) {
      this.setState({ elementRef: el }, ()=>{
        this.setState({
          elCoordinates: this.state.elementRef?.getBoundingClientRect(),
        })
      });
    }
  }

  render() {
    let {
      anchorEl,
      collapsed,
      isPostionRelative,
      isFirstLevel,
    } = this.state;
    let { classes, children } = this.props;
    let { name, icon, badge, description } = this.props.item;
    const { t, i18n } = this.props;
    let topAnchor = this.calculationAnchorPosition().top;
    const leftAnchor = this.calculationAnchorPosition().left;
    const bottomAnchor = this.calculationAnchorPosition().bottom;
    const heightAnchor = this.calculationAnchorPosition().height;
    const footerTop = this.calculationAnchorPosition().footerTop;
    return (
      <TouchRipple
        className={clsx(
          "nav-item flex flex-middle h-48 w-100",
          collapsed ? " parent-active" : "",
          isFirstLevel ? " flex-space-between nav-item-firstLevel" : "flex-start"
        )}
        key={this.props.index}
        tabIndex={this.props.index}
        onClick={this.handleClick}
        onMouseEnter={this.handlePopoverOpen}
        onMouseLeave={this.handlePopoverClose}
      >
        {collapsed && anchorEl && (
          <div style={{
            position: "fixed",
            top: topAnchor,
            left: leftAnchor - 25,
            height: heightAnchor,
            width: 30,
            zIndex: 100000,
          }}></div>
        )}
        {icon && (
          <Icon className="text-middle item-icon">{icon}</Icon>
        )}
        <span
          className={clsx(
            "text-middle w-100 item-text pl-2",
            isFirstLevel
              ? ""
              : icon
                ? "pr-10"
                : "pr-20",
          )}
        >
          {!isFirstLevel ? (
            <LightTooltip
              title={description}
            >
              <div>
                {t(name)}
              </div>
            </LightTooltip>
          ) : (
            t(name)
          )}
        </span>
        {badge && (
          <div className={`badge bg-${badge.color}`}>{badge.value}</div>
        )}
        <div
          className={
            collapsed
              ? classes.collapseIcon + " item-arrow"
              : classes.expandIcon + " item-arrow"
          }
        >
          <Icon className="text-middle">chevron_right</Icon>
        </div>
        {collapsed && (
          <Card
            ref={this.handleGetElement}
            className="p-10 bg-white anchor-container"
            onMouseLeave={this.handlePopoverClose}
            elevation={4}
            style={{
              position: 'fixed',
              opacity: anchorEl ? 1 : 0,
              top: topAnchor,
              bottom: bottomAnchor,
              left: leftAnchor,
              height: heightAnchor,
              maxHeight: footerTop - topAnchor,
              boxShadow: isPostionRelative ? "none !important" : "",
              transition: anchorEl ? `opacity 500ms` : 'opacity 0ms',
            }}
          >
            <div className="shadow shadow-top"></div>
            <div className="shadow shadow-bottom"></div>
            <ScrollBar className="scrollable mb-0 h-100 position-relative">
              {React.Children.map(children, (child) => {
                if (React.isValidElement(child)) {
                  return React.cloneElement(
                    child,
                    {
                      getChildrenCollapsed: this.getChildrenCollapsed,
                      handlePopoverClose: this.handlePopoverClose // todo: pass props to children to close all menu popovers
                    }
                  );
                }
                return child;
              })}
            </ScrollBar>
          </Card>
        )}
      </TouchRipple>
    );
  }
}

export default withRouter(withStyles(styles)(EgretVerticalNavExpansionPanel));
