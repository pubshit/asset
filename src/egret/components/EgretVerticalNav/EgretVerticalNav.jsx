import React, {Component} from "react";
import { NavLink } from "react-router-dom";
import { Icon } from "@material-ui/core";
import TouchRipple from "@material-ui/core/ButtonBase";
import EgretVerticalNavExpansionPanel from "./EgretVerticalNavExpansionPanel";
import { withStyles } from "@material-ui/styles";
import { useTranslation, withTranslation, Trans } from "react-i18next";
import ConstantList from "../../../app/appConfig.js";
import clsx from "clsx";
import { LightTooltip } from "app/appFunction";

const ViewEgretVerticalNavExpansionPanel = withTranslation()(
  EgretVerticalNavExpansionPanel
);
const styles = theme => ({
  expandIcon: {
    transition: "transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms",
    transform: "rotate(90deg)"
  },
  collapseIcon: {
    transition: "transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms",
    transform: "rotate(0deg)"
  }
});

class EgretVerticalNav extends Component {
  state = {
    collapsed: true,
    level: 1,
    currentIndex: null,
    isCloseOthersMenu: false,
  };

  renderLevels = (data, level) => {
    const {
      t,
      isNavClose,
      handlePopoverClose = () => {},
      getChildrenCollapsed = () => {}
    } = this.props;
    const isFirstLevel = level === this.state.level

    return data.map((item, index) => {
      if (
        item.path && item.path.length > 0 &&
        !item.path.startsWith(ConstantList.ROOT_PATH)
      ) {
        item.path = ConstantList.ROOT_PATH + item.path;
      }
      let visible = item.isVisible;
      if (item.children && item.children.length > 0) {
        return (
          <ViewEgretVerticalNavExpansionPanel
            key={index}
            item={item}
            index={index}
            level={level}
            isNavClose={isNavClose}
            isFirstLevel={isFirstLevel}
            getChildrenCollapsed={getChildrenCollapsed}
            handlePopoverClose={handlePopoverClose}
          >
            {this.renderLevels(item.children, level + 1)}
          </ViewEgretVerticalNavExpansionPanel>
        );
      } else if (visible) {
        if (item.path == null) {
          item.path = "";
        }
        return (
          <NavLink key={index} to={item.path} className="nav-item flex">
            <TouchRipple key={item.name} name="child" className="w-100 h-48 flex-middle">
              {(() => {
                if (item.icon) {
                  return (
                    <Icon className="item-icon text-middle icon-text">{item.icon}</Icon>
                  );
                } else if (item.iconText) {
                  return (
                    <span className="item-icon icon-text">{item.iconText}</span>
                  );
                }
              })()}
              <span
                className={clsx(
                  "text-middle w-100 item-text",
                  isFirstLevel
                    ? ""
                    : item.icon || item.iconText
                      ? "pr-10"
                      : "pr-20 pl-40",
                )}
              >
                {!isFirstLevel ? (
                  <LightTooltip
                    title={item.description}
                  >
                    <div>
                      {t(item.name)}
                    </div>
                  </LightTooltip>
                ) : (
                  t(item.name)
                )}
              </span>
              <div className="mx-auto"></div>
              {item.badge && (
                <div className={`badge bg-${item.badge.color}`}>
                  {item.badge.value}
                </div>
              )}
            </TouchRipple>
          </NavLink>
        );
      }
    });
  };

  handleClick = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  render() {
    return (
      <div className="navigation">
        {this.renderLevels(this.props.navigation, this.state.level)}
      </div>
    );
  }
}

export default withStyles(styles)(EgretVerticalNav);
