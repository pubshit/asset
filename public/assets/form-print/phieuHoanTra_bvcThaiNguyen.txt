<div class="form-print" style="font-size: 18px;">
    <div style="display: flex; justify-content: space-between;">
        <div style="display: flex;">
            <div>
                <div style="text-transform: uppercase;">SỞ Y TẾ {{addressOfEnterprise}}</div>
                <div style="font-weight: bold; margin-bottom: 0px;">{{currentUser.org.name}}</div>
            </div>
        </div>
        <div style="min-width: 170px;">
            Số phiếu: {{code}}<br />
            Ngày lập: {{suggestDate}}
        </div>
    </div>
    <div style="margin-top: 30px;">
        Nơi nhập: {{receiptDepartmentName}}<br />
        Nơi xuất: {{departmentName}}
    </div>
    <div>
        <div style="text-align: center; margin-top: 10px;">
            <div>
                <div class="form-print-title" style="font-weight: bold; text-transform: uppercase; text-align: center;">
                    PHIẾU HOÀN TRẢ TÀI SẢN, TRANG THIẾT BỊ, DỤNG CỤ
                </div>
            </div>
        </div>
        <div style="margin-bottom: 30px; margin-top: 0px; text-align: center;"><i>Ngày chứng từ: {{issueDate}}</i></div>
    </div>
    <div>
        <table style="width: 100%; border: 1px solid; border-collapse: collapse;">
            <thead>
                <tr>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 3%; text-align: center;">STT</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 20%;">Tên thiết bị , tài sản, dụng cụ</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 12%;">ĐVT</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 12%;">Nước sản xuất</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 12%;">Hãng sản xuất</th>
                    <th colspan="2" style="border: 1px solid; font-size: 14px; width: 12%;">Số lượng</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 7%; text-align: center;">Đơn giá</th>
                    <th rowspan="2" style="border: 1px solid; font-size: 14px; width: 7%; text-align: center;">Thành tiền</th>
                </tr>
                <tr>
                    <th style="border: 1px solid; padding: 0px 5px; font-size: 14px; width: 6%;">Yêu cầu</th>
                    <th style="border: 1px solid; padding: 0px 5px; font-size: 14px; width: 6%;">Phát</th>
                </tr>
            </thead>
            <tbody>
                {{#assetVouchers}}
                    <tr>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{index}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: left;">{{name}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{skuName}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{madeIn}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{manufacturerName}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{dxRequestQuantity}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;">{{quantity}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: right;">{{price}}</td>
                        <td style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: right;">{{originalCost}}</td>
                    </tr>
                {{/assetVouchers}}
                <tr>
                    <td colspan="5" style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;"><b>Cộng khoản: {{assetVouchers.length}}</b></td>
                    <td colspan="1" style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;"><b>{{totalRequestQuantity}}</b></td>
                    <td colspan="1" style="border: 1px solid; padding: 0px 5px; font-size: 0.975rem; text-align: center;"><b>{{totalQuantity}}</b></td>
                    <td colspan="2" style="padding: 0px 5px; font-size: 0.975rem; text-align: right;"><b>{{totalPrice}}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="display: flex; justify-content: end; margin-top: 20px;">
        <div style="font-size: inherit; font-style: italic; display: block;"><span> </span><span>Ngày&nbsp;{{day}}&nbsp; tháng&nbsp;{{month}}&nbsp; năm&nbsp;{{year}}</span></div>
    </div>
    <div style="display: flex;justify-content: space-between;">
        <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
            <div class="signature-title mt-20" style="text-transform: none; font-weight: bold; margin-top: 0px;">Trưởng phòng {{signDepartmentName}}</div>
            <div style="font-style: italic;">(Ký, họ tên)</div>
        </div>
        <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
            <div class="signature-title mt-20" style="text-transform: none; font-weight: bold; margin-top: 0px;">Người nhận</div>
            <div style="font-style: italic;">(Ký, họ tên)</div>
        </div>
        <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
            <div class="signature-title mt-20" style="text-transform: none; font-weight: bold; margin-top: 0px;">Người trả</div>
            <div style="font-style: italic;">(Ký, họ tên)</div>
        </div>
        <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
            <div class="signature-title mt-20" style="text-transform: none; font-weight: bold; margin-top: 0px;">
                Trưởng khoa/phòng sử dụng
            </div>
            <div style="font-style: italic;">(Ký, họ tên)</div>
        </div>
    </div>
</div>
