<div class="form-print">
    <div style="text-align: right; margin-bottom: 10px;">BM.QT.01.QTTTB.03-09</div>
    <div>
        <div style="display: flex; justify-content: space-between;">
            <div style="display: flex; align-items: center; flex-direction: column; width: 50%; text-transform: uppercase;">
                <div style="display: flex; align-items: center; justify-content: center;">
                    <div style="display: flex; flex-direction: column; text-align: center;">
                        <div style="margin: 0px; text-underline-offset: 5px;">
                            SỞ Y TẾ {{organization.province}}
                        </div>
                    </div>
                </div>
                <div style="display: flex; align-items: center; justify-content: center;">
                    <div style="display: flex; flex-direction: column; text-align: center;">
                        <div style="font-weight: bold; margin: 0px; text-underline-offset: 5px;">
                            {{organization.org.name}}
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 60%; display: flex; flex-direction: column; align-items: flex-end;">
                <div>
                    <div style="display: flex; flex-direction: column; align-items: center;">
                        <div style="margin: 0px; font-weight: bold; text-underline-offset: 5px;">
                            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
                        </div>
                        <div style="margin: 0px; font-weight: bold; text-underline-offset: 5px;">
                            <div>Độc lập - Tự do - Hạnh phúc</div>
                            <div style="width: 70%; margin: 0 auto; height: 1px; background: #000;"></div>
                        </div>
                    </div>
                </div>
                <div style="margin: 16px 0 0 0; text-align: right;">
                    <div style="font-style: italic;">{{organization.province}}, Ngày {{day}} tháng {{month}} năm {{year}}</div>
                </div>
            </div>
        </div>

        <div style="text-align: center; margin-bottom: 20px; margin-top: 20px; font-weight: bold; text-transform: uppercase; line-height: 1.3;" class="form-print-title">
            Biên bản kiểm kê trang thiết bị
        </div>

        <div style="font-size: 1.07rem; margin-bottom: 8px;">
            <div style="font-size: 1.07rem;">
                &nbsp;&nbsp; Thời điểm kiểm kê: ............. giờ ..... ngày {{day}} tháng {{month}} năm {{year}}. Hội đồng kiểm kê gồm:
            </div>
            {{#inventoryCountPersons}}
                <div style="font-size: 1.07rem;">
                    &nbsp;&nbsp; {{index}}. Ông (bà): {{personName}}. Chức vụ: {{position}}. Đại diện: {{departmentName}}. {{role}}
                </div>
            {{/inventoryCountPersons}}
            <div style="font-size: 1.07rem;">
                &nbsp;&nbsp; Đã kiểm kê trang thiết bị y tế, kết quả như sau:
            </div>
        </div>
    </div>
    <div style="font-size: 1.07rem;">
        <table style="width: 100%; border-collapse: collapse; border: 1px solid; font-size: 1rem;">
            <thead style="border: 1px solid;">
                <tr style="border: 1px solid;">
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 2%;">TT</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 15%;">TÊN CÔNG CỤ DỤNG CỤ</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 6%;">ĐVT</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 15%;">MODEL</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 5%;">NHÀ SẢN XUẤT</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">NƯỚC SẢN XUẤT</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">NĂM SX</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">SỐ MÁY</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">SL SỔ SÁCH</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">SL THỰC TẾ</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">THỪA</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">THIẾU</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">TÌNH TRẠNG THIẾT BỊ</th>
                    <th style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; width: 10%;">VỊ TRÍ ĐẶT THIẾT BỊ</th>
                </tr>
            </thead>

            <tbody>
                {{#assets}}
                <tr style="border: 1px solid;">
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{index}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{asset.name}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{asset.unit.name}} </td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{asset.model}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{asset.manufacturer.name}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{asset.madeIn}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{asset.yearOfManufacture}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{inventoryQuantity}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{accountantQuantity}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{isExcess}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem; text-align: center;">{{isDeficiency}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;">{{currentStatus.name}}</td>
                    <td style="border-left: 1px solid; padding: 4px; font-size: 0.85rem;"></td>
                </tr>
                {{/assets}}
            </tbody>
        </table>
    </div>
     <div style="font-size: 1.07rem; margin-top: 10px;">
        <div>&nbsp;&nbsp; Ý kiến: ...................................</div>
        <div>&nbsp;&nbsp; Các thành viên (Ký ghi rõ họ tên)</div>
    </div>
    <div style="display: grid; grid-template-columns: 1fr 1fr 1fr;">
        <div>
            <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
                <div class="signature-title" style="text-transform: unset; font-weight: bold; margin-top: 10px;">Khoa/phòng được kiểm kê</div>
            </div>
        </div>
        <div>
            <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
                <div class="signature-title" style="text-transform: unset; font-weight: bold; margin-top: 10px;">Khoa dược-VTTB</div>
            </div>
        </div>
        <div>
            <div class="signature-container" style="text-align: center; font-size: 16px; padding-bottom: 80px;">
                <div class="signature-title" style="text-transform: unset; font-weight: bold; margin-top: 10px;">Phòng TCKT</div>
            </div>
        </div>
    </div>
</div>