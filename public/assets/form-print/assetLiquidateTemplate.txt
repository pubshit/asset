
<div class="MuiGrid-root">
    <div class="form-print" style="text-align: center; width: 21cm; margin: auto; font-size: 18px;">
        <div style="display: flex; justify-content: space-between;">
            <div style="display: flex; flex-direction: column; align-content: space-between; align-items: flex-start;">
                <div style="font-weight: bold; margin-bottom: 0px;">Đơn vị: {{orgName}}</div>
                <div style="font-weight: bold; margin-top: 0px;">Mã QHNS: {{budgetCode}}</div>
            </div>
            <div style="display: flex; flex-direction: column; align-items: flex-end;"><div style="font-weight: bold; margin-bottom: 0px;">Mẫu số: C51-HD</div></div>
        </div>
        <div style="margin: 30px 0px;">
            <div style="margin-top: 30px;">
                <div class="form-print-title" style="font-weight: bold; text-transform: uppercase; text-align: center;">BIÊN BẢN THANH LÝ {{nameByType}}</div>
                <div style="font-style: italic;">Số:&nbsp;{{code}}</div>
            </div>
        </div>
        <div style="text-align: left;"><div>Căn cứ Quyết định số: {{decisionCode}}&nbsp; ngày ..... tháng ..... năm...... của ................................. về việc thanh lý {{nameByTypeLong}}.</div></div>
        <div style="text-align: left;">
            <div style="text-align: left; font-weight: bold; margin-bottom: 0px;">I. Ban thanh lý {{nameByType}} gồm:</div>
            <div style="padding-left: 20px;">
                <div style="display: flex;">
                    <div style="flex: 1 1 0%;">Ông/Bà: {{handoverPersonName}}.</div>
                    <div style="flex: 1 1 0%;">Chức vụ:</div>
                    <div style="flex: 1 1 0%;">Đại diện: {{handoverDepartmentName}}.</div>
                </div>
                {{#persons}}
                    <div style="display: grid; grid-template-columns: 1fr 1fr 1fr;">
                        <div>Ông/Bà: {{personName}}.</div>
                        <div>Chức vụ: {{position}}.</div>
                        <div>Đại diện: {{departmentName}}.</div>
                    </div>
                {{/persons}}
            </div>

        </div>
        <div style="text-align: left;">
            <div style="text-align: left; font-weight: bold; margin-bottom: 0px;">II. Tiến hành thanh lý {{nameByType}} :</div>
            <div style="padding-left: 20px; padding-bottom: 5px;">
                <div>Lý do thanh lý:</div>
                {{{reason}}}
            </div>
           <table class="table-report" style="width: 100%; border: 1px solid; border-collapse: collapse;">
                <thead>
                    <tr>
                        <th style="border: 1px solid; width: 20%; text-align: center;">Tên TSCĐ</th>
                        <th style="border: 1px solid; width: 15%; text-align: center;">Số hiệu TSCĐ</th>
                        {{#isNotTSCD}}
                            <th style="{{style.w_5}}">Số Lượng</th>
                        {{/isNotTSCD}}
                        <th style="border: 1px solid; width: 11%; text-align: center;">Nước SX/ HSX</th>
                        <th style="{{#isTSCD}}{{style.w_5}}{{/isTSCD}}{{^isTSCD}}{{style.w_9}}{{/isTSCD}}">NSX</th>
                        <th style="{{#isTSCD}}{{style.w_5}}{{/isTSCD}}{{^isTSCD}}{{style.w_9}}{{/isTSCD}}">NSD</th>
                        <th style="border: 1px solid; width: 11%; text-align: center;">{{costName}}</th>
                        {{#isTSCD}}
                            <th style="{{style.w_11}}">KHLK</th>
                            <th style="{{style.w_11}}">GTCL</th>
                        {{/isTSCD}}
                    </tr>
                </thead>
                <tbody>
                    {{#assetVouchers}}
                        <tr>
                            <td style="border: 1px solid; padding: 5px;">{{asset.name}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: left;">
                                <div><b>Mã TS: </b>{{asset.code}}</div>
                                <div><b>Số seri: </b>{{assetSerialNumber}}</div>
                                <div><b>Model: </b>{{assetModel}}</div>
                            </td>
                            {{#isNotTSCD}}
                                <th style="border: 1px solid; padding: 5px; text-align: center;">{{asset.quantity}}</th>
                            {{/isNotTSCD}}
                            <td style="border: 1px solid; padding: 5px; text-align: center;">{{asset.madeIn}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: center;">{{asset.yearOfManufacture}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: center;">{{asset.yearPutIntoUse}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: right;">{{originalCost}}</td>
                            {{#isTSCD}}
                                <td style="border: 1px solid; padding: 5px; text-align: right;">{{KHLK}}</td>
                                <td style="border: 1px solid; padding: 5px; text-align: right;">{{carryingAmount}}</td>
                            {{/isTSCD}}
                        </tr>
                    {{/assetVouchers}}
                </tbody>
            </table>

        </div>
        <div style="text-align: left;">
            <div style="text-align: left; font-weight: bold; margin-bottom: 0px;">III. Kết luận của Ban thanh lý {{nameByType}} :</div>
            <div style="padding-left: 20px; padding-bottom: 5px;">
                {{{conclude}}}
            </div>
        </div>
        <div style="text-align: left;">
            <div class="pt-20" style="display: grid; grid-template-columns: 1fr 1fr;">
                <div></div>
                <div class="signature-container" style="text-align: center; font-size: 14px; padding-bottom: 80px; margin-top: 11px;">
                    <div class="date-signature">
                        <div style="font-size: inherit; font-style: italic; display: block;"><span> </span><span>Ngày&nbsp;{{day}}&nbsp; tháng&nbsp;{{month}}&nbsp; năm&nbsp;{{year}}</span></div>
                    </div>
                    <div class="signature-title" style="text-transform: uppercase; font-weight: bold; font-size: 18px; margin-top: 10px;">TRƯỞNG BAN THANH LÝ</div>
                    <div style="font-style: italic;">(Ký, ghi rõ họ tên)</div>
                </div>
            </div>
        </div>
        <div style="text-align: left;">
            <div style="text-align: left; font-weight: bold; margin-bottom: 0px;">IV. Kết quả thanh lý {{nameByType}} gồm:</div>
            <ul class="form-print-list">
                <li>
                    <span>Chi phí thanh lý {{nameByType}} : {{liquidationCost}}&nbsp;đồng.</span>
                    <div>Viết bằng chữ:&nbsp;{{liquidationCostToWords}}&nbsp;đồng.</div>
                </li>
                <li style="margin-top: 10px;">
                    <span>Giá trị thu hồi: {{recoveryValue}}&nbsp;đồng.</span>
                    <div>Viết bằng chữ:&nbsp;{{recoveryValueToWords}}&nbsp;đồng.</div>
                </li>
                <li style="margin-top: 10px;"><span>Đã ghi giảm sổ {{nameByType}} ngày .......... </span><span>tháng .......... </span><span>năm .......... </span></li>
            </ul>
        </div>
        <div style="text-align: left;">
            <div class="pt-20" style="display: grid; grid-template-columns: 1fr 1fr;">
                <div class="mt-30">
                    <div class="signature-container" style="text-align: center; font-size: 14px; padding-bottom: 80px;">
                        <div class="signature-title mt-20" style="text-transform: uppercase; font-weight: bold; font-size: 18px; margin-top: 45px;">THỦ TRƯỞNG ĐƠN VỊ</div>
                        <div style="font-style: italic;">(Ký, họ tên, đóng dấu)</div>
                    </div>
                </div>
                <div class="m-0" style="margin-top: 7px;">
                    <div class="signature-container" style="text-align: center; font-size: 14px; padding-bottom: 80px; margin-top: 11px;">
                        <div class="date-signature">
                            <div style="font-size: inherit; font-style: italic; display: block;"><span> </span><span>Ngày........&nbsp;tháng........ &nbsp;năm........</span></div>
                        </div>
                        <div class="signature-title" style="text-transform: uppercase; font-weight: bold; font-size: 18px; margin-top: 10px;">KẾ TOÁN TRƯỞNG</div>
                        <div style="font-style: italic;">(Ký, họ tên)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>