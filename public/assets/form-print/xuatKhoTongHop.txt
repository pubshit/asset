{{#item.voucherDetails}}
    <div class="form-print">
        <div style="text-align: center;">
            <div style="display: flex; justify-content: space-between;">
                <div style="text-align: left; max-width: 50%;">
                    <div>{{titleName}}</div>
                    <div style="font-weight: bold;">Mã QHNS: {{budgetCode}}</div>
                </div>
                <div style="max-width: 50%;">
                    <div style="font-weight: bold;">Mẫu số C31 - HD</div>
                    <i>(Ban hành kèm theo Thông tư <br>107/2017/TT-BTC ngày 24/11/2017)</i>
                </div>
            </div>
            <div style="display: flex; justify-content: center; margin: 30px;">
                <div>
                    <div style="font-weight: bold;">PHIẾU XUẤT KHO</div>
                    <div style="text-align: left;">
                        <div>Ngày {{day}} tháng {{month}} năm {{year}}</div>
                        <div>Số: {{code}}</div>
                    </div>
                </div>
            </div>
            <div style="text-align: left;">
                <table style="width: 100%; border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <th style="font-weight: normal; text-align: left;">- Họ tên người nhận hàng: {{receiverPerson.displayName}}.</th>
                            <th style="font-weight: normal; text-align: left;">Địa chỉ (bộ phận): {{receiverDepartment.name}}.</th>
                        </tr>
                        <tr>
                            <th style="font-weight: normal; text-align: left;">- Lý do xuất kho: {{reason}}</th>
                        </tr>
                        <tr>
                            <th style="font-weight: normal; text-align: left;">- Xuất tại kho (ngăn lô): {{stockReceiptDeliveryStore.name}}.</th>
                            <th style="font-weight: normal; text-align: left;">Địa điểm: {{stockReceiptDeliveryStore.address}}.</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table style="width: 100%; border: 1px solid; padding: 5px; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th rowspan="2" style="border: 1px solid; padding: 5px;">STT</th>
                            <th rowspan="2" style="border: 1px solid; padding: 5px;">Tên, nhãn hiệu, quy cách, phẩm chất</th>
                            <th rowspan="2" style="border: 1px solid; padding: 5px;">Mã số</th>
                            <th rowspan="2" style="border: 1px solid; padding: 5px;">Đơn vị tính</th>
                            <th colspan="2" style="border: 1px solid; padding: 5px; text-align: center;">Số lượng</th>
                            <th rowspan="2" style="border: 1px solid; padding: 5px; text-align: center;">Đơn giá(VNĐ)</th>
                            <th rowspan="2" style="border: 1px solid; padding: 5px; text-align: center;">Thành tiền(VNĐ)</th>
                        </tr>
                        <tr>
                            <th style="border: 1px solid; padding: 5px;">Yêu cầu</th>
                            <th style="border: 1px solid; padding: 5px;">Thực xuất</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{#voucherDetails}}
                            <tr>
                                <td style="border: 1px solid; padding: 5px; text-align: center;">{{index}}</td>
                                <td style="border: 1px solid; padding: 5px; text-align: left;">{{productName}}</td>
                                <td style="border: 1px solid; padding: 5px;">{{productCode}}</td>
                                <td style="border: 1px solid; padding: 5px;">{{unitName}}</td>
                                <td style="border: 1px solid; padding: 5px;"></td>
                                <td style="border: 1px solid; padding: 5px; text-align: center;">{{totalQuantityExport}}</td>
                                <td style="border: 1px solid; padding: 5px; text-align: right;">{{price}}</td>
                                <td style="border: 1px solid; padding: 5px; text-align: right;">{{amount}}</td>
                            </tr>
                        {{/voucherDetails}}
                        <tr>
                            <td style="border: 1px solid; padding: 5px;"></td>
                            <td style="border: 1px solid; padding: 5px; font-weight: bold; text-align: left;">Cộng</td>
                            <td style="border: 1px solid; padding: 5px;"></td>
                            <td style="border: 1px solid; padding: 5px;"></td>
                            <td style="border: 1px solid; padding: 5px;"></td>
                            <td style="border: 1px solid; padding: 5px; text-align: center;">{{totalQuantity}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: right; font-weight: bold;">{{totalPrice}}</td>
                            <td style="border: 1px solid; padding: 5px; text-align: right; font-weight: bold;">{{totalAmount}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: left; margin-top: 4px;">
                Tổng số tiền (viết bằng chữ): {{totalAmountToWords}}&nbsp;đồng.<br />
                Số chứng từ kèm theo:
            </div>
            <div style="text-align: right;"><i>Ngày &nbsp;&nbsp; tháng &nbsp;&nbsp; năm {{year}}</i></div>
            <div style="display: flex; justify-content: space-between; min-height: 200px;">
                <div>
                    <div style="font-weight: bold;">Người lập phiếu</div>
                    <i>(Ký, họ tên)</i>
                </div>
                <div>
                    <div style="font-weight: bold;">Người nhận hàng</div>
                    <i>(Ký, họ tên)</i>
                </div>
                <div>
                    <div style="font-weight: bold;">Thủ kho</div>
                    <i>(Ký, họ tên)</i>
                </div>
                <div>
                    <div style="font-weight: bold;">Kế toán trưởng</div>
                    <i>(Ký, họ tên)</i>
                </div>
                <div>
                    <div style="font-weight: bold;">Thủ trưởng đơn vị</div>
                    <i>(Ký, họ tên)</i>
                </div>
            </div>
        </div>
    </div>
   <div style="page-break-before:always"></div>
{{/item.voucherDetails}}